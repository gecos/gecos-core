/**
 */
package gecos.dag;

import gecos.core.GecosNode;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visitable</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.dag.DagPackage#getDagVisitable()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface DagVisitable extends GecosNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 * @generated
	 */
	void accept(DagVisitor visitor);

} // DagVisitable
