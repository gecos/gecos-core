/**
 */
package gecos.blocks;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see gecos.blocks.BlocksPackage
 * @generated
 */
public interface BlocksFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BlocksFactory eINSTANCE = gecos.blocks.impl.BlocksFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Basic Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Basic Block</em>'.
	 * @generated
	 */
	BasicBlock createBasicBlock();

	/**
	 * Returns a new object of class '<em>Composite Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Composite Block</em>'.
	 * @generated
	 */
	CompositeBlock createCompositeBlock();

	/**
	 * Returns a new object of class '<em>Control Edge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Control Edge</em>'.
	 * @generated
	 */
	ControlEdge createControlEdge();

	/**
	 * Returns a new object of class '<em>Data Edge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Edge</em>'.
	 * @generated
	 */
	DataEdge createDataEdge();

	/**
	 * Returns a new object of class '<em>If Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>If Block</em>'.
	 * @generated
	 */
	IfBlock createIfBlock();

	/**
	 * Returns a new object of class '<em>While Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>While Block</em>'.
	 * @generated
	 */
	WhileBlock createWhileBlock();

	/**
	 * Returns a new object of class '<em>Do While Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Do While Block</em>'.
	 * @generated
	 */
	DoWhileBlock createDoWhileBlock();

	/**
	 * Returns a new object of class '<em>For Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>For Block</em>'.
	 * @generated
	 */
	ForBlock createForBlock();

	/**
	 * Returns a new object of class '<em>For C99 Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>For C99 Block</em>'.
	 * @generated
	 */
	ForC99Block createForC99Block();

	/**
	 * Returns a new object of class '<em>Simple For Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simple For Block</em>'.
	 * @generated
	 */
	SimpleForBlock createSimpleForBlock();

	/**
	 * Returns a new object of class '<em>Switch Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Switch Block</em>'.
	 * @generated
	 */
	SwitchBlock createSwitchBlock();

	/**
	 * Returns a new object of class '<em>Case Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Case Block</em>'.
	 * @generated
	 */
	CaseBlock createCaseBlock();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	BlocksPackage getBlocksPackage();

} //BlocksFactory
