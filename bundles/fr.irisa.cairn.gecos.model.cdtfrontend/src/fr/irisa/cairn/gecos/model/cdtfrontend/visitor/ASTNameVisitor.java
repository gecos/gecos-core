/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.cdtfrontend.visitor;

import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.cdt.core.dom.ast.IASTImplicitName;
import org.eclipse.cdt.core.dom.ast.IASTName;
import org.eclipse.cdt.core.dom.ast.IASTNode;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTConversionName;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTOperatorName;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTQualifiedName;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTTemplateId;

/**
 * @author mnaullet
 */
public class ASTNameVisitor {

	private CDTToGecosModel gecosModel;
	private Map<Integer, Object> instructions;
	private String value;
	
	public void visit(CDTToGecosModel gecosModel, IASTName name) {
		this.gecosModel = gecosModel;
		if (name instanceof IASTImplicitName)
			buildIASTImplicitName((IASTImplicitName)name);
		else if (name instanceof ICPPASTConversionName)
			buildICPPASTConversionName((ICPPASTConversionName)name);
		else if (name instanceof ICPPASTOperatorName)
			buildICPPASTOperatorName((ICPPASTOperatorName)name);
		else if (name instanceof ICPPASTQualifiedName)
			buildICPPASTQualifiedName((ICPPASTQualifiedName)name);
		else if (name instanceof ICPPASTTemplateId)
			buildICPPASTTemplateId((ICPPASTTemplateId)name);
		else
			buildIASTName(name);
	}
	
	public String getName() {
		return value;
	}
	
	public Map<Integer, Object> getInstructions() {
		return instructions;
	}
	
	private void buildIASTName(IASTName name) {
		value = name.toString();
	}
	
	private void buildICPPASTTemplateId(ICPPASTTemplateId name) {
		instructions = new LinkedHashMap<Integer, Object>();
		name.getTemplateName().accept(gecosModel);
		value = ((ASTNameVisitor)gecosModel.getObject()).getName();
		int i = 0;
		for(IASTNode node : name.getTemplateArguments()) {
			node.accept(gecosModel);
			if (gecosModel.getObject() instanceof ASTExpressionVisitor)
				if (((ASTExpressionVisitor)gecosModel.getObject()).getInstruction() == null)
					instructions.put(i, ((ASTExpressionVisitor)gecosModel.getObject()).getName());
				else
					instructions.put(i, ((ASTExpressionVisitor)gecosModel.getObject()).getInstruction());
			else if (gecosModel.getObject() instanceof ASTTypeIdVisitor) {
				instructions.put(i, ((ASTTypeIdVisitor)gecosModel.getObject()).getString());
			}
			i++;
		}
	}
	
	private void buildIASTImplicitName(IASTImplicitName name) {
		notYetSupported(name);
	}

	private void buildICPPASTConversionName(ICPPASTConversionName name) {
		notYetSupported(name);
	}
	
	private void buildICPPASTOperatorName(ICPPASTOperatorName name) {
		notYetSupported(name);
	}
	
	private void buildICPPASTQualifiedName(ICPPASTQualifiedName name) {
		notYetSupported(name);
	}
	
	private static void notYetSupported(IASTName name) {
		notYetSupported(name);
	}
	
}