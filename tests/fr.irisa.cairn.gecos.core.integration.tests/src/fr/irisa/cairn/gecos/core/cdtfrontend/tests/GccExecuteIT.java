package fr.irisa.cairn.gecos.core.cdtfrontend.tests;

import org.junit.Test;

import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.core.testframework.impl.GSProjectVersion;
import fr.irisa.cairn.gecos.core.testframework.impl.GecosS2STestTemplate;
import fr.irisa.cairn.gecos.model.cdtfrontend.CDTFrontEnd;
import fr.irisa.cairn.gecos.testframework.data.ICxxProjectData;
import fr.irisa.cairn.gecos.testframework.dataprovider.DataFromPathProvider;
import fr.irisa.cairn.gecos.testframework.dataprovider.ResourcesLocation;
import gecos.gecosproject.GecosProject;

/**
 * Tests C front-end {@link CDTFrontEnd} and the C code generator.
 * 
 * @author aelmouss
 */
public class GccExecuteIT extends GecosS2STestTemplate<GSProjectVersion> {
	
	/**
	 * The project is the same in both versions,
	 * however for the original version the codegenerator does not generate 
	 * code from the {@link GecosProject} but rather copy original sources.
	 */
	@Test
	@UseDataProvider(location = DataFromPathProvider.class, value = DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation(bundleName="fr.irisa.cairn.gecos.testframework.resources", value = "src-c/single/gcc/execute/pass", limit = 30)
//	@ResourcesLocation(bundleName="fr.irisa.cairn.gecos.testframework.resources", value = "src-c/single/gcc/execute/fail")
	public void gccExecute(ICxxProjectData d) {
		/**
		 * TODO we should only skip on "not-yet-supported" exception in CDTFrontend
		 * instead of all CDTParserProblemException this way we only skip
		 * the code that fails to parse because it is not yet supported, while
		 * code that fails for other reasons should be reported as Failure.
		 */
		skipOnParsingFail = true; //skip tests that cannot be parsed.  
		runTest(d, p -> {}); 
	}
	
}
