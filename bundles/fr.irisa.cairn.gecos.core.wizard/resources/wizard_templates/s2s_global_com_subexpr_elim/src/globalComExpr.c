#include <stdio.h>
#define M 150

void testGlobal_CSE(int a, int b, int res[6], int f[M], int g[M]) {
  int c, d, e, i, j;
  {
    c = a + b;
    d = a * c;
    e = d * d;
    i = 1;
  }
  do {
    f[i] = a + b;
    c = c * 2;
    if (c > d)
      g[i] = a * c;
    else
      g[i] = d * d;
     i = i + 1;
  } while (i < M) ;
}
