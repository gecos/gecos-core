/**
 */
package gecos.dag;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Vector Extract Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.DAGVectorExtractNode#getPos <em>Pos</em>}</li>
 * </ul>
 *
 * @see gecos.dag.DagPackage#getDAGVectorExtractNode()
 * @model
 * @generated
 */
public interface DAGVectorExtractNode extends DAGVectorNode, DagVisitable {
	/**
	 * Returns the value of the '<em><b>Pos</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pos</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pos</em>' attribute.
	 * @see #setPos(int)
	 * @see gecos.dag.DagPackage#getDAGVectorExtractNode_Pos()
	 * @model default="0" unique="false" dataType="gecos.dag.int"
	 * @generated
	 */
	int getPos();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGVectorExtractNode#getPos <em>Pos</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pos</em>' attribute.
	 * @see #getPos()
	 * @generated
	 */
	void setPos(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGVectorExtractNode(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.dag.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _pos = this.getPos();\n&lt;%java.lang.String%&gt; _plus = (\"Ext(\" + &lt;%java.lang.Integer%&gt;.valueOf(_pos));\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \")_\");\nint _ident = this.getIdent();\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + &lt;%java.lang.Integer%&gt;.valueOf(_ident));\n&lt;%java.lang.String%&gt; _plus_3 = (_plus_2 + \"_\");\n&lt;%gecos.types.Type%&gt; _type = this.getType();\nreturn (_plus_3 + _type);'"
	 * @generated
	 */
	String toString();

} // DAGVectorExtractNode
