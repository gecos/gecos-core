package fr.irisa.cairn.gecos.model.analysis.types;

import gecos.types.Type;

/**
 * @author aelmouss
 */
public interface TypeLevel {
	/**
	 * Construct a Type that mimics this {@link TypeLevel} starting from {@code type}
	 * @param type
	 * @return Type resulting from reproducing this {@link TypeLevel} on {@code type} 
	 */
	public Type revertOn(Type type);
	
	/**
	 * Compare {@code this} and {@code other} {@link TypeLevel}s.
	 * Does not consider type qualifiers (const, static ...), aliasing ...
	 * @param other {@link TypeLevel}
	 * @return true if {@link TypeLevel}s are similar.
	 */
	public boolean isSimilar(TypeLevel other);

}