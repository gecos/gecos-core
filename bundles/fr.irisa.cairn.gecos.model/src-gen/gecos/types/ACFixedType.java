/**
 */
package gecos.types;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AC Fixed Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.ACFixedType#getBitwidth <em>Bitwidth</em>}</li>
 *   <li>{@link gecos.types.ACFixedType#getMode <em>Mode</em>}</li>
 *   <li>{@link gecos.types.ACFixedType#getFraction <em>Fraction</em>}</li>
 *   <li>{@link gecos.types.ACFixedType#getInteger <em>Integer</em>}</li>
 *   <li>{@link gecos.types.ACFixedType#getQuantificationMode <em>Quantification Mode</em>}</li>
 *   <li>{@link gecos.types.ACFixedType#getOverflowMode <em>Overflow Mode</em>}</li>
 * </ul>
 *
 * @see gecos.types.TypesPackage#getACFixedType()
 * @model
 * @generated
 */
public interface ACFixedType extends ArrayType, ACType, TypesVisitable {
	/**
	 * Returns the value of the '<em><b>Bitwidth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bitwidth</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bitwidth</em>' attribute.
	 * @see #setBitwidth(long)
	 * @see gecos.types.TypesPackage#getACFixedType_Bitwidth()
	 * @model unique="false" dataType="gecos.types.long"
	 * @generated
	 */
	long getBitwidth();

	/**
	 * Sets the value of the '{@link gecos.types.ACFixedType#getBitwidth <em>Bitwidth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bitwidth</em>' attribute.
	 * @see #getBitwidth()
	 * @generated
	 */
	void setBitwidth(long value);

	/**
	 * Returns the value of the '<em><b>Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mode</em>' attribute.
	 * @see #setMode(long)
	 * @see gecos.types.TypesPackage#getACFixedType_Mode()
	 * @model unique="false" dataType="gecos.types.long"
	 * @generated
	 */
	long getMode();

	/**
	 * Sets the value of the '{@link gecos.types.ACFixedType#getMode <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mode</em>' attribute.
	 * @see #getMode()
	 * @generated
	 */
	void setMode(long value);

	/**
	 * Returns the value of the '<em><b>Fraction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fraction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fraction</em>' attribute.
	 * @see #setFraction(long)
	 * @see gecos.types.TypesPackage#getACFixedType_Fraction()
	 * @model unique="false" dataType="gecos.types.long"
	 * @generated
	 */
	long getFraction();

	/**
	 * Sets the value of the '{@link gecos.types.ACFixedType#getFraction <em>Fraction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fraction</em>' attribute.
	 * @see #getFraction()
	 * @generated
	 */
	void setFraction(long value);

	/**
	 * Returns the value of the '<em><b>Integer</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer</em>' attribute.
	 * @see #setInteger(long)
	 * @see gecos.types.TypesPackage#getACFixedType_Integer()
	 * @model unique="false" dataType="gecos.types.long"
	 * @generated
	 */
	long getInteger();

	/**
	 * Sets the value of the '{@link gecos.types.ACFixedType#getInteger <em>Integer</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Integer</em>' attribute.
	 * @see #getInteger()
	 * @generated
	 */
	void setInteger(long value);

	/**
	 * Returns the value of the '<em><b>Quantification Mode</b></em>' attribute.
	 * The literals are from the enumeration {@link gecos.types.QuantificationMode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantification Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantification Mode</em>' attribute.
	 * @see gecos.types.QuantificationMode
	 * @see #setQuantificationMode(QuantificationMode)
	 * @see gecos.types.TypesPackage#getACFixedType_QuantificationMode()
	 * @model unique="false"
	 * @generated
	 */
	QuantificationMode getQuantificationMode();

	/**
	 * Sets the value of the '{@link gecos.types.ACFixedType#getQuantificationMode <em>Quantification Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantification Mode</em>' attribute.
	 * @see gecos.types.QuantificationMode
	 * @see #getQuantificationMode()
	 * @generated
	 */
	void setQuantificationMode(QuantificationMode value);

	/**
	 * Returns the value of the '<em><b>Overflow Mode</b></em>' attribute.
	 * The literals are from the enumeration {@link gecos.types.OverflowMode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overflow Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overflow Mode</em>' attribute.
	 * @see gecos.types.OverflowMode
	 * @see #setOverflowMode(OverflowMode)
	 * @see gecos.types.TypesPackage#getACFixedType_OverflowMode()
	 * @model unique="false"
	 * @generated
	 */
	OverflowMode getOverflowMode();

	/**
	 * Sets the value of the '{@link gecos.types.ACFixedType#getOverflowMode <em>Overflow Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Overflow Mode</em>' attribute.
	 * @see gecos.types.OverflowMode
	 * @see #getOverflowMode()
	 * @generated
	 */
	void setOverflowMode(OverflowMode value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitACFixedType(this);'"
	 * @generated
	 */
	void accept(TypesVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.types.long" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getBitwidth();'"
	 * @generated
	 */
	long getSize();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final boolean defaultMode = ((this.getOverflowMode().getValue() == 0) &amp;&amp; (this.getQuantificationMode().getValue() == 0));\n&lt;%gecos.types.QuantificationMode%&gt; _quantificationMode = this.getQuantificationMode();\n&lt;%java.lang.String%&gt; _plus = (_quantificationMode + \",\");\n&lt;%gecos.types.OverflowMode%&gt; _overflowMode = this.getOverflowMode();\nfinal &lt;%java.lang.String%&gt; mode = (_plus + _overflowMode);\nboolean _isSigned = this.isSigned();\nboolean _not = (!_isSigned);\nif (_not)\n{\n\tif (defaultMode)\n\t{\n\t\tlong _bitwidth = this.getBitwidth();\n\t\t&lt;%java.lang.String%&gt; _plus_1 = (\"ac_fixed&lt;\" + &lt;%java.lang.Long%&gt;.valueOf(_bitwidth));\n\t\t&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + \",\");\n\t\tlong _integer = this.getInteger();\n\t\t&lt;%java.lang.String%&gt; _plus_3 = (_plus_2 + &lt;%java.lang.Long%&gt;.valueOf(_integer));\n\t\treturn (_plus_3 + \",false&gt;\");\n\t}\n\telse\n\t{\n\t\tlong _bitwidth_1 = this.getBitwidth();\n\t\t&lt;%java.lang.String%&gt; _plus_4 = (\"ac_fixed&lt;\" + &lt;%java.lang.Long%&gt;.valueOf(_bitwidth_1));\n\t\t&lt;%java.lang.String%&gt; _plus_5 = (_plus_4 + \",\");\n\t\tlong _integer_1 = this.getInteger();\n\t\t&lt;%java.lang.String%&gt; _plus_6 = (_plus_5 + &lt;%java.lang.Long%&gt;.valueOf(_integer_1));\n\t\t&lt;%java.lang.String%&gt; _plus_7 = (_plus_6 + \",false,\");\n\t\t&lt;%java.lang.String%&gt; _plus_8 = (_plus_7 + mode);\n\t\treturn (_plus_8 + \"&gt;\");\n\t}\n}\nelse\n{\n\tif (defaultMode)\n\t{\n\t\tlong _bitwidth_2 = this.getBitwidth();\n\t\t&lt;%java.lang.String%&gt; _plus_9 = (\"ac_fixed&lt;\" + &lt;%java.lang.Long%&gt;.valueOf(_bitwidth_2));\n\t\t&lt;%java.lang.String%&gt; _plus_10 = (_plus_9 + \",\");\n\t\tlong _integer_2 = this.getInteger();\n\t\t&lt;%java.lang.String%&gt; _plus_11 = (_plus_10 + &lt;%java.lang.Long%&gt;.valueOf(_integer_2));\n\t\treturn (_plus_11 + \"&gt;\");\n\t}\n\telse\n\t{\n\t\tlong _bitwidth_3 = this.getBitwidth();\n\t\t&lt;%java.lang.String%&gt; _plus_12 = (\"ac_fixed&lt;\" + &lt;%java.lang.Long%&gt;.valueOf(_bitwidth_3));\n\t\t&lt;%java.lang.String%&gt; _plus_13 = (_plus_12 + \",\");\n\t\tlong _integer_3 = this.getInteger();\n\t\t&lt;%java.lang.String%&gt; _plus_14 = (_plus_13 + &lt;%java.lang.Long%&gt;.valueOf(_integer_3));\n\t\t&lt;%java.lang.String%&gt; _plus_15 = (_plus_14 + \",true,\");\n\t\t&lt;%java.lang.String%&gt; _plus_16 = (_plus_15 + mode);\n\t\treturn (_plus_16 + \"&gt;\");\n\t}\n}'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.boolean" unique="false" otherUnique="false" ignoreStorageClassDataType="gecos.types.boolean" ignoreStorageClassUnique="false" ignoreConstDataType="gecos.types.boolean" ignoreConstUnique="false" ignoreVolatileDataType="gecos.types.boolean" ignoreVolatileUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((other == this))\n{\n\treturn true;\n}\nif ((other instanceof &lt;%gecos.types.ACFixedType%&gt;))\n{\n\treturn ((((((((&lt;%gecos.types.ACFixedType%&gt;)other).getBitwidth() == this.getBitwidth()) &amp;&amp; (((&lt;%gecos.types.ACFixedType%&gt;)other).getMode() == this.getMode())) &amp;&amp; (((&lt;%gecos.types.ACFixedType%&gt;)other).getFraction() == this.getFraction())) &amp;&amp; (((&lt;%gecos.types.ACFixedType%&gt;)other).getQuantificationMode() == this.getQuantificationMode())) &amp;&amp; (((&lt;%gecos.types.ACFixedType%&gt;)other).getOverflowMode() == this.getOverflowMode())) &amp;&amp; super.isEqual(other, ignoreStorageClass, ignoreConst, ignoreVolatile));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isEqual(Type other, boolean ignoreStorageClass, boolean ignoreConst, boolean ignoreVolatile);

} // ACFixedType
