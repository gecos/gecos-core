#include <stdio.h>
#include <stdlib.h>
#include "data_size.h"

void testComSubExpr(int [N+1][M+1], int [N+1][M+1]) ;

int main(int argc, const char **argv) {
	int i,j;
	FILE *outfile;
	int in[N+1][M+1], out[N+1][M+1];

	/* check parameters */
	if (argc < 2) {
		fprintf(stderr, "Usage: %s output_file \n", argv[0]);
		exit(-1);
	}

	outfile = fopen(argv[1], "w");
	if (outfile == NULL) {
		fprintf(stderr, "couldn't open file \"%s\"!\n", argv[1]);
		exit(-2);
	}

	/**
	 * Initialize the input Matrix :
	 */

	for (i = 0; i <= N; i++)
		for (j = 0; j <= M; j++) {
			in[i][j]=2;
			out[i][j]=0;
		}

	testComSubExpr(in, out);

	/**
	 * Write the result in the output File
	 */

	for (i = 0; i <= N; i++)
		for (j = 0; j <= M; j++)
			fprintf(outfile, "out[%d][%d] = %d\n",i , j, out[i][j]);

	fclose(outfile);
	return 0;
}
