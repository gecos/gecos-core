#!/usr/bin/perl

use strict;
use File::Basename;

my %STAT;
my %WIDTH;
my @LOGS;
my $NCHART=0;
my $ln=0;
my $size = @ARGV[0];
my $num = @ARGV[1];
my $outdir = @ARGV[2];

#print "$size\n";

for my $arg (@ARGV[3 .. $#ARGV]) {
	#print "$ln: $arg\n";
		
    parseLog($arg,$ln);
    $ln ++;
}
printStats();
exit 0;

sub parseLog {
    my $log = shift;
	my $ln = shift;
	my $sectionLines = 0;
	my $data = 0;
	push @LOGS, $log;
	open FH, "<$log";
	while (<FH>) {
		chomp;
		s/[)(%]//g;
		my @fields = split(/[\s]+/);
	    if    (/Total Cycles:/)         { $STAT{$log}{'TotalClockCycles'} = $fields[2]; 
	                                      $STAT{$log}{'Time'} = $fields[3]; }
		elsif (/Execution Cycles:/)     { $STAT{$log}{'ExecutionCycles'} =  $fields[2]; }
		elsif (/Stall Cycles:/)         { $STAT{$log}{'StallCyclesRatio'}  = $fields[3]; }
		elsif (/Taken branches:/)       { $STAT{$log}{'TakenBranches'} = $fields[2]; }
		elsif (/Executed operations:/)  { $STAT{$log}{'ExecutedOps'} = $fields[2]; }
#		elsif (/Avg. IPC.*no stalls:/)  { $STAT{$log}{'IPCfunc'} = $fields[4]; }
#		elsif (/Avg. IPC.*with stalls:/){ $STAT{$log}{'IPCtot'} = $fields[4]; }
#		elsif (/width\[ ([0-9]*)\]/)    { $WIDTH{$log}{"$1"} = $fields[4]; }
		elsif(/Instruction Memory Operations:/) {
			$sectionLines = 4;
			$data = 0;
		}
		elsif (/Instruction Memory Stall Cycles/) {
			$sectionLines = 2;
			$data = 0;
		}
		elsif (/Data Memory Operations:/) {
			$sectionLines = 4;
			$data = 1;
		}
		elsif (/Data Memory Stall Cycles/) {
			$sectionLines = 2;
			$data = 1;
		}
		elsif (/Accesses:/) {
			if($sectionLines > 0) {
				if($data == 1) 	{ $STAT{$log}{'DcahceAccess'} = $fields[2]; }
				else 			{ $STAT{$log}{'IcacheAccess'} = $fields[2]; }
			}
		}
		elsif (/Misses/) {
			if($sectionLines > 0) {
				if($data == 1) 	{ $STAT{$log}{'DcacheMisses'} = $fields[4]; }
				else 			{ $STAT{$log}{'IcacheMisses'} = $fields[4]; }
			}
		}
		elsif (/Total/) {
			if($sectionLines > 0) {
				if($data == 1) 	{ $STAT{$log}{'DcacheStallCycles'} = $fields[4]; }
				else 			{ $STAT{$log}{'IcacheStallCycles'} = $fields[4]; }
			}
		}
		$sectionLines --;
	}
	close FH;
}

sub printStats {

	emitMetric('TotalClockCycles');
	emitMetric('StallCyclesRatio');
	emitMetric('ExecutionCycles');
	emitMetric('ExecutedOps');
	emitMetric('TakenBranches');
	emitMetric('DcahceAccess');
	emitMetric('IcacheAccess');
	emitMetric('DcacheMisses');
	emitMetric('IcacheMisses');
	emitMetric('DcacheStallCycles');
	emitMetric('IcacheStallCycles');

#	plotMetric('Time');
#	plotMetric('IPCfunc','IPCtot');
#	foreach my $log (@LOGS) {
#	    print "<p>";print "Run $log"; print "</p>\n";
#		print "<table><tr><td>\n";
#	    plotPie($log, 'Execution','Stall','Nops');
#		print "</td><td>\n";
#	    plotWidth($log);
#		print "</td></tr></table>\n";
#	}
}

sub emitMetric {
    my $m = shift; # metric
    my $separator = ' ';
    my $outfile = "$outdir/$m.vex.stats";
    open FILE, ">>$outfile";
    
    if ($num == 1) {
		print FILE "imageSize";
		foreach my $log (@LOGS) {
			my $base = basename($log, ".vex.stats");
			print FILE "$separator$base";
		}
		print FILE "\n";
	}
    
    print FILE "$size";

	foreach my $log (@LOGS) {
	    print FILE "$separator$STAT{$log}{$m}"; 
	}
	print FILE "\n";
	
	close FILE;
}
