package fr.irisa.cairn.gecos.model.transforms.tools;

import gecos.instrs.ComponentInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;

/**
 * An abstract class for TOM generic instructions transformations. Original
 * instructions aren't modified.
 * 
 * @author antoine
 * 
 */
public abstract class GenericInstructionTransformation extends
		AbstractInstructionTransformation {

	public GenericInstructionTransformation() {
		this.tSwitch = new GenericTransform();
	}

	/**
	 * Nested transformation switch. Default behavior is to transform generic
	 * instructions and analyse all children of components instructions.
	 * 
	 * @author antoine
	 * 
	 */
	private class GenericTransform extends InstructionTransformSwitch {

		@Override
		public Instruction caseGenericInstruction(GenericInstruction g) {
			return doTrans(g);
		}
		
		@Override
		public Instruction caseInstruction(Instruction original) {
			return original;
		}

		@Override
		public Instruction caseComponentInstruction(ComponentInstruction object) {
			boolean changed = false;
			List<Instruction> transformed = new ArrayList<Instruction>();
			for (Instruction c : object.listChildren()) {
				Instruction trans = doSwitch(c);
				transformed.add(trans);
				changed = true;
			}
			if (changed) {
				ComponentInstruction copy = (ComponentInstruction) object.copy();
//				for (int i = 0; i < object.getChildrenCount(); ++i) {
//					Instruction o = object.getComponentAt(i);
//					Instruction t = transformed.get(i);
//					if (t != o) {
//						copy.setComponentAt(i, t);
//					} else {
//						copy.setComponentAt(i, o.copy());
//					}
//				}
				EList<Instruction> listChildren = copy.listChildren();
				for (int i = 0; i < listChildren.size(); ++i) {
					Instruction o = listChildren.get(i);
					Instruction t = transformed.get(i);
					copy.replaceChild(o, t);
				}
				return copy;
			}
			return object;
		}
	}
}
