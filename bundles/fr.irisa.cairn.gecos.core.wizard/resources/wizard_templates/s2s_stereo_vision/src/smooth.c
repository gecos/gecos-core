/*
 Copyright (C) 2006 Pedro Felzenszwalb

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

void smooth(unsigned char **src, unsigned char **dst, int width, int height) {
	int i, j, k, l;
	int order = 5;
	int mask[5][5] = {
			{ 1, 4, 7, 4, 1 },
			{ 4, 16, 26, 16, 4 },
			{ 7, 26, 41, 26, 7 },
			{ 4, 16, 26, 16, 4 },
			{ 1, 4, 7, 4, 1 }
	};

	int border = (order / 2);
	for (i = 0; i < width; i++) {
		for (j = 0; j < height; j++) {
			if ((i >= border) && (i < width - border) && (j >= border) && (j
					< height - border)) {
				float sum = 0.0;
				for (k = -border; k < border; k++) {
					for (l = -border; l < border; l++) {
						sum += src[i + k][j + l] * mask[k + border][l + border];
					}
				}
				dst[i][j] = (int) sum / 273.0;

			} else {
				dst[i][j] = src[i][j];
			}
		}

	}
	printf("Smooth\n");
	fflush(stdout);
}

float min(float a, float b) {
	if (a > b) {
		return b;
	} else {
		return a;
	}
}

