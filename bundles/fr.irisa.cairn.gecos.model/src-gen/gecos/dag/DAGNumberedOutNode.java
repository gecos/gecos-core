/**
 */
package gecos.dag;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Numbered Out Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.DAGNumberedOutNode#getNumber <em>Number</em>}</li>
 * </ul>
 *
 * @see gecos.dag.DagPackage#getDAGNumberedOutNode()
 * @model
 * @generated
 */
public interface DAGNumberedOutNode extends DAGOutNode, DagVisitable {
	/**
	 * Returns the value of the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number</em>' attribute.
	 * @see #setNumber(int)
	 * @see gecos.dag.DagPackage#getDAGNumberedOutNode_Number()
	 * @model unique="false" dataType="gecos.dag.int"
	 * @generated
	 */
	int getNumber();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGNumberedOutNode#getNumber <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number</em>' attribute.
	 * @see #getNumber()
	 * @generated
	 */
	void setNumber(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGNumberedOutNode(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.dag.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _elvis = null;\n&lt;%gecos.core.Symbol%&gt; _symbol = this.getSymbol();\n&lt;%java.lang.String%&gt; _name = null;\nif (_symbol!=null)\n{\n\t_name=_symbol.getName();\n}\nif (_name != null)\n{\n\t_elvis = _name;\n} else\n{\n\t_elvis = \"?\";\n}\n&lt;%java.lang.String%&gt; _plus = (_elvis + \"_\");\nint _number = this.getNumber();\nreturn (_plus + &lt;%java.lang.Integer%&gt;.valueOf(_number));'"
	 * @generated
	 */
	String toString();

} // DAGNumberedOutNode
