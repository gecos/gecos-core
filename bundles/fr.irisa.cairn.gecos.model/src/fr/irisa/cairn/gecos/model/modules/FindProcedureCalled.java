/**
 * 
 */
package fr.irisa.cairn.gecos.model.modules;

import gecos.core.Procedure;
import gecos.core.ProcedureSymbol;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;

/**
 * @author cloatre
 *
 *	class to find a procedure called with a CallInstruction for example in a gecosProject
 *	used when a function call another one written in another file
 */
public class FindProcedureCalled {
	
	private GecosProject gecosProject;
	private ProcedureSymbol procedureCalled;
	
	public FindProcedureCalled( GecosProject myGecosProject, ProcedureSymbol myProcedureCalled) {
		
		this.gecosProject = myGecosProject;
		this.procedureCalled = myProcedureCalled;
	}

	/**
	 * function which scans each procedure in gecosSourceFile of a Gecos project
	 * if the procedure researched is found, it returns the ProcedureSymbol found
	 * @return ProcedureSymbol found or null
	 */
	public ProcedureSymbol compute(){
		ProcedureSymbol retProcedureSymbol = null;
		
		System.out.println("Search of procedure called : " + procedureCalled.getName() + " , type : " + procedureCalled.getType());
		for(GecosSourceFile sourceFile : gecosProject.getSources()){
			//System.out.println("Search of procedure called : in file " + sourceFile.getName());
			for(Procedure proc : sourceFile.getModel().listProcedures()){
				//System.out.println("Search of procedure called : in procedure " + proc.getSymbol().getName());
				if(proc.getSymbol().getName().equals(procedureCalled.getName())){
					if(proc.getSymbol().getType().isEqual(procedureCalled.getType())){
						System.out.println("procedure called found in : " + proc.getSymbol().getName() + " of file : " + sourceFile.getName());
						retProcedureSymbol = proc.getSymbol();
						return retProcedureSymbol;
					}					
				}
			}
		}
		return null;
	}
}
