package fr.irisa.cairn.gecos.model.transforms.commonsubexpressionelimination.internal;

import gecos.core.Symbol;

/**
 * Structure used in the Local common sub-expression elimination algorithm. It
 * represents a quadruple, its position in the block and the corresponding
 * temporary variable if it exists.
 * 
 * @author imen
 * @date 20/04/2016
 */

public class quintuples_instr {

	private int pos; 
	private String opd1;
	private String opr;
	private String opd2;
	private Symbol tmp;

	/**
	 * Creates a new quintuple : <pos, opd1, opr, opd2, tmp>
	 *  
	 * @param pos : the position where the expression is used in the basic block
	 * @param opd1 : first operand in the binary expression
	 * @param opr : the operator of the binary expression 
	 * @param opd2 : second operand in the binary expression
	 * @param tmp : temporary variable or null 
	 */
	public quintuples_instr(int pos, String opd1, String opr, String opd2, Symbol tmp) {
		this.pos = pos;
		this.opd1 = opd1;
		this.opr = opr;
		this.opd2 = opd2;
		this.tmp = tmp;
	}

	public void incrementPos() {
		this.pos++;
	}

	public void print_quintuples() {
		System.out.println(
				"< " + this.pos + ", " + this.opd1 + ", " + this.opr + ", " + this.opd2 + ", " + tmp.getName() + " >");
	}

	public int getPos() {
		return pos;
	}

	public void setPos(int pos) {
		this.pos = pos;
	}

	public String getOpd1() {
		return opd1;
	}

	public void setOpd1(String opd1) {
		this.opd1 = opd1;
	}

	public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}

	public String getOpd2() {
		return opd2;
	}

	public void setOpd2(String opd2) {
		this.opd2 = opd2;
	}

	public Symbol getTmp() {
		return tmp;
	}

	public void setTmp(Symbol tmp) {
		this.tmp = tmp;
	}

}
