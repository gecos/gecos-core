/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.cdtfrontend.visitor;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.cdt.core.dom.ast.ASTVisitor;
import org.eclipse.cdt.core.dom.ast.IASTArrayModifier;
import org.eclipse.cdt.core.dom.ast.IASTDeclSpecifier;
import org.eclipse.cdt.core.dom.ast.IASTDeclaration;
import org.eclipse.cdt.core.dom.ast.IASTDeclarator;
import org.eclipse.cdt.core.dom.ast.IASTEnumerationSpecifier.IASTEnumerator;
import org.eclipse.cdt.core.dom.ast.IASTExpression;
import org.eclipse.cdt.core.dom.ast.IASTFileLocation;
import org.eclipse.cdt.core.dom.ast.IASTInitializer;
import org.eclipse.cdt.core.dom.ast.IASTName;
import org.eclipse.cdt.core.dom.ast.IASTNode;
import org.eclipse.cdt.core.dom.ast.IASTParameterDeclaration;
import org.eclipse.cdt.core.dom.ast.IASTPointerOperator;
import org.eclipse.cdt.core.dom.ast.IASTPreprocessorPragmaStatement;
import org.eclipse.cdt.core.dom.ast.IASTPreprocessorStatement;
import org.eclipse.cdt.core.dom.ast.IASTProblem;
import org.eclipse.cdt.core.dom.ast.IASTStatement;
import org.eclipse.cdt.core.dom.ast.IASTTranslationUnit;
import org.eclipse.cdt.core.dom.ast.IASTTypeId;
import org.eclipse.cdt.core.model.ICProject;
import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.gecos.model.cdtfrontend.IASTNodeReference;
import fr.irisa.cairn.gecos.model.cdtfrontend.builder.ControlStructureBuilder;
import fr.irisa.cairn.gecos.model.cdtfrontend.builder.CoreBuilder;
import fr.irisa.cairn.gecos.model.cdtfrontend.builder.impl.CoreBuilderImpl;
import fr.irisa.cairn.gecos.model.cdtfrontend.exception.CDTParserErrorException;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import gecos.annotations.AnnotatedElement;
import gecos.annotations.PragmaAnnotation;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;
import gecos.instrs.LabelInstruction;

/**
 * @author mnaullet
 */
public class CDTToGecosModel extends ASTVisitor {
	
	// gives access to the specific front end factories
	private CoreBuilder gecosCoreContextedFactory;
	
//	// list of visited includes to avoid visiting them twice
//	private List<String> visitedIncludes;
	
	// whether the implementation should be ignored. 
	// Typically, if it is part of an include file
	private boolean ignoreImplementation;
	
	// the GecosProject currently parsed
	private GecosProject project;
	
	// whether to keep track of IASTNodes from the CDT model 
	private boolean storeIASTNode;
	
	// if true, each GecosNode is annotated with its corresponding FileLocationAnnotation  
	private boolean enableFileLocationAnnotations;
	
	// if false, do not try to generate SimpleForBlocks
	private boolean generateSimpleForBlock;
	
	// the ICProject associated to the GecosProject
	private ICProject cProject;
	
	// the last IASTTranslationUnit that has been parsed
	private IASTTranslationUnit lastAst = null;
	
	private Map<String, LabelInstruction> labelInstructions;
	private List<IASTPreprocessorPragmaStatement> pragmaStatements;
	private Object object;
	
	
	public CDTToGecosModel(GecosProject project, ICProject cProject) {
		this (project,cProject,false);
	}

	public CDTToGecosModel(GecosProject project, ICProject cProject, boolean storeIASTNode) {
		super(true);
		this.gecosCoreContextedFactory = new CoreBuilderImpl();
		this.pragmaStatements = null;
		this.labelInstructions = new LinkedHashMap<String, LabelInstruction>();
		this.cProject = cProject;
		this.project = project;
		this.storeIASTNode = storeIASTNode;
		this.ignoreImplementation = false;
		this.enableFileLocationAnnotations = false;
	}

	public void setIgnoreImplementation(boolean b) {
		this.ignoreImplementation = b;
	}
	
	public boolean isIgnoreImplementation() {
		return ignoreImplementation;
	}
	
	public boolean isStoreIASTNode() {
		return storeIASTNode;
	}
	
	public void setEnableFileLocationAnnotations(boolean addFileLocationAnnotations) {
		this.enableFileLocationAnnotations = addFileLocationAnnotations;
	}
	
	public boolean isEnableFileLocationAnnotations() {
		return enableFileLocationAnnotations;
	}
	
	public void setGenerateSimpleForBlock(boolean generateSimpleForBlock) {
		this.generateSimpleForBlock = generateSimpleForBlock;
	}
	
	public boolean isGenerateSimpleForBlock() {
		return generateSimpleForBlock;
	}
	
	public void addIASTAnnotation(EObject e, IASTNode node) {
		if (e == null || node == null || !isStoreIASTNode()) return;
		IASTNodeReference.annotate(e,node);
	}
	
	public void addFileLocationAnnotation(AnnotatedElement e, IASTFileLocation location) {
		if(enableFileLocationAnnotations && e != null) {
			GecosUserAnnotationFactory.fileLocation(e, location.getFileName(), 
					location.getStartingLineNumber(), location.getEndingLineNumber(),
					location.getNodeLength(), location.getNodeOffset());
		}
	}

	public void ignore(AnnotatedElement ... el) {
		for (AnnotatedElement e : el)
			if(e != null)
				GecosUserAnnotationFactory.pragma(e,GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
	}
	
	public void removeIgnore(AnnotatedElement ... el) {
		for (AnnotatedElement e : el)
			if(e != null) {
				PragmaAnnotation pragma = e.getPragma();
				if(pragma != null)
					pragma.getContent().remove(GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
			}
	}

	/**
	 * @deprecated use {@link #isIgnoreImplementation()} instead
	 */
	@Deprecated
	public boolean visitingInclude() {
		return isIgnoreImplementation();
	}

	public void setPragmaList(List<IASTPreprocessorPragmaStatement> pragmaStatements) {
		this.pragmaStatements = pragmaStatements;
	}
	
	public List<PragmaAnnotation> findAnnotation(String filePath, int nodeOffset) {
		List<IASTPreprocessorPragmaStatement> pragmaStatementsUsed = new ArrayList<IASTPreprocessorPragmaStatement>();
		List<PragmaAnnotation> pragmaAnnotations = new ArrayList<PragmaAnnotation>();
		if (!pragmaStatements.isEmpty())
			for(int i = 0 ;i < pragmaStatements.size() && pragmaStatements.get(i).getFileLocation().getNodeOffset() < nodeOffset; i++) {
				IASTPreprocessorPragmaStatement iastPreprocessorPragmaStatement = pragmaStatements.get(i);
				if (filePath.equals(iastPreprocessorPragmaStatement.getFileLocation().getFileName())) {
					visit(iastPreprocessorPragmaStatement);
					pragmaStatementsUsed.add(iastPreprocessorPragmaStatement);
					ASTPreprocessorStatementVisitor preprocessorStatementVisitor = (ASTPreprocessorStatementVisitor)object;
					PragmaAnnotation pragmaAnnotation = (PragmaAnnotation)preprocessorStatementVisitor.getAnnotation();
					pragmaAnnotations.add(pragmaAnnotation);
				}
			}
		pragmaStatements.removeAll(pragmaStatementsUsed);
		return pragmaAnnotations;
	}
	
	public ICProject getCProject() {
		return this.cProject;
	}
	
	public ControlStructureBuilder getGecosControlStructureFactory() {
		return gecosCoreContextedFactory.getControlStructureBuilder();
	}

	public CoreBuilder getGecosCoreContextedFactory() {
		return gecosCoreContextedFactory;
	}

	public LabelInstruction getLabelInstruction(String name) {
		LabelInstruction labelInstruction = labelInstructions.get(name);
		if (labelInstruction == null) {
			labelInstruction = GecosUserInstructionFactory.label(name);
			labelInstructions.put(name, labelInstruction);
		}
		return labelInstruction;
	}

	public IASTTranslationUnit getLastAst() {
		return lastAst;
	}

	public ProcedureSet getProcedureSet() {
		return gecosCoreContextedFactory.getProcedureSet();
	}

	public GecosProject getProject() {
		return project;
	}

	public Object getObject() {
		return object;
	}
	
	public int visit(IASTArrayModifier arrayModifier) {
		ASTArrayModifierVisitor arrayModifierVisitor = new ASTArrayModifierVisitor();
		arrayModifierVisitor.visit(this, arrayModifier);
		object = arrayModifierVisitor;
		return PROCESS_ABORT;
	}
	
	public int visit(IASTDeclaration declaration) {
		ASTDeclarationVisitor declarationVisitor = new ASTDeclarationVisitor();
		declarationVisitor.visit(this, declaration);
		object = declarationVisitor;
		return PROCESS_ABORT;
	}
	
	public int visit(IASTDeclarator declarator) {
		ASTDeclaratorVisitor declaratorVisitor = new ASTDeclaratorVisitor();
		declaratorVisitor.visit(this, declarator);
		object = declaratorVisitor;
		return PROCESS_ABORT;
	}
	
	public int visit(IASTDeclSpecifier declSpecifier) {
		ASTDeclSpecifierVisitor declSpecifierVisitor = new ASTDeclSpecifierVisitor();
		declSpecifierVisitor.visit(this, declSpecifier);
		object = declSpecifierVisitor;
		return PROCESS_ABORT;
	}
	
	public int visit(IASTEnumerator enumerator) {
		ASTEnumeratorVisitor enumeratorVisitor = new ASTEnumeratorVisitor();
		enumeratorVisitor.visit(this, enumerator);
		object = enumeratorVisitor;
		return PROCESS_ABORT;
	}
	
	public int visit(IASTExpression expression) {
		ASTExpressionVisitor expressionVisitor = new ASTExpressionVisitor();
		expressionVisitor.visit(this, expression);
		object = expressionVisitor;
		return PROCESS_ABORT;
	}
	
	public int visit(IASTInitializer initializer) {
		ASTInitializerVisitor initializerVisitor = new ASTInitializerVisitor();
		initializerVisitor.visit(this, initializer);
		object = initializerVisitor;
		return PROCESS_ABORT;
	}
	
	public int visit(IASTName name) {
		ASTNameVisitor nameVisitor = new ASTNameVisitor();
		nameVisitor.visit(this, name);
		object = nameVisitor;
		return PROCESS_ABORT;
	}
	
	public int visit(IASTParameterDeclaration parameterDeclaration) {
		ASTParameterDeclarationVisitor parameterDeclarationVisitor = new ASTParameterDeclarationVisitor();
		parameterDeclarationVisitor.visit(this, parameterDeclaration);
		object = parameterDeclarationVisitor;
		return PROCESS_ABORT;
	}
	
	public int visit(IASTPointerOperator pointerOperator) {
		ASTPointerOperatorVisitor pointerOperatorVisitor = new ASTPointerOperatorVisitor();
		pointerOperatorVisitor.visit(this, pointerOperator);
		object = pointerOperatorVisitor;
		return PROCESS_ABORT;
	}
	
	public int visit(IASTPreprocessorStatement preprocessorStatement) {
		ASTPreprocessorStatementVisitor preprocessorStatementVisitor = new ASTPreprocessorStatementVisitor();
		preprocessorStatementVisitor.visit(this, preprocessorStatement);
		object = preprocessorStatementVisitor;
		return PROCESS_ABORT;
	}

	public int visit(IASTProblem problem) {
		throw new CDTParserErrorException(problem, "Problem : "+problem.getMessage());
	}

	public int visit(IASTStatement statement) {
		if (!visitingInclude()) {
			ASTStatementVisitor statementVisitor = new ASTStatementVisitor();
			statementVisitor.visit(this, statement);
			object = statementVisitor;
		}
		return PROCESS_ABORT;
	}
	
	public int visit(IASTTranslationUnit translationUnit) {
		ASTTranslationUnitVisitor translationUnitVisitor = new ASTTranslationUnitVisitor();
		translationUnitVisitor.visit(this, translationUnit);
		object = translationUnitVisitor;
		lastAst = translationUnit;
		return PROCESS_ABORT;
	}
	
	public int visit(IASTTypeId typeId) {
		ASTTypeIdVisitor typeIdVisitor = new ASTTypeIdVisitor();
		typeIdVisitor.visit(this, typeId);
		object = typeIdVisitor;
		return PROCESS_ABORT;
	}
}