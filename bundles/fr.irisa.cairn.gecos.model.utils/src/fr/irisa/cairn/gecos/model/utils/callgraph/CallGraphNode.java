/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.utils.callgraph;

import gecos.core.Procedure;

import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

public class CallGraphNode {

	public static final int NORMAL = 0;
	public static final int EXTERN = 1;
	public static final int SIMPLE = 2;
	public static final int DELETE = 3;

	private Procedure proc;
	private Vector<CallGraphEdge> inEdges;
	private Vector<CallGraphEdge> outEdges;
	private int mode;

	public CallGraphNode(Procedure proc) {
		this.proc = proc;
		this.inEdges = new Vector<CallGraphEdge>();
		this.outEdges = new Vector<CallGraphEdge>();
		this.mode = NORMAL;
	}

	public Procedure procedure() { return proc; }
	public List<CallGraphEdge> inEdges() { return inEdges; }
	public List<CallGraphEdge> outEdges() { return outEdges; }
	public int outEdgesCount() { return outEdges.size(); }
	public int inEdgesCount() { return inEdges.size(); }

	public void setMode(int m) { mode = m; }
	public int mode() { return mode; }

	public void linkTo(CallGraphNode a) {
		CallGraphEdge e = new CallGraphEdge(this, a);
		outEdges.add(e);
		a.inEdges.add(e);
	}
	
	public int depth() {
		int maxDepth = 0;
		for (int i = 0; i < outEdges.size(); ++i) {
			CallGraphEdge e = (CallGraphEdge) outEdges.elementAt(i);
			int depth = e.to().depth() + 1;
			if (depth > maxDepth)
				maxDepth = depth;
		}
		return maxDepth;
	}

	public void removeInEdge(CallGraphEdge edge) {
		inEdges.remove(edge);
	}

	public void removeOutEdge(CallGraphEdge edge) {
		outEdges.remove(edge);
	}

	public void disconnect() {
		List<?> iter = inEdges();
		for (CallGraphEdge e : inEdges()) {
			e.from().removeOutEdge(e);
		}
		inEdges.clear();

		for (CallGraphEdge e : outEdges()) {
			e.from().removeInEdge(e);
		}
		outEdges.clear();
	}
	
	public Object accept(CallGraphVisitor visitor) {
		return visitor.visitCallGraphNode(this);
	}
}
