package fr.irisa.cairn.gecos.core.transforms.tests;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.core.testframework.impl.GSProjectVersion;
import fr.irisa.cairn.gecos.core.testframework.impl.GecosS2STestTemplate;
import fr.irisa.cairn.gecos.model.transforms.procedures.outlining.ProcedureOutlining;
import fr.irisa.cairn.gecos.model.transforms.ssa.ComputeSSAForm;
import fr.irisa.cairn.gecos.model.transforms.ssa.RemoveSSAForm;
import fr.irisa.cairn.gecos.testframework.data.ICxxProjectData;
import fr.irisa.cairn.gecos.testframework.dataprovider.AddStringParams;
import fr.irisa.cairn.gecos.testframework.dataprovider.DataFromPathProvider;
import fr.irisa.cairn.gecos.testframework.dataprovider.ResourcesLocation;
import gecos.gecosproject.GecosProject;

/**
 * Tests {@link ProcedureOutlining}.
 * 
 * @author aelmouss
 */
public class BlockOutlinerIT extends GecosS2STestTemplate<GSProjectVersion> {
	
	private void compute(GSProjectVersion v, boolean outlineInNewFile) {
		GecosProject p = v.getProject();
		new ComputeSSAForm(p).compute();
		new ProcedureOutlining(p, outlineInNewFile).compute();
		new RemoveSSAForm(p).compute();
	}
	
	@Test
	@UseDataProvider(location = DataFromPathProvider.class, value = DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation("resources/src-c/block-outliner/shouldPass")
	@AddStringParams({"same file", "new file"})
	public void testOutlinePass(ICxxProjectData d, String outlineInNewFile) {
		runTest(d, v -> compute(v, outlineInNewFile.contentEquals("new file")));
	}
	
	@Test
	@UseDataProvider(location = DataFromPathProvider.class, value = DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation("resources/src-c/block-outliner/shouldFail")
	@AddStringParams({"same file", "new file"})
	public void testOutlineFail(ICxxProjectData d, String outlineInNewFile) {
		expectedExceptions.register(CoreMatchers.instanceOf(RuntimeException.class));
		runTest(d, v -> compute(v, outlineInNewFile.contentEquals("new file")));
	}
}
