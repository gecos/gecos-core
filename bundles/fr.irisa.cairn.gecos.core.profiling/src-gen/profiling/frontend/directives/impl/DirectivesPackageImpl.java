/**
 */
package profiling.frontend.directives.impl;

import gecos.annotations.AnnotationsPackage;

import gecos.blocks.BlocksPackage;

import gecos.core.CorePackage;

import gecos.dag.DagPackage;

import gecos.instrs.InstrsPackage;

import gecos.types.TypesPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import profiling.frontend.directives.Directive;
import profiling.frontend.directives.DirectiveName;
import profiling.frontend.directives.DirectivesFactory;
import profiling.frontend.directives.DirectivesPackage;
import profiling.frontend.directives.FromFileDirective;
import profiling.frontend.directives.FromVariableDirective;
import profiling.frontend.directives.InjectDirective;
import profiling.frontend.directives.InjectorSourceDirective;
import profiling.frontend.directives.RandomNormalDirective;
import profiling.frontend.directives.RandomUniformDirective;
import profiling.frontend.directives.SaveDirective;
import profiling.frontend.directives.SizeDirective;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DirectivesPackageImpl extends EPackageImpl implements DirectivesPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass directiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sizeDirectiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass saveDirectiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass injectDirectiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass injectorSourceDirectiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fromFileDirectiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fromVariableDirectiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass randomUniformDirectiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass randomNormalDirectiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum directiveNameEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see profiling.frontend.directives.DirectivesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private DirectivesPackageImpl() {
		super(eNS_URI, DirectivesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link DirectivesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static DirectivesPackage init() {
		if (isInited) return (DirectivesPackage)EPackage.Registry.INSTANCE.getEPackage(DirectivesPackage.eNS_URI);

		// Obtain or create and register package
		DirectivesPackageImpl theDirectivesPackage = (DirectivesPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof DirectivesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new DirectivesPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		BlocksPackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();
		InstrsPackage.eINSTANCE.eClass();
		CorePackage.eINSTANCE.eClass();
		AnnotationsPackage.eINSTANCE.eClass();
		DagPackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theDirectivesPackage.createPackageContents();

		// Initialize created meta-data
		theDirectivesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theDirectivesPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(DirectivesPackage.eNS_URI, theDirectivesPackage);
		return theDirectivesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDirective() {
		return directiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDirective_Call() {
		return (EReference)directiveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSizeDirective() {
		return sizeDirectiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSizeDirective_NAME() {
		return (EAttribute)sizeDirectiveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSizeDirective_Address() {
		return (EReference)sizeDirectiveEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSizeDirective_SizesOutermostFirst() {
		return (EReference)sizeDirectiveEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSaveDirective() {
		return saveDirectiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSaveDirective_NAME() {
		return (EAttribute)saveDirectiveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSaveDirective_Address() {
		return (EReference)saveDirectiveEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSaveDirective_SizesOutermostFirst() {
		return (EReference)saveDirectiveEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInjectDirective() {
		return injectDirectiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInjectDirective_NAME() {
		return (EAttribute)injectDirectiveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInjectDirective_Address() {
		return (EReference)injectDirectiveEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInjectDirective_SourceDirective() {
		return (EReference)injectDirectiveEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInjectDirective_SizesOutermostFirst() {
		return (EReference)injectDirectiveEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInjectorSourceDirective() {
		return injectorSourceDirectiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFromFileDirective() {
		return fromFileDirectiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFromFileDirective_NAME() {
		return (EAttribute)fromFileDirectiveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFromFileDirective_FilePath() {
		return (EReference)fromFileDirectiveEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFromVariableDirective() {
		return fromVariableDirectiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFromVariableDirective_NAME() {
		return (EAttribute)fromVariableDirectiveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFromVariableDirective_Inst() {
		return (EReference)fromVariableDirectiveEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRandomUniformDirective() {
		return randomUniformDirectiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRandomUniformDirective_NAME() {
		return (EAttribute)randomUniformDirectiveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRandomUniformDirective_Min() {
		return (EReference)randomUniformDirectiveEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRandomUniformDirective_Max() {
		return (EReference)randomUniformDirectiveEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRandomUniformDirective_Seed() {
		return (EReference)randomUniformDirectiveEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRandomNormalDirective() {
		return randomNormalDirectiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRandomNormalDirective_NAME() {
		return (EAttribute)randomNormalDirectiveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRandomNormalDirective_Mean() {
		return (EReference)randomNormalDirectiveEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRandomNormalDirective_Stddev() {
		return (EReference)randomNormalDirectiveEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRandomNormalDirective_Seed() {
		return (EReference)randomNormalDirectiveEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDirectiveName() {
		return directiveNameEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DirectivesFactory getDirectivesFactory() {
		return (DirectivesFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		directiveEClass = createEClass(DIRECTIVE);
		createEReference(directiveEClass, DIRECTIVE__CALL);

		sizeDirectiveEClass = createEClass(SIZE_DIRECTIVE);
		createEAttribute(sizeDirectiveEClass, SIZE_DIRECTIVE__NAME);
		createEReference(sizeDirectiveEClass, SIZE_DIRECTIVE__ADDRESS);
		createEReference(sizeDirectiveEClass, SIZE_DIRECTIVE__SIZES_OUTERMOST_FIRST);

		saveDirectiveEClass = createEClass(SAVE_DIRECTIVE);
		createEAttribute(saveDirectiveEClass, SAVE_DIRECTIVE__NAME);
		createEReference(saveDirectiveEClass, SAVE_DIRECTIVE__ADDRESS);
		createEReference(saveDirectiveEClass, SAVE_DIRECTIVE__SIZES_OUTERMOST_FIRST);

		injectDirectiveEClass = createEClass(INJECT_DIRECTIVE);
		createEAttribute(injectDirectiveEClass, INJECT_DIRECTIVE__NAME);
		createEReference(injectDirectiveEClass, INJECT_DIRECTIVE__ADDRESS);
		createEReference(injectDirectiveEClass, INJECT_DIRECTIVE__SOURCE_DIRECTIVE);
		createEReference(injectDirectiveEClass, INJECT_DIRECTIVE__SIZES_OUTERMOST_FIRST);

		injectorSourceDirectiveEClass = createEClass(INJECTOR_SOURCE_DIRECTIVE);

		fromFileDirectiveEClass = createEClass(FROM_FILE_DIRECTIVE);
		createEAttribute(fromFileDirectiveEClass, FROM_FILE_DIRECTIVE__NAME);
		createEReference(fromFileDirectiveEClass, FROM_FILE_DIRECTIVE__FILE_PATH);

		fromVariableDirectiveEClass = createEClass(FROM_VARIABLE_DIRECTIVE);
		createEAttribute(fromVariableDirectiveEClass, FROM_VARIABLE_DIRECTIVE__NAME);
		createEReference(fromVariableDirectiveEClass, FROM_VARIABLE_DIRECTIVE__INST);

		randomUniformDirectiveEClass = createEClass(RANDOM_UNIFORM_DIRECTIVE);
		createEAttribute(randomUniformDirectiveEClass, RANDOM_UNIFORM_DIRECTIVE__NAME);
		createEReference(randomUniformDirectiveEClass, RANDOM_UNIFORM_DIRECTIVE__MIN);
		createEReference(randomUniformDirectiveEClass, RANDOM_UNIFORM_DIRECTIVE__MAX);
		createEReference(randomUniformDirectiveEClass, RANDOM_UNIFORM_DIRECTIVE__SEED);

		randomNormalDirectiveEClass = createEClass(RANDOM_NORMAL_DIRECTIVE);
		createEAttribute(randomNormalDirectiveEClass, RANDOM_NORMAL_DIRECTIVE__NAME);
		createEReference(randomNormalDirectiveEClass, RANDOM_NORMAL_DIRECTIVE__MEAN);
		createEReference(randomNormalDirectiveEClass, RANDOM_NORMAL_DIRECTIVE__STDDEV);
		createEReference(randomNormalDirectiveEClass, RANDOM_NORMAL_DIRECTIVE__SEED);

		// Create enums
		directiveNameEEnum = createEEnum(DIRECTIVE_NAME);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		InstrsPackage theInstrsPackage = (InstrsPackage)EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI);
		BlocksPackage theBlocksPackage = (BlocksPackage)EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		sizeDirectiveEClass.getESuperTypes().add(this.getDirective());
		saveDirectiveEClass.getESuperTypes().add(this.getDirective());
		injectDirectiveEClass.getESuperTypes().add(this.getDirective());
		injectorSourceDirectiveEClass.getESuperTypes().add(this.getDirective());
		fromFileDirectiveEClass.getESuperTypes().add(this.getInjectorSourceDirective());
		fromVariableDirectiveEClass.getESuperTypes().add(this.getInjectorSourceDirective());
		randomUniformDirectiveEClass.getESuperTypes().add(this.getInjectorSourceDirective());
		randomNormalDirectiveEClass.getESuperTypes().add(this.getInjectorSourceDirective());

		// Initialize classes and features; add operations and parameters
		initEClass(directiveEClass, Directive.class, "Directive", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDirective_Call(), theInstrsPackage.getCallInstruction(), null, "call", null, 0, 1, Directive.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(directiveEClass, null, "parse", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(directiveEClass, theBlocksPackage.getBlock(), "createCodeBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(directiveEClass, theEcorePackage.getEString(), "printLocation", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(sizeDirectiveEClass, SizeDirective.class, "SizeDirective", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSizeDirective_NAME(), this.getDirectiveName(), "NAME", "$size", 0, 1, SizeDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSizeDirective_Address(), theCorePackage.getSymbol(), null, "address", null, 0, 1, SizeDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSizeDirective_SizesOutermostFirst(), theInstrsPackage.getInstruction(), null, "sizesOutermostFirst", null, 0, -1, SizeDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(sizeDirectiveEClass, null, "parse", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(sizeDirectiveEClass, theBlocksPackage.getBlock(), "createCodeBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(saveDirectiveEClass, SaveDirective.class, "SaveDirective", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSaveDirective_NAME(), this.getDirectiveName(), "NAME", "$save", 0, 1, SaveDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSaveDirective_Address(), theCorePackage.getSymbol(), null, "address", null, 0, 1, SaveDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSaveDirective_SizesOutermostFirst(), theInstrsPackage.getInstruction(), null, "sizesOutermostFirst", null, 0, -1, SaveDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(saveDirectiveEClass, null, "parse", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(saveDirectiveEClass, theBlocksPackage.getBlock(), "createCodeBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(injectDirectiveEClass, InjectDirective.class, "InjectDirective", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getInjectDirective_NAME(), this.getDirectiveName(), "NAME", "$inject", 0, 1, InjectDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInjectDirective_Address(), theInstrsPackage.getInstruction(), null, "address", null, 0, 1, InjectDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInjectDirective_SourceDirective(), this.getInjectorSourceDirective(), null, "sourceDirective", null, 0, 1, InjectDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInjectDirective_SizesOutermostFirst(), theInstrsPackage.getInstruction(), null, "sizesOutermostFirst", null, 0, -1, InjectDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(injectDirectiveEClass, null, "parse", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(injectDirectiveEClass, theBlocksPackage.getBlock(), "createCodeBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(injectorSourceDirectiveEClass, InjectorSourceDirective.class, "InjectorSourceDirective", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fromFileDirectiveEClass, FromFileDirective.class, "FromFileDirective", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFromFileDirective_NAME(), this.getDirectiveName(), "NAME", "$from_file", 0, 1, FromFileDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFromFileDirective_FilePath(), theInstrsPackage.getInstruction(), null, "filePath", null, 0, 1, FromFileDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(fromFileDirectiveEClass, null, "parse", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(fromFileDirectiveEClass, theBlocksPackage.getBlock(), "createCodeBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(fromVariableDirectiveEClass, FromVariableDirective.class, "FromVariableDirective", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFromVariableDirective_NAME(), this.getDirectiveName(), "NAME", "$from_var", 0, 1, FromVariableDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFromVariableDirective_Inst(), theInstrsPackage.getInstruction(), null, "inst", null, 0, 1, FromVariableDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(fromVariableDirectiveEClass, null, "parse", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(fromVariableDirectiveEClass, theBlocksPackage.getBlock(), "createCodeBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(randomUniformDirectiveEClass, RandomUniformDirective.class, "RandomUniformDirective", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRandomUniformDirective_NAME(), this.getDirectiveName(), "NAME", "$random_uniform", 0, 1, RandomUniformDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRandomUniformDirective_Min(), theInstrsPackage.getInstruction(), null, "min", null, 0, 1, RandomUniformDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRandomUniformDirective_Max(), theInstrsPackage.getInstruction(), null, "max", null, 0, 1, RandomUniformDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRandomUniformDirective_Seed(), theInstrsPackage.getInstruction(), null, "seed", null, 0, 1, RandomUniformDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(randomUniformDirectiveEClass, null, "parse", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(randomUniformDirectiveEClass, theBlocksPackage.getBlock(), "createCodeBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(randomNormalDirectiveEClass, RandomNormalDirective.class, "RandomNormalDirective", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRandomNormalDirective_NAME(), this.getDirectiveName(), "NAME", "$random_normal", 0, 1, RandomNormalDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRandomNormalDirective_Mean(), theInstrsPackage.getInstruction(), null, "mean", null, 0, 1, RandomNormalDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRandomNormalDirective_Stddev(), theInstrsPackage.getInstruction(), null, "stddev", null, 0, 1, RandomNormalDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRandomNormalDirective_Seed(), theInstrsPackage.getInstruction(), null, "seed", null, 0, 1, RandomNormalDirective.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(randomNormalDirectiveEClass, null, "parse", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(randomNormalDirectiveEClass, theBlocksPackage.getBlock(), "createCodeBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(directiveNameEEnum, DirectiveName.class, "DirectiveName");
		addEEnumLiteral(directiveNameEEnum, DirectiveName.SIZE);
		addEEnumLiteral(directiveNameEEnum, DirectiveName.SAVE);
		addEEnumLiteral(directiveNameEEnum, DirectiveName.INJECT);
		addEEnumLiteral(directiveNameEEnum, DirectiveName.FROM_FILE);
		addEEnumLiteral(directiveNameEEnum, DirectiveName.FROM_VAR);
		addEEnumLiteral(directiveNameEEnum, DirectiveName.RANDOM_UNIFORM);
		addEEnumLiteral(directiveNameEEnum, DirectiveName.RANDOM_NORMAL);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
		   });
	}

} //DirectivesPackageImpl
