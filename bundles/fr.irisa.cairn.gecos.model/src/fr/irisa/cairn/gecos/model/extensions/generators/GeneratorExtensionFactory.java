package fr.irisa.cairn.gecos.model.extensions.generators;

import java.lang.reflect.Constructor;

/**
 * Factory for the extensions of code generators. 
 * 
 * @author Antoine Floc'h - Initial contribution and API
 * 
 */
final class GeneratorExtensionFactory {

	IGecosCodeGenerator buildGenerator(Class<? extends IGecosCodeGenerator> generatorClass) {

		Constructor<Object> constructor = getDefaultConstructor(generatorClass);
		try {
			Object generator = constructor.newInstance();
			return (IGecosCodeGenerator) generator;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Constructor<Object> getDefaultConstructor(Class generatorClass) {
		Constructor<Object>[] declaredConstructors = (Constructor<Object>[]) generatorClass
				.getDeclaredConstructors();
		for (Constructor<Object> constructor : declaredConstructors) {
			Class<?>[] parameterTypes = constructor.getParameterTypes();
			if (parameterTypes.length == 0) {
					return constructor;
			}
		}
		throw new IllegalArgumentException(
				"No matching constructor in the generator ("
						+ generatorClass.getCanonicalName() + ")");
	}
}