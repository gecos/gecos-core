/**
 */
package gecos.blocks;

import fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;

import gecos.instrs.BranchType;
import gecos.instrs.BreakInstruction;
import gecos.instrs.ContinueInstruction;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>For Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.blocks.ForBlock#getInitBlock <em>Init Block</em>}</li>
 *   <li>{@link gecos.blocks.ForBlock#getTestBlock <em>Test Block</em>}</li>
 *   <li>{@link gecos.blocks.ForBlock#getStepBlock <em>Step Block</em>}</li>
 *   <li>{@link gecos.blocks.ForBlock#getBodyBlock <em>Body Block</em>}</li>
 * </ul>
 *
 * @see gecos.blocks.BlocksPackage#getForBlock()
 * @model annotation="gmf.node figure='rectangle' label='number' label.placement='external' label.icon='false'"
 * @generated
 */
public interface ForBlock extends Block, BlocksVisitable, Loop {
	/**
	 * Returns the value of the '<em><b>Init Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Init Block</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Init Block</em>' containment reference.
	 * @see #setInitBlock(BasicBlock)
	 * @see gecos.blocks.BlocksPackage#getForBlock_InitBlock()
	 * @model containment="true"
	 *        annotation="gmf.compartment foo='bar'"
	 * @generated
	 */
	BasicBlock getInitBlock();

	/**
	 * Sets the value of the '{@link gecos.blocks.ForBlock#getInitBlock <em>Init Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Init Block</em>' containment reference.
	 * @see #getInitBlock()
	 * @generated
	 */
	void setInitBlock(BasicBlock value);

	/**
	 * Returns the value of the '<em><b>Test Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Block</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Block</em>' containment reference.
	 * @see #setTestBlock(BasicBlock)
	 * @see gecos.blocks.BlocksPackage#getForBlock_TestBlock()
	 * @model containment="true"
	 *        annotation="gmf.compartment foo='bar'"
	 * @generated
	 */
	BasicBlock getTestBlock();

	/**
	 * Sets the value of the '{@link gecos.blocks.ForBlock#getTestBlock <em>Test Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Test Block</em>' containment reference.
	 * @see #getTestBlock()
	 * @generated
	 */
	void setTestBlock(BasicBlock value);

	/**
	 * Returns the value of the '<em><b>Step Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Step Block</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Step Block</em>' containment reference.
	 * @see #setStepBlock(BasicBlock)
	 * @see gecos.blocks.BlocksPackage#getForBlock_StepBlock()
	 * @model containment="true"
	 *        annotation="gmf.compartment foo='bar'"
	 * @generated
	 */
	BasicBlock getStepBlock();

	/**
	 * Sets the value of the '{@link gecos.blocks.ForBlock#getStepBlock <em>Step Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Step Block</em>' containment reference.
	 * @see #getStepBlock()
	 * @generated
	 */
	void setStepBlock(BasicBlock value);

	/**
	 * Returns the value of the '<em><b>Body Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body Block</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body Block</em>' containment reference.
	 * @see #setBodyBlock(Block)
	 * @see gecos.blocks.BlocksPackage#getForBlock_BodyBlock()
	 * @model containment="true"
	 *        annotation="gmf.compartment foo='bar'"
	 * @generated
	 */
	Block getBodyBlock();

	/**
	 * Sets the value of the '{@link gecos.blocks.ForBlock#getBodyBlock <em>Body Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body Block</em>' containment reference.
	 * @see #getBodyBlock()
	 * @generated
	 */
	void setBodyBlock(Block value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitForBlock(this);'"
	 * @generated
	 */
	void accept(BlocksVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%fr.irisa.cairn.gecos.model.tools.utils.BlocksContinuesBreaksFinder%&gt; finder = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.BlocksContinuesBreaksFinder%&gt;();\nfinder.doSwitch(this.getBodyBlock());\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt;unmodifiableEList(finder.getContinues());'"
	 * @generated
	 */
	EList<BasicBlock> listContinueBlocks();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder%&gt; finder = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder%&gt;();\nfinder.doSwitch(this.getBodyBlock());\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.instrs.ContinueInstruction%&gt;&gt;unmodifiableEList(finder.getContinues());'"
	 * @generated
	 */
	EList<ContinueInstruction> listContinueInstructions();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%fr.irisa.cairn.gecos.model.tools.utils.BlocksContinuesBreaksFinder%&gt; finder = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.BlocksContinuesBreaksFinder%&gt;();\nfinder.doSwitch(this.getBodyBlock());\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt;unmodifiableEList(finder.getBreaks());'"
	 * @generated
	 */
	EList<BasicBlock> listBreakBlocks();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder%&gt; finder = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder%&gt;();\nfinder.doSwitch(this.getBodyBlock());\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.instrs.BreakInstruction%&gt;&gt;unmodifiableEList(finder.getBreaks());'"
	 * @generated
	 */
	EList<BreakInstruction> listBreakInstructions();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" mngDataType="gecos.core.BlockCopyManager" mngUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.blocks.ForBlock%&gt; forBlock = &lt;%gecos.blocks.BlocksFactory%&gt;.eINSTANCE.createForBlock();\n&lt;%gecos.blocks.Block%&gt; _managedCopy = this.getInitBlock().managedCopy(mng);\nforBlock.setInitBlock(((&lt;%gecos.blocks.BasicBlock%&gt;) _managedCopy));\n&lt;%gecos.blocks.Block%&gt; _managedCopy_1 = this.getTestBlock().managedCopy(mng);\nforBlock.setTestBlock(((&lt;%gecos.blocks.BasicBlock%&gt;) _managedCopy_1));\n&lt;%gecos.blocks.Block%&gt; _managedCopy_2 = this.getStepBlock().managedCopy(mng);\nforBlock.setStepBlock(((&lt;%gecos.blocks.BasicBlock%&gt;) _managedCopy_2));\n&lt;%gecos.blocks.Block%&gt; _managedCopy_3 = this.getBodyBlock().managedCopy(mng);\nforBlock.setBodyBlock(((&lt;%gecos.blocks.BasicBlock%&gt;) _managedCopy_3));\n&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory%&gt;.copyAnnotations(this, forBlock);\nmng.link(this, forBlock);\nreturn forBlock;'"
	 * @generated
	 */
	Block managedCopy(BlockCopyManager mng);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model oldbUnique="false" newbUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.Block%&gt; _bodyBlock = this.getBodyBlock();\nboolean _tripleEquals = (oldb == _bodyBlock);\nif (_tripleEquals)\n{\n\tthis.setBodyBlock(newb);\n}\nelse\n{\n\t&lt;%gecos.blocks.BasicBlock%&gt; _initBlock = this.getInitBlock();\n\tboolean _tripleEquals_1 = (oldb == _initBlock);\n\tif (_tripleEquals_1)\n\t{\n\t\tthis.setInitBlock(((&lt;%gecos.blocks.BasicBlock%&gt;) newb));\n\t}\n\telse\n\t{\n\t\t&lt;%gecos.blocks.BasicBlock%&gt; _stepBlock = this.getStepBlock();\n\t\tboolean _tripleEquals_2 = (oldb == _stepBlock);\n\t\tif (_tripleEquals_2)\n\t\t{\n\t\t\tthis.setStepBlock(((&lt;%gecos.blocks.BasicBlock%&gt;) newb));\n\t\t}\n\t\telse\n\t\t{\n\t\t\t&lt;%gecos.blocks.BasicBlock%&gt; _testBlock = this.getTestBlock();\n\t\t\tboolean _tripleEquals_3 = (oldb == _testBlock);\n\t\t\tif (_tripleEquals_3)\n\t\t\t{\n\t\t\t\tthis.setTestBlock(((&lt;%gecos.blocks.BasicBlock%&gt;) newb));\n\t\t\t}\n\t\t}\n\t}\n}'"
	 * @generated
	 */
	void replace(Block oldb, Block newb);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getInitBlock().simplifyBlock();\nthis.getStepBlock().simplifyBlock();\nthis.getTestBlock().simplifyBlock();\nthis.getBodyBlock().simplifyBlock();\nreturn this;'"
	 * @generated
	 */
	Block simplifyBlock();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model insertedBlockUnique="false" existingBlockUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getBodyBlock().addBlockAfter(insertedBlock, existingBlock);'"
	 * @generated
	 */
	void addBlockAfter(Block insertedBlock, Block existingBlock);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bbUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.BasicBlock%&gt; _testBlock = this.getTestBlock();\nboolean _tripleEquals = (_testBlock == bb);\nif (_tripleEquals)\n{\n\tthis.setTestBlock(null);\n}\nelse\n{\n\t&lt;%gecos.blocks.BasicBlock%&gt; _initBlock = this.getInitBlock();\n\tboolean _tripleEquals_1 = (_initBlock == bb);\n\tif (_tripleEquals_1)\n\t{\n\t\tthis.setInitBlock(null);\n\t}\n\telse\n\t{\n\t\t&lt;%gecos.blocks.BasicBlock%&gt; _stepBlock = this.getStepBlock();\n\t\tboolean _tripleEquals_2 = (_stepBlock == bb);\n\t\tif (_tripleEquals_2)\n\t\t{\n\t\t\tthis.setStepBlock(null);\n\t\t}\n\t\telse\n\t\t{\n\t\t\t&lt;%gecos.blocks.Block%&gt; _bodyBlock = this.getBodyBlock();\n\t\t\tboolean _tripleEquals_3 = (_bodyBlock == bb);\n\t\t\tif (_tripleEquals_3)\n\t\t\t{\n\t\t\t\tthis.setBodyBlock(null);\n\t\t\t}\n\t\t\telse\n\t\t\t{\n\t\t\t\t&lt;%gecos.blocks.Block%&gt; _bodyBlock_1 = this.getBodyBlock();\n\t\t\t\tboolean _tripleNotEquals = (_bodyBlock_1 != null);\n\t\t\t\tif (_tripleNotEquals)\n\t\t\t\t{\n\t\t\t\t\tthis.getBodyBlock().removeBlock(bb);\n\t\t\t\t}\n\t\t\t}\n\t\t}\n\t}\n}'"
	 * @generated
	 */
	void removeBlock(Block bb);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" dispatchUnique="false" condUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.BasicBlock%&gt; _initBlock = this.getInitBlock();\nboolean _tripleNotEquals = (_initBlock != null);\nif (_tripleNotEquals)\n{\n\treturn this.getInitBlock().connectFromBasic(dispatch, cond);\n}\n&lt;%gecos.blocks.BasicBlock%&gt; _testBlock = this.getTestBlock();\nboolean _tripleNotEquals_1 = (_testBlock != null);\nif (_tripleNotEquals_1)\n{\n\treturn this.getTestBlock().connectFromBasic(dispatch, cond);\n}\n&lt;%gecos.blocks.Block%&gt; _bodyBlock = this.getBodyBlock();\nboolean _tripleNotEquals_2 = (_bodyBlock != null);\nif (_tripleNotEquals_2)\n{\n\treturn this.getBodyBlock().connectFromBasic(dispatch, cond);\n}\n&lt;%gecos.blocks.BasicBlock%&gt; _stepBlock = this.getStepBlock();\nboolean _tripleNotEquals_3 = (_stepBlock != null);\nif (_tripleNotEquals_3)\n{\n\treturn this.getStepBlock().connectFromBasic(dispatch, cond);\n}\nreturn null;'"
	 * @generated
	 */
	ControlEdge connectFromBasic(BasicBlock dispatch, BranchType cond);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" toUnique="false" condUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%gecos.blocks.ControlEdge%&gt;&gt; res = &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.blocks.ControlEdge%&gt;&gt;newBasicEList();\n&lt;%gecos.blocks.BasicBlock%&gt; _testBlock = this.getTestBlock();\nboolean _tripleNotEquals = (_testBlock != null);\nif (_tripleNotEquals)\n{\n\tres.addAll(this.getTestBlock().connectTo(to, &lt;%gecos.instrs.BranchType%&gt;.IF_FALSE));\n}\n&lt;%gecos.blocks.Block%&gt; _bodyBlock = this.getBodyBlock();\nboolean _tripleNotEquals_1 = (_bodyBlock != null);\nif (_tripleNotEquals_1)\n{\n\tfinal &lt;%fr.irisa.cairn.gecos.model.tools.utils.BlocksContinuesBreaksFinder%&gt; visitor = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.BlocksContinuesBreaksFinder%&gt;();\n\tvisitor.doSwitch(this.getBodyBlock());\n\t&lt;%java.util.List%&gt;&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt; _breaks = visitor.getBreaks();\n\tfor (final &lt;%gecos.blocks.BasicBlock%&gt; bb : _breaks)\n\t{\n\t\tres.addAll(bb.connectTo(to, &lt;%gecos.instrs.BranchType%&gt;.UNCONDITIONAL));\n\t}\n}\nelse\n{\n\t&lt;%gecos.blocks.BasicBlock%&gt; _testBlock_1 = this.getTestBlock();\n\tboolean _tripleNotEquals_2 = (_testBlock_1 != null);\n\tif (_tripleNotEquals_2)\n\t{\n\t\tres.addAll(this.getTestBlock().connectTo(to, &lt;%gecos.instrs.BranchType%&gt;.IF_TRUE));\n\t}\n}\nreturn res;'"
	 * @generated
	 */
	EList<ControlEdge> connectTo(Block to, BranchType cond);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.blocks.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _number = this.getNumber();\nreturn (\"For\" + &lt;%java.lang.Integer%&gt;.valueOf(_number));'"
	 * @generated
	 */
	String toShortString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.blocks.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _pathString = this.getPathString();\nint _number = this.getNumber();\n&lt;%java.lang.String%&gt; _plus = (_pathString + &lt;%java.lang.Integer%&gt;.valueOf(_number));\n&lt;%java.lang.String%&gt; string = (_plus + \"(\");\n&lt;%java.lang.String%&gt; _string = string;\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; _instructions = this.getInitBlock().getInstructions();\nstring = (_string + _instructions);\n&lt;%java.lang.String%&gt; _string_1 = string;\nstring = (_string_1 + \";\");\n&lt;%java.lang.String%&gt; _string_2 = string;\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; _instructions_1 = this.getTestBlock().getInstructions();\nstring = (_string_2 + _instructions_1);\n&lt;%java.lang.String%&gt; _string_3 = string;\nstring = (_string_3 + \";\");\n&lt;%java.lang.String%&gt; _string_4 = string;\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; _instructions_2 = this.getStepBlock().getInstructions();\nstring = (_string_4 + _instructions_2);\n&lt;%java.lang.String%&gt; _string_5 = string;\nstring = (_string_5 + \")\");\n&lt;%gecos.blocks.Block%&gt; _bodyBlock = this.getBodyBlock();\nboolean _tripleNotEquals = (_bodyBlock != null);\nif (_tripleNotEquals)\n{\n\t&lt;%java.lang.String%&gt; _string_6 = string;\n\t&lt;%java.lang.String%&gt; _shortString = this.getBodyBlock().toShortString();\n\t&lt;%java.lang.String%&gt; _plus_1 = (\"{\" + _shortString);\n\t&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + \"}\");\n\tstring = (_string_6 + _plus_2);\n}\nelse\n{\n\t&lt;%java.lang.String%&gt; _string_7 = string;\n\tstring = (_string_7 + \"null\");\n}\nreturn string;'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.blocks.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return super.getPathString(\"FOR\");'"
	 * @generated
	 */
	String getPathString();

} // ForBlock
