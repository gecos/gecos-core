package fr.irisa.cairn.gecos.model.modules;

import java.io.File;
import java.io.IOException;

import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import gecos.gecosproject.GecosHeaderDirectory;
import gecos.gecosproject.GecosProject;

@GSModule("Add Include directories to a GecosProject.\n"
		+ "\nSee: 'CreateGecosProject' module.")
public class AddIncludeDirToGecosProject {
	
	private GecosProject project;

	@GSModuleConstructor(
		"-arg1: GecosProject to which the include directories will be added.\n"
	  + "-arg2: List of include directory paths.")
	public AddIncludeDirToGecosProject(GecosProject project, String... includePaths) {
		this(project, false, includePaths);
	}
	
	public AddIncludeDirToGecosProject(GecosProject project, boolean isStandardInclude, String... includePaths) {
		this.project=project;
		for (String name : includePaths) {
			try {
				File file = new File(new File(name).getCanonicalPath());
				if (file.isDirectory()) {
					addDirectory(file, isStandardInclude);
				} else {
					throw new IOException(" File " +file.getAbsolutePath()+ " does not exist");
				}
			} catch (IOException e) {
				throw new RuntimeException("Could not add include directory "+name,e);
			}
		}
	}

	private void addDirectory(File dir, boolean isStandardInclude) {
		GecosHeaderDirectory hfile = GecosUserCoreFactory.includeDir(dir.getAbsolutePath(), isStandardInclude);
		project.getIncludes().add(hfile);
		
		for (File file : dir.listFiles()) {
			//do not visit hidden directories (whose name starts with a dot, like ".svn")
			if (file.isDirectory() && !file.getName().startsWith(".")) {
				addDirectory(file, isStandardInclude);
			}
		}
	}

	public void compute() {}
}
