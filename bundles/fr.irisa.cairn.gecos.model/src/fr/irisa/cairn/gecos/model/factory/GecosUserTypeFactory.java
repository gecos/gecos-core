package fr.irisa.cairn.gecos.model.factory;

import java.util.Arrays;
import java.util.List;

import org.eclipse.emf.common.util.EList;

import gecos.core.Scope;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.types.ACChannelType;
import gecos.types.ACComplexType;
import gecos.types.ACFixedType;
import gecos.types.ACFloatType;
import gecos.types.ACIntType;
import gecos.types.AliasType;
import gecos.types.ArrayType;
import gecos.types.BaseType;
import gecos.types.BoolType;
import gecos.types.EnumType;
import gecos.types.Enumerator;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.FloatType;
import gecos.types.FunctionType;
import gecos.types.IntegerType;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.PtrType;
import gecos.types.QuantificationMode;
import gecos.types.RecordType;
import gecos.types.SignModifiers;
import gecos.types.SimdType;
import gecos.types.Type;
import gecos.types.TypesFactory;
import gecos.types.UndefinedType;
import gecos.types.VoidType;

public class GecosUserTypeFactory {


	public interface ACTypes {
		public static final String ACType = "";
		public static final String ACIntType = "ac_int";
		public static final String ACFloatType = "ac_float";
		public static final String ACFixedType = "ac_fixed";
		public static final String ACChannelType = "ac_channel";
		public static final String ACComplexType= "ac_complex";
	}

	public interface APTypes {
		public static final String ACType = "";
		public static final String APIntType = "ap_int";		
		public static final String APUIntType = "ap_uint";
		public static final String APFixedType = "ap_fixed";
	}
	
	public interface TypeSizes {
		// architecture dependent sizes
		public static final int defaultPointerSize = -1; //used for pointers, arrays, functions.
		public static final int defaultLongSize = -1;
		
		// misc sizes
		public static final int defaultVoidSize = 0;
		public static final int defaultBoolSize = 1;
		
		// integer sizes
		public static final int defaultCharSize = 8; // char
		public static final int defaultShortSize = 16; // short
		public static final int defaultIntSize = 32; // int
		public static final int defaultLongLongSize = 64; // long long
		
		// floating point sizes
		public static final int defaultFloatSize = 32; // float
		public static final int defaultDoubleSize = 64; // double
		public static final int defaultLongDoubleSize = 80; // x86 Extended Precision Format (long double)
	}
	
	private static TypesFactory factory = TypesFactory.eINSTANCE;
	
	private static Scope scope;
	
	//TODO with the new type model it is easier to reactivate types reuse, however
	// should be responsible of copying/handling type references when changing a type specifier; to avoid 
	// unintentionally affecting other objects referring to the same type !
	//e.g.: 
	// - S0: int0;
	// - calling INT() with the intension to create a new type, say int1, will return the existing int0 (int1==int0)
	// - S1: int1
	// - modifying int1 will also (unintentionally) impact S0.
	//
	//The factory method should be very clear about this problem and maybe provide option to enforce creating a new type ?
	public static boolean ALWAYS_RETURN_NEW_TYPE = true; // False is not safe!!!! due to type modifier

	private static <T extends Type> T addTypeToScope(T t) {
		if(scope!=null) {
			if(!ALWAYS_RETURN_NEW_TYPE) {
				EList<Type> types = scope.getTypes();
				for (int i = 0; i < types.size(); i++) {
					Type type = types.get(i);
					if (type.isEqual(t)) {
						@SuppressWarnings("unchecked")
						T t2 = (T)type;
						return t2;
					}
				}
			}
			t.installOn(scope);
		} else {
			throw new RuntimeException("Scope is not provided in the factory \""+GecosUserTypeFactory.class.getCanonicalName()+"\". "
					+ "Please initialize the factory using "+GecosUserTypeFactory.class.getSimpleName()+".setScope(Scope) with the proper ProcedureSet scope.");
		}
		return t;	
	}
	
	public static Scope getScope() {
		return scope;
	}
	
	public static void setScope(Scope _scope) {
		//argument check
		if(_scope==null)
			throw new IllegalArgumentException("GecosUserTypeFactory error: Cannot specify a null Scope for type creation.");
		scope = _scope;
	}
	public GecosUserTypeFactory() {

	}

	/* ****************************
	 * VOID BASE TYPE
	 ****************************/
	public static VoidType VOID() {
		VoidType voidType = factory.createVoidType();
		voidType.setSize(TypeSizes.defaultVoidSize);
		return addTypeToScope(voidType);
	}
	
	/* ****************************
	 * Boolean BASE TYPES
	 ****************************/
	public static BoolType BOOL() {
		BoolType baseType = factory.createBoolType();
		baseType.setSize(TypeSizes.defaultBoolSize);
		return addTypeToScope(baseType);
	}
	
	/* ****************************
	 * Floating point BASE TYPES
	 ****************************/
	public static FloatType FLOAT() {
		FloatType flotaType = factory.createFloatType();
		flotaType.setPrecision(FloatPrecisions.SINGLE);
		flotaType.setSize(TypeSizes.defaultFloatSize);
		return addTypeToScope(flotaType);
	}
	public static FloatType DOUBLE() {
		FloatType doubleType = factory.createFloatType();
		doubleType.setPrecision(FloatPrecisions.DOUBLE);
		doubleType.setSize(TypeSizes.defaultDoubleSize);
		return addTypeToScope(doubleType);
	}
	public static FloatType LONGDOUBLE() {
		FloatType doubleType = factory.createFloatType();
		doubleType.setPrecision(FloatPrecisions.LONG_DOUBLE);
		doubleType.setSize(TypeSizes.defaultLongDoubleSize);
		return addTypeToScope(doubleType);
	}

	/* ****************************
	 * Integer BASE TYPES
	 ****************************/
	public static IntegerType CHAR(SignModifiers sign) {
		IntegerType baseType = factory.createIntegerType();
		baseType.setSignModifier(sign);
		baseType.setType(IntegerTypes.CHAR);
		baseType.setSize(TypeSizes.defaultCharSize);
		return addTypeToScope(baseType);
	}
	public static IntegerType SHORT(SignModifiers sign) {
		IntegerType baseType = factory.createIntegerType();
		baseType.setSignModifier(sign);
		baseType.setType(IntegerTypes.SHORT);
		baseType.setSize(TypeSizes.defaultShortSize);
		return addTypeToScope(baseType);
	}
	public static IntegerType INT(SignModifiers sign) {
		IntegerType baseType = factory.createIntegerType();
		baseType.setSignModifier(sign);
		baseType.setType(IntegerTypes.INT);
		baseType.setSize(TypeSizes.defaultIntSize);
		return addTypeToScope(baseType);
	}
	public static IntegerType LONG(SignModifiers sign) {
		IntegerType baseType = factory.createIntegerType();
		baseType.setSignModifier(sign);
		baseType.setType(IntegerTypes.LONG);
		baseType.setSize(TypeSizes.defaultLongSize);
		return addTypeToScope(baseType);
	}
	public static IntegerType LONGLONG(SignModifiers sign) {
		IntegerType baseType = factory.createIntegerType();
		baseType.setSignModifier(sign);
		baseType.setType(IntegerTypes.LONG_LONG);
		baseType.setSize(TypeSizes.defaultLongLongSize);
		return addTypeToScope(baseType);
	}
	
	/* ****************************
	 * Default integer BASE TYPES
	 ****************************/
	public static IntegerType CHAR()     { return CHAR    (SignModifiers.NONE); }
	public static IntegerType SHORT()    { return SHORT   (SignModifiers.NONE); }
	public static IntegerType INT()      { return INT     (SignModifiers.NONE); }
	public static IntegerType LONG()     { return LONG    (SignModifiers.NONE); }
	public static IntegerType LONGLONG() { return LONGLONG(SignModifiers.NONE); }
	
	/* ****************************
	 * SIGNED integer BASE TYPES
	 ****************************/
	public static IntegerType SCHAR()     { return CHAR    (SignModifiers.SIGNED); }
	public static IntegerType SSHORT()    { return SHORT   (SignModifiers.SIGNED); }
	public static IntegerType SINT()      { return INT     (SignModifiers.SIGNED); }
	public static IntegerType SLONG()     { return LONG    (SignModifiers.SIGNED); }
	public static IntegerType SLONGLONG() { return LONGLONG(SignModifiers.SIGNED); }
	
	/* ****************************
	 * UNSIGNED integer BASE TYPES
	 ****************************/
	public static IntegerType UCHAR()     { return CHAR    (SignModifiers.UNSIGNED); }
	public static IntegerType USHORT()    { return SHORT   (SignModifiers.UNSIGNED); }
	public static IntegerType UINT()      { return INT     (SignModifiers.UNSIGNED); }
	public static IntegerType ULONG()     { return LONG    (SignModifiers.UNSIGNED); }
	public static IntegerType ULONGLONG() { return LONGLONG(SignModifiers.UNSIGNED); }
	
	/* ****************************
	 * Pointers and Arrays
	 ****************************/
	public static PtrType PTR(Type base) {
		PtrType ptr = factory.createPtrType();
		ptr.setBase(base);
		return addTypeToScope(ptr);
	}
	
	public static PtrType PTR(Type scalarBase, int nbDims) {
		if(scalarBase == null || nbDims < 1) {
			throw new IllegalArgumentException("invalid arguments: " + scalarBase + ", " + nbDims);
		}
		
		PtrType ptrType = PTR(scalarBase);
		for(int dim = 1; dim < nbDims; dim++) {
			ptrType = PTR(ptrType);
		}
		return addTypeToScope(ptrType);
	}

	public static ArrayType ARRAY(Type type, Instruction sizeInstr) {
		ArrayType arrayType = factory.createArrayType();
		arrayType.setBase(type);
		arrayType.setSizeExpr(sizeInstr);
		if (sizeInstr instanceof IntInstruction) {
			arrayType.setLower(0);
			arrayType.setUpper(((IntInstruction)sizeInstr).getValue()-1);
		} else {
			arrayType.setLower(0);
			arrayType.setUpper(-1);
		}
		return addTypeToScope(arrayType);
	}

	public static ArrayType ARRAY(Type base, long size) {
		return ARRAY(base,GecosUserInstructionFactory.Int(size));
	}
	
	/**
	 * @param scalarBase
	 * @param nbDims
	 * @param sizes_outermost_first 
	 * 		size expression of each dimension, OUTERMOST first.
	 * @return ArrayType of <code>nbDims</code> dimensions.
	 * 		e.g: ARRAY(INT(), 2, [3, 5]) returns int[3][5]
	 */
	public static ArrayType ARRAY(Type scalarBase, int nbDims, List<Instruction> sizes_outermost_first) {
		if(scalarBase == null || nbDims < 1 || sizes_outermost_first.size() != nbDims) {
			throw new IllegalArgumentException("invalid arguments: " + scalarBase + ", " + nbDims + ", " + sizes_outermost_first);
		}
		
		ArrayType arrayType = ARRAY(scalarBase, sizes_outermost_first.get(nbDims-1));
		for(int dim = nbDims-2; dim >= 0; dim--) {
			arrayType = ARRAY(arrayType, sizes_outermost_first.get(dim));
		}
		
		return addTypeToScope(arrayType);
	}
	
	/**
	 * @param scalarBase
	 * @param nbDims
	 * @param sizes_outermost_first 
	 * 		size expression of each dimension, OUTERMOST first.
	 * @return ArrayType of <code>nbDims</code> dimensions.
	 * 		e.g: ARRAY(INT(), 2, [3, 5]) returns int[3][5]
	 */
	public static ArrayType ARRAY(Type scalarBase, int nbDims, long[] sizes_outermost_first) {
		if(scalarBase == null || nbDims < 1 || sizes_outermost_first.length != nbDims) {
			throw new IllegalArgumentException("invalid arguments: " + scalarBase + ", " + nbDims + ", " + sizes_outermost_first);
		}
		
		ArrayType arrayType = ARRAY(scalarBase, sizes_outermost_first[nbDims-1]);
		for(int dim = nbDims-2; dim >= 0; dim--) {
			arrayType = ARRAY(arrayType, sizes_outermost_first[dim]);
		}
		
		return addTypeToScope(arrayType);
	}
	
	public static ArrayType STRING(long size) {
		return ARRAY(CHAR(), size);
	}
	
	/* ****************************
	 * MODIFIERS
	 ****************************/
	
	public static AliasType ALIAS(Type typeFor, String name) {
		AliasType aliasType = factory.createAliasType();
		aliasType.setAlias(typeFor);
		aliasType.setName(name);
		return addTypeToScope(aliasType);
	}

//	/**
//	 * @deprecated use {@link GecosUserTypeFactory#ALIAS()}
//	 */
//	@Deprecated
//	public static AliasType AliasType(String name, Type typeFor) {
//		return ALIAS(typeFor, name);
//	}
//
//	public static ConstType CONST(Type base) {
//		ConstType constType = factory.createConstType();
//		constType.setBase(base);
//		return addTypeToScope(constType);
//	}
//	
//	public static VolatileType VOLATILE(Type base) {
//		VolatileType volatil = factory.createVolatileType();
//		volatil.setBase(base);
//		return addTypeToScope(volatil);
//	}
//	
//	public static StaticType STATIC(Type base) {
//		StaticType staticType = factory.createStaticType();
//		staticType.setBase(base);
//		return addTypeToScope(staticType);
//	}
//
//	public static RegisterType REGISTER(Type type) {
//		RegisterType registerType = factory.createRegisterType();
//		registerType.setBase(type);
//		return addTypeToScope(registerType);
//	}

	/* ****************************
	 * FUNCTIONS
	 ****************************/
	
//	@Deprecated
//	public static FunctionType functType() {
//		return FUNCTION(null);
//	}
//
//	@Deprecated
//	public static FunctionType voidFunctType() {
//		return FUNCTION(VOID());
//	}
//	@Deprecated
//	public static FunctionType functType(Type returnType, Type...params) {
//		return FUNCTION(returnType, params);
//	}

	public static FunctionType FUNCTION(Type returnType, Type... params) {
		return FUNCTION(returnType, false, Arrays.asList(params));
	}
	public static FunctionType FUNCTION(Type returnType, boolean hasElipsis, Type... params) {
		return FUNCTION(returnType, hasElipsis, Arrays.asList(params));
	}
	public static FunctionType FUNCTION(Type returnType, boolean hasElipsis, List<Type> parameterTypes) {
		FunctionType functionType = factory.createFunctionType();
		functionType.setReturnType(returnType);
		functionType.setHasElipsis(hasElipsis);
		for(Type parameterType : parameterTypes) {
			functionType.addParameter(parameterType);
		}
		return addTypeToScope(functionType);
	}

	/* ****************************
	 * Struct & Unions
	 ****************************/

	public static EnumType ENUM(String name) {
		EnumType enumType = factory.createEnumType();
		enumType.setName(name);
		return addTypeToScope(enumType);
	}
	
	public static Enumerator ENUMERATOR(String name, Instruction instruction) {
		Enumerator enumerator = factory.createEnumerator();
		enumerator.setName(name);
		enumerator.setExpression(instruction);
		return enumerator;
	}
	
	/**
	 * Do not set the offset attribute
	 */
	public static Field FIELD(String name, Type type, long size) {
		Field field = factory.createField();
		field.setType(type);
		field.setName(name);
		field.setBitwidth(size);
		return field;
	}
	/**
	 * Do not set the offset attribute
	 */
	public static Field FIELD(String name, Type type) {
		return FIELD(name, type, -1);
	}
	
	private static RecordType RECORD(String name, Kinds k, Field... fields) {
		if (name == null || name.isEmpty())
			throw new IllegalArgumentException();
		RecordType recordType = factory.createRecordType();
		recordType.setKind(k);
		recordType.setDefined(true);
		recordType.setName(name);
		recordType.getFields().addAll(Arrays.asList(fields));
		return addTypeToScope(recordType);
	}
	/**
	 * Also updates offset for every field
	 * @param name
	 * @param fields
	 * @return
	 */
	public static RecordType STRUCT(String name, Field... fields) {
		int offset =0;
		for (Field field : fields) {
			field.setOffset(offset);
			offset+=field.getType().getSize();
		}
		return RECORD(name,Kinds.STRUCT, fields);
	}
	/**
	 * Also updates offset to 0 for every field
	 * @param name
	 * @param fields
	 * @return
	 */
	public static RecordType UNION(String name, Field... fields) {
		for (Field field : fields) {
			field.setOffset(0);
		}
		return RECORD(name,Kinds.UNION, fields);
	}

	/* ****************************
	 * Algorithmics C Datatypes
	 ****************************/
	public static ACChannelType ACCHANNEL() {
		ACChannelType acChannelType = factory.createACChannelType();
		return addTypeToScope(acChannelType);
	}
	
	public static ACComplexType ACCOMPLEX(Type baseType) {
		ACComplexType acComplexType = factory.createACComplexType();
		acComplexType.setBaseType(baseType);
		return addTypeToScope(acComplexType);
	}
	
	/**
	 * Do not add 1 bit for sign
	 */
	public static BaseType ACFIXED(int width, int integerWidth, boolean signed){
		return ACFIXED(width, integerWidth, signed, null, null);
	}
	
	/**
	 * Do not add 1 bit for sign
	 * 
	 * @param bitwidth
	 * @param fraction
	 * @param signed
	 * @param quantificationMode null = QuantificationMode.AC_TRN
	 * @param overflowMode null = OverflowMode.AC_SAT
	 * @return
	 */
	public static ACFixedType ACFIXED(int bitwidth, int integerWidth, boolean signed, QuantificationMode quantificationMode, OverflowMode overflowMode) {
		if (quantificationMode == null)
			quantificationMode = QuantificationMode.AC_TRN;
		if (overflowMode == null)
			overflowMode = OverflowMode.AC_SAT;
		
		ACFixedType acFixedType = factory.createACFixedType();
		acFixedType.setBase(BOOL());
		acFixedType.setBitwidth(bitwidth);
		acFixedType.setFraction(bitwidth-integerWidth);
		acFixedType.setInteger(integerWidth);
		acFixedType.setSigned(signed);
		acFixedType.setSize(bitwidth);
		acFixedType.setQuantificationMode(quantificationMode);
		acFixedType.setOverflowMode(overflowMode);
		return addTypeToScope(acFixedType);
	}
	
	/**
	 * 
	 * @param exponent
	 * @param mantissa
	 * @return float of size exponent + mantissa + 1 (add 1 bit for sign)
	 */
	public static ACFloatType ACFLOAT(int exponent, int mantissa) {
		return ACFLOAT(exponent, mantissa, true,2);
	}
	
	/**
	 * Always signed
	 */
	public static ACFloatType ACFLOAT(int exponent, int mantissa, boolean signed, int radix) {
		ACFloatType acFloatType = factory.createACFloatType();
		acFloatType.setSigned(signed);
		acFloatType.setExponent(exponent);
		acFloatType.setMantissa(mantissa);
		acFloatType.setRadix(radix);
		acFloatType.setSize(exponent+mantissa+(signed?1:0));
		return addTypeToScope(acFloatType);
	}

	/**
	 * 
	 * @param width
	 * @param signed
	 * @return ac_int of size width (do not add 1 bit for sign)
	 */
	public static ACIntType ACINT(int width,boolean signed) {
		return ACINT(width, signed, null);
	}
	
	/**
	 * 
	 * @param bitwidth
	 * @param signed
	 * @param overflowMode null = OverflowMode.AC_SAT
	 * @return ac_int of size bitwidth (do not add 1 bit for sign)
	 */
	public static ACIntType ACINT(int bitwidth, boolean signed, OverflowMode overflowMode) {
		if (overflowMode == null)
			overflowMode = OverflowMode.AC_SAT;
		ACIntType acIntType = factory.createACIntType();
		acIntType.setBase(BOOL());
		acIntType.setBitwidth(bitwidth);
		acIntType.setSigned(signed);
		acIntType.setOverflowMode(overflowMode);
		acIntType.setSize(bitwidth);
		return addTypeToScope(acIntType);
	}

	/* ****************************
	 * Specific types
	 ****************************/
	
	public static SimdType Vector(Type subwordType, long size) {
		SimdType vecType = factory.createSimdType();
		vecType.setSubwordType(subwordType);
		vecType.setSize(size);
		return addTypeToScope(vecType);
	}

	public static UndefinedType UNDEFINED(){
		return  addTypeToScope(factory.createUndefinedType());
	}
}