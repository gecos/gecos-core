/**
 */
package gecos.blocks;

import gecos.instrs.BreakInstruction;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Breakable</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.blocks.BlocksPackage#getBreakable()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface Breakable extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.instrs.BreakInstruction%&gt;, &lt;%gecos.blocks.BasicBlock%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.instrs.BreakInstruction%&gt;, &lt;%gecos.blocks.BasicBlock%&gt;&gt;()\n{\n\tpublic &lt;%gecos.blocks.BasicBlock%&gt; apply(final &lt;%gecos.instrs.BreakInstruction%&gt; it)\n\t{\n\t\treturn it.getBasicBlock();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt;asEList(((&lt;%gecos.blocks.BasicBlock%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt;filterNull(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%gecos.instrs.BreakInstruction%&gt;, &lt;%gecos.blocks.BasicBlock%&gt;&gt;map(this.listBreakInstructions(), _function)), &lt;%gecos.blocks.BasicBlock%&gt;.class))));'"
	 * @generated
	 */
	EList<BasicBlock> listBreakBlocks();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EList<BreakInstruction> listBreakInstructions();

} // Breakable
