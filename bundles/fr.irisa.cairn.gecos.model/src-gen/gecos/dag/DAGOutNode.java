/**
 */
package gecos.dag;

import gecos.core.Symbol;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Out Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.DAGOutNode#getSymbol <em>Symbol</em>}</li>
 * </ul>
 *
 * @see gecos.dag.DagPackage#getDAGOutNode()
 * @model
 * @generated
 */
public interface DAGOutNode extends DAGNode, DagVisitable {
	/**
	 * Returns the value of the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Symbol</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbol</em>' reference.
	 * @see #setSymbol(Symbol)
	 * @see gecos.dag.DagPackage#getDAGOutNode_Symbol()
	 * @model
	 * @generated
	 */
	Symbol getSymbol();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGOutNode#getSymbol <em>Symbol</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Symbol</em>' reference.
	 * @see #getSymbol()
	 * @generated
	 */
	void setSymbol(Symbol value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGOutNode(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.dag.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _ident = this.getIdent();\n&lt;%java.lang.String%&gt; _plus = (\"OutNode_\" + &lt;%java.lang.Integer%&gt;.valueOf(_ident));\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \":\");\n&lt;%gecos.core.Symbol%&gt; _symbol = this.getSymbol();\nreturn (_plus_1 + _symbol);'"
	 * @generated
	 */
	String toString();

} // DAGOutNode
