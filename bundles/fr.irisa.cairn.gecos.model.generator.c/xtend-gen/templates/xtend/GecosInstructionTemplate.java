package templates.xtend;

import com.google.common.base.Objects;
import fr.irisa.cairn.gecos.model.c.generator.CharBuilder;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableInstructionCGenerator;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableTypeCGenerator;
import fr.irisa.cairn.gecos.model.c.generator.OperatorPrecedenceTools;
import fr.irisa.cairn.gecos.model.extensions.generators.IGecosCodeGenerator;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import gecos.annotations.PragmaAnnotation;
import gecos.core.ProcedureSet;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.ArrayValueInstruction;
import gecos.instrs.BreakInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.CondInstruction;
import gecos.instrs.ConstantInstruction;
import gecos.instrs.ContinueInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.DummyInstruction;
import gecos.instrs.EnumeratorInstruction;
import gecos.instrs.FieldInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.GotoInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.InstrsPackage;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.LabelInstruction;
import gecos.instrs.MethodCallInstruction;
import gecos.instrs.RetInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SimpleArrayInstruction;
import gecos.instrs.SizeofTypeInstruction;
import gecos.instrs.SizeofValueInstruction;
import gecos.instrs.StringInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.IntegerType;
import gecos.types.IntegerTypes;
import gecos.types.Type;
import java.util.Arrays;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IntegerRange;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import templates.xtend.GecosTemplate;
import templates.xtend.GecosTypeTemplate;

@SuppressWarnings("all")
public class GecosInstructionTemplate extends GecosTemplate {
  protected Object _generate(final AddressInstruction addressInstruction) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("&");
    String _generate = ExtendableInstructionCGenerator.eInstance.generate(addressInstruction.getAddress());
    _builder.append(_generate);
    return _builder;
  }
  
  protected Object _generate(final ArrayInstruction arrayInstruction) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      StringConcatenation _builder = new StringConcatenation();
      String _generate = ExtendableInstructionCGenerator.eInstance.generate(arrayInstruction.getDest());
      _builder.append(_generate);
      buffer.append(_builder);
      EList<Instruction> _index = arrayInstruction.getIndex();
      for (final Instruction instruction : _index) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("[");
        String _generate_1 = ExtendableInstructionCGenerator.eInstance.generate(instruction);
        _builder_1.append(_generate_1);
        _builder_1.append("]");
        buffer.append(_builder_1);
      }
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append(buffer);
      _xblockexpression = _builder_2;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final ArrayValueInstruction arrayValueInstruction) {
    StringBuffer _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("{");
      buffer.append(_builder);
      EList<Instruction> _children = arrayValueInstruction.getChildren();
      for (final Instruction instruction : _children) {
        {
          StringConcatenation _builder_1 = new StringConcatenation();
          String _generate = ExtendableInstructionCGenerator.eInstance.generate(instruction);
          _builder_1.append(_generate);
          buffer.append(_builder_1);
          int _indexOf = arrayValueInstruction.getChildren().indexOf(instruction);
          int _size = arrayValueInstruction.getChildren().size();
          int _minus = (_size - 1);
          boolean _lessThan = (_indexOf < _minus);
          if (_lessThan) {
            buffer.append(", ");
          }
        }
      }
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("}");
      _xblockexpression = buffer.append(_builder_1);
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final BreakInstruction breakInstruction) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("break");
    return _builder;
  }
  
  protected Object _generate(final MethodCallInstruction methodInstruction) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      StringConcatenation _builder = new StringConcatenation();
      String _generate = ExtendableInstructionCGenerator.eInstance.generate(methodInstruction.getAddress());
      _builder.append(_generate);
      buffer.append(_builder);
      String _name = methodInstruction.getName();
      String _plus = ("." + _name);
      String _plus_1 = (_plus + "(");
      buffer.append(_plus_1);
      EList<Instruction> _args = methodInstruction.getArgs();
      for (final Instruction instruction : _args) {
        {
          StringConcatenation _builder_1 = new StringConcatenation();
          String _generate_1 = ExtendableInstructionCGenerator.eInstance.generate(instruction);
          _builder_1.append(_generate_1);
          buffer.append(_builder_1);
          int _indexOf = methodInstruction.getArgs().indexOf(instruction);
          int _size = methodInstruction.getArgs().size();
          int _minus = (_size - 1);
          boolean _lessThan = (_indexOf < _minus);
          if (_lessThan) {
            buffer.append(", ");
          }
        }
      }
      buffer.append(")");
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append(buffer);
      _xblockexpression = _builder_1;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final CallInstruction callInstruction) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      StringConcatenation _builder = new StringConcatenation();
      String _generate = ExtendableInstructionCGenerator.eInstance.generate(callInstruction.getAddress());
      _builder.append(_generate);
      buffer.append(_builder);
      buffer.append("(");
      final Function1<Instruction, CharSequence> _function = (Instruction it) -> {
        return ExtendableInstructionCGenerator.eInstance.generate(it);
      };
      buffer.append(IterableExtensions.<Instruction>join(callInstruction.getArgs(), ", ", _function));
      buffer.append(")");
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append(buffer);
      _xblockexpression = _builder_1;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final CondInstruction condInstruction) {
    StringConcatenation _builder = new StringConcatenation();
    String _generate = ExtendableInstructionCGenerator.eInstance.generate(condInstruction.getCond());
    _builder.append(_generate);
    return _builder;
  }
  
  protected Object _generate(final ContinueInstruction continueInstruction) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("continue");
    return _builder;
  }
  
  protected Object _generate(final ConvertInstruction convertInstruction) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      final boolean needParenthesis = OperatorPrecedenceTools.needParenthesis(convertInstruction);
      Type type = convertInstruction.getType();
      if (needParenthesis) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("(");
        buffer.append(_builder);
      }
      IGecosCodeGenerator.afterSymbolInformation.setLength(0);
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("(");
      String _generate = ExtendableTypeCGenerator.eInstance.generate(type);
      _builder_1.append(_generate);
      _builder_1.append(IGecosCodeGenerator.afterSymbolInformation);
      _builder_1.append(")");
      buffer.append(_builder_1);
      IGecosCodeGenerator.afterSymbolInformation.setLength(0);
      StringConcatenation _builder_2 = new StringConcatenation();
      String _generate_1 = ExtendableInstructionCGenerator.eInstance.generate(convertInstruction.getExpr());
      _builder_2.append(_generate_1);
      buffer.append(_builder_2);
      if (needParenthesis) {
        StringConcatenation _builder_3 = new StringConcatenation();
        _builder_3.append(")");
        buffer.append(_builder_3);
      }
      StringConcatenation _builder_4 = new StringConcatenation();
      String _string = buffer.toString();
      _builder_4.append(_string);
      _xblockexpression = _builder_4;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final DummyInstruction dummyInstruction) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  protected Object _generate(final EnumeratorInstruction enumeratorInstruction) {
    CharSequence _xblockexpression = null;
    {
      final GecosTypeTemplate gecosTypeTemplate = new GecosTypeTemplate();
      StringConcatenation _builder = new StringConcatenation();
      Object _generate = gecosTypeTemplate.generate(enumeratorInstruction.getEnumerator());
      _builder.append(_generate);
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final FieldInstruction fieldInstruction) {
    CharSequence _xblockexpression = null;
    {
      final GecosTypeTemplate gecosTypeTemplate = new GecosTypeTemplate();
      final StringBuffer buffer = new StringBuffer();
      int _childrenCount = fieldInstruction.getChildrenCount();
      boolean _equals = (_childrenCount == 1);
      if (_equals) {
        Instruction _expr = fieldInstruction.getExpr();
        if ((_expr instanceof IndirInstruction)) {
          StringConcatenation _builder = new StringConcatenation();
          Instruction _expr_1 = fieldInstruction.getExpr();
          String _generate = ExtendableInstructionCGenerator.eInstance.generate(((IndirInstruction) _expr_1).getAddress());
          _builder.append(_generate);
          buffer.append(_builder);
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append("->");
          Object _generate_1 = gecosTypeTemplate.generate(fieldInstruction.getField());
          _builder_1.append(_generate_1);
          buffer.append(_builder_1);
        } else {
          StringConcatenation _builder_2 = new StringConcatenation();
          String _generate_2 = ExtendableInstructionCGenerator.eInstance.generate(fieldInstruction.getExpr());
          _builder_2.append(_generate_2);
          buffer.append(_builder_2);
          StringConcatenation _builder_3 = new StringConcatenation();
          _builder_3.append(".");
          Object _generate_3 = gecosTypeTemplate.generate(fieldInstruction.getField());
          _builder_3.append(_generate_3);
          buffer.append(_builder_3);
        }
      } else {
        StringConcatenation _builder_4 = new StringConcatenation();
        _builder_4.append("/* Not manageable !!!*/");
        buffer.append(_builder_4);
      }
      StringConcatenation _builder_5 = new StringConcatenation();
      String _string = buffer.toString();
      _builder_5.append(_string);
      _xblockexpression = _builder_5;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final FloatInstruction floatInstruction) {
    String res = "";
    boolean _isInfinite = Double.valueOf(floatInstruction.getValue()).isInfinite();
    if (_isInfinite) {
      double _value = floatInstruction.getValue();
      boolean _greaterThan = (_value > 0);
      if (_greaterThan) {
        res = "(1.0/0.0) /*+INFINITE*/";
      } else {
        res = "-(1.0/0.0) /*-INFINITE*/";
      }
    } else {
      boolean _isNaN = Double.valueOf(floatInstruction.getValue()).isNaN();
      if (_isNaN) {
        res = "(0.0/0.0) /*NAN*/";
      } else {
        res = Double.valueOf(floatInstruction.getValue()).toString();
      }
    }
    StringConcatenation _builder = new StringConcatenation();
    _builder.append(res);
    return _builder;
  }
  
  protected Object _generate(final GenericInstruction genericInstruction) {
    CharSequence _xblockexpression = null;
    {
      EPackage _ePackage = genericInstruction.eClass().getEPackage();
      boolean _equals = Objects.equal(_ePackage, InstrsPackage.eINSTANCE);
      boolean _not = (!_equals);
      if (_not) {
        return null;
      }
      final StringBuffer buffer = new StringBuffer();
      final boolean needParenthesis = OperatorPrecedenceTools.needParenthesis(genericInstruction);
      if (needParenthesis) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("(");
        buffer.append(_builder);
      }
      String _name = genericInstruction.getName();
      if (_name != null) {
        switch (_name) {
          case "add":
            StringConcatenation _builder_1 = new StringConcatenation();
            CharSequence _generateInstructionWithMultipleSubInstruction = this.generateInstructionWithMultipleSubInstruction(genericInstruction.getOperands(), "+");
            _builder_1.append(_generateInstructionWithMultipleSubInstruction);
            buffer.append(_builder_1);
            break;
          case "sub":
            StringConcatenation _builder_2 = new StringConcatenation();
            CharSequence _generateInstructionWithMultipleSubInstruction_1 = this.generateInstructionWithMultipleSubInstruction(genericInstruction.getOperands(), "-");
            _builder_2.append(_generateInstructionWithMultipleSubInstruction_1);
            buffer.append(_builder_2);
            break;
          case "mul":
            StringConcatenation _builder_3 = new StringConcatenation();
            CharSequence _generateInstructionWithMultipleSubInstruction_2 = this.generateInstructionWithMultipleSubInstruction(genericInstruction.getOperands(), "*");
            _builder_3.append(_generateInstructionWithMultipleSubInstruction_2);
            buffer.append(_builder_3);
            break;
          case "div":
            StringConcatenation _builder_4 = new StringConcatenation();
            CharSequence _generateInstructionWithMultipleSubInstruction_3 = this.generateInstructionWithMultipleSubInstruction(genericInstruction.getOperands(), "/");
            _builder_4.append(_generateInstructionWithMultipleSubInstruction_3);
            buffer.append(_builder_4);
            break;
          case "shl":
            StringConcatenation _builder_5 = new StringConcatenation();
            CharSequence _generateInstructionWithMultipleSubInstruction_4 = this.generateInstructionWithMultipleSubInstruction(genericInstruction.getOperands(), "<<");
            _builder_5.append(_generateInstructionWithMultipleSubInstruction_4);
            buffer.append(_builder_5);
            break;
          case "shr":
            StringConcatenation _builder_6 = new StringConcatenation();
            CharSequence _generateInstructionWithMultipleSubInstruction_5 = this.generateInstructionWithMultipleSubInstruction(genericInstruction.getOperands(), ">>");
            _builder_6.append(_generateInstructionWithMultipleSubInstruction_5);
            buffer.append(_builder_6);
            break;
          case "neg":
            StringConcatenation _builder_7 = new StringConcatenation();
            _builder_7.append("-");
            String _generate = ExtendableInstructionCGenerator.eInstance.generate(genericInstruction.getOperands().get(0));
            _builder_7.append(_generate);
            buffer.append(_builder_7);
            break;
          case "not":
            StringConcatenation _builder_8 = new StringConcatenation();
            _builder_8.append("~");
            String _generate_1 = ExtendableInstructionCGenerator.eInstance.generate(genericInstruction.getOperands().get(0));
            _builder_8.append(_generate_1);
            buffer.append(_builder_8);
            break;
          case "lnot":
            StringConcatenation _builder_9 = new StringConcatenation();
            _builder_9.append("!");
            String _generate_2 = ExtendableInstructionCGenerator.eInstance.generate(genericInstruction.getOperands().get(0));
            _builder_9.append(_generate_2);
            buffer.append(_builder_9);
            break;
          case "mod":
            StringConcatenation _builder_10 = new StringConcatenation();
            CharSequence _generateInstructionWithMultipleSubInstruction_6 = this.generateInstructionWithMultipleSubInstruction(genericInstruction.getOperands(), "%");
            _builder_10.append(_generateInstructionWithMultipleSubInstruction_6);
            buffer.append(_builder_10);
            break;
          case "eq":
            StringConcatenation _builder_11 = new StringConcatenation();
            CharSequence _generateInstructionWithMultipleSubInstruction_7 = this.generateInstructionWithMultipleSubInstruction(genericInstruction.getOperands(), "==");
            _builder_11.append(_generateInstructionWithMultipleSubInstruction_7);
            buffer.append(_builder_11);
            break;
          case "ne":
            StringConcatenation _builder_12 = new StringConcatenation();
            CharSequence _generateInstructionWithMultipleSubInstruction_8 = this.generateInstructionWithMultipleSubInstruction(genericInstruction.getOperands(), "!=");
            _builder_12.append(_generateInstructionWithMultipleSubInstruction_8);
            buffer.append(_builder_12);
            break;
          case "neq":
            StringConcatenation _builder_13 = new StringConcatenation();
            CharSequence _generateInstructionWithMultipleSubInstruction_9 = this.generateInstructionWithMultipleSubInstruction(genericInstruction.getOperands(), "!=");
            _builder_13.append(_generateInstructionWithMultipleSubInstruction_9);
            buffer.append(_builder_13);
            break;
          case "lt":
            StringConcatenation _builder_14 = new StringConcatenation();
            CharSequence _generateInstructionWithMultipleSubInstruction_10 = this.generateInstructionWithMultipleSubInstruction(genericInstruction.getOperands(), "<");
            _builder_14.append(_generateInstructionWithMultipleSubInstruction_10);
            buffer.append(_builder_14);
            break;
          case "le":
            StringConcatenation _builder_15 = new StringConcatenation();
            CharSequence _generateInstructionWithMultipleSubInstruction_11 = this.generateInstructionWithMultipleSubInstruction(genericInstruction.getOperands(), "<=");
            _builder_15.append(_generateInstructionWithMultipleSubInstruction_11);
            buffer.append(_builder_15);
            break;
          case "gt":
            StringConcatenation _builder_16 = new StringConcatenation();
            CharSequence _generateInstructionWithMultipleSubInstruction_12 = this.generateInstructionWithMultipleSubInstruction(genericInstruction.getOperands(), ">");
            _builder_16.append(_generateInstructionWithMultipleSubInstruction_12);
            buffer.append(_builder_16);
            break;
          case "ge":
            StringConcatenation _builder_17 = new StringConcatenation();
            CharSequence _generateInstructionWithMultipleSubInstruction_13 = this.generateInstructionWithMultipleSubInstruction(genericInstruction.getOperands(), ">=");
            _builder_17.append(_generateInstructionWithMultipleSubInstruction_13);
            buffer.append(_builder_17);
            break;
          case "land":
            EList<Instruction> _operands = genericInstruction.getOperands();
            for (final Instruction instruction : _operands) {
              {
                StringConcatenation _builder_18 = new StringConcatenation();
                String _generate_3 = ExtendableInstructionCGenerator.eInstance.generate(instruction);
                _builder_18.append(_generate_3);
                buffer.append(_builder_18);
                EList<Instruction> _operands_1 = genericInstruction.getOperands();
                int _size = genericInstruction.getOperands().size();
                int _minus = (_size - 1);
                Instruction _get = _operands_1.get(_minus);
                boolean _tripleNotEquals = (instruction != _get);
                if (_tripleNotEquals) {
                  buffer.append(" && ");
                }
              }
            }
            break;
          case "and":
            EList<Instruction> _operands_1 = genericInstruction.getOperands();
            for (final Instruction instruction_1 : _operands_1) {
              {
                StringConcatenation _builder_18 = new StringConcatenation();
                String _generate_3 = ExtendableInstructionCGenerator.eInstance.generate(instruction_1);
                _builder_18.append(_generate_3);
                buffer.append(_builder_18);
                EList<Instruction> _operands_2 = genericInstruction.getOperands();
                int _size = genericInstruction.getOperands().size();
                int _minus = (_size - 1);
                Instruction _get = _operands_2.get(_minus);
                boolean _tripleNotEquals = (instruction_1 != _get);
                if (_tripleNotEquals) {
                  buffer.append(" & ");
                }
              }
            }
            break;
          case "lor":
            EList<Instruction> _operands_2 = genericInstruction.getOperands();
            for (final Instruction instruction_2 : _operands_2) {
              {
                StringConcatenation _builder_18 = new StringConcatenation();
                String _generate_3 = ExtendableInstructionCGenerator.eInstance.generate(instruction_2);
                _builder_18.append(_generate_3);
                buffer.append(_builder_18);
                EList<Instruction> _operands_3 = genericInstruction.getOperands();
                int _size = genericInstruction.getOperands().size();
                int _minus = (_size - 1);
                Instruction _get = _operands_3.get(_minus);
                boolean _tripleNotEquals = (instruction_2 != _get);
                if (_tripleNotEquals) {
                  buffer.append(" || ");
                }
              }
            }
            break;
          case "or":
            EList<Instruction> _operands_3 = genericInstruction.getOperands();
            for (final Instruction instruction_3 : _operands_3) {
              {
                StringConcatenation _builder_18 = new StringConcatenation();
                String _generate_3 = ExtendableInstructionCGenerator.eInstance.generate(instruction_3);
                _builder_18.append(_generate_3);
                buffer.append(_builder_18);
                EList<Instruction> _operands_4 = genericInstruction.getOperands();
                int _size = genericInstruction.getOperands().size();
                int _minus = (_size - 1);
                Instruction _get = _operands_4.get(_minus);
                boolean _tripleNotEquals = (instruction_3 != _get);
                if (_tripleNotEquals) {
                  buffer.append(" | ");
                }
              }
            }
            break;
          case "xor":
            EList<Instruction> _operands_4 = genericInstruction.getOperands();
            for (final Instruction instruction_4 : _operands_4) {
              {
                StringConcatenation _builder_18 = new StringConcatenation();
                String _generate_3 = ExtendableInstructionCGenerator.eInstance.generate(instruction_4);
                _builder_18.append(_generate_3);
                buffer.append(_builder_18);
                EList<Instruction> _operands_5 = genericInstruction.getOperands();
                int _size = genericInstruction.getOperands().size();
                int _minus = (_size - 1);
                Instruction _get = _operands_5.get(_minus);
                boolean _tripleNotEquals = (instruction_4 != _get);
                if (_tripleNotEquals) {
                  buffer.append(" ^ ");
                }
              }
            }
            break;
          case "mux":
            int _size = genericInstruction.getOperands().size();
            boolean _equals_1 = (_size == 3);
            if (_equals_1) {
              StringConcatenation _builder_18 = new StringConcatenation();
              String _generate_3 = ExtendableInstructionCGenerator.eInstance.generate(genericInstruction.getOperands().get(0));
              _builder_18.append(_generate_3);
              buffer.append(_builder_18);
              buffer.append(" ? ");
              StringConcatenation _builder_19 = new StringConcatenation();
              String _generate_4 = ExtendableInstructionCGenerator.eInstance.generate(genericInstruction.getOperands().get(1));
              _builder_19.append(_generate_4);
              buffer.append(_builder_19);
              buffer.append(" : ");
              StringConcatenation _builder_20 = new StringConcatenation();
              String _generate_5 = ExtendableInstructionCGenerator.eInstance.generate(genericInstruction.getOperands().get(2));
              _builder_20.append(_generate_5);
              buffer.append(_builder_20);
            }
            break;
          case "sequence":
            EList<Instruction> _operands_5 = genericInstruction.getOperands();
            for (final Instruction instruction_5 : _operands_5) {
              {
                StringConcatenation _builder_21 = new StringConcatenation();
                String _generate_6 = ExtendableInstructionCGenerator.eInstance.generate(instruction_5);
                _builder_21.append(_generate_6);
                buffer.append(_builder_21);
                EList<Instruction> _operands_6 = genericInstruction.getOperands();
                int _size_1 = genericInstruction.getOperands().size();
                int _minus = (_size_1 - 1);
                Instruction _get = _operands_6.get(_minus);
                boolean _tripleNotEquals = (instruction_5 != _get);
                if (_tripleNotEquals) {
                  buffer.append(", ");
                }
              }
            }
            break;
          case "min":
            final ProcedureSet cps = genericInstruction.getContainingProcedureSet();
            cps.setAnnotation("need_define_min", null);
            int _size_1 = genericInstruction.getOperands().size();
            int _minus = (_size_1 - 2);
            IntegerRange _upTo = new IntegerRange(0, _minus);
            for (final Integer i : _upTo) {
              buffer.append("_min_(");
            }
            boolean tmp = false;
            EList<Instruction> _operands_6 = genericInstruction.getOperands();
            for (final Instruction instruction_6 : _operands_6) {
              {
                StringConcatenation _builder_21 = new StringConcatenation();
                String _generate_6 = ExtendableInstructionCGenerator.eInstance.generate(instruction_6);
                _builder_21.append(_generate_6);
                buffer.append(_builder_21);
                if ((!tmp)) {
                  tmp = true;
                } else {
                  buffer.append(")");
                }
                EList<Instruction> _operands_7 = genericInstruction.getOperands();
                int _size_2 = genericInstruction.getOperands().size();
                int _minus_1 = (_size_2 - 1);
                Instruction _get = _operands_7.get(_minus_1);
                boolean _tripleNotEquals = (instruction_6 != _get);
                if (_tripleNotEquals) {
                  buffer.append(", ");
                }
              }
            }
            break;
          case "max":
            final ProcedureSet cps_1 = genericInstruction.getContainingProcedureSet();
            cps_1.setAnnotation("need_define_max", null);
            int _size_2 = genericInstruction.getOperands().size();
            int _minus_1 = (_size_2 - 2);
            IntegerRange _upTo_1 = new IntegerRange(0, _minus_1);
            for (final Integer i_1 : _upTo_1) {
              buffer.append("_max_(");
            }
            boolean tmp_1 = false;
            EList<Instruction> _operands_7 = genericInstruction.getOperands();
            for (final Instruction instruction_7 : _operands_7) {
              {
                StringConcatenation _builder_21 = new StringConcatenation();
                String _generate_6 = ExtendableInstructionCGenerator.eInstance.generate(instruction_7);
                _builder_21.append(_generate_6);
                buffer.append(_builder_21);
                if ((!tmp_1)) {
                  tmp_1 = true;
                } else {
                  buffer.append(")");
                }
                EList<Instruction> _operands_8 = genericInstruction.getOperands();
                int _size_3 = genericInstruction.getOperands().size();
                int _minus_2 = (_size_3 - 1);
                Instruction _get = _operands_8.get(_minus_2);
                boolean _tripleNotEquals = (instruction_7 != _get);
                if (_tripleNotEquals) {
                  buffer.append(", ");
                }
              }
            }
            break;
          case "floor":
            final ProcedureSet cps_2 = genericInstruction.getContainingProcedureSet();
            cps_2.setAnnotation("need_define_floor", null);
            StringConcatenation _builder_21 = new StringConcatenation();
            _builder_21.append("_floor_(");
            String _generate_6 = ExtendableInstructionCGenerator.eInstance.generate(genericInstruction.getOperands().get(0));
            _builder_21.append(_generate_6);
            _builder_21.append(", ");
            String _generate_7 = ExtendableInstructionCGenerator.eInstance.generate(genericInstruction.getOperands().get(1));
            _builder_21.append(_generate_7);
            _builder_21.append(")");
            buffer.append(_builder_21);
            break;
          case "ceil":
            final ProcedureSet cps_3 = genericInstruction.getContainingProcedureSet();
            cps_3.setAnnotation("need_define_ceil", null);
            StringConcatenation _builder_22 = new StringConcatenation();
            _builder_22.append("_ceil_(");
            String _generate_8 = ExtendableInstructionCGenerator.eInstance.generate(genericInstruction.getOperands().get(0));
            _builder_22.append(_generate_8);
            _builder_22.append(", ");
            String _generate_9 = ExtendableInstructionCGenerator.eInstance.generate(genericInstruction.getOperands().get(1));
            _builder_22.append(_generate_9);
            _builder_22.append(")");
            buffer.append(_builder_22);
            break;
          case "mmod":
            final ProcedureSet cps_4 = genericInstruction.getContainingProcedureSet();
            cps_4.setAnnotation("need_define_mod", null);
            StringConcatenation _builder_23 = new StringConcatenation();
            _builder_23.append("_mmod_(");
            String _generate_10 = ExtendableInstructionCGenerator.eInstance.generate(genericInstruction.getOperands().get(0));
            _builder_23.append(_generate_10);
            _builder_23.append(", ");
            String _generate_11 = ExtendableInstructionCGenerator.eInstance.generate(genericInstruction.getOperands().get(1));
            _builder_23.append(_generate_11);
            _builder_23.append(")");
            buffer.append(_builder_23);
            break;
          default:
            StringConcatenation _builder_24 = new StringConcatenation();
            _builder_24.append("/* GenericInstruction : \"");
            _builder_24.append(genericInstruction);
            _builder_24.append("\" not supported */");
            buffer.append(_builder_24);
            break;
        }
      } else {
        StringConcatenation _builder_24 = new StringConcatenation();
        _builder_24.append("/* GenericInstruction : \"");
        _builder_24.append(genericInstruction);
        _builder_24.append("\" not supported */");
        buffer.append(_builder_24);
      }
      if (needParenthesis) {
        StringConcatenation _builder_25 = new StringConcatenation();
        _builder_25.append(")");
        buffer.append(_builder_25);
      }
      StringConcatenation _builder_26 = new StringConcatenation();
      String _string = buffer.toString();
      _builder_26.append(_string);
      _xblockexpression = _builder_26;
    }
    return _xblockexpression;
  }
  
  public CharSequence generateInstructionWithMultipleSubInstruction(final List<Instruction> subInstructions, final String operator) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      boolean first = true;
      for (final Instruction instruction : subInstructions) {
        {
          if (first) {
            first = false;
          } else {
            if ((operator.equals("*") && ((instruction instanceof ConstantInstruction) || (instruction instanceof SymbolInstruction)))) {
              StringConcatenation _builder = new StringConcatenation();
              _builder.append(operator);
              buffer.append(_builder);
            } else {
              StringConcatenation _builder_1 = new StringConcatenation();
              _builder_1.append(" ");
              _builder_1.append(operator, " ");
              _builder_1.append(" ");
              buffer.append(_builder_1);
            }
          }
          StringConcatenation _builder_2 = new StringConcatenation();
          String _generate = ExtendableInstructionCGenerator.eInstance.generate(instruction);
          _builder_2.append(_generate);
          buffer.append(_builder_2);
        }
      }
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(buffer);
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final GotoInstruction gotoInstruction) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("goto ");
    String _labelName = gotoInstruction.getLabelName();
    _builder.append(_labelName);
    return _builder;
  }
  
  protected Object _generate(final IndirInstruction indirInstruction) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      final boolean needParenthesis = OperatorPrecedenceTools.needParenthesis(indirInstruction);
      if (needParenthesis) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("(");
        buffer.append(_builder);
      }
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("*");
      String _generate = ExtendableInstructionCGenerator.eInstance.generate(indirInstruction.getAddress());
      _builder_1.append(_generate);
      buffer.append(_builder_1);
      if (needParenthesis) {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append(")");
        buffer.append(_builder_2);
      }
      StringConcatenation _builder_3 = new StringConcatenation();
      _builder_3.append(buffer);
      _xblockexpression = _builder_3;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final IntInstruction intInstruction) {
    CharSequence _xblockexpression = null;
    {
      PragmaAnnotation pragmaAnnotation = intInstruction.getPragma();
      String unsigned = "";
      String longlong = "";
      Type _type = intInstruction.getType();
      if ((_type instanceof IntegerType)) {
        Type _type_1 = intInstruction.getType();
        final IntegerType t = ((IntegerType) _type_1);
        boolean _signed = t.getSigned();
        boolean _not = (!_signed);
        if (_not) {
          unsigned = "u";
        }
        boolean _equals = t.getType().equals(IntegerTypes.LONG);
        if (_equals) {
          longlong = "l";
        } else {
          boolean _equals_1 = t.getType().equals(IntegerTypes.LONG_LONG);
          if (_equals_1) {
            longlong = "ll";
          }
        }
      }
      if ((pragmaAnnotation != null)) {
        EList<String> _content = pragmaAnnotation.getContent();
        for (final String directive : _content) {
          boolean _contains = directive.contains(GecosUserAnnotationFactory.PRAGMA_CHAR_LITTERAL);
          if (_contains) {
            String _buildCharLitteralFromIntInstruction = CharBuilder.buildCharLitteralFromIntInstruction(intInstruction.getValue());
            String _plus = ("\'" + _buildCharLitteralFromIntInstruction);
            return (_plus + "\'");
          } else {
            boolean _contains_1 = directive.contains(GecosUserAnnotationFactory.PRAGMA_HEX_LITTERAL);
            if (_contains_1) {
              String _hexString = Long.toHexString(intInstruction.getValue());
              String _plus_1 = ("0x" + _hexString);
              String _plus_2 = (_plus_1 + unsigned);
              return (_plus_2 + longlong);
            } else {
              boolean _contains_2 = directive.contains(GecosUserAnnotationFactory.PRAGMA_OCT_LITTERAL);
              if (_contains_2) {
                String _octalString = Long.toOctalString(intInstruction.getValue());
                String _plus_3 = ("0" + _octalString);
                String _plus_4 = (_plus_3 + unsigned);
                return (_plus_4 + longlong);
              } else {
                boolean _contains_3 = directive.contains(GecosUserAnnotationFactory.PRAGMA_BIN_LITTERAL);
                if (_contains_3) {
                  String _binaryString = Long.toBinaryString(intInstruction.getValue());
                  String _plus_5 = ("0b" + _binaryString);
                  String _plus_6 = (_plus_5 + unsigned);
                  return (_plus_6 + longlong);
                } else {
                }
              }
            }
          }
        }
      }
      StringConcatenation _builder = new StringConcatenation();
      String _string = Long.toString(intInstruction.getValue());
      String _plus_7 = (_string + unsigned);
      String _plus_8 = (_plus_7 + longlong);
      _builder.append(_plus_8);
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final LabelInstruction labelInstruction) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = labelInstruction.getName();
    _builder.append(_name);
    _builder.append(":");
    return _builder;
  }
  
  protected Object _generate(final RetInstruction retInstruction) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("return ");
    String _generate = ExtendableInstructionCGenerator.eInstance.generate(retInstruction.getExpr());
    _builder.append(_generate);
    return _builder;
  }
  
  protected Object _generate(final SetInstruction setInstruction) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      final boolean needParenthesis = OperatorPrecedenceTools.needParenthesis(setInstruction);
      if (needParenthesis) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("(");
        buffer.append(_builder);
      }
      StringConcatenation _builder_1 = new StringConcatenation();
      String _generate = ExtendableInstructionCGenerator.eInstance.generate(setInstruction.getDest());
      _builder_1.append(_generate);
      _builder_1.append(" = ");
      String _generate_1 = ExtendableInstructionCGenerator.eInstance.generate(setInstruction.getSource());
      _builder_1.append(_generate_1);
      buffer.append(_builder_1);
      if (needParenthesis) {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append(")");
        buffer.append(_builder_2);
      }
      StringConcatenation _builder_3 = new StringConcatenation();
      _builder_3.append(buffer);
      _xblockexpression = _builder_3;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final SimpleArrayInstruction simpleArrayInstruction) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      StringConcatenation _builder = new StringConcatenation();
      String _generate = ExtendableInstructionCGenerator.eInstance.generate(simpleArrayInstruction.getDest());
      _builder.append(_generate);
      buffer.append(_builder);
      EList<Instruction> _index = simpleArrayInstruction.getIndex();
      for (final Instruction instruction : _index) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("[");
        String _generate_1 = ExtendableInstructionCGenerator.eInstance.generate(instruction);
        _builder_1.append(_generate_1);
        _builder_1.append("]");
        buffer.append(_builder_1);
      }
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append(buffer);
      _xblockexpression = _builder_2;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final SizeofTypeInstruction sizeofTypeInstruction) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      final ExtendableTypeCGenerator gecosTypeTemplate = new ExtendableTypeCGenerator();
      IGecosCodeGenerator.afterSymbolInformation.setLength(0);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("sizeof (");
      String _generate = gecosTypeTemplate.generate(sizeofTypeInstruction.getTargetType());
      _builder.append(_generate);
      String _string = IGecosCodeGenerator.afterSymbolInformation.toString();
      _builder.append(_string);
      _builder.append(")");
      buffer.append(_builder);
      IGecosCodeGenerator.afterSymbolInformation.setLength(0);
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append(buffer);
      _xblockexpression = _builder_1;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final SizeofValueInstruction sizeofValueInstruction) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("sizeof ");
    String _generate = ExtendableInstructionCGenerator.eInstance.generate(sizeofValueInstruction.getExpr());
    _builder.append(_generate);
    return _builder;
  }
  
  protected Object _generate(final StringInstruction stringInstruction) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\"");
    String _value = stringInstruction.getValue();
    _builder.append(_value);
    _builder.append("\"");
    return _builder;
  }
  
  protected Object _generate(final SymbolInstruction symbolInstruction) {
    StringConcatenation _builder = new StringConcatenation();
    String _symbolName = symbolInstruction.getSymbolName();
    _builder.append(_symbolName);
    return _builder;
  }
  
  protected Object _generate(final EObject object) {
    return null;
  }
  
  public Object generate(final EObject addressInstruction) {
    if (addressInstruction instanceof AddressInstruction) {
      return _generate((AddressInstruction)addressInstruction);
    } else if (addressInstruction instanceof BreakInstruction) {
      return _generate((BreakInstruction)addressInstruction);
    } else if (addressInstruction instanceof CallInstruction) {
      return _generate((CallInstruction)addressInstruction);
    } else if (addressInstruction instanceof CondInstruction) {
      return _generate((CondInstruction)addressInstruction);
    } else if (addressInstruction instanceof ContinueInstruction) {
      return _generate((ContinueInstruction)addressInstruction);
    } else if (addressInstruction instanceof ConvertInstruction) {
      return _generate((ConvertInstruction)addressInstruction);
    } else if (addressInstruction instanceof FieldInstruction) {
      return _generate((FieldInstruction)addressInstruction);
    } else if (addressInstruction instanceof GotoInstruction) {
      return _generate((GotoInstruction)addressInstruction);
    } else if (addressInstruction instanceof IndirInstruction) {
      return _generate((IndirInstruction)addressInstruction);
    } else if (addressInstruction instanceof MethodCallInstruction) {
      return _generate((MethodCallInstruction)addressInstruction);
    } else if (addressInstruction instanceof RetInstruction) {
      return _generate((RetInstruction)addressInstruction);
    } else if (addressInstruction instanceof SimpleArrayInstruction) {
      return _generate((SimpleArrayInstruction)addressInstruction);
    } else if (addressInstruction instanceof SizeofValueInstruction) {
      return _generate((SizeofValueInstruction)addressInstruction);
    } else if (addressInstruction instanceof ArrayInstruction) {
      return _generate((ArrayInstruction)addressInstruction);
    } else if (addressInstruction instanceof ArrayValueInstruction) {
      return _generate((ArrayValueInstruction)addressInstruction);
    } else if (addressInstruction instanceof GenericInstruction) {
      return _generate((GenericInstruction)addressInstruction);
    } else if (addressInstruction instanceof SetInstruction) {
      return _generate((SetInstruction)addressInstruction);
    } else if (addressInstruction instanceof FloatInstruction) {
      return _generate((FloatInstruction)addressInstruction);
    } else if (addressInstruction instanceof IntInstruction) {
      return _generate((IntInstruction)addressInstruction);
    } else if (addressInstruction instanceof SizeofTypeInstruction) {
      return _generate((SizeofTypeInstruction)addressInstruction);
    } else if (addressInstruction instanceof StringInstruction) {
      return _generate((StringInstruction)addressInstruction);
    } else if (addressInstruction instanceof DummyInstruction) {
      return _generate((DummyInstruction)addressInstruction);
    } else if (addressInstruction instanceof EnumeratorInstruction) {
      return _generate((EnumeratorInstruction)addressInstruction);
    } else if (addressInstruction instanceof LabelInstruction) {
      return _generate((LabelInstruction)addressInstruction);
    } else if (addressInstruction instanceof SymbolInstruction) {
      return _generate((SymbolInstruction)addressInstruction);
    } else if (addressInstruction != null) {
      return _generate(addressInstruction);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(addressInstruction).toString());
    }
  }
}
