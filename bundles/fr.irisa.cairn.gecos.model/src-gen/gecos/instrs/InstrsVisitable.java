/**
 */
package gecos.instrs;

import gecos.core.GecosNode;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visitable</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.instrs.InstrsPackage#getInstrsVisitable()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface InstrsVisitable extends GecosNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

} // InstrsVisitable
