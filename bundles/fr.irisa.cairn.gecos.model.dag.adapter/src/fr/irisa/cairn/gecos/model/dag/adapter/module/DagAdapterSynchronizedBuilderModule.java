package fr.irisa.cairn.gecos.model.dag.adapter.module;

import fr.irisa.cairn.gecos.model.dag.adapter.builder.AdapterBuilder;
import fr.irisa.cairn.gecos.model.dag.adapter.builder.DagAdapterSynchronizedBuilder;
import fr.irisa.cairn.gecos.model.dag.adapter.observers.DagAdapterSynchronizationObserver;
import fr.irisa.cairn.gecos.model.dag.adapter.observers.IDagSynchronizationObserver;
import fr.irisa.cairn.graph.IGraph;
import gecos.dag.DAGInstruction;

/**
 * A Builder module keeping an adapted {@link DAGInstruction} consistent with the
 * {@link IGraph} adapter.
 * 
 * @author Antoine Floc'h - Initial contribution and API
 * 
 */
public class DagAdapterSynchronizedBuilderModule extends AdapterBuilderModule {
	
	@Override
	protected void bindBuilder() {
		this.bind(AdapterBuilder.class).to(DagAdapterSynchronizedBuilder.class);
	}
	
	protected void bindIDagSynchronisationObserver(){
		this.bind(IDagSynchronizationObserver.class).to(DagAdapterSynchronizationObserver.class);
	}
	
	@Override
	protected void configure() {
		bindIDagSynchronisationObserver();
		super.configure();
	}

}
