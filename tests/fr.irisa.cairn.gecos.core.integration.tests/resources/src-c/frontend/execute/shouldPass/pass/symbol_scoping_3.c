char a ;

int main() {
	{
		int a;
		{
			long long a;

			if (sizeof(a) != sizeof(long long))
				abort ();
		}

		if (sizeof(a) != sizeof(int))
			abort ();
	}
	if (sizeof(a) != sizeof(char))
		abort ();

	exit (0);
}
