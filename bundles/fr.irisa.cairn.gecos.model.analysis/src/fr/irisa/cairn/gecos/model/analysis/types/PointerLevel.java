package fr.irisa.cairn.gecos.model.analysis.types;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import gecos.types.PtrType;
import gecos.types.Type;

/**
 * Represent a N dimensional pointer Type by: N and the const qualifiers of each dimension.
 * 
 * @author aelmouss
 */
public class PointerLevel extends AbstractMultiDimLevel {
	
	private List<Boolean> isConst = new ArrayList<>(); //outermost dimension First
	
	private PointerLevel() {}
	
	public static PointerLevel constructFrom(Type type, TypeAnalyzer typeAnalyzer) {
		Type skipAliases = typeAnalyzer.skipAliases(type);
		if(!checkType(skipAliases))
			return null;
		
		PointerLevel level = new PointerLevel();
		
		while (checkType(skipAliases)) {
			type = skipAliases;
			
			level.nDims++;
			
			level.isConst.add(type.isConstant());
			
			type = ((PtrType) type).getBase();
			skipAliases = typeAnalyzer.skipAliases(type);
		}
		
		level.remainingType = type;
		return level;
	}
	
//	private static Type getBase(Type type) {
//		if(type instanceof ConstType)
//			return ((ConstType) type).getBase();
//		else if(type instanceof PtrType)
//			return ((PtrType) type).getBase();
//		return null;
//	}
	
	private static boolean checkType(Type type) {
		return (type != null) && (type instanceof PtrType);
	}
	
	@Override
	public Type revertOn(Type type) {
		for(int i = nDims-1; i >= 0; i--) {
			type = GecosUserTypeFactory.PTR(type);
			type.setConstant(isConst.get(i));
		}
		return type;
	}
	
	/**
	 * Outermost dimension first
	 */
	public List<Boolean> getConst() {
		return isConst;
	}
	
	/**
	 * @param dim pointer dimension (in outermost dimension first order)
	 * @return true if pointer dimension {@code dim} is const, false otherwise 
	 * @throws InvalidParameterException when {@code dim} is out of range
	 */
	public boolean isConst(int dim) {
		if(dim < 0 || dim >= isConst.size())
			throw new InvalidParameterException("dim (=" + dim + ") is out of range! (max=" + (isConst.size()-1) + ")");
		return isConst.get(dim);
	}

	@Override
	public String toString() {
		return "Pointer " + nDims + " dimensions " + isConst;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if(!(obj instanceof PointerLevel))
			return false;
		
		PointerLevel other = (PointerLevel)obj;
		if(other.getNDims() != this.getNDims())
			return false;
		
		for(int i = 0; i < this.getNDims(); ++i)
			if(other.isConst(i) != this.isConst(i))
				return false;
		
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((isConst == null) ? 0 : isConst.hashCode());
		result = prime * result + nDims;
		return result;
	}

	@Override
	public boolean isSimilar(TypeLevel obj) {
		if(!(obj instanceof PointerLevel))
			return false;
		
		PointerLevel other = (PointerLevel)obj;
		return other.getNDims() == this.getNDims();
		
	}

}
