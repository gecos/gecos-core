/**
 */
package gecos.types;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Integer Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.IntegerType#getSignModifier <em>Sign Modifier</em>}</li>
 *   <li>{@link gecos.types.IntegerType#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see gecos.types.TypesPackage#getIntegerType()
 * @model
 * @generated
 */
public interface IntegerType extends BaseType, TypesVisitable {
	/**
	 * Returns the value of the '<em><b>Sign Modifier</b></em>' attribute.
	 * The default value is <code>"NONE"</code>.
	 * The literals are from the enumeration {@link gecos.types.SignModifiers}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sign Modifier</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sign Modifier</em>' attribute.
	 * @see gecos.types.SignModifiers
	 * @see #setSignModifier(SignModifiers)
	 * @see gecos.types.TypesPackage#getIntegerType_SignModifier()
	 * @model default="NONE" unique="false"
	 * @generated
	 */
	SignModifiers getSignModifier();

	/**
	 * Sets the value of the '{@link gecos.types.IntegerType#getSignModifier <em>Sign Modifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sign Modifier</em>' attribute.
	 * @see gecos.types.SignModifiers
	 * @see #getSignModifier()
	 * @generated
	 */
	void setSignModifier(SignModifiers value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The default value is <code>"INT"</code>.
	 * The literals are from the enumeration {@link gecos.types.IntegerTypes}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see gecos.types.IntegerTypes
	 * @see #setType(IntegerTypes)
	 * @see gecos.types.TypesPackage#getIntegerType_Type()
	 * @model default="INT" unique="false"
	 * @generated
	 */
	IntegerTypes getType();

	/**
	 * Sets the value of the '{@link gecos.types.IntegerType#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see gecos.types.IntegerTypes
	 * @see #getType()
	 * @generated
	 */
	void setType(IntegerTypes value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitIntegerType(this);'"
	 * @generated
	 */
	void accept(TypesVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return true if {@link #getSignModifier()} is not {@link SignModifiers.UNSIGNED}.
	 * <!-- end-model-doc -->
	 * @model kind="operation" dataType="gecos.types.boolean" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.SignModifiers%&gt; _signModifier = this.getSignModifier();\nreturn (!&lt;%com.google.common.base.Objects%&gt;.equal(_signModifier, &lt;%gecos.types.SignModifiers%&gt;.UNSIGNED));'"
	 * @generated
	 */
	boolean getSigned();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.types.boolean" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.IntegerTypes%&gt; _type = this.getType();\nreturn &lt;%com.google.common.base.Objects%&gt;.equal(_type, &lt;%gecos.types.IntegerTypes%&gt;.CHAR);'"
	 * @generated
	 */
	boolean isChar();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.types.boolean" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.IntegerTypes%&gt; _type = this.getType();\nreturn &lt;%com.google.common.base.Objects%&gt;.equal(_type, &lt;%gecos.types.IntegerTypes%&gt;.SHORT);'"
	 * @generated
	 */
	boolean isShort();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.types.boolean" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.IntegerTypes%&gt; _type = this.getType();\nreturn &lt;%com.google.common.base.Objects%&gt;.equal(_type, &lt;%gecos.types.IntegerTypes%&gt;.LONG);'"
	 * @generated
	 */
	boolean isLong();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.types.boolean" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.IntegerTypes%&gt; _type = this.getType();\nreturn &lt;%com.google.common.base.Objects%&gt;.equal(_type, &lt;%gecos.types.IntegerTypes%&gt;.LONG_LONG);'"
	 * @generated
	 */
	boolean isLongLong();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.boolean" unique="false" otherUnique="false" ignoreStorageClassDataType="gecos.types.boolean" ignoreStorageClassUnique="false" ignoreConstDataType="gecos.types.boolean" ignoreConstUnique="false" ignoreVolatileDataType="gecos.types.boolean" ignoreVolatileUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((other == this))\n{\n\treturn true;\n}\nif ((other instanceof &lt;%gecos.types.IntegerType%&gt;))\n{\n\treturn ((((&lt;%gecos.types.IntegerType%&gt;)other).getType().equals(this.getType()) &amp;&amp; (((&lt;%gecos.types.IntegerType%&gt;)other).getSignModifier().getValue() == this.getSignModifier().getValue())) &amp;&amp; super.isEqual(other, ignoreStorageClass, ignoreConst, ignoreVolatile));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isEqual(Type other, boolean ignoreStorageClass, boolean ignoreConst, boolean ignoreVolatile);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _string = super.toString();\nfinal &lt;%java.lang.StringBuffer%&gt; result = new &lt;%java.lang.StringBuffer%&gt;(_string);\nint _value = this.getSignModifier().getValue();\nboolean _tripleNotEquals = (_value != &lt;%gecos.types.SignModifiers%&gt;.NONE_VALUE);\nif (_tripleNotEquals)\n{\n\tresult.append(this.getSignModifier().getLiteral().toLowerCase()).append(\" \");\n}\nresult.append(this.getType().getLiteral().toLowerCase());\nreturn result.toString();'"
	 * @generated
	 */
	String toString();

} // IntegerType
