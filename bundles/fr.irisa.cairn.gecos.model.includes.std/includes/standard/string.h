#ifndef _STRING_H
#define _STRING_H

extern char * strcpy(char *,const char *);
extern char * strncpy(char *,const char *, int);
extern char * strcat(char *, const char *);
extern char * strncat(char *, const char *, int);
extern char * strchr(const char *,int);
extern char * strrchr(const char *,int);
extern char * strpbrk(const char *,const char *);
extern char * strtok(char *,const char *);
extern char * strstr(const char *,const char *);
extern int strlen(const char *);
extern int strnlen(const char *,int);
extern int strspn(const char *,const char *);
extern int strcspn(const char *,const char *);
extern int strcmp(const char *,const char *);
extern int strncmp(const char *,const char *,int);
extern int strnicmp(const char *, const char *, int);
extern void * memset(void *,int,int);
extern void * memcpy(void *,const void *,int);
extern void * memmove(void *,const void *,int);
extern void * memscan(void *,int,int);
extern int memcmp(const void *,const void *,int);

extern char *strpbrk (const char *__s, const char *__accept);
extern char *strstr (const char *__haystack, const char *__needle);

void ___string_h();

#endif 

