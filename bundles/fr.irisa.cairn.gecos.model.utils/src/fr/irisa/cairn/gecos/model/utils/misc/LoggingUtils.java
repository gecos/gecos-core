package fr.irisa.cairn.gecos.model.utils.misc;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.StreamHandler;

public class LoggingUtils {
	

	public static Logger loggerToFile(Logger logger, Level level, Path logFilePath, boolean teeToStdout, Formatter formatter) {
		try {
			if(teeToStdout)
				useStdout(logger, level);
			else
				logger.setUseParentHandlers(false);
			
			FileHandler fh = new FileHandler(logFilePath.toString());
			logger.addHandler(fh);
			
	        if (formatter != null)
	        	fh.setFormatter(formatter);
	        else
	        	fh.setFormatter(new BasicFormatter());
	        
		} catch (SecurityException | IOException e) {
			e.printStackTrace();
		}
       
        return logger;
	}

	public static Logger loggerToFile(Logger logger, Level level, Path logFilePath, boolean teeToStdout) {
		return loggerToFile(logger, level, logFilePath, teeToStdout, null);
	}

	public static Logger anonymousLoggerToFile(Path logFilePath, boolean useParentHandler) {
		return loggerToFile(Logger.getAnonymousLogger(), Level.ALL, logFilePath, useParentHandler);
	}
	public static Logger anonymousLoggerToFile(Level level, Path logFilePath, boolean useParentHandler) {
		return loggerToFile(Logger.getAnonymousLogger(), level, logFilePath, useParentHandler);
	}

	public static Logger anonymousLoggerToFile(Path logFilePath) {
		return anonymousLoggerToFile(Level.ALL, logFilePath, false);
	}
	public static Logger anonymousLoggerToFile(Level level, Path logFilePath) {
		return anonymousLoggerToFile(level, logFilePath, false);
	}
	
	public static class BasicFormatter extends Formatter {
	    private final Date dat = new Date();
	    private final SimpleDateFormat dateFormater = new SimpleDateFormat("HH:mm:ss");
	    
	    public synchronized String format(LogRecord record) {
	        dat.setTime(record.getMillis());
//	        String source;
//	        if (record.getSourceClassName() != null) {
//	            source = record.getSourceClassName();
//	            if (record.getSourceMethodName() != null) {
//	               source += " " + record.getSourceMethodName();
//	            }
//	        } else {
//	            source = record.getLoggerName();
//	        }
	        String message = formatMessage(record);
	        String throwable = "";
	        if (record.getThrown() != null) {
	            StringWriter sw = new StringWriter();
	            PrintWriter pw = new PrintWriter(sw);
	            pw.println();
	            record.getThrown().printStackTrace(pw);
	            pw.close();
	            throwable = sw.toString();
	        }
	        return String.format("[%s][%s][%s] %s %s%n",
	        		record.getLoggerName(),
	        		record.getLevel().getName(),
	        		dateFormater.format(dat),
	                             message,
	                             throwable);
	    }
	}
	

	public static class PlainFormatter extends Formatter {
	    public synchronized String format(LogRecord record) {
	        String message = formatMessage(record);
	        return String.format("%s\n", message);
	    }
	}
	
	public static Logger useStdout(Logger logger, Level level) {
		logger.setLevel(level);
		StreamHandler fh = new StreamHandler(System.out, new BasicFormatter()) {
			@Override
			public synchronized void publish(LogRecord record) {
				super.publish(record);
				super.flush();
			}

			@Override
			public synchronized void close() throws SecurityException {
				super.flush();
			}
		};
		fh.setLevel(level);
		logger.addHandler(fh);
		logger.setUseParentHandlers(false);
		
		return logger;
	}
	
}
