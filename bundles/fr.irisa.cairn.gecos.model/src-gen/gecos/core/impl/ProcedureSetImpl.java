/**
 */
package gecos.core.impl;

import com.google.common.base.Objects;

import com.google.common.collect.Iterables;

import fr.irisa.cairn.gecos.model.tools.utils.GecosCopier;

import gecos.annotations.AnnotatedElement;
import gecos.annotations.AnnotationKeys;
import gecos.annotations.AnnotationsPackage;
import gecos.annotations.FileLocationAnnotation;
import gecos.annotations.IAnnotation;
import gecos.annotations.PragmaAnnotation;

import gecos.annotations.impl.StringToIAnnotationMapImpl;

import gecos.core.CorePackage;
import gecos.core.CoreVisitor;
import gecos.core.ISymbolUse;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.Symbol;

import java.util.function.Consumer;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Procedure Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.core.impl.ProcedureSetImpl#getScope <em>Scope</em>}</li>
 *   <li>{@link gecos.core.impl.ProcedureSetImpl#getAnnotations <em>Annotations</em>}</li>
 *   <li>{@link gecos.core.impl.ProcedureSetImpl#getUses <em>Uses</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProcedureSetImpl extends MinimalEObjectImpl.Container implements ProcedureSet {
	/**
	 * The cached value of the '{@link #getScope() <em>Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScope()
	 * @generated
	 * @ordered
	 */
	protected Scope scope;

	/**
	 * The cached value of the '{@link #getAnnotations() <em>Annotations</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, IAnnotation> annotations;

	/**
	 * The cached value of the '{@link #getUses() <em>Uses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUses()
	 * @generated
	 * @ordered
	 */
	protected EList<ISymbolUse> uses;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProcedureSetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.PROCEDURE_SET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scope getScope() {
		return scope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetScope(Scope newScope, NotificationChain msgs) {
		Scope oldScope = scope;
		scope = newScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.PROCEDURE_SET__SCOPE, oldScope, newScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScope(Scope newScope) {
		if (newScope != scope) {
			NotificationChain msgs = null;
			if (scope != null)
				msgs = ((InternalEObject)scope).eInverseRemove(this, CorePackage.SCOPE__CONTAINER, Scope.class, msgs);
			if (newScope != null)
				msgs = ((InternalEObject)newScope).eInverseAdd(this, CorePackage.SCOPE__CONTAINER, Scope.class, msgs);
			msgs = basicSetScope(newScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PROCEDURE_SET__SCOPE, newScope, newScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, IAnnotation> getAnnotations() {
		if (annotations == null) {
			annotations = new EcoreEMap<String,IAnnotation>(AnnotationsPackage.Literals.STRING_TO_IANNOTATION_MAP, StringToIAnnotationMapImpl.class, this, CorePackage.PROCEDURE_SET__ANNOTATIONS);
		}
		return annotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ISymbolUse> getUses() {
		if (uses == null) {
			uses = new EObjectContainmentEList<ISymbolUse>(ISymbolUse.class, this, CorePackage.PROCEDURE_SET__USES);
		}
		return uses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Procedure findProcedure(final String name) {
		final Function1<Procedure, Boolean> _function = new Function1<Procedure, Boolean>() {
			public Boolean apply(final Procedure p) {
				String _name = p.getSymbol().getName();
				return Boolean.valueOf(Objects.equal(_name, name));
			}
		};
		return IterableExtensions.<Procedure>findFirst(this.listProcedures(), _function);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Procedure> listProcedures() {
		final Function1<ProcedureSymbol, Procedure> _function = new Function1<ProcedureSymbol, Procedure>() {
			public Procedure apply(final ProcedureSymbol ps) {
				return ps.getProcedure();
			}
		};
		return ECollections.<Procedure>unmodifiableEList(ECollections.<Procedure>asEList(((Procedure[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<Procedure>filterNull(IterableExtensions.<ProcedureSymbol, Procedure>map(Iterables.<ProcedureSymbol>filter(this.getScope().getSymbols(), ProcedureSymbol.class), _function)), Procedure.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addProcedure(final Procedure proc) {
		final ProcedureSymbol symbol = proc.getSymbol();
		if ((symbol == null)) {
			throw new IllegalArgumentException("Cannot add a procedure without symbol.");
		}
		this.addSymbol(symbol);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addAllProcedures(final EList<Procedure> procs) {
		final Consumer<Procedure> _function = new Consumer<Procedure>() {
			public void accept(final Procedure it) {
				ProcedureSetImpl.this.addProcedure(it);
			}
		};
		procs.forEach(_function);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeProcedure(final Procedure proc) {
		final ProcedureSymbol symbol = proc.getSymbol();
		if ((symbol == null)) {
			throw new IllegalArgumentException("Cannot remove a procedure without symbol.");
		}
		this.removeSymbol(symbol);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeAllProcedures() {
		final Function1<Procedure, ProcedureSymbol> _function = new Function1<Procedure, ProcedureSymbol>() {
			public ProcedureSymbol apply(final Procedure it) {
				return it.getSymbol();
			}
		};
		final Consumer<ProcedureSymbol> _function_1 = new Consumer<ProcedureSymbol>() {
			public void accept(final ProcedureSymbol it) {
				ProcedureSetImpl.this.removeSymbol(it);
			}
		};
		XcoreEListExtensions.<Procedure, ProcedureSymbol>map(this.listProcedures(), _function).forEach(_function_1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		return "ProcedureSet";
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final CoreVisitor visitor) {
		visitor.visitProcedureSet(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureSet copy() {
		final GecosCopier gecosCopier = new GecosCopier();
		EObject _copy = gecosCopier.copy(this);
		final ProcedureSet procedureSet = ((ProcedureSet) _copy);
		gecosCopier.copyReferences();
		return procedureSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IAnnotation getAnnotation(final String key) {
		return this.getAnnotations().get(key);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnnotation(final String key, final IAnnotation annot) {
		this.getAnnotations().put(key, annot);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PragmaAnnotation getPragma() {
		final IAnnotation annot = this.getAnnotation(AnnotationKeys.PRAGMA_ANNOTATION_KEY.getLiteral());
		if ((annot instanceof PragmaAnnotation)) {
			return ((PragmaAnnotation)annot);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FileLocationAnnotation getFileLocation() {
		final IAnnotation annot = this.getAnnotation(AnnotationKeys.FILE_LOCATION_ANNOTATION_KEY.getLiteral());
		if ((annot instanceof FileLocationAnnotation)) {
			return ((FileLocationAnnotation)annot);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addSymbol(final Symbol symbol) {
		this.getScope().addSymbol(symbol);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeSymbol(final Symbol symbol) {
		this.getScope().removeSymbol(symbol);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.PROCEDURE_SET__SCOPE:
				if (scope != null)
					msgs = ((InternalEObject)scope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROCEDURE_SET__SCOPE, null, msgs);
				return basicSetScope((Scope)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.PROCEDURE_SET__SCOPE:
				return basicSetScope(null, msgs);
			case CorePackage.PROCEDURE_SET__ANNOTATIONS:
				return ((InternalEList<?>)getAnnotations()).basicRemove(otherEnd, msgs);
			case CorePackage.PROCEDURE_SET__USES:
				return ((InternalEList<?>)getUses()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CorePackage.PROCEDURE_SET__SCOPE:
				return getScope();
			case CorePackage.PROCEDURE_SET__ANNOTATIONS:
				if (coreType) return getAnnotations();
				else return getAnnotations().map();
			case CorePackage.PROCEDURE_SET__USES:
				return getUses();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CorePackage.PROCEDURE_SET__SCOPE:
				setScope((Scope)newValue);
				return;
			case CorePackage.PROCEDURE_SET__ANNOTATIONS:
				((EStructuralFeature.Setting)getAnnotations()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CorePackage.PROCEDURE_SET__SCOPE:
				setScope((Scope)null);
				return;
			case CorePackage.PROCEDURE_SET__ANNOTATIONS:
				getAnnotations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CorePackage.PROCEDURE_SET__SCOPE:
				return scope != null;
			case CorePackage.PROCEDURE_SET__ANNOTATIONS:
				return annotations != null && !annotations.isEmpty();
			case CorePackage.PROCEDURE_SET__USES:
				return uses != null && !uses.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == AnnotatedElement.class) {
			switch (derivedFeatureID) {
				case CorePackage.PROCEDURE_SET__ANNOTATIONS: return AnnotationsPackage.ANNOTATED_ELEMENT__ANNOTATIONS;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == AnnotatedElement.class) {
			switch (baseFeatureID) {
				case AnnotationsPackage.ANNOTATED_ELEMENT__ANNOTATIONS: return CorePackage.PROCEDURE_SET__ANNOTATIONS;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ProcedureSetImpl
