/**
 */
package gecos.types.impl;

import gecos.annotations.AnnotationsPackage;

import gecos.annotations.impl.AnnotationsPackageImpl;

import gecos.blocks.BlocksPackage;

import gecos.blocks.impl.BlocksPackageImpl;

import gecos.core.CorePackage;

import gecos.core.impl.CorePackageImpl;

import gecos.dag.DagPackage;

import gecos.dag.impl.DagPackageImpl;

import gecos.instrs.InstrsPackage;

import gecos.instrs.impl.InstrsPackageImpl;

import gecos.types.ACChannelType;
import gecos.types.ACComplexType;
import gecos.types.ACFixedType;
import gecos.types.ACFloatType;
import gecos.types.ACIntType;
import gecos.types.ACType;
import gecos.types.AliasType;
import gecos.types.ArrayType;
import gecos.types.BaseType;
import gecos.types.BoolType;
import gecos.types.EnumType;
import gecos.types.Enumerator;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.FloatType;
import gecos.types.FunctionType;
import gecos.types.IntegerType;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.PtrType;
import gecos.types.QuantificationMode;
import gecos.types.RecordType;
import gecos.types.ScalarType;
import gecos.types.SignModifiers;
import gecos.types.SimdType;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import gecos.types.TypesFactory;
import gecos.types.TypesPackage;
import gecos.types.TypesVisitable;
import gecos.types.TypesVisitor;
import gecos.types.UndefinedType;
import gecos.types.VoidType;
import gecos.types.__Internal_ParameterTypeWrapper;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.ETypeParameter;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TypesPackageImpl extends EPackageImpl implements TypesPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typesVisitableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fieldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass enumeratorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass arrayTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ptrTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass __Internal_ParameterTypeWrapperEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass recordTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aliasTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass enumTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simdTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass undefinedTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scalarTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass integerTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass floatTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass boolTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass voidTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acIntTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acFixedTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acFloatTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acComplexTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acChannelTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typesVisitorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum storageClassSpecifiersEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum kindsEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum integerTypesEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum signModifiersEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum floatPrecisionsEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum overflowModeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum quantificationModeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType stringEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType longEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType intEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType booleanEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see gecos.types.TypesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TypesPackageImpl() {
		super(eNS_URI, TypesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TypesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TypesPackage init() {
		if (isInited) return (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);

		// Obtain or create and register package
		TypesPackageImpl theTypesPackage = (TypesPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TypesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TypesPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
		InstrsPackageImpl theInstrsPackage = (InstrsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI) instanceof InstrsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI) : InstrsPackage.eINSTANCE);
		AnnotationsPackageImpl theAnnotationsPackage = (AnnotationsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI) instanceof AnnotationsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI) : AnnotationsPackage.eINSTANCE);
		BlocksPackageImpl theBlocksPackage = (BlocksPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI) instanceof BlocksPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI) : BlocksPackage.eINSTANCE);
		DagPackageImpl theDagPackage = (DagPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DagPackage.eNS_URI) instanceof DagPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DagPackage.eNS_URI) : DagPackage.eINSTANCE);

		// Create package meta-data objects
		theTypesPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theInstrsPackage.createPackageContents();
		theAnnotationsPackage.createPackageContents();
		theBlocksPackage.createPackageContents();
		theDagPackage.createPackageContents();

		// Initialize created meta-data
		theTypesPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theInstrsPackage.initializePackageContents();
		theAnnotationsPackage.initializePackageContents();
		theBlocksPackage.initializePackageContents();
		theDagPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTypesPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TypesPackage.eNS_URI, theTypesPackage);
		return theTypesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTypesVisitable() {
		return typesVisitableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getField() {
		return fieldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getField_Record() {
		return (EReference)fieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getField_Name() {
		return (EAttribute)fieldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getField_Type() {
		return (EReference)fieldEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getField_Offset() {
		return (EAttribute)fieldEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getField_Bitwidth() {
		return (EAttribute)fieldEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnumerator() {
		return enumeratorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEnumerator_EnumType() {
		return (EReference)enumeratorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEnumerator_Name() {
		return (EAttribute)enumeratorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEnumerator_Expression() {
		return (EReference)enumeratorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getType() {
		return typeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getType_Constant() {
		return (EAttribute)typeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getType_Volatile() {
		return (EAttribute)typeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getType_StorageClass() {
		return (EAttribute)typeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getArrayType() {
		return arrayTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getArrayType_Base() {
		return (EReference)arrayTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getArrayType_Lower() {
		return (EAttribute)arrayTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getArrayType_Upper() {
		return (EAttribute)arrayTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getArrayType_SizeExpr() {
		return (EReference)arrayTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPtrType() {
		return ptrTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPtrType_Base() {
		return (EReference)ptrTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPtrType_Restrict() {
		return (EAttribute)ptrTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass get__Internal_ParameterTypeWrapper() {
		return __Internal_ParameterTypeWrapperEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference get__Internal_ParameterTypeWrapper_Parameter() {
		return (EReference)__Internal_ParameterTypeWrapperEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFunctionType() {
		return functionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunctionType_ReturnType() {
		return (EReference)functionTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunctionType___internal_parameter_wrapers() {
		return (EReference)functionTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFunctionType_HasElipsis() {
		return (EAttribute)functionTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFunctionType_Inline() {
		return (EAttribute)functionTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFunctionType_Noreturn() {
		return (EAttribute)functionTypeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRecordType() {
		return recordTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRecordType_Fields() {
		return (EReference)recordTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRecordType_Kind() {
		return (EAttribute)recordTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRecordType_Defined() {
		return (EAttribute)recordTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRecordType_Name() {
		return (EAttribute)recordTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAliasType() {
		return aliasTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAliasType_Alias() {
		return (EReference)aliasTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAliasType_Name() {
		return (EAttribute)aliasTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnumType() {
		return enumTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEnumType_Name() {
		return (EAttribute)enumTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEnumType_Enumerators() {
		return (EReference)enumTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimdType() {
		return simdTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimdType_SubwordType() {
		return (EReference)simdTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimdType_Size() {
		return (EAttribute)simdTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUndefinedType() {
		return undefinedTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScalarType() {
		return scalarTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseType() {
		return baseTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseType_Size() {
		return (EAttribute)baseTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntegerType() {
		return integerTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntegerType_SignModifier() {
		return (EAttribute)integerTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntegerType_Type() {
		return (EAttribute)integerTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFloatType() {
		return floatTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFloatType_Precision() {
		return (EAttribute)floatTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBoolType() {
		return boolTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVoidType() {
		return voidTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getACType() {
		return acTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getACType_Signed() {
		return (EAttribute)acTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getACIntType() {
		return acIntTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getACIntType_Bitwidth() {
		return (EAttribute)acIntTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getACIntType_OverflowMode() {
		return (EAttribute)acIntTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getACFixedType() {
		return acFixedTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getACFixedType_Bitwidth() {
		return (EAttribute)acFixedTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getACFixedType_Mode() {
		return (EAttribute)acFixedTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getACFixedType_Fraction() {
		return (EAttribute)acFixedTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getACFixedType_Integer() {
		return (EAttribute)acFixedTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getACFixedType_QuantificationMode() {
		return (EAttribute)acFixedTypeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getACFixedType_OverflowMode() {
		return (EAttribute)acFixedTypeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getACFloatType() {
		return acFloatTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getACFloatType_Exponent() {
		return (EAttribute)acFloatTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getACFloatType_Mantissa() {
		return (EAttribute)acFloatTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getACFloatType_Radix() {
		return (EAttribute)acFloatTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getACComplexType() {
		return acComplexTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getACComplexType_BaseType() {
		return (EReference)acComplexTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getACChannelType() {
		return acChannelTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getACChannelType_BaseType() {
		return (EReference)acChannelTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTypesVisitor() {
		return typesVisitorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getStorageClassSpecifiers() {
		return storageClassSpecifiersEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getKinds() {
		return kindsEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getIntegerTypes() {
		return integerTypesEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSignModifiers() {
		return signModifiersEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getFloatPrecisions() {
		return floatPrecisionsEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getOverflowMode() {
		return overflowModeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getQuantificationMode() {
		return quantificationModeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getString() {
		return stringEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getlong() {
		return longEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getint() {
		return intEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getboolean() {
		return booleanEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypesFactory getTypesFactory() {
		return (TypesFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		typesVisitableEClass = createEClass(TYPES_VISITABLE);

		fieldEClass = createEClass(FIELD);
		createEReference(fieldEClass, FIELD__RECORD);
		createEAttribute(fieldEClass, FIELD__NAME);
		createEReference(fieldEClass, FIELD__TYPE);
		createEAttribute(fieldEClass, FIELD__OFFSET);
		createEAttribute(fieldEClass, FIELD__BITWIDTH);

		enumeratorEClass = createEClass(ENUMERATOR);
		createEReference(enumeratorEClass, ENUMERATOR__ENUM_TYPE);
		createEAttribute(enumeratorEClass, ENUMERATOR__NAME);
		createEReference(enumeratorEClass, ENUMERATOR__EXPRESSION);

		typeEClass = createEClass(TYPE);
		createEAttribute(typeEClass, TYPE__CONSTANT);
		createEAttribute(typeEClass, TYPE__VOLATILE);
		createEAttribute(typeEClass, TYPE__STORAGE_CLASS);

		arrayTypeEClass = createEClass(ARRAY_TYPE);
		createEReference(arrayTypeEClass, ARRAY_TYPE__BASE);
		createEAttribute(arrayTypeEClass, ARRAY_TYPE__LOWER);
		createEAttribute(arrayTypeEClass, ARRAY_TYPE__UPPER);
		createEReference(arrayTypeEClass, ARRAY_TYPE__SIZE_EXPR);

		ptrTypeEClass = createEClass(PTR_TYPE);
		createEReference(ptrTypeEClass, PTR_TYPE__BASE);
		createEAttribute(ptrTypeEClass, PTR_TYPE__RESTRICT);

		__Internal_ParameterTypeWrapperEClass = createEClass(_INTERNAL_PARAMETER_TYPE_WRAPPER);
		createEReference(__Internal_ParameterTypeWrapperEClass, _INTERNAL_PARAMETER_TYPE_WRAPPER__PARAMETER);

		functionTypeEClass = createEClass(FUNCTION_TYPE);
		createEReference(functionTypeEClass, FUNCTION_TYPE__RETURN_TYPE);
		createEReference(functionTypeEClass, FUNCTION_TYPE__INTERNAL_PARAMETER_WRAPERS);
		createEAttribute(functionTypeEClass, FUNCTION_TYPE__HAS_ELIPSIS);
		createEAttribute(functionTypeEClass, FUNCTION_TYPE__INLINE);
		createEAttribute(functionTypeEClass, FUNCTION_TYPE__NORETURN);

		recordTypeEClass = createEClass(RECORD_TYPE);
		createEReference(recordTypeEClass, RECORD_TYPE__FIELDS);
		createEAttribute(recordTypeEClass, RECORD_TYPE__KIND);
		createEAttribute(recordTypeEClass, RECORD_TYPE__DEFINED);
		createEAttribute(recordTypeEClass, RECORD_TYPE__NAME);

		aliasTypeEClass = createEClass(ALIAS_TYPE);
		createEReference(aliasTypeEClass, ALIAS_TYPE__ALIAS);
		createEAttribute(aliasTypeEClass, ALIAS_TYPE__NAME);

		enumTypeEClass = createEClass(ENUM_TYPE);
		createEAttribute(enumTypeEClass, ENUM_TYPE__NAME);
		createEReference(enumTypeEClass, ENUM_TYPE__ENUMERATORS);

		simdTypeEClass = createEClass(SIMD_TYPE);
		createEReference(simdTypeEClass, SIMD_TYPE__SUBWORD_TYPE);
		createEAttribute(simdTypeEClass, SIMD_TYPE__SIZE);

		undefinedTypeEClass = createEClass(UNDEFINED_TYPE);

		scalarTypeEClass = createEClass(SCALAR_TYPE);

		baseTypeEClass = createEClass(BASE_TYPE);
		createEAttribute(baseTypeEClass, BASE_TYPE__SIZE);

		integerTypeEClass = createEClass(INTEGER_TYPE);
		createEAttribute(integerTypeEClass, INTEGER_TYPE__SIGN_MODIFIER);
		createEAttribute(integerTypeEClass, INTEGER_TYPE__TYPE);

		floatTypeEClass = createEClass(FLOAT_TYPE);
		createEAttribute(floatTypeEClass, FLOAT_TYPE__PRECISION);

		boolTypeEClass = createEClass(BOOL_TYPE);

		voidTypeEClass = createEClass(VOID_TYPE);

		acTypeEClass = createEClass(AC_TYPE);
		createEAttribute(acTypeEClass, AC_TYPE__SIGNED);

		acIntTypeEClass = createEClass(AC_INT_TYPE);
		createEAttribute(acIntTypeEClass, AC_INT_TYPE__BITWIDTH);
		createEAttribute(acIntTypeEClass, AC_INT_TYPE__OVERFLOW_MODE);

		acFixedTypeEClass = createEClass(AC_FIXED_TYPE);
		createEAttribute(acFixedTypeEClass, AC_FIXED_TYPE__BITWIDTH);
		createEAttribute(acFixedTypeEClass, AC_FIXED_TYPE__MODE);
		createEAttribute(acFixedTypeEClass, AC_FIXED_TYPE__FRACTION);
		createEAttribute(acFixedTypeEClass, AC_FIXED_TYPE__INTEGER);
		createEAttribute(acFixedTypeEClass, AC_FIXED_TYPE__QUANTIFICATION_MODE);
		createEAttribute(acFixedTypeEClass, AC_FIXED_TYPE__OVERFLOW_MODE);

		acFloatTypeEClass = createEClass(AC_FLOAT_TYPE);
		createEAttribute(acFloatTypeEClass, AC_FLOAT_TYPE__EXPONENT);
		createEAttribute(acFloatTypeEClass, AC_FLOAT_TYPE__MANTISSA);
		createEAttribute(acFloatTypeEClass, AC_FLOAT_TYPE__RADIX);

		acComplexTypeEClass = createEClass(AC_COMPLEX_TYPE);
		createEReference(acComplexTypeEClass, AC_COMPLEX_TYPE__BASE_TYPE);

		acChannelTypeEClass = createEClass(AC_CHANNEL_TYPE);
		createEReference(acChannelTypeEClass, AC_CHANNEL_TYPE__BASE_TYPE);

		typesVisitorEClass = createEClass(TYPES_VISITOR);

		// Create enums
		storageClassSpecifiersEEnum = createEEnum(STORAGE_CLASS_SPECIFIERS);
		kindsEEnum = createEEnum(KINDS);
		integerTypesEEnum = createEEnum(INTEGER_TYPES);
		signModifiersEEnum = createEEnum(SIGN_MODIFIERS);
		floatPrecisionsEEnum = createEEnum(FLOAT_PRECISIONS);
		overflowModeEEnum = createEEnum(OVERFLOW_MODE);
		quantificationModeEEnum = createEEnum(QUANTIFICATION_MODE);

		// Create data types
		stringEDataType = createEDataType(STRING);
		longEDataType = createEDataType(LONG);
		intEDataType = createEDataType(INT);
		booleanEDataType = createEDataType(BOOLEAN);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		InstrsPackage theInstrsPackage = (InstrsPackage)EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI);
		AnnotationsPackage theAnnotationsPackage = (AnnotationsPackage)EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		typesVisitableEClass.getESuperTypes().add(theCorePackage.getGecosNode());
		fieldEClass.getESuperTypes().add(this.getTypesVisitable());
		fieldEClass.getESuperTypes().add(theCorePackage.getITypedElement());
		enumeratorEClass.getESuperTypes().add(this.getTypesVisitable());
		typeEClass.getESuperTypes().add(theAnnotationsPackage.getAnnotatedElement());
		typeEClass.getESuperTypes().add(this.getTypesVisitable());
		arrayTypeEClass.getESuperTypes().add(this.getType());
		arrayTypeEClass.getESuperTypes().add(this.getTypesVisitable());
		ptrTypeEClass.getESuperTypes().add(this.getType());
		ptrTypeEClass.getESuperTypes().add(this.getTypesVisitable());
		functionTypeEClass.getESuperTypes().add(this.getType());
		functionTypeEClass.getESuperTypes().add(this.getTypesVisitable());
		recordTypeEClass.getESuperTypes().add(this.getType());
		recordTypeEClass.getESuperTypes().add(this.getTypesVisitable());
		aliasTypeEClass.getESuperTypes().add(this.getType());
		aliasTypeEClass.getESuperTypes().add(theCorePackage.getITypedElement());
		aliasTypeEClass.getESuperTypes().add(this.getTypesVisitable());
		enumTypeEClass.getESuperTypes().add(this.getType());
		enumTypeEClass.getESuperTypes().add(this.getTypesVisitable());
		simdTypeEClass.getESuperTypes().add(this.getType());
		simdTypeEClass.getESuperTypes().add(this.getTypesVisitable());
		undefinedTypeEClass.getESuperTypes().add(this.getType());
		undefinedTypeEClass.getESuperTypes().add(this.getTypesVisitable());
		scalarTypeEClass.getESuperTypes().add(this.getType());
		scalarTypeEClass.getESuperTypes().add(this.getTypesVisitable());
		baseTypeEClass.getESuperTypes().add(this.getScalarType());
		baseTypeEClass.getESuperTypes().add(this.getTypesVisitable());
		integerTypeEClass.getESuperTypes().add(this.getBaseType());
		integerTypeEClass.getESuperTypes().add(this.getTypesVisitable());
		floatTypeEClass.getESuperTypes().add(this.getBaseType());
		floatTypeEClass.getESuperTypes().add(this.getTypesVisitable());
		boolTypeEClass.getESuperTypes().add(this.getBaseType());
		boolTypeEClass.getESuperTypes().add(this.getTypesVisitable());
		voidTypeEClass.getESuperTypes().add(this.getBaseType());
		voidTypeEClass.getESuperTypes().add(this.getTypesVisitable());
		acTypeEClass.getESuperTypes().add(this.getBaseType());
		acTypeEClass.getESuperTypes().add(this.getTypesVisitable());
		acIntTypeEClass.getESuperTypes().add(this.getArrayType());
		acIntTypeEClass.getESuperTypes().add(this.getACType());
		acIntTypeEClass.getESuperTypes().add(this.getTypesVisitable());
		acFixedTypeEClass.getESuperTypes().add(this.getArrayType());
		acFixedTypeEClass.getESuperTypes().add(this.getACType());
		acFixedTypeEClass.getESuperTypes().add(this.getTypesVisitable());
		acFloatTypeEClass.getESuperTypes().add(this.getACType());
		acFloatTypeEClass.getESuperTypes().add(this.getTypesVisitable());
		acComplexTypeEClass.getESuperTypes().add(this.getACType());
		acComplexTypeEClass.getESuperTypes().add(this.getTypesVisitable());
		acChannelTypeEClass.getESuperTypes().add(this.getACType());
		acChannelTypeEClass.getESuperTypes().add(this.getTypesVisitable());

		// Initialize classes and features; add operations and parameters
		initEClass(typesVisitableEClass, TypesVisitable.class, "TypesVisitable", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = addEOperation(typesVisitableEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypesVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(fieldEClass, Field.class, "Field", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getField_Record(), this.getRecordType(), this.getRecordType_Fields(), "record", null, 0, 1, Field.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getField_Name(), this.getString(), "name", null, 0, 1, Field.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getField_Type(), this.getType(), null, "type", null, 0, 1, Field.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getField_Offset(), this.getint(), "offset", null, 0, 1, Field.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getField_Bitwidth(), this.getlong(), "bitwidth", "-1", 1, 1, Field.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(fieldEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypesVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(fieldEClass, this.getboolean(), "isEqual", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getField(), "field", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(enumeratorEClass, Enumerator.class, "Enumerator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEnumerator_EnumType(), this.getEnumType(), this.getEnumType_Enumerators(), "enumType", null, 0, 1, Enumerator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEnumerator_Name(), this.getString(), "name", null, 0, 1, Enumerator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEnumerator_Expression(), theInstrsPackage.getInstruction(), null, "expression", null, 0, 1, Enumerator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(enumeratorEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypesVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(enumeratorEClass, this.getboolean(), "isEqual", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEnumerator(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(typeEClass, Type.class, "Type", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getType_Constant(), this.getboolean(), "constant", "false", 0, 1, Type.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getType_Volatile(), this.getboolean(), "volatile", "false", 0, 1, Type.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getType_StorageClass(), this.getStorageClassSpecifiers(), "storageClass", "NONE", 0, 1, Type.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(typeEClass, null, "setInnermostStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getStorageClassSpecifiers(), "innermostStorage", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(typeEClass, this.getlong(), "getSize", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(typeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypesVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(typeEClass, theCorePackage.getScope(), "getContainingScope", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(typeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(typeEClass, null, "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		ETypeParameter t1 = addETypeParameter(op, "T");
		EGenericType g1 = createEGenericType(this.getType());
		t1.getEBounds().add(g1);
		g1 = createEGenericType(t1);
		initEOperation(op, g1);

		op = addEOperation(typeEClass, null, "installOn", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getScope(), "scope", 1, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(typeEClass, this.getboolean(), "isStatic", 1, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(typeEClass, this.getboolean(), "isRegister", 1, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(typeEClass, this.getboolean(), "isExtern", 1, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(typeEClass, this.getboolean(), "isAuto", 1, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(typeEClass, this.getBaseType(), "asBase", 1, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(typeEClass, this.getArrayType(), "asArray", 1, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(typeEClass, this.getPtrType(), "asPointer", 1, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(typeEClass, this.getFunctionType(), "asFunction", 1, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(typeEClass, this.getRecordType(), "asRecord", 1, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(typeEClass, this.getAliasType(), "asAlias", 1, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(typeEClass, this.getEnumType(), "asEnum", 1, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(typeEClass, this.getSimdType(), "asSimd", 1, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(typeEClass, this.getboolean(), "isArray", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(typeEClass, this.getboolean(), "isPointer", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(typeEClass, this.getboolean(), "isEqual", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(typeEClass, this.getboolean(), "isEqual", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreConst", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreVolatile", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(typeEClass, this.getboolean(), "isSimilar", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(typeEClass, this.getboolean(), "isSimilar", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreConst", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreVolatile", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(arrayTypeEClass, ArrayType.class, "ArrayType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getArrayType_Base(), this.getType(), null, "base", null, 0, 1, ArrayType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArrayType_Lower(), this.getlong(), "lower", null, 0, 1, ArrayType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArrayType_Upper(), this.getlong(), "upper", null, 0, 1, ArrayType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getArrayType_SizeExpr(), theInstrsPackage.getInstruction(), null, "sizeExpr", null, 0, 1, ArrayType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(arrayTypeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypesVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(arrayTypeEClass, this.getint(), "getNbDims", 1, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(arrayTypeEClass, this.getType(), "getInnermostBase", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(arrayTypeEClass, null, "setInnermostBase", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "innermostBase", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(arrayTypeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(arrayTypeEClass, this.getboolean(), "isEqual", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreConst", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreVolatile", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(arrayTypeEClass, this.getlong(), "getSize", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(arrayTypeEClass, theInstrsPackage.getInstruction(), "getSizes", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(arrayTypeEClass, null, "setInnermostStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getStorageClassSpecifiers(), "innermostStorage", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(ptrTypeEClass, PtrType.class, "PtrType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPtrType_Base(), this.getType(), null, "base", null, 0, 1, PtrType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPtrType_Restrict(), this.getboolean(), "restrict", "false", 0, 1, PtrType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(ptrTypeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypesVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(ptrTypeEClass, this.getint(), "getNbDims", 1, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(ptrTypeEClass, this.getType(), "getInnermostBase", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ptrTypeEClass, null, "setInnermostBase", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "innermostBase", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ptrTypeEClass, this.getboolean(), "isEqual", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreConst", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreVolatile", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(ptrTypeEClass, this.getlong(), "getSize", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(ptrTypeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ptrTypeEClass, null, "setInnermostStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getStorageClassSpecifiers(), "innermostStorage", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(__Internal_ParameterTypeWrapperEClass, __Internal_ParameterTypeWrapper.class, "__Internal_ParameterTypeWrapper", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(get__Internal_ParameterTypeWrapper_Parameter(), this.getType(), null, "parameter", null, 0, 1, __Internal_ParameterTypeWrapper.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(functionTypeEClass, FunctionType.class, "FunctionType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFunctionType_ReturnType(), this.getType(), null, "returnType", null, 0, 1, FunctionType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFunctionType___internal_parameter_wrapers(), this.get__Internal_ParameterTypeWrapper(), null, "__internal_parameter_wrapers", null, 0, -1, FunctionType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFunctionType_HasElipsis(), this.getboolean(), "hasElipsis", "false", 0, 1, FunctionType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFunctionType_Inline(), this.getboolean(), "inline", "false", 0, 1, FunctionType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFunctionType_Noreturn(), this.getboolean(), "noreturn", "false", 0, 1, FunctionType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(functionTypeEClass, this.getType(), "listParameters", 0, -1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(functionTypeEClass, null, "addParameter", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "param", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(functionTypeEClass, this.getType(), "getParameterAt", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getint(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(functionTypeEClass, null, "removeParameterAt", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getint(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(functionTypeEClass, null, "clearParameters", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(functionTypeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypesVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(functionTypeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(functionTypeEClass, this.getlong(), "getSize", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(functionTypeEClass, this.getboolean(), "isEqual", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreConst", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreVolatile", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(functionTypeEClass, null, "setInnermostStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getStorageClassSpecifiers(), "innermostStorage", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(recordTypeEClass, RecordType.class, "RecordType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRecordType_Fields(), this.getField(), this.getField_Record(), "fields", null, 0, -1, RecordType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRecordType_Kind(), this.getKinds(), "kind", null, 0, 1, RecordType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRecordType_Defined(), this.getboolean(), "defined", null, 0, 1, RecordType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRecordType_Name(), this.getString(), "name", "anonymous", 0, 1, RecordType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(recordTypeEClass, this.getlong(), "getSize", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(recordTypeEClass, this.getField(), "findField", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getString(), "key", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(recordTypeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypesVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(recordTypeEClass, this.getboolean(), "isEqual", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreConst", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreVolatile", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(aliasTypeEClass, AliasType.class, "AliasType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAliasType_Alias(), this.getType(), null, "alias", null, 0, 1, AliasType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAliasType_Name(), this.getString(), "name", null, 0, 1, AliasType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(aliasTypeEClass, this.getType(), "getType", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(aliasTypeEClass, null, "setType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "t", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(aliasTypeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypesVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(aliasTypeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(aliasTypeEClass, this.getlong(), "getSize", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(aliasTypeEClass, this.getboolean(), "isEqual", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreConst", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreVolatile", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(aliasTypeEClass, this.getboolean(), "isSimilar", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreConst", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreVolatile", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(enumTypeEClass, EnumType.class, "EnumType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEnumType_Name(), this.getString(), "name", null, 0, 1, EnumType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEnumType_Enumerators(), this.getEnumerator(), this.getEnumerator_EnumType(), "enumerators", null, 0, -1, EnumType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(enumTypeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypesVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(enumTypeEClass, this.getlong(), "getSize", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(enumTypeEClass, this.getboolean(), "isEqual", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreConst", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreVolatile", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(simdTypeEClass, SimdType.class, "SimdType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSimdType_SubwordType(), this.getType(), null, "subwordType", null, 0, 1, SimdType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimdType_Size(), this.getlong(), "size", null, 0, 1, SimdType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(simdTypeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypesVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(simdTypeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(simdTypeEClass, this.getboolean(), "isEqual", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreConst", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreVolatile", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(undefinedTypeEClass, UndefinedType.class, "UndefinedType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(undefinedTypeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(undefinedTypeEClass, this.getboolean(), "isEqual", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreConst", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreVolatile", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(undefinedTypeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypesVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(scalarTypeEClass, ScalarType.class, "ScalarType", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(scalarTypeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(baseTypeEClass, BaseType.class, "BaseType", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBaseType_Size(), this.getlong(), "size", null, 0, 1, BaseType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(baseTypeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypesVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(baseTypeEClass, this.getIntegerType(), "asInt", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(baseTypeEClass, this.getFloatType(), "asFloat", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(baseTypeEClass, this.getBoolType(), "asBool", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(baseTypeEClass, this.getVoidType(), "asVoid", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(baseTypeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(baseTypeEClass, this.getboolean(), "isEqual", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreConst", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreVolatile", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(integerTypeEClass, IntegerType.class, "IntegerType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIntegerType_SignModifier(), this.getSignModifiers(), "signModifier", "NONE", 0, 1, IntegerType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIntegerType_Type(), this.getIntegerTypes(), "type", "INT", 0, 1, IntegerType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(integerTypeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypesVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(integerTypeEClass, this.getboolean(), "getSigned", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(integerTypeEClass, this.getboolean(), "isChar", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(integerTypeEClass, this.getboolean(), "isShort", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(integerTypeEClass, this.getboolean(), "isLong", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(integerTypeEClass, this.getboolean(), "isLongLong", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(integerTypeEClass, this.getboolean(), "isEqual", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreConst", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreVolatile", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(integerTypeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(floatTypeEClass, FloatType.class, "FloatType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFloatType_Precision(), this.getFloatPrecisions(), "precision", "SINGLE", 0, 1, FloatType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(floatTypeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypesVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(floatTypeEClass, this.getboolean(), "isHalf", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(floatTypeEClass, this.getboolean(), "isSingle", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(floatTypeEClass, this.getboolean(), "isDouble", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(floatTypeEClass, this.getboolean(), "isLongDouble", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(floatTypeEClass, this.getboolean(), "isEqual", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreConst", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreVolatile", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(floatTypeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(boolTypeEClass, BoolType.class, "BoolType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(boolTypeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypesVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(boolTypeEClass, this.getboolean(), "isEqual", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreConst", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreVolatile", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(boolTypeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(voidTypeEClass, VoidType.class, "VoidType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(voidTypeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypesVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(voidTypeEClass, this.getboolean(), "isEqual", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreConst", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreVolatile", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(voidTypeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(acTypeEClass, ACType.class, "ACType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getACType_Signed(), this.getboolean(), "signed", "false", 0, 1, ACType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(acTypeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypesVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(acTypeEClass, this.getboolean(), "getSigned", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(acTypeEClass, this.getboolean(), "isEqual", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreConst", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreVolatile", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(acIntTypeEClass, ACIntType.class, "ACIntType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getACIntType_Bitwidth(), this.getlong(), "bitwidth", null, 0, 1, ACIntType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getACIntType_OverflowMode(), this.getOverflowMode(), "overflowMode", null, 0, 1, ACIntType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(acIntTypeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypesVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(acIntTypeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(acIntTypeEClass, this.getboolean(), "isEqual", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreConst", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreVolatile", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(acFixedTypeEClass, ACFixedType.class, "ACFixedType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getACFixedType_Bitwidth(), this.getlong(), "bitwidth", null, 0, 1, ACFixedType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getACFixedType_Mode(), this.getlong(), "mode", null, 0, 1, ACFixedType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getACFixedType_Fraction(), this.getlong(), "fraction", null, 0, 1, ACFixedType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getACFixedType_Integer(), this.getlong(), "integer", null, 0, 1, ACFixedType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getACFixedType_QuantificationMode(), this.getQuantificationMode(), "quantificationMode", null, 0, 1, ACFixedType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getACFixedType_OverflowMode(), this.getOverflowMode(), "overflowMode", null, 0, 1, ACFixedType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(acFixedTypeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypesVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(acFixedTypeEClass, this.getlong(), "getSize", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(acFixedTypeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(acFixedTypeEClass, this.getboolean(), "isEqual", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreConst", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreVolatile", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(acFloatTypeEClass, ACFloatType.class, "ACFloatType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getACFloatType_Exponent(), this.getlong(), "exponent", null, 0, 1, ACFloatType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getACFloatType_Mantissa(), this.getlong(), "mantissa", null, 0, 1, ACFloatType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getACFloatType_Radix(), this.getlong(), "radix", null, 0, 1, ACFloatType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(acFloatTypeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypesVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(acFloatTypeEClass, this.getboolean(), "isEqual", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreConst", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreVolatile", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(acFloatTypeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(acComplexTypeEClass, ACComplexType.class, "ACComplexType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getACComplexType_BaseType(), this.getType(), null, "baseType", null, 0, 1, ACComplexType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(acComplexTypeEClass, this.getboolean(), "isEqual", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreConst", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreVolatile", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(acChannelTypeEClass, ACChannelType.class, "ACChannelType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getACChannelType_BaseType(), this.getType(), null, "baseType", null, 0, 1, ACChannelType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(acChannelTypeEClass, this.getboolean(), "isEqual", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getType(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreStorageClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreConst", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getboolean(), "ignoreVolatile", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(typesVisitorEClass, TypesVisitor.class, "TypesVisitor", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(typesVisitorEClass, null, "visitField", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getField(), "f", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(typesVisitorEClass, null, "visitArrayType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getArrayType(), "a", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(typesVisitorEClass, null, "visitFunctionType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getFunctionType(), "f", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(typesVisitorEClass, null, "visitPtrType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getPtrType(), "p", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(typesVisitorEClass, null, "visitRecordType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getRecordType(), "r", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(typesVisitorEClass, null, "visitACFixedType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getACFixedType(), "a", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(typesVisitorEClass, null, "visitACFloatType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getACFloatType(), "a", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(typesVisitorEClass, null, "visitACIntType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getACIntType(), "a", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(typesVisitorEClass, null, "visitACType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getACType(), "a", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(typesVisitorEClass, null, "visitAliasType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAliasType(), "a", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(typesVisitorEClass, null, "visitEnumType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEnumType(), "enumType", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(typesVisitorEClass, null, "visitEnumerator", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEnumerator(), "enumerator", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(typesVisitorEClass, null, "visitSimdType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSimdType(), "st", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(typesVisitorEClass, null, "visitVoidType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVoidType(), "b", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(typesVisitorEClass, null, "visitIntegerType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIntegerType(), "b", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(typesVisitorEClass, null, "visitBoolType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBoolType(), "b", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(typesVisitorEClass, null, "visitFloatType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getFloatType(), "b", 0, 1, !IS_UNIQUE, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(storageClassSpecifiersEEnum, StorageClassSpecifiers.class, "StorageClassSpecifiers");
		addEEnumLiteral(storageClassSpecifiersEEnum, StorageClassSpecifiers.NONE);
		addEEnumLiteral(storageClassSpecifiersEEnum, StorageClassSpecifiers.AUTO);
		addEEnumLiteral(storageClassSpecifiersEEnum, StorageClassSpecifiers.REGISTER);
		addEEnumLiteral(storageClassSpecifiersEEnum, StorageClassSpecifiers.STATIC);
		addEEnumLiteral(storageClassSpecifiersEEnum, StorageClassSpecifiers.EXTERN);

		initEEnum(kindsEEnum, Kinds.class, "Kinds");
		addEEnumLiteral(kindsEEnum, Kinds.STRUCT);
		addEEnumLiteral(kindsEEnum, Kinds.UNION);

		initEEnum(integerTypesEEnum, IntegerTypes.class, "IntegerTypes");
		addEEnumLiteral(integerTypesEEnum, IntegerTypes.CHAR);
		addEEnumLiteral(integerTypesEEnum, IntegerTypes.SHORT);
		addEEnumLiteral(integerTypesEEnum, IntegerTypes.INT);
		addEEnumLiteral(integerTypesEEnum, IntegerTypes.LONG);
		addEEnumLiteral(integerTypesEEnum, IntegerTypes.LONG_LONG);

		initEEnum(signModifiersEEnum, SignModifiers.class, "SignModifiers");
		addEEnumLiteral(signModifiersEEnum, SignModifiers.NONE);
		addEEnumLiteral(signModifiersEEnum, SignModifiers.SIGNED);
		addEEnumLiteral(signModifiersEEnum, SignModifiers.UNSIGNED);

		initEEnum(floatPrecisionsEEnum, FloatPrecisions.class, "FloatPrecisions");
		addEEnumLiteral(floatPrecisionsEEnum, FloatPrecisions.HALF);
		addEEnumLiteral(floatPrecisionsEEnum, FloatPrecisions.SINGLE);
		addEEnumLiteral(floatPrecisionsEEnum, FloatPrecisions.DOUBLE);
		addEEnumLiteral(floatPrecisionsEEnum, FloatPrecisions.LONG_DOUBLE);

		initEEnum(overflowModeEEnum, OverflowMode.class, "OverflowMode");
		addEEnumLiteral(overflowModeEEnum, OverflowMode.AC_WRAP);
		addEEnumLiteral(overflowModeEEnum, OverflowMode.AC_SAT_SYM);
		addEEnumLiteral(overflowModeEEnum, OverflowMode.AC_SAT_ZERO);
		addEEnumLiteral(overflowModeEEnum, OverflowMode.AC_SAT);

		initEEnum(quantificationModeEEnum, QuantificationMode.class, "QuantificationMode");
		addEEnumLiteral(quantificationModeEEnum, QuantificationMode.AC_TRN);
		addEEnumLiteral(quantificationModeEEnum, QuantificationMode.AC_TRN_ZERO);
		addEEnumLiteral(quantificationModeEEnum, QuantificationMode.AC_RND);
		addEEnumLiteral(quantificationModeEEnum, QuantificationMode.AC_RND_ZERO);
		addEEnumLiteral(quantificationModeEEnum, QuantificationMode.AC_RND_INF);
		addEEnumLiteral(quantificationModeEEnum, QuantificationMode.AC_RND_MIN_INF);
		addEEnumLiteral(quantificationModeEEnum, QuantificationMode.AC_RND_CONV);

		// Initialize data types
		initEDataType(stringEDataType, String.class, "String", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(longEDataType, long.class, "long", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(intEDataType, int.class, "int", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(booleanEDataType, boolean.class, "boolean", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
		   });
	}

} //TypesPackageImpl
