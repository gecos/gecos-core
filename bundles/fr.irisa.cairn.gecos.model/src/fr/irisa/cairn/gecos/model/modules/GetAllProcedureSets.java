package fr.irisa.cairn.gecos.model.modules;

import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;

import java.util.List;

import fr.irisa.r2d2.gecos.framework.GSModule;

@GSModule("Return: a list of all ProcedureSets in a given GecosProject.")
public class GetAllProcedureSets {

	private GecosProject src;
	
	public GetAllProcedureSets(GecosProject src) {
		this.src = src;
	}
	
	public List<ProcedureSet> compute() {
		return src.listProcedureSets();
	}
}
