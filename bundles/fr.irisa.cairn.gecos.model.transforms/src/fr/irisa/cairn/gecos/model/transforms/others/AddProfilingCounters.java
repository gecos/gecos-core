/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.transforms.others;

import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.Int;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.add;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.array;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.set;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.ARRAY;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.INT;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.setScope;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch;
import fr.irisa.r2d2.gecos.framework.GSModule;
import gecos.blocks.BasicBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.Instruction;

@GSModule("This pass instrument code with profiling oriented \n"
		+ "routine to measure the number of execution of each BB.")
public class AddProfilingCounters extends BlocksDefaultSwitch<Object> {

	private static Symbol __prof;
	private ProcedureSet procset;
	private Procedure current;

	/* this passes add profiling counters directly in the source code. This is is 
	 * achieved by creating a integer variable associated to each BasicBlock. The variable 
	 * is incremented whenever the BasicBlock is executed. */
	
	
	public AddProfilingCounters(ProcedureSet procset) {
		this.procset = procset;
	}

	public boolean compute() {
		for (Procedure proc : procset.listProcedures()) {
			current=proc;
			prepareSymbolTable(procset.getScope());
			doSwitch(proc.getBody());
		}
		return false;
	}

	/* I only have RangeInstruction to do array access
	 * it seems to be generic enough to match any case
	 */
	@Override
	public Object caseBasicBlock(BasicBlock b) {
		
		int indexOf = current.getBasicBlocks().indexOf(b);
		Instruction g = set(
				array(__prof,Int(indexOf)),
				add(array(__prof,Int(indexOf)),Int(1)),INT());
		
		b.prependInstruction(g);
		return null;
	}

	public  void prepareSymbolTable(Scope scope) {
		
		int nbBB = current.getBasicBlocks().size();
		String profileTableName = current.getSymbol().getName()+"__prof";
		setScope(current.getScope());
		__prof = GecosUserCoreFactory.symbol(profileTableName, ARRAY(INT(),nbBB), scope);
		scope.getSymbols().add(__prof);
	}
}
