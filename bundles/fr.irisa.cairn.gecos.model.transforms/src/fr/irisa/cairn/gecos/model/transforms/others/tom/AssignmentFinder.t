/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.transforms.others;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDefaultVisitor;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.core.Procedure;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;

/**
 * Find every written variable in a procedure.
 * 
 * @author llhours
 */
@SuppressWarnings("all")
public class AssignmentFinder extends GecosBlocksInstructionsDefaultVisitor {
 
	%include { sl.tom }

	%include { gecos_common.tom }
	%include { gecos_terminals.tom }
	%include { gecos_basic.tom }
   
	private final static boolean VERBOSE = false;

	private static void debug(String mess) {
		if(VERBOSE) System.out.println(mess);
	}
	private Map<Symbol, List<BasicBlock>> assignments;
	private BasicBlock currentBasic;

	private Procedure proc;
	private Block block;

	int mode; 
	
	public static final int ARRAYS =1;
	public static final int SCALARS=2;
	public static final int POINTERS=4;
 
	public AssignmentFinder(Procedure proc, int mode ) {
		this.proc = proc;
		this.mode= mode;
	}



	public AssignmentFinder() {
		this.mode=ARRAYS+SCALARS;
	}
 

	public AssignmentFinder(Block block, int mode ) {
		this.block = block;
		this.mode= mode;
	}
 
	public Map<Symbol, List<BasicBlock>> compute() {
		this.assignments = new LinkedHashMap<Symbol, List<BasicBlock>>();
		if (proc != null)
			proc.getBody().accept(this);
		else if (block != null)
			block.accept(this);
		return assignments;
	}
 
	public Map<Symbol, List<BasicBlock>> search(Block blk, int mode) {
		this.mode= mode;
		this.assignments = new LinkedHashMap<Symbol, List<BasicBlock>>();
		blk.accept(this);
		return assignments;
	}

	public Map<Symbol, List<BasicBlock>> search(Block blk) {
		this.assignments = new LinkedHashMap<Symbol, List<BasicBlock>>();
		blk.accept(this);
		return assignments;
	}

	@Override
	protected void visitInstruction(Instruction i) {
		debug("Falling back to default behavior for "+i+" of type "+i.getClass().getSimpleName());
	}
  
	@Override
	public void visitBasicBlock(BasicBlock b) {
		currentBasic = b;
		super.visitBasicBlock(b); 
	}
 
	@Override 
	public void visitSetInstruction(SetInstruction inst) {
		%match (Inst inst){
			a@set(symref(s),_) -> {
				debug("Scalar assignment to "+(`s)+" in "+(`a));
				addAssignment(`s, currentBasic);
			}
			a@set(array(sr@symref(s),InstL(index*)),_) -> {
				debug("Array cell assignment to "+(`s)+" in "+(`a));
				// We insert a "dummy" update instruction to capture 
				// may RAW dependency on array accesses.
				insertUpdateOperation(`sr,(SetInstruction) `a,`index);
				addAssignment(`s, currentBasic);
				debug("   changed to "+(`a));
			}
		}		
	} 

	private static void insertUpdateOperation(Instruction symref, SetInstruction s, EList<Instruction> index) {
		GenericInstruction newSrc = (GenericInstruction) `generic("update",InstL(symref.copy(),s.getSource().copy()));
		newSrc.getChildren().addAll(index);
		newSrc.setType(symref.getType());
		s.setType(symref.getType()); 
		s.setSource(newSrc);
		s.setDest(symref.copy());

	}
 
	private void addAssignment(Symbol symbol, BasicBlock b) { 
		List<BasicBlock> blocks = assignments.get(symbol);
		if (blocks == null)
			assignments.put(symbol, blocks = new ArrayList<BasicBlock>()); 
		blocks.add(b);
	}

} 