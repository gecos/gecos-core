package fr.irisa.cairn.gecos.model.transforms.symbols.removeunused.internal;

import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;

public abstract class AbstractGecosTransformModule {

	protected ProcedureSet ps;
	protected GecosProject project;

	protected boolean VERBOSE = true;

	protected void debug(String string) {
		if(VERBOSE) System.out.println("["+this.getClass().getSimpleName()+"] "+string);
	}
	
	public AbstractGecosTransformModule(ProcedureSet ps) {
		this.ps=ps;
	}

	public AbstractGecosTransformModule(GecosProject project) {
		this.project=project;
	}

	
	protected Object computeProcedure(Procedure proc) { return null; };

	protected void compute() {
		if (project==null) {
			computeProcedureSet();
		} else {
			computeGecosProject();
		}
	}

	protected void computeProcedureSet() {
		for(Procedure proc : ps.listProcedures()) {
			computeProcedure(proc);
		}
	}
	
	protected void computeGecosProject() {
		for(GecosSourceFile file : project.getSources()) {
			this.ps =file.getModel();
			computeProcedureSet();
		}
	}

}
