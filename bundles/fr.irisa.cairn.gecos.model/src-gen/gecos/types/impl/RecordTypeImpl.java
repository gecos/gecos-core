/**
 */
package gecos.types.impl;

import com.google.common.base.Objects;

import gecos.types.Field;
import gecos.types.Kinds;
import gecos.types.RecordType;
import gecos.types.Type;
import gecos.types.TypesPackage;
import gecos.types.TypesVisitor;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions;

import org.eclipse.xtext.xbase.lib.ExclusiveRange;

import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.Functions.Function2;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Record Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.impl.RecordTypeImpl#getFields <em>Fields</em>}</li>
 *   <li>{@link gecos.types.impl.RecordTypeImpl#getKind <em>Kind</em>}</li>
 *   <li>{@link gecos.types.impl.RecordTypeImpl#isDefined <em>Defined</em>}</li>
 *   <li>{@link gecos.types.impl.RecordTypeImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RecordTypeImpl extends TypeImpl implements RecordType {
	/**
	 * The cached value of the '{@link #getFields() <em>Fields</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFields()
	 * @generated
	 * @ordered
	 */
	protected EList<Field> fields;

	/**
	 * The default value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected static final Kinds KIND_EDEFAULT = Kinds.STRUCT;

	/**
	 * The cached value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected Kinds kind = KIND_EDEFAULT;

	/**
	 * The default value of the '{@link #isDefined() <em>Defined</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDefined()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DEFINED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDefined() <em>Defined</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDefined()
	 * @generated
	 * @ordered
	 */
	protected boolean defined = DEFINED_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = "anonymous";

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RecordTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypesPackage.Literals.RECORD_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Field> getFields() {
		if (fields == null) {
			fields = new EObjectContainmentWithInverseEList<Field>(Field.class, this, TypesPackage.RECORD_TYPE__FIELDS, TypesPackage.FIELD__RECORD);
		}
		return fields;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Kinds getKind() {
		return kind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKind(Kinds newKind) {
		Kinds oldKind = kind;
		kind = newKind == null ? KIND_EDEFAULT : newKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.RECORD_TYPE__KIND, oldKind, kind));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDefined() {
		return defined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefined(boolean newDefined) {
		boolean oldDefined = defined;
		defined = newDefined;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.RECORD_TYPE__DEFINED, oldDefined, defined));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.RECORD_TYPE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getSize() {
		Long _switchResult = null;
		Kinds _kind = this.getKind();
		if (_kind != null) {
			switch (_kind) {
				case UNION:
					final Function1<Field, Long> _function = new Function1<Field, Long>() {
						public Long apply(final Field it) {
							return Long.valueOf(it.getType().getSize());
						}
					};
					_switchResult = IterableExtensions.<Long>max(XcoreEListExtensions.<Field, Long>map(this.getFields(), _function));
					break;
				case STRUCT:
					final Function1<Field, Long> _function_1 = new Function1<Field, Long>() {
						public Long apply(final Field it) {
							long _xifexpression = (long) 0;
							long _bitwidth = it.getBitwidth();
							boolean _equals = (_bitwidth == (-1));
							if (_equals) {
								_xifexpression = it.getType().getSize();
							}
							else {
								_xifexpression = it.getBitwidth();
							}
							return Long.valueOf(_xifexpression);
						}
					};
					final Function2<Long, Long, Long> _function_2 = new Function2<Long, Long, Long>() {
						public Long apply(final Long s1, final Long s2) {
							return Long.valueOf(((s1).longValue() + (s2).longValue()));
						}
					};
					_switchResult = IterableExtensions.<Long>reduce(XcoreEListExtensions.<Field, Long>map(this.getFields(), _function_1), _function_2);
					break;
				default:
					throw new UnsupportedOperationException("Not implemented yet");
			}
		}
		else {
			throw new UnsupportedOperationException("Not implemented yet");
		}
		return (_switchResult).longValue();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Field findField(final String key) {
		final Function1<Field, Boolean> _function = new Function1<Field, Boolean>() {
			public Boolean apply(final Field it) {
				String _name = it.getName();
				return Boolean.valueOf(Objects.equal(_name, key));
			}
		};
		return IterableExtensions.<Field>findFirst(this.getFields(), _function);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final TypesVisitor visitor) {
		visitor.visitRecordType(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEqual(final Type other, final boolean ignoreStorageClass, final boolean ignoreConst, final boolean ignoreVolatile) {
		if ((other == this)) {
			return true;
		}
		if ((other instanceof RecordType)) {
			final RecordType rtype = ((RecordType)other);
			int _size = this.getFields().size();
			int _size_1 = rtype.getFields().size();
			boolean _tripleEquals = (_size == _size_1);
			if (_tripleEquals) {
				int _size_2 = this.getFields().size();
				ExclusiveRange _doubleDotLessThan = new ExclusiveRange(0, _size_2, true);
				for (final Integer i : _doubleDotLessThan) {
					{
						Field _get = rtype.getFields().get((i).intValue());
						boolean _tripleNotEquals = (_get != null);
						Field _get_1 = this.getFields().get((i).intValue());
						boolean _tripleNotEquals_1 = (_get_1 != null);
						boolean _xor = (_tripleNotEquals ^ _tripleNotEquals_1);
						if (_xor) {
							return false;
						}
						if (((rtype.getFields().get((i).intValue()) != null) && (this.getFields().get((i).intValue()) != null))) {
							boolean _isEqual = rtype.getFields().get((i).intValue()).isEqual(this.getFields().get((i).intValue()));
							boolean _not = (!_isEqual);
							if (_not) {
								return false;
							}
						}
					}
				}
			}
			return (((this.getKind().equals(rtype.getKind()) && (Boolean.valueOf(this.isDefined()) == Boolean.valueOf(rtype.isDefined()))) && this.getName().equals(rtype.getName())) && super.isEqual(rtype, ignoreStorageClass, ignoreConst, ignoreVolatile));
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TypesPackage.RECORD_TYPE__FIELDS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getFields()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TypesPackage.RECORD_TYPE__FIELDS:
				return ((InternalEList<?>)getFields()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TypesPackage.RECORD_TYPE__FIELDS:
				return getFields();
			case TypesPackage.RECORD_TYPE__KIND:
				return getKind();
			case TypesPackage.RECORD_TYPE__DEFINED:
				return isDefined();
			case TypesPackage.RECORD_TYPE__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TypesPackage.RECORD_TYPE__FIELDS:
				getFields().clear();
				getFields().addAll((Collection<? extends Field>)newValue);
				return;
			case TypesPackage.RECORD_TYPE__KIND:
				setKind((Kinds)newValue);
				return;
			case TypesPackage.RECORD_TYPE__DEFINED:
				setDefined((Boolean)newValue);
				return;
			case TypesPackage.RECORD_TYPE__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TypesPackage.RECORD_TYPE__FIELDS:
				getFields().clear();
				return;
			case TypesPackage.RECORD_TYPE__KIND:
				setKind(KIND_EDEFAULT);
				return;
			case TypesPackage.RECORD_TYPE__DEFINED:
				setDefined(DEFINED_EDEFAULT);
				return;
			case TypesPackage.RECORD_TYPE__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TypesPackage.RECORD_TYPE__FIELDS:
				return fields != null && !fields.isEmpty();
			case TypesPackage.RECORD_TYPE__KIND:
				return kind != KIND_EDEFAULT;
			case TypesPackage.RECORD_TYPE__DEFINED:
				return defined != DEFINED_EDEFAULT;
			case TypesPackage.RECORD_TYPE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (kind: ");
		result.append(kind);
		result.append(", defined: ");
		result.append(defined);
		result.append(", name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //RecordTypeImpl
