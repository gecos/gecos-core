/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.analysis.dataflow.liveness;

import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.analysis.dataflow.framework.AbstractLivenessAnalyser;
import fr.irisa.cairn.gecos.model.analysis.dataflow.framework.BooleanLattice;
import fr.irisa.cairn.gecos.model.tools.switches.DefaultInstructionSwitch;
import gecos.annotations.AnnotationsFactory;
import gecos.annotations.LivenessAnnotation;
import gecos.annotations.StringAnnotation;
import gecos.blocks.BasicBlock;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.Symbol;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;

/**
 * LivenessAnalyser. Compute the liveness of procedure variables using the
 * BackwardAnalyser.
 * 
 * For each block b : - DEFINED(b) is the set of (re)defined variables by 
 * block b, - USED(b) is the set of variables used in block b.
 * 
 * Living variables at the end of a block b are : v_new(b) = \sum_{n\in
 * succ(b)}v_old(b)+USED(n)-DEFINED(n)
 * 
 * As we are more interested in living variables at the beginning of the blocks,
 * the final results are computed after the iterative algorithm stop.
 */

public class LivenessAnalyser extends AbstractLivenessAnalyser {

	public static final String USED_ANNOTATION = "Used";
	public static final String DEFINED_ANNOTATION = "Defined";
	public static final String LIVEIN_ANNOTATION = "Livein";
	public static final String LIVEOUT_ANNOTATION = "Liveout";
	private static final boolean VERBOSE = false;
	
	private Map<BasicBlock, BooleanLattice<Symbol>> livein;
	private Map<BasicBlock, BooleanLattice<Symbol>> liveout;
	private boolean enableAnnotate = true;

	public LivenessAnalyser(Procedure proc, Symbol[] variables) {
		//initializes 
		// - proc : procedure to process
		// - workList : list of basic blocks to process
		// - values : Map<BasicBlock,BooleanLattice> (for each basic block, the lattice tells if a variable is live)
		// - super.variables (list of all variables used in the procedure) 
		// - super.used : Map<BasicBlock,BooleanLattice> (for each basic block, the lattice tells if a variable is used)
		// - super.defined : Map<BasicBlock,BooleanLattice>  (for each basic block, the lattice tells if a variable is defined)
		super(proc, variables);
	}

	public void disableAnnotate() {
		enableAnnotate = false;
	}
	
	public void compute() {

		EList<BasicBlock> basicBlocks = procedure().getBasicBlocks();
		
		if (isDebug()) {
			for (BasicBlock bb : basicBlocks) {
				debug("Liveness for BB"+bb.getNumber());
				debug("	at output :"+getLattice(bb));
			}
		}
		
		super.compute();

		livein = new LinkedHashMap<BasicBlock, BooleanLattice<Symbol>>();
		liveout = new LinkedHashMap<BasicBlock, BooleanLattice<Symbol>>();

		for (BasicBlock bb : basicBlocks) {
			if (isDebug()) {
				debug("Liveness for BB"+bb.getNumber());
				debug(" at output :"+getLattice(bb));
			}
			liveout.put(bb, (BooleanLattice<Symbol>) getLattice(bb));
			EList<BasicBlock> predecessors = bb.getPredecessors();
			BooleanLattice<Symbol> res = new BooleanLattice<Symbol>(variables, !predecessors.isEmpty());
			for (BasicBlock pred : predecessors) {
				BooleanLattice<Symbol> res2  = (BooleanLattice<Symbol>) res.and(getLattice(pred));
				res=res2;
			}
			if (isDebug())
				debug(" at input : "+res+"\n");
			livein.put(bb, res);
		}
		
		if(VERBOSE) print();
		
		if (enableAnnotate)
			annotate();		
	}

	public BooleanLattice<Symbol> getResultAt(BasicBlock b) {
		return (BooleanLattice<Symbol>) livein.get(b);
	}

	public void annotate() {
		EList<BasicBlock> basicBlocks = procedure().getBasicBlocks();
		
		for (BasicBlock bb : basicBlocks) {
			LivenessAnnotation annotation = AnnotationsFactory.eINSTANCE.createLivenessAnnotation();
			annotation.getRefs().addAll(getResultAt(bb).getTrueResults());
			bb.getAnnotations().put(LIVEIN_ANNOTATION, annotation);
		}

		for (BasicBlock bb : basicBlocks) {
			LivenessAnnotation annotation = AnnotationsFactory.eINSTANCE.createLivenessAnnotation();
			annotation.getRefs().addAll(liveout.get(bb).getTrueResults());
			bb.getAnnotations().put(LIVEOUT_ANNOTATION, annotation);
		}
		
		for (BasicBlock bb : basicBlocks) {
			StringAnnotation annotation = AnnotationsFactory.eINSTANCE.createStringAnnotation();
			annotation.setContent(defined.get(bb).toString());
			bb.getAnnotations().put(DEFINED_ANNOTATION, annotation);
		}
		
		for (BasicBlock bb : basicBlocks) {
			StringAnnotation annotation = AnnotationsFactory.eINSTANCE.createStringAnnotation();
			annotation.setContent(used.get(bb).toString());
			bb.getAnnotations().put(USED_ANNOTATION, annotation);
		}
	}

	public void print() {
		System.out.println("== Liveness analysis for procedure "+procedure().getSymbol().getName());
		for (BasicBlock bb : procedure().getBasicBlocks()) {
			System.out.println("\tBB" +bb.getNumber() +" " + getResultAt(bb));
		}
	}

	private static class DefVisitor extends DefaultInstructionSwitch<Object> {
		private boolean isWrite = false;
		BooleanLattice bl;
		
		public DefVisitor(BooleanLattice bl) {
			this.bl=bl;
		}
		
		public void run(Instruction inst) {
			doSwitch(inst);
		}
		@Override
		public Object caseSetInstruction(SetInstruction set) {
			isWrite = true;
			doSwitch(set.getDest());
			isWrite = false;
			doSwitch(set.getSource());
			return this;
		}

		@Override
		public Object caseGenericInstruction(GenericInstruction g) {
			isWrite = false;
			return super.caseGenericInstruction(g);
		}

		@Override
		public Object caseSymbolInstruction(SymbolInstruction sym) {
			if (isWrite)
				bl.setValue(sym.getSymbol(), true);
			return this;
		}
	}

	@Override
	protected BooleanLattice computeDefined(BasicBlock b) {
		BooleanLattice def = new BooleanLattice(variables);
		for (int i = b.getInstructions().size() - 1; i >= 0; --i) {
			Instruction inst = b.getInstructions().get(i);
			DefVisitor visitor = new DefVisitor(def);
			visitor.run(inst);
			//debug("Defined => "+def);
		}
		if (isDebug()) 
			debug("Defined in BB"+b.getNumber()+" => "+def);
		return def;
	}

	private static class UseAddVisitor extends DefaultInstructionSwitch<Object> {
		private boolean isUse = true;
		BooleanLattice use ;

		public UseAddVisitor( BooleanLattice use ) {
			this.use=use;
		}
		
		@Override
		public Object caseSetInstruction(SetInstruction set) {
			doSwitch(set.getSource());
			isUse = false;
			doSwitch(set.getDest());
			return this;
		}

		@Override
		public Object caseGenericInstruction(GenericInstruction g) {
			isUse = true;
			return super.caseGenericInstruction(g);
		}

		@Override
		public Object caseSymbolInstruction(SymbolInstruction sym) {
			if (isUse)
				use.setValue(sym.getSymbol(), true);
			return this;
		}
	}

	private static class UseRemoveVisitor extends DefaultInstructionSwitch<Object> {

		
		BooleanLattice bl;
		
		public UseRemoveVisitor(BooleanLattice bl) {
			this.bl= bl;
			
		}
		@Override
		public Object caseSetInstruction(SetInstruction set) {
			doSwitch(set.getDest());
			return this;
		}


		@Override
		public Object caseGenericInstruction(GenericInstruction g) {
			return null;
		}

		@Override
		public Object caseSymbolInstruction(SymbolInstruction sym) {
			bl.setValue(sym.getSymbol(), false);
			return this;
		}
	}

	@Override
	protected BooleanLattice computeUsed(BasicBlock b) {
		BooleanLattice use = new BooleanLattice(variables);
		for (int i = b.getInstructions().size()- 1; i >= 0; --i) {
			Instruction inst = b.getInstructions().get(i);
			
			new UseAddVisitor(use).doSwitch(inst);
			
			if (i != 0) {
				inst = b.getInstructions().get(i - 1);
				new UseRemoveVisitor(use).doSwitch(inst);
			}
		}
		if (isDebug())
			debug("Used in BB"+b.getNumber()+" => "+use);
		return use;
	}

	@Override
	protected void computeDefined(Procedure proc) {
		// All parameters are defined at the function entry point
		BasicBlock start = proc.getStart();
		for (ParameterSymbol sym: proc.listParameters()) {
			defined.get(start).setValue(sym, true);
		}
	}

	@Override
	protected void computeUsed(Procedure proc) {
		// Reference parameters may be used after the end of the procedure
		BasicBlock end = proc.getEnd();
		for (ParameterSymbol sym: proc.listParameters()) {
			if (sym.getType().isArray() || sym.getType().isPointer()) {
				used.get(end).setValue(sym, true);				
			}
		}
	}
	
//	protected ILattice compute(BasicBlock b, ILattice out) {
//		return getUsed(b).or(out.and(getDefined(b).not()));
//	}
}
