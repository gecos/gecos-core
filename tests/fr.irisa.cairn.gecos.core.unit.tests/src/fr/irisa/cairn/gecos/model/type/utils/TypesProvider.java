package fr.irisa.cairn.gecos.model.type.utils;

import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.ARRAY;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.BOOL;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.CHAR;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.DOUBLE;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.FLOAT;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.FUNCTION;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.INT;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.LONG;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.LONGDOUBLE;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.LONGLONG;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.PTR;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.SHORT;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.VOID;
import static fr.irisa.cairn.gecos.model.type.utils.Utils.CDECL;
import static fr.irisa.cairn.gecos.model.type.utils.Utils.SDECL;
import static fr.irisa.cairn.gecos.model.type.utils.Utils.SPEC;
import static fr.irisa.cairn.gecos.model.type.utils.Utils.TD;
import static fr.irisa.cairn.gecos.model.type.utils.Utils.cnst;
import static fr.irisa.cairn.gecos.model.type.utils.Utils.ext;
import static fr.irisa.cairn.gecos.model.type.utils.Utils.merge;
import static fr.irisa.cairn.gecos.model.type.utils.Utils.modifyDecl;
import static fr.irisa.cairn.gecos.model.type.utils.Utils.modifySpec;
import static fr.irisa.cairn.gecos.model.type.utils.Utils.modify;
import static fr.irisa.cairn.gecos.model.type.utils.Utils.randomSelect;
import static fr.irisa.cairn.gecos.model.type.utils.Utils.reg;
import static fr.irisa.cairn.gecos.model.type.utils.Utils.restrct;
import static fr.irisa.cairn.gecos.model.type.utils.Utils.sign;
import static fr.irisa.cairn.gecos.model.type.utils.Utils.sttc;
import static fr.irisa.cairn.gecos.model.type.utils.Utils.toRaw;
import static fr.irisa.cairn.gecos.model.type.utils.Utils.unsign;
import static fr.irisa.cairn.gecos.model.type.utils.Utils.voltile;
import static fr.irisa.cairn.gecos.model.type.utils.Utils.inline;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import com.tngtech.java.junit.dataprovider.DataProvider;

import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import gecos.types.Type;

public class TypesProvider {

	static {
		GecosUserTypeFactory.setScope(GecosUserCoreFactory.scope());
	}
	
/* Declaration Specifiers -----------------------------------------------------------------------------------------
 * 
 * DECL_SPEC = [STOR | SPEC | QUAL]+
 * 
 *    STOR (storage class specifier) = [extern | static | auto | register | typedef]
 *    SPEC (type specifier) = [void | char | short | int | long | float | double | signed | unsigned | ...(union, struct ...) ]
 *    QUAL (type qualifier) = [const | volatile]
 * ---------------------------------------------------------------------------------------------------------------*/
	
	static List<TypeData> _integerSpec = Arrays.asList(
		TD("char"        , CHAR()      ),
		TD("short"       , SHORT()     ),
		TD("int"         , INT()       ),
		TD("long"        , LONG()      ),
		TD("long long"   , LONGLONG()  )
		//NOTE: Ignoring the following since they represent the same gecos.Type.
//		TD("short int"    , SHORT()    ),
//		TD("long int"     , LONG()     ),
//		TD("long long int", LONGLONG() )
	);
	
	static List<TypeData> _floatSpec = Arrays.asList(
		TD("float"       , FLOAT()     ),
		TD("double"      , DOUBLE()    ),
		TD("long double" , LONGDOUBLE())
	);
	
	static List<TypeData> _otherSpec = Arrays.asList(
		TD("_Bool"       , BOOL()      ),
		TD("void"        , VOID()      )
	);
	
	private static List<TypeData> makeSignModifiers(List<TypeData> baseData) {
		return merge(
			modifySpec(baseData, s -> SPEC("signed "  +s), t -> sign(t.copy())),
			modifySpec(baseData, s -> SPEC("unsigned "+s), t -> unsign(t.copy()))
		);
	}
	
	private static List<TypeData> makeQualifiers(List<TypeData> baseData) {
		return merge(
			modifySpec(baseData, s -> SPEC("const "         +s), t -> cnst(t.copy())),
			modifySpec(baseData, s -> SPEC("volatile "      +s), t -> voltile(t.copy())),
			modifySpec(baseData, s -> SPEC("const volatile "+s), t -> cnst(voltile(t.copy())))
		);
	}
	
	private static List<TypeData> makeStorageClassSpecifiers(List<TypeData> baseData, boolean useStatic, boolean useExtern, boolean useRegstr/*, boolean useAuto*/) {
		List<List<TypeData>> array = new ArrayList<>(4);
		if(useStatic) array.add(modifySpec(baseData, s -> SPEC("static "  +s), t -> sttc(t.copy())));
		if(useExtern) array.add(modifySpec(baseData, s -> SPEC("extern "  +s), t -> ext (t.copy())));
		if(useRegstr) array.add(modifySpec(baseData, s -> SPEC("register "+s), t -> reg (t.copy())));
		//NOTE: Ignoring the following since the tested type is generated in the scope of a procedureSet (i.e. global variable) which makes such declaration invalid
//		if(useAuto) array.add(modifySpec(baseData, s -> SPEC("auto "    +s), t -> auto(t.copy())));
		//missing 'typedef'
		
		return merge(array);
	}
	
	/** all type specifiers (20) **/
	final static List<TypeData> basicAllSpec = merge(
		_integerSpec,
		makeSignModifiers(_integerSpec),
		_floatSpec,
		_otherSpec
	);
	
	/** all type specifiers with all type qualifiers (80) **/
	final static List<TypeData> basicAllSpecQual = merge(
		basicAllSpec,
		makeQualifiers(basicAllSpec)
	);
	
	/** all declaration specifiers (400): null 
	 * 
	 * The order is important: it should match the order used by the code generator:
	 *  [Storage_class] [const] [volatile] [sign] base
	 */
	final static List<TypeData> basicAllSpecQualStor = merge(
		basicAllSpecQual,
		makeStorageClassSpecifiers(basicAllSpecQual, true, true, true)
	);
	
	final static List<TypeData> basicSpecQualStorNoReg = merge(
		basicAllSpecQual,
		makeStorageClassSpecifiers(basicAllSpecQual, true, true, false)
	);
	
	
	
/* Declarators ------------------------------------------------------------------------------------------------
 * 
 * DECL = [POINTER] identifier | (DECL) [ '['[const-expr]']' | '('[params | identifiers]')' ]*
 * ---------------------------------------------------------------------------------------------------------- */
	public static final String _ID= "a";
	
	
/* Declarations -----------------------------------------------------------------------------------------------
 * 
 * declaration = [DECL_SPEC]+ DECL
 * 
 * NOTES: the following declarations are NOT valid but syntactically possible:
 * 	- function that returns an array or a function.
 *  - array of functions.
 *  - function return type, or global variable with Storage class 'register' or 'auto'.
 *  - function parameter with Storage class 'auto', 'extern' or 'static'
 */
	
	private static int ARRAY_SIZE = 5;
	
	private static final List<FParam> FPARAMS = Arrays.asList(
		new FParam(false, ""                            , new Type[] { }),
		new FParam(false, "int"                         , new Type[] { INT() }),
		new FParam(false, "int, double"                 , new Type[] { INT(), DOUBLE() }),
		new FParam(false, "int["+ARRAY_SIZE+"], double*", new Type[] { ARRAY(INT(), ARRAY_SIZE), PTR(DOUBLE()) }),
		
//		new FParam(true , "..."                              , new Type[] { }), //error: ISO C requires a named parameter before '...'
		new FParam(true , "int, ..."                         , new Type[] { INT() }),
		new FParam(true , "int, double, ..."                 , new Type[] { INT(), DOUBLE() }),
		new FParam(true , "int["+ARRAY_SIZE+"], double*, ...", new Type[] { ARRAY(INT(), ARRAY_SIZE), PTR(DOUBLE()) }),
		
		new FParam(true, "int["+ARRAY_SIZE+"]["+(ARRAY_SIZE+1)+"], double*(int, ...), ...", 
				new Type[] { ARRAY(ARRAY(INT(), ARRAY_SIZE+1), ARRAY_SIZE), 
							FUNCTION(PTR(DOUBLE()), true, INT()) })
	);
	
	private static class FParam {
		Type[] types;
		String code;
		boolean varArgs;
		FParam(boolean hasVarArgs, String ccode, Type[] types) {
			this.varArgs = hasVarArgs;
			this.code = ccode;
			this.types = types;
		}
	}
	
	private static List<TypeData> makeSimpleDeclarations(List<TypeData> specifiers) {
		return modifyDecl(specifiers, d -> SDECL("", _ID, ""), t -> t);
	}
	
	private static List<TypeData> makeSimplePtrDeclarations(List<TypeData> declarations) {
		return merge(//--------------------------- inner -------- outer -------------------------------- outer--inner----------
			modifyDecl(declarations, d -> SDECL(catPtr(d, "*"        ), _ID, d.getArraysOrFunction()), t -> PTR(t.copy())),
			modifyDecl(declarations, d -> SDECL(catPtr(d, "*const"   ), _ID, d.getArraysOrFunction()), t -> cnst(PTR(t.copy()))),
			modifyDecl(declarations, d -> SDECL(catPtr(d, "*volatile"), _ID, d.getArraysOrFunction()), t -> voltile(PTR(t.copy()))),
			modifyDecl(declarations, d -> SDECL(catPtr(d, "*restrict"), _ID, d.getArraysOrFunction()), t -> restrct(PTR(t.copy()))),
			modifyDecl(declarations, d -> SDECL(catPtr(d, "*restrict const volatile"), _ID, d.getArraysOrFunction()), 
					t -> restrct(cnst(voltile(PTR(t.copy())))))
		);
	}
	private static String catPtr(Declarator d, String newPointers) {
		String pointers = d.getPointers();
		if(pointers.isEmpty())
			return newPointers;
		return pointers + " " + newPointers;
	}
	
	private static List<TypeData> makeSimpleAryDeclarations(List<TypeData> declarations) {
		ARRAY_SIZE++;
		return //----------------------------------------------------- outer ----------- inner ------------- outer--inner----------
			modifyDecl(declarations, d -> SDECL(d.getPointers(), _ID, "[" + ARRAY_SIZE + "]" + d.getArraysOrFunction()), t -> ARRAY(t.copy(), ARRAY_SIZE))
			//array declaration with no size requires an initializer
		;
	}
	
	private static List<TypeData> makeSimpleFncDeclarations(List<TypeData> declarations, boolean addInline) {
		Stream<List<TypeData>> datas;
		if(addInline)
			datas = FPARAMS.stream().map(p -> modify(declarations,
					s -> SPEC("inline "  +s),
					d -> SDECL(d.getPointers(), _ID, "(" + p.code + ")"), 
					t -> inline(FUNCTION(t.copy(), p.varArgs, p.types))));
		else
			datas = FPARAMS.stream().map(p -> modifyDecl(declarations, 
					d -> SDECL(d.getPointers(), _ID, "(" + p.code + ")"), 
					t -> FUNCTION(t.copy(), p.varArgs, p.types)));
		
		return merge(datas.collect(toList()));
	}
	
	/**
	 * int *a[1]: array of pointer
	 * int (*a)[1] : pointer to array
	 */
	private static List<TypeData> makeComplexPtrDeclarations(List<TypeData> declarations) {
		return merge(//--------------------------- inner -------- outer -------------------------------- outer--inner----------
			modifyDecl(declarations, d -> CDECL("*"         , d, ""), t -> PTR(t.copy())),
			modifyDecl(declarations, d -> CDECL("*const"    , d, ""), t -> cnst(PTR(t.copy()))),
			modifyDecl(declarations, d -> CDECL("*volatile" , d, ""), t -> voltile(PTR(t.copy()))),
			modifyDecl(declarations, d -> CDECL("*restrict" , d, ""), t -> restrct(PTR(t.copy()))),
			modifyDecl(declarations, d -> CDECL("*restrict const volatile", d, ""), 
					t -> restrct(cnst(voltile(PTR(t.copy())))))
		);
	}
	
	private static List<TypeData> makeComplexAryDeclarations(List<TypeData> declarations) {
		ARRAY_SIZE++;
		return //----------------------------------------------------- outer ----------- inner ------------- outer--inner----------
			modifyDecl(declarations, d -> CDECL("", d, "[" + ARRAY_SIZE + "]"), t -> ARRAY(t.copy(), ARRAY_SIZE))
			//array declaration with no size requires an initializer
		;
	}
	
	private static List<TypeData> makeComplexFncDeclarations(List<TypeData> declarations) {
		return merge(FPARAMS.stream().map(p ->
			modifyDecl(declarations, d -> CDECL("", d, "(" + p.code + ")"), t -> FUNCTION(t.copy(), p.varArgs, p.types))
		).collect(toList()));
	}
	
	
	enum TypeProvidersSize {
		XXSMALL(10),
		XSMALL (25),
		SMALL  (50), 
		MEDIUM (100),
		LARGE  (200),
		XLARGE (300),
		XXLARGE(400),
		ALL(-1);
		
		int nBasic, nPtr, nAry, nFnc;
		TypeProvidersSize(int n){
			this.nBasic = n;
			this.nPtr = n;
			this.nAry = n;
			this.nFnc = n;
		}
	}
	
	private static final TypeProvidersSize SIZE = TypeTestTemplate.SIZE;
	
	@FunctionalInterface
	interface TDTransform extends Function<List<TypeData>, List<TypeData>> {}
	
	private static List<TypeData> createFrom(List<TypeData> base, TDTransform... functions) {
		for(TDTransform f : functions)
			base = f.apply(base);
		return base;
	}
	
	private static List<TypeData> makeBasic() {
		return createFrom(randomSelect(basicAllSpecQualStor, SIZE.nBasic), b -> makeSimpleDeclarations(b));
	}
	
	private static List<TypeData> makeBasicNoReg() {
		return createFrom(randomSelect(basicSpecQualStorNoReg, SIZE.nBasic), b -> makeSimpleDeclarations(b));
	}
	
	private static List<TypeData> makeSPtr(List<TypeData> base) {
		return createFrom(base, b -> randomSelect(makeSimplePtrDeclarations(b), SIZE.nBasic));
	}
	
	private static List<TypeData> makeSAry(List<TypeData> base) {
		return createFrom(base, b -> randomSelect(makeSimpleAryDeclarations(b), SIZE.nAry));
	}
	
	private static List<TypeData> makeSFnc(List<TypeData> base) {
		return createFrom(base, b -> randomSelect(makeSimpleFncDeclarations(b, false), SIZE.nFnc));
	}
	
	private static List<TypeData> makeSInlineFnc(List<TypeData> base) {
		return createFrom(base, b -> randomSelect(makeSimpleFncDeclarations(b, true), SIZE.nFnc));
	}
	
	private static List<TypeData> makeCPtr(List<TypeData> base) {
		return createFrom(base, b -> randomSelect(makeComplexPtrDeclarations(b), SIZE.nPtr));
	}
	
	private static List<TypeData> makeCAry(List<TypeData> base) {
		return createFrom(base, b -> randomSelect(makeComplexAryDeclarations(b), SIZE.nAry));
	}
	
	private static List<TypeData> makeCFnc(List<TypeData> base) {
		return createFrom(base, b -> randomSelect(makeComplexFncDeclarations(b), SIZE.nFnc));
	}
	
	
// Data providers ---------------------------------------------------------------------------------------------

	/**
	 * provide all declaration specifiers (storage class specifier, 
	 * type specifier, type qualifier) with a simple (identifier) declarator.
	 */
	@DataProvider
	public static Object[][] provideBasic() {
		return toRaw(makeBasic());
	}
	
	@DataProvider
	public static Object[][] providePtr() {
		return toRaw(makeSPtr(makeBasic()));
	}
	
	@DataProvider
	public static Object[][] provideAry() {
		return toRaw(makeSAry(makeBasic()));
	}
	
	@DataProvider
	public static Object[][] providePtrPtr() {
		return toRaw(makeSPtr(makeSPtr(makeBasic())));
	}
	
	@DataProvider
	public static Object[][] provideAryAry() {
		return toRaw(makeSAry(makeSAry(makeBasic())));
//		return toRaw(makeCAry(makeSAry(makeBasic()))); //equivalent
	}

	@DataProvider
	public static Object[][] provideAryPtr() {
		return toRaw(makeSAry(makeSPtr(makeBasic())));
//		return toRaw(makeCAry(makeSPtr(makeBasic()))); //equivalent
	}
	
	@DataProvider
	public static Object[][] providePtrAry() {
		return toRaw(makeCPtr(makeSAry(makeBasic())));
	}
	
	@DataProvider
	public static Object[][] provideAryPtrAry() {
		return toRaw(makeCAry(makeCPtr(makeSAry(makeBasic()))));
	}
	
	@DataProvider
	public static Object[][] provideAryAryPtrAryPtrAry() {
		return toRaw(makeCAry(makeCAry(makeCPtr(makeCAry(makeCPtr(makeSAry(makeBasic())))))));
	}
	
	@DataProvider
	public static Object[][] provideFnc() {
		return toRaw(merge(
				makeSFnc(makeBasicNoReg()),
				makeSInlineFnc(makeBasicNoReg())
			));
	}
	
	@DataProvider
	public static Object[][] providePtrFnc() {
		return toRaw(makeCPtr(makeSFnc(makeBasicNoReg())));
	}
	
	@DataProvider
	public static Object[][] provideFncPtr() {
		return toRaw(makeCFnc(makeSPtr(makeBasicNoReg())));
	}
	
	@DataProvider
	public static Object[][] provideFncPtrFnc() {
		return toRaw(makeCFnc(makeCPtr(makeSFnc(makeBasicNoReg()))));
	}
	
	@DataProvider
	public static Object[][] providePtrFncPtrAryAry() {
		return toRaw(makeCPtr(makeCFnc(makeCPtr(makeCAry(makeSAry(makeBasicNoReg()))))));
	}
	
	@DataProvider
	public static Object[][] providePtrPtrFncPtrPtrAryAryPtrFncPtrPtrAryAryPtr() {
		return toRaw(makeCPtr(makeCPtr(makeCFnc(makeCPtr(makeCPtr(makeCAry(makeCAry(makeCPtr(makeCFnc(makeCPtr(makeCPtr(makeCAry(makeCAry(makeSPtr(makeBasicNoReg())))))))))))))));
	}
	
}
