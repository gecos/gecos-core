package fr.irisa.cairn.gecos.core.profiling.backend;

import static fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION;
import static fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION;
import static fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory.pragma;
import static fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory.procSymbol;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.VOID;
import static fr.irisa.r2d2.gecos.framework.utils.AbstractShell.outToStream;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.joining;

import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Stream;

import com.google.common.base.Throwables;
import com.google.common.collect.Streams;

import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.utils.misc.StreamUtils;
import fr.irisa.r2d2.gecos.framework.utils.AbstractShell;
import fr.irisa.r2d2.gecos.framework.utils.FileUtils;
import fr.irisa.r2d2.gecos.framework.utils.SystemExec;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.Symbol;

/**
 * @author aelmouss
 */
public class NativeProfilingEngineSupport {
	
	private NativeProfilingEngineSupport() {}

	public static String CC = "g++";
	public static String EXTRA_CFLAGS = "";
	
	//----------------------------------------------------------
	// Native Profiling Engine resources
	//----------------------------------------------------------
	
	private static final Path RESOURCES_LOCATION = Paths.get(
			NativeProfilingEngineSupport.class.getProtectionDomain().getCodeSource().getLocation().getPath()).resolve("resources");
	
//	private static final Path RESOURCES_LOCATION = Paths.get("/Users/tyuki/projects/GeCoS/gecos-core/bundles/fr.irisa.cairn.gecos.core.profiling/resources") ;
//			//new ResourceLocator(NativeProfilingEngineSupport.class, "resources").locate("resources");
	
	private static final Path PROF_ENGINE_LOCATION = RESOURCES_LOCATION.resolve("ProfilingEngine_double");

	static final Path PROF_ENGINE_LIB_DIR  = PROF_ENGINE_LOCATION.resolve("bin");
	static final Path PROF_ENGINE_BUILD_DIR = PROF_ENGINE_LOCATION.resolve("build");
	static final Path PROF_ENGINE_INC_DIR  = PROF_ENGINE_LOCATION.resolve("include");
	static final Path PROF_ENGINE_ALLIN1_INC_DIR  = PROF_ENGINE_LOCATION.resolve("include_all-in-one"); //! only if PROF_ENGINE_LOCATION is 'ProfilingEngine'
	
	static final String PROF_ENGINE_LIB_NAME = "profilingengine";
	static final String PROF_ENGINE_HEADER = "ProfilingEngine.h";
	static final String PROF_ENGINE_LIB_FUNCTIONS_NAME_PREFIX = "ProfilingEngine_";
	
	// Backend Deserializer
	static final String BACKEND_COMPILED_EXEC_NAME = "executable";
	static final String BACKEND_PROF_FILES_OUTPUT_DIRNAME = "profiling-files/";
	
	// -- Txt format
	static final String BACKEND_PROF_FILE_TXT_EXT = ".txt";
	static final int    BACKEND_PROF_FILE_TXT_NB_HEADER_LINES = 5;
	
	// -- Bin format
	static final String BACKEND_PROF_FILE_BIN_EXT = ".bin";
	
	//----------------------------------------------------------
	// Native Profiling Engine API
	//----------------------------------------------------------
	
	public static final boolean BACKEND_PROF_FILE_USE_BIN = true;
	
	static final String FILE_EXT = BACKEND_PROF_FILE_USE_BIN ? BACKEND_PROF_FILE_BIN_EXT : BACKEND_PROF_FILE_TXT_EXT;
	
	/**
	 * If false: link against precompiled lib
	 * If true: use all-in-one header (compilation takes longer)
	 */
	public static final boolean USE_ALLIN1 = false; //! true is only supported if PROF_ENGINE_LOCATION is 'ProfilingEngine'
	
	public enum NativeFunction {
		START("start"),
		START_SEEDED("start_seeded"),
		FINALIZE("finalize"),
		
		PUSH_VALUE("pushValue"),
		SAVE_SNAPSHOT("saveSnapshot"),
		
		INJECTOR_FROM_VAR("initInjector_fromVar"),
		INJECTOR_FROM_FILE("initInjector_fromFile"),
		
		INJECTOR_RANDOM_SET_SEED("initInjector_RandomSeed"),
		INJECTOR_RANDOM_UNIFORM_INT("initInjector_RandomUniformInt"),
		INJECTOR_RANDOM_UNIFORN_DOUBLE("initInjector_RandomUniformDouble"),
		INJECTOR_RANDOM_NORMAL_INT("initInjector_RandomNormalInt"),
		INJECTOR_RANDOM_NORMAL_DOUBLE("initInjector_RandomNormalDouble"),
		
		INJECT_VALUE("injectValue")
		;
		
		private String name;
		private NativeFunction(String nativeName) { this.name = nativeName; }
		public String getName() { return name; }
	}
	
	public static ProcedureSymbol getNativeProc(ProcedureSet ps, NativeFunction nativeFunction) {
		String name = nativeFunction.getName();
		ProcedureSymbol proc = (ProcedureSymbol) ps.getScope().lookup(PROF_ENGINE_LIB_FUNCTIONS_NAME_PREFIX + name);
		if(proc == null) { 
			addProfilingEngineSupport(ps);
			proc = (ProcedureSymbol) ps.getScope().lookup(PROF_ENGINE_LIB_FUNCTIONS_NAME_PREFIX + name);
		}
		if(proc == null)
			throw new RuntimeException("FIXME: this should not happen! the function names are probably not correct.");
		return proc;
	}
	
	private static void addProfilingEngineSupport(ProcedureSet ps){
		pragma(ps, CODEGEN_PRINT_ANNOTATION + "#include \"" + PROF_ENGINE_HEADER + "\"");
		
		Scope scope = ps.getScope();
		GecosUserTypeFactory.setScope(scope);
		
		Arrays.stream(NativeFunction.values())
			.map(NativeFunction::getName)
			.map(NativeProfilingEngineSupport::createProcSymbol)
			.forEach(scope::addSymbol);
	}

	private static ProcedureSymbol createProcSymbol(String name) {
		ProcedureSymbol procSymbol = procSymbol(PROF_ENGINE_LIB_FUNCTIONS_NAME_PREFIX + name, VOID(), emptyList());
		pragma(procSymbol, CODEGEN_IGNORE_ANNOTATION);
		return procSymbol;
	}
	
	
	//----------------------------------------------------------
	// Native Profiling Engine Compile / Execute / Retrieve
	//----------------------------------------------------------
	private static final boolean debugMode = false;
	private static final boolean verbose = true;
	
	
	/**
	 * @param srcFiles
	 * @param outDir
	 * @param incDirs
	 * @param headers
	 * @param libDirs
	 * @param libs
	 * @param macros
	 * @param stdout
	 * @param stderr
	 * @param logger
	 * @throws ProfilingBackendFailure if compilation fails
	 */
	public static void compile(List<Path> srcFiles, Path outDir, Iterable<Path> incDirs, List<String> headers, Iterable<Path> libDirs,
			Iterable<String> libs, Map<String,String> macros, OutputStream stdout, OutputStream stderr, Logger logger) 
					throws ProfilingBackendFailure {
		
		if(!srcFiles.stream().allMatch(Files::isRegularFile))
			throw new InvalidParameterException("Some source files are not valid" + srcFiles);
		
		
		boolean isMacos = SystemExec.isMacOS();
		
		AbstractShell shell = SystemExec.createShell().setVerbose(verbose).setLogger(logger);
		
		/**
		 * Check if profiling engine library exists
		 */
		Path libProfileEngineLibraryPath = PROF_ENGINE_LIB_DIR.resolve("lib" + PROF_ENGINE_LIB_NAME + (isMacos? ".dylib" : ".so"));
		if(! libProfileEngineLibraryPath.toFile().isFile()) {
			logger.warning("Did not find Profiling engine library! Trying to build it in: " + PROF_ENGINE_BUILD_DIR.toAbsolutePath());
			
			try {
				shell.shell("chmod +x ./cmake_release.sh", PROF_ENGINE_BUILD_DIR.toFile(), outToStream(stdout), outToStream(stderr));
			} catch (Exception e) {
				logger.info(Throwables.getStackTraceAsString(e));
			}
			int exitCode = shell.shell("./cmake_release.sh && make", PROF_ENGINE_BUILD_DIR.toFile(), outToStream(stdout), outToStream(stderr));
			if(exitCode != shell.successExitCode())
				throw new ProfilingBackendFailure("Failed to build Profiling engine library! Try to build it manually and rerun. NOTE: cmake is required!");
		}
		
		
		List<String> cmd = new LinkedList<>();
		cmd.add(CC);
		cmd.add(EXTRA_CFLAGS);
		
		if(USE_ALLIN1)
			cmd.add("-std=c++11");
		
		if(debugMode){
			cmd.add("-g -Wall -Wno-unknown-pragmas");
		}
		
		/* add include dirs */
		cmd.add("-I" + (USE_ALLIN1? PROF_ENGINE_ALLIN1_INC_DIR.toAbsolutePath() : PROF_ENGINE_INC_DIR.toAbsolutePath()));
		if(incDirs != null) {
			Streams.stream(incDirs)
				.map(d -> "-I" + d.toAbsolutePath())
				.forEach(cmd::add);
		}
		
		/* add headers */
		if(headers != null){
			headers.stream()
				.map(h -> "-include" + h)
				.forEach(cmd::add);
		}
		
		/* add lib dirs */
		if(! USE_ALLIN1)
			cmd.add("-L" + PROF_ENGINE_LIB_DIR.toAbsolutePath());
		if(libDirs != null) {
			Streams.stream(libDirs)
				.map(d -> "-L" + d.toAbsolutePath())
				.forEach(cmd::add);
		}
		
		/* add source files */
		srcFiles.stream()
			.map(f -> f.toAbsolutePath().toString())
			.forEach(cmd::add);
		
		/* output file */
		Path executable = outDir.resolve(BACKEND_COMPILED_EXEC_NAME);
		cmd.add("-o");
		cmd.add(executable.toAbsolutePath().toString());
		
		/* add macros */
		if(! BACKEND_PROF_FILE_USE_BIN)
			cmd.add("-D_WRITE_TXT_");
		
		if(macros != null) {
			macros.entrySet().stream()
				.map(e -> "-D" + e.getKey() + "='" + e.getValue()+"'")
				.forEach(cmd::add);
		}

		/* add libs */
		if(! USE_ALLIN1) {
			cmd.add("-l" + PROF_ENGINE_LIB_NAME);
			if(isMacos) {
				cmd.add("-rpath " + PROF_ENGINE_LIB_DIR.toAbsolutePath());
			} else {
				cmd.add("-Wl,-rpath=" + PROF_ENGINE_LIB_DIR.toAbsolutePath());
			}
		}
		if(libs != null) {
			Streams.stream(libs)
				.map(lib -> "-l" + lib)
				.forEach(cmd::add);
		}

		String waitAndSyncCmd = "while lsof "+executable.toAbsolutePath().toString()+" 2> /dev/null > /dev/null; do sleep 0.1; done"
				 +" && sync";

		int exitCode = shell.shell(cmd.stream().collect(joining(" "))
				+" && "+ waitAndSyncCmd
				, outToStream(stdout), outToStream(stderr));

		if(exitCode != shell.successExitCode())
			throw new ProfilingBackendFailure("Native Compilation failed!");
	}
	
	/**
	 * @throws ProfilingBackendFailure if execution fails.
	 */
	public static void execute(Path executableDir, OutputStream stdout, OutputStream stderr, Logger logger, String ... arguments) throws ProfilingBackendFailure {
		//XXX
		synchronized (NativeProfilingEngineSupport.PROF_ENGINE_LOCATION) {
			Path referenceResultsDir = executableDir.resolve(BACKEND_PROF_FILES_OUTPUT_DIRNAME);
			if(referenceResultsDir.toFile().isDirectory())
				FileUtils.deleteRecursive(referenceResultsDir.toString());
			referenceResultsDir.toFile().mkdirs();
		}
		
		Path executablePath = executableDir.resolve(BACKEND_COMPILED_EXEC_NAME).toAbsolutePath();
		if(!Files.isRegularFile(executablePath))
			throw new InvalidParameterException("The specified directory does not contain an executable file '" + executablePath + "'");
		
		Path dir = executableDir.toAbsolutePath();
		Path outputDir = executableDir.resolve(BACKEND_PROF_FILES_OUTPUT_DIRNAME);
		String waitAndSyncCmd = "while lsof +d "+outputDir.toAbsolutePath().toString()+" 2> /dev/null > /dev/null; do sleep 0.1; done"
				 +" && sync";
		
		AbstractShell shell = SystemExec.createShell().setVerbose(verbose).setLogger(logger);
		String cmd = executablePath.toString();
		if (arguments.length > 0) {
			cmd += " " + String.join(" ", arguments);
		}
		int exitCode = shell.shell(cmd + " && " + waitAndSyncCmd, dir.toFile(), outToStream(stdout), outToStream(stderr));
		if(exitCode != shell.successExitCode())
			throw new ProfilingBackendFailure("Native execution failed! exitcode=" + exitCode);
		
		executablePath.toFile().delete();
	}
	
	
	//XXX: specify also the list of symbols that are profiled and look directly for their profiling files (must match native)
	// if a symbol's profiling file not found throw exception! 
	public static Map<Symbol, ProfilingInfo<? extends Number>> retrieveProfilingInfo(Path executableDir, 
			Map<Integer, Symbol> instrumentedSymbols) throws ProfilingBackendFailure {
		
		Path outputsDir = executableDir.resolve(BACKEND_PROF_FILES_OUTPUT_DIRNAME);
		
		try {
//			// quick check if profiling files were generated
//			if(!Files.list(referenceResultsDir)
//					.anyMatch(f -> f.toString().endsWith(BACKEND_PROF_FILES_EXT))){
//				throw new ProfilingBackendFailure("No profiling result files were generated at: " + referenceResultsDir);
//			}
			
			Map<Integer, ProfilingInfo<? extends Number>> variablesInfo = new ProfilingInfoParser(outputsDir).retrieve();
			
			return instrumentedSymbols.keySet().stream()
					.collect(StreamUtils.toMap(instrumentedSymbols::get, variablesInfo::get, LinkedHashMap::new));
		} catch(Exception e) {
			throw new ProfilingBackendFailure(e);
		}
	}
	
	public static void removeProfilingOutputs(Path executableDir) throws ProfilingBackendFailure {

		Path outputsDir = executableDir.resolve(BACKEND_PROF_FILES_OUTPUT_DIRNAME);
		
		try {
			try(Stream<Path> stream = Files.list(outputsDir)){
			    stream.filter(f -> Files.isRegularFile(f) && f.getFileName().toString().endsWith(FILE_EXT))
				.forEach(f->f.toFile().delete());
			}
			try(Stream<Path> stream = Files.list(outputsDir)) {
				if (stream.count() == 0) {
					outputsDir.toFile().delete();
				}
			}
		} catch (Exception e) {
			throw new ProfilingBackendFailure("Failed to remove profiling outputs in directory " + outputsDir, e);
		}

		
	}
	
}
