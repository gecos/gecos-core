package fr.irisa.cairn.gecos.model.transforms.procedures.inliner;

import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.tools.controlflow.BuildControlFlow;
import fr.irisa.cairn.gecos.model.tools.controlflow.ClearControlFlow;
import fr.irisa.cairn.gecos.model.transforms.blocks.simplifier.MergeBasicBlocks;
import fr.irisa.cairn.gecos.model.transforms.blocks.simplifier.MergeCompositeBlocks;
import fr.irisa.cairn.gecos.model.utils.callgraph.CallGraph;
import fr.irisa.cairn.gecos.model.utils.callgraph.CallGraphBuilder;
import fr.irisa.cairn.gecos.model.utils.callgraph.CallGraphEdge;
import fr.irisa.cairn.gecos.model.utils.callgraph.CallGraphNode;
import fr.irisa.cairn.gecos.model.utils.callgraph.CallGraphVisitor;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import gecos.annotations.AnnotatedElement;
import gecos.annotations.AnnotationKeys;
import gecos.annotations.PragmaAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksVisitor;
import gecos.blocks.CompositeBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.AddressInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.RetInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.BaseType;
import gecos.types.FunctionType;
import gecos.types.Type;

/** 
 * @author amorvan
 */
@GSModule(" This class implements a simple Procedure inliner.\n"
		+ "Only supports functions with one return at most.")
public class ModelProcedureInliner {
	
	static final boolean debug = false;
	static final String PREFIX = "[INLINER] ";
	static void debug(Object o) { if (debug) System.out.println(PREFIX+o.toString().replace("\n", "\n"+PREFIX)); }
	
	public static final String pragmaKey = "gcs_inline".toLowerCase();
	
	/**
	 * Inline all simple calls to the 'symbolFilter' procedure found in
	 * 'targetAST'. Do not rebuild the control flow.
	 * 
	 * @param symbolFilter
	 *            The ProcedureSymbol to look for in the targetAST for inlining.
	 *            Should have a non null procedure. If null, try to inline
	 *            everything;
	 * @param targetAST
	 *            Either a GecosProject, ProcedureSet, Procedure, Block,
	 *            Instruction or any object containing a CallInstruction;
	 */
	public static void filterInline(ProcedureSymbol symbolFilter, EObject targetAST) {
		new ModelProcedureInliner(symbolFilter, targetAST).compute();
	}

	/**
	 * Look for pragma annotations in the targetAST. Process all annotations
	 * whose content contains "gcs_inline".
	 * 
	 * @param targetAST
	 */
	public static void pragmaInline(EObject targetAST) {
		new ModelProcedureInliner(targetAST).compute();
	}
	
	// if not null, specifies the ProcedureSymbol to inline; if null, the
	// inliner will look for pragma annotation
	private ProcedureSymbol symbolFilter = null;
	private EObject targetAST;
	
	@GSModuleConstructor("-arg1: a GecosObject containing one or more Procedures"
			+ "(e.g. GecosProject or ProcedureSet)")
	public ModelProcedureInliner(EObject targetAST) {
		this.targetAST = targetAST;
		this.symbolFilter = null;
	}
	
	/**
	 * Looks for a Procedure whose name is 'symbolNameFilter' in the targetAST,
	 * and inlines all the calls referencing it.
	 * 
	 * @param symbolNameFilter
	 *            the name of the procedure to inline. Its body has to be
	 *            declared in the targetAST.
	 * @param targetAST
	 *            the targetAST should be a Procedure container
	 */
	@GSModuleConstructor(
			 "-arg1: the name of the procedure to inline. Its body has to be"
			+ "present in the object passed as second argument.\n"
			+"-arg2: a GecosObject containing one or more Procedures"
			+"(e.g. GecosProject or ProcedureSet)")
	public ModelProcedureInliner(String symbolNameFilter, EObject targetAST) {
		this.targetAST = targetAST;
		this.symbolFilter = null;
		EList<Procedure> procs = EMFUtils.eAllContentsInstancesOf(targetAST, Procedure.class);
		for (Procedure proc : procs) {
			if (proc.getSymbol().getName().compareTo(symbolNameFilter)==0) {
				this.symbolFilter = proc.getSymbol();
				break;
			}
		}
		if (this.symbolFilter == null) {
			throw new IllegalArgumentException("Could not find a ProcedureSymbol named "+symbolNameFilter);
		}
	}
	
	public ModelProcedureInliner(ProcedureSymbol symbolFilter, EObject targetAST) {
		this.targetAST = targetAST;
		this.symbolFilter = symbolFilter;
	}
	
	public void compute() {
		debug("-- Starting");

		debug("Clear control flow");
		ClearControlFlow.clearCF(targetAST);
		
		if (symbolFilter != null) {
			debug("ProcedureSymbol filter is specified");
			inlineAll(symbolFilter, targetAST);
		} else {
			debug("ProcedureSymbol filter is not specified, using pragmas");
			EList<PragmaAnnotation> annotations = EMFUtils.eAllContentsInstancesOf(targetAST, PragmaAnnotation.class);
			debug(" >> Found "+annotations.size()+" pragmas in the AST");
			int inlinePragmaCounter = 0;
			for (PragmaAnnotation pragma : annotations) {
				EList<String> content = pragma.getContent();
				for (String s : content.toArray(new String[content.size()]))
					if (s.toLowerCase().contains(pragmaKey)) {
						AnnotatedElement annotatedElement = pragma.getAnnotatedElement();
						content.remove(s);
						if (content.isEmpty())
							annotatedElement.getAnnotations().remove(AnnotationKeys.PRAGMA_ANNOTATION_KEY.getLiteral());
						
						processInlinePragma(annotatedElement, targetAST);
						inlinePragmaCounter++;
						break;
					}
			}
			debug(" >> Processed "+inlinePragmaCounter+" inline pragmas");
		}

		debug("Build control flow");
		BuildControlFlow.buildCF(targetAST);
		
		debug("-- Done");
	}
	
	private static void processInlinePragma(AnnotatedElement annotatedElement, EObject targetAST) {
		if (annotatedElement instanceof ProcedureSymbol) {
			// pragma is attached to a procedure declaration/prototype
			// >> inline all occurrences of this procedure within this.targetAST
			debug("inline all occurrences of ["+annotatedElement+"] in the target AST.");
			inlineAll((ProcedureSymbol)annotatedElement, targetAST);
		} else if (annotatedElement instanceof CallInstruction) {
			// pragma is attached to an Instruction
			// >> look for call instructions in the instruction's AST, and
			//    inline them if possible
			debug("inline specific call instruction ["+annotatedElement+"]");
			inline((CallInstruction)annotatedElement);
		} else if (annotatedElement instanceof Instruction) {
			// pragma is attached to an Instruction
			// >> look for call instructions in the instruction's AST, and
			//    inline them if possible
			debug("look for calls inside the instruction ["+annotatedElement+"] for potential inlining.");
			inlineAll(null, annotatedElement);
		} else {
			System.err.println(PREFIX+"WARNING: Pragma attached to an unsupported object ["+annotatedElement+"].");
		}
	}

	/**
	 * Inline all simple calls to 'symbolFilter' procedure found in
	 * 'targetAST'. Do not rebuild the control flow.
	 * 
	 * @param symbolFilter
	 *            The ProcedureSymbol to look for in the targetAST for
	 *            inlining. If null, try to inline everything;
	 * @param targetAST
	 *            Either a GecosProject, ProcedureSet, Procedure, Block,
	 *            Instruction or any object containing a CallInstruction
	 */
	static void inlineAll(ProcedureSymbol symbolFilter, EObject targetAST) {
		//lookup all candidate CallInstruction in the target AST;
		EList<CallInstruction> eAllContentsInstancesOf = EMFUtils.eAllContentsInstancesOf(targetAST, CallInstruction.class);
		for (CallInstruction call : eAllContentsInstancesOf) {
			Instruction address = call.getAddress();
			//filter the calls with a ProcedureSymbol as address
			if  (address instanceof SymbolInstruction && 
				((SymbolInstruction)address).getSymbol() instanceof ProcedureSymbol) {
				//if a filter is given, filter the calls again
				if (symbolFilter == null) {
					inline(call);
				} else if (((SymbolInstruction)address).getSymbol().getName().compareTo(symbolFilter.getName()) == 0) {
					// the symbolFilter may reference a Procedure declared in
					// another source file, the symbol used in the call may be
					// different, and the symbol in the call may not refer to
					// a ProcedureSymbol with a body. Therefore we assume the
					// symbolFilter reference the Procedure with the body
					((SymbolInstruction)address).setSymbol(symbolFilter);
					try {
						inline(call);
					} catch (Exception e) {
						System.err.println(e.getMessage());
					}
				} else {
					//symbol do not match the call; ignore.
				}
			}
		}
	}
	
	/**
	 * Replaces the given call instruction the actual body of the Procedure
	 * called. The call address should be a SymbolInstruction whose symbol
	 * is a ProcedureSymbol with a body declaration.
	 * 
	 * @param call
	 */
	static void inline(CallInstruction call) {
		// argument check
		Instruction address = call.getAddress();
		if (!(address instanceof SymbolInstruction))
			throw new IllegalArgumentException(PREFIX+"The call address is not a known symbol. ("+call+")");
		Symbol symbol = ((SymbolInstruction)address).getSymbol();
		if (symbol == null || !(symbol instanceof ProcedureSymbol))
			throw new IllegalArgumentException(PREFIX+"The address symbol is not a ProcedureSymbol. ("+call+")");
		ProcedureSymbol toInline = (ProcedureSymbol)symbol;
		Procedure procedure = toInline.getProcedure();
		if (procedure == null) 
			throw new IllegalArgumentException(PREFIX+"Cannot inline a function whose body is unknown. ("+call+")");
		ProcedureSymbol calledRecursively = isCalledRecursively(toInline);
		if (calledRecursively != null) 
			throw new IllegalArgumentException(PREFIX+"Cannot inline a function that is called recursively. ("+call+")");
		
		Block procedureBody = ((CompositeBlock)procedure.getBody()).getChildren().get(1);
		
		
		debug("   inlining ["+call+"]");

		Instruction rootInstr = call.getRoot();
		BasicBlock callBasicBlock = rootInstr.getBasicBlock();
		if (callBasicBlock == null)
			throw new IllegalArgumentException(PREFIX+"The call instruction is not contained in a BasicBlock, but in a "+rootInstr.eContainer().getClass().getSimpleName());
		
		debug("     1- Select working CompositeBlock");
		CompositeBlock workingCompositeBlock;
		if (callBasicBlock.getParent() instanceof CompositeBlock) {
			workingCompositeBlock = (CompositeBlock) callBasicBlock.getParent();
		} else {
			//wrap the BasicBlock containing the call in a new CompositeBlock
			workingCompositeBlock = GecosUserBlockFactory.CompositeBlock();			
			callBasicBlock.getParent().replace(callBasicBlock, workingCompositeBlock);
			workingCompositeBlock.addChildren(callBasicBlock);
		}
		EList<Block> children = workingCompositeBlock.getChildren();
		int callBBIndex = children.indexOf(callBasicBlock);

		debug("     2- Split BasicBlock");
		// if the call instruction is not the first instruction in the
		// BasicBlock, we need to split the BasicBlock at the call instruction
		int indexOf = callBasicBlock.getInstructions().indexOf(rootInstr);
		if (indexOf < 0)
			throw new RuntimeException();
		if (indexOf == 0) {
			//no need to split BB
			debug("        >> no need to split BB");
		} else {
			BasicBlock newBB = GecosUserBlockFactory.BBlock();
			children.add(callBBIndex, newBB);
			callBBIndex++;
			
			for (int i = 0; i < indexOf; i++) {
				newBB.addInstruction(callBasicBlock.getFirstInstruction());
			}
			debug("        >> split");
		}

		debug("     3- Copy procedure body");
		Block copiedBody = procedureBody.copy();
		
		debug("     4- Substitute arguments");
		Scope arguments = toInline.getScope();
		EList<SymbolInstruction> symRefs = EMFUtils.eAllContentsInstancesOf(copiedBody, SymbolInstruction.class);
		for (SymbolInstruction symRef : symRefs) {
			Symbol argumentSymbol = symRef.getSymbol();
			if (arguments.getSymbols().contains(argumentSymbol)) {
				int indexOfArgument = arguments.getSymbols().indexOf(argumentSymbol);
				debug("        >> substitute ["+symRef+"] ("+indexOfArgument+")");
				Instruction argumentInstruction = call.getArgs().get(indexOfArgument).copy();
				symRef.substituteWith(argumentInstruction);
			}
		}

		debug("     5- Check & handle return statements");
		FunctionType type = (FunctionType) toInline.getType();
		Type returnType = type.getReturnType();
		boolean returnIsVoid = returnType instanceof BaseType && ((BaseType)returnType).asVoid() != null;
		EList<RetInstruction> returnInstrs = EMFUtils.eAllContentsInstancesOf(copiedBody, RetInstruction.class);
		if (returnIsVoid && returnInstrs.size() > 0)
			throw new UnsupportedOperationException(PREFIX+"Inliner supports return statements for procedures with non-void return type only. ("+call+")");
		
		if (!returnIsVoid && returnInstrs.size() != 1)
			throw new UnsupportedOperationException(PREFIX+"Inliner only supports functions with exactly one return statement. ("+call+")");
		
		if (!returnIsVoid) {
			//insert temporary variable for return value
			Symbol returnSymbol = GecosUserCoreFactory.symbol("inline_tmp", returnType);
			workingCompositeBlock.getScope().makeUnique(returnSymbol);
			workingCompositeBlock.addSymbol(returnSymbol);
			debug("        >> return type is not void; insert new symbol ["+returnSymbol+"]");
			
			for (RetInstruction returnInstr : returnInstrs.toArray(new RetInstruction[returnInstrs.size()])) {
				SetInstruction set = GecosUserInstructionFactory.set(returnSymbol, returnInstr.getExpr());
				returnInstr.substituteWith(set);
			}
			debug("        >> replacing call instruction by symbol reference");
			SymbolInstruction symbref = GecosUserInstructionFactory.symbref(returnSymbol);
			call.substituteWith(symbref);
			
			// Cannot use the following commented code, except if we can enforce
			// the return statement to be in the outermost CompositeBlock for
			// the Procedure
			
//			debug("        >> return type is not void; substitute call by return expression");
//			RetInstruction returnInstr = returnInstrs.get(0);
//			call.substituteWith(returnInstr.getExpr());
//			
//			if (returnInstr.getParent() != null)
//				returnInstr.getParent().getChildren().remove(returnInstr);
//			else 
//				returnInstr.getBasicBlock().getInstructions().remove(returnInstr);
		} else  {
			debug("        >> return is void; removing call instruction");
			if (call.getParent() != null)
				call.getParent().removeChild(call);
			else 
				call.getBasicBlock().getInstructions().remove(call);
		}
		
		debug("     6- Insert copied body");
		EList<Instruction> copiedInstructions = EMFUtils.eAllContentsFirstInstancesOf(copiedBody, Instruction.class);
		if (copiedInstructions.size() == 0) {
			debug("        >> no instruction in the copied block; no need to insert copied body");
		} else {
			children.add(callBBIndex, copiedBody);
			debug("        >> copied body inserted");
			copiedBody.getAnnotations().addAll(call.getAnnotations());
			debug("        >> annotations from call instruction copied");
			if (callBasicBlock.getInstructionCount() == 0) {
				// if the basic block is empty (due to the removing of the call
				// instruction) it will be discarded, hence we need to copy its
				// annotations too
				copiedBody.getAnnotations().addAll(callBasicBlock.getAnnotations());
				debug("        >> annotations from basic block copied");
			}
		}

		debug("     7- Simplifying");

		debug("        >> Removing indir(addr(...))");
		EList<AddressInstruction> l = EMFUtils.eAllContentsFirstInstancesOf(copiedBody, AddressInstruction.class);
		for (AddressInstruction addr : l) {
			if (addr.getParent() != null) {
				ComplexInstruction parent = addr.getParent();
				 if (parent instanceof IndirInstruction)
					 parent.substituteWith(addr.getAddress());
				 else if (parent instanceof ConvertInstruction) {
					 ComplexInstruction parent2 = parent.getParent();
					 if (parent2 instanceof IndirInstruction)
						 parent2.substituteWith(addr.getAddress());
				 }
			}
		}
		debug("        >> merge composite blocks");
		workingCompositeBlock.accept((BlocksVisitor)new MergeCompositeBlocks());

		debug("        >> merge basic blocks");
		workingCompositeBlock.accept((BlocksVisitor)new MergeBasicBlocks());
		
	}
	
	private static ProcedureSymbol isCalledRecursively(ProcedureSymbol pSym) {
		CallGraph compute = new CallGraphBuilder(pSym.getProcedure().getContainingProcedureSet()).compute();
		RecursiveCallFinder recursiveCallFinder = new RecursiveCallFinder(pSym);
		Object accept = compute.accept(recursiveCallFinder);
		if (accept != null)
			return ((CallGraphNode)accept).procedure().getSymbol();
		return null;
	}
	
	private static class RecursiveCallFinder extends CallGraphVisitor {

		private Set<CallGraphNode> visitedNodes;
		private ProcedureSymbol pSym;
		
		public RecursiveCallFinder(ProcedureSymbol pSym) {
			visitedNodes = new LinkedHashSet<CallGraphNode>();
			this.pSym = pSym;
		}
		
		@Override
		public Object visitCallGraph(CallGraph callGraph) {
			CallGraphNode callGraphNode = callGraph.get(this.pSym.getName());
			if (callGraphNode != null)
				return callGraphNode.accept(this);
			return null;
		}

		@Override
		public Object visitCallGraphNode(CallGraphNode node) {
			for (CallGraphEdge  edge : node.outEdges()) {
				CallGraphNode toNode = edge.to();
				if (!visitedNodes.contains(toNode)) {
					visitedNodes.add(toNode);
					return toNode.accept(this);
				} else {
					return toNode;
				}
			}
			return null;
		}

		@Override
		public Object visitCallGraphEdge(CallGraphEdge edge) {
			throw new UnsupportedOperationException();
		}
		
	}
}
