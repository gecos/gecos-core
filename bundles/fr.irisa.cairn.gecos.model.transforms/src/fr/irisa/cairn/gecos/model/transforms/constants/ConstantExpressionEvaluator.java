package fr.irisa.cairn.gecos.model.transforms.constants;

import fr.irisa.cairn.gecos.model.analysis.instructions.evaluation.InstructionEvaluation;
import fr.irisa.cairn.gecos.model.tools.switches.DefaultInstructionSwitch;
import gecos.core.Symbol;
import gecos.instrs.ArrayValueInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.RangeInstruction;
import gecos.instrs.SymbolInstruction;

public class ConstantExpressionEvaluator extends DefaultInstructionSwitch<Object> {

	private Instruction root;

	@Override
	public Object caseGenericInstruction(GenericInstruction g) {
		Instruction retInstr =null;

		int size = g.getOperands().size();
		for (int i = 0; i < size; ++i)
			doSwitch(g.getOperand(i));
		for (int i = 0; i < size; ++i)
			if (! g.getOperand(i).isConstant())
				return retInstr;

		// at this point children are constants
		if (size == 2 && ! g.getName().equals("call")) {
			Instruction t = InstructionEvaluation.applyBinary(g);
			if (t != null) {
				replaceChild(g, t);
				root= t;
			}

		} else if (g.getName().equals("array")) {
			if (! (g.getOperand(0) instanceof ArrayValueInstruction)) return retInstr;
			ArrayValueInstruction value = (ArrayValueInstruction) g.getOperand(0);
			IntInstruction index = (IntInstruction) g.getOperand(1);
			if (index.getValue() < 0 || index.getValue() >= value.getChildren().size())
				throw new RuntimeException("bad array value index ");
			replaceChild(g, value.getChildren().get((int)index.getValue()));
		}
		return retInstr;
		//return super.caseGenericInstruction(g);
	}



	private void replaceChild(Instruction oldInst, Instruction newInst) {
		if (oldInst.getParent() == null) {
			root = newInst;
		} else {
			oldInst.getParent().replaceChild(oldInst, newInst);
		}
	}

	@Override
	public Object caseConvertInstruction(ConvertInstruction c) {
		doSwitch(c.getExpr());
		return super.caseConvertInstruction(c);
	}

	@Override
	public Object caseSymbolInstruction(SymbolInstruction s) {
		if (s.isConstant()) {
			Symbol sym = s.getSymbol();
			if (sym != null && sym.getValue() != null) {
				s.getParent().replaceChild(s, sym.getValue());
			}
		}
		return super.caseSymbolInstruction(s);
	}

	@Override
	public Object caseRangeInstruction(RangeInstruction range) {
		doSwitch(range.getExpr());
		if (range.getExpr().isConstant()) {
			Instruction t = InstructionEvaluation.applyRange(range);
			if (t != null) {
				range.getParent().replaceChild(range, t);
			}
		}
		return super.caseRangeInstruction(range);
	}

	public IntInstruction evaluate(Instruction inst) {
		root = inst;
		doSwitch(inst);
		if (root == null || !( root instanceof IntInstruction)) {
			return null;
		}
		return (IntInstruction) root;
	}
}
