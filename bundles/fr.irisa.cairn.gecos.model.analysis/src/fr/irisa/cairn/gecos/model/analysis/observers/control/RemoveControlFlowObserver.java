package fr.irisa.cairn.gecos.model.analysis.observers.control;

import gecos.core.Procedure;

public class RemoveControlFlowObserver {
	
	Procedure proc;
	ControlFlowObserver observer;
	
	public RemoveControlFlowObserver(Procedure proc, ControlFlowObserver observer) {
		this.proc=proc;
		this.observer=observer;
	}

	public void compute() {
		proc.eAdapters().remove(observer);
	}
}
