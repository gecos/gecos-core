package fr.irisa.cairn.gecos.model.analysis.instructions.comparator;

public enum FuzzyBoolean {
	YES,
	NO,
	MAYBE;
	
	public FuzzyBoolean and(FuzzyBoolean other) {
		if(this.equals(MAYBE) || other.equals(MAYBE))
			return MAYBE;
		if(this.equals(YES) && other.equals(YES))
			return YES;
		return NO;
	}
	
}
