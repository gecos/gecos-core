/**
 */
package gecos.instrs.impl;

import gecos.instrs.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class InstrsFactoryImpl extends EFactoryImpl implements InstrsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static InstrsFactory init() {
		try {
			InstrsFactory theInstrsFactory = (InstrsFactory)EPackage.Registry.INSTANCE.getEFactory(InstrsPackage.eNS_URI);
			if (theInstrsFactory != null) {
				return theInstrsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new InstrsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InstrsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case InstrsPackage.DUMMY_INSTRUCTION: return createDummyInstruction();
			case InstrsPackage.LABEL_INSTRUCTION: return createLabelInstruction();
			case InstrsPackage.SYMBOL_INSTRUCTION: return createSymbolInstruction();
			case InstrsPackage.NUMBERED_SYMBOL_INSTRUCTION: return createNumberedSymbolInstruction();
			case InstrsPackage.SSA_DEF_SYMBOL: return createSSADefSymbol();
			case InstrsPackage.SSA_USE_SYMBOL: return createSSAUseSymbol();
			case InstrsPackage.SIZEOF_INSTRUCTION: return createSizeofInstruction();
			case InstrsPackage.SIZEOF_TYPE_INSTRUCTION: return createSizeofTypeInstruction();
			case InstrsPackage.SIZEOF_VALUE_INSTRUCTION: return createSizeofValueInstruction();
			case InstrsPackage.INT_INSTRUCTION: return createIntInstruction();
			case InstrsPackage.FLOAT_INSTRUCTION: return createFloatInstruction();
			case InstrsPackage.STRING_INSTRUCTION: return createStringInstruction();
			case InstrsPackage.ARRAY_VALUE_INSTRUCTION: return createArrayValueInstruction();
			case InstrsPackage.PHI_INSTRUCTION: return createPhiInstruction();
			case InstrsPackage.SET_INSTRUCTION: return createSetInstruction();
			case InstrsPackage.ADDRESS_INSTRUCTION: return createAddressInstruction();
			case InstrsPackage.INDIR_INSTRUCTION: return createIndirInstruction();
			case InstrsPackage.CALL_INSTRUCTION: return createCallInstruction();
			case InstrsPackage.METHOD_CALL_INSTRUCTION: return createMethodCallInstruction();
			case InstrsPackage.ARRAY_INSTRUCTION: return createArrayInstruction();
			case InstrsPackage.SIMPLE_ARRAY_INSTRUCTION: return createSimpleArrayInstruction();
			case InstrsPackage.RANGE_ARRAY_INSTRUCTION: return createRangeArrayInstruction();
			case InstrsPackage.COND_INSTRUCTION: return createCondInstruction();
			case InstrsPackage.BREAK_INSTRUCTION: return createBreakInstruction();
			case InstrsPackage.CONTINUE_INSTRUCTION: return createContinueInstruction();
			case InstrsPackage.GOTO_INSTRUCTION: return createGotoInstruction();
			case InstrsPackage.RET_INSTRUCTION: return createRetInstruction();
			case InstrsPackage.CONVERT_INSTRUCTION: return createConvertInstruction();
			case InstrsPackage.FIELD_INSTRUCTION: return createFieldInstruction();
			case InstrsPackage.ENUMERATOR_INSTRUCTION: return createEnumeratorInstruction();
			case InstrsPackage.RANGE_INSTRUCTION: return createRangeInstruction();
			case InstrsPackage.CASE_INSTRUCTION: return createCaseInstruction();
			case InstrsPackage.GENERIC_INSTRUCTION: return createGenericInstruction();
			case InstrsPackage.ARITHMETIC_INSTRUCTION: return createArithmeticInstruction();
			case InstrsPackage.COMPARISON_INSTRUCTION: return createComparisonInstruction();
			case InstrsPackage.LOGICAL_INSTRUCTION: return createLogicalInstruction();
			case InstrsPackage.BITWISE_INSTRUCTION: return createBitwiseInstruction();
			case InstrsPackage.SIMD_INSTRUCTION: return createSimdInstruction();
			case InstrsPackage.SIMD_GENERIC_INSTRUCTION: return createSimdGenericInstruction();
			case InstrsPackage.SIMD_PACK_INSTRUCTION: return createSimdPackInstruction();
			case InstrsPackage.SIMD_EXTRACT_INSTRUCTION: return createSimdExtractInstruction();
			case InstrsPackage.SIMD_SHUFFLE_INSTRUCTION: return createSimdShuffleInstruction();
			case InstrsPackage.SIMD_EXPAND_INSTRUCTION: return createSimdExpandInstruction();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case InstrsPackage.ARITHMETIC_OPERATOR:
				return createArithmeticOperatorFromString(eDataType, initialValue);
			case InstrsPackage.COMPARISON_OPERATOR:
				return createComparisonOperatorFromString(eDataType, initialValue);
			case InstrsPackage.LOGICAL_OPERATOR:
				return createLogicalOperatorFromString(eDataType, initialValue);
			case InstrsPackage.BITWISE_OPERATOR:
				return createBitwiseOperatorFromString(eDataType, initialValue);
			case InstrsPackage.SELECT_OPERATOR:
				return createSelectOperatorFromString(eDataType, initialValue);
			case InstrsPackage.REDUCTION_OPERATOR:
				return createReductionOperatorFromString(eDataType, initialValue);
			case InstrsPackage.BRANCH_TYPE:
				return createBranchTypeFromString(eDataType, initialValue);
			case InstrsPackage.STRING:
				return createStringFromString(eDataType, initialValue);
			case InstrsPackage.INT:
				return createintFromString(eDataType, initialValue);
			case InstrsPackage.LONG:
				return createlongFromString(eDataType, initialValue);
			case InstrsPackage.DOUBLE:
				return createdoubleFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case InstrsPackage.ARITHMETIC_OPERATOR:
				return convertArithmeticOperatorToString(eDataType, instanceValue);
			case InstrsPackage.COMPARISON_OPERATOR:
				return convertComparisonOperatorToString(eDataType, instanceValue);
			case InstrsPackage.LOGICAL_OPERATOR:
				return convertLogicalOperatorToString(eDataType, instanceValue);
			case InstrsPackage.BITWISE_OPERATOR:
				return convertBitwiseOperatorToString(eDataType, instanceValue);
			case InstrsPackage.SELECT_OPERATOR:
				return convertSelectOperatorToString(eDataType, instanceValue);
			case InstrsPackage.REDUCTION_OPERATOR:
				return convertReductionOperatorToString(eDataType, instanceValue);
			case InstrsPackage.BRANCH_TYPE:
				return convertBranchTypeToString(eDataType, instanceValue);
			case InstrsPackage.STRING:
				return convertStringToString(eDataType, instanceValue);
			case InstrsPackage.INT:
				return convertintToString(eDataType, instanceValue);
			case InstrsPackage.LONG:
				return convertlongToString(eDataType, instanceValue);
			case InstrsPackage.DOUBLE:
				return convertdoubleToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DummyInstruction createDummyInstruction() {
		DummyInstructionImpl dummyInstruction = new DummyInstructionImpl();
		return dummyInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LabelInstruction createLabelInstruction() {
		LabelInstructionImpl labelInstruction = new LabelInstructionImpl();
		return labelInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SymbolInstruction createSymbolInstruction() {
		SymbolInstructionImpl symbolInstruction = new SymbolInstructionImpl();
		return symbolInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NumberedSymbolInstruction createNumberedSymbolInstruction() {
		NumberedSymbolInstructionImpl numberedSymbolInstruction = new NumberedSymbolInstructionImpl();
		return numberedSymbolInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SSADefSymbol createSSADefSymbol() {
		SSADefSymbolImpl ssaDefSymbol = new SSADefSymbolImpl();
		return ssaDefSymbol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SSAUseSymbol createSSAUseSymbol() {
		SSAUseSymbolImpl ssaUseSymbol = new SSAUseSymbolImpl();
		return ssaUseSymbol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SizeofInstruction createSizeofInstruction() {
		SizeofInstructionImpl sizeofInstruction = new SizeofInstructionImpl();
		return sizeofInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SizeofTypeInstruction createSizeofTypeInstruction() {
		SizeofTypeInstructionImpl sizeofTypeInstruction = new SizeofTypeInstructionImpl();
		return sizeofTypeInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SizeofValueInstruction createSizeofValueInstruction() {
		SizeofValueInstructionImpl sizeofValueInstruction = new SizeofValueInstructionImpl();
		return sizeofValueInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntInstruction createIntInstruction() {
		IntInstructionImpl intInstruction = new IntInstructionImpl();
		return intInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FloatInstruction createFloatInstruction() {
		FloatInstructionImpl floatInstruction = new FloatInstructionImpl();
		return floatInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StringInstruction createStringInstruction() {
		StringInstructionImpl stringInstruction = new StringInstructionImpl();
		return stringInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArrayValueInstruction createArrayValueInstruction() {
		ArrayValueInstructionImpl arrayValueInstruction = new ArrayValueInstructionImpl();
		return arrayValueInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhiInstruction createPhiInstruction() {
		PhiInstructionImpl phiInstruction = new PhiInstructionImpl();
		return phiInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SetInstruction createSetInstruction() {
		SetInstructionImpl setInstruction = new SetInstructionImpl();
		return setInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AddressInstruction createAddressInstruction() {
		AddressInstructionImpl addressInstruction = new AddressInstructionImpl();
		return addressInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IndirInstruction createIndirInstruction() {
		IndirInstructionImpl indirInstruction = new IndirInstructionImpl();
		return indirInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallInstruction createCallInstruction() {
		CallInstructionImpl callInstruction = new CallInstructionImpl();
		return callInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MethodCallInstruction createMethodCallInstruction() {
		MethodCallInstructionImpl methodCallInstruction = new MethodCallInstructionImpl();
		return methodCallInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArrayInstruction createArrayInstruction() {
		ArrayInstructionImpl arrayInstruction = new ArrayInstructionImpl();
		return arrayInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleArrayInstruction createSimpleArrayInstruction() {
		SimpleArrayInstructionImpl simpleArrayInstruction = new SimpleArrayInstructionImpl();
		return simpleArrayInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RangeArrayInstruction createRangeArrayInstruction() {
		RangeArrayInstructionImpl rangeArrayInstruction = new RangeArrayInstructionImpl();
		return rangeArrayInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CondInstruction createCondInstruction() {
		CondInstructionImpl condInstruction = new CondInstructionImpl();
		return condInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BreakInstruction createBreakInstruction() {
		BreakInstructionImpl breakInstruction = new BreakInstructionImpl();
		return breakInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ContinueInstruction createContinueInstruction() {
		ContinueInstructionImpl continueInstruction = new ContinueInstructionImpl();
		return continueInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GotoInstruction createGotoInstruction() {
		GotoInstructionImpl gotoInstruction = new GotoInstructionImpl();
		return gotoInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RetInstruction createRetInstruction() {
		RetInstructionImpl retInstruction = new RetInstructionImpl();
		return retInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConvertInstruction createConvertInstruction() {
		ConvertInstructionImpl convertInstruction = new ConvertInstructionImpl();
		return convertInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FieldInstruction createFieldInstruction() {
		FieldInstructionImpl fieldInstruction = new FieldInstructionImpl();
		return fieldInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumeratorInstruction createEnumeratorInstruction() {
		EnumeratorInstructionImpl enumeratorInstruction = new EnumeratorInstructionImpl();
		return enumeratorInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RangeInstruction createRangeInstruction() {
		RangeInstructionImpl rangeInstruction = new RangeInstructionImpl();
		return rangeInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CaseInstruction createCaseInstruction() {
		CaseInstructionImpl caseInstruction = new CaseInstructionImpl();
		return caseInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericInstruction createGenericInstruction() {
		GenericInstructionImpl genericInstruction = new GenericInstructionImpl();
		return genericInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArithmeticInstruction createArithmeticInstruction() {
		ArithmeticInstructionImpl arithmeticInstruction = new ArithmeticInstructionImpl();
		return arithmeticInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComparisonInstruction createComparisonInstruction() {
		ComparisonInstructionImpl comparisonInstruction = new ComparisonInstructionImpl();
		return comparisonInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalInstruction createLogicalInstruction() {
		LogicalInstructionImpl logicalInstruction = new LogicalInstructionImpl();
		return logicalInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BitwiseInstruction createBitwiseInstruction() {
		BitwiseInstructionImpl bitwiseInstruction = new BitwiseInstructionImpl();
		return bitwiseInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimdInstruction createSimdInstruction() {
		SimdInstructionImpl simdInstruction = new SimdInstructionImpl();
		return simdInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimdGenericInstruction createSimdGenericInstruction() {
		SimdGenericInstructionImpl simdGenericInstruction = new SimdGenericInstructionImpl();
		return simdGenericInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimdPackInstruction createSimdPackInstruction() {
		SimdPackInstructionImpl simdPackInstruction = new SimdPackInstructionImpl();
		return simdPackInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimdExtractInstruction createSimdExtractInstruction() {
		SimdExtractInstructionImpl simdExtractInstruction = new SimdExtractInstructionImpl();
		return simdExtractInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimdShuffleInstruction createSimdShuffleInstruction() {
		SimdShuffleInstructionImpl simdShuffleInstruction = new SimdShuffleInstructionImpl();
		return simdShuffleInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimdExpandInstruction createSimdExpandInstruction() {
		SimdExpandInstructionImpl simdExpandInstruction = new SimdExpandInstructionImpl();
		return simdExpandInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArithmeticOperator createArithmeticOperatorFromString(EDataType eDataType, String initialValue) {
		ArithmeticOperator result = ArithmeticOperator.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertArithmeticOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComparisonOperator createComparisonOperatorFromString(EDataType eDataType, String initialValue) {
		ComparisonOperator result = ComparisonOperator.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertComparisonOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalOperator createLogicalOperatorFromString(EDataType eDataType, String initialValue) {
		LogicalOperator result = LogicalOperator.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLogicalOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BitwiseOperator createBitwiseOperatorFromString(EDataType eDataType, String initialValue) {
		BitwiseOperator result = BitwiseOperator.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBitwiseOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SelectOperator createSelectOperatorFromString(EDataType eDataType, String initialValue) {
		SelectOperator result = SelectOperator.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSelectOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReductionOperator createReductionOperatorFromString(EDataType eDataType, String initialValue) {
		ReductionOperator result = ReductionOperator.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertReductionOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BranchType createBranchTypeFromString(EDataType eDataType, String initialValue) {
		BranchType result = BranchType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBranchTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createStringFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStringToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer createintFromString(EDataType eDataType, String initialValue) {
		return (Integer)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertintToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Long createlongFromString(EDataType eDataType, String initialValue) {
		return (Long)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertlongToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double createdoubleFromString(EDataType eDataType, String initialValue) {
		return (Double)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertdoubleToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InstrsPackage getInstrsPackage() {
		return (InstrsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static InstrsPackage getPackage() {
		return InstrsPackage.eINSTANCE;
	}

} //InstrsFactoryImpl
