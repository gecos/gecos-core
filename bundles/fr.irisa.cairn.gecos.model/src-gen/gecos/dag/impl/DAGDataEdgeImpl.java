/**
 */
package gecos.dag.impl;

import gecos.dag.DAGDataEdge;
import gecos.dag.DAGNode;
import gecos.dag.DagPackage;
import gecos.dag.DagVisitor;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DAG Data Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DAGDataEdgeImpl extends DAGEdgeImpl implements DAGDataEdge {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DAGDataEdgeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DagPackage.Literals.DAG_DATA_EDGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final DagVisitor visitor) {
		visitor.visitDAGDataEdge(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		DAGNode _sinkNode = this.getSinkNode();
		return ("Value Dependency to " + _sinkNode);
	}

} //DAGDataEdgeImpl
