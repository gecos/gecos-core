/**
 */
package gecos.types;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AC Int Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.ACIntType#getBitwidth <em>Bitwidth</em>}</li>
 *   <li>{@link gecos.types.ACIntType#getOverflowMode <em>Overflow Mode</em>}</li>
 * </ul>
 *
 * @see gecos.types.TypesPackage#getACIntType()
 * @model
 * @generated
 */
public interface ACIntType extends ArrayType, ACType, TypesVisitable {
	/**
	 * Returns the value of the '<em><b>Bitwidth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bitwidth</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bitwidth</em>' attribute.
	 * @see #setBitwidth(long)
	 * @see gecos.types.TypesPackage#getACIntType_Bitwidth()
	 * @model unique="false" dataType="gecos.types.long"
	 * @generated
	 */
	long getBitwidth();

	/**
	 * Sets the value of the '{@link gecos.types.ACIntType#getBitwidth <em>Bitwidth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bitwidth</em>' attribute.
	 * @see #getBitwidth()
	 * @generated
	 */
	void setBitwidth(long value);

	/**
	 * Returns the value of the '<em><b>Overflow Mode</b></em>' attribute.
	 * The literals are from the enumeration {@link gecos.types.OverflowMode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overflow Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overflow Mode</em>' attribute.
	 * @see gecos.types.OverflowMode
	 * @see #setOverflowMode(OverflowMode)
	 * @see gecos.types.TypesPackage#getACIntType_OverflowMode()
	 * @model unique="false"
	 * @generated
	 */
	OverflowMode getOverflowMode();

	/**
	 * Sets the value of the '{@link gecos.types.ACIntType#getOverflowMode <em>Overflow Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Overflow Mode</em>' attribute.
	 * @see gecos.types.OverflowMode
	 * @see #getOverflowMode()
	 * @generated
	 */
	void setOverflowMode(OverflowMode value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitACIntType(this);'"
	 * @generated
	 */
	void accept(TypesVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%java.lang.StringBuffer%&gt; result = new &lt;%java.lang.StringBuffer%&gt;(\"ac_int&lt;\");\nresult.append(this.getBitwidth());\n&lt;%java.lang.String%&gt; _xifexpression = null;\nboolean _signed = this.getSigned();\nif (_signed)\n{\n\t_xifexpression = \",true\";\n}\nelse\n{\n\t_xifexpression = \"false\";\n}\nresult.append(_xifexpression);\nint _value = this.getOverflowMode().getValue();\nboolean _notEquals = (_value != 0);\nif (_notEquals)\n{\n\t&lt;%java.lang.String%&gt; _name = this.getOverflowMode().getName();\n\t&lt;%java.lang.String%&gt; _plus = (\",\" + _name);\n\tresult.append(_plus);\n}\nresult.append(\"&gt;\");\nreturn result.toString();'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.boolean" unique="false" otherUnique="false" ignoreStorageClassDataType="gecos.types.boolean" ignoreStorageClassUnique="false" ignoreConstDataType="gecos.types.boolean" ignoreConstUnique="false" ignoreVolatileDataType="gecos.types.boolean" ignoreVolatileUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((this == other))\n{\n\treturn true;\n}\nif ((other instanceof &lt;%gecos.types.ACIntType%&gt;))\n{\n\treturn (((((&lt;%gecos.types.ACIntType%&gt;)other).getBitwidth() == this.getBitwidth()) &amp;&amp; (((&lt;%gecos.types.ACIntType%&gt;)other).getOverflowMode() == this.getOverflowMode())) &amp;&amp; super.isEqual(other, ignoreStorageClass, ignoreConst, ignoreVolatile));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isEqual(Type other, boolean ignoreStorageClass, boolean ignoreConst, boolean ignoreVolatile);

} // ACIntType
