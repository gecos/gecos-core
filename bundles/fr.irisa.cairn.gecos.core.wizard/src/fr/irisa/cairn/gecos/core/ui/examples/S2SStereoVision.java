package fr.irisa.cairn.gecos.core.ui.examples;

import org.eclipse.core.runtime.Platform;
import org.eclipse.ui.INewWizard;
import org.osgi.framework.Bundle;

import fr.irisa.cairn.gecos.core.ui.Activator;
import fr.irisa.cairn.gecos.core.ui.examples.utils.BasicGecosExampleWizard;

public class S2SStereoVision extends BasicGecosExampleWizard implements INewWizard {
	@Override
	public String getResourcesFolderToCopyIntoProject() {
		return "resources/wizard_templates/s2s_stereo_vision/";
	}

	@Override
	public String getPageName() {
		return "Source to source transformation on a Stereo Vision algorithm";
	}

	@Override
	public String getPageDescription() {
		return "This wizard will create a complete project of source-to-source on a Stereo Vision algorithm";
	}

	@Override
	protected Bundle getPluginBundle() {
		return Platform.getBundle(Activator.PLUGIN_ID);
	}
}
