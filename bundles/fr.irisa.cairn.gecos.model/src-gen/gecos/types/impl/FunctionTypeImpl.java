/**
 */
package gecos.types.impl;

import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.TypeSizes;

import gecos.types.FunctionType;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import gecos.types.TypesFactory;
import gecos.types.TypesPackage;
import gecos.types.TypesVisitor;
import gecos.types.__Internal_ParameterTypeWrapper;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions;

import org.eclipse.xtext.xbase.lib.ExclusiveRange;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.impl.FunctionTypeImpl#getReturnType <em>Return Type</em>}</li>
 *   <li>{@link gecos.types.impl.FunctionTypeImpl#get__internal_parameter_wrapers <em>internal parameter wrapers</em>}</li>
 *   <li>{@link gecos.types.impl.FunctionTypeImpl#isHasElipsis <em>Has Elipsis</em>}</li>
 *   <li>{@link gecos.types.impl.FunctionTypeImpl#isInline <em>Inline</em>}</li>
 *   <li>{@link gecos.types.impl.FunctionTypeImpl#isNoreturn <em>Noreturn</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FunctionTypeImpl extends TypeImpl implements FunctionType {
	/**
	 * The cached value of the '{@link #getReturnType() <em>Return Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReturnType()
	 * @generated
	 * @ordered
	 */
	protected Type returnType;

	/**
	 * The cached value of the '{@link #get__internal_parameter_wrapers() <em>internal parameter wrapers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #get__internal_parameter_wrapers()
	 * @generated
	 * @ordered
	 */
	protected EList<__Internal_ParameterTypeWrapper> __internal_parameter_wrapers;

	/**
	 * The default value of the '{@link #isHasElipsis() <em>Has Elipsis</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasElipsis()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HAS_ELIPSIS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHasElipsis() <em>Has Elipsis</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasElipsis()
	 * @generated
	 * @ordered
	 */
	protected boolean hasElipsis = HAS_ELIPSIS_EDEFAULT;

	/**
	 * The default value of the '{@link #isInline() <em>Inline</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInline()
	 * @generated
	 * @ordered
	 */
	protected static final boolean INLINE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isInline() <em>Inline</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInline()
	 * @generated
	 * @ordered
	 */
	protected boolean inline = INLINE_EDEFAULT;

	/**
	 * The default value of the '{@link #isNoreturn() <em>Noreturn</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNoreturn()
	 * @generated
	 * @ordered
	 */
	protected static final boolean NORETURN_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isNoreturn() <em>Noreturn</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNoreturn()
	 * @generated
	 * @ordered
	 */
	protected boolean noreturn = NORETURN_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FunctionTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypesPackage.Literals.FUNCTION_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type getReturnType() {
		return returnType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReturnType(Type newReturnType) {
		Type oldReturnType = returnType;
		returnType = newReturnType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.FUNCTION_TYPE__RETURN_TYPE, oldReturnType, returnType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<__Internal_ParameterTypeWrapper> get__internal_parameter_wrapers() {
		if (__internal_parameter_wrapers == null) {
			__internal_parameter_wrapers = new EObjectContainmentEList<__Internal_ParameterTypeWrapper>(__Internal_ParameterTypeWrapper.class, this, TypesPackage.FUNCTION_TYPE__INTERNAL_PARAMETER_WRAPERS);
		}
		return __internal_parameter_wrapers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHasElipsis() {
		return hasElipsis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasElipsis(boolean newHasElipsis) {
		boolean oldHasElipsis = hasElipsis;
		hasElipsis = newHasElipsis;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.FUNCTION_TYPE__HAS_ELIPSIS, oldHasElipsis, hasElipsis));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isInline() {
		return inline;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInline(boolean newInline) {
		boolean oldInline = inline;
		inline = newInline;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.FUNCTION_TYPE__INLINE, oldInline, inline));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isNoreturn() {
		return noreturn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNoreturn(boolean newNoreturn) {
		boolean oldNoreturn = noreturn;
		noreturn = newNoreturn;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.FUNCTION_TYPE__NORETURN, oldNoreturn, noreturn));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Type> listParameters() {
		final Function1<__Internal_ParameterTypeWrapper, Type> _function = new Function1<__Internal_ParameterTypeWrapper, Type>() {
			public Type apply(final __Internal_ParameterTypeWrapper it) {
				return it.getParameter();
			}
		};
		return ECollections.<Type>unmodifiableEList(ECollections.<Type>asEList(XcoreEListExtensions.<__Internal_ParameterTypeWrapper, Type>map(this.get__internal_parameter_wrapers(), _function)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addParameter(final Type param) {
		final __Internal_ParameterTypeWrapper wrapper = TypesFactory.eINSTANCE.create__Internal_ParameterTypeWrapper();
		wrapper.setParameter(param);
		this.get__internal_parameter_wrapers().add(wrapper);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type getParameterAt(final int i) {
		return this.get__internal_parameter_wrapers().get(i).getParameter();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeParameterAt(final int i) {
		this.get__internal_parameter_wrapers().remove(i);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void clearParameters() {
		this.get__internal_parameter_wrapers().clear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final TypesVisitor visitor) {
		visitor.visitFunctionType(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _xifexpression = null;
		boolean _isInline = this.isInline();
		if (_isInline) {
			_xifexpression = "inline ";
		}
		else {
			_xifexpression = "";
		}
		String _plus = (_xifexpression + "(");
		String _join = IterableExtensions.join(this.listParameters(), ",");
		String _plus_1 = (_plus + _join);
		String _xifexpression_1 = null;
		boolean _isHasElipsis = this.isHasElipsis();
		if (_isHasElipsis) {
			_xifexpression_1 = ",...";
		}
		else {
			_xifexpression_1 = "";
		}
		String _plus_2 = (_plus_1 + _xifexpression_1);
		String _plus_3 = (_plus_2 + ")");
		String _plus_4 = (_plus_3 + " -> (");
		String _elvis = null;
		Type _returnType = this.getReturnType();
		String _string = null;
		if (_returnType!=null) {
			_string=_returnType.toString();
		}
		if (_string != null) {
			_elvis = _string;
		} else {
			_elvis = "?";
		}
		String _plus_5 = (_plus_4 + _elvis);
		return (_plus_5 + ")");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getSize() {
		return TypeSizes.defaultPointerSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEqual(final Type other, final boolean ignoreStorageClass, final boolean ignoreConst, final boolean ignoreVolatile) {
		if ((other == this)) {
			return true;
		}
		if ((other instanceof FunctionType)) {
			final FunctionType ft = ((FunctionType)other);
			Type _returnType = ft.getReturnType();
			boolean _tripleNotEquals = (_returnType != null);
			Type _returnType_1 = this.getReturnType();
			boolean _tripleNotEquals_1 = (_returnType_1 != null);
			boolean _xor = (_tripleNotEquals ^ _tripleNotEquals_1);
			if (_xor) {
				return false;
			}
			if (((ft.getReturnType() != null) && (this.getReturnType() != null))) {
				boolean _isEqual = ft.getReturnType().isEqual(this.getReturnType(), ignoreStorageClass, ignoreConst, ignoreVolatile);
				boolean _not = (!_isEqual);
				if (_not) {
					return false;
				}
			}
			int _size = ft.listParameters().size();
			int _size_1 = this.listParameters().size();
			boolean _tripleNotEquals_2 = (_size != _size_1);
			if (_tripleNotEquals_2) {
				return false;
			}
			int _size_2 = ft.listParameters().size();
			ExclusiveRange _doubleDotLessThan = new ExclusiveRange(0, _size_2, true);
			for (final Integer i : _doubleDotLessThan) {
				{
					Type _get = ft.listParameters().get((i).intValue());
					boolean _tripleNotEquals_3 = (_get != null);
					Type _get_1 = this.listParameters().get((i).intValue());
					boolean _tripleNotEquals_4 = (_get_1 != null);
					boolean _xor_1 = (_tripleNotEquals_3 ^ _tripleNotEquals_4);
					if (_xor_1) {
						return false;
					}
					if (((ft.listParameters().get((i).intValue()) != null) && (this.listParameters().get((i).intValue()) != null))) {
						boolean _isEqual_1 = ft.listParameters().get((i).intValue()).isEqual(this.listParameters().get((i).intValue()), ignoreStorageClass, ignoreConst, ignoreVolatile);
						boolean _not_1 = (!_isEqual_1);
						if (_not_1) {
							return false;
						}
					}
				}
			}
			return ((((this.isHasElipsis() == ft.isHasElipsis()) && (this.isInline() == ft.isInline())) && (this.isNoreturn() == ft.isNoreturn())) && super.isEqual(ft, ignoreStorageClass, ignoreConst, ignoreVolatile));
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInnermostStorageClass(final StorageClassSpecifiers innermostStorage) {
		Type _returnType = this.getReturnType();
		_returnType.setInnermostStorageClass(innermostStorage);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TypesPackage.FUNCTION_TYPE__INTERNAL_PARAMETER_WRAPERS:
				return ((InternalEList<?>)get__internal_parameter_wrapers()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TypesPackage.FUNCTION_TYPE__RETURN_TYPE:
				return getReturnType();
			case TypesPackage.FUNCTION_TYPE__INTERNAL_PARAMETER_WRAPERS:
				return get__internal_parameter_wrapers();
			case TypesPackage.FUNCTION_TYPE__HAS_ELIPSIS:
				return isHasElipsis();
			case TypesPackage.FUNCTION_TYPE__INLINE:
				return isInline();
			case TypesPackage.FUNCTION_TYPE__NORETURN:
				return isNoreturn();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TypesPackage.FUNCTION_TYPE__RETURN_TYPE:
				setReturnType((Type)newValue);
				return;
			case TypesPackage.FUNCTION_TYPE__INTERNAL_PARAMETER_WRAPERS:
				get__internal_parameter_wrapers().clear();
				get__internal_parameter_wrapers().addAll((Collection<? extends __Internal_ParameterTypeWrapper>)newValue);
				return;
			case TypesPackage.FUNCTION_TYPE__HAS_ELIPSIS:
				setHasElipsis((Boolean)newValue);
				return;
			case TypesPackage.FUNCTION_TYPE__INLINE:
				setInline((Boolean)newValue);
				return;
			case TypesPackage.FUNCTION_TYPE__NORETURN:
				setNoreturn((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TypesPackage.FUNCTION_TYPE__RETURN_TYPE:
				setReturnType((Type)null);
				return;
			case TypesPackage.FUNCTION_TYPE__INTERNAL_PARAMETER_WRAPERS:
				get__internal_parameter_wrapers().clear();
				return;
			case TypesPackage.FUNCTION_TYPE__HAS_ELIPSIS:
				setHasElipsis(HAS_ELIPSIS_EDEFAULT);
				return;
			case TypesPackage.FUNCTION_TYPE__INLINE:
				setInline(INLINE_EDEFAULT);
				return;
			case TypesPackage.FUNCTION_TYPE__NORETURN:
				setNoreturn(NORETURN_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TypesPackage.FUNCTION_TYPE__RETURN_TYPE:
				return returnType != null;
			case TypesPackage.FUNCTION_TYPE__INTERNAL_PARAMETER_WRAPERS:
				return __internal_parameter_wrapers != null && !__internal_parameter_wrapers.isEmpty();
			case TypesPackage.FUNCTION_TYPE__HAS_ELIPSIS:
				return hasElipsis != HAS_ELIPSIS_EDEFAULT;
			case TypesPackage.FUNCTION_TYPE__INLINE:
				return inline != INLINE_EDEFAULT;
			case TypesPackage.FUNCTION_TYPE__NORETURN:
				return noreturn != NORETURN_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //FunctionTypeImpl
