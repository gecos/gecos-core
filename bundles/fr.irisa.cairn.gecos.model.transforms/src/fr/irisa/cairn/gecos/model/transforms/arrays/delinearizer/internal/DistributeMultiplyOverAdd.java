package fr.irisa.cairn.gecos.model.transforms.arrays.delinearizer.internal;

import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;

/**
 * SampleModule is a sample module. When the script evaluator encounters the
 * 'Sample' function, it calls the compute method.
 */
public class DistributeMultiplyOverAdd extends BlockInstructionSwitch<Object> {

	Instruction inst;
	public DistributeMultiplyOverAdd(Instruction inst) {
		this.inst = inst;
	}

	public void compute() {
		doSwitch(this.inst);
	}

	@Override
	public Object caseGenericInstruction(GenericInstruction s) {
		if ( s.getName() != "mul" )
			return super.caseGenericInstruction(s);
		Instruction op1 = s.getChild(0);
		Instruction op2 = s.getChild(1);
		Instruction inst = null;
		if ( (op1 instanceof GenericInstruction) && 
				( (((GenericInstruction)op1).getName() == "add") || (((GenericInstruction)op1).getName() == "sub") ) )  {
			Instruction add_op1 = ((GenericInstruction)op1).getChild(0);
			Instruction add_op2 = ((GenericInstruction)op1).getChild(1);
			if ((((GenericInstruction)op1).getName() == "sub")  ) {
				add_op2 = GecosUserInstructionFactory.mul(add_op2.copy(), GecosUserInstructionFactory.Int(-1));
			}
			inst = GecosUserInstructionFactory.add(
					GecosUserInstructionFactory.mul(add_op1.copy(), op2.copy()), 
					GecosUserInstructionFactory.mul(add_op2.copy(), op2.copy()));
			s.getParent().replaceChild(s, inst);
			return doSwitch(inst);
		} else if ( (op2 instanceof GenericInstruction) && 
				( (((GenericInstruction)op2).getName() == "add") || (((GenericInstruction)op2).getName() == "sub") ) ) {
			Instruction add_op1 = ((GenericInstruction)op2).getChild(0);
			Instruction add_op2 = ((GenericInstruction)op2).getChild(1);
			if ((((GenericInstruction)op2).getName() == "sub")  ) {
				add_op2 = GecosUserInstructionFactory.mul(add_op2.copy(), GecosUserInstructionFactory.Int(-1));
			}
			inst = GecosUserInstructionFactory.add(
					GecosUserInstructionFactory.mul(add_op1.copy(), op1.copy()), 
					GecosUserInstructionFactory.mul(add_op2.copy(), op1.copy()));
			s.getParent().replaceChild(s, inst);
			return doSwitch(inst);
		} else {
			return super.caseGenericInstruction(s);
		}
		
	}
}