package fr.irisa.cairn.gecos.core.profiling.backend;

import gecos.annotations.impl.IAnnotationImpl;

public class SymbolProfilingAnnotation extends IAnnotationImpl {

	private ProfilingInfo<? extends Number> info;

	public SymbolProfilingAnnotation(ProfilingInfo<? extends Number> variableInfo) {
		super();
		this.info = variableInfo;
	}
	
	public ProfilingInfo<? extends Number> getInfo() {
		return info;
	}

}
