/**
 */
package gecos.dag;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see gecos.dag.DagPackage
 * @generated
 */
public interface DagFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DagFactory eINSTANCE = gecos.dag.impl.DagFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>DAG Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Instruction</em>'.
	 * @generated
	 */
	DAGInstruction createDAGInstruction();

	/**
	 * Returns a new object of class '<em>DAG In Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG In Port</em>'.
	 * @generated
	 */
	DAGInPort createDAGInPort();

	/**
	 * Returns a new object of class '<em>DAG Out Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Out Port</em>'.
	 * @generated
	 */
	DAGOutPort createDAGOutPort();

	/**
	 * Returns a new object of class '<em>DAG Symbol Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Symbol Node</em>'.
	 * @generated
	 */
	DAGSymbolNode createDAGSymbolNode();

	/**
	 * Returns a new object of class '<em>DAG Call Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Call Node</em>'.
	 * @generated
	 */
	DAGCallNode createDAGCallNode();

	/**
	 * Returns a new object of class '<em>DAG Array Value Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Array Value Node</em>'.
	 * @generated
	 */
	DAGArrayValueNode createDAGArrayValueNode();

	/**
	 * Returns a new object of class '<em>DAG Op Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Op Node</em>'.
	 * @generated
	 */
	DAGOpNode createDAGOpNode();

	/**
	 * Returns a new object of class '<em>DAG Out Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Out Node</em>'.
	 * @generated
	 */
	DAGOutNode createDAGOutNode();

	/**
	 * Returns a new object of class '<em>DAG Control Edge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Control Edge</em>'.
	 * @generated
	 */
	DAGControlEdge createDAGControlEdge();

	/**
	 * Returns a new object of class '<em>DAG Data Edge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Data Edge</em>'.
	 * @generated
	 */
	DAGDataEdge createDAGDataEdge();

	/**
	 * Returns a new object of class '<em>DAG Numbered Symbol Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Numbered Symbol Node</em>'.
	 * @generated
	 */
	DAGNumberedSymbolNode createDAGNumberedSymbolNode();

	/**
	 * Returns a new object of class '<em>DAG Numbered Out Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Numbered Out Node</em>'.
	 * @generated
	 */
	DAGNumberedOutNode createDAGNumberedOutNode();

	/**
	 * Returns a new object of class '<em>DAG Int Imm Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Int Imm Node</em>'.
	 * @generated
	 */
	DAGIntImmNode createDAGIntImmNode();

	/**
	 * Returns a new object of class '<em>DAG Float Imm Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Float Imm Node</em>'.
	 * @generated
	 */
	DAGFloatImmNode createDAGFloatImmNode();

	/**
	 * Returns a new object of class '<em>DAG String Imm Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG String Imm Node</em>'.
	 * @generated
	 */
	DAGStringImmNode createDAGStringImmNode();

	/**
	 * Returns a new object of class '<em>DAG Array Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Array Node</em>'.
	 * @generated
	 */
	DAGArrayNode createDAGArrayNode();

	/**
	 * Returns a new object of class '<em>DAG Simple Array Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Simple Array Node</em>'.
	 * @generated
	 */
	DAGSimpleArrayNode createDAGSimpleArrayNode();

	/**
	 * Returns a new object of class '<em>DAG Field Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Field Instruction</em>'.
	 * @generated
	 */
	DAGFieldInstruction createDAGFieldInstruction();

	/**
	 * Returns a new object of class '<em>DAG Jump Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Jump Node</em>'.
	 * @generated
	 */
	DAGJumpNode createDAGJumpNode();

	/**
	 * Returns a new object of class '<em>DAG Pattern Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Pattern Node</em>'.
	 * @generated
	 */
	DAGPatternNode createDAGPatternNode();

	/**
	 * Returns a new object of class '<em>DAG Vector Array Access Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Vector Array Access Node</em>'.
	 * @generated
	 */
	DAGVectorArrayAccessNode createDAGVectorArrayAccessNode();

	/**
	 * Returns a new object of class '<em>DAG Vector Op Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Vector Op Node</em>'.
	 * @generated
	 */
	DAGVectorOpNode createDAGVectorOpNode();

	/**
	 * Returns a new object of class '<em>DAG Vector Shuffle Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Vector Shuffle Node</em>'.
	 * @generated
	 */
	DAGVectorShuffleNode createDAGVectorShuffleNode();

	/**
	 * Returns a new object of class '<em>DAG Vector Extract Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Vector Extract Node</em>'.
	 * @generated
	 */
	DAGVectorExtractNode createDAGVectorExtractNode();

	/**
	 * Returns a new object of class '<em>DAG Vector Expand Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Vector Expand Node</em>'.
	 * @generated
	 */
	DAGVectorExpandNode createDAGVectorExpandNode();

	/**
	 * Returns a new object of class '<em>DAG Vector Pack Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Vector Pack Node</em>'.
	 * @generated
	 */
	DAGVectorPackNode createDAGVectorPackNode();

	/**
	 * Returns a new object of class '<em>DAGSSA Use Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAGSSA Use Node</em>'.
	 * @generated
	 */
	DAGSSAUseNode createDAGSSAUseNode();

	/**
	 * Returns a new object of class '<em>DAGSSA Def Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAGSSA Def Node</em>'.
	 * @generated
	 */
	DAGSSADefNode createDAGSSADefNode();

	/**
	 * Returns a new object of class '<em>DAG Phi Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DAG Phi Node</em>'.
	 * @generated
	 */
	DAGPhiNode createDAGPhiNode();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	DagPackage getDagPackage();

} //DagFactory
