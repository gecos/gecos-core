/**
 */
package gecos.dag;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Simple Array Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.dag.DagPackage#getDAGSimpleArrayNode()
 * @model
 * @generated
 */
public interface DAGSimpleArrayNode extends DAGSymbolNode, DagVisitable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGSimpleArrayNode(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getPredecessors().get(0);'"
	 * @generated
	 */
	DAGNode getIndex();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.dag.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _ident = this.getIdent();\nreturn (\"SAN_\" + &lt;%java.lang.Integer%&gt;.valueOf(_ident));'"
	 * @generated
	 */
	String toString();

} // DAGSimpleArrayNode
