struct s1{
	int x[1];
};

int main() {
	struct s1 a;
	struct s1{
		int x[10];
	};

	{
		struct s1 b;

		if (sizeof(a) != 1*sizeof(int))
			abort ();
		if (sizeof(b) != 10*sizeof(int))
			abort ();
	}

	exit (0);
}
