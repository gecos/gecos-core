/**
 */
package gecos.core;

import gecos.annotations.AnnotatedElement;

import gecos.instrs.Instruction;

import gecos.types.Type;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Symbol</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.core.Symbol#getType <em>Type</em>}</li>
 *   <li>{@link gecos.core.Symbol#getName <em>Name</em>}</li>
 *   <li>{@link gecos.core.Symbol#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see gecos.core.CorePackage#getSymbol()
 * @model
 * @generated
 */
public interface Symbol extends AnnotatedElement, ITypedElement, CoreVisitable {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(Type)
	 * @see gecos.core.CorePackage#getSymbol_Type()
	 * @model
	 * @generated
	 */
	Type getType();

	/**
	 * Sets the value of the '{@link gecos.core.Symbol#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Type value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see gecos.core.CorePackage#getSymbol_Name()
	 * @model unique="false" dataType="gecos.core.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link gecos.core.Symbol#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(Instruction)
	 * @see gecos.core.CorePackage#getSymbol_Value()
	 * @model containment="true"
	 * @generated
	 */
	Instruction getValue();

	/**
	 * Sets the value of the '{@link gecos.core.Symbol#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Instruction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.core.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _name = this.getName();\n&lt;%java.lang.String%&gt; _plus = (_name + \":\");\n&lt;%java.lang.Object%&gt; _elvis = null;\n&lt;%gecos.types.Type%&gt; _type = this.getType();\nif (_type != null)\n{\n\t_elvis = _type;\n} else\n{\n\t_elvis = \"?\";\n}\nreturn (_plus + _elvis);'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%fr.irisa.cairn.gecos.model.tools.utils.GecosCopier%&gt; gecosCopier = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.GecosCopier%&gt;();\n&lt;%org.eclipse.emf.ecore.EObject%&gt; _copy = gecosCopier.copy(this);\nfinal T symbol = ((T) _copy);\ngecosCopier.copyReferences();\nreturn symbol;'"
	 * @generated
	 */
	<T extends Symbol> T copy();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.ecore.EObject%&gt; c = this.eContainer();\nif ((c instanceof &lt;%gecos.core.Scope%&gt;))\n{\n\treturn ((&lt;%gecos.core.Scope%&gt;)c);\n}\nreturn null;'"
	 * @generated
	 */
	Scope getContainingScope();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitSymbol(this);'"
	 * @generated
	 */
	void accept(CoreVisitor visitor);

} // Symbol
