/**
 */
package gecos.types;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Overflow Mode</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see gecos.types.TypesPackage#getOverflowMode()
 * @model
 * @generated
 */
public enum OverflowMode implements Enumerator {
	/**
	 * The '<em><b>AC WRAP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AC_WRAP_VALUE
	 * @generated
	 * @ordered
	 */
	AC_WRAP(0, "AC_WRAP", "AC_WRAP"),

	/**
	 * The '<em><b>AC SAT SYM</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AC_SAT_SYM_VALUE
	 * @generated
	 * @ordered
	 */
	AC_SAT_SYM(1, "AC_SAT_SYM", "AC_SAT_SYM"),

	/**
	 * The '<em><b>AC SAT ZERO</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AC_SAT_ZERO_VALUE
	 * @generated
	 * @ordered
	 */
	AC_SAT_ZERO(2, "AC_SAT_ZERO", "AC_SAT_ZERO"),

	/**
	 * The '<em><b>AC SAT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AC_SAT_VALUE
	 * @generated
	 * @ordered
	 */
	AC_SAT(3, "AC_SAT", "AC_SAT");

	/**
	 * The '<em><b>AC WRAP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>AC WRAP</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AC_WRAP
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int AC_WRAP_VALUE = 0;

	/**
	 * The '<em><b>AC SAT SYM</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>AC SAT SYM</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AC_SAT_SYM
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int AC_SAT_SYM_VALUE = 1;

	/**
	 * The '<em><b>AC SAT ZERO</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>AC SAT ZERO</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AC_SAT_ZERO
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int AC_SAT_ZERO_VALUE = 2;

	/**
	 * The '<em><b>AC SAT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>AC SAT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AC_SAT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int AC_SAT_VALUE = 3;

	/**
	 * An array of all the '<em><b>Overflow Mode</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final OverflowMode[] VALUES_ARRAY =
		new OverflowMode[] {
			AC_WRAP,
			AC_SAT_SYM,
			AC_SAT_ZERO,
			AC_SAT,
		};

	/**
	 * A public read-only list of all the '<em><b>Overflow Mode</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<OverflowMode> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Overflow Mode</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static OverflowMode get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			OverflowMode result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Overflow Mode</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static OverflowMode getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			OverflowMode result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Overflow Mode</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static OverflowMode get(int value) {
		switch (value) {
			case AC_WRAP_VALUE: return AC_WRAP;
			case AC_SAT_SYM_VALUE: return AC_SAT_SYM;
			case AC_SAT_ZERO_VALUE: return AC_SAT_ZERO;
			case AC_SAT_VALUE: return AC_SAT;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private OverflowMode(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //OverflowMode
