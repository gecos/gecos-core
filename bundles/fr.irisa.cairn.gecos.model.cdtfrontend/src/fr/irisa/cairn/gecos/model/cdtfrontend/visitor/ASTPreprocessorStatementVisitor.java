/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.cdtfrontend.visitor;

import org.eclipse.cdt.core.dom.ast.IASTPreprocessorElifStatement;
import org.eclipse.cdt.core.dom.ast.IASTPreprocessorElseStatement;
import org.eclipse.cdt.core.dom.ast.IASTPreprocessorEndifStatement;
import org.eclipse.cdt.core.dom.ast.IASTPreprocessorErrorStatement;
import org.eclipse.cdt.core.dom.ast.IASTPreprocessorFunctionStyleMacroDefinition;
import org.eclipse.cdt.core.dom.ast.IASTPreprocessorIfStatement;
import org.eclipse.cdt.core.dom.ast.IASTPreprocessorIfdefStatement;
import org.eclipse.cdt.core.dom.ast.IASTPreprocessorIfndefStatement;
import org.eclipse.cdt.core.dom.ast.IASTPreprocessorIncludeStatement;
import org.eclipse.cdt.core.dom.ast.IASTPreprocessorMacroDefinition;
import org.eclipse.cdt.core.dom.ast.IASTPreprocessorObjectStyleMacroDefinition;
import org.eclipse.cdt.core.dom.ast.IASTPreprocessorPragmaStatement;
import org.eclipse.cdt.core.dom.ast.IASTPreprocessorStatement;
import org.eclipse.cdt.core.dom.ast.IASTPreprocessorUndefStatement;

import fr.irisa.cairn.gecos.model.cdtfrontend.exception.CDTParserNotYetSupportedException;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import gecos.annotations.IAnnotation;

public class ASTPreprocessorStatementVisitor {

	private IAnnotation annotation;
	
	public ASTPreprocessorStatementVisitor() {
	}

	public void visit(CDTToGecosModel gecosModel, IASTPreprocessorStatement preprocessorStatement) {
		if (preprocessorStatement instanceof IASTPreprocessorElifStatement)
			buildIASTPreprocessorElifStatement((IASTPreprocessorElifStatement)preprocessorStatement);
		else if (preprocessorStatement instanceof IASTPreprocessorElseStatement)
			buildIASTPreprocessorElseStatement((IASTPreprocessorElseStatement)preprocessorStatement);
		else if (preprocessorStatement instanceof IASTPreprocessorEndifStatement)
			buildIASTPreprocessorEndifStatement((IASTPreprocessorEndifStatement)preprocessorStatement);
		else if (preprocessorStatement instanceof IASTPreprocessorErrorStatement)
			buildIASTPreprocessorErrorStatement((IASTPreprocessorErrorStatement)preprocessorStatement);
		else if (preprocessorStatement instanceof IASTPreprocessorFunctionStyleMacroDefinition)
			buildIASTPreprocessorFunctionStyleMacroDefinition((IASTPreprocessorFunctionStyleMacroDefinition)preprocessorStatement);
		else if (preprocessorStatement instanceof IASTPreprocessorIfdefStatement)
			buildIASTPreprocessorIfdefStatement((IASTPreprocessorIfdefStatement)preprocessorStatement);
		else if (preprocessorStatement instanceof IASTPreprocessorIfndefStatement)
			buildIASTPreprocessorIfndefStatement((IASTPreprocessorIfndefStatement)preprocessorStatement);
		else if (preprocessorStatement instanceof IASTPreprocessorIfStatement)
			buildIASTPreprocessorIfStatement((IASTPreprocessorIfStatement)preprocessorStatement);
		else if (preprocessorStatement instanceof IASTPreprocessorIncludeStatement)
			buildIASTPreprocessorIncludeStatement((IASTPreprocessorIncludeStatement)preprocessorStatement);
		else if (preprocessorStatement instanceof IASTPreprocessorMacroDefinition)
			buildIASTPreprocessorMacroDefinition((IASTPreprocessorMacroDefinition)preprocessorStatement);
		else if (preprocessorStatement instanceof IASTPreprocessorObjectStyleMacroDefinition)
			buildIASTPreprocessorObjectStyleMacroDefinition((IASTPreprocessorObjectStyleMacroDefinition)preprocessorStatement);
		else if (preprocessorStatement instanceof IASTPreprocessorPragmaStatement)
			buildIASTPreprocessorPragmaStatement((IASTPreprocessorPragmaStatement)preprocessorStatement);
		else if (preprocessorStatement instanceof IASTPreprocessorUndefStatement)
			buildIASTPreprocessorUndefStatement((IASTPreprocessorUndefStatement)preprocessorStatement);
		if (!gecosModel.visitingInclude())
			gecosModel.addIASTAnnotation(annotation, preprocessorStatement);
	}
	
	public IAnnotation getAnnotation() {
		return annotation;
	}

	private void buildIASTPreprocessorElifStatement(IASTPreprocessorElifStatement preprocessorStatement) {
		notYetSupported(preprocessorStatement);
	}

	private void buildIASTPreprocessorElseStatement(IASTPreprocessorElseStatement preprocessorStatement) {
		notYetSupported(preprocessorStatement);
	}

	private void buildIASTPreprocessorEndifStatement(IASTPreprocessorEndifStatement preprocessorStatement) {
		notYetSupported(preprocessorStatement);
	}

	private void buildIASTPreprocessorErrorStatement(IASTPreprocessorErrorStatement preprocessorStatement) {
		notYetSupported(preprocessorStatement);
	}

	private void buildIASTPreprocessorFunctionStyleMacroDefinition(IASTPreprocessorFunctionStyleMacroDefinition preprocessorStatement) {
		notYetSupported(preprocessorStatement);
	}

	private void buildIASTPreprocessorIfdefStatement(IASTPreprocessorIfdefStatement preprocessorStatement) {
		notYetSupported(preprocessorStatement);
	}

	private void buildIASTPreprocessorIfndefStatement(IASTPreprocessorIfndefStatement preprocessorStatement) {
		notYetSupported(preprocessorStatement);
	}

	private void buildIASTPreprocessorIfStatement(IASTPreprocessorIfStatement preprocessorStatement) {
		notYetSupported(preprocessorStatement);
	}

	private void buildIASTPreprocessorIncludeStatement(IASTPreprocessorIncludeStatement preprocessorStatement) {
		notYetSupported(preprocessorStatement);
	}

	private void buildIASTPreprocessorMacroDefinition(IASTPreprocessorMacroDefinition preprocessorStatement) {
		notYetSupported(preprocessorStatement);
	}

	private void buildIASTPreprocessorObjectStyleMacroDefinition(IASTPreprocessorObjectStyleMacroDefinition preprocessorStatement) {
		notYetSupported(preprocessorStatement);
	}

	private void buildIASTPreprocessorPragmaStatement(IASTPreprocessorPragmaStatement preprocessorStatement) {
		annotation = GecosUserAnnotationFactory.createPragma(String.valueOf(preprocessorStatement.getMessage()));
	}

	private void buildIASTPreprocessorUndefStatement(IASTPreprocessorUndefStatement preprocessorStatement) {
		notYetSupported(preprocessorStatement);
	}
	
	private static void notYetSupported(IASTPreprocessorStatement preprocessorStatement) {
		throw new CDTParserNotYetSupportedException(preprocessorStatement, "Support for "+preprocessorStatement.getClass().getSimpleName()+ ":" +preprocessorStatement.getRawSignature()+ " not yet implemented ");
	}
}