/**
 */
package gecos.core;

import fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;

import gecos.types.Type;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scope</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.core.Scope#getTypes <em>Types</em>}</li>
 *   <li>{@link gecos.core.Scope#getSymbols <em>Symbols</em>}</li>
 *   <li>{@link gecos.core.Scope#getContainer <em>Container</em>}</li>
 *   <li>{@link gecos.core.Scope#getParent <em>Parent</em>}</li>
 *   <li>{@link gecos.core.Scope#getChildren <em>Children</em>}</li>
 * </ul>
 *
 * @see gecos.core.CorePackage#getScope()
 * @model
 * @generated
 */
public interface Scope extends CoreVisitable {
	/**
	 * Returns the value of the '<em><b>Types</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.types.Type}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Types</em>' containment reference list.
	 * @see gecos.core.CorePackage#getScope_Types()
	 * @model containment="true"
	 * @generated
	 */
	EList<Type> getTypes();

	/**
	 * Returns the value of the '<em><b>Symbols</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.core.Symbol}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Symbols</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbols</em>' containment reference list.
	 * @see gecos.core.CorePackage#getScope_Symbols()
	 * @model containment="true"
	 * @generated
	 */
	EList<Symbol> getSymbols();

	/**
	 * Returns the value of the '<em><b>Container</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link gecos.core.ScopeContainer#getScope <em>Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Container</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Container</em>' container reference.
	 * @see #setContainer(ScopeContainer)
	 * @see gecos.core.CorePackage#getScope_Container()
	 * @see gecos.core.ScopeContainer#getScope
	 * @model opposite="scope" transient="false"
	 * @generated
	 */
	ScopeContainer getContainer();

	/**
	 * Sets the value of the '{@link gecos.core.Scope#getContainer <em>Container</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Container</em>' container reference.
	 * @see #getContainer()
	 * @generated
	 */
	void setContainer(ScopeContainer value);

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link gecos.core.Scope#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' reference.
	 * @see gecos.core.CorePackage#getScope_Parent()
	 * @see gecos.core.Scope#getChildren
	 * @model opposite="children" resolveProxies="false" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='&lt;%org.eclipse.emf.ecore.EObject%&gt; current = this.eContainer();\nwhile ((current != null))\n{\n\t&lt;%org.eclipse.emf.ecore.EObject%&gt; _eContainer = current.eContainer();\n\tboolean _tripleNotEquals = (_eContainer != null);\n\tif (_tripleNotEquals)\n\t{\n\t\t&lt;%org.eclipse.emf.ecore.EObject%&gt; _eContainer_1 = current.eContainer();\n\t\tif ((_eContainer_1 instanceof &lt;%gecos.core.ScopeContainer%&gt;))\n\t\t{\n\t\t\t&lt;%org.eclipse.emf.ecore.EObject%&gt; _eContainer_2 = current.eContainer();\n\t\t\tfinal &lt;%gecos.core.Scope%&gt; scope = ((&lt;%gecos.core.ScopeContainer%&gt;) _eContainer_2).getScope();\n\t\t\tif ((scope != null))\n\t\t\t{\n\t\t\t\treturn scope;\n\t\t\t}\n\t\t}\n\t\tcurrent = current.eContainer();\n\t}\n\telse\n\t{\n\t\treturn null;\n\t}\n}\nreturn null;'"
	 * @generated
	 */
	Scope getParent();

	/**
	 * Returns the value of the '<em><b>Children</b></em>' reference list.
	 * The list contents are of type {@link gecos.core.Scope}.
	 * It is bidirectional and its opposite is '{@link gecos.core.Scope#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Children</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' reference list.
	 * @see gecos.core.CorePackage#getScope_Children()
	 * @see gecos.core.Scope#getParent
	 * @model opposite="parent" resolveProxies="false" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='&lt;%gecos.core.ScopeContainer%&gt; _container = this.getContainer();\n&lt;%org.eclipse.emf.ecore.EObject%&gt; current = ((&lt;%org.eclipse.emf.ecore.EObject%&gt;) _container);\nif ((current != null))\n{\n\tfinal &lt;%org.eclipse.emf.common.util.TreeIterator%&gt;&lt;&lt;%org.eclipse.emf.ecore.EObject%&gt;&gt; i = current.eAllContents();\n\tfinal &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%gecos.core.Scope%&gt;&gt; res = new &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%gecos.core.Scope%&gt;&gt;();\n\twhile (i.hasNext())\n\t{\n\t\t{\n\t\t\tcurrent = i.next();\n\t\t\tif ((current instanceof &lt;%gecos.core.ScopeContainer%&gt;))\n\t\t\t{\n\t\t\t\tres.add(((&lt;%gecos.core.ScopeContainer%&gt;)current).getScope());\n\t\t\t\ti.prune();\n\t\t\t}\n\t\t}\n\t}\n\treturn res;\n}\nreturn null;'"
	 * @generated
	 */
	EList<Scope> getChildren();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.core.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.core.ScopeContainer%&gt; _container = this.getContainer();\n&lt;%java.lang.String%&gt; _plus = (\"Scope(\" + _container);\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \") {\");\n&lt;%java.lang.String%&gt; _join = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.join(this.getSymbols(), \", \");\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + _join);\nreturn (_plus_2 + \"}\");'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" managerDataType="gecos.core.BlockCopyManager" managerUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.core.Scope%&gt; scope = &lt;%gecos.core.CoreFactory%&gt;.eINSTANCE.createScope();\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.core.Symbol%&gt;&gt; _symbols = this.getSymbols();\nfor (final &lt;%gecos.core.Symbol%&gt; sym : _symbols)\n{\n\tif ((!(sym instanceof &lt;%gecos.core.ProcedureSymbol%&gt;)))\n\t{\n\t\tfinal &lt;%gecos.core.Symbol%&gt; newSymbol = sym.&lt;&lt;%gecos.core.Symbol%&gt;&gt;copy();\n\t\tscope.getSymbols().add(newSymbol);\n\t\tmanager.link(sym, newSymbol);\n\t}\n}\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.types.Type%&gt;&gt; _types = this.getTypes();\nfor (final &lt;%gecos.types.Type%&gt; type : _types)\n{\n\t{\n\t\tfinal &lt;%gecos.types.Type%&gt; newType = type.&lt;&lt;%gecos.types.Type%&gt;&gt;copy();\n\t\tscope.getTypes().add(newType);\n\t\tmanager.link(type, newType);\n\t}\n}\nmanager.link(this, scope);\nreturn scope;'"
	 * @generated
	 */
	Scope managedCopy(BlockCopyManager manager);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%fr.irisa.cairn.gecos.model.tools.utils.GecosCopier%&gt; gecosCopier = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.GecosCopier%&gt;();\n&lt;%org.eclipse.emf.ecore.EObject%&gt; _copy = gecosCopier.copy(this);\nfinal &lt;%gecos.core.Scope%&gt; scope = ((&lt;%gecos.core.Scope%&gt;) _copy);\ngecosCopier.copyReferences();\nreturn scope;'"
	 * @generated
	 */
	Scope copy();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" nameDataType="gecos.core.String" nameUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.types.Type%&gt;&gt; _types = this.getTypes();\nfor (final &lt;%gecos.types.Type%&gt; t : _types)\n{\n\t{\n\t\tif ((t instanceof &lt;%gecos.types.RecordType%&gt;))\n\t\t{\n\t\t\t&lt;%java.lang.String%&gt; _name = ((&lt;%gecos.types.RecordType%&gt;)t).getName();\n\t\t\tboolean _equals = &lt;%com.google.common.base.Objects%&gt;.equal(_name, name);\n\t\t\tif (_equals)\n\t\t\t{\n\t\t\t\treturn t;\n\t\t\t}\n\t\t}\n\t\tif ((t instanceof &lt;%gecos.types.EnumType%&gt;))\n\t\t{\n\t\t\t&lt;%java.lang.String%&gt; _name_1 = ((&lt;%gecos.types.EnumType%&gt;)t).getName();\n\t\t\tboolean _equals_1 = &lt;%com.google.common.base.Objects%&gt;.equal(_name_1, name);\n\t\t\tif (_equals_1)\n\t\t\t{\n\t\t\t\treturn t;\n\t\t\t}\n\t\t}\n\t\tif ((t instanceof &lt;%gecos.types.AliasType%&gt;))\n\t\t{\n\t\t\t&lt;%java.lang.String%&gt; _name_2 = ((&lt;%gecos.types.AliasType%&gt;)t).getName();\n\t\t\tboolean _equals_2 = &lt;%com.google.common.base.Objects%&gt;.equal(_name_2, name);\n\t\t\tif (_equals_2)\n\t\t\t{\n\t\t\t\treturn t;\n\t\t\t}\n\t\t}\n\t}\n}\n&lt;%gecos.core.Scope%&gt; _parent = this.getParent();\n&lt;%gecos.types.Type%&gt; _lookupType = null;\nif (_parent!=null)\n{\n\t_lookupType=_parent.lookupType(name);\n}\nreturn _lookupType;'"
	 * @generated
	 */
	Type lookupType(String name);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" nameDataType="gecos.core.String" nameUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.Type%&gt; _elvis = null;\nfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.types.AliasType%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.types.AliasType%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%gecos.types.AliasType%&gt; it)\n\t{\n\t\t&lt;%java.lang.String%&gt; _name = it.getName();\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf(&lt;%com.google.common.base.Objects%&gt;.equal(_name, name));\n\t}\n};\n&lt;%gecos.types.AliasType%&gt; _findFirst = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.types.AliasType%&gt;&gt;findFirst(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%gecos.types.AliasType%&gt;&gt;filter(this.getTypes(), &lt;%gecos.types.AliasType%&gt;.class), _function);\nif (_findFirst != null)\n{\n\t_elvis = _findFirst;\n} else\n{\n\t&lt;%gecos.core.Scope%&gt; _parent = this.getParent();\n\t&lt;%gecos.types.Type%&gt; _lookupAliasType = null;\n\tif (_parent!=null)\n\t{\n\t\t_lookupAliasType=_parent.lookupAliasType(name);\n\t}\n\t_elvis = _lookupAliasType;\n}\nreturn _elvis;'"
	 * @generated
	 */
	Type lookupAliasType(String name);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" nameDataType="gecos.core.String" nameUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.Type%&gt; _elvis = null;\nfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.types.EnumType%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.types.EnumType%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%gecos.types.EnumType%&gt; it)\n\t{\n\t\t&lt;%java.lang.String%&gt; _name = it.getName();\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf(&lt;%com.google.common.base.Objects%&gt;.equal(_name, name));\n\t}\n};\n&lt;%gecos.types.EnumType%&gt; _findFirst = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.types.EnumType%&gt;&gt;findFirst(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%gecos.types.EnumType%&gt;&gt;filter(this.getTypes(), &lt;%gecos.types.EnumType%&gt;.class), _function);\nif (_findFirst != null)\n{\n\t_elvis = _findFirst;\n} else\n{\n\t&lt;%gecos.core.Scope%&gt; _parent = this.getParent();\n\t&lt;%gecos.types.Type%&gt; _lookupEnumType = null;\n\tif (_parent!=null)\n\t{\n\t\t_lookupEnumType=_parent.lookupEnumType(name);\n\t}\n\t_elvis = _lookupEnumType;\n}\nreturn _elvis;'"
	 * @generated
	 */
	Type lookupEnumType(String name);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" nameDataType="gecos.core.String" nameUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.Type%&gt; _elvis = null;\nfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.types.RecordType%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.types.RecordType%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%gecos.types.RecordType%&gt; it)\n\t{\n\t\t&lt;%java.lang.String%&gt; _name = it.getName();\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf(&lt;%com.google.common.base.Objects%&gt;.equal(_name, name));\n\t}\n};\n&lt;%gecos.types.RecordType%&gt; _findFirst = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.types.RecordType%&gt;&gt;findFirst(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%gecos.types.RecordType%&gt;&gt;filter(this.getTypes(), &lt;%gecos.types.RecordType%&gt;.class), _function);\nif (_findFirst != null)\n{\n\t_elvis = _findFirst;\n} else\n{\n\t&lt;%gecos.core.Scope%&gt; _parent = this.getParent();\n\t&lt;%gecos.types.Type%&gt; _lookupRecordType = null;\n\tif (_parent!=null)\n\t{\n\t\t_lookupRecordType=_parent.lookupRecordType(name);\n\t}\n\t_elvis = _lookupRecordType;\n}\nreturn _elvis;'"
	 * @generated
	 */
	Type lookupRecordType(String name);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" typUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.Type%&gt; _elvis = null;\nfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.types.Type%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.types.Type%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%gecos.types.Type%&gt; it)\n\t{\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf(it.isEqual(typ));\n\t}\n};\n&lt;%gecos.types.Type%&gt; _findFirst = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.types.Type%&gt;&gt;findFirst(this.getTypes(), _function);\nif (_findFirst != null)\n{\n\t_elvis = _findFirst;\n} else\n{\n\t&lt;%gecos.core.Scope%&gt; _parent = this.getParent();\n\t&lt;%gecos.types.Type%&gt; _lookup = null;\n\tif (_parent!=null)\n\t{\n\t\t_lookup=_parent.lookup(typ);\n\t}\n\t_elvis = _lookup;\n}\nreturn _elvis;'"
	 * @generated
	 */
	Type lookup(Type typ);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model symUnique="false" refUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.core.Symbol%&gt; _lookup = ref.lookup(sym.getName());\nboolean _tripleNotEquals = (_lookup != null);\nif (_tripleNotEquals)\n{\n\tfinal &lt;%java.lang.String%&gt; name = sym.getName();\n\tint counter = 0;\n\twhile ((ref.lookup((name + &lt;%java.lang.Integer%&gt;.valueOf(counter))) != null))\n\t{\n\t\tcounter++;\n\t}\n\tsym.setName((name + &lt;%java.lang.Integer%&gt;.valueOf(counter)));\n}'"
	 * @generated
	 */
	void makeUnique(Symbol sym, Scope ref);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model symUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.makeUnique(sym, this);'"
	 * @generated
	 */
	void makeUnique(Symbol sym);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model scopeUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((scope != this))\n{\n\tfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.core.Symbol%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.core.Symbol%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n\t{\n\t\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%gecos.core.Symbol%&gt; it)\n\t\t{\n\t\t\t&lt;%gecos.core.Symbol%&gt; _lookup = &lt;%this%&gt;.lookup(it.getName());\n\t\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf((_lookup != null));\n\t\t}\n\t};\n\tfinal &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.core.Symbol%&gt;&gt; _function_1 = new &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.core.Symbol%&gt;&gt;()\n\t{\n\t\tpublic void accept(final &lt;%gecos.core.Symbol%&gt; it)\n\t\t{\n\t\t\t&lt;%this%&gt;.makeUnique(it);\n\t\t}\n\t};\n\t&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.core.Symbol%&gt;&gt;filter(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%gecos.core.Symbol%&gt;&gt;immutableCopy(scope.getSymbols()), _function).forEach(_function_1);\n\tthis.getSymbols().addAll(scope.getSymbols());\n\tthis.getTypes().addAll(scope.getTypes());\n}'"
	 * @generated
	 */
	void mergeWith(Scope scope);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" scopeUnique="false"
	 * @generated
	 */
	Scope copy(Scope scope);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 *  Symbol lookup from the symbol name (recursive in Scope hierarchy)
	 * <!-- end-model-doc -->
	 * @model unique="false" nameDataType="gecos.core.String" nameUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.core.Symbol%&gt; _elvis = null;\nfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.core.Symbol%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.core.Symbol%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%gecos.core.Symbol%&gt; it)\n\t{\n\t\t&lt;%java.lang.String%&gt; _name = it.getName();\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf(&lt;%com.google.common.base.Objects%&gt;.equal(_name, name));\n\t}\n};\n&lt;%gecos.core.Symbol%&gt; _findFirst = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.core.Symbol%&gt;&gt;findFirst(this.getSymbols(), _function);\nif (_findFirst != null)\n{\n\t_elvis = _findFirst;\n} else\n{\n\t&lt;%gecos.core.Scope%&gt; _parent = this.getParent();\n\t&lt;%gecos.core.Symbol%&gt; _lookup = null;\n\tif (_parent!=null)\n\t{\n\t\t_lookup=_parent.lookup(name);\n\t}\n\t_elvis = _lookup;\n}\nreturn _elvis;'"
	 * @generated
	 */
	Symbol lookup(String name);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 *  @return true if {@code symbol} exists in the scope hierarchy
	 * <!-- end-model-doc -->
	 * @model unique="false" symbolUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _xifexpression = false;\nfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.core.Symbol%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.core.Symbol%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%gecos.core.Symbol%&gt; it)\n\t{\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf(&lt;%com.google.common.base.Objects%&gt;.equal(it, symbol));\n\t}\n};\nboolean _exists = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.core.Symbol%&gt;&gt;exists(this.getSymbols(), _function);\nif (_exists)\n{\n\t_xifexpression = true;\n}\nelse\n{\n\tboolean _xifexpression_1 = false;\n\t&lt;%gecos.core.Scope%&gt; _parent = this.getParent();\n\tboolean _tripleNotEquals = (_parent != null);\n\tif (_tripleNotEquals)\n\t{\n\t\t_xifexpression_1 = this.getParent().hasInScope(symbol);\n\t}\n\telse\n\t{\n\t\t_xifexpression_1 = false;\n\t}\n\t_xifexpression = _xifexpression_1;\n}\nreturn _xifexpression;'"
	 * @generated
	 */
	boolean hasInScope(Symbol symbol);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 *  Move symbol from this to parent scope
	 * <!-- end-model-doc -->
	 * @model symUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.core.Scope%&gt; _parent = this.getParent();\nboolean _tripleNotEquals = (_parent != null);\nif (_tripleNotEquals)\n{\n\tthis.getSymbols().remove(sym);\n\tthis.getParent().getSymbols().add(sym);\n}\nelse\n{\n\tthrow new &lt;%java.lang.RuntimeException%&gt;(((\"Cannot move Symbol \" + sym) + \" because there is no parent scope\"));\n}'"
	 * @generated
	 */
	void moveUp(Symbol sym);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 *  @return the root Scope i.e. the first non-null Scope in the parents hierarchy
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.core.Scope%&gt; current = this;\nwhile ((current.getParent() != null))\n{\n\tcurrent = current.getParent();\n}\nreturn current;'"
	 * @generated
	 */
	Scope getRoot();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model symbolUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getSymbols().add(symbol);'"
	 * @generated
	 */
	void addSymbol(Symbol symbol);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model symbolUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getSymbols().remove(symbol);'"
	 * @generated
	 */
	void removeSymbol(Symbol symbol);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitScope(this);'"
	 * @generated
	 */
	void accept(CoreVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 *  @return true has no parent i.e. {@code getRoot() == this}
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.core.Scope%&gt; _parent = this.getParent();\nreturn (_parent == null);'"
	 * @generated
	 */
	boolean isGlobal();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" aUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%gecos.core.Scope%&gt;&gt; thisParents = new &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%gecos.core.Scope%&gt;&gt;();\n&lt;%gecos.core.Scope%&gt; current = this;\nwhile ((current != null))\n{\n\t{\n\t\tthisParents.add(current);\n\t\tcurrent = current.getParent();\n\t}\n}\ncurrent = a;\nwhile (((current != null) &amp;&amp; (!thisParents.contains(current))))\n{\n\tcurrent = current.getParent();\n}\nreturn current;'"
	 * @generated
	 */
	Scope findFirstCommonScopeWith(Scope a);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%java.util.LinkedHashMap%&gt;&lt;&lt;%java.lang.String%&gt;, &lt;%gecos.core.Symbol%&gt;&gt; m = new &lt;%java.util.LinkedHashMap%&gt;&lt;&lt;%java.lang.String%&gt;, &lt;%gecos.core.Symbol%&gt;&gt;();\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.core.Symbol%&gt;&gt; _symbols = this.getSymbols();\nfor (final &lt;%gecos.core.Symbol%&gt; s : _symbols)\n{\n\tm.put(s.getName(), s);\n}\n&lt;%gecos.core.Scope%&gt; _parent = this.getParent();\nboolean _tripleNotEquals = (_parent != null);\nif (_tripleNotEquals)\n{\n\tfinal &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.core.Symbol%&gt;&gt; upperScopeSymbols = this.getParent().lookupAllSymbols();\n\tfor (final &lt;%gecos.core.Symbol%&gt; s_1 : upperScopeSymbols)\n\t{\n\t\tboolean _containsKey = m.containsKey(s_1.getName());\n\t\tboolean _not = (!_containsKey);\n\t\tif (_not)\n\t\t{\n\t\t\tm.put(s_1.getName(), s_1);\n\t\t}\n\t}\n}\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.core.Symbol%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.core.Symbol%&gt;&gt;asEList(((&lt;%gecos.core.Symbol%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(m.values(), &lt;%gecos.core.Symbol%&gt;.class))));'"
	 * @generated
	 */
	EList<Symbol> lookupAllSymbols();

} // Scope
