package fr.irisa.cairn.gecos.model.type.utils;

abstract public class Declarator {

	
	String pointers;
	String arrays_params; //arrays or params
	
	public Declarator(String pointers, String arraysOrParams) {
		this.pointers = pointers;
		this.arrays_params = arraysOrParams;
	}
	
	public String getPointers() {
		return pointers;
	}

	public String getArraysOrFunction() {
		return arrays_params;
	}
	
	abstract public String getCCode();
	
	abstract public String getIdentifier();

	@Override
	public String toString() {
		return getCCode();
	}

}
