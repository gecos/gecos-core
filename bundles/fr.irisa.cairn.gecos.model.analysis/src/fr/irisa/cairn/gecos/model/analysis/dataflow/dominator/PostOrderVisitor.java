/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.analysis.dataflow.dominator;

import java.util.Vector;

import fr.irisa.cairn.gecos.model.tools.utils.DepthFirstVisitor;
import fr.irisa.cairn.gecos.model.tools.utils.IGraphNode;
import gecos.blocks.BasicBlock;



public class PostOrderVisitor extends DepthFirstVisitor {

	private Vector<BasicBlock> stack;

	public PostOrderVisitor() {
		super();
	}

	public void processAfter(IGraphNode node) {
		stack.add(((DominatorTreeNode)node).block());
	}

	public BasicBlock[] process(IGraphNode node) {
		stack = new Vector<BasicBlock>();
		visit(node);
		return (BasicBlock[]) stack.toArray(new BasicBlock[stack.size()]);
	}
}
