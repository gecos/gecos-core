package fr.irisa.cairn.gecos.model.dag.adapter.impl;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import fr.irisa.cairn.gecos.model.dag.adapter.model.IAdaptedContainer;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGEdgeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGInstructionAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGNodeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGPortAdapter;
import gecos.dag.DAGEdge;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGNode;
import gecos.dag.DAGPort;

/**
 * implementation of the container.
 * @author <your name>
 *
 */
public class AdaptedContainerImpl implements IAdaptedContainer {

	private Map<DAGInstruction, IDAGInstructionAdapter> graphsMap;

	private Map<DAGNode, IDAGNodeAdapter> nodesMap;
	private Map<DAGPort, IDAGPortAdapter> portsMap;
	private Map<DAGEdge, IDAGEdgeAdapter> edgesMap;

	private IAdaptedContainer parent;

	public AdaptedContainerImpl() {
		super();
		this.graphsMap = new HashMap<DAGInstruction, IDAGInstructionAdapter>();
		this.nodesMap = new HashMap<DAGNode, IDAGNodeAdapter>();
		this.portsMap = new HashMap<DAGPort, IDAGPortAdapter>();

		this.edgesMap = new HashMap<DAGEdge, IDAGEdgeAdapter>();
		this.parent = null;
	}

	public IAdaptedContainer getParent() {
		return this.parent;
	}

	public void setParent(IAdaptedContainer parent) {
		this.parent = parent;
		for (Iterator<IDAGInstructionAdapter> iterator = this.graphsMap
				.values().iterator(); iterator.hasNext();) {
			IDAGInstructionAdapter graphAdapter = iterator.next();
			parent.addGraph(graphAdapter);
		}
		this.graphsMap.clear();
		for (Iterator<IDAGNodeAdapter> iterator = this.nodesMap.values()
				.iterator(); iterator.hasNext();) {
			IDAGNodeAdapter nodeAdapter = iterator.next();
			parent.addNode(nodeAdapter);
		}
		this.nodesMap.clear();

		for (Iterator<IDAGPortAdapter> iterator = this.portsMap.values()
				.iterator(); iterator.hasNext();) {
			IDAGPortAdapter portAdapter = iterator.next();
			parent.addPort(portAdapter);
		}
		this.portsMap.clear();

		for (Iterator<IDAGEdgeAdapter> iterator = this.edgesMap.values()
				.iterator(); iterator.hasNext();) {
			IDAGEdgeAdapter edgeAdapter = iterator.next();
			parent.addEdge(edgeAdapter);
		}
		this.edgesMap.clear();

	}

	public IAdaptedContainer getRoot() {
		IAdaptedContainer root;
		if (this.isRoot()) {
			root = this;
		} else {
			root = this.getParent().getRoot();
		}
		return root;
	}

	private boolean isRoot() {
		return this.parent == null;
	}

	public IDAGInstructionAdapter getAdapterGraph(DAGInstruction graph) {
		if (isRoot()) {
			return this.graphsMap.get(graph);
		} else {
			return getRoot().getAdapterGraph(graph);
		}
	}

	public IDAGNodeAdapter getAdapterNode(DAGNode node) {
		if (isRoot()) {
			return nodesMap.get(node);
		} else {
			return getRoot().getAdapterNode(node);
		}
	}

	public IDAGPortAdapter getAdapterPort(DAGPort port) {
		if (isRoot()) {
			return portsMap.get(port);
		} else {
			return getRoot().getAdapterPort(port);
		}
	}

	public IDAGEdgeAdapter getAdapterEdge(DAGEdge edge) {
		if (isRoot()) {
			return edgesMap.get(edge);
		} else {
			return getRoot().getAdapterEdge(edge);
		}
	}

	public void addGraph(IDAGInstructionAdapter adapter) {
		if (isRoot()) {
			DAGInstruction adapted = adapter.getAdaptedGraph();
			if (!graphsMap.containsKey(adapted)) {
				graphsMap.put(adapted, adapter);
			}
		} else {
			getRoot().addGraph(adapter);
		}
	}

	public void addNode(IDAGNodeAdapter adapter) {
		if (isRoot()) {
			DAGNode adapted = adapter.getAdaptedNode();
			if (!nodesMap.containsKey(adapted)) {
				nodesMap.put(adapted, adapter);
			}
		} else {
			getRoot().addNode(adapter);
		}
	}

	public void addPort(IDAGPortAdapter adapter) {
		if (isRoot()) {
			DAGPort adapted = adapter.getAdaptedPort();
			if (!portsMap.containsKey(adapted)) {
				portsMap.put(adapted, adapter);
			}
		} else {
			getRoot().addPort(adapter);
		}
	}

	public void addEdge(IDAGEdgeAdapter adapter) {
		if (isRoot()) {
			DAGEdge adapted = adapter.getAdaptedEdge();
			if (!edgesMap.containsKey(adapted)) {
				edgesMap.put(adapted, adapter);
			}
		} else {
			getRoot().addEdge(adapter);
		}
	}

	public void removeGraph(DAGInstruction adapted) {
		if (isRoot()) {
			this.graphsMap.remove(adapted);
		} else {
			getRoot().removeGraph(adapted);
		}
	}

	public void removeGraph(IDAGInstructionAdapter adapter) {
		if (isRoot()) {
			this.graphsMap.remove(adapter.getAdaptedGraph());
		} else {
			getRoot().removeGraph(adapter);
		}
	}

	public void removeEdge(DAGEdge adapted) {
		if (isRoot()) {
			edgesMap.remove(adapted);
		} else {
			getRoot().removeEdge(adapted);
		}
	}

	public void removeEdge(IDAGEdgeAdapter adapter) {
		if (isRoot()) {
			removeEdge(adapter.getAdaptedEdge());
		} else {
			getRoot().removeEdge(adapter);
		}

	}

	public void removeNode(DAGNode adapted) {
		if (isRoot()) {
			nodesMap.remove(adapted);
		} else {
			getRoot().removeNode(adapted);
		}

	}

	public void removeNode(IDAGNodeAdapter adapter) {
		if (isRoot()) {
			removeNode(adapter.getAdaptedNode());
		} else {
			getRoot().removeNode(adapter);
		}

	}

	public void removePort(DAGPort adapted) {
		if (isRoot()) {
			portsMap.remove(adapted);
		} else {
			getRoot().removePort(adapted);
		}

	}

	public void removePort(IDAGPortAdapter adapter) {
		if (isRoot()) {
			removePort(adapter.getAdaptedPort());
		} else {
			getRoot().removePort(adapter);
		}
	}

	public boolean containGraph(DAGInstruction adapted) {
		if (isRoot()) {
			return this.graphsMap.containsKey(adapted);
		} else {
			return getRoot().containGraph(adapted);
		}
	}

	public boolean containEdge(DAGEdge adapted) {
		if (isRoot()) {
			return edgesMap.containsKey(adapted);
		} else {
			return getRoot().containEdge(adapted);
		}
	}

	public boolean containNode(DAGNode adapted) {
		if (isRoot()) {
			return nodesMap.containsKey(adapted);
		} else {
			return getRoot().containNode(adapted);
		}
	}

	public boolean containPort(DAGPort adapted) {
		if (isRoot()) {
			return portsMap.containsKey(adapted);
		} else {
			return getRoot().containPort(adapted);
		}
	}

}
