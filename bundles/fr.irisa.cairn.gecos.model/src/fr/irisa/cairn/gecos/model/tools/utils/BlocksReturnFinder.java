package fr.irisa.cairn.gecos.model.tools.utils;

import gecos.blocks.BasicBlock;
import gecos.instrs.Instruction;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author amorvan
 * @generated NOT
 */
public class BlocksReturnFinder extends BlocksDefaultSwitch<Object> {

	private boolean containsReturns = false;
	public boolean containsReturns() { return containsReturns; }
	
	private List<BasicBlock> returns = new ArrayList<BasicBlock>();
	public List<BasicBlock> getReturns() { return returns; }

	@Override
	public Object caseBasicBlock(BasicBlock object) {
		BranchInstructionFinder visitor = new BranchInstructionFinder();
		for (Instruction i : object.getInstructions()) {
			visitor.doSwitch(i);
		}
		if (visitor.containsReturns()) {
			returns.add(object);
			containsReturns = true;
		}
		return null;
	}
}
