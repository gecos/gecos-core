package fr.irisa.cairn.gecos.core.transforms.tests;

import org.junit.Test;

import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.core.integration.tests.utils.DotAppDirData;
import fr.irisa.cairn.gecos.core.integration.tests.utils.DotAppDirDataTemplate;
import fr.irisa.cairn.gecos.model.transforms.ssa.ComputeSSAForm;
import fr.irisa.cairn.gecos.model.transforms.ssa.RemoveSSAForm;
import fr.irisa.cairn.gecos.testframework.data.ICxxProjectData;
import fr.irisa.cairn.gecos.testframework.dataprovider.DataFromPathProvider;
import fr.irisa.cairn.gecos.testframework.dataprovider.ResourcesLocation;

/**
 * Tests {@link ComputeSSAForm} and {@link RemoveSSAForm}.
 * 
 * @author aelmouss
 */
public class Tree2SSA2TreeIT extends DotAppDirDataTemplate {
	
	@Test
	@UseDataProvider(location=DataFromPathProvider.class, value=DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation(value = "resources/src-c/dot-app-data/misc", dataClasses = DotAppDirData.class)
	@ResourcesLocation(value = "resources/src-c/dot-app-data/idfix", dataClasses = DotAppDirData.class) 
	@ResourcesLocation("resources/src-c/block-outliner")
	public void testSSA(ICxxProjectData d) {
		runTest(d, v -> {
			new ComputeSSAForm(v.getProject()).compute();
			new RemoveSSAForm(v.getProject()).compute();
		});
	}

}
