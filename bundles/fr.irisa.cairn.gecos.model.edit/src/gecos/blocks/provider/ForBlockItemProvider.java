/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package gecos.blocks.provider;

import gecos.blocks.BasicBlock;
import gecos.blocks.BlocksPackage;
import gecos.blocks.ForBlock;
import gecos.instrs.Instruction;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link gecos.blocks.ForBlock} object.
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * @generated
 */
public class ForBlockItemProvider extends BlockItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ForBlockItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	protected void addInitPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				(ComposeableAdapterFactory) adapterFactory,
				getResourceLocator(), "Init block ", null,
				BlocksPackage.Literals.FOR_BLOCK__INIT_BLOCK, false, false,
				false, null, null, null));
	}

	protected void addStepPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				(ComposeableAdapterFactory) adapterFactory,
				getResourceLocator(), "Step block ", null,
				BlocksPackage.Literals.FOR_BLOCK__STEP_BLOCK, false, false,
				false, null, null, null));
	}

	protected void addTestPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				(ComposeableAdapterFactory) adapterFactory,
				getResourceLocator(), "Test block ", null,
				BlocksPackage.Literals.FOR_BLOCK__TEST_BLOCK, false, false,
				false, null, null, null));
	}

	protected void addBodyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				(ComposeableAdapterFactory) adapterFactory,
				getResourceLocator(), "Body block ", null,
				BlocksPackage.Literals.FOR_BLOCK__BODY_BLOCK, false, false,
				false, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(BlocksPackage.Literals.FOR_BLOCK__INIT_BLOCK);
			childrenFeatures.add(BlocksPackage.Literals.FOR_BLOCK__TEST_BLOCK);
			childrenFeatures.add(BlocksPackage.Literals.FOR_BLOCK__STEP_BLOCK);
			childrenFeatures.add(BlocksPackage.Literals.FOR_BLOCK__BODY_BLOCK);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ForBlock.gif.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ForBlock"));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean shouldComposeCreationImage() {
		return true;
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		ForBlock forBlock = (ForBlock) object;
		if (forBlock != null) {
			try {
				Instruction inst = ((BasicBlock)forBlock.getInitBlock()).getInstructions().get(0);
				String initText  = inst.toString();
				inst = ((BasicBlock)forBlock.getTestBlock()).getInstructions().get(0);
				String condText  =inst.toString();
				inst = ((BasicBlock)forBlock.getStepBlock()).getInstructions().get(0);
				String stepText  =inst.toString();
				return super.getText(object) + "for (" + initText + ";"
				+ condText + ";" + stepText	+ ")";
			} catch (Exception e) {
				return super.getText(object) + "for (" + forBlock.getInitBlock() + ";"
				+ forBlock.getTestBlock() + ";" + forBlock.getStepBlock()
				+ ")";
			}
		} else {
			return null;
		}
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ForBlock.class)) {
			case BlocksPackage.FOR_BLOCK__INIT_BLOCK:
			case BlocksPackage.FOR_BLOCK__TEST_BLOCK:
			case BlocksPackage.FOR_BLOCK__STEP_BLOCK:
			case BlocksPackage.FOR_BLOCK__BODY_BLOCK:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

}
