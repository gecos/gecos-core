/**
 */
package gecos.dag;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Call Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.DAGCallNode#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see gecos.dag.DagPackage#getDAGCallNode()
 * @model
 * @generated
 */
public interface DAGCallNode extends DAGNode, DagVisitable {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see gecos.dag.DagPackage#getDAGCallNode_Name()
	 * @model unique="false" dataType="gecos.dag.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGCallNode#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGCallNode(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.dag.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _name = this.getName();\nreturn (\"call \" + _name);'"
	 * @generated
	 */
	String toString();

} // DAGCallNode
