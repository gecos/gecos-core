package fr.irisa.cairn.gecos.model.type.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.eclipse.emf.common.util.EList;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.common.base.Strings;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.core.testframework.utils.GSProjectUtils;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableCoreCGenerator;
import fr.irisa.cairn.gecos.model.cdtfrontend.CDTFrontEnd;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.type.utils.TypesProvider.TypeProvidersSize;
import fr.irisa.r2d2.gecos.framework.utils.AbstractShell;
import fr.irisa.r2d2.gecos.framework.utils.SystemExec;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.types.FunctionType;
import gecos.types.Type;

/**
 * Tests Simple declarations ({@link Type}) parsing (with {@link CDTFrontEnd}) and
 * code generation (with {@link ExtendableCoreCGenerator}).
 * 
 * @author aelmouss
 */
@RunWith(DataProviderRunner.class)
public abstract class TypeTestTemplate {

	static private final boolean VERBOSE = false;
	static private final AbstractShell SHELL = SystemExec.createShell().setVerbose(VERBOSE);
	
	static final TypeProvidersSize SIZE = TypeProvidersSize.XSMALL;
	
	
	@Test
	@UseDataProvider(location=TypesProvider.class, value="provideBasic")
	public void testBasic(TypeData data) {
		run(data);
	}

	@Test
	@UseDataProvider(location=TypesProvider.class, value="providePtr")
	public void testPtr(TypeData data) {
		run(data);
	}

	@Test
	@UseDataProvider(location=TypesProvider.class, value="providePtrPtr")
	public void testPtrPtr(TypeData data) {
		run(data);
	}
	
	@Test
	@UseDataProvider(location=TypesProvider.class, value="provideAry")
	public void testAry(TypeData data) {
		run(data);
	}
	
	@Test
	@UseDataProvider(location=TypesProvider.class, value="provideAryAry")
	public void testAryAry(TypeData data) {
		run(data);
	}
	
	@Test
	@UseDataProvider(location=TypesProvider.class, value="provideAryPtr")
	public void testAryPtr(TypeData data) {
		run(data);
	}
	
	@Test
	@UseDataProvider(location=TypesProvider.class, value="providePtrAry")
	public void testPtrAry(TypeData data) {
		run(data);
	}
	
	@Test
	@UseDataProvider(location=TypesProvider.class, value="provideAryPtrAry")
	public void testAryPtrAry(TypeData data) {
		run(data);
	}

	@Test
	@UseDataProvider(location=TypesProvider.class, value="provideAryAryPtrAryPtrAry")
	public void testAryAryPtrAryPtrAry(TypeData data) {
		run(data);
	}
	
	@Test
	@UseDataProvider(location=TypesProvider.class, value="provideFnc")
	public void testFnc(TypeData data) {
		run(data);
	}

	@Test
	@UseDataProvider(location=TypesProvider.class, value="providePtrFnc")
	public void testPtrFnc(TypeData data) {
		run(data);
	}
	
	@Test
	@UseDataProvider(location=TypesProvider.class, value="provideFncPtr")
	public void testFncPtr(TypeData data) {
		run(data);
	}
	
	@Test
	@UseDataProvider(location=TypesProvider.class, value="provideFncPtrFnc")
	public void testFncPtrFnc(TypeData data) {
		run(data);
	}
	
	@Test
	@UseDataProvider(location=TypesProvider.class, value="providePtrFncPtrAryAry")
	public void testPtrFncPtrAryAry(TypeData data) {
		run(data);
	}
	
	@Test
	@UseDataProvider(location=TypesProvider.class, value="providePtrPtrFncPtrPtrAryAryPtrFncPtrPtrAryAryPtr")
	public void testPtrPtrFncPtrPtrAryAryPtrFncPtrPtrAryAryPtr(TypeData data) {
		run(data);
	}

	
	abstract protected void run(TypeData data);
	
	/**
	 * Tests {@link Type} parsing with {@link CDTFrontEnd}.
	 * @param data
	 */
	protected void testCodeToType(TypeData data) {
		if(VERBOSE) System.out.println("[INFO][Code-to-Type] ----------------------------------------");
		if(VERBOSE) System.out.println("[INFO][Code-to-Type] testing data: " + data);
		Type typeFromCode = codeToType(data);
		Assert.assertNotNull("[Code-to-Type] Generated Type is null", typeFromCode);
		
		Type expectedType = data.getType();
		if(VERBOSE) System.out.println("[INFO][Code-to-Type] expected type: " + expectedType);
		Assert.assertNotNull("[Code-to-Type] Expected Type is null", expectedType);
		
		Assert.assertTrue("[Code-to-Type] no match:\n" +
				"generated: '" + typeFromCode + "'\n" +
				"expected : '" + expectedType + "'",
				compareTypes(typeFromCode, expectedType));
	}

	/**
	 * Tests {@link Type} code generation with {@link ExtendableCoreCGenerator}.
	 * @param data
	 */
	protected void testTypeToCode(TypeData data){
		if(VERBOSE) System.out.println("[INFO][Type-to-Code] ----------------------------------------");
		if(VERBOSE) System.out.println("[INFO][Type-to-Code] testing data: " + data);
		String codeFromType = typeToCode(data);
		Assert.assertFalse("[Type-to-Code] generated code is not valid", Strings.isNullOrEmpty(codeFromType));
		
		String expectedCode = data.getCCode();
		if(VERBOSE) System.out.println("[INFO][Type-to-Code] expected code: " + expectedCode);
		Assert.assertFalse("[Type-to-Code] expected code is not valid", Strings.isNullOrEmpty(expectedCode));
		
		Assert.assertTrue("[Type-to-Code] no match:\n" +
				"generated: '" + codeFromType + "' from: " + data.getType() + "\n" +
				"expected : '" + expectedCode + "'",
				compareCode(codeFromType, expectedCode));
	}

	protected void testAll(TypeData data) {
		testCodeToType(data);
		testTypeToCode(data);
	}

	
	protected boolean compareTypes(Type typeFromCode, Type expectedType) {
		return expectedType.isEqual(typeFromCode);
	}
	
	protected boolean compareCode(String codeFromType, String expectedCode) {
//		return compareCodeString(codeFromType, expectedCode);
//		return compareCodeStringIgnoreSpaces(codeFromType, expectedCode);
		return compareCodeClang(codeFromType, expectedCode);
	}
	
	protected static boolean compareCodeString(String codeFromType, String expectedCode) {
		return codeFromType.equals(expectedCode);
	}

	protected static boolean compareCodeStringIgnoreSpaces(String codeFromType, String expectedCode) {
		String str1 = codeFromType.replaceAll("\\s", "");
		String str2 = expectedCode.replaceAll("\\s", "");
		return str1.equals(str2);
	}
	
	/**
	 * Compares declarations using clang ast-dump.
	 * Requires clang to be installed!
	 */
	protected static boolean compareCodeClang(String codeFromType, String expectedCode) {
		String clangCmd = "clang -cc1 -ast-dump ";
		String id = TypesProvider._ID;
		String grepCmd = "egrep '\\-VarDecl|\\-FunctionDecl'"; // | "FunctionDecl" ...
		String cmd2 = " 2>/dev/null | " + grepCmd + " | sed -e 's/.* "+id+" '\\''/a /'";
		
		try {
			File stdout = File.createTempFile("gecos_tmp", "clang.out");
			stdout.deleteOnExit();
			int res = SHELL.shell(clangCmd + _wrapCode(codeFromType) + cmd2, null, null, AbstractShell.outToFile(stdout), null);
			if(res != SHELL.successExitCode())
				throw new RuntimeException("failed to parse generated with clang");
			String clangGen = Files.readAllLines(stdout.toPath()).get(0);

			stdout = File.createTempFile("gecos_tmp", "clang.out");
			stdout.deleteOnExit();
			res = SHELL.shell(clangCmd + _wrapCode(expectedCode) + cmd2, null, null, AbstractShell.outToFile(stdout), null);
			if(res != SHELL.successExitCode())
				throw new RuntimeException("failed to parse expected with clang");
			String clangExp = Files.readAllLines(stdout.toPath()).get(0);
			
			return clangGen.equals(clangExp);
			
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
	}
	
	private static String _wrapCode(String code) {
		if(!code.endsWith(";"))
			code += ";";
//		return wrapInCFile(code).toString();
		return " <<<\"" + code + "\" ";
	}


	/**
	 * Performs the following steps:
	 * <li> Generates a C file containing the typed symbol declaration C code
	 * in the global scope.
	 * <li> Create/parse a {@link GecosProject} from the generated C file.
	 * <li> Retrieve the generated {@link Type} from the project.
	 * 
	 * @param data
	 * @return the generated {@link Type}.
	 */
	protected Type codeToType(TypeData data) {
		String cCode = data.getCCode();
		Assume.assumeTrue("[Code-to-Type][IGNORE]: the specified code is not parsable by GCC. " + cCode, Utils.isGccParsable(data));
		
		if(!cCode.endsWith(";"))
			cCode += ";";
		if(VERBOSE) System.out.println("[INFO][Code-to-Type] code to parse: " + cCode);
		Path cFile = Utils.wrapInCFile(cCode);
		GecosProject proj = GSProjectUtils.initGecosProject("tmp", cFile);
		EList<Symbol> symbols = proj.getSources().get(0).getModel().getScope().getSymbols();
		Type type = symbols.get(symbols.size()-1).getType(); //XXX
		if(VERBOSE) System.out.println("[INFO][Code-to-Type] obtained type: " + type);
		try {
			Files.delete(cFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return type;
	}

	protected String typeToCode(TypeData d) {
		Type type = d.getType();
		if(VERBOSE) System.out.println("[INFO][Type-to-Code] type to generate: " + type);
		
		Symbol sym;
		if(type instanceof FunctionType) {
			sym = GecosUserCoreFactory.procSymbol(d.getDeclarator().getIdentifier(), (FunctionType) type);
		} else {
			sym = GecosUserCoreFactory.symbol(d.getDeclarator().getIdentifier(), type);
		}
		String generatedCode = ExtendableCoreCGenerator.eInstance.generate(sym);
		if(!generatedCode.endsWith(";"))
			generatedCode += ";";
		
		if(VERBOSE) System.out.println("[INFO][Type-to-Code] obtained code: " + generatedCode);
		return generatedCode;
	}
	
}
