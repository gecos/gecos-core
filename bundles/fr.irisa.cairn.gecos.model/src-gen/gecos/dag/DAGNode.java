/**
 */
package gecos.dag;

import gecos.annotations.AnnotatedElement;

import gecos.core.ITypedElement;

import gecos.types.Type;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.DAGNode#getType <em>Type</em>}</li>
 *   <li>{@link gecos.dag.DAGNode#getOutputs <em>Outputs</em>}</li>
 *   <li>{@link gecos.dag.DAGNode#getParent <em>Parent</em>}</li>
 *   <li>{@link gecos.dag.DAGNode#getInputs <em>Inputs</em>}</li>
 *   <li>{@link gecos.dag.DAGNode#getIdent <em>Ident</em>}</li>
 * </ul>
 *
 * @see gecos.dag.DagPackage#getDAGNode()
 * @model abstract="true"
 * @generated
 */
public interface DAGNode extends AnnotatedElement, DagVisitable, ITypedElement {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(Type)
	 * @see gecos.dag.DagPackage#getDAGNode_Type()
	 * @model
	 * @generated
	 */
	Type getType();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGNode#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Type value);

	/**
	 * Returns the value of the '<em><b>Outputs</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.dag.DAGOutPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outputs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outputs</em>' containment reference list.
	 * @see gecos.dag.DagPackage#getDAGNode_Outputs()
	 * @model containment="true"
	 * @generated
	 */
	EList<DAGOutPort> getOutputs();

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link gecos.dag.DAGInstruction#getNodes <em>Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' container reference.
	 * @see #setParent(DAGInstruction)
	 * @see gecos.dag.DagPackage#getDAGNode_Parent()
	 * @see gecos.dag.DAGInstruction#getNodes
	 * @model opposite="nodes" transient="false"
	 * @generated
	 */
	DAGInstruction getParent();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGNode#getParent <em>Parent</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' container reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(DAGInstruction value);

	/**
	 * Returns the value of the '<em><b>Inputs</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.dag.DAGInPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inputs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inputs</em>' containment reference list.
	 * @see gecos.dag.DagPackage#getDAGNode_Inputs()
	 * @model containment="true"
	 * @generated
	 */
	EList<DAGInPort> getInputs();

	/**
	 * Returns the value of the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ident</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ident</em>' attribute.
	 * @see #setIdent(int)
	 * @see gecos.dag.DagPackage#getDAGNode_Ident()
	 * @model unique="false" dataType="gecos.dag.int"
	 * @generated
	 */
	int getIdent();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGNode#getIdent <em>Ident</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ident</em>' attribute.
	 * @see #getIdent()
	 * @generated
	 */
	void setIdent(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 * @generated
	 */
	void accept(DagVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return an unmodifiable list of {@link DAGOutPort} whose sinks contains a {DAGDataEdge}.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%gecos.dag.DAGOutPort%&gt;&gt; res = new &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%gecos.dag.DAGOutPort%&gt;&gt;();\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGOutPort%&gt;&gt; _outputs = this.getOutputs();\nfor (final &lt;%gecos.dag.DAGOutPort%&gt; out : _outputs)\n{\n\tif (((out.getSinks().size() &gt; 0) &amp;&amp; (out.getSinks().get(0) instanceof &lt;%gecos.dag.DAGDataEdge%&gt;)))\n\t{\n\t\tres.add(out);\n\t}\n}\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGOutPort%&gt;&gt;unmodifiableEList(res);'"
	 * @generated
	 */
	EList<DAGOutPort> getDataOutputs();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return an unmodifiable list of {@link DAGOutPort} whose sinks contains a {DAGControlEdge}.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%gecos.dag.DAGOutPort%&gt;&gt; res = new &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%gecos.dag.DAGOutPort%&gt;&gt;();\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGOutPort%&gt;&gt; _outputs = this.getOutputs();\nfor (final &lt;%gecos.dag.DAGOutPort%&gt; out : _outputs)\n{\n\tif (((out.getSinks().size() &gt; 0) &amp;&amp; (out.getSinks().get(0) instanceof &lt;%gecos.dag.DAGControlEdge%&gt;)))\n\t{\n\t\tres.add(out);\n\t}\n}\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGOutPort%&gt;&gt;unmodifiableEList(res);'"
	 * @generated
	 */
	EList<DAGOutPort> getControlOutputs();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return an unmodifiable list of {@link DAGInPort} whose source contains a {DAGDataEdge}.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%gecos.dag.DAGInPort%&gt;&gt; res = new &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%gecos.dag.DAGInPort%&gt;&gt;();\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGInPort%&gt;&gt; _inputs = this.getInputs();\nfor (final &lt;%gecos.dag.DAGInPort%&gt; in : _inputs)\n{\n\t&lt;%gecos.dag.DAGEdge%&gt; _source = in.getSource();\n\tif ((_source instanceof &lt;%gecos.dag.DAGDataEdge%&gt;))\n\t{\n\t\tres.add(in);\n\t}\n}\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGInPort%&gt;&gt;unmodifiableEList(res);'"
	 * @generated
	 */
	EList<DAGInPort> getDataInputs();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return an unmodifiable list of {@link DAGInPort} whose source contains a {DAGControlEdge}.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%gecos.dag.DAGInPort%&gt;&gt; res = new &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%gecos.dag.DAGInPort%&gt;&gt;();\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGInPort%&gt;&gt; _inputs = this.getInputs();\nfor (final &lt;%gecos.dag.DAGInPort%&gt; in : _inputs)\n{\n\t&lt;%gecos.dag.DAGEdge%&gt; _source = in.getSource();\n\tif ((_source instanceof &lt;%gecos.dag.DAGControlEdge%&gt;))\n\t{\n\t\tres.add(in);\n\t}\n}\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGInPort%&gt;&gt;unmodifiableEList(res);'"
	 * @generated
	 */
	EList<DAGInPort> getControlInputs();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return an unmodifiable list of all (non-null) input {@link DAGEdge}s.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGInPort%&gt;, &lt;%gecos.dag.DAGEdge%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGInPort%&gt;, &lt;%gecos.dag.DAGEdge%&gt;&gt;()\n{\n\tpublic &lt;%gecos.dag.DAGEdge%&gt; apply(final &lt;%gecos.dag.DAGInPort%&gt; it)\n\t{\n\t\treturn it.getSource();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;asEList(((&lt;%gecos.dag.DAGEdge%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;filterNull(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGInPort%&gt;, &lt;%gecos.dag.DAGEdge%&gt;&gt;map(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGInPort%&gt;&gt;filterNull(this.getInputs()), _function)), &lt;%gecos.dag.DAGEdge%&gt;.class))));'"
	 * @generated
	 */
	EList<DAGEdge> getInputEdges();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return an unmodifiable list of all (non-null) input {@link DAGDataEdge}s.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGInPort%&gt;, &lt;%gecos.dag.DAGEdge%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGInPort%&gt;, &lt;%gecos.dag.DAGEdge%&gt;&gt;()\n{\n\tpublic &lt;%gecos.dag.DAGEdge%&gt; apply(final &lt;%gecos.dag.DAGInPort%&gt; it)\n\t{\n\t\treturn it.getSource();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGDataEdge%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGDataEdge%&gt;&gt;asEList(((&lt;%gecos.dag.DAGDataEdge%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%gecos.dag.DAGDataEdge%&gt;&gt;filter(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGInPort%&gt;, &lt;%gecos.dag.DAGEdge%&gt;&gt;map(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGInPort%&gt;&gt;filterNull(this.getInputs()), _function), &lt;%gecos.dag.DAGDataEdge%&gt;.class), &lt;%gecos.dag.DAGDataEdge%&gt;.class))));'"
	 * @generated
	 */
	EList<DAGDataEdge> getDataInputEdges();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return an unmodifiable list of all (non-null) input {@link DAGControlEdge}s.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGInPort%&gt;, &lt;%gecos.dag.DAGEdge%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGInPort%&gt;, &lt;%gecos.dag.DAGEdge%&gt;&gt;()\n{\n\tpublic &lt;%gecos.dag.DAGEdge%&gt; apply(final &lt;%gecos.dag.DAGInPort%&gt; it)\n\t{\n\t\treturn it.getSource();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGControlEdge%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGControlEdge%&gt;&gt;asEList(((&lt;%gecos.dag.DAGControlEdge%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%gecos.dag.DAGControlEdge%&gt;&gt;filter(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGInPort%&gt;, &lt;%gecos.dag.DAGEdge%&gt;&gt;map(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGInPort%&gt;&gt;filterNull(this.getInputs()), _function), &lt;%gecos.dag.DAGControlEdge%&gt;.class), &lt;%gecos.dag.DAGControlEdge%&gt;.class))));'"
	 * @generated
	 */
	EList<DAGControlEdge> getControlInputEdges();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return an unmodifiable list of all (non-null) output {@link DAGEdge}s.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGOutPort%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGOutPort%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;&gt;()\n{\n\tpublic &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt; apply(final &lt;%gecos.dag.DAGOutPort%&gt; it)\n\t{\n\t\treturn it.getSinks();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;asEList(((&lt;%gecos.dag.DAGEdge%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;filterNull(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;concat(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGOutPort%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;&gt;map(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGOutPort%&gt;&gt;filterNull(this.getOutputs()), _function))), &lt;%gecos.dag.DAGEdge%&gt;.class))));'"
	 * @generated
	 */
	EList<DAGEdge> getOutputEdges();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return an unmodifiable list of all (non-null) output {@link DAGDataEdge}s.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGOutPort%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGOutPort%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;&gt;()\n{\n\tpublic &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt; apply(final &lt;%gecos.dag.DAGOutPort%&gt; it)\n\t{\n\t\treturn it.getSinks();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGDataEdge%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGDataEdge%&gt;&gt;asEList(((&lt;%gecos.dag.DAGDataEdge%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%gecos.dag.DAGDataEdge%&gt;&gt;filter(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;concat(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGOutPort%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;&gt;map(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGOutPort%&gt;&gt;filterNull(this.getOutputs()), _function)), &lt;%gecos.dag.DAGDataEdge%&gt;.class), &lt;%gecos.dag.DAGDataEdge%&gt;.class))));'"
	 * @generated
	 */
	EList<DAGDataEdge> getDataOutputEdges();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return an unmodifiable list of all (non-null) output {@link DAGControlEdge}s.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGOutPort%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGOutPort%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;&gt;()\n{\n\tpublic &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt; apply(final &lt;%gecos.dag.DAGOutPort%&gt; it)\n\t{\n\t\treturn it.getSinks();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGControlEdge%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGControlEdge%&gt;&gt;asEList(((&lt;%gecos.dag.DAGControlEdge%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%gecos.dag.DAGControlEdge%&gt;&gt;filter(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;concat(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGOutPort%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;&gt;map(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGOutPort%&gt;&gt;filterNull(this.getOutputs()), _function)), &lt;%gecos.dag.DAGControlEdge%&gt;.class), &lt;%gecos.dag.DAGControlEdge%&gt;.class))));'"
	 * @generated
	 */
	EList<DAGControlEdge> getControlOutputEdges();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return an unmodifiable list of all (non-null) predecessor {@link DAGNode}s.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGInPort%&gt;, &lt;%gecos.dag.DAGNode%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGInPort%&gt;, &lt;%gecos.dag.DAGNode%&gt;&gt;()\n{\n\tpublic &lt;%gecos.dag.DAGNode%&gt; apply(final &lt;%gecos.dag.DAGInPort%&gt; it)\n\t{\n\t\treturn it.getSourceNode();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGNode%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGNode%&gt;&gt;asEList(((&lt;%gecos.dag.DAGNode%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGNode%&gt;&gt;filterNull(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%gecos.dag.DAGInPort%&gt;, &lt;%gecos.dag.DAGNode%&gt;&gt;map(this.getInputs(), _function)), &lt;%gecos.dag.DAGNode%&gt;.class))));'"
	 * @generated
	 */
	EList<DAGNode> getPredecessors();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return an unmodifiable list of all (non-null) Data predecessor {@link DAGNode}s.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGInPort%&gt;, &lt;%gecos.dag.DAGEdge%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGInPort%&gt;, &lt;%gecos.dag.DAGEdge%&gt;&gt;()\n{\n\tpublic &lt;%gecos.dag.DAGEdge%&gt; apply(final &lt;%gecos.dag.DAGInPort%&gt; it)\n\t{\n\t\treturn it.getSource();\n\t}\n};\nfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGDataEdge%&gt;, &lt;%gecos.dag.DAGNode%&gt;&gt; _function_1 = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGDataEdge%&gt;, &lt;%gecos.dag.DAGNode%&gt;&gt;()\n{\n\tpublic &lt;%gecos.dag.DAGNode%&gt; apply(final &lt;%gecos.dag.DAGDataEdge%&gt; it)\n\t{\n\t\treturn it.getSourceNode();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGNode%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGNode%&gt;&gt;asEList(((&lt;%gecos.dag.DAGNode%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGNode%&gt;&gt;filterNull(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGDataEdge%&gt;, &lt;%gecos.dag.DAGNode%&gt;&gt;map(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%gecos.dag.DAGDataEdge%&gt;&gt;filter(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%gecos.dag.DAGInPort%&gt;, &lt;%gecos.dag.DAGEdge%&gt;&gt;map(this.getInputs(), _function), &lt;%gecos.dag.DAGDataEdge%&gt;.class), _function_1)), &lt;%gecos.dag.DAGNode%&gt;.class))));'"
	 * @generated
	 */
	EList<DAGNode> getDataPredecessors();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return an unmodifiable list of all (non-null) Control predecessor {@link DAGNode}s.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGInPort%&gt;, &lt;%gecos.dag.DAGEdge%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGInPort%&gt;, &lt;%gecos.dag.DAGEdge%&gt;&gt;()\n{\n\tpublic &lt;%gecos.dag.DAGEdge%&gt; apply(final &lt;%gecos.dag.DAGInPort%&gt; it)\n\t{\n\t\treturn it.getSource();\n\t}\n};\nfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGControlEdge%&gt;, &lt;%gecos.dag.DAGNode%&gt;&gt; _function_1 = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGControlEdge%&gt;, &lt;%gecos.dag.DAGNode%&gt;&gt;()\n{\n\tpublic &lt;%gecos.dag.DAGNode%&gt; apply(final &lt;%gecos.dag.DAGControlEdge%&gt; it)\n\t{\n\t\treturn it.getSourceNode();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGNode%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGNode%&gt;&gt;asEList(((&lt;%gecos.dag.DAGNode%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGNode%&gt;&gt;filterNull(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGControlEdge%&gt;, &lt;%gecos.dag.DAGNode%&gt;&gt;map(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%gecos.dag.DAGControlEdge%&gt;&gt;filter(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%gecos.dag.DAGInPort%&gt;, &lt;%gecos.dag.DAGEdge%&gt;&gt;map(this.getInputs(), _function), &lt;%gecos.dag.DAGControlEdge%&gt;.class), _function_1)), &lt;%gecos.dag.DAGNode%&gt;.class))));'"
	 * @generated
	 */
	EList<DAGNode> getControlPredecessors();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return an unmodifiable list of all (non-null) successor {@link DAGNode}s.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGOutPort%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGOutPort%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;&gt;()\n{\n\tpublic &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt; apply(final &lt;%gecos.dag.DAGOutPort%&gt; it)\n\t{\n\t\treturn it.getSinks();\n\t}\n};\nfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;, &lt;%gecos.dag.DAGNode%&gt;&gt; _function_1 = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;, &lt;%gecos.dag.DAGNode%&gt;&gt;()\n{\n\tpublic &lt;%gecos.dag.DAGNode%&gt; apply(final &lt;%gecos.dag.DAGEdge%&gt; it)\n\t{\n\t\treturn it.getSinkNode();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGNode%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGNode%&gt;&gt;asEList(((&lt;%gecos.dag.DAGNode%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGNode%&gt;&gt;filterNull(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGEdge%&gt;, &lt;%gecos.dag.DAGNode%&gt;&gt;map(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;filterNull(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;concat(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%gecos.dag.DAGOutPort%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;&gt;map(this.getOutputs(), _function))), _function_1)), &lt;%gecos.dag.DAGNode%&gt;.class))));'"
	 * @generated
	 */
	EList<DAGNode> getSuccessors();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return an unmodifiable list of all (non-null) Data successor {@link DAGNode}s.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGOutPort%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGOutPort%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;&gt;()\n{\n\tpublic &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt; apply(final &lt;%gecos.dag.DAGOutPort%&gt; it)\n\t{\n\t\treturn it.getSinks();\n\t}\n};\nfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGDataEdge%&gt;, &lt;%gecos.dag.DAGNode%&gt;&gt; _function_1 = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGDataEdge%&gt;, &lt;%gecos.dag.DAGNode%&gt;&gt;()\n{\n\tpublic &lt;%gecos.dag.DAGNode%&gt; apply(final &lt;%gecos.dag.DAGDataEdge%&gt; it)\n\t{\n\t\treturn it.getSinkNode();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGNode%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGNode%&gt;&gt;asEList(((&lt;%gecos.dag.DAGNode%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGNode%&gt;&gt;filterNull(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGDataEdge%&gt;, &lt;%gecos.dag.DAGNode%&gt;&gt;map(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%gecos.dag.DAGDataEdge%&gt;&gt;filter(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;concat(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%gecos.dag.DAGOutPort%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;&gt;map(this.getOutputs(), _function)), &lt;%gecos.dag.DAGDataEdge%&gt;.class), _function_1)), &lt;%gecos.dag.DAGNode%&gt;.class))));'"
	 * @generated
	 */
	EList<DAGNode> getDataSuccessors();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return an unmodifiable list of all (non-null) Control successor {@link DAGNode}s.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGOutPort%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGOutPort%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;&gt;()\n{\n\tpublic &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt; apply(final &lt;%gecos.dag.DAGOutPort%&gt; it)\n\t{\n\t\treturn it.getSinks();\n\t}\n};\nfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGControlEdge%&gt;, &lt;%gecos.dag.DAGNode%&gt;&gt; _function_1 = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGControlEdge%&gt;, &lt;%gecos.dag.DAGNode%&gt;&gt;()\n{\n\tpublic &lt;%gecos.dag.DAGNode%&gt; apply(final &lt;%gecos.dag.DAGControlEdge%&gt; it)\n\t{\n\t\treturn it.getSinkNode();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGNode%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGNode%&gt;&gt;asEList(((&lt;%gecos.dag.DAGNode%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGNode%&gt;&gt;filterNull(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGControlEdge%&gt;, &lt;%gecos.dag.DAGNode%&gt;&gt;map(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%gecos.dag.DAGControlEdge%&gt;&gt;filter(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;concat(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%gecos.dag.DAGOutPort%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;&gt;map(this.getOutputs(), _function)), &lt;%gecos.dag.DAGControlEdge%&gt;.class), _function_1)), &lt;%gecos.dag.DAGNode%&gt;.class))));'"
	 * @generated
	 */
	EList<DAGNode> getControlSuccessors();

} // DAGNode
