/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.cdtfrontend.visitor;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.cdt.core.dom.ast.IASTArrayDeclarator;
import org.eclipse.cdt.core.dom.ast.IASTArrayModifier;
import org.eclipse.cdt.core.dom.ast.IASTDeclSpecifier;
import org.eclipse.cdt.core.dom.ast.IASTDeclarator;
import org.eclipse.cdt.core.dom.ast.IASTExpression;
import org.eclipse.cdt.core.dom.ast.IASTFieldDeclarator;
import org.eclipse.cdt.core.dom.ast.IASTFunctionDeclarator;
import org.eclipse.cdt.core.dom.ast.IASTName;
import org.eclipse.cdt.core.dom.ast.IASTNode;
import org.eclipse.cdt.core.dom.ast.IASTParameterDeclaration;
import org.eclipse.cdt.core.dom.ast.IASTPointerOperator;
import org.eclipse.cdt.core.dom.ast.IASTSimpleDeclaration;
import org.eclipse.cdt.core.dom.ast.IASTStandardFunctionDeclarator;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTArrayDeclarator;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTFieldDeclarator;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTFunctionDeclarator;
import org.eclipse.cdt.core.dom.ast.gnu.c.ICASTKnRFunctionDeclarator;

import fr.irisa.cairn.gecos.model.cdtfrontend.exception.CDTParserErrorException;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import gecos.annotations.PragmaAnnotation;
import gecos.core.ParameterSymbol;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.types.Type;

/**
 * @author mnaullet
 */
public class ASTDeclaratorVisitor {
	
	/* common */
	private CDTToGecosModel gecosModel;
//	private IASTDeclarator declarator;
	
	private String name;
	private Instruction initializer;
	private IASTPointerOperator[] pointers; //innermost dimension first (e.g. *const *volatile a: pointers[0] = *const, pointers[1] = *volatile)
	private ASTDeclaratorVisitor inner;
	
	/* Function special */
	private boolean isFunction; // true: this represents a function declarator.
	private boolean takesVarArgs;
	private IASTNode[] functionParameters;
	private List<ParameterSymbol> parameterSymbols;
	
	/* Array special */ //NOTE: a valid declarator cannot have arrayModifiers and function at the same time
	private IASTArrayModifier[] arrayModifiers; //outermost dimension first (e.g. a[2][3]: arrayModifiers[1] = [3], arrayModifiers[0] = [2])
	
	/* record Field special */
	private long bitSize;

	
	public ASTDeclaratorVisitor() {
		isFunction = false;
		takesVarArgs = false;
	}

	public void visit(CDTToGecosModel gecosModel, IASTDeclarator declarator) {
		this.gecosModel = gecosModel;
//		this.declarator = declarator;
		
		buildIASTDeclarator(declarator);
		
		//specific nodes
		if (declarator instanceof IASTArrayDeclarator)
			if (declarator instanceof ICPPASTArrayDeclarator)
				buildICPPASTArrayDeclarator((ICPPASTArrayDeclarator)declarator);
			else
				buildIASTArrayDeclarator((IASTArrayDeclarator)declarator);
		
		else if (declarator instanceof IASTFieldDeclarator)
			if (declarator instanceof ICPPASTFieldDeclarator)
				buildICPPASTFieldDeclarator((ICPPASTFieldDeclarator)declarator);
			else
				buildIASTFieldDeclarator((IASTFieldDeclarator)declarator);
		
		else if (declarator instanceof IASTFunctionDeclarator)
			if (declarator instanceof IASTStandardFunctionDeclarator)
				if (declarator instanceof ICPPASTFunctionDeclarator)
					buildICPPASTFunctionDeclarator((ICPPASTFunctionDeclarator)declarator);
				else
					buildIASTStandardFunctionDeclarator((IASTStandardFunctionDeclarator)declarator);
			else if (declarator instanceof ICASTKnRFunctionDeclarator)
				buildICASTKnRFunctionDeclarator((ICASTKnRFunctionDeclarator)declarator);
			else
				buildIASTFunctionDeclarator((IASTFunctionDeclarator)declarator);
	}
	
	public String getName() {
		return name;
	}

	public Instruction getInitializer() {
		return initializer;
	}

	public IASTPointerOperator[] getPointers() {
		return pointers;
	}
	
	public long getBitSize() {
		return bitSize;
	}
	
	public boolean isFunction() {
		return isFunction;
	}
	
	public boolean takesVarArgs() {
		return takesVarArgs;
	}
	
	public ASTDeclaratorVisitor getInner() {
		return inner;
	}
	
	public ASTDeclaratorVisitor getInnermost() {
		if(inner == null)
			return this;
		return inner.getInnermost();
	}
	
	
	private void buildIASTDeclarator(IASTDeclarator declarator) {
		//Common nodes
		if (declarator.getInitializer() != null) {
			declarator.getInitializer().accept(gecosModel);
			initializer = ((ASTInitializerVisitor)gecosModel.getObject()).getInstruction();
		}
		if (declarator.getName() != null) {
			declarator.getName().accept(gecosModel);
			name = ((ASTNameVisitor)gecosModel.getObject()).getName();
		}
		
		pointers = declarator.getPointerOperators();
		
		if (declarator.getNestedDeclarator() != null) {
			declarator.getNestedDeclarator().accept(gecosModel);
			this.inner = (ASTDeclaratorVisitor)gecosModel.getObject();
			
			if (name == null || name.isEmpty()) //XXX
				name = inner.getName();
		}
		bitSize = -1;
		
	}

	private void buildIASTArrayDeclarator(IASTArrayDeclarator declarator) {
		this.arrayModifiers = declarator.getArrayModifiers();
	}
	
	private void buildICPPASTArrayDeclarator(ICPPASTArrayDeclarator declarator) {
		buildIASTArrayDeclarator(declarator);
	}

	private void buildIASTFieldDeclarator(IASTFieldDeclarator declarator) {
		buildIASTDeclarator(declarator);
		IASTExpression bitFieldSize = declarator.getBitFieldSize();
		bitFieldSize.accept(gecosModel);
		ASTExpressionVisitor v = (ASTExpressionVisitor)gecosModel.getObject();
		Instruction bitSize = v.getInstruction();
		if (bitSize instanceof IntInstruction) {
			long size = ((IntInstruction) bitSize).getValue();
			this.bitSize = size;
		} else {
			throw new CDTParserErrorException(declarator, "size should be a constant");
		}
	}
	
	private void buildICPPASTFieldDeclarator(ICPPASTFieldDeclarator declarator) {
		buildIASTFieldDeclarator(declarator);
	}

	private void buildIASTFunctionDeclarator(IASTFunctionDeclarator declarator) {
		isFunction = true;
	}

	private void buildICPPASTFunctionDeclarator(ICPPASTFunctionDeclarator declarator) {
		buildIASTStandardFunctionDeclarator(declarator);
	}
	
	private void buildIASTStandardFunctionDeclarator(IASTStandardFunctionDeclarator declarator) {
		buildIASTFunctionDeclarator(declarator);
		
		takesVarArgs = declarator.takesVarArgs();
		functionParameters = declarator.getParameters();
	}
	
	private void buildICASTKnRFunctionDeclarator(ICASTKnRFunctionDeclarator declarator) {
		buildIASTFunctionDeclarator(declarator);
		
		IASTName[] parameterNames = declarator.getParameterNames();
		functionParameters = new IASTNode[parameterNames.length];
		for (int i=0; i < parameterNames.length; i++) {
			IASTName pName = parameterNames[i];
			IASTDeclarator declaratorForParameterName = declarator.getDeclaratorForParameterName(pName);
			if (declaratorForParameterName == null)
				functionParameters[i] = pName;
			else
				functionParameters[i] = declaratorForParameterName;
		}
	}	
			
	/**
	 * @param _type base type for pointers
	 * @return the pointer type represented by this declarator, using {@code type} as base type.
	 * In case this has no pointer operators, it returns {@code type}.
	 */
	private Type _buildPtr(Type base) {
		if(pointers != null) {
			for(IASTPointerOperator p : pointers) { //innermost dimension first
				p.accept(gecosModel);
				ASTPointerOperatorVisitor astPointerOperatorVisitor = (ASTPointerOperatorVisitor)gecosModel.getObject();
				base = astPointerOperatorVisitor.buildPtrType(base);
			}
		}
		return base;
	}

	/**
	 * @param type base type for arrays
	 * @return the array type represented by this declarator, using {@code type} as base type.
	 * In case this has no array modifiers, it returns {@code type}.
	 */
	private Type _buildArray(Type type) {
		if(arrayModifiers != null) {
			for (int i = arrayModifiers.length-1; i>=0; i--) { //innermost dimension first
				arrayModifiers[i].accept(gecosModel);
				ASTArrayModifierVisitor arrayModifierVisitor = (ASTArrayModifierVisitor)gecosModel.getObject();
				Instruction arraySizeInstr = arrayModifierVisitor.getInstruction();
				type = GecosUserTypeFactory.ARRAY(type, arraySizeInstr);
			}
		}
		return type;
	}
	
	/**
	 * @param base
	 * @return
	 */
	private Type _buildFunction(Type base) {
		parameterSymbols = new ArrayList<ParameterSymbol>();
		if(functionParameters != null) {
			for(IASTNode parameterDeclaration : functionParameters) {
				ParameterSymbolInfo paramInfo = null;
				if(parameterDeclaration instanceof IASTParameterDeclaration) {
					/* case of StandardFunction parameter */
					parameterDeclaration.accept(gecosModel);
					ASTParameterDeclarationVisitor parameterDeclarationVisitor = (ASTParameterDeclarationVisitor) gecosModel.getObject();
					paramInfo = parameterDeclarationVisitor.getParameterSymbolInfo();
				}
				else if(parameterDeclaration instanceof IASTName) {
					/* case of KnRFunction parameter with no explicit declaration: default type is int */
					parameterDeclaration.accept(gecosModel);
					String name = ((ASTNameVisitor)gecosModel.getObject()).getName();
					Type type = GecosUserTypeFactory.INT();
					List<PragmaAnnotation> pragmaAnnotations = new ArrayList<>();
					
					paramInfo = new ParameterSymbolInfo(name, type, pragmaAnnotations);
				}
				else if(parameterDeclaration instanceof IASTDeclarator) {
					/* case of KnRFunction parameter with explicit declaration */
					List<PragmaAnnotation> pragmaAnnotations = gecosModel.findAnnotation(parameterDeclaration.getFileLocation().getFileName(), parameterDeclaration.getFileLocation().getNodeOffset());

					IASTDeclSpecifier declSpecifier = ((IASTSimpleDeclaration) parameterDeclaration.getParent()).getDeclSpecifier();
					declSpecifier.accept(gecosModel);
					ASTDeclSpecifierVisitor declSpecifierVisitor = (ASTDeclSpecifierVisitor)gecosModel.getObject();
					
					Type type = declSpecifierVisitor.getType();
					if (type == null)
						throw new CDTParserErrorException(parameterDeclaration, "Could not determine type for "+parameterDeclaration.getRawSignature());

					parameterDeclaration.accept(gecosModel);
					ASTDeclaratorVisitor declaratorVisitor = (ASTDeclaratorVisitor)gecosModel.getObject();
					
					String name = declaratorVisitor.getName();
					type = declaratorVisitor.buildType(type);
					
					paramInfo = new ParameterSymbolInfo(name, type, pragmaAnnotations);
				} else
					throw new RuntimeException("Unexpected parameter type: " + parameterDeclaration.getRawSignature());
				
				parameterSymbols.add(paramInfo.buildParameterSymbol(gecosModel));
			}
		} 
		
		List<Type> parameterTypes = parameterSymbols.stream()
			.map(ps -> ps.getType())
			.collect(toList());
		base = GecosUserTypeFactory.FUNCTION(base, takesVarArgs(), parameterTypes);
		return base;
	}

	/**
	 * @param base
	 * @return
	 */
	private Type _buildType_one(Type base) {
		base = _buildPtr(base);
		if(isFunction())
			base = _buildFunction(base);
		else
			base = _buildArray(base);
		return base;
	}
	
	public Type buildType(Type base) {
		ASTDeclaratorVisitor decl = this;
		while(decl != null) {
			base = decl._buildType_one(base);
			decl = decl.getInner();
		}
		
		return base;
	}
	
	/**
	 * ParameterSymbols are initialized only after the type has been built
	 * by invoking {@link #buildType(Type)}.
	 * @return list of {@link ParameterSymbol} if the visited Declarator represents a function, null otherwise.
	 */
	public List<ParameterSymbol> getParameterSymbols() {
		return parameterSymbols;
	}

	
	static class ParameterSymbolInfo {
		private Type _type;
		private String _name;
		private List<PragmaAnnotation> _pragmas;
		private ParameterSymbol _paramSymbol;
		
		public ParameterSymbolInfo(String name, Type type, List<PragmaAnnotation> pragmaAnnotations) {
			this._name = name;
			this._type = type;
			this._pragmas = pragmaAnnotations;
		}
		
		public Type getType() {
			return _type;
		}

		public ParameterSymbol buildParameterSymbol(CDTToGecosModel gecosModel) {
			if(_paramSymbol == null) {
				_paramSymbol = gecosModel.getGecosCoreContextedFactory().createParameterSymbol(_name, _type);
				GecosUserAnnotationFactory.pragma(_paramSymbol, _pragmas);
				if (gecosModel.visitingInclude())
					gecosModel.ignore(_paramSymbol);
			}
			return _paramSymbol;
		}
	}
	
}