/**
 */
package profiling.frontend.directives.impl;

import fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport;

import fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport.NativeFunction;

import fr.irisa.cairn.gecos.core.profiling.frontend.DirectivesProcessor;
import fr.irisa.cairn.gecos.core.profiling.frontend.InvalidDirective;
import fr.irisa.cairn.gecos.core.profiling.frontend.UidProvider;

import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;

import gecos.core.ProcedureSet;
import gecos.core.Symbol;

import gecos.instrs.CallInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.SymbolInstruction;

import gecos.types.Type;

import java.util.Collection;
import java.util.LinkedList;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions;

import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.ExclusiveRange;

import profiling.frontend.directives.DirectiveName;
import profiling.frontend.directives.DirectivesPackage;
import profiling.frontend.directives.SaveDirective;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Save Directive</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link profiling.frontend.directives.impl.SaveDirectiveImpl#getNAME <em>NAME</em>}</li>
 *   <li>{@link profiling.frontend.directives.impl.SaveDirectiveImpl#getAddress <em>Address</em>}</li>
 *   <li>{@link profiling.frontend.directives.impl.SaveDirectiveImpl#getSizesOutermostFirst <em>Sizes Outermost First</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SaveDirectiveImpl extends DirectiveImpl implements SaveDirective {
	/**
	 * The default value of the '{@link #getNAME() <em>NAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNAME()
	 * @generated
	 * @ordered
	 */
	protected static final DirectiveName NAME_EDEFAULT = DirectiveName.SAVE;

	/**
	 * The cached value of the '{@link #getNAME() <em>NAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNAME()
	 * @generated
	 * @ordered
	 */
	protected DirectiveName name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAddress() <em>Address</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAddress()
	 * @generated
	 * @ordered
	 */
	protected Symbol address;

	/**
	 * The cached value of the '{@link #getSizesOutermostFirst() <em>Sizes Outermost First</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSizesOutermostFirst()
	 * @generated
	 * @ordered
	 */
	protected EList<Instruction> sizesOutermostFirst;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SaveDirectiveImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DirectivesPackage.Literals.SAVE_DIRECTIVE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DirectiveName getNAME() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNAME(DirectiveName newNAME) {
		DirectiveName oldNAME = name;
		name = newNAME == null ? NAME_EDEFAULT : newNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DirectivesPackage.SAVE_DIRECTIVE__NAME, oldNAME, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol getAddress() {
		if (address != null && address.eIsProxy()) {
			InternalEObject oldAddress = (InternalEObject)address;
			address = (Symbol)eResolveProxy(oldAddress);
			if (address != oldAddress) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DirectivesPackage.SAVE_DIRECTIVE__ADDRESS, oldAddress, address));
			}
		}
		return address;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol basicGetAddress() {
		return address;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAddress(Symbol newAddress) {
		Symbol oldAddress = address;
		address = newAddress;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DirectivesPackage.SAVE_DIRECTIVE__ADDRESS, oldAddress, address));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instruction> getSizesOutermostFirst() {
		if (sizesOutermostFirst == null) {
			sizesOutermostFirst = new EObjectResolvingEList<Instruction>(Instruction.class, this, DirectivesPackage.SAVE_DIRECTIVE__SIZES_OUTERMOST_FIRST);
		}
		return sizesOutermostFirst;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void parse() {
		final EList<Instruction> args = this.getCall().getArgs();
		boolean _isEmpty = args.isEmpty();
		if (_isEmpty) {
			String _printLocation = this.printLocation();
			String _plus = (_printLocation + "\nMust have at least one argument (Symbol)!");
			throw new InvalidDirective(_plus);
		}
		Symbol _xtrycatchfinallyexpression = null;
		try {
			Instruction _get = args.get(0);
			Symbol _symbol = ((SymbolInstruction) _get).getSymbol();
			_xtrycatchfinallyexpression = _symbol;
		}
		catch (final Throwable _t) {
			if (_t instanceof Exception) {
				String _printLocation_1 = this.printLocation();
				String _plus_1 = (_printLocation_1 + "\nMust have a SymbolInstruction as first argument!");
				throw new InvalidDirective(_plus_1);
			}
			else {
				throw Exceptions.sneakyThrow(_t);
			}
		}
		this.setAddress(_xtrycatchfinallyexpression);
		Type _type = this.getAddress().getType();
		final TypeAnalyzer ta = new TypeAnalyzer(_type, true);
		int _totalNbDims = ta.getTotalNbDims();
		ExclusiveRange _doubleDotLessThan = new ExclusiveRange(0, _totalNbDims, true);
		for (final Integer dim : _doubleDotLessThan) {
			{
				Instruction _xifexpression = null;
				int _size = args.size();
				boolean _greaterThan = (_size > ((dim).intValue() + 1));
				if (_greaterThan) {
					_xifexpression = args.get((1 + (dim).intValue()));
				}
				else {
					_xifexpression = DirectivesProcessor.getDimensionSize(this.getAddress(), (dim).intValue());
				}
				final Instruction sz = _xifexpression;
				if ((sz == null)) {
					String _printLocation_2 = this.printLocation();
					String _plus_2 = (_printLocation_2 + "\nMust specify a size for dimension \'");
					String _plus_3 = (_plus_2 + dim);
					String _plus_4 = (_plus_3 + "\' which couldn\'t be determined automatically!");
					throw new InvalidDirective(_plus_4);
				}
				this.getSizesOutermostFirst().add(sz);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block createCodeBlock() {
		final ProcedureSet ps = this.getCall().getContainingProcedureSet();
		final int symbolUid = UidProvider.setUID(this.getAddress());
		final CompositeBlock cb = GecosUserBlockFactory.CompositeBlock();
		GecosUserAnnotationFactory.comment(cb, this.getCall().toString());
		GecosUserTypeFactory.setScope(cb.getScope());
		final BasicBlock innermostBody = GecosUserBlockFactory.BBlock();
		Block currentBlock = ((Block) innermostBody);
		final LinkedList<Instruction> indexesOutermostFirst = new LinkedList<Instruction>();
		EList<Instruction> _reverseView = XcoreEListExtensions.<Instruction>reverseView(this.getSizesOutermostFirst());
		for (final Instruction dimSize : _reverseView) {
			{
				final Symbol index = GecosUserCoreFactory.symbol("i", GecosUserTypeFactory.INT(), cb.getScope());
				cb.getScope().makeUnique(index);
				indexesOutermostFirst.addFirst(GecosUserInstructionFactory.symbref(index));
				currentBlock = GecosUserBlockFactory.For(index, 0, dimSize.copy(), 1, currentBlock);
			}
		}
		final CallInstruction pushValue = GecosUserInstructionFactory.call(
			NativeProfilingEngineSupport.getNativeProc(ps, NativeFunction.PUSH_VALUE), 
			GecosUserInstructionFactory.Int(symbolUid), 
			GecosUserInstructionFactory.string(this.getAddress().getName()), 
			GecosUserInstructionFactory.array(this.getAddress(), indexesOutermostFirst));
		innermostBody.addInstruction(pushValue);
		cb.addChildren(currentBlock);
		final BasicBlock finishBlock = GecosUserBlockFactory.BBlock(
			GecosUserInstructionFactory.call(
				NativeProfilingEngineSupport.getNativeProc(ps, NativeFunction.SAVE_SNAPSHOT), 
				GecosUserInstructionFactory.Int(symbolUid)));
		cb.addChildren(finishBlock);
		return cb;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DirectivesPackage.SAVE_DIRECTIVE__NAME:
				return getNAME();
			case DirectivesPackage.SAVE_DIRECTIVE__ADDRESS:
				if (resolve) return getAddress();
				return basicGetAddress();
			case DirectivesPackage.SAVE_DIRECTIVE__SIZES_OUTERMOST_FIRST:
				return getSizesOutermostFirst();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DirectivesPackage.SAVE_DIRECTIVE__NAME:
				setNAME((DirectiveName)newValue);
				return;
			case DirectivesPackage.SAVE_DIRECTIVE__ADDRESS:
				setAddress((Symbol)newValue);
				return;
			case DirectivesPackage.SAVE_DIRECTIVE__SIZES_OUTERMOST_FIRST:
				getSizesOutermostFirst().clear();
				getSizesOutermostFirst().addAll((Collection<? extends Instruction>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DirectivesPackage.SAVE_DIRECTIVE__NAME:
				setNAME(NAME_EDEFAULT);
				return;
			case DirectivesPackage.SAVE_DIRECTIVE__ADDRESS:
				setAddress((Symbol)null);
				return;
			case DirectivesPackage.SAVE_DIRECTIVE__SIZES_OUTERMOST_FIRST:
				getSizesOutermostFirst().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DirectivesPackage.SAVE_DIRECTIVE__NAME:
				return name != NAME_EDEFAULT;
			case DirectivesPackage.SAVE_DIRECTIVE__ADDRESS:
				return address != null;
			case DirectivesPackage.SAVE_DIRECTIVE__SIZES_OUTERMOST_FIRST:
				return sizesOutermostFirst != null && !sizesOutermostFirst.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (NAME: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //SaveDirectiveImpl
