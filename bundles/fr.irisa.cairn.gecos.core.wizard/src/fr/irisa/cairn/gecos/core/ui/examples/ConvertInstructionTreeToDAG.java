package fr.irisa.cairn.gecos.core.ui.examples;

import org.eclipse.core.runtime.Platform;
import org.eclipse.ui.INewWizard;
import org.osgi.framework.Bundle;

import fr.irisa.cairn.gecos.core.ui.Activator;
import fr.irisa.cairn.gecos.core.ui.examples.utils.BasicGecosExampleWizard;

public class ConvertInstructionTreeToDAG extends BasicGecosExampleWizard implements INewWizard {
	@Override
	public String getResourcesFolderToCopyIntoProject() {
		return "resources/wizard_templates/convert_inst_trees_to_dags/";
	}

	@Override
	public String getPageName() {
		return "Convert instruction trees to DAG example";
	}

	@Override
	public String getPageDescription() {
		return "This wizard will create a example project for converting instruction trees to DAG";
	}

	@Override
	protected Bundle getPluginBundle() {
		return Platform.getBundle(Activator.PLUGIN_ID);
	}

}
