/**
 */
package gecos.dag;

import gecos.annotations.AnnotatedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.DAGEdge#getSrc <em>Src</em>}</li>
 *   <li>{@link gecos.dag.DAGEdge#getSink <em>Sink</em>}</li>
 *   <li>{@link gecos.dag.DAGEdge#getParent <em>Parent</em>}</li>
 * </ul>
 *
 * @see gecos.dag.DagPackage#getDAGEdge()
 * @model abstract="true"
 *        annotation="gmf.link source='src' target='sink' style='dot' width='1'"
 * @generated
 */
public interface DAGEdge extends DagVisitable, AnnotatedElement {
	/**
	 * Returns the value of the '<em><b>Src</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link gecos.dag.DAGOutPort#getSinks <em>Sinks</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Src</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Src</em>' reference.
	 * @see #setSrc(DAGOutPort)
	 * @see gecos.dag.DagPackage#getDAGEdge_Src()
	 * @see gecos.dag.DAGOutPort#getSinks
	 * @model opposite="sinks" required="true"
	 * @generated
	 */
	DAGOutPort getSrc();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGEdge#getSrc <em>Src</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Src</em>' reference.
	 * @see #getSrc()
	 * @generated
	 */
	void setSrc(DAGOutPort value);

	/**
	 * Returns the value of the '<em><b>Sink</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link gecos.dag.DAGInPort#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sink</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sink</em>' reference.
	 * @see #setSink(DAGInPort)
	 * @see gecos.dag.DagPackage#getDAGEdge_Sink()
	 * @see gecos.dag.DAGInPort#getSource
	 * @model opposite="source" required="true"
	 * @generated
	 */
	DAGInPort getSink();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGEdge#getSink <em>Sink</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sink</em>' reference.
	 * @see #getSink()
	 * @generated
	 */
	void setSink(DAGInPort value);

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link gecos.dag.DAGInstruction#getEdges <em>Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' container reference.
	 * @see #setParent(DAGInstruction)
	 * @see gecos.dag.DagPackage#getDAGEdge_Parent()
	 * @see gecos.dag.DAGInstruction#getEdges
	 * @model opposite="edges" transient="false"
	 * @generated
	 */
	DAGInstruction getParent();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGEdge#getParent <em>Parent</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' container reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(DAGInstruction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 * @generated
	 */
	void accept(DagVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.dag.DAGInPort%&gt; _sink = this.getSink();\nboolean _tripleNotEquals = (_sink != null);\nif (_tripleNotEquals)\n{\n\treturn this.getSink().getParent();\n}\nreturn null;'"
	 * @generated
	 */
	DAGNode getSinkNode();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.dag.DAGOutPort%&gt; _src = this.getSrc();\nboolean _tripleNotEquals = (_src != null);\nif (_tripleNotEquals)\n{\n\treturn this.getSrc().getParent();\n}\nreturn null;'"
	 * @generated
	 */
	DAGNode getSourceNode();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.dag.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.dag.DAGOutPort%&gt; _src = this.getSrc();\n&lt;%java.lang.String%&gt; _plus = (_src + \"-&gt;\");\n&lt;%gecos.dag.DAGInPort%&gt; _sink = this.getSink();\nreturn (_plus + _sink);'"
	 * @generated
	 */
	String toString();

} // DAGEdge
