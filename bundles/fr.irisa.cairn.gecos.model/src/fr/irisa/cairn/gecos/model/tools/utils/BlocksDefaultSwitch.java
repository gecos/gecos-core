/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.tools.utils;

import fr.irisa.cairn.gecos.model.extensions.switchs.AdaptableBlockSwitch;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.SwitchBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;

/**
 *  {@link BlocksDefaultSwitch} is used to call a function for every kind of block of
 *  a procedure. It's default behavior is to analyze every contained block.
 * 
 * @see Procedure 
 * @see ProcedureSet
 * @see Block
 * 
 * @author anfloch
 */
public class BlocksDefaultSwitch<T> extends AdaptableBlockSwitch<T> {

	public BlocksDefaultSwitch() {
	
	}

	public T doSwitch(GecosProject proj) {
		for (ProcedureSet ps : proj.listProcedureSets()) {
			doSwitch(ps);
		}
		return null;
	}

	public T doSwitch(Procedure proc) {
		if (proc.getStart()!=null)	doSwitch(proc.getStart());
		if (proc.getBody()!=null)	doSwitch(proc.getBody());
		if (proc.getEnd()!=null)	doSwitch(proc.getEnd()); 
		return null;
	}
	
	public T doSwitch(ProcedureSet ps) {
		for (Procedure aProcedure : ps.listProcedures()) {
			doSwitch(aProcedure);
		}
		return null;
	}

	@Override
	public  T caseBasicBlock(BasicBlock b) {
		return null;
	}
	
	@Override
	public T caseCompositeBlock(CompositeBlock b) {
		//iterate over a copy of the list in order to prevent from ConcurrentModificationException
		Block[] tab = b.getChildren().toArray(new Block[b.getChildren().size()]);
		for (Block block  : tab) {
			doSwitch(block);
		}
		return null;
	}

	@Override
	public T caseIfBlock(IfBlock b) {
		doSwitch(b.getTestBlock());
		if (b.getThenBlock() != null)
			doSwitch(b.getThenBlock());
		if (b.getElseBlock() != null)
			doSwitch(b.getElseBlock());
		return null;
	}

	@Override
	public T caseDoWhileBlock(DoWhileBlock b) {
		doSwitch(b.getTestBlock());
		if (b.getBodyBlock() != null)
			doSwitch(b.getBodyBlock());
		return null;
	}
	
	@Override
	public T caseWhileBlock(WhileBlock b) {
		doSwitch(b.getTestBlock());
		doSwitch(b.getBodyBlock());
		return null;
	}
	
	@Override
	public T caseForBlock(ForBlock b) {
		BasicBlock initBlock = b.getInitBlock();
		BasicBlock testBlock = b.getTestBlock();
		BasicBlock stepBlock = b.getStepBlock();
		
		Block bodyBlock = b.getBodyBlock();
		
		if(initBlock!=null) doSwitch(initBlock);
		if(testBlock!=null) doSwitch(testBlock);
		if(stepBlock!=null) doSwitch(stepBlock);
		if(bodyBlock!=null) doSwitch(bodyBlock);
		return null;
	}


	@Override
	public T caseSwitchBlock(SwitchBlock b) {
		if (b.getDispatchBlock()!=null) doSwitch(b.getDispatchBlock());
		if (b.getBodyBlock()!=null) doSwitch(b.getBodyBlock());
		if (b.getDefaultBlock()!=null) doSwitch(b.getDefaultBlock());
		return null;
	}
}
