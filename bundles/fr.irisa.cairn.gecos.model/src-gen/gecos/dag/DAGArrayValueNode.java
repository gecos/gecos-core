/**
 */
package gecos.dag;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Array Value Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.dag.DagPackage#getDAGArrayValueNode()
 * @model
 * @generated
 */
public interface DAGArrayValueNode extends DAGNode, DagVisitable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGArrayValueNode(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.dag.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return \"ArrayValue\";'"
	 * @generated
	 */
	String toString();

} // DAGArrayValueNode
