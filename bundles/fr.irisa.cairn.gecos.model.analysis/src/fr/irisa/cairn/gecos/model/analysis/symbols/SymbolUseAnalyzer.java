package fr.irisa.cairn.gecos.model.analysis.symbols;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.UniqueEList;

import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDagDefaultVisitor;
import gecos.blocks.BasicBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.Symbol;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGNode;
import gecos.dag.DAGOutNode;
import gecos.dag.DAGSymbolNode;
import gecos.instrs.Instruction;
import gecos.instrs.RetInstruction;
import gecos.instrs.SymbolInstruction;

/**
 * A visitor that capture all uses of each symbol.
 * 
 * @author Antoine Floc'h - Initial contribution and API
 * 
 */
public class SymbolUseAnalyzer extends GecosBlocksInstructionsDagDefaultVisitor {
	private ProcedureSet ps;
	private Map<Symbol, List<SymbolUse>> symbolsUsesMap = new LinkedHashMap<Symbol, List<SymbolUse>>();

	public SymbolUseAnalyzer(ProcedureSet ps) {
		this.ps = ps;
	}

	public Map<Symbol, List<SymbolUse>> compute() {
		visit(ps);
		return symbolsUsesMap;
	}

	@Override
	public void visitSymbolInstruction(SymbolInstruction s) {
		addUse(new SymbolUse(s.getSymbol(), s));
	}

	@Override
	public void visitDAGSymbolNode(DAGSymbolNode s) {
		addUse(new DagSymbolUse(s.getSymbol(), s));
	}

	@Override
	public void visitDAGOutNode(DAGOutNode s) {
		addUse(new DagSymbolUse(s.getSymbol(), s));
	}

	public void addUse(SymbolUse use) {
		getSymbolsUses(use.getSymbol()).add(use);
	}

	/**
	 * Get all known uses of a symbol.
	 * 
	 * @param s
	 * @return
	 */
	public List<SymbolUse> getSymbolsUses(Symbol s) {
		List<SymbolUse> uses = symbolsUsesMap.get(s);
		if (uses == null) {
			uses = new ArrayList<SymbolUse>();
			symbolsUsesMap.put(s, uses);
		}
		return uses;
	}

	/**
	 * Get all basic blocks that are using a symbol.
	 * 
	 * @param s
	 * @return
	 */
	public List<BasicBlock> getBasicBlocksUsingSymbol(Symbol s) {
		List<SymbolUse> symbolsUses = getSymbolsUses(s);
		UniqueEList<BasicBlock> blocks = new UniqueEList<BasicBlock>();
		for (SymbolUse use : symbolsUses) {
			blocks.add(use.getBasicBlock());
		}
		return blocks;
	}

	public static class SymbolUse {
		private Symbol symbol;
		private Instruction instruction;

		public SymbolUse(Symbol symbol, Instruction instruction) {
			this.symbol = symbol;
			this.instruction = instruction;
		}

		public BasicBlock getBasicBlock() {
			return instruction.getBasicBlock();
		}

		public Symbol getSymbol() {
			return symbol;
		}

		public Instruction getInstruction() {
			return instruction;
		}

		public boolean isMandatory() {
			return instruction.getRoot() instanceof RetInstruction || isAProcedureParameter();
		}
		
		protected boolean isAProcedureParameter(){
			if(symbol.getContainingScope().getContainer() instanceof Procedure)
				return true;
			return false;
		}

		@Override
		public String toString() {
			BasicBlock basicBlock = getBasicBlock();
			int pos = basicBlock.getInstructions().indexOf(
					instruction.getRoot());
			return symbol.getName() + ":(BB" + basicBlock.getNumber() + ","
					+ "i" + pos + ")";
		}
	}

	public static class DagSymbolUse extends SymbolUse {
		private DAGNode node;

		public DagSymbolUse(Symbol symbol, DAGNode n) {
			super(symbol, (Instruction) n.eContainer());
			this.node = n;
		}

		public DAGNode getNode() {
			return node;
		}
		
		@Override
		public boolean isMandatory() {
//			if(node.getPredecessors().size()>0){
//				
//				DAGNode def = node.getPredecessors().get(0);
//				for(DAGNode n: def.getSuccessors()){
//					if(n instanceof DAGOpNode){
//						if(((DAGOpNode) n).getOpcode().equals("ret"))
//							return true;
//					}
//				}
//			}
		
			return isAProcedureParameter();
		}

		@Override
		public String toString() {
			BasicBlock basicBlock = getBasicBlock();
			int pos = ((DAGInstruction) getInstruction()).getNodes().indexOf(
					node);
			return getSymbol().getName() + ":(BB" + basicBlock.getNumber()
					+ "," + "n" + pos + ")";
		}

	}
}
