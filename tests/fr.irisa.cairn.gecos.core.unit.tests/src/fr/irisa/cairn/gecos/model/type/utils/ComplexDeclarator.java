package fr.irisa.cairn.gecos.model.type.utils;

import com.google.common.base.Strings;

public class ComplexDeclarator extends Declarator {

	Declarator inner;
	
	public ComplexDeclarator(String pointers, Declarator innerDecl, String arraysOrParams) {
		super(pointers, arraysOrParams);
		this.inner = innerDecl;
		if(inner == null)
			throw new RuntimeException("Inner Declaratorof a ComplexDeclator cannot be null: " + this);
	}
	
	@Override
	public String getIdentifier() {
		return inner.getIdentifier();
	}

	/**
	 * innermost.pointers ( ... ( inner.pointers ( this.pointers id this.arrays ) inner.arrays ) ... ) innermost.arrays
	 */
	@Override
	public String getCCode() {
		StringBuilder sb = new StringBuilder();
		
		if(!Strings.isNullOrEmpty(pointers)) sb.append(pointers).append(" ");
		sb.append(getIdentifier());
		if(!Strings.isNullOrEmpty(arrays_params)) sb.append(arrays_params);
		
		Declarator curr = this;
		Declarator in = this;
		while(in instanceof ComplexDeclarator) {
			in = ((ComplexDeclarator)in).inner;
			
//			boolean needParenth = true;
			boolean needParenth = (isPointer(curr) && isArrayOrFunction(in));
			
			if(needParenth) sb.insert(0, "(");
			if(!Strings.isNullOrEmpty(in.pointers)) sb.insert(0, in.pointers + " ");
			
			if(needParenth) sb.append(")");
			if(!Strings.isNullOrEmpty(in.arrays_params)) sb.append(in.arrays_params);
			
			curr = in;
		}
		
		return sb.toString();
	}
	
	private static boolean isPointer(Declarator d) {
		return !Strings.isNullOrEmpty(d.getPointers());
	}
	
	private static boolean isArrayOrFunction(Declarator d) {
		return !Strings.isNullOrEmpty(d.getArraysOrFunction());
	}
//
//	private List<Declarator> getAllInners() {
//		List<Declarator> inners = new ArrayList<>();
//		Declarator d = this;
//		while(d instanceof ComplexDeclarator) {
//			d = ((ComplexDeclarator)d).inner;
//			inners.add(d);
//		}
//		return inners;
//	}
	

	
}
