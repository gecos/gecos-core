project = CreateGecosProject("ProcedureInliner");
AddSourceToGecosProject(project,"./src/function_inlining.c");
CDTFrontend(project);

SaveGecosProject(project, "project_before.gecosproject");

#calls the procedure inlining transformations on the whole project
ModelProcedureInliner(project);

SaveGecosProject(project, "project_after.gecosproject");

output(project, "c", "./src-gen");