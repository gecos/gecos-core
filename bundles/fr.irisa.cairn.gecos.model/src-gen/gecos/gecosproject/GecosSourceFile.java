/**
 */
package gecos.gecosproject;

import gecos.core.ProcedureSet;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Gecos Source File</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.gecosproject.GecosSourceFile#getModel <em>Model</em>}</li>
 * </ul>
 *
 * @see gecos.gecosproject.GecosprojectPackage#getGecosSourceFile()
 * @model
 * @generated
 */
public interface GecosSourceFile extends GecosFile {
	/**
	 * Returns the value of the '<em><b>Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model</em>' containment reference.
	 * @see #setModel(ProcedureSet)
	 * @see gecos.gecosproject.GecosprojectPackage#getGecosSourceFile_Model()
	 * @model containment="true"
	 * @generated
	 */
	ProcedureSet getModel();

	/**
	 * Sets the value of the '{@link gecos.gecosproject.GecosSourceFile#getModel <em>Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model</em>' containment reference.
	 * @see #getModel()
	 * @generated
	 */
	void setModel(ProcedureSet value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.core.ProcedureSet%&gt; _model = this.getModel();\nreturn (_model != null);'"
	 * @generated
	 */
	boolean isParsed();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitSourceFile(this);'"
	 * @generated
	 */
	void accept(ProjectVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.gecosproject.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _name = this.getName();\nfinal &lt;%java.io.File%&gt; f = new &lt;%java.io.File%&gt;(_name);\nreturn f.getName();'"
	 * @generated
	 */
	String toString();

} // GecosSourceFile
