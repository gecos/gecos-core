package fr.irisa.cairn.gecos.model.analysis.instructions.comparator;

import fr.irisa.cairn.gecos.model.analysis.instructions.comparator.InstructionComparatorUtils;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.ConstantInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.NumberedSymbolInstruction;
import gecos.instrs.SymbolInstruction;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.eclipse.emf.common.util.EList;

import com.google.common.collect.LinkedHashMultimap;


/**
 * Limited to ConstantInstruction, SymbolInstruction and GenericInstruction.
 * If not in SSA form, assumes that if a symbol appears in both instructions, it's the same (with same value ?)
 * 
 * FIXME not finished/tested for MUL and DIV
 * 
 * @author aelmouss
 */
public class InstructionComparator {
	
	//TODO case of false is not supported yet (due to assumption in mergeSymbols())
	public static boolean ASSUME_SYMBOL_HAS_UNIQUE_VALUE = true; // true: a =? a return YES. false: a =? b return MAYBE.
	
	public static final String NEGATE_ANNOTATION = "InstructionComparator.NEGATION__";
	public static final String INVERSE_ANNOTATION = "InstructionComparator.INVERTION__";
	
	/**
	 * Assume that if a symbol appears in both instructions, it's the same (with same value ?)
	 * @param inst1
	 * @param inst2
	 * @return Double value if the difference is known, null otherwise.
	 */
	public static Double getDifference(Instruction inst1, Instruction inst2) {
//		System.out.println("*** getDifference: " + inst1 + " - " + inst2);
		
		Double diff = null;
		try {
			Instruction sub = GecosUserInstructionFactory.sub(inst1.copy(), inst2.copy());
			sub = InstructionComparatorUtils.makeCanonical(sub);
			diff = InstructionComparatorUtils.getValue(sub);
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		
//		System.out.println("|___ " + diff);
		return diff;
	}
	
	public static FuzzyBoolean isEquivalent(Instruction inst1, Instruction inst2) {
		Double diff = getDifference(inst1, inst2);
		FuzzyBoolean res = (diff == null ? FuzzyBoolean.MAYBE : ( diff == 0.0 ? FuzzyBoolean.YES : FuzzyBoolean.NO));
//		System.out.println("|___ " + res);
		return res;
	}

	public static void sort(GenericInstruction inst) {
		boolean isMul = false;
		if(inst.getName().equals(ArithmeticOperator.MUL.getLiteral()))
			isMul = true;
		else if(!(inst.getName().equals(ArithmeticOperator.ADD.getLiteral())))
			throw new RuntimeException("Unsupported operation: " + inst);
			
		/* 0- Factorize Negations inside MUL (e.g. MUL(-a, -2, -b, 5) => -MUL(a, 2, b, 5)) */
		if(isMul)
			factorizeNeg(inst);
		
		EList<Instruction> children = inst.getChildren();
		List<GenericInstruction> generics = new LinkedList<GenericInstruction>();
		LinkedHashMultimap<String, Instruction> symbols_m = LinkedHashMultimap.create(); //<name, [symbolInst..]>
		List<ConstantInstruction> constants = new LinkedList<ConstantInstruction>();

		for(Instruction i : children) {
			if(i instanceof GenericInstruction)
				generics.add((GenericInstruction)i);
			else if(i instanceof SymbolInstruction)
				symbols_m.put(getSymbolName((SymbolInstruction)i), (SymbolInstruction)i);
			else if(i instanceof ConstantInstruction)
				constants.add((ConstantInstruction)i);
			else
				throw new RuntimeException("not yet supported! " + i);
		}

		/* 1- Simplify/Merge symbols */
		if(!symbols_m.isEmpty()) {
			for(String name : new HashSet<String>(symbols_m.keySet())) {
				Set<Instruction> syms = symbols_m.get(name);
				if(syms.size() == 1)
					continue;
				mergeSymbols(syms, isMul, generics);
			}
		}
		
		/* 2- Simplify/Merge generics */
		LinkedHashMultimap<String, Instruction> generics_m = LinkedHashMultimap.create(); //<name, [GenericInst..]>
		if(!generics.isEmpty()) {
			for(GenericInstruction g : generics)
				generics_m.put(getGenericName(g), g);
			
			for(String name : new HashSet<String>(generics_m.keySet())) {
				Set<Instruction> gens = generics_m.get(name);
				if(gens.size() == 1)
					continue;
				mergeGenerics(gens, isMul);
			}
		}
		
		/* 3- Simplify/Merge constants */
		if(!constants.isEmpty()) {
			mergeConstants(constants, isMul);
			assert(constants.size() <= 1);
		}
		
		/* Sort children: generics first, then symbols, then constant */
		children.clear();
		for(String name : new TreeSet<String>(generics_m.keySet()))
			children.addAll(generics_m.get(name));
		for(String name : new TreeSet<String>(symbols_m.keySet()))
			children.addAll(symbols_m.get(name));
		children.addAll(constants);
	}

	/**
	 * 
	 * @param inst
	 */
	private static void factorizeNeg(GenericInstruction inst) {
		assert(inst.getName().equals(ArithmeticOperator.MUL.getLiteral()));
		
		int nbNegs = 0;
		for(Instruction i : inst.getChildren()) {
			if(isNeg(i)) {
				nbNegs++;
				negate(i);
			}
		}
		
		if(nbNegs % 2 != 0)
			negate(inst);
	}

	private static void mergeGenerics(Set<Instruction> generics, final boolean isMulParent) {
		eliminateInversePairs(generics, isMulParent);
		
		/* factorize sign */
//		if(isMulParent) {
//			for(Instruction g : generics) {
//				if(isNeg(g))
//			}
//		}
	}

	/**
	 * Identical Symbols are merged: inverse pairs are eliminated, the rest (in case of ADD only), 
	 * if more than one (n), are replaced with a "MUL(symbol, n)" properly sorted.
	 * 
	 * NOTE: Assume that a Symbol (not in SSA form) value is the same (such that a - a = 0).
	 * TODO use this assumption only when ASSUME_SYMBOL_HAS_UNIQUE_VALUE is true
	 * 
	 * @param symbols
	 * @param isMul
	 * @param generics
	 */
	private static void mergeSymbols(Set<Instruction> symbols, final boolean isMul, List<GenericInstruction> generics) {
//		if(!ASSUME_SYMBOL_HAS_UNIQUE_VALUE) {
//			if(!(symbols.iterator().next() instanceof NumberedSymbolInstruction)) {
//				//TODO group positives and negatives but do not eliminate inverse Pairs
//			}
//		}
		
		int nRemPos = eliminateInversePairs(symbols, isMul);
		assert(Math.abs(nRemPos) == symbols.size());
		
		/* merge the remainder in case of ADD only */
		if(!isMul && (symbols.size() > 1)) {
			Instruction sym = symbols.iterator().next();
			if(isNeg(sym)) negate(sym);
			GenericInstruction replace = GecosUserInstructionFactory.mul(sym, GecosUserInstructionFactory.Int((Math.abs(nRemPos))));
			if(nRemPos < 0) negate(replace);
			generics.add(replace);
			symbols.clear();
		}
	}

	private static int eliminateInversePairs(Set<Instruction> identicalChildren, final boolean isMulParent) {
		List<Instruction> inverses = new ArrayList<>();
		List<Instruction> positives = new ArrayList<>();
		for(Instruction s : identicalChildren) {
			if(isInverse(s, isMulParent)) 
				inverses.add(s);
			else
				positives.add(s);
		}
		
		/* Eliminate all inverse pairs */
		if(positives.size() >= inverses.size()) {
			identicalChildren.removeAll(inverses);
			identicalChildren.removeAll(positives.subList(0, inverses.size()));
		}
		else {
			identicalChildren.removeAll(positives);
			identicalChildren.removeAll(inverses.subList(0, positives.size()));
		}
		
		return positives.size() - inverses.size();
	}
	
	/**
	 * Assumes that all constant instructions have POSITIVE values and sign information is carried by annotation!
	 * 
	 * @param list of {@link ConstantInstruction}s.
	 * @param isMul : true if operation is a multiplication, false if an addition.
	 */
	private static void mergeConstants(List<ConstantInstruction> list, boolean isMul) {
		boolean isInt = true;
		double val = (isMul? 1 : 0);

		double c;
		for(ConstantInstruction inst : list) {
			if(inst instanceof IntInstruction) {
				c = ((IntInstruction)inst).getValue();
			} else if(inst instanceof FloatInstruction) {
				c = ((FloatInstruction)inst).getValue();
				isInt = false;
			}
			else 
				throw new RuntimeException("Unexpected instruction! otherConstants must all be ConstantInstruction (in canonical form). " + inst);
			assert(c >= 0);
			
			if(isNeg(inst)) c = -c;
			val = (isMul? val*c : val+c);
		}
		
		list.clear();
		if((isMul && val != 1) || (!isMul && val != 0))
			list.add((isInt? GecosUserInstructionFactory.Int((long)val) : GecosUserInstructionFactory.Float(val)));
	}
	
	private static String getSymbolName(SymbolInstruction s) {
		String name1 = s.getSymbol().getName();
		if(s instanceof NumberedSymbolInstruction)
			name1 += ":" + ((NumberedSymbolInstruction) s).getNumber();
		return name1;
	}
	
	private static String getGenericName(GenericInstruction g) {
//		return g.getName();
		return g.toString(); //allow sorting according to children as well
	}

	public static void negate(Instruction i) {
		if(InstructionComparator.isNeg(i))
			i.getAnnotations().removeKey(InstructionComparator.NEGATE_ANNOTATION);
		else
			i.setAnnotation(InstructionComparator.NEGATE_ANNOTATION, null);
	}
	
	//XXX make sure to inverse value of ConstantInstruction at the end !!!
	public static void inverse(Instruction i) {
		if(InstructionComparator.isInv(i))
			i.getAnnotations().removeKey(InstructionComparator.INVERSE_ANNOTATION);
		else
			i.setAnnotation(InstructionComparator.INVERSE_ANNOTATION, null);
	}
	
	private static boolean isInverse(Instruction s, boolean isMul) {
		if(isMul)
			return isInv(s);
		return isNeg(s);
	}

	public static boolean isNeg(Instruction i) {
		return i.getAnnotations().containsKey(InstructionComparator.NEGATE_ANNOTATION);
	}
	
	public static boolean isInv(Instruction i) {
		return i.getAnnotations().containsKey(InstructionComparator.INVERSE_ANNOTATION);
	}

	public static String instToString(Instruction i) {
		if(i == null) return "";
		
		StringBuffer sb = new StringBuffer();
		
		if(isNeg(i))
			sb.append("-(");
		else if(isInv(i))
			sb.append("1/(");
		
		if(i instanceof GenericInstruction) {
			GenericInstruction g = (GenericInstruction)i;
			assert(g.getName().equals(ArithmeticOperator.ADD.getLiteral()) ||
				   g.getName().equals(ArithmeticOperator.MUL.getLiteral()));
			
			sb.append(g.getName()).append("(");
			sb.append(instToString(g.getChild(0)));
			for(int child = 1; child < g.getChildrenCount(); ++child)
				sb.append(", ").append(instToString(g.getChild(child)));
			sb.append(")");
		}
		else
			sb.append(i.toString());
		
		if(isNeg(i) || isInv(i))
			sb.append(")");
		
		return sb.toString();
	}

	public static String instToString(Instruction... list) {
		if(list.length > 0) {
			StringBuffer sb = new StringBuffer();
			sb.append(instToString(list[0]));
			for(int i = 1; i < list.length; ++i)
				sb.append(", ").append(instToString(list[i]));
			return sb.toString();
		}
		return "";
	}
	
//	private static void sortGenerics(List<GenericInstruction> list) {
//		Collections.sort(list, new Comparator<GenericInstruction>() {
//			@Override
//			public int compare(GenericInstruction i1, GenericInstruction i2) {
//				return getGenericName(i1).compareTo(getGenericName(i2));
//			}
//		});
//	}
//	
//	private static Comparator<SymbolInstruction> symbolComparator = new Comparator<SymbolInstruction>() {
//	@Override
//	public int compare(SymbolInstruction i1, SymbolInstruction i2) {
//		String name1 = getSymbolName(i1);
//		String name2 = getSymbolName(i2);
//		return name1.compareTo(name2);
//	}
//};
//
//private static void sortSymbols(List<String> sym) {
//	Collections.sort(sym, symbolComparator);
//}
	
//	private static void sortConstants(List<ConstantInstruction> list) {
//		Collections.sort(list, new Comparator<ConstantInstruction>() {
//			@Override
//			public int compare(ConstantInstruction i1, ConstantInstruction i2) {
//				if(i1 instanceof IntInstruction) {
//					if(i2 instanceof IntInstruction)
//						return ((IntInstruction) i1).getValue() < ((IntInstruction) i2).getValue() ? -1 : 1;
//					if(i2 instanceof FloatInstruction)
//						return ((IntInstruction) i1).getValue() < ((FloatInstruction) i2).getValue() ? -1 : 1;
//				}
//				else if(i1 instanceof FloatInstruction) {
//					if(i2 instanceof IntInstruction)
//						return ((FloatInstruction) i1).getValue() < ((IntInstruction) i2).getValue() ? -1 : 1;
//					if(i2 instanceof FloatInstruction)
//						return ((FloatInstruction) i1).getValue() < ((FloatInstruction) i2).getValue() ? -1 : 1;
//				}
//				return 0;
//			}
//		});
//	}
	

	
//	static { GecosUserTypeFactory.setScope(GecosUserCoreFactory.scope()); }
//	public static void main(String[] args) {
//		Instruction a = GecosUserInstructionFactory.symbref(GecosUserCoreFactory.symbol("a", GecosUserTypeFactory.INT()));
//		Instruction b = GecosUserInstructionFactory.symbref(GecosUserCoreFactory.symbol("b", GecosUserTypeFactory.INT()));
//		Instruction one = GecosUserInstructionFactory.Int(1);
//		Instruction two = GecosUserInstructionFactory.Int(2);
//		Instruction five = GecosUserInstructionFactory.Int(5);
//		
//		Instruction apb = GecosUserInstructionFactory.add(b.copy(), a.copy());
//		Instruction bpa = GecosUserInstructionFactory.add(a.copy(), b.copy());
//		Instruction ap1 = GecosUserInstructionFactory.add(a.copy(), one.copy());
//		Instruction bp1 = GecosUserInstructionFactory.add(b.copy(), one.copy());
//		Instruction bs2 = GecosUserInstructionFactory.sub(b.copy(), two.copy());
//		Instruction _2sb = GecosUserInstructionFactory.sub(two.copy(), b.copy());
//		Instruction _5sa = GecosUserInstructionFactory.sub(five.copy(), a.copy());
//
//		Instruction apbp1 = GecosUserInstructionFactory.add(apb.copy(), one.copy());
//		Instruction ap1pb = GecosUserInstructionFactory.add(ap1.copy(), b.copy());
//		Instruction bp1pa = GecosUserInstructionFactory.add(bp1.copy(), a.copy());
//		
//		Instruction apbp1p5 = GecosUserInstructionFactory.add(apbp1.copy(), five.copy());
//		Instruction _5pbp1pa = GecosUserInstructionFactory.add(five.copy(), bp1pa.copy());
//		
//		Instruction _5sapb = GecosUserInstructionFactory.add(_5sa.copy(),b.copy());
//		
//		Instruction i1 = GecosUserInstructionFactory.add(GecosUserInstructionFactory.add(one.copy(), apbp1p5.copy()), apb.copy());
//		Instruction i2 = GecosUserInstructionFactory.add(one.copy(), GecosUserInstructionFactory.add(_5pbp1pa.copy(), bpa.copy()));
//		
//		InstructionComparator.isEquivalent(apb.copy(), bpa.copy()); //yes
//		InstructionComparator.isEquivalent(apbp1.copy(), ap1pb.copy()); //yes
//		InstructionComparator.isEquivalent(apbp1.copy(), bp1pa.copy()); //yes
//		InstructionComparator.isEquivalent(apbp1p5.copy(), _5pbp1pa.copy()); //yes
//		InstructionComparator.isEquivalent(i1.copy(), i2.copy()); //yes
//		
//		InstructionComparator.isEquivalent(bs2.copy(), bs2.copy()); //yes
//		InstructionComparator.isEquivalent(bs2.copy(), _2sb.copy()); //maybe
//	}
	
}
