package fr.irisa.cairn.gecos.model.analysis.profiling;


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDefaultVisitor;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.core.Procedure;
import gecos.core.Symbol;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;

public class AssignmentFinderCustom extends GecosBlocksInstructionsDefaultVisitor{
	
	private Map<Symbol, List<BasicBlock>> assignments;
	private BasicBlock currentBasic;
	
	private Procedure proc;
	private Block block;
	private int mode;
	
	public static final int ARRAYS =1;
	public static final int SCALARS=2;
	public static final int POINTERS=4;
	
	public AssignmentFinderCustom(Procedure proc, int mode ) {
		this.proc = proc;
		this.mode= mode;
	}

	public AssignmentFinderCustom() {
		this.mode=ARRAYS+SCALARS;
	}
 
	public AssignmentFinderCustom(Block block, int mode ) {
		this.block = block;
		this.mode= mode;
	}
	
	public Map<Symbol, List<BasicBlock>> compute(){
		this.assignments = new LinkedHashMap<Symbol, List<BasicBlock>>();
		if (proc != null)
			proc.getBody().accept(this);
		else if (block != null)
			block.accept(this);
		return assignments;
	}
	
	@Override
	public void visitBasicBlock(BasicBlock b) {
		currentBasic = b;
		super.visitBasicBlock(b); 
	}
	@Override
	public void visitSetInstruction(SetInstruction inst) {
		if (inst.getDest() instanceof SymbolInstruction) {
			SymbolInstruction sym = (SymbolInstruction) inst.getDest();
			if (sym.getSymbol() == null) {
				System.out.println("*** " + sym.getSymbolName()
						+ " is not declare in a scope");
				return;
			}
			addAssignment(sym.getSymbol(), currentBasic);
		}else if (inst.getDest() instanceof ArrayInstruction){
			ArrayInstruction arr = (ArrayInstruction) inst.getDest();
			if (arr.getDest() instanceof SymbolInstruction){
				SymbolInstruction sym = (SymbolInstruction) arr.getDest();
				if (sym.getSymbol() == null) {
					System.out.println("*** " + sym.getSymbolName()
							+ " is not declare in a scope");
					return;
				}
				addAssignment(sym.getSymbol(), currentBasic);
			}
		}
	}
	
	private void addAssignment(Symbol symbol, BasicBlock b) { 
		List<BasicBlock> blocks = assignments.get(symbol);
		if (blocks == null)
			assignments.put(symbol, blocks = new ArrayList<BasicBlock>()); 
		blocks.add(b);
	}
}
