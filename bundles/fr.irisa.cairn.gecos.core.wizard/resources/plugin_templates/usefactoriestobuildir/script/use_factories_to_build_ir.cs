
# Build a GeCoS IR thanks to factories 
project_from_factories = BuildIRWithFactory(); # Call the script command which correspond to our GeCoS module
SaveGecosProject(project_from_factories, "project_from_factories.gecosproject");
output(project_from_factories, "c", "output"); # Generate the C source code from the GeCoS IR

#Build a GeCoS IR from a C source file for comparison
project_from_c = CreateGecosProject("dummy_from_c");
AddSourceToGecosProject(project_from_c, "src-c/factories_example.c");
CDTFrontend(project_from_c);
SaveGecosProject(project_from_c, "project_from_c.gecosproject");