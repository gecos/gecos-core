
debug(1);

projectname = "testssa";

#initialize project
project = CreateGecosProject(projectname);

#add files to the projects (sourdes and includes)
AddSourceToGecosProject(project,"./testssa.c");
AddIncDirToGecosProject(project,"./include/");

GecosProjectBuilder(project);

for model in project do
	for proc in model do
		ModelSSAAnalyzer(proc);
	done;
done;

SaveGecosProject(project,"./ssa.gecosproject");

echo("done");