package fr.irisa.cairn.gecos.model.transforms.blocks.simplifier;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.tools.visitors.GecosDefaultVisitor;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.core.Procedure;
import gecos.gecosproject.GecosProject;

public class MergeCompositeBlocks extends GecosDefaultVisitor {
	
	private GecosProject proj;

	public MergeCompositeBlocks() {
		this(null);
	}
	public MergeCompositeBlocks(GecosProject p) {
		super(false,false,false);
		this.proj = p;
	}
	
	public void compute() {
		if (proj != null)
			proj.accept(this);
	}
	
	public void visitCompositeBlock(CompositeBlock c) {
		super.visitCompositeBlock(c);
		if (filter(c))
			return;
		CompositeBlock parent = (CompositeBlock) c.getParent();
		//first merge scope
		parent.getScope().mergeWith(c.getScope());
		
		//move blocks from c to parent
		EList<Block> children = parent.getChildren();
		int indexOf = children.indexOf(c);
		if (indexOf == -1)
			throw new RuntimeException("Error");
		
		int size = c.getChildren().size();
		for (int i = size-1; i >= 0; i--) {
			children.add(indexOf, c.getChildren().get(i));
		}
		
		//remove c
		parent.removeBlock(c);
	}
	
	/**
	 * Returns true if the CompositeBlock should <b>NOT</b> be merged
	 * @param c
	 * @return
	 */
	private boolean filter(CompositeBlock c) {

		//ignore CompositeBlocks with annotations
		if (c.getAnnotations().size() > 0)
			return true;
		
		//merge nested CompositeBlocks only
		if (!(c.getParent() instanceof CompositeBlock))
			return true;
		
		//ignore the first CompositeBlock of the Procedure's body
		if (c.getParent() != null && c.getParent().eContainer() instanceof Procedure)
			return true;
		
		return false;
	}

}
