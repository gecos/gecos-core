package fr.irisa.cairn.gecos.model.extensions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.spi.RegistryContributor;
import org.osgi.framework.Bundle;

import fr.irisa.cairn.gecos.model.GecosModelPlugin;

/**
 * Abstract class providing tools related to eclipse {@link IExtension}.
 * 
 * @author Antoine Floc'h - Initial contribution and API
 * 
 */
public abstract class GecosRegistry {

	/**
	 * Get all extensions for a given extension point identifier.
	 * 
	 * @param point
	 *            extension point id
	 * @return
	 */
	protected List<IExtension> getExtensions(String point) {

		List<IExtension> extensions = new ArrayList<IExtension>();
		IExtensionRegistry reg = Platform.getExtensionRegistry();
		if (reg != null) {
			IExtensionPoint iExtensionPoint = reg.getExtensionPoint(point);
			if (iExtensionPoint == null)
				throw new RuntimeException("Could not find extension : " + point);
			IExtension[] ext = iExtensionPoint.getExtensions();
			for (IExtension iExtension : ext) {
				extensions.add(iExtension);
			}
		}

		return extensions;
	}

	/**
	 * Get the class pointed by a java {@link IConfigurationElement} identifier.
	 * 
	 * @param <A>
	 *            type of the class
	 * @param className
	 *            name of the class
	 * @param conf
	 * @return
	 */
	@SuppressWarnings({ "unchecked" })
	protected <A> Class<A> getClass(String className, IConfigurationElement conf) {
		String id = ((RegistryContributor) conf.getContributor()).getId();
		long OSGiId;
		try {
			OSGiId = Long.parseLong(id);
		} catch (NumberFormatException e) {
			return null;
		}
		Bundle contributingBundle = GecosModelPlugin.getDefault().getContext().getBundle(OSGiId);
		// load the requested class from this bundle
		try {
			return (Class<A>) contributingBundle.loadClass(className);
		} catch (ClassNotFoundException e) {
			//System.err.println("Warning Could not load class "+e.getMessage()+" from bundle "+contributingBundle.getSymbolicName());
			//return null;
			throw new RuntimeException("Could not load class "+e.getMessage()+" from bundle "+contributingBundle.getSymbolicName());
		}
	}
}
