package fr.irisa.cairn.gecos.model.extensions.outputs;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;

import fr.irisa.cairn.gecos.model.extensions.GecosRegistry;
import fr.irisa.cairn.gecos.model.extensions.generators.IGecosCodeGenerator;

/**
 * 
 * @author Antoine Floc'h - Initial Contribution and API
 * 
 */
public final class GecosOutputRegistry extends GecosRegistry {

	public final static GecosOutputRegistry INSTANCE = new GecosOutputRegistry();

	private final static String EXTENSION_POINT = "fr.irisa.cairn.gecos.model.GecosOutput";
	private final static String PRINTER_ATTRIBUTE = "printer";
	private final static String TARGET_ATTRIBUTE = "target";
	private final static String FORMAT_ATTRIBUTE = "format";

	private GecosOutputRegistry() {

	}

	public GecosOutput getOutputPrinter(Object object, String format) {
		for (IExtension e : getExtensions(EXTENSION_POINT)) {
			
			for (IConfigurationElement iConfigurationElement : e
					.getConfigurationElements()) {

				String extensionFormat = iConfigurationElement
						.getAttribute(FORMAT_ATTRIBUTE);
				String printerClassID = iConfigurationElement
						.getAttribute(PRINTER_ATTRIBUTE);
				String targetClassID = iConfigurationElement
						.getAttribute(TARGET_ATTRIBUTE);
				if (format.equals(extensionFormat)) {
					
					Class<IGecosCodeGenerator> targetType = getClass(
							targetClassID, iConfigurationElement);

					Class<? extends Object> objectClass = object.getClass();
					boolean selected = objectClass == targetType;
					if (targetType.isInterface()) {
						selected = targetType.isInstance(object);
					}
					if (selected) {
						Class<GecosOutput> extensionClass = getClass(
								printerClassID, iConfigurationElement);

						try {
							return extensionClass.newInstance();
						} catch (Exception e1) {
							throw new RuntimeException(e1);
						}
					}
				}

			}
		}
		return null;
	}

}
