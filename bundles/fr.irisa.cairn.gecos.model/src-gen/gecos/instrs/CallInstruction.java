/**
 */
package gecos.instrs;

import gecos.core.Symbol;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Call Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.CallInstruction#getArgs <em>Args</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getCallInstruction()
 * @model
 * @generated
 */
public interface CallInstruction extends AddressComponent {
	/**
	 * Returns the value of the '<em><b>Args</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.instrs.Instruction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Args</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Args</em>' containment reference list.
	 * @see gecos.instrs.InstrsPackage#getCallInstruction_Args()
	 * @model containment="true"
	 * @generated
	 */
	EList<Instruction> getArgs();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitCallInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; _xblockexpression = null;\n{\n\tfinal &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; list = new &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;();\n\tlist.add(this.getAddress());\n\tlist.addAll(this.getArgs());\n\t_xblockexpression = &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;unmodifiableEList(list);\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	EList<Instruction> listChildren();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _elvis = null;\n&lt;%gecos.instrs.Instruction%&gt; _address = this.getAddress();\n&lt;%java.lang.String%&gt; _string = null;\nif (_address!=null)\n{\n\t_string=_address.toString();\n}\nif (_string != null)\n{\n\t_elvis = _string;\n} else\n{\n\t_elvis = \"?\";\n}\n&lt;%java.lang.String%&gt; _plus = (\"call \" + _elvis);\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \"(\");\n&lt;%java.lang.String%&gt; _join = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.join(this.getArgs(), \",\");\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + _join);\nreturn (_plus_2 + \")\");'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.instrs.Instruction%&gt; _address = this.getAddress();\nif ((_address instanceof &lt;%gecos.instrs.SymbolInstruction%&gt;))\n{\n\t&lt;%gecos.instrs.Instruction%&gt; _address_1 = this.getAddress();\n\treturn ((&lt;%gecos.instrs.SymbolInstruction%&gt;) _address_1).getSymbol();\n}\n&lt;%java.lang.String%&gt; _plus = (this + \" is not a Simple Call Instruction\");\nthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus);'"
	 * @generated
	 */
	Symbol getProcedureSymbol();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getProcedureSymbol().getName();'"
	 * @generated
	 */
	String getSymbolAddress();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model argUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getArgs().add(arg);'"
	 * @generated
	 */
	void addArg(Instruction arg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _isSymbol = this.getAddress().isSymbol();\nreturn (!_isSymbol);'"
	 * @generated
	 */
	boolean isSimpleCallInstruction();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.CallInstruction%&gt;))\n{\n\treturn super.isSame(instr);\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // CallInstruction
