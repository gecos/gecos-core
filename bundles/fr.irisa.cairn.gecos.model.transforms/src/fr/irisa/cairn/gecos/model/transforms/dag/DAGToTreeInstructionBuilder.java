package fr.irisa.cairn.gecos.model.transforms.dag;

import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserDagFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.tools.controlflow.BuildControlFlow;
import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDefaultVisitor;
import fr.irisa.cairn.gecos.model.transforms.dag.DAGToTreeSequentialScan.Dag2TreeException;
import fr.irisa.cairn.gecos.model.transforms.dag.DAGToTreeSequentialScan.OperatorBuilder;
import gecos.blocks.BasicBlock;
import gecos.blocks.CompositeBlock;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.dag.DAGCallNode;
import gecos.dag.DAGControlEdge;
import gecos.dag.DAGDataEdge;
import gecos.dag.DAGEdge;
import gecos.dag.DAGFieldInstruction;
import gecos.dag.DAGFloatImmNode;
import gecos.dag.DAGInPort;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGIntImmNode;
import gecos.dag.DAGNode;
import gecos.dag.DAGOpNode;
import gecos.dag.DAGOperator;
import gecos.dag.DAGOutNode;
import gecos.dag.DAGOutPort;
import gecos.dag.DAGPatternNode;
import gecos.dag.DAGSimpleArrayNode;
import gecos.dag.DAGStringImmNode;
import gecos.dag.DAGSymbolNode;
import gecos.dag.DAGVectorArrayAccessNode;
import gecos.dag.DAGVectorExpandNode;
import gecos.dag.DAGVectorExtractNode;
import gecos.dag.DAGVectorOpNode;
import gecos.dag.DAGVectorPackNode;
import gecos.dag.DAGVectorShuffleNode;
import gecos.dag.DependencyType;
import gecos.dag.util.DagSwitch;
import gecos.instrs.CallInstruction;
import gecos.instrs.FieldInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.InstrsFactory;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.RangeArrayInstruction;
import gecos.instrs.RetInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SimdExpandInstruction;
import gecos.instrs.SimdExtractInstruction;
import gecos.instrs.SimdGenericInstruction;
import gecos.instrs.SimdPackInstruction;
import gecos.instrs.SimdShuffleInstruction;
import gecos.instrs.SimpleArrayInstruction;
import gecos.instrs.StringInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.PtrType;
import gecos.types.Type;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;

public class DAGToTreeInstructionBuilder extends DagSwitch<Instruction> {

	protected List<Instruction> instructions; //SetInstructions
	private int depth;
	private int maxDepth;
	private Scope scope;
	private final Map<DAGOutPort, Symbol> dataDefinitions;

	protected String tmpSymbolPrefix = "d2ttmp";
	protected String tmpVectorSymbolPrefix = "v";
	
	private static final boolean DEBUG = false;
	private static int INDEX = 3;
	private static final void debug(String str) {
		if(DEBUG) {
			for(int i=0; i<INDEX; i++)
				System.out.print("   ");
			System.out.println(str);
		}
	}
	private static final void incINDEX() { if(DEBUG) INDEX ++; }
	private static final void decINDEX() { if(DEBUG) INDEX --; }

	public DAGToTreeInstructionBuilder(int maxDepth) {
		this.maxDepth = maxDepth;
		this.instructions = new UniqueEList<Instruction>();
		this.dataDefinitions = new LinkedHashMap<DAGOutPort, Symbol>();
	}

	/**
	 * Pop all built instructions (clear list of previously built instructions).
	 * 
	 * @return
	 */
	public List<Instruction> popInstructions() {
		List<Instruction> result = new UniqueEList<Instruction>(instructions);
		instructions.clear();
		return result;
	}

	private DAGInstruction currentDagInstruction;
	protected Set<DAGNode> visitedNodes;
	
	public boolean isNodeVisited(DAGNode n) {
		return (this.visitedNodes != null) && (this.visitedNodes.contains(n));
	}
	

	public Instruction build(DAGOutPort valueDefinition, boolean cut) {
		if(DEBUG) debug("build Port: " + valueDefinition);
		incINDEX();
		DAGNode sourceNode = valueDefinition.getParent();
		// DAGInstruction parent = sourceNode.getParent();
		// if (currentDagInstruction != parent)
		// initialize(parent);
		Symbol sym = dataDefinitions.get(valueDefinition);
		Instruction instruction;
		if (sym == null) {
			manageControl(sourceNode);
			instruction = doSwitch(sourceNode);
			boolean sourceIsPattern = sourceNode instanceof DAGPatternNode;
			DAGOutNode explicitOutput = getExplicitOutput(sourceNode);
			if (sourceIsPattern) {
				instruction = getDataDefinition(valueDefinition);
			}  
			if(explicitOutput!=null) { //XXX do not do it if sourcenode is a SymbolNode
				if(DEBUG) debug("! explicit Output");
				cut(explicitOutput.getSymbol(),instruction,sourceNode);
				decINDEX();
				visitedNodes.add(sourceNode);
				return GecosUserInstructionFactory.symbref(explicitOutput.getSymbol());
			}
			if (cut || needACut(sourceNode)) {
				if(DEBUG) debug("! need a cut");
				// If maximal instruction
				// depth is reached then cut the instruction
				Type cutType = valueDefinition.getParent().getType();
				sym = createTempSymbol(cutType);
				instruction = cut(sym, instruction, sourceNode);
			}
			visitedNodes.add(sourceNode);
		} else {
			instruction = GecosUserInstructionFactory.symbref(sym);
		}
		decINDEX();
		if(DEBUG) debug("|->" + instruction);
		return instruction;
	}

	public Symbol getSymbol(DAGOutPort n) {
		return dataDefinitions.get(n);
	}

	protected boolean isSelfAffectation(DAGOutNode output, Instruction value) {
		if (value instanceof SymbolInstruction) {
			if (((SymbolInstruction) value).getSymbol() == output.getSymbol())
				return true;
		}
		return false;
	}

	public Instruction build(DAGNode n) {
		if(DEBUG) debug("build Node: " + n);
		incINDEX();
		manageControl(n);
		if (n.getOutputs().size() > 0) {
			decINDEX();
			return build(n.getOutputs().get(0), false);
		}
		else {
			manageControl(n);
			Instruction instruction = doSwitch(n);
			visitedNodes.add(n);
			decINDEX();
			return instruction;
		}
	}

	protected Instruction getDataDefinition(DAGOutPort port) {
		Symbol sym = dataDefinitions.get(port);
		if (sym != null)
			return GecosUserInstructionFactory.symbref(sym);
		else
			return build(port.getParent());
	}

	protected void manageControl(DAGNode n) {
		if(DEBUG) debug(">>START managing control");
		incINDEX();
		if (currentDagInstruction != n.getParent()){
			if(n.getParent()==null)
				n.setParent(currentDagInstruction);
			initialize(n.getParent());
		}
		List<DAGOutPort> controlPredecessors = getControlPredecessors(n);
		Collections.sort(controlPredecessors, new DagOutTopologicalSorter());
		if (controlPredecessors.size() > 0) {
			for (DAGOutPort pred : controlPredecessors) {
				if (!isNodeVisited(pred.getParent())) {
					DAGNode predNode = pred.getParent();
					
					//XXX
					DAGControlEdge ce = null;
					for(DAGInPort in : n.getControlInputs()) {
						 if(in.getSourceNode() == predNode) {
							ce = (DAGControlEdge)in.getSource();
						 	break;
						}
					}
					
					// If RAW or WAW dependency then predNode is a Write and must be built first.
					if(ce.getType().equals(DependencyType.RAW) || ce.getType().equals(DependencyType.WAW)) {
						if(predNode instanceof DAGSimpleArrayNode && !predNode.getDataSuccessors().isEmpty()) {
							DAGNode setNode = predNode.getDataSuccessors().get(0);
							if(setNode instanceof DAGOpNode && ((DAGOpNode) setNode).getOpcode().equals(DAGOperator.SET.getLiteral())) {
								build(setNode);
							}
						}
						else {
							build(predNode);
						}
					}
					
					//If WAR then predNode is a read and must be built and cut first.
					else if(ce.getType().equals(DependencyType.WAR)) {
//						build(pred, true);
						Instruction predInst = build(predNode);
						Symbol sym = getNewTempSymbol(pred.getParent().getParent(), pred.getParent().getType(), tmpSymbolPrefix);
						cut(sym, predInst, pred.getParent());
					}
				}
				else
					if(DEBUG) debug(pred + ": already visited");
			}
		}
		decINDEX();
		if(DEBUG) debug("<<FINISH managing control");
	}

	private List<DAGOutPort> getControlPredecessors(DAGNode n) {
		EList<DAGOutPort> preds = new BasicEList<DAGOutPort>();
		for (DAGInPort pin : n.getControlInputs()) {
//			if (pin.getSource() != null && (pin.getSource() instanceof DAGControlEdge)) {
				DAGOutPort src = pin.getSource().getSrc();
				preds.add(src);
//			}
		}
		return preds;
	}

	private void initialize(DAGInstruction i) {
		if(DEBUG) debug("INITIALIZE: " + i.getBasicBlock());
		this.currentDagInstruction = i;
		if(i.getBasicBlock() != null && i.getBasicBlock().getScope() != null)
			this.scope = i.getBasicBlock().getScope();
		else
			this.scope = GecosUserCoreFactory.scope();
		this.visitedNodes = new LinkedHashSet<DAGNode>();
	}

	private boolean needACut(DAGNode n) {
		if (maxDepth != DAGToTreeSequentialScan.DEPTH_INFINITE
				&& depth >= maxDepth) {
			if (n instanceof DAGOpNode) {
				String op = ((DAGOpNode) n).getOpcode();
				if (op.equals("CVT"))
					return false;
				if (op.equals("INDIR"))
					return false;
				if (op.equals("ret"))
					return false;
				return true;
			}
			return false;

		}
		return false;
	}

	/**
	 * When maximal depth of an instruction is reached: create a temp variable
	 * and set instruction to store the result of the instruction.
	 * 
	 * @param sym
	 * @param i
	 * @param n
	 * @return
	 */
	private Instruction cut(Symbol sym, Instruction i, DAGNode n) {
		SymbolInstruction isym = GecosUserInstructionFactory.symbref(sym);
		SetInstruction cutInstruction = GecosUserInstructionFactory.set(isym, i); //XXX use set()
		notifySet(n.getOutputs().get(0), sym);
		instructions.add(cutInstruction);
		if(DEBUG) debug("cut create SetInstruction: " + cutInstruction);
		
		depth -= depth(i);
		return GecosUserInstructionFactory.symbref(sym);
	}
	
	private Instruction cutVector(Instruction i, DAGNode n) {
		Symbol sym = getNewTempSymbol(n.getParent(), i.getType(), tmpVectorSymbolPrefix);
		return cut(sym, i, n);
	}
	
	
	private int depth(Instruction i) {
		return DepthAnalyzer.INSTANCE.depth(i);
	}

	private static class DepthAnalyzer extends
			GecosBlocksInstructionsDefaultVisitor {
		private int depth;

		public final static DAGToTreeInstructionBuilder.DepthAnalyzer INSTANCE = new DepthAnalyzer();

		public int depth(Instruction i) {
			depth = 0;
			i.accept(this);
			return depth;
		}

		@Override
		public void visitGenericInstruction(GenericInstruction g) {
			depth++;
			super.visitGenericInstruction(g);
		}
	}

	/**
	 * Get an eventual output in the successors of a node.
	 * ONLY if the output does not have any unresolved Control dependency !!!
	 * 
	 * @param n
	 * @return
	 */
	private DAGOutNode getExplicitOutput(DAGNode n) {
		for (DAGNode succ : getDataSuccessors(n)) {
			if (succ instanceof DAGOutNode) {
				for(DAGInPort inport : succ.getControlInputs())
					if(!isNodeVisited(inport.getSourceNode()))
						return null;
				return ((DAGOutNode) succ);
			}
		}
		return null;
	}

	//XXX use createTempSymbol
	private Symbol getNewTempSymbol(Instruction instruction, Type type, String prefix) {
		Scope scope = instruction.getBasicBlock().getScope();
		int i = 1;
		while (scope.lookup(prefix + i) != null)
			i++;
		scope = ((CompositeBlock)instruction.getBasicBlock().getContainingProcedure().getBody()).getChildren().get(1).getScope();
		return GecosUserCoreFactory.symbol(prefix + i, type, scope);
	}
	
	protected Symbol createTempSymbol(Type type) {
		String name = getNextTempSymbolName();
		Symbol sym = GecosUserCoreFactory.symbol(name, type);
		scope.getSymbols().add(sym);
		return sym;
	}

	private String getNextTempSymbolName() {
		int t = 0;
		String name = tmpSymbolPrefix + t;
		while (scope.lookup(name) != null) {
			t++;
			name = tmpSymbolPrefix + t;
		}
		return name;
	}

	@Override
	public Instruction caseDAGOpNode(DAGOpNode object) {
//		if(DEBUG) debug("caseOpNode "+object);
//		incINDEX();
		if (object.getType() == null) {
			throw new UnsupportedOperationException("DAGNode " + object
					+ " has no (null) type information");
		}
		Instruction res = opSwitch.doSwitch(object);
		
//		decINDEX();
//		if(DEBUG) debug("|->" + res);
		return res;
	}

	private DagOpSwitch opSwitch = new DagOpSwitch();
	protected final static String SET = DAGOperator.SET.getLiteral();
	protected final static String RET = DAGOperator.RET.getLiteral();
	protected final static String CVT = DAGOperator.CVT.getLiteral();
	protected final static String INDIR = DAGOperator.INDIR.getLiteral();

	protected class DagOpSwitch extends DagSwitch<Instruction> {

		@Override
		public Instruction caseDAGOpNode(DAGOpNode n) {

			if (n.getOpcode().equals(SET)) {
				return caseSet(n);
			}

			if (n.getOpcode().equals(CVT)) {
				return caseConvert(n);
			}

			if (n.getOpcode().equals(INDIR)) {
				return caseIndir(n);
			}

			if (n.getOpcode().equals(RET)) {
				return caseReturn(n);
			}
			
//			if(n.getOpcode().equals(DAGOperator.VEC_SET.getLiteral())) {
//				return caseVecSet(n);
//			}
//
//			if(n.getOpcode().equals(DAGOperator.VEC_PACK.getLiteral())) {
//				return caseVecVPack(n);
//			}
//			
//			if(n.getOpcode().equals(DAGOperator.VEC_PERMUTE.getLiteral())) {
//				return caseVecPerm(n);
//			}
//
//			if(n.getOpcode().startsWith(DAGOperator.VEC_EXTRACT.getLiteral())) {
//				return caseVecExtract(n);
//			}
//			
//			if(n.getOpcode().startsWith("VEC_")) { //XXX
//				return caseVecOp(n);
//			}

			return caseDefault(n);
		}
		
		public Instruction caseReturn(DAGOpNode n) {
			RetInstruction ret = InstrsFactory.eINSTANCE.createRetInstruction();
			ret.setType(n.getType());
			EList<DAGOutPort> predecessors = getDataPredecessorsDefinitions(n);
			if (predecessors.size() != 1) {
				throw new Dag2TreeException("Invalid Return node: "
						+ predecessors.size() + " predecessors.");
			}
			Instruction value = getDataDefinition(predecessors.get(0));
			ret.setExpr(value);
			instructions.add(ret);
			// InstructionFactory.ret(value);
			return ret;
		}

		public Instruction caseDefault(DAGOpNode n) {
			// Default Behavior
			if (n.getOpcode().equals("address")) {
				return GecosUserInstructionFactory.address(getDataDefinition(getDataPredecessorsDefinitions(n).get(0)));
			}
			
			GenericInstruction generic = InstrsFactory.eINSTANCE
					.createGenericInstruction();
			generic.setName(OperatorBuilder.INSTANCE.operator(n.getOpcode()));
			generic.setType(n.getType());

			for (DAGOutPort operand : getDataPredecessorsDefinitions(n)) {
				Instruction iop = getDataDefinition(operand);
				generic.addOperand(iop);
			}
			depth++;
			return generic;
		}

		public Instruction caseConvert(DAGOpNode n) {
			EList<DAGOutPort> predecessors = getDataPredecessorsDefinitions(n);
			if (predecessors.size() != 1) {
				throw new Dag2TreeException("Invalid Convert node:"
						+ predecessors.size() + " predecessors.");
			}
			Instruction value = getDataDefinition(predecessors.get(0));
			Instruction cvt = GecosUserInstructionFactory.cast(n.getType(),value);
			return cvt;
		}

		public Instruction caseSet(DAGOpNode n) {
			if(DEBUG) debug("caseSetNode "+n);
			incINDEX();
			EList<DAGOutPort> predecessors = getDataPredecessorsDefinitions(n);
			if (predecessors.size() != 2) {
				throw new Dag2TreeException("Invalid SET node: " + n + " has "
						+ predecessors.size() + " predecessors.");
			}
			Instruction target = getDataDefinition(predecessors.get(0));
			Instruction value = getDataDefinition(predecessors.get(1));
			SetInstruction set = GecosUserInstructionFactory.set(target, value);
			if (!visitedNodes.contains(n)) {
				instructions.add(set);
			}

//			if(DEBUG) debug("Opswitch create SetInstruction: " + set);
			decINDEX();
			if(DEBUG) debug("|->" + set);
			return set;
		}

		public Instruction caseIndir(DAGOpNode n) {
			EList<DAGOutPort> predecessors = getDataPredecessorsDefinitions(n);
			if (predecessors.size() != 1) {
				throw new Dag2TreeException("Invalid Indir node:"
						+ predecessors.size() + " predecessors.");
			}
			Instruction adress = getDataDefinition(predecessors.get(0));
			Type typeNode = n.getType();

			// FIXME this is a hack to circumvent the fact that the DAGOpNode
			// type may
			// not be correctly set
			if (typeNode == null) {
				if (adress.getType() != null
						&& adress.getType() instanceof PtrType) {
					typeNode = ((PtrType) adress.getType()).getBase();
					n.setType(typeNode);
				}
			}
			IndirInstruction indir = GecosUserInstructionFactory.indir(typeNode, adress);
			return indir;
		}

	}

	@Override
	public Instruction caseDAGCallNode(DAGCallNode n) {
		EList<DAGOutPort> predecessors = getDataPredecessorsDefinitions(n);
		if (predecessors.size() == 0) {
			throw new Dag2TreeException("Invalid Call node: no predecessors.");
		}
		Instruction adress = getDataDefinition(predecessors.get(0));
		CallInstruction call = GecosUserInstructionFactory.call(adress);
		call.setType(n.getType());
		for (int i = 1; i < predecessors.size(); i++) {
			call.addArg(getDataDefinition(predecessors.get(i)));
		}
		
		/* add call to instructions only when it does not have any output */
		if(n.getDataSuccessors().isEmpty()) {
			if (!visitedNodes.contains(n))
				instructions.add(call);
		}
		
		return call;
	}

	@Override
	public Instruction caseDAGIntImmNode(DAGIntImmNode object) {
 		IntInstruction instruction = GecosUserInstructionFactory.Int(object.getValue().getValue());
		instruction.setType(object.getType());
		return instruction;
	}

	@Override
	public Instruction caseDAGFloatImmNode(DAGFloatImmNode object) {
		FloatInstruction instruction = GecosUserInstructionFactory.Float(object.getFloatValue());
		instruction.setType(object.getType());
		return instruction;
	}
	
	@Override
	public Instruction caseDAGStringImmNode(DAGStringImmNode object) {
		StringInstruction instruction = GecosUserInstructionFactory.string(object.getStringValue());
		instruction.setType(object.getType());
		return instruction;
	}

	@Override
	public Instruction caseDAGSimpleArrayNode(DAGSimpleArrayNode object) {
		if(DEBUG) debug("caseSANode " + object + " " + object.getSymbol());
		incINDEX();
		EList<DAGOutPort> predecessors = getDataPredecessorsDefinitions(object);//!!! This is assumed to return index Instructions; innermost first !!!
		if (predecessors.size() < 1) {
			throw new Dag2TreeException("Invalid Simple Array Node node:"
					+ predecessors.size() + " predecessors." + object);
		}
		SimpleArrayInstruction sarray = GecosUserInstructionFactory.array(object
				.getSymbol());
		sarray.setType(object.getType());
		
		Collections.reverse(predecessors);
		for (DAGOutPort index : predecessors) {
			sarray.addIndex(getDataDefinition(index));
		}
		
		visitedNodes.add(object);
		
		decINDEX();
		if(DEBUG) debug("|->" + sarray);
		return sarray;
	}
	
	@Override
	public Instruction caseDAGVectorOpNode(DAGVectorOpNode n) {
		if(n.getOpcode().equals(DAGOperator.SET.getLiteral()))
			return caseVectorSet(n);
		
		SimdGenericInstruction simd = GecosUserInstructionFactory.simdOp(OperatorBuilder.INSTANCE.operator(n.getOpcode()), n.getType());
		for (DAGOutPort operand : getDataPredecessorsDefinitions(n)) {
			Instruction iop = getDataDefinition(operand);
			simd.addOperand(iop);
		}
		depth++;
		return cutVector(simd, n);
	}
	
	public Instruction caseVectorSet(DAGVectorOpNode n) {
		if(DEBUG) debug("caseVecSetNode "+n);
		incINDEX();
		EList<DAGOutPort> predecessors = getDataPredecessorsDefinitions(n);
		if (predecessors.size() != 2) {
			throw new Dag2TreeException("Invalid SET node:"
					+ predecessors.size() + " predecessors.");
		}
		Instruction target = getDataDefinition(predecessors.get(0));
		Instruction value = getDataDefinition(predecessors.get(1));
		SimdGenericInstruction set = GecosUserInstructionFactory.simdOp(OperatorBuilder.INSTANCE.operator(n.getOpcode()), n.getType(), target, value);
		if (!visitedNodes.contains(n)) {
			instructions.add(set);
		}
		decINDEX();
		if(DEBUG) debug("|->" + set);
		return set;
	}
	
	@Override
	public Instruction caseDAGVectorShuffleNode(DAGVectorShuffleNode n) {
		SimdShuffleInstruction simd = GecosUserInstructionFactory.simdShuffle(n.getPermutation(), n.getType());
		for (DAGOutPort operand : getDataPredecessorsDefinitions(n)) {
			Instruction iop = getDataDefinition(operand);
			simd.getChildren().add(iop);
		}
		depth++;
		return cutVector(simd, n);
	}
	
	@Override
	public Instruction caseDAGVectorExpandNode(DAGVectorExpandNode n) {
		 SimdExpandInstruction simd = GecosUserInstructionFactory.simdExpand(n.getPermutation(), n.getType());
		for (DAGOutPort operand : getDataPredecessorsDefinitions(n)) {
			Instruction iop = getDataDefinition(operand);
			simd.getChildren().add(iop);
		}
		depth++;
		return cutVector(simd, n);
	}
	
	@Override
	public Instruction caseDAGVectorPackNode(DAGVectorPackNode n) {
		SimdPackInstruction simd = GecosUserInstructionFactory.simdPack(n.getType());
		for (DAGOutPort operand : getDataPredecessorsDefinitions(n)) {
			Instruction iop = getDataDefinition(operand);
			simd.getChildren().add(iop);
		}
		depth++;
		return cutVector(simd, n);
	}
	
	@Override
	public Instruction caseDAGVectorExtractNode(DAGVectorExtractNode n) {
		EList<DAGOutPort> dataPreds = getDataPredecessorsDefinitions(n);
		if(dataPreds.size() != 1)
			throw new RuntimeException("VectorExtractNode should have only one data input!");
		SimdExtractInstruction simd = GecosUserInstructionFactory.simdExtract(
				getDataDefinition(dataPreds.get(0)), 
				GecosUserInstructionFactory.Int(n.getPos()), n.getType());
		depth++;
		return simd;
	}
	
	@Override
	public Instruction caseDAGVectorArrayAccessNode(DAGVectorArrayAccessNode n) {
		if(DEBUG) debug("caseVectorArrayAccessNode "+n);
		incINDEX();
		EList<DAGOutPort> predecessors = getDataPredecessorsDefinitions(n);//!!! This is assumed to return index Instructions; innermost first !!!
		if (predecessors.size() < 1)
			throw new Dag2TreeException("Invalid Vector Array Access node:" + predecessors.size() + " predecessors.");
		
		RangeArrayInstruction rarray = InstrsFactory.eINSTANCE.createRangeArrayInstruction();
		rarray.setDest(GecosUserInstructionFactory.symbref(n.getSymbol()));
		rarray.setType(n.getType());
		Collections.reverse(predecessors);
		for (DAGOutPort index : predecessors)
			rarray.addIndex(getDataDefinition(index));
		
//		SimdInstruction simd = InstrsFactory.eINSTANCE.createSimdInstruction();
//		simd.setName(OperatorBuilder.INSTANCE.operator("VEC_READ"));
//		simd.setType(n.getType());
//		simd.addOperand(sarray);
		//ODOT
		
		//TODO If has WAR dep => cut in order to Satisfy the WAR dependencies
//		for(DAGOutPort out : n.getOutputs()) {
//			if(out.getSinks().isEmpty())
//				continue;
//			if(out.getSinks().get(0) instanceof DAGControlEdge) {
//				DAGControlEdge ce = (DAGControlEdge) out.getSinks().get(0);
//				if(ce.getType() == DependencyType.WAR) {
//					Symbol sym = getNewTempSymbol(n.getParent(), simd.getType(), tmpSymbolPrefix);
//					cut(sym, simd, n);
//					visitedNodes.add(n);
//					return InstructionFactory.symbref(sym);
//				}
//			}
//		}
		visitedNodes.add(n);
		
		decINDEX();
		if(DEBUG) debug("|->" + rarray);
		
		if ((n.getSuccessors().get(0) instanceof DAGVectorOpNode) && ((DAGVectorOpNode)n.getSuccessors().get(0)).getOpcode().equals(DAGOperator.SET.getLiteral())) {
			return rarray;
		}
		return cutVector(rarray, n);
	}

//	private DAGNode getOutput(DAGNode object) {
//		DAGNode node = object;
//
//		for(DAGNode suc : node.getDataSuccessors()) {
//			if(suc instanceof DAGOpNode || (suc instanceof DAGOpNode && ((DAGOpNode)suc).getOpcode().equals(SET)))
//				return suc;
//		}
//		for(DAGNode suc : node.getDataSuccessors())
//			return getOutput(suc);
//		
//		return null;
//	}
	
	@Override
	public Instruction caseDAGFieldInstruction(DAGFieldInstruction object) {
		EList<DAGOutPort> predecessors = getDataPredecessorsDefinitions(object);
		if (predecessors.size() == 0) {
			throw new Dag2TreeException("Invalid Field node: no predecessors.");
		}
		FieldInstruction field = GecosUserInstructionFactory.field(
				getDataDefinition(predecessors.get(0)), object.getField());
		return field;
	}

	@Override
	public Instruction caseDAGOutNode(DAGOutNode object) {
		if(DEBUG) debug("caseOutNode "+object);
		
		EList<DAGOutPort> predecessors = getDataPredecessorsDefinitions(object);
		if (predecessors.size() != 1) {
			throw new Dag2TreeException("Invalid Out node: " + predecessors.size() + " predecessors.");
		}
		
		manageControl(object);
		
		Instruction value = getDataDefinition(predecessors.get(0));
		if (!isNodeVisited(object) && !isSelfAffectation(object, value)) {
			notifySet(predecessors.get(0), object.getSymbol());
			SetInstruction set = GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(object.getSymbol()), value);
			instructions.add(set);
			if(DEBUG) debug("create SetInstuction: " + set);
			return set;
		} else
			return null;
	}

	@Override
	public Instruction caseDAGSymbolNode(DAGSymbolNode object) {
		return GecosUserInstructionFactory.symbref(object.getSymbol());
	}

	/**
	 * Notify that a node has been affected to a symbol
	 * 
	 * @param n
	 * @param sym
	 */
	protected void notifySet(DAGOutPort n, Symbol sym) {
		// symbolsMap.put(n, sym);
		dataDefinitions.put(n, sym);
	}

	/**
	 * Get all data definitions ports of a node.
	 * 
	 * @param n
	 * @return
	 */
	protected EList<DAGOutPort> getDataPredecessorsDefinitions(DAGNode n) {
		EList<DAGOutPort> preds = new BasicEList<DAGOutPort>();
		for (DAGInPort pin : n.getDataInputs()) {
//			if (pin.getSource() != null && (pin.getSource() instanceof DAGDataEdge)) {
				DAGOutPort src = pin.getSource().getSrc();
				preds.add(src);
//			}
		}
		return preds;
	}

	// private EList<DAGNode> getDataPredecessors(DAGNode n) {
	// EList<DAGNode> preds = new BasicEList<DAGNode>();
	// for (DAGInPort pin : n.getInputs()) {
	//
	// if (pin.getSource() != null
	// && (pin.getSource() instanceof DAGDataEdge)) {
	// DAGOutPort src = pin.getSource().getSrc();
	// preds.add(src.getParent());
	// }
	// }
	// return preds;
	// }

	private EList<DAGNode> getDataSuccessors(DAGNode n) {
		EList<DAGNode> succs = new BasicEList<DAGNode>();
		for (DAGOutPort pout : n.getOutputs()) {
			for (DAGEdge out : pout.getSinks()) {
				if (out instanceof DAGDataEdge) {
					succs.add(out.getSink().getParent());
				}
			}
		}
		return succs;
	}


	@Override
	public Instruction caseDAGPatternNode(DAGPatternNode object) {
		if(DEBUG) debug("casePatternNode: " + object);
		ProcedureSymbol patternProcedure = getProcedureSymbol(object);
		CallInstruction callToPattern = GecosUserInstructionFactory
				.call(patternProcedure);
		// Manager parameters of the pattern procedure
		for (DAGInPort input : object.getInputs()) {
			if (input.getSource() != null) {
				Instruction arg = getDataDefinition(input.getSource().getSrc());
				callToPattern.addArg(arg);
			}
		}

		// Create temp outputs for each pattern outputs
		for (DAGOutPort output : object.getOutputs()) {
			DAGOutPort nestedOutput = object.getOutputsMap().get(output);
			Type outputType = nestedOutput.getParent().getType();
			Symbol tmpOutSymbol = createTempSymbol(outputType);
			Instruction arg = GecosUserInstructionFactory.address(GecosUserInstructionFactory
					.symbref(tmpOutSymbol));
			callToPattern.addArg(arg);
			notifySet(output, tmpOutSymbol);
		}
		instructions.add(callToPattern);
		return callToPattern;
	}
	


	private Map<String, ProcedureSymbol> proceduresSymbolsMap = new LinkedHashMap<String, ProcedureSymbol>();

	private ProcedureSymbol getProcedureSymbol(DAGPatternNode object) {
		ProcedureSymbol psym = proceduresSymbolsMap.get(object.getOpcode());
		if (psym == null) {
			Procedure proc = buildPatternProcedure(object);
			psym = proc.getSymbol();
		}
		return psym;
	}

	private static Type findInputDataType(DAGInPort dagInPort) {
		DAGOutPort definition = dagInPort.getSource().getSrc();
		DAGNode sourceNode = definition.getParent();
		if (sourceNode instanceof DAGPatternNode) {
			DAGOutPort nestedSourcePort = ((DAGPatternNode) sourceNode)
					.getOutputsMap().get(definition);
			sourceNode = nestedSourcePort.getParent();
		}

		return sourceNode.getType();

	}

	private Procedure buildPatternProcedure(DAGPatternNode object) {
		// Build symbol of the procedure
		List<ParameterSymbol> parameters = new ArrayList<ParameterSymbol>();

		// Manage parameters of the pattern procedure
		int input = 0;
		for (DAGInPort dagInPort : object.getInputs()) {
			if (dagInPort.getSource() != null
					&& dagInPort.getSource() instanceof DAGDataEdge) {
				Type inputType = findInputDataType(dagInPort);
				ParameterSymbol arg = GecosUserCoreFactory.paramSymbol("in_"
						+ input, inputType);
				parameters.add(arg);
				input++;
			}
		}
		int output = 0;
		for (DAGOutPort dagOutPort : object.getOutputs()) {
			DAGOutPort nestedOutputPort = object.getOutputsMap()
					.get(dagOutPort);
			Type outputType = nestedOutputPort.getParent().getType();
			PtrType outputPtrType = GecosUserTypeFactory.PTR(outputType);
			scope.lookup(outputPtrType);
			ParameterSymbol arg = GecosUserCoreFactory.paramSymbol("out_" + output,
					install(outputPtrType));
			parameters.add(arg);
			output++;
		}

		ProcedureSet ps = (ProcedureSet) scope.getRoot().eContainer();
//		Procedure proc = GecosUserCoreFactory.proc(ps, object.getOpcode(),
//				install(GecosUserTypeFactory.VOID()),
//				GecosUserBlockFactory.CompositeBlock(), parameters);
		ProcedureSymbol psym = GecosUserCoreFactory.procSymbol(object.getOpcode(), install(GecosUserTypeFactory.VOID()), parameters);
		Procedure proc = GecosUserCoreFactory.proc(ps, psym, GecosUserBlockFactory.CompositeBlock());
		
		ps.addProcedure(proc);
		CompositeBlock composite = (CompositeBlock) proc.getBody();
		BasicBlock patternBasicBlock = buildPatternBody(object, proc);
		CompositeBlock bodyContainer = GecosUserBlockFactory.CompositeBlock();
		bodyContainer.addChildren(patternBasicBlock);
		composite.getChildren().add(1, bodyContainer);
		proceduresSymbolsMap.put(object.getOpcode(), proc.getSymbol());
		DAGToTreeConvertion transform = new DAGToTreeConvertion(proc);
		transform.compute();
		new BuildControlFlow(proc).compute();
		return proc;
	}

	private BasicBlock buildPatternBody(DAGPatternNode pattern, Procedure proc) {
		BasicBlock body = GecosUserBlockFactory.BBlock();
		DAGInstruction patternInstruction = (DAGInstruction) pattern
				.getPattern();
		int arg = 0;
		for (DAGInPort input : pattern.getInputs()) {
			DAGInPort nestedInput = pattern.getInputsMap().get(input);
			if (nestedInput != null) {
				// If a port is mapped then it is an input of the pattern
				// procedure
				ParameterSymbol parameterSymbol = proc.listParameters().get(arg);
				DAGSymbolNode symbolNode = GecosUserDagFactory.createDAGSymbolNode(
						patternInstruction, parameterSymbol);
				if (nestedInput.getSource() != null)
					throw new RuntimeException();
				DAGDataEdge edge = GecosUserDagFactory.createDAGDataEdge(
						nestedInput, symbolNode.getOutputs().get(0));
				patternInstruction.getEdges().add(edge);
				arg++;
			}
		}
		for (DAGOutPort output : pattern.getOutputs()) {
			DAGOutPort nestedOutput = pattern.getOutputsMap().get(output);
			if (nestedOutput != null) {
				ParameterSymbol parameterSymbol = proc.listParameters().get(arg);

				// Create nodes to do the affectation
				PtrType outputPtrType = (PtrType) parameterSymbol.getType();
				Type outputType = outputPtrType.getBase();
				DAGOpNode outputSetNode = GecosUserDagFactory.createDAGOPNode(
						patternInstruction, SET, outputType);
				DAGOpNode indirNode = GecosUserDagFactory.createDAGOPNode(
						patternInstruction, INDIR, outputType);
				DAGSymbolNode symbolNode = GecosUserDagFactory.createDAGSymbolNode(
						patternInstruction, parameterSymbol);
				// Connect them
				connect(patternInstruction, indirNode, outputSetNode);
				connect(patternInstruction, symbolNode, indirNode);

				DAGInPort dataDefinitionInput = GecosUserDagFactory
						.createDAGInputPort(outputSetNode);
				DAGDataEdge edge = GecosUserDagFactory.createDAGDataEdge(
						dataDefinitionInput, nestedOutput);
				patternInstruction.getEdges().add(edge);
				arg++;
			}
		}
		body.addInstruction(patternInstruction);

		return body;
	}

	private static void connect(DAGInstruction dag, DAGNode source, DAGNode dest) {
		DAGInPort input = GecosUserDagFactory.createDAGInputPort(dest);
		if (source.getOutputs().size() == 0)
			GecosUserDagFactory.createDAGOutputPort(source);
		DAGDataEdge edge = GecosUserDagFactory.createDAGDataEdge(input, source
				.getOutputs().get(0));
		dag.getEdges().add(edge);
	}

	/**
	 * Get existing type in the scope. Install it if no equivalent type already
	 * exist in the scope.
	 * 
	 * @param type
	 * @return
	 */
	private Type install(Type type) {
		Type lookupType = scope.lookup(type);
		if (lookupType == null) {
			scope.getRoot().getTypes().add(type);
			return type;
		}
		return lookupType;
	}

	/**
	 * @author kmartin
	 * 
	 */
	public static class DagOutTopologicalSorter implements
			Comparator<DAGOutPort> {

		public int compare(DAGOutPort o1, DAGOutPort o2) {
			if (o1.getParent().getPredecessors().contains(o2.getParent())) {
				return 1;
			} else if (o2.getParent().getPredecessors()
					.contains(o1.getParent())) {
				return -1;
			}
			return 0;
		}

	}

}