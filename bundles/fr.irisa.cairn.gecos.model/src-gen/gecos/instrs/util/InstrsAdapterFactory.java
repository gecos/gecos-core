/**
 */
package gecos.instrs.util;

import gecos.annotations.AnnotatedElement;

import gecos.core.GecosNode;
import gecos.core.ISymbolUse;
import gecos.core.ITypedElement;

import gecos.instrs.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see gecos.instrs.InstrsPackage
 * @generated
 */
public class InstrsAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static InstrsPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InstrsAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = InstrsPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InstrsSwitch<Adapter> modelSwitch =
		new InstrsSwitch<Adapter>() {
			@Override
			public Adapter caseInstrsVisitor(InstrsVisitor object) {
				return createInstrsVisitorAdapter();
			}
			@Override
			public Adapter caseInstrsVisitable(InstrsVisitable object) {
				return createInstrsVisitableAdapter();
			}
			@Override
			public Adapter caseInstruction(Instruction object) {
				return createInstructionAdapter();
			}
			@Override
			public Adapter caseDummyInstruction(DummyInstruction object) {
				return createDummyInstructionAdapter();
			}
			@Override
			public Adapter caseLabelInstruction(LabelInstruction object) {
				return createLabelInstructionAdapter();
			}
			@Override
			public Adapter caseSymbolInstruction(SymbolInstruction object) {
				return createSymbolInstructionAdapter();
			}
			@Override
			public Adapter caseNumberedSymbolInstruction(NumberedSymbolInstruction object) {
				return createNumberedSymbolInstructionAdapter();
			}
			@Override
			public Adapter caseSSADefSymbol(SSADefSymbol object) {
				return createSSADefSymbolAdapter();
			}
			@Override
			public Adapter caseSSAUseSymbol(SSAUseSymbol object) {
				return createSSAUseSymbolAdapter();
			}
			@Override
			public Adapter caseSizeofInstruction(SizeofInstruction object) {
				return createSizeofInstructionAdapter();
			}
			@Override
			public Adapter caseSizeofTypeInstruction(SizeofTypeInstruction object) {
				return createSizeofTypeInstructionAdapter();
			}
			@Override
			public Adapter caseSizeofValueInstruction(SizeofValueInstruction object) {
				return createSizeofValueInstructionAdapter();
			}
			@Override
			public Adapter caseConstantInstruction(ConstantInstruction object) {
				return createConstantInstructionAdapter();
			}
			@Override
			public Adapter caseIntInstruction(IntInstruction object) {
				return createIntInstructionAdapter();
			}
			@Override
			public Adapter caseFloatInstruction(FloatInstruction object) {
				return createFloatInstructionAdapter();
			}
			@Override
			public Adapter caseStringInstruction(StringInstruction object) {
				return createStringInstructionAdapter();
			}
			@Override
			public Adapter caseComplexInstruction(ComplexInstruction object) {
				return createComplexInstructionAdapter();
			}
			@Override
			public Adapter caseChildrenListInstruction(ChildrenListInstruction object) {
				return createChildrenListInstructionAdapter();
			}
			@Override
			public Adapter caseArrayValueInstruction(ArrayValueInstruction object) {
				return createArrayValueInstructionAdapter();
			}
			@Override
			public Adapter casePhiInstruction(PhiInstruction object) {
				return createPhiInstructionAdapter();
			}
			@Override
			public Adapter caseComponentInstruction(ComponentInstruction object) {
				return createComponentInstructionAdapter();
			}
			@Override
			public Adapter caseSetInstruction(SetInstruction object) {
				return createSetInstructionAdapter();
			}
			@Override
			public Adapter caseAddressComponent(AddressComponent object) {
				return createAddressComponentAdapter();
			}
			@Override
			public Adapter caseAddressInstruction(AddressInstruction object) {
				return createAddressInstructionAdapter();
			}
			@Override
			public Adapter caseIndirInstruction(IndirInstruction object) {
				return createIndirInstructionAdapter();
			}
			@Override
			public Adapter caseCallInstruction(CallInstruction object) {
				return createCallInstructionAdapter();
			}
			@Override
			public Adapter caseMethodCallInstruction(MethodCallInstruction object) {
				return createMethodCallInstructionAdapter();
			}
			@Override
			public Adapter caseArrayInstruction(ArrayInstruction object) {
				return createArrayInstructionAdapter();
			}
			@Override
			public Adapter caseSimpleArrayInstruction(SimpleArrayInstruction object) {
				return createSimpleArrayInstructionAdapter();
			}
			@Override
			public Adapter caseRangeArrayInstruction(RangeArrayInstruction object) {
				return createRangeArrayInstructionAdapter();
			}
			@Override
			public Adapter caseBranchInstruction(BranchInstruction object) {
				return createBranchInstructionAdapter();
			}
			@Override
			public Adapter caseCondInstruction(CondInstruction object) {
				return createCondInstructionAdapter();
			}
			@Override
			public Adapter caseBreakInstruction(BreakInstruction object) {
				return createBreakInstructionAdapter();
			}
			@Override
			public Adapter caseContinueInstruction(ContinueInstruction object) {
				return createContinueInstructionAdapter();
			}
			@Override
			public Adapter caseGotoInstruction(GotoInstruction object) {
				return createGotoInstructionAdapter();
			}
			@Override
			public Adapter caseExprComponent(ExprComponent object) {
				return createExprComponentAdapter();
			}
			@Override
			public Adapter caseRetInstruction(RetInstruction object) {
				return createRetInstructionAdapter();
			}
			@Override
			public Adapter caseConvertInstruction(ConvertInstruction object) {
				return createConvertInstructionAdapter();
			}
			@Override
			public Adapter caseFieldInstruction(FieldInstruction object) {
				return createFieldInstructionAdapter();
			}
			@Override
			public Adapter caseEnumeratorInstruction(EnumeratorInstruction object) {
				return createEnumeratorInstructionAdapter();
			}
			@Override
			public Adapter caseRangeInstruction(RangeInstruction object) {
				return createRangeInstructionAdapter();
			}
			@Override
			public Adapter caseCaseInstruction(CaseInstruction object) {
				return createCaseInstructionAdapter();
			}
			@Override
			public Adapter caseGenericInstruction(GenericInstruction object) {
				return createGenericInstructionAdapter();
			}
			@Override
			public Adapter caseArithmeticInstruction(ArithmeticInstruction object) {
				return createArithmeticInstructionAdapter();
			}
			@Override
			public Adapter caseComparisonInstruction(ComparisonInstruction object) {
				return createComparisonInstructionAdapter();
			}
			@Override
			public Adapter caseLogicalInstruction(LogicalInstruction object) {
				return createLogicalInstructionAdapter();
			}
			@Override
			public Adapter caseBitwiseInstruction(BitwiseInstruction object) {
				return createBitwiseInstructionAdapter();
			}
			@Override
			public Adapter caseSimdInstruction(SimdInstruction object) {
				return createSimdInstructionAdapter();
			}
			@Override
			public Adapter caseSimdGenericInstruction(SimdGenericInstruction object) {
				return createSimdGenericInstructionAdapter();
			}
			@Override
			public Adapter caseSimdPackInstruction(SimdPackInstruction object) {
				return createSimdPackInstructionAdapter();
			}
			@Override
			public Adapter caseSimdExtractInstruction(SimdExtractInstruction object) {
				return createSimdExtractInstructionAdapter();
			}
			@Override
			public Adapter caseSimdShuffleInstruction(SimdShuffleInstruction object) {
				return createSimdShuffleInstructionAdapter();
			}
			@Override
			public Adapter caseSimdExpandInstruction(SimdExpandInstruction object) {
				return createSimdExpandInstructionAdapter();
			}
			@Override
			public Adapter caseGecosNode(GecosNode object) {
				return createGecosNodeAdapter();
			}
			@Override
			public Adapter caseAnnotatedElement(AnnotatedElement object) {
				return createAnnotatedElementAdapter();
			}
			@Override
			public Adapter caseITypedElement(ITypedElement object) {
				return createITypedElementAdapter();
			}
			@Override
			public Adapter caseISymbolUse(ISymbolUse object) {
				return createISymbolUseAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.InstrsVisitor <em>Visitor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.InstrsVisitor
	 * @generated
	 */
	public Adapter createInstrsVisitorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.InstrsVisitable <em>Visitable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.InstrsVisitable
	 * @generated
	 */
	public Adapter createInstrsVisitableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.Instruction <em>Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.Instruction
	 * @generated
	 */
	public Adapter createInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.DummyInstruction <em>Dummy Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.DummyInstruction
	 * @generated
	 */
	public Adapter createDummyInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.LabelInstruction <em>Label Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.LabelInstruction
	 * @generated
	 */
	public Adapter createLabelInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.SymbolInstruction <em>Symbol Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.SymbolInstruction
	 * @generated
	 */
	public Adapter createSymbolInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.NumberedSymbolInstruction <em>Numbered Symbol Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.NumberedSymbolInstruction
	 * @generated
	 */
	public Adapter createNumberedSymbolInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.SSADefSymbol <em>SSA Def Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.SSADefSymbol
	 * @generated
	 */
	public Adapter createSSADefSymbolAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.SSAUseSymbol <em>SSA Use Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.SSAUseSymbol
	 * @generated
	 */
	public Adapter createSSAUseSymbolAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.SizeofInstruction <em>Sizeof Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.SizeofInstruction
	 * @generated
	 */
	public Adapter createSizeofInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.SizeofTypeInstruction <em>Sizeof Type Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.SizeofTypeInstruction
	 * @generated
	 */
	public Adapter createSizeofTypeInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.SizeofValueInstruction <em>Sizeof Value Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.SizeofValueInstruction
	 * @generated
	 */
	public Adapter createSizeofValueInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.ConstantInstruction <em>Constant Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.ConstantInstruction
	 * @generated
	 */
	public Adapter createConstantInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.IntInstruction <em>Int Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.IntInstruction
	 * @generated
	 */
	public Adapter createIntInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.FloatInstruction <em>Float Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.FloatInstruction
	 * @generated
	 */
	public Adapter createFloatInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.StringInstruction <em>String Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.StringInstruction
	 * @generated
	 */
	public Adapter createStringInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.ComplexInstruction <em>Complex Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.ComplexInstruction
	 * @generated
	 */
	public Adapter createComplexInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.ChildrenListInstruction <em>Children List Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.ChildrenListInstruction
	 * @generated
	 */
	public Adapter createChildrenListInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.ArrayValueInstruction <em>Array Value Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.ArrayValueInstruction
	 * @generated
	 */
	public Adapter createArrayValueInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.PhiInstruction <em>Phi Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.PhiInstruction
	 * @generated
	 */
	public Adapter createPhiInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.ComponentInstruction <em>Component Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.ComponentInstruction
	 * @generated
	 */
	public Adapter createComponentInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.SetInstruction <em>Set Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.SetInstruction
	 * @generated
	 */
	public Adapter createSetInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.AddressComponent <em>Address Component</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.AddressComponent
	 * @generated
	 */
	public Adapter createAddressComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.AddressInstruction <em>Address Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.AddressInstruction
	 * @generated
	 */
	public Adapter createAddressInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.IndirInstruction <em>Indir Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.IndirInstruction
	 * @generated
	 */
	public Adapter createIndirInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.CallInstruction <em>Call Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.CallInstruction
	 * @generated
	 */
	public Adapter createCallInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.MethodCallInstruction <em>Method Call Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.MethodCallInstruction
	 * @generated
	 */
	public Adapter createMethodCallInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.ArrayInstruction <em>Array Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.ArrayInstruction
	 * @generated
	 */
	public Adapter createArrayInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.SimpleArrayInstruction <em>Simple Array Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.SimpleArrayInstruction
	 * @generated
	 */
	public Adapter createSimpleArrayInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.RangeArrayInstruction <em>Range Array Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.RangeArrayInstruction
	 * @generated
	 */
	public Adapter createRangeArrayInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.BranchInstruction <em>Branch Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.BranchInstruction
	 * @generated
	 */
	public Adapter createBranchInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.CondInstruction <em>Cond Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.CondInstruction
	 * @generated
	 */
	public Adapter createCondInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.BreakInstruction <em>Break Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.BreakInstruction
	 * @generated
	 */
	public Adapter createBreakInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.ContinueInstruction <em>Continue Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.ContinueInstruction
	 * @generated
	 */
	public Adapter createContinueInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.GotoInstruction <em>Goto Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.GotoInstruction
	 * @generated
	 */
	public Adapter createGotoInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.ExprComponent <em>Expr Component</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.ExprComponent
	 * @generated
	 */
	public Adapter createExprComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.RetInstruction <em>Ret Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.RetInstruction
	 * @generated
	 */
	public Adapter createRetInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.ConvertInstruction <em>Convert Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.ConvertInstruction
	 * @generated
	 */
	public Adapter createConvertInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.FieldInstruction <em>Field Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.FieldInstruction
	 * @generated
	 */
	public Adapter createFieldInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.EnumeratorInstruction <em>Enumerator Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.EnumeratorInstruction
	 * @generated
	 */
	public Adapter createEnumeratorInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.RangeInstruction <em>Range Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.RangeInstruction
	 * @generated
	 */
	public Adapter createRangeInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.CaseInstruction <em>Case Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.CaseInstruction
	 * @generated
	 */
	public Adapter createCaseInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.GenericInstruction <em>Generic Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.GenericInstruction
	 * @generated
	 */
	public Adapter createGenericInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.ArithmeticInstruction <em>Arithmetic Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.ArithmeticInstruction
	 * @generated
	 */
	public Adapter createArithmeticInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.ComparisonInstruction <em>Comparison Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.ComparisonInstruction
	 * @generated
	 */
	public Adapter createComparisonInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.LogicalInstruction <em>Logical Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.LogicalInstruction
	 * @generated
	 */
	public Adapter createLogicalInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.BitwiseInstruction <em>Bitwise Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.BitwiseInstruction
	 * @generated
	 */
	public Adapter createBitwiseInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.SimdInstruction <em>Simd Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.SimdInstruction
	 * @generated
	 */
	public Adapter createSimdInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.SimdGenericInstruction <em>Simd Generic Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.SimdGenericInstruction
	 * @generated
	 */
	public Adapter createSimdGenericInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.SimdPackInstruction <em>Simd Pack Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.SimdPackInstruction
	 * @generated
	 */
	public Adapter createSimdPackInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.SimdExtractInstruction <em>Simd Extract Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.SimdExtractInstruction
	 * @generated
	 */
	public Adapter createSimdExtractInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.SimdShuffleInstruction <em>Simd Shuffle Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.SimdShuffleInstruction
	 * @generated
	 */
	public Adapter createSimdShuffleInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.SimdExpandInstruction <em>Simd Expand Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.SimdExpandInstruction
	 * @generated
	 */
	public Adapter createSimdExpandInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.core.GecosNode <em>Gecos Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.core.GecosNode
	 * @generated
	 */
	public Adapter createGecosNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.annotations.AnnotatedElement <em>Annotated Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.annotations.AnnotatedElement
	 * @generated
	 */
	public Adapter createAnnotatedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.core.ITypedElement <em>ITyped Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.core.ITypedElement
	 * @generated
	 */
	public Adapter createITypedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.core.ISymbolUse <em>ISymbol Use</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.core.ISymbolUse
	 * @generated
	 */
	public Adapter createISymbolUseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //InstrsAdapterFactory
