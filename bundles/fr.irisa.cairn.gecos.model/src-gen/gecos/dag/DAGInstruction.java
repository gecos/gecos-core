/**
 */
package gecos.dag;

import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.DAGInstruction#getNodes <em>Nodes</em>}</li>
 *   <li>{@link gecos.dag.DAGInstruction#getEdges <em>Edges</em>}</li>
 *   <li>{@link gecos.dag.DAGInstruction#getIdent <em>Ident</em>}</li>
 *   <li>{@link gecos.dag.DAGInstruction#getSourceNodes <em>Source Nodes</em>}</li>
 *   <li>{@link gecos.dag.DAGInstruction#getSinkNodes <em>Sink Nodes</em>}</li>
 * </ul>
 *
 * @see gecos.dag.DagPackage#getDAGInstruction()
 * @model
 * @generated
 */
public interface DAGInstruction extends Instruction, DagVisitable {
	/**
	 * Returns the value of the '<em><b>Nodes</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.dag.DAGNode}.
	 * It is bidirectional and its opposite is '{@link gecos.dag.DAGNode#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nodes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nodes</em>' containment reference list.
	 * @see gecos.dag.DagPackage#getDAGInstruction_Nodes()
	 * @see gecos.dag.DAGNode#getParent
	 * @model opposite="parent" containment="true"
	 * @generated
	 */
	EList<DAGNode> getNodes();

	/**
	 * Returns the value of the '<em><b>Edges</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.dag.DAGEdge}.
	 * It is bidirectional and its opposite is '{@link gecos.dag.DAGEdge#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edges</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edges</em>' containment reference list.
	 * @see gecos.dag.DagPackage#getDAGInstruction_Edges()
	 * @see gecos.dag.DAGEdge#getParent
	 * @model opposite="parent" containment="true"
	 * @generated
	 */
	EList<DAGEdge> getEdges();

	/**
	 * Returns the value of the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ident</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ident</em>' attribute.
	 * @see #setIdent(int)
	 * @see gecos.dag.DagPackage#getDAGInstruction_Ident()
	 * @model unique="false" dataType="gecos.dag.int"
	 * @generated
	 */
	int getIdent();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGInstruction#getIdent <em>Ident</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ident</em>' attribute.
	 * @see #getIdent()
	 * @generated
	 */
	void setIdent(int value);

	/**
	 * Returns the value of the '<em><b>Source Nodes</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.dag.DAGNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Nodes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Nodes</em>' containment reference list.
	 * @see gecos.dag.DagPackage#getDAGInstruction_SourceNodes()
	 * @model containment="true" derived="true"
	 * @generated
	 */
	EList<DAGNode> getSourceNodes();

	/**
	 * Returns the value of the '<em><b>Sink Nodes</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.dag.DAGNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sink Nodes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sink Nodes</em>' containment reference list.
	 * @see gecos.dag.DagPackage#getDAGInstruction_SinkNodes()
	 * @model containment="true" derived="true"
	 * @generated
	 */
	EList<DAGNode> getSinkNodes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGInstruction(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.dag.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _ident = this.getIdent();\n&lt;%java.lang.String%&gt; _plus = (\"DAG_\" + &lt;%java.lang.Integer%&gt;.valueOf(_ident));\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGNode%&gt;&gt; _nodes = this.getNodes();\nreturn (_plus + _nodes);'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Shortcut to {@link #connectNodes(srcNode, dstNode, -1)}
	 * <!-- end-model-doc -->
	 * @model srcNodeUnique="false" dstNodeUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.connectNodes(srcNode, dstNode, (-1));'"
	 * @generated
	 */
	void connectNodes(DAGNode srcNode, DAGNode dstNode);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Connect 'srcNode' to 'dstNode'.
	 * <li> If 'srcNode' already has {@link DAGOutPort} then the first one is used,
	 * otherwise a newly created one is used.
	 * <li> A newly created {@link DAGInPort} is used for 'dstNode'. if 'dstInportPosition'
	 * is valid then this inport is moved to the position specified by 'dstInportPosition',
	 * otherwise it is added at the end of 'dstNode''s inputs list.
	 * <!-- end-model-doc -->
	 * @model srcNodeUnique="false" dstNodeUnique="false" dstInportPositionDataType="gecos.dag.int" dstInportPositionUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.dag.DAGOutPort%&gt; _xifexpression = null;\nif (((srcNode.getOutputs().size() &gt; 0) &amp;&amp; (srcNode.getOutputs().get(0) != null)))\n{\n\t_xifexpression = srcNode.getOutputs().get(0);\n}\nelse\n{\n\t_xifexpression = &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserDagFactory%&gt;.createDAGOutputPort(srcNode);\n}\nfinal &lt;%gecos.dag.DAGOutPort%&gt; srcPort = _xifexpression;\nfinal &lt;%gecos.dag.DAGInPort%&gt; sinkPort = &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserDagFactory%&gt;.createDAGInputPort(dstNode);\nif (((dstInportPosition &gt;= 0) &amp;&amp; (dstInportPosition &lt; dstNode.getInputs().size())))\n{\n\tdstNode.getInputs().move(dstInportPosition, sinkPort);\n}\nthis.getEdges().add(&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserDagFactory%&gt;.createDAGDataEdge(sinkPort, srcPort));'"
	 * @generated
	 */
	void connectNodes(DAGNode srcNode, DAGNode dstNode, int dstInportPosition);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Connect 'srcPort' to a newly created {@link DAGInPort} for 'dstNode'
	 * (which added at the end of 'dstNode''s inputs list) via {@link DAGDataEdge}.
	 * <!-- end-model-doc -->
	 * @model srcPortUnique="false" dstNodeUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.dag.DAGInPort%&gt; sinkPort = &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserDagFactory%&gt;.createDAGInputPort(dstNode);\nthis.getEdges().add(&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserDagFactory%&gt;.createDAGDataEdge(sinkPort, srcPort));'"
	 * @generated
	 */
	void connect(DAGOutPort srcPort, DAGNode dstNode);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model newNodeUnique="false" oldNodeUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='newNode.getInputs().addAll(oldNode.getInputs());\nnewNode.getOutputs().addAll(oldNode.getOutputs());\nfinal &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGNode%&gt;&gt; nodes = this.getNodes();\nnodes.add(newNode);\nnodes.remove(oldNode);'"
	 * @generated
	 */
	void replaceNode(DAGNode newNode, DAGNode oldNode);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model newNodeUnique="false" predNodeUnique="false" succNodeUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%gecos.dag.DAGEdge%&gt; it)\n\t{\n\t\t&lt;%gecos.dag.DAGNode%&gt; _sinkNode = it.getSinkNode();\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf((_sinkNode == succNode));\n\t}\n};\nfinal &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt; edgesBetween = &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;asEList(((&lt;%gecos.dag.DAGEdge%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;filter(predNode.getOutputEdges(), _function), &lt;%gecos.dag.DAGEdge%&gt;.class)));\nint edgeBetweenFirstPosition = (-1);\nboolean _isEmpty = edgesBetween.isEmpty();\nboolean _not = (!_isEmpty);\nif (_not)\n{\n\tedgeBetweenFirstPosition = succNode.getInputEdges().indexOf(edgesBetween.get(0));\n\tfinal &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt; _function_1 = new &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;()\n\t{\n\t\tpublic void accept(final &lt;%gecos.dag.DAGEdge%&gt; it)\n\t\t{\n\t\t\t&lt;%this%&gt;.removeEdge(it);\n\t\t}\n\t};\n\tedgesBetween.forEach(_function_1);\n}\nthis.connectNodes(predNode, newNode);\nthis.connectNodes(newNode, succNode, edgeBetweenFirstPosition);'"
	 * @generated
	 */
	void insertNodeBetween(DAGNode newNode, DAGNode predNode, DAGNode succNode);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nodeUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((node == null))\n{\n\treturn;\n}\nfinal &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt; _function = new &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;()\n{\n\tpublic void accept(final &lt;%gecos.dag.DAGEdge%&gt; it)\n\t{\n\t\t&lt;%this%&gt;.removeEdge(it);\n\t}\n};\nnode.getInputEdges().forEach(_function);\nfinal &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt; _function_1 = new &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.dag.DAGEdge%&gt;&gt;()\n{\n\tpublic void accept(final &lt;%gecos.dag.DAGEdge%&gt; it)\n\t{\n\t\t&lt;%this%&gt;.removeEdge(it);\n\t}\n};\nnode.getOutputEdges().forEach(_function_1);\nthis.getNodes().remove(node);'"
	 * @generated
	 */
	void removeNode(DAGNode node);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nodeUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.dag.DAGNode%&gt;&gt; dataPreds = node.getDataPredecessors();\nint _size = dataPreds.size();\nboolean _notEquals = (_size != 1);\nif (_notEquals)\n{\n\tthrow new &lt;%java.lang.RuntimeException%&gt;(\"This operation is only possible for nodes with ONLY 1 data predecessor!\");\n}\nfinal &lt;%gecos.dag.DAGNode%&gt; predNode = dataPreds.get(0);\npredNode.getOutputs().addAll(node.getOutputs());\nthis.removeNode(node);'"
	 * @generated
	 */
	void removeNodeAndReconnect(DAGNode node);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model predUnique="false" succUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.dag.DAGDataEdge%&gt;&gt; _function = new &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.dag.DAGDataEdge%&gt;&gt;()\n{\n\tpublic void accept(final &lt;%gecos.dag.DAGDataEdge%&gt; it)\n\t{\n\t\t&lt;%this%&gt;.removeEdge(it);\n\t}\n};\nsucc.getDataInputEdges().forEach(_function);'"
	 * @generated
	 */
	void disconnectNodes(DAGNode pred, DAGNode succ);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model eUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((e == null))\n{\n\treturn;\n}\n&lt;%gecos.dag.DAGOutPort%&gt; srcPort = e.getSrc();\nif (((srcPort != null) &amp;&amp; (srcPort.getSinks().size() == 1)))\n{\n\t&lt;%gecos.dag.DAGNode%&gt; _parent = srcPort.getParent();\n\tboolean _tripleNotEquals = (_parent != null);\n\tif (_tripleNotEquals)\n\t{\n\t\tsrcPort.getParent().getOutputs().remove(srcPort);\n\t}\n}\n&lt;%gecos.dag.DAGNode%&gt; sinkNode = e.getSinkNode();\nif ((sinkNode != null))\n{\n\tsinkNode.getInputs().remove(e.getSink());\n}\nthis.getEdges().remove(e);'"
	 * @generated
	 */
	void removeEdge(DAGEdge e);

} // DAGInstruction
