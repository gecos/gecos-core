package fr.irisa.cairn.gecos.model.analysis.profiling;

import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.BBlock;
import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.CompositeBlock;
import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.For;
import static fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory.procSymbol;
import static fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory.symbol;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.Int;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.array;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.lt;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.set;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.string;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.sum;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.symbref;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.INT;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.javatuples.Pair;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksDefaultVisitor;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Symbol;
import gecos.instrs.CallInstruction;
import gecos.instrs.Instruction;
import gecos.types.ArrayType;
import gecos.types.BaseType;
import gecos.types.IntegerType;
import gecos.types.Type;  

public class VariablePrinter extends GecosBlocksDefaultVisitor {

	private Procedure proc;
	Map<Symbol,Pair<Symbol, Symbol>> arrays;
	List<Symbol> scalars;
	
	public VariablePrinter(Procedure proc, List<Symbol> scalars, Map<Symbol,Pair<Symbol, Symbol>> arrays) {
		this.proc = proc;
		this.scalars = scalars;
		this.arrays = arrays;
	}
	
	public VariablePrinter(Procedure proc) {
		this.proc = proc;
	}

	public VariablePrinter() {
	}
	
	public void compute(){
		if (proc != null && scalars != null && arrays != null)
			proc.getBody().accept(this);
	}
	
	@Override
	public void visitBasicBlock(BasicBlock b) {
		
		CompositeBlock prBody = getBody(proc);
		List<Block> children = prBody.getChildren();
		Block last = children.get(children.size()-1);
		if (b.getInstructionCount()!=0 && ( b.getLastInstruction().isRet() || (proc.getSymbolName().equals("main") && last == b))){
			
			
			Block bFather = b.getParent();
			BasicBlock bbis = b.copy();
			CompositeBlock newBlock = CompositeBlock(bbis);
			bFather.replace(b, newBlock);
			bFather.removeBlock(b);
			
			
			ProcedureSymbol printfSym = procSymbol("printf", INT(), null);
			ProcedureSet ps = proc.getContainingProcedureSet();
			ps.getScope().getSymbols().add(printfSym);
			GecosUserAnnotationFactory.pragma(printfSym, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
			BasicBlock newEndBlock = null;
			if(!scalars.isEmpty()){
				
				String scalarFormats = "";
				List<Instruction> scalarInstrList = new ArrayList<Instruction>();
				
				for (Symbol symb : scalars){
					scalarInstrList.add(symbref(symb));
					
					Type t = symb.getType();
					if(t.asBase() == null)
						continue;
					
					scalarFormats += symb.getName() + " = " + getPrintModifier(t.asBase()) + " ;\\n ";
				}
	
				Instruction argOne = string(scalarFormats);
				scalarInstrList.add(0, argOne);
				
				
				CallInstruction printfScalar = GecosUserInstructionFactory.call(printfSym, scalarInstrList);
				
				
				
				if(bbis.getLastInstruction().isRet())
					newEndBlock = BBlock(printfScalar,bbis.getLastInstruction());
						
				else
					 newEndBlock= BBlock(printfScalar);
				newBlock.addBlockAfter(newEndBlock, bbis);
				
			}
			/************************* min/max display generation for arrays *************************/
			
			for (Symbol arraySymb : arrays.keySet()){
				
				ArrayType currentArrayType = (ArrayType) arraySymb.getType();
				
				List<Instruction> arraySizes = currentArrayType.getSizes(); // (1,2,3) for array A[1][2][3]
				List<Instruction> arrayIterators = new ArrayList<Instruction>(); //container of the iterators of the generated for-loops
				int dimNumber = arraySizes.size();
				Symbol iterSymbol;
			
				for (int i = 0; i<dimNumber; i++){ //actual creation of the iterators
					String iterName = "ar_it_"+(i);
					iterSymbol = prBody.getScope().lookup(iterName);
					if (iterSymbol == null){
						iterSymbol = symbol(iterName, INT());
						prBody.getScope().addSymbol(iterSymbol);
					}
					arrayIterators.add(symbref(iterSymbol));
				}
								
				Symbol minSymb = arrays.get(arraySymb).getValue0();
				Symbol maxSymb = arrays.get(arraySymb).getValue1();
				
				List<Instruction> arrayIteratorsTmp = new ArrayList<Instruction>();
				for (Instruction iters : arrayIterators){
					arrayIteratorsTmp.add(iters.copy());
				}
				ArrayType arrayTy = (ArrayType) arraySymb.getType();
				
				Instruction maxArray = array(symbref(maxSymb), arrayIterators);
				Instruction minArray = array(symbref(minSymb), arrayIteratorsTmp);
				String arrayFormat = "";
				
				String tmp = "";
				for(int i = 0; i<dimNumber; i++){
					tmp+="[%d]";
				}
				
				Type arrayBase = arrayTy.getInnermostBase();
				if(arrayBase.asBase() != null) {
					String printMod = getPrintModifier(arrayBase.asBase());
					arrayFormat += minSymb.getName()+tmp+" = "+printMod+" --- "+maxSymb.getName()+tmp+" = "+printMod+"\\n ";
				}
				
				List<Instruction> arrayInstrList = new ArrayList<Instruction>();
				arrayInstrList.add(string(arrayFormat));
				for(int i = 0; i<dimNumber; i++){
					arrayInstrList.add(arrayIterators.get(i).copy());
				}
				arrayInstrList.add(minArray);
				for(int i = 0; i<dimNumber; i++){
					arrayInstrList.add(arrayIterators.get(i).copy());
				}
				arrayInstrList.add(maxArray);
				
				CallInstruction printfArray = GecosUserInstructionFactory.call(printfSym, arrayInstrList);
				Block forBlock = BBlock(printfArray);// inner "for" body
				
				for (int i = dimNumber-1; i>=0; i--){ //generation of nested initialization for-loops
					Instruction initIter = set(arrayIterators.get(i).copy(), Int(0));
					Instruction testIter = lt(arrayIterators.get(i).copy(), arraySizes.get(dimNumber-i-1));
					Instruction IncrIter = sum(arrayIterators.get(i).copy(), Int(1));
					Instruction stepIter = set(arrayIterators.get(i).copy(),IncrIter);
					forBlock = For(BBlock(initIter), BBlock(testIter), BBlock(stepIter), forBlock.copy());//inner "for"(s) is(are) the body of outer "for"(s) 						
				}
				
				if (newEndBlock==null){
					
					newBlock.addBlockAfter(forBlock, bbis);
					if(bbis.getLastInstruction().isRet())
						 newBlock.addBlockAfter(BBlock(bbis.getLastInstruction()), forBlock);
				}	
				else{
					newBlock.addBlockAfter(forBlock, newEndBlock);
					if (newEndBlock.getLastInstruction().isRet()){
						newEndBlock = BBlock(newEndBlock.getLastInstruction());
						newBlock.addBlockAfter(newEndBlock, forBlock);
					}
				}
			}
			//bFather.removeBlock(b);
		}
	}
	
	private String getPrintModifier(BaseType t) {
		String printModifier = null;
		if(t.asBase().asInt() != null) {
			IntegerType it = t.asBase().asInt();
			printModifier = it.isLong() ? "%ld" : it.isShort() ? "%hd" : "%d";
		}
		else if(t.asBase().asFloat() != null)
			printModifier = t.asBase().asFloat().isSingle() ? "%f" : "%lf";
		else
			throw new RuntimeException("Not yet supported for: " + t);
		
		return printModifier;
	}

	private CompositeBlock getBody(Procedure pr) {
		Block prBody = pr.getBody().getChildren().get(1);
		if (!(prBody instanceof CompositeBlock))
			throw new RuntimeException();
		
		return (CompositeBlock) prBody;
	}
}