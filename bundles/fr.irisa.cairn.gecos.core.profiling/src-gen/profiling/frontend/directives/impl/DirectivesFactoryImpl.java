/**
 */
package profiling.frontend.directives.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import profiling.frontend.directives.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DirectivesFactoryImpl extends EFactoryImpl implements DirectivesFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DirectivesFactory init() {
		try {
			DirectivesFactory theDirectivesFactory = (DirectivesFactory)EPackage.Registry.INSTANCE.getEFactory(DirectivesPackage.eNS_URI);
			if (theDirectivesFactory != null) {
				return theDirectivesFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DirectivesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DirectivesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case DirectivesPackage.SIZE_DIRECTIVE: return createSizeDirective();
			case DirectivesPackage.SAVE_DIRECTIVE: return createSaveDirective();
			case DirectivesPackage.INJECT_DIRECTIVE: return createInjectDirective();
			case DirectivesPackage.FROM_FILE_DIRECTIVE: return createFromFileDirective();
			case DirectivesPackage.FROM_VARIABLE_DIRECTIVE: return createFromVariableDirective();
			case DirectivesPackage.RANDOM_UNIFORM_DIRECTIVE: return createRandomUniformDirective();
			case DirectivesPackage.RANDOM_NORMAL_DIRECTIVE: return createRandomNormalDirective();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case DirectivesPackage.DIRECTIVE_NAME:
				return createDirectiveNameFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case DirectivesPackage.DIRECTIVE_NAME:
				return convertDirectiveNameToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SizeDirective createSizeDirective() {
		SizeDirectiveImpl sizeDirective = new SizeDirectiveImpl();
		return sizeDirective;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SaveDirective createSaveDirective() {
		SaveDirectiveImpl saveDirective = new SaveDirectiveImpl();
		return saveDirective;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InjectDirective createInjectDirective() {
		InjectDirectiveImpl injectDirective = new InjectDirectiveImpl();
		return injectDirective;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FromFileDirective createFromFileDirective() {
		FromFileDirectiveImpl fromFileDirective = new FromFileDirectiveImpl();
		return fromFileDirective;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FromVariableDirective createFromVariableDirective() {
		FromVariableDirectiveImpl fromVariableDirective = new FromVariableDirectiveImpl();
		return fromVariableDirective;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RandomUniformDirective createRandomUniformDirective() {
		RandomUniformDirectiveImpl randomUniformDirective = new RandomUniformDirectiveImpl();
		return randomUniformDirective;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RandomNormalDirective createRandomNormalDirective() {
		RandomNormalDirectiveImpl randomNormalDirective = new RandomNormalDirectiveImpl();
		return randomNormalDirective;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DirectiveName createDirectiveNameFromString(EDataType eDataType, String initialValue) {
		DirectiveName result = DirectiveName.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDirectiveNameToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DirectivesPackage getDirectivesPackage() {
		return (DirectivesPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DirectivesPackage getPackage() {
		return DirectivesPackage.eINSTANCE;
	}

} //DirectivesFactoryImpl
