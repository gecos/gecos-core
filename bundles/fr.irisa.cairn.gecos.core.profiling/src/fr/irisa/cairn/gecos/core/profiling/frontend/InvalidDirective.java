package fr.irisa.cairn.gecos.core.profiling.frontend;

public class InvalidDirective extends RuntimeException {

	private static final long serialVersionUID = 5328011809053325093L;

	public InvalidDirective(String msg) {
		super(msg);
	}
	
}
