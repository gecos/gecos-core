/**
 */
package gecos.dag.impl;

import gecos.core.Symbol;

import gecos.dag.DAGNumberedOutNode;
import gecos.dag.DAGNumberedSymbolNode;
import gecos.dag.DagPackage;
import gecos.dag.DagVisitor;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DAG Numbered Symbol Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.impl.DAGNumberedSymbolNodeImpl#getNumber <em>Number</em>}</li>
 *   <li>{@link gecos.dag.impl.DAGNumberedSymbolNodeImpl#getSymbolDefinition <em>Symbol Definition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DAGNumberedSymbolNodeImpl extends DAGSymbolNodeImpl implements DAGNumberedSymbolNode {
	/**
	 * The default value of the '{@link #getNumber() <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumber() <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber()
	 * @generated
	 * @ordered
	 */
	protected int number = NUMBER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSymbolDefinition() <em>Symbol Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSymbolDefinition()
	 * @generated
	 * @ordered
	 */
	protected DAGNumberedOutNode symbolDefinition;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DAGNumberedSymbolNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DagPackage.Literals.DAG_NUMBERED_SYMBOL_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumber(int newNumber) {
		int oldNumber = number;
		number = newNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DagPackage.DAG_NUMBERED_SYMBOL_NODE__NUMBER, oldNumber, number));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGNumberedOutNode getSymbolDefinition() {
		if (symbolDefinition != null && symbolDefinition.eIsProxy()) {
			InternalEObject oldSymbolDefinition = (InternalEObject)symbolDefinition;
			symbolDefinition = (DAGNumberedOutNode)eResolveProxy(oldSymbolDefinition);
			if (symbolDefinition != oldSymbolDefinition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DagPackage.DAG_NUMBERED_SYMBOL_NODE__SYMBOL_DEFINITION, oldSymbolDefinition, symbolDefinition));
			}
		}
		return symbolDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGNumberedOutNode basicGetSymbolDefinition() {
		return symbolDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSymbolDefinition(DAGNumberedOutNode newSymbolDefinition) {
		DAGNumberedOutNode oldSymbolDefinition = symbolDefinition;
		symbolDefinition = newSymbolDefinition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DagPackage.DAG_NUMBERED_SYMBOL_NODE__SYMBOL_DEFINITION, oldSymbolDefinition, symbolDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final DagVisitor visitor) {
		visitor.visitDAGNumberedSymbolNode(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _elvis = null;
		Symbol _symbol = this.getSymbol();
		String _name = null;
		if (_symbol!=null) {
			_name=_symbol.getName();
		}
		if (_name != null) {
			_elvis = _name;
		} else {
			_elvis = "?";
		}
		String _plus = (_elvis + "_");
		int _number = this.getNumber();
		return (_plus + Integer.valueOf(_number));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DagPackage.DAG_NUMBERED_SYMBOL_NODE__NUMBER:
				return getNumber();
			case DagPackage.DAG_NUMBERED_SYMBOL_NODE__SYMBOL_DEFINITION:
				if (resolve) return getSymbolDefinition();
				return basicGetSymbolDefinition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DagPackage.DAG_NUMBERED_SYMBOL_NODE__NUMBER:
				setNumber((Integer)newValue);
				return;
			case DagPackage.DAG_NUMBERED_SYMBOL_NODE__SYMBOL_DEFINITION:
				setSymbolDefinition((DAGNumberedOutNode)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DagPackage.DAG_NUMBERED_SYMBOL_NODE__NUMBER:
				setNumber(NUMBER_EDEFAULT);
				return;
			case DagPackage.DAG_NUMBERED_SYMBOL_NODE__SYMBOL_DEFINITION:
				setSymbolDefinition((DAGNumberedOutNode)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DagPackage.DAG_NUMBERED_SYMBOL_NODE__NUMBER:
				return number != NUMBER_EDEFAULT;
			case DagPackage.DAG_NUMBERED_SYMBOL_NODE__SYMBOL_DEFINITION:
				return symbolDefinition != null;
		}
		return super.eIsSet(featureID);
	}

} //DAGNumberedSymbolNodeImpl
