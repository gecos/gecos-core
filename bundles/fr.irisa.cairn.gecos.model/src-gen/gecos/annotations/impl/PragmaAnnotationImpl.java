/**
 */
package gecos.annotations.impl;

import fr.irisa.cairn.gecos.model.tools.utils.GecosCopier;

import gecos.annotations.AnnotationsPackage;
import gecos.annotations.PragmaAnnotation;

import gecos.core.Symbol;

import java.lang.CharSequence;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Pragma Annotation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.annotations.impl.PragmaAnnotationImpl#getContent <em>Content</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PragmaAnnotationImpl extends IAnnotationImpl implements PragmaAnnotation {
	/**
	 * The cached value of the '{@link #getContent() <em>Content</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected EList<String> content;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PragmaAnnotationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnnotationsPackage.Literals.PRAGMA_ANNOTATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getContent() {
		if (content == null) {
			content = new EDataTypeUniqueEList<String>(String.class, this, AnnotationsPackage.PRAGMA_ANNOTATION__CONTENT);
		}
		return content;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		final Function1<String, CharSequence> _function = new Function1<String, CharSequence>() {
			public CharSequence apply(final String it) {
				return (("\"" + it) + "\"");
			}
		};
		return IterableExtensions.<String>join(this.getContent(), "#pragma {", ",", "}", _function);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public <T extends Symbol> T copy() {
		final GecosCopier gecosCopier = new GecosCopier();
		EObject _copy = gecosCopier.copy(this);
		final T symbol = ((T) _copy);
		gecosCopier.copyReferences();
		return symbol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AnnotationsPackage.PRAGMA_ANNOTATION__CONTENT:
				return getContent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AnnotationsPackage.PRAGMA_ANNOTATION__CONTENT:
				getContent().clear();
				getContent().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AnnotationsPackage.PRAGMA_ANNOTATION__CONTENT:
				getContent().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AnnotationsPackage.PRAGMA_ANNOTATION__CONTENT:
				return content != null && !content.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PragmaAnnotationImpl
