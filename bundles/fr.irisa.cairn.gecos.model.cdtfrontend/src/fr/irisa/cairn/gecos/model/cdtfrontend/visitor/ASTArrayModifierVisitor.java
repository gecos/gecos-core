/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.cdtfrontend.visitor;

import gecos.instrs.Instruction;

import org.eclipse.cdt.core.dom.ast.IASTArrayModifier;

/**
 * @author mnaullet
 */
public class ASTArrayModifierVisitor {

	private Instruction instruction;

	public ASTArrayModifierVisitor() {
	}
	
	public void visit(CDTToGecosModel gecosModel, IASTArrayModifier arrayModifier) {
		if (arrayModifier.getConstantExpression() != null) {
			arrayModifier.getConstantExpression().accept(gecosModel);
			instruction = ((ASTExpressionVisitor)gecosModel.getObject()).getInstruction();
			if (!gecosModel.visitingInclude())
				gecosModel.addIASTAnnotation(instruction, arrayModifier);
			
			gecosModel.addFileLocationAnnotation(instruction, arrayModifier.getFileLocation());
		}
	}
	
	public Instruction getInstruction() {
		return instruction;
	}
}