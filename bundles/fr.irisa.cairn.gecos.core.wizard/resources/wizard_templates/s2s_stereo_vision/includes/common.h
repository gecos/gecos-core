#ifndef COMMON_H
#define COMMON_H

#define ITER 5       // number of BP iterations at each scale
#define LEVELS 5     // number of scales
#define DISC_K 1.7F         // truncation of discontinuity cost
#define DATA_K 15.0F        // truncation of data cost
#define LAMBDA 0.07F        // weighting of data cost
#define INF 1E20     // large cost
#define VALUES 16    // was 16 number of possible disparities
#define SCALE 16     // scaling from disparity to graylevel in output
#define SIGMA 0.7    // amount to smooth the input images
//#define MAX_WIDTH 384
//#define MAX_HEIGHT 288
#define MAX_WIDTH 400
#define MAX_HEIGHT 300

//#include <ac_fixed.h>

float min(float a, float b);

void merged_bp_cb(float*** u, float ***d, float ***l, float ***r, float ***data, int width, int height);


int load_pgm(char* name, int* width, int* height, unsigned char** img);
int save_pgm(char* name, int width, int height, unsigned char **img);

void comp_data(float ***data,
		unsigned char **img1, unsigned char **img2,
		unsigned char **out1, unsigned char **out2,
		int width,int height);


float**** hypercube_malloc(int level, int width, int height, int value);

float*** cube_malloc(int width, int height, int value);

unsigned char** mat_malloc(int width, int height);

void smooth (unsigned char **src,unsigned char **dst,int width,int height);

void output(unsigned char ** out, float*** u,float*** d,float*** l, float*** r, float*** data, int width, int height);


#endif
