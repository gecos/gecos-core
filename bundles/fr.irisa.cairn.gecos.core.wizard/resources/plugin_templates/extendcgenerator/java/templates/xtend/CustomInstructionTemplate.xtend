package $packageName$.templates.xtend

import custom.instruction.FooInstruction
import fr.irisa.cairn.gecos.model.c.generator.ExtendableInstructionCGenerator
import org.eclipse.emf.ecore.EObject
import templates.xtend.GecosTemplate

/**
 * Extension implementation of the GeCoS C generator
 */
class CustomInstructionTemplate extends GecosTemplate {
	
	def dispatch generate(EObject object){
		null
	}
	
	/**
	 * Implement how is pretty print the FooInstruction object
	 */
	def dispatch generate(FooInstruction foo){
			val buffer = new StringBuffer()
			if(foo.firstOperand != null){
				// Call the extendable instruction generator to pretty print the first operand instruction
				buffer.append(ExtendableInstructionCGenerator::eInstance.generate(foo.firstOperand));
			}
			else{
				buffer.append('''?''')
			}
			buffer.append(''' foo ''')
			
			if(foo.secondOperand != null){
				// Call the extendable instruction generator to pretty print the second operand instruction
				buffer.append(ExtendableInstructionCGenerator::eInstance.generate(foo.secondOperand));
			}
			else{
				buffer.append('''?''')
			}
	}
}