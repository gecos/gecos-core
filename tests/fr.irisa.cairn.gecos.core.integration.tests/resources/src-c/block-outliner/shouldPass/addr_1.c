int main() {
	int A[3] = {1,2,3};
	int (*p)[];

	#pragma outline
	{
		int *B = A;
		p = &A;
		**p = 4;
		B[1] = 5;
		A[2] = A[1];
	}

	//-- test --
	if(*p != A)
		abort();
	if(A[0] != 4 || A[1] != 5 || A[2] != A[1])
		abort();

	return 0;
}
