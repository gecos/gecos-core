package fr.irisa.cairn.gecos.model.transforms.others;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDefaultVisitor;
import gecos.blocks.Block;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;
import gecos.instrs.ConvertInstruction;
import gecos.types.Type;

public class RemoveUnnecessaryCasts extends GecosBlocksInstructionsDefaultVisitor {

	List<Block> blocks;
	
	public RemoveUnnecessaryCasts(GecosProject proj) {
		EList<Procedure> procs = proj.listProcedures();
		blocks = new ArrayList<>(procs.size());
		for (Procedure p : procs)
			blocks.add(p.getBody());
	}
	
	public RemoveUnnecessaryCasts(GecosSourceFile src) {
		this(src.getModel());
	}

	public RemoveUnnecessaryCasts(ProcedureSet ps) {
		EList<Procedure> procs = ps.listProcedures();
		blocks = new ArrayList<>(procs.size());
		for (Procedure p : procs)
			blocks.add(p.getBody());
	}

	public RemoveUnnecessaryCasts(Procedure proc) {
		blocks = new ArrayList<>(1);
		blocks.add(proc.getBody());
	}
	
	public RemoveUnnecessaryCasts(Block b) {
		blocks = new ArrayList<>(1);
		blocks.add(b);
	}
	
	public void compute() {
		for (Block b : blocks)
			b.accept(this);
	}
	
	@Override
	public void visitConvertInstruction(ConvertInstruction c) {
		super.visitConvertInstruction(c);
		if (isSame(c.getType(), c.getExpr().getType()))
			c.getParent().replaceChild(c, c.getExpr());
	}

	private boolean isSame(Type type, Type type2) {
		try {
			TypeAnalyzer ta1 = new TypeAnalyzer(type);
			TypeAnalyzer ta2 = new TypeAnalyzer(type2);
			
			return ta1.isSimilar(ta2); //ta1.equals(ta2)
//			if(ta1.getBase().isSame(ta2.getBase()))
//				return true;
			
		} catch (Exception e) {
			return false;
		}
	}
	
}
