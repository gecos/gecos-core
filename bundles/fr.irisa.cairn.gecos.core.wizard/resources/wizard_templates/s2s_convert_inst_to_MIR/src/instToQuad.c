#include <stdio.h>
#define M	128
#define N	128

void testInstToQuad(int in[N][M], int out[N][M]) {
  int i, j, a;
  a = M + N * 2 ;
  for (i = 0; i < N; i++)
    if (M > 100)
      for (j = 0; j < 100; j++)
        out[i][j] = (in[i][j] + i + j) * 2;
    else
      for (j = 0; j < M; j++)
        out[i][j] = in[i][j] + a ;
}
