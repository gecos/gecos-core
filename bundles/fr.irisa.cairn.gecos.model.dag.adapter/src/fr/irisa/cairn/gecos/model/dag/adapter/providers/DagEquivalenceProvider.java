package fr.irisa.cairn.gecos.model.dag.adapter.providers;

import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGEdgeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGNodeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGPortAdapter;
import fr.irisa.cairn.graph.providers.IEquivalenceProvider;
import gecos.dag.DAGNode;
import gecos.dag.DAGOpNode;
import gecos.dag.DAGPort;
import gecos.dag.util.DagSwitch;
import gecos.types.Type;

/**
 * Default DAG {@link IEquivalenceProvider}. Two {@link DAGOpNode}s are
 * equivalents if they have the same opcode and works and have the same
 * {@link Type}. Other nodes are equivalents if they are instances of the same
 * class. Two {@link DAGPort} are equivalents if they have the same id.
 * 
 * @author Antoine Floc'h - Initial contribution and API
 * 
 */
public class DagEquivalenceProvider implements
		IEquivalenceProvider<IDAGEdgeAdapter, IDAGNodeAdapter, IDAGPortAdapter> {

	public boolean edgesEquivalence(IDAGEdgeAdapter e1, IDAGEdgeAdapter e2) {
		return e1.getClass() == e2.getClass();
	}

	public boolean nodesEquivalence(IDAGNodeAdapter n1, IDAGNodeAdapter n2) {
		NodeComparator comparator = new NodeComparator(n1.getAdaptedNode());
		return comparator.doSwitch(n2.getAdaptedNode());
	}

	public boolean portsEquivalence(IDAGPortAdapter p1, IDAGPortAdapter p2) {
		return true;
	}

	protected static class NodeComparator extends DagSwitch<Boolean> {
		protected DAGNode reference;

		public NodeComparator(DAGNode reference) {
			this.reference = reference;
		}

		@Override
		public Boolean caseDAGNode(DAGNode object) {
			return reference.getClass() == object.getClass();
		}

		@Override
		public Boolean caseDAGOpNode(DAGOpNode object) {
			if (reference instanceof DAGOpNode) {
				DAGOpNode refop = (DAGOpNode) reference;
				if (!refop.getOpcode().equals(object.getOpcode()))
					return false;
				if (refop.getType() != null && object.getType() != null) {
					if (!refop.getType().isEqual(object.getType()))
						return false;
				}
				return true;
			}
			return false;
		}
	}

}
