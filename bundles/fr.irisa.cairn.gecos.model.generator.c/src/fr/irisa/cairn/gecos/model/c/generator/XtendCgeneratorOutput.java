/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.c.generator;

import fr.irisa.cairn.gecos.model.extensions.outputs.GecosOutput;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;

public class XtendCgeneratorOutput extends GecosOutput{

	public void print(GecosProject project){
		XtendCGenerator generator = new XtendCGenerator(project, file);
		generator.compute();
	}
	
	public void print(ProcedureSet ps){
		XtendCGenerator generator = new XtendCGenerator(ps, file);
		generator.compute();
	}
}
