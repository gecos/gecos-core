import csv
import sys
import string

infile = sys.argv[1]
outfile = sys.argv[2]

with open(infile) as f:
    reader = csv.reader(f)
    rows = []
    for row in reader:
        rows.append(row)

with open(outfile, 'wb') as f:
    writer = csv.writer(f)
    for o in range(len(rows)):
		row = rows[o]
		if (o > 0):
			baseline = float(row[1])
			for i in range(len(row)):
				if (i > 1):
					current = float(row[i])
					val = current/baseline
					row[i] = val
			row[1] = 1
		writer.writerow(row)
