/**
 */
package profiling.frontend.directives.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import profiling.frontend.directives.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see profiling.frontend.directives.DirectivesPackage
 * @generated
 */
public class DirectivesSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DirectivesPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DirectivesSwitch() {
		if (modelPackage == null) {
			modelPackage = DirectivesPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case DirectivesPackage.DIRECTIVE: {
				Directive directive = (Directive)theEObject;
				T result = caseDirective(directive);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DirectivesPackage.SIZE_DIRECTIVE: {
				SizeDirective sizeDirective = (SizeDirective)theEObject;
				T result = caseSizeDirective(sizeDirective);
				if (result == null) result = caseDirective(sizeDirective);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DirectivesPackage.SAVE_DIRECTIVE: {
				SaveDirective saveDirective = (SaveDirective)theEObject;
				T result = caseSaveDirective(saveDirective);
				if (result == null) result = caseDirective(saveDirective);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DirectivesPackage.INJECT_DIRECTIVE: {
				InjectDirective injectDirective = (InjectDirective)theEObject;
				T result = caseInjectDirective(injectDirective);
				if (result == null) result = caseDirective(injectDirective);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DirectivesPackage.INJECTOR_SOURCE_DIRECTIVE: {
				InjectorSourceDirective injectorSourceDirective = (InjectorSourceDirective)theEObject;
				T result = caseInjectorSourceDirective(injectorSourceDirective);
				if (result == null) result = caseDirective(injectorSourceDirective);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DirectivesPackage.FROM_FILE_DIRECTIVE: {
				FromFileDirective fromFileDirective = (FromFileDirective)theEObject;
				T result = caseFromFileDirective(fromFileDirective);
				if (result == null) result = caseInjectorSourceDirective(fromFileDirective);
				if (result == null) result = caseDirective(fromFileDirective);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DirectivesPackage.FROM_VARIABLE_DIRECTIVE: {
				FromVariableDirective fromVariableDirective = (FromVariableDirective)theEObject;
				T result = caseFromVariableDirective(fromVariableDirective);
				if (result == null) result = caseInjectorSourceDirective(fromVariableDirective);
				if (result == null) result = caseDirective(fromVariableDirective);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DirectivesPackage.RANDOM_UNIFORM_DIRECTIVE: {
				RandomUniformDirective randomUniformDirective = (RandomUniformDirective)theEObject;
				T result = caseRandomUniformDirective(randomUniformDirective);
				if (result == null) result = caseInjectorSourceDirective(randomUniformDirective);
				if (result == null) result = caseDirective(randomUniformDirective);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DirectivesPackage.RANDOM_NORMAL_DIRECTIVE: {
				RandomNormalDirective randomNormalDirective = (RandomNormalDirective)theEObject;
				T result = caseRandomNormalDirective(randomNormalDirective);
				if (result == null) result = caseInjectorSourceDirective(randomNormalDirective);
				if (result == null) result = caseDirective(randomNormalDirective);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Directive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Directive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDirective(Directive object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Size Directive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Size Directive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSizeDirective(SizeDirective object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Save Directive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Save Directive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSaveDirective(SaveDirective object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Inject Directive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Inject Directive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInjectDirective(InjectDirective object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Injector Source Directive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Injector Source Directive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInjectorSourceDirective(InjectorSourceDirective object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>From File Directive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>From File Directive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFromFileDirective(FromFileDirective object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>From Variable Directive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>From Variable Directive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFromVariableDirective(FromVariableDirective object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Random Uniform Directive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Random Uniform Directive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRandomUniformDirective(RandomUniformDirective object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Random Normal Directive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Random Normal Directive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRandomNormalDirective(RandomNormalDirective object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //DirectivesSwitch
