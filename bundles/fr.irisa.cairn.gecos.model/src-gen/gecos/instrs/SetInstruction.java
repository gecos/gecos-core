/**
 */
package gecos.instrs;

import gecos.types.Type;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Set Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.SetInstruction#getDest <em>Dest</em>}</li>
 *   <li>{@link gecos.instrs.SetInstruction#getSource <em>Source</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getSetInstruction()
 * @model
 * @generated
 */
public interface SetInstruction extends ComponentInstruction {
	/**
	 * Returns the value of the '<em><b>Dest</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dest</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dest</em>' containment reference.
	 * @see #setDest(Instruction)
	 * @see gecos.instrs.InstrsPackage#getSetInstruction_Dest()
	 * @model containment="true"
	 * @generated
	 */
	Instruction getDest();

	/**
	 * Sets the value of the '{@link gecos.instrs.SetInstruction#getDest <em>Dest</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dest</em>' containment reference.
	 * @see #getDest()
	 * @generated
	 */
	void setDest(Instruction value);

	/**
	 * Returns the value of the '<em><b>Source</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' containment reference.
	 * @see #setSource(Instruction)
	 * @see gecos.instrs.InstrsPackage#getSetInstruction_Source()
	 * @model containment="true"
	 * @generated
	 */
	Instruction getSource();

	/**
	 * Sets the value of the '{@link gecos.instrs.SetInstruction#getSource <em>Source</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' containment reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(Instruction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;asEList(this.getDest(), this.getSource()));'"
	 * @generated
	 */
	EList<Instruction> listChildren();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitSetInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _xblockexpression = null;\n{\n\t&lt;%java.lang.String%&gt; _elvis = null;\n\t&lt;%gecos.instrs.Instruction%&gt; _dest = this.getDest();\n\t&lt;%java.lang.String%&gt; _string = null;\n\tif (_dest!=null)\n\t{\n\t\t_string=_dest.toString();\n\t}\n\tif (_string != null)\n\t{\n\t\t_elvis = _string;\n\t} else\n\t{\n\t\t_elvis = \"?\";\n\t}\n\tfinal &lt;%java.lang.String%&gt; dst = _elvis;\n\t&lt;%java.lang.String%&gt; _elvis_1 = null;\n\t&lt;%gecos.instrs.Instruction%&gt; _source = this.getSource();\n\t&lt;%java.lang.String%&gt; _string_1 = null;\n\tif (_source!=null)\n\t{\n\t\t_string_1=_source.toString();\n\t}\n\tif (_string_1 != null)\n\t{\n\t\t_elvis_1 = _string_1;\n\t} else\n\t{\n\t\t_elvis_1 = \"?\";\n\t}\n\tfinal &lt;%java.lang.String%&gt; src = _elvis_1;\n\t_xblockexpression = ((dst + \"=\") + src);\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return true;'"
	 * @generated
	 */
	boolean isSet();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.instrs.Instruction%&gt; _dest = this.getDest();\n&lt;%gecos.types.Type%&gt; _type = null;\nif (_dest!=null)\n{\n\t_type=_dest.getType();\n}\nreturn _type;'"
	 * @generated
	 */
	Type computeType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((inst instanceof &lt;%gecos.instrs.SetInstruction%&gt;))\n{\n\tfinal boolean isSameDest = (((this.getDest() == null) &amp;&amp; (((&lt;%gecos.instrs.SetInstruction%&gt;)inst).getDest() == null)) || this.getDest().isSame(((&lt;%gecos.instrs.SetInstruction%&gt;)inst).getDest()));\n\tfinal boolean isSameSource = (((this.getSource() == null) &amp;&amp; (((&lt;%gecos.instrs.SetInstruction%&gt;)inst).getSource() == null)) || this.getSource().isSame(((&lt;%gecos.instrs.SetInstruction%&gt;)inst).getSource()));\n\treturn ((isSameDest &amp;&amp; isSameSource) &amp;&amp; super.isSame(inst));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction inst);

} // SetInstruction
