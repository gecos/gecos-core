/**
 */
package gecos.annotations;

import gecos.core.GecosNode;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IAnnotation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.annotations.AnnotationsPackage#getIAnnotation()
 * @model abstract="true"
 * @generated
 */
public interface IAnnotation extends GecosNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.ecore.EObject%&gt; _eContainer = this.eContainer();\nboolean _tripleNotEquals = (_eContainer != null);\nif (_tripleNotEquals)\n{\n\tfinal &lt;%org.eclipse.emf.ecore.EObject%&gt; containe = this.eContainer().eContainer();\n\tif ((containe instanceof &lt;%gecos.annotations.AnnotatedElement%&gt;))\n\t{\n\t\treturn ((&lt;%gecos.annotations.AnnotatedElement%&gt;)containe);\n\t}\n}\nreturn null;'"
	 * @generated
	 */
	AnnotatedElement getAnnotatedElement();

} // IAnnotation
