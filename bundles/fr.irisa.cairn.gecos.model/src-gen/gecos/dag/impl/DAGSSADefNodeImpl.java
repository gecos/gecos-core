/**
 */
package gecos.dag.impl;

import gecos.dag.DAGSSADefNode;
import gecos.dag.DAGSSAUseNode;
import gecos.dag.DagPackage;
import gecos.dag.DagVisitor;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DAGSSA Def Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.impl.DAGSSADefNodeImpl#getSSAUses <em>SSA Uses</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DAGSSADefNodeImpl extends DAGOutNodeImpl implements DAGSSADefNode {
	/**
	 * The cached value of the '{@link #getSSAUses() <em>SSA Uses</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSSAUses()
	 * @generated
	 * @ordered
	 */
	protected EList<DAGSSAUseNode> ssaUses;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DAGSSADefNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DagPackage.Literals.DAGSSA_DEF_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGSSAUseNode> getSSAUses() {
		if (ssaUses == null) {
			ssaUses = new EObjectResolvingEList<DAGSSAUseNode>(DAGSSAUseNode.class, this, DagPackage.DAGSSA_DEF_NODE__SSA_USES);
		}
		return ssaUses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final DagVisitor visitor) {
		visitor.visitDAGSSADefNode(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DagPackage.DAGSSA_DEF_NODE__SSA_USES:
				return getSSAUses();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DagPackage.DAGSSA_DEF_NODE__SSA_USES:
				getSSAUses().clear();
				getSSAUses().addAll((Collection<? extends DAGSSAUseNode>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DagPackage.DAGSSA_DEF_NODE__SSA_USES:
				getSSAUses().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DagPackage.DAGSSA_DEF_NODE__SSA_USES:
				return ssaUses != null && !ssaUses.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DAGSSADefNodeImpl
