/**
 */
package gecos.annotations.impl;

import gecos.annotations.AnnotationsPackage;
import gecos.annotations.SymbolDefinitionAnnotation;

import gecos.instrs.Instruction;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Symbol Definition Annotation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.annotations.impl.SymbolDefinitionAnnotationImpl#getSymbolDefinition <em>Symbol Definition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SymbolDefinitionAnnotationImpl extends IAnnotationImpl implements SymbolDefinitionAnnotation {
	/**
	 * The cached value of the '{@link #getSymbolDefinition() <em>Symbol Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSymbolDefinition()
	 * @generated
	 * @ordered
	 */
	protected Instruction symbolDefinition;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SymbolDefinitionAnnotationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnnotationsPackage.Literals.SYMBOL_DEFINITION_ANNOTATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getSymbolDefinition() {
		if (symbolDefinition != null && symbolDefinition.eIsProxy()) {
			InternalEObject oldSymbolDefinition = (InternalEObject)symbolDefinition;
			symbolDefinition = (Instruction)eResolveProxy(oldSymbolDefinition);
			if (symbolDefinition != oldSymbolDefinition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AnnotationsPackage.SYMBOL_DEFINITION_ANNOTATION__SYMBOL_DEFINITION, oldSymbolDefinition, symbolDefinition));
			}
		}
		return symbolDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction basicGetSymbolDefinition() {
		return symbolDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSymbolDefinition(Instruction newSymbolDefinition) {
		Instruction oldSymbolDefinition = symbolDefinition;
		symbolDefinition = newSymbolDefinition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AnnotationsPackage.SYMBOL_DEFINITION_ANNOTATION__SYMBOL_DEFINITION, oldSymbolDefinition, symbolDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AnnotationsPackage.SYMBOL_DEFINITION_ANNOTATION__SYMBOL_DEFINITION:
				if (resolve) return getSymbolDefinition();
				return basicGetSymbolDefinition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AnnotationsPackage.SYMBOL_DEFINITION_ANNOTATION__SYMBOL_DEFINITION:
				setSymbolDefinition((Instruction)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AnnotationsPackage.SYMBOL_DEFINITION_ANNOTATION__SYMBOL_DEFINITION:
				setSymbolDefinition((Instruction)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AnnotationsPackage.SYMBOL_DEFINITION_ANNOTATION__SYMBOL_DEFINITION:
				return symbolDefinition != null;
		}
		return super.eIsSet(featureID);
	}

} //SymbolDefinitionAnnotationImpl
