package fr.irisa.cairn.gecos.model.transforms.tools;

import org.eclipse.emf.ecore.EObject;

import tom.library.sl.VisitFailure;
import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import gecos.blocks.Block;
import gecos.core.Procedure;
import gecos.instrs.Instruction;

/**
 * Abstract class for a CDFG analysis. Visit all blocks and instructions from a
 * root.
 * 
 * @author antoine
 * 
 */
public abstract class AbstractTomAnalyzer {

	private BlockInstructionSwitch<Object> traversal;


	public AbstractTomAnalyzer() {
		traversal = new GecosCdfgTraversal();
	}

	public void analyze(Block block) {
		traversal.doSwitch(block);
	}

	public void analyze(Instruction i) {
		traversal.doSwitch(i);
	}

	public void analyze(Procedure proc) {
		traversal.doSwitch(proc);
	}

	protected void analyzeBlock(Block b) throws VisitFailure {

	}

	protected void analyzeInstruction(Instruction i) throws VisitFailure {

	}

	protected class GecosCdfgTraversal extends BlockInstructionSwitch<Object> {
		@Override
		public Object doSwitch(Block block) {
			try {
				analyzeBlock(block);
			} catch (VisitFailure e) {
				throw new AnalyzeFailure(e);
			}
			return super.doSwitch(block);
		}

		@Override
		public Object doSwitch(EObject instruction) {
			try {
				analyzeInstruction((Instruction) instruction);
			} catch (VisitFailure e) {
				throw new AnalyzeFailure(e);
			}
			return new Object();
		}
	}

	@SuppressWarnings("serial")
	public static class AnalyzeFailure extends RuntimeException {

		public AnalyzeFailure(String message) {
			super(message);
		}

		public AnalyzeFailure(Throwable cause) {
			super(cause);
		}

	}
}
