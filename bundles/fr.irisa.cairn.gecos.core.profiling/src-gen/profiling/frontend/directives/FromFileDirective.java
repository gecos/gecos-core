/**
 */
package profiling.frontend.directives;

import gecos.blocks.Block;

import gecos.instrs.Instruction;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>From File Directive</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * $from_file(filePath)
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link profiling.frontend.directives.FromFileDirective#getNAME <em>NAME</em>}</li>
 *   <li>{@link profiling.frontend.directives.FromFileDirective#getFilePath <em>File Path</em>}</li>
 * </ul>
 *
 * @see profiling.frontend.directives.DirectivesPackage#getFromFileDirective()
 * @model
 * @generated
 */
public interface FromFileDirective extends InjectorSourceDirective {
	/**
	 * Returns the value of the '<em><b>NAME</b></em>' attribute.
	 * The default value is <code>"$from_file"</code>.
	 * The literals are from the enumeration {@link profiling.frontend.directives.DirectiveName}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>NAME</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>NAME</em>' attribute.
	 * @see profiling.frontend.directives.DirectiveName
	 * @see #setNAME(DirectiveName)
	 * @see profiling.frontend.directives.DirectivesPackage#getFromFileDirective_NAME()
	 * @model default="$from_file" unique="false"
	 * @generated
	 */
	DirectiveName getNAME();

	/**
	 * Sets the value of the '{@link profiling.frontend.directives.FromFileDirective#getNAME <em>NAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>NAME</em>' attribute.
	 * @see profiling.frontend.directives.DirectiveName
	 * @see #getNAME()
	 * @generated
	 */
	void setNAME(DirectiveName value);

	/**
	 * Returns the value of the '<em><b>File Path</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>File Path</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>File Path</em>' reference.
	 * @see #setFilePath(Instruction)
	 * @see profiling.frontend.directives.DirectivesPackage#getFromFileDirective_FilePath()
	 * @model
	 * @generated
	 */
	Instruction getFilePath();

	/**
	 * Sets the value of the '{@link profiling.frontend.directives.FromFileDirective#getFilePath <em>File Path</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>File Path</em>' reference.
	 * @see #getFilePath()
	 * @generated
	 */
	void setFilePath(Instruction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; args = this.getCall().getArgs();\nint _size = args.size();\nboolean _lessThan = (_size &lt; 1);\nif (_lessThan)\n{\n\t&lt;%java.lang.String%&gt; _printLocation = this.printLocation();\n\t&lt;%java.lang.String%&gt; _plus = (_printLocation + \"\\nMust have at least 1 arguments (filePath)!\");\n\tthrow new &lt;%fr.irisa.cairn.gecos.core.profiling.frontend.InvalidDirective%&gt;(_plus);\n}\nthis.setFilePath(&lt;%java.util.Objects%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;requireNonNull(args.get(0)));'"
	 * @generated
	 */
	void parse();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.Block%&gt; _xblockexpression = null;\n{\n\tfinal &lt;%gecos.core.ProcedureSet%&gt; ps = this.getCall().getContainingProcedureSet();\n\t&lt;%gecos.blocks.Block%&gt; _xifexpression = null;\n\tint _length = ((&lt;%java.lang.Object%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(this.getCall().getArgs(), &lt;%java.lang.Object%&gt;.class)).length;\n\tboolean _greaterThan = (_length &gt; 1);\n\tif (_greaterThan)\n\t{\n\t\t&lt;%gecos.blocks.SwitchBlock%&gt; _xblockexpression_1 = null;\n\t\t{\n\t\t\tint _length_1 = ((&lt;%java.lang.Object%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(this.getCall().getArgs(), &lt;%java.lang.Object%&gt;.class)).length;\n\t\t\tfinal &lt;%java.util.ArrayList%&gt;&lt;&lt;%gecos.blocks.Block%&gt;&gt; blocks = new &lt;%java.util.ArrayList%&gt;&lt;&lt;%gecos.blocks.Block%&gt;&gt;(_length_1);\n\t\t\tint _length_2 = ((&lt;%java.lang.Object%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(this.getCall().getArgs(), &lt;%java.lang.Object%&gt;.class)).length;\n\t\t\t&lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt; _doubleDotLessThan = new &lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt;(0, _length_2, true);\n\t\t\tfor (final &lt;%java.lang.Integer%&gt; i : _doubleDotLessThan)\n\t\t\t{\n\t\t\t\tblocks.add(\n\t\t\t\t\t&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory%&gt;.BBlock(\n\t\t\t\t\t\t&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory%&gt;.call(\n\t\t\t\t\t\t\t&lt;%fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport%&gt;.getNativeProc(ps, &lt;%fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport.NativeFunction%&gt;.INJECTOR_FROM_FILE), &lt;%java.util.Objects%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;requireNonNull(this.getCall().getArgs().get((i).intValue()).copy())), \n\t\t\t\t\t\t&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory%&gt;.breakInst()));\n\t\t\t}\n\t\t\t_xblockexpression_1 = &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory%&gt;.Switch(ps.getScope().lookup(&lt;%fr.irisa.cairn.gecos.core.profiling.backend.MainProcedureCreator%&gt;.SIM_COUNT_INDEX_NAME), ((&lt;%gecos.blocks.Block%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(blocks, &lt;%gecos.blocks.Block%&gt;.class)));\n\t\t}\n\t\t_xifexpression = _xblockexpression_1;\n\t}\n\telse\n\t{\n\t\t_xifexpression = &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory%&gt;.BBlock(\n\t\t\t&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory%&gt;.call(\n\t\t\t\t&lt;%fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport%&gt;.getNativeProc(ps, &lt;%fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport.NativeFunction%&gt;.INJECTOR_FROM_FILE), \n\t\t\t\tthis.getFilePath()));\n\t}\n\t_xblockexpression = _xifexpression;\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	Block createCodeBlock();

} // FromFileDirective
