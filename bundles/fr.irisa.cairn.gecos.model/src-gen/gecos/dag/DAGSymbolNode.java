/**
 */
package gecos.dag;

import gecos.core.Symbol;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Symbol Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.DAGSymbolNode#getName <em>Name</em>}</li>
 *   <li>{@link gecos.dag.DAGSymbolNode#getSymbol <em>Symbol</em>}</li>
 * </ul>
 *
 * @see gecos.dag.DagPackage#getDAGSymbolNode()
 * @model
 * @generated
 */
public interface DAGSymbolNode extends DAGNode, DagVisitable {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see gecos.dag.DagPackage#getDAGSymbolNode_Name()
	 * @model unique="false" dataType="gecos.dag.String" derived="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGSymbolNode#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Symbol</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbol</em>' reference.
	 * @see #setSymbol(Symbol)
	 * @see gecos.dag.DagPackage#getDAGSymbolNode_Symbol()
	 * @model
	 * @generated
	 */
	Symbol getSymbol();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGSymbolNode#getSymbol <em>Symbol</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Symbol</em>' reference.
	 * @see #getSymbol()
	 * @generated
	 */
	void setSymbol(Symbol value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGSymbolNode(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.dag.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getSymbol().toString();'"
	 * @generated
	 */
	String toString();

} // DAGSymbolNode
