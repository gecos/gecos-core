/**
 */
package gecos.annotations;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>File Location Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.annotations.FileLocationAnnotation#getFilename <em>Filename</em>}</li>
 *   <li>{@link gecos.annotations.FileLocationAnnotation#getStartingLine <em>Starting Line</em>}</li>
 *   <li>{@link gecos.annotations.FileLocationAnnotation#getEndingLine <em>Ending Line</em>}</li>
 *   <li>{@link gecos.annotations.FileLocationAnnotation#getNodeOffset <em>Node Offset</em>}</li>
 *   <li>{@link gecos.annotations.FileLocationAnnotation#getNodeLength <em>Node Length</em>}</li>
 * </ul>
 *
 * @see gecos.annotations.AnnotationsPackage#getFileLocationAnnotation()
 * @model
 * @generated
 */
public interface FileLocationAnnotation extends IAnnotation {
	/**
	 * Returns the value of the '<em><b>Filename</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Filename</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filename</em>' attribute.
	 * @see #setFilename(String)
	 * @see gecos.annotations.AnnotationsPackage#getFileLocationAnnotation_Filename()
	 * @model unique="false" dataType="gecos.annotations.String"
	 * @generated
	 */
	String getFilename();

	/**
	 * Sets the value of the '{@link gecos.annotations.FileLocationAnnotation#getFilename <em>Filename</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Filename</em>' attribute.
	 * @see #getFilename()
	 * @generated
	 */
	void setFilename(String value);

	/**
	 * Returns the value of the '<em><b>Starting Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Starting Line</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Starting Line</em>' attribute.
	 * @see #setStartingLine(int)
	 * @see gecos.annotations.AnnotationsPackage#getFileLocationAnnotation_StartingLine()
	 * @model unique="false"
	 * @generated
	 */
	int getStartingLine();

	/**
	 * Sets the value of the '{@link gecos.annotations.FileLocationAnnotation#getStartingLine <em>Starting Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Starting Line</em>' attribute.
	 * @see #getStartingLine()
	 * @generated
	 */
	void setStartingLine(int value);

	/**
	 * Returns the value of the '<em><b>Ending Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ending Line</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ending Line</em>' attribute.
	 * @see #setEndingLine(int)
	 * @see gecos.annotations.AnnotationsPackage#getFileLocationAnnotation_EndingLine()
	 * @model unique="false"
	 * @generated
	 */
	int getEndingLine();

	/**
	 * Sets the value of the '{@link gecos.annotations.FileLocationAnnotation#getEndingLine <em>Ending Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ending Line</em>' attribute.
	 * @see #getEndingLine()
	 * @generated
	 */
	void setEndingLine(int value);

	/**
	 * Returns the value of the '<em><b>Node Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node Offset</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node Offset</em>' attribute.
	 * @see #setNodeOffset(int)
	 * @see gecos.annotations.AnnotationsPackage#getFileLocationAnnotation_NodeOffset()
	 * @model unique="false"
	 * @generated
	 */
	int getNodeOffset();

	/**
	 * Sets the value of the '{@link gecos.annotations.FileLocationAnnotation#getNodeOffset <em>Node Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Node Offset</em>' attribute.
	 * @see #getNodeOffset()
	 * @generated
	 */
	void setNodeOffset(int value);

	/**
	 * Returns the value of the '<em><b>Node Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node Length</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node Length</em>' attribute.
	 * @see #setNodeLength(int)
	 * @see gecos.annotations.AnnotationsPackage#getFileLocationAnnotation_NodeLength()
	 * @model unique="false"
	 * @generated
	 */
	int getNodeLength();

	/**
	 * Sets the value of the '{@link gecos.annotations.FileLocationAnnotation#getNodeLength <em>Node Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Node Length</em>' attribute.
	 * @see #getNodeLength()
	 * @generated
	 */
	void setNodeLength(int value);

} // FileLocationAnnotation
