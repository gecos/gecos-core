package fr.irisa.cairn.gecos.model.transforms.initializationexposition;

import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.BBlock;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.set;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.symbref;

import java.util.Iterator;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.tools.controlflow.BuildControlFlow;
import fr.irisa.cairn.gecos.model.tools.controlflow.ClearControlFlow;
import fr.irisa.cairn.gecos.model.tools.controlflow.ClearControlFlow.MODE;
import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.annotations.AnnotationKeys;
import gecos.annotations.AnnotationsFactory;
import gecos.annotations.StringAnnotation;
import gecos.annotations.SymbolDefinitionAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ForC99Block;
import gecos.blocks.SimpleForBlock;
import gecos.core.Procedure;
import gecos.core.Scope;
import gecos.core.ScopeContainer;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.instrs.ArrayValueInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.SetInstruction;
import gecos.types.ArrayType;

/** AssignInitialValue is used to make explicit assignment
 * between a symbol and its initializer.
 * 
 * @author llhours
 */
public class ExposeVariableInitialization extends BlocksDefaultSwitch<EObject> {

	
	private Procedure proc;
	private GecosProject proj;
	
	public ExposeVariableInitialization(Procedure proc) {
		this.proc = proc;
	}
	
	public ExposeVariableInitialization(GecosProject proj) {
		this.proj = proj;
	}

	public void compute() {
		if(proj!=null) {
			for(Procedure proc : proj.listProcedures())
				compute(proc);
			}
		else
			compute(proc);
	}
	
	public void compute(Procedure proc) {
		ClearControlFlow.clearCF(proc,MODE.CFG);
		doSwitch(proc.getBody());
		BuildControlFlow.buildCF(proc);

	} 
	
	
	
	@Override
	public EObject caseCompositeBlock(CompositeBlock b) {
		Scope scope = b.getScope();
		BasicBlock newBlock = BBlock();
		for (Symbol sym : scope.getSymbols()) {
			// fix initialization of scalars with brackets as
			// double n = {0}; 
			// by removing the brackets.
			while (	sym.getValue() != null && 
					!(sym.getType() instanceof ArrayType) && 
					(sym.getValue() instanceof ArrayValueInstruction)) {
				ArrayValueInstruction v = (ArrayValueInstruction) sym.getValue();
				sym.setValue(v.getChild(0));
			}
			if (sym.getValue() != null && !(sym.getType() instanceof ArrayType)) {
				SymbolDefinitionAnnotation annot = AnnotationsFactory.eINSTANCE.createSymbolDefinitionAnnotation();
				annot.setSymbolDefinition(sym.getValue());
				sym.getAnnotations().put(AnnotationKeys.SYMBOL_DEF_ANNOTATION_KEY.getLiteral(),annot);
				sym.getValue().getAnnotations().put(AnnotationKeys.SYMBOL_DEF_ANNOTATION_KEY.getLiteral(), GecosUserAnnotationFactory.STRING(sym.getName()));
				Instruction setInstruction = set(symbref(sym), sym.getValue());
				newBlock.addInstruction(setInstruction);
			}
		}
		if (newBlock.getInstructionCount() > 0)
			b.getChildren().add(0,newBlock);
		
		return super.caseCompositeBlock(b);
	}

	@Override
	public EObject caseForC99Block(ForC99Block forC99Block) {
		Scope scope = forC99Block.getScope();
		Iterator<Symbol> i = scope.getSymbols().iterator();
		if (scope.getSymbols().size() > 0) {
			while (i.hasNext()) {
				Symbol sym = (Symbol) i.next();
				if (sym.getValue() != null && !(sym.getType() instanceof ArrayType)) {
					SymbolDefinitionAnnotation annot = AnnotationsFactory.eINSTANCE.createSymbolDefinitionAnnotation();
					annot.setSymbolDefinition(sym.getValue());
					sym.getAnnotations().put(AnnotationKeys.SYMBOL_DEF_ANNOTATION_KEY.getLiteral(),annot);
					//note : creating a new set instructions moves the symbol value
					Instruction setInstruction = set(symbref(sym), sym.getValue());
					//therefore the block init becomes empty 
					((BasicBlock)forC99Block.getInitBlock()).getInstructions().add(setInstruction);
				}
			}
		}
		return super.caseForC99Block(forC99Block);
	}
	
	@Override
	public EObject caseSimpleForBlock(SimpleForBlock simpleForBlock) {
		Scope scope = simpleForBlock.getScope();
		Iterator<Symbol> i = scope.getSymbols().iterator();
		if (scope.getSymbols().size() > 0) {
			while (i.hasNext()) {
				Symbol sym = (Symbol) i.next();
				if (sym.getValue() != null && !(sym.getType() instanceof ArrayType)) {
					SymbolDefinitionAnnotation annot = AnnotationsFactory.eINSTANCE.createSymbolDefinitionAnnotation();
					annot.setSymbolDefinition(sym.getValue());
					sym.getAnnotations().put(AnnotationKeys.SYMBOL_DEF_ANNOTATION_KEY.getLiteral(),annot);
					//note : creating a new set instructions moves the symbol value
					Instruction setInstruction = set(symbref(sym), sym.getValue());
					//therefore the block init becomes empty 
					((BasicBlock)simpleForBlock.getInitBlock()).getInstructions().add(setInstruction);
				}
			}
		}
		return super.caseSimpleForBlock(simpleForBlock);
	}
	
	public static void fixReferencesAfterCopy(EObject o) {
		EList<SetInstruction> eAllContentsFirstInstancesOf = EMFUtils.eAllContentsFirstInstancesOf(o, SetInstruction.class);
		for (SetInstruction si : eAllContentsFirstInstancesOf) {
			Instruction source = si.getSource();
			if (source != null && source.getAnnotations().containsKey(AnnotationKeys.SYMBOL_DEF_ANNOTATION_KEY.getLiteral())) {
				StringAnnotation annotation = (StringAnnotation)source.getAnnotation(AnnotationKeys.SYMBOL_DEF_ANNOTATION_KEY.getLiteral());
				String name = annotation.getContent();
				
				EObject container = si.eContainer();
				while (container != null && !(container instanceof ScopeContainer )) {
					container = container.eContainer();
				}
				if (container != null && container instanceof ScopeContainer) {
					Scope scope = ((ScopeContainer)container).getScope();
					if (scope == null) {
						throw new RuntimeException();
					}
					Symbol lookup = scope.lookup(name);
					if (!lookup.getAnnotations().containsKey(AnnotationKeys.SYMBOL_DEF_ANNOTATION_KEY.getLiteral()))
						throw new RuntimeException("Could not find SymbolDefinitionAnnotation");
					((SymbolDefinitionAnnotation)lookup.getAnnotation(AnnotationKeys.SYMBOL_DEF_ANNOTATION_KEY.getLiteral())).setSymbolDefinition(source);
				} else {
					throw new RuntimeException("No ScopeContainer found in the containment hierarchy of "+si);
				}
			}
		}
		
	}
}
