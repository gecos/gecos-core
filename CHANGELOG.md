## Release version 0.9.2 (29 Aug. 2019)
* More extensions to support features in gecos-float2fix

## Release version 0.9.1 (20 Aug. 2019)
* Additional extensions to support features in gecos-float2fix

## Release version 0.9.0 (08 Aug. 2019)
* This release concerns core.profiling project, which is extended to support additional features necessary for gecost-float2fix.


## Release version 0.8.1 (12 fév 2019)

* Technical release

## Release version 0.7.8 (07 fév 2019)

* - Updated Xcore and generated files
* Merge branch 'develop' of git@gitlab.inria.fr:gecos/gecos-core.git into develop
* - Fixed bug in GenericInstruction compute type (incorrect initial value for inferred type)
* Merge branch 'classpath-patch-1' into develop
* Update target definition
* Fix .classpath: remove deleted xtend-gen folder
* Fixed NPE bug in SSA analyzer when dealing with readonly variables
---
# Changelog

## Release version 0.7.7 (05 oct 2018)

* Fix typo on getSymbolAddress
* start working on next version SNAPSHOT

## Release version 0.7.6 (28 sep 2018)

* - Cosmetic changes to several toString methods
* Merge branch 'develop' of gitlab.inria.fr:gecos/gecos-core into develop
* Add specialized getSymbolAddress method for CallInstruction to make it more robust
* Merge branch 'develop' of https://gitlab.inria.fr/gecos/gecos-core into develop
* wrapped Files.list in ProfilingInfoParser with try-with-resources clause to prevent output directory to be left open during exploration
* start working on next version SNAPSHOT
## Release version 0.7.4 (03 juil. 2018)

* Merge branch 'develop' of https://gitlab.inria.fr/gecos/gecos-core into develop
* Added missing xtend-gen files
* Added missing xtend-gen files
* NPE defensive recoding for getSuccessor and getPredecessors
* [fix] Add line return before pragma, parenthesis around shift operators
* [fix] Fix test condition to cover outlining in new files, fix function return type scope and type
* [fix] build properties
* Add Procedure Separation module, and refactor procedure separation in BlockOutliner
* start working on next version SNAPSHOT
* Merge branch 'master' of https://gitlab.inria.fr/gecos/gecos-core
* add cstring to ProfileEngine.h
* adding binary for mac
* adding io_png files for reading png images
* profiling engine updated to handle injection from multiple files
* prepare version v0.7.3
* Revert "[fix] remove dummy function which caused problem during code generation"
* start working on next version SNAPSHOT
* prepare version v0.7.2
* [fix] remove dummy function which caused problem during code generation
* start working on next version SNAPSHOT
* prepare version v0.7.1
* [Fix] compiling Profiling Engine library for Macos
* [Fix] compiling Profiling Engine library for Macos
* [Fix] Profiling Engine library is now build at runtime if not already present, also now supports Macos
* [Fix] commit previously ignored resources/**/bin directories
* add cstring to ProfileEngine.h
* Added missing xtend-gen files
* Added missing xtend-gen files
* NPE defensive recoding for getSuccessor and getPredecessors
* Added support for block level analysis/transformations
* adding binary for mac
* adding io_png files for reading png images
* profiling engine updated to handle injection from multiple files
* [fix] Add line return before pragma, parenthesis around shift operators
* [fix] Fix test condition to cover outlining in new files, fix function return type scope and type
* export profiling.frontend package
* [fix] build properties
* Add Procedure Separation module, and refactor procedure separation in BlockOutliner

## Release version 0.7.3 (12 juin 2018)

* Revert "[fix] remove dummy function which caused problem during code generation"
---

## Release version 0.7.2 (11 juin 2018)

* [fix] remove dummy function which caused problem during code generation

---

## Release version 0.7.1 (08 juin 2018)

* [Fix] Profiling Engine library is now build at runtime if not already present
* [Fix] commit previously ignored resources/\*\*/bin directories
* [Add] Profiling support for Macos

---

## Release version 0.7.0 (06 juin 2018)

* [Mod] improve gecos.core.profiling:
  - fix code generation for AcFloat type and add CustomFloatDataType enum to select custom floating point backend (ac\_float or ct\_float)
  - add support for binary profiling files generation and retrieving
  - improve logging and error reporting 
  - fix resource location so it works when in jar 
  - add ErrorStats
* [Update] gecos-framework to v1.4.0
* [Add] more utils: ResourceLocator, ValuesNormalizer and ThreadUtils

---

## Release version 0.6.0 (18 mai 2018)

* [Update] gecos-framework to v1.3.0 and gecos-testframework to v2.1.0
* [Add] gecos.core.profiling providing simulation based symbol values profiling
* [Add] some common utility classes in gecos.utils
* [Add] method to create ExtendedAnnotation with references to `GecosUserAnnotationFactory`

---

## Release version 0.5.1 (18 avril 2018)

* [Fix] bug in MergeBasicBlock merging non contiguous BasicBlocks
* [Add] AbstractMultiDimLevel as parent of Pointer and Array Level and enhanced TypeAnalyzer API
* [Add] liveout symbols annotation distinguished from livein annotations
* [Add] error management for return and non SSA form in Outliner
* [Add] test coverage on BlockOutliner.
* [Add] option to outline procedure in separated files
* [CI] gitlab-ci can now use variable 'JENKINS\_FAIL\_MODE' to specify the behaviour depending on test results
* Improve support for outlining in new file.
* Silent Liveness Analysis
* Re-enable extended annotation factory method

---

## Release version 0.5.0 (23 mars 2018)

* [Releng] update eclipse dependencies to releases/oxygen/releng/201803211000
* [Releng] update 'gecos-testframework' to version 2.0.0 and 'gecos-framework' to version 1.2.0
* [Releng] relaxe version specification to range in target definition.
* [Add] explicit liveness annotation.
* [Add] copy() method to `GecosProject`.
* [Add] `CopyGecosProject` module.
* [Mod] modify Liveness using the new Liveness Annotation.
* [Mod] core.testframework was heavily changed; now use testframework v2.0.0
* [Test] merge similar test methods using DataProvider parameters.
* [Test] Refactor tests, enabled CdtGCC tests, moved resources from testframework.
* [Test] use default directory (in ./target) for test generated files.
* [CI] update to latest 'gecos-buildtools'; fix Jenkins pipeline configuration and improve logging.

---


## Release version 0.4.0 (12 févr. 2018)

* [Fix] C code generator for `CompositeBlock` when it is a `Procedure`'s body
* [Mod] Chang from `Enumeration` to `List` interface in CallGraph related classes.
* [Releng] update dependency to 'gecos-testframework' to version v1.0.2.

---


## Release version 0.3.1 (09 janv. 2018)

* [New] option in `CDTFrontend` to select whether or not it generates `SimpleForBlock`s (enabled by default i.e. same behaviour as before).
* [Fix] ConcurentModification problem in `BasicBlock.splitAt()` (issue #12).
* [Fix] make `BasicBlockSwitch` visit Switch blocks.
* [Update] _gecos-tools-*_ depedencies to latest release versions.
* [CI] deploying a release version now automatically triggers an integration build/test in a dedicated branch in 'gecos-composite'.

---


## Release version 0.3.0 (23 Nov 2017)

* [New] a Dangling reference checker module.
* [New] support for skipping all `AliasTypes` in `TypeAnalyzer`:
  - `TypeAnalyzer` now builds levels lazily.
  - `BaseLevel` is now added to `TypeAnalyzer.levels`.
* [New] block-to-procedure outlining transformation and integration tests.
* [New] `ComputeSSA` now annotate all added Wrapper `CompositeBlock`s and Dummy `BasicBlock`s.
* [New] _gecos.core.testframework.impl_ to *Core* feature.
* [New] added _model.integration.tests_.
* [New] method to create an `ExtendedAnnotation` in `GecosUserAnnotationFactory`.
* [Fix] bug in frontend related to function calls via function pointers.
* [Fix] support for `IndirInstruction`s in `RemoveSSAForm`.
* [Fix] `Emunerator.isEqual()` (issue #8).
* [Mod] change tests default output directory to: _target/test-regen/TEST_CLASS_NAME_.
* [Mod] relocated `TypeAnalyzer` unit tests.
* [Releng] update _gecos.testframework_ to release version 1.0.1.
* [Releng] update submodule _gecos-buildtools_ to latest version.

---


## Release version 0.2.0 (18 Aug 2017)

Update bundles to svn HEAD (rev 17804) with major API changes.

### Changes
* `ArrayValueInstruction` is no longer a `ConstantInstruction`.
* `SizeofValueInstruction` is now `ExprComponent`: 
  - _targetInst_ attribute becomes _expr_.
* `MethodCallInstruction` is now `AddressComponent`:
  - _receiver_ attribute becomes _address_.
  - _isSame()_ now also compare names.
* `GecosFile` and `GecosProject` are now `AnnotatedElement`.
* `GecosProject` _macros_ attribute becomes a contained EMap <String,String> (now serializable):
  - _setMacros()_ no longer exists; use _getMarcos()_ instead.
* removed custom mappingInstances from tom mapping.
* disable _operation reflection_ for all Xcore models:
  see merge request [5](https://gitlab.inria.fr/gecos/gecos-core/merge_requests/5) for more details.
* `EnumeratorInstruction` no longer inherits `ExprComponent`.
* add 'Xcore SDK feature' to core feature dependencies.

### Add
* add new annotation 'FileLocationAnnotation' and method AnnotatedElement.getFileLocation().
* CDT frontend now support adding FileLocation annotations (disabled by default).
* add `ExternalLinkResoulver` module: can be used after `CDTFrontEnd` to resolve external
  (i.e. from another file/ProcedureSet) Symbols.

### Fixes
* fix `ICProject` name conflicts (in CDT frontend).
* fix problem with including GeCoS standard header files (from gecos.model.includes.std)
  when used from installed jar (i.e. sources not available in workspace).
* fix extracting includes from jar when running on windows.
* fix `SaveGecosProject` file path.
* fix Xcore models location in packaged jars.

---


## Release version 0.1.0 (26 Jul 2017)

Migrated GeCoS Core plugins from SVN repository at */trunk/gecos/framework (rev 17751)*
except _fr.irisa.cairn.gecos.model.includes.std_ at rev 17799.

### Changes
* Convert Gecos.ecore model to Xcore.
* Most annotation keys are moved either to `GecosUserAnnotationFactory` or `AnnotationKeys` enum.
* Remove direct access to `FunctionType`.parameters; replaced by listParameters() and addParameter() methods.
* Move `BlocksUpdate` to package _fr.irisa.cairn.gecos.model.tools.utils_.
* `BasicBlock` numbering is now managed by `GecosUserBlockFactory`.
* `CaseInstruction` is now a `ExprComponent` (was ComponentInstruction)
* `ArrayInstruction` is now a `ComponentInstruction` (was ComplexInstruction)
* `ComplexInstruction` is now only an interface and no longer have direct access to children:
  - replace getChildren() by listChildren(): return an **UNmodifiable**, ordered list of the contained child Instructions.
  - remove getElements()
  - getRootInstuction() is deprecated and fall back to `Instruction`.root()
  - modify implementation of removeChild()/replaceChild(); they now use `EObject`.remove()/replace().
* `ChildrenListInstruction` replaces the old implementation of `ComplexInstruction`.
* `ComponentInstruction` (still) inherits `ComplexInstruction` but had the following modifications:
  - remove children (components) EList; each inheriting `ComponentInstruction` now define its own components.
  - replace getChildren() by listChildren(): returns an **UNmodifiable**, ordered list of the contained child Instructions.
  - remove set/getComponentAt().
* Replace the generic `PortMap` with non-generic `InPortMap` and `OutPortMap` in Dag EPackage.

### Add
* Add `AnnotationKeys` enum to Annotations EPackage.
* Add `ChildrenListInstruction` to Insts EPackage.
* Add _gecos-core_ specific implementation of _gecos-testframewrork_ model.
* Add attribute '\_\_internal\_parameter\_wrapers' to `FunctionType`; this is used internally and **must NOT** be accessed !
* Add attribute '\_\_\_internal\_cached\_\_type' to `Instruction`; this is used internally and **must NOT** be accessed !



