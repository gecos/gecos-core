package fr.irisa.cairn.gecos.model.dag.adapter.model;
import gecos.dag.DAGEdge;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGNode;
import gecos.dag.DAGPort;

import java.util.Set;

/**
 * Interface of a graph adapter.
 * This graph adapt a DAGInstruction
 * @author <your name>
 *
 */
public interface IDAGInstructionAdapter
		extends
			fr.irisa.cairn.graph.IGraph,
			fr.irisa.cairn.graph.observer.IObservable {

	/**
	 * Get the adapted graph.
	 *
	 * @return the adapted graph
	 */
	DAGInstruction getAdaptedGraph();

	/**
	 * Get the container of all adapter/adapted Objects.
	 *
	 * @return the container
	 */
	IAdaptedContainer getContainer();

	Set<IDAGNodeAdapter> getNodes();

	Set<IDAGEdgeAdapter> getEdges();

	IDAGInstructionAdapter getAdapterGraph(DAGInstruction graph);

	IDAGNodeAdapter getAdapterNode(DAGNode node);

	IDAGPortAdapter getAdapterPort(DAGPort port);

	IDAGEdgeAdapter getAdapterEdge(DAGEdge edge);

}
