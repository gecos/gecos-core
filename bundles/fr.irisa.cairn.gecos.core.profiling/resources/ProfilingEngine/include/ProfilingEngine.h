#ifndef __PROFILING_ENGINE_H__
#define __PROFILING_ENGINE_H__

#include <string>
#include <typeinfo>
#include <vector>
#include <iostream>
#include <fstream>
#include <stdlib.h>


namespace profengine {

	/**
	 *
	 */
	class Variable {
	protected:
		std::string m_name;
		int m_uid;

		Variable(std::string name, int uid) : m_name(name), m_uid(uid) {};

	public:
		virtual ~Variable() {};
		std::string getName();
		int getUid();
		std::string toString();

		virtual void newSnapshot() = 0;
		virtual void writeHuman(std::fstream &fs) = 0;
    };


    /**
	 * Template not pre-compiled to support any type (ac_fixed ..)
	 */
	template <class T> class GenericVariable : public Variable {
	private:
		std::vector<T> current_snapshot;
		std::vector< std::vector<T> > snapshots; // [ snapshot values ]

	public:
		GenericVariable(std::string name, int uid):Variable(name, uid){};
		~GenericVariable(){}

		void addValue(T value) {
		#ifdef DEBUG
			   std::cout << "  Pushing value to variable '" << toString() << "' = " << value <<std::endl;
		#endif
		   current_snapshot.push_back(value);
		}

		void newSnapshot() {
		#ifdef DEBUG
			std::cout << "Finish recording current copy values of variable '" << toString() << "'" << std::endl;
		#endif
			snapshots.push_back(current_snapshot);
			current_snapshot.clear();
		}

	   void writeHuman(std::fstream &fs) {
		   fs << "uid: " << m_uid << std::endl;
		   fs << "name: " << m_name << std::endl;
		   fs << "elemTypeIdName: " << typeid(T).name() << std::endl;
		   fs << "elemSizeBytes: " << sizeof(T) << std::endl;
		   fs << "nbSnapshots: " << snapshots.size() << std::endl;
		   fs << std::endl;

		   for(unsigned i = 0; i < snapshots.size(); i++) {
			   std::vector<T> values = snapshots[i];
			   fs << "snapshot: " << i << std::endl;
			   fs << "nbElems: " << values.size() << std::endl;
			   for(unsigned j = 0; j < values.size(); j++)
				   fs << values[j] << std::endl;
			   fs << std::endl;
		   }
	   }
	};


	class Generator;
	class VariablesMap;

    /**
     * ProfilingEngine
     */
    class ProfilingEngine {
	private:
		VariablesMap *m_values; //saved variables <uid, variable>
		Generator *generator;
	public:
		ProfilingEngine();
		~ProfilingEngine();


		void saveSnapshot(int uid);
		Variable* getVariable(int uid);
		void saveVariable(int uid, Variable *variable);
		void writeHuman(std::string outputFolderPath);
		void setGeneratorSeed(int seed);
		void setGeneratorUniformIntDistribution(int minInclusive, int maxInclusive);
		void setGeneratorUniformDoubleDistribution(double minInclusive, double maxExclusive);
		void setGeneratorNormalIntDistribution(double mean, double stddev);
		void setGeneratorNormalDoubleDistribution(double mean, double stddev);
		void setGeneratorFromFile(const char *filePath);
		double generate();
    };

}

//----------------------------------------------------------------------------------------
//  Collect values API
//----------------------------------------------------------------------------------------


static profengine::ProfilingEngine *__internal_sim_manager;

void ProfilingEngine_start() {
	__internal_sim_manager = new profengine::ProfilingEngine;
}

void ProfilingEngine_finalize(const char* outputFolderPath) {
	__internal_sim_manager->writeHuman(std::string(outputFolderPath));
//    __internal_sim_manager->writeBinary(std::string(outputFolderPath));
	delete __internal_sim_manager;
}

template<typename T>
void ProfilingEngine_pushValue(int uid, const char* name, T value) {
	profengine::Variable* var = __internal_sim_manager->getVariable(uid);

	profengine::GenericVariable<T> *gvar;
    if(var == NULL) {
        gvar = new profengine::GenericVariable<T>(std::string(name), uid);
        __internal_sim_manager->saveVariable(uid, gvar);
    }
    else{
        gvar = dynamic_cast< profengine::GenericVariable<T>* >(var);
        if(gvar == NULL) {
        	std::cerr <<"The variable '" << name << "' was saved earlier as a different element type than '" << typeid(T).name() << "'" << std::endl;
        	exit(1);
    	}
    }

    gvar->addValue(value);
}


#define ProfilingEngine_saveSnapshot(/*int*/ uid)  	__internal_sim_manager->saveSnapshot(uid);
	
//void ProfilingEngine_saveSnapshot(int uid) {
//	profengine::Variable* var = __internal_sim_manager->getVariable(uid);
//	if(var == NULL) {
//		std::cerr <<"No variable with uid '" << uid << "' was registered!" << std::endl;
//		exit(1);
//	}
//
//	var->newSnapshot();
//}

//----------------------------------------------------------------------------------------
//  Inject values API
//----------------------------------------------------------------------------------------

#define ProfilingEngine_initInjector_fromVar(var)  \
	__internal_sim_manager->setGeneratorFromVar(var)

#define ProfilingEngine_initInjector_fromFile(filePath) \
	__internal_sim_manager->setGeneratorFromFile(filePath)

#define ProfilingEngine_initInjector_RandomSeed(newSeed) \
	__internal_sim_manager->setGeneratorSeed(newSeed);

#define ProfilingEngine_initInjector_RandomUniformInt(minInclusive, maxInclusive) \
	__internal_sim_manager->setGeneratorUniformIntDistribution(minInclusive, maxInclusive);

#define ProfilingEngine_initInjector_RandomUniformDouble(minInclusive, maxInclusive) \
	__internal_sim_manager->setGeneratorUniformDoubleDistribution(minInclusive, maxInclusive);

#define ProfilingEngine_initInjector_RandomNormalInt(mean, stddev) \
	__internal_sim_manager->setGeneratorNormalIntDistribution(mean, stddev);

#define ProfilingEngine_initInjector_RandomNormalDouble(mean, stddev) \
	__internal_sim_manager->setGeneratorNormalDoubleDistribution(mean, stddev);

#define ProfilingEngine_injectValue()   __internal_sim_manager->generate()


#endif // __PROFILING_ENGINE_H__
