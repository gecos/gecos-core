package fr.irisa.cairn.gecos.model.cdtfrontend.exception;

import org.eclipse.cdt.core.dom.ast.IASTNode;

public class CDTParserProblemException extends UnsupportedOperationException {

	private static final long serialVersionUID = 1L;

	public final IASTNode node;
	public CDTParserProblemException(IASTNode node, String string) {
		super(string);
		this.node = node;
	}
	public CDTParserProblemException(IASTNode node, String string, Exception e) {
		super(string,e);
		this.node = node;
	}
	
	@Override
	public String getMessage() {
		String res = super.getMessage();
		return getLocation()+"\n"+res;		
	}
	
	public String getLocation() {
		String res = "Could not determine location";
		if (this.node != null)
			res = " >> line "+this.node.getFileLocation().getStartingLineNumber()+" of "+this.node.getFileLocation().getFileName()+" : "+this.node.getRawSignature();
		return res;
	}

}
