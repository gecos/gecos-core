package fr.irisa.cairn.gecos.model.type.tests;

import fr.irisa.cairn.gecos.model.type.utils.TypeData;
import fr.irisa.cairn.gecos.model.type.utils.TypeTestTemplate;

public class TestTypeParsing extends TypeTestTemplate {
	
	@Override
	protected void run(TypeData data) {
		testCodeToType(data);
	}
	
}
