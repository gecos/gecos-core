/**
 */
package gecos.blocks;

import fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;

import gecos.core.Scope;

import gecos.instrs.BranchType;
import gecos.instrs.Instruction;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Basic Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.blocks.BasicBlock#getInstructions <em>Instructions</em>}</li>
 *   <li>{@link gecos.blocks.BasicBlock#getDefEdges <em>Def Edges</em>}</li>
 *   <li>{@link gecos.blocks.BasicBlock#getOutEdges <em>Out Edges</em>}</li>
 *   <li>{@link gecos.blocks.BasicBlock#getUseEdges <em>Use Edges</em>}</li>
 *   <li>{@link gecos.blocks.BasicBlock#getInEdges <em>In Edges</em>}</li>
 *   <li>{@link gecos.blocks.BasicBlock#getLabel <em>Label</em>}</li>
 *   <li>{@link gecos.blocks.BasicBlock#getFlags <em>Flags</em>}</li>
 * </ul>
 *
 * @see gecos.blocks.BlocksPackage#getBasicBlock()
 * @model annotation="gmf.node figure='rectangle' label='number' label.placement='external' label.icon='false'"
 * @generated
 */
public interface BasicBlock extends Block, BlocksVisitable {
	/**
	 * Returns the value of the '<em><b>Instructions</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.instrs.Instruction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instructions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instructions</em>' containment reference list.
	 * @see gecos.blocks.BlocksPackage#getBasicBlock_Instructions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Instruction> getInstructions();

	/**
	 * Returns the value of the '<em><b>Def Edges</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.blocks.DataEdge}.
	 * It is bidirectional and its opposite is '{@link gecos.blocks.DataEdge#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Def Edges</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Def Edges</em>' containment reference list.
	 * @see gecos.blocks.BlocksPackage#getBasicBlock_DefEdges()
	 * @see gecos.blocks.DataEdge#getFrom
	 * @model opposite="from" containment="true"
	 * @generated
	 */
	EList<DataEdge> getDefEdges();

	/**
	 * Returns the value of the '<em><b>Out Edges</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.blocks.ControlEdge}.
	 * It is bidirectional and its opposite is '{@link gecos.blocks.ControlEdge#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Out Edges</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Out Edges</em>' containment reference list.
	 * @see gecos.blocks.BlocksPackage#getBasicBlock_OutEdges()
	 * @see gecos.blocks.ControlEdge#getFrom
	 * @model opposite="from" containment="true"
	 * @generated
	 */
	EList<ControlEdge> getOutEdges();

	/**
	 * Returns the value of the '<em><b>Use Edges</b></em>' reference list.
	 * The list contents are of type {@link gecos.blocks.DataEdge}.
	 * It is bidirectional and its opposite is '{@link gecos.blocks.DataEdge#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use Edges</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Edges</em>' reference list.
	 * @see gecos.blocks.BlocksPackage#getBasicBlock_UseEdges()
	 * @see gecos.blocks.DataEdge#getTo
	 * @model opposite="to"
	 * @generated
	 */
	EList<DataEdge> getUseEdges();

	/**
	 * Returns the value of the '<em><b>In Edges</b></em>' reference list.
	 * The list contents are of type {@link gecos.blocks.ControlEdge}.
	 * It is bidirectional and its opposite is '{@link gecos.blocks.ControlEdge#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Edges</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In Edges</em>' reference list.
	 * @see gecos.blocks.BlocksPackage#getBasicBlock_InEdges()
	 * @see gecos.blocks.ControlEdge#getTo
	 * @model opposite="to"
	 * @generated
	 */
	EList<ControlEdge> getInEdges();

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see gecos.blocks.BlocksPackage#getBasicBlock_Label()
	 * @model default="" unique="false" dataType="gecos.blocks.String"
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link gecos.blocks.BasicBlock#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Flags</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Flags</em>' attribute.
	 * @see #setFlags(int)
	 * @see gecos.blocks.BlocksPackage#getBasicBlock_Flags()
	 * @model unique="false" dataType="gecos.blocks.int"
	 * @generated
	 */
	int getFlags();

	/**
	 * Sets the value of the '{@link gecos.blocks.BasicBlock#getFlags <em>Flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Flags</em>' attribute.
	 * @see #getFlags()
	 * @generated
	 */
	void setFlags(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.blocks.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%java.lang.StringBuffer%&gt; sb = new &lt;%java.lang.StringBuffer%&gt;();\n&lt;%java.lang.String%&gt; content = this.getInstructions().toString();\nint _length = content.length();\nboolean _greaterThan = (_length &gt; 40);\nif (_greaterThan)\n{\n\tint _size = this.getInstructions().size();\n\tboolean _greaterThan_1 = (_size &gt; 1);\n\tif (_greaterThan_1)\n\t{\n\t\t&lt;%java.lang.String%&gt; _string = this.getInstructions().get(0).toString();\n\t\t&lt;%java.lang.String%&gt; _plus = (_string + \", ...\");\n\t\tcontent = _plus;\n\t}\n}\n&lt;%java.lang.String%&gt; _pathString = this.getPathString();\n&lt;%java.lang.String%&gt; _string_1 = sb.toString();\nreturn (_pathString + _string_1);'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.blocks.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _number = this.getNumber();\nreturn (\"BB\" + &lt;%java.lang.Integer%&gt;.valueOf(_number));'"
	 * @generated
	 */
	String toShortString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt; _xblockexpression = null;\n{\n\tfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.blocks.ControlEdge%&gt;, &lt;%gecos.blocks.BasicBlock%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.blocks.ControlEdge%&gt;, &lt;%gecos.blocks.BasicBlock%&gt;&gt;()\n\t{\n\t\tpublic &lt;%gecos.blocks.BasicBlock%&gt; apply(final &lt;%gecos.blocks.ControlEdge%&gt; it)\n\t\t{\n\t\t\treturn it.getFrom();\n\t\t}\n\t};\n\tfinal &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt; res = &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt;asEList(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%gecos.blocks.ControlEdge%&gt;, &lt;%gecos.blocks.BasicBlock%&gt;&gt;map(this.getInEdges(), _function)));\n\tfinal &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt; _function_1 = new &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt;()\n\t{\n\t\tpublic void accept(final &lt;%gecos.blocks.BasicBlock%&gt; bb)\n\t\t{\n\t\t\tif ((bb == null))\n\t\t\t{\n\t\t\t\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(\"Inconsistent control flow in IR\");\n\t\t\t}\n\t\t}\n\t};\n\tres.forEach(_function_1);\n\t_xblockexpression = res;\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	EList<BasicBlock> getPredecessors();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitBasicBlock(this);'"
	 * @generated
	 */
	void accept(BlocksVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.blocks.ControlEdge%&gt;&gt; _function = new &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.blocks.ControlEdge%&gt;&gt;()\n{\n\tpublic void accept(final &lt;%gecos.blocks.ControlEdge%&gt; it)\n\t{\n\t\tit.setTo(null);\n\t\tit.setFrom(null);\n\t\tit.setCond(null);\n\t}\n};\n((&lt;%java.util.List%&gt;&lt;&lt;%gecos.blocks.ControlEdge%&gt;&gt;)org.eclipse.xtext.xbase.lib.Conversions.doWrapArray(((&lt;%gecos.blocks.ControlEdge%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(this.getOutEdges(), &lt;%gecos.blocks.ControlEdge%&gt;.class)).clone())).forEach(_function);\nthis.getOutEdges().clear();'"
	 * @generated
	 */
	void clearOutEdges();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt; _xblockexpression = null;\n{\n\tfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.blocks.ControlEdge%&gt;, &lt;%gecos.blocks.BasicBlock%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.blocks.ControlEdge%&gt;, &lt;%gecos.blocks.BasicBlock%&gt;&gt;()\n\t{\n\t\tpublic &lt;%gecos.blocks.BasicBlock%&gt; apply(final &lt;%gecos.blocks.ControlEdge%&gt; it)\n\t\t{\n\t\t\treturn it.getTo();\n\t\t}\n\t};\n\tfinal &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt; res = &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt;asEList(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%gecos.blocks.ControlEdge%&gt;, &lt;%gecos.blocks.BasicBlock%&gt;&gt;map(this.getOutEdges(), _function)));\n\tfinal &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt; _function_1 = new &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt;()\n\t{\n\t\tpublic void accept(final &lt;%gecos.blocks.BasicBlock%&gt; bb)\n\t\t{\n\t\t\tif ((bb == null))\n\t\t\t{\n\t\t\t\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(\"Inconsistent control flow in IR\");\n\t\t\t}\n\t\t}\n\t};\n\tres.forEach(_function_1);\n\t_xblockexpression = res;\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	EList<BasicBlock> getSuccessors();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model sUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getInstructions().remove(s);'"
	 * @generated
	 */
	void removeInstruction(Instruction s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.blocks.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.join(this.getInstructions(), \"\\\\n\");'"
	 * @generated
	 */
	String contentString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Merge this block with its unique successor
	 * 
	 * @param b successor block
	 * <!-- end-model-doc -->
	 * @model bUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _size = this.getOutEdges().size();\nboolean _greaterThan = (_size &gt; 1);\nif (_greaterThan)\n{\n\tthrow new &lt;%java.lang.IllegalArgumentException%&gt;(\"Cannot merge a basicBloc with more than one out Edge\");\n}\nint _size_1 = b.getInEdges().size();\nboolean _greaterThan_1 = (_size_1 &gt; 1);\nif (_greaterThan_1)\n{\n\tthrow new &lt;%java.lang.IllegalArgumentException%&gt;(\"Cannot merge with a basicBloc with more than one in Edge\");\n}\nfinal &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; _function = new &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;()\n{\n\tpublic void accept(final &lt;%gecos.instrs.Instruction%&gt; it)\n\t{\n\t\tb.prependInstruction(it);\n\t}\n};\n&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;reverseView(this.getInstructions()).forEach(_function);\nthis.getParent().removeBlock(this);\nthis.getOutEdges().get(0).disconnect();\nwhile ((this.getInEdges().size() != 0))\n{\n\t{\n\t\tfinal &lt;%gecos.blocks.ControlEdge%&gt; e = this.getInEdges().get(0);\n\t\tb.connectFromBasic(e.getFrom(), e.getCond());\n\t\te.disconnect();\n\t}\n}'"
	 * @generated
	 */
	void mergeWithBasic(BasicBlock b);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Split {@code this} block just before the instruction {@code g}.
	 * A new {@link BasicBlock} is created, to which all instructions
	 * after {@code g} are moved. The new block is inserted just after
	 * {@code this} block, and it might be empty.
	 * The instruction {@code g} will not be contained in neither blocks.
	 * 
	 * @param g cutting point. Must be contained in {@code this}.
	 * @return the newly created {@link BasicBlock}.
	 * @throws IllegalArgumentException if {@code g} is not contained in {@code this}.
	 * <!-- end-model-doc -->
	 * @model unique="false" gUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final int idx = this.getInstructions().indexOf(g);\nif ((idx &lt; 0))\n{\n\tthrow new &lt;%java.lang.IllegalArgumentException%&gt;(\"The specified instruction is not contained in this basic block!\");\n}\nfinal &lt;%gecos.blocks.BasicBlock%&gt; b = &lt;%gecos.blocks.BlocksFactory%&gt;.eINSTANCE.createBasicBlock();\nthis.getParent().addBlockAfter(b, this);\nthis.removeInstruction(g);\nfinal &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; _function = new &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;()\n{\n\tpublic void accept(final &lt;%gecos.instrs.Instruction%&gt; it)\n\t{\n\t\tb.addInstruction(it);\n\t}\n};\n((&lt;%java.util.List%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;)org.eclipse.xtext.xbase.lib.Conversions.doWrapArray(((&lt;%gecos.instrs.Instruction%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;drop(this.getInstructions(), idx), &lt;%gecos.instrs.Instruction%&gt;.class)).clone())).forEach(_function);\nwhile ((this.getOutEdges().size() != 0))\n{\n\t{\n\t\tfinal &lt;%gecos.blocks.ControlEdge%&gt; e = this.getOutEdges().get(0);\n\t\te.disconnect();\n\t\t&lt;%gecos.blocks.BasicBlock%&gt; _to = e.getTo();\n\t\tif (_to!=null)\n\t\t{\n\t\t\t_to.connectFromBasic(b, e.getCond());\n\t\t}\n\t}\n}\nreturn b;'"
	 * @generated
	 */
	BasicBlock splitAt(Instruction g);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getInstructions().add(0, instr);'"
	 * @generated
	 */
	void prependInstruction(Instruction instr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.Block%&gt; _parent = this.getParent();\nboolean _tripleEquals = (_parent == null);\nif (_tripleEquals)\n{\n\tthrow new &lt;%java.lang.NullPointerException%&gt;(((\"Basic block \" + this) + \" has an empty Scope\"));\n}\n&lt;%gecos.blocks.Block%&gt; _parent_1 = this.getParent();\nreturn ((&lt;%gecos.core.ScopeContainer%&gt;) _parent_1).getScope();'"
	 * @generated
	 */
	Scope getAssociatedScope();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" fromUnique="false" condUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((from == null))\n{\n\tthrow new &lt;%java.lang.IllegalArgumentException%&gt;(\"Cannot connect to a null BB\");\n}\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.blocks.ControlEdge%&gt;&gt; _outEdges = from.getOutEdges();\nfor (final &lt;%gecos.blocks.ControlEdge%&gt; e : _outEdges)\n{\n\tif ((((e.getFrom() == from) &amp;&amp; (e.getTo() == this)) &amp;&amp; (e.getCond() == cond)))\n\t{\n\t\treturn null;\n\t}\n}\nfinal &lt;%gecos.blocks.ControlEdge%&gt; e_1 = &lt;%gecos.blocks.BlocksFactory%&gt;.eINSTANCE.createControlEdge();\ne_1.setCond(cond);\ne_1.setFrom(from);\ne_1.setTo(this);\nreturn e_1;'"
	 * @generated
	 */
	ControlEdge connectFromBasic(BasicBlock from, BranchType cond);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" toUnique="false" condUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder%&gt; visitor = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder%&gt;();\nvisitor.doSwitch(this);\nboolean _containsGotos = visitor.containsGotos();\nif (_containsGotos)\n{\n\tfinal &lt;%gecos.instrs.GotoInstruction%&gt; g = visitor.getGotos().get(0);\n\tfinal &lt;%gecos.blocks.BasicBlock%&gt; sndTo = g.getLabelInstruction().getBasicBlock();\n\tif ((sndTo != to))\n\t{\n\t\t&lt;%java.lang.System%&gt;.out.println(\"[BasicBlock] Warning : avoid connection. Basic Block has goto instruction.\");\n\t\treturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.blocks.ControlEdge%&gt;&gt;asEList();\n\t}\n}\nfinal &lt;%gecos.blocks.ControlEdge%&gt; edge = to.connectFromBasic(this, cond);\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.blocks.ControlEdge%&gt;&gt;newBasicEList(edge);'"
	 * @generated
	 */
	EList<ControlEdge> connectTo(Block to, BranchType cond);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getInstructions().add(instr);'"
	 * @generated
	 */
	void addInstruction(Instruction instr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" iDataType="gecos.blocks.int" iUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getInstructions().get(i);'"
	 * @generated
	 */
	Instruction getInstruction(int i);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Inserts {@code instr} before {@code pos}
	 * <!-- end-model-doc -->
	 * @model instrUnique="false" posUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final int i = this.getInstructions().indexOf(pos);\nif ((i &gt;= 0))\n{\n\tthis.getInstructions().add(i, instr);\n}\nelse\n{\n\t&lt;%java.lang.System%&gt;.err.println((((\"Warning : could not insert \" + instr) + \" before \") + pos));\n}'"
	 * @generated
	 */
	void insertInstructionBefore(Instruction instr, Instruction pos);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Inserts {@code instr} after {@code pos}
	 * <!-- end-model-doc -->
	 * @model instrUnique="false" posUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final int i = this.getInstructions().indexOf(pos);\nif ((i &gt;= 0))\n{\n\tthis.getInstructions().add((i + 1), instr);\n}\nelse\n{\n\t&lt;%java.lang.System%&gt;.err.println((((\"Warning : could not insert \" + instr) + \" after \") + pos));\n}'"
	 * @generated
	 */
	void insertInstructionAfter(Instruction instr, Instruction pos);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model oldiUnique="false" newiUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final int i = this.getInstructions().indexOf(oldi);\nif ((i &gt;= 0))\n{\n\tthis.getInstructions().set(i, newi);\n}'"
	 * @generated
	 */
	void replaceInstruction(Instruction oldi, Instruction newi);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='while ((this.getInstructions().size() &gt; 0))\n{\n\tthis.removeInstruction(this.getInstructions().get(0));\n}'"
	 * @generated
	 */
	void removeAllInstructions();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return the last {@Instruction} in this basic block, or {@code null} if empty.
	 * <!-- end-model-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;last(this.getInstructions());'"
	 * @generated
	 */
	Instruction lastInstruction();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return true if {@code this} basic block is connected to {@code b}.
	 * <!-- end-model-doc -->
	 * @model unique="false" bUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.blocks.ControlEdge%&gt;&gt; _outEdges = this.getOutEdges();\nfor (final &lt;%gecos.blocks.ControlEdge%&gt; e : _outEdges)\n{\n\t&lt;%gecos.blocks.BasicBlock%&gt; _to = e.getTo();\n\tboolean _tripleEquals = (_to == b);\n\tif (_tripleEquals)\n\t{\n\t\treturn true;\n\t}\n}\nreturn false;'"
	 * @generated
	 */
	boolean isConnectedTo(BasicBlock b);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * In case {@code this} basic block is a successor of {@code from},
	 * then disconnect it.
	 * <!-- end-model-doc -->
	 * @model fromUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.blocks.ControlEdge%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.blocks.ControlEdge%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%gecos.blocks.ControlEdge%&gt; it)\n\t{\n\t\t&lt;%gecos.blocks.BasicBlock%&gt; _to = it.getTo();\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf((_to == &lt;%this%&gt;));\n\t}\n};\nfinal &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.blocks.ControlEdge%&gt;&gt; res = &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.blocks.ControlEdge%&gt;&gt;asEList(((&lt;%gecos.blocks.ControlEdge%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.blocks.ControlEdge%&gt;&gt;filter(from.getOutEdges(), _function), &lt;%gecos.blocks.ControlEdge%&gt;.class)));\nfinal &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.blocks.ControlEdge%&gt;&gt; _function_1 = new &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.blocks.ControlEdge%&gt;&gt;()\n{\n\tpublic void accept(final &lt;%gecos.blocks.ControlEdge%&gt; it)\n\t{\n\t\tit.setTo(null);\n\t\tit.getFrom().getOutEdges().remove(it);\n\t}\n};\nres.forEach(_function_1);'"
	 * @generated
	 */
	void disconnectFromBasic(BasicBlock from);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Disconnect {@code this} BasicBlock from all other BBs (operates
	 * on both incoming and outgoing Control Flow Edges)
	 * <!-- end-model-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.blocks.ControlEdge%&gt;&gt; _function = new &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.blocks.ControlEdge%&gt;&gt;()\n{\n\tpublic void accept(final &lt;%gecos.blocks.ControlEdge%&gt; it)\n\t{\n\t\tit.setTo(null);\n\t}\n};\nthis.getOutEdges().forEach(_function);\nfinal &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.blocks.DataEdge%&gt;&gt; _function_1 = new &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.blocks.DataEdge%&gt;&gt;()\n{\n\tpublic void accept(final &lt;%gecos.blocks.DataEdge%&gt; it)\n\t{\n\t\tit.setTo(null);\n\t}\n};\nthis.getDefEdges().forEach(_function_1);\nthis.getOutEdges().clear();\nthis.getInEdges().clear();\nthis.getDefEdges().clear();\nthis.getUseEdges().clear();'"
	 * @generated
	 */
	void disconnect();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.blocks.int" unique="false" bUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.blocks.ControlEdge%&gt;&gt; _inEdges = this.getInEdges();\nfor (final &lt;%gecos.blocks.ControlEdge%&gt; e : _inEdges)\n{\n\t&lt;%gecos.blocks.BasicBlock%&gt; _from = e.getFrom();\n\tboolean _equals = &lt;%com.google.common.base.Objects%&gt;.equal(_from, b);\n\tif (_equals)\n\t{\n\t\treturn this.getInEdges().indexOf(e);\n\t}\n}\nreturn (-1);'"
	 * @generated
	 */
	int whichPredecessor(BasicBlock b);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model fDataType="gecos.blocks.int" fUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.setFlags(f);'"
	 * @generated
	 */
	void setFlag(int f);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model fDataType="gecos.blocks.int" fUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.setFlags((this.getFlags() | f));'"
	 * @generated
	 */
	void addFlag(int f);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model fDataType="gecos.blocks.int" fUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.setFlags((this.getFlags() &amp; (~f)));'"
	 * @generated
	 */
	void removeFlag(int f);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" fDataType="gecos.blocks.int" fUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return ((this.getFlags() &amp; f) != 0);'"
	 * @generated
	 */
	boolean isSet(int f);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.instrs.Instruction%&gt; i = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;head(this.getInstructions());\nif ((i == null))\n{\n\tthrow new &lt;%java.lang.RuntimeException%&gt;((\"Cannot get first instruction of an empty BasicBlock \" + this));\n}\nreturn i;'"
	 * @generated
	 */
	Instruction getFirstInstruction();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.instrs.Instruction%&gt; i = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;last(this.getInstructions());\nif ((i == null))\n{\n\tthrow new &lt;%java.lang.RuntimeException%&gt;((\"Cannot get last instruction of an empty BasicBlock \" + this));\n}\nreturn i;'"
	 * @generated
	 */
	Instruction getLastInstruction();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.blocks.int" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getInstructions().size();'"
	 * @generated
	 */
	int getInstructionCount();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.blocks.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _number = this.getNumber();\n&lt;%java.lang.String%&gt; _plus = (\"BB\" + &lt;%java.lang.Integer%&gt;.valueOf(_number));\nreturn this.getPathString(_plus);'"
	 * @generated
	 */
	String getPathString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" mngDataType="gecos.core.BlockCopyManager" mngUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.blocks.BasicBlock%&gt; newBB = &lt;%gecos.blocks.BlocksFactory%&gt;.eINSTANCE.createBasicBlock();\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; _instructions = this.getInstructions();\nfor (final &lt;%gecos.instrs.Instruction%&gt; inst : _instructions)\n{\n\t{\n\t\tfinal &lt;%gecos.instrs.Instruction%&gt; newInst = inst.copy();\n\t\tnewBB.getInstructions().add(newInst);\n\t}\n}\n&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory%&gt;.copyAnnotations(this, newBB);\nmng.link(this, newBB);\nreturn newBB;'"
	 * @generated
	 */
	Block managedCopy(BlockCopyManager mng);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return ((this.getInstructionCount() == 1) &amp;&amp; (this.getInstruction(0) instanceof &lt;%gecos.dag.DAGInstruction%&gt;));'"
	 * @generated
	 */
	boolean isDAG();

} // BasicBlock
