package fr.irisa.cairn.gecos.model.utils.misc;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;

public class ThreadUtils {

	public static Thread runInNewThread(Runnable task) {
		Thread t = new Thread() {
			@Override
			public void run() {
				task.run();
			}
		};
		t.start();
		return t;
	}

	public static void joinWrapper(Thread thread, boolean ignoreError) {
		try {
			thread.join();
		} catch (InterruptedException e) {
			if(ignoreError)
				e.printStackTrace();
			else
				throw new RuntimeException(e);
		}
	}
	
	public static void runTaskInForkJoinPool(int nbThreads, Runnable task) {
		ForkJoinPool pool = new ForkJoinPool(nbThreads);
		try {
			pool.submit(task).get();
		} catch (InterruptedException | ExecutionException e) {
			throw new RuntimeException(e);
		} finally {
			if (pool != null)
				 pool.shutdown();
		}
	}
	
}
