package fr.irisa.cairn.gecos.model.transforms.deadcodeelimination;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksDefaultVisitor;
import fr.irisa.cairn.gecos.model.transforms.blocks.simplifier.MergeCompositeBlocks;
import gecos.blocks.BasicBlock;
import gecos.blocks.BlocksVisitor;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ControlEdge;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;

/**
 * This pass iterates over the basic blocks of a procedure, following the
 * control flow, and remove all the basic blocks that are unreachable.
 */
public class UnreacheableCodeRemover extends GecosBlocksDefaultVisitor {
	
	List<Procedure> procs;
	public UnreacheableCodeRemover(GecosProject proj) {
		procs = proj.listProcedures();
	}
	public UnreacheableCodeRemover(ProcedureSet ps) {
		procs = ps.listProcedures();
	}
	public UnreacheableCodeRemover(Procedure proc) {
		procs = new ArrayList<Procedure>(1);
		procs.add(proc);
	}
	
	List<BasicBlock> visited;
	public void compute() {
		for (Procedure proc : procs) {
			
			List<BasicBlock> basicBlocks = proc.getBasicBlocks();
			BasicBlock start = proc.getStart();
			
			if (start.getOutEdges().size() == 0)
				System.err.println("["+this.getClass().getSimpleName()+"] [Warning] The control flow seems to be missing in procedure "+proc.getSymbolName()+". All blocks will be removed.");
			
			// 1- get the list of reachable blocks
			visited = new ArrayList<BasicBlock>();
			visited.add(proc.getStart());
			visited.add(proc.getEnd());
			visit(start);
			// 2- remove unreachable basic blocks
			basicBlocks.removeAll(visited);
			for (BasicBlock bb : basicBlocks) {
				for (ControlEdge e : bb.getOutEdges()) 
					e.setTo(null);
				bb.getOutEdges().clear();
				bb.getParent().removeBlock(bb);
			}

			// 3- remove empty control structures
			proc.getBody().accept(this);
			proc.getBody().accept((BlocksVisitor)new MergeCompositeBlocks());
		}
	}

	public void visit(BasicBlock bb) {
		visited.add(bb);
		for (ControlEdge e : bb.getOutEdges()) {
			BasicBlock to = e.getTo();
			if (visited.contains(to))
				continue;
			visit(to);
		}
	}
	
	@Override
	public void visitIfBlock(IfBlock i) {
		super.visitIfBlock(i);
		if  (i.getTestBlock() == null && 
			(i.getThenBlock() == null || (i.getThenBlock() instanceof CompositeBlock && ((CompositeBlock)i.getThenBlock()).getChildren().size() == 0)) && 
			(i.getElseBlock() == null || (i.getElseBlock() instanceof CompositeBlock && ((CompositeBlock)i.getElseBlock()).getChildren().size() == 0))
			) {
			i.getParent().removeBlock(i);
		}
	}
	
	@Override
	public void visitForBlock(ForBlock f) {
		super.visitForBlock(f);
		if  (f.getInitBlock() == null && f.getTestBlock() == null && f.getStepBlock() == null &&
			(f.getBodyBlock() == null || (f.getBodyBlock() instanceof CompositeBlock && ((CompositeBlock)f.getBodyBlock()).getChildren().size() == 0))
			) {
			f.getParent().removeBlock(f);
		}
	}
	
	@Override
	public void visitWhileBlock(WhileBlock w) {
		super.visitWhileBlock(w);
		if  (w.getTestBlock() == null &&
			(w.getBodyBlock() == null || (w.getBodyBlock() instanceof CompositeBlock && ((CompositeBlock)w.getBodyBlock()).getChildren().size() == 0))
			) {
			w.getParent().removeBlock(w);
		}
	}
	
	@Override
	public void visitLoopBlock(DoWhileBlock l) {
		super.visitLoopBlock(l);
		if  (l.getTestBlock() == null &&
			(l.getBodyBlock() == null || (l.getBodyBlock() instanceof CompositeBlock && ((CompositeBlock)l.getBodyBlock()).getChildren().size() == 0))
			) {
			l.getParent().removeBlock(l);
		}
	}
	
}
