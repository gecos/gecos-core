/**
 */
package gecos.types;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Float Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.FloatType#getPrecision <em>Precision</em>}</li>
 * </ul>
 *
 * @see gecos.types.TypesPackage#getFloatType()
 * @model
 * @generated
 */
public interface FloatType extends BaseType, TypesVisitable {
	/**
	 * Returns the value of the '<em><b>Precision</b></em>' attribute.
	 * The default value is <code>"SINGLE"</code>.
	 * The literals are from the enumeration {@link gecos.types.FloatPrecisions}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Precision</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Precision</em>' attribute.
	 * @see gecos.types.FloatPrecisions
	 * @see #setPrecision(FloatPrecisions)
	 * @see gecos.types.TypesPackage#getFloatType_Precision()
	 * @model default="SINGLE" unique="false"
	 * @generated
	 */
	FloatPrecisions getPrecision();

	/**
	 * Sets the value of the '{@link gecos.types.FloatType#getPrecision <em>Precision</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Precision</em>' attribute.
	 * @see gecos.types.FloatPrecisions
	 * @see #getPrecision()
	 * @generated
	 */
	void setPrecision(FloatPrecisions value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitFloatType(this);'"
	 * @generated
	 */
	void accept(TypesVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.types.boolean" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.FloatPrecisions%&gt; _precision = this.getPrecision();\nreturn &lt;%com.google.common.base.Objects%&gt;.equal(_precision, &lt;%gecos.types.FloatPrecisions%&gt;.HALF);'"
	 * @generated
	 */
	boolean isHalf();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.types.boolean" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.FloatPrecisions%&gt; _precision = this.getPrecision();\nreturn &lt;%com.google.common.base.Objects%&gt;.equal(_precision, &lt;%gecos.types.FloatPrecisions%&gt;.SINGLE);'"
	 * @generated
	 */
	boolean isSingle();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.types.boolean" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.FloatPrecisions%&gt; _precision = this.getPrecision();\nreturn &lt;%com.google.common.base.Objects%&gt;.equal(_precision, &lt;%gecos.types.FloatPrecisions%&gt;.DOUBLE);'"
	 * @generated
	 */
	boolean isDouble();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.types.boolean" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.FloatPrecisions%&gt; _precision = this.getPrecision();\nreturn &lt;%com.google.common.base.Objects%&gt;.equal(_precision, &lt;%gecos.types.FloatPrecisions%&gt;.LONG_DOUBLE);'"
	 * @generated
	 */
	boolean isLongDouble();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.boolean" unique="false" otherUnique="false" ignoreStorageClassDataType="gecos.types.boolean" ignoreStorageClassUnique="false" ignoreConstDataType="gecos.types.boolean" ignoreConstUnique="false" ignoreVolatileDataType="gecos.types.boolean" ignoreVolatileUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((other == this))\n{\n\treturn true;\n}\nif ((other instanceof &lt;%gecos.types.FloatType%&gt;))\n{\n\treturn (((&lt;%gecos.types.FloatType%&gt;)other).getPrecision().equals(this.getPrecision()) &amp;&amp; super.isEqual(other, ignoreStorageClass, ignoreConst, ignoreVolatile));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isEqual(Type other, boolean ignoreStorageClass, boolean ignoreConst, boolean ignoreVolatile);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _xblockexpression = null;\n{\n\tboolean _eIsProxy = this.eIsProxy();\n\tif (_eIsProxy)\n\t{\n\t\treturn super.toString();\n\t}\n\t&lt;%java.lang.String%&gt; _string = super.toString();\n\t&lt;%java.lang.String%&gt; _switchResult = null;\n\t&lt;%gecos.types.FloatPrecisions%&gt; _precision = this.getPrecision();\n\tif (_precision != null)\n\t{\n\t\tswitch (_precision)\n\t\t{\n\t\t\tcase HALF:\n\t\t\t\t_switchResult = \" half float\";\n\t\t\t\tbreak;\n\t\t\tcase SINGLE:\n\t\t\t\t_switchResult = \" float\";\n\t\t\t\tbreak;\n\t\t\tcase DOUBLE:\n\t\t\t\t_switchResult = \" double\";\n\t\t\t\tbreak;\n\t\t\tcase LONG_DOUBLE:\n\t\t\t\t_switchResult = \" long double\";\n\t\t\t\tbreak;\n\t\t\tdefault:\n\t\t\t\tbreak;\n\t\t}\n\t}\n\t_xblockexpression = (_string + _switchResult);\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	String toString();

} // FloatType
