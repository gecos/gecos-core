/**
 */
package gecos.instrs.impl;

import fr.irisa.cairn.gecos.model.factory.conversion.ConversionMatrix;

import gecos.instrs.GenericInstruction;
import gecos.instrs.InstrsPackage;
import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;

import gecos.types.Type;

import java.lang.CharSequence;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.ExclusiveRange;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Generic Instruction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.impl.GenericInstructionImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GenericInstructionImpl extends ChildrenListInstructionImpl implements GenericInstruction {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GenericInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstrsPackage.eINSTANCE.getGenericInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InstrsPackage.GENERIC_INSTRUCTION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instruction> getOperands() {
		return this.getChildren();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final InstrsVisitor visitor) {
		visitor.visitGenericInstruction(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		try {
			final StringBuffer res = new StringBuffer();
			String _name = this.getName();
			boolean _tripleEquals = (_name == null);
			if (_tripleEquals) {
				res.append("?");
			}
			else {
				res.append(this.getName());
			}
			res.append("(");
			final Function1<Instruction, CharSequence> _function = new Function1<Instruction, CharSequence>() {
				public CharSequence apply(final Instruction it) {
					String _elvis = null;
					String _string = null;
					if (it!=null) {
						_string=it.toString();
					}
					if (_string != null) {
						_elvis = _string;
					} else {
						_elvis = "?";
					}
					return _elvis;
				}
			};
			res.append(IterableExtensions.<Instruction>join(this.getOperands(), ",", _function));
			res.append(")");
			return res.toString();
		}
		catch (final Throwable _t) {
			if (_t instanceof Exception) {
				final Exception e = (Exception)_t;
				String _message = e.getMessage();
				return ("NPE " + _message);
			}
			else {
				throw Exceptions.sneakyThrow(_t);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getOperand(final int i) {
		EList<Instruction> _operands = this.getOperands();
		boolean _tripleNotEquals = (_operands != null);
		if (_tripleNotEquals) {
			return this.getOperands().get(i);
		}
		throw new RuntimeException("Null Operand List for generic inst");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addOperand(final Instruction inst) {
		this.getOperands().add(inst);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isNamed(final String name) {
		return this.getName().equals(name);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type computeType() {
		boolean _isNullOrEmpty = IterableExtensions.isNullOrEmpty(this.getOperands());
		boolean _not = (!_isNullOrEmpty);
		if (_not) {
			Type res = this.getChild(0).getType();
			int _childrenCount = this.getChildrenCount();
			ExclusiveRange _doubleDotLessThan = new ExclusiveRange(1, _childrenCount, true);
			for (final Integer i : _doubleDotLessThan) {
				res = ConversionMatrix.getCommon(this.getChild((i).intValue()).getType(), res);
			}
			return res;
		}
		return this.getType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSame(final Instruction instr) {
		if ((instr instanceof GenericInstruction)) {
			boolean isSameName = false;
			if (((((GenericInstruction)instr).getName() == null) && (this.getName() == null))) {
				isSameName = true;
			}
			else {
				String _name = ((GenericInstruction)instr).getName();
				boolean _tripleEquals = (_name == null);
				String _name_1 = this.getName();
				boolean _tripleEquals_1 = (_name_1 == null);
				boolean _xor = (_tripleEquals ^ _tripleEquals_1);
				if (_xor) {
					return false;
				}
				else {
					isSameName = ((GenericInstruction)instr).getName().equals(this.getName());
				}
			}
			return (super.isSame(instr) && isSameName);
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InstrsPackage.GENERIC_INSTRUCTION__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InstrsPackage.GENERIC_INSTRUCTION__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InstrsPackage.GENERIC_INSTRUCTION__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InstrsPackage.GENERIC_INSTRUCTION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

} //GenericInstructionImpl
