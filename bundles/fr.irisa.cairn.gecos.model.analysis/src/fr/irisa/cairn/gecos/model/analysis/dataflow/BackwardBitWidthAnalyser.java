/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.analysis.dataflow;


import java.util.LinkedHashMap;
import java.util.Map;

import fr.irisa.cairn.gecos.model.analysis.dataflow.framework.BitString;
import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import gecos.core.Procedure;
import gecos.instrs.CallInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.RangeInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;

public class BackwardBitWidthAnalyser extends BlockInstructionSwitch<Object> {

	private Procedure proc;
	private Map<String, BitString> symbols;
	private boolean changed;

	public BackwardBitWidthAnalyser(Procedure proc) {
		this.proc = proc;
		this.symbols = new LinkedHashMap<String, BitString>();
	}

	public boolean computeIterative() {
		changed = false;
		doSwitch(proc.getBody());
		return changed;
	}

	@Override
	public Object caseGenericInstruction(GenericInstruction g) {
		if (g.getName().equals("add") || g.getName().equals("sub")) {
			changed |= dontCareOfMostDC(g,0);
			changed |= dontCareOfMostDC(g,1);
		} else {
			BitString bS0 = (BitString) g.getOperand(0).getAnnotation("BitString");
			BitString bS1 = (BitString) g.getOperand(1).getAnnotation("BitString");
			if (g.getName().equals("and")) {
				BitString tmp = bS0.dontCareOfZERO(bS1).dontCareOfDC(bS0);
				changed |= setBitString(g.getOperand(1),bS1.dontCareOfZERO(bS0).dontCareOfDC(bS0));
				changed |= setBitString(g.getOperand(0),tmp);

			} else if (g.getName().equals("or")) {
				BitString tmp = bS0.dontCareOfONE(bS1).dontCareOfDC(bS0);
				changed |= setBitString(g.getOperand(1),bS1.dontCareOfONE(bS0).dontCareOfDC(bS0));
				changed |= setBitString(g.getOperand(0),tmp);

			} else if (g.getName().equals("xor")) {
				System.out
						.println("*** backward bitwidth analyser: what to do for xor ?");
			} else if (g.getName().equals("shr")) {
				changed |= setBitString(g.getOperand(0),bS0.inverseAsr(bS1,bS0.size()));			} else if (g.getName().equals("shl")) {
				changed |= setBitString(g.getOperand(0),bS0.inverseLsl(bS1,bS0.size()));
			} else if (g.getName().equals("ne")) {
				BitString tmp = bS0.dontCareOfEqual(
						bS1);
				changed |= setBitString(g.getOperand(1),bS1.dontCareOfEqual(bS0));
				changed |= setBitString(g.getOperand(0),tmp);
			} else if (g.getName().equals("lt") || g.getName().equals("le")
					|| g.getName().equals("gt") || g.getName().equals("ge")
					|| g.getName().equals("eq")) {
				// nothing to do

			} else if (g.getName().equals("not")) {
				System.out
						.println("*** backward bitwidth analyser: what to do for not ?");
			} else if (g.getName().equals("mul")) {
				System.out
						.println("*** backward bitwidth analyser: what to do for mul ?");
			} else if (g.getName().equals("str")) {
				// same as set
				changed |= setBitString(g.getOperand(1),
						bS1.dontCareOfDC(
								bS0));
			} else if (g.getName().equals("array")) {
				// nothing to do
			} else
				throw new RuntimeException("generic " + g);
		}
		return super.caseGenericInstruction(g);
		
	}

	private boolean dontCareOfMostDC(GenericInstruction g, int pos) {
		//setBitString(g.getOperand(pos),getBitString(g).dontCareOfMostDC(getBitString(g)));
		throw new UnsupportedOperationException();
	}

	private boolean setBitString(Instruction operand, BitString dontCareOfMostDC) {
		throw new UnsupportedOperationException();
	}

	BitString getBitString(GenericInstruction g) {
		throw new UnsupportedOperationException();
	}

	// @Override public Object casePhy(PhyInstruction g) {
	// for (int i = 0; i < g.childCount(); ++i)
	// changed |= g.getOperand(i).setBitString(
	// g.getOperand(i).getAnnotationFromName("BitString").dontCareOfDC(getBitString(g)));
	// super.visitGeneric(g, arg);
	// }

	@Override
	public Object caseCallInstruction(CallInstruction c) {
		return super.caseGenericInstruction((GenericInstruction) c);
	}

	@Override
	public Object caseConvertInstruction(ConvertInstruction c) {
		// same as and
		changed |= setBitString(c.getExpr(),
				((BitString) c.getExpr().getAnnotation("BitString")).dontCareOfZERO(
						BitString.fromInt((1 << c.getType().getSize()) - 1))
						.dontCareOfDC((BitString) c.getAnnotation("BitString")));
		doSwitch(c.getExpr());
		return null;

	}

	@Override
	public Object caseSetInstruction(SetInstruction s) {
		changed |= setBitString(s.getSource(),
				((BitString) s.getSource().getAnnotation("BitString")).dontCareOfDC(
						(BitString) s.getDest().getAnnotation("BitString")));
		doSwitch(s.getDest());
		doSwitch(s.getSource());
		return null;
	}

	@Override
	public Object caseSymbolInstruction(SymbolInstruction s) {
		BitString str;
		if ((str = symbols.get(s.toString())) == null)
			str = (BitString) s.getAnnotation("BitString");
		else
			str = str.interWith((BitString) s.getAnnotation("BitString"));
		// changed |= s.setBitString(str);
		symbols.put(s.toString(), str);
		return null;
	}

	@Override
	public Object caseRangeInstruction(RangeInstruction r) {
		return r;
		// r.expr().setBitString(getBitString(g).inverseAsr(
		// new BitString(r.getOperand(1).low())));
	}

	public void debug() {
		for (String name : symbols.keySet()) {
			BitString str = symbols.get(name);
			System.out.println(name + " -> " + str);
		}
	}
}
