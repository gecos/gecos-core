package fr.irisa.cairn.gecos.model.tools.utils;

/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ControlEdge;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.SwitchBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.BranchType;

import java.io.PrintStream;
import java.util.Iterator;


public class GecosModelDotExport extends BlocksDefaultSwitch<Object> {

	private static final int PRINT_NODES  = 0;
	private static final int PRINT_EDGES  = 1;
	private static final int PRINT_SIMPLE = 2;

	private int state;
	private PrintStream out;
	private boolean madeCompound;

	public GecosModelDotExport(PrintStream out) {
		this.out = out;
	}

	public void output(Block b, boolean simple) {
		madeCompound = false;
		out.println("digraph G {");
		out.println("node [shape=box];");
		state = simple ? PRINT_SIMPLE : PRINT_NODES;
		doSwitch(b);
		state = PRINT_EDGES;
		doSwitch(b);
		out.println("}");
	}

	public void visitBasic(BasicBlock b, Object arg) {
		boolean isSimple = isSimple(b);
		if (state == PRINT_NODES || state == PRINT_SIMPLE) {
			if (! madeCompound && ! isSimple) {
				madeCompound = true;
				out.println("compound=true;");
			}
			outputBasic(b, isSimple);
		} else {
			for (ControlEdge edge  :b.getOutEdges()) {
				if (isSimple) {
					out.print("n"+edge.getFrom().getNumber()+" -> n"
							+edge.getTo().getNumber());
					if (edge.getCond() != BranchType.UNCONDITIONAL)
						out.print("[label=\"J\"]");
					out.println(";");
				} else {
					out.print("n"+edge.getFrom().getNumber()+" -> n"+edge.getTo().getNumber()
							+" [ltail=cluster"+edge.getFrom().getNumber()
							+",lhead=cluster"+edge.getTo().getNumber());
					if (edge.getCond() != BranchType.UNCONDITIONAL)
						out.print(",label=\"J\"");
					out.println("];");
				}
			}
		}
	}
	
	private boolean isSimple(BasicBlock b) {
		boolean isSimple = b.getInstructions().size() == 0;
		for (int i = 0; i < b.getInstructions().size(); ++i) {
//			Instruction inst = (Instruction) b.getInstructions().get(i);
//			if (! (inst instanceof IDotPrintable))
//				isSimple = true;
		}
		return isSimple;
	}

	private void outputBasic(BasicBlock b, boolean isSimple) {
		if (isSimple)
			out.println("n"+b.getNumber()+" [label=\""+b
					+"\\n"+b.contentString()+"\"];");
		else {
			out.println("subgraph cluster"+b.getNumber()+" {");
			out.println("n" + b.getNumber()+"[shape=circle,label=\""+b.getNumber()+"\"];");
			out.println("style=solid; color=black;");
			for (int i = 0; i < b.getInstructions().size(); ++i) {
				out.append(b.getInstructions().get(i).toString());
				//IDotPrintable printable = (IDotPrintable) b.getInstruction(i);
				//printable.printOn(out, uniq++);
			}
			out.println("}");
		}
	}

	public void visitComposite(CompositeBlock b, Object arg) {
		if (state == PRINT_NODES) {
			out.println("subgraph cluster"+b.getNumber()+" {");
			out.println("style=dashed;");
			out.println("color=black;");
			out.println("label=\"B"+b.getNumber()
					+scopeString(b.getScope())+"\";");
		}
		for (Block child : b.getChildren()) {
			doSwitch(child);
		}
		if (state == PRINT_NODES)
			out.println("}");
	}

	public void visitIf(IfBlock b, Object arg) {
		if (state == PRINT_NODES) {
			out.println("subgraph cluster"+b.getNumber()+" {");
			out.println("style=solid;");
			out.println("color=blue;");
			out.println("label=\"B"+b.getNumber()+"\";");
		}
		doSwitch(b.getTestBlock());
		if (b.getThenBlock() != null)
			doSwitch(b.getThenBlock());
		if (b.getElseBlock() != null)
			doSwitch(b.getElseBlock());
		if (state == PRINT_NODES)
			out.println("}");
	}
	//(b.get\w*)\(\)\Q.visit(this, arg)\E;
	//doSwitch($1);
	
	public void visitLoop(DoWhileBlock b, Object arg) {
		if (state == PRINT_NODES) {
			out.println("subgraph cluster"+b.getNumber()+" {");
			out.println("style=solid;");
			out.println("color=red;");
			out.println("label=\"B"+b.getNumber()+"\";");
		}
		doSwitch(b.getTestBlock());
		if (b.getBodyBlock() != null) doSwitch(b.getBodyBlock());
		if (state == PRINT_NODES)
			out.println("}");
	}
	//b.get[(\w\w)]*.visit\(this\, arg\)\;
	public void visitWhile(WhileBlock b, Object arg) {
		if (state == PRINT_NODES) {
			out.println("subgraph cluster"+b.getNumber()+" {");
			out.println("style=solid;");
			out.println("color=purple;");
			out.println("label=\"B"+b.getNumber()+"\";");
		}
		doSwitch(b.getTestBlock());
		if (b.getBodyBlock() != null) doSwitch(b.getBodyBlock());
		if (state == PRINT_NODES)
			out.println("}");
	}

	public void visitFor(ForBlock b, Object arg) {
		if (state == PRINT_NODES) {
			out.println("subgraph cluster"+b.getNumber()+" {");
			out.println("style=solid;");
			out.println("color=green;");
			out.println("label=\"B"+b.getNumber()+"\";");
		}
		doSwitch(b.getTestBlock());
		doSwitch(b.getBodyBlock());
		doSwitch(b.getStepBlock());
		doSwitch(b.getInitBlock());
		if (state == PRINT_NODES)
			out.println("}");
	}
	
	public void visitSwitch(SwitchBlock b, Object arg) {
		if (state == PRINT_NODES) {
			out.println("subgraph cluster"+b.getNumber()+" {");
			out.println("style=solid;");
			out.println("color=orange;");
			out.println("label=\"B"+b.getNumber()+"\";");
		}
		doSwitch(b.getDispatchBlock());
		doSwitch(b.getBodyBlock());	
		if (state == PRINT_NODES)
			out.println("}");
	}
	
	private String scopeString(Scope scope) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("(");
		Iterator<Symbol> i = scope.getSymbols().iterator();
		while (i.hasNext()) {
			Symbol sym = i.next();
			buffer.append(sym.getName());
			if (i.hasNext())
				buffer.append(",");
		}
		buffer.append(")");
		return buffer.toString();
	}
	
}
