package fr.irisa.cairn.gecos.model.analysis.profiling;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDefaultVisitor;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.core.Procedure;
import gecos.instrs.CallInstruction;


public class CallFinder extends GecosBlocksInstructionsDefaultVisitor{
	
	private List<CallInstruction> calls;
	private BasicBlock currentBasic;
	
	private Procedure proc;
	private Block block;
	

	
	public CallFinder(Procedure proc) {
		this.proc = proc;
	}

	public CallFinder() {
	}
 
	public CallFinder(Block block) {
		this.block = block;
	}
	
	public List<CallInstruction> compute(){
		this.calls = new ArrayList<CallInstruction>();
		if (proc != null)
			proc.getBody().accept(this);
		else if (block != null)
			block.accept(this);
		return calls;
	}
	
	@Override
	public void visitCallInstruction(CallInstruction inst) {
		calls.add(inst);
		super.visitCallInstruction(inst);
	}

}

