package fr.irisa.cairn.gecos.model.tools.queries;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.core.ProcedureSet;
import gecos.instrs.Instruction;
import gecos.instrs.LabelInstruction;

/**
 * Utility class to fin Labeled object (Block or instructions) in the Gecos IR
 * @author sderrien
 *
 */
public class FindLabel extends BlockInstructionSwitch<LabelInstruction>{
	ProcedureSet ps;
	String name;
	
	public FindLabel() {
	}

	public LabelInstruction findLabel(ProcedureSet ps,String name) {
		this.name=name;
		return doSwitch(ps);
	}

	public Block findLabeleledBlock(ProcedureSet ps,String name) {
		this.name=name;
		LabelInstruction lblInstr =  doSwitch(ps);
		if(lblInstr.getRoot()!=lblInstr) 
			throw new UnsupportedOperationException("Unsupported case");
		BasicBlock basicBlock = lblInstr.getBasicBlock();
		if(basicBlock!=null) {
			if(basicBlock.getLastInstruction()==lblInstr) {
				int offset = basicBlock.getInstructions().indexOf(lblInstr);
				if(basicBlock.getParent() instanceof CompositeBlock) {
					CompositeBlock cb = (CompositeBlock) basicBlock.getParent();
					EList<Block> children = cb.getChildren();
					offset = children.indexOf(basicBlock);
					if(offset==children.size()) throw new UnsupportedOperationException("Unsupported case : ");
					return children.get(offset+1);
				} else {
					throw new UnsupportedOperationException("Unsupported case : ");
				}
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public Instruction findLabeleledInstr(ProcedureSet ps,String name) {
		this.name=name;
		LabelInstruction lblInstr =  doSwitch(ps);
		if(lblInstr.getRoot()!=lblInstr) 
			throw new UnsupportedOperationException("Unsupported case");
		BasicBlock basicBlock = lblInstr.getBasicBlock();
		if(basicBlock!=null) {
			if(basicBlock.getLastInstruction()==lblInstr) {
				EList<Instruction> instructions = basicBlock.getInstructions();
				int offset = instructions.indexOf(lblInstr);
				if(offset==instructions.size()) {
					throw new UnsupportedOperationException("Unsupported case : ");
				}
				return instructions.get(offset+1);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	@Override
	public LabelInstruction caseLabelInstruction(LabelInstruction object) {
		if(object.getName().equals(name)) {
			return object;
		}
		return null;
	}
	
	
}
