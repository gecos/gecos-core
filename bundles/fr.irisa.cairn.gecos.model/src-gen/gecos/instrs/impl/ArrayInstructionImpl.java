/**
 */
package gecos.instrs.impl;

import gecos.instrs.ArrayInstruction;
import gecos.instrs.InstrsPackage;
import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;

import gecos.types.Type;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Array Instruction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.impl.ArrayInstructionImpl#getDest <em>Dest</em>}</li>
 *   <li>{@link gecos.instrs.impl.ArrayInstructionImpl#getIndex <em>Index</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ArrayInstructionImpl extends ComponentInstructionImpl implements ArrayInstruction {
	/**
	 * The cached value of the '{@link #getDest() <em>Dest</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDest()
	 * @generated
	 * @ordered
	 */
	protected Instruction dest;

	/**
	 * The cached value of the '{@link #getIndex() <em>Index</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndex()
	 * @generated
	 * @ordered
	 */
	protected EList<Instruction> index;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArrayInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstrsPackage.eINSTANCE.getArrayInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getDest() {
		return dest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDest(Instruction newDest, NotificationChain msgs) {
		Instruction oldDest = dest;
		dest = newDest;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, InstrsPackage.ARRAY_INSTRUCTION__DEST, oldDest, newDest);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDest(Instruction newDest) {
		if (newDest != dest) {
			NotificationChain msgs = null;
			if (dest != null)
				msgs = ((InternalEObject)dest).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - InstrsPackage.ARRAY_INSTRUCTION__DEST, null, msgs);
			if (newDest != null)
				msgs = ((InternalEObject)newDest).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - InstrsPackage.ARRAY_INSTRUCTION__DEST, null, msgs);
			msgs = basicSetDest(newDest, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InstrsPackage.ARRAY_INSTRUCTION__DEST, newDest, newDest));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instruction> getIndex() {
		if (index == null) {
			index = new EObjectContainmentEList<Instruction>(Instruction.class, this, InstrsPackage.ARRAY_INSTRUCTION__INDEX);
		}
		return index;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instruction> listChildren() {
		EList<Instruction> _xblockexpression = null;
		{
			final BasicEList<Instruction> list = new BasicEList<Instruction>();
			list.add(this.getDest());
			list.addAll(this.getIndex());
			_xblockexpression = ECollections.<Instruction>unmodifiableEList(list);
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addIndex(final Instruction idx) {
		this.getIndex().add(idx);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addIndexAt(final int i, final Instruction idx) {
		this.getIndex().add(i, idx);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final InstrsVisitor visitor) {
		visitor.visitArrayInstruction(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type computeType() {
		return super.computeType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		boolean sym = true;
		final StringBuffer sb = new StringBuffer();
		EList<Instruction> _listChildren = this.listChildren();
		for (final Instruction i : _listChildren) {
			if (sym) {
				sb.append(i);
				sym = false;
			}
			else {
				if ((i != null)) {
					sb.append("[").append(i).append("]");
				}
				else {
					sb.append("[null]");
				}
			}
		}
		return sb.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSame(final Instruction instr) {
		if ((instr instanceof ArrayInstruction)) {
			return super.isSame(instr);
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InstrsPackage.ARRAY_INSTRUCTION__DEST:
				return basicSetDest(null, msgs);
			case InstrsPackage.ARRAY_INSTRUCTION__INDEX:
				return ((InternalEList<?>)getIndex()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InstrsPackage.ARRAY_INSTRUCTION__DEST:
				return getDest();
			case InstrsPackage.ARRAY_INSTRUCTION__INDEX:
				return getIndex();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InstrsPackage.ARRAY_INSTRUCTION__DEST:
				setDest((Instruction)newValue);
				return;
			case InstrsPackage.ARRAY_INSTRUCTION__INDEX:
				getIndex().clear();
				getIndex().addAll((Collection<? extends Instruction>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InstrsPackage.ARRAY_INSTRUCTION__DEST:
				setDest((Instruction)null);
				return;
			case InstrsPackage.ARRAY_INSTRUCTION__INDEX:
				getIndex().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InstrsPackage.ARRAY_INSTRUCTION__DEST:
				return dest != null;
			case InstrsPackage.ARRAY_INSTRUCTION__INDEX:
				return index != null && !index.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ArrayInstructionImpl
