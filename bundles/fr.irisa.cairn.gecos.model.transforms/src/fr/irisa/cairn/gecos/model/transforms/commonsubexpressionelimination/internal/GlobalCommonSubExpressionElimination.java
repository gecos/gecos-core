package fr.irisa.cairn.gecos.model.transforms.commonsubexpressionelimination.internal;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import fr.irisa.cairn.gecos.model.analysis.instructions.comparator.InstructionComparator;
import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.ForC99Block;
import gecos.blocks.IfBlock;
import gecos.blocks.SimpleForBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.GenericInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.IntegerType;
import gecos.types.Type;

/**
 * @author Imen Fassi
 * @date 25/04/2016
 */

/**
 * Input : For each basic block compute the set of expressions that are
 * available on its entry i.e. AEin(i)
 *
 */
/**
 * @author imen
 *
 */
public class GlobalCommonSubExpressionElimination {
	private ArrayList<ArrayList<Instruction>> AEin;
	private ArrayList<BasicBlock> BB;
	ArrayList<Integer> BBdepth;
	private int counter;

	private int incr_counter() {
		counter++;
		return counter;
	}

	public GlobalCommonSubExpressionElimination(ArrayList<ArrayList<Instruction>> aein, ArrayList<BasicBlock> bb,
			ArrayList<Integer> bbd) {
		this.AEin = aein;
		this.BBdepth = bbd;
		this.BB = bb;
		/*
		 * System.out.println(
		 * "---------------------------------------------------------------");
		 * int i = 0; for (ArrayList<Instruction> in : AEin) {
		 * System.out.println("Computed AEin [" + BB.get(i).getNumber() + "] : "
		 * + in); i++; }
		 */
		counter = 0;
	}

	private boolean compareAsString(Instruction g1, Instruction g2) {
		String s1 = InstructionComparator.instToString(g1);
		String s2 = InstructionComparator.instToString(g2);
		return (s1.equals(s2));
	}
	
	
	private ArrayList<Instruction> getDestinationOrListOfPointers (Instruction instAux) {
		ArrayList<Instruction> listInstr = new ArrayList<Instruction>();

		if ((instAux instanceof SetInstruction)) { 
			Instruction des = ((SetInstruction) instAux).getDest();
			
			/**
			 * useful for cases like (*p15)[0] or *(p16[0]) -> need to get the
			 * name of the pointer
			 */
			while (des instanceof IndirInstruction)
				des = ((IndirInstruction) des).getAddress();

			while (des instanceof ArrayInstruction)
				des = ((ArrayInstruction) des).getDest();

			while (des instanceof IndirInstruction)
				des = ((IndirInstruction) des).getAddress();
			
			/**
			 * useful for cases like (1+p14)[0]
			 */

			ArrayList<SymbolInstruction> listSymbolsGI = null;

			if (des instanceof GenericInstruction) {
				SymbolsCounter countSymbols = new SymbolsCounter();
				des.accept(countSymbols);
				listSymbolsGI = countSymbols.getlistSymbsInst();
			} else {
				listSymbolsGI = new ArrayList<SymbolInstruction> ();
				if (des instanceof SymbolInstruction)
					listSymbolsGI.add((SymbolInstruction)des);
			}
			for (Instruction st2 : listSymbolsGI) {
				listInstr.add(st2);
				if (st2 instanceof SymbolInstruction) {
					Symbol symbl = ((SymbolInstruction) st2).getSymbol();
					if (symbl.getValue() != null)
						listInstr.add(symbl.getValue());
				}
			}
		} else {
			if ((instAux instanceof CallInstruction))

			for (Instruction st : ((CallInstruction) instAux).getArgs()) {
				
				/**
				 * useful for cases like (*p15)[0] or *(p16[0]) -> need to get the
				 * name of the pointer
				 */
				while (st instanceof IndirInstruction)
					st = ((IndirInstruction) st).getAddress();

				while (st instanceof ArrayInstruction)
					st = ((ArrayInstruction) st).getDest();

				while (st instanceof IndirInstruction)
					st = ((IndirInstruction) st).getAddress();
														
				/**
				 * useful for cases like (1+p14)[0]
				 */

				ArrayList<SymbolInstruction> listSymbolsGI = null;

				if (st instanceof GenericInstruction) {
					SymbolsCounter countSymbols = new SymbolsCounter();
					st.accept(countSymbols);
					listSymbolsGI = countSymbols.getlistSymbsInst();
				} else {
					listSymbolsGI = new ArrayList<SymbolInstruction> ();
					if (st instanceof SymbolInstruction)
						listSymbolsGI.add((SymbolInstruction)st);
				}

				for (Instruction st2 : listSymbolsGI) {
					listInstr.add(st2);
					if (st2 instanceof SymbolInstruction) {
						Symbol symbl = ((SymbolInstruction) st2).getSymbol();
						if (symbl.getValue() != null)
							listInstr.add(symbl.getValue());
					}
				}
			}
		}
		return listInstr;
	}

	public void Global_CSE() {
		int index = 0;
		for (BasicBlock b : BB) {
			for (Instruction aexp : AEin.get(index)) {
				int j = 0;
				int indexArg = 0;
				for (Instruction instrc : b.getInstructions()) {
					List<Instruction> listArgs = new ArrayList<Instruction>();

					if (instrc instanceof CallInstruction) {
						CallInstruction instPos = (CallInstruction) instrc;
						listArgs.addAll(instPos.getArgs());
					} else {
						if ((instrc instanceof SetInstruction)) {
							SetInstruction setInst = (SetInstruction) instrc;
							listArgs.add(setInst.getSource());
						} else {
							if ((instrc instanceof GenericInstruction)) {
								listArgs.add(instrc);
							}
						}
					}
					for (Instruction instr : listArgs) {

						if ((instr instanceof GenericInstruction) && ((aexp instanceof GenericInstruction))) {
							GenericInstruction gi = (GenericInstruction) instr;
							GenericInstruction agi = (GenericInstruction) aexp;
							// look for the first evaluation of the expression
							if (gi.getName().equals(agi.getName()) && (commutative(agi.getName())
									&& compareAsString(gi.getOperand(0), agi.getOperand(1))
									&& compareAsString(gi.getOperand(1), agi.getOperand(0))
									|| (compareAsString(gi.getOperand(0), agi.getOperand(0))
											&& compareAsString(gi.getOperand(1), agi.getOperand(1))))) {
								
								
								/**
								 * is the operands initialized at the beginning
								 * of the Basic Block
								 */
								
								boolean ctnue = false;

								if (b.getParent() instanceof CompositeBlock) {
									Scope scp = b.getParent().getScope();
									if (scp != null) {
										for (Symbol sbl : scp.getSymbols()) {
											if (sbl.getValue() != null) {
												ArrayList<Instruction> listInstr = new ArrayList<Instruction> ();												
												listInstr.add(GecosUserInstructionFactory.symbref(sbl));												
												if (operandsModified(listInstr, gi.getOperand(0), gi.getOperand(1), false)) {
													ctnue = true;
													break;
												} 
											}
										}
									}
								}
								
								if (ctnue)
									continue;
								
								/**
								 * Search backward from the first occurrence to
								 * see whether any of the operands have been
								 * previously assigned in the block
								 */
																
								for (int k = b.getInstructionCount()-1; k >= 0; k--) {
									if (k!=j) {
									Instruction instAux = b.getInstruction(k);
																		
									ArrayList<Instruction> listInstr = getDestinationOrListOfPointers(instAux);
									
									if (operandsModified(listInstr, gi.getOperand(0), gi.getOperand(1), instAux instanceof CallInstruction)) {
										ctnue = true;
										break;
									} 
								}
								} // for k
								if (ctnue)
									continue;
								// find sources : the result is a list of
								// (index_BB,
								// index_Of_Instruction_Inside_The_BB)

								ArrayList<int[]> S = FindSources(aexp, index - 1);

								if (!S.isEmpty()) {
									/*
									 * System.out.println(
									 * "Result OF FindSources : "); for (int[]
									 * aux : S) System.out.println("(" + aux[0]
									 * + ", " + aux[1] + ")");
									 */
									// Create new temporary variables
									Scope scope;
									if (b.getContainingProcedure() == null) {
										System.out.println(
												"BB " + b.getNumber() + " b.getContainingProcedure() is null !!!!");
										scope = b.getScope();
									} else
										scope = b.getContainingProcedure().getBody().getChildren().get(1).getScope();
									String name = new_temp(scope);

									// Replace the expression by the temp
									Type _type = gi.getType();
									if (instrc instanceof SetInstruction)
										_type = ((SetInstruction) instrc).getDest().getType();
									else {
										if (instrc instanceof CallInstruction) {
											CallInstruction callinstr = (CallInstruction) instrc;
											Instruction instr2 = callinstr.getArgs().get(indexArg);
											_type = instr2.getType();
										}
									}
									TypeAnalyzer type_var = new TypeAnalyzer(_type);
									if (type_var.getBaseLevel().isInt()) {
										IntegerType base = type_var.getBaseLevel().getBaseAsInt();
										if (!base.getSigned() && base.getSize() == 1)
											_type = GecosUserTypeFactory.INT();
									}
									Symbol symb = GecosUserCoreFactory.symbol(name, _type, scope);
									/**
									 * Create the SymbolInstruction
									 */
									Instruction symbInstrTMP = GecosUserInstructionFactory.symbref(symb);
									/**
									 * replace gi by the SymbolInstruction
									 */
									gi.getParent().replaceChild(gi, symbInstrTMP);

									for (int[] s : S) {
										BasicBlock bbs = BB.get(s[0]);
										Instruction is = bbs.getInstruction(s[1]);
										if (is instanceof SetInstruction) {
											Instruction l = ((SetInstruction) is).getDest();
											((SetInstruction) is).setDest(symbInstrTMP.copy());

											// insert l <- temp
											/**
											 * Create a new SetInstruction
											 */
											Instruction setInstTEMP = GecosUserInstructionFactory.set(l.copy(), symbInstrTMP.copy());
											insertInstructionInBasicBlockAtPosition(setInstTEMP, bbs, s[1] + 1);
										}
									}
								} // if S not empty
							} // if instr found
						} // if GenericInstruction
						indexArg++;
					} // for list arg
					j++;
				} // for Instructions
			} // for AEin
			index++;
		} // for BB
	}

	private void insertInstructionInBasicBlockAtPosition(Instruction setInstTEMP, BasicBlock b, int i) {
		if (!is_BB_From_TestBlock(b, setInstTEMP))
			b.getInstructions().add(i, setInstTEMP);
	}

	private boolean is_BB_From_TestBlock(BasicBlock b, Instruction setInstTEMP) {
		Block parent = b.getParent();
		if ((parent instanceof IfBlock) && (((IfBlock) parent).getTestBlock() == b)) {
			Block bt = ((IfBlock) parent).getThenBlock();
			BasicBlock newb = GecosUserBlockFactory.BBlock(setInstTEMP);
			if (bt instanceof CompositeBlock) {
				CompositeBlock cb = (CompositeBlock) bt;
				cb.addBlockBefore(newb, cb.getChildren().get(0));
			} else {
				CompositeBlock ncb = GecosUserBlockFactory.CompositeBlock(newb, bt);
				((IfBlock) parent).setThenBlock(ncb);
			}
			Block be = ((IfBlock) parent).getElseBlock();
			if (be != null) {
				newb = GecosUserBlockFactory.BBlock(setInstTEMP);
				if (be instanceof CompositeBlock) {
					CompositeBlock cb = (CompositeBlock) be;
					cb.addBlockBefore(newb, cb.getChildren().get(0));
				} else {
					CompositeBlock ncb = GecosUserBlockFactory.CompositeBlock(newb, be);
					((IfBlock) parent).setElseBlock(ncb);
				}
			}
			return true;
		}
		if ((parent instanceof ForBlock) && (((ForBlock) parent).getInitBlock() == b
				|| ((ForBlock) parent).getTestBlock() == b || ((ForBlock) parent).getStepBlock() == b)) {
			Block body = ((ForBlock) parent).getBodyBlock();
			BasicBlock newb = GecosUserBlockFactory.BBlock(setInstTEMP);
			if (body instanceof CompositeBlock) {
				CompositeBlock cb = (CompositeBlock) body;
				cb.addBlockBefore(newb, cb.getChildren().get(0));
			} else {
				CompositeBlock ncb = GecosUserBlockFactory.CompositeBlock(newb, body);
				((ForBlock) parent).setBodyBlock(ncb);
			}
			return true;
		}
		if ((parent instanceof SimpleForBlock) && (((SimpleForBlock) parent).getInitBlock() == b
				|| ((SimpleForBlock) parent).getTestBlock() == b || ((SimpleForBlock) parent).getStepBlock() == b)) {
			Block body = ((SimpleForBlock) parent).getBodyBlock();
			BasicBlock newb = GecosUserBlockFactory.BBlock(setInstTEMP);
			if (body instanceof CompositeBlock) {
				CompositeBlock cb = (CompositeBlock) body;
				cb.addBlockBefore(newb, cb.getChildren().get(0));
			} else {
				CompositeBlock ncb = GecosUserBlockFactory.CompositeBlock(newb, body);
				((SimpleForBlock) parent).setBodyBlock(ncb);
			}
			return true;
		}
		if ((parent instanceof ForC99Block) && (((ForC99Block) parent).getInitBlock() == b
				|| ((ForC99Block) parent).getTestBlock() == b || ((ForC99Block) parent).getStepBlock() == b)) {
			Block body = ((ForC99Block) parent).getBodyBlock();
			BasicBlock newb = GecosUserBlockFactory.BBlock(setInstTEMP);
			if (body instanceof CompositeBlock) {
				CompositeBlock cb = (CompositeBlock) body;
				cb.addBlockBefore(newb, cb.getChildren().get(0));
			} else {
				CompositeBlock ncb = GecosUserBlockFactory.CompositeBlock(newb, body);
				((ForC99Block) parent).setBodyBlock(ncb);
			}
			return true;
		}
		if ((parent instanceof DoWhileBlock) && (((DoWhileBlock) parent).getTestBlock() == b)) {
			Block body = ((DoWhileBlock) parent).getBodyBlock();
			BasicBlock newb = GecosUserBlockFactory.BBlock(setInstTEMP);
			if (body instanceof CompositeBlock) {
				CompositeBlock cb = (CompositeBlock) body;
				cb.addBlockBefore(newb, cb.getChildren().get(0));
			} else {
				CompositeBlock ncb = GecosUserBlockFactory.CompositeBlock(newb, body);
				((DoWhileBlock) parent).setBodyBlock(ncb);
			}
			return true;
		}
		if ((parent instanceof WhileBlock) && (((WhileBlock) parent).getTestBlock() == b)) {
			Block body = ((WhileBlock) parent).getBodyBlock();
			BasicBlock newb = GecosUserBlockFactory.BBlock(setInstTEMP);
			if (body instanceof CompositeBlock) {
				CompositeBlock cb = (CompositeBlock) body;
				cb.addBlockBefore(newb, cb.getChildren().get(0));
			} else {
				CompositeBlock ncb = GecosUserBlockFactory.CompositeBlock(newb, body);
				((WhileBlock) parent).setBodyBlock(ncb);
			}
			return true;
		}
		return false;
	}

	private boolean strContainsAsToken(String str1, String str2) {

		StringTokenizer st = new StringTokenizer(str1, " ,()/*-+<>=!&|");
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (token.equals(str2))
				return true;
		}
		return false;
	}

	// (index_BB, index_Of_Instruction_Inside_The_BB)
	private ArrayList<int[]> FindSources(Instruction aexp, int idx) {

		ArrayList<int[]> Res = new ArrayList<int[]>();

		for (int i = 0; i <= idx; i++) {
			BasicBlock b = BB.get(i);
			int j = 0;
			int nbInstr = b.getInstructions().size();
			for (Instruction instr : b.getInstructions()) {
				boolean analyse = true;
				if ((instr instanceof SetInstruction)) {
					SetInstruction setInst = (SetInstruction) instr;
					instr = setInst.getSource();
					String dest = InstructionComparator.instToString(setInst.getDest());
					String expr = InstructionComparator.instToString(instr);
					if (strContainsAsToken(expr, dest))
						analyse = false;
				}
				if (analyse)
					if (instr instanceof GenericInstruction) {
						GenericInstruction gi = (GenericInstruction) instr;
						GenericInstruction agi = (GenericInstruction) aexp;
						if (gi.getName().equals(agi.getName())
								&& (commutative(agi.getName()) && compareAsString(gi.getOperand(0), agi.getOperand(1))
										&& compareAsString(gi.getOperand(1), agi.getOperand(0))
										|| (compareAsString(gi.getOperand(0), agi.getOperand(0))
												&& compareAsString(gi.getOperand(1), agi.getOperand(1))))) {

							// verify if the operands of the expression are not
							// assigned after
							// is it in the EVAL set ?
							
							boolean add = true;
													
							for (int k = j + 1; k<nbInstr ; k++) {
								Instruction instAux = b.getInstruction(k);
																
								ArrayList<Instruction> listInstr = getDestinationOrListOfPointers (instAux);

								if (operandsModified(listInstr, gi.getOperand(0), gi.getOperand(1), instAux instanceof CallInstruction)) {
									add = false;
									break;
								} 
							} // for k
														
							if (!add)
							k_loop: for (int k = i + 1; k <= idx; k++) {
								BasicBlock b2 = BB.get(k);
								for (Instruction instAux : b2.getInstructions()) {
																		
									ArrayList<Instruction> listInstr = getDestinationOrListOfPointers(instAux);

									if (operandsModified(listInstr, gi.getOperand(0), gi.getOperand(1), instAux instanceof CallInstruction)) {
										add = false;
										break k_loop;
									} 
								}
							} // for k

							if (add) {
								if (BBdepth.get(i) <= BBdepth.get(idx + 1)) {									
									int t[] = new int[2];
									t[0] = i;
									t[1] = j;
									Res.add(t);
								}
							}
						}
					}
				j++;
			} // for instr
		}
		return Res;
	}
	
	
	
	

	static boolean operandsModified(ArrayList<Instruction> listInstr, Instruction opr, Instruction opr2, boolean fctCall) {
		String s1 = InstructionComparator.instToString(opr);
		String s2 = InstructionComparator.instToString(opr2);
		
		boolean oprdModified =  false;
		for (Instruction dest : listInstr) {
			
			String d = InstructionComparator.instToString(dest);
				if (dest instanceof SymbolInstruction) {
					TypeAnalyzer type_var = new TypeAnalyzer(((SymbolInstruction) dest).getSymbol().getType());
					if (type_var.isPointer()) {
					//	System.out.println("dest :" +	InstructionComparator.instToString (dest) + "----" );
						/**
						 * case 1 
						 *    *u + .....;
						 *    u = ...; 
						 *    *u + .....;
						Or
						 *
						 *    *u + .....;
						 *    f(u); 
						 *    *u + .....;
						 */
						String instr_str = "*(" + d + ")";
						if (s1.equals(instr_str) || s2.equals(instr_str))
							return true;
					} else {
						if (type_var.isArray()) {
							/**
							 * case 2 
							 *    u[] + .....; 
							 *    u = ... ; 
							 *    u[] + .....;
							 Or	
						 	 * 
							 *    u[] + .....; 
							 *    f(u); 
							 *    u[] + .....;
							 */
							String instr_str = d + "[";
							if ((s1.startsWith(instr_str) && s1.endsWith("]")) 
									|| (s2.startsWith(instr_str) && s2.endsWith("]")))
								return true;		
						}
					}
				} else {  
					/** case 3
					 * 		p = &u;
					 *      u + .....;
					 *      p = ..;
					 *      u + .....;
					 Or		
					 *
					 *      u + .....;
					 *      f(&u);
					 *      u + .....;
					 */	
						if (dest instanceof AddressInstruction) {
							 //System.out.println("dest :" + InstructionComparator.instToString (dest) + "----" +
							// InstructionComparator.instToString(((AddressInstruction)dest).getAddress()));

							String instr_str = InstructionComparator.instToString(((AddressInstruction) dest).getAddress());
							
							if (s1.equals(instr_str) || s2.equals(instr_str))
								return true;
														
							String d_aux= InstructionComparator.instToString(((AddressInstruction) dest).getAddress());
							
							instr_str = "*(" + d_aux + ")";
							if (s1.equals(instr_str) || s2.equals(instr_str))
								return true;
							
							instr_str = d_aux + "[";
							if ((s1.startsWith(instr_str) && s1.endsWith("]")) 
									|| (s2.startsWith(instr_str) && s2.endsWith("]")))
								return true;
						} 
					}
			
				if (!fctCall) {
				/** case 4
				 *      u + .....;
				 *      u = ... ;
				 *      u + .....;
				 */
				oprdModified = oprdModified || ( d.equals(s1) || d.equals(s2) );
				if (oprdModified)
					return true;
			}
		}
		return oprdModified;
	}

	/**
	 * returns true if opr (operator) is commutative, and false otherwise
	 * 
	 * @param opr
	 * @return boolean
	 */
	private boolean commutative(String opr) {
		if (opr.equals(ArithmeticOperator.MUL.getLiteral()))
			return true;
		if (opr.equals(ArithmeticOperator.ADD.getLiteral()))
			return true;
		if (opr.equals(ComparisonOperator.EQ.getLiteral()))
			return true;
		if (opr.equals(ComparisonOperator.NE.getLiteral()))
			return true;
		if (opr.equals(LogicalOperator.AND.getLiteral()))
			return true;
		if (opr.equals(LogicalOperator.OR.getLiteral()))
			return true;

		return false;
	}

	/**
	 * returns a new temporary name and verifies if The symbol is already used
	 * or not
	 * 
	 * @param scope
	 * @return String
	 */
	private String new_temp(Scope scope) {
		String name;
		boolean ok;
		if (scope != null) {
			do {
				name = "_temp" + incr_counter();
				ok = true;
				for (Symbol s : scope.getSymbols()) {
					if (s.getName().equals(name)) {
						ok = false;
						break;
					}
				}
			} while (!ok);
		} else
			name = "_temp" + incr_counter();
		return name;
	}

}