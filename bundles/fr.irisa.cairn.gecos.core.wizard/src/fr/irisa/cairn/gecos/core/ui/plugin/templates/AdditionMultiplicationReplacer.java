package fr.irisa.cairn.gecos.core.ui.plugin.templates;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.pde.core.plugin.IMatchRules;
import org.eclipse.pde.core.plugin.IPluginReference;
import org.eclipse.pde.ui.templates.PluginReference;

import fr.irisa.cairn.gecos.core.ui.plugin.templates.utils.BasicCopyPluginTemplate;
import fr.irisa.cairn.gecos.model.GecosModelPlugin;

public class AdditionMultiplicationReplacer extends BasicCopyPluginTemplate {
	@Override
	public IPluginReference[] getDependencies(String schemaVersion) {
		return new IPluginReference[] { new PluginReference("fr.irisa.r2d2.gecos.framework", null, IMatchRules.NONE),
				new PluginReference(GecosModelPlugin.PLUGIN_ID, null, IMatchRules.NONE) };
	}

	@Override
	protected String getTemplateDirectory() {
		return "resources/plugin_templates"; //$NON-NLS-1$
	}

	@Override
	public String getSectionId() {
		return "additionmultiplicationreplacer"; //$NON-NLS-1$
	}

	@Override
	public String getTitle() {
		return "GeCoS module which replace all additions with multplications and multiplications with additions";
	}

	@Override
	public String getDescription() {
		return "Generate a GeCoS module with its implementation which replace all additions with multplications and multiplications with additions";
	}

	@Override
	public Map<String, String> getModuleClass() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("AdditionMultiplicationReplacer", "modules.AdditionMultiplicationReplacerModule");
		return map;
	}

	@Override
	public String getSubPackageName() {
		return "replacer";
	}
}