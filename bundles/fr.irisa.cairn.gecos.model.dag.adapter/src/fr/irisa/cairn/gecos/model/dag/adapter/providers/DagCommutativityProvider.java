package fr.irisa.cairn.gecos.model.dag.adapter.providers;

import java.util.HashSet;
import java.util.Set;

import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGNodeAdapter;
import fr.irisa.cairn.graph.providers.ICommutativityProvider;
import gecos.dag.DAGOpNode;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.LogicalOperator;

public class DagCommutativityProvider implements
		ICommutativityProvider<IDAGNodeAdapter> {

	public boolean isCommutative(IDAGNodeAdapter n) {
		if (n.getAdaptedNode() instanceof DAGOpNode) {
			boolean commmutative = CommutativityTable.INSTANCE.commutatives
					.contains(((DAGOpNode) n.getAdaptedNode()).getOpcode());
			return commmutative;
		}
		return false;
	}

	public static class CommutativityTable {
		private final Set<String> commutatives;
		public final static CommutativityTable INSTANCE = new CommutativityTable();

		private CommutativityTable() {
			this.commutatives = new HashSet<String>();
			this.commutatives.add(ArithmeticOperator.ADD.getLiteral());
			this.commutatives.add(ArithmeticOperator.MUL.getLiteral());
			
			this.commutatives.add(LogicalOperator.AND.getLiteral());
			this.commutatives.add(LogicalOperator.OR.getLiteral());
			
			this.commutatives.add(BitwiseOperator.XOR.getLiteral());
			this.commutatives.add(BitwiseOperator.AND.getLiteral());
			this.commutatives.add(BitwiseOperator.OR.getLiteral());
		}

	}
}
