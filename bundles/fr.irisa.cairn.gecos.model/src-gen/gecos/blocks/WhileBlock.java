/**
 */
package gecos.blocks;

import fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;

import gecos.instrs.BranchType;
import gecos.instrs.BreakInstruction;
import gecos.instrs.ContinueInstruction;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>While Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.blocks.WhileBlock#getTestBlock <em>Test Block</em>}</li>
 *   <li>{@link gecos.blocks.WhileBlock#getBodyBlock <em>Body Block</em>}</li>
 * </ul>
 *
 * @see gecos.blocks.BlocksPackage#getWhileBlock()
 * @model annotation="gmf.node figure='rectangle' label='number' label.placement='external' label.icon='false'"
 * @generated
 */
public interface WhileBlock extends Block, BlocksVisitable, Loop {
	/**
	 * Returns the value of the '<em><b>Test Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Block</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Block</em>' containment reference.
	 * @see #setTestBlock(Block)
	 * @see gecos.blocks.BlocksPackage#getWhileBlock_TestBlock()
	 * @model containment="true"
	 *        annotation="gmf.compartment foo='bar'"
	 * @generated
	 */
	Block getTestBlock();

	/**
	 * Sets the value of the '{@link gecos.blocks.WhileBlock#getTestBlock <em>Test Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Test Block</em>' containment reference.
	 * @see #getTestBlock()
	 * @generated
	 */
	void setTestBlock(Block value);

	/**
	 * Returns the value of the '<em><b>Body Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body Block</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body Block</em>' containment reference.
	 * @see #setBodyBlock(Block)
	 * @see gecos.blocks.BlocksPackage#getWhileBlock_BodyBlock()
	 * @model containment="true"
	 *        annotation="gmf.compartment foo='bar'"
	 * @generated
	 */
	Block getBodyBlock();

	/**
	 * Sets the value of the '{@link gecos.blocks.WhileBlock#getBodyBlock <em>Body Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body Block</em>' containment reference.
	 * @see #getBodyBlock()
	 * @generated
	 */
	void setBodyBlock(Block value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitWhileBlock(this);'"
	 * @generated
	 */
	void accept(BlocksVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%fr.irisa.cairn.gecos.model.tools.utils.BlocksContinuesBreaksFinder%&gt; finder = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.BlocksContinuesBreaksFinder%&gt;();\nfinder.doSwitch(this.getBodyBlock());\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt;unmodifiableEList(finder.getContinues());'"
	 * @generated
	 */
	EList<BasicBlock> listContinueBlocks();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder%&gt; finder = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder%&gt;();\nfinder.doSwitch(this.getBodyBlock());\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.instrs.ContinueInstruction%&gt;&gt;unmodifiableEList(finder.getContinues());'"
	 * @generated
	 */
	EList<ContinueInstruction> listContinueInstructions();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%fr.irisa.cairn.gecos.model.tools.utils.BlocksContinuesBreaksFinder%&gt; finder = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.BlocksContinuesBreaksFinder%&gt;();\nfinder.doSwitch(this.getBodyBlock());\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt;unmodifiableEList(finder.getBreaks());'"
	 * @generated
	 */
	EList<BasicBlock> listBreakBlocks();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder%&gt; finder = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder%&gt;();\nfinder.doSwitch(this.getBodyBlock());\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.instrs.BreakInstruction%&gt;&gt;unmodifiableEList(finder.getBreaks());'"
	 * @generated
	 */
	EList<BreakInstruction> listBreakInstructions();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" mngDataType="gecos.core.BlockCopyManager" mngUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.blocks.WhileBlock%&gt; whileBlock = &lt;%gecos.blocks.BlocksFactory%&gt;.eINSTANCE.createWhileBlock();\nwhileBlock.setTestBlock(this.getTestBlock().managedCopy(mng));\nwhileBlock.setBodyBlock(this.getBodyBlock().managedCopy(mng));\n&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory%&gt;.copyAnnotations(this, whileBlock);\nmng.link(this, whileBlock);\nreturn whileBlock;'"
	 * @generated
	 */
	Block managedCopy(BlockCopyManager mng);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model toUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='throw new &lt;%java.lang.UnsupportedOperationException%&gt;(\"Not yet implemented\");'"
	 * @generated
	 */
	void mergeWithBasic(BasicBlock to);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" compositeBlockUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='throw new &lt;%java.lang.UnsupportedOperationException%&gt;(\"Not yet implemented\");'"
	 * @generated
	 */
	Block mergeWithComposite(CompositeBlock compositeBlock);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='throw new &lt;%java.lang.UnsupportedOperationException%&gt;(\"Not yet implemented\");'"
	 * @generated
	 */
	Block simplifyBlock();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model aUnique="false" bUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='throw new &lt;%java.lang.UnsupportedOperationException%&gt;(\"Not yet implemented\");'"
	 * @generated
	 */
	void addBlockAfter(Block a, Block b);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bbUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.Block%&gt; _testBlock = this.getTestBlock();\nboolean _tripleEquals = (_testBlock == bb);\nif (_tripleEquals)\n{\n\tthis.setTestBlock(null);\n}\nelse\n{\n\t&lt;%gecos.blocks.Block%&gt; _bodyBlock = this.getBodyBlock();\n\tboolean _tripleEquals_1 = (_bodyBlock == bb);\n\tif (_tripleEquals_1)\n\t{\n\t\tthis.setBodyBlock(null);\n\t}\n\telse\n\t{\n\t\t&lt;%gecos.blocks.Block%&gt; _bodyBlock_1 = this.getBodyBlock();\n\t\tboolean _tripleNotEquals = (_bodyBlock_1 != null);\n\t\tif (_tripleNotEquals)\n\t\t{\n\t\t\tthis.getBodyBlock().removeBlock(bb);\n\t\t}\n\t}\n}'"
	 * @generated
	 */
	void removeBlock(Block bb);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='throw new &lt;%java.lang.UnsupportedOperationException%&gt;(\"Not yet implemented\");'"
	 * @generated
	 */
	void mergeChildComposite(CompositeBlock b);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" dispatchUnique="false" condUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getTestBlock().connectFromBasic(dispatch, cond);'"
	 * @generated
	 */
	ControlEdge connectFromBasic(BasicBlock dispatch, BranchType cond);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" nextUnique="false" condUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.blocks.ControlEdge%&gt;&gt; res = this.getTestBlock().connectTo(next, &lt;%gecos.instrs.BranchType%&gt;.IF_FALSE);\nfinal &lt;%fr.irisa.cairn.gecos.model.tools.utils.BlocksContinuesBreaksFinder%&gt; visitor = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.BlocksContinuesBreaksFinder%&gt;();\nvisitor.doSwitch(this.getBodyBlock());\n&lt;%java.util.List%&gt;&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt; _breaks = visitor.getBreaks();\nfor (final &lt;%gecos.blocks.BasicBlock%&gt; bb : _breaks)\n{\n\t{\n\t\t&lt;%gecos.blocks.ControlEdge%&gt;[] _clone = ((&lt;%gecos.blocks.ControlEdge%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(bb.getOutEdges(), &lt;%gecos.blocks.ControlEdge%&gt;.class)).clone();\n\t\tfor (final &lt;%gecos.blocks.ControlEdge%&gt; e : _clone)\n\t\t{\n\t\t\t{\n\t\t\t\te.setFrom(null);\n\t\t\t\te.setTo(null);\n\t\t\t}\n\t\t}\n\t\tbb.getOutEdges().clear();\n\t\tres.addAll(bb.connectTo(next, &lt;%gecos.instrs.BranchType%&gt;.UNCONDITIONAL));\n\t}\n}\nreturn res;'"
	 * @generated
	 */
	EList<ControlEdge> connectTo(Block next, BranchType cond);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model oldbUnique="false" newbUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.Block%&gt; _testBlock = this.getTestBlock();\nboolean _tripleEquals = (oldb == _testBlock);\nif (_tripleEquals)\n{\n\tthis.setTestBlock(newb);\n}\nelse\n{\n\t&lt;%gecos.blocks.Block%&gt; _bodyBlock = this.getBodyBlock();\n\tboolean _tripleEquals_1 = (oldb == _bodyBlock);\n\tif (_tripleEquals_1)\n\t{\n\t\tthis.setBodyBlock(newb);\n\t}\n}'"
	 * @generated
	 */
	void replace(Block oldb, Block newb);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.blocks.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return super.getPathString(this.toShortString());'"
	 * @generated
	 */
	String getPathString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.blocks.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _number = this.getNumber();\nreturn (\"While_\" + &lt;%java.lang.Integer%&gt;.valueOf(_number));'"
	 * @generated
	 */
	String toShortString();

} // WhileBlock
