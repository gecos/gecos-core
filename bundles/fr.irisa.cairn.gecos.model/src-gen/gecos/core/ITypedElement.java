/**
 */
package gecos.core;

import gecos.types.Type;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ITyped Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.core.CorePackage#getITypedElement()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ITypedElement extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 * @generated
	 */
	Type getType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model tUnique="false"
	 * @generated
	 */
	void setType(Type t);

} // ITypedElement
