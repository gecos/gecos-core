/**
 */
package gecos.blocks;

import gecos.instrs.SSAUseSymbol;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.blocks.DataEdge#getUses <em>Uses</em>}</li>
 *   <li>{@link gecos.blocks.DataEdge#getTo <em>To</em>}</li>
 *   <li>{@link gecos.blocks.DataEdge#getFrom <em>From</em>}</li>
 * </ul>
 *
 * @see gecos.blocks.BlocksPackage#getDataEdge()
 * @model annotation="gmf.link source='from' target='to' style='dot' width='1'"
 * @generated
 */
public interface DataEdge extends FlowEdge {
	/**
	 * Returns the value of the '<em><b>Uses</b></em>' reference list.
	 * The list contents are of type {@link gecos.instrs.SSAUseSymbol}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uses</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uses</em>' reference list.
	 * @see gecos.blocks.BlocksPackage#getDataEdge_Uses()
	 * @model
	 * @generated
	 */
	EList<SSAUseSymbol> getUses();

	/**
	 * Returns the value of the '<em><b>To</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link gecos.blocks.BasicBlock#getUseEdges <em>Use Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To</em>' reference.
	 * @see #setTo(BasicBlock)
	 * @see gecos.blocks.BlocksPackage#getDataEdge_To()
	 * @see gecos.blocks.BasicBlock#getUseEdges
	 * @model opposite="useEdges" resolveProxies="false"
	 * @generated
	 */
	BasicBlock getTo();

	/**
	 * Sets the value of the '{@link gecos.blocks.DataEdge#getTo <em>To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To</em>' reference.
	 * @see #getTo()
	 * @generated
	 */
	void setTo(BasicBlock value);

	/**
	 * Returns the value of the '<em><b>From</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link gecos.blocks.BasicBlock#getDefEdges <em>Def Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From</em>' container reference.
	 * @see #setFrom(BasicBlock)
	 * @see gecos.blocks.BlocksPackage#getDataEdge_From()
	 * @see gecos.blocks.BasicBlock#getDefEdges
	 * @model opposite="defEdges" transient="false"
	 * @generated
	 */
	BasicBlock getFrom();

	/**
	 * Sets the value of the '{@link gecos.blocks.DataEdge#getFrom <em>From</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From</em>' container reference.
	 * @see #getFrom()
	 * @generated
	 */
	void setFrom(BasicBlock value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDataEdge(this);'"
	 * @generated
	 */
	void accept(BlocksVisitor visitor);

} // DataEdge
