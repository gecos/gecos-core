package fr.irisa.cairn.gecos.core.ui.examples;

import org.eclipse.core.runtime.Platform;
import org.eclipse.ui.INewWizard;
import org.osgi.framework.Bundle;

import fr.irisa.cairn.gecos.core.ui.Activator;
import fr.irisa.cairn.gecos.core.ui.examples.utils.BasicGecosExampleWizard;

public class S2SFunctionInlining extends BasicGecosExampleWizard implements INewWizard {
	@Override
	public String getResourcesFolderToCopyIntoProject() {
		return "resources/wizard_templates/s2s_function_inlining/";
	}

	@Override
	public String getPageName() {
		return "Source-to-Source function inlining example";
	}

	@Override
	public String getPageDescription() {
		return "This wizard will create a example project of source-to-source function inlining transformation";
	}

	@Override
	protected Bundle getPluginBundle() {
		return Platform.getBundle(Activator.PLUGIN_ID);
	}
}
