package fr.irisa.cairn.gecos.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.handlers.HandlerUtil;

public abstract class AbstactGecosCFileHandler extends AbstractHandler {
	
	protected String PREFIX;
	public AbstactGecosCFileHandler() {
		super();
		PREFIX = "[AbstactGecosCFileHandler] ";
	}
	public AbstactGecosCFileHandler(String prefix) {
		super();
		PREFIX = prefix;
	}
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
        try {
	        ISelection selection = HandlerUtil.getCurrentSelection(event);
            if (selection instanceof StructuredSelection) {
            	StructuredSelection structuredSelection = (StructuredSelection) selection;
    			for (Object o : structuredSelection.toArray()) {
    				final IFile file = (IFile) (o);
    				handle(file);
    			}
            } else if (selection instanceof TextSelection) {      
            	IEditorInput input = HandlerUtil.getActiveEditorInput(event);
            	final IFile file = (IFile)input.getAdapter(IFile.class);
            	handle(file);
            } else {
            	throw new IllegalArgumentException(getPrefix()+"ERROR. Unsupported selection : "+selection.getClass().getSimpleName());
            }
        } catch (Exception e) {
        	System.err.println(getPrefix()+"ERROR while processing selection.");
        	e.printStackTrace();
        }
        return null;
	}

	//for debug information
	protected String getPrefix() {
		return PREFIX;
	}
	public abstract void handle(IFile f);

}
