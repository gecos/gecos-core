/**
 */
package gecos.types;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Internal Parameter Type Wrapper</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.__Internal_ParameterTypeWrapper#getParameter <em>Parameter</em>}</li>
 * </ul>
 *
 * @see gecos.types.TypesPackage#get__Internal_ParameterTypeWrapper()
 * @model
 * @generated
 */
public interface __Internal_ParameterTypeWrapper extends EObject {
	/**
	 * Returns the value of the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter</em>' reference.
	 * @see #setParameter(Type)
	 * @see gecos.types.TypesPackage#get__Internal_ParameterTypeWrapper_Parameter()
	 * @model resolveProxies="false"
	 * @generated
	 */
	Type getParameter();

	/**
	 * Sets the value of the '{@link gecos.types.__Internal_ParameterTypeWrapper#getParameter <em>Parameter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter</em>' reference.
	 * @see #getParameter()
	 * @generated
	 */
	void setParameter(Type value);

} // __Internal_ParameterTypeWrapper
