package fr.irisa.cairn.gecos.model.utils.emf;

import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.tools.ecore.DanglingAnalyzer;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import fr.irisa.r2d2.gecos.framework.GSArg;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;

@GSModule("Check if the specified object containement structure has dangling references.")
public class DanglingRefPrinter {

	private EObject obj;
	private boolean fail;

	@GSModuleConstructor(args= {
		@GSArg(name = "obj", info = "the object to check"),
		@GSArg(name = "fail", info = "if true throw a RuntimeException, otherwise only print the dandling references.")
	})
	public DanglingRefPrinter(EObject obj, boolean fail) {
		this.obj = obj;
		this.fail = fail;
	}
	
	public void compute() {
		List<EObject> dr = new DanglingAnalyzer(obj).getAllDanglingReferences(obj);
		if(!dr.isEmpty()) {
			String msg = dr.stream()
				.map(r -> "- " + printObj(r) + ", contained by: " + printObj(r.eContainer())+ ", referenced by: " + EMFUtils.getAllReferencesOf(r, obj))
				.collect(Collectors.joining("\n"));
			
				String str = "[" + this.getClass().getSimpleName() + "]: the EObject " +
					printObj(obj) + " has the following dangling references:\n" + msg;
				if(fail) 
					throw new RuntimeException(str);
				else
					System.out.println(str);
		}
	}
	
	private String printObj(Object o) {
		if(o == null) 
			return "null";
		return "'" + o + "' (of type " + o.getClass().getSimpleName() + ")";
	}
}
