/**
 */
package gecos.annotations.provider;


import gecos.annotations.AnnotationsPackage;
import gecos.annotations.FileLocationAnnotation;

import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link gecos.annotations.FileLocationAnnotation} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class FileLocationAnnotationItemProvider extends IAnnotationItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FileLocationAnnotationItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addFilenamePropertyDescriptor(object);
			addStartingLinePropertyDescriptor(object);
			addEndingLinePropertyDescriptor(object);
			addNodeOffsetPropertyDescriptor(object);
			addNodeLengthPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Filename feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFilenamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FileLocationAnnotation_filename_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FileLocationAnnotation_filename_feature", "_UI_FileLocationAnnotation_type"),
				 AnnotationsPackage.Literals.FILE_LOCATION_ANNOTATION__FILENAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Starting Line feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStartingLinePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FileLocationAnnotation_startingLine_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FileLocationAnnotation_startingLine_feature", "_UI_FileLocationAnnotation_type"),
				 AnnotationsPackage.Literals.FILE_LOCATION_ANNOTATION__STARTING_LINE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Ending Line feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEndingLinePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FileLocationAnnotation_endingLine_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FileLocationAnnotation_endingLine_feature", "_UI_FileLocationAnnotation_type"),
				 AnnotationsPackage.Literals.FILE_LOCATION_ANNOTATION__ENDING_LINE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Node Offset feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNodeOffsetPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FileLocationAnnotation_nodeOffset_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FileLocationAnnotation_nodeOffset_feature", "_UI_FileLocationAnnotation_type"),
				 AnnotationsPackage.Literals.FILE_LOCATION_ANNOTATION__NODE_OFFSET,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Node Length feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNodeLengthPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FileLocationAnnotation_nodeLength_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FileLocationAnnotation_nodeLength_feature", "_UI_FileLocationAnnotation_type"),
				 AnnotationsPackage.Literals.FILE_LOCATION_ANNOTATION__NODE_LENGTH,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns FileLocationAnnotation.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/FileLocationAnnotation"));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean shouldComposeCreationImage() {
		return true;
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((FileLocationAnnotation)object).getFilename();
		return label == null || label.length() == 0 ?
			getString("_UI_FileLocationAnnotation_type") :
			getString("_UI_FileLocationAnnotation_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(FileLocationAnnotation.class)) {
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__FILENAME:
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__STARTING_LINE:
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__ENDING_LINE:
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__NODE_OFFSET:
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__NODE_LENGTH:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

}
