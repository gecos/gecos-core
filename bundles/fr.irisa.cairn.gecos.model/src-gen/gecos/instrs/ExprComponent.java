/**
 */
package gecos.instrs;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expr Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.ExprComponent#getExpr <em>Expr</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getExprComponent()
 * @model abstract="true"
 * @generated
 */
public interface ExprComponent extends ComponentInstruction {
	/**
	 * Returns the value of the '<em><b>Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expr</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expr</em>' containment reference.
	 * @see #setExpr(Instruction)
	 * @see gecos.instrs.InstrsPackage#getExprComponent_Expr()
	 * @model containment="true"
	 * @generated
	 */
	Instruction getExpr();

	/**
	 * Sets the value of the '{@link gecos.instrs.ExprComponent#getExpr <em>Expr</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expr</em>' containment reference.
	 * @see #getExpr()
	 * @generated
	 */
	void setExpr(Instruction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;asEList(this.getExpr()));'"
	 * @generated
	 */
	EList<Instruction> listChildren();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.ExprComponent%&gt;))\n{\n\tboolean isSame = false;\n\tif (((((&lt;%gecos.instrs.ExprComponent%&gt;)instr).getExpr() == null) &amp;&amp; (this.getExpr() == null)))\n\t{\n\t\tisSame = true;\n\t}\n\telse\n\t{\n\t\t&lt;%gecos.instrs.Instruction%&gt; _expr = ((&lt;%gecos.instrs.ExprComponent%&gt;)instr).getExpr();\n\t\tboolean _tripleEquals = (_expr == null);\n\t\t&lt;%gecos.instrs.Instruction%&gt; _expr_1 = this.getExpr();\n\t\tboolean _tripleEquals_1 = (_expr_1 == null);\n\t\tboolean _xor = (_tripleEquals ^ _tripleEquals_1);\n\t\tif (_xor)\n\t\t{\n\t\t\treturn false;\n\t\t}\n\t\telse\n\t\t{\n\t\t\tisSame = ((&lt;%gecos.instrs.ExprComponent%&gt;)instr).getExpr().isSame(this.getExpr());\n\t\t}\n\t}\n\treturn (super.isSame(instr) &amp;&amp; isSame);\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // ExprComponent
