/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.factory.conversion;

import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import gecos.types.ArrayType;
import gecos.types.BaseType;
import gecos.types.PtrType;
import gecos.types.Type;



public class PtrConversionLine extends ConversionLine {

	/* ========================================================================
	 *        | char   | short  | int    | long   | longlong | float  | double
	 * ========================================================================
	 * char   | char   | short  | int    | long   | longlong | float  | double
	 * short  | short  | short  | int    | long   | longlong | float  | double
	 * int    | int    | int    | int    | long   | longlong | float  | double
	 * long   | long   | long   | long   | long   | longlong | float  | double
	 * longl  | longl  | longl  | longl  | longl  | longlong | float  | double
	 * float  | float  | float  | float  | float  | double   | float  | double
	 * double | double | double | double | double | double   | double | double
	 */
	private PtrType baseType;

	public PtrConversionLine(PtrType type) {
		baseType = type;
	}
	
	public PtrType getBaseType() {
		return baseType;
	}

	public Object caseBaseType(BaseType t) {
		return baseType;
	}

	public Object casePtrType(PtrType t) {
		// XXX : There was an issue with pointer/pointer comparison, I believe that this should solve the problem     
		if (t.isEqual(baseType)) {
			return baseType;
		} else {
			return null;
		}
	}

	public Object caseArrayType(ArrayType t) {
		// XXX : There was an issue with pointer/array comparison, I believe that this should solve the problem
		Type commonBaseType = ConversionMatrix.getCommon(t.getBase(), baseType.getBase());
		if (commonBaseType!=null) {
			GecosUserTypeFactory.setScope(commonBaseType.getContainingScope());
			return  GecosUserTypeFactory.PTR(commonBaseType);
		} else {
			return null;
		}
	}

}