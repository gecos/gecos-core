project = CreateGecosProject("tutorial"); # Just create a GeCoS project
AddSourceToGecosProject(project, "src/ex1.c"); # Add source to the GeCoS project
CDTFrontend(project); # Call the GeCoS Front-end and build the GeCoS IR corresponding to the project

cg = CallGraphBuilder(project); # Module which build the call graph corresponding to the GeCoS project
output(cg, "dot", "callgraph_folder"); # Generate the call graph into graphviz format