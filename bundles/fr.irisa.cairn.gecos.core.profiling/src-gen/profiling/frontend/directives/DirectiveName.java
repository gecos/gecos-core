/**
 */
package profiling.frontend.directives;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Directive Name</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see profiling.frontend.directives.DirectivesPackage#getDirectiveName()
 * @model
 * @generated
 */
public enum DirectiveName implements Enumerator {
	/**
	 * The '<em><b>SIZE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SIZE_VALUE
	 * @generated
	 * @ordered
	 */
	SIZE(0, "SIZE", "$size"),

	/**
	 * The '<em><b>SAVE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAVE_VALUE
	 * @generated
	 * @ordered
	 */
	SAVE(0, "SAVE", "$save"),

	/**
	 * The '<em><b>INJECT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INJECT_VALUE
	 * @generated
	 * @ordered
	 */
	INJECT(0, "INJECT", "$inject"),

	/**
	 * The '<em><b>FROM FILE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FROM_FILE_VALUE
	 * @generated
	 * @ordered
	 */
	FROM_FILE(0, "FROM_FILE", "$from_file"),

	/**
	 * The '<em><b>FROM VAR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FROM_VAR_VALUE
	 * @generated
	 * @ordered
	 */
	FROM_VAR(0, "FROM_VAR", "$from_var"),

	/**
	 * The '<em><b>RANDOM UNIFORM</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RANDOM_UNIFORM_VALUE
	 * @generated
	 * @ordered
	 */
	RANDOM_UNIFORM(0, "RANDOM_UNIFORM", "$random_uniform"),

	/**
	 * The '<em><b>RANDOM NORMAL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RANDOM_NORMAL_VALUE
	 * @generated
	 * @ordered
	 */
	RANDOM_NORMAL(0, "RANDOM_NORMAL", "$random_normal");

	/**
	 * The '<em><b>SIZE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SIZE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SIZE
	 * @model literal="$size"
	 * @generated
	 * @ordered
	 */
	public static final int SIZE_VALUE = 0;

	/**
	 * The '<em><b>SAVE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SAVE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SAVE
	 * @model literal="$save"
	 * @generated
	 * @ordered
	 */
	public static final int SAVE_VALUE = 0;

	/**
	 * The '<em><b>INJECT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>INJECT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INJECT
	 * @model literal="$inject"
	 * @generated
	 * @ordered
	 */
	public static final int INJECT_VALUE = 0;

	/**
	 * The '<em><b>FROM FILE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FROM FILE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FROM_FILE
	 * @model literal="$from_file"
	 * @generated
	 * @ordered
	 */
	public static final int FROM_FILE_VALUE = 0;

	/**
	 * The '<em><b>FROM VAR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FROM VAR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FROM_VAR
	 * @model literal="$from_var"
	 * @generated
	 * @ordered
	 */
	public static final int FROM_VAR_VALUE = 0;

	/**
	 * The '<em><b>RANDOM UNIFORM</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>RANDOM UNIFORM</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RANDOM_UNIFORM
	 * @model literal="$random_uniform"
	 * @generated
	 * @ordered
	 */
	public static final int RANDOM_UNIFORM_VALUE = 0;

	/**
	 * The '<em><b>RANDOM NORMAL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>RANDOM NORMAL</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RANDOM_NORMAL
	 * @model literal="$random_normal"
	 * @generated
	 * @ordered
	 */
	public static final int RANDOM_NORMAL_VALUE = 0;

	/**
	 * An array of all the '<em><b>Directive Name</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final DirectiveName[] VALUES_ARRAY =
		new DirectiveName[] {
			SIZE,
			SAVE,
			INJECT,
			FROM_FILE,
			FROM_VAR,
			RANDOM_UNIFORM,
			RANDOM_NORMAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Directive Name</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<DirectiveName> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Directive Name</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DirectiveName get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DirectiveName result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Directive Name</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DirectiveName getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DirectiveName result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Directive Name</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DirectiveName get(int value) {
		switch (value) {
			case SIZE_VALUE: return SIZE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private DirectiveName(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //DirectiveName
