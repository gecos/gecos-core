/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.analysis.dataflow.framework;

import gecos.blocks.BasicBlock;
import gecos.core.Procedure;

import org.eclipse.emf.common.util.EList;

/** ForwardAnalyser. specialize the IterativeAnalyser
 * with next(b) = succ(b)
 */

public abstract class ForwardAnalyser extends IterativeAnalyser {

	public ForwardAnalyser(Procedure proc) {
		super(proc);
	}

	protected EList<BasicBlock> nextBlocks(BasicBlock b) {
		return b.getPredecessors();
	}

	protected EList<BasicBlock> reverseNextBlocks(BasicBlock b) {
		return b.getSuccessors();
	}
}
