/**
 */
package gecos.instrs.impl;

import gecos.blocks.BasicBlock;

import gecos.instrs.ChildrenListInstruction;
import gecos.instrs.InstrsPackage;
import gecos.instrs.Instruction;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.xtext.xbase.lib.ExclusiveRange;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Children List Instruction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.impl.ChildrenListInstructionImpl#getChildren <em>Children</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ChildrenListInstructionImpl extends InstructionImpl implements ChildrenListInstruction {
	/**
	 * The cached value of the '{@link #getChildren() <em>Children</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChildren()
	 * @generated
	 * @ordered
	 */
	protected EList<Instruction> children;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ChildrenListInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstrsPackage.eINSTANCE.getChildrenListInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instruction> getChildren() {
		if (children == null) {
			children = new EObjectContainmentEList<Instruction>(Instruction.class, this, InstrsPackage.CHILDREN_LIST_INSTRUCTION__CHILDREN);
		}
		return children;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instruction> listChildren() {
		return ECollections.<Instruction>unmodifiableEList(this.getChildren());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addChild(final Instruction inst) {
		this.getChildren().add(inst);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeChild(final Instruction inst) {
		this.getChildren().remove(inst);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getChildrenCount() {
		return this.getChildren().size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getChild(final int i) {
		int _childrenCount = this.getChildrenCount();
		boolean _lessEqualsThan = (_childrenCount <= i);
		if (_lessEqualsThan) {
			return null;
		}
		return this.getChildren().get(i);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSame(final Instruction instr) {
		if ((instr instanceof ChildrenListInstruction)) {
			return super.isSame(instr);
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		EList<Instruction> _listChildren = this.listChildren();
		String _plus = ("ComplexInstruction[" + _listChildren);
		return (_plus + "]");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instruction> listValidChildren() {
		return ECollections.<Instruction>unmodifiableEList(ECollections.<Instruction>asEList(((Instruction[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<Instruction>filterNull(this.listChildren()), Instruction.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int indexOfChild(final Instruction child) {
		int _size = this.listChildren().size();
		ExclusiveRange _doubleDotLessThan = new ExclusiveRange(0, _size, true);
		for (final Integer i : _doubleDotLessThan) {
			Instruction _get = this.listChildren().get((i).intValue());
			boolean _tripleEquals = (_get == child);
			if (_tripleEquals) {
				return (i).intValue();
			}
		}
		return (-1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getChildAt(final int index) {
		return this.listChildren().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeChildAt(final int index) {
		this.removeChild(this.getChildAt(index));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void replaceChild(final Instruction oldInstr, final Instruction newInstr) {
		EObject _eContainer = oldInstr.eContainer();
		boolean _tripleNotEquals = (_eContainer != this);
		if (_tripleNotEquals) {
			throw new IllegalArgumentException((((("[" + oldInstr) + "] is not contained by [") + this) + "]"));
		}
		EcoreUtil.replace(((EObject) oldInstr), newInstr);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicBlock getBlock() {
		final EObject c = this.getRoot().eContainer();
		if ((c instanceof BasicBlock)) {
			return ((BasicBlock)c);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getRootInstuction() {
		return this.getRoot();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InstrsPackage.CHILDREN_LIST_INSTRUCTION__CHILDREN:
				return ((InternalEList<?>)getChildren()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InstrsPackage.CHILDREN_LIST_INSTRUCTION__CHILDREN:
				return getChildren();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InstrsPackage.CHILDREN_LIST_INSTRUCTION__CHILDREN:
				getChildren().clear();
				getChildren().addAll((Collection<? extends Instruction>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InstrsPackage.CHILDREN_LIST_INSTRUCTION__CHILDREN:
				getChildren().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InstrsPackage.CHILDREN_LIST_INSTRUCTION__CHILDREN:
				return children != null && !children.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ChildrenListInstructionImpl
