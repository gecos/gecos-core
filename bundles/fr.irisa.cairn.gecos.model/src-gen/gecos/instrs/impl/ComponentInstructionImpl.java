/**
 */
package gecos.instrs.impl;

import gecos.blocks.BasicBlock;

import gecos.instrs.ComponentInstruction;
import gecos.instrs.InstrsPackage;
import gecos.instrs.Instruction;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.EcoreUtil;

import org.eclipse.xtext.xbase.lib.ExclusiveRange;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class ComponentInstructionImpl extends InstructionImpl implements ComponentInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstrsPackage.eINSTANCE.getComponentInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSame(final Instruction instr) {
		if ((instr instanceof ComponentInstruction)) {
			return super.isSame(instr);
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		EList<Instruction> _listChildren = this.listChildren();
		String _plus = ("ComponentInstruction[" + _listChildren);
		return (_plus + "]");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instruction> listChildren() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instruction> listValidChildren() {
		return ECollections.<Instruction>unmodifiableEList(ECollections.<Instruction>asEList(((Instruction[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<Instruction>filterNull(this.listChildren()), Instruction.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int indexOfChild(final Instruction child) {
		int _size = this.listChildren().size();
		ExclusiveRange _doubleDotLessThan = new ExclusiveRange(0, _size, true);
		for (final Integer i : _doubleDotLessThan) {
			Instruction _get = this.listChildren().get((i).intValue());
			boolean _tripleEquals = (_get == child);
			if (_tripleEquals) {
				return (i).intValue();
			}
		}
		return (-1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getChildAt(final int index) {
		return this.listChildren().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeChildAt(final int index) {
		this.removeChild(this.getChildAt(index));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeChild(final Instruction instr) {
		EObject _eContainer = instr.eContainer();
		boolean _tripleEquals = (_eContainer == this);
		if (_tripleEquals) {
			EcoreUtil.remove(((EObject) instr));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void replaceChild(final Instruction oldInstr, final Instruction newInstr) {
		EObject _eContainer = oldInstr.eContainer();
		boolean _tripleNotEquals = (_eContainer != this);
		if (_tripleNotEquals) {
			throw new IllegalArgumentException((((("[" + oldInstr) + "] is not contained by [") + this) + "]"));
		}
		EcoreUtil.replace(((EObject) oldInstr), newInstr);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getChildrenCount() {
		return this.listChildren().size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicBlock getBlock() {
		final EObject c = this.getRoot().eContainer();
		if ((c instanceof BasicBlock)) {
			return ((BasicBlock)c);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getRootInstuction() {
		return this.getRoot();
	}

} //ComponentInstructionImpl
