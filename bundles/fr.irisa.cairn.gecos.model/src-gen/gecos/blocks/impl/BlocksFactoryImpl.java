/**
 */
package gecos.blocks.impl;

import gecos.blocks.*;

import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BlocksFactoryImpl extends EFactoryImpl implements BlocksFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static BlocksFactory init() {
		try {
			BlocksFactory theBlocksFactory = (BlocksFactory)EPackage.Registry.INSTANCE.getEFactory(BlocksPackage.eNS_URI);
			if (theBlocksFactory != null) {
				return theBlocksFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new BlocksFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlocksFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case BlocksPackage.BASIC_BLOCK: return createBasicBlock();
			case BlocksPackage.COMPOSITE_BLOCK: return createCompositeBlock();
			case BlocksPackage.CONTROL_EDGE: return createControlEdge();
			case BlocksPackage.DATA_EDGE: return createDataEdge();
			case BlocksPackage.IF_BLOCK: return createIfBlock();
			case BlocksPackage.WHILE_BLOCK: return createWhileBlock();
			case BlocksPackage.DO_WHILE_BLOCK: return createDoWhileBlock();
			case BlocksPackage.FOR_BLOCK: return createForBlock();
			case BlocksPackage.FOR_C99_BLOCK: return createForC99Block();
			case BlocksPackage.SIMPLE_FOR_BLOCK: return createSimpleForBlock();
			case BlocksPackage.SWITCH_BLOCK: return createSwitchBlock();
			case BlocksPackage.CASE_BLOCK: return createCaseBlock();
			case BlocksPackage.CASE_MAP: return (EObject)createCaseMap();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case BlocksPackage.STRING:
				return createStringFromString(eDataType, initialValue);
			case BlocksPackage.EE_LONG:
				return createEELongFromString(eDataType, initialValue);
			case BlocksPackage.INT:
				return createintFromString(eDataType, initialValue);
			case BlocksPackage.LONG:
				return createlongFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case BlocksPackage.STRING:
				return convertStringToString(eDataType, instanceValue);
			case BlocksPackage.EE_LONG:
				return convertEELongToString(eDataType, instanceValue);
			case BlocksPackage.INT:
				return convertintToString(eDataType, instanceValue);
			case BlocksPackage.LONG:
				return convertlongToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicBlock createBasicBlock() {
		BasicBlockImpl basicBlock = new BasicBlockImpl();
		return basicBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeBlock createCompositeBlock() {
		CompositeBlockImpl compositeBlock = new CompositeBlockImpl();
		return compositeBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlEdge createControlEdge() {
		ControlEdgeImpl controlEdge = new ControlEdgeImpl();
		return controlEdge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataEdge createDataEdge() {
		DataEdgeImpl dataEdge = new DataEdgeImpl();
		return dataEdge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfBlock createIfBlock() {
		IfBlockImpl ifBlock = new IfBlockImpl();
		return ifBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WhileBlock createWhileBlock() {
		WhileBlockImpl whileBlock = new WhileBlockImpl();
		return whileBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DoWhileBlock createDoWhileBlock() {
		DoWhileBlockImpl doWhileBlock = new DoWhileBlockImpl();
		return doWhileBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ForBlock createForBlock() {
		ForBlockImpl forBlock = new ForBlockImpl();
		return forBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ForC99Block createForC99Block() {
		ForC99BlockImpl forC99Block = new ForC99BlockImpl();
		return forC99Block;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleForBlock createSimpleForBlock() {
		SimpleForBlockImpl simpleForBlock = new SimpleForBlockImpl();
		return simpleForBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SwitchBlock createSwitchBlock() {
		SwitchBlockImpl switchBlock = new SwitchBlockImpl();
		return switchBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CaseBlock createCaseBlock() {
		CaseBlockImpl caseBlock = new CaseBlockImpl();
		return caseBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Long, Block> createCaseMap() {
		CaseMapImpl caseMap = new CaseMapImpl();
		return caseMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createStringFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStringToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Long createEELongFromString(EDataType eDataType, String initialValue) {
		return (Long)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEELongToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer createintFromString(EDataType eDataType, String initialValue) {
		return (Integer)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertintToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Long createlongFromString(EDataType eDataType, String initialValue) {
		return (Long)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertlongToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlocksPackage getBlocksPackage() {
		return (BlocksPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static BlocksPackage getPackage() {
		return BlocksPackage.eINSTANCE;
	}

} //BlocksFactoryImpl
