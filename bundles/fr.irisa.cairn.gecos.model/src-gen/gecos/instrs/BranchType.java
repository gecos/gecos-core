/**
 */
package gecos.instrs;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Branch Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see gecos.instrs.InstrsPackage#getBranchType()
 * @model
 * @generated
 */
public enum BranchType implements Enumerator {
	/**
	 * The '<em><b>Unconditional</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNCONDITIONAL_VALUE
	 * @generated
	 * @ordered
	 */
	UNCONDITIONAL(0, "unconditional", "unconditional"),

	/**
	 * The '<em><b>If True</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IF_TRUE_VALUE
	 * @generated
	 * @ordered
	 */
	IF_TRUE(1, "ifTrue", "ifTrue"),

	/**
	 * The '<em><b>If False</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IF_FALSE_VALUE
	 * @generated
	 * @ordered
	 */
	IF_FALSE(2, "ifFalse", "ifFalse"),

	/**
	 * The '<em><b>Dispatch</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DISPATCH_VALUE
	 * @generated
	 * @ordered
	 */
	DISPATCH(3, "dispatch", "dispatch");

	/**
	 * The '<em><b>Unconditional</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Unconditional</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNCONDITIONAL
	 * @model name="unconditional"
	 * @generated
	 * @ordered
	 */
	public static final int UNCONDITIONAL_VALUE = 0;

	/**
	 * The '<em><b>If True</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>If True</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IF_TRUE
	 * @model name="ifTrue"
	 * @generated
	 * @ordered
	 */
	public static final int IF_TRUE_VALUE = 1;

	/**
	 * The '<em><b>If False</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>If False</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IF_FALSE
	 * @model name="ifFalse"
	 * @generated
	 * @ordered
	 */
	public static final int IF_FALSE_VALUE = 2;

	/**
	 * The '<em><b>Dispatch</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Dispatch</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DISPATCH
	 * @model name="dispatch"
	 * @generated
	 * @ordered
	 */
	public static final int DISPATCH_VALUE = 3;

	/**
	 * An array of all the '<em><b>Branch Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final BranchType[] VALUES_ARRAY =
		new BranchType[] {
			UNCONDITIONAL,
			IF_TRUE,
			IF_FALSE,
			DISPATCH,
		};

	/**
	 * A public read-only list of all the '<em><b>Branch Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<BranchType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Branch Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static BranchType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			BranchType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Branch Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static BranchType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			BranchType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Branch Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static BranchType get(int value) {
		switch (value) {
			case UNCONDITIONAL_VALUE: return UNCONDITIONAL;
			case IF_TRUE_VALUE: return IF_TRUE;
			case IF_FALSE_VALUE: return IF_FALSE;
			case DISPATCH_VALUE: return DISPATCH;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private BranchType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //BranchType
