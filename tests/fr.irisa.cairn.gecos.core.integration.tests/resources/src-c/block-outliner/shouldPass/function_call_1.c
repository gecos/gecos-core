int add(int X, int Y) {
	return X + Y;
}

int main() {
	int res;

	#pragma outline
	{
		res = add(3, 2);
	}

	if (res != 5) {
		abort();
	}

	return 0;
}
