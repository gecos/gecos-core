/**
 */
package gecos.annotations.impl;

import gecos.annotations.*;

import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AnnotationsFactoryImpl extends EFactoryImpl implements AnnotationsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AnnotationsFactory init() {
		try {
			AnnotationsFactory theAnnotationsFactory = (AnnotationsFactory)EPackage.Registry.INSTANCE.getEFactory(AnnotationsPackage.eNS_URI);
			if (theAnnotationsFactory != null) {
				return theAnnotationsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new AnnotationsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case AnnotationsPackage.STRING_ANNOTATION: return createStringAnnotation();
			case AnnotationsPackage.PRAGMA_ANNOTATION: return createPragmaAnnotation();
			case AnnotationsPackage.COMMENT_ANNOTATION: return createCommentAnnotation();
			case AnnotationsPackage.EXTENDED_ANNOTATION: return createExtendedAnnotation();
			case AnnotationsPackage.LIVENESS_ANNOTATION: return createLivenessAnnotation();
			case AnnotationsPackage.SYMBOL_DEFINITION_ANNOTATION: return createSymbolDefinitionAnnotation();
			case AnnotationsPackage.IAST_ANNOTATION: return createIASTAnnotation();
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION: return createFileLocationAnnotation();
			case AnnotationsPackage.STRING_TO_IANNOTATION_MAP: return (EObject)createStringToIAnnotationMap();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case AnnotationsPackage.ANNOTATION_KEYS:
				return createAnnotationKeysFromString(eDataType, initialValue);
			case AnnotationsPackage.LIVENESS_INFO:
				return createLivenessInfoFromString(eDataType, initialValue);
			case AnnotationsPackage.STRING:
				return createStringFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case AnnotationsPackage.ANNOTATION_KEYS:
				return convertAnnotationKeysToString(eDataType, instanceValue);
			case AnnotationsPackage.LIVENESS_INFO:
				return convertLivenessInfoToString(eDataType, instanceValue);
			case AnnotationsPackage.STRING:
				return convertStringToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StringAnnotation createStringAnnotation() {
		StringAnnotationImpl stringAnnotation = new StringAnnotationImpl();
		return stringAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PragmaAnnotation createPragmaAnnotation() {
		PragmaAnnotationImpl pragmaAnnotation = new PragmaAnnotationImpl();
		return pragmaAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommentAnnotation createCommentAnnotation() {
		CommentAnnotationImpl commentAnnotation = new CommentAnnotationImpl();
		return commentAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExtendedAnnotation createExtendedAnnotation() {
		ExtendedAnnotationImpl extendedAnnotation = new ExtendedAnnotationImpl();
		return extendedAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LivenessAnnotation createLivenessAnnotation() {
		LivenessAnnotationImpl livenessAnnotation = new LivenessAnnotationImpl();
		return livenessAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SymbolDefinitionAnnotation createSymbolDefinitionAnnotation() {
		SymbolDefinitionAnnotationImpl symbolDefinitionAnnotation = new SymbolDefinitionAnnotationImpl();
		return symbolDefinitionAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IASTAnnotation createIASTAnnotation() {
		IASTAnnotationImpl iastAnnotation = new IASTAnnotationImpl();
		return iastAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FileLocationAnnotation createFileLocationAnnotation() {
		FileLocationAnnotationImpl fileLocationAnnotation = new FileLocationAnnotationImpl();
		return fileLocationAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<String, IAnnotation> createStringToIAnnotationMap() {
		StringToIAnnotationMapImpl stringToIAnnotationMap = new StringToIAnnotationMapImpl();
		return stringToIAnnotationMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationKeys createAnnotationKeysFromString(EDataType eDataType, String initialValue) {
		AnnotationKeys result = AnnotationKeys.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAnnotationKeysToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LivenessInfo createLivenessInfoFromString(EDataType eDataType, String initialValue) {
		LivenessInfo result = LivenessInfo.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLivenessInfoToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createStringFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStringToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationsPackage getAnnotationsPackage() {
		return (AnnotationsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static AnnotationsPackage getPackage() {
		return AnnotationsPackage.eINSTANCE;
	}

} //AnnotationsFactoryImpl
