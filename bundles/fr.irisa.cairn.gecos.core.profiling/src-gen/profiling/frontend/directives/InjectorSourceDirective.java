/**
 */
package profiling.frontend.directives;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Injector Source Directive</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see profiling.frontend.directives.DirectivesPackage#getInjectorSourceDirective()
 * @model abstract="true"
 * @generated
 */
public interface InjectorSourceDirective extends Directive {
} // InjectorSourceDirective
