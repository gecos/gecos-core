/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.factory.conversion;

import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import gecos.core.Scope;
import gecos.types.ArrayType;
import gecos.types.BaseType;
import gecos.types.BoolType;
import gecos.types.FloatType;
import gecos.types.IntegerType;
import gecos.types.PtrType;
import gecos.types.SignModifiers;


public class BaseConversionLine extends ConversionLine {

	/* ========================================================================
	 *        | char   | short  | int    | long   | longlong | float  | double
	 * ========================================================================
	 * char   | char   | short  | int    | long   | longlong | float  | double
	 * short  | short  | short  | int    | long   | longlong | float  | double
	 * int    | int    | int    | int    | long   | longlong | float  | double
	 * long   | long   | long   | long   | long   | longlong | float  | double
	 * longl  | longl  | longl  | longl  | longl  | longlong | float  | double
	 * float  | float  | float  | float  | float  | double   | float  | double
	 * double | double | double | double | double | double   | double | double
	 */
	private BaseType baseType;

	public BaseConversionLine(BaseType type) {
		baseType = type;
	}
	
	public BaseType getBaseType() {
		return baseType;
	}

//	@Override 
//	public Object caseBaseType(BaseType t) {
//		BaseType commonType;
//		if (t.getType()== baseType.getType())
//			commonType = (baseType.getSize() <= t.getSize())?t:baseType;
//		else if (t.getType() == Types.FLOAT)
//			commonType = t;
//		else
//			commonType = baseType;
//		
//		boolean signed = t.isSigned() || baseType.isSigned();
//		if (signed && !commonType.isSigned()) {
//			commonType = commonType.copy();
//			commonType.setSigned(true);
//		}	
//		return commonType;
//	}
	
	@Override
	public Object caseFloatType(FloatType t) {
		if(baseType instanceof FloatType)
			return baseType.getSize() <= t.getSize() ? t : baseType;
		else //XXX void ?
			return t;
	}
	
	@Override 
	public Object caseIntegerType(IntegerType t) {
		if(baseType instanceof FloatType)
			return baseType;
		else if(baseType instanceof IntegerType) { 
			//FIXME: if this is supposed to perform type promotion then it does it wrong!
			IntegerType commonType;
			if(baseType.getSize() <= t.getSize())
				commonType = t;
			else
				commonType = (IntegerType) baseType;
			
			boolean signed = t.getSigned() || ((IntegerType) baseType).getSigned();
			if (signed && !commonType.getSigned()) {
				Scope scope = commonType.getContainingScope();
				if(scope == null)
					scope = GecosUserTypeFactory.getScope();
				
				commonType = commonType.copy();
				commonType.setSignModifier(SignModifiers.SIGNED);
				if(scope != null)
					commonType.installOn(scope);
			}
			return commonType;
		}
		else 			return t;
	}
	
	@Override
	public Object caseBoolType(BoolType t) {
		if(baseType instanceof FloatType || baseType instanceof IntegerType)
			return baseType;
		else //XXX void ?
			return t;
	}
	
	public Object casePtrType(PtrType t) {
		return t;
	}

	public Object caseArrayType(ArrayType t) {
		return t;
	}
}