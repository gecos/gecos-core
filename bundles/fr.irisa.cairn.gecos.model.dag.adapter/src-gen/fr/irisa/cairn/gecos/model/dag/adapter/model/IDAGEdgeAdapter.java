package fr.irisa.cairn.gecos.model.dag.adapter.model;

import gecos.dag.DAGEdge;

/**
 * Interface of an edge adapter.

 * This edge adapt a DAGEdge

 * @author <your name>
 *
 */
public interface IDAGEdgeAdapter
		extends
			fr.irisa.cairn.graph.IEdge,
			fr.irisa.cairn.graph.observer.IObservable {

	/**
	 * get the adapted edge
	 *
	 * @return the adapted edge
	 */
	DAGEdge getAdaptedEdge();

	/**
	 * @model
	 */
	IDAGPortAdapter getSourcePort();

	/**
	 * @model
	 */
	IDAGPortAdapter getSinkPort();

}
