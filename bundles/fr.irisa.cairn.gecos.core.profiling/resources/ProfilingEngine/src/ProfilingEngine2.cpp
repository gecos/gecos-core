#include "ProfilingEngine.h"

#include <stdio.h>
// not included in "ProfilingEngine.h" cause it significantly increases compilation time !
#include <random>
#include <map>
#include <iostream>
#include <fstream>
#include <string>


namespace profengine {


//-----------------------------------------------------------
// Variable
//-----------------------------------------------------------
	const char * Variable::getName() {
		return m_name;
	}

	int Variable::getUid() {
		return m_uid;
	}

	const char * Variable::toString() {
		std::string s = std::string(m_name) + "_" + std::to_string(m_uid);
		return s.c_str();
	}


//-----------------------------------------------------------
// Generators
//-----------------------------------------------------------

	static std::default_random_engine *_RANDOM_GENERATOR = new std::default_random_engine(std::random_device()());

	class Generator {
	public:
		virtual ~Generator() {};
		virtual double generate() = 0;
	};

	class UniformRandomGenerator : public Generator {
	private:
		std::uniform_real_distribution<double> _distribution;
	public:
		UniformRandomGenerator(double minInclusive, double maxExclusive) : _distribution(minInclusive, maxExclusive) {}
		double generate() override {
			return _distribution(*_RANDOM_GENERATOR);
		}
	};

	class NormalRandomGenerator : public Generator {
	private:
		std::normal_distribution<double> _distribution;
	public:
		NormalRandomGenerator(double mean, double stddev) : _distribution(mean, stddev) {}
		double generate() override {
			return _distribution(*_RANDOM_GENERATOR);
		}
	};

	class FromFileGenerator : public Generator {
	private:
		std::string filePath;
		std::ifstream inputStream;
		int nbDims;
		int *sizes = NULL;
	public:
		FromFileGenerator(const char* inputPath) : filePath(inputPath) {
			inputStream.open(inputPath);
			inputStream >> nbDims;
			sizes = new int[nbDims];
			for(int i = 0; i < nbDims; i++) {
				inputStream >> sizes[i];
			}
		}
		~FromFileGenerator() override {
			delete [] sizes;
			sizes = NULL;
			inputStream.close();
		}
		double generate() override {
			double val;
			inputStream >> val;
			return val;
		}
	};

//-----------------------------------------------------------
// ProfilingEngine
//-----------------------------------------------------------

	class VariablesMap {
	private:
		std::map<int, Variable*> internal_map;
	public:
		~VariablesMap() {
			for(auto const& entry : internal_map)
				delete entry.second;
		}

		friend class ProfilingEngine;
	};



	ProfilingEngine::ProfilingEngine() {
		m_values = new VariablesMap();
		generator = new UniformRandomGenerator(0.0, 1.0);
	}

	ProfilingEngine::~ProfilingEngine() {
		delete _RANDOM_GENERATOR;
		delete generator;
		delete m_values;
	}

	void ProfilingEngine::saveSnapshot(int uid) {
		Variable* var = getVariable(uid);
		if(var == NULL) {
			fprintf(stderr, "No variable with uid '%d' was registered!", uid);
			exit(1);
		}

		var->newSnapshot();
	}

	Variable* ProfilingEngine::getVariable(int uid) {
		return m_values->internal_map[uid];
	}

	void ProfilingEngine::saveVariable(int uid, Variable *variable) {
		if(m_values->internal_map[uid] != NULL){
			std::cerr << "Variable already created. Problem during the construction of the manager variable mapping" << std::endl;
			exit(1);
		}

		m_values->internal_map[uid] = variable;
	}

	void ProfilingEngine::writeHuman(const char *outputFolderPath) {
		std::string pathOutput;
		std::fstream fs;
//		FILE *fs = NULL;

		for(auto const& entry : m_values->internal_map) {
			Variable* var = entry.second;
			pathOutput = std::string(outputFolderPath) +"/" + var->toString() + ".txt";


			fs.open(pathOutput.c_str(), std::fstream::out);
			if(!fs.is_open()){
//			fs = fopen(pathOutput.c_str(), "w");
//			if(fs == NULL) {
				std::cerr << "File to write simulation results can not be open: " << pathOutput << std::endl;
				exit(1);
			}

			var->writeHuman(fs);

			fs.close();
//			fclose(fs);
		}
	}


    void ProfilingEngine::setGeneratorUniformDoubleDistribution(double minInclusive, double maxExclusive) {
    	delete generator; // delete previous generator
    	generator = new UniformRandomGenerator(minInclusive, maxExclusive);
    }

    void ProfilingEngine::setGeneratorNormalDoubleDistribution(double mean, double stddev) {
    	delete generator; // delete previous generator
    	generator = new NormalRandomGenerator(mean, stddev);
    }

    void ProfilingEngine::setGeneratorFromFile(const char *filePath) {
    	delete generator; // delete previous generator
    	generator = new FromFileGenerator(filePath);
    }


//	 void ProfilingEngine::setGeneratorUniformIntDistribution(int minInclusive, int maxInclusive) {
//		std::uniform_int_distribution<int> _distribution(minInclusive, maxInclusive);
//	 }
//
//    void ProfilingEngine::setGeneratorNormalIntDistribution(double mean, double stddev) {
//    	std::normal_distribution<int> _distribution(mean, stddev);
//    }

	void ProfilingEngine::setGeneratorSeed(int seed) {
		_RANDOM_GENERATOR = new std::default_random_engine(seed);
	}

    double ProfilingEngine::generate() {
    	return generator->generate();
    }
}
