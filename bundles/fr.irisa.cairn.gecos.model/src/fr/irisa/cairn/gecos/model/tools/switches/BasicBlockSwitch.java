
package fr.irisa.cairn.gecos.model.tools.switches;

import fr.irisa.cairn.gecos.model.extensions.switchs.AdaptableBlockSwitch;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CaseBlock;
import gecos.blocks.CompositeBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.SwitchBlock;
import gecos.blocks.WhileBlock;

/**
 *  BasicBlockSwitch is used to visit every kind of block of
 *  a procedure. It's default behavior is to visit every block
 *  recursively until reaching a {@link BasicBlock}.
 * 
 * 
 * @author antoine
 */
public abstract class BasicBlockSwitch<T> extends AdaptableBlockSwitch<T>{

	public BasicBlockSwitch() {
	}

	public abstract T caseBasicBlock(BasicBlock b) ;

	@Override
	public T caseCompositeBlock(CompositeBlock b) {
		for (Block block  : b.getChildren()) {
			doSwitch(block);
		}
		return super.caseCompositeBlock(b);
	}

	@Override
	public T caseIfBlock(IfBlock b) {
		doSwitch(b.getTestBlock());
		if (b.getThenBlock() != null)
			doSwitch(b.getThenBlock());
		if (b.getElseBlock() != null)
			doSwitch(b.getElseBlock());
		return super.caseIfBlock(b);
	}

	@Override
	public T caseDoWhileBlock(DoWhileBlock b) {
		doSwitch(b.getTestBlock());
		if (b.getBodyBlock() != null)
			doSwitch(b.getBodyBlock());
		return super.caseDoWhileBlock(b);
	}
	
	@Override
	public T caseWhileBlock(WhileBlock b) {
		doSwitch(b.getTestBlock());
		doSwitch(b.getBodyBlock());
		return super.caseWhileBlock(b);
	}
	
	@Override
	public T caseForBlock(ForBlock b) {
		doSwitch(b.getTestBlock());
		doSwitch(b.getBodyBlock());
		doSwitch(b.getStepBlock());
		doSwitch(b.getInitBlock());
		return super.caseForBlock(b);
	}
	
	@Override
	public T caseSwitchBlock(SwitchBlock object) {
		doSwitch(object.getDispatchBlock());
		doSwitch(object.getBodyBlock());
		if (object.getDefaultBlock() != null)
			doSwitch(object.getDefaultBlock());
		return super.caseSwitchBlock(object);
	}
	
	@Override
	public T caseCaseBlock(CaseBlock object) {
		if (object.getBody() != null)
			doSwitch(object.getBody());
		return super.caseCaseBlock(object);
	}
	
}
