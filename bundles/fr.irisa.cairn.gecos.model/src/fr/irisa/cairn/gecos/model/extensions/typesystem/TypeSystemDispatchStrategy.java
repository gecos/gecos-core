/**
 * 
 */
package fr.irisa.cairn.gecos.model.extensions.typesystem;

import gecos.instrs.Instruction;
import gecos.types.Type;

import java.util.List;

/**
 * Strategy that select the first compatible generator extension.
 * @author mnaullet
 */
public class TypeSystemDispatchStrategy  {

	public Type dispatch(List<ITypeSystemRuleSet> generators, Instruction o) {
		/*
		 * Find finally a suitable generator
		 */
		for (ITypeSystemRuleSet iGecosCodeGenerator : generators) {
			Type generated = iGecosCodeGenerator.computeType(o);
			if(generated!=null)
				return generated;
		}
		
		return null;
	}

}