/**
 */
package gecos.core.impl;

import fr.irisa.cairn.gecos.model.tools.utils.BlocksUpdate;
import fr.irisa.cairn.gecos.model.tools.utils.GecosCopier;

import fr.irisa.cairn.tools.ecore.query.EMFUtils;

import gecos.annotations.impl.AnnotatedElementImpl;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;

import gecos.core.CorePackage;
import gecos.core.CoreVisitor;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.ScopeContainer;
import gecos.core.Symbol;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Procedure</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.core.impl.ProcedureImpl#getStart <em>Start</em>}</li>
 *   <li>{@link gecos.core.impl.ProcedureImpl#getBody <em>Body</em>}</li>
 *   <li>{@link gecos.core.impl.ProcedureImpl#getEnd <em>End</em>}</li>
 *   <li>{@link gecos.core.impl.ProcedureImpl#getSymbol <em>Symbol</em>}</li>
 *   <li>{@link gecos.core.impl.ProcedureImpl#getContainingProcedureSet <em>Containing Procedure Set</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProcedureImpl extends AnnotatedElementImpl implements Procedure {
	/**
	 * The cached value of the '{@link #getStart() <em>Start</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStart()
	 * @generated
	 * @ordered
	 */
	protected BasicBlock start;

	/**
	 * The cached value of the '{@link #getBody() <em>Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBody()
	 * @generated
	 * @ordered
	 */
	protected CompositeBlock body;

	/**
	 * The cached value of the '{@link #getEnd() <em>End</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnd()
	 * @generated
	 * @ordered
	 */
	protected BasicBlock end;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProcedureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.PROCEDURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicBlock getStart() {
		return start;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStart(BasicBlock newStart) {
		BasicBlock oldStart = start;
		start = newStart;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PROCEDURE__START, oldStart, start));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeBlock getBody() {
		return body;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBody(CompositeBlock newBody, NotificationChain msgs) {
		CompositeBlock oldBody = body;
		body = newBody;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.PROCEDURE__BODY, oldBody, newBody);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBody(CompositeBlock newBody) {
		if (newBody != body) {
			NotificationChain msgs = null;
			if (body != null)
				msgs = ((InternalEObject)body).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROCEDURE__BODY, null, msgs);
			if (newBody != null)
				msgs = ((InternalEObject)newBody).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROCEDURE__BODY, null, msgs);
			msgs = basicSetBody(newBody, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PROCEDURE__BODY, newBody, newBody));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicBlock getEnd() {
		return end;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnd(BasicBlock newEnd) {
		BasicBlock oldEnd = end;
		end = newEnd;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PROCEDURE__END, oldEnd, end));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureSymbol getSymbol() {
		if (eContainerFeatureID() != CorePackage.PROCEDURE__SYMBOL) return null;
		return (ProcedureSymbol)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureSymbol basicGetSymbol() {
		if (eContainerFeatureID() != CorePackage.PROCEDURE__SYMBOL) return null;
		return (ProcedureSymbol)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSymbol(ProcedureSymbol newSymbol, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newSymbol, CorePackage.PROCEDURE__SYMBOL, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSymbol(ProcedureSymbol newSymbol) {
		if (newSymbol != eInternalContainer() || (eContainerFeatureID() != CorePackage.PROCEDURE__SYMBOL && newSymbol != null)) {
			if (EcoreUtil.isAncestor(this, newSymbol))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newSymbol != null)
				msgs = ((InternalEObject)newSymbol).eInverseAdd(this, CorePackage.PROCEDURE_SYMBOL__PROCEDURE, ProcedureSymbol.class, msgs);
			msgs = basicSetSymbol(newSymbol, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PROCEDURE__SYMBOL, newSymbol, newSymbol));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureSet getContainingProcedureSet() {
		if ((((this.getSymbol() != null) && (this.getSymbol().getContainingScope() != null)) && (this.getSymbol().getContainingScope().getContainer() instanceof ProcedureSet))) {
			ScopeContainer _container = this.getSymbol().getContainingScope().getContainer();
			return ((ProcedureSet) _container);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParameterSymbol> listParameters() {
		return this.getSymbol().listParameters();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BasicBlock> getBasicBlocks() {
		return EMFUtils.<BasicBlock>eAllContentsFirstInstancesOf(this, BasicBlock.class);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final CoreVisitor visitor) {
		visitor.visitProcedure(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _valueOf = String.valueOf(this.getSymbol());
		return ("Procedure: " + _valueOf);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scope getScope() {
		return this.getSymbol().getScope();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void replaceBlock(final Block oldBlk, final Block newBlk) {
		final BlocksUpdate update = new BlocksUpdate(oldBlk, newBlk);
		update.doSwitch(this);
		BasicBlock _start = this.getStart();
		boolean _tripleEquals = (_start == oldBlk);
		if (_tripleEquals) {
			this.setStart(((BasicBlock) newBlk));
		}
		else {
			BasicBlock _end = this.getEnd();
			boolean _tripleEquals_1 = (_end == oldBlk);
			if (_tripleEquals_1) {
				this.setEnd(((BasicBlock) newBlk));
			}
			else {
				CompositeBlock _body = this.getBody();
				boolean _tripleEquals_2 = (_body == oldBlk);
				if (_tripleEquals_2) {
					this.setBody(((CompositeBlock) newBlk));
				}
				else {
					this.getBody().replace(oldBlk, newBlk);
				}
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Procedure copy() {
		final GecosCopier gecosCopier = new GecosCopier();
		EObject _copy = gecosCopier.copy(this);
		final Procedure procedure = ((Procedure) _copy);
		gecosCopier.copyReferences();
		return procedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol getUsedSymbol() {
		return this.getSymbol();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSymbol(final Symbol newSymbol) {
		if ((newSymbol instanceof ProcedureSymbol)) {
			((ProcedureSymbol)newSymbol).setProcedure(this);
		}
		else {
			throw new IllegalArgumentException();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void updateSymbol(final Symbol originalSymbol, final Symbol newSymbol) {
		Symbol _usedSymbol = this.getUsedSymbol();
		boolean _tripleEquals = (_usedSymbol == originalSymbol);
		if (_tripleEquals) {
			this.setSymbol(newSymbol);
		}
		else {
			throw new IllegalArgumentException("Given Symbol is not used.");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSymbolName() {
		return this.getUsedSymbol().getName();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.PROCEDURE__SYMBOL:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetSymbol((ProcedureSymbol)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.PROCEDURE__BODY:
				return basicSetBody(null, msgs);
			case CorePackage.PROCEDURE__SYMBOL:
				return basicSetSymbol(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CorePackage.PROCEDURE__SYMBOL:
				return eInternalContainer().eInverseRemove(this, CorePackage.PROCEDURE_SYMBOL__PROCEDURE, ProcedureSymbol.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CorePackage.PROCEDURE__START:
				return getStart();
			case CorePackage.PROCEDURE__BODY:
				return getBody();
			case CorePackage.PROCEDURE__END:
				return getEnd();
			case CorePackage.PROCEDURE__SYMBOL:
				if (resolve) return getSymbol();
				return basicGetSymbol();
			case CorePackage.PROCEDURE__CONTAINING_PROCEDURE_SET:
				return getContainingProcedureSet();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CorePackage.PROCEDURE__START:
				setStart((BasicBlock)newValue);
				return;
			case CorePackage.PROCEDURE__BODY:
				setBody((CompositeBlock)newValue);
				return;
			case CorePackage.PROCEDURE__END:
				setEnd((BasicBlock)newValue);
				return;
			case CorePackage.PROCEDURE__SYMBOL:
				setSymbol((ProcedureSymbol)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CorePackage.PROCEDURE__START:
				setStart((BasicBlock)null);
				return;
			case CorePackage.PROCEDURE__BODY:
				setBody((CompositeBlock)null);
				return;
			case CorePackage.PROCEDURE__END:
				setEnd((BasicBlock)null);
				return;
			case CorePackage.PROCEDURE__SYMBOL:
				setSymbol((ProcedureSymbol)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CorePackage.PROCEDURE__START:
				return start != null;
			case CorePackage.PROCEDURE__BODY:
				return body != null;
			case CorePackage.PROCEDURE__END:
				return end != null;
			case CorePackage.PROCEDURE__SYMBOL:
				return basicGetSymbol() != null;
			case CorePackage.PROCEDURE__CONTAINING_PROCEDURE_SET:
				return getContainingProcedureSet() != null;
		}
		return super.eIsSet(featureID);
	}

} //ProcedureImpl
