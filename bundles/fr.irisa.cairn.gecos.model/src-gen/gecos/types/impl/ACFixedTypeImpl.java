/**
 */
package gecos.types.impl;

import gecos.types.ACFixedType;
import gecos.types.ACType;
import gecos.types.BaseType;
import gecos.types.BoolType;
import gecos.types.FloatType;
import gecos.types.IntegerType;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.ScalarType;
import gecos.types.Type;
import gecos.types.TypesPackage;
import gecos.types.TypesVisitor;
import gecos.types.VoidType;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AC Fixed Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.impl.ACFixedTypeImpl#getSize <em>Size</em>}</li>
 *   <li>{@link gecos.types.impl.ACFixedTypeImpl#isSigned <em>Signed</em>}</li>
 *   <li>{@link gecos.types.impl.ACFixedTypeImpl#getBitwidth <em>Bitwidth</em>}</li>
 *   <li>{@link gecos.types.impl.ACFixedTypeImpl#getMode <em>Mode</em>}</li>
 *   <li>{@link gecos.types.impl.ACFixedTypeImpl#getFraction <em>Fraction</em>}</li>
 *   <li>{@link gecos.types.impl.ACFixedTypeImpl#getInteger <em>Integer</em>}</li>
 *   <li>{@link gecos.types.impl.ACFixedTypeImpl#getQuantificationMode <em>Quantification Mode</em>}</li>
 *   <li>{@link gecos.types.impl.ACFixedTypeImpl#getOverflowMode <em>Overflow Mode</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ACFixedTypeImpl extends ArrayTypeImpl implements ACFixedType {
	/**
	 * The default value of the '{@link #getSize() <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSize()
	 * @generated
	 * @ordered
	 */
	protected static final long SIZE_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getSize() <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSize()
	 * @generated
	 * @ordered
	 */
	protected long size = SIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #isSigned() <em>Signed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSigned()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SIGNED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSigned() <em>Signed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSigned()
	 * @generated
	 * @ordered
	 */
	protected boolean signed = SIGNED_EDEFAULT;

	/**
	 * The default value of the '{@link #getBitwidth() <em>Bitwidth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBitwidth()
	 * @generated
	 * @ordered
	 */
	protected static final long BITWIDTH_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getBitwidth() <em>Bitwidth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBitwidth()
	 * @generated
	 * @ordered
	 */
	protected long bitwidth = BITWIDTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getMode() <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMode()
	 * @generated
	 * @ordered
	 */
	protected static final long MODE_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getMode() <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMode()
	 * @generated
	 * @ordered
	 */
	protected long mode = MODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getFraction() <em>Fraction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFraction()
	 * @generated
	 * @ordered
	 */
	protected static final long FRACTION_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getFraction() <em>Fraction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFraction()
	 * @generated
	 * @ordered
	 */
	protected long fraction = FRACTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getInteger() <em>Integer</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInteger()
	 * @generated
	 * @ordered
	 */
	protected static final long INTEGER_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getInteger() <em>Integer</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInteger()
	 * @generated
	 * @ordered
	 */
	protected long integer = INTEGER_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantificationMode() <em>Quantification Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantificationMode()
	 * @generated
	 * @ordered
	 */
	protected static final QuantificationMode QUANTIFICATION_MODE_EDEFAULT = QuantificationMode.AC_TRN;

	/**
	 * The cached value of the '{@link #getQuantificationMode() <em>Quantification Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantificationMode()
	 * @generated
	 * @ordered
	 */
	protected QuantificationMode quantificationMode = QUANTIFICATION_MODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getOverflowMode() <em>Overflow Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverflowMode()
	 * @generated
	 * @ordered
	 */
	protected static final OverflowMode OVERFLOW_MODE_EDEFAULT = OverflowMode.AC_WRAP;

	/**
	 * The cached value of the '{@link #getOverflowMode() <em>Overflow Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverflowMode()
	 * @generated
	 * @ordered
	 */
	protected OverflowMode overflowMode = OVERFLOW_MODE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ACFixedTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypesPackage.Literals.AC_FIXED_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getSize_() {
		return size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSize(long newSize) {
		long oldSize = size;
		size = newSize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.AC_FIXED_TYPE__SIZE, oldSize, size));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSigned() {
		return signed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSigned(boolean newSigned) {
		boolean oldSigned = signed;
		signed = newSigned;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.AC_FIXED_TYPE__SIGNED, oldSigned, signed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getBitwidth() {
		return bitwidth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBitwidth(long newBitwidth) {
		long oldBitwidth = bitwidth;
		bitwidth = newBitwidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.AC_FIXED_TYPE__BITWIDTH, oldBitwidth, bitwidth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getMode() {
		return mode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMode(long newMode) {
		long oldMode = mode;
		mode = newMode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.AC_FIXED_TYPE__MODE, oldMode, mode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getFraction() {
		return fraction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFraction(long newFraction) {
		long oldFraction = fraction;
		fraction = newFraction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.AC_FIXED_TYPE__FRACTION, oldFraction, fraction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getInteger() {
		return integer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInteger(long newInteger) {
		long oldInteger = integer;
		integer = newInteger;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.AC_FIXED_TYPE__INTEGER, oldInteger, integer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QuantificationMode getQuantificationMode() {
		return quantificationMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantificationMode(QuantificationMode newQuantificationMode) {
		QuantificationMode oldQuantificationMode = quantificationMode;
		quantificationMode = newQuantificationMode == null ? QUANTIFICATION_MODE_EDEFAULT : newQuantificationMode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.AC_FIXED_TYPE__QUANTIFICATION_MODE, oldQuantificationMode, quantificationMode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OverflowMode getOverflowMode() {
		return overflowMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOverflowMode(OverflowMode newOverflowMode) {
		OverflowMode oldOverflowMode = overflowMode;
		overflowMode = newOverflowMode == null ? OVERFLOW_MODE_EDEFAULT : newOverflowMode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.AC_FIXED_TYPE__OVERFLOW_MODE, oldOverflowMode, overflowMode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final TypesVisitor visitor) {
		visitor.visitACFixedType(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getSize() {
		return this.getBitwidth();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		final boolean defaultMode = ((this.getOverflowMode().getValue() == 0) && (this.getQuantificationMode().getValue() == 0));
		QuantificationMode _quantificationMode = this.getQuantificationMode();
		String _plus = (_quantificationMode + ",");
		OverflowMode _overflowMode = this.getOverflowMode();
		final String mode = (_plus + _overflowMode);
		boolean _isSigned = this.isSigned();
		boolean _not = (!_isSigned);
		if (_not) {
			if (defaultMode) {
				long _bitwidth = this.getBitwidth();
				String _plus_1 = ("ac_fixed<" + Long.valueOf(_bitwidth));
				String _plus_2 = (_plus_1 + ",");
				long _integer = this.getInteger();
				String _plus_3 = (_plus_2 + Long.valueOf(_integer));
				return (_plus_3 + ",false>");
			}
			else {
				long _bitwidth_1 = this.getBitwidth();
				String _plus_4 = ("ac_fixed<" + Long.valueOf(_bitwidth_1));
				String _plus_5 = (_plus_4 + ",");
				long _integer_1 = this.getInteger();
				String _plus_6 = (_plus_5 + Long.valueOf(_integer_1));
				String _plus_7 = (_plus_6 + ",false,");
				String _plus_8 = (_plus_7 + mode);
				return (_plus_8 + ">");
			}
		}
		else {
			if (defaultMode) {
				long _bitwidth_2 = this.getBitwidth();
				String _plus_9 = ("ac_fixed<" + Long.valueOf(_bitwidth_2));
				String _plus_10 = (_plus_9 + ",");
				long _integer_2 = this.getInteger();
				String _plus_11 = (_plus_10 + Long.valueOf(_integer_2));
				return (_plus_11 + ">");
			}
			else {
				long _bitwidth_3 = this.getBitwidth();
				String _plus_12 = ("ac_fixed<" + Long.valueOf(_bitwidth_3));
				String _plus_13 = (_plus_12 + ",");
				long _integer_3 = this.getInteger();
				String _plus_14 = (_plus_13 + Long.valueOf(_integer_3));
				String _plus_15 = (_plus_14 + ",true,");
				String _plus_16 = (_plus_15 + mode);
				return (_plus_16 + ">");
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEqual(final Type other, final boolean ignoreStorageClass, final boolean ignoreConst, final boolean ignoreVolatile) {
		if ((other == this)) {
			return true;
		}
		if ((other instanceof ACFixedType)) {
			return ((((((((ACFixedType)other).getBitwidth() == this.getBitwidth()) && (((ACFixedType)other).getMode() == this.getMode())) && (((ACFixedType)other).getFraction() == this.getFraction())) && (((ACFixedType)other).getQuantificationMode() == this.getQuantificationMode())) && (((ACFixedType)other).getOverflowMode() == this.getOverflowMode())) && super.isEqual(other, ignoreStorageClass, ignoreConst, ignoreVolatile));
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean getSigned() {
		return this.isSigned();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerType asInt() {
		if ((this instanceof IntegerType)) {
			return ((IntegerType) this);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FloatType asFloat() {
		if ((this instanceof FloatType)) {
			return ((FloatType) this);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BoolType asBool() {
		if ((this instanceof BoolType)) {
			return ((BoolType) this);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VoidType asVoid() {
		if ((this instanceof VoidType)) {
			return ((VoidType) this);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TypesPackage.AC_FIXED_TYPE__SIZE:
				return getSize();
			case TypesPackage.AC_FIXED_TYPE__SIGNED:
				return isSigned();
			case TypesPackage.AC_FIXED_TYPE__BITWIDTH:
				return getBitwidth();
			case TypesPackage.AC_FIXED_TYPE__MODE:
				return getMode();
			case TypesPackage.AC_FIXED_TYPE__FRACTION:
				return getFraction();
			case TypesPackage.AC_FIXED_TYPE__INTEGER:
				return getInteger();
			case TypesPackage.AC_FIXED_TYPE__QUANTIFICATION_MODE:
				return getQuantificationMode();
			case TypesPackage.AC_FIXED_TYPE__OVERFLOW_MODE:
				return getOverflowMode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TypesPackage.AC_FIXED_TYPE__SIZE:
				setSize((Long)newValue);
				return;
			case TypesPackage.AC_FIXED_TYPE__SIGNED:
				setSigned((Boolean)newValue);
				return;
			case TypesPackage.AC_FIXED_TYPE__BITWIDTH:
				setBitwidth((Long)newValue);
				return;
			case TypesPackage.AC_FIXED_TYPE__MODE:
				setMode((Long)newValue);
				return;
			case TypesPackage.AC_FIXED_TYPE__FRACTION:
				setFraction((Long)newValue);
				return;
			case TypesPackage.AC_FIXED_TYPE__INTEGER:
				setInteger((Long)newValue);
				return;
			case TypesPackage.AC_FIXED_TYPE__QUANTIFICATION_MODE:
				setQuantificationMode((QuantificationMode)newValue);
				return;
			case TypesPackage.AC_FIXED_TYPE__OVERFLOW_MODE:
				setOverflowMode((OverflowMode)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TypesPackage.AC_FIXED_TYPE__SIZE:
				setSize(SIZE_EDEFAULT);
				return;
			case TypesPackage.AC_FIXED_TYPE__SIGNED:
				setSigned(SIGNED_EDEFAULT);
				return;
			case TypesPackage.AC_FIXED_TYPE__BITWIDTH:
				setBitwidth(BITWIDTH_EDEFAULT);
				return;
			case TypesPackage.AC_FIXED_TYPE__MODE:
				setMode(MODE_EDEFAULT);
				return;
			case TypesPackage.AC_FIXED_TYPE__FRACTION:
				setFraction(FRACTION_EDEFAULT);
				return;
			case TypesPackage.AC_FIXED_TYPE__INTEGER:
				setInteger(INTEGER_EDEFAULT);
				return;
			case TypesPackage.AC_FIXED_TYPE__QUANTIFICATION_MODE:
				setQuantificationMode(QUANTIFICATION_MODE_EDEFAULT);
				return;
			case TypesPackage.AC_FIXED_TYPE__OVERFLOW_MODE:
				setOverflowMode(OVERFLOW_MODE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TypesPackage.AC_FIXED_TYPE__SIZE:
				return size != SIZE_EDEFAULT;
			case TypesPackage.AC_FIXED_TYPE__SIGNED:
				return signed != SIGNED_EDEFAULT;
			case TypesPackage.AC_FIXED_TYPE__BITWIDTH:
				return bitwidth != BITWIDTH_EDEFAULT;
			case TypesPackage.AC_FIXED_TYPE__MODE:
				return mode != MODE_EDEFAULT;
			case TypesPackage.AC_FIXED_TYPE__FRACTION:
				return fraction != FRACTION_EDEFAULT;
			case TypesPackage.AC_FIXED_TYPE__INTEGER:
				return integer != INTEGER_EDEFAULT;
			case TypesPackage.AC_FIXED_TYPE__QUANTIFICATION_MODE:
				return quantificationMode != QUANTIFICATION_MODE_EDEFAULT;
			case TypesPackage.AC_FIXED_TYPE__OVERFLOW_MODE:
				return overflowMode != OVERFLOW_MODE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ScalarType.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == BaseType.class) {
			switch (derivedFeatureID) {
				case TypesPackage.AC_FIXED_TYPE__SIZE: return TypesPackage.BASE_TYPE__SIZE;
				default: return -1;
			}
		}
		if (baseClass == ACType.class) {
			switch (derivedFeatureID) {
				case TypesPackage.AC_FIXED_TYPE__SIGNED: return TypesPackage.AC_TYPE__SIGNED;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ScalarType.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == BaseType.class) {
			switch (baseFeatureID) {
				case TypesPackage.BASE_TYPE__SIZE: return TypesPackage.AC_FIXED_TYPE__SIZE;
				default: return -1;
			}
		}
		if (baseClass == ACType.class) {
			switch (baseFeatureID) {
				case TypesPackage.AC_TYPE__SIGNED: return TypesPackage.AC_FIXED_TYPE__SIGNED;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ACFixedTypeImpl
