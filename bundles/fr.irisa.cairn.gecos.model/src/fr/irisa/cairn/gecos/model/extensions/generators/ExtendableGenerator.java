package fr.irisa.cairn.gecos.model.extensions.generators;

import java.util.List;

public class ExtendableGenerator {

	private IGeneratorDispatchStrategy dispatchStrategy;
	private List<IGecosCodeGenerator> allGeneratorExtensionsFor;
	
	public ExtendableGenerator(IGeneratorDispatchStrategy dispatchStrategy,
			IGecosCodeGenerator extended) {
		this.dispatchStrategy = dispatchStrategy;
		//Get all registered generators for the extended generator
		allGeneratorExtensionsFor = GecosGeneratorsRegistry.INSTANCE.getAllGeneratorExtensionsFor(extended);
		allGeneratorExtensionsFor.add(0,extended);
	}

	public ExtendableGenerator(
			IGecosCodeGenerator extended) {
		this(new PerfectMatchDispatchStrategy(),extended);
	}

	public String generate(Object o){
		return dispatchStrategy.dispatch(allGeneratorExtensionsFor, o);
	}
}