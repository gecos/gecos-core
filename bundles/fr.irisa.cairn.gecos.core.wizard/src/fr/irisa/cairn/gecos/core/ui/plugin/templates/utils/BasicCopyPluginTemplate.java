package fr.irisa.cairn.gecos.core.ui.plugin.templates.utils;

import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.pde.core.plugin.IPluginBase;
import org.eclipse.pde.core.plugin.IPluginElement;
import org.eclipse.pde.core.plugin.IPluginExtension;
import org.eclipse.pde.core.plugin.IPluginModelBase;
import org.eclipse.pde.core.plugin.IPluginModelFactory;
import org.eclipse.pde.core.plugin.IPluginReference;
import org.eclipse.pde.ui.IFieldData;
import org.eclipse.pde.ui.templates.OptionTemplateSection;
import org.eclipse.pde.ui.templates.TemplateOption;

import fr.irisa.cairn.gecos.core.ui.Activator;

// TODO Remake this class. Make it more user
public abstract class BasicCopyPluginTemplate extends OptionTemplateSection {	
	abstract public IPluginReference[] getDependencies(String schemaVersion);
	abstract protected String getTemplateDirectory();
	abstract public String getSectionId();
	abstract public String getTitle();
	abstract public String getDescription();
	abstract public Map<String,String> getModuleClass();
	abstract public String getSubPackageName();

	public BasicCopyPluginTemplate() {
		setPageCount(1);
		createOptions();
	}
	
	
	private void createOptions() {
		addOption(KEY_PACKAGE_NAME, "Package name:", (String) null, 0);
	}
	
	@Override
	protected URL getInstallURL() {
		return Activator.getDefault().getInstallURL();
	}
	
	@Override
	public void addPages(Wizard wizard) {
		
		WizardPage page = createPage(0, getTitle());
		page.setTitle(getTitle());
		page.setDescription(getDescription());
		wizard.addPage(page);
		markPagesAdded();
	}
	
	@Override
	public boolean isDependentOnParentWizard() {
		return true;
	}
	
	@Override
	protected void initializeFields(IFieldData data) {
		// In a new project wizard, we don't know this yet - the
		// model has not been created
		String packageName = getFormattedPackageName(data.getId());
		initializeOption(KEY_PACKAGE_NAME, packageName);
	}
	
	@Override
	public void initializeFields(IPluginModelBase model) {
		String pluginId = model.getPluginBase().getId();
		initializeOption(KEY_PACKAGE_NAME, getFormattedPackageName(pluginId)); 
	}

	private String getFormattedPackageName(String name) {
		if(getSubPackageName().equals(""))
			return name + ".examples";
		else
			return name + ".examples." + getSubPackageName();
	}

	@Override
	public String getUsedExtensionPoint() {
		return "fr.irisa.r2d2.gecos.framework.modules";
	}
	
	@Override
	public void validateOptions(TemplateOption source) {
		if (source.isRequired() && source.isEmpty()) {
			flagMissingRequiredOption(source);
		} else {
			//validateContainerPage(source);
		}
	}
	
	@Override
	public String[] getNewFiles() {
		return new String[0];
	}

	protected Map<String,String> getGenerators(){
		return null;
	}
	
	@Override
	protected void updateModel(IProgressMonitor monitor) throws CoreException {
		IPluginBase plugin = model.getPluginBase();
		
		if(getModuleClass() != null){
			IPluginExtension extension = createExtension("fr.irisa.r2d2.gecos.framework.modules", true); //$NON-NLS-1$
			IPluginModelFactory factory = model.getPluginFactory();
			
			for(String key : getModuleClass().keySet()){
				String fullClassName = getStringOption(KEY_PACKAGE_NAME) + "." + getModuleClass().get(key);
				IPluginElement moduleElement = factory.createElement(extension);
				moduleElement.setName("module");
				moduleElement.setAttribute("name", key);
				moduleElement.setAttribute("class", fullClassName);
				extension.add(moduleElement);
			}
			
			if (!extension.isInTheModel())
				plugin.add(extension);		
		}
		if(getGenerators() != null){
			IPluginExtension extension = createExtension("fr.irisa.cairn.gecos.model.GecosGenerators", true); //$NON-NLS-1$
			IPluginModelFactory factory = model.getPluginFactory();
			
			for(String key : getGenerators().keySet()){
				String fullClassName = getStringOption(KEY_PACKAGE_NAME) + "." + getGenerators().get(key);
				IPluginElement moduleElement = factory.createElement(extension);
				moduleElement.setName("generator");
				moduleElement.setAttribute("extended", key);
				moduleElement.setAttribute("extension", fullClassName);
				extension.add(moduleElement);
			}
			
			if (!extension.isInTheModel())
				plugin.add(extension);		
		}	
	}

	@Override
	protected ResourceBundle getPluginResourceBundle() {
//		Bundle bundle = Platform.getBundle(Activator.PLUGIN_ID);
//		return Platform.getResourceBundle(bundle);
		return null;
	}
}