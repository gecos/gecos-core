package fr.irisa.cairn.gecos.core.ui.plugin.templates;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.pde.core.plugin.IMatchRules;
import org.eclipse.pde.core.plugin.IPluginReference;
import org.eclipse.pde.ui.templates.PluginReference;

import fr.irisa.cairn.gecos.core.ui.plugin.templates.utils.BasicCopyPluginTemplate;
import fr.irisa.cairn.gecos.model.GecosModelPlugin;

public class ExtendCGenerator extends BasicCopyPluginTemplate {
	@Override
	public IPluginReference[] getDependencies(String schemaVersion) {
		return new IPluginReference[] { new PluginReference("fr.irisa.r2d2.gecos.framework", null, IMatchRules.NONE),
				new PluginReference(GecosModelPlugin.PLUGIN_ID, null, IMatchRules.NONE),
				new PluginReference("fr.irisa.cairn.gecos.model.transforms", null, IMatchRules.NONE),
				new PluginReference("fr.irisa.cairn.gecos.model.generator.c", null, IMatchRules.NONE)
		};
	}

	@Override
	protected String getTemplateDirectory() {
		return "resources/plugin_templates"; //$NON-NLS-1$
	}

	@Override
	public String getSectionId() {
		return "extendcgenerator"; //$NON-NLS-1$
	}

	@Override
	public String getTitle() {
		return "Extend the GeCoS C generator";
	}

	@Override
	public String getDescription() {
		return "Extend the GeCoS C generator";
	}

	@Override
	protected Map<String, String> getGenerators() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("templates.xtend.GecosInstructionTemplate", "templates.xtend.CustomInstructionTemplate");
		map.put("templates.xtend.GecosTypeTemplate", "templates.xtend.CustomTypeTemplate");
		return map;
	}

	@Override
	public Map<String, String> getModuleClass() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("ReplaceWithFoo", "modules.ReplaceMultiplicationWithFooInstructionModule");
		return map;
	}

	@Override
	public String getSubPackageName() {
		return "cgenerator";
	}
}