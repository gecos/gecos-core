package tom.mapping.introspectors.dag;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EClass;

import tom.library.sl.Introspector;
import tom.mapping.IntrospectorManager;

import gecos.dag.DagPackage;

import gecos.blocks.*; 
import gecos.blocks.util.*;
import gecos.core.*; 
import gecos.core.util.*;
import gecos.dag.*; 
import gecos.dag.util.*;
import gecos.instrs.*; 
import gecos.instrs.util.*;
import gecos.types.*; 
import gecos.types.util.*;

/* PROTECTED REGION ID(introspector_imports) ENABLED START */
// protected imports
/* PROTECTED REGION END */

/**
* TOM introspector for gecos.
* -- Autogenerated by TOM mapping EMF genrator --
*/

public class DagIntrospector implements Introspector {
	
	public static final DagIntrospector INSTANCE = new DagIntrospector();
	
	static {
		IntrospectorManager.INSTANCE.register(DagPackage.eINSTANCE, INSTANCE);
	}

	/* PROTECTED REGION ID(introspector_members) ENABLED START */
	/* PROTECTED REGION END */
	
	protected DagIntrospector() {}
	
	public Object getChildAt(Object o, int i) {
		return getChildren(o)[i];
	}
	
	public int getChildCount(Object o) {
		return getChildren(o).length;
	}
	
	@SuppressWarnings("unchecked")
	public Object[] getChildren(Object arg0) {
		List<Object> l = new ArrayList<Object>();
		if (arg0 instanceof List) {
			// Children of a list are its content
			for(Object object : (List<Object>) arg0) {
				l.add(object);
			}
			return l.toArray();
		}
		return DagChildrenGetter.INSTANCE.children(arg0);
	}
	
	private static class DagChildrenGetter extends DagSwitch<Object[]> {
		public final static DagChildrenGetter INSTANCE = new DagChildrenGetter();
		
		private DagChildrenGetter(){}
		
		public Object[] children(Object i) {
			Object[] children = doSwitch((EObject) i);
			return children != null ? children : new Object[0];
		}
		
		public Object[] caseDAGInstruction(DAGInstruction o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			if (o.getNodes() != null)
				l.add(o.getNodes());
			
			/*PROTECTED REGION ID(getter_dag_DAGInstruction) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGNode(DAGNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			
			/*PROTECTED REGION ID(getter_dag_DAGNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGImmNode(DAGImmNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			
			/*PROTECTED REGION ID(getter_dag_DAGImmNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGSymbolNode(DAGSymbolNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			if (o.getSymbol() != null)
				l.add(o.getSymbol());
			
			/*PROTECTED REGION ID(getter_dag_DAGSymbolNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGCallNode(DAGCallNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			
			/*PROTECTED REGION ID(getter_dag_DAGCallNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGArrayValueNode(DAGArrayValueNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			
			/*PROTECTED REGION ID(getter_dag_DAGArrayValueNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGOpNode(DAGOpNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			
			/*PROTECTED REGION ID(getter_dag_DAGOpNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGOutNode(DAGOutNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			if (o.getSymbol() != null)
				l.add(o.getSymbol());
			
			/*PROTECTED REGION ID(getter_dag_DAGOutNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGNumberedSymbolNode(DAGNumberedSymbolNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			if (o.getSymbol() != null)
				l.add(o.getSymbol());
			if (o.getSymbolDefinition() != null)
				l.add(o.getSymbolDefinition());
			
			/*PROTECTED REGION ID(getter_dag_DAGNumberedSymbolNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGNumberedOutNode(DAGNumberedOutNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			if (o.getSymbol() != null)
				l.add(o.getSymbol());
			
			/*PROTECTED REGION ID(getter_dag_DAGNumberedOutNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGIntImmNode(DAGIntImmNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			if (o.getValue() != null)
				l.add(o.getValue());
			
			/*PROTECTED REGION ID(getter_dag_DAGIntImmNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGFloatImmNode(DAGFloatImmNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			if (o.getValue() != null)
				l.add(o.getValue());
			
			/*PROTECTED REGION ID(getter_dag_DAGFloatImmNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGStringImmNode(DAGStringImmNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			
			/*PROTECTED REGION ID(getter_dag_DAGStringImmNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGArrayNode(DAGArrayNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			
			/*PROTECTED REGION ID(getter_dag_DAGArrayNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGSimpleArrayNode(DAGSimpleArrayNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			if (o.getSymbol() != null)
				l.add(o.getSymbol());
			
			/*PROTECTED REGION ID(getter_dag_DAGSimpleArrayNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGFieldInstruction(DAGFieldInstruction o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			if (o.getField() != null)
				l.add(o.getField());
			
			/*PROTECTED REGION ID(getter_dag_DAGFieldInstruction) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGJumpNode(DAGJumpNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			
			/*PROTECTED REGION ID(getter_dag_DAGJumpNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGPatternNode(DAGPatternNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			if (o.getPattern() != null)
				l.add(o.getPattern());
			
			/*PROTECTED REGION ID(getter_dag_DAGPatternNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGVectorNode(DAGVectorNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			
			/*PROTECTED REGION ID(getter_dag_DAGVectorNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGVectorArrayAccessNode(DAGVectorArrayAccessNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			if (o.getSymbol() != null)
				l.add(o.getSymbol());
			
			/*PROTECTED REGION ID(getter_dag_DAGVectorArrayAccessNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGVectorOpNode(DAGVectorOpNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			
			/*PROTECTED REGION ID(getter_dag_DAGVectorOpNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGVectorShuffleNode(DAGVectorShuffleNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			
			/*PROTECTED REGION ID(getter_dag_DAGVectorShuffleNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGVectorExtractNode(DAGVectorExtractNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			
			/*PROTECTED REGION ID(getter_dag_DAGVectorExtractNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGVectorExpandNode(DAGVectorExpandNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			
			/*PROTECTED REGION ID(getter_dag_DAGVectorExpandNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGVectorPackNode(DAGVectorPackNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			
			/*PROTECTED REGION ID(getter_dag_DAGVectorPackNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGSSAUseNode(DAGSSAUseNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			if (o.getSymbol() != null)
				l.add(o.getSymbol());
			if (o.getDef() != null)
				l.add(o.getDef());
			
			/*PROTECTED REGION ID(getter_dag_DAGSSAUseNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGSSADefNode(DAGSSADefNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			if (o.getSymbol() != null)
				l.add(o.getSymbol());
			if (o.getSSAUses() != null)
				l.add(o.getSSAUses());
			
			/*PROTECTED REGION ID(getter_dag_DAGSSADefNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseDAGPhiNode(DAGPhiNode o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			
			/*PROTECTED REGION ID(getter_dag_DAGPhiNode) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T> T setChildren(T arg0, Object[] arg1) {
		if (arg0 instanceof List) {
			// If object is a list then content of the original list has to be replaced
			List<Object> list = (List<Object>) arg0;
			list.clear();
			for (int i = 0; i < arg1.length; i++) {
				list.add(arg1[i]);
			}
			return arg0;
		} else {
			return (T) DagChildrenSetter.INSTANCE.set(arg0, arg1);
		}
	}
	
	private static class DagChildrenSetter extends DagSwitch<Object> {
		public final static DagChildrenSetter INSTANCE = new DagChildrenSetter();
		
		private Object[] children;
		private DagChildrenSetter(){}
		
		public Object set(Object i, Object[] children) {
			this.children = children;
			return doSwitch((EObject) i);
		}
		
		public Object caseDAGInstruction(DAGInstruction o) {
			o.setType((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_dag_DAGInstruction) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGNode(DAGNode o) {
			o.setType((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_dag_DAGNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGImmNode(DAGImmNode o) {
			o.setType((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_dag_DAGImmNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGSymbolNode(DAGSymbolNode o) {
			o.setType((Type)children[0]);
			o.setSymbol((Symbol)children[1]);
		
			/*PROTECTED REGION ID(setter_dag_DAGSymbolNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGCallNode(DAGCallNode o) {
			o.setType((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_dag_DAGCallNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGArrayValueNode(DAGArrayValueNode o) {
			o.setType((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_dag_DAGArrayValueNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGOpNode(DAGOpNode o) {
			o.setType((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_dag_DAGOpNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGOutNode(DAGOutNode o) {
			o.setType((Type)children[0]);
			o.setSymbol((Symbol)children[1]);
		
			/*PROTECTED REGION ID(setter_dag_DAGOutNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGNumberedSymbolNode(DAGNumberedSymbolNode o) {
			o.setType((Type)children[0]);
			o.setSymbol((Symbol)children[1]);
			o.setSymbolDefinition((DAGNumberedOutNode)children[2]);
		
			/*PROTECTED REGION ID(setter_dag_DAGNumberedSymbolNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGNumberedOutNode(DAGNumberedOutNode o) {
			o.setType((Type)children[0]);
			o.setSymbol((Symbol)children[1]);
		
			/*PROTECTED REGION ID(setter_dag_DAGNumberedOutNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGIntImmNode(DAGIntImmNode o) {
			o.setType((Type)children[0]);
			o.setValue((IntInstruction)children[1]);
		
			/*PROTECTED REGION ID(setter_dag_DAGIntImmNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGFloatImmNode(DAGFloatImmNode o) {
			o.setType((Type)children[0]);
			o.setValue((FloatInstruction)children[1]);
		
			/*PROTECTED REGION ID(setter_dag_DAGFloatImmNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGStringImmNode(DAGStringImmNode o) {
			o.setType((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_dag_DAGStringImmNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGArrayNode(DAGArrayNode o) {
			o.setType((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_dag_DAGArrayNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGSimpleArrayNode(DAGSimpleArrayNode o) {
			o.setType((Type)children[0]);
			o.setSymbol((Symbol)children[1]);
		
			/*PROTECTED REGION ID(setter_dag_DAGSimpleArrayNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGFieldInstruction(DAGFieldInstruction o) {
			o.setType((Type)children[0]);
			o.setField((Field)children[1]);
		
			/*PROTECTED REGION ID(setter_dag_DAGFieldInstruction) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGJumpNode(DAGJumpNode o) {
			o.setType((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_dag_DAGJumpNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGPatternNode(DAGPatternNode o) {
			o.setType((Type)children[0]);
			o.setPattern((DAGInstruction)children[1]);
		
			/*PROTECTED REGION ID(setter_dag_DAGPatternNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGVectorNode(DAGVectorNode o) {
			o.setType((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_dag_DAGVectorNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGVectorArrayAccessNode(DAGVectorArrayAccessNode o) {
			o.setType((Type)children[0]);
			o.setSymbol((Symbol)children[1]);
		
			/*PROTECTED REGION ID(setter_dag_DAGVectorArrayAccessNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGVectorOpNode(DAGVectorOpNode o) {
			o.setType((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_dag_DAGVectorOpNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGVectorShuffleNode(DAGVectorShuffleNode o) {
			o.setType((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_dag_DAGVectorShuffleNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGVectorExtractNode(DAGVectorExtractNode o) {
			o.setType((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_dag_DAGVectorExtractNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGVectorExpandNode(DAGVectorExpandNode o) {
			o.setType((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_dag_DAGVectorExpandNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGVectorPackNode(DAGVectorPackNode o) {
			o.setType((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_dag_DAGVectorPackNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGSSAUseNode(DAGSSAUseNode o) {
			o.setType((Type)children[0]);
			o.setSymbol((Symbol)children[1]);
			o.setDef((DAGSSADefNode)children[2]);
		
			/*PROTECTED REGION ID(setter_dag_DAGSSAUseNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGSSADefNode(DAGSSADefNode o) {
			o.setType((Type)children[0]);
			o.setSymbol((Symbol)children[1]);
		
			/*PROTECTED REGION ID(setter_dag_DAGSSADefNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseDAGPhiNode(DAGPhiNode o) {
			o.setType((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_dag_DAGPhiNode) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
	}

	public <T> T setChildAt(T o, int i, Object obj) {
		throw new RuntimeException("Not implemented");
	}
}
