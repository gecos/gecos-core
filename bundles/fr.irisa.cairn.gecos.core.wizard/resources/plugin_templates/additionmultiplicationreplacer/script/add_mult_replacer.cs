p = CreateGecosProject("AddMultReplacer"); # Create a empty GeCoS project
AddSourceToGecosProject(p, "src-c/add_mult_replacer.c"); # Add a new C source in the project
CDTFrontend(p); # Generate the GeCoS IR from the C source added

AdditionMultiplicationReplacer(p);  # Call the script command which correspond to our GeCoS module

output(p, "c", "output"); # Generate the C source code from the GeCoS IR