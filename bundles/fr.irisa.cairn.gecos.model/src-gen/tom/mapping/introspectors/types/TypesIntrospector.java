package tom.mapping.introspectors.types;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EClass;

import tom.library.sl.Introspector;
import tom.mapping.IntrospectorManager;

import gecos.types.TypesPackage;

import gecos.blocks.*; 
import gecos.blocks.util.*;
import gecos.core.*; 
import gecos.core.util.*;
import gecos.dag.*; 
import gecos.dag.util.*;
import gecos.instrs.*; 
import gecos.instrs.util.*;
import gecos.types.*; 
import gecos.types.util.*;

/* PROTECTED REGION ID(introspector_imports) ENABLED START */
// protected imports
/* PROTECTED REGION END */

/**
* TOM introspector for gecos.
* -- Autogenerated by TOM mapping EMF genrator --
*/

public class TypesIntrospector implements Introspector {
	
	public static final TypesIntrospector INSTANCE = new TypesIntrospector();
	
	static {
		IntrospectorManager.INSTANCE.register(TypesPackage.eINSTANCE, INSTANCE);
	}

	/* PROTECTED REGION ID(introspector_members) ENABLED START */
	/* PROTECTED REGION END */
	
	protected TypesIntrospector() {}
	
	public Object getChildAt(Object o, int i) {
		return getChildren(o)[i];
	}
	
	public int getChildCount(Object o) {
		return getChildren(o).length;
	}
	
	@SuppressWarnings("unchecked")
	public Object[] getChildren(Object arg0) {
		List<Object> l = new ArrayList<Object>();
		if (arg0 instanceof List) {
			// Children of a list are its content
			for(Object object : (List<Object>) arg0) {
				l.add(object);
			}
			return l.toArray();
		}
		return TypesChildrenGetter.INSTANCE.children(arg0);
	}
	
	private static class TypesChildrenGetter extends TypesSwitch<Object[]> {
		public final static TypesChildrenGetter INSTANCE = new TypesChildrenGetter();
		
		private TypesChildrenGetter(){}
		
		public Object[] children(Object i) {
			Object[] children = doSwitch((EObject) i);
			return children != null ? children : new Object[0];
		}
		
		public Object[] caseField(Field o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getType() != null)
				l.add(o.getType());
			
			/*PROTECTED REGION ID(getter_types_Field) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseEnumerator(Enumerator o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getExpression() != null)
				l.add(o.getExpression());
			
			/*PROTECTED REGION ID(getter_types_Enumerator) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseArrayType(ArrayType o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getBase() != null)
				l.add(o.getBase());
			if (o.getSizeExpr() != null)
				l.add(o.getSizeExpr());
			
			/*PROTECTED REGION ID(getter_types_ArrayType) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] casePtrType(PtrType o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getBase() != null)
				l.add(o.getBase());
			
			/*PROTECTED REGION ID(getter_types_PtrType) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] case__Internal_ParameterTypeWrapper(__Internal_ParameterTypeWrapper o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getParameter() != null)
				l.add(o.getParameter());
			
			/*PROTECTED REGION ID(getter_types___Internal_ParameterTypeWrapper) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseFunctionType(FunctionType o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getReturnType() != null)
				l.add(o.getReturnType());
			
			/*PROTECTED REGION ID(getter_types_FunctionType) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseRecordType(RecordType o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getFields() != null)
				l.add(o.getFields());
			
			/*PROTECTED REGION ID(getter_types_RecordType) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseAliasType(AliasType o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getAlias() != null)
				l.add(o.getAlias());
			
			/*PROTECTED REGION ID(getter_types_AliasType) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseSimdType(SimdType o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getSubwordType() != null)
				l.add(o.getSubwordType());
			
			/*PROTECTED REGION ID(getter_types_SimdType) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseACIntType(ACIntType o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getBase() != null)
				l.add(o.getBase());
			if (o.getSizeExpr() != null)
				l.add(o.getSizeExpr());
			
			/*PROTECTED REGION ID(getter_types_ACIntType) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseACFixedType(ACFixedType o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getBase() != null)
				l.add(o.getBase());
			if (o.getSizeExpr() != null)
				l.add(o.getSizeExpr());
			
			/*PROTECTED REGION ID(getter_types_ACFixedType) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseACComplexType(ACComplexType o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getBaseType() != null)
				l.add(o.getBaseType());
			
			/*PROTECTED REGION ID(getter_types_ACComplexType) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
		
		public Object[] caseACChannelType(ACChannelType o) {
			List<Object> l = new ArrayList<Object>();
			if (o.getBaseType() != null)
				l.add(o.getBaseType());
			
			/*PROTECTED REGION ID(getter_types_ACChannelType) ENABLED START*/
			/*PROTECTED REGION END*/
			
			return l.toArray();
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T> T setChildren(T arg0, Object[] arg1) {
		if (arg0 instanceof List) {
			// If object is a list then content of the original list has to be replaced
			List<Object> list = (List<Object>) arg0;
			list.clear();
			for (int i = 0; i < arg1.length; i++) {
				list.add(arg1[i]);
			}
			return arg0;
		} else {
			return (T) TypesChildrenSetter.INSTANCE.set(arg0, arg1);
		}
	}
	
	private static class TypesChildrenSetter extends TypesSwitch<Object> {
		public final static TypesChildrenSetter INSTANCE = new TypesChildrenSetter();
		
		private Object[] children;
		private TypesChildrenSetter(){}
		
		public Object set(Object i, Object[] children) {
			this.children = children;
			return doSwitch((EObject) i);
		}
		
		public Object caseField(Field o) {
			o.setType((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_types_Field) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseEnumerator(Enumerator o) {
			o.setExpression((Instruction)children[0]);
		
			/*PROTECTED REGION ID(setter_types_Enumerator) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseArrayType(ArrayType o) {
			o.setBase((Type)children[0]);
			o.setSizeExpr((Instruction)children[1]);
		
			/*PROTECTED REGION ID(setter_types_ArrayType) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object casePtrType(PtrType o) {
			o.setBase((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_types_PtrType) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object case__Internal_ParameterTypeWrapper(__Internal_ParameterTypeWrapper o) {
			o.setParameter((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_types___Internal_ParameterTypeWrapper) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseFunctionType(FunctionType o) {
			o.setReturnType((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_types_FunctionType) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseAliasType(AliasType o) {
			o.setAlias((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_types_AliasType) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseSimdType(SimdType o) {
			o.setSubwordType((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_types_SimdType) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseACIntType(ACIntType o) {
			o.setBase((Type)children[0]);
			o.setSizeExpr((Instruction)children[1]);
		
			/*PROTECTED REGION ID(setter_types_ACIntType) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseACFixedType(ACFixedType o) {
			o.setBase((Type)children[0]);
			o.setSizeExpr((Instruction)children[1]);
		
			/*PROTECTED REGION ID(setter_types_ACFixedType) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseACComplexType(ACComplexType o) {
			o.setBaseType((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_types_ACComplexType) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
		
		public Object caseACChannelType(ACChannelType o) {
			o.setBaseType((Type)children[0]);
		
			/*PROTECTED REGION ID(setter_types_ACChannelType) ENABLED START*/
			/*PROTECTED REGION END*/
		
			return o;
		}
	}

	public <T> T setChildAt(T o, int i, Object obj) {
		throw new RuntimeException("Not implemented");
	}
}
