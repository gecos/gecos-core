/**
 */
package gecos.annotations.util;

import gecos.annotations.*;

import gecos.core.GecosNode;

import java.util.Map;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see gecos.annotations.AnnotationsPackage
 * @generated
 */
public class AnnotationsAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static AnnotationsPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationsAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = AnnotationsPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnnotationsSwitch<Adapter> modelSwitch =
		new AnnotationsSwitch<Adapter>() {
			@Override
			public Adapter caseIAnnotation(IAnnotation object) {
				return createIAnnotationAdapter();
			}
			@Override
			public Adapter caseStringAnnotation(StringAnnotation object) {
				return createStringAnnotationAdapter();
			}
			@Override
			public Adapter casePragmaAnnotation(PragmaAnnotation object) {
				return createPragmaAnnotationAdapter();
			}
			@Override
			public Adapter caseCommentAnnotation(CommentAnnotation object) {
				return createCommentAnnotationAdapter();
			}
			@Override
			public Adapter caseExtendedAnnotation(ExtendedAnnotation object) {
				return createExtendedAnnotationAdapter();
			}
			@Override
			public Adapter caseLivenessAnnotation(LivenessAnnotation object) {
				return createLivenessAnnotationAdapter();
			}
			@Override
			public Adapter caseSymbolDefinitionAnnotation(SymbolDefinitionAnnotation object) {
				return createSymbolDefinitionAnnotationAdapter();
			}
			@Override
			public Adapter caseIASTAnnotation(IASTAnnotation object) {
				return createIASTAnnotationAdapter();
			}
			@Override
			public Adapter caseFileLocationAnnotation(FileLocationAnnotation object) {
				return createFileLocationAnnotationAdapter();
			}
			@Override
			public Adapter caseStringToIAnnotationMap(Map.Entry<String, IAnnotation> object) {
				return createStringToIAnnotationMapAdapter();
			}
			@Override
			public Adapter caseAnnotatedElement(AnnotatedElement object) {
				return createAnnotatedElementAdapter();
			}
			@Override
			public Adapter caseGecosNode(GecosNode object) {
				return createGecosNodeAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link gecos.annotations.IAnnotation <em>IAnnotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.annotations.IAnnotation
	 * @generated
	 */
	public Adapter createIAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.annotations.StringAnnotation <em>String Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.annotations.StringAnnotation
	 * @generated
	 */
	public Adapter createStringAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.annotations.PragmaAnnotation <em>Pragma Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.annotations.PragmaAnnotation
	 * @generated
	 */
	public Adapter createPragmaAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.annotations.CommentAnnotation <em>Comment Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.annotations.CommentAnnotation
	 * @generated
	 */
	public Adapter createCommentAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.annotations.ExtendedAnnotation <em>Extended Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.annotations.ExtendedAnnotation
	 * @generated
	 */
	public Adapter createExtendedAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.annotations.LivenessAnnotation <em>Liveness Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.annotations.LivenessAnnotation
	 * @generated
	 */
	public Adapter createLivenessAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.annotations.SymbolDefinitionAnnotation <em>Symbol Definition Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.annotations.SymbolDefinitionAnnotation
	 * @generated
	 */
	public Adapter createSymbolDefinitionAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.annotations.IASTAnnotation <em>IAST Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.annotations.IASTAnnotation
	 * @generated
	 */
	public Adapter createIASTAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.annotations.FileLocationAnnotation <em>File Location Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.annotations.FileLocationAnnotation
	 * @generated
	 */
	public Adapter createFileLocationAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>String To IAnnotation Map</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createStringToIAnnotationMapAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.annotations.AnnotatedElement <em>Annotated Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.annotations.AnnotatedElement
	 * @generated
	 */
	public Adapter createAnnotatedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.core.GecosNode <em>Gecos Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.core.GecosNode
	 * @generated
	 */
	public Adapter createGecosNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //AnnotationsAdapterFactory
