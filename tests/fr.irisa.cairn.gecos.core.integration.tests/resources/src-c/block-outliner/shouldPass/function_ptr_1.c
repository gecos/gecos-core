int add(int X, int Y) {
	return X + Y;
}

int sub(int X, int Y) {
	return X - Y;
}

int mul(int X, int Y) {
	return X + Y;
}

int op(int (*pF)(int, int), int x, int y) {
	return pF(x,y);
}

int main() {

	const int x = 3;
	const int y = 7;
	int rAdd = 123;
	int rSub = 345;
	int rMul = 567;

	int (*pF)(int, int) = add;

	#pragma outline
	{
		rAdd = op(pF,x,y);
		pF = sub;
	}

	rSub = op(pF, x,y);
	pF = mul;

	#pragma outline
	{
		rMul = pF(x,y);
		pF = add;
	}


	//-- test --
	if(pF != add)
		exit(-1);
	if(rAdd != add(x,y))
		exit(-2);
	if(rSub != sub(x,y))
		exit(-3);
	if(rMul != mul(x,y))
		exit(-4);

	return 0;
}
