/**
 */
package gecos.instrs.impl;

import gecos.instrs.InstrsPackage;
import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;
import gecos.instrs.SetInstruction;

import gecos.types.Type;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Set Instruction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.impl.SetInstructionImpl#getDest <em>Dest</em>}</li>
 *   <li>{@link gecos.instrs.impl.SetInstructionImpl#getSource <em>Source</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SetInstructionImpl extends ComponentInstructionImpl implements SetInstruction {
	/**
	 * The cached value of the '{@link #getDest() <em>Dest</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDest()
	 * @generated
	 * @ordered
	 */
	protected Instruction dest;

	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected Instruction source;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SetInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstrsPackage.eINSTANCE.getSetInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getDest() {
		return dest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDest(Instruction newDest, NotificationChain msgs) {
		Instruction oldDest = dest;
		dest = newDest;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, InstrsPackage.SET_INSTRUCTION__DEST, oldDest, newDest);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDest(Instruction newDest) {
		if (newDest != dest) {
			NotificationChain msgs = null;
			if (dest != null)
				msgs = ((InternalEObject)dest).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - InstrsPackage.SET_INSTRUCTION__DEST, null, msgs);
			if (newDest != null)
				msgs = ((InternalEObject)newDest).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - InstrsPackage.SET_INSTRUCTION__DEST, null, msgs);
			msgs = basicSetDest(newDest, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InstrsPackage.SET_INSTRUCTION__DEST, newDest, newDest));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getSource() {
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSource(Instruction newSource, NotificationChain msgs) {
		Instruction oldSource = source;
		source = newSource;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, InstrsPackage.SET_INSTRUCTION__SOURCE, oldSource, newSource);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource(Instruction newSource) {
		if (newSource != source) {
			NotificationChain msgs = null;
			if (source != null)
				msgs = ((InternalEObject)source).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - InstrsPackage.SET_INSTRUCTION__SOURCE, null, msgs);
			if (newSource != null)
				msgs = ((InternalEObject)newSource).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - InstrsPackage.SET_INSTRUCTION__SOURCE, null, msgs);
			msgs = basicSetSource(newSource, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InstrsPackage.SET_INSTRUCTION__SOURCE, newSource, newSource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instruction> listChildren() {
		return ECollections.<Instruction>unmodifiableEList(ECollections.<Instruction>asEList(this.getDest(), this.getSource()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final InstrsVisitor visitor) {
		visitor.visitSetInstruction(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _xblockexpression = null;
		{
			String _elvis = null;
			Instruction _dest = this.getDest();
			String _string = null;
			if (_dest!=null) {
				_string=_dest.toString();
			}
			if (_string != null) {
				_elvis = _string;
			} else {
				_elvis = "?";
			}
			final String dst = _elvis;
			String _elvis_1 = null;
			Instruction _source = this.getSource();
			String _string_1 = null;
			if (_source!=null) {
				_string_1=_source.toString();
			}
			if (_string_1 != null) {
				_elvis_1 = _string_1;
			} else {
				_elvis_1 = "?";
			}
			final String src = _elvis_1;
			_xblockexpression = ((dst + "=") + src);
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSet() {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type computeType() {
		Instruction _dest = this.getDest();
		Type _type = null;
		if (_dest!=null) {
			_type=_dest.getType();
		}
		return _type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSame(final Instruction inst) {
		if ((inst instanceof SetInstruction)) {
			final boolean isSameDest = (((this.getDest() == null) && (((SetInstruction)inst).getDest() == null)) || this.getDest().isSame(((SetInstruction)inst).getDest()));
			final boolean isSameSource = (((this.getSource() == null) && (((SetInstruction)inst).getSource() == null)) || this.getSource().isSame(((SetInstruction)inst).getSource()));
			return ((isSameDest && isSameSource) && super.isSame(inst));
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InstrsPackage.SET_INSTRUCTION__DEST:
				return basicSetDest(null, msgs);
			case InstrsPackage.SET_INSTRUCTION__SOURCE:
				return basicSetSource(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InstrsPackage.SET_INSTRUCTION__DEST:
				return getDest();
			case InstrsPackage.SET_INSTRUCTION__SOURCE:
				return getSource();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InstrsPackage.SET_INSTRUCTION__DEST:
				setDest((Instruction)newValue);
				return;
			case InstrsPackage.SET_INSTRUCTION__SOURCE:
				setSource((Instruction)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InstrsPackage.SET_INSTRUCTION__DEST:
				setDest((Instruction)null);
				return;
			case InstrsPackage.SET_INSTRUCTION__SOURCE:
				setSource((Instruction)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InstrsPackage.SET_INSTRUCTION__DEST:
				return dest != null;
			case InstrsPackage.SET_INSTRUCTION__SOURCE:
				return source != null;
		}
		return super.eIsSet(featureID);
	}

} //SetInstructionImpl
