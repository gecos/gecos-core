/**
 */
package gecos.gecosproject;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Project Visitor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.gecosproject.GecosprojectPackage#getProjectVisitor()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ProjectVisitor extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model projectUnique="false"
	 * @generated
	 */
	void visitProject(GecosProject project);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model sourceFileUnique="false"
	 * @generated
	 */
	void visitSourceFile(GecosSourceFile sourceFile);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model headerDirectoryUnique="false"
	 * @generated
	 */
	void visitHeaderDirectory(GecosHeaderDirectory headerDirectory);

} // ProjectVisitor
