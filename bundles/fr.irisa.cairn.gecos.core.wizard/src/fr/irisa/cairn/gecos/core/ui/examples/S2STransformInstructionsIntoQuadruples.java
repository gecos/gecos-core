package fr.irisa.cairn.gecos.core.ui.examples;

import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

import fr.irisa.cairn.gecos.core.ui.Activator;
import fr.irisa.cairn.gecos.core.ui.examples.utils.BasicGecosExampleWizard;

public class S2STransformInstructionsIntoQuadruples extends BasicGecosExampleWizard {
	@Override
	public String getResourcesFolderToCopyIntoProject() {
		return "resources/wizard_templates/s2s_convert_inst_to_MIR/";
	}

	@Override
	public String getPageName() {
		return "Transform Instructions Into MIR Format Example";
	}

	@Override
	public String getPageDescription() {
		return "This wizard will create an example project for transforming an instruction into MIR format.";
	}

	@Override
	protected Bundle getPluginBundle() {
		return Platform.getBundle(Activator.PLUGIN_ID);
	}

}
