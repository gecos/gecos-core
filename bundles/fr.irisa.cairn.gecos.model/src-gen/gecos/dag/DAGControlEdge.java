/**
 */
package gecos.dag;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Control Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.DAGControlEdge#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see gecos.dag.DagPackage#getDAGControlEdge()
 * @model
 * @generated
 */
public interface DAGControlEdge extends DAGEdge, DagVisitable {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link gecos.dag.DependencyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see gecos.dag.DependencyType
	 * @see #setType(DependencyType)
	 * @see gecos.dag.DagPackage#getDAGControlEdge_Type()
	 * @model unique="false"
	 * @generated
	 */
	DependencyType getType();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGControlEdge#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see gecos.dag.DependencyType
	 * @see #getType()
	 * @generated
	 */
	void setType(DependencyType value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGControlEdge(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.dag.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.dag.DependencyType%&gt; _type = this.getType();\n&lt;%java.lang.String%&gt; _plus = (\"Mem Dependency \" + _type);\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \" to \");\n&lt;%gecos.dag.DAGNode%&gt; _sinkNode = this.getSinkNode();\nreturn (_plus_1 + _sinkNode);'"
	 * @generated
	 */
	String toString();

} // DAGControlEdge
