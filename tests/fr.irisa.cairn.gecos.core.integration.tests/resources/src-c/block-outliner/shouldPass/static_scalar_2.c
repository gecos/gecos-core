int main() {
	static int val = 4;
	int res = 2;

	#pragma outline
	{
		res = val;
	}

	if(res != 4)
		abort();

	return 0;
}
