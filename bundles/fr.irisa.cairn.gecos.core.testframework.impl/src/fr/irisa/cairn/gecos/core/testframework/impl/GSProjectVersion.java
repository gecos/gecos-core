package fr.irisa.cairn.gecos.core.testframework.impl;


import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import fr.irisa.cairn.gecos.testframework.model.IVersion;
import gecos.gecosproject.GecosHeaderDirectory;
import gecos.gecosproject.GecosProject;

/**
 * This is an implementation of {@link IVersion} associated to a
 * {@link GecosProject}.
 * 
 * @author aelmouss
 */
public class GSProjectVersion extends IVersion {

	public static final String MAIN_PROC_NAME = "main";
	public static final String EXECUTABLE_EXT = ".out";
	public static final String STDOUT_EXT = ".stdout";
	public static final String STDERR_EXT = ".stderr";
	
	/**
	 * The {@link GecosProject} represented by this version.
	 * It is used for applying {@link ITransformation}s.
	 * The source files included in {@link #gProject} must be a subset of {@link #sourceFiles}.
	 */
	protected GecosProject gProject;
	
	/**
	 * If true set the sdtout/err files of each version before running it.
	 */
	protected boolean enableStdRedirection ;
	
	public void setEnableStdRedirection(boolean enableStdRedirection) {
		this.enableStdRedirection = enableStdRedirection;
	}
	
	public boolean isEnableStdRedirection() {
		return enableStdRedirection;
	}
	
	public void setProject(GecosProject gProject) {
		this.gProject = gProject;
	}

	public GecosProject getProject() {
		return gProject;
	}
	
	/**
	 * Check whether the project contains a main procedure
	 * (i.e. matching {@value #MAIN_PROC_NAME}).
	 *
	 * @return true if this version has a main procedure
	 */
	public boolean hasMainProcedure() {
		try{
			return getProject().listProcedures().stream()
				.anyMatch(p -> p.getSymbolName().equals(MAIN_PROC_NAME));
		} catch(Exception e) {
			return false;
		}
	}

	/**
	 * Return a list of the include dirs of the project excepting gecos standard ones.
	 */
	@Override
	public List<Path> getIncludeDirs() {
		if(gProject == null)
			return null;
		if(includeDirs == null)
			setIncludeDirs(getProjectIncludeDirs(true).collect(Collectors.toList()));
		return includeDirs;
	}
	
	/**
	 * @param skipStandard
	 * @return stream of include directories of the project skipping Gecos standard 
	 * library headers if {@code skipStandard} is true.
	 */
	public Stream<Path> getProjectIncludeDirs(boolean skipStandard) {
		Stream<GecosHeaderDirectory> stream = gProject.getIncludes().stream();
		if(skipStandard)
			stream = stream.filter(h -> !h.isIsStandardHeader()); //GecosStandardIncludes.getDefault().isGecosStandardInclude(d.toString()))
		return stream
			.map(h -> Paths.get(h.getName()));
	}
	
	@Override
	public Path getExecutable() {
		if(executable == null)
			executable = getOutputDir().resolve(getName() + EXECUTABLE_EXT);
		return executable;
	}
	
	@Override
	public Path getStdoutFile() {
		if(enableStdRedirection)
			return getOutputDir().resolve(getName() + STDOUT_EXT);
		return null;
	}
	
	@Override
	public Path getStderrFile() {
		if(enableStdRedirection)
			return getOutputDir().resolve(getName() + STDERR_EXT);
		return null;
	}
}
