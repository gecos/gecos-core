#include <stdio.h>
#define M	128
#define N	128

void testComSubExpr(int in[N + 1][M + 1], int out[N + 1][M + 1]) {

	int i, j, a;

	// Loop nest without '{' and '}'

	for (i = 0; i <= N; i++)
		out[i][M - 2] = in[N - 1][M - 2] + in[N - 1][M - 2];

	for (i = 0; i <= N; i++)
		out[i][0] = in[N - 1][M - 2] - out[N - 1][M - 2];

	out[N][M] = in[N - 1][M - 2] + in[N - 1][M - 2];

	for (i = 0; i <= N; i++)
		if (M >= 100)
			for (j = 0; j <= 100; j++)
				out[i][j] = in[i][j] + i;
		else
			for (j = 0; j <= M; j++)
				out[i][j] = in[i][j] + i;

	// Loop nest with '{' and '}'
	for (i = 0; i <= N; i++) {
		a = M - 10;
		for (j = 0; j <= a; j++) {
			out[i][j] = in[i][j] + i;
		}
	}

	// if test
	if (M % 2)
		if (N % 2)
			out[M][N] = M + N;

	// while test
	i = N - 1;
	while (i > 0) {
		j = M - 2;
		while (j > 1) {
			out[i - 1][j] = in[i - 1][j - 2] - in[i - 1][j - 1];
			out[i][j] = in[i - 1][j - 2] - in[i - 1][j - 1];
			out[i][j] = in[i + 1][j + 1] - in[i + 1][j + 2];
			out[i][j] += in[i][j];
			j--;
		}
		i--;
	}

	// do while test
	i = N - 1;
	do {
		j = M - 2;
		do {
			out[i][j] = in[i - 1][j - 2] - in[i + 1][j + 2];
			j--;
		} while (j > 1);
		i--;
	} while (i > 0);

}

