project = CreateGecosProject("tutorial"); # Just create a GeCoS project
AddSourceToGecosProject(project, "src/ex1.c"); # Add source to the GeCoS project
CDTFrontend(project); # Call the GeCoS Front-end and build the GeCoS IR corresponding to the project

output(project, "c", "src-regen"); # Generate C source code from the IR