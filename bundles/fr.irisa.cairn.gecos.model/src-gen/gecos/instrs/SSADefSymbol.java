/**
 */
package gecos.instrs;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SSA Def Symbol</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.SSADefSymbol#getSSAUses <em>SSA Uses</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getSSADefSymbol()
 * @model
 * @generated
 */
public interface SSADefSymbol extends NumberedSymbolInstruction {
	/**
	 * Returns the value of the '<em><b>SSA Uses</b></em>' reference list.
	 * The list contents are of type {@link gecos.instrs.SSAUseSymbol}.
	 * It is bidirectional and its opposite is '{@link gecos.instrs.SSAUseSymbol#getDef <em>Def</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SSA Uses</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SSA Uses</em>' reference list.
	 * @see gecos.instrs.InstrsPackage#getSSADefSymbol_SSAUses()
	 * @see gecos.instrs.SSAUseSymbol#getDef
	 * @model opposite="def"
	 * @generated
	 */
	EList<SSAUseSymbol> getSSAUses();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitSSADefSymbol(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _elvis = null;\n&lt;%gecos.core.Symbol%&gt; _symbol = this.getSymbol();\n&lt;%java.lang.String%&gt; _name = null;\nif (_symbol!=null)\n{\n\t_name=_symbol.getName();\n}\nif (_name != null)\n{\n\t_elvis = _name;\n} else\n{\n\t_elvis = \"null\";\n}\n&lt;%java.lang.String%&gt; _plus = (\"def(\" + _elvis);\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \"_\");\nint _number = this.getNumber();\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + &lt;%java.lang.Integer%&gt;.valueOf(_number));\n&lt;%java.lang.String%&gt; _plus_3 = (_plus_2 + \":\");\n&lt;%java.lang.Object%&gt; _elvis_1 = null;\n&lt;%gecos.core.Symbol%&gt; _symbol_1 = this.getSymbol();\n&lt;%gecos.types.Type%&gt; _type = null;\nif (_symbol_1!=null)\n{\n\t_type=_symbol_1.getType();\n}\nif (_type != null)\n{\n\t_elvis_1 = _type;\n} else\n{\n\t_elvis_1 = \"\";\n}\n&lt;%java.lang.String%&gt; _plus_4 = (_plus_3 + _elvis_1);\nreturn (_plus_4 + \")\");'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.SSADefSymbol%&gt;))\n{\n\treturn super.isSame(instr);\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // SSADefSymbol
