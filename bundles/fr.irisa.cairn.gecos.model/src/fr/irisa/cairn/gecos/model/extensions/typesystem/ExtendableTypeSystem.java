package fr.irisa.cairn.gecos.model.extensions.typesystem;

import gecos.instrs.Instruction;
import gecos.types.Type;

import java.util.List;

public class ExtendableTypeSystem {

	private TypeSystemDispatchStrategy dispatchStrategy;
	private List<ITypeSystemRuleSet> allGeneratorExtensionsFor;
	
	public ExtendableTypeSystem(TypeSystemDispatchStrategy dispatchStrategy,ITypeSystemRuleSet extended) {
		this.dispatchStrategy = dispatchStrategy;
		//Get all registered generators for the extended generator
		allGeneratorExtensionsFor = GecosTypeManagerRegistry.INSTANCE.getAllTypeSystemExtensionsFor(extended);
		allGeneratorExtensionsFor.add(0,extended);
	}

	public ExtendableTypeSystem(ITypeSystemRuleSet extended) {
		this(new TypeSystemDispatchStrategy(),extended);
	}

	public Type computeType(Instruction o) {
		return dispatchStrategy.dispatch(allGeneratorExtensionsFor, o);
	};
}