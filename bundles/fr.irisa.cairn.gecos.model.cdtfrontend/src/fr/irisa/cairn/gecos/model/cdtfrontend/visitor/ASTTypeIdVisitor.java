/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.cdtfrontend.visitor;

import org.eclipse.cdt.core.dom.ast.IASTDeclSpecifier;
import org.eclipse.cdt.core.dom.ast.IASTDeclarator;
import org.eclipse.cdt.core.dom.ast.IASTProblemTypeId;
import org.eclipse.cdt.core.dom.ast.IASTTypeId;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTTypeId;

import fr.irisa.cairn.gecos.model.cdtfrontend.exception.CDTParserErrorException;
import gecos.types.Type;

public class ASTTypeIdVisitor {

	private CDTToGecosModel gecosModel;
	private String string;
	private Type type;

	public ASTTypeIdVisitor() {
	}
	
	public void visit(CDTToGecosModel gecosModel, IASTTypeId typeId) {
		this.gecosModel = gecosModel;
		if (typeId instanceof IASTProblemTypeId)
			buildIASTProblemTypeId((IASTProblemTypeId)typeId);
		else if (typeId instanceof ICPPASTTypeId)
			buildICPPASTTypeId((ICPPASTTypeId)typeId);
		else
			buildIASTTypeId(typeId);
		
		gecosModel.addFileLocationAnnotation(type, typeId.getFileLocation());
	}
	
	public String getString() {
		return string;
	}
	
	public Type getType() {
		return type;
	}
	
	private void buildIASTProblemTypeId(IASTProblemTypeId typeId) {
		throw new CDTParserErrorException(typeId, "Parse error in the file " + typeId.getContainingFilename() + " at line  : " + typeId.getFileLocation().getStartingLineNumber() + 
				"\n\tCDT could not parse \"" + typeId.getRawSignature() + "\" : " + typeId.getProblem().getMessage());
	}
	
	private void buildICPPASTTypeId(ICPPASTTypeId typeId) {
		buildIASTTypeId(typeId);
	}
	
	private void buildIASTTypeId(IASTTypeId typeId) {
		IASTDeclSpecifier declSpecifier = typeId.getDeclSpecifier();
		declSpecifier.accept(gecosModel);
		type = ((ASTDeclSpecifierVisitor)gecosModel.getObject()).getType();
		IASTDeclarator abstractDeclarator = typeId.getAbstractDeclarator();
		abstractDeclarator.accept(gecosModel);
		ASTDeclaratorVisitor declaratorVisitor = (ASTDeclaratorVisitor)gecosModel.getObject();
		
		
//		for (int i = 0; i < abstractDeclarator.getPointerOperators().length; i++)
//			type = GecosUserTypeFactory.PTR(type);
//
//		for(Instruction instruction : declaratorVisitor.getArrayModifiers()) {
//			type = GecosUserTypeFactory.ARRAY(type, instruction);
//		}
//		
//		if (abstractDeclarator.getNestedDeclarator() != null && abstractDeclarator.getNestedDeclarator().isActive()) {
//			IASTPointerOperator[] pointerOperators = abstractDeclarator.getNestedDeclarator().getPointerOperators();
//			for (int j = 0; j < pointerOperators.length; j++) {
//				type = GecosUserTypeFactory.PTR(type);
//				
//				if(pointerOperators[j] instanceof IASTPointer) {
//					IASTPointer po = (IASTPointer)pointerOperators[j];
//					type.setConstant(po.isConst());
//					type.setVolatile(po.isVolatile());
//					//TODO restrict ??
//				}
//			}
//		}
		
		type = declaratorVisitor.buildType(type);
	}
}
