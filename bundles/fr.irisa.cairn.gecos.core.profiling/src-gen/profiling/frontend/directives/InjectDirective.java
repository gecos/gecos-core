/**
 */
package profiling.frontend.directives;

import gecos.blocks.Block;

import gecos.instrs.Instruction;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Inject Directive</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link profiling.frontend.directives.InjectDirective#getNAME <em>NAME</em>}</li>
 *   <li>{@link profiling.frontend.directives.InjectDirective#getAddress <em>Address</em>}</li>
 *   <li>{@link profiling.frontend.directives.InjectDirective#getSourceDirective <em>Source Directive</em>}</li>
 *   <li>{@link profiling.frontend.directives.InjectDirective#getSizesOutermostFirst <em>Sizes Outermost First</em>}</li>
 * </ul>
 *
 * @see profiling.frontend.directives.DirectivesPackage#getInjectDirective()
 * @model
 * @generated
 */
public interface InjectDirective extends Directive {
	/**
	 * Returns the value of the '<em><b>NAME</b></em>' attribute.
	 * The default value is <code>"$inject"</code>.
	 * The literals are from the enumeration {@link profiling.frontend.directives.DirectiveName}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>NAME</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>NAME</em>' attribute.
	 * @see profiling.frontend.directives.DirectiveName
	 * @see #setNAME(DirectiveName)
	 * @see profiling.frontend.directives.DirectivesPackage#getInjectDirective_NAME()
	 * @model default="$inject" unique="false"
	 * @generated
	 */
	DirectiveName getNAME();

	/**
	 * Sets the value of the '{@link profiling.frontend.directives.InjectDirective#getNAME <em>NAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>NAME</em>' attribute.
	 * @see profiling.frontend.directives.DirectiveName
	 * @see #getNAME()
	 * @generated
	 */
	void setNAME(DirectiveName value);

	/**
	 * Returns the value of the '<em><b>Address</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Address</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Address</em>' reference.
	 * @see #setAddress(Instruction)
	 * @see profiling.frontend.directives.DirectivesPackage#getInjectDirective_Address()
	 * @model
	 * @generated
	 */
	Instruction getAddress();

	/**
	 * Sets the value of the '{@link profiling.frontend.directives.InjectDirective#getAddress <em>Address</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Address</em>' reference.
	 * @see #getAddress()
	 * @generated
	 */
	void setAddress(Instruction value);

	/**
	 * Returns the value of the '<em><b>Source Directive</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Directive</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Directive</em>' containment reference.
	 * @see #setSourceDirective(InjectorSourceDirective)
	 * @see profiling.frontend.directives.DirectivesPackage#getInjectDirective_SourceDirective()
	 * @model containment="true"
	 * @generated
	 */
	InjectorSourceDirective getSourceDirective();

	/**
	 * Sets the value of the '{@link profiling.frontend.directives.InjectDirective#getSourceDirective <em>Source Directive</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Directive</em>' containment reference.
	 * @see #getSourceDirective()
	 * @generated
	 */
	void setSourceDirective(InjectorSourceDirective value);

	/**
	 * Returns the value of the '<em><b>Sizes Outermost First</b></em>' reference list.
	 * The list contents are of type {@link gecos.instrs.Instruction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sizes Outermost First</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sizes Outermost First</em>' reference list.
	 * @see profiling.frontend.directives.DirectivesPackage#getInjectDirective_SizesOutermostFirst()
	 * @model
	 * @generated
	 */
	EList<Instruction> getSizesOutermostFirst();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; args = this.getCall().getArgs();\nint _size = args.size();\nboolean _lessThan = (_size &lt; 2);\nif (_lessThan)\n{\n\t&lt;%java.lang.String%&gt; _printLocation = this.printLocation();\n\t&lt;%java.lang.String%&gt; _plus = (_printLocation + \"\\nMust have at least 2 arguments (address, injection source)!\");\n\tthrow new &lt;%fr.irisa.cairn.gecos.core.profiling.frontend.InvalidDirective%&gt;(_plus);\n}\nthis.setAddress(&lt;%java.util.Objects%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;requireNonNull(args.get(0)));\n&lt;%gecos.instrs.CallInstruction%&gt; _xtrycatchfinallyexpression = null;\ntry\n{\n\t&lt;%gecos.instrs.Instruction%&gt; _get = args.get(1);\n\t_xtrycatchfinallyexpression = ((&lt;%gecos.instrs.CallInstruction%&gt;) _get);\n}\ncatch (final Throwable _t) {\n\tif (_t instanceof &lt;%java.lang.Exception%&gt;) {\n\t\t&lt;%java.lang.String%&gt; _printLocation_1 = this.printLocation();\n\t\t&lt;%java.lang.String%&gt; _plus_1 = (_printLocation_1 + \"\\nMust have an injector directive as second argument \\n\\t\\t\\t\\t($from_file(filePath) | $from_var(var) | $random_uniform(min,max) | $random_normal(mean,dev))\");\n\t\tthrow new &lt;%fr.irisa.cairn.gecos.core.profiling.frontend.InvalidDirective%&gt;(_plus_1);\n\t}\n\telse\n\t{\n\t\tthrow &lt;%org.eclipse.xtext.xbase.lib.Exceptions%&gt;.sneakyThrow(_t);\n\t}\n}\nfinal &lt;%gecos.instrs.CallInstruction%&gt; source = _xtrycatchfinallyexpression;\nfinal &lt;%java.lang.String%&gt; name = source.getProcedureSymbol().getName().toLowerCase();\n&lt;%profiling.frontend.directives.InjectorSourceDirective%&gt; _xifexpression = null;\n&lt;%java.lang.String%&gt; _literal = &lt;%profiling.frontend.directives.DirectiveName%&gt;.FROM_FILE.getLiteral();\nboolean _equals = &lt;%com.google.common.base.Objects%&gt;.equal(name, _literal);\nif (_equals)\n{\n\t_xifexpression = &lt;%profiling.frontend.directives.DirectivesFactory%&gt;.eINSTANCE.createFromFileDirective();\n}\nelse\n{\n\t&lt;%profiling.frontend.directives.InjectorSourceDirective%&gt; _xifexpression_1 = null;\n\t&lt;%java.lang.String%&gt; _literal_1 = &lt;%profiling.frontend.directives.DirectiveName%&gt;.FROM_VAR.getLiteral();\n\tboolean _equals_1 = &lt;%com.google.common.base.Objects%&gt;.equal(name, _literal_1);\n\tif (_equals_1)\n\t{\n\t\t_xifexpression_1 = &lt;%profiling.frontend.directives.DirectivesFactory%&gt;.eINSTANCE.createFromVariableDirective();\n\t}\n\telse\n\t{\n\t\t&lt;%profiling.frontend.directives.RandomUniformDirective%&gt; _xifexpression_2 = null;\n\t\t&lt;%java.lang.String%&gt; _literal_2 = &lt;%profiling.frontend.directives.DirectiveName%&gt;.RANDOM_UNIFORM.getLiteral();\n\t\tboolean _equals_2 = &lt;%com.google.common.base.Objects%&gt;.equal(name, _literal_2);\n\t\tif (_equals_2)\n\t\t{\n\t\t\t_xifexpression_2 = &lt;%profiling.frontend.directives.DirectivesFactory%&gt;.eINSTANCE.createRandomUniformDirective();\n\t\t}\n\t\telse\n\t\t{\n\t\t\t&lt;%profiling.frontend.directives.RandomUniformDirective%&gt; _xifexpression_3 = null;\n\t\t\t&lt;%java.lang.String%&gt; _literal_3 = &lt;%profiling.frontend.directives.DirectiveName%&gt;.RANDOM_NORMAL.getLiteral();\n\t\t\tboolean _equals_3 = &lt;%com.google.common.base.Objects%&gt;.equal(name, _literal_3);\n\t\t\tif (_equals_3)\n\t\t\t{\n\t\t\t\t_xifexpression_3 = &lt;%profiling.frontend.directives.DirectivesFactory%&gt;.eINSTANCE.createRandomUniformDirective();\n\t\t\t}\n\t\t\telse\n\t\t\t{\n\t\t\t\t&lt;%java.lang.String%&gt; _printLocation_2 = this.printLocation();\n\t\t\t\t&lt;%java.lang.String%&gt; _plus_2 = (_printLocation_2 + \"\\nMust have an injector directive as second argument \\n\\t\\t\\t\\t($from_file(filePath) | $from_var(var) | $random_uniform(min,max) | $random_normal(mean,dev))\");\n\t\t\t\tthrow new &lt;%fr.irisa.cairn.gecos.core.profiling.frontend.InvalidDirective%&gt;(_plus_2);\n\t\t\t}\n\t\t\t_xifexpression_2 = _xifexpression_3;\n\t\t}\n\t\t_xifexpression_1 = _xifexpression_2;\n\t}\n\t_xifexpression = _xifexpression_1;\n}\nthis.setSourceDirective(_xifexpression);\n&lt;%profiling.frontend.directives.InjectorSourceDirective%&gt; _sourceDirective = this.getSourceDirective();\n_sourceDirective.setCall(source);\nthis.getSourceDirective().parse();\n&lt;%gecos.types.Type%&gt; _type = this.getAddress().getType();\nfinal &lt;%fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer%&gt; ta = new &lt;%fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer%&gt;(_type, true);\nint _totalNbDims = ta.getTotalNbDims();\n&lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt; _doubleDotLessThan = new &lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt;(0, _totalNbDims, true);\nfor (final &lt;%java.lang.Integer%&gt; dim : _doubleDotLessThan)\n{\n\t{\n\t\t&lt;%gecos.instrs.Instruction%&gt; _xifexpression_4 = null;\n\t\tint _size_1 = args.size();\n\t\tboolean _greaterThan = (_size_1 &gt; ((dim).intValue() + 2));\n\t\tif (_greaterThan)\n\t\t{\n\t\t\t_xifexpression_4 = args.get((2 + (dim).intValue()));\n\t\t}\n\t\telse\n\t\t{\n\t\t\t_xifexpression_4 = ta.tryGetDimSize((dim).intValue());\n\t\t}\n\t\tfinal &lt;%gecos.instrs.Instruction%&gt; sz = _xifexpression_4;\n\t\tif ((sz == null))\n\t\t{\n\t\t\t&lt;%java.lang.String%&gt; _printLocation_3 = this.printLocation();\n\t\t\t&lt;%java.lang.String%&gt; _plus_3 = (_printLocation_3 + \"\\nMust specify a size for dimension \\\'\");\n\t\t\t&lt;%java.lang.String%&gt; _plus_4 = (_plus_3 + dim);\n\t\t\t&lt;%java.lang.String%&gt; _plus_5 = (_plus_4 + \"\\\' which couldn\\\'t be determined automatically!\");\n\t\t\tthrow new &lt;%fr.irisa.cairn.gecos.core.profiling.frontend.InvalidDirective%&gt;(_plus_5);\n\t\t}\n\t\tthis.getSizesOutermostFirst().add(sz);\n\t}\n}'"
	 * @generated
	 */
	void parse();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.core.ProcedureSet%&gt; ps = this.getCall().getContainingProcedureSet();\nfinal &lt;%gecos.blocks.CompositeBlock%&gt; cb = &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory%&gt;.CompositeBlock();\n&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory%&gt;.comment(cb, this.getCall().toString());\n&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory%&gt;.setScope(cb.getScope());\ncb.addChildren(this.getSourceDirective().createCodeBlock());\nfinal &lt;%gecos.blocks.BasicBlock%&gt; innermostBody = &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory%&gt;.BBlock();\n&lt;%gecos.blocks.Block%&gt; currentBlock = ((&lt;%gecos.blocks.Block%&gt;) innermostBody);\nfinal &lt;%java.util.LinkedList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; indexesOutermostFirst = new &lt;%java.util.LinkedList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;();\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; _reverseView = &lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;reverseView(this.getSizesOutermostFirst());\nfor (final &lt;%gecos.instrs.Instruction%&gt; dimSize : _reverseView)\n{\n\t{\n\t\tfinal &lt;%gecos.core.Symbol%&gt; index = &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory%&gt;.symbol(\"i\", &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory%&gt;.INT(), cb.getScope());\n\t\tcb.getScope().makeUnique(index);\n\t\tindexesOutermostFirst.addFirst(&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory%&gt;.symbref(index));\n\t\tcurrentBlock = &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory%&gt;.For(index, 0, dimSize.copy(), 1, currentBlock);\n\t}\n}\n&lt;%gecos.instrs.Instruction%&gt; _xifexpression = null;\nboolean _isEmpty = indexesOutermostFirst.isEmpty();\nif (_isEmpty)\n{\n\t_xifexpression = this.getAddress();\n}\nelse\n{\n\t_xifexpression = &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory%&gt;.array(this.getAddress(), indexesOutermostFirst);\n}\nfinal &lt;%gecos.instrs.Instruction%&gt; dest = _xifexpression;\nfinal &lt;%gecos.instrs.CallInstruction%&gt; value = &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory%&gt;.call(&lt;%fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport%&gt;.getNativeProc(ps, &lt;%fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport.NativeFunction%&gt;.INJECT_VALUE));\nfinal &lt;%gecos.instrs.SetInstruction%&gt; injectValue = &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory%&gt;.set(dest, value);\ninnermostBody.addInstruction(injectValue);\ncb.addChildren(currentBlock);\nreturn cb;'"
	 * @generated
	 */
	Block createCodeBlock();

} // InjectDirective
