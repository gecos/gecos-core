/**
 */
package gecos.annotations.impl;

import gecos.annotations.AnnotationsPackage;
import gecos.annotations.CommentAnnotation;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Comment Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CommentAnnotationImpl extends StringAnnotationImpl implements CommentAnnotation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommentAnnotationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnnotationsPackage.Literals.COMMENT_ANNOTATION;
	}

} //CommentAnnotationImpl
