package fr.irisa.cairn.gecos.model.tools.visitors;

import gecos.blocks.BlocksVisitor;
import gecos.instrs.InstrsVisitor;

public interface IBlocksInstructionVisitor extends BlocksVisitor,InstrsVisitor {

}
