package fr.irisa.cairn.gecos.model.tools.switches;

import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch;
import gecos.blocks.CompositeBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.ScopeContainer;
import gecos.core.Symbol;
import gecos.core.util.CoreSwitch;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;

public class SymbolSwitch<T> extends CoreSwitch<T> {
	private ScopeContainerVisitor blockVisitor;
	
	public SymbolSwitch() {
		blockVisitor = new ScopeContainerVisitor(this);
		
	}
	
	private class ScopeContainerVisitor extends BlocksDefaultSwitch<T> {
		SymbolSwitch<T> symVisitor;
		
		public ScopeContainerVisitor(SymbolSwitch<T> symvisitor) {
			symVisitor=symvisitor;
		}

		@Override
		public T caseCompositeBlock(CompositeBlock b) {
			symVisitor.caseScopeContainer(b);
			return super.caseCompositeBlock(b);
		}

	}
	
	@Override
	public T caseProcedureSymbol(ProcedureSymbol object) {
		caseScopeContainer(object);
		return super.caseProcedureSymbol(object);
	}
	
	@Override
	public T caseScopeContainer(ScopeContainer object) {
		Scope scope = object.getScope();
		EList<Symbol> symbols = scope.getSymbols();
		List<Symbol> syms =  new ArrayList<Symbol>(symbols);
		for (Symbol sym : syms) {
			doSwitch(sym);
		}
		return super.caseScopeContainer(object);
	}

	@Override
	public T caseProcedureSet(ProcedureSet object) {
		caseScopeContainer(object);
		for (Procedure proc: object.listProcedures()) {
			doSwitch(proc);
		}
		return super.caseProcedureSet(object);
	}

	@Override
	public T caseProcedure(Procedure object) {
		blockVisitor.doSwitch(object.getBody());
		return super.caseProcedure(object);
	}

	

	
}
