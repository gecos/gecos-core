/**
 */
package gecos.types;

import gecos.core.ITypedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alias Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.AliasType#getAlias <em>Alias</em>}</li>
 *   <li>{@link gecos.types.AliasType#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see gecos.types.TypesPackage#getAliasType()
 * @model
 * @generated
 */
public interface AliasType extends Type, ITypedElement, TypesVisitable {
	/**
	 * Returns the value of the '<em><b>Alias</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alias</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alias</em>' reference.
	 * @see #setAlias(Type)
	 * @see gecos.types.TypesPackage#getAliasType_Alias()
	 * @model resolveProxies="false"
	 * @generated
	 */
	Type getAlias();

	/**
	 * Sets the value of the '{@link gecos.types.AliasType#getAlias <em>Alias</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alias</em>' reference.
	 * @see #getAlias()
	 * @generated
	 */
	void setAlias(Type value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see gecos.types.TypesPackage#getAliasType_Name()
	 * @model unique="false" dataType="gecos.types.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link gecos.types.AliasType#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getAlias();'"
	 * @generated
	 */
	Type getType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model tUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.setAlias(t);'"
	 * @generated
	 */
	void setType(Type t);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitAliasType(this);'"
	 * @generated
	 */
	void accept(TypesVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _eIsProxy = this.eIsProxy();\nif (_eIsProxy)\n{\n\treturn super.toString();\n}\nfinal &lt;%java.lang.StringBuffer%&gt; result = new &lt;%java.lang.StringBuffer%&gt;();\nresult.append(this.getName()).append(\"(alias \").append(this.getAlias()).append(\")\");\nreturn result.toString();'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.types.long" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getAlias().getSize();'"
	 * @generated
	 */
	long getSize();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.boolean" unique="false" otherUnique="false" ignoreStorageClassDataType="gecos.types.boolean" ignoreStorageClassUnique="false" ignoreConstDataType="gecos.types.boolean" ignoreConstUnique="false" ignoreVolatileDataType="gecos.types.boolean" ignoreVolatileUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((other instanceof &lt;%gecos.types.AliasType%&gt;))\n{\n\tfinal &lt;%gecos.types.AliasType%&gt; at = ((&lt;%gecos.types.AliasType%&gt;)other);\n\treturn (this.getName().equals(at.getName()) &amp;&amp; super.isEqual(at, ignoreStorageClass, ignoreConst, ignoreVolatile));\n}\nelse\n{\n\treturn false;\n}'"
	 * @generated
	 */
	boolean isEqual(Type other, boolean ignoreStorageClass, boolean ignoreConst, boolean ignoreVolatile);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.boolean" unique="false" otherUnique="false" ignoreStorageClassDataType="gecos.types.boolean" ignoreStorageClassUnique="false" ignoreConstDataType="gecos.types.boolean" ignoreConstUnique="false" ignoreVolatileDataType="gecos.types.boolean" ignoreVolatileUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((other == this))\n{\n\treturn true;\n}\n&lt;%gecos.types.Type%&gt; _alias = this.getAlias();\nboolean _tripleNotEquals = (_alias != null);\nif (_tripleNotEquals)\n{\n\treturn this.getAlias().isSimilar(other, ignoreStorageClass, ignoreConst, ignoreVolatile);\n}\nelse\n{\n\tfinal &lt;%gecos.types.AliasType%&gt; at = other.asAlias();\n\tif ((at == null))\n\t{\n\t\treturn false;\n\t}\n\t&lt;%gecos.types.Type%&gt; _alias_1 = at.getAlias();\n\tboolean _tripleEquals = (_alias_1 == null);\n\tif (_tripleEquals)\n\t{\n\t\treturn (this.getName().equals(at.getName()) &amp;&amp; super.isEqual(other, ignoreStorageClass, ignoreConst, ignoreVolatile));\n\t}\n\treturn false;\n}'"
	 * @generated
	 */
	boolean isSimilar(Type other, boolean ignoreStorageClass, boolean ignoreConst, boolean ignoreVolatile);

} // AliasType
