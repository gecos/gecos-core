/**
 */
package gecos.blocks;

import gecos.instrs.ContinueInstruction;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Continuable</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.blocks.BlocksPackage#getContinuable()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface Continuable extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.instrs.ContinueInstruction%&gt;, &lt;%gecos.blocks.BasicBlock%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.instrs.ContinueInstruction%&gt;, &lt;%gecos.blocks.BasicBlock%&gt;&gt;()\n{\n\tpublic &lt;%gecos.blocks.BasicBlock%&gt; apply(final &lt;%gecos.instrs.ContinueInstruction%&gt; it)\n\t{\n\t\treturn it.getBasicBlock();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt;asEList(((&lt;%gecos.blocks.BasicBlock%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt;filterNull(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%gecos.instrs.ContinueInstruction%&gt;, &lt;%gecos.blocks.BasicBlock%&gt;&gt;map(this.listContinueInstructions(), _function)), &lt;%gecos.blocks.BasicBlock%&gt;.class))));'"
	 * @generated
	 */
	EList<BasicBlock> listContinueBlocks();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EList<ContinueInstruction> listContinueInstructions();

} // Continuable
