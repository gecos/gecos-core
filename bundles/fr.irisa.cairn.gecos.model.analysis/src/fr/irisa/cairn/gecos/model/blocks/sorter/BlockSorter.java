/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.blocks.sorter;

import fr.irisa.cairn.gecos.model.analysis.dataflow.dominator.DominatorAnalyser;
import fr.irisa.cairn.gecos.model.analysis.dataflow.dominator.DominatorTree;
import fr.irisa.cairn.gecos.model.analysis.dataflow.dominator.DominatorTreeNode;
import fr.irisa.cairn.gecos.model.tools.controlflow.BuildControlFlow;
import gecos.blocks.BasicBlock;
import gecos.blocks.ControlEdge;
import gecos.core.Procedure;
import gecos.instrs.BranchType;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Vector;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;


public class BlockSorter {

	private Procedure proc;

	private static final int MARK      = 1;
	private static final int LOOP_BODY = 2;
	private static final int BACK_EDGE = 4;

	private Vector<BasicBlock> order;

	public BlockSorter(Procedure proc) {
		this.proc = proc;
	}

	public BasicBlock[] blocks() {
		return (BasicBlock[]) order.toArray(new BasicBlock[order.size()]);
	}

	public boolean compute() {
		new BuildControlFlow(proc).compute();

		DominatorAnalyser domin = new DominatorAnalyser(proc);
		DominatorTree tree = domin.getDominatorTree();

		// a back edge is an edge whose head dominate its tail
		EList<ControlEdge> backEdges = new BasicEList<ControlEdge>();
		for (BasicBlock b  : proc.getBasicBlocks()) {
			for (ControlEdge e : b.getOutEdges()) {
				e.removeFlag(MARK | LOOP_BODY | BACK_EDGE);
				
				e.getTo().removeFlag(MARK);
				
				DominatorTreeNode from = tree.node(e.getFrom());
				DominatorTreeNode to = tree.node(e.getTo());

				while (from != null && ! from.equals(to))
					from = from.parent();
				if (from != null) backEdges.add(e);
			}
		}
	
		// found loop body entry edge
		for (int j = 0; j < backEdges.size(); ++j) {
			ControlEdge back = (ControlEdge) backEdges.get(j);
			back.addFlag(BACK_EDGE);
			Set<BasicBlock> loop = new LinkedHashSet<BasicBlock>();
			Vector<BasicBlock> stack = new Vector<BasicBlock>();
			loop.add(back.getTo());
			loop.add(back.getFrom());
			if (back.getFrom() != back.getTo()) stack.add(back.getFrom());
			while (stack.size() != 0) {
				BasicBlock b = (BasicBlock) stack.remove(0);
				for (ControlEdge e : b.getInEdges()) {
					if (! loop.contains(e.getFrom())) {
						loop.add(e.getFrom());
						stack.add(e.getFrom());
					}
					if (e.getFrom().equals(back.getTo())
							&& (e.getCond() != BranchType.IF_TRUE|| e.getCond() != BranchType.IF_FALSE))
						e.addFlag(LOOP_BODY);
				}
			}
		}

		// do the block sorting
		Vector<BasicBlock> stack = new Vector<BasicBlock>();
		order = new Vector<BasicBlock>();
		stack.add(proc.getStart());
		while(stack.size() > 0) {
			BasicBlock b = (BasicBlock) stack.remove(0);

			// check the in edges
			boolean ok = true;
			
			for (ControlEdge e : b.getInEdges()) {
				if (e.isSet(BACK_EDGE)) continue;
				if (! e.isSet(MARK)) {
					// dont put at the end to maximize
					// the depth first
					if(stack.size()!=0) 
						stack.insertElementAt(b, 1);
					
					ok = false;
					break;
				}
			}
			if (! ok) continue;
			
			order.add(b);

			BasicBlock loop = null;
			for (ControlEdge e : b.getOutEdges()) {
			
				if(e.isSet(MARK)) continue;
				e.addFlag(MARK);
				if (! e.getTo().isSet(MARK)) {
					if (e.isSet(LOOP_BODY))
						loop = e.getTo();
					else
						// do a depth search
						stack.insertElementAt(e.getTo(), 0);
					e.getTo().addFlag(MARK);
				}
			}
			// make sure loop body is visited first
			if (loop != null) stack.insertElementAt(loop, 0);
		}
		return false;
	}
}
