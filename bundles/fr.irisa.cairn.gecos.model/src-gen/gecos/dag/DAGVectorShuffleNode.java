/**
 */
package gecos.dag;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Vector Shuffle Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.DAGVectorShuffleNode#getPermutation <em>Permutation</em>}</li>
 * </ul>
 *
 * @see gecos.dag.DagPackage#getDAGVectorShuffleNode()
 * @model
 * @generated
 */
public interface DAGVectorShuffleNode extends DAGVectorNode, DagVisitable {
	/**
	 * Returns the value of the '<em><b>Permutation</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Permutation</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Permutation</em>' attribute list.
	 * @see gecos.dag.DagPackage#getDAGVectorShuffleNode_Permutation()
	 * @model default="0" unique="false" dataType="gecos.dag.int"
	 * @generated
	 */
	EList<Integer> getPermutation();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGVectorShuffleNode(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

} // DAGVectorShuffleNode
