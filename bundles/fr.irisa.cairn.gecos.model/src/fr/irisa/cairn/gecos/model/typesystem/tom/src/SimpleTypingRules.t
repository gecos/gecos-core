package fr.irisa.cairn.gecos.model.typesystem;

import java.util.Arrays;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.extensions.typesystem.ITypeSystemRuleSet;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import gecos.blocks.Block;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.InstrsPackage;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.PhiInstruction;
import gecos.instrs.ReductionOperator;
import gecos.instrs.RetInstruction;
import gecos.instrs.SSADefSymbol;
import gecos.instrs.SSAUseSymbol;
import gecos.instrs.SelectOperator;
import gecos.instrs.SetInstruction;
import gecos.instrs.StringInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.ArrayType;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.FloatType;
import gecos.types.IntegerType;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.PtrType;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import gecos.types.TypesPackage;

@SuppressWarnings("all")
public class SimpleTypingRules implements ITypeSystemRuleSet {

	%include { sl.tom }
	%include { long.tom }
	%include { double.tom }
	%include { gecos_common.tom }
	%include { gecos_terminals.tom } 
	%include { gecos_basic.tom }
	%include { gecos_arithmetic.tom }  
	%include { gecos_compare.tom } 
	%include { gecos_control.tom } 
	%include { gecos_ssa.tom } 
	%include { gecos_logical.tom } 
    %include { gecos_operators.tom } 
    %include { gecos_misc.tom } 
    %include { gecos_types.tom } 

	protected static void debug(String mess) {
		if(VERBOSE) System.out.println(mess);
	}

	protected static void debug(int n, String mess) {  
		debug("Rule "+n+" : "+mess); 
	}

   public static final boolean VERBOSE = false;
 
    public SimpleTypingRules() {
	 
    }

	public boolean isExternalType(Type type) {
			if(type!=null)
				return (type.eClass().getEPackage()!=TypesPackage.eINSTANCE) ;
			else
				return true;
	}

	public boolean usesExternalTypes(Instruction... instrs) {
		return usesExternalTypes(Arrays.asList(instrs));
	}

	public boolean usesExternalTypes(List<Instruction> instrs) {
		for(Instruction instr : instrs) {
			if(isExternalType(instr.getType())) {
				System.out.println("External type "+instr.getType()+" in "+instr);
				return true;
			}
		}
		return false; 
	}


	@Override
	public Type computeType(Instruction o) {
		
		if(o.eClass().getEPackage()!=InstrsPackage.eINSTANCE)
			return null;
		Type res = tomType(o);
		return res;
    }

    
	public Type tomAddSubMulDivType(String n, Type t1, Type t2) {
		Type res = null;
		%match(t1,t2) { // TODO : more precise size for floats?
			floatType(_),floatType(_) -> {
				res = GecosUserTypeFactory.DOUBLE();
			}
			floatType(_),ptrType(_) -> {
				// invalid
				res = null;
			}
			floatType(_),intType(_,_) -> {
				res = GecosUserTypeFactory.DOUBLE();
			}
			ptrType(_),floatType(_) -> {
				// invalid
				res = null;
			}
			ptrType(_),ptrType(_) -> { // TODO : check
				if (n.equals("add")) {
					res = GecosUserTypeFactory.INT();
				}
				else {
					res = null;
				}
			}
			ptrType(_),intType(_,_) -> {
				PtrType ptr = (PtrType) `t1;
				res = GecosUserTypeFactory.PTR(ptr.getBase());
			}
			intType(_,_),floatType(_) -> {
				res = GecosUserTypeFactory.DOUBLE();
			}
			intType(_,_),ptrType(_) -> {
				res = GecosUserTypeFactory.INT();
			}
			intType(signedA,sizeA),intType(signedB,sizeB) -> {
				if ((n == "add")||(n == "sub")) {
					res = `intType((signedA == SignModifiers.SIGNED||signedB==SignModifiers.SIGNED?SignModifiers.SIGNED:SignModifiers.UNSIGNED), sizeA.getValue()>sizeB.getValue()?sizeA:sizeB);
					((IntegerType)res).setSize(Math.max(t1.getSize(), t2.getSize()) + 1);
				}
				else {
					res = `intType((signedA == SignModifiers.SIGNED||signedB==SignModifiers.SIGNED?SignModifiers.SIGNED:SignModifiers.UNSIGNED), sizeA.getValue()>sizeB.getValue()?sizeA:sizeB);
					((IntegerType)res).setSize(t1.getSize() + t2.getSize());
				}
			}
		}
		return res;
	}

	public Type tomNegType(Type t1) {
		Type res = null;
		%match(t1) {
			floatType(_) -> {
				res = t1;
			}
			intType(_,_) -> {
				res = t1;
			}
		}
		return res;
	}

	public Type tomType(Instruction o) {
		%match(o) {
 
			generic(_,children@InstL(_*)) -> {

				if(usesExternalTypes(`children)) {
					debug(0,"Non standard types in "+(`children));
					return null;
				} 
		 	}

			set(dst,src) -> {
				//
				
				if(usesExternalTypes(`dst, `src)) { 				
					return null;
				}
				// TODO : here we should check that the types of src and dst are consistent 
				Type typeD = `dst.getType();
				Type typeS = `src.getType();
				%match(typeD,typeS) {
					// int autocasts to float
					// float autocasts to int
					// int autocasts to pointer      gcc warning
					// pointer autocasts to int      gcc warning
					// float DOESN'T autocast to pointer
					// pointer DOESN'T autocast to float
					ptrType(_),floatType(_) -> {
						debug(20,"Set operation "+o+" invalid typing : dest pointer, src float");
						return null;
					} 
					floatType(_),ptrType(_) -> {
						debug(21,"Set operation "+o+" invalid typing : dest float, src pointer");
						return null;
					}
					intType(_,_),floatType(_) -> {
						debug(22,"Set operation "+o+" is not advised : src int, dest float");
					}
					floatType(_),intType(_,_) -> {
						debug(23,"Set operation "+o+" is not advised : src float, dest int");
					}
				}
				debug(1,"Set instruction " + o + " : dest " + typeD + " / src " + typeS);
				return (`dst).getType();
			}

			ret(expr) -> { 
				if(usesExternalTypes(`expr)) {				
					return null;
				}
				debug(1,"Return instruction "+o+" typed as "+(`expr).getType());
				return (`expr).getType();
			} 

			generic(name,InstL(op1)) -> { 
				%match(name) { // Unary operations
					"neg" -> {
						Type t1 = `op1.getType();
						Type res = tomNegType(t1);
						if (res != null) {debug(2,"Unary neg "+o+" resolved to "+res);}
						else {debug(3,"Unary neg "+o+" not resolved");}
						return res;
					}
					"not" -> {
						debug(4,"Unary not operation "+o);
						// only INT() & result in BOOL()
						Type t1 = `op1.getType();
						%match(t1) {
							intType(_,_) -> { return GecosUserTypeFactory.BOOL(); }
						}
						return null;
					}
				} 
			}

			generic(name,InstL(op1,op2)) -> { // Binary operations
				%match(name) {
					"add"|"sub"|"mul"|"div" -> {
						String n = `name;
						Type res = null;
						Type t1 = `op1.getType();
						Type t2 = `op2.getType();

						%match(t1,t2) { // TODO : precise size for floats?
							floatType(_),floatType(_) -> {
								res = GecosUserTypeFactory.DOUBLE();
							}
							floatType(_),ptrType(_) -> {
								// invalid
								debug(5,"Binary operation "+o+" is badly typed (float + pointer)");
								res = null;
							}
							floatType(_),intType(_,_) -> {
								res = GecosUserTypeFactory.DOUBLE();
							}
							ptrType(_),floatType(_) -> {
								// invalid
								debug(6,"Binary operation "+o+" is badly typed (pointer + float)");
								res = null;
							}
							ptrType(_),ptrType(_) -> {
								PtrType ptr = (PtrType) `t1;
								res = GecosUserTypeFactory.PTR(ptr.getBase());
							}
							ptrType(_),intType(_,_) -> {
								PtrType ptr = (PtrType) `t1;
								res = GecosUserTypeFactory.PTR(ptr.getBase());
							}
							intType(_,_),floatType(_) -> {
								res = GecosUserTypeFactory.DOUBLE();
							}
							intType(_,_),ptrType(_) -> {
								res = GecosUserTypeFactory.INT();
							}
							intType(_,sizeA),intType(_,sizeB) -> {
								IntegerTypes size = `sizeA.getValue()>`sizeB.getValue()?`sizeA:`sizeB;
								if (size == IntegerTypes.CHAR ||size == IntegerTypes.SHORT) {
									res = GecosUserTypeFactory.SHORT();
								}
								else if (size == IntegerTypes.INT) {
									res = GecosUserTypeFactory.INT();
								}
								else if (size == IntegerTypes.LONG) {
									res = GecosUserTypeFactory.LONG();
								}
								else {
									res = GecosUserTypeFactory.LONGLONG();
								}
							}
						}
						res = tomAddSubMulDivType(n, t1, t2);

						if (res != null) {
							debug(7,"Binary operation "+o+" type is "+res);
						}
						return res; 
					}

					"and"|"or"|"xor" -> {
						debug(8,"Bitwise logic operation "+o);
						// only INT() & result in BOOL()
						Type t1 = `op1.getType();
						Type t2 = `op2.getType();
						%match(t1,t2) {
							intType(_,_),intType(_,_) -> {return GecosUserTypeFactory.BOOL();}
						}
						debug(19,"Bitwise logic op "+o+" badly typed");
					}

					"land"|"lor" -> {
						debug(9,"Logical operation "+o);
						// only INT() & result in INT(max(a,b))
						Type t1 = `op1.getType();
						Type t2 = `op2.getType();
						%match(t1,t2) {
							intType(_,sizeA),intType(_,sizeB) -> {

								IntegerTypes size = `sizeA.getValue()>`sizeB.getValue()?`sizeA:`sizeB;
								if (size == IntegerTypes.CHAR ||size == IntegerTypes.SHORT) {
									return GecosUserTypeFactory.SHORT();
								}
								else if (size == IntegerTypes.INT) {
									return GecosUserTypeFactory.INT();
								}
								else if (size == IntegerTypes.LONG) {
									return GecosUserTypeFactory.LONG();
								}
								else {
									return GecosUserTypeFactory.LONGLONG();
								}
							}
						}
						debug(20,"Logic op "+o+" badly typed");
					}

					"geq"|"ge"|"gt"|"leq"|"le"|"lt"|"eq"|"neq" -> {
						Type t1 = `op1.getType();
						Type t2 = `op2.getType();
						Type res = null;
						%match(t1,t2) {
							intType(_,_),intType(_,_) -> {
								

								res = GecosUserTypeFactory.BOOL();
							}
							intType(_,_),floatType(_) -> {
								res = GecosUserTypeFactory.BOOL();
							}
							intType(_,_),ptrType(_) -> {
								// warning in gcc
								res = GecosUserTypeFactory.BOOL();
							}
							floatType(_),intType(_,_) -> {
								res = GecosUserTypeFactory.BOOL();
							}
							floatType(_),floatType(_) -> {
								res = GecosUserTypeFactory.BOOL();
							}
							floatType(_),ptrType(_) -> {
								// invalid
								debug(17,"Invalid comparison "+o+" between float and pointer");
							}
							ptrType(_),intType(_,_) -> {
								// warning in gcc
								res = GecosUserTypeFactory.BOOL();
							}
							ptrType(_),floatType(_) -> {
								// invalid
								debug(18,"Invalid comparison "+o+" between pointer and float");
							}
							ptrType(_),ptrType(_) -> {
								res = GecosUserTypeFactory.BOOL();
							}
						}
						if (res != null) {
							debug(10,"Comparison operation "+o+" typed as "+res);
						}
						else {
							debug(11,"Comparison operation "+o+" not resolved");
						}
						return res;
					}
				} 
			} 

			generic(name,InstL(_,_,_)) -> {
				%match(name) {
					"mux" -> {
						// Float(), Ptr, INT() & result in INT(max(a,b))
					}
				} 
			}
 
			generic(name,InstL(_,_,_,_*)) -> {
				%match(name) {
					"sum" -> {
						// Float(), Ptr, INT() & result in INT(max(a,b))
					}
				} 
			}


			//
			//  address taking and indirection operator
			// 
			s@indir(addr) -> {
				Type t= (`addr).getType(); 
				%match(t) {
					ptrType(base) -> {
						// Do something
						return (`base);
					} 
					arrayType(base,_,_,_) -> {
						// Do something
						return (`base);
					}
					_ -> {
						System.err.println("Indirection on neither a popinter or array "+(`s));
						return null;
					}
				}
			}
			
			address(lvalue) -> {
				%match(lvalue) {
					array(trgt, InstL(_*)) -> {
						return GecosUserTypeFactory.PTR((`trgt).getType());
					}
					symref(sym) -> {
						Symbol sym = (`sym);
						return GecosUserTypeFactory.PTR(sym.getType());
						
					}
				}
				
			}
			s@array(arrayTrgt, InstL(_*)) -> {
				Type t= (`arrayTrgt).getType();
				%match(t) {
					ptrType(base) -> {
						// Do something
						return (`base);
					} 
					arrayType(base,_,_,_) -> {
						// Do something
						return (`base);
					}
					_ -> {
						System.err.println("Array access on neither an array or pointer "+(`s));
						return null;
					}
				}
			}
			
			ssaDef(_,_) -> { 
				return  null;  
			}
 
			ssaUse(_,_) -> { 
				return  null;  
			}

			phi(InstL(_,_*)) -> { 
				return null;  
			}

			//  Symbol reference
			symref(sym) -> {
				if((`sym)!=null) {
					Type type= (`sym).getType();
				} else {
					return null;
				}
				Type type= (`sym).getType();
				debug(12,"Symbol instruction "+o+" with Type "+type);
				return type;  
			}


			//  Litteral & constant values
			ival(a) -> { 
				debug(13,"TypeResolver -> IntInstruction : "+(`a));
				return GecosUserTypeFactory.INT();  
			}
			
			fval(a) -> {
				debug(14,"TypeResolver -> FloatInstruction : "+(`a));
				return GecosUserTypeFactory.DOUBLE();  				
				  
			}
			
			i@strval(a) -> {
				((StringInstruction)(`i)).setValue(`a);
				if(`a.equals("true")|(`a.equals("false"))){
					debug(15,"TypeResolver -> BoolInstruction "+(`a));
					return GecosUserTypeFactory.BOOL();
				} else {
					debug(16,"TypeResolver -> StringLitteralInstruction "+(`a));
					return GecosUserTypeFactory.STRING((`a).length());
				}
				  
			}


			_ -> {

			}

		}	
		return null;

	}
	
}