package $packageName$.modules;

import custom.instruction.FooInstruction;
import custom.instruction.InstructionFactory;
import custom.type.FooType;
import custom.type.TypeFactory;
import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import fr.irisa.cairn.gecos.model.tools.switches.SymbolSwitch;
import fr.irisa.cairn.gecos.model.transforms.types.TypeAnalyzer;
import gecos.core.ProcedureSet;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.types.Type;

/**
 * GeCoS module implementation which replace multiplication with our custom FooInstruction and integer type of a variable with FooType
 */
public class ReplaceMultiplicationWithFooInstructionModule {
	private GecosProject _project;
	
	public ReplaceMultiplicationWithFooInstructionModule(GecosProject project){
		_project = project;
	}
	
	/**
	 * Method called by the GeCoS framework when the script command defined in the extension is called
	 */
	public void compute(){
		new ReplaceMultiplicationWithFooInstructionSwitch().doSwitch(_project); // Apply the switch on the GeCoS project
		for(ProcedureSet ps : _project.listProcedureSets()) // Iterate on each procedure set
			new ReplaceIntTypeWithFooTypeSwitch().doSwitch(ps); // Apply switch on on procedure set
	}
	
	/**
	 * Implementation of a custom Switch replace multiplication with FooInstruction
	 * 
	 * This class extends BlockInstructionSwitch which is a Default switch implementation.
	 * BlockInstructionSwitch performs a depth-first traversal of the IR until a instruction is reached
	 */
	private class ReplaceMultiplicationWithFooInstructionSwitch extends BlockInstructionSwitch<Object> {
		private InstructionFactory _customInstFactory = InstructionFactory.eINSTANCE;
		private TypeFactory _customTypeFactory = TypeFactory.eINSTANCE;
		
		@Override
		public Object caseGenericInstruction(GenericInstruction g) {
			// Look up first the content of the g node
			super.caseGenericInstruction(g);
			
			// If a multiplication is detected
			if(g.getName().equals(ArithmeticOperator.MUL.getLiteral())){
				FooInstruction fooInst = _customInstFactory.createFooInstruction(); // Create a FooInstruction thanks to the custom factory
				
				// Get back multiplication operands and set them on the FooInstruction
				Instruction op1 = g.getOperand(0);
				Instruction op2 = g.getOperand(1);
				fooInst.setFirstOperand(op1); 
				fooInst.setSecondOperand(op2);
				
				// Set the type of the FooInstruction (means the FooInstruction return a FooType as result)
				// We just check if there are not already a FooType in the GeCoS IR.
				// If not, we add the FooType in the scope, otherwise we use it.
				FooType fooType = _customTypeFactory.createFooType();
				Type type = g.getBasicBlock().getScope().lookup(fooType);
				if(type == null){
					fooType.installOn(g.getBasicBlock().getScope());
					fooInst.setType(fooType);
				}
				else{
					fooInst.setType(type);
				}
				
				// Test if the current generic instruction is contained in a instruction
				// getParent() return null if the instruction is not contained in an another instruction
				// otherwise, it return the parent instruction
				if(g.getParent() == null){
					g.getBasicBlock().replaceInstruction(g, fooInst);
				}
				else{
					g.getParent().replaceChild(g, fooInst); // Replace the instruction in the parent instuction by the new one
				}
			}
			
			return null;
		}	
	}
	
	/**
	 * Implementation of a custom Switch which replace Integer type of a variable with FooType
	 * 
	 * This class extends BlockInstructionSwitch which is a Default switch implementation.
	 * BlockInstructionSwitch performs a depth-first traversal of the IR until a instruction is reached
	 */
	private class ReplaceIntTypeWithFooTypeSwitch extends SymbolSwitch<Object>{
		private TypeFactory _customTypeFactory = TypeFactory.eINSTANCE;
		
		@Override
		public Object caseSymbol(Symbol object) {
			TypeAnalyzer analyzer = new TypeAnalyzer(object.getType()); // Toolbox which analyze type
			if(analyzer.getBaseLevel().isInt()){ // Test if the base level of a type is an integer
				// Apply qualifier (static, const...) of the original type on the new FooType.
				Type newType = analyzer.revertBaseLevelOn(_customTypeFactory.createFooType());
				
				// We just check if there are not already a corresponding type in the GeCoS IR.
				// If not, we add the new type in the scope, otherwise we use it.
				Type typeInScope = object.getContainingScope().lookup(newType);
				if(typeInScope == null){
					newType.installOn(object.getContainingScope());
					object.setType(newType); // Set the new type of the symbol 
				}
				else{
					object.setType(typeInScope); // Set the new type of the symbol 
				}
			}
			
			return super.caseSymbol(object);
		}
		
	}
}	
