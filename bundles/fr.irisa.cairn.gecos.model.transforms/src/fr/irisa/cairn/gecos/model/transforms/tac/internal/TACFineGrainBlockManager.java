package fr.irisa.cairn.gecos.model.transforms.tac.internal;

import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.tools.switches.BasicBlockSwitch;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Scope;
import gecos.instrs.Instruction;

@Deprecated
public class TACFineGrainBlockManager extends BasicBlockSwitch<Object>{
	private TACFineGrainInstManager _inst;
	private Scope _newScope; // New scope which contains the new symbol creating during computing of basic block instructions
	private int _numberTmpVariable;
	
	public TACFineGrainBlockManager(){
		_inst = new TACFineGrainInstManager(this);
	}
	
	public Scope getNewScope(){
		return _newScope;
	}
	
	public int useNumberTmpVariable(){
		int res = _numberTmpVariable;
		_numberTmpVariable++;
		return res;
	}

	@Override
	public Object caseBasicBlock(BasicBlock b) {
		
		for(Instruction inst : b.getInstructions()){
			_inst.doSwitch(inst);
		}
		
		for(Instruction key : _inst.getMap().keySet()){
			int pos = b.getInstructions().indexOf(key);
			if(pos >= 0){
				b.getInstructions().addAll(pos, _inst.getMap().get(key));
			}
			else{
				throw new RuntimeException("The last instruction representing the instruction to transform is not present into the current basic block");
			}
		}

		_inst.getMap().clear();
		
		return null;
	}
	
	@Override
	public Object caseCompositeBlock(CompositeBlock b){
		int parentNumberTmpVariable = _numberTmpVariable;
		_numberTmpVariable = 0;
		
		Scope parentScope = _newScope;
		_newScope = GecosUserCoreFactory.scope();
		
		for(Block block : b.getChildren()){
			doSwitch(block);
		}
		
		b.getScope().mergeWith(_newScope);
		_numberTmpVariable = parentNumberTmpVariable;
		_newScope = parentScope;
		return null;
	}

	@Override
	public Object caseIfBlock(IfBlock b){
		if(b.getThenBlock() != null)
			doSwitch(b.getThenBlock());
		if(b.getElseBlock() != null)
			doSwitch(b.getElseBlock());
		return null;
	}
	
	@Override
	public Object caseForBlock(ForBlock b){
		if(b.getBodyBlock() != null)
			doSwitch(b.getBodyBlock());
		return null;
	}
	
	@Override
	public Object caseWhileBlock(WhileBlock b){
		if(b.getBodyBlock() != null)
			doSwitch(b.getBodyBlock());
		return null;
	}
	
	@Override
	public Object caseDoWhileBlock(DoWhileBlock b){
		if(b.getBodyBlock() != null)
			doSwitch(b.getBodyBlock());
		return null;
	}
	
}
