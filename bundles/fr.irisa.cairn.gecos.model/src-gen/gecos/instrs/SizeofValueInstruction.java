/**
 */
package gecos.instrs;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sizeof Value Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.instrs.InstrsPackage#getSizeofValueInstruction()
 * @model
 * @generated
 */
public interface SizeofValueInstruction extends ExprComponent, SizeofInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitSizeofValueInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.instrs.Instruction%&gt; _expr = this.getExpr();\n&lt;%java.lang.String%&gt; _plus = (\"sizeof(\" + _expr);\nreturn (_plus + \")\");'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.SizeofValueInstruction%&gt;))\n{\n\tboolean isSame = false;\n\tif (((this.getExpr() == null) &amp;&amp; (((&lt;%gecos.instrs.SizeofValueInstruction%&gt;)instr).getExpr() == null)))\n\t{\n\t\tisSame = true;\n\t}\n\telse\n\t{\n\t\t&lt;%gecos.instrs.Instruction%&gt; _expr = this.getExpr();\n\t\tboolean _tripleEquals = (_expr == null);\n\t\t&lt;%gecos.instrs.Instruction%&gt; _expr_1 = ((&lt;%gecos.instrs.SizeofValueInstruction%&gt;)instr).getExpr();\n\t\tboolean _tripleEquals_1 = (_expr_1 == null);\n\t\tboolean _xor = (_tripleEquals ^ _tripleEquals_1);\n\t\tif (_xor)\n\t\t{\n\t\t\treturn false;\n\t\t}\n\t\telse\n\t\t{\n\t\t\tisSame = this.getExpr().isSame(((&lt;%gecos.instrs.SizeofValueInstruction%&gt;)instr).getExpr());\n\t\t}\n\t}\n\treturn (super.isSame(instr) &amp;&amp; isSame);\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // SizeofValueInstruction
