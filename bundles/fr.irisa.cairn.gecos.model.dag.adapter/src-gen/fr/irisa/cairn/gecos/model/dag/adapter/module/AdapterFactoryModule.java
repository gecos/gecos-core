package fr.irisa.cairn.gecos.model.dag.adapter.module;

import com.google.inject.Singleton;

import fr.irisa.cairn.gecos.model.dag.adapter.builder.AdapterFactory;
import fr.irisa.cairn.graph.guice.modules.servicesBinding.FactoryServiceModule;

public class AdapterFactoryModule extends FactoryServiceModule {

	@Override
	protected void configure() {
		super.configure();
		this.bindFactory();
	}

	protected void bindFactory() {
		this.bind(AdapterFactory.class).in(Singleton.class);
	}
}
