typedef struct PAIR{ int a; int b; } pair;

int main() {
	pair res;

	#pragma outline
	{
		res.a = 4;
		res.b = 5;
	}

	if(res.b != 5) {
		abort();
	}

	return 0;
}
