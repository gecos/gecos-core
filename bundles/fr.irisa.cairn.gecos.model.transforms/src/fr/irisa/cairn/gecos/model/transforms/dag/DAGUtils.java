package fr.irisa.cairn.gecos.model.transforms.dag;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import fr.irisa.cairn.gecos.model.dag.adapter.DAGInstructionAdapterComponent;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGInstructionAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGNodeAdapter;
import fr.irisa.cairn.graph.INode;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGNode;
import gecos.dag.DAGOpNode;
import gecos.dag.DAGOperator;
import gecos.dag.DAGVectorExtractNode;
import gecos.dag.DAGVectorNode;
import gecos.dag.DAGVectorPackNode;
import gecos.instrs.BitwiseOperator;

public class DAGUtils {

	private DAGUtils(){}
	
	public static void exportDAG2Dot(DAGInstruction dag, String filename) {
		IDAGInstructionAdapter adapter = DAGInstructionAdapterComponent.INSTANCE.build(dag);
		DAGInstructionAdapterComponent.INSTANCE.getGraphExportService().export(filename, adapter);
	}

	public static boolean hasCycle(DAGInstruction dag) {
			IDAGInstructionAdapter adapter = DAGInstructionAdapterComponent.INSTANCE.build(dag);
			return DAGInstructionAdapterComponent.INSTANCE.getGraphAnalysisService().hasCycle(adapter);
	//		SimpleDirectedGraph<DAGNode, Edge> graph = DAGUtils.createSimpleDirectedGraph(dag);
	//		return new CycleDetector<DAGNode, Edge>(graph).detectCycles();
		}

	public static List<DAGNode> topologicalSort(DAGInstruction dag) {
		IDAGInstructionAdapter adapter = DAGInstructionAdapterComponent.INSTANCE.build(dag);
		Iterator<INode> topIter = DAGInstructionAdapterComponent.INSTANCE.getGraphAnalysisService().topologicalIterator(adapter);
		List<DAGNode> res = new ArrayList<DAGNode>();
		while(topIter.hasNext())
			res.add(((IDAGNodeAdapter)topIter.next()).getAdaptedNode());
		return res;
	}

	public static Multimap<Integer, DAGNode> topologicalSteps(DAGInstruction dag) {
		IDAGInstructionAdapter adapter = DAGInstructionAdapterComponent.INSTANCE.build(dag);
		Map<INode, Integer> topmap = DAGInstructionAdapterComponent.INSTANCE.getGraphAnalysisService().topologicalSteps(adapter);
		Multimap<Integer, DAGNode> res = HashMultimap.create();
		for(Entry<INode, Integer> entry : topmap.entrySet())
			res.put(entry.getValue(), ((IDAGNodeAdapter)entry.getKey()).getAdaptedNode());
		return res;
	}

	public static boolean isOp(DAGNode n) {
		return (n instanceof DAGOpNode);
	}

	public static boolean isCVT(DAGNode n) {
		return isOp(n) && DAGUtils.asOp(n).getOpcode().equals(DAGOperator.CVT.getLiteral());
	}

	public static boolean isShr(DAGNode n) {
		return isOp(n) && DAGUtils.asOp(n).getOpcode().equalsIgnoreCase(BitwiseOperator.SHR.getLiteral());
	}

	public static boolean isShl(DAGNode o) {
		return isOp(o) && DAGUtils.asOp(o).getOpcode().equalsIgnoreCase(BitwiseOperator.SHL.getLiteral());
	}

	public static boolean isVec(DAGNode n) {
		return (n instanceof DAGVectorNode);
	}

	public static boolean isPack(DAGNode n) {
		return (n instanceof DAGVectorPackNode);
	}

	public static boolean isExt(DAGNode n) {
		return (n instanceof DAGVectorExtractNode);
	}

	public static DAGOpNode asOp(DAGNode n) {
		return (DAGOpNode)n;
	}

	public static DAGVectorNode asVec(DAGNode n) {
		return (DAGVectorNode)n;
	}

	public static DAGVectorPackNode asPack(DAGNode n) {
		return (DAGVectorPackNode)n;
	}

	public static DAGVectorExtractNode asExt(DAGNode n) {
		return (DAGVectorExtractNode)n;
	}

	/**
	 * @param dag
	 * @param prefix
	 * @param suffix
	 * @return [prefix][containing procedure symbol name]_[containing basicblock number][suffix]
	 */
	public static String nameDag(DAGInstruction dag, String prefix, String suffix) {
		StringBuilder sb = new StringBuilder();
		if(prefix != null)
			sb.append(prefix);
		if(dag.getContainingProcedure() != null && dag.getContainingProcedure().getSymbolName() != null)
			sb.append(dag.getContainingProcedure().getSymbolName()).append("_");
		if(dag.getBasicBlock() != null)
			sb.append(dag.getBasicBlock().getNumber());
		if(suffix != null)
			sb.append(suffix);
		return sb.toString();
	}

}
