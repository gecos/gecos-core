/**
 */
package gecos.instrs;

import gecos.types.Type;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generic Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.GenericInstruction#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getGenericInstruction()
 * @model
 * @generated
 */
public interface GenericInstruction extends ChildrenListInstruction {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see gecos.instrs.InstrsPackage#getGenericInstruction_Name()
	 * @model unique="false" dataType="gecos.instrs.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link gecos.instrs.GenericInstruction#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getChildren();'"
	 * @generated
	 */
	EList<Instruction> getOperands();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitGenericInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='try\n{\n\tfinal &lt;%java.lang.StringBuffer%&gt; res = new &lt;%java.lang.StringBuffer%&gt;();\n\t&lt;%java.lang.String%&gt; _name = this.getName();\n\tboolean _tripleEquals = (_name == null);\n\tif (_tripleEquals)\n\t{\n\t\tres.append(\"?\");\n\t}\n\telse\n\t{\n\t\tres.append(this.getName());\n\t}\n\tres.append(\"(\");\n\tfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;, &lt;%java.lang.CharSequence%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;, &lt;%java.lang.CharSequence%&gt;&gt;()\n\t{\n\t\tpublic &lt;%java.lang.CharSequence%&gt; apply(final &lt;%gecos.instrs.Instruction%&gt; it)\n\t\t{\n\t\t\t&lt;%java.lang.String%&gt; _elvis = null;\n\t\t\t&lt;%java.lang.String%&gt; _string = null;\n\t\t\tif (it!=null)\n\t\t\t{\n\t\t\t\t_string=it.toString();\n\t\t\t}\n\t\t\tif (_string != null)\n\t\t\t{\n\t\t\t\t_elvis = _string;\n\t\t\t} else\n\t\t\t{\n\t\t\t\t_elvis = \"?\";\n\t\t\t}\n\t\t\treturn _elvis;\n\t\t}\n\t};\n\tres.append(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;join(this.getOperands(), \",\", _function));\n\tres.append(\")\");\n\treturn res.toString();\n}\ncatch (final Throwable _t) {\n\tif (_t instanceof &lt;%java.lang.Exception%&gt;) {\n\t\tfinal &lt;%java.lang.Exception%&gt; e = (&lt;%java.lang.Exception%&gt;)_t;\n\t\t&lt;%java.lang.String%&gt; _message = e.getMessage();\n\t\treturn (\"NPE \" + _message);\n\t}\n\telse\n\t{\n\t\tthrow &lt;%org.eclipse.xtext.xbase.lib.Exceptions%&gt;.sneakyThrow(_t);\n\t}\n}'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" iDataType="gecos.instrs.int" iUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; _operands = this.getOperands();\nboolean _tripleNotEquals = (_operands != null);\nif (_tripleNotEquals)\n{\n\treturn this.getOperands().get(i);\n}\nthrow new &lt;%java.lang.RuntimeException%&gt;(\"Null Operand List for generic inst\");'"
	 * @generated
	 */
	Instruction getOperand(int i);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model instUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getOperands().add(inst);'"
	 * @generated
	 */
	void addOperand(Instruction inst);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" nameDataType="gecos.instrs.String" nameUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getName().equals(name);'"
	 * @generated
	 */
	boolean isNamed(String name);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _isNullOrEmpty = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.isNullOrEmpty(this.getOperands());\nboolean _not = (!_isNullOrEmpty);\nif (_not)\n{\n\t&lt;%gecos.types.Type%&gt; res = this.getChild(0).getType();\n\tint _childrenCount = this.getChildrenCount();\n\t&lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt; _doubleDotLessThan = new &lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt;(1, _childrenCount, true);\n\tfor (final &lt;%java.lang.Integer%&gt; i : _doubleDotLessThan)\n\t{\n\t\tres = &lt;%fr.irisa.cairn.gecos.model.factory.conversion.ConversionMatrix%&gt;.getCommon(this.getChild((i).intValue()).getType(), res);\n\t}\n\treturn res;\n}\nreturn this.getType();'"
	 * @generated
	 */
	Type computeType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.GenericInstruction%&gt;))\n{\n\tboolean isSameName = false;\n\tif (((((&lt;%gecos.instrs.GenericInstruction%&gt;)instr).getName() == null) &amp;&amp; (this.getName() == null)))\n\t{\n\t\tisSameName = true;\n\t}\n\telse\n\t{\n\t\t&lt;%java.lang.String%&gt; _name = ((&lt;%gecos.instrs.GenericInstruction%&gt;)instr).getName();\n\t\tboolean _tripleEquals = (_name == null);\n\t\t&lt;%java.lang.String%&gt; _name_1 = this.getName();\n\t\tboolean _tripleEquals_1 = (_name_1 == null);\n\t\tboolean _xor = (_tripleEquals ^ _tripleEquals_1);\n\t\tif (_xor)\n\t\t{\n\t\t\treturn false;\n\t\t}\n\t\telse\n\t\t{\n\t\t\tisSameName = ((&lt;%gecos.instrs.GenericInstruction%&gt;)instr).getName().equals(this.getName());\n\t\t}\n\t}\n\treturn (super.isSame(instr) &amp;&amp; isSameName);\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // GenericInstruction
