/**
 */
package gecos.instrs;

import gecos.annotations.AnnotationsPackage;

import gecos.core.CorePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see gecos.instrs.InstrsFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel modelName='Gecos' modelPluginClass='' creationCommands='false' creationIcons='false' interfaceNamePattern='' importerID='org.eclipse.emf.importer.ecore' loadInitialization='false' operationReflection='false' basePackage='gecos'"
 * @generated
 */
public interface InstrsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "instrs";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.gecos.org/instrs";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "instrs";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	InstrsPackage eINSTANCE = gecos.instrs.impl.InstrsPackageImpl.init();

	/**
	 * The meta object id for the '{@link gecos.instrs.InstrsVisitor <em>Visitor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.InstrsVisitor
	 * @see gecos.instrs.impl.InstrsPackageImpl#getInstrsVisitor()
	 * @generated
	 */
	int INSTRS_VISITOR = 0;

	/**
	 * The number of structural features of the '<em>Visitor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRS_VISITOR_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.InstrsVisitable <em>Visitable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.InstrsVisitable
	 * @see gecos.instrs.impl.InstrsPackageImpl#getInstrsVisitable()
	 * @generated
	 */
	int INSTRS_VISITABLE = 1;

	/**
	 * The number of structural features of the '<em>Visitable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRS_VISITABLE_FEATURE_COUNT = CorePackage.GECOS_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.InstructionImpl <em>Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.InstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getInstruction()
	 * @generated
	 */
	int INSTRUCTION = 2;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTION__ANNOTATIONS = AnnotationsPackage.ANNOTATED_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTION__INTERNAL_CACHED_TYPE = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTION_FEATURE_COUNT = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.DummyInstructionImpl <em>Dummy Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.DummyInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getDummyInstruction()
	 * @generated
	 */
	int DUMMY_INSTRUCTION = 3;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUMMY_INSTRUCTION__ANNOTATIONS = INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUMMY_INSTRUCTION__INTERNAL_CACHED_TYPE = INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The number of structural features of the '<em>Dummy Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUMMY_INSTRUCTION_FEATURE_COUNT = INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.LabelInstructionImpl <em>Label Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.LabelInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getLabelInstruction()
	 * @generated
	 */
	int LABEL_INSTRUCTION = 4;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL_INSTRUCTION__ANNOTATIONS = INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL_INSTRUCTION__INTERNAL_CACHED_TYPE = INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL_INSTRUCTION__NAME = INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Label Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL_INSTRUCTION_FEATURE_COUNT = INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.SymbolInstructionImpl <em>Symbol Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.SymbolInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getSymbolInstruction()
	 * @generated
	 */
	int SYMBOL_INSTRUCTION = 5;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_INSTRUCTION__ANNOTATIONS = INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_INSTRUCTION__INTERNAL_CACHED_TYPE = INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_INSTRUCTION__SYMBOL = INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Symbol Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_INSTRUCTION_FEATURE_COUNT = INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.NumberedSymbolInstructionImpl <em>Numbered Symbol Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.NumberedSymbolInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getNumberedSymbolInstruction()
	 * @generated
	 */
	int NUMBERED_SYMBOL_INSTRUCTION = 6;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBERED_SYMBOL_INSTRUCTION__ANNOTATIONS = SYMBOL_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBERED_SYMBOL_INSTRUCTION__INTERNAL_CACHED_TYPE = SYMBOL_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBERED_SYMBOL_INSTRUCTION__SYMBOL = SYMBOL_INSTRUCTION__SYMBOL;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBERED_SYMBOL_INSTRUCTION__NUMBER = SYMBOL_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Numbered Symbol Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBERED_SYMBOL_INSTRUCTION_FEATURE_COUNT = SYMBOL_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.SSADefSymbolImpl <em>SSA Def Symbol</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.SSADefSymbolImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getSSADefSymbol()
	 * @generated
	 */
	int SSA_DEF_SYMBOL = 7;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SSA_DEF_SYMBOL__ANNOTATIONS = NUMBERED_SYMBOL_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SSA_DEF_SYMBOL__INTERNAL_CACHED_TYPE = NUMBERED_SYMBOL_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SSA_DEF_SYMBOL__SYMBOL = NUMBERED_SYMBOL_INSTRUCTION__SYMBOL;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SSA_DEF_SYMBOL__NUMBER = NUMBERED_SYMBOL_INSTRUCTION__NUMBER;

	/**
	 * The feature id for the '<em><b>SSA Uses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SSA_DEF_SYMBOL__SSA_USES = NUMBERED_SYMBOL_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>SSA Def Symbol</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SSA_DEF_SYMBOL_FEATURE_COUNT = NUMBERED_SYMBOL_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.SSAUseSymbolImpl <em>SSA Use Symbol</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.SSAUseSymbolImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getSSAUseSymbol()
	 * @generated
	 */
	int SSA_USE_SYMBOL = 8;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SSA_USE_SYMBOL__ANNOTATIONS = NUMBERED_SYMBOL_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SSA_USE_SYMBOL__INTERNAL_CACHED_TYPE = NUMBERED_SYMBOL_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SSA_USE_SYMBOL__SYMBOL = NUMBERED_SYMBOL_INSTRUCTION__SYMBOL;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SSA_USE_SYMBOL__NUMBER = NUMBERED_SYMBOL_INSTRUCTION__NUMBER;

	/**
	 * The feature id for the '<em><b>Def</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SSA_USE_SYMBOL__DEF = NUMBERED_SYMBOL_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>SSA Use Symbol</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SSA_USE_SYMBOL_FEATURE_COUNT = NUMBERED_SYMBOL_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.SizeofInstructionImpl <em>Sizeof Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.SizeofInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getSizeofInstruction()
	 * @generated
	 */
	int SIZEOF_INSTRUCTION = 9;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZEOF_INSTRUCTION__ANNOTATIONS = INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZEOF_INSTRUCTION__INTERNAL_CACHED_TYPE = INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The number of structural features of the '<em>Sizeof Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZEOF_INSTRUCTION_FEATURE_COUNT = INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.SizeofTypeInstructionImpl <em>Sizeof Type Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.SizeofTypeInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getSizeofTypeInstruction()
	 * @generated
	 */
	int SIZEOF_TYPE_INSTRUCTION = 10;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZEOF_TYPE_INSTRUCTION__ANNOTATIONS = SIZEOF_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZEOF_TYPE_INSTRUCTION__INTERNAL_CACHED_TYPE = SIZEOF_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Target Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZEOF_TYPE_INSTRUCTION__TARGET_TYPE = SIZEOF_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Sizeof Type Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZEOF_TYPE_INSTRUCTION_FEATURE_COUNT = SIZEOF_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.instrs.ComplexInstruction <em>Complex Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.ComplexInstruction
	 * @see gecos.instrs.impl.InstrsPackageImpl#getComplexInstruction()
	 * @generated
	 */
	int COMPLEX_INSTRUCTION = 16;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_INSTRUCTION__ANNOTATIONS = INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_INSTRUCTION__INTERNAL_CACHED_TYPE = INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The number of structural features of the '<em>Complex Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_INSTRUCTION_FEATURE_COUNT = INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.ComponentInstructionImpl <em>Component Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.ComponentInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getComponentInstruction()
	 * @generated
	 */
	int COMPONENT_INSTRUCTION = 20;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTRUCTION__ANNOTATIONS = COMPLEX_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTRUCTION__INTERNAL_CACHED_TYPE = COMPLEX_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The number of structural features of the '<em>Component Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTRUCTION_FEATURE_COUNT = COMPLEX_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.ExprComponentImpl <em>Expr Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.ExprComponentImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getExprComponent()
	 * @generated
	 */
	int EXPR_COMPONENT = 35;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPR_COMPONENT__ANNOTATIONS = COMPONENT_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPR_COMPONENT__INTERNAL_CACHED_TYPE = COMPONENT_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPR_COMPONENT__EXPR = COMPONENT_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Expr Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPR_COMPONENT_FEATURE_COUNT = COMPONENT_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.SizeofValueInstructionImpl <em>Sizeof Value Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.SizeofValueInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getSizeofValueInstruction()
	 * @generated
	 */
	int SIZEOF_VALUE_INSTRUCTION = 11;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZEOF_VALUE_INSTRUCTION__ANNOTATIONS = EXPR_COMPONENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZEOF_VALUE_INSTRUCTION__INTERNAL_CACHED_TYPE = EXPR_COMPONENT__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZEOF_VALUE_INSTRUCTION__EXPR = EXPR_COMPONENT__EXPR;

	/**
	 * The number of structural features of the '<em>Sizeof Value Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZEOF_VALUE_INSTRUCTION_FEATURE_COUNT = EXPR_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.ConstantInstruction <em>Constant Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.ConstantInstruction
	 * @see gecos.instrs.impl.InstrsPackageImpl#getConstantInstruction()
	 * @generated
	 */
	int CONSTANT_INSTRUCTION = 12;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_INSTRUCTION__ANNOTATIONS = INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_INSTRUCTION__INTERNAL_CACHED_TYPE = INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The number of structural features of the '<em>Constant Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_INSTRUCTION_FEATURE_COUNT = INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.IntInstructionImpl <em>Int Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.IntInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getIntInstruction()
	 * @generated
	 */
	int INT_INSTRUCTION = 13;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INSTRUCTION__ANNOTATIONS = CONSTANT_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INSTRUCTION__INTERNAL_CACHED_TYPE = CONSTANT_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INSTRUCTION__VALUE = CONSTANT_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Int Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INSTRUCTION_FEATURE_COUNT = CONSTANT_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.FloatInstructionImpl <em>Float Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.FloatInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getFloatInstruction()
	 * @generated
	 */
	int FLOAT_INSTRUCTION = 14;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_INSTRUCTION__ANNOTATIONS = CONSTANT_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_INSTRUCTION__INTERNAL_CACHED_TYPE = CONSTANT_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_INSTRUCTION__VALUE = CONSTANT_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Float Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_INSTRUCTION_FEATURE_COUNT = CONSTANT_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.StringInstructionImpl <em>String Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.StringInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getStringInstruction()
	 * @generated
	 */
	int STRING_INSTRUCTION = 15;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_INSTRUCTION__ANNOTATIONS = CONSTANT_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_INSTRUCTION__INTERNAL_CACHED_TYPE = CONSTANT_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_INSTRUCTION__VALUE = CONSTANT_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>String Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_INSTRUCTION_FEATURE_COUNT = CONSTANT_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.ChildrenListInstructionImpl <em>Children List Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.ChildrenListInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getChildrenListInstruction()
	 * @generated
	 */
	int CHILDREN_LIST_INSTRUCTION = 17;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHILDREN_LIST_INSTRUCTION__ANNOTATIONS = COMPLEX_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHILDREN_LIST_INSTRUCTION__INTERNAL_CACHED_TYPE = COMPLEX_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHILDREN_LIST_INSTRUCTION__CHILDREN = COMPLEX_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Children List Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHILDREN_LIST_INSTRUCTION_FEATURE_COUNT = COMPLEX_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.ArrayValueInstructionImpl <em>Array Value Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.ArrayValueInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getArrayValueInstruction()
	 * @generated
	 */
	int ARRAY_VALUE_INSTRUCTION = 18;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_VALUE_INSTRUCTION__ANNOTATIONS = CHILDREN_LIST_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_VALUE_INSTRUCTION__INTERNAL_CACHED_TYPE = CHILDREN_LIST_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_VALUE_INSTRUCTION__CHILDREN = CHILDREN_LIST_INSTRUCTION__CHILDREN;

	/**
	 * The number of structural features of the '<em>Array Value Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_VALUE_INSTRUCTION_FEATURE_COUNT = CHILDREN_LIST_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.PhiInstructionImpl <em>Phi Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.PhiInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getPhiInstruction()
	 * @generated
	 */
	int PHI_INSTRUCTION = 19;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHI_INSTRUCTION__ANNOTATIONS = CHILDREN_LIST_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHI_INSTRUCTION__INTERNAL_CACHED_TYPE = CHILDREN_LIST_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHI_INSTRUCTION__CHILDREN = CHILDREN_LIST_INSTRUCTION__CHILDREN;

	/**
	 * The number of structural features of the '<em>Phi Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHI_INSTRUCTION_FEATURE_COUNT = CHILDREN_LIST_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.SetInstructionImpl <em>Set Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.SetInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getSetInstruction()
	 * @generated
	 */
	int SET_INSTRUCTION = 21;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_INSTRUCTION__ANNOTATIONS = COMPONENT_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_INSTRUCTION__INTERNAL_CACHED_TYPE = COMPONENT_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Dest</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_INSTRUCTION__DEST = COMPONENT_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_INSTRUCTION__SOURCE = COMPONENT_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Set Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_INSTRUCTION_FEATURE_COUNT = COMPONENT_INSTRUCTION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.AddressComponentImpl <em>Address Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.AddressComponentImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getAddressComponent()
	 * @generated
	 */
	int ADDRESS_COMPONENT = 22;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS_COMPONENT__ANNOTATIONS = COMPONENT_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS_COMPONENT__INTERNAL_CACHED_TYPE = COMPONENT_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Address</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS_COMPONENT__ADDRESS = COMPONENT_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Address Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS_COMPONENT_FEATURE_COUNT = COMPONENT_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.AddressInstructionImpl <em>Address Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.AddressInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getAddressInstruction()
	 * @generated
	 */
	int ADDRESS_INSTRUCTION = 23;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS_INSTRUCTION__ANNOTATIONS = ADDRESS_COMPONENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS_INSTRUCTION__INTERNAL_CACHED_TYPE = ADDRESS_COMPONENT__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Address</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS_INSTRUCTION__ADDRESS = ADDRESS_COMPONENT__ADDRESS;

	/**
	 * The number of structural features of the '<em>Address Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS_INSTRUCTION_FEATURE_COUNT = ADDRESS_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.IndirInstructionImpl <em>Indir Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.IndirInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getIndirInstruction()
	 * @generated
	 */
	int INDIR_INSTRUCTION = 24;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDIR_INSTRUCTION__ANNOTATIONS = ADDRESS_COMPONENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDIR_INSTRUCTION__INTERNAL_CACHED_TYPE = ADDRESS_COMPONENT__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Address</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDIR_INSTRUCTION__ADDRESS = ADDRESS_COMPONENT__ADDRESS;

	/**
	 * The number of structural features of the '<em>Indir Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDIR_INSTRUCTION_FEATURE_COUNT = ADDRESS_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.CallInstructionImpl <em>Call Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.CallInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getCallInstruction()
	 * @generated
	 */
	int CALL_INSTRUCTION = 25;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_INSTRUCTION__ANNOTATIONS = ADDRESS_COMPONENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_INSTRUCTION__INTERNAL_CACHED_TYPE = ADDRESS_COMPONENT__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Address</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_INSTRUCTION__ADDRESS = ADDRESS_COMPONENT__ADDRESS;

	/**
	 * The feature id for the '<em><b>Args</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_INSTRUCTION__ARGS = ADDRESS_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Call Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_INSTRUCTION_FEATURE_COUNT = ADDRESS_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.MethodCallInstructionImpl <em>Method Call Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.MethodCallInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getMethodCallInstruction()
	 * @generated
	 */
	int METHOD_CALL_INSTRUCTION = 26;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_CALL_INSTRUCTION__ANNOTATIONS = ADDRESS_COMPONENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_CALL_INSTRUCTION__INTERNAL_CACHED_TYPE = ADDRESS_COMPONENT__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Address</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_CALL_INSTRUCTION__ADDRESS = ADDRESS_COMPONENT__ADDRESS;

	/**
	 * The feature id for the '<em><b>Args</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_CALL_INSTRUCTION__ARGS = ADDRESS_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_CALL_INSTRUCTION__NAME = ADDRESS_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Method Call Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_CALL_INSTRUCTION_FEATURE_COUNT = ADDRESS_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.ArrayInstructionImpl <em>Array Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.ArrayInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getArrayInstruction()
	 * @generated
	 */
	int ARRAY_INSTRUCTION = 27;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_INSTRUCTION__ANNOTATIONS = COMPONENT_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_INSTRUCTION__INTERNAL_CACHED_TYPE = COMPONENT_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Dest</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_INSTRUCTION__DEST = COMPONENT_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Index</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_INSTRUCTION__INDEX = COMPONENT_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Array Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_INSTRUCTION_FEATURE_COUNT = COMPONENT_INSTRUCTION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.SimpleArrayInstructionImpl <em>Simple Array Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.SimpleArrayInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getSimpleArrayInstruction()
	 * @generated
	 */
	int SIMPLE_ARRAY_INSTRUCTION = 28;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_ARRAY_INSTRUCTION__ANNOTATIONS = ARRAY_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_ARRAY_INSTRUCTION__INTERNAL_CACHED_TYPE = ARRAY_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Dest</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_ARRAY_INSTRUCTION__DEST = ARRAY_INSTRUCTION__DEST;

	/**
	 * The feature id for the '<em><b>Index</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_ARRAY_INSTRUCTION__INDEX = ARRAY_INSTRUCTION__INDEX;

	/**
	 * The number of structural features of the '<em>Simple Array Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_ARRAY_INSTRUCTION_FEATURE_COUNT = ARRAY_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.RangeArrayInstructionImpl <em>Range Array Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.RangeArrayInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getRangeArrayInstruction()
	 * @generated
	 */
	int RANGE_ARRAY_INSTRUCTION = 29;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ARRAY_INSTRUCTION__ANNOTATIONS = ARRAY_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ARRAY_INSTRUCTION__INTERNAL_CACHED_TYPE = ARRAY_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Dest</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ARRAY_INSTRUCTION__DEST = ARRAY_INSTRUCTION__DEST;

	/**
	 * The feature id for the '<em><b>Index</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ARRAY_INSTRUCTION__INDEX = ARRAY_INSTRUCTION__INDEX;

	/**
	 * The number of structural features of the '<em>Range Array Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_ARRAY_INSTRUCTION_FEATURE_COUNT = ARRAY_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.BranchInstructionImpl <em>Branch Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.BranchInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getBranchInstruction()
	 * @generated
	 */
	int BRANCH_INSTRUCTION = 30;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_INSTRUCTION__ANNOTATIONS = COMPONENT_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_INSTRUCTION__INTERNAL_CACHED_TYPE = COMPONENT_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The number of structural features of the '<em>Branch Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_INSTRUCTION_FEATURE_COUNT = COMPONENT_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.CondInstructionImpl <em>Cond Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.CondInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getCondInstruction()
	 * @generated
	 */
	int COND_INSTRUCTION = 31;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COND_INSTRUCTION__ANNOTATIONS = BRANCH_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COND_INSTRUCTION__INTERNAL_CACHED_TYPE = BRANCH_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COND_INSTRUCTION__LABEL = BRANCH_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cond</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COND_INSTRUCTION__COND = BRANCH_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Cond Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COND_INSTRUCTION_FEATURE_COUNT = BRANCH_INSTRUCTION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.BreakInstructionImpl <em>Break Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.BreakInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getBreakInstruction()
	 * @generated
	 */
	int BREAK_INSTRUCTION = 32;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_INSTRUCTION__ANNOTATIONS = BRANCH_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_INSTRUCTION__INTERNAL_CACHED_TYPE = BRANCH_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The number of structural features of the '<em>Break Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_INSTRUCTION_FEATURE_COUNT = BRANCH_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.ContinueInstructionImpl <em>Continue Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.ContinueInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getContinueInstruction()
	 * @generated
	 */
	int CONTINUE_INSTRUCTION = 33;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTINUE_INSTRUCTION__ANNOTATIONS = BRANCH_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTINUE_INSTRUCTION__INTERNAL_CACHED_TYPE = BRANCH_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The number of structural features of the '<em>Continue Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTINUE_INSTRUCTION_FEATURE_COUNT = BRANCH_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.GotoInstructionImpl <em>Goto Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.GotoInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getGotoInstruction()
	 * @generated
	 */
	int GOTO_INSTRUCTION = 34;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_INSTRUCTION__ANNOTATIONS = BRANCH_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_INSTRUCTION__INTERNAL_CACHED_TYPE = BRANCH_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Label Instruction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_INSTRUCTION__LABEL_INSTRUCTION = BRANCH_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Label Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_INSTRUCTION__LABEL_NAME = BRANCH_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Goto Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_INSTRUCTION_FEATURE_COUNT = BRANCH_INSTRUCTION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.RetInstructionImpl <em>Ret Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.RetInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getRetInstruction()
	 * @generated
	 */
	int RET_INSTRUCTION = 36;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RET_INSTRUCTION__ANNOTATIONS = EXPR_COMPONENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RET_INSTRUCTION__INTERNAL_CACHED_TYPE = EXPR_COMPONENT__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RET_INSTRUCTION__EXPR = EXPR_COMPONENT__EXPR;

	/**
	 * The number of structural features of the '<em>Ret Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RET_INSTRUCTION_FEATURE_COUNT = EXPR_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.ConvertInstructionImpl <em>Convert Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.ConvertInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getConvertInstruction()
	 * @generated
	 */
	int CONVERT_INSTRUCTION = 37;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONVERT_INSTRUCTION__ANNOTATIONS = EXPR_COMPONENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONVERT_INSTRUCTION__INTERNAL_CACHED_TYPE = EXPR_COMPONENT__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONVERT_INSTRUCTION__EXPR = EXPR_COMPONENT__EXPR;

	/**
	 * The number of structural features of the '<em>Convert Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONVERT_INSTRUCTION_FEATURE_COUNT = EXPR_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.FieldInstructionImpl <em>Field Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.FieldInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getFieldInstruction()
	 * @generated
	 */
	int FIELD_INSTRUCTION = 38;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INSTRUCTION__ANNOTATIONS = EXPR_COMPONENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INSTRUCTION__INTERNAL_CACHED_TYPE = EXPR_COMPONENT__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INSTRUCTION__EXPR = EXPR_COMPONENT__EXPR;

	/**
	 * The feature id for the '<em><b>Field</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INSTRUCTION__FIELD = EXPR_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Field Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INSTRUCTION_FEATURE_COUNT = EXPR_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.EnumeratorInstructionImpl <em>Enumerator Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.EnumeratorInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getEnumeratorInstruction()
	 * @generated
	 */
	int ENUMERATOR_INSTRUCTION = 39;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATOR_INSTRUCTION__ANNOTATIONS = INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATOR_INSTRUCTION__INTERNAL_CACHED_TYPE = INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Enumerator</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATOR_INSTRUCTION__ENUMERATOR = INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Enumerator Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATOR_INSTRUCTION_FEATURE_COUNT = INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.RangeInstructionImpl <em>Range Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.RangeInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getRangeInstruction()
	 * @generated
	 */
	int RANGE_INSTRUCTION = 40;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_INSTRUCTION__ANNOTATIONS = EXPR_COMPONENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_INSTRUCTION__INTERNAL_CACHED_TYPE = EXPR_COMPONENT__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_INSTRUCTION__EXPR = EXPR_COMPONENT__EXPR;

	/**
	 * The feature id for the '<em><b>Low</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_INSTRUCTION__LOW = EXPR_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>High</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_INSTRUCTION__HIGH = EXPR_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Range Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_INSTRUCTION_FEATURE_COUNT = EXPR_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.CaseInstructionImpl <em>Case Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.CaseInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getCaseInstruction()
	 * @generated
	 */
	int CASE_INSTRUCTION = 41;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_INSTRUCTION__ANNOTATIONS = EXPR_COMPONENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_INSTRUCTION__INTERNAL_CACHED_TYPE = EXPR_COMPONENT__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_INSTRUCTION__EXPR = EXPR_COMPONENT__EXPR;

	/**
	 * The number of structural features of the '<em>Case Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_INSTRUCTION_FEATURE_COUNT = EXPR_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.GenericInstructionImpl <em>Generic Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.GenericInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getGenericInstruction()
	 * @generated
	 */
	int GENERIC_INSTRUCTION = 42;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_INSTRUCTION__ANNOTATIONS = CHILDREN_LIST_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_INSTRUCTION__INTERNAL_CACHED_TYPE = CHILDREN_LIST_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_INSTRUCTION__CHILDREN = CHILDREN_LIST_INSTRUCTION__CHILDREN;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_INSTRUCTION__NAME = CHILDREN_LIST_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Generic Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_INSTRUCTION_FEATURE_COUNT = CHILDREN_LIST_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.ArithmeticInstructionImpl <em>Arithmetic Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.ArithmeticInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getArithmeticInstruction()
	 * @generated
	 */
	int ARITHMETIC_INSTRUCTION = 43;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARITHMETIC_INSTRUCTION__ANNOTATIONS = GENERIC_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARITHMETIC_INSTRUCTION__INTERNAL_CACHED_TYPE = GENERIC_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARITHMETIC_INSTRUCTION__CHILDREN = GENERIC_INSTRUCTION__CHILDREN;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARITHMETIC_INSTRUCTION__NAME = GENERIC_INSTRUCTION__NAME;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARITHMETIC_INSTRUCTION__OPERATOR = GENERIC_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Arithmetic Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARITHMETIC_INSTRUCTION_FEATURE_COUNT = GENERIC_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.ComparisonInstructionImpl <em>Comparison Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.ComparisonInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getComparisonInstruction()
	 * @generated
	 */
	int COMPARISON_INSTRUCTION = 44;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARISON_INSTRUCTION__ANNOTATIONS = GENERIC_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARISON_INSTRUCTION__INTERNAL_CACHED_TYPE = GENERIC_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARISON_INSTRUCTION__CHILDREN = GENERIC_INSTRUCTION__CHILDREN;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARISON_INSTRUCTION__NAME = GENERIC_INSTRUCTION__NAME;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARISON_INSTRUCTION__OPERATOR = GENERIC_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Comparison Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARISON_INSTRUCTION_FEATURE_COUNT = GENERIC_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.LogicalInstructionImpl <em>Logical Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.LogicalInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getLogicalInstruction()
	 * @generated
	 */
	int LOGICAL_INSTRUCTION = 45;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_INSTRUCTION__ANNOTATIONS = GENERIC_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_INSTRUCTION__INTERNAL_CACHED_TYPE = GENERIC_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_INSTRUCTION__CHILDREN = GENERIC_INSTRUCTION__CHILDREN;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_INSTRUCTION__NAME = GENERIC_INSTRUCTION__NAME;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_INSTRUCTION__OPERATOR = GENERIC_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Logical Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_INSTRUCTION_FEATURE_COUNT = GENERIC_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.BitwiseInstructionImpl <em>Bitwise Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.BitwiseInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getBitwiseInstruction()
	 * @generated
	 */
	int BITWISE_INSTRUCTION = 46;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BITWISE_INSTRUCTION__ANNOTATIONS = GENERIC_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BITWISE_INSTRUCTION__INTERNAL_CACHED_TYPE = GENERIC_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BITWISE_INSTRUCTION__CHILDREN = GENERIC_INSTRUCTION__CHILDREN;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BITWISE_INSTRUCTION__NAME = GENERIC_INSTRUCTION__NAME;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BITWISE_INSTRUCTION__OPERATOR = GENERIC_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Bitwise Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BITWISE_INSTRUCTION_FEATURE_COUNT = GENERIC_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.SimdInstructionImpl <em>Simd Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.SimdInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getSimdInstruction()
	 * @generated
	 */
	int SIMD_INSTRUCTION = 47;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_INSTRUCTION__ANNOTATIONS = CHILDREN_LIST_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_INSTRUCTION__INTERNAL_CACHED_TYPE = CHILDREN_LIST_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_INSTRUCTION__CHILDREN = CHILDREN_LIST_INSTRUCTION__CHILDREN;

	/**
	 * The number of structural features of the '<em>Simd Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_INSTRUCTION_FEATURE_COUNT = CHILDREN_LIST_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.SimdGenericInstructionImpl <em>Simd Generic Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.SimdGenericInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getSimdGenericInstruction()
	 * @generated
	 */
	int SIMD_GENERIC_INSTRUCTION = 48;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_GENERIC_INSTRUCTION__ANNOTATIONS = GENERIC_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_GENERIC_INSTRUCTION__INTERNAL_CACHED_TYPE = GENERIC_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_GENERIC_INSTRUCTION__CHILDREN = GENERIC_INSTRUCTION__CHILDREN;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_GENERIC_INSTRUCTION__NAME = GENERIC_INSTRUCTION__NAME;

	/**
	 * The number of structural features of the '<em>Simd Generic Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_GENERIC_INSTRUCTION_FEATURE_COUNT = GENERIC_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.SimdPackInstructionImpl <em>Simd Pack Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.SimdPackInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getSimdPackInstruction()
	 * @generated
	 */
	int SIMD_PACK_INSTRUCTION = 49;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_PACK_INSTRUCTION__ANNOTATIONS = SIMD_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_PACK_INSTRUCTION__INTERNAL_CACHED_TYPE = SIMD_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_PACK_INSTRUCTION__CHILDREN = SIMD_INSTRUCTION__CHILDREN;

	/**
	 * The number of structural features of the '<em>Simd Pack Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_PACK_INSTRUCTION_FEATURE_COUNT = SIMD_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.SimdExtractInstructionImpl <em>Simd Extract Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.SimdExtractInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getSimdExtractInstruction()
	 * @generated
	 */
	int SIMD_EXTRACT_INSTRUCTION = 50;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_EXTRACT_INSTRUCTION__ANNOTATIONS = SIMD_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_EXTRACT_INSTRUCTION__INTERNAL_CACHED_TYPE = SIMD_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_EXTRACT_INSTRUCTION__CHILDREN = SIMD_INSTRUCTION__CHILDREN;

	/**
	 * The number of structural features of the '<em>Simd Extract Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_EXTRACT_INSTRUCTION_FEATURE_COUNT = SIMD_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.SimdShuffleInstructionImpl <em>Simd Shuffle Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.SimdShuffleInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getSimdShuffleInstruction()
	 * @generated
	 */
	int SIMD_SHUFFLE_INSTRUCTION = 51;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_SHUFFLE_INSTRUCTION__ANNOTATIONS = SIMD_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_SHUFFLE_INSTRUCTION__INTERNAL_CACHED_TYPE = SIMD_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_SHUFFLE_INSTRUCTION__CHILDREN = SIMD_INSTRUCTION__CHILDREN;

	/**
	 * The feature id for the '<em><b>Permutation</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_SHUFFLE_INSTRUCTION__PERMUTATION = SIMD_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Simd Shuffle Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_SHUFFLE_INSTRUCTION_FEATURE_COUNT = SIMD_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.instrs.impl.SimdExpandInstructionImpl <em>Simd Expand Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.SimdExpandInstructionImpl
	 * @see gecos.instrs.impl.InstrsPackageImpl#getSimdExpandInstruction()
	 * @generated
	 */
	int SIMD_EXPAND_INSTRUCTION = 52;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_EXPAND_INSTRUCTION__ANNOTATIONS = SIMD_INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_EXPAND_INSTRUCTION__INTERNAL_CACHED_TYPE = SIMD_INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_EXPAND_INSTRUCTION__CHILDREN = SIMD_INSTRUCTION__CHILDREN;

	/**
	 * The feature id for the '<em><b>Permutation</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_EXPAND_INSTRUCTION__PERMUTATION = SIMD_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Simd Expand Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_EXPAND_INSTRUCTION_FEATURE_COUNT = SIMD_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.instrs.ArithmeticOperator <em>Arithmetic Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.ArithmeticOperator
	 * @see gecos.instrs.impl.InstrsPackageImpl#getArithmeticOperator()
	 * @generated
	 */
	int ARITHMETIC_OPERATOR = 53;

	/**
	 * The meta object id for the '{@link gecos.instrs.ComparisonOperator <em>Comparison Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.ComparisonOperator
	 * @see gecos.instrs.impl.InstrsPackageImpl#getComparisonOperator()
	 * @generated
	 */
	int COMPARISON_OPERATOR = 54;

	/**
	 * The meta object id for the '{@link gecos.instrs.LogicalOperator <em>Logical Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.LogicalOperator
	 * @see gecos.instrs.impl.InstrsPackageImpl#getLogicalOperator()
	 * @generated
	 */
	int LOGICAL_OPERATOR = 55;

	/**
	 * The meta object id for the '{@link gecos.instrs.BitwiseOperator <em>Bitwise Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.BitwiseOperator
	 * @see gecos.instrs.impl.InstrsPackageImpl#getBitwiseOperator()
	 * @generated
	 */
	int BITWISE_OPERATOR = 56;

	/**
	 * The meta object id for the '{@link gecos.instrs.SelectOperator <em>Select Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.SelectOperator
	 * @see gecos.instrs.impl.InstrsPackageImpl#getSelectOperator()
	 * @generated
	 */
	int SELECT_OPERATOR = 57;

	/**
	 * The meta object id for the '{@link gecos.instrs.ReductionOperator <em>Reduction Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.ReductionOperator
	 * @see gecos.instrs.impl.InstrsPackageImpl#getReductionOperator()
	 * @generated
	 */
	int REDUCTION_OPERATOR = 58;

	/**
	 * The meta object id for the '{@link gecos.instrs.BranchType <em>Branch Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.BranchType
	 * @see gecos.instrs.impl.InstrsPackageImpl#getBranchType()
	 * @generated
	 */
	int BRANCH_TYPE = 59;

	/**
	 * The meta object id for the '<em>String</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see gecos.instrs.impl.InstrsPackageImpl#getString()
	 * @generated
	 */
	int STRING = 60;

	/**
	 * The meta object id for the '<em>int</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.InstrsPackageImpl#getint()
	 * @generated
	 */
	int INT = 61;

	/**
	 * The meta object id for the '<em>long</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.InstrsPackageImpl#getlong()
	 * @generated
	 */
	int LONG = 62;

	/**
	 * The meta object id for the '<em>double</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.instrs.impl.InstrsPackageImpl#getdouble()
	 * @generated
	 */
	int DOUBLE = 63;


	/**
	 * Returns the meta object for class '{@link gecos.instrs.InstrsVisitor <em>Visitor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visitor</em>'.
	 * @see gecos.instrs.InstrsVisitor
	 * @generated
	 */
	EClass getInstrsVisitor();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.InstrsVisitable <em>Visitable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visitable</em>'.
	 * @see gecos.instrs.InstrsVisitable
	 * @generated
	 */
	EClass getInstrsVisitable();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.Instruction <em>Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Instruction</em>'.
	 * @see gecos.instrs.Instruction
	 * @generated
	 */
	EClass getInstruction();

	/**
	 * Returns the meta object for the reference '{@link gecos.instrs.Instruction#get___internal_cached___type <em>internal cached type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>internal cached type</em>'.
	 * @see gecos.instrs.Instruction#get___internal_cached___type()
	 * @see #getInstruction()
	 * @generated
	 */
	EReference getInstruction____internal_cached___type();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.DummyInstruction <em>Dummy Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dummy Instruction</em>'.
	 * @see gecos.instrs.DummyInstruction
	 * @generated
	 */
	EClass getDummyInstruction();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.LabelInstruction <em>Label Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Label Instruction</em>'.
	 * @see gecos.instrs.LabelInstruction
	 * @generated
	 */
	EClass getLabelInstruction();

	/**
	 * Returns the meta object for the attribute '{@link gecos.instrs.LabelInstruction#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see gecos.instrs.LabelInstruction#getName()
	 * @see #getLabelInstruction()
	 * @generated
	 */
	EAttribute getLabelInstruction_Name();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.SymbolInstruction <em>Symbol Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Symbol Instruction</em>'.
	 * @see gecos.instrs.SymbolInstruction
	 * @generated
	 */
	EClass getSymbolInstruction();

	/**
	 * Returns the meta object for the reference '{@link gecos.instrs.SymbolInstruction#getSymbol <em>Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Symbol</em>'.
	 * @see gecos.instrs.SymbolInstruction#getSymbol()
	 * @see #getSymbolInstruction()
	 * @generated
	 */
	EReference getSymbolInstruction_Symbol();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.NumberedSymbolInstruction <em>Numbered Symbol Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Numbered Symbol Instruction</em>'.
	 * @see gecos.instrs.NumberedSymbolInstruction
	 * @generated
	 */
	EClass getNumberedSymbolInstruction();

	/**
	 * Returns the meta object for the attribute '{@link gecos.instrs.NumberedSymbolInstruction#getNumber <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number</em>'.
	 * @see gecos.instrs.NumberedSymbolInstruction#getNumber()
	 * @see #getNumberedSymbolInstruction()
	 * @generated
	 */
	EAttribute getNumberedSymbolInstruction_Number();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.SSADefSymbol <em>SSA Def Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SSA Def Symbol</em>'.
	 * @see gecos.instrs.SSADefSymbol
	 * @generated
	 */
	EClass getSSADefSymbol();

	/**
	 * Returns the meta object for the reference list '{@link gecos.instrs.SSADefSymbol#getSSAUses <em>SSA Uses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>SSA Uses</em>'.
	 * @see gecos.instrs.SSADefSymbol#getSSAUses()
	 * @see #getSSADefSymbol()
	 * @generated
	 */
	EReference getSSADefSymbol_SSAUses();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.SSAUseSymbol <em>SSA Use Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SSA Use Symbol</em>'.
	 * @see gecos.instrs.SSAUseSymbol
	 * @generated
	 */
	EClass getSSAUseSymbol();

	/**
	 * Returns the meta object for the reference '{@link gecos.instrs.SSAUseSymbol#getDef <em>Def</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Def</em>'.
	 * @see gecos.instrs.SSAUseSymbol#getDef()
	 * @see #getSSAUseSymbol()
	 * @generated
	 */
	EReference getSSAUseSymbol_Def();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.SizeofInstruction <em>Sizeof Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sizeof Instruction</em>'.
	 * @see gecos.instrs.SizeofInstruction
	 * @generated
	 */
	EClass getSizeofInstruction();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.SizeofTypeInstruction <em>Sizeof Type Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sizeof Type Instruction</em>'.
	 * @see gecos.instrs.SizeofTypeInstruction
	 * @generated
	 */
	EClass getSizeofTypeInstruction();

	/**
	 * Returns the meta object for the reference '{@link gecos.instrs.SizeofTypeInstruction#getTargetType <em>Target Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target Type</em>'.
	 * @see gecos.instrs.SizeofTypeInstruction#getTargetType()
	 * @see #getSizeofTypeInstruction()
	 * @generated
	 */
	EReference getSizeofTypeInstruction_TargetType();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.SizeofValueInstruction <em>Sizeof Value Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sizeof Value Instruction</em>'.
	 * @see gecos.instrs.SizeofValueInstruction
	 * @generated
	 */
	EClass getSizeofValueInstruction();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.ConstantInstruction <em>Constant Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constant Instruction</em>'.
	 * @see gecos.instrs.ConstantInstruction
	 * @generated
	 */
	EClass getConstantInstruction();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.IntInstruction <em>Int Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Instruction</em>'.
	 * @see gecos.instrs.IntInstruction
	 * @generated
	 */
	EClass getIntInstruction();

	/**
	 * Returns the meta object for the attribute '{@link gecos.instrs.IntInstruction#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see gecos.instrs.IntInstruction#getValue()
	 * @see #getIntInstruction()
	 * @generated
	 */
	EAttribute getIntInstruction_Value();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.FloatInstruction <em>Float Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Float Instruction</em>'.
	 * @see gecos.instrs.FloatInstruction
	 * @generated
	 */
	EClass getFloatInstruction();

	/**
	 * Returns the meta object for the attribute '{@link gecos.instrs.FloatInstruction#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see gecos.instrs.FloatInstruction#getValue()
	 * @see #getFloatInstruction()
	 * @generated
	 */
	EAttribute getFloatInstruction_Value();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.StringInstruction <em>String Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Instruction</em>'.
	 * @see gecos.instrs.StringInstruction
	 * @generated
	 */
	EClass getStringInstruction();

	/**
	 * Returns the meta object for the attribute '{@link gecos.instrs.StringInstruction#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see gecos.instrs.StringInstruction#getValue()
	 * @see #getStringInstruction()
	 * @generated
	 */
	EAttribute getStringInstruction_Value();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.ComplexInstruction <em>Complex Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Complex Instruction</em>'.
	 * @see gecos.instrs.ComplexInstruction
	 * @generated
	 */
	EClass getComplexInstruction();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.ChildrenListInstruction <em>Children List Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Children List Instruction</em>'.
	 * @see gecos.instrs.ChildrenListInstruction
	 * @generated
	 */
	EClass getChildrenListInstruction();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.instrs.ChildrenListInstruction#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Children</em>'.
	 * @see gecos.instrs.ChildrenListInstruction#getChildren()
	 * @see #getChildrenListInstruction()
	 * @generated
	 */
	EReference getChildrenListInstruction_Children();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.ArrayValueInstruction <em>Array Value Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Array Value Instruction</em>'.
	 * @see gecos.instrs.ArrayValueInstruction
	 * @generated
	 */
	EClass getArrayValueInstruction();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.PhiInstruction <em>Phi Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Phi Instruction</em>'.
	 * @see gecos.instrs.PhiInstruction
	 * @generated
	 */
	EClass getPhiInstruction();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.ComponentInstruction <em>Component Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Instruction</em>'.
	 * @see gecos.instrs.ComponentInstruction
	 * @generated
	 */
	EClass getComponentInstruction();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.SetInstruction <em>Set Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Set Instruction</em>'.
	 * @see gecos.instrs.SetInstruction
	 * @generated
	 */
	EClass getSetInstruction();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.instrs.SetInstruction#getDest <em>Dest</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Dest</em>'.
	 * @see gecos.instrs.SetInstruction#getDest()
	 * @see #getSetInstruction()
	 * @generated
	 */
	EReference getSetInstruction_Dest();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.instrs.SetInstruction#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Source</em>'.
	 * @see gecos.instrs.SetInstruction#getSource()
	 * @see #getSetInstruction()
	 * @generated
	 */
	EReference getSetInstruction_Source();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.AddressComponent <em>Address Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Address Component</em>'.
	 * @see gecos.instrs.AddressComponent
	 * @generated
	 */
	EClass getAddressComponent();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.instrs.AddressComponent#getAddress <em>Address</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Address</em>'.
	 * @see gecos.instrs.AddressComponent#getAddress()
	 * @see #getAddressComponent()
	 * @generated
	 */
	EReference getAddressComponent_Address();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.AddressInstruction <em>Address Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Address Instruction</em>'.
	 * @see gecos.instrs.AddressInstruction
	 * @generated
	 */
	EClass getAddressInstruction();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.IndirInstruction <em>Indir Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Indir Instruction</em>'.
	 * @see gecos.instrs.IndirInstruction
	 * @generated
	 */
	EClass getIndirInstruction();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.CallInstruction <em>Call Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Call Instruction</em>'.
	 * @see gecos.instrs.CallInstruction
	 * @generated
	 */
	EClass getCallInstruction();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.instrs.CallInstruction#getArgs <em>Args</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Args</em>'.
	 * @see gecos.instrs.CallInstruction#getArgs()
	 * @see #getCallInstruction()
	 * @generated
	 */
	EReference getCallInstruction_Args();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.MethodCallInstruction <em>Method Call Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Method Call Instruction</em>'.
	 * @see gecos.instrs.MethodCallInstruction
	 * @generated
	 */
	EClass getMethodCallInstruction();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.instrs.MethodCallInstruction#getArgs <em>Args</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Args</em>'.
	 * @see gecos.instrs.MethodCallInstruction#getArgs()
	 * @see #getMethodCallInstruction()
	 * @generated
	 */
	EReference getMethodCallInstruction_Args();

	/**
	 * Returns the meta object for the attribute '{@link gecos.instrs.MethodCallInstruction#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see gecos.instrs.MethodCallInstruction#getName()
	 * @see #getMethodCallInstruction()
	 * @generated
	 */
	EAttribute getMethodCallInstruction_Name();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.ArrayInstruction <em>Array Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Array Instruction</em>'.
	 * @see gecos.instrs.ArrayInstruction
	 * @generated
	 */
	EClass getArrayInstruction();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.instrs.ArrayInstruction#getDest <em>Dest</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Dest</em>'.
	 * @see gecos.instrs.ArrayInstruction#getDest()
	 * @see #getArrayInstruction()
	 * @generated
	 */
	EReference getArrayInstruction_Dest();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.instrs.ArrayInstruction#getIndex <em>Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Index</em>'.
	 * @see gecos.instrs.ArrayInstruction#getIndex()
	 * @see #getArrayInstruction()
	 * @generated
	 */
	EReference getArrayInstruction_Index();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.SimpleArrayInstruction <em>Simple Array Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple Array Instruction</em>'.
	 * @see gecos.instrs.SimpleArrayInstruction
	 * @generated
	 */
	EClass getSimpleArrayInstruction();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.RangeArrayInstruction <em>Range Array Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Range Array Instruction</em>'.
	 * @see gecos.instrs.RangeArrayInstruction
	 * @generated
	 */
	EClass getRangeArrayInstruction();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.BranchInstruction <em>Branch Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Branch Instruction</em>'.
	 * @see gecos.instrs.BranchInstruction
	 * @generated
	 */
	EClass getBranchInstruction();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.CondInstruction <em>Cond Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cond Instruction</em>'.
	 * @see gecos.instrs.CondInstruction
	 * @generated
	 */
	EClass getCondInstruction();

	/**
	 * Returns the meta object for the attribute '{@link gecos.instrs.CondInstruction#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see gecos.instrs.CondInstruction#getLabel()
	 * @see #getCondInstruction()
	 * @generated
	 */
	EAttribute getCondInstruction_Label();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.instrs.CondInstruction#getCond <em>Cond</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Cond</em>'.
	 * @see gecos.instrs.CondInstruction#getCond()
	 * @see #getCondInstruction()
	 * @generated
	 */
	EReference getCondInstruction_Cond();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.BreakInstruction <em>Break Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Break Instruction</em>'.
	 * @see gecos.instrs.BreakInstruction
	 * @generated
	 */
	EClass getBreakInstruction();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.ContinueInstruction <em>Continue Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Continue Instruction</em>'.
	 * @see gecos.instrs.ContinueInstruction
	 * @generated
	 */
	EClass getContinueInstruction();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.GotoInstruction <em>Goto Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Goto Instruction</em>'.
	 * @see gecos.instrs.GotoInstruction
	 * @generated
	 */
	EClass getGotoInstruction();

	/**
	 * Returns the meta object for the reference '{@link gecos.instrs.GotoInstruction#getLabelInstruction <em>Label Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Label Instruction</em>'.
	 * @see gecos.instrs.GotoInstruction#getLabelInstruction()
	 * @see #getGotoInstruction()
	 * @generated
	 */
	EReference getGotoInstruction_LabelInstruction();

	/**
	 * Returns the meta object for the attribute '{@link gecos.instrs.GotoInstruction#getLabelName <em>Label Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label Name</em>'.
	 * @see gecos.instrs.GotoInstruction#getLabelName()
	 * @see #getGotoInstruction()
	 * @generated
	 */
	EAttribute getGotoInstruction_LabelName();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.ExprComponent <em>Expr Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expr Component</em>'.
	 * @see gecos.instrs.ExprComponent
	 * @generated
	 */
	EClass getExprComponent();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.instrs.ExprComponent#getExpr <em>Expr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expr</em>'.
	 * @see gecos.instrs.ExprComponent#getExpr()
	 * @see #getExprComponent()
	 * @generated
	 */
	EReference getExprComponent_Expr();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.RetInstruction <em>Ret Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ret Instruction</em>'.
	 * @see gecos.instrs.RetInstruction
	 * @generated
	 */
	EClass getRetInstruction();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.ConvertInstruction <em>Convert Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Convert Instruction</em>'.
	 * @see gecos.instrs.ConvertInstruction
	 * @generated
	 */
	EClass getConvertInstruction();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.FieldInstruction <em>Field Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Field Instruction</em>'.
	 * @see gecos.instrs.FieldInstruction
	 * @generated
	 */
	EClass getFieldInstruction();

	/**
	 * Returns the meta object for the reference '{@link gecos.instrs.FieldInstruction#getField <em>Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Field</em>'.
	 * @see gecos.instrs.FieldInstruction#getField()
	 * @see #getFieldInstruction()
	 * @generated
	 */
	EReference getFieldInstruction_Field();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.EnumeratorInstruction <em>Enumerator Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enumerator Instruction</em>'.
	 * @see gecos.instrs.EnumeratorInstruction
	 * @generated
	 */
	EClass getEnumeratorInstruction();

	/**
	 * Returns the meta object for the reference '{@link gecos.instrs.EnumeratorInstruction#getEnumerator <em>Enumerator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Enumerator</em>'.
	 * @see gecos.instrs.EnumeratorInstruction#getEnumerator()
	 * @see #getEnumeratorInstruction()
	 * @generated
	 */
	EReference getEnumeratorInstruction_Enumerator();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.RangeInstruction <em>Range Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Range Instruction</em>'.
	 * @see gecos.instrs.RangeInstruction
	 * @generated
	 */
	EClass getRangeInstruction();

	/**
	 * Returns the meta object for the attribute '{@link gecos.instrs.RangeInstruction#getLow <em>Low</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Low</em>'.
	 * @see gecos.instrs.RangeInstruction#getLow()
	 * @see #getRangeInstruction()
	 * @generated
	 */
	EAttribute getRangeInstruction_Low();

	/**
	 * Returns the meta object for the attribute '{@link gecos.instrs.RangeInstruction#getHigh <em>High</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>High</em>'.
	 * @see gecos.instrs.RangeInstruction#getHigh()
	 * @see #getRangeInstruction()
	 * @generated
	 */
	EAttribute getRangeInstruction_High();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.CaseInstruction <em>Case Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Case Instruction</em>'.
	 * @see gecos.instrs.CaseInstruction
	 * @generated
	 */
	EClass getCaseInstruction();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.GenericInstruction <em>Generic Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Generic Instruction</em>'.
	 * @see gecos.instrs.GenericInstruction
	 * @generated
	 */
	EClass getGenericInstruction();

	/**
	 * Returns the meta object for the attribute '{@link gecos.instrs.GenericInstruction#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see gecos.instrs.GenericInstruction#getName()
	 * @see #getGenericInstruction()
	 * @generated
	 */
	EAttribute getGenericInstruction_Name();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.ArithmeticInstruction <em>Arithmetic Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Arithmetic Instruction</em>'.
	 * @see gecos.instrs.ArithmeticInstruction
	 * @generated
	 */
	EClass getArithmeticInstruction();

	/**
	 * Returns the meta object for the attribute '{@link gecos.instrs.ArithmeticInstruction#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see gecos.instrs.ArithmeticInstruction#getOperator()
	 * @see #getArithmeticInstruction()
	 * @generated
	 */
	EAttribute getArithmeticInstruction_Operator();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.ComparisonInstruction <em>Comparison Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Comparison Instruction</em>'.
	 * @see gecos.instrs.ComparisonInstruction
	 * @generated
	 */
	EClass getComparisonInstruction();

	/**
	 * Returns the meta object for the attribute '{@link gecos.instrs.ComparisonInstruction#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see gecos.instrs.ComparisonInstruction#getOperator()
	 * @see #getComparisonInstruction()
	 * @generated
	 */
	EAttribute getComparisonInstruction_Operator();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.LogicalInstruction <em>Logical Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Logical Instruction</em>'.
	 * @see gecos.instrs.LogicalInstruction
	 * @generated
	 */
	EClass getLogicalInstruction();

	/**
	 * Returns the meta object for the attribute '{@link gecos.instrs.LogicalInstruction#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see gecos.instrs.LogicalInstruction#getOperator()
	 * @see #getLogicalInstruction()
	 * @generated
	 */
	EAttribute getLogicalInstruction_Operator();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.BitwiseInstruction <em>Bitwise Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bitwise Instruction</em>'.
	 * @see gecos.instrs.BitwiseInstruction
	 * @generated
	 */
	EClass getBitwiseInstruction();

	/**
	 * Returns the meta object for the attribute '{@link gecos.instrs.BitwiseInstruction#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see gecos.instrs.BitwiseInstruction#getOperator()
	 * @see #getBitwiseInstruction()
	 * @generated
	 */
	EAttribute getBitwiseInstruction_Operator();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.SimdInstruction <em>Simd Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simd Instruction</em>'.
	 * @see gecos.instrs.SimdInstruction
	 * @generated
	 */
	EClass getSimdInstruction();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.SimdGenericInstruction <em>Simd Generic Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simd Generic Instruction</em>'.
	 * @see gecos.instrs.SimdGenericInstruction
	 * @generated
	 */
	EClass getSimdGenericInstruction();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.SimdPackInstruction <em>Simd Pack Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simd Pack Instruction</em>'.
	 * @see gecos.instrs.SimdPackInstruction
	 * @generated
	 */
	EClass getSimdPackInstruction();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.SimdExtractInstruction <em>Simd Extract Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simd Extract Instruction</em>'.
	 * @see gecos.instrs.SimdExtractInstruction
	 * @generated
	 */
	EClass getSimdExtractInstruction();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.SimdShuffleInstruction <em>Simd Shuffle Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simd Shuffle Instruction</em>'.
	 * @see gecos.instrs.SimdShuffleInstruction
	 * @generated
	 */
	EClass getSimdShuffleInstruction();

	/**
	 * Returns the meta object for the attribute list '{@link gecos.instrs.SimdShuffleInstruction#getPermutation <em>Permutation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Permutation</em>'.
	 * @see gecos.instrs.SimdShuffleInstruction#getPermutation()
	 * @see #getSimdShuffleInstruction()
	 * @generated
	 */
	EAttribute getSimdShuffleInstruction_Permutation();

	/**
	 * Returns the meta object for class '{@link gecos.instrs.SimdExpandInstruction <em>Simd Expand Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simd Expand Instruction</em>'.
	 * @see gecos.instrs.SimdExpandInstruction
	 * @generated
	 */
	EClass getSimdExpandInstruction();

	/**
	 * Returns the meta object for the attribute list '{@link gecos.instrs.SimdExpandInstruction#getPermutation <em>Permutation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Permutation</em>'.
	 * @see gecos.instrs.SimdExpandInstruction#getPermutation()
	 * @see #getSimdExpandInstruction()
	 * @generated
	 */
	EAttribute getSimdExpandInstruction_Permutation();

	/**
	 * Returns the meta object for enum '{@link gecos.instrs.ArithmeticOperator <em>Arithmetic Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Arithmetic Operator</em>'.
	 * @see gecos.instrs.ArithmeticOperator
	 * @generated
	 */
	EEnum getArithmeticOperator();

	/**
	 * Returns the meta object for enum '{@link gecos.instrs.ComparisonOperator <em>Comparison Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Comparison Operator</em>'.
	 * @see gecos.instrs.ComparisonOperator
	 * @generated
	 */
	EEnum getComparisonOperator();

	/**
	 * Returns the meta object for enum '{@link gecos.instrs.LogicalOperator <em>Logical Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Logical Operator</em>'.
	 * @see gecos.instrs.LogicalOperator
	 * @generated
	 */
	EEnum getLogicalOperator();

	/**
	 * Returns the meta object for enum '{@link gecos.instrs.BitwiseOperator <em>Bitwise Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Bitwise Operator</em>'.
	 * @see gecos.instrs.BitwiseOperator
	 * @generated
	 */
	EEnum getBitwiseOperator();

	/**
	 * Returns the meta object for enum '{@link gecos.instrs.SelectOperator <em>Select Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Select Operator</em>'.
	 * @see gecos.instrs.SelectOperator
	 * @generated
	 */
	EEnum getSelectOperator();

	/**
	 * Returns the meta object for enum '{@link gecos.instrs.ReductionOperator <em>Reduction Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Reduction Operator</em>'.
	 * @see gecos.instrs.ReductionOperator
	 * @generated
	 */
	EEnum getReductionOperator();

	/**
	 * Returns the meta object for enum '{@link gecos.instrs.BranchType <em>Branch Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Branch Type</em>'.
	 * @see gecos.instrs.BranchType
	 * @generated
	 */
	EEnum getBranchType();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>String</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getString();

	/**
	 * Returns the meta object for data type '<em>int</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>int</em>'.
	 * @model instanceClass="int"
	 * @generated
	 */
	EDataType getint();

	/**
	 * Returns the meta object for data type '<em>long</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>long</em>'.
	 * @model instanceClass="long"
	 * @generated
	 */
	EDataType getlong();

	/**
	 * Returns the meta object for data type '<em>double</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>double</em>'.
	 * @model instanceClass="double"
	 * @generated
	 */
	EDataType getdouble();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	InstrsFactory getInstrsFactory();

} //InstrsPackage
