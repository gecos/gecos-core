/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.tools.switches;

import org.eclipse.emf.common.util.EList;

import gecos.blocks.BasicBlock;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.ArrayValueInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.CondInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.FieldInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.LabelInstruction;
import gecos.instrs.MethodCallInstruction;
import gecos.instrs.NumberedSymbolInstruction;
import gecos.instrs.PhiInstruction;
import gecos.instrs.RangeInstruction;
import gecos.instrs.RetInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.instrs.util.InstrsSwitch;

public class DefaultInstructionSwitch<T> extends InstrsSwitch<T> {

	

	public void run(BasicBlock bb) {
		for (Instruction i : bb.getInstructions()) {
			doSwitch(i);
		}
	}
	
	/**
	 * ComplexInstruction is an abstract class. Although all classes inheriting
	 * ComplexInstruction have the same structure, that is a list of children,
	 * they should not be visited the same way. Thus, by default, the default
	 * switch returns null, and delegate the treatment to the next case in the
	 * class inheritance hierarchy.
	 */
	@Override
	public T caseComplexInstruction(ComplexInstruction object) {
		return null;
	}

	@Override
	public T caseGenericInstruction(GenericInstruction g) {
		//iterate over a copy of the list in order to prevent from ConcurrentModificationException
		Instruction[] ops = g.getOperands().toArray(new Instruction[g.getOperands().size()]);
		for (int i = 0; i < ops.length; ++i) {
			doSwitch(ops[i]);
		}
		return null;
	}

	@Override
	public T caseCallInstruction(CallInstruction g) {
		//iterate over a copy of the list in order to prevent from ConcurrentModificationException
//		EList<Instruction> listChildren = g.listChildren();
//		Instruction[] ops = listChildren.toArray(new Instruction[listChildren.size()]);
//		for (int i = 0; i < ops.length; ++i) {
//			doSwitch(ops[i]);
//		}
		doSwitch(g.getAddress());
		EList<Instruction> _args = g.getArgs();
		Instruction[] args = _args.toArray(new Instruction[_args.size()]);
		for (int i = 0; i < args.length; ++i) {
			doSwitch(args[i]);
		}
		return null;
	}

	@Override
	public T caseMethodCallInstruction(MethodCallInstruction g) {
		doSwitch(g.getAddress());
		EList<Instruction> args = g.getArgs();
		Instruction[] ops = args.toArray(new Instruction[args.size()]);
		for (int i = 0; i < ops.length; ++i) {
			doSwitch(ops[i]);
		}
		return null;
	}

	public T caseNumberedSymbol(NumberedSymbolInstruction s) {
		return null;
	}

	@Override
	public T caseSetInstruction(SetInstruction s) {
		doSwitch(s.getSource());
		doSwitch(s.getDest());
		return null;
	}

	@Override
	public T caseCondInstruction(CondInstruction g) {
		if (g.getCond() != null) return doSwitch(g.getCond());
		return null;
	}

	@Override
	public T caseRetInstruction(RetInstruction g) {
		if (g.getExpr() != null)
			doSwitch(g.getExpr());
		return null;
	}

	@Override
	public T caseIntInstruction(IntInstruction g) {
		return null;
	}

	@Override
	public T caseConvertInstruction(ConvertInstruction g) {
		doSwitch(g.getExpr());
		return null;
	}

	@Override
	public T caseRangeInstruction(RangeInstruction g) {
		doSwitch(g.getExpr());
		return null;
	}
	
	@Override
	public T caseIndirInstruction(IndirInstruction g) {
		doSwitch(g.getAddress());
		return null;
	}
	
	@Override
	public T caseAddressInstruction(AddressInstruction g) {
		doSwitch(g.getAddress());
		return null;
	}
	
	@Override
	public T caseFieldInstruction(FieldInstruction g) {
		doSwitch(g.getExpr());
		return null;
	}
	
	@Override
	public T caseArrayValueInstruction(ArrayValueInstruction object) {
		//iterate over a copy of the list in order to prevent from ConcurrentModificationException
		Instruction[] elems = (Instruction[]) object.getChildren().toArray();
		for (int i = 0; i < elems.length; ++i) {
			doSwitch(elems[i]);
		}
		return null;
	}

	@Override
	public T caseFloatInstruction(FloatInstruction floatInstruction) {
		return null;
	}


	@Override
	public T caseNumberedSymbolInstruction(NumberedSymbolInstruction s) {
		return null;
	}

	@Override
	public T casePhiInstruction(PhiInstruction object) {
		//iterate over a copy of the list in order to prevent from ConcurrentModificationException
		Instruction[] children = (Instruction[]) object.getChildren().toArray();
		for (Instruction inst : children) {
			doSwitch(inst);
		}
		return null;
	}

	@Override
	public T caseLabelInstruction(LabelInstruction object) {
		return null;
	}

	@Override
	public T caseSymbolInstruction(SymbolInstruction object) {
		return null;
	}

	@Override
	public T caseArrayInstruction(ArrayInstruction object) {
		//iterate over a copy of the list in order to prevent from ConcurrentModificationException
//		Instruction[] children = (Instruction[]) object.getChildren().toArray();
//		for (Instruction inst : children) {
//			doSwitch(inst);
//		}
		doSwitch(object.getDest());
		Instruction[] indexes = (Instruction[]) object.getIndex().toArray();
		for (Instruction inst : indexes) {
			doSwitch(inst);
		}
		return null;
	}
	
}
