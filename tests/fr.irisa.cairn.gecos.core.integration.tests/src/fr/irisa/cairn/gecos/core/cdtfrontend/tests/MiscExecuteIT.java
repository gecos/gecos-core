package fr.irisa.cairn.gecos.core.cdtfrontend.tests;

import org.junit.Test;

import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.core.testframework.impl.GSProjectVersion;
import fr.irisa.cairn.gecos.core.testframework.impl.GecosS2STestTemplate;
import fr.irisa.cairn.gecos.model.cdtfrontend.CDTFrontEnd;
import fr.irisa.cairn.gecos.testframework.data.ICxxProjectData;
import fr.irisa.cairn.gecos.testframework.dataprovider.DataFromPathProvider;
import fr.irisa.cairn.gecos.testframework.dataprovider.ResourcesLocation;
import gecos.gecosproject.GecosProject;

/**
 * Tests C front-end {@link CDTFrontEnd} and the C code generator.
 * 
 * @author aelmouss
 */
public class MiscExecuteIT extends GecosS2STestTemplate<GSProjectVersion> {
	
	/**
	 * The project is the same in both versions, however for the root version
	 * the code generator does not generate code from the {@link GecosProject} 
	 * but rather copy original sources.
	 */
	@Test
	@UseDataProvider(location=DataFromPathProvider.class, value=DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation(value = "resources/src-c/frontend/execute/shouldPass/pass")
	public void testPass(ICxxProjectData d) {
		runTest(d, p -> {}); 
	}
	
	/**
	 * These test should pass but currently they do not: they represent bugs!
	 */
	@Test
	@UseDataProvider(location=DataFromPathProvider.class, value=DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation(value = "resources/src-c/frontend/execute/shouldPass/fail")
	public void testBugs(ICxxProjectData d) {
		runTest(d, p -> {}); 
	}
	
}
