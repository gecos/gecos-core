/**
 */
package gecos.annotations.impl;

import gecos.annotations.AnnotatedElement;
import gecos.annotations.AnnotationKeys;
import gecos.annotations.AnnotationsFactory;
import gecos.annotations.AnnotationsPackage;
import gecos.annotations.CommentAnnotation;
import gecos.annotations.ExtendedAnnotation;
import gecos.annotations.FileLocationAnnotation;
import gecos.annotations.IASTAnnotation;
import gecos.annotations.IAnnotation;
import gecos.annotations.LivenessAnnotation;
import gecos.annotations.LivenessInfo;
import gecos.annotations.PragmaAnnotation;
import gecos.annotations.StringAnnotation;
import gecos.annotations.SymbolDefinitionAnnotation;

import gecos.blocks.BlocksPackage;

import gecos.blocks.impl.BlocksPackageImpl;

import gecos.core.CorePackage;

import gecos.core.impl.CorePackageImpl;

import gecos.dag.DagPackage;

import gecos.dag.impl.DagPackageImpl;

import gecos.instrs.InstrsPackage;

import gecos.instrs.impl.InstrsPackageImpl;

import gecos.types.TypesPackage;

import gecos.types.impl.TypesPackageImpl;

import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.ETypeParameter;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AnnotationsPackageImpl extends EPackageImpl implements AnnotationsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iAnnotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringAnnotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pragmaAnnotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass commentAnnotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass extendedAnnotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass livenessAnnotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass symbolDefinitionAnnotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iastAnnotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fileLocationAnnotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringToIAnnotationMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass annotatedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum annotationKeysEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum livenessInfoEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType stringEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see gecos.annotations.AnnotationsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private AnnotationsPackageImpl() {
		super(eNS_URI, AnnotationsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link AnnotationsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static AnnotationsPackage init() {
		if (isInited) return (AnnotationsPackage)EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI);

		// Obtain or create and register package
		AnnotationsPackageImpl theAnnotationsPackage = (AnnotationsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof AnnotationsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new AnnotationsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
		InstrsPackageImpl theInstrsPackage = (InstrsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI) instanceof InstrsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI) : InstrsPackage.eINSTANCE);
		TypesPackageImpl theTypesPackage = (TypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI) instanceof TypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI) : TypesPackage.eINSTANCE);
		BlocksPackageImpl theBlocksPackage = (BlocksPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI) instanceof BlocksPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI) : BlocksPackage.eINSTANCE);
		DagPackageImpl theDagPackage = (DagPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DagPackage.eNS_URI) instanceof DagPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DagPackage.eNS_URI) : DagPackage.eINSTANCE);

		// Create package meta-data objects
		theAnnotationsPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theInstrsPackage.createPackageContents();
		theTypesPackage.createPackageContents();
		theBlocksPackage.createPackageContents();
		theDagPackage.createPackageContents();

		// Initialize created meta-data
		theAnnotationsPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theInstrsPackage.initializePackageContents();
		theTypesPackage.initializePackageContents();
		theBlocksPackage.initializePackageContents();
		theDagPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theAnnotationsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(AnnotationsPackage.eNS_URI, theAnnotationsPackage);
		return theAnnotationsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIAnnotation() {
		return iAnnotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringAnnotation() {
		return stringAnnotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringAnnotation_Content() {
		return (EAttribute)stringAnnotationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPragmaAnnotation() {
		return pragmaAnnotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPragmaAnnotation_Content() {
		return (EAttribute)pragmaAnnotationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCommentAnnotation() {
		return commentAnnotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExtendedAnnotation() {
		return extendedAnnotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExtendedAnnotation_Fields() {
		return (EReference)extendedAnnotationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExtendedAnnotation_Refs() {
		return (EReference)extendedAnnotationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLivenessAnnotation() {
		return livenessAnnotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLivenessAnnotation_Refs() {
		return (EReference)livenessAnnotationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSymbolDefinitionAnnotation() {
		return symbolDefinitionAnnotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSymbolDefinitionAnnotation_SymbolDefinition() {
		return (EReference)symbolDefinitionAnnotationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIASTAnnotation() {
		return iastAnnotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIASTAnnotation_IastNode() {
		return (EReference)iastAnnotationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFileLocationAnnotation() {
		return fileLocationAnnotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFileLocationAnnotation_Filename() {
		return (EAttribute)fileLocationAnnotationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFileLocationAnnotation_StartingLine() {
		return (EAttribute)fileLocationAnnotationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFileLocationAnnotation_EndingLine() {
		return (EAttribute)fileLocationAnnotationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFileLocationAnnotation_NodeOffset() {
		return (EAttribute)fileLocationAnnotationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFileLocationAnnotation_NodeLength() {
		return (EAttribute)fileLocationAnnotationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringToIAnnotationMap() {
		return stringToIAnnotationMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringToIAnnotationMap_Key() {
		return (EAttribute)stringToIAnnotationMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStringToIAnnotationMap_Value() {
		return (EReference)stringToIAnnotationMapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnnotatedElement() {
		return annotatedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnnotatedElement_Annotations() {
		return (EReference)annotatedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAnnotationKeys() {
		return annotationKeysEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getLivenessInfo() {
		return livenessInfoEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getString() {
		return stringEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationsFactory getAnnotationsFactory() {
		return (AnnotationsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		iAnnotationEClass = createEClass(IANNOTATION);

		stringAnnotationEClass = createEClass(STRING_ANNOTATION);
		createEAttribute(stringAnnotationEClass, STRING_ANNOTATION__CONTENT);

		pragmaAnnotationEClass = createEClass(PRAGMA_ANNOTATION);
		createEAttribute(pragmaAnnotationEClass, PRAGMA_ANNOTATION__CONTENT);

		commentAnnotationEClass = createEClass(COMMENT_ANNOTATION);

		extendedAnnotationEClass = createEClass(EXTENDED_ANNOTATION);
		createEReference(extendedAnnotationEClass, EXTENDED_ANNOTATION__FIELDS);
		createEReference(extendedAnnotationEClass, EXTENDED_ANNOTATION__REFS);

		livenessAnnotationEClass = createEClass(LIVENESS_ANNOTATION);
		createEReference(livenessAnnotationEClass, LIVENESS_ANNOTATION__REFS);

		symbolDefinitionAnnotationEClass = createEClass(SYMBOL_DEFINITION_ANNOTATION);
		createEReference(symbolDefinitionAnnotationEClass, SYMBOL_DEFINITION_ANNOTATION__SYMBOL_DEFINITION);

		iastAnnotationEClass = createEClass(IAST_ANNOTATION);
		createEReference(iastAnnotationEClass, IAST_ANNOTATION__IAST_NODE);

		fileLocationAnnotationEClass = createEClass(FILE_LOCATION_ANNOTATION);
		createEAttribute(fileLocationAnnotationEClass, FILE_LOCATION_ANNOTATION__FILENAME);
		createEAttribute(fileLocationAnnotationEClass, FILE_LOCATION_ANNOTATION__STARTING_LINE);
		createEAttribute(fileLocationAnnotationEClass, FILE_LOCATION_ANNOTATION__ENDING_LINE);
		createEAttribute(fileLocationAnnotationEClass, FILE_LOCATION_ANNOTATION__NODE_OFFSET);
		createEAttribute(fileLocationAnnotationEClass, FILE_LOCATION_ANNOTATION__NODE_LENGTH);

		stringToIAnnotationMapEClass = createEClass(STRING_TO_IANNOTATION_MAP);
		createEAttribute(stringToIAnnotationMapEClass, STRING_TO_IANNOTATION_MAP__KEY);
		createEReference(stringToIAnnotationMapEClass, STRING_TO_IANNOTATION_MAP__VALUE);

		annotatedElementEClass = createEClass(ANNOTATED_ELEMENT);
		createEReference(annotatedElementEClass, ANNOTATED_ELEMENT__ANNOTATIONS);

		// Create enums
		annotationKeysEEnum = createEEnum(ANNOTATION_KEYS);
		livenessInfoEEnum = createEEnum(LIVENESS_INFO);

		// Create data types
		stringEDataType = createEDataType(STRING);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		InstrsPackage theInstrsPackage = (InstrsPackage)EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		iAnnotationEClass.getESuperTypes().add(theCorePackage.getGecosNode());
		stringAnnotationEClass.getESuperTypes().add(this.getIAnnotation());
		pragmaAnnotationEClass.getESuperTypes().add(this.getIAnnotation());
		commentAnnotationEClass.getESuperTypes().add(this.getStringAnnotation());
		extendedAnnotationEClass.getESuperTypes().add(this.getIAnnotation());
		livenessAnnotationEClass.getESuperTypes().add(this.getIAnnotation());
		symbolDefinitionAnnotationEClass.getESuperTypes().add(this.getIAnnotation());
		iastAnnotationEClass.getESuperTypes().add(this.getIAnnotation());
		fileLocationAnnotationEClass.getESuperTypes().add(this.getIAnnotation());
		annotatedElementEClass.getESuperTypes().add(theCorePackage.getGecosNode());

		// Initialize classes and features; add operations and parameters
		initEClass(iAnnotationEClass, IAnnotation.class, "IAnnotation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(iAnnotationEClass, this.getAnnotatedElement(), "getAnnotatedElement", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(stringAnnotationEClass, StringAnnotation.class, "StringAnnotation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringAnnotation_Content(), this.getString(), "content", null, 0, 1, StringAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(stringAnnotationEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(pragmaAnnotationEClass, PragmaAnnotation.class, "PragmaAnnotation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPragmaAnnotation_Content(), this.getString(), "content", null, 0, -1, PragmaAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(pragmaAnnotationEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		EOperation op = addEOperation(pragmaAnnotationEClass, null, "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		ETypeParameter t1 = addETypeParameter(op, "T");
		EGenericType g1 = createEGenericType(theCorePackage.getSymbol());
		t1.getEBounds().add(g1);
		g1 = createEGenericType(t1);
		initEOperation(op, g1);

		initEClass(commentAnnotationEClass, CommentAnnotation.class, "CommentAnnotation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(extendedAnnotationEClass, ExtendedAnnotation.class, "ExtendedAnnotation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getExtendedAnnotation_Fields(), theEcorePackage.getEObject(), null, "fields", null, 0, -1, ExtendedAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getExtendedAnnotation_Refs(), theEcorePackage.getEObject(), null, "refs", null, 0, -1, ExtendedAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(livenessAnnotationEClass, LivenessAnnotation.class, "LivenessAnnotation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLivenessAnnotation_Refs(), theCorePackage.getSymbol(), null, "refs", null, 0, -1, LivenessAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(symbolDefinitionAnnotationEClass, SymbolDefinitionAnnotation.class, "SymbolDefinitionAnnotation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSymbolDefinitionAnnotation_SymbolDefinition(), theInstrsPackage.getInstruction(), null, "symbolDefinition", null, 0, 1, SymbolDefinitionAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iastAnnotationEClass, IASTAnnotation.class, "IASTAnnotation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIASTAnnotation_IastNode(), theEcorePackage.getEObject(), null, "iastNode", null, 0, 1, IASTAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fileLocationAnnotationEClass, FileLocationAnnotation.class, "FileLocationAnnotation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFileLocationAnnotation_Filename(), this.getString(), "filename", null, 0, 1, FileLocationAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFileLocationAnnotation_StartingLine(), theEcorePackage.getEInt(), "startingLine", null, 0, 1, FileLocationAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFileLocationAnnotation_EndingLine(), theEcorePackage.getEInt(), "endingLine", null, 0, 1, FileLocationAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFileLocationAnnotation_NodeOffset(), theEcorePackage.getEInt(), "nodeOffset", null, 0, 1, FileLocationAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFileLocationAnnotation_NodeLength(), theEcorePackage.getEInt(), "nodeLength", null, 0, 1, FileLocationAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stringToIAnnotationMapEClass, Map.Entry.class, "StringToIAnnotationMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringToIAnnotationMap_Key(), this.getString(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStringToIAnnotationMap_Value(), this.getIAnnotation(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(annotatedElementEClass, AnnotatedElement.class, "AnnotatedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAnnotatedElement_Annotations(), this.getStringToIAnnotationMap(), null, "annotations", null, 0, -1, AnnotatedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(annotatedElementEClass, this.getIAnnotation(), "getAnnotation", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getString(), "key", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(annotatedElementEClass, null, "setAnnotation", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getString(), "key", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIAnnotation(), "annot", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(annotatedElementEClass, this.getPragmaAnnotation(), "getPragma", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(annotatedElementEClass, this.getFileLocationAnnotation(), "getFileLocation", 0, 1, !IS_UNIQUE, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(annotationKeysEEnum, AnnotationKeys.class, "AnnotationKeys");
		addEEnumLiteral(annotationKeysEEnum, AnnotationKeys.PRAGMA_ANNOTATION_KEY);
		addEEnumLiteral(annotationKeysEEnum, AnnotationKeys.COMMENT_ANNOTATION_KEY);
		addEEnumLiteral(annotationKeysEEnum, AnnotationKeys.SYMBOL_DEF_ANNOTATION_KEY);
		addEEnumLiteral(annotationKeysEEnum, AnnotationKeys.FILE_LOCATION_ANNOTATION_KEY);

		initEEnum(livenessInfoEEnum, LivenessInfo.class, "LivenessInfo");
		addEEnumLiteral(livenessInfoEEnum, LivenessInfo.DEF);
		addEEnumLiteral(livenessInfoEEnum, LivenessInfo.USED);
		addEEnumLiteral(livenessInfoEEnum, LivenessInfo.ALIVE);

		// Initialize data types
		initEDataType(stringEDataType, String.class, "String", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
		   });
	}

} //AnnotationsPackageImpl
