package fr.irisa.cairn.gecos.model.transforms.commonsubexpressionelimination.internal;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.gecos.model.analysis.instructions.comparator.InstructionComparator;
import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDefaultVisitor;
import fr.irisa.r2d2.gecos.framework.GSModule;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.GenericInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.IntegerType;
import gecos.types.Type;

/**
 * @author Imen Fassi
 * @date 20/04/2016
 */

@GSModule("This Module eliminates the Local Common-Subexpression in the code. This\n"
		+ "transformation works within a single basic block. It assumes that the instructions\n"
		+ " are in the MIR (Medium Level Intermediate Representation) format i.e. quadruples.\n"
		+ " The module implements the algorithm described in this book :\n"
		+ "\t Advanced Compiler Design & Implementation, Steven S. Muchnick")

public class LocalCommonSubExpressionElimination extends GecosBlocksInstructionsDefaultVisitor {

	private int i = 0;
	private boolean consider_func_call;
	private boolean pointer_conservative;  
	private ArrayList<quintuples_instr> AEB = null;
	private ArrayList<Instruction> non_valid_symbol_list = null;
	private ArrayList<Instruction> local_symbol_list = null;
	private int counter = 0;

	private int incr_counter() {
		counter++;
		return counter;
	}

	public LocalCommonSubExpressionElimination(boolean bf, boolean bp, ArrayList<Instruction> list1) {
		super();
		this.consider_func_call = bf;
		this.pointer_conservative = bp;
		this.non_valid_symbol_list = list1;
		this.local_symbol_list = new ArrayList<Instruction>();
	}

	@Override
	public void visitBasicBlock(BasicBlock b) {
		i = 0;
		/**
		 * Compute the set S = {Global variables} - {Local variables}
		 */
		ArrayList<Instruction> temp = new ArrayList<Instruction>(non_valid_symbol_list);
		for (Instruction d : temp) {
			for (Instruction s : local_symbol_list) {
				if (InstructionComparator.instToString(s).equals(InstructionComparator.instToString(d)))
					non_valid_symbol_list.remove(d);
			}
		}
		AEB = new ArrayList<quintuples_instr>();
		for (Instruction inst : new ArrayList<Instruction>(b.getInstructions())) {
			inst.accept(this);
			i++;
		}
	}

	@Override
	public void visitCompositeBlock(CompositeBlock c) {
		/**
		 * get the list of local variables
		 */
		Scope scope = c.getScope();
		if (scope != null) {
			for (Symbol s : scope.getSymbols()) {
				if (!(s instanceof ProcedureSymbol)) {
					Instruction symbInstr = GecosUserInstructionFactory.symbref(s);
					local_symbol_list.add(symbInstr);
				}
			}
		}

		super.visitCompositeBlock(c);
	}

	@Override
	public void visitSetInstruction(SetInstruction s) {
		super.visitSetInstruction(s);
		Instruction instrc = s.getDest();
		/**
		 * useful for cases like (*p15)[0] = ... or *(p16[0]) = ... we want to
		 * get the name of the pointer
		 */
		while (instrc instanceof IndirInstruction)
			instrc = ((IndirInstruction) instrc).getAddress();

		while (instrc instanceof ArrayInstruction)
			instrc = ((ArrayInstruction) instrc).getDest();

		while (instrc instanceof IndirInstruction)
			instrc = ((IndirInstruction) instrc).getAddress();

		/**
		 * useful for cases like (1+p14)[0] = ...
		 */

		ArrayList<SymbolInstruction> listSymbolsGI = null;

		if (instrc instanceof GenericInstruction) {

			SymbolsCounter countSymbols = new SymbolsCounter();
			instrc.accept(countSymbols);
			listSymbolsGI = countSymbols.getlistSymbsInst();

		} else {
			if (instrc instanceof SymbolInstruction) {
				listSymbolsGI = new ArrayList<SymbolInstruction>();
				listSymbolsGI.add(((SymbolInstruction) instrc));
			}
		}

		/**
		 * Remove tuples with operands that are referenced with the previous
		 * identified pointers or symbols , suppose that the pointer name is p
		 */
		for (SymbolInstruction symbl : listSymbolsGI) {
			
			TypeAnalyzer type_var = new TypeAnalyzer(symbl.getSymbol().getType());
			
			if (pointer_conservative) {
				if (type_var.isPointer()) {
					AEB.clear();
					break;
				} else {
					if (type_var.isBase()) {
						ArrayList<quintuples_instr> Tmp = new ArrayList<quintuples_instr>(AEB);
						for (quintuples_instr aebt : Tmp) {
							if (aebt.getOpd1().contains("*") || aebt.getOpd2().contains("*"))
								AEB.remove(aebt);
						}
					}
				}
			}
			
			/**
			 * Remove all tuples that use the variable assigned to by the
			 * current instruction
			 */
			ArrayList<quintuples_instr> Tmp = new ArrayList<quintuples_instr>(AEB);
			String current_assigned_var = InstructionComparator.instToString(symbl);

			for (quintuples_instr aebt : Tmp) {
				if (aebt.getOpd1().equals(current_assigned_var) || aebt.getOpd2().equals(current_assigned_var))
					AEB.remove(aebt);
			}

			
			if (type_var.isPointer()) {
				/**
				 * if the symbol is a pointer remove all tuples using
				 * *(symbol)
				 */
				String instr_str = "*(" + InstructionComparator.instToString(symbl) + ")";
				Tmp = new ArrayList<quintuples_instr>(AEB);
				for (quintuples_instr aebt : Tmp) {
					// aebt.print_quintuples();
					if (aebt.getOpd1().equals(instr_str) || aebt.getOpd2().equals(instr_str))
						AEB.remove(aebt);
				}
			} else {
				if (type_var.isArray()) {
					/**
					 * if the symbol is an Array remove all tuples using
					 * symbol[...]
					 */
					
					
					if (pointer_conservative) {						
							Tmp = new ArrayList<quintuples_instr>(AEB);
							for (quintuples_instr aebt : Tmp) {
								// aebt.print_quintuples();
								if ( (aebt.getOpd1().contains("[") && aebt.getOpd1().endsWith("]")) 
										|| (aebt.getOpd2().contains("[") && aebt.getOpd2().endsWith("]") ) )
									AEB.remove(aebt);
							}
					}
					
					
					String instr_str = InstructionComparator.instToString(symbl) + "[";
					Tmp = new ArrayList<quintuples_instr>(AEB);
					for (quintuples_instr aebt : Tmp) {
						// aebt.print_quintuples();
						if ( (aebt.getOpd1().startsWith(instr_str) && aebt.getOpd1().endsWith("]")) 
								|| (aebt.getOpd2().startsWith(instr_str) && aebt.getOpd2().endsWith("]") ) )
							AEB.remove(aebt);
					}
				}
			}

			/**
			 * If the symbol is initialized when declared p = alpha
			 */
			if (symbl.getSymbol().getValue() != null) {
				Instruction instref = symbl.getSymbol().getValue();

				if (instref instanceof SymbolInstruction) {
					type_var = new TypeAnalyzer(((SymbolInstruction) instref).getSymbol().getType());
					if (type_var.isPointer()) {
						/**
						 * Remove tuples containing *(alpha)
						 */
						String instr_str = "*(" + InstructionComparator.instToString(instref) + ")";
						Tmp = new ArrayList<quintuples_instr>(AEB);
						for (quintuples_instr aebt : Tmp) {
							// aebt.print_quintuples();
							if (aebt.getOpd1().equals(instr_str) || aebt.getOpd2().equals(instr_str))
								AEB.remove(aebt);
						}
					} else {
						if (type_var.isArray()) {
							/**
							 * Remove tuples containing alpha[...]
							 */
							String instr_str = InstructionComparator.instToString(instref) + "[";
							Tmp = new ArrayList<quintuples_instr>(AEB);
							for (quintuples_instr aebt : Tmp) {
								// aebt.print_quintuples();
								if ( (aebt.getOpd1().startsWith(instr_str) && aebt.getOpd1().endsWith("]")) 
										|| (aebt.getOpd2().startsWith(instr_str) && aebt.getOpd2().endsWith("]")))
									AEB.remove(aebt);
							}
						}
					}
				} else {
					if (instref instanceof AddressInstruction) {
						/**
						 * if it is some thing like '&a' remove all tuples using
						 * 'a'
						 */
						String instr_str = InstructionComparator
								.instToString(((AddressInstruction) instref).getAddress());
						Tmp = new ArrayList<quintuples_instr>(AEB);
						for (quintuples_instr aebt : Tmp) {
							if (aebt.getOpd1().equals(instr_str) || aebt.getOpd2().equals(instr_str))
								AEB.remove(aebt);
						}

						Instruction instref2 = ((AddressInstruction) instref).getAddress();
						/**
						 * 'a' may be a pointer , so remove tuples containing
						 * *(a) and a[..]
						 */
						if (instref2 instanceof SymbolInstruction) {
							type_var = new TypeAnalyzer(
									((SymbolInstruction) instref2).getSymbol().getType());
							if (type_var.isPointer()) {
								instr_str = "*(" + InstructionComparator.instToString(instref2) + ")";
								Tmp = new ArrayList<quintuples_instr>(AEB);
								for (quintuples_instr aebt : Tmp) {
									// aebt.print_quintuples();
									if (aebt.getOpd1().equals(instr_str) || aebt.getOpd2().equals(instr_str))
										AEB.remove(aebt);
								}
							} else {
								if (type_var.isArray()) {
									instr_str = InstructionComparator.instToString(instref2) + "[";
									Tmp = new ArrayList<quintuples_instr>(AEB);
									for (quintuples_instr aebt : Tmp) {
										// aebt.print_quintuples();
										if ( (aebt.getOpd1().startsWith(instr_str) && aebt.getOpd1().endsWith("]"))
												|| (aebt.getOpd2().startsWith(instr_str) && aebt.getOpd2().endsWith("]")))
											AEB.remove(aebt);
									}
								}
							}
						}
					}
				}
			} // end if symbl.getValue()
		} // end for
	}
	

	@Override
	public void visitCallInstruction(CallInstruction c) {
		super.visitCallInstruction(c);

		List<Instruction> listArgs = new ArrayList<Instruction>(c.getArgs());
		
		Loop:
		for (Instruction instrr : listArgs) {
			/**
			 * useful for cases like (*p15)[0] or *(p16[0]) -> need to get the
			 * name of the pointer
			 */
			while (instrr instanceof IndirInstruction)
				instrr = ((IndirInstruction) instrr).getAddress();

			while (instrr instanceof ArrayInstruction)
				instrr = ((ArrayInstruction) instrr).getDest();

			while (instrr instanceof IndirInstruction)
				instrr = ((IndirInstruction) instrr).getAddress();

			/**
			 * useful for cases like (1+p14)[0]
			 */

			ArrayList<SymbolInstruction> listSymbolsGI = null;

			if (instrr instanceof GenericInstruction) {

				SymbolsCounter countSymbols = new SymbolsCounter();
				instrr.accept(countSymbols);
				listSymbolsGI = countSymbols.getlistSymbsInst();

			} else {
				listSymbolsGI = new ArrayList<SymbolInstruction>();
				if (instrr instanceof SymbolInstruction) {
					listSymbolsGI.add((SymbolInstruction) instrr);
				} else {
					if (instrr instanceof AddressInstruction) {
						// System.out.println("instr :" +
						// InstructionComparator.instToString (instr) + "----" +
						// InstructionComparator.instToString
						// (((AddressInstruction)
						// instr).getAddress()));
						
						
						if (pointer_conservative) {
								AEB.clear();
								break Loop;
						}

						String instr_str = InstructionComparator
								.instToString(((AddressInstruction) instrr).getAddress());

						ArrayList<quintuples_instr> Tmp = new ArrayList<quintuples_instr>(AEB);
						for (quintuples_instr aebt : Tmp) {
							if (aebt.getOpd1().equals(instr_str) || aebt.getOpd2().equals(instr_str))
								AEB.remove(aebt);
						}
					}
				}
			}

			/**
			 * Remove tuples with operands that are referenced with the previous
			 * identified pointers or symbols
			 */

			for (SymbolInstruction instruction : listSymbolsGI) {
				Symbol symbl = instruction.getSymbol();
				TypeAnalyzer type_var = new TypeAnalyzer(symbl.getType());
								
				if (pointer_conservative) {
					if(type_var.isPointer()) {
						AEB.clear();
						break;
					}
				}
								
				if (type_var.isPointer()) {
					/**
					 * if the symbol is a pointer remove all tuples using
					 * *(symbol)
					 */
					// System.out.println(" (visitCallInstruction) Voila un
					// pointeur : " +
					// InstructionComparator.instToString(instr));

					String instr_str = "*(" + InstructionComparator.instToString(instruction) + ")";
					ArrayList<quintuples_instr> Tmp = new ArrayList<quintuples_instr>(AEB);
					for (quintuples_instr aebt : Tmp) {
						// aebt.print_quintuples();
						if (aebt.getOpd1().equals(instr_str) || aebt.getOpd2().equals(instr_str))
							AEB.remove(aebt);
					}
				} else {
					if (type_var.isArray()) {
						
						if (pointer_conservative) {						
							ArrayList<quintuples_instr> Tmp = new ArrayList<quintuples_instr>(AEB);
							for (quintuples_instr aebt : Tmp) {
								// aebt.print_quintuples();
								if ( (aebt.getOpd1().contains("[") && aebt.getOpd1().endsWith("]")) 
										|| (aebt.getOpd2().contains("[") && aebt.getOpd2().endsWith("]") ) )
									AEB.remove(aebt);
							}
					}
						
						/**
						 * if the symbol is an array remove all tuples using
						 * symbol[...]
						 */
						String instr_str = InstructionComparator.instToString(instruction) + "[";
						ArrayList<quintuples_instr> Tmp = new ArrayList<quintuples_instr>(AEB);
						for (quintuples_instr aebt : Tmp) {
							// aebt.print_quintuples();
							if ( (aebt.getOpd1().startsWith(instr_str) && aebt.getOpd1().endsWith("]")) 
									|| (aebt.getOpd2().startsWith(instr_str) && aebt.getOpd2().endsWith("]")))
								AEB.remove(aebt);
						}
					}
				}

				if (symbl.getValue() != null) {
					/**
					 * If the symbol is initialized
					 */
					Instruction instref = symbl.getValue();

					if (instref instanceof SymbolInstruction) {
						type_var = new TypeAnalyzer(((SymbolInstruction) instref).getSymbol().getType());
						if (type_var.isPointer()) {

							String instr_str = "*(" + InstructionComparator.instToString(instref) + ")";
							ArrayList<quintuples_instr> Tmp = new ArrayList<quintuples_instr>(AEB);
							for (quintuples_instr aebt : Tmp) {
								// aebt.print_quintuples();
								if (aebt.getOpd1().equals(instr_str) || aebt.getOpd2().equals(instr_str))
									AEB.remove(aebt);
							}
						} else {
							if (type_var.isArray()) {
								String instr_str = InstructionComparator.instToString(instref) + "[";
								ArrayList<quintuples_instr> Tmp = new ArrayList<quintuples_instr>(AEB);
								for (quintuples_instr aebt : Tmp) {
									// aebt.print_quintuples();
									if ((aebt.getOpd1().startsWith(instr_str) && aebt.getOpd1().endsWith("]")) 
											|| (aebt.getOpd2().startsWith(instr_str) && aebt.getOpd2().endsWith("]")))
										AEB.remove(aebt);
								}
							}
						}
					} else {
						if (instref instanceof AddressInstruction) {

							String instr_str = InstructionComparator
									.instToString(((AddressInstruction) instref).getAddress());
							ArrayList<quintuples_instr> Tmp = new ArrayList<quintuples_instr>(AEB);
							for (quintuples_instr aebt : Tmp) {
								if (aebt.getOpd1().equals(instr_str) || aebt.getOpd2().equals(instr_str))
									AEB.remove(aebt);
							}

							Instruction instref2 = ((AddressInstruction) instref).getAddress();

							if (instref2 instanceof SymbolInstruction) {
								type_var = new TypeAnalyzer(((SymbolInstruction) instref2).getSymbol().getType());
								String instr_str1 = "*(" + InstructionComparator.instToString(instref2) + ")";
								Tmp = new ArrayList<quintuples_instr>(AEB);
								for (quintuples_instr aebt : Tmp) {
									// aebt.print_quintuples();
									if (aebt.getOpd1().equals(instr_str1) || aebt.getOpd2().equals(instr_str1))
										AEB.remove(aebt);
								}

								String instr_str2 = InstructionComparator.instToString(instref2) + "[";
								Tmp = new ArrayList<quintuples_instr>(AEB);
								for (quintuples_instr aebt : Tmp) {
									// aebt.print_quintuples();
									if ( (aebt.getOpd1().startsWith(instr_str2) && aebt.getOpd1().endsWith("]")) 
											|| (aebt.getOpd2().startsWith(instr_str2) && aebt.getOpd2().endsWith("]")))
										AEB.remove(aebt);
								}
							}
						}
					}
				} // end if
			} // end for
		}
	
		/**
		 * Useful for this case : a = value + global_variable ; f(...) ; b =
		 * value + global_variable ;
		 * 
		 * Remove all tuples that use global variables (As we don't know if the
		 * function modifies the global variables or not)
		 */

		for (Instruction current_assigned_Instr : new ArrayList<Instruction>(non_valid_symbol_list)) {
			ArrayList<quintuples_instr> Tmp = new ArrayList<quintuples_instr>(AEB);
			String current_assigned_var = InstructionComparator.instToString(current_assigned_Instr);
			for (quintuples_instr aebt : Tmp) {
				if (aebt.getOpd1().equals(current_assigned_var) || aebt.getOpd2().equals(current_assigned_var))
					AEB.remove(aebt);
			}

			if (current_assigned_Instr instanceof SymbolInstruction) {
				TypeAnalyzer type_var = new TypeAnalyzer(
						((SymbolInstruction) current_assigned_Instr).getSymbol().getType());
				if (type_var.isPointer()) {
					String instr_str = "*(" + InstructionComparator.instToString(current_assigned_Instr) + ")";
					Tmp = new ArrayList<quintuples_instr>(AEB);
					for (quintuples_instr aebt : Tmp) {
						// aebt.print_quintuples();
						if (aebt.getOpd1().equals(instr_str) || aebt.getOpd2().equals(instr_str))
							AEB.remove(aebt);
					}
				} else {
					if (type_var.isArray()) {
						String instr_str = InstructionComparator.instToString(current_assigned_Instr) + "[";
						Tmp = new ArrayList<quintuples_instr>(AEB);
						for (quintuples_instr aebt : Tmp) {
							// aebt.print_quintuples();
							if ( (aebt.getOpd1().startsWith(instr_str) && aebt.getOpd1().endsWith("]"))
									|| (aebt.getOpd2().startsWith(instr_str) && aebt.getOpd2().endsWith("]")))
								AEB.remove(aebt);
						}
					}
				}
			}
		}
	}

	@Override
	public void visitGenericInstruction(GenericInstruction gi) {
		
		/**
		 * Arithmetic, Comparison and logical operator are generic instruction.
		 */
		Instruction current_assigned_Instr = null;
		/**
		 * Look up first the content of the g node
		 */
		super.visitGenericInstruction(gi);
		if (gi.getChildrenCount() == 2) {

			boolean found = false;
			int pos = 0;

			/**
			 * get the Current assigned variable
			 */
			if (gi.getParent() != null) {
				if (gi.getParent() instanceof SetInstruction) {
					SetInstruction parent = (SetInstruction) gi.getParent();
					current_assigned_Instr = parent.getDest().copy();
				}
			} 			
			// save the reference to g
			GenericInstruction ggi = (GenericInstruction) gi.copy();

			ArrayList<quintuples_instr> Tmp = new ArrayList<quintuples_instr>(AEB);

			for (quintuples_instr aeb : Tmp) {
				// aeb.print_quintuples();
				if (gi.getName().equals(aeb.getOpr()) && ((commutative(aeb.getOpr())
						&& InstructionComparator.instToString(gi.getOperand(0)).equals(aeb.getOpd2())
						&& InstructionComparator.instToString(gi.getOperand(1)).equals(aeb.getOpd1()))
						|| (InstructionComparator.instToString(gi.getOperand(0)).equals(aeb.getOpd1())
								&& InstructionComparator.instToString(gi.getOperand(1)).equals(aeb.getOpd2())))) {
					pos = aeb.getPos();
					found = true;
					if (aeb.getTmp() == null) {
						/**
						 * insert instruction "ti <- opd1 op opd2" before
						 * instruction at position pos Create a symbol
						 */

						Symbol symb = null;
						// get the containing basic block
						BasicBlock bb = gi.getBasicBlock();
						Scope scope = bb.getScope();						
						Block parent  = bb.getParent();
						if (parent != null)
						if (!(parent instanceof CompositeBlock)) {
							BasicBlock aux = GecosUserBlockFactory.BBlock();
							parent.replace(bb, aux);
							CompositeBlock cb = GecosUserBlockFactory.CompositeBlock(bb);
							parent.replace( aux, cb);
							scope = cb.getScope();
						}
						
						String name = new_temp(scope);
						Type _type = gi.getType();

						if (bb.getInstructions().get(pos) instanceof SetInstruction) {
							SetInstruction instPos = (SetInstruction) bb.getInstructions().get(pos);
							_type = instPos.getDest().getType();
						}

						if (bb.getInstructions().get(pos) instanceof CallInstruction) {
							CallInstruction instPos = (CallInstruction) bb.getInstructions().get(pos);
							List<Instruction> listArgs = new ArrayList<Instruction>(instPos.getArgs());
							for (Instruction instr : listArgs) {
								String instr_str = InstructionComparator.instToString(instr);
								if (instr_str.equals(InstructionComparator.instToString(gi))) {
									_type = instr.getType();
								}
							}
						}

						TypeAnalyzer type_var = new TypeAnalyzer(_type);
						if (type_var.getBaseLevel().isInt()) {
							IntegerType base = type_var.getBaseLevel().getBaseAsInt();
							if (!base.getSigned() && base.getSize() == 1)
								_type = GecosUserTypeFactory.INT();
						}

						symb = GecosUserCoreFactory.symbol(name, _type, scope);

						aeb.setTmp(symb);

						/**
						 * Create the SymbolInstruction
						 */
						Instruction symbInstrTMP = GecosUserInstructionFactory.symbref(symb);

						/**
						 * Create a new SetInstruction
						 */
						Instruction setInstTEMP = GecosUserInstructionFactory.set(symbInstrTMP, ggi);

						bb.getInstructions().add(pos, setInstTEMP);

						renumber(AEB, pos);
						pos++;
						i++;

						Instruction symbInstrTMP2 = GecosUserInstructionFactory.symbref(symb);
						GrepReplaceInst gri = new GrepReplaceInst(symbInstrTMP2, gi);
						bb.getInstructions().get(pos).accept(gri);
					}
					/**
					 * Replace the current instruction by the one that copies
					 * the temporary
					 */
					if (gi.getParent() != null) {
						/**
						 * Create the SymbolInstruction
						 */
						Instruction symbInstrTMP2 = GecosUserInstructionFactory.symbref(aeb.getTmp());
						/**
						 * replace gi by the SymbolInstruction
						 */
						gi.getParent().replaceChild(gi, symbInstrTMP2);
					} 
				}
			}

			if (!found) {
				/**
				 * Consider tuple containing a function call or not ?
				 * 
				 * In this case for example : a = f(value1) + value2; // f uses
				 * the global variable global_variable global_variable =
				 * global_variable +1; b = f(value1) + value2;
				 * 
				 */
				if (consider_tuple_func_call(ggi.getOperand(0), ggi.getOperand(1))) {
					/**
					 * Insert a new tuple !
					 */
					quintuples_instr newTuple = new quintuples_instr(i,
							InstructionComparator.instToString(ggi.getOperand(0)), ggi.getName(),
							InstructionComparator.instToString(ggi.getOperand(1)), null);

					AEB.add(newTuple);
				}
			}
			if (current_assigned_Instr != null) {
				/**
				 * Remove all tuples that use the variable assigned to by the
				 * current instruction
				 */
				Tmp = new ArrayList<quintuples_instr>(AEB);
				String current_assigned_var = InstructionComparator.instToString(current_assigned_Instr);

				for (quintuples_instr aebt : Tmp) {
					if (aebt.getOpd1().equals(current_assigned_var) || aebt.getOpd2().equals(current_assigned_var))
						AEB.remove(aebt);
				}

				if (current_assigned_Instr instanceof SymbolInstruction) {
					TypeAnalyzer type_var = new TypeAnalyzer(
							((SymbolInstruction) current_assigned_Instr).getSymbol().getType());
					if (type_var.isPointer()) {
						String instr_str = "*(" + InstructionComparator.instToString(current_assigned_Instr) + ")";
						Tmp = new ArrayList<quintuples_instr>(AEB);
						for (quintuples_instr aebt : Tmp) {
							// aebt.print_quintuples();
							if (aebt.getOpd1().equals(instr_str) || aebt.getOpd2().equals(instr_str))
								AEB.remove(aebt);
						}
					} else {
						if (type_var.isArray()) {
							String instr_str = InstructionComparator.instToString(current_assigned_Instr) + "[";
							Tmp = new ArrayList<quintuples_instr>(AEB);
							for (quintuples_instr aebt : Tmp) {
								// aebt.print_quintuples();
								if ((aebt.getOpd1().startsWith(instr_str) && aebt.getOpd1().endsWith("]"))
										|| (aebt.getOpd2().startsWith(instr_str) && aebt.getOpd2().endsWith("]")))
									AEB.remove(aebt);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Consider the expression operand op operand2 as a potential local common
	 * expression or not
	 * 
	 * @param operand
	 *            (Instruction)
	 * @param operand2
	 *            (Instruction)
	 * @return boolean
	 */
	private boolean consider_tuple_func_call(Instruction operand, Instruction operand2) {
		if (consider_func_call)
			return true;
		else {
			if ((operand instanceof CallInstruction) || (operand2 instanceof CallInstruction))
				return false;
			else
				return true;
		}
	}

	/**
	 * returns a new temporary name and verifies if The symbol is already used
	 * or not
	 * 
	 * @param scope
	 * @return String
	 */
	private String new_temp(Scope scope) {
		String name;
		boolean ok;
		if (scope != null) {
			do {
				name = "_tmp" + incr_counter();
				ok = true;
				for (Symbol s : scope.getSymbols()) {
					if (s.getName().equals(name)) {
						ok = false;
						break;
					}
				}
			} while (!ok);
		} else
			name = "_tmp" + incr_counter();
		return name;
	}

	/**
	 * Renumber the first entry in each of the quintuplets in AEB, as necessary,
	 * to reflect the effect of inserted instruction
	 * 
	 * @param AEB
	 * @param pos
	 */
	private void renumber(ArrayList<quintuples_instr> AEB, int pos) {
		for (quintuples_instr aeb : AEB) {
			if (aeb.getPos() >= pos)
				aeb.incrementPos();
		}
	}

	/**
	 * returns true if opr (operator) is commutative, and false otherwise
	 * 
	 * @param opr
	 * @return boolean
	 */
	private boolean commutative(String opr) {
		if (opr.equals(ArithmeticOperator.MUL.getLiteral()))
			return true;
		if (opr.equals(ArithmeticOperator.ADD.getLiteral()))
			return true;
		if (opr.equals(ComparisonOperator.EQ.getLiteral()))
			return true;
		if (opr.equals(ComparisonOperator.NE.getLiteral()))
			return true;
		if (opr.equals(LogicalOperator.AND.getLiteral()))
			return true;
		if (opr.equals(LogicalOperator.OR.getLiteral()))
			return true;

		return false;
	}

	private class GrepReplaceInst extends GecosBlocksInstructionsDefaultVisitor {
		private Instruction newg;
		private GenericInstruction oldg;
		public GrepReplaceInst(Instruction ng, GenericInstruction og) {
			super();
			this.newg = ng;
			this.oldg = og;
		}

		@Override
		public void visitGenericInstruction(GenericInstruction g) {
			super.visitGenericInstruction(g);
			if (g!=oldg) {
				String ch1 = InstructionComparator.instToString(g).trim();
				String ch2 = InstructionComparator.instToString(oldg).trim();
				if (ch1.equals(ch2)) {
					g.getParent().replaceChild(g, newg.copy());
				}
			}
		}
	}
}