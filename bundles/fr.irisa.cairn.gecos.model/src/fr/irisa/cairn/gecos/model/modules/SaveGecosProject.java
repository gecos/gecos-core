package fr.irisa.cairn.gecos.model.modules;

import fr.irisa.cairn.gecos.model.tools.utils.FixTypeContainers;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosprojectPackage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

@GSModule("Export (serialize) the XMI representation of a GecosProject.\n"
		+ "\nSee: 'LoadGecosProject' module.")
public class SaveGecosProject {

	GecosProject project;
	String filename;
	
	@GSModuleConstructor(
		"-arg1: GecosProject to export.\n"
	  + "-arg2: file path of the exported '.gecosproject' file.")
	public SaveGecosProject(GecosProject project, String filename) {
		this.project=project;
		this.filename=filename;
	}

	public void compute() throws FileNotFoundException, IOException {
		ResourceSet resourceSet = new ResourceSetImpl();

		// Register the appropriate resource factory to handle all file extensions.
		//
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put
		(Resource.Factory.Registry.DEFAULT_EXTENSION, 
				new XMIResourceFactoryImpl());

		// Register the package to ensure it is available during loading.
		//
		resourceSet.getPackageRegistry().put
		(GecosprojectPackage.eNS_URI, 
				GecosprojectPackage.eINSTANCE);

		if (!filename.endsWith(".gecosproject")) filename = filename+".gecosproject";
		File file = new File(filename).getAbsoluteFile();
		
		if (file.exists()) {
			if (file.isFile())
				file.delete();
			else 
				throw new RuntimeException("Destination file is a directory...");
		} else {
			file.mkdirs();
			file.delete();
		}
		
		URI uri = URI.createFileURI(file.getAbsolutePath());

		new FixTypeContainers(project).compute();
				
		Resource resource = resourceSet.createResource(uri);
		resource.getContents().add(project);
		FileOutputStream outputStream = new FileOutputStream(file);
		try {
			resource.save(outputStream,null);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		outputStream.close();
	}

}
