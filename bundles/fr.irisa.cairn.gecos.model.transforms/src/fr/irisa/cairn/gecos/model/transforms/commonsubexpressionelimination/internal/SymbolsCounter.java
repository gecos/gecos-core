package fr.irisa.cairn.gecos.model.transforms.commonsubexpressionelimination.internal;

import java.util.ArrayList;

import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDefaultVisitor;
import gecos.core.Symbol;
import gecos.instrs.SymbolInstruction;

public class SymbolsCounter extends GecosBlocksInstructionsDefaultVisitor {
	private ArrayList<Symbol> listSymbs = null;
	private ArrayList<SymbolInstruction> listSymbsInst = null;

	public ArrayList<Symbol> getSymbolList() {
		return this.listSymbs;
	}

	public ArrayList<SymbolInstruction> getlistSymbsInst() {
		return this.listSymbsInst;
	}

	public SymbolsCounter() {
		super();
		this.listSymbs = new ArrayList<Symbol>();
		this.listSymbsInst = new ArrayList<SymbolInstruction>();
	}

	@Override
	public void visitSymbolInstruction(SymbolInstruction s) {
		super.visitSymbolInstruction(s);
		Symbol symb = s.getSymbol();
		if (!listSymbs.contains(symb)) {
			listSymbs.add(symb);
			listSymbsInst.add(s);
		}
	}
}
