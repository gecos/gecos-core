void foo(int *f); //f: should be in the foo ParameterSymbol scope


void f(int a, int b) {// f: should be in global scope => no conflict should occur
}

int main() {
	exit(0);
}
