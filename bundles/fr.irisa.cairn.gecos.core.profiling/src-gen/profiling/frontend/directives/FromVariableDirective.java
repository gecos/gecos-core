/**
 */
package profiling.frontend.directives;

import gecos.blocks.Block;

import gecos.instrs.Instruction;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>From Variable Directive</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * TODO
 * $from_var(??)
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link profiling.frontend.directives.FromVariableDirective#getNAME <em>NAME</em>}</li>
 *   <li>{@link profiling.frontend.directives.FromVariableDirective#getInst <em>Inst</em>}</li>
 * </ul>
 *
 * @see profiling.frontend.directives.DirectivesPackage#getFromVariableDirective()
 * @model
 * @generated
 */
public interface FromVariableDirective extends InjectorSourceDirective {
	/**
	 * Returns the value of the '<em><b>NAME</b></em>' attribute.
	 * The default value is <code>"$from_var"</code>.
	 * The literals are from the enumeration {@link profiling.frontend.directives.DirectiveName}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>NAME</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>NAME</em>' attribute.
	 * @see profiling.frontend.directives.DirectiveName
	 * @see #setNAME(DirectiveName)
	 * @see profiling.frontend.directives.DirectivesPackage#getFromVariableDirective_NAME()
	 * @model default="$from_var" unique="false"
	 * @generated
	 */
	DirectiveName getNAME();

	/**
	 * Sets the value of the '{@link profiling.frontend.directives.FromVariableDirective#getNAME <em>NAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>NAME</em>' attribute.
	 * @see profiling.frontend.directives.DirectiveName
	 * @see #getNAME()
	 * @generated
	 */
	void setNAME(DirectiveName value);

	/**
	 * Returns the value of the '<em><b>Inst</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inst</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inst</em>' reference.
	 * @see #setInst(Instruction)
	 * @see profiling.frontend.directives.DirectivesPackage#getFromVariableDirective_Inst()
	 * @model
	 * @generated
	 */
	Instruction getInst();

	/**
	 * Sets the value of the '{@link profiling.frontend.directives.FromVariableDirective#getInst <em>Inst</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inst</em>' reference.
	 * @see #getInst()
	 * @generated
	 */
	void setInst(Instruction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; args = this.getCall().getArgs();\nint _size = args.size();\nboolean _lessThan = (_size &lt; 1);\nif (_lessThan)\n{\n\t&lt;%java.lang.String%&gt; _printLocation = this.printLocation();\n\t&lt;%java.lang.String%&gt; _plus = (_printLocation + \"\\nMust have at least 1 arguments (instruction)!\");\n\tthrow new &lt;%fr.irisa.cairn.gecos.core.profiling.frontend.InvalidDirective%&gt;(_plus);\n}\nthis.setInst(&lt;%java.util.Objects%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;requireNonNull(args.get(0)));'"
	 * @generated
	 */
	void parse();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.BasicBlock%&gt; _xblockexpression = null;\n{\n\tfinal &lt;%gecos.core.ProcedureSet%&gt; ps = this.getCall().getContainingProcedureSet();\n\t_xblockexpression = &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory%&gt;.BBlock(\n\t\t&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory%&gt;.call(\n\t\t\t&lt;%fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport%&gt;.getNativeProc(ps, &lt;%fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport.NativeFunction%&gt;.INJECTOR_FROM_VAR), this.getInst()));\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	Block createCodeBlock();

} // FromVariableDirective
