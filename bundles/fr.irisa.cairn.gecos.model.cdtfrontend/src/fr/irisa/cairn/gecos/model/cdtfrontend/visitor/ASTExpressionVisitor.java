/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.cdtfrontend.visitor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.cdt.core.dom.ast.IASTArraySubscriptExpression;
import org.eclipse.cdt.core.dom.ast.IASTBinaryExpression;
import org.eclipse.cdt.core.dom.ast.IASTCastExpression;
import org.eclipse.cdt.core.dom.ast.IASTConditionalExpression;
import org.eclipse.cdt.core.dom.ast.IASTExpression;
import org.eclipse.cdt.core.dom.ast.IASTExpressionList;
import org.eclipse.cdt.core.dom.ast.IASTExpressionStatement;
import org.eclipse.cdt.core.dom.ast.IASTFieldReference;
import org.eclipse.cdt.core.dom.ast.IASTFunctionCallExpression;
import org.eclipse.cdt.core.dom.ast.IASTIdExpression;
import org.eclipse.cdt.core.dom.ast.IASTLiteralExpression;
import org.eclipse.cdt.core.dom.ast.IASTName;
import org.eclipse.cdt.core.dom.ast.IASTNode;
import org.eclipse.cdt.core.dom.ast.IASTProblemExpression;
import org.eclipse.cdt.core.dom.ast.IASTTypeIdExpression;
import org.eclipse.cdt.core.dom.ast.IASTTypeIdInitializerExpression;
import org.eclipse.cdt.core.dom.ast.IASTUnaryExpression;
import org.eclipse.cdt.core.dom.ast.c.ICASTTypeIdInitializerExpression;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTArraySubscriptExpression;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTBinaryExpression;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTCastExpression;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTDeleteExpression;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTExpressionList;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTFieldReference;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTFunctionCallExpression;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTLiteralExpression;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTNewExpression;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTPackExpansionExpression;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTSimpleTypeConstructorExpression;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTTypeIdExpression;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTUnaryExpression;
import org.eclipse.cdt.core.dom.ast.gnu.IGNUASTCompoundStatementExpression;
import org.eclipse.cdt.core.dom.ast.gnu.cpp.IGPPASTBinaryExpression;
import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.cdtfrontend.exception.CDTParserErrorException;
import fr.irisa.cairn.gecos.model.cdtfrontend.exception.CDTParserNotYetSupportedException;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.factory.conversion.ConversionMatrix;
import gecos.core.ProcedureSymbol;
import gecos.core.Symbol;
import gecos.instrs.CallInstruction;
import gecos.instrs.ConstantInstruction;
import gecos.instrs.FieldInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SimpleArrayInstruction;
import gecos.instrs.SizeofTypeInstruction;
import gecos.instrs.SizeofValueInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.ACType;
import gecos.types.AliasType;
import gecos.types.ArrayType;
import gecos.types.EnumType;
import gecos.types.Enumerator;
import gecos.types.Field;
import gecos.types.FunctionType;
import gecos.types.IntegerType;
import gecos.types.PtrType;
import gecos.types.RecordType;
import gecos.types.SignModifiers;
import gecos.types.Type;

/**
 * @author mnaullet
 * 
 *         FIXME: Post Increment/Decrement operations are not handled correctly
 *         in many cases !!
 */
public class ASTExpressionVisitor {

	private CDTToGecosModel gecosModel;
	private List<Instruction> beforeInstructions;
	private List<Instruction> afterInstructions;
	private Instruction instruction;
	private Type type;
	private Enumerator enumerator;
	private String name;

	public ASTExpressionVisitor() {
	}

	public void visit(CDTToGecosModel gecosModel, IASTExpression expression) {
		this.gecosModel = gecosModel;
		if (expression instanceof IASTArraySubscriptExpression)
			if (expression instanceof ICPPASTArraySubscriptExpression)
				buildICPPASTArraySubscriptExpression((ICPPASTArraySubscriptExpression) expression);
			else
				buildIASTArraySubscriptExpression((IASTArraySubscriptExpression) expression);
		else if (expression instanceof IASTBinaryExpression)
			if (expression instanceof ICPPASTBinaryExpression)
				buildICPPASTBinaryExpression((ICPPASTBinaryExpression) expression);
			else if (expression instanceof IGPPASTBinaryExpression)
				buildIGPPASTBinaryExpression((IGPPASTBinaryExpression) expression);
			else
				buildIASTBinaryExpression((IASTBinaryExpression) expression);
		else if (expression instanceof IASTCastExpression)
			if (expression instanceof ICPPASTCastExpression)
				buildICPPASTCastExpression((ICPPASTCastExpression) expression);
			else
				buildIASTCastExpression((IASTCastExpression) expression);
		else if (expression instanceof IASTConditionalExpression)
			buildIASTConditionalExpression((IASTConditionalExpression) expression);
		else if (expression instanceof IASTExpressionList)
			if (expression instanceof ICPPASTExpressionList)
				buildICPPASTExpressionList((ICPPASTExpressionList) expression);
			else
				buildIASTExpressionList((IASTExpressionList) expression);
		else if (expression instanceof IASTFieldReference)
			if (expression instanceof ICPPASTFieldReference)
				buildICPPASTFieldReference((ICPPASTFieldReference) expression);
			else
				buildIASTFieldReference((IASTFieldReference) expression);
		else if (expression instanceof IASTFunctionCallExpression)
			if (expression instanceof ICPPASTFunctionCallExpression)
				buildICPPASTFunctionCallExpression((ICPPASTFunctionCallExpression) expression);
			else
				buildIASTFunctionCallExpression((IASTFunctionCallExpression) expression);
		else if (expression instanceof IASTIdExpression)
			buildIASTIdExpression((IASTIdExpression) expression);
		else if (expression instanceof IASTLiteralExpression)
			if (expression instanceof ICPPASTLiteralExpression)
				buildICPPASTLiteralExpression((ICPPASTLiteralExpression) expression);
			else
				buildIASTLiteralExpression((IASTLiteralExpression) expression);
		else if (expression instanceof IASTProblemExpression)
			buildIASTProblemExpression((IASTProblemExpression) expression);
		else if (expression instanceof IASTTypeIdExpression)
			if (expression instanceof ICPPASTTypeIdExpression)
				buildICPPASTTypeIdExpression((ICPPASTTypeIdExpression) expression);
			else
				buildIASTTypeIdExpression((IASTTypeIdExpression) expression);
		else if (expression instanceof IASTTypeIdInitializerExpression)
			if (expression instanceof ICASTTypeIdInitializerExpression)
				buildICASTTypeIdInitializerExpression((ICASTTypeIdInitializerExpression) expression);
			else
				buildIASTTypeIdInitializerExpression((IASTTypeIdInitializerExpression) expression);
		else if (expression instanceof IASTUnaryExpression)
			if (expression instanceof ICPPASTUnaryExpression)
				buildICPPASTUnaryExpression((ICPPASTUnaryExpression) expression);
			else
				buildIASTUnaryExpression((IASTUnaryExpression) expression);
		else if (expression instanceof ICPPASTDeleteExpression)
			buildICPPASTDeleteExpression((ICPPASTDeleteExpression) expression);
		else if (expression instanceof ICPPASTNewExpression)
			buildICPPASTNewExpression((ICPPASTNewExpression) expression);
		else if (expression instanceof ICPPASTPackExpansionExpression)
			buildICPPASTPackExpansionExpression((ICPPASTPackExpansionExpression) expression);
		else if (expression instanceof ICPPASTSimpleTypeConstructorExpression)
			buildICPPASTSimpleTypeConstructorExpression((ICPPASTSimpleTypeConstructorExpression) expression);
		else if (expression instanceof IGNUASTCompoundStatementExpression)
			buildIGNUASTCompoundStatementExpression((IGNUASTCompoundStatementExpression) expression);
		else if (expression instanceof IASTExpressionList) {
			buildIASTExpressionList((IASTExpressionList) expression);
		} else {
			throw new CDTParserNotYetSupportedException(expression,
					"This type of expression is not supported by the Gecos-CDT front-end ask naullet@inria.fr");
		}

		if (!gecosModel.visitingInclude())
			gecosModel.addIASTAnnotation(instruction, expression);
		
		gecosModel.addFileLocationAnnotation(instruction, expression.getFileLocation());
	}

	public List<Instruction> getBeforeInstructions() {
		return beforeInstructions;
	}

	public List<Instruction> getAfterInstructions() {
		return afterInstructions;
	}

	public Instruction getInstruction() {
		return instruction;
	}

	public Type getType() {
		return type;
	}

	public Enumerator getEnumerator() {
		return enumerator;
	}

	public String getName() {
		return name;
	}

	private void buildIASTArraySubscriptExpression(IASTArraySubscriptExpression expression) {
		expression.getArgument().accept(gecosModel);
		setBeforeAfterInstructions();
		Instruction argument = ((ASTExpressionVisitor) gecosModel.getObject()).getInstruction();
		if (argument == null) {
			throw new CDTParserErrorException(expression, "Error in CDT parser for " + expression.getRawSignature());
		}
		expression.getArrayExpression().accept(gecosModel);
		addAllBeforeAllAfterInstructions();
		Instruction arrayExpression = ((ASTExpressionVisitor) gecosModel.getObject()).getInstruction();
		if (arrayExpression instanceof SymbolInstruction) {
			instruction = GecosUserInstructionFactory.array(((SymbolInstruction) arrayExpression).getSymbol(),
					argument);
		} else if (arrayExpression instanceof SimpleArrayInstruction) {
			((SimpleArrayInstruction) arrayExpression).addIndex(argument);
			if (arrayExpression.getType() instanceof ArrayType)
				arrayExpression.setType(((ArrayType) arrayExpression.getType()).getBase());
			else if (arrayExpression.getType() instanceof PtrType)
				arrayExpression.setType(((PtrType) arrayExpression.getType()).getBase());
			else
				arrayExpression.setType(arrayExpression.getType());
			instruction = arrayExpression;
		} else {
			instruction = GecosUserInstructionFactory.array(arrayExpression, argument);
			if (arrayExpression.getType() instanceof ArrayType)
				instruction.setType(((ArrayType) arrayExpression.getType()).getBase());
			else if (arrayExpression.getType() instanceof PtrType)
				instruction.setType(((PtrType) arrayExpression.getType()).getBase());
			else
				instruction.setType(arrayExpression.getType());
		}
	}

	private void buildICPPASTArraySubscriptExpression(ICPPASTArraySubscriptExpression expression) {
		buildIASTArraySubscriptExpression(expression);
	}

	private void buildIASTBinaryExpression(IASTBinaryExpression expression) {
		expression.getOperand1().accept(gecosModel);
		setBeforeAfterInstructions();
		Instruction operand1 = ((ASTExpressionVisitor) gecosModel.getObject()).getInstruction();
		expression.getOperand2().accept(gecosModel);
		addAllBeforeAllAfterInstructions();
		Instruction operand2 = ((ASTExpressionVisitor) gecosModel.getObject()).getInstruction();
		if (operand1 == null) {
			throw new CDTParserErrorException(expression,
					"Parse error : " + expression.getOperand1().getRawSignature());
		}
		if (operand2 == null) {
			Enumerator enumerator = ((ASTExpressionVisitor) gecosModel.getObject()).getEnumerator();
			if (enumerator == null) {
				throw new CDTParserErrorException(expression,
						"unsupported operand " + expression.getOperand2().getRawSignature());
			}
			Type type1 = operand1.getType();
			if (!type1.isEqual(enumerator.getEnumType()) && type1 instanceof EnumType) {
				enumerator = gecosModel.getGecosCoreContextedFactory().findEnumerator((EnumType) operand1.getType(),
						enumerator.getName());
			}
			operand2 = GecosUserInstructionFactory.enumorator(enumerator);
		}
		instruction = findBinaryOperator(expression.getOperator(), operand1, operand2);
	}

	private void buildICPPASTBinaryExpression(ICPPASTBinaryExpression expression) {
		buildIASTBinaryExpression(expression);
	}

	private void buildIGPPASTBinaryExpression(IGPPASTBinaryExpression expression) {
		buildIASTBinaryExpression(expression);
	}

	private void buildIASTCastExpression(IASTCastExpression expression) {
		expression.getTypeId().accept(gecosModel);
		Type type = ((ASTTypeIdVisitor) gecosModel.getObject()).getType();
		expression.getOperand().accept(gecosModel);
		this.instruction = GecosUserInstructionFactory.cast(type,
				((ASTExpressionVisitor) gecosModel.getObject()).getInstruction());
	}

	private void buildICPPASTCastExpression(ICPPASTCastExpression expression) {
		buildIASTCastExpression(expression);
	}

	private void buildIASTConditionalExpression(IASTConditionalExpression expression) {
		List<Instruction> beforeInstructions;
		expression.getLogicalConditionExpression().accept(gecosModel);
		Instruction cond = ((ASTExpressionVisitor) gecosModel.getObject()).getInstruction();
		beforeInstructions = ((ASTExpressionVisitor) gecosModel.getObject()).getAfterInstructions();

		expression.getNegativeResultExpression().accept(gecosModel);
		Instruction neg = sequentialiseAfterInstruction(beforeInstructions,
				(ASTExpressionVisitor) gecosModel.getObject());

		expression.getPositiveResultExpression().accept(gecosModel);
		Instruction pos = sequentialiseAfterInstruction(beforeInstructions,
				(ASTExpressionVisitor) gecosModel.getObject());

		instruction = GecosUserInstructionFactory.mux(cond, pos, neg);
	}

	private void buildIASTExpressionList(IASTExpressionList expression) {
		List<Instruction> seqList = new ArrayList<>();
		for (IASTExpression expr : expression.getExpressions()) {
			expr.accept(gecosModel);
			Instruction instruction2 = ((ASTExpressionVisitor) gecosModel.getObject()).getInstruction();
			if (instruction2 != null)
				seqList.add(instruction2);
			List<Instruction> instructions = ((ASTExpressionVisitor) gecosModel.getObject()).getAfterInstructions();
			if (instructions != null)
				for (int i = 0; i < instructions.size(); i++)
					seqList.add(instructions.get(i));
		}

		GenericInstruction seqInst = GecosUserInstructionFactory.sequence(seqList);
		this.instruction = seqInst;
	}

	private void buildICPPASTExpressionList(ICPPASTExpressionList expression) {
		buildIASTExpressionList(expression);
	}

	private void buildIASTFieldReference(IASTFieldReference expression) {
		expression.getFieldOwner().accept(gecosModel);
		ASTExpressionVisitor expressionVisitor = (ASTExpressionVisitor) gecosModel.getObject();
		IASTName fieldName = expression.getFieldName();
		fieldName.accept(gecosModel);
		String name = ((ASTNameVisitor) gecosModel.getObject()).getName();
		if (expressionVisitor.getInstruction() != null) {
			Instruction expressionInstruction = expressionVisitor.getInstruction();
			Field field = null;
			Type type;
			if (expressionInstruction instanceof SymbolInstruction)
				type = ((SymbolInstruction) expressionInstruction).getSymbol().getType();
			else if (expressionInstruction instanceof FieldInstruction)
				type = ((FieldInstruction) expressionInstruction).getField().getType();
			else
				type = expressionInstruction.getType();
			if (!(type instanceof ACType))
				while (!(type instanceof RecordType)) {
					if (type instanceof PtrType)
						type = ((PtrType) type).getBase();
					else if (type instanceof ArrayType)
						type = ((ArrayType) type).getBase();
//					else if (type instanceof ConstType)
//						type = ((ConstType) type).getBase();
					else if (type instanceof AliasType)
						type = ((AliasType) type).getAlias();
//					else if (type instanceof RegisterType)
//						type = ((RegisterType) type).getBase();
					else
						break;
				}
			if (type instanceof AliasType)
				type = ((AliasType) type).getAlias();
			if (type instanceof ACType) {

				throw new CDTParserNotYetSupportedException(expression, "not implemented yet");

			}
			for (Field f : ((RecordType) type).getFields()) {
				if (f.getName().equals(name))
					field = f;
			}
			if (expression.isPointerDereference()) {
				// instruction =
				// GecosUserInstructionFactory.field(GecosUserInstructionFactory.address(expressionInstruction),
				// field);
				instruction = GecosUserInstructionFactory
						.field(GecosUserInstructionFactory.indir(expressionInstruction), field);
			} else {
				instruction = GecosUserInstructionFactory.field(expressionInstruction, field);
			}

		} else if (expressionVisitor.getType() != null) {
			Enumerator enumerator = gecosModel.getGecosCoreContextedFactory()
					.findEnumerator((EnumType) expressionVisitor.getType(), name);
			instruction = GecosUserInstructionFactory.enumorator(enumerator);
		} else if (expressionVisitor.getEnumerator() != null) {
			instruction = GecosUserInstructionFactory.enumorator(expressionVisitor.getEnumerator());
		}
	}

	private void buildICPPASTFieldReference(ICPPASTFieldReference expression) {
		expression.getFieldOwner().accept(gecosModel);
		ASTExpressionVisitor expressionVisitor = (ASTExpressionVisitor) gecosModel.getObject();
		IASTName fieldName = expression.getFieldName();
		fieldName.accept(gecosModel);
		String name = ((ASTNameVisitor) gecosModel.getObject()).getName();
		if (expressionVisitor.getInstruction() != null) {
			Instruction expressionInstruction = expressionVisitor.getInstruction();
			Type type;
						
			if (expressionInstruction instanceof SymbolInstruction)
				type = ((SymbolInstruction) expressionInstruction).getSymbol().getType();
			else if (expressionInstruction instanceof FieldInstruction)
				type = ((FieldInstruction) expressionInstruction).getField().getType();
			else
				type = expressionInstruction.getType();
			
			TypeAnalyzer analyzer = new TypeAnalyzer(type);
			if(analyzer.getBaseLevel().isRecord()){
				buildIASTFieldReference(expression);
			}else{
				this.name = name;
				instruction = expressionInstruction;
			}
		} else if (expressionVisitor.getType() != null) {
			Enumerator enumerator = gecosModel.getGecosCoreContextedFactory()
					.findEnumerator((EnumType) expressionVisitor.getType(), name);
			instruction = GecosUserInstructionFactory.enumorator(enumerator);
		} else if (expressionVisitor.getEnumerator() != null) {
			instruction = GecosUserInstructionFactory.enumorator(expressionVisitor.getEnumerator());
		}
	}

	private void buildIASTFunctionCallExpression(IASTFunctionCallExpression expression) {
		IASTExpression functionNameExpression = expression.getFunctionNameExpression();
		functionNameExpression.accept(gecosModel);
		Instruction calledAddress = ((ASTExpressionVisitor) gecosModel.getObject()).getInstruction();
		if (calledAddress != null) {
			CallInstruction callInstruction = GecosUserInstructionFactory.call(calledAddress);
			
			/*
			 * The called address is either a Procedure with a FunctionType,
			 * or a Ptr.. to a FunctionType (call via a function ptr).
			 */
			FunctionType functionType = null;
			Type tmpType = calledAddress.getType();
			if(tmpType != null) {
				TypeAnalyzer tAnalyer = new TypeAnalyzer(tmpType, true);
				functionType = tAnalyer.getBase().asFunction();
			}

			EList<Type> funcParameterTypes = functionType != null ? functionType.listParameters() : null;
			for (int i = 0; i < expression.getArguments().length; i++) {
				/* Build call ith argument instruction */
				expression.getArguments()[i].accept(gecosModel);
				Instruction arg = ((ASTExpressionVisitor) gecosModel.getObject()).getInstruction();
				if(arg == null)
					throw new CDTParserErrorException(expression, "error while building function call argument number " + i);
				
				/* 
				 * Implicit cast of argument if:
				 * its type is not similar to that specified by the called procedure.
				 */
				if(funcParameterTypes != null) {
					if (i < funcParameterTypes.size()
	//						&& i < ((FunctionType) calledAddress.getType()).listParameters().size()
							&& !funcParameterTypes.get(i).isEqual(arg.getType())) {
						if (!lookslikeSameType(funcParameterTypes.get(i), arg.getType())) {
							// FIXME throw exception or warning
	
							Type castType = funcParameterTypes.get(i); 
							// TODO remove const/static  qualifier from castType
							arg = GecosUserInstructionFactory.cast(castType, arg);
						}
					}
				}
				
				callInstruction.addArg(arg);
			}

			// /* TODO case of implicit function declaration */
			// // ! In absence of a function prototype, char and short become
			// int, and float becomes double.
			// if(functionType.getReturnType() == null) {
			// functionType.getParameters().clear();
			// //specify function type according to its call arguments
			// for(Instruction arg : callInstruction.getArgs()) {
			// functionType.getParameters().add(arg.getType());
			// }
			// functionType.setReturnType(GecosUserTypeFactory.INT()); //XXX
			// callInstruction.setType(functionType.getReturnType());
			// }

			instruction = callInstruction;
		} else {
			throw new CDTParserErrorException(expression,
					"Parse error for " + expression.getRawSignature() + " at line "
							+ expression.getFileLocation().getStartingLineNumber() + " (probably implicit symbol use)");
		}
	}

	private void buildICPPASTFunctionCallExpression(ICPPASTFunctionCallExpression expression) {
		IASTExpression functionNameExpression = expression.getFunctionNameExpression();
		
		if(functionNameExpression instanceof ICPPASTFieldReference){
			functionNameExpression.accept(gecosModel);
			Instruction receiver = ((ASTExpressionVisitor) gecosModel.getObject()).getInstruction();
			String methodName = ((ASTExpressionVisitor) gecosModel.getObject()).getName();
			
			int length = expression.getArguments().length;
			Instruction operands[] = new Instruction[length];
			for (int i = 0; i < length; i++) {
				expression.getArguments()[i].accept(gecosModel);
				operands[i] = ((ASTExpressionVisitor) gecosModel.getObject()).getInstruction();
			}
			
			instruction = GecosUserInstructionFactory.methodCallInstruction(methodName.toString(), receiver, operands);
		}
		else if(functionNameExpression instanceof IASTIdExpression){
			buildIASTFunctionCallExpression(expression);
		}
		else{
			throw new CDTParserErrorException(expression, "Unknown function call or not implemeted yet");
		}
	}

	private void buildIASTIdExpression(IASTIdExpression expression) {
		IASTName exprname = expression.getName();
		exprname.accept(gecosModel);
		name = ((ASTNameVisitor) gecosModel.getObject()).getName();
		Symbol symbol = gecosModel.getGecosCoreContextedFactory().getSymbol(name);
		if (symbol != null) {
			instruction = GecosUserInstructionFactory.symbref(symbol);
		} else {
			type = gecosModel.getGecosCoreContextedFactory().findType(name);
			if (type == null) {
				enumerator = gecosModel.getGecosCoreContextedFactory().findEnumerator(name);
				if (enumerator != null) {
					instruction = GecosUserInstructionFactory.enumorator(enumerator);
				}
			}
		}
		if (instruction == null && type == null && enumerator == null && !"AC_TRN".equals(name)
				&& !"AC_WRAP".equals(name) && !"AC_SAT".equals(name) && !"AC_RND".equals(name)) {
			if ((expression.getParent() instanceof IASTFunctionCallExpression)
					&& (expression == ((IASTFunctionCallExpression) expression.getParent())
							.getFunctionNameExpression())) {
				System.err.println("Warning : implicit function declaration : " + expression.getRawSignature()
						+ " from parent " + expression.getParent().getRawSignature());
//				ProcedureSymbol ps = gecosModel.getGecosCoreContextedFactory().addProcedureSymbol(name,
//						GecosUserTypeFactory.UNDEFINED(), new ArrayList<ParameterSymbol>(0), false);
				ProcedureSymbol ps = gecosModel.getGecosCoreContextedFactory().addProcedureSymbol(name,
						GecosUserTypeFactory.FUNCTION(GecosUserTypeFactory.INT())); //XXX use Undefined() instead ?
				
				// ProcedureSymbol ps =
				// gecosModel.getGecosCoreContextedFactory().addProcedureSymbol(name,
				// null, new ArrayList<ParameterSymbol>(0), false);
				instruction = GecosUserInstructionFactory.symbref(ps);
				gecosModel.ignore(ps);
			} else
				throw new CDTParserErrorException(expression,
						"Parse Error : the identifier \"" + exprname + "\" is not declared in expression \""
								+ expression.getRawSignature() + "\" at line "
								+ expression.getFileLocation().getStartingLineNumber());
		}
	}

	private void buildIASTLiteralExpression(IASTLiteralExpression expression) {
		char c;
		switch (expression.getKind()) {
		case IASTLiteralExpression.lk_char_constant:
			if (expression.getValue()[1] == '\\')
				c = GecosUserInstructionFactory.getEscapedChar(expression.getValue()[2]);
			else
				c = expression.getValue()[1];
			instruction = GecosUserInstructionFactory.Int(Integer.valueOf(c));
			instruction.setType(GecosUserTypeFactory.UCHAR());
			GecosUserAnnotationFactory.pragma(instruction,GecosUserAnnotationFactory.PRAGMA_CHAR_LITTERAL);
			break;
		case IASTLiteralExpression.lk_false:
			instruction = GecosUserInstructionFactory.Int(0);
			instruction.setType(GecosUserTypeFactory.BOOL());
			break;
		case IASTLiteralExpression.lk_float_constant:
			String f = String.valueOf(expression.getValue());
			if (expression.getParent() instanceof IASTUnaryExpression
					&& ((IASTUnaryExpression) expression.getParent()).getOperator() == IASTUnaryExpression.op_minus)
				f = "-" + f;
			if (!f.startsWith("0x") && (f.endsWith("f") || f.endsWith("F"))) {
				f = f.substring(0, f.length() - 1);
			}
			if (!f.startsWith("0x") && (f.endsWith("L"))) {
				f = f.substring(0, f.length() - 1);
			}
			// f = f.replace("+", "");
			instruction = GecosUserInstructionFactory.Float(Double.valueOf(f));
			instruction.setType(GecosUserTypeFactory.FLOAT());
			break;
		case IASTLiteralExpression.lk_integer_constant:
			String integer = String.valueOf(expression.getValue()).toLowerCase();
			if (expression.getParent() instanceof IASTUnaryExpression
					&& ((IASTUnaryExpression) expression.getParent()).getOperator() == IASTUnaryExpression.op_minus)
				integer = "-" + integer;

			boolean isLong = false;
			boolean isLongLong = false;
			boolean isUnsigned = false;

			// pre process
			if (integer.endsWith("ll")) {
				isLongLong = true;
				integer = integer.substring(0, integer.length() - 2);
			}
			if (integer.endsWith("l")) {
				isLong = true;
				integer = integer.substring(0, integer.length() - 1);
			}
			if (integer.endsWith("u")) {
				isUnsigned = true;
				integer = integer.substring(0, integer.length() - 1);
			}

			// process
			if (integer.equals("0") || integer.equals("-0")) {
				// special case
				instruction = GecosUserInstructionFactory.Int(Long.parseLong(integer));
			} else if (integer.startsWith("0b")) {
				long val = Long.parseLong(integer.substring(2), 2);
				instruction = GecosUserInstructionFactory.Int(val);
				GecosUserAnnotationFactory.pragma(instruction,GecosUserAnnotationFactory.PRAGMA_BIN_LITTERAL);
			} else if (integer.startsWith("-0b")) {
				long val = Long.parseLong(integer.substring(3), 2);
				instruction = GecosUserInstructionFactory.Int(val);
				GecosUserAnnotationFactory.pragma(instruction,GecosUserAnnotationFactory.PRAGMA_BIN_LITTERAL);
			} else if (integer.startsWith("0x")) {
				long val = Long.parseLong(integer.substring(2), 16);
				instruction = GecosUserInstructionFactory.Int(val);
				GecosUserAnnotationFactory.pragma(instruction,GecosUserAnnotationFactory.PRAGMA_HEX_LITTERAL);
			} else if (integer.startsWith("-0x")) {
				long val = Long.parseLong(integer.substring(3), 16);
				instruction = GecosUserInstructionFactory.Int(val);
				GecosUserAnnotationFactory.pragma(instruction,GecosUserAnnotationFactory.PRAGMA_HEX_LITTERAL);
			} else if (integer.startsWith("0")) {
				instruction = GecosUserInstructionFactory.Int(Long.parseLong(integer.substring(1), 8));
				GecosUserAnnotationFactory.pragma(instruction,GecosUserAnnotationFactory.PRAGMA_OCT_LITTERAL);
			} else if (integer.startsWith("-0")) {
				instruction = GecosUserInstructionFactory.Int(Long.parseLong(integer.substring(2), 8));
				GecosUserAnnotationFactory.pragma(instruction,GecosUserAnnotationFactory.PRAGMA_OCT_LITTERAL);
			} else {
				instruction = GecosUserInstructionFactory.Int(Long.parseLong(integer));
			}

			// post process
			if (isLong)
				instruction.setType(GecosUserTypeFactory.LONG());
			else if (isLongLong)
				instruction.setType(GecosUserTypeFactory.LONGLONG());
			
			if(isUnsigned)
				((IntegerType) instruction.getType()).setSignModifier(SignModifiers.UNSIGNED);

			break;
		case IASTLiteralExpression.lk_string_literal:
			char[] t = expression.getValue();
			char[] copyOfRange = Arrays.copyOfRange(t, 1, t.length - 1);
			this.instruction = GecosUserInstructionFactory.string(copyOfRange);
			break;
		case IASTLiteralExpression.lk_true:
			instruction = GecosUserInstructionFactory.Int(1);
			instruction.setType(GecosUserTypeFactory.BOOL());
			break;
		case IASTLiteralExpression.lk_this:
			throw new CDTParserNotYetSupportedException(expression, "keyword 'this' not supported");
		default:
			throw new CDTParserNotYetSupportedException(expression, "Literal expression not supported");
		}
	}

	private void buildICPPASTLiteralExpression(ICPPASTLiteralExpression expression) {
		buildIASTLiteralExpression(expression);
	}

	private void buildIASTProblemExpression(IASTProblemExpression expression) {
		throw new CDTParserErrorException(expression,
				"Parse error in the file " + expression.getContainingFilename() + " at line  : "
						+ expression.getFileLocation().getStartingLineNumber() + "\n\tCDT could not parse \""
						+ expression.getRawSignature() + "\" : " + expression.getProblem().getMessage());
	}

	private void buildIASTTypeIdExpression(IASTTypeIdExpression expression) {
		expression.getTypeId().accept(gecosModel);
		Type type = ((ASTTypeIdVisitor) gecosModel.getObject()).getType();
		if (type == null)
			throw new CDTParserErrorException(expression,
					"Could not determine type info from " + expression.getTypeId().getRawSignature());
		SizeofTypeInstruction instruction = GecosUserInstructionFactory.sizeOf(type);
		this.instruction = instruction;
	}

	private void buildICPPASTTypeIdExpression(ICPPASTTypeIdExpression expression) {
		buildIASTTypeIdExpression(expression);
	}

	private void buildIASTTypeIdInitializerExpression(IASTTypeIdInitializerExpression expression) {
		expression.getTypeId().accept(gecosModel);
		Type type = ((ASTTypeIdVisitor) gecosModel.getObject()).getType();
		expression.getInitializer().accept(gecosModel);
		Instruction value = ((ASTInitializerVisitor) gecosModel.getObject()).getInstruction();
		instruction = GecosUserInstructionFactory.cast(type, value);
	}

	private void buildICASTTypeIdInitializerExpression(ICASTTypeIdInitializerExpression expression) {
		buildIASTTypeIdInitializerExpression(expression);
	}
	
	private void buildICPPASTUnaryExpression(ICPPASTUnaryExpression expression) {
		buildIASTUnaryExpression(expression);
	}
	
	private void buildIASTUnaryExpression(IASTUnaryExpression expression) {
		expression.getOperand().accept(gecosModel);
		setBeforeAfterInstructions();
		Instruction operand1 = ((ASTExpressionVisitor) gecosModel.getObject()).getInstruction();
		if (operand1 instanceof ConstantInstruction)
			instruction = operand1;
		else
			instruction = findUnaryExpression(expression, operand1);
	}

	private Instruction findBinaryOperator(int i, Instruction operand1, Instruction operand2) {
		switch (i) {
		case IASTBinaryExpression.op_assign: /* code 17 */
			return GecosUserInstructionFactory.set(operand1, operand2);
		case IASTBinaryExpression.op_binaryAnd: /* code 12 */
			return GecosUserInstructionFactory.and(operand1, operand2);
		case IASTBinaryExpression.op_binaryAndAssign: /* code 25 */
			return GecosUserInstructionFactory.set(operand1,
					GecosUserInstructionFactory.and(operand1.copy(), operand2));
		case IASTBinaryExpression.op_binaryOr: /* code 14 */
			return GecosUserInstructionFactory.or(operand1, operand2);
		case IASTBinaryExpression.op_binaryOrAssign: /* code 27 */
			return GecosUserInstructionFactory.set(operand1, GecosUserInstructionFactory.or(operand1.copy(), operand2));
		case IASTBinaryExpression.op_binaryXor: /* code 13 */
			return GecosUserInstructionFactory.xor(operand1, operand2);
		case IASTBinaryExpression.op_binaryXorAssign: /* code 26 */
			return GecosUserInstructionFactory.set(operand1,
					GecosUserInstructionFactory.xor(operand1.copy(), operand2));
		case IASTBinaryExpression.op_divide: /* code 2 */
			return GecosUserInstructionFactory.div(operand1, operand2);
		case IASTBinaryExpression.op_divideAssign: /* code 19 */
			return GecosUserInstructionFactory.set(operand1,
					GecosUserInstructionFactory.div(operand1.copy(), operand2));
		case IASTBinaryExpression.op_ellipses: /* code 34 */
			return GecosUserInstructionFactory.ellipses(operand1, operand2);
		case IASTBinaryExpression.op_equals: /* code 28 */
			return GecosUserInstructionFactory.eq(operand1, operand2);
		case IASTBinaryExpression.op_greaterEqual: /* code 11 */
			return GecosUserInstructionFactory.ge(operand1, operand2);
		case IASTBinaryExpression.op_greaterThan: /* code 9 */
			return GecosUserInstructionFactory.gt(operand1, operand2);
		case IASTBinaryExpression.op_lessEqual: /* code 10 */
			return GecosUserInstructionFactory.le(operand1, operand2);
		case IASTBinaryExpression.op_lessThan: /* code 8 */
			return GecosUserInstructionFactory.lt(operand1, operand2);
		case IASTBinaryExpression.op_logicalAnd: /* code 15 */
			return GecosUserInstructionFactory.land(operand1, operand2);
		case IASTBinaryExpression.op_logicalOr: /* code 16 */
			return GecosUserInstructionFactory.lor(operand1, operand2);
		case IASTBinaryExpression.op_minus: /* code 5 */
			return GecosUserInstructionFactory.sub(operand1, operand2);
		case IASTBinaryExpression.op_minusAssign: /* code 22 */
			return GecosUserInstructionFactory.set(operand1,
					GecosUserInstructionFactory.sub(operand1.copy(), operand2));
		case IASTBinaryExpression.op_modulo: /* code 3 */
			return GecosUserInstructionFactory.mod(operand1, operand2);
		case IASTBinaryExpression.op_moduloAssign: /* code 20 */
			return GecosUserInstructionFactory.set(operand1,
					GecosUserInstructionFactory.mod(operand1.copy(), operand2));
		case IASTBinaryExpression.op_multiply: /* code 1 */
			return GecosUserInstructionFactory.mul(operand1, operand2);
		case IASTBinaryExpression.op_multiplyAssign: /* code 18 */
			return GecosUserInstructionFactory.set(operand1,
					GecosUserInstructionFactory.mul(operand1.copy(), operand2));
		case IASTBinaryExpression.op_notequals: /* code 29 */
			return GecosUserInstructionFactory.ne(operand1, operand2);
		case IASTBinaryExpression.op_plus: /* code 4 */
			return GecosUserInstructionFactory.add(operand1, operand2);
		case IASTBinaryExpression.op_plusAssign: /* code 21 */
			return GecosUserInstructionFactory.set(operand1,
					GecosUserInstructionFactory.add(operand1.copy(), operand2));
		case IASTBinaryExpression.op_shiftLeft: /* code 6 */
			return GecosUserInstructionFactory.shl(operand1, operand2);
		case IASTBinaryExpression.op_shiftLeftAssign: /* code 23 */
			return GecosUserInstructionFactory.set(operand1, GecosUserInstructionFactory.shl(operand1, operand2));
		case IASTBinaryExpression.op_shiftRight: /* code 7 */
			return GecosUserInstructionFactory.shr(operand1, operand2);
		case IASTBinaryExpression.op_shiftRightAssign: /* code 24 */
			return GecosUserInstructionFactory.set(operand1, GecosUserInstructionFactory.shr(operand1, operand2));

		case IASTBinaryExpression.op_max: /* code 32 G++ only */
			return GecosUserInstructionFactory.max(operand1, operand2);
		case IASTBinaryExpression.op_min: /* code 33 G++ only */
			return GecosUserInstructionFactory.min(operand1, operand2);

		// case IASTBinaryExpression.op_pmarrow : /* code 31 C++ only */
		// return GecosUserInstructionFactory.generic("->", operand1, operand2);
		// case IASTBinaryExpression.op_pmdot : /* code 30 C++ only */
		// return GecosUserInstructionFactory.generic(".", operand1, operand2);
		default:
			throw new RuntimeException("Binary opcode " + i + " Not yet supported");
		}
	}

	private Instruction findUnaryExpression(IASTUnaryExpression unaryExpression, Instruction operand) {
		Instruction instruction;
		switch (unaryExpression.getOperator()) {
		case IASTUnaryExpression.op_prefixIncr: /* code 0 */
			instruction = GecosUserInstructionFactory.Int(1);
			return GecosUserInstructionFactory.set(operand.copy(),
					GecosUserInstructionFactory.add(operand.copy(), instruction));
		case IASTUnaryExpression.op_prefixDecr: /* code 1 */
			instruction = GecosUserInstructionFactory.Int(1);
			return GecosUserInstructionFactory.set(operand.copy(),
					GecosUserInstructionFactory.sub(operand.copy(), instruction));
		case IASTUnaryExpression.op_plus: /* code 2 */
			return operand;
		case IASTUnaryExpression.op_minus: /* code 3 */
			if (operand instanceof IntInstruction) {
				((IntInstruction) operand).setValue(-((IntInstruction) operand).getValue());
				return operand;
			} else if (operand instanceof FloatInstruction) {
				((FloatInstruction) operand).setValue(-((FloatInstruction) operand).getValue());
				return operand;
			}
			return GecosUserInstructionFactory.neg(operand);
		case IASTUnaryExpression.op_star: /* code 4 */
			return GecosUserInstructionFactory.indir(operand);
		case IASTUnaryExpression.op_amper: /* code 5 */
			return GecosUserInstructionFactory.address(operand);
		case IASTUnaryExpression.op_tilde: /* code 6 */
			return GecosUserInstructionFactory.not(operand);
		case IASTUnaryExpression.op_not: /* code 7 */
			return GecosUserInstructionFactory.lnot(operand);
		case IASTUnaryExpression.op_sizeof: /* code 8 */
			SizeofValueInstruction sizeofValueInstruction = GecosUserInstructionFactory.sizeOf(operand);
			return sizeofValueInstruction;
		case IASTUnaryExpression.op_postFixIncr: /* code 9 */
			instruction = GecosUserInstructionFactory.Int(1);
			SetInstruction set = GecosUserInstructionFactory.set(operand.copy(),
					GecosUserInstructionFactory.add(operand.copy(), instruction));

			IASTNode parent = unaryExpression.getParent();

			/** FIXME: handling i++ is not correct in most cases !! **/
			/* Cases where i++ is generated as: i=i+1; */
			if ((parent instanceof IASTExpressionList) // XXX? case of sequence
														// ??
					|| (parent instanceof IASTExpressionStatement) // case like
																	// "i++;"
																	// (the
																	// evaluation
																	// of i++ is
																	// not used)
			)
				operand = set;
			/* cases where i++ is generated as: i; i=i+1; */
			else {
				if (afterInstructions == null)
					afterInstructions = new ArrayList<Instruction>();
				afterInstructions.add(set);
			}
			return operand;
		case IASTUnaryExpression.op_postFixDecr: /* code 10 */
			instruction = GecosUserInstructionFactory.Int(1);
			SetInstruction set_decr = GecosUserInstructionFactory.set(operand.copy(),
					GecosUserInstructionFactory.sub(operand.copy(), instruction));

			IASTNode parent_decr = unaryExpression.getParent();

			/** FIXME: handling i-- is not correct in most cases !! **/
			/* Cases where i-- is generated as: i=i-1; */
			if ((parent_decr instanceof IASTExpressionList) // XXX? case of
															// sequence ??
					|| (parent_decr instanceof IASTExpressionStatement) // case
																		// like
																		// "i--;"
																		// (the
																		// evaluation
																		// of
																		// i--
																		// is
																		// not
																		// used)
			)
				operand = set_decr;
			/* cases where i-- is generated as: i; i=i-1; */
			else {
				if (afterInstructions == null)
					afterInstructions = new ArrayList<Instruction>();
				afterInstructions.add(set_decr);
			}
			return operand;
		case IASTUnaryExpression.op_bracketedPrimary: /* code 11 */
			return operand;
		// case IASTUnaryExpression.op_throw : /* code 12 : C++ only */
		// case IASTUnaryExpression.op_typeid : /* code 13 : C++ only */
		// case IASTUnaryExpression.op_typeof : /* code 14 : C++ only */
		// case IASTUnaryExpression.op_alignOf : /* code 15 : C++ only */
		// case IASTUnaryExpression.op_sizeofParameterPack : /* code 16 : C++
		// only */
		// case IASTUnaryExpression.op_noexcept : /* code 17 : C++ only */
		default:
			throw new CDTParserNotYetSupportedException(unaryExpression,
					"Support for " + unaryExpression.getClass().getSimpleName() + " not yet implemented");
		}
	}

	private void setBeforeAfterInstructions() {
		beforeInstructions = ((ASTExpressionVisitor) gecosModel.getObject()).getBeforeInstructions();
		afterInstructions = ((ASTExpressionVisitor) gecosModel.getObject()).getAfterInstructions();
	}

	private void addAllBeforeAllAfterInstructions() {
		List<Instruction> instructions = ((ASTExpressionVisitor) gecosModel.getObject()).getBeforeInstructions();
		if (beforeInstructions != null && instructions != null)
			beforeInstructions.addAll(instructions);
		else if (beforeInstructions == null)
			beforeInstructions = instructions;
		instructions = ((ASTExpressionVisitor) gecosModel.getObject()).getAfterInstructions();
		if (afterInstructions != null && instructions != null)
			afterInstructions.addAll(instructions);
		else if (afterInstructions == null)
			afterInstructions = instructions;
	}

	private Instruction sequentialiseAfterInstruction(List<Instruction> beforeInstructions,
			ASTExpressionVisitor expressionVisitor) {
		if (beforeInstructions == null) {
			if (expressionVisitor.getAfterInstructions() != null) {
				if (expressionVisitor.getInstruction() != null
						&& expressionVisitor.getInstruction() instanceof SymbolInstruction
						&& expressionVisitor.getAfterInstructions().size() == 1)
					return expressionVisitor.getAfterInstructions().get(0);
				else {
					List<Instruction> seqList = new ArrayList<>();
					seqList.add(expressionVisitor.getInstruction());
					for (int i = 0; i < expressionVisitor.getAfterInstructions().size(); i++)
						seqList.add(expressionVisitor.getAfterInstructions().get(i));
					GenericInstruction genericInstruction = GecosUserInstructionFactory.sequence(seqList);
					return genericInstruction;
				}
			} else
				return expressionVisitor.getInstruction();
		} else {
			List<Instruction> seqList = new ArrayList<>();
			for (int i = 0; i < beforeInstructions.size(); i++)
				seqList.add(beforeInstructions.get(i).copy());
			if (expressionVisitor.getInstruction() != null
					&& !(expressionVisitor.getInstruction() instanceof SymbolInstruction))
				seqList.add(expressionVisitor.getInstruction());
			if (expressionVisitor.getAfterInstructions() != null)
				for (int i = 0; i < expressionVisitor.getAfterInstructions().size(); i++)
					seqList.add(expressionVisitor.getAfterInstructions().get(i));
			GenericInstruction genericInstruction = GecosUserInstructionFactory.sequence(seqList);
			return genericInstruction;
		}
	}

	// private boolean lookslikeSameType(Type requestedType, Type providedType)
	// {
	// Type requestedSubType = getSubType(requestedType);
	// Type providedSubType = getSubType(providedType);
	// try {
	// if (requestedSubType != null && providedSubType != null)
	// if (requestedSubType instanceof BaseType && providedSubType instanceof
	// BaseType)
	// return ConversionMatrix.getCommon(requestedSubType, providedSubType) !=
	// null;
	// else if (requestedSubType instanceof RecordType && providedSubType
	// instanceof RecordType)
	// return requestedSubType.isSame(providedSubType);
	// else if (requestedSubType instanceof AliasType && providedSubType
	// instanceof AliasType)
	// return requestedSubType.isSame(providedSubType);
	// else if (requestedSubType instanceof BaseType &&
	// ((BaseType)requestedSubType).getType() == Types.VOID && requestedType
	// instanceof PtrType && providedType instanceof PtrType)
	// return true;
	// else if (providedSubType instanceof BaseType &&
	// ((BaseType)providedSubType).getType() == Types.VOID && requestedType
	// instanceof PtrType && providedType instanceof PtrType)
	// return true;
	// else
	// return lookslikeSameType(requestedSubType, providedSubType);
	// else if (requestedSubType == null && providedSubType == null)
	// if (requestedType instanceof AliasType)
	// return lookslikeSameType(((AliasType)requestedType).getAlias(),
	// providedType);
	// else if (providedType instanceof AliasType)
	// return lookslikeSameType(requestedType,
	// ((AliasType)providedType).getAlias());
	// else
	// return ConversionMatrix.getCommon(requestedType, providedType) != null;
	// else
	// return false;
	// } catch (RuntimeException runtimeException) {
	// return false;
	// }
	// return false;
	// }
	//
	// private Type getSubType(Type type) {
	// Type subType = null;
	// if (type instanceof ArrayType)
	// subType = ((ArrayType)type).getBase();
	// else if (type instanceof PtrType)
	// subType = ((PtrType)type).getBase();
	// return subType;
	// }

	private boolean lookslikeSameType(Type requestedType, Type providedType) {
		try {
			TypeAnalyzer req = new TypeAnalyzer(requestedType);
			TypeAnalyzer pro = new TypeAnalyzer(providedType);

			// FIXME in case of pointer or array types. We should compare the
			// outermost dimensions

			// if (req.getBaseLevel().isAlias() && pro.getBaseLevel().isAlias())
			// return
			// req.getBaseLevel().getAlias().isSame(pro.getBaseLevel().getAlias());
			if (req.getBaseLevel().isRecord() && pro.getBaseLevel().isRecord())
				return req.getBase().isEqual(pro.getBase());
			if (req.isPointer() && req.getBaseLevel().isVoid() && pro.isPointer())
				return true;
			if (pro.isPointer() && pro.getBaseLevel().isVoid() && req.isPointer())
				return true;
			return ConversionMatrix.getCommon(req.getBase(), pro.getBase()) != null;

		} catch (RuntimeException runtimeException) {
			return false;
		}
	}

	private void buildICPPASTDeleteExpression(ICPPASTDeleteExpression expression) {
		notYetSupported(expression);
	}

	private void buildICPPASTNewExpression(ICPPASTNewExpression expression) {
		notYetSupported(expression);
	}

	private void buildICPPASTPackExpansionExpression(ICPPASTPackExpansionExpression expression) {
		notYetSupported(expression);
	}

	private void buildICPPASTSimpleTypeConstructorExpression(ICPPASTSimpleTypeConstructorExpression expression) {
		notYetSupported(expression);
	}

	private void buildIGNUASTCompoundStatementExpression(IGNUASTCompoundStatementExpression expression) {
		notYetSupported(expression);
	}
	
	private static void notYetSupported(IASTExpression expression) {
		throw new CDTParserNotYetSupportedException(expression,
				"Support for " + expression.getClass().getSimpleName() + " not yet implemented");
	}

}