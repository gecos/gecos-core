package templates.xtend;

import com.google.common.base.Objects;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableAnnotationCGenerator;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableBlockCGenerator;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableCoreCGenerator;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableInstructionCGenerator;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableTypeCGenerator;
import gecos.annotations.AnnotatedElement;
import gecos.annotations.CommentAnnotation;
import gecos.annotations.IAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.ForC99Block;
import gecos.blocks.IfBlock;
import gecos.blocks.SimpleForBlock;
import gecos.blocks.SwitchBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Procedure;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.DummyInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.LabelInstruction;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import templates.xtend.GecosTemplate;

@SuppressWarnings("all")
public class GecosBlockTemplate extends GecosTemplate {
  private String comma;
  
  private String jump;
  
  private EMap<Long, Block> caseMaps;
  
  private Block defaultBlock;
  
  private static GecosBlockTemplate singleton;
  
  public GecosBlockTemplate() {
    GecosBlockTemplate.singleton = this;
  }
  
  public static GecosBlockTemplate getInstance() {
    return GecosBlockTemplate.singleton;
  }
  
  protected Object _generate(final BasicBlock basicBlock) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      StringConcatenation _builder = new StringConcatenation();
      buffer.append(_builder);
      StringConcatenation _builder_1 = new StringConcatenation();
      CharSequence _generateAnnotations = this.generateAnnotations(basicBlock);
      _builder_1.append(_generateAnnotations);
      buffer.append(_builder_1);
      boolean needBraces = false;
      if (((basicBlock.getPragma() != null) && (!basicBlock.getPragma().getContent().isEmpty()))) {
        needBraces = true;
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append("{");
        _builder_2.newLine();
        buffer.append(_builder_2);
      }
      EList<Instruction> _instructions = basicBlock.getInstructions();
      for (final Instruction instruction : _instructions) {
        {
          if (((instruction.getPragma() != null) && (!instruction.getPragma().getContent().isEmpty()))) {
            StringConcatenation _builder_3 = new StringConcatenation();
            String _generate = ExtendableAnnotationCGenerator.eInstance.generate(instruction.getPragma());
            _builder_3.append(_generate);
            buffer.append(_builder_3);
          }
          StringConcatenation _builder_4 = new StringConcatenation();
          String _generate_1 = ExtendableInstructionCGenerator.eInstance.generate(instruction);
          _builder_4.append(_generate_1);
          buffer.append(_builder_4);
          if ((!((instruction instanceof DummyInstruction) || (instruction instanceof LabelInstruction)))) {
            StringConcatenation _builder_5 = new StringConcatenation();
            _builder_5.append(this.comma);
            buffer.append(_builder_5);
          }
          if (((instruction instanceof LabelInstruction) && this.addLabelComma(((LabelInstruction) instruction), basicBlock))) {
            StringConcatenation _builder_6 = new StringConcatenation();
            _builder_6.append(this.comma);
            buffer.append(_builder_6);
          }
          if ((!(instruction instanceof DummyInstruction))) {
            StringConcatenation _builder_7 = new StringConcatenation();
            _builder_7.append(this.jump);
            buffer.append(_builder_7);
          }
        }
      }
      if (needBraces) {
        StringConcatenation _builder_3 = new StringConcatenation();
        _builder_3.append("}");
        _builder_3.newLine();
        buffer.append(_builder_3);
      }
      StringConcatenation _builder_4 = new StringConcatenation();
      _builder_4.append(buffer);
      _xblockexpression = _builder_4;
    }
    return _xblockexpression;
  }
  
  public boolean addLabelComma(final LabelInstruction instruction, final BasicBlock block) {
    int _indexOf = block.getInstructions().indexOf(instruction);
    int _instructionCount = block.getInstructionCount();
    int _minus = (_instructionCount - 1);
    boolean b1 = (_indexOf == _minus);
    boolean b2 = ((block.getParent() instanceof CompositeBlock) && (((CompositeBlock) block.getParent()).getChildren().indexOf(block) == (((CompositeBlock) block.getParent()).getChildren().size() - 1)));
    return (b1 && b2);
  }
  
  protected Object _generate(final CompositeBlock compositeBlock) {
    CharSequence _xblockexpression = null;
    {
      this.comma = ";";
      this.jump = "\n";
      CharSequence _xifexpression = null;
      if (((compositeBlock.eContainer() instanceof Procedure) && ((compositeBlock.getChildren().size() > 1) && (compositeBlock.getChildren().get(1) instanceof CompositeBlock)))) {
        StringConcatenation _builder = new StringConcatenation();
        CharSequence _generateAnnotations = this.generateAnnotations(compositeBlock);
        _builder.append(_generateAnnotations);
        String _generate = ExtendableBlockCGenerator.eInstance.generate(compositeBlock.getChildren().get(1));
        _builder.append(_generate);
        _xifexpression = _builder;
      } else {
        CharSequence _xblockexpression_1 = null;
        {
          final StringBuffer buffer = new StringBuffer();
          StringConcatenation _builder_1 = new StringConcatenation();
          CharSequence _printCase = this.printCase(compositeBlock);
          _builder_1.append(_printCase);
          CharSequence _generateAnnotations_1 = this.generateAnnotations(compositeBlock);
          _builder_1.append(_generateAnnotations_1);
          _builder_1.append("{");
          _builder_1.newLineIfNotEmpty();
          buffer.append(_builder_1);
          Scope _scope = compositeBlock.getScope();
          boolean _tripleNotEquals = (_scope != null);
          if (_tripleNotEquals) {
            final String scopeGen = ExtendableCoreCGenerator.eInstance.generate(compositeBlock.getScope());
            int _length = scopeGen.length();
            boolean _greaterThan = (_length > 0);
            if (_greaterThan) {
              StringConcatenation _builder_2 = new StringConcatenation();
              _builder_2.append("\t");
              _builder_2.append(scopeGen, "\t");
              buffer.append(_builder_2);
            }
          }
          EList<Block> _children = compositeBlock.getChildren();
          for (final Block block : _children) {
            {
              final String charSequence = ExtendableBlockCGenerator.eInstance.generate(block);
              boolean _equals = charSequence.toString().equals("");
              boolean _not = (!_equals);
              if (_not) {
                StringConcatenation _builder_3 = new StringConcatenation();
                _builder_3.append("\t");
                CharSequence _printCase_1 = this.printCase(block);
                _builder_3.append(_printCase_1, "\t");
                _builder_3.append(charSequence, "\t");
                buffer.append(_builder_3);
              }
            }
          }
          StringConcatenation _builder_3 = new StringConcatenation();
          _builder_3.append("}");
          buffer.append(_builder_3);
          if (((compositeBlock.eContainer() instanceof CompositeBlock) && (!(compositeBlock.eContainer().eContainer() instanceof Procedure)))) {
            buffer.append(this.jump);
          }
          StringConcatenation _builder_4 = new StringConcatenation();
          String _string = buffer.toString();
          _builder_4.append(_string);
          _xblockexpression_1 = _builder_4;
        }
        _xifexpression = _xblockexpression_1;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final ForBlock forBlock) {
    CharSequence _xblockexpression = null;
    {
      this.comma = "";
      this.jump = "";
      final StringBuffer buffer = new StringBuffer();
      StringConcatenation _builder = new StringConcatenation();
      buffer.append(_builder);
      buffer.append(this.generateAnnotations(forBlock));
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("for(");
      String _generate = ExtendableBlockCGenerator.eInstance.generate(forBlock.getInitBlock());
      _builder_1.append(_generate);
      _builder_1.append("; ");
      String _generate_1 = ExtendableBlockCGenerator.eInstance.generate(forBlock.getTestBlock());
      _builder_1.append(_generate_1);
      _builder_1.append("; ");
      String _generate_2 = ExtendableBlockCGenerator.eInstance.generate(forBlock.getStepBlock());
      _builder_1.append(_generate_2);
      _builder_1.append(")");
      buffer.append(_builder_1);
      this.comma = ";";
      this.jump = "\n";
      Block _bodyBlock = forBlock.getBodyBlock();
      if ((_bodyBlock instanceof CompositeBlock)) {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append(" ");
        buffer.append(_builder_2);
        StringConcatenation _builder_3 = new StringConcatenation();
        String _generate_3 = ExtendableBlockCGenerator.eInstance.generate(forBlock.getBodyBlock());
        _builder_3.append(_generate_3);
        buffer.append(_builder_3);
      } else {
        if (((forBlock.getBodyBlock() instanceof BasicBlock) && (((BasicBlock) forBlock.getBodyBlock()).getInstructionCount() > 1))) {
          StringConcatenation _builder_4 = new StringConcatenation();
          _builder_4.append(" ");
          _builder_4.append("{");
          _builder_4.newLine();
          buffer.append(_builder_4);
          StringConcatenation _builder_5 = new StringConcatenation();
          _builder_5.append("\t");
          String _generate_4 = ExtendableBlockCGenerator.eInstance.generate(forBlock.getBodyBlock());
          _builder_5.append(_generate_4, "\t");
          buffer.append(_builder_5);
          StringConcatenation _builder_6 = new StringConcatenation();
          _builder_6.append("}");
          buffer.append(_builder_6);
        } else {
          Block _bodyBlock_1 = forBlock.getBodyBlock();
          boolean _tripleNotEquals = (_bodyBlock_1 != null);
          if (_tripleNotEquals) {
            StringConcatenation _builder_7 = new StringConcatenation();
            _builder_7.newLine();
            _builder_7.append("\t");
            String _generate_5 = ExtendableBlockCGenerator.eInstance.generate(forBlock.getBodyBlock());
            _builder_7.append(_generate_5, "\t");
            buffer.append(_builder_7);
          } else {
            buffer.append("/* null body block */;");
          }
        }
      }
      StringConcatenation _builder_8 = new StringConcatenation();
      _builder_8.append(buffer);
      _builder_8.newLineIfNotEmpty();
      _xblockexpression = _builder_8;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final ForC99Block forC99Block) {
    CharSequence _xblockexpression = null;
    {
      this.comma = "";
      this.jump = "";
      final StringBuffer buffer = new StringBuffer();
      StringConcatenation _builder = new StringConcatenation();
      CharSequence _generateAnnotations = this.generateAnnotations(forC99Block);
      _builder.append(_generateAnnotations);
      _builder.append("for(");
      buffer.append(_builder);
      StringConcatenation _builder_1 = new StringConcatenation();
      StringBuffer _printForScope = this.printForScope(forC99Block.getScope());
      _builder_1.append(_printForScope);
      _builder_1.append("; ");
      buffer.append(_builder_1);
      StringConcatenation _builder_2 = new StringConcatenation();
      String _generate = ExtendableBlockCGenerator.eInstance.generate(forC99Block.getTestBlock());
      _builder_2.append(_generate);
      _builder_2.append("; ");
      String _generate_1 = ExtendableBlockCGenerator.eInstance.generate(forC99Block.getStepBlock());
      _builder_2.append(_generate_1);
      _builder_2.append(")");
      buffer.append(_builder_2);
      this.comma = ";";
      this.jump = "\n";
      Block _bodyBlock = forC99Block.getBodyBlock();
      if ((_bodyBlock instanceof CompositeBlock)) {
        StringConcatenation _builder_3 = new StringConcatenation();
        _builder_3.append(" ");
        buffer.append(_builder_3);
        StringConcatenation _builder_4 = new StringConcatenation();
        String _generate_2 = ExtendableBlockCGenerator.eInstance.generate(forC99Block.getBodyBlock());
        _builder_4.append(_generate_2);
        buffer.append(_builder_4);
      } else {
        if (((forC99Block.getBodyBlock() instanceof BasicBlock) && (((BasicBlock) forC99Block.getBodyBlock()).getInstructionCount() > 1))) {
          StringConcatenation _builder_5 = new StringConcatenation();
          _builder_5.append(" ");
          _builder_5.append("{");
          _builder_5.newLine();
          buffer.append(_builder_5);
          StringConcatenation _builder_6 = new StringConcatenation();
          _builder_6.append("\t");
          String _generate_3 = ExtendableBlockCGenerator.eInstance.generate(forC99Block.getBodyBlock());
          _builder_6.append(_generate_3, "\t");
          buffer.append(_builder_6);
          StringConcatenation _builder_7 = new StringConcatenation();
          _builder_7.append("}");
          buffer.append(_builder_7);
        } else {
          Block _bodyBlock_1 = forC99Block.getBodyBlock();
          boolean _tripleNotEquals = (_bodyBlock_1 != null);
          if (_tripleNotEquals) {
            StringConcatenation _builder_8 = new StringConcatenation();
            _builder_8.newLine();
            _builder_8.append("\t");
            String _generate_4 = ExtendableBlockCGenerator.eInstance.generate(forC99Block.getBodyBlock());
            _builder_8.append(_generate_4, "\t");
            buffer.append(_builder_8);
          } else {
            buffer.append("/* null body block */;");
          }
        }
      }
      StringConcatenation _builder_9 = new StringConcatenation();
      _builder_9.append(buffer);
      _builder_9.newLineIfNotEmpty();
      _xblockexpression = _builder_9;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final IfBlock ifBlock) {
    CharSequence _xblockexpression = null;
    {
      this.comma = "";
      this.jump = "";
      final StringBuffer buffer = new StringBuffer();
      StringConcatenation _builder = new StringConcatenation();
      CharSequence _generateAnnotations = this.generateAnnotations(ifBlock);
      _builder.append(_generateAnnotations);
      _builder.append("if (");
      String _generate = ExtendableBlockCGenerator.eInstance.generate(ifBlock.getTestBlock());
      _builder.append(_generate);
      _builder.append(")");
      buffer.append(_builder);
      this.comma = ";";
      this.jump = "\n";
      Block _thenBlock = ifBlock.getThenBlock();
      if ((_thenBlock instanceof CompositeBlock)) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append(" ");
        buffer.append(_builder_1);
        StringConcatenation _builder_2 = new StringConcatenation();
        String _generate_1 = ExtendableBlockCGenerator.eInstance.generate(ifBlock.getThenBlock());
        _builder_2.append(_generate_1);
        buffer.append(_builder_2);
        Block _elseBlock = ifBlock.getElseBlock();
        boolean _tripleNotEquals = (_elseBlock != null);
        if (_tripleNotEquals) {
          StringConcatenation _builder_3 = new StringConcatenation();
          _builder_3.append(" ");
          buffer.append(_builder_3);
        }
      } else {
        if (((ifBlock.getThenBlock() instanceof BasicBlock) && (((BasicBlock) ifBlock.getThenBlock()).getInstructionCount() > 1))) {
          StringConcatenation _builder_4 = new StringConcatenation();
          _builder_4.append(" ");
          _builder_4.append("{");
          _builder_4.newLine();
          buffer.append(_builder_4);
          StringConcatenation _builder_5 = new StringConcatenation();
          _builder_5.append("\t");
          String _generate_2 = ExtendableBlockCGenerator.eInstance.generate(ifBlock.getThenBlock());
          _builder_5.append(_generate_2, "\t");
          buffer.append(_builder_5);
          StringConcatenation _builder_6 = new StringConcatenation();
          _builder_6.append("}");
          buffer.append(_builder_6);
          Block _elseBlock_1 = ifBlock.getElseBlock();
          boolean _tripleNotEquals_1 = (_elseBlock_1 != null);
          if (_tripleNotEquals_1) {
            StringConcatenation _builder_7 = new StringConcatenation();
            _builder_7.append(" ");
            buffer.append(_builder_7);
          }
        } else {
          buffer.append("\n");
          StringConcatenation _builder_8 = new StringConcatenation();
          _builder_8.append("\t");
          String _generate_3 = ExtendableBlockCGenerator.eInstance.generate(ifBlock.getThenBlock());
          _builder_8.append(_generate_3, "\t");
          buffer.append(_builder_8);
        }
      }
      Block _elseBlock_2 = ifBlock.getElseBlock();
      boolean _tripleNotEquals_2 = (_elseBlock_2 != null);
      if (_tripleNotEquals_2) {
        Block _elseBlock_3 = ifBlock.getElseBlock();
        if ((_elseBlock_3 instanceof CompositeBlock)) {
          StringConcatenation _builder_9 = new StringConcatenation();
          _builder_9.append("else ");
          buffer.append(_builder_9);
          StringConcatenation _builder_10 = new StringConcatenation();
          String _generate_4 = ExtendableBlockCGenerator.eInstance.generate(ifBlock.getElseBlock());
          _builder_10.append(_generate_4);
          buffer.append(_builder_10);
        } else {
          if (((ifBlock.getElseBlock() instanceof BasicBlock) && (((BasicBlock) ifBlock.getElseBlock()).getInstructionCount() > 1))) {
            StringConcatenation _builder_11 = new StringConcatenation();
            _builder_11.append("else {");
            _builder_11.newLine();
            buffer.append(_builder_11);
            StringConcatenation _builder_12 = new StringConcatenation();
            _builder_12.append("\t");
            String _generate_5 = ExtendableBlockCGenerator.eInstance.generate(ifBlock.getElseBlock());
            _builder_12.append(_generate_5, "\t");
            buffer.append(_builder_12);
            StringConcatenation _builder_13 = new StringConcatenation();
            _builder_13.append("}");
            buffer.append(_builder_13);
          } else {
            StringConcatenation _builder_14 = new StringConcatenation();
            _builder_14.append("else");
            _builder_14.newLine();
            buffer.append(_builder_14);
            StringConcatenation _builder_15 = new StringConcatenation();
            _builder_15.append("\t");
            String _generate_6 = ExtendableBlockCGenerator.eInstance.generate(ifBlock.getElseBlock());
            _builder_15.append(_generate_6, "\t");
            buffer.append(_builder_15);
          }
        }
      }
      StringConcatenation _builder_16 = new StringConcatenation();
      String _string = buffer.toString();
      _builder_16.append(_string);
      _builder_16.newLineIfNotEmpty();
      _xblockexpression = _builder_16;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final DoWhileBlock loopBlock) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      StringConcatenation _builder = new StringConcatenation();
      CharSequence _generateAnnotations = this.generateAnnotations(loopBlock);
      _builder.append(_generateAnnotations);
      _builder.append("do ");
      String _generate = ExtendableBlockCGenerator.eInstance.generate(loopBlock.getBodyBlock());
      _builder.append(_generate);
      _builder.append(" while (");
      buffer.append(_builder);
      this.comma = "";
      this.jump = "";
      Block _testBlock = loopBlock.getTestBlock();
      boolean _tripleEquals = (_testBlock == null);
      if (_tripleEquals) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("1 /* warning: test block is null */");
        buffer.append(_builder_1);
      } else {
        StringConcatenation _builder_2 = new StringConcatenation();
        String _generate_1 = ExtendableBlockCGenerator.eInstance.generate(loopBlock.getTestBlock());
        _builder_2.append(_generate_1);
        buffer.append(_builder_2);
      }
      StringConcatenation _builder_3 = new StringConcatenation();
      _builder_3.append(");");
      _builder_3.newLine();
      buffer.append(_builder_3);
      this.comma = ";";
      this.jump = "\n";
      StringConcatenation _builder_4 = new StringConcatenation();
      _builder_4.append(buffer);
      _xblockexpression = _builder_4;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final SimpleForBlock simpleForBlock) {
    CharSequence _xblockexpression = null;
    {
      this.comma = "";
      this.jump = "";
      final StringBuffer buffer = new StringBuffer();
      StringConcatenation _builder = new StringConcatenation();
      CharSequence _generateAnnotations = this.generateAnnotations(simpleForBlock);
      _builder.append(_generateAnnotations);
      _builder.append("for(");
      buffer.append(_builder);
      BasicBlock _initBlock = simpleForBlock.getInitBlock();
      int _instructionCount = ((BasicBlock) _initBlock).getInstructionCount();
      boolean _equals = (_instructionCount == 0);
      if (_equals) {
        StringConcatenation _builder_1 = new StringConcatenation();
        StringBuffer _printForScope = this.printForScope(simpleForBlock.getScope());
        _builder_1.append(_printForScope);
        _builder_1.append("; ");
        buffer.append(_builder_1);
      } else {
        StringConcatenation _builder_2 = new StringConcatenation();
        String _generate = ExtendableBlockCGenerator.eInstance.generate(simpleForBlock.getInitBlock());
        _builder_2.append(_generate);
        _builder_2.append("; ");
        buffer.append(_builder_2);
      }
      StringConcatenation _builder_3 = new StringConcatenation();
      String _generate_1 = ExtendableBlockCGenerator.eInstance.generate(simpleForBlock.getTestBlock());
      _builder_3.append(_generate_1);
      _builder_3.append("; ");
      String _generate_2 = ExtendableBlockCGenerator.eInstance.generate(simpleForBlock.getStepBlock());
      _builder_3.append(_generate_2);
      _builder_3.append(")");
      buffer.append(_builder_3);
      this.comma = ";";
      this.jump = "\n";
      Block _bodyBlock = simpleForBlock.getBodyBlock();
      if ((_bodyBlock instanceof CompositeBlock)) {
        StringConcatenation _builder_4 = new StringConcatenation();
        _builder_4.append(" ");
        buffer.append(_builder_4);
        StringConcatenation _builder_5 = new StringConcatenation();
        String _generate_3 = ExtendableBlockCGenerator.eInstance.generate(simpleForBlock.getBodyBlock());
        _builder_5.append(_generate_3);
        buffer.append(_builder_5);
      } else {
        if (((simpleForBlock.getBodyBlock() instanceof BasicBlock) && (((BasicBlock) simpleForBlock.getBodyBlock()).getInstructionCount() == 1))) {
          StringConcatenation _builder_6 = new StringConcatenation();
          _builder_6.newLine();
          _builder_6.append("\t");
          String _generate_4 = ExtendableBlockCGenerator.eInstance.generate(simpleForBlock.getBodyBlock());
          _builder_6.append(_generate_4, "\t");
          buffer.append(_builder_6);
        } else {
          Block _bodyBlock_1 = simpleForBlock.getBodyBlock();
          boolean _tripleNotEquals = (_bodyBlock_1 != null);
          if (_tripleNotEquals) {
            StringConcatenation _builder_7 = new StringConcatenation();
            _builder_7.append(" ");
            _builder_7.append("{");
            _builder_7.newLine();
            buffer.append(_builder_7);
            StringConcatenation _builder_8 = new StringConcatenation();
            _builder_8.append("\t");
            String _generate_5 = ExtendableBlockCGenerator.eInstance.generate(simpleForBlock.getBodyBlock());
            _builder_8.append(_generate_5, "\t");
            buffer.append(_builder_8);
            StringConcatenation _builder_9 = new StringConcatenation();
            _builder_9.append("}");
            buffer.append(_builder_9);
          } else {
            buffer.append("/* null body block */;");
          }
        }
      }
      StringConcatenation _builder_10 = new StringConcatenation();
      _builder_10.append(buffer);
      _builder_10.newLineIfNotEmpty();
      _xblockexpression = _builder_10;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final SwitchBlock switchBlock) {
    CharSequence _xblockexpression = null;
    {
      this.comma = "";
      this.jump = "";
      final EMap<Long, Block> oldCaseMaps = this.caseMaps;
      final Block oldDefaultBlock = this.defaultBlock;
      final StringBuffer buffer = new StringBuffer();
      this.caseMaps = switchBlock.getCases();
      this.defaultBlock = switchBlock.getDefaultBlock();
      StringConcatenation _builder = new StringConcatenation();
      CharSequence _generateAnnotations = this.generateAnnotations(switchBlock);
      _builder.append(_generateAnnotations);
      _builder.append("switch (");
      String _generate = ExtendableBlockCGenerator.eInstance.generate(switchBlock.getDispatchBlock());
      _builder.append(_generate);
      _builder.append(")");
      buffer.append(_builder);
      StringConcatenation _builder_1 = new StringConcatenation();
      String _generate_1 = ExtendableBlockCGenerator.eInstance.generate(switchBlock.getBodyBlock());
      _builder_1.append(_generate_1);
      _builder_1.newLineIfNotEmpty();
      buffer.append(_builder_1);
      this.caseMaps = oldCaseMaps;
      this.defaultBlock = oldDefaultBlock;
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append(buffer);
      _xblockexpression = _builder_2;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final WhileBlock whileBlock) {
    CharSequence _xblockexpression = null;
    {
      this.comma = "";
      this.jump = "";
      final StringBuffer buffer = new StringBuffer();
      StringConcatenation _builder = new StringConcatenation();
      CharSequence _generateAnnotations = this.generateAnnotations(whileBlock);
      _builder.append(_generateAnnotations);
      _builder.append("while (");
      buffer.append(_builder);
      Block _testBlock = whileBlock.getTestBlock();
      boolean _tripleEquals = (_testBlock == null);
      if (_tripleEquals) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("1 /* warning: test block is null */");
        buffer.append(_builder_1);
      } else {
        StringConcatenation _builder_2 = new StringConcatenation();
        String _generate = ExtendableBlockCGenerator.eInstance.generate(whileBlock.getTestBlock());
        _builder_2.append(_generate);
        buffer.append(_builder_2);
      }
      StringConcatenation _builder_3 = new StringConcatenation();
      _builder_3.append(")");
      buffer.append(_builder_3);
      this.comma = ";";
      this.jump = "\n";
      if (((whileBlock.getBodyBlock() instanceof BasicBlock) && (((BasicBlock) whileBlock.getBodyBlock()).getInstructionCount() > 1))) {
        StringConcatenation _builder_4 = new StringConcatenation();
        _builder_4.append(" ");
        _builder_4.append("{");
        _builder_4.newLine();
        buffer.append(_builder_4);
        StringConcatenation _builder_5 = new StringConcatenation();
        _builder_5.append("\t");
        String _generate_1 = ExtendableBlockCGenerator.eInstance.generate(whileBlock.getBodyBlock());
        _builder_5.append(_generate_1, "\t");
        buffer.append(_builder_5);
        StringConcatenation _builder_6 = new StringConcatenation();
        _builder_6.append("}");
        buffer.append(_builder_6);
      } else {
        boolean _not = (!((whileBlock.getBodyBlock() instanceof BasicBlock) && (((BasicBlock) whileBlock.getBodyBlock()).getInstructions().size() == 0)));
        if (_not) {
          StringConcatenation _builder_7 = new StringConcatenation();
          _builder_7.append(" ");
          buffer.append(_builder_7);
          StringConcatenation _builder_8 = new StringConcatenation();
          String _generate_2 = ExtendableBlockCGenerator.eInstance.generate(whileBlock.getBodyBlock());
          _builder_8.append(_generate_2);
          _builder_8.newLineIfNotEmpty();
          buffer.append(_builder_8);
        } else {
          StringConcatenation _builder_9 = new StringConcatenation();
          _builder_9.append(";");
          _builder_9.newLine();
          buffer.append(_builder_9);
        }
      }
      StringConcatenation _builder_10 = new StringConcatenation();
      _builder_10.append(buffer);
      _xblockexpression = _builder_10;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final EObject object) {
    return null;
  }
  
  public CharSequence printCase(final Block block) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      if (((this.caseMaps != null) && this.caseMaps.containsValue(block))) {
        Set<Long> _keySet = this.caseMaps.keySet();
        for (final Long l : _keySet) {
          Block _get = this.caseMaps.get(l);
          boolean _equals = Objects.equal(block, _get);
          if (_equals) {
            StringConcatenation _builder = new StringConcatenation();
            _builder.append("case ");
            _builder.append(l);
            _builder.append(" :");
            _builder.newLineIfNotEmpty();
            buffer.append(_builder);
          }
        }
      }
      if (((this.defaultBlock != null) && Objects.equal(block, this.defaultBlock))) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("\t");
        _builder_1.append("default :");
        _builder_1.newLine();
        buffer.append(_builder_1);
      }
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append(buffer);
      _xblockexpression = _builder_2;
    }
    return _xblockexpression;
  }
  
  public CharSequence generateAnnotations(final AnnotatedElement annotatedElement) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer res = new StringBuffer();
      Collection<IAnnotation> _values = annotatedElement.getAnnotations().values();
      for (final IAnnotation a : _values) {
        if ((a instanceof CommentAnnotation)) {
          String _content = ((CommentAnnotation) a).getContent();
          String _plus = ("/* " + _content);
          String _plus_1 = (_plus + " */\n");
          res.append(_plus_1);
        }
      }
      if (((annotatedElement.getPragma() != null) && (!annotatedElement.getPragma().getContent().isEmpty()))) {
        res.append(ExtendableAnnotationCGenerator.eInstance.generate(annotatedElement.getPragma()));
      }
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(res);
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  public StringBuffer printForScope(final Scope scope) {
    StringBuffer _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      EList<Symbol> _symbols = scope.getSymbols();
      for (final Symbol symbol : _symbols) {
        {
          boolean _equals = symbol.equals(scope.getSymbols().get(0));
          if (_equals) {
            StringConcatenation _builder = new StringConcatenation();
            String _generate = ExtendableTypeCGenerator.eInstance.generate(symbol.getType());
            _builder.append(_generate);
            buffer.append(_builder);
          }
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append(" ");
          String _name = symbol.getName();
          _builder_1.append(_name, " ");
          buffer.append(_builder_1);
          Instruction _value = symbol.getValue();
          boolean _tripleNotEquals = (_value != null);
          if (_tripleNotEquals) {
            StringConcatenation _builder_2 = new StringConcatenation();
            _builder_2.append(" ");
            _builder_2.append("= ");
            String _generate_1 = ExtendableInstructionCGenerator.eInstance.generate(symbol.getValue());
            _builder_2.append(_generate_1, " ");
            buffer.append(_builder_2);
          }
          EList<Symbol> _symbols_1 = scope.getSymbols();
          int _size = scope.getSymbols().size();
          int _minus = (_size - 1);
          boolean _equals_1 = symbol.equals(_symbols_1.get(_minus));
          boolean _not = (!_equals_1);
          if (_not) {
            StringConcatenation _builder_3 = new StringConcatenation();
            _builder_3.append(",");
            buffer.append(_builder_3);
          }
        }
      }
      StringConcatenation _builder = new StringConcatenation();
      _xblockexpression = buffer.append(_builder);
    }
    return _xblockexpression;
  }
  
  public Object generate(final EObject simpleForBlock) {
    if (simpleForBlock instanceof SimpleForBlock) {
      return _generate((SimpleForBlock)simpleForBlock);
    } else if (simpleForBlock instanceof ForC99Block) {
      return _generate((ForC99Block)simpleForBlock);
    } else if (simpleForBlock instanceof BasicBlock) {
      return _generate((BasicBlock)simpleForBlock);
    } else if (simpleForBlock instanceof CompositeBlock) {
      return _generate((CompositeBlock)simpleForBlock);
    } else if (simpleForBlock instanceof DoWhileBlock) {
      return _generate((DoWhileBlock)simpleForBlock);
    } else if (simpleForBlock instanceof ForBlock) {
      return _generate((ForBlock)simpleForBlock);
    } else if (simpleForBlock instanceof IfBlock) {
      return _generate((IfBlock)simpleForBlock);
    } else if (simpleForBlock instanceof SwitchBlock) {
      return _generate((SwitchBlock)simpleForBlock);
    } else if (simpleForBlock instanceof WhileBlock) {
      return _generate((WhileBlock)simpleForBlock);
    } else if (simpleForBlock != null) {
      return _generate(simpleForBlock);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(simpleForBlock).toString());
    }
  }
}
