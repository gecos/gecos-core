#ifndef _STDLIB_H
#define _STDLIB_H

#include <stddef.h>

int atoi(const char *);
double atof(const char *);
long atol(const char *);
void abort(void);
void exit(int code);
void system(const char *command);
#define	RAND_MAX	2147483647
void* malloc(int);
char *realloc (char *p, int n);
long random(void);
long rand(void);
int free(void *ptr);
void srandom(unsigned long seed);
void *calloc(unsigned long nelem, unsigned long elsize);
char *mktemp(char *templat);

//void qsort (void * base, int num, int size, int ( * comparator ) ( const void *, const void * ) );

unsigned long strtoul(const char *str, char **endptr, int base);
unsigned long long strtoull(const char *str, char **endptr, int base);


#endif



