
debug(2);

projectname = "example";

#initialize project
project = CreateGecosProject(projectname);

#add files to the projects (sourdes and includes)
AddSourceToGecosProject(project,"./src/toto.c");
AddIncDirToGecosProject(project,"./src/");
AddIncDirToGecosProject(project,"./include/");

echo("Parse ...");
CDTFrontend(project);

echo("Copy ...");
proj2 = CopyGecosProject(project);
#ComputeSSAForm(proj2);
#RemoveSSAForm(proj2);

echo("Test ...");
TesterModule({project, proj2}, "AUTO", "STDOUT", true, "./test-regen");