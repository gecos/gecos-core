p = CreateGecosProject("Test"); # Create a empty GeCoS project
AddSourceToGecosProject(p, "src-c/cgenerator_example.c"); # Add a new C source in the project

CDTFrontend(p);  # Generate the GeCoS IR from the C source added
ReplaceWithFoo(p); # Call the script command which correspond to our GeCoS module
output(p, "c", "output"); # Generate the C source code from the GeCoS IR