int add(int X, int Y) {
	return X + Y;
}

typedef int (*op_t)(int, int);

float exec(op_t operation, float x, float y) {
	return operation(x,y);
}

int main() {

	op_t pF = add;


	//-- test --
	if(exec(pF, 3,7) != add(3,7))
		exit(-2);

	return 0;
}
