package fr.irisa.cairn.gecos.model.analysis.dataflow.aliasing;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import fr.irisa.cairn.gecos.model.tools.switches.DefaultInstructionSwitch;
import gecos.blocks.BasicBlock;
import gecos.core.ProcedureSet;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;

public class FindContextAndFlowInsentitiveAliasing extends BlockInstructionSwitch<Object> {

	GecosProject ps;
	List<Symbol> aliasList;

	public FindContextAndFlowInsentitiveAliasing(GecosProject ps) {
		this.ps=ps;
		aliasList = new ArrayList<Symbol>() ;
	}

	public void compute () {
		for(ProcedureSet pset : ps.listProcedureSets()) {
			doSwitch(pset);
		}
		System.err.println(aliasList.toString());
	}


	private class FindLValueSymbol extends DefaultInstructionSwitch<Object> {

		Instruction inst;

		public FindLValueSymbol(Instruction inst) {
			this.inst=inst;
		}

		public Symbol compute() {
			Object result = doSwitch(inst);
			if(result!=null) {
				return (Symbol) result;
			} 
			return null;
		}

		@Override
		public Object caseSymbolInstruction(SymbolInstruction s) {
//			if ((hasIndirection==0) && (s.getType().isPointer() || s.getType().isArray())) {
//				throw new RuntimeException("Symbol "+s.getSymbol()+" is not an LValue symbol in "+inst);
//			}
			System.err.println("Found aliasing on "+s.getSymbol());
			return s.getSymbol();
		}

		@Override
		public Object caseArrayInstruction(ArrayInstruction object) {
			Object doSwitch = doSwitch(object.getDest());
			return doSwitch;
		}

		@Override
		public Object caseIndirInstruction(IndirInstruction g) {
			Object doSwitch = null;
			if(g.getType()!=null && g.getType().asPointer() != null) {
				doSwitch = doSwitch(g.getAddress());
			}
			return doSwitch;
		}

		@Override
		public Object caseSetInstruction(SetInstruction s) {
			if(s.getType()!=null && s.getType().asPointer() != null) {
				return doSwitch(s.getSource());
			}
			return true;
		}

		@Override
		public Object caseComplexInstruction(ComplexInstruction object) {
			Object last=null;
			if(object.getType()!=null && object.getType().asPointer() != null) {
				for (Instruction inst : object.listChildren()) {
					if(inst.getType()!=null && inst.getType().asPointer() != null) {
						System.err.println("visiting "+inst);
						last = doSwitch(inst);
					}
				}
			}
			return last;
		}
	}

	@Override
	public Object caseAddressInstruction(AddressInstruction g) {
		System.err.println("Found "+g);
		FindLValueSymbol lvaluefinder = new FindLValueSymbol(g.getAddress());
		aliasList.add(lvaluefinder.compute());
		return true;
	}

	@Override
	public Object caseSetInstruction(SetInstruction g) {
		System.err.println("Found "+g);
		Instruction source = g.getSource();
		if(source!=null && source.getType()!=null && source.getType().asPointer() != null) {
			System.err.println("Possible aliasing");
			FindLValueSymbol lvaluefinder = new FindLValueSymbol(g.getSource());
			aliasList.add(lvaluefinder.compute());
		}
		return true;
	}

	@Override
	public Object caseBasicBlock(BasicBlock b) {
		Instruction[] insts = (Instruction[]) b.getInstructions().toArray();
		for (Instruction inst  : insts) {
			doSwitch(inst);
		}
		return true;
	}
	
	
}
