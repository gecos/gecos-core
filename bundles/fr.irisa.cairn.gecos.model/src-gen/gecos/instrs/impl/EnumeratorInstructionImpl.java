/**
 */
package gecos.instrs.impl;

import gecos.instrs.EnumeratorInstruction;
import gecos.instrs.InstrsPackage;
import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;

import gecos.types.Enumerator;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Enumerator Instruction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.impl.EnumeratorInstructionImpl#getEnumerator <em>Enumerator</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EnumeratorInstructionImpl extends InstructionImpl implements EnumeratorInstruction {
	/**
	 * The cached value of the '{@link #getEnumerator() <em>Enumerator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnumerator()
	 * @generated
	 * @ordered
	 */
	protected Enumerator enumerator;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EnumeratorInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstrsPackage.eINSTANCE.getEnumeratorInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Enumerator getEnumerator() {
		return enumerator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnumerator(Enumerator newEnumerator) {
		Enumerator oldEnumerator = enumerator;
		enumerator = newEnumerator;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InstrsPackage.ENUMERATOR_INSTRUCTION__ENUMERATOR, oldEnumerator, enumerator));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final InstrsVisitor visitor) {
		visitor.visitEnumeratorInstruction(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSame(final Instruction instr) {
		if ((instr instanceof EnumeratorInstruction)) {
			boolean isSame = false;
			if (((((EnumeratorInstruction)instr).getEnumerator() == null) && (this.getEnumerator() == null))) {
				isSame = true;
			}
			else {
				Enumerator _enumerator = ((EnumeratorInstruction)instr).getEnumerator();
				boolean _tripleEquals = (_enumerator == null);
				Enumerator _enumerator_1 = this.getEnumerator();
				boolean _tripleEquals_1 = (_enumerator_1 == null);
				boolean _xor = (_tripleEquals ^ _tripleEquals_1);
				if (_xor) {
					return false;
				}
				else {
					isSame = ((EnumeratorInstruction)instr).getEnumerator().isEqual(this.getEnumerator());
				}
			}
			return (isSame && super.isSame(instr));
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InstrsPackage.ENUMERATOR_INSTRUCTION__ENUMERATOR:
				return getEnumerator();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InstrsPackage.ENUMERATOR_INSTRUCTION__ENUMERATOR:
				setEnumerator((Enumerator)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InstrsPackage.ENUMERATOR_INSTRUCTION__ENUMERATOR:
				setEnumerator((Enumerator)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InstrsPackage.ENUMERATOR_INSTRUCTION__ENUMERATOR:
				return enumerator != null;
		}
		return super.eIsSet(featureID);
	}

} //EnumeratorInstructionImpl
