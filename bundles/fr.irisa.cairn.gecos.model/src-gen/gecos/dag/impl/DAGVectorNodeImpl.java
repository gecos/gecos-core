/**
 */
package gecos.dag.impl;

import gecos.dag.DAGVectorNode;
import gecos.dag.DagPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DAG Vector Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class DAGVectorNodeImpl extends DAGNodeImpl implements DAGVectorNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DAGVectorNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DagPackage.Literals.DAG_VECTOR_NODE;
	}

} //DAGVectorNodeImpl
