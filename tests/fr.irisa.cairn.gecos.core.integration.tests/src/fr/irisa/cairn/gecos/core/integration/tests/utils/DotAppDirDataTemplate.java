package fr.irisa.cairn.gecos.core.integration.tests.utils;

import fr.irisa.cairn.gecos.core.testframework.impl.GSProjectVersion;
import fr.irisa.cairn.gecos.core.testframework.impl.GecosS2STestTemplate;

public class DotAppDirDataTemplate extends GecosS2STestTemplate<GSProjectVersion> {
	
	@Override
	protected void configure() {
		super.configure();
		
		registerTestFlow(DotAppDirData.class, DotAppDirData.defaultDotAppDirDataTestFlow(
				DotAppDirData::defaultMakeCompileTarget, DotAppDirData::defaultMakeRunTarget));
	}

}
