

typedef struct s1_t s1;
typedef struct s2_t s2;
struct s2_t {
	s1* a;
	s2* b;
};
struct s1_t {
	s1* a;
	s2* b;
};






typedef int aaa, bbb, ccc;
aaa     int1;
bbb     int2;
ccc     int3;

typedef struct _matrix{
	int x;
	int y;
} *Matrix;

// GCC supports declaring typedefs before the struct declaration
typedef struct _Coordonnees Coordonnees;

struct _Coordonnees
{
    int x;
    int y;
    Coordonnees * toto;
};
union Example
{
    short a;
    long b;
    float c;
};

struct recursive_struct {
	int c;
	struct recursive_struct* next;
};

struct Personne
{
    char nom[100];
    char prenom[100];
    char adresse[1000];
};

enum Volume
{
	FAIBLE = 10, MOYEN = 50, FORT = 100, UNKNOWN
};

void initCoordonnees(Coordonnees *point, int x, int y) {
	point->x = x;
	point->y = y;
}

int main() {
	Coordonnees point = {0,0};
	struct Personne personne;
	enum Volume volume = FAIBLE;
	union Example example;
	initCoordonnees(&point, 1, 1);
	personne.nom[0] = 'T';
	personne.nom[1] = 'o';
	personne.nom[2] = 't';
	personne.nom[3] = 'o';
	example.c = 1.0;
	volume = MOYEN;
	volume = MOYEN;
	return 0;
}


int SPMaddressINT[4][5];
int main();


int main() {
	int(* a)[5] = (int(*)[5])SPMaddressINT;

}


#include <stdio.h>

int tt = 25;
int fooo(int a[tt]);

int fooo(int t[tt]) {
	return t[0];
}


#pragma GCS_PURE_FUNCTION
void koub(int n);

#pragma GCS_PURE_FUNCTION
void koub(int n) {
	int a = 1;
	a = 2;
}



void toto() {
	int nb;
	int tab[nb][nb];
	int t[nb][nb][tab[0][nb-1]];
}

typedef int int32;

int toto2(int32 a) {
	return a;
}


////Legal syntax not managed
void test(int nb, int tab[][nb]);
void main(int a) {
	int nb = 5;
	int c;
	int tab[nb][nb];
	test(nb, tab);
	c = nb + 2 + a;
}

////Illegal syntax managed
//void test2(int nb, int tab[][]);
//void main2() {
//	int nb = 5;
//	int tab[nb][nb];
//	test2(nb, tab);
//}



void test3(int nb, int tab[][nb], int t[][nb][tab[0][nb-1]]);



void test_for_if(int N, int a[N][N], int b[N][N], int c[N][N]) {
	int i, j, k;
	k = 5;
	for (i = 0; i < N; ++i) {
		if(k>2){
			k=0;
			for (j = 0; j < N; ++j) {
				k = k + 2;
			}
		}
	}

}

// sobel 7x7
// 3 2 1  0 -1 -2 -3
// 4 3 2  0 -2 -3 -4
// 5 4 3  0 -3 -4 -5
// 6 5 4  0 -4 -5 -6
// 5 4 3  0 -3 -4 -5
// 4 3 2  0 -2 -3 -4
// 3 2 1  0 -1 -2 -3
void sobel7x7(int W, int im_in[W][W], int im_out[W][W], int sobel[W][W]) {
	int i, j, ii,jj;

//	int sobel[][] = {
//			{3,2,1,0},
//			{4,3,2,0},
//			{5,4,3,0},
//			{6,5,4,0}};

	for (i = (7/2); i < W-(7/2); ++i) {
		for (j = (7/2); j < W-(7/2); ++j) {
			int tmp=0;
			for ( ii = 1; ii < (7/2); ++ii) {
				for (jj = 1; jj < (7/2); ++jj) {
					tmp += (im_in[i-ii][j-jj]+
							im_in[i-ii][j+jj]-
							im_in[i+ii][j-jj]-
							im_in[i+ii][j+jj] )*sobel[ii][jj];
				}
			}
			im_out[i][j]=tmp+im_in[i][j]*sobel[0][0];
		}
	}
}


void foo3(int a,int b) {
	int c;
	c = a + 2;
	c *= b;
	return;
}

