#define N 2
int foobar() {
	int i,j;
	int t[N][N];
	#pragma gcs_unroll
	for (i = 0; i < N; i++)
		#pragma gcs_unroll
		for (j = i; j < N; j++)
			t[i][j] = i == j ? 1 : 0;
	return 0;
}
