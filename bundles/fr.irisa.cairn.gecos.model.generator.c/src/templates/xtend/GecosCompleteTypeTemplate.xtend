package templates.xtend

import fr.irisa.cairn.gecos.model.c.generator.ExtendableCompleteTypeCGenerator
import fr.irisa.cairn.gecos.model.c.generator.ExtendableInstructionCGenerator
import fr.irisa.cairn.gecos.model.c.generator.ExtendableTypeCGenerator
import gecos.types.AliasType
import gecos.types.ArrayType
import gecos.types.EnumType
import gecos.types.Enumerator
import gecos.types.Field
import gecos.types.FunctionType
import gecos.types.Kinds
import gecos.types.RecordType

class GecosCompleteTypeTemplate extends GecosTypeTemplate {

	override dispatch generate(AliasType aliasType) {
		val generated = new StringBuffer()
		generated.append(ExtendableTypeCGenerator::eInstance.generate(aliasType.getAlias)+" "+aliasType.name)
		generated.append(afterSymbolInformation)
		'''typedef «generated.toString»'''//*/
	}
	
	override dispatch generate(ArrayType arrayType) {
		val generated = ExtendableCompleteTypeCGenerator::eInstance.generate(arrayType.base)
		val value =  ExtendableInstructionCGenerator::eInstance.generate(arrayType.sizeExpr)//(arrayType.upper.intValue - arrayType.lower.intValue + 1)
		if (value == 0)
			afterSymbolInformation.append("[]")
		else
			afterSymbolInformation.append("[" + value + "]")
		'''«generated»'''
	}

	override dispatch generate(Enumerator enumerator) {
		if (enumerator.expression === null)
			'''«enumerator.name»'''
		else
			'''«enumerator.name» = «ExtendableInstructionCGenerator::eInstance.generate(enumerator.expression)»'''
	}
	
	override dispatch generate(EnumType enumType) {
		val buffer = new StringBuffer()
		buffer.append('''enum «enumType.name» {
		''')
		for(Enumerator enumerator : enumType.enumerators) {
			if (enumerator.equals(enumType.enumerators.get(enumType.enumerators.size - 1)))
				buffer.append('''	«ExtendableCompleteTypeCGenerator::eInstance.generate(enumerator)»
				''')
			else
				buffer.append('''	«ExtendableCompleteTypeCGenerator::eInstance.generate(enumerator)»,
				''')
		}
		buffer.append('''}''')
		'''«buffer»'''
	}
	
	override dispatch generate(Field field) {
		var StringBuffer buffer = new StringBuffer();
		if (field.bitwidth != -1) 
			buffer.append(" : "+field.bitwidth)
		'''«ExtendableTypeCGenerator::eInstance.generate(field.type)» «field.name»«afterSymbolInformation.toString»«buffer»'''
	}
	
	override dispatch generate(FunctionType functionType) {
		val StringBuffer buffer = new StringBuffer
		buffer.append(afterSymbolInformation)
		buffer.append("(")
		var i = 0
		while(i < (functionType.listParameters.size-1)) {
			afterSymbolInformation.setLength(0)
			buffer.append('''«ExtendableCompleteTypeCGenerator::eInstance.generate(functionType.listParameters.get(i))», ''')
			buffer.append(afterSymbolInformation)
			i = i + 1
		}
		if (i < functionType.listParameters.size) {
			afterSymbolInformation.setLength(0)
			buffer.append('''«ExtendableCompleteTypeCGenerator::eInstance.generate(functionType.listParameters.get(i))»''')
			buffer.append(afterSymbolInformation)
		}
		buffer.append(")")
		afterSymbolInformation.setLength(0)
		afterSymbolInformation.append(buffer)
		'''«ExtendableCompleteTypeCGenerator::eInstance.generate(functionType.returnType)»'''
	}
	
	override dispatch generate(RecordType recordType) {
		val buffer = new StringBuffer()
		if (recordType.kind == Kinds::UNION)
			buffer.append('''union ''')
		else
			buffer.append('''struct ''')
		buffer.append('''«recordType.name» {
		''')
		for(Field field : recordType.fields) {
			afterSymbolInformation.setLength(0)
			buffer.append('''	«ExtendableCompleteTypeCGenerator::eInstance.generate(field)»;
			''');
			afterSymbolInformation.setLength(0)
		}
		buffer.append('''}''')
		'''«buffer»'''
	}
}