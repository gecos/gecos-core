package fr.irisa.cairn.gecos.core.ui.examples;

import org.eclipse.core.runtime.Platform;
import org.eclipse.ui.INewWizard;
import org.osgi.framework.Bundle;

import fr.irisa.cairn.gecos.core.ui.Activator;
import fr.irisa.cairn.gecos.core.ui.examples.utils.BasicGecosExampleWizard;

public class GenerateCallGraph extends BasicGecosExampleWizard implements INewWizard {
	@Override
	public String getResourcesFolderToCopyIntoProject() {
		return "resources/wizard_templates/generate_call_graph/";
	}

	@Override
	public String getPageName() {
		return "Generating application call graph example";
	}

	@Override
	public String getPageDescription() {
		return "This wizard will create a example project for generating application call graph from GeCoS IR";
	}

	@Override
	protected Bundle getPluginBundle() {
		return Platform.getBundle(Activator.PLUGIN_ID);
	}

}
