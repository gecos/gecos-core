/**
 */
package gecos.types.util;

import gecos.annotations.AnnotatedElement;

import gecos.core.GecosNode;
import gecos.core.ITypedElement;

import gecos.types.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see gecos.types.TypesPackage
 * @generated
 */
public class TypesSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TypesPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypesSwitch() {
		if (modelPackage == null) {
			modelPackage = TypesPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case TypesPackage.TYPES_VISITABLE: {
				TypesVisitable typesVisitable = (TypesVisitable)theEObject;
				T result = caseTypesVisitable(typesVisitable);
				if (result == null) result = caseGecosNode(typesVisitable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.FIELD: {
				Field field = (Field)theEObject;
				T result = caseField(field);
				if (result == null) result = caseTypesVisitable(field);
				if (result == null) result = caseITypedElement(field);
				if (result == null) result = caseGecosNode(field);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.ENUMERATOR: {
				Enumerator enumerator = (Enumerator)theEObject;
				T result = caseEnumerator(enumerator);
				if (result == null) result = caseTypesVisitable(enumerator);
				if (result == null) result = caseGecosNode(enumerator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.TYPE: {
				Type type = (Type)theEObject;
				T result = caseType(type);
				if (result == null) result = caseAnnotatedElement(type);
				if (result == null) result = caseTypesVisitable(type);
				if (result == null) result = caseGecosNode(type);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.ARRAY_TYPE: {
				ArrayType arrayType = (ArrayType)theEObject;
				T result = caseArrayType(arrayType);
				if (result == null) result = caseType(arrayType);
				if (result == null) result = caseAnnotatedElement(arrayType);
				if (result == null) result = caseTypesVisitable(arrayType);
				if (result == null) result = caseGecosNode(arrayType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.PTR_TYPE: {
				PtrType ptrType = (PtrType)theEObject;
				T result = casePtrType(ptrType);
				if (result == null) result = caseType(ptrType);
				if (result == null) result = caseAnnotatedElement(ptrType);
				if (result == null) result = caseTypesVisitable(ptrType);
				if (result == null) result = caseGecosNode(ptrType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage._INTERNAL_PARAMETER_TYPE_WRAPPER: {
				__Internal_ParameterTypeWrapper __Internal_ParameterTypeWrapper = (__Internal_ParameterTypeWrapper)theEObject;
				T result = case__Internal_ParameterTypeWrapper(__Internal_ParameterTypeWrapper);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.FUNCTION_TYPE: {
				FunctionType functionType = (FunctionType)theEObject;
				T result = caseFunctionType(functionType);
				if (result == null) result = caseType(functionType);
				if (result == null) result = caseAnnotatedElement(functionType);
				if (result == null) result = caseTypesVisitable(functionType);
				if (result == null) result = caseGecosNode(functionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.RECORD_TYPE: {
				RecordType recordType = (RecordType)theEObject;
				T result = caseRecordType(recordType);
				if (result == null) result = caseType(recordType);
				if (result == null) result = caseAnnotatedElement(recordType);
				if (result == null) result = caseTypesVisitable(recordType);
				if (result == null) result = caseGecosNode(recordType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.ALIAS_TYPE: {
				AliasType aliasType = (AliasType)theEObject;
				T result = caseAliasType(aliasType);
				if (result == null) result = caseType(aliasType);
				if (result == null) result = caseITypedElement(aliasType);
				if (result == null) result = caseAnnotatedElement(aliasType);
				if (result == null) result = caseTypesVisitable(aliasType);
				if (result == null) result = caseGecosNode(aliasType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.ENUM_TYPE: {
				EnumType enumType = (EnumType)theEObject;
				T result = caseEnumType(enumType);
				if (result == null) result = caseType(enumType);
				if (result == null) result = caseAnnotatedElement(enumType);
				if (result == null) result = caseTypesVisitable(enumType);
				if (result == null) result = caseGecosNode(enumType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.SIMD_TYPE: {
				SimdType simdType = (SimdType)theEObject;
				T result = caseSimdType(simdType);
				if (result == null) result = caseType(simdType);
				if (result == null) result = caseAnnotatedElement(simdType);
				if (result == null) result = caseTypesVisitable(simdType);
				if (result == null) result = caseGecosNode(simdType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.UNDEFINED_TYPE: {
				UndefinedType undefinedType = (UndefinedType)theEObject;
				T result = caseUndefinedType(undefinedType);
				if (result == null) result = caseType(undefinedType);
				if (result == null) result = caseAnnotatedElement(undefinedType);
				if (result == null) result = caseTypesVisitable(undefinedType);
				if (result == null) result = caseGecosNode(undefinedType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.SCALAR_TYPE: {
				ScalarType scalarType = (ScalarType)theEObject;
				T result = caseScalarType(scalarType);
				if (result == null) result = caseType(scalarType);
				if (result == null) result = caseAnnotatedElement(scalarType);
				if (result == null) result = caseTypesVisitable(scalarType);
				if (result == null) result = caseGecosNode(scalarType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.BASE_TYPE: {
				BaseType baseType = (BaseType)theEObject;
				T result = caseBaseType(baseType);
				if (result == null) result = caseScalarType(baseType);
				if (result == null) result = caseType(baseType);
				if (result == null) result = caseAnnotatedElement(baseType);
				if (result == null) result = caseTypesVisitable(baseType);
				if (result == null) result = caseGecosNode(baseType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.INTEGER_TYPE: {
				IntegerType integerType = (IntegerType)theEObject;
				T result = caseIntegerType(integerType);
				if (result == null) result = caseBaseType(integerType);
				if (result == null) result = caseScalarType(integerType);
				if (result == null) result = caseType(integerType);
				if (result == null) result = caseAnnotatedElement(integerType);
				if (result == null) result = caseTypesVisitable(integerType);
				if (result == null) result = caseGecosNode(integerType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.FLOAT_TYPE: {
				FloatType floatType = (FloatType)theEObject;
				T result = caseFloatType(floatType);
				if (result == null) result = caseBaseType(floatType);
				if (result == null) result = caseScalarType(floatType);
				if (result == null) result = caseType(floatType);
				if (result == null) result = caseAnnotatedElement(floatType);
				if (result == null) result = caseTypesVisitable(floatType);
				if (result == null) result = caseGecosNode(floatType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.BOOL_TYPE: {
				BoolType boolType = (BoolType)theEObject;
				T result = caseBoolType(boolType);
				if (result == null) result = caseBaseType(boolType);
				if (result == null) result = caseScalarType(boolType);
				if (result == null) result = caseType(boolType);
				if (result == null) result = caseAnnotatedElement(boolType);
				if (result == null) result = caseTypesVisitable(boolType);
				if (result == null) result = caseGecosNode(boolType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.VOID_TYPE: {
				VoidType voidType = (VoidType)theEObject;
				T result = caseVoidType(voidType);
				if (result == null) result = caseBaseType(voidType);
				if (result == null) result = caseScalarType(voidType);
				if (result == null) result = caseType(voidType);
				if (result == null) result = caseAnnotatedElement(voidType);
				if (result == null) result = caseTypesVisitable(voidType);
				if (result == null) result = caseGecosNode(voidType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.AC_TYPE: {
				ACType acType = (ACType)theEObject;
				T result = caseACType(acType);
				if (result == null) result = caseBaseType(acType);
				if (result == null) result = caseScalarType(acType);
				if (result == null) result = caseType(acType);
				if (result == null) result = caseAnnotatedElement(acType);
				if (result == null) result = caseTypesVisitable(acType);
				if (result == null) result = caseGecosNode(acType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.AC_INT_TYPE: {
				ACIntType acIntType = (ACIntType)theEObject;
				T result = caseACIntType(acIntType);
				if (result == null) result = caseArrayType(acIntType);
				if (result == null) result = caseACType(acIntType);
				if (result == null) result = caseBaseType(acIntType);
				if (result == null) result = caseAnnotatedElement(acIntType);
				if (result == null) result = caseScalarType(acIntType);
				if (result == null) result = caseType(acIntType);
				if (result == null) result = caseGecosNode(acIntType);
				if (result == null) result = caseTypesVisitable(acIntType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.AC_FIXED_TYPE: {
				ACFixedType acFixedType = (ACFixedType)theEObject;
				T result = caseACFixedType(acFixedType);
				if (result == null) result = caseArrayType(acFixedType);
				if (result == null) result = caseACType(acFixedType);
				if (result == null) result = caseBaseType(acFixedType);
				if (result == null) result = caseAnnotatedElement(acFixedType);
				if (result == null) result = caseScalarType(acFixedType);
				if (result == null) result = caseType(acFixedType);
				if (result == null) result = caseGecosNode(acFixedType);
				if (result == null) result = caseTypesVisitable(acFixedType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.AC_FLOAT_TYPE: {
				ACFloatType acFloatType = (ACFloatType)theEObject;
				T result = caseACFloatType(acFloatType);
				if (result == null) result = caseACType(acFloatType);
				if (result == null) result = caseBaseType(acFloatType);
				if (result == null) result = caseScalarType(acFloatType);
				if (result == null) result = caseType(acFloatType);
				if (result == null) result = caseAnnotatedElement(acFloatType);
				if (result == null) result = caseTypesVisitable(acFloatType);
				if (result == null) result = caseGecosNode(acFloatType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.AC_COMPLEX_TYPE: {
				ACComplexType acComplexType = (ACComplexType)theEObject;
				T result = caseACComplexType(acComplexType);
				if (result == null) result = caseACType(acComplexType);
				if (result == null) result = caseBaseType(acComplexType);
				if (result == null) result = caseScalarType(acComplexType);
				if (result == null) result = caseType(acComplexType);
				if (result == null) result = caseAnnotatedElement(acComplexType);
				if (result == null) result = caseTypesVisitable(acComplexType);
				if (result == null) result = caseGecosNode(acComplexType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.AC_CHANNEL_TYPE: {
				ACChannelType acChannelType = (ACChannelType)theEObject;
				T result = caseACChannelType(acChannelType);
				if (result == null) result = caseACType(acChannelType);
				if (result == null) result = caseBaseType(acChannelType);
				if (result == null) result = caseScalarType(acChannelType);
				if (result == null) result = caseType(acChannelType);
				if (result == null) result = caseAnnotatedElement(acChannelType);
				if (result == null) result = caseTypesVisitable(acChannelType);
				if (result == null) result = caseGecosNode(acChannelType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.TYPES_VISITOR: {
				TypesVisitor typesVisitor = (TypesVisitor)theEObject;
				T result = caseTypesVisitor(typesVisitor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypesVisitable(TypesVisitable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Field</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Field</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseField(Field object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enumerator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enumerator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnumerator(Enumerator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseType(Type object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Array Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Array Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArrayType(ArrayType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ptr Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ptr Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePtrType(PtrType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Internal Parameter Type Wrapper</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Internal Parameter Type Wrapper</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T case__Internal_ParameterTypeWrapper(__Internal_ParameterTypeWrapper object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFunctionType(FunctionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Record Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Record Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRecordType(RecordType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Alias Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Alias Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAliasType(AliasType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enum Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enum Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnumType(EnumType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simd Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simd Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimdType(SimdType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Undefined Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Undefined Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUndefinedType(UndefinedType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Scalar Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Scalar Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScalarType(ScalarType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseType(BaseType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Integer Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Integer Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIntegerType(IntegerType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Float Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Float Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFloatType(FloatType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Bool Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bool Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBoolType(BoolType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Void Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Void Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVoidType(VoidType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AC Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AC Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseACType(ACType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AC Int Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AC Int Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseACIntType(ACIntType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AC Fixed Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AC Fixed Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseACFixedType(ACFixedType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AC Float Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AC Float Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseACFloatType(ACFloatType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AC Complex Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AC Complex Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseACComplexType(ACComplexType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AC Channel Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AC Channel Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseACChannelType(ACChannelType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visitor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visitor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypesVisitor(TypesVisitor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Gecos Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Gecos Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGecosNode(GecosNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ITyped Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ITyped Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseITypedElement(ITypedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotatedElement(AnnotatedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //TypesSwitch
