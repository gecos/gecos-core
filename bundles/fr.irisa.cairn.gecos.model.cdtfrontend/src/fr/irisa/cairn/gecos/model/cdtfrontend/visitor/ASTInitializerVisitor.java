/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.cdtfrontend.visitor;

import org.eclipse.cdt.core.dom.ast.IASTEqualsInitializer;
import org.eclipse.cdt.core.dom.ast.IASTInitializer;
import org.eclipse.cdt.core.dom.ast.IASTInitializerClause;
import org.eclipse.cdt.core.dom.ast.IASTInitializerList;
import org.eclipse.cdt.core.dom.ast.c.ICASTDesignatedInitializer;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTConstructorInitializer;

import fr.irisa.cairn.gecos.model.cdtfrontend.exception.CDTParserNotYetSupportedException;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import gecos.instrs.ArrayValueInstruction;
import gecos.instrs.Instruction;
import gecos.types.Type;

/**
 * @author mnaullet
 */
public class ASTInitializerVisitor {

	private CDTToGecosModel gecosModel;
	private Instruction instruction;

	public ASTInitializerVisitor() {
	}

	public void visit(CDTToGecosModel gecosModel, IASTInitializer initializer) {
		this.gecosModel = gecosModel;
		instruction = null;
		if (initializer instanceof IASTInitializerList)
			buildIASTInitializerList((IASTInitializerList)initializer);
		else if (initializer instanceof ICASTDesignatedInitializer)
			buildICASTDesignatedInitializer((ICASTDesignatedInitializer)initializer);
		else if (initializer instanceof ICPPASTConstructorInitializer)
			buildICPPASTConstructorInitializer((ICPPASTConstructorInitializer)initializer);
		else if (initializer instanceof IASTEqualsInitializer)
			buildIASTEqualsInitializer((IASTEqualsInitializer)initializer);
		
		if (!gecosModel.visitingInclude())		
			gecosModel.addIASTAnnotation(instruction, initializer);
		
		gecosModel.addFileLocationAnnotation(instruction, initializer.getFileLocation());
	}

	public Instruction getInstruction() {
		return instruction;
	}

	private void buildIASTEqualsInitializer(IASTEqualsInitializer initializer) {
		initializer.getInitializerClause().accept(gecosModel);
		if (gecosModel.getObject() instanceof ASTInitializerVisitor)
			instruction = ((ASTInitializerVisitor)gecosModel.getObject()).getInstruction();
		else if (gecosModel.getObject() instanceof ASTExpressionVisitor)
			instruction = ((ASTExpressionVisitor)gecosModel.getObject()).getInstruction();
	}

	private void buildIASTInitializerList(IASTInitializerList initializer) {
		ArrayValueInstruction arrayValueInstruction = GecosUserInstructionFactory.arrayValue();
		Type type = null;
		for (IASTInitializerClause initializerClause : initializer.getClauses()) {
			initializerClause.accept(gecosModel);
			if (gecosModel.getObject() instanceof ASTExpressionVisitor) {
				arrayValueInstruction.getChildren().add(((ASTExpressionVisitor)gecosModel.getObject()).getInstruction());
				type = ((ASTExpressionVisitor)gecosModel.getObject()).getInstruction().getType();
			}
			else if (gecosModel.getObject() instanceof ASTInitializerVisitor) {
				arrayValueInstruction.getChildren().add(((ASTInitializerVisitor)gecosModel.getObject()).getInstruction());	
				type = ((ASTInitializerVisitor)gecosModel.getObject()).getInstruction().getType();
			}
		}
		arrayValueInstruction.setType(type);
		instruction = arrayValueInstruction;
	}

	private void buildICPPASTConstructorInitializer(ICPPASTConstructorInitializer initializer) {
		notYetSupported(initializer);
	}
	
	private void buildICASTDesignatedInitializer(ICASTDesignatedInitializer initializer) {
		notYetSupported(initializer);
	}
	
	private static void notYetSupported(IASTInitializer initializer) {
		throw new CDTParserNotYetSupportedException(initializer, "Support for "+initializer.getClass().getSimpleName()+ ":" +initializer+ " not yet implemented ");
	}
	
}