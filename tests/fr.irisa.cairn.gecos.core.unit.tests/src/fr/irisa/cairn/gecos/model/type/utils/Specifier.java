package fr.irisa.cairn.gecos.model.type.utils;

public class Specifier {

	String spec;
	
	public Specifier(String spec) {
		this.spec = spec;
	}
	
	public String getSpec() {
		return spec;
	}
	
	public void setSpec(String spec) {
		this.spec = spec;
	}
	
	@Override
	public String toString() {
		return spec; //!!! MUST return spec to make Specifier appendable without calling getSpec()
	}
	
}
