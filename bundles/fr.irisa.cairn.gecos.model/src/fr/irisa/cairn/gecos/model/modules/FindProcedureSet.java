package fr.irisa.cairn.gecos.model.modules;

import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;

import java.io.File;

import fr.irisa.r2d2.gecos.framework.GSModule;

@GSModule("Find a ProcedureSet by name in a GecosProject.\n"
		+ "\nReturn: the corresponding ProcedureSet if found.\n"
		+ "The command fails if no PS with the specified name is found.")
public class FindProcedureSet {

	private GecosProject project;
	private String name;
	
	
	public FindProcedureSet(GecosProject project, String name) {
		this.project = project;
		this.name = name;
	}
	
	public ProcedureSet compute() {
		for (GecosSourceFile file : project.getSources()) {
			File f = new File(file.getName());
			if (f.getName().equals(name)) return file.getModel();
		}
		
		throw new RuntimeException("Error : ProcedureSet ["+name+"] not found in project "+project.getName());
	}
}
