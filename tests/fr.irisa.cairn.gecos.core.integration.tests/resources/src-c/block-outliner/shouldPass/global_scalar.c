int res = 2;

int main() {
	#pragma outline
	{
		res = 4;
	}

	if(res != 4)
		abort();

	return 0;
}
