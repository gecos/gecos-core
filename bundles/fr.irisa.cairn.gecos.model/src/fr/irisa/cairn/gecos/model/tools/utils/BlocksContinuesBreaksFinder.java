package fr.irisa.cairn.gecos.model.tools.utils;

import gecos.blocks.BasicBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.SwitchBlock;
import gecos.blocks.WhileBlock;
import gecos.instrs.Instruction;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;

/**
 * This visitor only return breaks and continues that are linked to the current
 * block, and not the inner loop or switch blocks.
 * 
 * 
 * @generated NOT
 */
public class BlocksContinuesBreaksFinder extends BlocksDefaultSwitch<Object> {
	
	private boolean containsBreak = false;
	private boolean containsContinue = false;
	public boolean containsBreak() { return containsBreak; }
	public boolean containsContinue() { return containsContinue; }

	private List<BasicBlock> breaks = new ArrayList<BasicBlock>();
	private List<BasicBlock> continues = new ArrayList<BasicBlock>();
	public List<BasicBlock> getBreaks() { return breaks; }
	public List<BasicBlock> getContinues() { return continues; }
	
	private Map<BasicBlock,BranchInstructionFinder> map = new LinkedHashMap<BasicBlock, BranchInstructionFinder>();
	public Map<BasicBlock,BranchInstructionFinder> getMap() { return map; }

	//*
	@Override
	public Object doSwitch(EObject o) {
		
		//visit only ForBlock, WhileBlock, LoopBlock, SwitchBlock
		if ((o instanceof ForBlock) || (o instanceof WhileBlock) || (o instanceof DoWhileBlock) || (o instanceof SwitchBlock)) {
			//System.out.println("visit only ForBlock, WhileBlock, LoopBlock, SwitchBlock");
			return true;
		}
		
		return super.doSwitch(o);
	}//*/
	
	@Override
	public Object caseBasicBlock(BasicBlock object) {
		BranchInstructionFinder visitor = new BranchInstructionFinder();
		for (Instruction i : object.getInstructions()) {
			visitor.doSwitch(i);
		}
		if (visitor.containsBreaks()) {
			breaks.add(object);
			containsBreak= true;
			map.put(object, visitor);
		}
		if (visitor.containsContinues()) {
			continues.add(object);
			containsContinue = true;
			map.put(object, visitor);
		}
		
		return true;
	}
	
}
