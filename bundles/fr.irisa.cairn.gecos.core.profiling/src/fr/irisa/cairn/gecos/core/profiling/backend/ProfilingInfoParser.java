package fr.irisa.cairn.gecos.core.profiling.backend;

import static fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport.FILE_EXT;

import java.nio.file.Files;
import java.nio.file.Path;
import java.security.InvalidParameterException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * @author aelmouss
 */
public class ProfilingInfoParser {

	protected Path outputsPath;
	protected Map<Integer, ProfilingInfo<? extends Number>> variables; //<uid, Variable>
	

	public ProfilingInfoParser(Path resultsFolder) {
		if(! resultsFolder.toFile().isDirectory())
			throw new InvalidParameterException("The simulation output directory is not valid " + resultsFolder);
		
		this.outputsPath = resultsFolder;
	}
	
	public Map<Integer, ProfilingInfo<? extends Number>> getVariables() {
		return variables;
	}
	
	public Map<Integer, ProfilingInfo<? extends Number>> retrieve() throws ProfilingBackendFailure {
		this.variables = new LinkedHashMap<>();
		
		try {
			try(Stream<Path> stream= Files.list(outputsPath)){
			    stream.filter(f -> Files.isRegularFile(f) && f.getFileName().toString().endsWith(FILE_EXT))
				.map(t -> {
					try {
						return ProfilingInfo.parseProfilingInfo(t);
					} catch (ProfilingBackendFailure e) {
						throw new RuntimeException(e);
					}
				})
				.forEach(v -> variables.put(v.getUid(), v));
			}
		} catch (Exception e) {
			throw new ProfilingBackendFailure("Couldn't list entries in directory " + outputsPath, e);
		}

		return variables;
	}
	
}
