/**
 */
package gecos.annotations.util;

import gecos.annotations.*;

import gecos.core.GecosNode;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see gecos.annotations.AnnotationsPackage
 * @generated
 */
public class AnnotationsSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static AnnotationsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationsSwitch() {
		if (modelPackage == null) {
			modelPackage = AnnotationsPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case AnnotationsPackage.IANNOTATION: {
				IAnnotation iAnnotation = (IAnnotation)theEObject;
				T result = caseIAnnotation(iAnnotation);
				if (result == null) result = caseGecosNode(iAnnotation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AnnotationsPackage.STRING_ANNOTATION: {
				StringAnnotation stringAnnotation = (StringAnnotation)theEObject;
				T result = caseStringAnnotation(stringAnnotation);
				if (result == null) result = caseIAnnotation(stringAnnotation);
				if (result == null) result = caseGecosNode(stringAnnotation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AnnotationsPackage.PRAGMA_ANNOTATION: {
				PragmaAnnotation pragmaAnnotation = (PragmaAnnotation)theEObject;
				T result = casePragmaAnnotation(pragmaAnnotation);
				if (result == null) result = caseIAnnotation(pragmaAnnotation);
				if (result == null) result = caseGecosNode(pragmaAnnotation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AnnotationsPackage.COMMENT_ANNOTATION: {
				CommentAnnotation commentAnnotation = (CommentAnnotation)theEObject;
				T result = caseCommentAnnotation(commentAnnotation);
				if (result == null) result = caseStringAnnotation(commentAnnotation);
				if (result == null) result = caseIAnnotation(commentAnnotation);
				if (result == null) result = caseGecosNode(commentAnnotation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AnnotationsPackage.EXTENDED_ANNOTATION: {
				ExtendedAnnotation extendedAnnotation = (ExtendedAnnotation)theEObject;
				T result = caseExtendedAnnotation(extendedAnnotation);
				if (result == null) result = caseIAnnotation(extendedAnnotation);
				if (result == null) result = caseGecosNode(extendedAnnotation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AnnotationsPackage.LIVENESS_ANNOTATION: {
				LivenessAnnotation livenessAnnotation = (LivenessAnnotation)theEObject;
				T result = caseLivenessAnnotation(livenessAnnotation);
				if (result == null) result = caseIAnnotation(livenessAnnotation);
				if (result == null) result = caseGecosNode(livenessAnnotation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AnnotationsPackage.SYMBOL_DEFINITION_ANNOTATION: {
				SymbolDefinitionAnnotation symbolDefinitionAnnotation = (SymbolDefinitionAnnotation)theEObject;
				T result = caseSymbolDefinitionAnnotation(symbolDefinitionAnnotation);
				if (result == null) result = caseIAnnotation(symbolDefinitionAnnotation);
				if (result == null) result = caseGecosNode(symbolDefinitionAnnotation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AnnotationsPackage.IAST_ANNOTATION: {
				IASTAnnotation iastAnnotation = (IASTAnnotation)theEObject;
				T result = caseIASTAnnotation(iastAnnotation);
				if (result == null) result = caseIAnnotation(iastAnnotation);
				if (result == null) result = caseGecosNode(iastAnnotation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION: {
				FileLocationAnnotation fileLocationAnnotation = (FileLocationAnnotation)theEObject;
				T result = caseFileLocationAnnotation(fileLocationAnnotation);
				if (result == null) result = caseIAnnotation(fileLocationAnnotation);
				if (result == null) result = caseGecosNode(fileLocationAnnotation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AnnotationsPackage.STRING_TO_IANNOTATION_MAP: {
				@SuppressWarnings("unchecked") Map.Entry<String, IAnnotation> stringToIAnnotationMap = (Map.Entry<String, IAnnotation>)theEObject;
				T result = caseStringToIAnnotationMap(stringToIAnnotationMap);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AnnotationsPackage.ANNOTATED_ELEMENT: {
				AnnotatedElement annotatedElement = (AnnotatedElement)theEObject;
				T result = caseAnnotatedElement(annotatedElement);
				if (result == null) result = caseGecosNode(annotatedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IAnnotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IAnnotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIAnnotation(IAnnotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStringAnnotation(StringAnnotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pragma Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pragma Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePragmaAnnotation(PragmaAnnotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Comment Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Comment Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommentAnnotation(CommentAnnotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Extended Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Extended Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExtendedAnnotation(ExtendedAnnotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Liveness Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Liveness Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLivenessAnnotation(LivenessAnnotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Symbol Definition Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Symbol Definition Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSymbolDefinitionAnnotation(SymbolDefinitionAnnotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IAST Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IAST Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIASTAnnotation(IASTAnnotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>File Location Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>File Location Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFileLocationAnnotation(FileLocationAnnotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String To IAnnotation Map</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String To IAnnotation Map</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStringToIAnnotationMap(Map.Entry<String, IAnnotation> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotatedElement(AnnotatedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Gecos Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Gecos Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGecosNode(GecosNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //AnnotationsSwitch
