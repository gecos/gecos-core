#include "defs.h"
#include "data_size.h"


void dag(TYPE_0 in[MC][NC], TYPE_1 out[1]){

	TYPE_0 A[1][10] = {0};
	TYPE_0 t = in[0][1];
	TYPE_1 res;

	{ //dag 1: test RAW 1
		A[0][0] = in[0][0];
		res = A[0][0] + 2;
	}

	{ //dag 2: test RAW 2
		A[0][0] = res;
		res = A[0][0] + 2;
		A[0][1] = res;
	}

	{ //dag 3: test WAR
		A[0][2] = A[0][1];
		A[0][1] = res + 2;
		res = A[0][1] + A[0][2];
	}

	{ //dag 4 scalar WAR
		res = t + 2;
		t = A[0][1] + 2;
	}

	out[0] = res + t;
}
