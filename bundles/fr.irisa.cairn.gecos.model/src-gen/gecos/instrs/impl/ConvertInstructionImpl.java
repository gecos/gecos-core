/**
 */
package gecos.instrs.impl;

import gecos.instrs.ConvertInstruction;
import gecos.instrs.InstrsPackage;
import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;

import gecos.types.Type;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Convert Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ConvertInstructionImpl extends ExprComponentImpl implements ConvertInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConvertInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstrsPackage.eINSTANCE.getConvertInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final InstrsVisitor visitor) {
		visitor.visitConvertInstruction(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _elvis = null;
		Type _type = this.getType();
		String _string = null;
		if (_type!=null) {
			_string=_type.toString();
		}
		if (_string != null) {
			_elvis = _string;
		} else {
			_elvis = "?";
		}
		String _plus = ("((" + _elvis);
		String _plus_1 = (_plus + ")");
		String _elvis_1 = null;
		Instruction _expr = this.getExpr();
		String _string_1 = null;
		if (_expr!=null) {
			_string_1=_expr.toString();
		}
		if (_string_1 != null) {
			_elvis_1 = _string_1;
		} else {
			_elvis_1 = "?";
		}
		String _plus_2 = (_plus_1 + _elvis_1);
		return (_plus_2 + ")");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSame(final Instruction instr) {
		if ((instr instanceof ConvertInstruction)) {
			return super.isSame(instr);
		}
		return false;
	}

} //ConvertInstructionImpl
