#==========================================================#
# Generic Makefile for building/running applications       #
# on x86, KAHRISMA, XENTIUM, VEX and ST240.                #
# Provided as part of GeCoS integration testing Framework. #
#                                                          #
# Author: aelmouss                                         #
#==========================================================#

########################################
##--------- Parameters ---------------##
########################################
##-----------------------##
# Directories             #
##-----------------------##
#SRC_DIRS ?=
#BUILD_DIR ?=
#INC_DIRS ?=
#LIB_DIRS ?=

SRCS ?= $(wildcard $(addsuffix /*.c,$(SRC_DIRS)))

APP_NAME ?= $(notdir $(word 1,$(patsubst %.c,%,$(SRCS))))
#PROG_ARGS ?=
PROG := $(BUILD_DIR)/$(APP_NAME)

FL2FXCONVERTOR = "$(TOOLS_DIR)/bin/Fix2FloatConvertor" 0
FX2FLCONVERTOR = "$(TOOLS_DIR)/bin/Fix2FloatConvertor" 1

##-----------------------##
# Flags     FIXME              #
##-----------------------##
COMMON_CFLAGS = $(patsubst %,-I%,$(INC_DIRS)) $(patsubst %,-L%,$(LIB_DIRS)) $(CFLAGS)
XCFLAGS += $(COMMON_CFLAGS)
KCFLAGS += $(COMMON_CFLAGS) -D__KAHRISMA__ #-g
RCFLAGS += $(COMMON_CFLAGS) -D__XENTIUM__  
VCFLAGS += $(COMMON_CFLAGS) -D__VEX__      #-H0 -mas_g -ms
SCFLAGS += $(COMMON_CFLAGS) -D__ST240__    #-g -keep
XOMPICFLAGS += $(COMMON_CFLAGS)

XLDFLAGS += 
KLDFLAGS += 
RLDFLAGS += 
VLDFLAGS += -lvexsimd
SLDFlAGS += 
XOMPILDFlAGS +=

##-----------------------##
# Compilers / Simulators  #
##-----------------------##
##-- X86
CC ?= gcc
CXX ?= g++

##-- KAHRISMA
KCC ?= kahrisma-gcc
KSIM_PARAM ?= --doe #--quiet
KTARGET ?= RSIW2
ifdef ISAPART
	ISAPARTFILE = $(patsubst %, %.PART, $@)
	ISAPARTCMD = -ts1-part-algo=partitioner -ts1-part-algo-file=${ISAPARTFILE}
endif

##-- RECORE
RCC ?= xentium-clang
RSIM_PARAM ?= --simulation-time --cycles

##-- VEX
VEX_DIR ?= ~/vex-3.43
VCC ?= $(VEX_DIR)/bin/cc
VEX_CONFIG_FILE ?= vex.cfg   #XXX unused
VEX_MM_FILE ?= $(TOOLS_DIR)/configs/vex.mm
VSIM_PARAM ?= -mas_t -fmm="$(VEX_MM_FILE)"

##-- ST240
SCC ?= st200cc
STARGET ?= st240
SSIM_PARAM ?= -c st200sp -t $(STARGET)simle #-C "PROFILING=true"

##-- Open MPI (x86)
XOMPICC ?= mpicc
XOMPISIM ?= mpirun
XOMPI_NPROCS ?= 4

##-----------------------##
# Output redirection      #
##-----------------------##
STDOUTERR_REDIRECT :=
ifdef STDOUT
	STDOUTERR_REDIRECT := > $(STDOUT)
endif
ifdef STDERR
	STDOUTERR_REDIRECT += 2> $(STDERR)
endif

##-----------------------##
# File Extensions         #
##-----------------------##
XEXT ?= x86
KEXT ?= kah
REXT ?= rec
VEXT ?= vex
SEXT ?= st240
XOMPIEXT ?= x86ompi
OUTEXT ?= output

########################################
##------------- Targets --------------##
########################################
all: help
.DEFAULT_GOAL := help

.PHONY: compx compr compk compv comps compxompi
.PHONY: runx runk runr runv runs runxompi
.PHONY: clean help info

########################################
##-------------- Build ---------------##
########################################
#----------
# arg1: EXT
define getObjs
$(addprefix $(BUILD_DIR)/,$(notdir $(patsubst %.c,%.$1.o,$(SRCS))))
endef
#----------
# arg1: EXT
# arg2: src
define makeObjs
$(addprefix $(BUILD_DIR)/,$(notdir $(patsubst %.c,%.$(1).o,$(2)))): $(2)
endef

XPROG := $(patsubst %,%.$(XEXT),$(PROG))
KPROG := $(patsubst %,%.$(KEXT),$(PROG))
RPROG := $(patsubst %,%.$(REXT),$(PROG))
VPROG := $(patsubst %,%.$(VEXT),$(PROG))
SPROG := $(patsubst %,%.$(SEXT),$(PROG))
XOMPIPROG := $(patsubst %,%.$(XOMPIEXT),$(PROG))

XOBJS := $(call getObjs,$(XEXT))
KOBJS := $(call getObjs,$(KEXT))
ROBJS := $(call getObjs,$(REXT))
VOBJS := $(call getObjs,$(VEXT))
SOBJS := $(call getObjs,$(SEXT))
XOMPIOBJS := $(call getObjs,$(XOMPIEXT))

$(foreach src,$(SRCS),$(eval $(call makeObjs,$(XEXT),$(src))))
$(foreach src,$(SRCS),$(eval $(call makeObjs,$(KEXT),$(src))))
$(foreach src,$(SRCS),$(eval $(call makeObjs,$(REXT),$(src))))
$(foreach src,$(SRCS),$(eval $(call makeObjs,$(VEXT),$(src))))
$(foreach src,$(SRCS),$(eval $(call makeObjs,$(SEXT),$(src))))
$(foreach src,$(SRCS),$(eval $(call makeObjs,$(XOMPIEXT),$(src))))

compx: $(XPROG)
compk: $(KPROG)
compr: $(RPROG)
compv: $(VPROG)
comps: $(SPROG)
compxompi: $(XOMPIPROG)

$(XPROG): $(XOBJS)
	$(CC) $(XCFLAGS) -o $@ $^ $(XLDFLAGS)
$(KPROG): $(KOBJS)
	kahrisma-ld -target="${KTARGET}" -o $@ $^ $(KLDFLAGS)
$(RPROG): $(ROBJS)
	$(RCC) $(RCFLAGS) -o $@ $^ $(RLDFLAGS)
$(VPROG): $(VOBJS)
	$(VCC) $(VCFLAGS) $(VSIM_PARAM) -o $@ $^ $(VLDFLAGS)
$(SPROG): $(SOBJS)
	$(SCC) $(SCFLAGS) -mcore=$(STARGET) -o $@ $^ $(SLDFLAGS)
$(XOMPIPROG): $(XOMPIOBJS)
	$(XOMPICC) $(XOMPICFLAGS) -o $@ $^ $(XOMPILDFLAGS)

%.$(XEXT).o:
	$(CC) $(XCFLAGS) -c -o $@ $<
%.$(KEXT).o:
	$(KCC) -mcpu="${KTARGET}" $(KCFLAGS) -c -o $@ $< ${ISAPARTCMD}
%.$(REXT).o:
	$(RCC) $(RCFLAGS) -c -o $@ $<
%.$(VEXT).o:
	$(VCC) $(VCFLAGS) $(VSIM_PARAM) -c -o $@  $<
%.$(SEXT).o:
	$(SCC) $(SCFLAGS) -mcore=$(STARGET) -c -o $@  $<
%.$(XOMPIEXT).o:
	$(XOMPICC) $(XOMPICFLAGS) -c -o $@ $<
	
########################################
##-------------- Run -----------------##
########################################
XOUTPUT := $(patsubst %,%.$(OUTEXT),$(XPROG))
KOUTPUT := $(patsubst %,%.$(OUTEXT),$(KPROG))
ROUTPUT := $(patsubst %,%.$(OUTEXT),$(RPROG))
VOUTPUT := $(patsubst %,%.$(OUTEXT),$(VPROG))
SOUTPUT := $(patsubst %,%.$(OUTEXT),$(SPROG))
XOMPIOUTPUT := $(patsubst %,%.$(OUTEXT),$(XOMPIPROG))

runx: $(XOUTPUT)
runk: $(KOUTPUT)
runr: $(ROUTPUT)
runv: $(VOUTPUT)
runs: $(SOUTPUT)
runxompi: $(XOMPIOUTPUT)

.PHONY: $(XOUTPUT)

$(XOUTPUT): $(XPROG)
	@echo "\n===========< RUNNING on X86 >==========="
	$(call preRun_func)
	$(eval $(call set_PROG_ARGS))
	
	@printf "\n> "
	$(realpath $<) $(PROG_ARGS) $(STDOUTERR_REDIRECT) || exit $$?
	
	$(call postRun_func)
	
$(KOUTPUT): $(KPROG)
	@echo "\n===========< RUNNING on KAHRISMA >==========="
	$(eval $(call set_PROG_ARGS))
	kahrisma-sim $< --target "$(KTARGET)" --allstats $< --functrace $(KSIM_PARAM) --args $(PROG_ARGS) || exit $$?
$(ROUTPUT): $(RPROG)
	@echo "\n===========< RUNNING on XENTIUM >==========="
	$(eval $(call set_PROG_ARGS))
	xentium-sim $< --profile $<.xprof $(RSIM_PARAM) --args $(PROG_ARGS) || exit $$?
	xentium-prof $<.xprof > $<.stats
$(VOUTPUT): $(VPROG)
	@echo "\n===========< RUNNING on VEX >==========="
	$(eval $(call set_PROG_ARGS))
	@export LD_LIBRARY_PATH=$(VEX_DIR)/lib; \
	$(realpath $<) $(PROG_ARGS) > $<.stats || exit $$?
$(SOUTPUT): $(SPROG)
	@echo "\n===========< RUNNING on ST240 >==========="
	$(eval $(call set_PROG_ARGS))
	st200xrun -e $< $(SSIM_PARAM) -a $(PROG_ARGS) > $<.stats || exit $$?
	
$(XOMPIOUTPUT): $(XOMPIPROG)
	@echo "\n===========< RUNNING on Open MPI (x86) >==========="
	$(call preRun_func)
	$(eval $(call set_PROG_ARGS))
	
	@printf "\n> "
	$(XOMPISIM) -np $(XOMPI_NPROCS) $< $(PROG_ARGS) $(STDOUTERR_REDIRECT) || exit $$?
	
	$(call postRun_func)

########################################
##-------------- Clean ---------------##
########################################
clean:
	-rm -f $(PROG) $(PROG).$(XEXT)* $(PROG).$(KEXT)* $(PROG).$(REXT)* $(PROG).$(VEXT)* $(PROG).$(XOMPIEXT)* *.o* *.log.* *.s *.cs.*

########################################
##-------------- Help ----------------##
########################################
help:
	@echo "Use one of the following commands:"
	@echo "   comp<?>      Compile for target <?>"
	@echo "   run<?>       Run all \"$(APP_NAME)\" versions on target <?>"
#	@echo "   cpr<?>       Compare results of all versions generated by <?>"
#	@echo "   cpra         Compare results of all versions generated by all"
#	@echo "   clean<?>     Clean for <?>"
	@echo "   <?>          Target Architecture:"
	@echo "                   - x:  x86"
	@echo "                   - k:  Kahrisma"
	@echo "                   - r:  Xentium"
	@echo "                   - v:  VEX"
	@echo "                   - s:  ST240"
	@echo "                   - xompi:  x86 open mpi"
	@echo "   clean        Clean for for all"
	@echo "   info         print Variables values and exit."
#	@echo "   tools        Compile tools in \"$(TOOLS_DIR)\""
#	@echo "   cleantools   Clean tools"
#	@echo "   cleansrc     Remove all generated sources"
#	@echo "   input        Generate random data input file \"$(INPUT_FILE)\". Use following variables"
#	@echo "                   - M = nb of lines"
#	@echo "                   - N = nb of columns"
#	@echo "                   - ISFLOAT = 0:integer 1:float"
#	@echo "                   - MIN = minimal value"
#	@echo "                   - MAX = maximal value"
#	@echo "   test         runk for different input sizes and extract stats to \"$(STATS_DIR)\""
#	@echo "   normalize    Normalize extracted stats in \"$(STATS_DIR)\""
#	@echo "   plot         Generate .pdf graphs for all stats in \"$(STATS_DIR)\""
#	@echo "   cleanplot    Clean generated plot graphs"
#	@echo "   cleanall     Clean all and launch \"$(BASE_DIR)/../clean.sh\""
	
info:
	@echo " SRC_DIRS  : $(SRC_DIRS) "
	@echo " BUILD_DIR : $(BUILD_DIR)"
	@echo " INC_DIRS  : $(INC_DIRS) "
	@echo " LIB_DIRS  : $(LIB_DIRS) "
	@echo " TOOLS_DIR : $(TOOLS_DIR)"
	
	@echo " STDOUT    : $(STDOUT)	"
	@echo " STDERR    : $(STDERR)	"
	@echo " STDOUTERR_REDIRECT    : $(STDOUTERR_REDIRECT)	"
	
	@echo " APP_NAME  : $(APP_NAME) "
	@echo " SRCS      : $(SRCS) 	"
	
	@echo " PROG      : $(PROG) 	"
	@echo " XPROG     : $(XPROG) 	"
	@echo " KPROG     : $(KPROG) 	"
	@echo " RPROG     : $(RPROG) 	"
	@echo " VPROG     : $(VPROG) 	"
	@echo " SPROG     : $(SPROG) 	"
	
	@echo " XOBJS     : $(XOBJS) 	"
	@echo " KOBJS     : $(KOBJS) 	"
	@echo " ROBJS     : $(ROBJS) 	"
	@echo " VOBJS     : $(VOBJS) 	"
	@echo " SOBJS     : $(SOBJS) 	"
	
	@echo " XOUTPUT   : $(XOUTPUT) 	"
	@echo " KOUTPUT   : $(KOUTPUT) 	"
	@echo " ROUTPUT   : $(ROUTPUT) 	"
	@echo " VOUTPUT   : $(VOUTPUT) 	"
	@echo " SOUTPUT   : $(SOUTPUT) 	"		

	@echo " CC        : $(CC) 		"
	@echo " KCC       : $(KCC) 	    "
	@echo " RCC       : $(RCC) 	    "
	@echo " VCC       : $(VCC) 	    "
	@echo " SCC       : $(SCC) 	    "
	
	@echo " XCFLAGS   : $(XCFLAGS) 	"
	@echo " KCFLAGS   : $(KCFLAGS) 	"
	@echo " RCFLAGS   : $(RCFLAGS) 	"
	@echo " VCFLAGS   : $(VCFLAGS) 	"
	@echo " SCFLAGS   : $(SCFLAGS) 	"
	
	@echo " XLDFLAGS  : $(XLDFLAGS) "
	@echo " KLDFLAGS  : $(KLDFLAGS) "
	@echo " RLDFLAGS  : $(RLDFLAGS) "
	@echo " VLDFLAGS  : $(VLDFLAGS) "
	@echo " SLDFLAGS  : $(SLDFLAGS) "
                                     
	@echo " KSIM_PARAM: $(KSIM_PARAM) "
	@echo " RSIM_PARAM: $(RSIM_PARAM) "
	@echo " VSIM_PARAM: $(VSIM_PARAM) "
	@echo " SSIM_PARAM: $(SSIM_PARAM) "
	
	@echo " KTARGET   : $(KTARGET)    "
	@echo " STARGET   : $(STARGET)    "
	@echo " VEX_CONFIG_FILE : $(VEX_CONFIG_FILE)"
	@echo " VEX_MM_FILE     : $(VEX_MM_FILE)    "
	
	