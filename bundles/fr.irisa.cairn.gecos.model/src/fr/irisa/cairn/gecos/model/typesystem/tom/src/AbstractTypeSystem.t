package fr.irisa.cairn.gecos.model.typesystem;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.extensions.typesystem.ExtendableTypeSystem;
import fr.irisa.cairn.gecos.model.extensions.typesystem.ITypeSystemRuleSet;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.tools.queries.GecosQuery;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.core.ProcedureSet;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.gecosproject.GecosProject;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.Instruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;

@SuppressWarnings("all")
public abstract class AbstractTypeSystem extends ExtendableTypeSystem{


	%include { sl.tom }

	%include { gecos_common.tom }
	%include { gecos_terminals.tom }
	%include { gecos_basic.tom }
	%include { gecos_logical.tom }
	%include { gecos_compare.tom }

	private GecosProject gecosProject;
	protected ProcedureSet currentPS;
	
 
	public AbstractTypeSystem(GecosProject gecosProject, ITypeSystemRuleSet ruleset) {
		super(ruleset);
		this.gecosProject = gecosProject;
	}

	public void compute() throws VisitFailure {
		for (ProcedureSet ps : gecosProject.listProcedureSets()) {
			compute(ps);
		}
	}
	
	abstract protected void unknownInstructionTypeHandler(Instruction i ) ;

	public void compute(ProcedureSet ps) throws VisitFailure {
			currentPS =ps;
			GecosUserTypeFactory.setScope(ps.getScope());
			for(BasicBlock bb : GecosQuery.findAllBasicBlocksIn(ps)) {
				for(Instruction i : bb.getInstructions()) {
					 apply(i);
				}	
			}
	}
	
	protected void apply(Instruction instruction) throws VisitFailure{
		Instruction res = `InnermostId(TypeInstruction(this)).visitLight(instruction, tom.mapping.GenericIntrospector.INSTANCE);
	}

	%typeterm ETS {
		implement { AbstractTypeSystem }
		is_sort(t) { t instanceof AbstractTypeSystem }
		equals(t1,t2) { t1.equals(t2) }
	}
	
	static void addTypeToScope(ProcedureSet ps, Type t) {
		ps.getScope().getTypes().add(t);
		
	}
	
	%strategy TypeInstruction(ETS ets) extends Identity() {
		visit Inst {
			i -> {
				Type t= ets.computeType((`i));
				if(t!=null) {
					addTypeToScope(ets.currentPS,t);
					(`i).setType(t);
					System.out.println("Type("+(`i)+")->"+t);
				} else {
					ets.unknownInstructionTypeHandler((`i));
				}
			}
				  
		} 
	}	
	
}