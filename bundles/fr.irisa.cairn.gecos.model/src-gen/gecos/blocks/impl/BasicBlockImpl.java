/**
 */
package gecos.blocks.impl;

import com.google.common.base.Objects;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;

import fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;
import fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksFactory;
import gecos.blocks.BlocksPackage;
import gecos.blocks.BlocksVisitor;
import gecos.blocks.ControlEdge;
import gecos.blocks.DataEdge;

import gecos.core.Scope;
import gecos.core.ScopeContainer;

import gecos.dag.DAGInstruction;

import gecos.instrs.BranchType;
import gecos.instrs.GotoInstruction;
import gecos.instrs.Instruction;

import java.util.Collection;
import java.util.List;

import java.util.function.Consumer;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Basic Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.blocks.impl.BasicBlockImpl#getInstructions <em>Instructions</em>}</li>
 *   <li>{@link gecos.blocks.impl.BasicBlockImpl#getDefEdges <em>Def Edges</em>}</li>
 *   <li>{@link gecos.blocks.impl.BasicBlockImpl#getOutEdges <em>Out Edges</em>}</li>
 *   <li>{@link gecos.blocks.impl.BasicBlockImpl#getUseEdges <em>Use Edges</em>}</li>
 *   <li>{@link gecos.blocks.impl.BasicBlockImpl#getInEdges <em>In Edges</em>}</li>
 *   <li>{@link gecos.blocks.impl.BasicBlockImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link gecos.blocks.impl.BasicBlockImpl#getFlags <em>Flags</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BasicBlockImpl extends BlockImpl implements BasicBlock {
	/**
	 * The cached value of the '{@link #getInstructions() <em>Instructions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstructions()
	 * @generated
	 * @ordered
	 */
	protected EList<Instruction> instructions;

	/**
	 * The cached value of the '{@link #getDefEdges() <em>Def Edges</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<DataEdge> defEdges;

	/**
	 * The cached value of the '{@link #getOutEdges() <em>Out Edges</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<ControlEdge> outEdges;

	/**
	 * The cached value of the '{@link #getUseEdges() <em>Use Edges</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUseEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<DataEdge> useEdges;

	/**
	 * The cached value of the '{@link #getInEdges() <em>In Edges</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<ControlEdge> inEdges;

	/**
	 * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected String label = LABEL_EDEFAULT;

	/**
	 * The default value of the '{@link #getFlags() <em>Flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFlags()
	 * @generated
	 * @ordered
	 */
	protected static final int FLAGS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getFlags() <em>Flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFlags()
	 * @generated
	 * @ordered
	 */
	protected int flags = FLAGS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BasicBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BlocksPackage.Literals.BASIC_BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instruction> getInstructions() {
		if (instructions == null) {
			instructions = new EObjectContainmentEList<Instruction>(Instruction.class, this, BlocksPackage.BASIC_BLOCK__INSTRUCTIONS);
		}
		return instructions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataEdge> getDefEdges() {
		if (defEdges == null) {
			defEdges = new EObjectContainmentWithInverseEList<DataEdge>(DataEdge.class, this, BlocksPackage.BASIC_BLOCK__DEF_EDGES, BlocksPackage.DATA_EDGE__FROM);
		}
		return defEdges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlEdge> getOutEdges() {
		if (outEdges == null) {
			outEdges = new EObjectContainmentWithInverseEList<ControlEdge>(ControlEdge.class, this, BlocksPackage.BASIC_BLOCK__OUT_EDGES, BlocksPackage.CONTROL_EDGE__FROM);
		}
		return outEdges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataEdge> getUseEdges() {
		if (useEdges == null) {
			useEdges = new EObjectWithInverseResolvingEList<DataEdge>(DataEdge.class, this, BlocksPackage.BASIC_BLOCK__USE_EDGES, BlocksPackage.DATA_EDGE__TO);
		}
		return useEdges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlEdge> getInEdges() {
		if (inEdges == null) {
			inEdges = new EObjectWithInverseResolvingEList<ControlEdge>(ControlEdge.class, this, BlocksPackage.BASIC_BLOCK__IN_EDGES, BlocksPackage.CONTROL_EDGE__TO);
		}
		return inEdges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabel(String newLabel) {
		String oldLabel = label;
		label = newLabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.BASIC_BLOCK__LABEL, oldLabel, label));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getFlags() {
		return flags;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFlags(int newFlags) {
		int oldFlags = flags;
		flags = newFlags;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.BASIC_BLOCK__FLAGS, oldFlags, flags));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		final StringBuffer sb = new StringBuffer();
		String content = this.getInstructions().toString();
		int _length = content.length();
		boolean _greaterThan = (_length > 40);
		if (_greaterThan) {
			int _size = this.getInstructions().size();
			boolean _greaterThan_1 = (_size > 1);
			if (_greaterThan_1) {
				String _string = this.getInstructions().get(0).toString();
				String _plus = (_string + ", ...");
				content = _plus;
			}
		}
		String _pathString = this.getPathString();
		String _string_1 = sb.toString();
		return (_pathString + _string_1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toShortString() {
		int _number = this.getNumber();
		return ("BB" + Integer.valueOf(_number));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BasicBlock> getPredecessors() {
		EList<BasicBlock> _xblockexpression = null;
		{
			final Function1<ControlEdge, BasicBlock> _function = new Function1<ControlEdge, BasicBlock>() {
				public BasicBlock apply(final ControlEdge it) {
					return it.getFrom();
				}
			};
			final EList<BasicBlock> res = ECollections.<BasicBlock>unmodifiableEList(ECollections.<BasicBlock>asEList(XcoreEListExtensions.<ControlEdge, BasicBlock>map(this.getInEdges(), _function)));
			final Consumer<BasicBlock> _function_1 = new Consumer<BasicBlock>() {
				public void accept(final BasicBlock bb) {
					if ((bb == null)) {
						throw new UnsupportedOperationException("Inconsistent control flow in IR");
					}
				}
			};
			res.forEach(_function_1);
			_xblockexpression = res;
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final BlocksVisitor visitor) {
		visitor.visitBasicBlock(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void clearOutEdges() {
		final Consumer<ControlEdge> _function = new Consumer<ControlEdge>() {
			public void accept(final ControlEdge it) {
				it.setTo(null);
				it.setFrom(null);
				it.setCond(null);
			}
		};
		((List<ControlEdge>)org.eclipse.xtext.xbase.lib.Conversions.doWrapArray(((ControlEdge[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(this.getOutEdges(), ControlEdge.class)).clone())).forEach(_function);
		this.getOutEdges().clear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BasicBlock> getSuccessors() {
		EList<BasicBlock> _xblockexpression = null;
		{
			final Function1<ControlEdge, BasicBlock> _function = new Function1<ControlEdge, BasicBlock>() {
				public BasicBlock apply(final ControlEdge it) {
					return it.getTo();
				}
			};
			final EList<BasicBlock> res = ECollections.<BasicBlock>unmodifiableEList(ECollections.<BasicBlock>asEList(XcoreEListExtensions.<ControlEdge, BasicBlock>map(this.getOutEdges(), _function)));
			final Consumer<BasicBlock> _function_1 = new Consumer<BasicBlock>() {
				public void accept(final BasicBlock bb) {
					if ((bb == null)) {
						throw new UnsupportedOperationException("Inconsistent control flow in IR");
					}
				}
			};
			res.forEach(_function_1);
			_xblockexpression = res;
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeInstruction(final Instruction s) {
		this.getInstructions().remove(s);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String contentString() {
		return IterableExtensions.join(this.getInstructions(), "\\n");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void mergeWithBasic(final BasicBlock b) {
		int _size = this.getOutEdges().size();
		boolean _greaterThan = (_size > 1);
		if (_greaterThan) {
			throw new IllegalArgumentException("Cannot merge a basicBloc with more than one out Edge");
		}
		int _size_1 = b.getInEdges().size();
		boolean _greaterThan_1 = (_size_1 > 1);
		if (_greaterThan_1) {
			throw new IllegalArgumentException("Cannot merge with a basicBloc with more than one in Edge");
		}
		final Consumer<Instruction> _function = new Consumer<Instruction>() {
			public void accept(final Instruction it) {
				b.prependInstruction(it);
			}
		};
		XcoreEListExtensions.<Instruction>reverseView(this.getInstructions()).forEach(_function);
		this.getParent().removeBlock(this);
		this.getOutEdges().get(0).disconnect();
		while ((this.getInEdges().size() != 0)) {
			{
				final ControlEdge e = this.getInEdges().get(0);
				b.connectFromBasic(e.getFrom(), e.getCond());
				e.disconnect();
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicBlock splitAt(final Instruction g) {
		final int idx = this.getInstructions().indexOf(g);
		if ((idx < 0)) {
			throw new IllegalArgumentException("The specified instruction is not contained in this basic block!");
		}
		final BasicBlock b = BlocksFactory.eINSTANCE.createBasicBlock();
		this.getParent().addBlockAfter(b, this);
		this.removeInstruction(g);
		final Consumer<Instruction> _function = new Consumer<Instruction>() {
			public void accept(final Instruction it) {
				b.addInstruction(it);
			}
		};
		((List<Instruction>)org.eclipse.xtext.xbase.lib.Conversions.doWrapArray(((Instruction[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<Instruction>drop(this.getInstructions(), idx), Instruction.class)).clone())).forEach(_function);
		while ((this.getOutEdges().size() != 0)) {
			{
				final ControlEdge e = this.getOutEdges().get(0);
				e.disconnect();
				BasicBlock _to = e.getTo();
				if (_to!=null) {
					_to.connectFromBasic(b, e.getCond());
				}
			}
		}
		return b;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void prependInstruction(final Instruction instr) {
		this.getInstructions().add(0, instr);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scope getAssociatedScope() {
		Block _parent = this.getParent();
		boolean _tripleEquals = (_parent == null);
		if (_tripleEquals) {
			throw new NullPointerException((("Basic block " + this) + " has an empty Scope"));
		}
		Block _parent_1 = this.getParent();
		return ((ScopeContainer) _parent_1).getScope();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlEdge connectFromBasic(final BasicBlock from, final BranchType cond) {
		if ((from == null)) {
			throw new IllegalArgumentException("Cannot connect to a null BB");
		}
		EList<ControlEdge> _outEdges = from.getOutEdges();
		for (final ControlEdge e : _outEdges) {
			if ((((e.getFrom() == from) && (e.getTo() == this)) && (e.getCond() == cond))) {
				return null;
			}
		}
		final ControlEdge e_1 = BlocksFactory.eINSTANCE.createControlEdge();
		e_1.setCond(cond);
		e_1.setFrom(from);
		e_1.setTo(this);
		return e_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlEdge> connectTo(final Block to, final BranchType cond) {
		final BranchInstructionFinder visitor = new BranchInstructionFinder();
		visitor.doSwitch(this);
		boolean _containsGotos = visitor.containsGotos();
		if (_containsGotos) {
			final GotoInstruction g = visitor.getGotos().get(0);
			final BasicBlock sndTo = g.getLabelInstruction().getBasicBlock();
			if ((sndTo != to)) {
				System.out.println("[BasicBlock] Warning : avoid connection. Basic Block has goto instruction.");
				return ECollections.<ControlEdge>asEList();
			}
		}
		final ControlEdge edge = to.connectFromBasic(this, cond);
		return ECollections.<ControlEdge>newBasicEList(edge);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addInstruction(final Instruction instr) {
		this.getInstructions().add(instr);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getInstruction(final int i) {
		return this.getInstructions().get(i);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void insertInstructionBefore(final Instruction instr, final Instruction pos) {
		final int i = this.getInstructions().indexOf(pos);
		if ((i >= 0)) {
			this.getInstructions().add(i, instr);
		}
		else {
			System.err.println(((("Warning : could not insert " + instr) + " before ") + pos));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void insertInstructionAfter(final Instruction instr, final Instruction pos) {
		final int i = this.getInstructions().indexOf(pos);
		if ((i >= 0)) {
			this.getInstructions().add((i + 1), instr);
		}
		else {
			System.err.println(((("Warning : could not insert " + instr) + " after ") + pos));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void replaceInstruction(final Instruction oldi, final Instruction newi) {
		final int i = this.getInstructions().indexOf(oldi);
		if ((i >= 0)) {
			this.getInstructions().set(i, newi);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeAllInstructions() {
		while ((this.getInstructions().size() > 0)) {
			this.removeInstruction(this.getInstructions().get(0));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction lastInstruction() {
		return IterableExtensions.<Instruction>last(this.getInstructions());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isConnectedTo(final BasicBlock b) {
		EList<ControlEdge> _outEdges = this.getOutEdges();
		for (final ControlEdge e : _outEdges) {
			BasicBlock _to = e.getTo();
			boolean _tripleEquals = (_to == b);
			if (_tripleEquals) {
				return true;
			}
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void disconnectFromBasic(final BasicBlock from) {
		final Function1<ControlEdge, Boolean> _function = new Function1<ControlEdge, Boolean>() {
			public Boolean apply(final ControlEdge it) {
				BasicBlock _to = it.getTo();
				return Boolean.valueOf((_to == BasicBlockImpl.this));
			}
		};
		final EList<ControlEdge> res = ECollections.<ControlEdge>asEList(((ControlEdge[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<ControlEdge>filter(from.getOutEdges(), _function), ControlEdge.class)));
		final Consumer<ControlEdge> _function_1 = new Consumer<ControlEdge>() {
			public void accept(final ControlEdge it) {
				it.setTo(null);
				it.getFrom().getOutEdges().remove(it);
			}
		};
		res.forEach(_function_1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void disconnect() {
		final Consumer<ControlEdge> _function = new Consumer<ControlEdge>() {
			public void accept(final ControlEdge it) {
				it.setTo(null);
			}
		};
		this.getOutEdges().forEach(_function);
		final Consumer<DataEdge> _function_1 = new Consumer<DataEdge>() {
			public void accept(final DataEdge it) {
				it.setTo(null);
			}
		};
		this.getDefEdges().forEach(_function_1);
		this.getOutEdges().clear();
		this.getInEdges().clear();
		this.getDefEdges().clear();
		this.getUseEdges().clear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int whichPredecessor(final BasicBlock b) {
		EList<ControlEdge> _inEdges = this.getInEdges();
		for (final ControlEdge e : _inEdges) {
			BasicBlock _from = e.getFrom();
			boolean _equals = Objects.equal(_from, b);
			if (_equals) {
				return this.getInEdges().indexOf(e);
			}
		}
		return (-1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFlag(final int f) {
		this.setFlags(f);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addFlag(final int f) {
		this.setFlags((this.getFlags() | f));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeFlag(final int f) {
		this.setFlags((this.getFlags() & (~f)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSet(final int f) {
		return ((this.getFlags() & f) != 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getFirstInstruction() {
		final Instruction i = IterableExtensions.<Instruction>head(this.getInstructions());
		if ((i == null)) {
			throw new RuntimeException(("Cannot get first instruction of an empty BasicBlock " + this));
		}
		return i;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getLastInstruction() {
		final Instruction i = IterableExtensions.<Instruction>last(this.getInstructions());
		if ((i == null)) {
			throw new RuntimeException(("Cannot get last instruction of an empty BasicBlock " + this));
		}
		return i;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getInstructionCount() {
		return this.getInstructions().size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPathString() {
		int _number = this.getNumber();
		String _plus = ("BB" + Integer.valueOf(_number));
		return this.getPathString(_plus);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block managedCopy(final BlockCopyManager mng) {
		final BasicBlock newBB = BlocksFactory.eINSTANCE.createBasicBlock();
		EList<Instruction> _instructions = this.getInstructions();
		for (final Instruction inst : _instructions) {
			{
				final Instruction newInst = inst.copy();
				newBB.getInstructions().add(newInst);
			}
		}
		GecosUserAnnotationFactory.copyAnnotations(this, newBB);
		mng.link(this, newBB);
		return newBB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDAG() {
		return ((this.getInstructionCount() == 1) && (this.getInstruction(0) instanceof DAGInstruction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BlocksPackage.BASIC_BLOCK__DEF_EDGES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getDefEdges()).basicAdd(otherEnd, msgs);
			case BlocksPackage.BASIC_BLOCK__OUT_EDGES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOutEdges()).basicAdd(otherEnd, msgs);
			case BlocksPackage.BASIC_BLOCK__USE_EDGES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getUseEdges()).basicAdd(otherEnd, msgs);
			case BlocksPackage.BASIC_BLOCK__IN_EDGES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getInEdges()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BlocksPackage.BASIC_BLOCK__INSTRUCTIONS:
				return ((InternalEList<?>)getInstructions()).basicRemove(otherEnd, msgs);
			case BlocksPackage.BASIC_BLOCK__DEF_EDGES:
				return ((InternalEList<?>)getDefEdges()).basicRemove(otherEnd, msgs);
			case BlocksPackage.BASIC_BLOCK__OUT_EDGES:
				return ((InternalEList<?>)getOutEdges()).basicRemove(otherEnd, msgs);
			case BlocksPackage.BASIC_BLOCK__USE_EDGES:
				return ((InternalEList<?>)getUseEdges()).basicRemove(otherEnd, msgs);
			case BlocksPackage.BASIC_BLOCK__IN_EDGES:
				return ((InternalEList<?>)getInEdges()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BlocksPackage.BASIC_BLOCK__INSTRUCTIONS:
				return getInstructions();
			case BlocksPackage.BASIC_BLOCK__DEF_EDGES:
				return getDefEdges();
			case BlocksPackage.BASIC_BLOCK__OUT_EDGES:
				return getOutEdges();
			case BlocksPackage.BASIC_BLOCK__USE_EDGES:
				return getUseEdges();
			case BlocksPackage.BASIC_BLOCK__IN_EDGES:
				return getInEdges();
			case BlocksPackage.BASIC_BLOCK__LABEL:
				return getLabel();
			case BlocksPackage.BASIC_BLOCK__FLAGS:
				return getFlags();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BlocksPackage.BASIC_BLOCK__INSTRUCTIONS:
				getInstructions().clear();
				getInstructions().addAll((Collection<? extends Instruction>)newValue);
				return;
			case BlocksPackage.BASIC_BLOCK__DEF_EDGES:
				getDefEdges().clear();
				getDefEdges().addAll((Collection<? extends DataEdge>)newValue);
				return;
			case BlocksPackage.BASIC_BLOCK__OUT_EDGES:
				getOutEdges().clear();
				getOutEdges().addAll((Collection<? extends ControlEdge>)newValue);
				return;
			case BlocksPackage.BASIC_BLOCK__USE_EDGES:
				getUseEdges().clear();
				getUseEdges().addAll((Collection<? extends DataEdge>)newValue);
				return;
			case BlocksPackage.BASIC_BLOCK__IN_EDGES:
				getInEdges().clear();
				getInEdges().addAll((Collection<? extends ControlEdge>)newValue);
				return;
			case BlocksPackage.BASIC_BLOCK__LABEL:
				setLabel((String)newValue);
				return;
			case BlocksPackage.BASIC_BLOCK__FLAGS:
				setFlags((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BlocksPackage.BASIC_BLOCK__INSTRUCTIONS:
				getInstructions().clear();
				return;
			case BlocksPackage.BASIC_BLOCK__DEF_EDGES:
				getDefEdges().clear();
				return;
			case BlocksPackage.BASIC_BLOCK__OUT_EDGES:
				getOutEdges().clear();
				return;
			case BlocksPackage.BASIC_BLOCK__USE_EDGES:
				getUseEdges().clear();
				return;
			case BlocksPackage.BASIC_BLOCK__IN_EDGES:
				getInEdges().clear();
				return;
			case BlocksPackage.BASIC_BLOCK__LABEL:
				setLabel(LABEL_EDEFAULT);
				return;
			case BlocksPackage.BASIC_BLOCK__FLAGS:
				setFlags(FLAGS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BlocksPackage.BASIC_BLOCK__INSTRUCTIONS:
				return instructions != null && !instructions.isEmpty();
			case BlocksPackage.BASIC_BLOCK__DEF_EDGES:
				return defEdges != null && !defEdges.isEmpty();
			case BlocksPackage.BASIC_BLOCK__OUT_EDGES:
				return outEdges != null && !outEdges.isEmpty();
			case BlocksPackage.BASIC_BLOCK__USE_EDGES:
				return useEdges != null && !useEdges.isEmpty();
			case BlocksPackage.BASIC_BLOCK__IN_EDGES:
				return inEdges != null && !inEdges.isEmpty();
			case BlocksPackage.BASIC_BLOCK__LABEL:
				return LABEL_EDEFAULT == null ? label != null : !LABEL_EDEFAULT.equals(label);
			case BlocksPackage.BASIC_BLOCK__FLAGS:
				return flags != FLAGS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //BasicBlockImpl
