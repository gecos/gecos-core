/**
 */
package gecos.blocks.impl;

import gecos.annotations.AnnotatedElement;
import gecos.annotations.AnnotationKeys;
import gecos.annotations.AnnotationsPackage;
import gecos.annotations.FileLocationAnnotation;
import gecos.annotations.IAnnotation;
import gecos.annotations.PragmaAnnotation;

import gecos.annotations.impl.StringToIAnnotationMapImpl;

import gecos.blocks.BasicBlock;
import gecos.blocks.BlocksPackage;
import gecos.blocks.BlocksVisitor;
import gecos.blocks.DataEdge;

import gecos.instrs.SSAUseSymbol;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Edge</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.blocks.impl.DataEdgeImpl#getAnnotations <em>Annotations</em>}</li>
 *   <li>{@link gecos.blocks.impl.DataEdgeImpl#getUses <em>Uses</em>}</li>
 *   <li>{@link gecos.blocks.impl.DataEdgeImpl#getTo <em>To</em>}</li>
 *   <li>{@link gecos.blocks.impl.DataEdgeImpl#getFrom <em>From</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataEdgeImpl extends MinimalEObjectImpl.Container implements DataEdge {
	/**
	 * The cached value of the '{@link #getAnnotations() <em>Annotations</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, IAnnotation> annotations;

	/**
	 * The cached value of the '{@link #getUses() <em>Uses</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUses()
	 * @generated
	 * @ordered
	 */
	protected EList<SSAUseSymbol> uses;

	/**
	 * The cached value of the '{@link #getTo() <em>To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTo()
	 * @generated
	 * @ordered
	 */
	protected BasicBlock to;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataEdgeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BlocksPackage.Literals.DATA_EDGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, IAnnotation> getAnnotations() {
		if (annotations == null) {
			annotations = new EcoreEMap<String,IAnnotation>(AnnotationsPackage.Literals.STRING_TO_IANNOTATION_MAP, StringToIAnnotationMapImpl.class, this, BlocksPackage.DATA_EDGE__ANNOTATIONS);
		}
		return annotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SSAUseSymbol> getUses() {
		if (uses == null) {
			uses = new EObjectResolvingEList<SSAUseSymbol>(SSAUseSymbol.class, this, BlocksPackage.DATA_EDGE__USES);
		}
		return uses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicBlock getTo() {
		return to;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTo(BasicBlock newTo, NotificationChain msgs) {
		BasicBlock oldTo = to;
		to = newTo;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BlocksPackage.DATA_EDGE__TO, oldTo, newTo);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTo(BasicBlock newTo) {
		if (newTo != to) {
			NotificationChain msgs = null;
			if (to != null)
				msgs = ((InternalEObject)to).eInverseRemove(this, BlocksPackage.BASIC_BLOCK__USE_EDGES, BasicBlock.class, msgs);
			if (newTo != null)
				msgs = ((InternalEObject)newTo).eInverseAdd(this, BlocksPackage.BASIC_BLOCK__USE_EDGES, BasicBlock.class, msgs);
			msgs = basicSetTo(newTo, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.DATA_EDGE__TO, newTo, newTo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicBlock getFrom() {
		if (eContainerFeatureID() != BlocksPackage.DATA_EDGE__FROM) return null;
		return (BasicBlock)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicBlock basicGetFrom() {
		if (eContainerFeatureID() != BlocksPackage.DATA_EDGE__FROM) return null;
		return (BasicBlock)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFrom(BasicBlock newFrom, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newFrom, BlocksPackage.DATA_EDGE__FROM, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFrom(BasicBlock newFrom) {
		if (newFrom != eInternalContainer() || (eContainerFeatureID() != BlocksPackage.DATA_EDGE__FROM && newFrom != null)) {
			if (EcoreUtil.isAncestor(this, newFrom))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newFrom != null)
				msgs = ((InternalEObject)newFrom).eInverseAdd(this, BlocksPackage.BASIC_BLOCK__DEF_EDGES, BasicBlock.class, msgs);
			msgs = basicSetFrom(newFrom, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.DATA_EDGE__FROM, newFrom, newFrom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final BlocksVisitor visitor) {
		visitor.visitDataEdge(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IAnnotation getAnnotation(final String key) {
		return this.getAnnotations().get(key);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnnotation(final String key, final IAnnotation annot) {
		this.getAnnotations().put(key, annot);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PragmaAnnotation getPragma() {
		final IAnnotation annot = this.getAnnotation(AnnotationKeys.PRAGMA_ANNOTATION_KEY.getLiteral());
		if ((annot instanceof PragmaAnnotation)) {
			return ((PragmaAnnotation)annot);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FileLocationAnnotation getFileLocation() {
		final IAnnotation annot = this.getAnnotation(AnnotationKeys.FILE_LOCATION_ANNOTATION_KEY.getLiteral());
		if ((annot instanceof FileLocationAnnotation)) {
			return ((FileLocationAnnotation)annot);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BlocksPackage.DATA_EDGE__TO:
				if (to != null)
					msgs = ((InternalEObject)to).eInverseRemove(this, BlocksPackage.BASIC_BLOCK__USE_EDGES, BasicBlock.class, msgs);
				return basicSetTo((BasicBlock)otherEnd, msgs);
			case BlocksPackage.DATA_EDGE__FROM:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetFrom((BasicBlock)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BlocksPackage.DATA_EDGE__ANNOTATIONS:
				return ((InternalEList<?>)getAnnotations()).basicRemove(otherEnd, msgs);
			case BlocksPackage.DATA_EDGE__TO:
				return basicSetTo(null, msgs);
			case BlocksPackage.DATA_EDGE__FROM:
				return basicSetFrom(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case BlocksPackage.DATA_EDGE__FROM:
				return eInternalContainer().eInverseRemove(this, BlocksPackage.BASIC_BLOCK__DEF_EDGES, BasicBlock.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BlocksPackage.DATA_EDGE__ANNOTATIONS:
				if (coreType) return getAnnotations();
				else return getAnnotations().map();
			case BlocksPackage.DATA_EDGE__USES:
				return getUses();
			case BlocksPackage.DATA_EDGE__TO:
				return getTo();
			case BlocksPackage.DATA_EDGE__FROM:
				if (resolve) return getFrom();
				return basicGetFrom();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BlocksPackage.DATA_EDGE__ANNOTATIONS:
				((EStructuralFeature.Setting)getAnnotations()).set(newValue);
				return;
			case BlocksPackage.DATA_EDGE__USES:
				getUses().clear();
				getUses().addAll((Collection<? extends SSAUseSymbol>)newValue);
				return;
			case BlocksPackage.DATA_EDGE__TO:
				setTo((BasicBlock)newValue);
				return;
			case BlocksPackage.DATA_EDGE__FROM:
				setFrom((BasicBlock)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BlocksPackage.DATA_EDGE__ANNOTATIONS:
				getAnnotations().clear();
				return;
			case BlocksPackage.DATA_EDGE__USES:
				getUses().clear();
				return;
			case BlocksPackage.DATA_EDGE__TO:
				setTo((BasicBlock)null);
				return;
			case BlocksPackage.DATA_EDGE__FROM:
				setFrom((BasicBlock)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BlocksPackage.DATA_EDGE__ANNOTATIONS:
				return annotations != null && !annotations.isEmpty();
			case BlocksPackage.DATA_EDGE__USES:
				return uses != null && !uses.isEmpty();
			case BlocksPackage.DATA_EDGE__TO:
				return to != null;
			case BlocksPackage.DATA_EDGE__FROM:
				return basicGetFrom() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == AnnotatedElement.class) {
			switch (derivedFeatureID) {
				case BlocksPackage.DATA_EDGE__ANNOTATIONS: return AnnotationsPackage.ANNOTATED_ELEMENT__ANNOTATIONS;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == AnnotatedElement.class) {
			switch (baseFeatureID) {
				case AnnotationsPackage.ANNOTATED_ELEMENT__ANNOTATIONS: return BlocksPackage.DATA_EDGE__ANNOTATIONS;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //DataEdgeImpl
