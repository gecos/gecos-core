package fr.irisa.cairn.gecos.model.modules;

import fr.irisa.r2d2.gecos.framework.GSModule;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosSourceFile;

@GSModule("Return: the ProcedureSet object corresponding to the\n"
		+ "specified GecosSourceFile.")
public class GetProcedureSet {

	private GecosSourceFile src;
	
	public GetProcedureSet(GecosSourceFile src) {
		this.src = src;
	}
	
	public ProcedureSet compute() {
		return src.getModel();
	}
}
