package fr.irisa.cairn.gecos.model.transforms.ssa;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.gecos.model.tools.queries.GecosQuery;
import fr.irisa.cairn.gecos.model.transforms.deadcodeelimination.UnreacheableCodeRemover;
import fr.irisa.cairn.tools.ecore.DanglingAnalyzer;
import fr.irisa.cairn.tools.ecore.DanglingAnalyzer.DanglingException;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import gecos.core.Procedure;

@GSModule("Convert the IR (CDFG) of the specified Gecos object\n"
		+ "into Static Single Assignement (SSA) form.\n"
		+ "\nSee: 'RemoveSSAForm' module to revert back this convertion.")
public class ComputeSSAForm {

	EObject target;
	
	@GSModuleConstructor(
			"-arg1: a Procedure container object \n"
			+ "   e.g. GecosProject or ProcedureSet.")
	public ComputeSSAForm(EObject target) {
		this.target=target;
	}
	
	/**
	 * Assume ControlFlow is built and consistent
	 * Assume no SimpleForBlocks
	 */
	public void compute() {
		EList<Procedure> procs = GecosQuery.findAllProceduresIn(target);
		for (Procedure proc : procs) {
			
			new UnreacheableCodeRemover(proc).compute();
			new BlockWrapper(proc).compute();

			SSAAnalyser analyze = new SSAAnalyser(proc);
			analyze.compute();
			
			try {
				DanglingAnalyzer an = new DanglingAnalyzer(target);
				an.hasDangling(proc);
			} catch (DanglingException danglingException) {
				System.err.println(danglingException.getMessage());
				//throw new RuntimeException(danglingException);
			}
		} 
	}
}
