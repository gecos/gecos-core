/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.analysis.dataflow;

import fr.irisa.cairn.gecos.model.analysis.dataflow.framework.BitString;
import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import gecos.core.Procedure;
import gecos.instrs.CallInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.PhiInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;

public class ForwardBitWidthAnalyser extends BlockInstructionSwitch<Object> {

	private Procedure proc;
	private boolean changed;

	public ForwardBitWidthAnalyser(Procedure proc) {
		this.proc = proc;
	}

	public boolean computeIterative() {
		changed = false;
		doSwitch(proc.getBody());
		return changed;
	}

	@Override
	public Object caseGenericInstruction(GenericInstruction g) {
		
		BitString bS0 = (BitString) g.getOperand(0).getAnnotation("BitString");
		BitString bS1 = (BitString) g.getOperand(1).getAnnotation("BitString");

		if (g.getName().equals("add")) {
			changed |= setBitString(g,bS0.add(bS1));
		} else if (g.getName().equals("sub")) {
			changed |= setBitString(g,bS0.sub(bS1));
		} else if (g.getName().equals("xor")) {
			changed |= setBitString(g,bS0.xor(bS1));
		} else if (g.getName().equals("and")) {
			changed |= setBitString(g,bS0.and(bS1));
		} else if (g.getName().equals("or")) {
			changed |= setBitString(g,bS0.or(bS1));
		} else if (g.getName().equals("shr")) {
			changed |= setBitString(g,bS0.asr(bS1));
		} else if (g.getName().equals("shl")) {
			changed |= setBitString(g,bS0.lsl(bS1));
		} else if (g.getName().equals("ne")) {
			changed |= setBitString(g,bS0.neq(bS1));
		} else if (g.getName().equals("eq")) {
			changed |= setBitString(g,bS0.neq(bS1).not());
		} else if (g.getName().equals("lt")) {
			changed |= setBitString(g,bS0.lower(bS1));
		} else if (g.getName().equals("le")) {
			changed |= setBitString(g,bS1.lower(bS0).not());
		} else if (g.getName().equals("gt")) {
			changed |= setBitString(g,bS1.lower(bS0));
		} else if (g.getName().equals("ge")) {
			changed |= setBitString(g,bS0.lower(bS1).not());
		} else if (g.getName().equals("mul")) {
			changed |= setBitString(g,bS0.mul(bS1).not());
		} else if (g.getName().equals("not")) {
			changed |= setBitString(g,bS0.not());
		} else if (g.getName().equals("str")) {
			changed |= setBitString(g.getOperand(0),bS1);
		} else if (g.getName().equals("array")) {
			// nothing to do
		} else
			throw new RuntimeException("generic " + g);
		return super.caseGenericInstruction(g);

	}

	private boolean setBitString(Instruction g, BitString add) {
		BitString old = (BitString) g.getAnnotation("BitString");
		if (old==null) {
			
		}
		throw new UnsupportedOperationException();
	}

	@Override
	public Object caseCallInstruction(CallInstruction c) {
		return super.caseGenericInstruction((GenericInstruction) c);
	}

	
 
	@Override
	public Object caseConvertInstruction(ConvertInstruction c) {
		doSwitch(c.getExpr());
		BitString mask = BitString.fromInt((1 << c.getType().getSize()) - 1)
				.dontCareOfDC((BitString) c.getExpr().getAnnotation("BitString"));
		changed |= setBitString(c,((BitString) c.getExpr().getAnnotation("BitString")).and(mask));
		return null;
	}

	@Override
	public Object caseSetInstruction(SetInstruction s) {
		doSwitch(s.getSource());
		if (s.getDest() instanceof SymbolInstruction) {
			changed |=  setBitString(s.getDest(),(BitString) s.getSource().getAnnotation("BitString"));
			/*
			 * symbols.put(s.dest().toString(), s.dest().getBitString()
			 * .interWith(s.source().getBitString()));
			 */
		}
		return null;
	}

	@Override
	public Object caseSymbolInstruction(SymbolInstruction s) {
		
//		 BitString b = (BitString) symbols.get(s.toString()); 
//		 if (b != null) {
//			 changed |= s.setBitString( s.getBitString().interWith(b));
//			 symbols.put(s.toString(), s.getBitString()); 
//		 }
		
		return null;
	}


	@Override
	public Object casePhiInstruction(PhiInstruction object) {
		Object res = super.casePhiInstruction(object);
//		BitString tmp = g.getOperand(0).getBitString();
//		for (int i = 1; i < g.childCount(); ++i)
//			tmp = tmp.interWith(g.getOperand(i).getBitString());
//		changed |= g.setBitString(tmp);
		return res;
	}
}
