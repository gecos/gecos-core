/**
 */
package gecos.blocks;

import fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;

import gecos.core.CoreVisitor;
import gecos.core.ScopeContainer;

import gecos.instrs.BranchType;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.blocks.CompositeBlock#getChildren <em>Children</em>}</li>
 * </ul>
 *
 * @see gecos.blocks.BlocksPackage#getCompositeBlock()
 * @model annotation="gmf.node figure='rectangle' label='number' label.placement='external' label.icon='false'"
 * @generated
 */
public interface CompositeBlock extends Block, ScopeContainer, BlocksVisitable {
	/**
	 * Returns the value of the '<em><b>Children</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.blocks.Block}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Children</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' containment reference list.
	 * @see gecos.blocks.BlocksPackage#getCompositeBlock_Children()
	 * @model containment="true"
	 *        annotation="gmf.compartment foo='bar'"
	 * @generated
	 */
	EList<Block> getChildren();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitScopeContainer(this);'"
	 * @generated
	 */
	void accept(CoreVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitCompositeBlock(this);'"
	 * @generated
	 */
	void accept(BlocksVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @param instr block to be inserted
	 * @param pos the insertion point block
	 * <!-- end-model-doc -->
	 * @model instrUnique="false" posUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _contains = this.getChildren().contains(pos);\nboolean _not = (!_contains);\nif (_not)\n{\n\t&lt;%java.lang.String%&gt; _plus = (pos + \" is not contained in \");\n\t&lt;%java.lang.String%&gt; _plus_1 = (_plus + this);\n\tthrow new &lt;%java.lang.RuntimeException%&gt;(_plus_1);\n}\nfinal int i = this.getChildren().indexOf(pos);\nthis.getChildren().add(i, instr);'"
	 * @generated
	 */
	void addBlockBefore(Block instr, Block pos);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @param instr block to be inserted
	 * @param pos the insertion point block
	 * <!-- end-model-doc -->
	 * @model instrUnique="false" posUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _contains = this.getChildren().contains(pos);\nboolean _not = (!_contains);\nif (_not)\n{\n\t&lt;%java.lang.String%&gt; _plus = (pos + \" is not contained in \");\n\t&lt;%java.lang.String%&gt; _plus_1 = (_plus + this);\n\tthrow new &lt;%java.lang.RuntimeException%&gt;(_plus_1);\n}\nfinal int i = this.getChildren().indexOf(pos);\nthis.getChildren().add((i + 1), instr);'"
	 * @generated
	 */
	void addBlockAfter(Block instr, Block pos);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model biUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getChildren().add(bi);'"
	 * @generated
	 */
	void addChildren(Block bi);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" mngDataType="gecos.core.BlockCopyManager" mngUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.blocks.CompositeBlock%&gt; newBlock = &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory%&gt;.CompositeBlock();\n&lt;%gecos.core.Scope%&gt; _scope = this.getScope();\nboolean _tripleNotEquals = (_scope != null);\nif (_tripleNotEquals)\n{\n\tfinal &lt;%gecos.core.Scope%&gt; newScope = this.getScope().managedCopy(mng);\n\tnewBlock.setScope(newScope);\n}\nnewBlock.setNumber(this.getNumber());\n&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory%&gt;.copyAnnotations(this, newBlock);\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.blocks.Block%&gt;&gt; _children = this.getChildren();\nfor (final &lt;%gecos.blocks.Block%&gt; child : _children)\n{\n\t{\n\t\tfinal &lt;%gecos.blocks.Block%&gt; newchildBlock = child.managedCopy(mng);\n\t\tnewBlock.getChildren().add(newchildBlock);\n\t}\n}\nmng.link(this, newBlock);\nreturn newBlock;'"
	 * @generated
	 */
	Block managedCopy(BlockCopyManager mng);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model oldbUnique="false" newbUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final int index = this.getChildren().indexOf(oldb);\nif ((index &gt;= 0))\n{\n\tthis.getChildren().set(index, newb);\n}'"
	 * @generated
	 */
	void replace(Block oldb, Block newb);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Simplify CompositeBlock whenever possible : simplified if it
	 * contains only one element. The scopes are merged so that
	 * no identifier or type declaration is lost.
	 * 
	 * @return the simplified block
	 * <!-- end-model-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final int size = this.getChildren().size();\nif ((size == 1))\n{\n\tfinal &lt;%gecos.blocks.Block%&gt; child = this.getChildren().get(0);\n\tif (((child != null) &amp;&amp; (child.eClass() == this.eClass())))\n\t{\n\t\tfinal &lt;%gecos.blocks.CompositeBlock%&gt; cbChild = ((&lt;%gecos.blocks.CompositeBlock%&gt;) child);\n\t\tchild.simplifyBlock();\n\t\tthis.getScope().mergeWith(child.getScope());\n\t\tthis.getChildren().addAll(cbChild.getChildren());\n\t\tthis.getChildren().remove(child);\n\t}\n}\nreturn this;'"
	 * @generated
	 */
	Block simplifyBlock();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" blockUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='block.getScope().mergeWith(this.getScope());\nreturn block;'"
	 * @generated
	 */
	Block mergeWithComposite(CompositeBlock block);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final int pos = this.getChildren().indexOf(b);\nif ((pos == (-1)))\n{\n\tthrow new &lt;%java.lang.RuntimeException%&gt;(\"Cannot merge composite\");\n}\nthis.getScope().mergeWith(b.getScope());\nb.setScope(null);\nthis.removeBlock(b);\nint _size = b.getChildren().size();\n&lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt; _greaterThanDoubleDot = new &lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt;(_size, 0, false);\nfor (final &lt;%java.lang.Integer%&gt; i : _greaterThanDoubleDot)\n{\n\tthis.getChildren().add(pos, b.getChildren().get((i).intValue()));\n}'"
	 * @generated
	 */
	void mergeChildComposite(CompositeBlock b);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Merge the children of this composite if possible.
	 * 
	 * @return true if the block has been changed
	 * @generated NOT
	 * FIXME : this procedure needs to be tested
	 * @deprecated use MergeCompositeBlocks from transforms plugin
	 * <!-- end-model-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean changed = false;\nfinal &lt;%gecos.blocks.Block%&gt;[] children = ((&lt;%gecos.blocks.Block%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(this.getChildren(), &lt;%gecos.blocks.Block%&gt;.class)).clone();\n&lt;%gecos.blocks.BasicBlock%&gt; prev = ((&lt;%gecos.blocks.BasicBlock%&gt;) null);\n&lt;%gecos.blocks.BasicBlock%&gt; lastChild = ((&lt;%gecos.blocks.BasicBlock%&gt;) null);\nfor (final &lt;%gecos.blocks.Block%&gt; child : children)\n{\n\tif ((child instanceof &lt;%gecos.blocks.BasicBlock%&gt;))\n\t{\n\t\tlastChild = ((&lt;%gecos.blocks.BasicBlock%&gt;)child);\n\t\tif ((prev != null))\n\t\t{\n\t\t\tfinal &lt;%fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder%&gt; il = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder%&gt;();\n\t\t\til.doSwitch(child);\n\t\t\tboolean _containsLabels = il.containsLabels();\n\t\t\tif (_containsLabels)\n\t\t\t{\n\t\t\t\tprev = ((&lt;%gecos.blocks.BasicBlock%&gt;)child);\n\t\t\t}\n\t\t\telse\n\t\t\t{\n\t\t\t\tfinal &lt;%gecos.blocks.BasicBlock%&gt; bb = ((&lt;%gecos.blocks.BasicBlock%&gt;)child);\n\t\t\t\t&lt;%gecos.instrs.Instruction%&gt;[] _clone = ((&lt;%gecos.instrs.Instruction%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(bb.getInstructions(), &lt;%gecos.instrs.Instruction%&gt;.class)).clone();\n\t\t\t\tfor (final &lt;%gecos.instrs.Instruction%&gt; instr : _clone)\n\t\t\t\t{\n\t\t\t\t\tprev.addInstruction(instr);\n\t\t\t\t}\n\t\t\t\tthis.removeBlock(child);\n\t\t\t\tchanged = true;\n\t\t\t}\n\t\t}\n\t\telse\n\t\t{\n\t\t\tfinal &lt;%fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder%&gt; ig = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder%&gt;();\n\t\t\tig.doSwitch(child);\n\t\t\tif (((ig.containsGotos() || ig.containsBreaks()) || ig.containsContinues()))\n\t\t\t{\n\t\t\t\tprev = null;\n\t\t\t}\n\t\t\telse\n\t\t\t{\n\t\t\t\tprev = ((&lt;%gecos.blocks.BasicBlock%&gt;)child);\n\t\t\t}\n\t\t}\n\t}\n\telse\n\t{\n\t\tif (((!&lt;%com.google.common.base.Objects%&gt;.equal(prev, lastChild)) &amp;&amp; (prev != null)))\n\t\t{\n\t\t\tif ((lastChild != null))\n\t\t\t{\n\t\t\t\tprev.getOutEdges().clear();\n\t\t\t\tprev.getOutEdges().addAll(lastChild.getOutEdges());\n\t\t\t\tlastChild = null;\n\t\t\t}\n\t\t}\n\t\tprev = null;\n\t}\n}\nreturn changed;'"
	 * @generated
	 */
	boolean mergeChildren();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getChildren().remove(b);'"
	 * @generated
	 */
	void removeBlock(Block b);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model childUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.addBlock(child, false);'"
	 * @generated
	 */
	void addBlock(Block child);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" toUnique="false" condUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _isEmpty = this.getChildren().isEmpty();\nif (_isEmpty)\n{\n\tfinal &lt;%gecos.blocks.BasicBlock%&gt; basicBlock = &lt;%gecos.blocks.BlocksFactory%&gt;.eINSTANCE.createBasicBlock();\n\tthis.addBlock(basicBlock);\n}\nfinal &lt;%gecos.blocks.Block%&gt; last = this.getLastChilld();\nreturn last.connectTo(to, &lt;%gecos.instrs.BranchType%&gt;.UNCONDITIONAL);'"
	 * @generated
	 */
	EList<ControlEdge> connectTo(Block to, BranchType cond);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.blocks.Block%&gt;&gt; _children = this.getChildren();\nint _size = this.getChildren().size();\nint _minus = (_size - 1);\nreturn _children.get(_minus);'"
	 * @generated
	 */
	Block getLastChilld();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model childUnique="false" nolinkUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if (((!nolink) &amp;&amp; (this.getChildren().size() &gt; 0)))\n{\n\tfinal &lt;%gecos.blocks.Block%&gt; prev = this.getLastChilld();\n\tprev.connectTo(child, &lt;%gecos.instrs.BranchType%&gt;.UNCONDITIONAL);\n}\nthis.getChildren().add(child);'"
	 * @generated
	 */
	void addBlock(Block child, boolean nolink);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" fromUnique="false" condUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _size = this.getChildren().size();\nboolean _equals = (_size == 0);\nif (_equals)\n{\n\tfinal &lt;%gecos.blocks.BasicBlock%&gt; basicBlock = &lt;%gecos.blocks.BlocksFactory%&gt;.eINSTANCE.createBasicBlock();\n\tthis.addBlock(basicBlock);\n}\nfinal &lt;%gecos.blocks.Block%&gt; first = this.getChildren().get(0);\nreturn first.connectFromBasic(from, cond);'"
	 * @generated
	 */
	ControlEdge connectFromBasic(BasicBlock from, BranchType cond);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.blocks.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _pathString = this.getPathString();\n&lt;%java.lang.String%&gt; _plus = (_pathString + \"[0:\");\nint _size = this.getChildren().size();\nint _minus = (_size - 1);\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + &lt;%java.lang.Integer%&gt;.valueOf(_minus));\nreturn (_plus_1 + \"]\");'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.blocks.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return super.getPathString(this.toShortString());'"
	 * @generated
	 */
	String getPathString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.blocks.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _number = this.getNumber();\nreturn (\"CB\" + &lt;%java.lang.Integer%&gt;.valueOf(_number));'"
	 * @generated
	 */
	String toShortString();

} // CompositeBlock
