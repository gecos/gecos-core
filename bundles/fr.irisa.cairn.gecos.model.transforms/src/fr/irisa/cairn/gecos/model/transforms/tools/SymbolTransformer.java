package fr.irisa.cairn.gecos.model.transforms.tools;

import java.util.Map;

import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import gecos.core.ProcedureSet;
import gecos.core.Symbol;
import gecos.instrs.Instruction;
import gecos.instrs.SimpleArrayInstruction;
import gecos.instrs.SymbolInstruction;

public abstract class SymbolTransformer extends BlockInstructionSwitch<Object> {
	
	protected ProcedureSet ps;
	protected Symbol sym;
	
	public SymbolTransformer(ProcedureSet ps,Symbol sym) {
		this.ps=ps;
		this.sym=sym;
	}
	protected static boolean VERBOSE=false;

	protected void debug(String mess) {
		if (VERBOSE) {
			System.out.println(mess);
		}
	}

	protected Map<Long, Symbol> transform() {
		debug("Transform symbol declaration for "+sym);
		transformSymbolDeclaration();
		debug("Transform symbol references to "+sym);
		doSwitch(ps);
		cleanup();
		return null;
	}

	protected abstract void transformSymbolDeclaration();
	protected abstract void transformArrayReference(SymbolInstruction object); //old version until 18/03/2010
	protected abstract void transformArrayReference(SimpleArrayInstruction object);	//new version 
	protected abstract void cleanup();

	@Override
	public Object caseSymbolInstruction(SymbolInstruction object) {
		if (object.getSymbol()==sym) {
			debug("Found symbol to transform in SymbolTransformer :"+object.getSymbol());
			transformArrayReference(object);
		} else {
			debug("symbol "+object.getSymbol()+"!="+sym+", skipping");
		}
		return super.caseSymbolInstruction(object);
	}
	 
	@Override
	public Object caseSimpleArrayInstruction(SimpleArrayInstruction object) {
		if (object.getSymbol()==sym) {
			debug("Found symbol to transform in SymbolTransformer :"+object.getSymbol());
			transformArrayReference(object);
		}
		for(Instruction inst : object.getIndex()){
			doSwitch(inst);
		}		
		return object;
	}


	
}
