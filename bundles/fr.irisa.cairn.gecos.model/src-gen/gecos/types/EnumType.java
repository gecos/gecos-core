/**
 */
package gecos.types;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enum Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.EnumType#getName <em>Name</em>}</li>
 *   <li>{@link gecos.types.EnumType#getEnumerators <em>Enumerators</em>}</li>
 * </ul>
 *
 * @see gecos.types.TypesPackage#getEnumType()
 * @model
 * @generated
 */
public interface EnumType extends Type, TypesVisitable {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see gecos.types.TypesPackage#getEnumType_Name()
	 * @model unique="false" dataType="gecos.types.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link gecos.types.EnumType#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Enumerators</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.types.Enumerator}.
	 * It is bidirectional and its opposite is '{@link gecos.types.Enumerator#getEnumType <em>Enum Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumerators</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumerators</em>' containment reference list.
	 * @see gecos.types.TypesPackage#getEnumType_Enumerators()
	 * @see gecos.types.Enumerator#getEnumType
	 * @model opposite="enumType" containment="true"
	 * @generated
	 */
	EList<Enumerator> getEnumerators();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitEnumType(this);'"
	 * @generated
	 */
	void accept(TypesVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.types.long" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.TypeSizes%&gt;.defaultIntSize;'"
	 * @generated
	 */
	long getSize();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.boolean" unique="false" otherUnique="false" ignoreStorageClassDataType="gecos.types.boolean" ignoreStorageClassUnique="false" ignoreConstDataType="gecos.types.boolean" ignoreConstUnique="false" ignoreVolatileDataType="gecos.types.boolean" ignoreVolatileUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((other == this))\n{\n\treturn true;\n}\nif ((other instanceof &lt;%gecos.types.EnumType%&gt;))\n{\n\treturn ((&lt;%gecos.types.EnumType%&gt;)other).getName().equals(this.getName());\n}\nreturn false;'"
	 * @generated
	 */
	boolean isEqual(Type other, boolean ignoreStorageClass, boolean ignoreConst, boolean ignoreVolatile);

} // EnumType
