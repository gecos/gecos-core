/**
 */
package profiling.frontend.directives.impl;

import fr.irisa.cairn.gecos.core.profiling.backend.MainProcedureCreator;
import fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport;

import fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport.NativeFunction;

import fr.irisa.cairn.gecos.core.profiling.frontend.InvalidDirective;

import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;

import gecos.blocks.Block;
import gecos.blocks.SwitchBlock;

import gecos.core.ProcedureSet;

import gecos.instrs.Instruction;

import java.util.ArrayList;
import java.util.Objects;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.xtext.xbase.lib.ExclusiveRange;

import profiling.frontend.directives.DirectiveName;
import profiling.frontend.directives.DirectivesPackage;
import profiling.frontend.directives.FromFileDirective;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>From File Directive</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link profiling.frontend.directives.impl.FromFileDirectiveImpl#getNAME <em>NAME</em>}</li>
 *   <li>{@link profiling.frontend.directives.impl.FromFileDirectiveImpl#getFilePath <em>File Path</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FromFileDirectiveImpl extends InjectorSourceDirectiveImpl implements FromFileDirective {
	/**
	 * The default value of the '{@link #getNAME() <em>NAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNAME()
	 * @generated
	 * @ordered
	 */
	protected static final DirectiveName NAME_EDEFAULT = DirectiveName.FROM_FILE;

	/**
	 * The cached value of the '{@link #getNAME() <em>NAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNAME()
	 * @generated
	 * @ordered
	 */
	protected DirectiveName name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFilePath() <em>File Path</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilePath()
	 * @generated
	 * @ordered
	 */
	protected Instruction filePath;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FromFileDirectiveImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DirectivesPackage.Literals.FROM_FILE_DIRECTIVE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DirectiveName getNAME() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNAME(DirectiveName newNAME) {
		DirectiveName oldNAME = name;
		name = newNAME == null ? NAME_EDEFAULT : newNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DirectivesPackage.FROM_FILE_DIRECTIVE__NAME, oldNAME, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getFilePath() {
		if (filePath != null && filePath.eIsProxy()) {
			InternalEObject oldFilePath = (InternalEObject)filePath;
			filePath = (Instruction)eResolveProxy(oldFilePath);
			if (filePath != oldFilePath) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DirectivesPackage.FROM_FILE_DIRECTIVE__FILE_PATH, oldFilePath, filePath));
			}
		}
		return filePath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction basicGetFilePath() {
		return filePath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFilePath(Instruction newFilePath) {
		Instruction oldFilePath = filePath;
		filePath = newFilePath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DirectivesPackage.FROM_FILE_DIRECTIVE__FILE_PATH, oldFilePath, filePath));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void parse() {
		final EList<Instruction> args = this.getCall().getArgs();
		int _size = args.size();
		boolean _lessThan = (_size < 1);
		if (_lessThan) {
			String _printLocation = this.printLocation();
			String _plus = (_printLocation + "\nMust have at least 1 arguments (filePath)!");
			throw new InvalidDirective(_plus);
		}
		this.setFilePath(Objects.<Instruction>requireNonNull(args.get(0)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block createCodeBlock() {
		Block _xblockexpression = null;
		{
			final ProcedureSet ps = this.getCall().getContainingProcedureSet();
			Block _xifexpression = null;
			int _length = ((Object[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(this.getCall().getArgs(), Object.class)).length;
			boolean _greaterThan = (_length > 1);
			if (_greaterThan) {
				SwitchBlock _xblockexpression_1 = null;
				{
					int _length_1 = ((Object[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(this.getCall().getArgs(), Object.class)).length;
					final ArrayList<Block> blocks = new ArrayList<Block>(_length_1);
					int _length_2 = ((Object[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(this.getCall().getArgs(), Object.class)).length;
					ExclusiveRange _doubleDotLessThan = new ExclusiveRange(0, _length_2, true);
					for (final Integer i : _doubleDotLessThan) {
						blocks.add(
							GecosUserBlockFactory.BBlock(
								GecosUserInstructionFactory.call(
									NativeProfilingEngineSupport.getNativeProc(ps, NativeFunction.INJECTOR_FROM_FILE), Objects.<Instruction>requireNonNull(this.getCall().getArgs().get((i).intValue()).copy())), 
								GecosUserInstructionFactory.breakInst()));
					}
					_xblockexpression_1 = GecosUserBlockFactory.Switch(ps.getScope().lookup(MainProcedureCreator.SIM_COUNT_INDEX_NAME), ((Block[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(blocks, Block.class)));
				}
				_xifexpression = _xblockexpression_1;
			}
			else {
				_xifexpression = GecosUserBlockFactory.BBlock(
					GecosUserInstructionFactory.call(
						NativeProfilingEngineSupport.getNativeProc(ps, NativeFunction.INJECTOR_FROM_FILE), 
						this.getFilePath()));
			}
			_xblockexpression = _xifexpression;
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DirectivesPackage.FROM_FILE_DIRECTIVE__NAME:
				return getNAME();
			case DirectivesPackage.FROM_FILE_DIRECTIVE__FILE_PATH:
				if (resolve) return getFilePath();
				return basicGetFilePath();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DirectivesPackage.FROM_FILE_DIRECTIVE__NAME:
				setNAME((DirectiveName)newValue);
				return;
			case DirectivesPackage.FROM_FILE_DIRECTIVE__FILE_PATH:
				setFilePath((Instruction)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DirectivesPackage.FROM_FILE_DIRECTIVE__NAME:
				setNAME(NAME_EDEFAULT);
				return;
			case DirectivesPackage.FROM_FILE_DIRECTIVE__FILE_PATH:
				setFilePath((Instruction)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DirectivesPackage.FROM_FILE_DIRECTIVE__NAME:
				return name != NAME_EDEFAULT;
			case DirectivesPackage.FROM_FILE_DIRECTIVE__FILE_PATH:
				return filePath != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (NAME: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //FromFileDirectiveImpl
