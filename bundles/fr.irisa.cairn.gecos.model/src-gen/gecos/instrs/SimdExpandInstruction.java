/**
 */
package gecos.instrs;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simd Expand Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.SimdExpandInstruction#getPermutation <em>Permutation</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getSimdExpandInstruction()
 * @model
 * @generated
 */
public interface SimdExpandInstruction extends SimdInstruction {
	/**
	 * Returns the value of the '<em><b>Permutation</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Permutation</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Permutation</em>' attribute list.
	 * @see gecos.instrs.InstrsPackage#getSimdExpandInstruction_Permutation()
	 * @model default="0" unique="false" dataType="gecos.instrs.int"
	 * @generated
	 */
	EList<Integer> getPermutation();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitSimdExpandInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

} // SimdExpandInstruction
