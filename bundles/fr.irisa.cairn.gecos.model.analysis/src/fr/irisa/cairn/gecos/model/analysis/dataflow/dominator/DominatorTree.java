/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.analysis.dataflow.dominator;

import gecos.blocks.BasicBlock;

import java.io.PrintStream;
import java.util.Map;

public class DominatorTree {

	private Map<BasicBlock, DominatorTreeNode> nodes;
	private DominatorTreeNode root;

	public DominatorTree(Map<BasicBlock, DominatorTreeNode> nodes, DominatorTreeNode root) {
		this.nodes = nodes;
		this.root = root;
	}
	
	public DominatorTreeNode root() { return root; }
	public DominatorTreeNode node(BasicBlock b) {
		return nodes.get(b);
	}

	public void outputDot(PrintStream out) {
		out.println("digraph G {");
		out.println("node [shape=box];");
		for (DominatorTreeNode node : nodes.values()) {
			out.println("n" + node.block().getNumber() + " [label=\""
					+ node.block().toString() + "\\n"
					+ node.block().contentString()
					+ "\"];");
			
			for (DominatorTreeNode succ : node.children()) {
				out.println("n" + node.block().getNumber() + " -> n" +
						succ.block().getNumber() + ";");
			}
		}
		out.println("}");
	}
}
