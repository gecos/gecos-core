package fr.irisa.cairn.gecos.core.transforms.tests;

import org.junit.Test;

import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.core.integration.tests.utils.DotAppDirData;
import fr.irisa.cairn.gecos.core.integration.tests.utils.DotAppDirDataTemplate;
import fr.irisa.cairn.gecos.model.transforms.loops.unroller.ModelForUnroller;
import fr.irisa.cairn.gecos.testframework.data.ICxxProjectData;
import fr.irisa.cairn.gecos.testframework.dataprovider.AddIntParams;
import fr.irisa.cairn.gecos.testframework.dataprovider.DataFromPathProvider;
import fr.irisa.cairn.gecos.testframework.dataprovider.ResourcesLocation;

/**
 * Tests {@link ModelForUnroller}
 * 
 * TODO use suitable benchmarks
 * 
 * @author aelmouss
 */
public class ForUnrollerIT extends DotAppDirDataTemplate {
	
	@Test
	@UseDataProvider(location=DataFromPathProvider.class, value=DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation(value = "resources/src-c/dot-app-data/idfix", dataClasses = DotAppDirData.class)
	@AddIntParams({ModelForUnroller.MODE_FULL, ModelForUnroller.MODE_PART, ModelForUnroller.MODE_PRAGMA})//TODO test other modes
	public void testFullUnroll(ICxxProjectData d, int unrollMode) {
		runTest(d, v -> new ModelForUnroller(v.getProject(), unrollMode).compute()); 
	}
	
}
