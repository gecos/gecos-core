package fr.irisa.cairn.gecos.model.c.generator;

import templates.xtend.GecosCompleteTypeTemplate;
import fr.irisa.cairn.gecos.model.extensions.generators.ExtendableGenerator;
import fr.irisa.cairn.gecos.model.extensions.generators.IGeneratorDispatchStrategy;

public class ExtendableCompleteTypeCGenerator extends ExtendableGenerator {

	public static ExtendableCompleteTypeCGenerator eInstance = new ExtendableCompleteTypeCGenerator();

	public ExtendableCompleteTypeCGenerator() {
		super(new GecosCompleteTypeTemplate());
	}
	
	public ExtendableCompleteTypeCGenerator(IGeneratorDispatchStrategy dispatchStrategy) {
		super(dispatchStrategy, new GecosCompleteTypeTemplate());
	}
}
