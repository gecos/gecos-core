/**
 */
package gecos.annotations;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Extended Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.annotations.ExtendedAnnotation#getFields <em>Fields</em>}</li>
 *   <li>{@link gecos.annotations.ExtendedAnnotation#getRefs <em>Refs</em>}</li>
 * </ul>
 *
 * @see gecos.annotations.AnnotationsPackage#getExtendedAnnotation()
 * @model
 * @generated
 */
public interface ExtendedAnnotation extends IAnnotation {
	/**
	 * Returns the value of the '<em><b>Fields</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fields</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fields</em>' containment reference list.
	 * @see gecos.annotations.AnnotationsPackage#getExtendedAnnotation_Fields()
	 * @model containment="true"
	 * @generated
	 */
	EList<EObject> getFields();

	/**
	 * Returns the value of the '<em><b>Refs</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Refs</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Refs</em>' reference list.
	 * @see gecos.annotations.AnnotationsPackage#getExtendedAnnotation_Refs()
	 * @model
	 * @generated
	 */
	EList<EObject> getRefs();

} // ExtendedAnnotation
