package fr.irisa.cairn.gecos.model.cdtfrontend.builder;

import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.Instruction;
import gecos.types.AliasType;
import gecos.types.EnumType;
import gecos.types.Enumerator;
import gecos.types.FunctionType;
import gecos.types.RecordType;
import gecos.types.Type;

/**
 * 
 * Interface for creating a procedureset in an easiest way because of context
 * 
 * @author mnaullet
 *
 */
public interface CoreBuilder {

	/**
	 * Create a {@link Procedure} with a name, the return{@link Type}, the parameterSymbols and add it to the procedureSet
	 * @param name
	 * @param returnType
	 * @param parameterSymbol
	 * @return the created procedure
	 */
	public Procedure addProcedure(ProcedureSymbol procedureSymbol);

	/**
	 * Add a compositeBlock to the current {@link Procedure}
	 * @param compositeBlock added to the current {@link Procedure}
	 */
	public void setBodyToCurrentProcedure(CompositeBlock compositeBlock);
	
	/**
	 * Create a new {@link ParameterSymbol} with the name and the type given in parameter
	 * @param name of the new {@link ParameterSymbol}
	 * @param type of the new {@link ParameterSymbol}
	 * @return 
	 */
	public ParameterSymbol createParameterSymbol(String name, Type type);
	
	/**
	 * Return a new {@link Symbol} with a name, a type, an instruction and add it to the current {@link Scope}.
	 * @param name of the created {@link Symbol}
	 * @param type of the created {@link Symbol}
	 * @param instruction of the created {@link Symbol}
	 * @return the symbol created
	 */
	public Symbol addSymbolToCurrentScope(String name, Type type, Instruction instruction);

//	/**
//	 * Create an {@link ProcedureSymbol} with a name, a type, a list of {@link ParameterSymbol}}.
//	 * @param procedureName of the created {@link ProcedureSymbol}
//	 * @param returnType of the created {@link ProcedureSymbol}
//	 * @param parameterSymbols of the created {@link ProcedureSymbol}
//	 */
//	public ProcedureSymbol addProcedureSymbol(String procedureName, Type returnType, List<ParameterSymbol> parameterSymbols, boolean hasElipsis);
	
	public ProcedureSymbol addProcedureSymbol(String name, FunctionType funcType, Scope procParamsScope);
	
	public ProcedureSymbol addProcedureSymbol(String name, FunctionType funcType);
	
	/**
	 * Return a new scope with the currentScope as parent and make it the new scope
	 * @return a new scope
	 */
	public Scope createScope();
	
	/**
	 * Return the {@link ControlStructureBuilder}
	 * @return the {@link ControlStructureBuilder}
	 */
	public ControlStructureBuilder getControlStructureBuilder();
	
	/**
	 * Return the symbol named name
	 * @param name of the symbol to look for
	 * @return the symbol named name
	 */
	public Symbol getSymbol(String name);


	/**
	 * Return the created procedureSet
	 * @return the created procedureSet
	 */
	public ProcedureSet getProcedureSet();
	
	/**
	 * Make the upper scope of the current scope as the new current scope
	 * If the current scope has no upper scope, nothing is done
	 */
	public void goToUpperScope();
	

	/**
	 * Return the current parent {@link Block}
	 * @return the current parent {@link Block}
	 */
	public Block getParentBlock();
	
	/**
	 *  set the current parent {@link Block}
	 * @param {@link Block}
	 */
	public void setParentBlock(Block block);
	
	/**
	 * Find an {@link Enumerator} named name in the closest {@link Scope}
	 * @param name
	 * @return an {@link Enumerator} named name in the closest {@link Scope}
	 */
	public Enumerator findEnumerator(String name);
	
	/**
	 * Find an {@link Enumerator} named name in the {@link EnumType}
	 * @param enumType
	 * @param name
	 * @return an {@link Enumerator} named name in the {@link EnumType}
	 */
	public Enumerator findEnumerator(EnumType enumType, String name);

	/**
	 * Find a {@link Symbol} named name in the closest {@link Scope}
	 * @param name
	 * @return a {@link Symbol} named name in the closest {@link Scope}
	 */
	public Symbol findSymbol(String name);
	
	/**
	 * Find a {@link Symbol} named name in the current {@link Scope}
	 * @param name
	 * @return a {@link Symbol} named name in the current {@link Scope}
	 */
	public Symbol findSymbolInCurrentScope(String name);
	
	/**
	 * Find a {@link Type} named name in the closest {@link Scope}
	 * @param name
	 * @return a {@link Type} named name in the closest {@link Scope}
	 */
	public Type findType(String name);
	
	/**
	 * Find a {@link AliasType} named name in the closest {@link Scope}
	 * @param name
	 * @return a {@link AliasType} named name in the closest {@link Scope}
	 */
	public AliasType findAliasType(String name);
	
	/**
	 * Find a {@link EnumType} named name in the closest {@link Scope}
	 * @param name
	 * @return a {@link EnumType} named name in the closest {@link Scope}
	 */
	public EnumType findEnumType(String name);
	
	/**
	 * Find a {@link RecordType} named name in the closest {@link Scope}
	 * @param name
	 * @return a {@link RecordType} named name in the closest {@link Scope}
	 */
	public RecordType findRecordType(String name);

	/**
	 * Find a {@link AliasType} named {@code name} in the current {@link Scope}
	 * @param name
	 * @return a {@link AliasType} named name in the current {@link Scope}
	 */
	public AliasType findAliasTypeInCurrentScope(String name);

	/**
	 * Check if {@code type} is defined in the {@link Scope}s hierarchy.
	 * @param type
	 * @return true if if {@code type} is defined in the {@link Scope}s hierarchy.
	 */
	public boolean isDefined(Type type);

	/**
	 * @param symbol {@link Symbol} to be checked
	 * @return true if {@code symbol} is contained in the current {@link Scope}. 
	 */
	public boolean isInCurrentScope(Symbol symbol);
	
	/**
	 * @param symbol {@link Symbol} to be checked
	 * @return true if {@code symbol} is contained in the global {@link Scope}. 
	 */
	public boolean isInGlobalScope(Symbol existingSymbol);

	/**
	 * @param type {@link Type} to be checked
	 * @return true if {@code type} is contained in the current {@link Scope}. 
	 */
	public boolean isInCurrentScope(Type type);

	/**
	 * Removes {@code type} from the current {@link Scope}
	 * @param type
	 */
	public void removeFromCurrentScope(Type type);

	/**
	 * Add {@code type} to current scope.
	 * @param type
	 */
	public void addToCurrentScope(Type type);

}