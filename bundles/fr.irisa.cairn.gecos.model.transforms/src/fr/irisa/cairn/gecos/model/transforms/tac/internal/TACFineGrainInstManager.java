package fr.irisa.cairn.gecos.model.transforms.tac.internal;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.tools.switches.DefaultInstructionSwitch;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.CallInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.RetInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SimpleArrayInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;

@Deprecated
class TACFineGrainInstManager  extends DefaultInstructionSwitch<Object>{
	private TACFineGrainBlockManager _blockManager;
	private List<SetInstruction> _listInst;
	private Map<Instruction, List<Instruction>> _map;
	private _TypeChecking _typeChecking;
	
	private final int LHS = 1;
	private final int RHS = 2;
	private int _mode;
	
	public TACFineGrainInstManager(TACFineGrainBlockManager blockManager){
		_listInst = new LinkedList<SetInstruction>();
		_map = new LinkedHashMap<Instruction, List<Instruction>>();
		_blockManager = blockManager;
		_typeChecking = new _TypeChecking();
	}
	
	private void myassert(boolean b, String s){
		if (!b){
			throw new RuntimeException(s);
		}
	}
	
	public Map<Instruction, List<Instruction>> getMap(){
		return _map;
	}
	
	private SetInstruction createNewSimpleSetInstruction(Instruction inst){
		// Création du nom temporaire de la variable
		// Une vérification est faites pour choisir un nom de symbol non utilisé dans le scope courant
		Scope scopeCurrent = inst.getBasicBlock().getScope();
		StringBuilder sBuilder = new StringBuilder();
		for(Symbol symb : scopeCurrent.lookupAllSymbols()){
			sBuilder.append(symb.getName());
		}
		Pattern pattern;
		Matcher matcher;
		
		String nameSymbol = inst.getBasicBlock().getContainingProcedure().getSymbol().getName();
		do{
			nameSymbol += "_temp";
			pattern = Pattern.compile(nameSymbol+"\\d+");
			matcher = pattern.matcher(sBuilder);
		}while(matcher.find());
		int temp = _blockManager.useNumberTmpVariable();
		nameSymbol += temp;
		
		SetInstruction res = null;
		
		Type type;
		if((type = _typeChecking.doSwitch(inst.getType())) == null)
			type = inst.getType();
		
		Symbol destSymbol = GecosUserCoreFactory.symbol(nameSymbol, type, _blockManager.getNewScope());
		SymbolInstruction dest = GecosUserInstructionFactory.symbref(destSymbol);
		res = GecosUserInstructionFactory.set(dest, inst.copy());
		
		return res;
	}
	
	@Override
	public Object caseSetInstruction(SetInstruction g){
		_mode = RHS;
		Object o = doSwitch(g.getSource());
		if(o instanceof SetInstruction){
			SetInstruction setInst = (SetInstruction) o;
			_listInst.add(setInst);
			g.setSource(setInst.getDest().copy());
		}
		
		_map.put(g, new ArrayList<Instruction>(_listInst));
		_listInst.clear();
		_mode = LHS;
		return null;
	}
	
	@Override
	public Object caseGenericInstruction(GenericInstruction g){
		for(Instruction inst : g.getOperands()){
			Object o = doSwitch(inst);
			if(o instanceof SetInstruction){
				_listInst.add((SetInstruction) o);
				g.getOperands().set(g.getOperands().indexOf(inst), _listInst.get(_listInst.size()-1).getDest().copy());
			}
		}
		SetInstruction res = this.createNewSimpleSetInstruction(g);
		myassert(res.getDest() instanceof SymbolInstruction, "The dest of the setInstruction is not a symbolInstruction");
		
		return res;
	}
	
	@Override
	public Object caseIntInstruction(IntInstruction g){
//		return this.createNewSimpleSetInstruction(g);
		return null;
	}
	
	@Override
	public Object caseFloatInstruction(FloatInstruction g){
//		return this.createNewSimpleSetInstruction(g);
		return null;
	}
	
	@Override
	public Object caseSymbolInstruction(SymbolInstruction g){
//		return this.createNewSimpleSetInstruction(g);
		return null;
	}
	
	@Override
	public Object caseSimpleArrayInstruction(SimpleArrayInstruction g){
//		return this.createNewSimpleSetInstruction(g);
		return null;
	}

	@Override
	public Object caseCallInstruction(CallInstruction g){
		if(_mode == RHS){
			for(Instruction inst : g.getArgs()){
				Object o = doSwitch(inst);
				if(o instanceof SetInstruction){
					SetInstruction setInst = (SetInstruction) o;
					_listInst.add(setInst);
//					getChildren().set(g.getChildren().indexOf(inst), setInst.getDest().copy());
					g.replaceChild(inst, setInst.getDest().copy());
				}
				
			}
			return this.createNewSimpleSetInstruction(g);
		}
		else{
			return null;
		}
	}
	
	@Override
	public Object caseConvertInstruction(ConvertInstruction g){
		doSwitch(g.getExpr());
		return this.createNewSimpleSetInstruction(g);
	}
	
	@Override
	public Object caseRetInstruction(RetInstruction g){
//		if(!(g.getExpr() instanceof FloatInstruction) && !(g.getExpr() instanceof IntInstruction) && !(g.getExpr() instanceof SimpleArrayInstruction) && !(g.getExpr() instanceof SymbolInstruction)){
//			throw new RuntimeException("The output of the system is not a simple instruction");
//		}
		doSwitch(g.getExpr());
		return null;
	}
	
	private class _TypeChecking /*extends DefaultTypeSwitch<Type>*/{
		
		public Type doSwitch(Type t) {
			if(t.isConstant() || t.isVolatile() || t.getStorageClass().equals(StorageClassSpecifiers.REGISTER) || t.getStorageClass().equals(StorageClassSpecifiers.STATIC))
				return t;
			return null;
		}
		
//		@Override
//		public Type caseStaticType(StaticType object) {
//			return object.getBase();
//		}
//
//		@Override
//		public Type caseRegisterType(RegisterType object) {
//			return object.getBase();
//		}
//
//		@Override
//		public Type caseVolatileType(VolatileType object) {
//			return object.getBase();
//		}
//
//		@Override
//		public Type caseConstType(ConstType object) {
//			return object.getBase();
//		}
		
	}
}
