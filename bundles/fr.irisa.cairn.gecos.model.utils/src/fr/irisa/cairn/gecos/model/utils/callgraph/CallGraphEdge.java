/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.utils.callgraph;


public class CallGraphEdge {

	private CallGraphNode from;
	private CallGraphNode to;

	public CallGraphEdge(CallGraphNode from, CallGraphNode to) {
		this.from = from;
		this.to = to;
	}

	public CallGraphNode to() { return to; }
	public CallGraphNode from() { return from; }
	
	public Object accept(CallGraphVisitor visitor) {
		return visitor.visitCallGraphEdge(this);
	}

}
