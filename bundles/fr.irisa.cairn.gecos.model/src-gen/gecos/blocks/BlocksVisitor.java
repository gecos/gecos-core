/**
 */
package gecos.blocks;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visitor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.blocks.BlocksPackage#getBlocksVisitor()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface BlocksVisitor extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bUnique="false"
	 * @generated
	 */
	void visitBasicBlock(BasicBlock b);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model cUnique="false"
	 * @generated
	 */
	void visitCompositeBlock(CompositeBlock c);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model cUnique="false"
	 * @generated
	 */
	void visitFlowEdge(FlowEdge c);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model cUnique="false"
	 * @generated
	 */
	void visitControlEdge(ControlEdge c);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model cUnique="false"
	 * @generated
	 */
	void visitDataEdge(DataEdge c);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model fUnique="false"
	 * @generated
	 */
	void visitForBlock(ForBlock f);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model iUnique="false"
	 * @generated
	 */
	void visitIfBlock(IfBlock i);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model wUnique="false"
	 * @generated
	 */
	void visitWhileBlock(WhileBlock w);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model lUnique="false"
	 * @generated
	 */
	void visitLoopBlock(DoWhileBlock l);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model sUnique="false"
	 * @generated
	 */
	void visitSwitchBlock(SwitchBlock s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model cUnique="false"
	 * @generated
	 */
	void visitCaseBlock(CaseBlock c);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model fUnique="false"
	 * @generated
	 */
	void visitForC99Block(ForC99Block f);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model fUnique="false"
	 * @generated
	 */
	void visitSimpleForBlock(SimpleForBlock f);

} // BlocksVisitor
