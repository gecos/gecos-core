/**
 */
package gecos.instrs;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simd Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.instrs.InstrsPackage#getSimdInstruction()
 * @model
 * @generated
 */
public interface SimdInstruction extends ChildrenListInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitSimdInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.SimdInstruction%&gt;))\n{\n\treturn super.isSame(instr);\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // SimdInstruction
