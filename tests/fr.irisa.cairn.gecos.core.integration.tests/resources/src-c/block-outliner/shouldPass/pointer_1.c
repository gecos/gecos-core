int main() {
	int A[10] = {3, 7, 11};
	int *pA;
	int x;

	#pragma outline
	{
		int tmp = A[1];
		x = tmp;
	} //x = A[1]

	#pragma outline
	{
		pA = A;
		x += *pA;
		x += *(pA+2);
	} //x = A[1] + A[0] + A[2];

	//-- test --
	if(pA != A)
		abort();
	if(x != A[1] + A[0] + A[2])
		abort();

	return 0;
}
