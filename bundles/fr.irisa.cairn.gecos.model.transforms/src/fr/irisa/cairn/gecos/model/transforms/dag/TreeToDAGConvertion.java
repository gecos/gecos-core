/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.transforms.dag;

import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.gecos.model.tools.switches.BasicBlockSwitch;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import gecos.blocks.BasicBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;

@GSModule("Transforms the instrutions Tree representation of \n"
		+ "a BasicBlock (BB), in the CDFG, into a Direct Acyclic Graph (DAG).\n"
		+ "By default, this will be applied on all BBs in the\n"
		+ "specified Gecos Object except those used by control structures\n"
		+ "such as for loop's inititialization, step and test blocks.\n"
		+ "However, it is possible to apply on ALL basicBlocks by\n"
		+ "setting the corresponding flag.\n"
		+ "\nNOTE: after this transformation a converted BB will contain a\n"
		+ "single Instruction of type DAGInstruction.\n"
		+ "\nSee: 'GecosDAGToTreeIRConversion' module to revert back this transformation.")
public class TreeToDAGConvertion extends BasicBlockSwitch<Object> {

	private static final boolean VERBOSE = false;
	
	private static final void debug(String string) {
		if (VERBOSE) System.out.println(string);
	}
	
	private boolean transformAllBasicblocks = false;
	private TreeToDAGInstructionBuilder builder;
	private EObject object;
	
	
	@GSModuleConstructor(
		"-arg1: Object to convert. It should be either \n"
	  + "   a GecosProject, ProcedureSet or a Procedure.\n"
	  + "-arg2: if set to 'true' the ALL basic blocks will be converted")
	public TreeToDAGConvertion(EObject gecosObject, boolean convertAllBasicBlocks) {
		this.object = gecosObject;
		this.builder = new TreeToDAGInstructionBuilder();
		this.transformAllBasicblocks = convertAllBasicBlocks;
	}
	
	@GSModuleConstructor("Blocks used by control structures such as for loop's\n"
			+ "inititialization, step and test blocks will not be converted.")
	public TreeToDAGConvertion(EObject gecosObject) {
		this(gecosObject, false);
	}

	
	public void compute() {
		if (object instanceof GecosProject)
			computeProj((GecosProject) object);
		else if (object instanceof ProcedureSet)
			computePS((ProcedureSet) object);
		else if (object instanceof Procedure)
			computeProc((Procedure) object);
		else
			System.err.println("[TreeToDAGConvertion] The specified object should be either a"
					+ "GecosProject, ProcedureSet or a Procedure. Abort.");
	}
	
	private void computeProj(GecosProject project) {
		for (GecosSourceFile file : project.getSources()) {
			computePS(file.getModel());
		}
	}
	
	private void computePS(ProcedureSet ps) {
		for (Procedure proc : ps.listProcedures()) {
			doSwitch(proc.getBody());
		}
	}

	private void computeProc(Procedure proc) {
		doSwitch(proc.getBody());
	}


	@Override
	public Object caseForBlock(ForBlock b) {
		if (transformAllBasicblocks) {
			return super.caseForBlock(b);
		} else {
			if (VERBOSE) debug("For block of the DAGBuilder is called");
			doSwitch(b.getBodyBlock());
		}
		return null;
	}

	@Override
	public Object caseDoWhileBlock(DoWhileBlock b) {
		if (transformAllBasicblocks) {
			return super.caseDoWhileBlock(b);
		} else {
			if (VERBOSE) debug("Loop block of the DAGBuilder is called");
			if (b.getBodyBlock() != null) {
				doSwitch(b.getBodyBlock());
			}
		}
		return null;
	}

	@Override
	public Object caseWhileBlock(WhileBlock b) {
		if (transformAllBasicblocks) {
			return super.caseWhileBlock(b);
		} else {
			if (VERBOSE) debug("While block of the DAGBuilder is called");
			if (b.getBodyBlock() != null) {
				doSwitch(b.getBodyBlock());
			}
		}
		return null;
	}

	@Override
	public Object caseBasicBlock(BasicBlock b) {
		if (VERBOSE) debug("Basic Block of the DAGBuilder is called: " + b);
		try {
			if(!b.isDAG())
				builder.build(b);
		} catch (UnsupportedOperationException e) {
			System.err.println(new Exception().getStackTrace()[0] + ": Tree To DAG Conversion failed for: " + b);
		}
		
		return null;
	}

	@Override
	public Object caseIfBlock(IfBlock b) {
		if (transformAllBasicblocks) {
			return super.caseIfBlock(b);
		} else {
			if (VERBOSE) debug("If block of the DAGBuilder is called");
			if (b.getThenBlock() != null)
				doSwitch(b.getThenBlock());
			if (b.getElseBlock() != null)
				doSwitch(b.getElseBlock());
		}
		return null;
	}
}
