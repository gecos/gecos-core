/**
 */
package gecos.gecosproject;

import gecos.core.CorePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see gecos.gecosproject.GecosprojectFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel modelName='Gecos' modelPluginClass='' creationCommands='false' creationIcons='false' interfaceNamePattern='' importerID='org.eclipse.emf.importer.ecore' operationReflection='false' basePackage='gecos'"
 * @generated
 */
public interface GecosprojectPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "gecosproject";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.irisa.fr/cairn/gecosproject";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "gecosproject";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GecosprojectPackage eINSTANCE = gecos.gecosproject.impl.GecosprojectPackageImpl.init();

	/**
	 * The meta object id for the '{@link gecos.gecosproject.ProjectVisitor <em>Project Visitor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.gecosproject.ProjectVisitor
	 * @see gecos.gecosproject.impl.GecosprojectPackageImpl#getProjectVisitor()
	 * @generated
	 */
	int PROJECT_VISITOR = 0;

	/**
	 * The number of structural features of the '<em>Project Visitor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT_VISITOR_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link gecos.gecosproject.ProjectVisitable <em>Project Visitable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.gecosproject.ProjectVisitable
	 * @see gecos.gecosproject.impl.GecosprojectPackageImpl#getProjectVisitable()
	 * @generated
	 */
	int PROJECT_VISITABLE = 1;

	/**
	 * The number of structural features of the '<em>Project Visitable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT_VISITABLE_FEATURE_COUNT = CorePackage.GECOS_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.gecosproject.impl.StringToStringMapImpl <em>String To String Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.gecosproject.impl.StringToStringMapImpl
	 * @see gecos.gecosproject.impl.GecosprojectPackageImpl#getStringToStringMap()
	 * @generated
	 */
	int STRING_TO_STRING_MAP = 2;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_STRING_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_STRING_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>String To String Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_STRING_MAP_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link gecos.gecosproject.impl.GecosProjectImpl <em>Gecos Project</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.gecosproject.impl.GecosProjectImpl
	 * @see gecos.gecosproject.impl.GecosprojectPackageImpl#getGecosProject()
	 * @generated
	 */
	int GECOS_PROJECT = 3;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_PROJECT__ANNOTATIONS = PROJECT_VISITABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_PROJECT__NAME = PROJECT_VISITABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Includes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_PROJECT__INCLUDES = PROJECT_VISITABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Sources</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_PROJECT__SOURCES = PROJECT_VISITABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Macros</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_PROJECT__MACROS = PROJECT_VISITABLE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Gecos Project</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_PROJECT_FEATURE_COUNT = PROJECT_VISITABLE_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link gecos.gecosproject.impl.GecosFileImpl <em>Gecos File</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.gecosproject.impl.GecosFileImpl
	 * @see gecos.gecosproject.impl.GecosprojectPackageImpl#getGecosFile()
	 * @generated
	 */
	int GECOS_FILE = 4;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_FILE__ANNOTATIONS = PROJECT_VISITABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_FILE__NAME = PROJECT_VISITABLE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Gecos File</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_FILE_FEATURE_COUNT = PROJECT_VISITABLE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.gecosproject.impl.GecosSourceFileImpl <em>Gecos Source File</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.gecosproject.impl.GecosSourceFileImpl
	 * @see gecos.gecosproject.impl.GecosprojectPackageImpl#getGecosSourceFile()
	 * @generated
	 */
	int GECOS_SOURCE_FILE = 5;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SOURCE_FILE__ANNOTATIONS = GECOS_FILE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SOURCE_FILE__NAME = GECOS_FILE__NAME;

	/**
	 * The feature id for the '<em><b>Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SOURCE_FILE__MODEL = GECOS_FILE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Gecos Source File</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_SOURCE_FILE_FEATURE_COUNT = GECOS_FILE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.gecosproject.impl.GecosHeaderDirectoryImpl <em>Gecos Header Directory</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.gecosproject.impl.GecosHeaderDirectoryImpl
	 * @see gecos.gecosproject.impl.GecosprojectPackageImpl#getGecosHeaderDirectory()
	 * @generated
	 */
	int GECOS_HEADER_DIRECTORY = 6;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_HEADER_DIRECTORY__ANNOTATIONS = GECOS_FILE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_HEADER_DIRECTORY__NAME = GECOS_FILE__NAME;

	/**
	 * The feature id for the '<em><b>Is Standard Header</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_HEADER_DIRECTORY__IS_STANDARD_HEADER = GECOS_FILE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Gecos Header Directory</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_HEADER_DIRECTORY_FEATURE_COUNT = GECOS_FILE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '<em>String</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see gecos.gecosproject.impl.GecosprojectPackageImpl#getString()
	 * @generated
	 */
	int STRING = 7;


	/**
	 * Returns the meta object for class '{@link gecos.gecosproject.ProjectVisitor <em>Project Visitor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Project Visitor</em>'.
	 * @see gecos.gecosproject.ProjectVisitor
	 * @generated
	 */
	EClass getProjectVisitor();

	/**
	 * Returns the meta object for class '{@link gecos.gecosproject.ProjectVisitable <em>Project Visitable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Project Visitable</em>'.
	 * @see gecos.gecosproject.ProjectVisitable
	 * @generated
	 */
	EClass getProjectVisitable();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>String To String Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String To String Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyUnique="false" keyDataType="gecos.gecosproject.String"
	 *        valueUnique="false" valueDataType="gecos.gecosproject.String"
	 * @generated
	 */
	EClass getStringToStringMap();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToStringMap()
	 * @generated
	 */
	EAttribute getStringToStringMap_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToStringMap()
	 * @generated
	 */
	EAttribute getStringToStringMap_Value();

	/**
	 * Returns the meta object for class '{@link gecos.gecosproject.GecosProject <em>Gecos Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Gecos Project</em>'.
	 * @see gecos.gecosproject.GecosProject
	 * @generated
	 */
	EClass getGecosProject();

	/**
	 * Returns the meta object for the attribute '{@link gecos.gecosproject.GecosProject#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see gecos.gecosproject.GecosProject#getName()
	 * @see #getGecosProject()
	 * @generated
	 */
	EAttribute getGecosProject_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.gecosproject.GecosProject#getIncludes <em>Includes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Includes</em>'.
	 * @see gecos.gecosproject.GecosProject#getIncludes()
	 * @see #getGecosProject()
	 * @generated
	 */
	EReference getGecosProject_Includes();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.gecosproject.GecosProject#getSources <em>Sources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sources</em>'.
	 * @see gecos.gecosproject.GecosProject#getSources()
	 * @see #getGecosProject()
	 * @generated
	 */
	EReference getGecosProject_Sources();

	/**
	 * Returns the meta object for the map '{@link gecos.gecosproject.GecosProject#getMacros <em>Macros</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Macros</em>'.
	 * @see gecos.gecosproject.GecosProject#getMacros()
	 * @see #getGecosProject()
	 * @generated
	 */
	EReference getGecosProject_Macros();

	/**
	 * Returns the meta object for class '{@link gecos.gecosproject.GecosFile <em>Gecos File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Gecos File</em>'.
	 * @see gecos.gecosproject.GecosFile
	 * @generated
	 */
	EClass getGecosFile();

	/**
	 * Returns the meta object for the attribute '{@link gecos.gecosproject.GecosFile#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see gecos.gecosproject.GecosFile#getName()
	 * @see #getGecosFile()
	 * @generated
	 */
	EAttribute getGecosFile_Name();

	/**
	 * Returns the meta object for class '{@link gecos.gecosproject.GecosSourceFile <em>Gecos Source File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Gecos Source File</em>'.
	 * @see gecos.gecosproject.GecosSourceFile
	 * @generated
	 */
	EClass getGecosSourceFile();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.gecosproject.GecosSourceFile#getModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Model</em>'.
	 * @see gecos.gecosproject.GecosSourceFile#getModel()
	 * @see #getGecosSourceFile()
	 * @generated
	 */
	EReference getGecosSourceFile_Model();

	/**
	 * Returns the meta object for class '{@link gecos.gecosproject.GecosHeaderDirectory <em>Gecos Header Directory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Gecos Header Directory</em>'.
	 * @see gecos.gecosproject.GecosHeaderDirectory
	 * @generated
	 */
	EClass getGecosHeaderDirectory();

	/**
	 * Returns the meta object for the attribute '{@link gecos.gecosproject.GecosHeaderDirectory#isIsStandardHeader <em>Is Standard Header</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Standard Header</em>'.
	 * @see gecos.gecosproject.GecosHeaderDirectory#isIsStandardHeader()
	 * @see #getGecosHeaderDirectory()
	 * @generated
	 */
	EAttribute getGecosHeaderDirectory_IsStandardHeader();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>String</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getString();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	GecosprojectFactory getGecosprojectFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link gecos.gecosproject.ProjectVisitor <em>Project Visitor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.gecosproject.ProjectVisitor
		 * @see gecos.gecosproject.impl.GecosprojectPackageImpl#getProjectVisitor()
		 * @generated
		 */
		EClass PROJECT_VISITOR = eINSTANCE.getProjectVisitor();

		/**
		 * The meta object literal for the '{@link gecos.gecosproject.ProjectVisitable <em>Project Visitable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.gecosproject.ProjectVisitable
		 * @see gecos.gecosproject.impl.GecosprojectPackageImpl#getProjectVisitable()
		 * @generated
		 */
		EClass PROJECT_VISITABLE = eINSTANCE.getProjectVisitable();

		/**
		 * The meta object literal for the '{@link gecos.gecosproject.impl.StringToStringMapImpl <em>String To String Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.gecosproject.impl.StringToStringMapImpl
		 * @see gecos.gecosproject.impl.GecosprojectPackageImpl#getStringToStringMap()
		 * @generated
		 */
		EClass STRING_TO_STRING_MAP = eINSTANCE.getStringToStringMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_TO_STRING_MAP__KEY = eINSTANCE.getStringToStringMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_TO_STRING_MAP__VALUE = eINSTANCE.getStringToStringMap_Value();

		/**
		 * The meta object literal for the '{@link gecos.gecosproject.impl.GecosProjectImpl <em>Gecos Project</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.gecosproject.impl.GecosProjectImpl
		 * @see gecos.gecosproject.impl.GecosprojectPackageImpl#getGecosProject()
		 * @generated
		 */
		EClass GECOS_PROJECT = eINSTANCE.getGecosProject();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GECOS_PROJECT__NAME = eINSTANCE.getGecosProject_Name();

		/**
		 * The meta object literal for the '<em><b>Includes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GECOS_PROJECT__INCLUDES = eINSTANCE.getGecosProject_Includes();

		/**
		 * The meta object literal for the '<em><b>Sources</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GECOS_PROJECT__SOURCES = eINSTANCE.getGecosProject_Sources();

		/**
		 * The meta object literal for the '<em><b>Macros</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GECOS_PROJECT__MACROS = eINSTANCE.getGecosProject_Macros();

		/**
		 * The meta object literal for the '{@link gecos.gecosproject.impl.GecosFileImpl <em>Gecos File</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.gecosproject.impl.GecosFileImpl
		 * @see gecos.gecosproject.impl.GecosprojectPackageImpl#getGecosFile()
		 * @generated
		 */
		EClass GECOS_FILE = eINSTANCE.getGecosFile();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GECOS_FILE__NAME = eINSTANCE.getGecosFile_Name();

		/**
		 * The meta object literal for the '{@link gecos.gecosproject.impl.GecosSourceFileImpl <em>Gecos Source File</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.gecosproject.impl.GecosSourceFileImpl
		 * @see gecos.gecosproject.impl.GecosprojectPackageImpl#getGecosSourceFile()
		 * @generated
		 */
		EClass GECOS_SOURCE_FILE = eINSTANCE.getGecosSourceFile();

		/**
		 * The meta object literal for the '<em><b>Model</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GECOS_SOURCE_FILE__MODEL = eINSTANCE.getGecosSourceFile_Model();

		/**
		 * The meta object literal for the '{@link gecos.gecosproject.impl.GecosHeaderDirectoryImpl <em>Gecos Header Directory</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.gecosproject.impl.GecosHeaderDirectoryImpl
		 * @see gecos.gecosproject.impl.GecosprojectPackageImpl#getGecosHeaderDirectory()
		 * @generated
		 */
		EClass GECOS_HEADER_DIRECTORY = eINSTANCE.getGecosHeaderDirectory();

		/**
		 * The meta object literal for the '<em><b>Is Standard Header</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GECOS_HEADER_DIRECTORY__IS_STANDARD_HEADER = eINSTANCE.getGecosHeaderDirectory_IsStandardHeader();

		/**
		 * The meta object literal for the '<em>String</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see gecos.gecosproject.impl.GecosprojectPackageImpl#getString()
		 * @generated
		 */
		EDataType STRING = eINSTANCE.getString();

	}

} //GecosprojectPackage
