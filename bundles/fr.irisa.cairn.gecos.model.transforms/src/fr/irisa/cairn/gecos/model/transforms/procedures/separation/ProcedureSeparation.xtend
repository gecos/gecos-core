package fr.irisa.cairn.gecos.model.transforms.procedures.separation

import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory
import fr.irisa.cairn.tools.ecore.query.EMFUtils
import fr.irisa.r2d2.gecos.framework.GSModule
import gecos.core.ISymbolUse
import gecos.core.Procedure
import gecos.core.Scope
import gecos.core.Symbol
import gecos.gecosproject.GecosProject
import gecos.gecosproject.GecosSourceFile
import gecos.types.AliasType
import gecos.types.ArrayType
import gecos.types.FunctionType
import gecos.types.PtrType
import gecos.types.RecordType
import gecos.types.StorageClassSpecifiers
import gecos.types.Type
import java.util.List

@GSModule("Move procedures to a new file based on the procedure name.")
class ProcedureSeparation {
	GecosProject proj
	List<Procedure> procs

	new(GecosProject proj) {
		this.proj = proj
		this.procs = proj.listProcedures()
	}
	
	new(GecosProject proj, List<Procedure> procs) {
		this.proj = proj
		this.procs = procs
	}
	
	new(GecosProject proj, Procedure proc) {
		this(proj, newArrayList(proc))
	}

	def void compute() {
		for (proc : procs) {
			val file = proc.containingProcedureSet.eContainer as GecosSourceFile
			if (file.toString != proc.symbol.name + ".c") {
				moveProcToNewFile(proc)				
			}
		}
	}
	
	private def void moveProcToNewFile(Procedure proc) {
		val procSym = proc.symbol
		val ps = proc.containingProcedureSet
		val newFile = GecosUserCoreFactory.source(procSym.name + ".c", GecosUserCoreFactory.procedureSet)
		proj.sources.add(newFile);
		val newPS = newFile.getModel();
		
		val extSymRefs = EMFUtils.eAllContentsInstancesOf(proc.body, ISymbolUse).filter[it !== null]
								 .map[it.usedSymbol].toSet.filter[isDeclaredOutside(proc)].toSet
		
		// Move procedure to new ProcedureSet and replace uses with a symbol
		procSym.type.innermostStorageClass = StorageClassSpecifiers.NONE
		val procSymRef = GecosUserCoreFactory.procSymbol(proc.symbolName, procSym.type as FunctionType)
		EMFUtils.substituteByNewObjectInContainer(proc.symbol, procSymRef)
		EMFUtils.eAllContentsInstancesOf(ps, ISymbolUse).filter[it.usedSymbol == procSym].forEach[it.symbol = procSymRef]
		newPS.addProcedure(proc)		
		
		// Copy function symbols references
		for (extSym : extSymRefs) {
			extSym.type.innermostStorageClass = StorageClassSpecifiers.NONE
			val copyType = copyTypeInScope(extSym.type, newPS.scope)
			val copySym = GecosUserCoreFactory.symbol(extSym.name, copyType)
			newPS.addSymbol(copySym)
			copySym.type.innermostStorageClass = StorageClassSpecifiers.EXTERN
		}
		
		// Copy globally defined types
		ps.scope.types.filter[it instanceof AliasType || it instanceof RecordType].toSet.forEach[newPS.scope.types.add(it.copy)]
	}

	def private boolean isDeclaredOutside(Symbol sym, Procedure proc) {
		if (sym === null)
			return false
		if (proc.listParameters.contains(sym))
			return false
		return proc.scope.parent.hasInScope(sym)
	}

	def private dispatch Type copyTypeInScope(Type type, Scope scope) {
		val copy = type.copy
		scope.types.add(copy)
		copy
	}

	def private dispatch Type copyTypeInScope(ArrayType type, Scope scope) {
		val copy = type.copy as ArrayType
		scope.types.add(copy)
		if (type.base !== null)
			copy.base = copyTypeInScope(type.base, scope)
		copy
	}

	def private dispatch Type copyTypeInScope(PtrType type, Scope scope) {
		val copy = type.copy as ArrayType
		scope.types.add(copy)
		if (type.base !== null)
			copy.base = copyTypeInScope(type.base, scope)
		copy
	}

	def private dispatch Type copyTypeInScope(FunctionType type, Scope scope) {
		val copy = type.copy as FunctionType
		scope.types.add(copy)
		if (type.returnType !== null)
			copy.returnType = copyTypeInScope(type.returnType, scope)
		copy
	}
}
