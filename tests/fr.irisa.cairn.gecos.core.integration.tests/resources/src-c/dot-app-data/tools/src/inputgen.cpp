#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>


/*
 * Randonly generates MxN float numbers with 'isfloat' decimal digits in the range [Min, Max) if 'isfloat'.
 * Otherwise, it generates integers in [Min, Max]
 * If N=0 => generates 1d vector of size M
 */
int main(int argc, const char **argv) {
	if(argc < 6) {
		printf("inputgen M [N,0] float? min max\n");
		return -1;
	}

	int m = atoi(argv[1]);
	int n = atoi(argv[2]);
	int isfloat = atoi(argv[3]);
	int min = atoi(argv[4]);
	int max = atoi(argv[5]);

	if(n<0 || m<0){
		printf("M and N cannot be negative!");
		return -1;
	}


	int i,j;

	srand(getpid() + time(NULL));
	srand48(getpid() + time(NULL));

	printf("%d\n", (n > 0? 2 : 1));
	printf("%d\n", m);
	if(n > 0) printf("%d\n", n);
	else n = 1;

	for(i=0; i<m; i++) {
		for(j=0; j<n; j++) {
			if(isfloat)
				//drand48 = [0, 1) => value = drand48 * width + min; width = max-min
				printf("%.*f ", isfloat, (drand48() * (max-min) + min)); //uniform distribution
			else
				printf("%d ", (int)((rand() % (max - min + 1)) + min)); //XXX not uniform: use same as the float case
			printf("\n");
		}
//		printf("\n");
	}

	return 0;
}
