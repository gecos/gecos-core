/**
 */
package gecos.types;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.FunctionType#getReturnType <em>Return Type</em>}</li>
 *   <li>{@link gecos.types.FunctionType#get__internal_parameter_wrapers <em>internal parameter wrapers</em>}</li>
 *   <li>{@link gecos.types.FunctionType#isHasElipsis <em>Has Elipsis</em>}</li>
 *   <li>{@link gecos.types.FunctionType#isInline <em>Inline</em>}</li>
 *   <li>{@link gecos.types.FunctionType#isNoreturn <em>Noreturn</em>}</li>
 * </ul>
 *
 * @see gecos.types.TypesPackage#getFunctionType()
 * @model
 * @generated
 */
public interface FunctionType extends Type, TypesVisitable {
	/**
	 * Returns the value of the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Return Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Return Type</em>' reference.
	 * @see #setReturnType(Type)
	 * @see gecos.types.TypesPackage#getFunctionType_ReturnType()
	 * @model resolveProxies="false"
	 * @generated
	 */
	Type getReturnType();

	/**
	 * Sets the value of the '{@link gecos.types.FunctionType#getReturnType <em>Return Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Return Type</em>' reference.
	 * @see #getReturnType()
	 * @generated
	 */
	void setReturnType(Type value);

	/**
	 * Returns the value of the '<em><b>internal parameter wrapers</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.types.__Internal_ParameterTypeWrapper}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>internal parameter wrapers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>internal parameter wrapers</em>' containment reference list.
	 * @see gecos.types.TypesPackage#getFunctionType___internal_parameter_wrapers()
	 * @model containment="true"
	 * @generated
	 */
	EList<__Internal_ParameterTypeWrapper> get__internal_parameter_wrapers();

	/**
	 * Returns the value of the '<em><b>Has Elipsis</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Elipsis</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Elipsis</em>' attribute.
	 * @see #setHasElipsis(boolean)
	 * @see gecos.types.TypesPackage#getFunctionType_HasElipsis()
	 * @model default="false" unique="false" dataType="gecos.types.boolean"
	 * @generated
	 */
	boolean isHasElipsis();

	/**
	 * Sets the value of the '{@link gecos.types.FunctionType#isHasElipsis <em>Has Elipsis</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Elipsis</em>' attribute.
	 * @see #isHasElipsis()
	 * @generated
	 */
	void setHasElipsis(boolean value);

	/**
	 * Returns the value of the '<em><b>Inline</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inline</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inline</em>' attribute.
	 * @see #setInline(boolean)
	 * @see gecos.types.TypesPackage#getFunctionType_Inline()
	 * @model default="false" unique="false" dataType="gecos.types.boolean"
	 * @generated
	 */
	boolean isInline();

	/**
	 * Sets the value of the '{@link gecos.types.FunctionType#isInline <em>Inline</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inline</em>' attribute.
	 * @see #isInline()
	 * @generated
	 */
	void setInline(boolean value);

	/**
	 * Returns the value of the '<em><b>Noreturn</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Noreturn</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Noreturn</em>' attribute.
	 * @see #setNoreturn(boolean)
	 * @see gecos.types.TypesPackage#getFunctionType_Noreturn()
	 * @model default="false" unique="false" dataType="gecos.types.boolean"
	 * @generated
	 */
	boolean isNoreturn();

	/**
	 * Sets the value of the '{@link gecos.types.FunctionType#isNoreturn <em>Noreturn</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Noreturn</em>' attribute.
	 * @see #isNoreturn()
	 * @generated
	 */
	void setNoreturn(boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return an unmodifiable list of parameters, never {@code null}.
	 * <!-- end-model-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.types.__Internal_ParameterTypeWrapper%&gt;, &lt;%gecos.types.Type%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.types.__Internal_ParameterTypeWrapper%&gt;, &lt;%gecos.types.Type%&gt;&gt;()\n{\n\tpublic &lt;%gecos.types.Type%&gt; apply(final &lt;%gecos.types.__Internal_ParameterTypeWrapper%&gt; it)\n\t{\n\t\treturn it.getParameter();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.types.Type%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.types.Type%&gt;&gt;asEList(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%gecos.types.__Internal_ParameterTypeWrapper%&gt;, &lt;%gecos.types.Type%&gt;&gt;map(this.get__internal_parameter_wrapers(), _function)));'"
	 * @generated
	 */
	EList<Type> listParameters();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * add the specified parameter {@code param} at the end of the parameters list.
	 * <!-- end-model-doc -->
	 * @model paramUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.types.__Internal_ParameterTypeWrapper%&gt; wrapper = &lt;%gecos.types.TypesFactory%&gt;.eINSTANCE.create__Internal_ParameterTypeWrapper();\nwrapper.setParameter(param);\nthis.get__internal_parameter_wrapers().add(wrapper);'"
	 * @generated
	 */
	void addParameter(Type param);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" iDataType="gecos.types.int" iUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.get__internal_parameter_wrapers().get(i).getParameter();'"
	 * @generated
	 */
	Type getParameterAt(int i);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model iDataType="gecos.types.int" iUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.get__internal_parameter_wrapers().remove(i);'"
	 * @generated
	 */
	void removeParameterAt(int i);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='this.get__internal_parameter_wrapers().clear();'"
	 * @generated
	 */
	void clearParameters();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitFunctionType(this);'"
	 * @generated
	 */
	void accept(TypesVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _xifexpression = null;\nboolean _isInline = this.isInline();\nif (_isInline)\n{\n\t_xifexpression = \"inline \";\n}\nelse\n{\n\t_xifexpression = \"\";\n}\n&lt;%java.lang.String%&gt; _plus = (_xifexpression + \"(\");\n&lt;%java.lang.String%&gt; _join = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.join(this.listParameters(), \",\");\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + _join);\n&lt;%java.lang.String%&gt; _xifexpression_1 = null;\nboolean _isHasElipsis = this.isHasElipsis();\nif (_isHasElipsis)\n{\n\t_xifexpression_1 = \",...\";\n}\nelse\n{\n\t_xifexpression_1 = \"\";\n}\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + _xifexpression_1);\n&lt;%java.lang.String%&gt; _plus_3 = (_plus_2 + \")\");\n&lt;%java.lang.String%&gt; _plus_4 = (_plus_3 + \" -&gt; (\");\n&lt;%java.lang.String%&gt; _elvis = null;\n&lt;%gecos.types.Type%&gt; _returnType = this.getReturnType();\n&lt;%java.lang.String%&gt; _string = null;\nif (_returnType!=null)\n{\n\t_string=_returnType.toString();\n}\nif (_string != null)\n{\n\t_elvis = _string;\n} else\n{\n\t_elvis = \"?\";\n}\n&lt;%java.lang.String%&gt; _plus_5 = (_plus_4 + _elvis);\nreturn (_plus_5 + \")\");'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.types.long" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.TypeSizes%&gt;.defaultPointerSize;'"
	 * @generated
	 */
	long getSize();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.boolean" unique="false" otherUnique="false" ignoreStorageClassDataType="gecos.types.boolean" ignoreStorageClassUnique="false" ignoreConstDataType="gecos.types.boolean" ignoreConstUnique="false" ignoreVolatileDataType="gecos.types.boolean" ignoreVolatileUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((other == this))\n{\n\treturn true;\n}\nif ((other instanceof &lt;%gecos.types.FunctionType%&gt;))\n{\n\tfinal &lt;%gecos.types.FunctionType%&gt; ft = ((&lt;%gecos.types.FunctionType%&gt;)other);\n\t&lt;%gecos.types.Type%&gt; _returnType = ft.getReturnType();\n\tboolean _tripleNotEquals = (_returnType != null);\n\t&lt;%gecos.types.Type%&gt; _returnType_1 = this.getReturnType();\n\tboolean _tripleNotEquals_1 = (_returnType_1 != null);\n\tboolean _xor = (_tripleNotEquals ^ _tripleNotEquals_1);\n\tif (_xor)\n\t{\n\t\treturn false;\n\t}\n\tif (((ft.getReturnType() != null) &amp;&amp; (this.getReturnType() != null)))\n\t{\n\t\tboolean _isEqual = ft.getReturnType().isEqual(this.getReturnType(), ignoreStorageClass, ignoreConst, ignoreVolatile);\n\t\tboolean _not = (!_isEqual);\n\t\tif (_not)\n\t\t{\n\t\t\treturn false;\n\t\t}\n\t}\n\tint _size = ft.listParameters().size();\n\tint _size_1 = this.listParameters().size();\n\tboolean _tripleNotEquals_2 = (_size != _size_1);\n\tif (_tripleNotEquals_2)\n\t{\n\t\treturn false;\n\t}\n\tint _size_2 = ft.listParameters().size();\n\t&lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt; _doubleDotLessThan = new &lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt;(0, _size_2, true);\n\tfor (final &lt;%java.lang.Integer%&gt; i : _doubleDotLessThan)\n\t{\n\t\t{\n\t\t\t&lt;%gecos.types.Type%&gt; _get = ft.listParameters().get((i).intValue());\n\t\t\tboolean _tripleNotEquals_3 = (_get != null);\n\t\t\t&lt;%gecos.types.Type%&gt; _get_1 = this.listParameters().get((i).intValue());\n\t\t\tboolean _tripleNotEquals_4 = (_get_1 != null);\n\t\t\tboolean _xor_1 = (_tripleNotEquals_3 ^ _tripleNotEquals_4);\n\t\t\tif (_xor_1)\n\t\t\t{\n\t\t\t\treturn false;\n\t\t\t}\n\t\t\tif (((ft.listParameters().get((i).intValue()) != null) &amp;&amp; (this.listParameters().get((i).intValue()) != null)))\n\t\t\t{\n\t\t\t\tboolean _isEqual_1 = ft.listParameters().get((i).intValue()).isEqual(this.listParameters().get((i).intValue()), ignoreStorageClass, ignoreConst, ignoreVolatile);\n\t\t\t\tboolean _not_1 = (!_isEqual_1);\n\t\t\t\tif (_not_1)\n\t\t\t\t{\n\t\t\t\t\treturn false;\n\t\t\t\t}\n\t\t\t}\n\t\t}\n\t}\n\treturn ((((this.isHasElipsis() == ft.isHasElipsis()) &amp;&amp; (this.isInline() == ft.isInline())) &amp;&amp; (this.isNoreturn() == ft.isNoreturn())) &amp;&amp; super.isEqual(ft, ignoreStorageClass, ignoreConst, ignoreVolatile));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isEqual(Type other, boolean ignoreStorageClass, boolean ignoreConst, boolean ignoreVolatile);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model innermostStorageUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.Type%&gt; _returnType = this.getReturnType();\n_returnType.setInnermostStorageClass(innermostStorage);'"
	 * @generated
	 */
	void setInnermostStorageClass(StorageClassSpecifiers innermostStorage);

} // FunctionType
