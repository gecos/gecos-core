package fr.irisa.cairn.gecos.model.transforms.arrays.flattening.internal;

import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.BBlock;
import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.CompositeBlock;
import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.IfEqThen;
import static fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory.paramSymbol;
import static fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory.proc;
import static fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory.symbol;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.array;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.call;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.ret;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.set;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.symbref;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.transforms.constants.ConstantExpressionEvaluator;
import fr.irisa.cairn.gecos.model.transforms.procedures.inliner.ProcedureInliner;
import fr.irisa.cairn.gecos.model.transforms.tools.SymbolTransformer;
import gecos.annotations.IAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.CompositeBlock;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.ArrayValueInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SimpleArrayInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.ArrayType;
import gecos.types.Type;

public class ArrayFlattener extends SymbolTransformer {

	public static boolean debug = false;
	private static final boolean EXPERIMENTAL = true;
	long depth;

	enum KIND { LHS, RHS, UNKNOWN }; 
	
	private KIND accessType(SimpleArrayInstruction ref) {
		ComplexInstruction current = ref.getParent();
		ComplexInstruction prev = ref;
		if(ref.getType().asBase() != null) {
			while(current!=null) {
				if(current.getType().asBase() != null) {
					if (current.isSet()) {
						SetInstruction set = (SetInstruction) current;
						if(set.getSource()==prev) {
							return KIND.RHS;
						} else if(set.getDest()==prev) {
							return KIND.LHS;
						}
		
					}
				} else {
					return KIND.UNKNOWN;
				}
				prev = current;
				current= current.getParent();
			}
		}
		return KIND.UNKNOWN;
	}
	
	public ArrayFlattener(ProcedureSet ps, Symbol sym, long depth) {
		super(ps, sym);
		pendingTransforms = new ArrayList<ArrayFlattener>();
		helpers = new BasicEList<Procedure>();
		this.depth = depth;
	}

	private Map<Long, Symbol> scalarizedSymbolMap;
	private List<ArrayFlattener> pendingTransforms;

	private List<Symbol> scalarizedArraysList; // map to save all array
	private EList<Procedure> helpers;

	@Override
	public Map<Long, Symbol> transform() {
		if (depth > 0) {
			this.scalarizedSymbolMap = new LinkedHashMap<Long, Symbol>();
			scalarizedArraysList = new ArrayList<Symbol>();
			super.transform();
			removeArray(); // remove array from scope
			ps.addAllProcedures(helpers);

			
			for (Procedure helper : helpers) {
				if (debug)
					System.out.println("**************************************************************** " );
				ProcedureInliner.filterInline(helper.getSymbol(),ps);
			}
			return this.scalarizedSymbolMap;
		}
		return null;
	}

	@Override
	protected void transformSymbolDeclaration() {
		Type type = sym.getType();
		Scope scope = sym.getContainingScope();
		if (scope == null) {
			throw new RuntimeException("symbol " + sym + " has no scope");
		}
		if (type instanceof ArrayType) {
			// ArrayType arrayType = getLastDimension((ArrayType) type);
			ArrayType arrayType = ((ArrayType) type);
			long lb = arrayType.getLower();
			long ub = arrayType.getUpper();
			debug("Flattening array " + sym + " : " + arrayType + " into " + ((ub - lb) + 1) + " " + arrayType.getBase() + " arrays");
			this.scalarizedArraysList.add(sym); // add symbol to map, it will be
												// deleted from scope
			for (long i = lb; i <= ub; i++) {
				flattenArrayDeclaration(sym, arrayType, lb, i);
			}
		} else {
			throw new RuntimeException("Cannot scalarize type " + type);
		}
	}

	private void flattenArrayDeclaration(Symbol sym, ArrayType arrayType, long lb,long i) {
		Symbol newSymbol = symbol(sym.getName() + "_a_" + i, arrayType.getBase());
		initializeNewSymbolValue(sym, lb, i, newSymbol);
		copyAnnotations(sym, newSymbol);
		EList<Symbol> symbols = sym.getContainingScope().getSymbols();
		symbols.add(newSymbol);
		scalarizedSymbolMap.put(i, newSymbol);
		debug("\t-> " + newSymbol);
		if (arrayType.getBase().asArray() != null && depth > 0) {
			debug("Post transformation request for new array " + newSymbol);
			pendingTransforms.add(new ArrayFlattener(ps, newSymbol, depth - 1));
		}
	}

	@Override
	protected void transformArrayReference(SymbolInstruction s) {
		if (s.getParent() instanceof SimpleArrayInstruction) {
			throw new UnsupportedOperationException(
					"Cannot transform a symbol reference which does not belong to a SimpleArrayInstruction ");
		} else {
			debug("Instruction " + s.getRoot() + " is not an Array, skipping ");
		}

	}

	@Override
	protected void transformArrayReference(SimpleArrayInstruction arrayinst) {

		if (arrayinst.eContainer() == null)
			throw new UnsupportedOperationException(arrayinst+ " has no container");
		
		int lastIndex = arrayinst.getIndex().size() - 1;
		Instruction indexToRemove = arrayinst.getIndex().get(lastIndex);
		IntInstruction intindex = simplifyIndexExpression(indexToRemove);
		if (intindex != null) {
			transformConstantRefIndex(arrayinst, intindex);
		} else {
			transformNonConstantRefIndex(arrayinst, indexToRemove);
		}
	}

	protected IntInstruction simplifyIndexExpression(Instruction indexExpr) {
		ConstantExpressionEvaluator evaluator = new ConstantExpressionEvaluator();
		IntInstruction intindex = evaluator.evaluate(indexExpr);
		return intindex;
	}

	protected void transformNonConstantRefIndex(SimpleArrayInstruction ref, Instruction idxInst) {
		if (EXPERIMENTAL) {
			/* Here we look in the array access is used in read mode or write mode */ 
			KIND kind = accessType(ref);
			switch(kind) {
				case RHS : 
					rightHandSideBankSelection(idxInst, ref,scalarizedSymbolMap.values());
					break;
				case LHS : 
					throw new UnsupportedOperationException("Not yet implemented");
					//Instruction res = runTimeBankSelection(idxInst, ref,scalarizedSymbolMap.values());
			default:
				throw new RuntimeException("unkown kind : "+kind);
					
			}

		} else {
			throw new UnsupportedOperationException(
					"Index values not known at compile time (" + ref
							+ ") are not supported by ArrayFlattenner ");
		}

	}

	protected void transformConstantRefIndex(SimpleArrayInstruction ref,
			IntInstruction intindex) {
		long value = intindex.getValue();
		if (scalarizedSymbolMap.containsKey(value)) {
			Symbol newSym = scalarizedSymbolMap.get(value);
			Instruction res = flattenReference(ref, newSym, null);
			debug(ref + " transformed into " + res);
			if (ref.getParent() != null) {
				ref.getParent().replaceChild(ref, res);
			} else if (ref.eContainer() instanceof BasicBlock) {
				BasicBlock BB = (BasicBlock) ref.eContainer();
				BB.replaceInstruction(ref, res);
			} else {
				// if (arrayinst.eContainer() instanceof BasicBlock)
				throw new UnsupportedOperationException(
						"Cannot transform a SymbolInstruction belonging to a "
								+ ref.eContainer() + "object ("
								+ ref.eContainer() + ")");
			}
		} else {
			throw new UnsupportedOperationException("Index value " + intindex
					+ " out of original array bounds " + sym);

		}
	}

	protected Instruction flattenReference(SimpleArrayInstruction ref,Symbol newSym, List<? extends Symbol> newAddresses) {

		if (newSym.getType() instanceof ArrayType) {
			/**
			 * When the resulting flattened symbol is still an array
			 */
			SimpleArrayInstruction newArrayInst = array(newSym);
			ArrayType arrayType = (ArrayType) newSym.getType();

			if (arrayType.getNbDims() >= 1) {
				if (newAddresses != null) {
					for (Symbol symbol : newAddresses) {
						newArrayInst.addIndex(symbref(symbol));
					}
				} else {
					/*for (int i = 0; i < ref.getIndex().size() - 1; i++) {
						Instruction child = ref.getIndex().get(i);
						if (newArrayInst.getChildrenCount() == 1) {
							newArrayInst.getChildren().add(child.copy());
						} else {
							newArrayInst.getIndex().add(child.copy());
						}
					}*/
					// Simara: .getIndex() does not return original list
					// so .add has no effect. Use getChildren instead.
					for (int i = 1; i < ref.getChildrenCount()-1; i++) {
						Instruction child = ref.getIndex().get(i);
						newArrayInst.addIndex(child.copy());
					}
				}
			}
			debug("Transforming " + ref + " into " + newArrayInst);
			return newArrayInst;
		} else {
			/**
			 * When the resulting flattened symbol becomes a scalar
			 */
			debug("Transforming " + ref + " into " + newSym);
			return symbref(newSym);
		}
		
	}

	@Override
	protected void cleanup() {
		debug("Cleanup");
		for (ArrayFlattener flattener : pendingTransforms) {
			flattener.transform();
		}
	}

	private void initializeNewSymbolValue(Symbol oldSym, long lb, long i,
			Symbol newSym) {
		if (oldSym.getValue() != null) {
			ArrayValueInstruction value = (ArrayValueInstruction) oldSym.getValue();
			int valueInit = Math.min(value.getChildren().size()-1,(int)(i-lb));
			Instruction valueCopy = value.getChildren().get(valueInit).copy();
			newSym.setValue(valueCopy);// value.getElements().get(i-lb));
		}
	}

	private void copyAnnotations(Symbol symbol, Symbol newscalar) {
		if (symbol.getAnnotations() != null) {
			for (int j = 0; j < symbol.getAnnotations().size(); j++) {
				Entry<String, IAnnotation> annot = symbol.getAnnotations().get(
						j);
				String key = annot.getKey();
				Copier copier = new Copier();
				IAnnotation newAnnotation = (IAnnotation) copier.copy(annot
						.getValue());
				newscalar.getAnnotations().put(key, newAnnotation);
			}
		}
	}

	private void removeArray() {
		debug("Remove scalarized array from scope: "
				+ this.scalarizedArraysList);
		for (Symbol array : scalarizedArraysList) {
			Scope scopeArray = (Scope) array.eContainer();
			scopeArray.getSymbols().remove(array);
		}
	}

	private void rightHandSideBankSelection(Instruction choice,
			SimpleArrayInstruction access, Collection<Symbol> banks) {

		Procedure proc = buildRuntimeAccessFunction(access, banks);
		helpers.add(proc);
		buildRuntimeAccessCall(choice, access, banks, proc);

	}

	private Procedure buildRuntimeAccessFunction(SimpleArrayInstruction access,
			Collection<Symbol> banks) {
		/**
		 * We create a function (that shall be inlined) that will capture the
		 * runtime access patterns and dispatch them to the adequate memory
		 * bank.
		 */
		int i = 0;
		List<ParameterSymbol> params = new ArrayList<ParameterSymbol>();
		List<ParameterSymbol> addresses = new ArrayList<ParameterSymbol>();
		List<ParameterSymbol> banksymbols = new ArrayList<ParameterSymbol>();
		EList<Instruction> index = access.getIndex();
		Type indexType = index.get(0).getType();
		Type type = null;

		CompositeBlock cb = CompositeBlock();

		Scope scope = cb.getScope();

		ArrayType accessType = (ArrayType) ((Symbol) banks.iterator().next())
				.getType();
		Symbol resSym = symbol("tmp", accessType.getBase(),
				scope);

		
		ParameterSymbol bankSelector = paramSymbol("bank_id",indexType);

		int nbAddresses = index.size() - 1;
		for (int depth = 0; depth < nbAddresses; depth++) {
			addresses.add(paramSymbol("addr_" + depth, indexType));
		}
		
		i=0;
		for (Symbol symbol : banks) {
			type = symbol.getType();
			
			ParameterSymbol bankSym = paramSymbol("bank_" + i,type);

			// build "if(index=...) tmp= ..."
			Instruction flattened = flattenReference(access, bankSym,addresses);
			cb.getChildren().add(
					IfEqThen(bankSelector, i,
							BBlock(set(symbref(resSym), flattened)),
							scope));
			banksymbols.add(bankSym);
			i++;

		}
		
		// build "return tmp"
		cb.getChildren().add(BBlock(ret(symbref(resSym))));

		params.add(bankSelector);
		params.addAll(addresses);
		params.addAll(banksymbols);

		// SwitchBlock switchBlk = Switch(symbref(sym2),bb);
		// CompositeBlock body = CompositeBlock(switchBlk);

		Procedure proc = proc(ps, "run_time_read_"+ access.getSymbol().getName(), type, cb, params);
		return proc;
	}

	private void buildRuntimeAccessCall(Instruction choice,
			SimpleArrayInstruction access, Collection<Symbol> banks,
			Procedure proc) {
		int i;
		CallInstruction call = call(proc.getSymbol());
		call.addArg(choice.copy());

		for (i = 0; i < (access.getIndex().size() - 1); i++) {
			call.addArg(access.getIndex().get(i).copy());
		}

		for (Symbol symbol : banks) {
			call.addArg(symbref(symbol));
		}
		GecosUserAnnotationFactory.pragma(call, "  GCS_INLINE");
		access.substituteWith(call);
	}

}