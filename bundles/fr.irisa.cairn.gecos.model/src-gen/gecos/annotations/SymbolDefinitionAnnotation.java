/**
 */
package gecos.annotations;

import gecos.instrs.Instruction;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Symbol Definition Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.annotations.SymbolDefinitionAnnotation#getSymbolDefinition <em>Symbol Definition</em>}</li>
 * </ul>
 *
 * @see gecos.annotations.AnnotationsPackage#getSymbolDefinitionAnnotation()
 * @model
 * @generated
 */
public interface SymbolDefinitionAnnotation extends IAnnotation {
	/**
	 * Returns the value of the '<em><b>Symbol Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Symbol Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbol Definition</em>' reference.
	 * @see #setSymbolDefinition(Instruction)
	 * @see gecos.annotations.AnnotationsPackage#getSymbolDefinitionAnnotation_SymbolDefinition()
	 * @model
	 * @generated
	 */
	Instruction getSymbolDefinition();

	/**
	 * Sets the value of the '{@link gecos.annotations.SymbolDefinitionAnnotation#getSymbolDefinition <em>Symbol Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Symbol Definition</em>' reference.
	 * @see #getSymbolDefinition()
	 * @generated
	 */
	void setSymbolDefinition(Instruction value);

} // SymbolDefinitionAnnotation
