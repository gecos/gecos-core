package $packageName$.modules;

import $packageName$.modules.utils.IfCounterSwitch;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;

/**
 * GeCoS module implementation which iterate on each procedure contained
 * in a GeCoS project and call a custom visitor which count the number of IfBlock present.
 */
public class IfCounterSwitchModule {

	private GecosProject p;
	
	public IfCounterSwitchModule(GecosProject p) {
		this.p = p;
	}
	
	/**
	 * Method called by the GeCoS framework when the script command defined in the extension is called
	 */
	public void compute() {
		for (ProcedureSet ps : p.listProcedureSets()) { // Iterate on each procedure set contained in a GeCoS project 
			for (Procedure pr : ps.listProcedures()) { // Iterate on each procedure contained in a procedure set
				IfCounterSwitch sw = new IfCounterSwitch();
				sw.doSwitch(pr.getBody()); // apply the switch on the body block of a procedure
				System.out.println("Procedure(" + pr.getSymbolName() + "): "+ sw.getCount()); // Just display the number of IfBlock detected in the procedure
			}
		}
	}
}
