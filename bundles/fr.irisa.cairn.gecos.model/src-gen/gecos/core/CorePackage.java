/**
 */
package gecos.core;

import gecos.annotations.AnnotationsPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see gecos.core.CoreFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel modelName='Gecos' modelPluginClass='' creationCommands='false' creationIcons='false' interfaceNamePattern='' importerID='org.eclipse.emf.importer.ecore' operationReflection='false' basePackage='gecos'"
 *        annotation="http://www.eclipse.org/emf/2011/Xcore GmfDiagram='gmf.diagram' GmfCompartment='gmf.compartment' GmfNode='gmf.node' GmfLink='gmf.link' OCL='http://www.eclipse.org/ocl/examples/OCL'"
 * @generated
 */
public interface CorePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "core";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.gecos.org/core";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "core";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CorePackage eINSTANCE = gecos.core.impl.CorePackageImpl.init();

	/**
	 * The meta object id for the '{@link gecos.core.GecosNode <em>Gecos Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.core.GecosNode
	 * @see gecos.core.impl.CorePackageImpl#getGecosNode()
	 * @generated
	 */
	int GECOS_NODE = 0;

	/**
	 * The number of structural features of the '<em>Gecos Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GECOS_NODE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link gecos.core.CoreVisitable <em>Visitable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.core.CoreVisitable
	 * @see gecos.core.impl.CorePackageImpl#getCoreVisitable()
	 * @generated
	 */
	int CORE_VISITABLE = 1;

	/**
	 * The number of structural features of the '<em>Visitable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORE_VISITABLE_FEATURE_COUNT = GECOS_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.core.CoreVisitor <em>Visitor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.core.CoreVisitor
	 * @see gecos.core.impl.CorePackageImpl#getCoreVisitor()
	 * @generated
	 */
	int CORE_VISITOR = 2;

	/**
	 * The number of structural features of the '<em>Visitor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORE_VISITOR_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link gecos.core.ITypedElement <em>ITyped Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.core.ITypedElement
	 * @see gecos.core.impl.CorePackageImpl#getITypedElement()
	 * @generated
	 */
	int ITYPED_ELEMENT = 3;

	/**
	 * The number of structural features of the '<em>ITyped Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITYPED_ELEMENT_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link gecos.core.impl.SymbolImpl <em>Symbol</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.core.impl.SymbolImpl
	 * @see gecos.core.impl.CorePackageImpl#getSymbol()
	 * @generated
	 */
	int SYMBOL = 4;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL__ANNOTATIONS = AnnotationsPackage.ANNOTATED_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL__TYPE = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL__NAME = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL__VALUE = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Symbol</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_FEATURE_COUNT = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link gecos.core.ISymbolUse <em>ISymbol Use</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.core.ISymbolUse
	 * @see gecos.core.impl.CorePackageImpl#getISymbolUse()
	 * @generated
	 */
	int ISYMBOL_USE = 5;

	/**
	 * The number of structural features of the '<em>ISymbol Use</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISYMBOL_USE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link gecos.core.impl.ProcedureSymbolImpl <em>Procedure Symbol</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.core.impl.ProcedureSymbolImpl
	 * @see gecos.core.impl.CorePackageImpl#getProcedureSymbol()
	 * @generated
	 */
	int PROCEDURE_SYMBOL = 6;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_SYMBOL__ANNOTATIONS = SYMBOL__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_SYMBOL__TYPE = SYMBOL__TYPE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_SYMBOL__NAME = SYMBOL__NAME;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_SYMBOL__VALUE = SYMBOL__VALUE;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_SYMBOL__SCOPE = SYMBOL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_SYMBOL__KIND = SYMBOL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Procedure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_SYMBOL__PROCEDURE = SYMBOL_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Procedure Symbol</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_SYMBOL_FEATURE_COUNT = SYMBOL_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link gecos.core.ScopeContainer <em>Scope Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.core.ScopeContainer
	 * @see gecos.core.impl.CorePackageImpl#getScopeContainer()
	 * @generated
	 */
	int SCOPE_CONTAINER = 7;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOPE_CONTAINER__SCOPE = CORE_VISITABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Scope Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOPE_CONTAINER_FEATURE_COUNT = CORE_VISITABLE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.core.impl.ScopeImpl <em>Scope</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.core.impl.ScopeImpl
	 * @see gecos.core.impl.CorePackageImpl#getScope()
	 * @generated
	 */
	int SCOPE = 8;

	/**
	 * The feature id for the '<em><b>Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOPE__TYPES = CORE_VISITABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Symbols</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOPE__SYMBOLS = CORE_VISITABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOPE__CONTAINER = CORE_VISITABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOPE__PARENT = CORE_VISITABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Children</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOPE__CHILDREN = CORE_VISITABLE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Scope</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCOPE_FEATURE_COUNT = CORE_VISITABLE_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link gecos.core.impl.ProcedureSetImpl <em>Procedure Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.core.impl.ProcedureSetImpl
	 * @see gecos.core.impl.CorePackageImpl#getProcedureSet()
	 * @generated
	 */
	int PROCEDURE_SET = 9;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_SET__SCOPE = SCOPE_CONTAINER__SCOPE;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_SET__ANNOTATIONS = SCOPE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Uses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_SET__USES = SCOPE_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Procedure Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_SET_FEATURE_COUNT = SCOPE_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.core.impl.ProcedureImpl <em>Procedure</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.core.impl.ProcedureImpl
	 * @see gecos.core.impl.CorePackageImpl#getProcedure()
	 * @generated
	 */
	int PROCEDURE = 10;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE__ANNOTATIONS = AnnotationsPackage.ANNOTATED_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Start</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE__START = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE__BODY = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE__END = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE__SYMBOL = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Containing Procedure Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE__CONTAINING_PROCEDURE_SET = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Procedure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_FEATURE_COUNT = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link gecos.core.impl.ParameterSymbolImpl <em>Parameter Symbol</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.core.impl.ParameterSymbolImpl
	 * @see gecos.core.impl.CorePackageImpl#getParameterSymbol()
	 * @generated
	 */
	int PARAMETER_SYMBOL = 11;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SYMBOL__ANNOTATIONS = SYMBOL__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SYMBOL__TYPE = SYMBOL__TYPE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SYMBOL__NAME = SYMBOL__NAME;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SYMBOL__VALUE = SYMBOL__VALUE;

	/**
	 * The number of structural features of the '<em>Parameter Symbol</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SYMBOL_FEATURE_COUNT = SYMBOL_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '<em>String</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see gecos.core.impl.CorePackageImpl#getString()
	 * @generated
	 */
	int STRING = 12;

	/**
	 * The meta object id for the '<em>int</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.core.impl.CorePackageImpl#getint()
	 * @generated
	 */
	int INT = 13;

	/**
	 * The meta object id for the '<em>Block Copy Manager</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager
	 * @see gecos.core.impl.CorePackageImpl#getBlockCopyManager()
	 * @generated
	 */
	int BLOCK_COPY_MANAGER = 14;


	/**
	 * Returns the meta object for class '{@link gecos.core.GecosNode <em>Gecos Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Gecos Node</em>'.
	 * @see gecos.core.GecosNode
	 * @generated
	 */
	EClass getGecosNode();

	/**
	 * Returns the meta object for class '{@link gecos.core.CoreVisitable <em>Visitable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visitable</em>'.
	 * @see gecos.core.CoreVisitable
	 * @generated
	 */
	EClass getCoreVisitable();

	/**
	 * Returns the meta object for class '{@link gecos.core.CoreVisitor <em>Visitor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visitor</em>'.
	 * @see gecos.core.CoreVisitor
	 * @generated
	 */
	EClass getCoreVisitor();

	/**
	 * Returns the meta object for class '{@link gecos.core.ITypedElement <em>ITyped Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ITyped Element</em>'.
	 * @see gecos.core.ITypedElement
	 * @generated
	 */
	EClass getITypedElement();

	/**
	 * Returns the meta object for class '{@link gecos.core.Symbol <em>Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Symbol</em>'.
	 * @see gecos.core.Symbol
	 * @generated
	 */
	EClass getSymbol();

	/**
	 * Returns the meta object for the reference '{@link gecos.core.Symbol#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see gecos.core.Symbol#getType()
	 * @see #getSymbol()
	 * @generated
	 */
	EReference getSymbol_Type();

	/**
	 * Returns the meta object for the attribute '{@link gecos.core.Symbol#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see gecos.core.Symbol#getName()
	 * @see #getSymbol()
	 * @generated
	 */
	EAttribute getSymbol_Name();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.core.Symbol#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see gecos.core.Symbol#getValue()
	 * @see #getSymbol()
	 * @generated
	 */
	EReference getSymbol_Value();

	/**
	 * Returns the meta object for class '{@link gecos.core.ISymbolUse <em>ISymbol Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISymbol Use</em>'.
	 * @see gecos.core.ISymbolUse
	 * @generated
	 */
	EClass getISymbolUse();

	/**
	 * Returns the meta object for class '{@link gecos.core.ProcedureSymbol <em>Procedure Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Procedure Symbol</em>'.
	 * @see gecos.core.ProcedureSymbol
	 * @generated
	 */
	EClass getProcedureSymbol();

	/**
	 * Returns the meta object for the attribute '{@link gecos.core.ProcedureSymbol#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see gecos.core.ProcedureSymbol#getKind()
	 * @see #getProcedureSymbol()
	 * @generated
	 */
	EAttribute getProcedureSymbol_Kind();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.core.ProcedureSymbol#getProcedure <em>Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Procedure</em>'.
	 * @see gecos.core.ProcedureSymbol#getProcedure()
	 * @see #getProcedureSymbol()
	 * @generated
	 */
	EReference getProcedureSymbol_Procedure();

	/**
	 * Returns the meta object for class '{@link gecos.core.ScopeContainer <em>Scope Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scope Container</em>'.
	 * @see gecos.core.ScopeContainer
	 * @generated
	 */
	EClass getScopeContainer();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.core.ScopeContainer#getScope <em>Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Scope</em>'.
	 * @see gecos.core.ScopeContainer#getScope()
	 * @see #getScopeContainer()
	 * @generated
	 */
	EReference getScopeContainer_Scope();

	/**
	 * Returns the meta object for class '{@link gecos.core.Scope <em>Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scope</em>'.
	 * @see gecos.core.Scope
	 * @generated
	 */
	EClass getScope();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.core.Scope#getTypes <em>Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Types</em>'.
	 * @see gecos.core.Scope#getTypes()
	 * @see #getScope()
	 * @generated
	 */
	EReference getScope_Types();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.core.Scope#getSymbols <em>Symbols</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Symbols</em>'.
	 * @see gecos.core.Scope#getSymbols()
	 * @see #getScope()
	 * @generated
	 */
	EReference getScope_Symbols();

	/**
	 * Returns the meta object for the container reference '{@link gecos.core.Scope#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Container</em>'.
	 * @see gecos.core.Scope#getContainer()
	 * @see #getScope()
	 * @generated
	 */
	EReference getScope_Container();

	/**
	 * Returns the meta object for the reference '{@link gecos.core.Scope#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parent</em>'.
	 * @see gecos.core.Scope#getParent()
	 * @see #getScope()
	 * @generated
	 */
	EReference getScope_Parent();

	/**
	 * Returns the meta object for the reference list '{@link gecos.core.Scope#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Children</em>'.
	 * @see gecos.core.Scope#getChildren()
	 * @see #getScope()
	 * @generated
	 */
	EReference getScope_Children();

	/**
	 * Returns the meta object for class '{@link gecos.core.ProcedureSet <em>Procedure Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Procedure Set</em>'.
	 * @see gecos.core.ProcedureSet
	 * @generated
	 */
	EClass getProcedureSet();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.core.ProcedureSet#getUses <em>Uses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Uses</em>'.
	 * @see gecos.core.ProcedureSet#getUses()
	 * @see #getProcedureSet()
	 * @generated
	 */
	EReference getProcedureSet_Uses();

	/**
	 * Returns the meta object for class '{@link gecos.core.Procedure <em>Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Procedure</em>'.
	 * @see gecos.core.Procedure
	 * @generated
	 */
	EClass getProcedure();

	/**
	 * Returns the meta object for the reference '{@link gecos.core.Procedure#getStart <em>Start</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Start</em>'.
	 * @see gecos.core.Procedure#getStart()
	 * @see #getProcedure()
	 * @generated
	 */
	EReference getProcedure_Start();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.core.Procedure#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body</em>'.
	 * @see gecos.core.Procedure#getBody()
	 * @see #getProcedure()
	 * @generated
	 */
	EReference getProcedure_Body();

	/**
	 * Returns the meta object for the reference '{@link gecos.core.Procedure#getEnd <em>End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>End</em>'.
	 * @see gecos.core.Procedure#getEnd()
	 * @see #getProcedure()
	 * @generated
	 */
	EReference getProcedure_End();

	/**
	 * Returns the meta object for the container reference '{@link gecos.core.Procedure#getSymbol <em>Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Symbol</em>'.
	 * @see gecos.core.Procedure#getSymbol()
	 * @see #getProcedure()
	 * @generated
	 */
	EReference getProcedure_Symbol();

	/**
	 * Returns the meta object for the reference '{@link gecos.core.Procedure#getContainingProcedureSet <em>Containing Procedure Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Containing Procedure Set</em>'.
	 * @see gecos.core.Procedure#getContainingProcedureSet()
	 * @see #getProcedure()
	 * @generated
	 */
	EReference getProcedure_ContainingProcedureSet();

	/**
	 * Returns the meta object for class '{@link gecos.core.ParameterSymbol <em>Parameter Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter Symbol</em>'.
	 * @see gecos.core.ParameterSymbol
	 * @generated
	 */
	EClass getParameterSymbol();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>String</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getString();

	/**
	 * Returns the meta object for data type '<em>int</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>int</em>'.
	 * @model instanceClass="int"
	 * @generated
	 */
	EDataType getint();

	/**
	 * Returns the meta object for data type '{@link fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager <em>Block Copy Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Block Copy Manager</em>'.
	 * @see fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager
	 * @model instanceClass="fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager"
	 * @generated
	 */
	EDataType getBlockCopyManager();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CoreFactory getCoreFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link gecos.core.GecosNode <em>Gecos Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.core.GecosNode
		 * @see gecos.core.impl.CorePackageImpl#getGecosNode()
		 * @generated
		 */
		EClass GECOS_NODE = eINSTANCE.getGecosNode();

		/**
		 * The meta object literal for the '{@link gecos.core.CoreVisitable <em>Visitable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.core.CoreVisitable
		 * @see gecos.core.impl.CorePackageImpl#getCoreVisitable()
		 * @generated
		 */
		EClass CORE_VISITABLE = eINSTANCE.getCoreVisitable();

		/**
		 * The meta object literal for the '{@link gecos.core.CoreVisitor <em>Visitor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.core.CoreVisitor
		 * @see gecos.core.impl.CorePackageImpl#getCoreVisitor()
		 * @generated
		 */
		EClass CORE_VISITOR = eINSTANCE.getCoreVisitor();

		/**
		 * The meta object literal for the '{@link gecos.core.ITypedElement <em>ITyped Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.core.ITypedElement
		 * @see gecos.core.impl.CorePackageImpl#getITypedElement()
		 * @generated
		 */
		EClass ITYPED_ELEMENT = eINSTANCE.getITypedElement();

		/**
		 * The meta object literal for the '{@link gecos.core.impl.SymbolImpl <em>Symbol</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.core.impl.SymbolImpl
		 * @see gecos.core.impl.CorePackageImpl#getSymbol()
		 * @generated
		 */
		EClass SYMBOL = eINSTANCE.getSymbol();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYMBOL__TYPE = eINSTANCE.getSymbol_Type();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYMBOL__NAME = eINSTANCE.getSymbol_Name();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYMBOL__VALUE = eINSTANCE.getSymbol_Value();

		/**
		 * The meta object literal for the '{@link gecos.core.ISymbolUse <em>ISymbol Use</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.core.ISymbolUse
		 * @see gecos.core.impl.CorePackageImpl#getISymbolUse()
		 * @generated
		 */
		EClass ISYMBOL_USE = eINSTANCE.getISymbolUse();

		/**
		 * The meta object literal for the '{@link gecos.core.impl.ProcedureSymbolImpl <em>Procedure Symbol</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.core.impl.ProcedureSymbolImpl
		 * @see gecos.core.impl.CorePackageImpl#getProcedureSymbol()
		 * @generated
		 */
		EClass PROCEDURE_SYMBOL = eINSTANCE.getProcedureSymbol();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCEDURE_SYMBOL__KIND = eINSTANCE.getProcedureSymbol_Kind();

		/**
		 * The meta object literal for the '<em><b>Procedure</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCEDURE_SYMBOL__PROCEDURE = eINSTANCE.getProcedureSymbol_Procedure();

		/**
		 * The meta object literal for the '{@link gecos.core.ScopeContainer <em>Scope Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.core.ScopeContainer
		 * @see gecos.core.impl.CorePackageImpl#getScopeContainer()
		 * @generated
		 */
		EClass SCOPE_CONTAINER = eINSTANCE.getScopeContainer();

		/**
		 * The meta object literal for the '<em><b>Scope</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOPE_CONTAINER__SCOPE = eINSTANCE.getScopeContainer_Scope();

		/**
		 * The meta object literal for the '{@link gecos.core.impl.ScopeImpl <em>Scope</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.core.impl.ScopeImpl
		 * @see gecos.core.impl.CorePackageImpl#getScope()
		 * @generated
		 */
		EClass SCOPE = eINSTANCE.getScope();

		/**
		 * The meta object literal for the '<em><b>Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOPE__TYPES = eINSTANCE.getScope_Types();

		/**
		 * The meta object literal for the '<em><b>Symbols</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOPE__SYMBOLS = eINSTANCE.getScope_Symbols();

		/**
		 * The meta object literal for the '<em><b>Container</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOPE__CONTAINER = eINSTANCE.getScope_Container();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOPE__PARENT = eINSTANCE.getScope_Parent();

		/**
		 * The meta object literal for the '<em><b>Children</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCOPE__CHILDREN = eINSTANCE.getScope_Children();

		/**
		 * The meta object literal for the '{@link gecos.core.impl.ProcedureSetImpl <em>Procedure Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.core.impl.ProcedureSetImpl
		 * @see gecos.core.impl.CorePackageImpl#getProcedureSet()
		 * @generated
		 */
		EClass PROCEDURE_SET = eINSTANCE.getProcedureSet();

		/**
		 * The meta object literal for the '<em><b>Uses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCEDURE_SET__USES = eINSTANCE.getProcedureSet_Uses();

		/**
		 * The meta object literal for the '{@link gecos.core.impl.ProcedureImpl <em>Procedure</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.core.impl.ProcedureImpl
		 * @see gecos.core.impl.CorePackageImpl#getProcedure()
		 * @generated
		 */
		EClass PROCEDURE = eINSTANCE.getProcedure();

		/**
		 * The meta object literal for the '<em><b>Start</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCEDURE__START = eINSTANCE.getProcedure_Start();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCEDURE__BODY = eINSTANCE.getProcedure_Body();

		/**
		 * The meta object literal for the '<em><b>End</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCEDURE__END = eINSTANCE.getProcedure_End();

		/**
		 * The meta object literal for the '<em><b>Symbol</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCEDURE__SYMBOL = eINSTANCE.getProcedure_Symbol();

		/**
		 * The meta object literal for the '<em><b>Containing Procedure Set</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCEDURE__CONTAINING_PROCEDURE_SET = eINSTANCE.getProcedure_ContainingProcedureSet();

		/**
		 * The meta object literal for the '{@link gecos.core.impl.ParameterSymbolImpl <em>Parameter Symbol</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.core.impl.ParameterSymbolImpl
		 * @see gecos.core.impl.CorePackageImpl#getParameterSymbol()
		 * @generated
		 */
		EClass PARAMETER_SYMBOL = eINSTANCE.getParameterSymbol();

		/**
		 * The meta object literal for the '<em>String</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see gecos.core.impl.CorePackageImpl#getString()
		 * @generated
		 */
		EDataType STRING = eINSTANCE.getString();

		/**
		 * The meta object literal for the '<em>int</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.core.impl.CorePackageImpl#getint()
		 * @generated
		 */
		EDataType INT = eINSTANCE.getint();

		/**
		 * The meta object literal for the '<em>Block Copy Manager</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager
		 * @see gecos.core.impl.CorePackageImpl#getBlockCopyManager()
		 * @generated
		 */
		EDataType BLOCK_COPY_MANAGER = eINSTANCE.getBlockCopyManager();

	}

} //CorePackage
