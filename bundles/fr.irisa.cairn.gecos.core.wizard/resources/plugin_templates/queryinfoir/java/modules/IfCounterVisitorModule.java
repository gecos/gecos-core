package $packageName$.modules;

import $packageName$.modules.utils.IfCounterVisitor;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;

/**
 * GeCoS module implementation which iterate on each procedure contained
 * in a GeCoS project and call a custom visitor which count the number of IfBlock present.
 */
public class IfCounterVisitorModule {
	
	private GecosProject p;

	public IfCounterVisitorModule(GecosProject p) {
		this.p = p;
	}

	/**
	 * Method called by the GeCoS framework when the script command defined in the extension is called
	 */
	public void compute() {
		for (ProcedureSet ps : p.listProcedureSets()) { // Iterate on each procedure set contained in a GeCoS project 
			for (Procedure pr : ps.listProcedures()) { // Iterate on each procedure contained in a procedure set
				IfCounterVisitor visitor = new IfCounterVisitor(); 
				pr.getBody().accept(visitor); // apply the visitor on the body block of a procedure
				System.out.println("Procedure(" + pr.getSymbolName() + "): "+ visitor.getCount()); // Just display the number of IfBlock detected in the procedure
			}
		}
	}
}
