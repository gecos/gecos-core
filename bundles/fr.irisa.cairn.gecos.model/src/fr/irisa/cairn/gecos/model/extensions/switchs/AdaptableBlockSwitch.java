/**
 * 
 */
package fr.irisa.cairn.gecos.model.extensions.switchs;

import gecos.blocks.util.BlocksSwitch;

import org.eclipse.emf.ecore.EObject;

/**
 * @author kmartin
 * 
 */
public class AdaptableBlockSwitch<T> extends BlocksSwitch<T> implements
		IAdaptableSwitch<T> {

	private SwitchDelegate<T> delegate = new SwitchDelegate<T>();

	public SwitchDelegate<T> getDelegate() {
		return delegate;
	}

	@Override
	public T doSwitch(EObject o) {
		if(o!=null) {
			T res = delegate.doSwitch(o, this);
			if (res == null) {
				res = super.doSwitch(o);
			}
			return res;
		}
		return null;
	}

}
