/**
 */
package gecos.dag.impl;

import gecos.dag.DAGArrayValueNode;
import gecos.dag.DagPackage;
import gecos.dag.DagVisitor;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DAG Array Value Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DAGArrayValueNodeImpl extends DAGNodeImpl implements DAGArrayValueNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DAGArrayValueNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DagPackage.Literals.DAG_ARRAY_VALUE_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final DagVisitor visitor) {
		visitor.visitDAGArrayValueNode(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		return "ArrayValue";
	}

} //DAGArrayValueNodeImpl
