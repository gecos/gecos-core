/**
 */
package gecos.types;

import gecos.core.GecosNode;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visitable</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.types.TypesPackage#getTypesVisitable()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface TypesVisitable extends GecosNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 * @generated
	 */
	void accept(TypesVisitor visitor);

} // TypesVisitable
