package fr.irisa.cairn.gecos.model.tools.visitors;

import gecos.dag.DAGArrayNode;
import gecos.dag.DAGArrayValueNode;
import gecos.dag.DAGCallNode;
import gecos.dag.DAGControlEdge;
import gecos.dag.DAGDataEdge;
import gecos.dag.DAGEdge;
import gecos.dag.DAGFieldInstruction;
import gecos.dag.DAGFloatImmNode;
import gecos.dag.DAGInPort;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGIntImmNode;
import gecos.dag.DAGJumpNode;
import gecos.dag.DAGNode;
import gecos.dag.DAGNumberedOutNode;
import gecos.dag.DAGNumberedSymbolNode;
import gecos.dag.DAGOpNode;
import gecos.dag.DAGOutNode;
import gecos.dag.DAGOutPort;
import gecos.dag.DAGPatternNode;
import gecos.dag.DAGPhiNode;
import gecos.dag.DAGSSADefNode;
import gecos.dag.DAGSSAUseNode;
import gecos.dag.DAGSimpleArrayNode;
import gecos.dag.DAGStringImmNode;
import gecos.dag.DAGSymbolNode;
import gecos.dag.DAGVectorArrayAccessNode;
import gecos.dag.DAGVectorExpandNode;
import gecos.dag.DAGVectorExtractNode;
import gecos.dag.DAGVectorOpNode;
import gecos.dag.DAGVectorPackNode;
import gecos.dag.DAGVectorShuffleNode;
import gecos.dag.DagVisitor;

/**
 * Gecos default Block and Instructions visitor that also visit nodes and edges
 * of each {@link DAGInstruction}.
 * 
 * @see GecosBlocksInstructionsDefaultVisitor
 * @author Antoine Floc'h - Initial contribution and API
 * 
 */
public abstract class GecosBlocksInstructionsDagDefaultVisitor extends
		GecosBlocksInstructionsDefaultVisitor implements DagVisitor {

	public void visitDAGInstruction(DAGInstruction s) {
		for (DAGNode n : s.getNodes()) {
			n.accept(this);
		}
		for (DAGEdge e : s.getEdges()) {
			e.accept(this);
		}
	}

	public void visitDAGInPort(DAGInPort d) {

	}

	public void visitDAGOutPort(DAGOutPort d) {

	}

	public void visitDAGSymbolNode(DAGSymbolNode d) {

	}

	public void visitDAGCallNode(DAGCallNode d) {

	}

	public void visitDAGArrayValueNode(DAGArrayValueNode d) {

	}

	public void visitDAGOpNode(DAGOpNode d) {

	}

	public void visitDAGOutNode(DAGOutNode d) {

	}

	public void visitDAGControlEdge(DAGControlEdge d) {

	}

	public void visitDAGDataEdge(DAGDataEdge d) {

	}

	public void visitDAGNumberedSymbolNode(DAGNumberedSymbolNode d) {

	}

	public void visitDAGNumberedOutNode(DAGNumberedOutNode d) {

	}

	public void visitDAGIntImmNode(DAGIntImmNode d) {

	}

	public void visitDAGFloatImmNode(DAGFloatImmNode d) {

	}
	
	public void visitDAGStringImmNode(DAGStringImmNode d) {
		
	}

	public void visitDAGArrayNode(DAGArrayNode d) {

	}

	public void visitDAGSimpleArrayNode(DAGSimpleArrayNode d) {

	}

	public void visitDAGJumpNode(DAGJumpNode d) {

	}

	public void visitDAGPatternNode(DAGPatternNode d) {

	}
	
	public void visitDAGVectorArrayAccessNode(DAGVectorArrayAccessNode d) {
		
	}

	public void visitDAGVectorOpNode(DAGVectorOpNode d) {
		
	}

	public void visitDAGVectorShuffleNode(DAGVectorShuffleNode d) {
		
	}

	public void visitDAGVectorExtractNode(DAGVectorExtractNode d) {
		
	}

	public void visitDAGVectorPackNode(DAGVectorPackNode d) {
		
	}
	
	@Override
	public void visitDAGSSAUseNode(DAGSSAUseNode d) {
		
	}

	@Override
	public void visitDAGSSADefNode(DAGSSADefNode d) {
		
	}

	@Override
	public void visitDAGPhiNode(DAGPhiNode d) {
		
	}

	@Override
	public void visitDAGFieldInstruction(DAGFieldInstruction d) {
		
	}
	
	@Override
	public void visitDAGVectorExpandNode(DAGVectorExpandNode d) {
		
	}

}
