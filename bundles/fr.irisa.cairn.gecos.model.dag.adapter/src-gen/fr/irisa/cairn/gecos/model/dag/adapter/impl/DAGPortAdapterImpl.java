package fr.irisa.cairn.gecos.model.dag.adapter.impl;
import java.util.Set;

import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGEdgeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGNodeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGPortAdapter;
import gecos.dag.DAGPort;

/**
 * implementation of a node adapter.
 * This graph adapt a DAGPort
 * @author <your name>
 *
 */
public class DAGPortAdapterImpl extends fr.irisa.cairn.graph.implement.Port
		implements
			IDAGPortAdapter {

	private final DAGPort adapted;

	public DAGPortAdapterImpl(DAGPort adapted) {
		super();
		this.adapted = adapted;

	}

	public DAGPort getAdaptedPort() {
		return this.adapted;
	}

	/** Get the port node */
	/**
	 * @model default=""
	 */
	@Override
	public IDAGNodeAdapter getNode() {
		return (IDAGNodeAdapter) super.getNode();
	}

	/** Get all connected edges */
	/**
	 * @model default=""
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Set<IDAGEdgeAdapter> getEdges() {
		return (Set<IDAGEdgeAdapter>) super.getEdges();
	}

}
