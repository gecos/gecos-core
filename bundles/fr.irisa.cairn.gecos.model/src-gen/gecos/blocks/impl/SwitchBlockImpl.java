/**
 */
package gecos.blocks.impl;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;

import fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;
import fr.irisa.cairn.gecos.model.tools.utils.BlocksContinuesBreaksFinder;
import fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder;
import fr.irisa.cairn.gecos.model.tools.utils.CaseBlockRelinker;

import gecos.annotations.AnnotationsFactory;
import gecos.annotations.ExtendedAnnotation;
import gecos.annotations.IAnnotation;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksFactory;
import gecos.blocks.BlocksPackage;
import gecos.blocks.BlocksVisitor;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ControlEdge;
import gecos.blocks.SwitchBlock;

import gecos.instrs.BranchType;
import gecos.instrs.BreakInstruction;
import gecos.instrs.Instruction;

import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Switch Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.blocks.impl.SwitchBlockImpl#getBodyBlock <em>Body Block</em>}</li>
 *   <li>{@link gecos.blocks.impl.SwitchBlockImpl#getCases <em>Cases</em>}</li>
 *   <li>{@link gecos.blocks.impl.SwitchBlockImpl#getDefaultBlock <em>Default Block</em>}</li>
 *   <li>{@link gecos.blocks.impl.SwitchBlockImpl#getDispatchBlock <em>Dispatch Block</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SwitchBlockImpl extends BlockImpl implements SwitchBlock {
	/**
	 * The cached value of the '{@link #getBodyBlock() <em>Body Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBodyBlock()
	 * @generated
	 * @ordered
	 */
	protected Block bodyBlock;

	/**
	 * The cached value of the '{@link #getCases() <em>Cases</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCases()
	 * @generated
	 * @ordered
	 */
	protected EMap<Long, Block> cases;

	/**
	 * The cached value of the '{@link #getDefaultBlock() <em>Default Block</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultBlock()
	 * @generated
	 * @ordered
	 */
	protected Block defaultBlock;

	/**
	 * The cached value of the '{@link #getDispatchBlock() <em>Dispatch Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDispatchBlock()
	 * @generated
	 * @ordered
	 */
	protected BasicBlock dispatchBlock;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SwitchBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BlocksPackage.Literals.SWITCH_BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block getBodyBlock() {
		return bodyBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBodyBlock(Block newBodyBlock, NotificationChain msgs) {
		Block oldBodyBlock = bodyBlock;
		bodyBlock = newBodyBlock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BlocksPackage.SWITCH_BLOCK__BODY_BLOCK, oldBodyBlock, newBodyBlock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBodyBlock(Block newBodyBlock) {
		if (newBodyBlock != bodyBlock) {
			NotificationChain msgs = null;
			if (bodyBlock != null)
				msgs = ((InternalEObject)bodyBlock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.SWITCH_BLOCK__BODY_BLOCK, null, msgs);
			if (newBodyBlock != null)
				msgs = ((InternalEObject)newBodyBlock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.SWITCH_BLOCK__BODY_BLOCK, null, msgs);
			msgs = basicSetBodyBlock(newBodyBlock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.SWITCH_BLOCK__BODY_BLOCK, newBodyBlock, newBodyBlock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Long, Block> getCases() {
		if (cases == null) {
			cases = new EcoreEMap<Long,Block>(BlocksPackage.Literals.CASE_MAP, CaseMapImpl.class, this, BlocksPackage.SWITCH_BLOCK__CASES);
		}
		return cases;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block getDefaultBlock() {
		return defaultBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultBlock(Block newDefaultBlock) {
		Block oldDefaultBlock = defaultBlock;
		defaultBlock = newDefaultBlock;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.SWITCH_BLOCK__DEFAULT_BLOCK, oldDefaultBlock, defaultBlock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicBlock getDispatchBlock() {
		return dispatchBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDispatchBlock(BasicBlock newDispatchBlock, NotificationChain msgs) {
		BasicBlock oldDispatchBlock = dispatchBlock;
		dispatchBlock = newDispatchBlock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BlocksPackage.SWITCH_BLOCK__DISPATCH_BLOCK, oldDispatchBlock, newDispatchBlock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDispatchBlock(BasicBlock newDispatchBlock) {
		if (newDispatchBlock != dispatchBlock) {
			NotificationChain msgs = null;
			if (dispatchBlock != null)
				msgs = ((InternalEObject)dispatchBlock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.SWITCH_BLOCK__DISPATCH_BLOCK, null, msgs);
			if (newDispatchBlock != null)
				msgs = ((InternalEObject)newDispatchBlock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.SWITCH_BLOCK__DISPATCH_BLOCK, null, msgs);
			msgs = basicSetDispatchBlock(newDispatchBlock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.SWITCH_BLOCK__DISPATCH_BLOCK, newDispatchBlock, newDispatchBlock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final BlocksVisitor visitor) {
		visitor.visitSwitchBlock(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BasicBlock> listBreakBlocks() {
		final BlocksContinuesBreaksFinder finder = new BlocksContinuesBreaksFinder();
		finder.doSwitch(this.getBodyBlock());
		return ECollections.<BasicBlock>unmodifiableEList(finder.getBreaks());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BreakInstruction> listBreakInstructions() {
		final BranchInstructionFinder finder = new BranchInstructionFinder();
		finder.doSwitch(this.getBodyBlock());
		return ECollections.<BreakInstruction>unmodifiableEList(finder.getBreaks());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlEdge connectFromBasic(final BasicBlock dispatch, final BranchType cond) {
		return this.getDispatchBlock().connectFromBasic(dispatch, cond);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlEdge> connectTo(final Block next, final BranchType cond) {
		final BasicEList<ControlEdge> res = ECollections.<ControlEdge>newBasicEList();
		Block _defaultBlock = this.getDefaultBlock();
		boolean _tripleNotEquals = (_defaultBlock != null);
		if (_tripleNotEquals) {
			res.addAll(this.getDefaultBlock().connectTo(next, BranchType.UNCONDITIONAL));
		}
		else {
			res.addAll(this.getDispatchBlock().connectTo(next, BranchType.IF_FALSE));
			res.addAll(this.getBodyBlock().connectTo(next, BranchType.UNCONDITIONAL));
		}
		final BlocksContinuesBreaksFinder visitor = new BlocksContinuesBreaksFinder();
		visitor.doSwitch(this.getBodyBlock());
		List<BasicBlock> _breaks = visitor.getBreaks();
		for (final BasicBlock bb : _breaks) {
			{
				ControlEdge[] _clone = ((ControlEdge[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(bb.getOutEdges(), ControlEdge.class)).clone();
				for (final ControlEdge e : _clone) {
					{
						e.setFrom(null);
						e.setTo(null);
					}
				}
				bb.getOutEdges().clear();
				res.addAll(bb.connectTo(next, BranchType.UNCONDITIONAL));
			}
		}
		return res;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block simplifyBlock() {
		Block _defaultBlock = this.getDefaultBlock();
		boolean _tripleNotEquals = (_defaultBlock != null);
		if (_tripleNotEquals) {
			this.getDefaultBlock().simplifyBlock();
		}
		Block _bodyBlock = this.getBodyBlock();
		boolean _tripleNotEquals_1 = (_bodyBlock != null);
		if (_tripleNotEquals_1) {
			this.getBodyBlock().simplifyBlock();
		}
		Block _defaultBlock_1 = this.getDefaultBlock();
		boolean _tripleNotEquals_2 = (_defaultBlock_1 != null);
		if (_tripleNotEquals_2) {
			this.getDefaultBlock().simplifyBlock();
		}
		return this;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void replace(final Block oldb, final Block newb) {
		Block _defaultBlock = this.getDefaultBlock();
		boolean _tripleEquals = (oldb == _defaultBlock);
		if (_tripleEquals) {
			this.setDefaultBlock(newb);
		}
		else {
			if (((this.getDispatchBlock() == oldb) && (newb instanceof BasicBlock))) {
				this.setDispatchBlock(((BasicBlock) newb));
			}
			else {
				Block _bodyBlock = this.getBodyBlock();
				boolean _tripleNotEquals = (_bodyBlock != null);
				if (_tripleNotEquals) {
					Block _bodyBlock_1 = this.getBodyBlock();
					boolean _tripleEquals_1 = (oldb == _bodyBlock_1);
					if (_tripleEquals_1) {
						this.setBodyBlock(newb);
						throw new UnsupportedOperationException("Not yet implemented");
					}
				}
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeBlock(final Block bb) {
		BasicBlock _dispatchBlock = this.getDispatchBlock();
		boolean _tripleEquals = (_dispatchBlock == bb);
		if (_tripleEquals) {
			this.setDispatchBlock(null);
		}
		else {
			Block _bodyBlock = this.getBodyBlock();
			boolean _tripleEquals_1 = (_bodyBlock == bb);
			if (_tripleEquals_1) {
				this.setBodyBlock(null);
			}
			else {
				Block _defaultBlock = this.getDefaultBlock();
				boolean _tripleEquals_2 = (_defaultBlock == bb);
				if (_tripleEquals_2) {
					this.setDefaultBlock(null);
				}
				else {
					Block _bodyBlock_1 = this.getBodyBlock();
					boolean _tripleNotEquals = (_bodyBlock_1 != null);
					if (_tripleNotEquals) {
						this.getBodyBlock().removeBlock(bb);
					}
				}
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block managedCopy(final BlockCopyManager mng) {
		final SwitchBlock switchBlock = BlocksFactory.eINSTANCE.createSwitchBlock();
		final Block copy = this.getBodyBlock().managedCopy(mng);
		switchBlock.setBodyBlock(copy);
		Block _bodyBlock = this.getBodyBlock();
		final CompositeBlock cb = ((CompositeBlock) _bodyBlock);
		Block _managedCopy = this.getDispatchBlock().managedCopy(mng);
		final BasicBlock copy2 = ((BasicBlock) _managedCopy);
		cb.getChildren().add(copy2);
		switchBlock.setDispatchBlock(copy2);
		final Block copy3 = this.getDefaultBlock().managedCopy(mng);
		cb.getChildren().add(copy3);
		switchBlock.setDefaultBlock(copy3);
		Set<Long> _keySet = this.getCases().keySet();
		for (final Long key : _keySet) {
			{
				final Block caseBranch = this.getCases().get(key);
				final ExtendedAnnotation ext = AnnotationsFactory.eINSTANCE.createExtendedAnnotation();
				ext.getFields().add(this);
				ext.getFields().add(caseBranch);
				caseBranch.getAnnotations().put(CaseBlockRelinker.RELINKING_KEY, ext);
			}
		}
		final CaseBlockRelinker relinker = new CaseBlockRelinker(switchBlock);
		relinker.relink();
		Set<Long> _keySet_1 = this.getCases().keySet();
		for (final Long key_1 : _keySet_1) {
			{
				final Block caseBranch = this.getCases().get(key_1);
				final IAnnotation annot = caseBranch.getAnnotation(CaseBlockRelinker.RELINKING_KEY);
				caseBranch.getAnnotations().remove(annot);
			}
		}
		int _number = this.getNumber();
		int _plus = (_number + 1000);
		switchBlock.setNumber(_plus);
		GecosUserAnnotationFactory.copyAnnotations(this, switchBlock);
		mng.link(this, switchBlock);
		return switchBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toShortString() {
		int _number = this.getNumber();
		return ("Switch_" + Integer.valueOf(_number));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _pathString = this.getPathString();
		String _plus = (_pathString + "(");
		EList<Instruction> _instructions = this.getDispatchBlock().getInstructions();
		String _plus_1 = (_plus + _instructions);
		return (_plus_1 + ")");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BlocksPackage.SWITCH_BLOCK__BODY_BLOCK:
				return basicSetBodyBlock(null, msgs);
			case BlocksPackage.SWITCH_BLOCK__CASES:
				return ((InternalEList<?>)getCases()).basicRemove(otherEnd, msgs);
			case BlocksPackage.SWITCH_BLOCK__DISPATCH_BLOCK:
				return basicSetDispatchBlock(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BlocksPackage.SWITCH_BLOCK__BODY_BLOCK:
				return getBodyBlock();
			case BlocksPackage.SWITCH_BLOCK__CASES:
				if (coreType) return getCases();
				else return getCases().map();
			case BlocksPackage.SWITCH_BLOCK__DEFAULT_BLOCK:
				return getDefaultBlock();
			case BlocksPackage.SWITCH_BLOCK__DISPATCH_BLOCK:
				return getDispatchBlock();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BlocksPackage.SWITCH_BLOCK__BODY_BLOCK:
				setBodyBlock((Block)newValue);
				return;
			case BlocksPackage.SWITCH_BLOCK__CASES:
				((EStructuralFeature.Setting)getCases()).set(newValue);
				return;
			case BlocksPackage.SWITCH_BLOCK__DEFAULT_BLOCK:
				setDefaultBlock((Block)newValue);
				return;
			case BlocksPackage.SWITCH_BLOCK__DISPATCH_BLOCK:
				setDispatchBlock((BasicBlock)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BlocksPackage.SWITCH_BLOCK__BODY_BLOCK:
				setBodyBlock((Block)null);
				return;
			case BlocksPackage.SWITCH_BLOCK__CASES:
				getCases().clear();
				return;
			case BlocksPackage.SWITCH_BLOCK__DEFAULT_BLOCK:
				setDefaultBlock((Block)null);
				return;
			case BlocksPackage.SWITCH_BLOCK__DISPATCH_BLOCK:
				setDispatchBlock((BasicBlock)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BlocksPackage.SWITCH_BLOCK__BODY_BLOCK:
				return bodyBlock != null;
			case BlocksPackage.SWITCH_BLOCK__CASES:
				return cases != null && !cases.isEmpty();
			case BlocksPackage.SWITCH_BLOCK__DEFAULT_BLOCK:
				return defaultBlock != null;
			case BlocksPackage.SWITCH_BLOCK__DISPATCH_BLOCK:
				return dispatchBlock != null;
		}
		return super.eIsSet(featureID);
	}

} //SwitchBlockImpl
