/**
 */
package gecos.annotations;

import gecos.core.Symbol;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pragma Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.annotations.PragmaAnnotation#getContent <em>Content</em>}</li>
 * </ul>
 *
 * @see gecos.annotations.AnnotationsPackage#getPragmaAnnotation()
 * @model
 * @generated
 */
public interface PragmaAnnotation extends IAnnotation {
	/**
	 * Returns the value of the '<em><b>Content</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Content</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content</em>' attribute list.
	 * @see gecos.annotations.AnnotationsPackage#getPragmaAnnotation_Content()
	 * @model dataType="gecos.annotations.String"
	 * @generated
	 */
	EList<String> getContent();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.annotations.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%java.lang.String%&gt;, &lt;%java.lang.CharSequence%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%java.lang.String%&gt;, &lt;%java.lang.CharSequence%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.CharSequence%&gt; apply(final &lt;%java.lang.String%&gt; it)\n\t{\n\t\treturn ((\"\\\"\" + it) + \"\\\"\");\n\t}\n};\nreturn &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%java.lang.String%&gt;&gt;join(this.getContent(), \"#pragma {\", \",\", \"}\", _function);'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%fr.irisa.cairn.gecos.model.tools.utils.GecosCopier%&gt; gecosCopier = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.GecosCopier%&gt;();\n&lt;%org.eclipse.emf.ecore.EObject%&gt; _copy = gecosCopier.copy(this);\nfinal T symbol = ((T) _copy);\ngecosCopier.copyReferences();\nreturn symbol;'"
	 * @generated
	 */
	<T extends Symbol> T copy();

} // PragmaAnnotation
