package fr.irisa.cairn.gecos.model.transforms.commonsubexpressionelimination.internal;

import java.util.ArrayList;

import fr.irisa.cairn.gecos.model.analysis.instructions.comparator.InstructionComparator;
import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksDefaultVisitor;
import gecos.blocks.BasicBlock;
import gecos.instrs.Instruction;

public class AvailableExpressionIN extends GecosBlocksDefaultVisitor {
	/**
	 * Input
	 */
	private ArrayList<ArrayList<Instruction>> EVAL;// EVAL:evaluated
											    // expr for each BB
	private ArrayList<ArrayList<Instruction>> KILL;// KILL : killed
										        // expr for each BB
	private ArrayList<Integer> BBID;
	int currentBB;
	/**
	 * Output
	 */
	private ArrayList<ArrayList<Instruction>> AEin;
	private ArrayList<ArrayList<Instruction>> AEout;
	boolean modifDone = false;
	
	public boolean getmodifDone() {
		return this.modifDone;
	}
	
	public ArrayList<ArrayList<Instruction>> getAEin() {
		return this.AEin;
	}
	
	public ArrayList<ArrayList<Instruction>> getAEout() {
		return this.AEout;
	}

	public AvailableExpressionIN(ArrayList<ArrayList<Instruction>> ev, ArrayList<ArrayList<Instruction>> kl, ArrayList<Integer> bbId, 
			ArrayList<ArrayList<Instruction>> in, ArrayList<ArrayList<Instruction>> out) {
		super();
		this.EVAL = ev;
		this.KILL = kl;
		this.BBID = bbId;// identifier of each basic block
		this.AEin = in;
		this.AEout = out;		
		currentBB = 0;
		modifDone = false;
	}

	@Override
	public void visitBasicBlock(BasicBlock b) {
		ArrayList<BasicBlock> abb = new ArrayList<BasicBlock>(b.getPredecessors());
		int nbPred = abb.size();
		ArrayList<Instruction> OldAEin = new ArrayList<Instruction>(AEin.get(currentBB));
		ArrayList<Instruction> OldAEout = new ArrayList<Instruction>(AEout.get(currentBB));
		
		//System.out.println("COMPUTIMG AEin : "+currentBB+" number :"+b.getNumber()+":");
		
		if (nbPred == 0)
			AEin.get(currentBB).clear();
		else {
			if (nbPred == 1) {
				BasicBlock bb = abb.get(0);
				int index = BBID.indexOf(bb.getNumber());
				AEin.set(currentBB,AEout.get(index));
			} else {
				// Intersection between element 0 and 1 
				BasicBlock bb0 = abb.get(0);
				BasicBlock bb1 = abb.get(1);
				
				int index0 = BBID.indexOf(bb0.getNumber());
				int index1 = BBID.indexOf(bb1.getNumber());
				
				ArrayList<Instruction> list0 = AEout.get(index0);				
				ArrayList<Instruction> list1 = AEout.get(index1);
				
				//System.out.println("INTERSECTION :");
				//System.out.println("list0 : "+list0);
				//System.out.println("list1 : "+list1);
				
				ArrayList<Instruction> intersection = intersection (list0, list1) ;
				
				//System.out.println("intersection : "+intersection);
				
				for (int i=2 ; i<nbPred ; i++) {
					BasicBlock bb = abb.get(i);
					int index = BBID.indexOf(bb.getNumber());
					ArrayList<Instruction> list = AEout.get(index);
					intersection = intersection (intersection, list) ;
					
					//System.out.println("       list : "+list);
					//System.out.println("       intersection : "+intersection);
				}
				AEin.set(currentBB,intersection);
			}
		}
		
		//System.out.println("AEin.get("+currentBB+") = "+AEin.get(currentBB));
		//System.out.println("Computing AEout :");
		
		ArrayList<Instruction> currentAEout = ComputeAEOut_i(EVAL.get(currentBB), AEin.get(currentBB), KILL.get(currentBB));
		AEout.set(currentBB, currentAEout);

		if(!modifDone) {
			modifDone = ModifiedOrNot (OldAEin, AEin.get(currentBB));
			if(!modifDone)
				modifDone = ModifiedOrNot (OldAEout, AEout.get(currentBB));
		}
		currentBB++;
	}

	
	private boolean ModifiedOrNot(ArrayList<Instruction> olde, ArrayList<Instruction> newe) {
		if(olde.size() != newe.size())
			return true;
		for(Instruction o : olde) {
			boolean found = false;
			String os = InstructionComparator.instToString(o).trim();
			for(Instruction n : newe) {
				String ns = InstructionComparator.instToString(n).trim();
				if (os.equals(ns)) {
					found = true;
					break;
				}
			}
			if (!found)
				return true;
		}
		return false;
	}

	private ArrayList<Instruction> intersection(ArrayList<Instruction> list0, ArrayList<Instruction> list1) {
		ArrayList<Instruction> output = new ArrayList<Instruction>();
		if (list0.isEmpty() || list1.isEmpty())
			return output;
		for(Instruction instr0 : list0) {
			String ch0 = InstructionComparator.instToString(instr0);
			for(Instruction instr1 : list1) {
				String ch1 = InstructionComparator.instToString(instr1);
				if (ch0.equals(ch1)) {
					output.add(instr0);
					break;
				}
			}
		}
		return output;
	}

	private ArrayList<Instruction> ComputeAEOut_i(ArrayList<Instruction> currentEval, ArrayList<Instruction> currentAEin,
			ArrayList<Instruction> currentKill) {
		// AEout(i) = EVAL(i) U ( AEin(i) - KILL(i) )
		
		//System.out.println("AEout(i) = EVAL(i) U ( AEin(i) - KILL(i) )");
		//System.out.println("EVAL(i) : "+currentEval);
		//System.out.println("AEin(i) : "+currentAEin);
		//System.out.println("KILL(i) : "+currentKill);
		
		ArrayList<Instruction> current_AEout = new ArrayList<Instruction>(currentEval);
		
		// Compute : AEin(i) - KILL(i)
		ArrayList<Instruction> aux = new ArrayList<Instruction>(currentAEin);
		
		for(Instruction instkill : currentKill) {
			String skl = InstructionComparator.instToString(instkill);
			for(Instruction instAEIN : currentAEin) {
				String sin = InstructionComparator.instToString(instAEIN);
				if (skl.equals(sin))
					aux.remove(instAEIN);
			}
		}
		
		//System.out.println("Compute : AEin(i) - KILL(i)  : "+aux);
		
		// Compute the union
		//System.out.println("Compute the union : ");
		
		for(Instruction instr : aux) {
			String newinst = InstructionComparator.instToString(instr);
			boolean notfound = true;
			for(Instruction instr2 : current_AEout) {
				String oldinst = InstructionComparator.instToString(instr2);
				if (oldinst.equals(newinst)) {
					notfound = false;
					break;
				}
			}
			if (notfound)
				current_AEout.add(instr);
		}
		
		//System.out.println("current_AEout : "+current_AEout);
		//System.out.println("=======================================================");
		
		return current_AEout;
	}
}
