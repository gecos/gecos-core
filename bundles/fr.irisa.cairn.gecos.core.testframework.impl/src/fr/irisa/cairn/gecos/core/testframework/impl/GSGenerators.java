package fr.irisa.cairn.gecos.core.testframework.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import fr.irisa.cairn.gecos.core.testframework.utils.GSProjectUtils;
import fr.irisa.cairn.gecos.testframework.exceptions.CodegenFailure;
import fr.irisa.cairn.gecos.testframework.model.IVersionOperator;
import gecos.gecosproject.GecosProject;

public class GSGenerators {

	private GSGenerators() {}
	
	
	/**
	 * Create a code generation operation that generates C code from the 
	 * {@link GecosProject} of each version in the directory specified by
	 * {@link GSProjectVersion#getOutputDir()}.
	 * 
	 * @param codegenDirProvider specify where to generate code for a given version.
	 * @param codeGenerator generate code for the specified project.
	 * @param copyExternalSources If true, copy {@link GSProjectVersion#getExternalSourceFiles() external sources}
	 * to the code gen directory as well.
	 * @return a code generator operation.
	 */
	public static <V extends GSProjectVersion> IVersionOperator<V> genericGenerator(
			Function<V, Path> codegenDirProvider,
			BiConsumer<GecosProject, Path> codeGenerator,
			boolean copyExternalSources) {
		return new IVersionOperator<V>() {
			@Override
			public void apply(V version) throws CodegenFailure {
				Path dir = codegenDirProvider.apply(version);
				try {
					codeGenerator.accept(version.getProject(), dir);
					if(copyExternalSources) {
						version.getExternalSourceFiles().stream()
							.forEach(f -> copy(f, dir, true));
					}
				} catch (Exception e) {
					throw new CodegenFailure("Failed to generate code for verion " + version, e);
				}
				
				// set version's generated files
				version.setGeneratedSourceFiles(version.getProject().getSources().stream()
					.map(gecosFile -> Paths.get(gecosFile.getName()).getFileName())
					.map(gecosFileName -> dir.resolve(gecosFileName))
					.collect(Collectors.toList()));
			}
		};
	}
	
	/**
	 * Shorthand to {@link #genericGenerator(Function, BiConsumer, boolean)}
	 * with {@link GSProjectUtils#generateCCode(GecosProject, Path)} as codeGenerator.
	 */
	public static <V extends GSProjectVersion> IVersionOperator<V> cGenerator(
			Function<V, Path> codegenDirProvider,
			boolean copyExternalSources) {
		return genericGenerator(codegenDirProvider, GSProjectUtils::generateCCode, copyExternalSources);
	}
	
	/**
	 * Create a code generation operation that generates C code from the 
	 * {@link GecosProject} of each version in the directory specified by
	 * {@link GSProjectVersion#getOutputDir()}.
	 * 
	 * @param copyExternalSources If true, copy {@link GSProjectVersion#getExternalSourceFiles() external sources}
	 * to the code gen directory as well.
	 * @return a code generator operation.
	 */
	public static <V extends GSProjectVersion> IVersionOperator<V> cGenerator(boolean copyExternalSources) {
		return cGenerator(GSProjectVersion::getOutputDir, copyExternalSources);
	}
	
	/**
	 * Create a code generation operation that simply copies all source files, attached to the 
	 * {@link GecosProject} of each version, to the directory specified by
	 * {@link GSProjectVersion#getOutputDir()}.
	 * 
	 * @param copyExternalSources If true, copy {@link GSProjectVersion#getExternalSourceFiles() external sources}
	 * to the code gen directory as well.
	 * @return a code generator operation.
	 */
	public static <V extends GSProjectVersion> IVersionOperator<V> codeCopier(boolean copyExternalSources) {
		return genericGenerator(GSProjectVersion::getOutputDir, 
				(proj, dir) -> proj.getSources().stream()
					.map(gf -> Paths.get(gf.getName()))
					.forEach(f -> copy(f, dir, true)),
				copyExternalSources);
	}
	
	
	private static Path copy(Path file, Path toDir, boolean override) {
		try {
			if(override)
				return Files.copy(file, toDir.resolve(file.getFileName()), StandardCopyOption.REPLACE_EXISTING);
			return Files.copy(file, toDir.resolve(file.getFileName()));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
