package fr.irisa.cairn.gecos.model.transforms.dag.internal;

import gecos.annotations.AnnotationsFactory;
import gecos.annotations.IAnnotation;
import gecos.annotations.PragmaAnnotation;
import gecos.annotations.StringAnnotation;
import gecos.annotations.util.AnnotationsSwitch;
import gecos.dag.DAGNode;
import gecos.instrs.Instruction;

public class DAGAnnotationBuilder extends AnnotationsSwitch<Object> {

	public  DAGNode node;
	public String currentKey;

	public DAGAnnotationBuilder() {

	}

	public  void buildDAGAnnotation(Instruction inst,DAGNode currentNode)
	{
		node=currentNode;
		if(inst.getAnnotations()!=null)
		{
			for(String key:inst.getAnnotations().keySet())
			{		currentKey=key;
			IAnnotation annot=inst.getAnnotations().get(key);
			doSwitch(annot);
			}
		}
	}

	@Override
	public Object caseStringAnnotation(StringAnnotation object) {


		StringAnnotation sAnnot=AnnotationsFactory.eINSTANCE.createStringAnnotation();
		sAnnot.setContent(object.getContent());
		node.getAnnotations().put(currentKey, sAnnot);

		return super.caseStringAnnotation(object);
	}
	@Override
	public Object casePragmaAnnotation(PragmaAnnotation object) {

		return super.casePragmaAnnotation(object);
	}
}
