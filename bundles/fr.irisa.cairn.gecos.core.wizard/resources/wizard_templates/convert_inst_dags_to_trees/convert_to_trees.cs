project = CreateGecosProject("tutorial"); # Just create a GeCoS project
AddSourceToGecosProject(project, "src/"); # Add source to the GeCoS project
CDTFrontend(project); # Call the GeCoS Front-end and build the GeCoS IR corresponding to the project

GecosTreeToDAGIRConversion(project); # Convert instruction trees to DAGs representation
SaveGecosProject(project, "project_dag.gecosproject");

GecosDAGToTreeIRConversion(project); # Convert instruction DAGs to trees representation
SaveGecosProject(project, "project_tree.gecosproject");

output(project, "dot", "trees_folder"); # Generate project into graphviz format