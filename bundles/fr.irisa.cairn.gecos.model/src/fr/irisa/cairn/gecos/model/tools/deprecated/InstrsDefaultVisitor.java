package fr.irisa.cairn.gecos.model.tools.deprecated;

import org.eclipse.emf.common.util.EList;

import gecos.blocks.BasicBlock;
import gecos.instrs.AddressComponent;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.ArrayValueInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.CondInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.ExprComponent;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.util.InstrsSwitch;
/**
 * 
 * @author amorvan
 * @generated NOT
 * @deprecated use {@link fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDefaultVisitor}
 */
@Deprecated
public abstract class InstrsDefaultVisitor extends InstrsSwitch<Object> {

	//add a doSwitch entry to easily browse BasicBlock
	public void doSwitch(BasicBlock bb) {
		Instruction[] is = bb.getInstructions().toArray(new Instruction[bb.getInstructions().size()]);
		for (Instruction i : is) doSwitch(i);
	}

	@Override
	public Object caseGenericInstruction(GenericInstruction object) {
		for (Instruction op : object.getOperands().toArray(new Instruction[object.getOperands().size()])) {
			doSwitch(op);
		}
		return true;
	}
	/*
	@Override
	public Object caseComplexInstruction(ComplexInstruction object) {
		for (Instruction child : (Instruction[]) object.getChildren().toArray()) {
			doSwitch(child);
		}
		return null;
	}//*/

	@Override
	public Object caseCondInstruction(CondInstruction object) {
		if (object.getCond() != null) doSwitch(object.getCond());
		return true;
	}

	@Override
	public Object caseArrayInstruction(ArrayInstruction object) {
		for (Instruction child : object.listChildren()) {
			doSwitch(child);
		}
		return true;
	}

	@Override
	public Object caseArrayValueInstruction(ArrayValueInstruction object) {
		for (Instruction child : object.getChildren().toArray(new Instruction[object.getChildren().size()])) {
			doSwitch(child);
		}
		return true;
	}

	@Override
	public Object caseAddressComponent(AddressComponent object) {
		doSwitch(object.getAddress());
		return true;
	}

	@Override
	public Object caseExprComponent(ExprComponent object) {
		if(object.getExpr()!=null){
			doSwitch(object.getExpr());
		}
		return true;
	}

	@Override
	public Object caseSetInstruction(SetInstruction object) {
		doSwitch(object.getSource());
		doSwitch(object.getDest());
		return true;
	}

	@Override
	public Object caseCallInstruction(CallInstruction object) {
		EList<Instruction> listChildren = object.listChildren();
		for (Instruction child : listChildren.toArray(new Instruction[listChildren.size()])) {
			doSwitch(child);
		}
		return true;
	}
	
	@Override
	public Object caseConvertInstruction(ConvertInstruction object) {
		doSwitch(object.getExpr());
		return true;
	}
}
