package fr.irisa.cairn.gecos.model.transforms.tac;

import fr.irisa.cairn.gecos.model.transforms.tac.internal.TACFineGrainBlockManager;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;

/**
 * Add temporary variable for each operand and output of operator.
 * This transformation let to quantify the number of bit of the operator thanks to data (temporary variable)
 * @author Nicolas Simon
 * @date Apr 15, 2013
 */
@Deprecated
public class ThreeAddressCodeComputationFineGrain {
	private TACFineGrainBlockManager _blockManager;
	
	private GecosProject _project;
	private ProcedureSet _ps;
	
	public ThreeAddressCodeComputationFineGrain(GecosProject project){
		_blockManager = new TACFineGrainBlockManager();		
		_project = project;
	}
	
	public ThreeAddressCodeComputationFineGrain(ProcedureSet ps){
		_blockManager = new TACFineGrainBlockManager();
		_ps = ps;
	}
	
	public void compute(){
		if(_ps != null){
			for(Procedure p : _ps.listProcedures())
				_blockManager.doSwitch(p.getBody());
		}
		else if(_project != null){
			for(Procedure p : _project.listProcedures())
				_blockManager.doSwitch(p.getBody());
		}
		else{
			throw new RuntimeException("Project or ProcedureSet given are null!");
		}
	}
}
