package fr.irisa.cairn.gecos.model.blocks.tests;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.instructions.utils.InstructionDataProvider;
import gecos.blocks.BasicBlock;
import gecos.blocks.CompositeBlock;
import gecos.instrs.Instruction;

/**
 * 
 * @author aelmouss
 */
@RunWith(DataProviderRunner.class)
public class TestBasicBlock {

	@Test
	@UseDataProvider(location=InstructionDataProvider.class, value="provideBasicBlocks")
	public void testSplitAt(BasicBlock bb) {
		System.out.println(bb);
		
		Assume.assumeFalse(bb.getInstructions().isEmpty());
		
		// test splitAt()
		testSplitAt(bb.copy(), 0);
		
		testSplitAt(bb.copy(), bb.getInstructionCount()-1);
		
		if(bb.getInstructionCount() > 1) {
			testSplitAt(bb.copy(), 1);
		}
		
	}
	
	void testSplitAt(BasicBlock bb, int idxAt) {
		Instruction at = bb.getInstruction(idxAt);
		CompositeBlock parent = GecosUserBlockFactory.CompositeBlock(bb);
		List<Instruction> before = new ArrayList<>(bb.getInstructions());
		
		BasicBlock split = bb.splitAt(at);
		
		Assert.assertTrue("test splitAt()", 
				parent.getChildren().size() == 2
				&& bb.getInstructionCount() == idxAt
				&& split.getInstructionCount() + bb.getInstructionCount() + 1 == before.size()
		);
		List<Instruction> after = new ArrayList<>(bb.getInstructions());
		after.addAll(split.getInstructions());
		before.remove(idxAt);
		Assert.assertTrue("test splitAt()", after.equals(before));
	}
	
}
	