/**
 */
package gecos.dag;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG String Imm Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.DAGStringImmNode#getStringValue <em>String Value</em>}</li>
 * </ul>
 *
 * @see gecos.dag.DagPackage#getDAGStringImmNode()
 * @model
 * @generated
 */
public interface DAGStringImmNode extends DAGImmNode, DagVisitable {
	/**
	 * Returns the value of the '<em><b>String Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>String Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String Value</em>' attribute.
	 * @see #setStringValue(String)
	 * @see gecos.dag.DagPackage#getDAGStringImmNode_StringValue()
	 * @model unique="false" dataType="gecos.dag.String" derived="true"
	 * @generated
	 */
	String getStringValue();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGStringImmNode#getStringValue <em>String Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>String Value</em>' attribute.
	 * @see #getStringValue()
	 * @generated
	 */
	void setStringValue(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGStringImmNode(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.dag.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _ident = this.getIdent();\n&lt;%java.lang.String%&gt; _plus = (\"StringNode_\" + &lt;%java.lang.Integer%&gt;.valueOf(_ident));\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \":\");\n&lt;%java.lang.String%&gt; _stringValue = this.getStringValue();\nreturn (_plus_1 + _stringValue);'"
	 * @generated
	 */
	String toString();

} // DAGStringImmNode
