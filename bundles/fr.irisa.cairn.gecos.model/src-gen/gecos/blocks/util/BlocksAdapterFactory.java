/**
 */
package gecos.blocks.util;

import gecos.annotations.AnnotatedElement;

import gecos.blocks.*;

import gecos.core.CoreVisitable;
import gecos.core.GecosNode;
import gecos.core.ScopeContainer;

import java.util.Map;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see gecos.blocks.BlocksPackage
 * @generated
 */
public class BlocksAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static BlocksPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlocksAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = BlocksPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BlocksSwitch<Adapter> modelSwitch =
		new BlocksSwitch<Adapter>() {
			@Override
			public Adapter caseBlocksVisitor(BlocksVisitor object) {
				return createBlocksVisitorAdapter();
			}
			@Override
			public Adapter caseBlocksVisitable(BlocksVisitable object) {
				return createBlocksVisitableAdapter();
			}
			@Override
			public Adapter caseBlock(Block object) {
				return createBlockAdapter();
			}
			@Override
			public Adapter caseBreakable(Breakable object) {
				return createBreakableAdapter();
			}
			@Override
			public Adapter caseContinuable(Continuable object) {
				return createContinuableAdapter();
			}
			@Override
			public Adapter caseLoop(Loop object) {
				return createLoopAdapter();
			}
			@Override
			public Adapter caseBasicBlock(BasicBlock object) {
				return createBasicBlockAdapter();
			}
			@Override
			public Adapter caseCompositeBlock(CompositeBlock object) {
				return createCompositeBlockAdapter();
			}
			@Override
			public Adapter caseControlEdge(ControlEdge object) {
				return createControlEdgeAdapter();
			}
			@Override
			public Adapter caseDataEdge(DataEdge object) {
				return createDataEdgeAdapter();
			}
			@Override
			public Adapter caseFlowEdge(FlowEdge object) {
				return createFlowEdgeAdapter();
			}
			@Override
			public Adapter caseIfBlock(IfBlock object) {
				return createIfBlockAdapter();
			}
			@Override
			public Adapter caseWhileBlock(WhileBlock object) {
				return createWhileBlockAdapter();
			}
			@Override
			public Adapter caseDoWhileBlock(DoWhileBlock object) {
				return createDoWhileBlockAdapter();
			}
			@Override
			public Adapter caseForBlock(ForBlock object) {
				return createForBlockAdapter();
			}
			@Override
			public Adapter caseForC99Block(ForC99Block object) {
				return createForC99BlockAdapter();
			}
			@Override
			public Adapter caseSimpleForBlock(SimpleForBlock object) {
				return createSimpleForBlockAdapter();
			}
			@Override
			public Adapter caseSwitchBlock(SwitchBlock object) {
				return createSwitchBlockAdapter();
			}
			@Override
			public Adapter caseCaseBlock(CaseBlock object) {
				return createCaseBlockAdapter();
			}
			@Override
			public Adapter caseCaseMap(Map.Entry<Long, Block> object) {
				return createCaseMapAdapter();
			}
			@Override
			public Adapter caseGecosNode(GecosNode object) {
				return createGecosNodeAdapter();
			}
			@Override
			public Adapter caseAnnotatedElement(AnnotatedElement object) {
				return createAnnotatedElementAdapter();
			}
			@Override
			public Adapter caseCoreVisitable(CoreVisitable object) {
				return createCoreVisitableAdapter();
			}
			@Override
			public Adapter caseScopeContainer(ScopeContainer object) {
				return createScopeContainerAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.BlocksVisitor <em>Visitor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.BlocksVisitor
	 * @generated
	 */
	public Adapter createBlocksVisitorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.BlocksVisitable <em>Visitable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.BlocksVisitable
	 * @generated
	 */
	public Adapter createBlocksVisitableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.Block <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.Block
	 * @generated
	 */
	public Adapter createBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.Breakable <em>Breakable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.Breakable
	 * @generated
	 */
	public Adapter createBreakableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.Continuable <em>Continuable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.Continuable
	 * @generated
	 */
	public Adapter createContinuableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.Loop <em>Loop</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.Loop
	 * @generated
	 */
	public Adapter createLoopAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.BasicBlock <em>Basic Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.BasicBlock
	 * @generated
	 */
	public Adapter createBasicBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.CompositeBlock <em>Composite Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.CompositeBlock
	 * @generated
	 */
	public Adapter createCompositeBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.ControlEdge <em>Control Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.ControlEdge
	 * @generated
	 */
	public Adapter createControlEdgeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.DataEdge <em>Data Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.DataEdge
	 * @generated
	 */
	public Adapter createDataEdgeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.FlowEdge <em>Flow Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.FlowEdge
	 * @generated
	 */
	public Adapter createFlowEdgeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.IfBlock <em>If Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.IfBlock
	 * @generated
	 */
	public Adapter createIfBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.WhileBlock <em>While Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.WhileBlock
	 * @generated
	 */
	public Adapter createWhileBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.DoWhileBlock <em>Do While Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.DoWhileBlock
	 * @generated
	 */
	public Adapter createDoWhileBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.ForBlock <em>For Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.ForBlock
	 * @generated
	 */
	public Adapter createForBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.ForC99Block <em>For C99 Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.ForC99Block
	 * @generated
	 */
	public Adapter createForC99BlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.SimpleForBlock <em>Simple For Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.SimpleForBlock
	 * @generated
	 */
	public Adapter createSimpleForBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.SwitchBlock <em>Switch Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.SwitchBlock
	 * @generated
	 */
	public Adapter createSwitchBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.blocks.CaseBlock <em>Case Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.blocks.CaseBlock
	 * @generated
	 */
	public Adapter createCaseBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Case Map</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createCaseMapAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.core.GecosNode <em>Gecos Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.core.GecosNode
	 * @generated
	 */
	public Adapter createGecosNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.annotations.AnnotatedElement <em>Annotated Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.annotations.AnnotatedElement
	 * @generated
	 */
	public Adapter createAnnotatedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.core.CoreVisitable <em>Visitable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.core.CoreVisitable
	 * @generated
	 */
	public Adapter createCoreVisitableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.core.ScopeContainer <em>Scope Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.core.ScopeContainer
	 * @generated
	 */
	public Adapter createScopeContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //BlocksAdapterFactory
