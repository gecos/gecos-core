package fr.irisa.cairn.gecos.model.typesystem;

import gecos.gecosproject.GecosProject;
import gecos.instrs.Instruction;

public class StaticTypeSystem extends AbstractTypeSystem {

	public StaticTypeSystem(GecosProject gecosProject) {
		super(gecosProject, new SimpleTypingRules());
	}

	@Override
	protected void unknownInstructionTypeHandler(Instruction i) {
		System.err.println("Warning : could not derive type for " + (i));
	}	
}