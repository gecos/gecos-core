project = CreateGecosProject("tutorial"); # Just create a GeCoS project
AddSourceToGecosProject(project, "src/"); # Add source to the GeCoS project
CDTFrontend(project); # Call the GeCoS Front-end and build the GeCoS IR corresponding to the project

SaveGecosProject(project, "project_tree.gecosproject");
GecosTreeToDAGIRConversion(project); # Convert instrustion into DAG
SaveGecosProject(project, "project_dag.gecosproject");

output(project, "dot", "dags_folder"); # Generate the call graph into graphviz format