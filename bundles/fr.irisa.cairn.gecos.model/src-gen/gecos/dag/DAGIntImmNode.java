/**
 */
package gecos.dag;

import gecos.instrs.IntInstruction;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Int Imm Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.DAGIntImmNode#getIntValue <em>Int Value</em>}</li>
 *   <li>{@link gecos.dag.DAGIntImmNode#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see gecos.dag.DagPackage#getDAGIntImmNode()
 * @model
 * @generated
 */
public interface DAGIntImmNode extends DAGImmNode, DagVisitable {
	/**
	 * Returns the value of the '<em><b>Int Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Int Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Int Value</em>' attribute.
	 * @see #setIntValue(long)
	 * @see gecos.dag.DagPackage#getDAGIntImmNode_IntValue()
	 * @model unique="false" dataType="gecos.dag.long" derived="true"
	 * @generated
	 */
	long getIntValue();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGIntImmNode#getIntValue <em>Int Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Int Value</em>' attribute.
	 * @see #getIntValue()
	 * @generated
	 */
	void setIntValue(long value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(IntInstruction)
	 * @see gecos.dag.DagPackage#getDAGIntImmNode_Value()
	 * @model containment="true"
	 * @generated
	 */
	IntInstruction getValue();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGIntImmNode#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(IntInstruction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGIntImmNode(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.dag.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _ident = this.getIdent();\n&lt;%java.lang.String%&gt; _plus = (\"IntNode_\" + &lt;%java.lang.Integer%&gt;.valueOf(_ident));\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \":\");\n&lt;%gecos.instrs.IntInstruction%&gt; _value = this.getValue();\nreturn (_plus_1 + _value);'"
	 * @generated
	 */
	String toString();

} // DAGIntImmNode
