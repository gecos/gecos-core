package templates.xtend;

import fr.irisa.cairn.gecos.model.extensions.generators.IGecosCodeGenerator;
import org.eclipse.emf.ecore.EObject;

@SuppressWarnings("all")
public class GecosTemplate implements IGecosCodeGenerator {
  public Object generate(final EObject o) {
    return null;
  }
  
  @Override
  public String generate(final Object o) {
    Object _xblockexpression = null;
    {
      final Object object = this.generate(((EObject) o));
      if ((object != null)) {
        return object.toString();
      }
      _xblockexpression = null;
    }
    return ((String)_xblockexpression);
  }
}
