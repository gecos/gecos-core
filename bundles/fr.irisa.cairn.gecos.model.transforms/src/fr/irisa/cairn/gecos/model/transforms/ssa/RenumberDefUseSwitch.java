package fr.irisa.cairn.gecos.model.transforms.ssa;

import gecos.blocks.BasicBlock;
import gecos.core.ProcedureSymbol;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.CondInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.NumberedSymbolInstruction;
import gecos.instrs.PhiInstruction;
import gecos.instrs.RetInstruction;
import gecos.instrs.SSADefSymbol;
import gecos.instrs.SSAUseSymbol;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;

import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;

class RenumberDefUseSwitch extends RenumberSwitch {

	private static final int UNSET = 0;
	private static final int LHS = 1;
	private static final int RHS = 2;
	private int mode = UNSET;

	Map<SymbolInstruction,NumberedSymbolInstruction> changemap = new LinkedHashMap<SymbolInstruction, NumberedSymbolInstruction>();

	/**
	 * @param ssaAnalyser
	 */
	public RenumberDefUseSwitch(SSAAnalyser ssaAnalyser) {
		super(ssaAnalyser);
	}



	@Override
	public void run(BasicBlock bb) {
		super.run(bb);

		for (SymbolInstruction sym : changemap.keySet()) {
			NumberedSymbolInstruction nsym = changemap.get(sym);
			debug("Changing "+sym+" to "+nsym+" in "+sym.getRoot());
			ComplexInstruction parent = sym.getParent();
			if (parent!=null) {
				parent.replaceChild(sym, nsym);
			}
			debug("					=> "+parent.getRoot());
		}

	}

	@Override
	public Object caseSetInstruction(SetInstruction set) {
		int oldmode = mode;
		mode = RHS;
		doSwitch(set.getSource());
		mode = LHS;
		doSwitch(set.getDest());
		/**
		 * Here we should use a Tom based rule
		 */
		mode = oldmode;
		return this;
	}

	@Override
	public Object caseCallInstruction(CallInstruction g) {
		int oldmode = mode;
		mode = RHS;
		super.caseCallInstruction(g);
		mode = oldmode;
		return true;
	}
	
	@Override
	public Object caseGenericInstruction(GenericInstruction g) {
		int oldmode = mode;
		mode = RHS;
		super.caseGenericInstruction(g);
		mode = oldmode;
		return true;
	}

	@Override
	public Object caseCondInstruction(CondInstruction g) {
		int oldmode = mode;
		mode = RHS;
		super.caseCondInstruction(g);
		mode = oldmode;
		return true;
	}
	
	@Override
	public Object caseArrayInstruction(ArrayInstruction object) {
		return super.caseArrayInstruction(object);
	}

	@Override
	public Object caseSymbolInstruction(SymbolInstruction sym) {
		//Map<Integer, SSADefSymbol> defMap = this.ssaAnalyser.getDefUseMapFor(sym.getSymbol());

		if(!(sym.getSymbol() instanceof ProcedureSymbol)){
			if (mode == RHS) {
				analyzeRHS(sym);
			} else {
				analyzeLHS(sym);
			}
		}
		return super.caseSymbolInstruction(sym);
	}

	/**
	 * TODO : rewrite using TOM for better case coverage, and for avoiding ugly instanceof
	 * @param symInstr
	 */
	private void analyzeLHS(SymbolInstruction symInstr) {
		EObject eContainer = symInstr.eContainer();
		if(eContainer instanceof SetInstruction) {
			SSADefSymbol newsym = makeSSADefinition(symInstr);
			debug("  - Scalar assignement "+symInstr+" => "+newsym+" in "+symInstr.eContainer());
		} else if(eContainer instanceof RetInstruction) {
			EObject eCont = symInstr.eContainer();
			debug("Cannnot analyze LHS in "+eCont+":"+eCont.getClass().getSimpleName()+" ignoring");
		} else {
			//			throw new UnsupportedOperationException("Cannnot analyze LHS in "+symInstr.eContainer());
			//			if(eContainer instanceof ArrayInstruction) {
			//				ArrayInstruction array = (ArrayInstruction) symInstr.eContainer();
			//				if(array.getDest()==symInstr && array.eContainer() instanceof SetInstruction) {
			//					SSADefSymbol newsym = makeSSADefinition(symInstr);
			//					debug("Array cell assignement "+newsym+" in "+array.eContainer());
			//				} else {
			//					System.out.println("Unsupported assignement "+array.eContainer());
			//				}
			//			}
		}
	}

	@Override
	protected SSADefSymbol makeSSADefinition(SymbolInstruction symInstr) {
		SSADefSymbol res = super.makeSSADefinition(symInstr);
		changemap.put(symInstr, res);
		return res;
	}

	@Override
	protected SSAUseSymbol makeSSAUse(SymbolInstruction symInstr) {
		SSAUseSymbol res =  super.makeSSAUse(symInstr);
		changemap.put(symInstr, res);
		return res;
	}

	private void analyzeRHS(SymbolInstruction symInst) {
		if (symInst.getParent() == null) {
			throw new RuntimeException("SSA problem: "+symInst.eContainer()+ " for "+symInst);
		} else {
			makeSSAUse(symInst);
		}
	}

	@Override
	public Object casePhiInstruction(PhiInstruction g) { 
		return true;
	}
}