package fr.irisa.cairn.gecos.model.analysis.profiling;

import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.BBlock;
import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.CompositeBlock;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.array;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.gt;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.lt;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.mux;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.set;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.symbref;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.javatuples.Pair;

import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDefaultVisitor;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ForBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSymbol;
import gecos.core.Symbol;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;  

public class AssignmentDuplicator extends GecosBlocksInstructionsDefaultVisitor {

	private BasicBlock currentBasic;
	private Procedure proc;
	private boolean overall;
	private List<String> symbolName;
	
	public AssignmentDuplicator(Procedure proc,List<String> symbolName,boolean overall) {
		this.proc = proc;
		this.overall=overall;
		this.symbolName=symbolName;
	}
	
	public AssignmentDuplicator(Procedure proc) {
		this.proc = proc;
		this.overall=true;
	}

	public AssignmentDuplicator() {
	}
	
	public void compute(){
		if (proc != null)
			proc.getBody().accept(this);
	}
	
	@Override
	public void visitBasicBlock(BasicBlock b) {
		currentBasic = b;
		super.visitBasicBlock(b); 
	}
	
	@Override
	public void visitSetInstruction(SetInstruction inst) {
		System.out.println(inst.getDest().toString());
		if (inst.getDest() instanceof SymbolInstruction) {
			SymbolInstruction sym = (SymbolInstruction) inst.getDest();
			if (sym.getSymbol() == null) {
				System.out.println("*** " + sym.getSymbolName()
						+ " is not declare in a scope");
				return;
			}
			if (symbolName.contains(sym.getSymbolName()+"_"+proc.getSymbolName()))
				duplicateAssignment(inst, currentBasic);
		}else if (inst.getDest() instanceof ArrayInstruction){
			ArrayInstruction arr = (ArrayInstruction) inst.getDest();
			if (arr.getDest() instanceof SymbolInstruction){
				SymbolInstruction sym = (SymbolInstruction) arr.getDest();
				if (sym.getSymbol() == null) {
					System.out.println("*** " + sym.getSymbolName()
							+ " is not declare in a scope");
					return;
				}
				if (symbolName.contains(sym.getSymbolName()+"_"+proc.getSymbolName()))
					duplicateAssignment(inst, currentBasic);
			}
		}
	}
	
	private void duplicateAssignment(SetInstruction inst, BasicBlock b) { 
		Instruction instMin = null;
		Instruction instMax = null;
		if (inst.getDest() instanceof SymbolInstruction) {
			SymbolInstruction instSym = (SymbolInstruction) inst.getDest();
			Symbol symMin = b.getScope().getRoot().lookup(instSym.getSymbolName()+"_"+proc.getSymbolName()+"_min");
			Symbol symMax = b.getScope().getRoot().lookup(instSym.getSymbolName()+"_"+proc.getSymbolName()+"_max");
			if (symMin == null || symMax == null){
				//System.out.println(instSym.getSymbolName()+" is not to be handled");
				return;
			}
			instMin = symbref(symMin);
			instMax = symbref(symMax);
		}
		else if (inst.getDest() instanceof ArrayInstruction){
			ArrayInstruction arr = (ArrayInstruction) inst.getDest();
			if (arr.getDest() instanceof SymbolInstruction){
				SymbolInstruction instSym = (SymbolInstruction) arr.getDest();
				Symbol symMin = b.getScope().getRoot().lookup(instSym.getSymbolName()+"_"+proc.getSymbolName()+"_min");
				Symbol symMax = b.getScope().getRoot().lookup(instSym.getSymbolName()+"_"+proc.getSymbolName()+"_max");
				if (symMin == null || symMax == null)
					return;
				if (overall){
					instMin = symbref(symMin);
					instMax = symbref(symMax);
				}else{
					SymbolInstruction instSymMin = symbref(symMin);
					SymbolInstruction instSymMax = symbref(symMax);
					
					List<Instruction> indexTmpMin = new ArrayList<Instruction>();
					List<Instruction> indexTmpMax = new ArrayList<Instruction>();
					for (Instruction instTmp : arr.getIndex()){
						indexTmpMin.add(instTmp.copy());
						indexTmpMax.add(instTmp.copy());
					}
					instMin = array(instSymMin, indexTmpMin);
					instMax = array(instSymMax, indexTmpMax);
				}
			}
		}
		else{
			System.out.println("dupicateAssignment : case not handled/unknown");
			throw new RuntimeException();
		}
		Instruction condMin = lt(inst.getDest().copy(), instMin.copy());
		Instruction condMax = gt(inst.getDest().copy(), instMax.copy());
		Instruction muxMin = mux(condMin, inst.getDest().copy(), instMin.copy());
		Instruction muxMax = mux(condMax, inst.getDest().copy(), instMax.copy());
		SetInstruction setMin = set(instMin, muxMin);
		SetInstruction setMax = set(instMax, muxMax);
		
		Block parent = b.getParent();
		if (parent instanceof ForBlock){
			ForBlock forB = (ForBlock) parent;
			if(forB.getStepBlock() == b){
				Block bb = forB.getBodyBlock();
				BasicBlock newInsts = BBlock(setMin, setMax);
				CompositeBlock newBody = CompositeBlock(newInsts, bb);
				forB.setBodyBlock(newBody);
			}
		}else{
			b.insertInstructionAfter(setMin, inst.getRoot());
			b.insertInstructionAfter(setMax, inst.getRoot());
		}
	}	
}
