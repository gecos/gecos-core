/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.utils.callgraph;

import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;



public class CallGraph {

	private Map<String, CallGraphNode> nodes;

	public CallGraph() {
		nodes = new LinkedHashMap<String, CallGraphNode>();
	}

	public void add(CallGraphNode node) {
		nodes.put(node.procedure().getSymbol().getName(), node);
	}

	public void remove(CallGraphNode node) {
		nodes.remove(node.procedure().getSymbol().getName());
		node.disconnect();
	}

	public CallGraphNode get(String name) {
		return (CallGraphNode) nodes.get(name);
	}
	
	public int depth() {
		int maxDepth = 0;
		for (CallGraphNode node : nodes()) {
			if (node.inEdgesCount() == 0) {
				int depth = node.depth();
				if (depth > maxDepth)
					maxDepth = depth;
			}
		}
		return maxDepth;
	}

	public List<CallGraphNode> nodes() { return new Vector<CallGraphNode>(nodes.values()); }

	public void outputDot(PrintStream stream) {
		stream.println("digraph G {");
		int cnt = 0;
		Map<CallGraphNode, String> nodeMap = new LinkedHashMap<CallGraphNode, String>();
		for (CallGraphNode node : nodes()) {
			String id = "n" + (cnt++);
			nodeMap.put(node, id);
			stream.println(id+"[label=\""
				+node.procedure().getSymbol().getName()+"\",color=\""
				+(node.mode()==CallGraphNode.SIMPLE?"red":"black")+"\"];");
		}
		for (CallGraphNode node : nodes()) {
			for (CallGraphEdge e :node.outEdges()){
				stream.println(nodeMap.get(node)+" -> "
					+ nodeMap.get(e.to())+ ";");
			}
		}
		stream.println("}");
	}
	
	public Object accept(CallGraphVisitor visitor) {
		return visitor.visitCallGraph(this);
	}

	
}
