/**
 */
package gecos.gecosproject;

import gecos.core.GecosNode;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Project Visitable</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.gecosproject.GecosprojectPackage#getProjectVisitable()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ProjectVisitable extends GecosNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 * @generated
	 */
	void accept(ProjectVisitor visitor);

} // ProjectVisitable
