/**
 */
package gecos.dag.impl;

import gecos.annotations.AnnotationsPackage;

import gecos.annotations.impl.AnnotationsPackageImpl;

import gecos.blocks.BlocksPackage;

import gecos.blocks.impl.BlocksPackageImpl;

import gecos.core.CorePackage;

import gecos.core.impl.CorePackageImpl;

import gecos.dag.DAGArrayNode;
import gecos.dag.DAGArrayValueNode;
import gecos.dag.DAGCallNode;
import gecos.dag.DAGControlEdge;
import gecos.dag.DAGDataEdge;
import gecos.dag.DAGEdge;
import gecos.dag.DAGFieldInstruction;
import gecos.dag.DAGFloatImmNode;
import gecos.dag.DAGImmNode;
import gecos.dag.DAGInPort;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGIntImmNode;
import gecos.dag.DAGJumpNode;
import gecos.dag.DAGNode;
import gecos.dag.DAGNumberedOutNode;
import gecos.dag.DAGNumberedSymbolNode;
import gecos.dag.DAGOpNode;
import gecos.dag.DAGOperator;
import gecos.dag.DAGOutNode;
import gecos.dag.DAGOutPort;
import gecos.dag.DAGPatternNode;
import gecos.dag.DAGPhiNode;
import gecos.dag.DAGPort;
import gecos.dag.DAGSSADefNode;
import gecos.dag.DAGSSAUseNode;
import gecos.dag.DAGSimpleArrayNode;
import gecos.dag.DAGStringImmNode;
import gecos.dag.DAGSymbolNode;
import gecos.dag.DAGVectorArrayAccessNode;
import gecos.dag.DAGVectorExpandNode;
import gecos.dag.DAGVectorExtractNode;
import gecos.dag.DAGVectorNode;
import gecos.dag.DAGVectorOpNode;
import gecos.dag.DAGVectorPackNode;
import gecos.dag.DAGVectorShuffleNode;
import gecos.dag.DagFactory;
import gecos.dag.DagPackage;
import gecos.dag.DagVisitable;
import gecos.dag.DagVisitor;
import gecos.dag.DependencyType;

import gecos.instrs.InstrsPackage;

import gecos.instrs.impl.InstrsPackageImpl;

import gecos.types.TypesPackage;

import gecos.types.impl.TypesPackageImpl;

import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DagPackageImpl extends EPackageImpl implements DagPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagVisitorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagVisitableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagEdgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagInPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagOutPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagImmNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagSymbolNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagCallNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagArrayValueNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagOpNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagOutNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagControlEdgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagDataEdgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagNumberedSymbolNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagNumberedOutNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagIntImmNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagFloatImmNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagStringImmNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagArrayNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagSimpleArrayNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagFieldInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagJumpNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagPatternNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inPortMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass outPortMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagVectorNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagVectorArrayAccessNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagVectorOpNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagVectorShuffleNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagVectorExtractNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagVectorExpandNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagVectorPackNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagssaUseNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagssaDefNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dagPhiNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dependencyTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dagOperatorEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType stringEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType intEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType longEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType doubleEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see gecos.dag.DagPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private DagPackageImpl() {
		super(eNS_URI, DagFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link DagPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static DagPackage init() {
		if (isInited) return (DagPackage)EPackage.Registry.INSTANCE.getEPackage(DagPackage.eNS_URI);

		// Obtain or create and register package
		DagPackageImpl theDagPackage = (DagPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof DagPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new DagPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
		InstrsPackageImpl theInstrsPackage = (InstrsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI) instanceof InstrsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI) : InstrsPackage.eINSTANCE);
		AnnotationsPackageImpl theAnnotationsPackage = (AnnotationsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI) instanceof AnnotationsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI) : AnnotationsPackage.eINSTANCE);
		TypesPackageImpl theTypesPackage = (TypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI) instanceof TypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI) : TypesPackage.eINSTANCE);
		BlocksPackageImpl theBlocksPackage = (BlocksPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI) instanceof BlocksPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI) : BlocksPackage.eINSTANCE);

		// Create package meta-data objects
		theDagPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theInstrsPackage.createPackageContents();
		theAnnotationsPackage.createPackageContents();
		theTypesPackage.createPackageContents();
		theBlocksPackage.createPackageContents();

		// Initialize created meta-data
		theDagPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theInstrsPackage.initializePackageContents();
		theAnnotationsPackage.initializePackageContents();
		theTypesPackage.initializePackageContents();
		theBlocksPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theDagPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(DagPackage.eNS_URI, theDagPackage);
		return theDagPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDagVisitor() {
		return dagVisitorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDagVisitable() {
		return dagVisitableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGInstruction() {
		return dagInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGInstruction_Nodes() {
		return (EReference)dagInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGInstruction_Edges() {
		return (EReference)dagInstructionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDAGInstruction_Ident() {
		return (EAttribute)dagInstructionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGInstruction_SourceNodes() {
		return (EReference)dagInstructionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGInstruction_SinkNodes() {
		return (EReference)dagInstructionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGNode() {
		return dagNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGNode_Type() {
		return (EReference)dagNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGNode_Outputs() {
		return (EReference)dagNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGNode_Parent() {
		return (EReference)dagNodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGNode_Inputs() {
		return (EReference)dagNodeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDAGNode_Ident() {
		return (EAttribute)dagNodeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGEdge() {
		return dagEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGEdge_Src() {
		return (EReference)dagEdgeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGEdge_Sink() {
		return (EReference)dagEdgeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGEdge_Parent() {
		return (EReference)dagEdgeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGPort() {
		return dagPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDAGPort_Ident() {
		return (EAttribute)dagPortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGInPort() {
		return dagInPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGInPort_Source() {
		return (EReference)dagInPortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGOutPort() {
		return dagOutPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGOutPort_Sinks() {
		return (EReference)dagOutPortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGImmNode() {
		return dagImmNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGSymbolNode() {
		return dagSymbolNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDAGSymbolNode_Name() {
		return (EAttribute)dagSymbolNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGSymbolNode_Symbol() {
		return (EReference)dagSymbolNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGCallNode() {
		return dagCallNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDAGCallNode_Name() {
		return (EAttribute)dagCallNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGArrayValueNode() {
		return dagArrayValueNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGOpNode() {
		return dagOpNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDAGOpNode_Opcode() {
		return (EAttribute)dagOpNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGOutNode() {
		return dagOutNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGOutNode_Symbol() {
		return (EReference)dagOutNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGControlEdge() {
		return dagControlEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDAGControlEdge_Type() {
		return (EAttribute)dagControlEdgeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGDataEdge() {
		return dagDataEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGNumberedSymbolNode() {
		return dagNumberedSymbolNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDAGNumberedSymbolNode_Number() {
		return (EAttribute)dagNumberedSymbolNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGNumberedSymbolNode_SymbolDefinition() {
		return (EReference)dagNumberedSymbolNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGNumberedOutNode() {
		return dagNumberedOutNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDAGNumberedOutNode_Number() {
		return (EAttribute)dagNumberedOutNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGIntImmNode() {
		return dagIntImmNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDAGIntImmNode_IntValue() {
		return (EAttribute)dagIntImmNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGIntImmNode_Value() {
		return (EReference)dagIntImmNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGFloatImmNode() {
		return dagFloatImmNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDAGFloatImmNode_FloatValue() {
		return (EAttribute)dagFloatImmNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGFloatImmNode_Value() {
		return (EReference)dagFloatImmNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGStringImmNode() {
		return dagStringImmNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDAGStringImmNode_StringValue() {
		return (EAttribute)dagStringImmNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGArrayNode() {
		return dagArrayNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGSimpleArrayNode() {
		return dagSimpleArrayNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGFieldInstruction() {
		return dagFieldInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGFieldInstruction_Field() {
		return (EReference)dagFieldInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGJumpNode() {
		return dagJumpNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDAGJumpNode_Label() {
		return (EAttribute)dagJumpNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGPatternNode() {
		return dagPatternNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGPatternNode_Pattern() {
		return (EReference)dagPatternNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGPatternNode_InputsMap() {
		return (EReference)dagPatternNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGPatternNode_OutputsMap() {
		return (EReference)dagPatternNodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInPortMap() {
		return inPortMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInPortMap_Key() {
		return (EReference)inPortMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInPortMap_Value() {
		return (EReference)inPortMapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOutPortMap() {
		return outPortMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOutPortMap_Key() {
		return (EReference)outPortMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOutPortMap_Value() {
		return (EReference)outPortMapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGVectorNode() {
		return dagVectorNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGVectorArrayAccessNode() {
		return dagVectorArrayAccessNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGVectorOpNode() {
		return dagVectorOpNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDAGVectorOpNode_Opcode() {
		return (EAttribute)dagVectorOpNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGVectorShuffleNode() {
		return dagVectorShuffleNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDAGVectorShuffleNode_Permutation() {
		return (EAttribute)dagVectorShuffleNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGVectorExtractNode() {
		return dagVectorExtractNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDAGVectorExtractNode_Pos() {
		return (EAttribute)dagVectorExtractNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGVectorExpandNode() {
		return dagVectorExpandNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDAGVectorExpandNode_Permutation() {
		return (EAttribute)dagVectorExpandNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGVectorPackNode() {
		return dagVectorPackNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGSSAUseNode() {
		return dagssaUseNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGSSAUseNode_Def() {
		return (EReference)dagssaUseNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGSSADefNode() {
		return dagssaDefNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDAGSSADefNode_SSAUses() {
		return (EReference)dagssaDefNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDAGPhiNode() {
		return dagPhiNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDependencyType() {
		return dependencyTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDAGOperator() {
		return dagOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getString() {
		return stringEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getint() {
		return intEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getlong() {
		return longEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getdouble() {
		return doubleEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DagFactory getDagFactory() {
		return (DagFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		dagVisitorEClass = createEClass(DAG_VISITOR);

		dagVisitableEClass = createEClass(DAG_VISITABLE);

		dagInstructionEClass = createEClass(DAG_INSTRUCTION);
		createEReference(dagInstructionEClass, DAG_INSTRUCTION__NODES);
		createEReference(dagInstructionEClass, DAG_INSTRUCTION__EDGES);
		createEAttribute(dagInstructionEClass, DAG_INSTRUCTION__IDENT);
		createEReference(dagInstructionEClass, DAG_INSTRUCTION__SOURCE_NODES);
		createEReference(dagInstructionEClass, DAG_INSTRUCTION__SINK_NODES);

		dagNodeEClass = createEClass(DAG_NODE);
		createEReference(dagNodeEClass, DAG_NODE__TYPE);
		createEReference(dagNodeEClass, DAG_NODE__OUTPUTS);
		createEReference(dagNodeEClass, DAG_NODE__PARENT);
		createEReference(dagNodeEClass, DAG_NODE__INPUTS);
		createEAttribute(dagNodeEClass, DAG_NODE__IDENT);

		dagEdgeEClass = createEClass(DAG_EDGE);
		createEReference(dagEdgeEClass, DAG_EDGE__SRC);
		createEReference(dagEdgeEClass, DAG_EDGE__SINK);
		createEReference(dagEdgeEClass, DAG_EDGE__PARENT);

		dagPortEClass = createEClass(DAG_PORT);
		createEAttribute(dagPortEClass, DAG_PORT__IDENT);

		dagInPortEClass = createEClass(DAG_IN_PORT);
		createEReference(dagInPortEClass, DAG_IN_PORT__SOURCE);

		dagOutPortEClass = createEClass(DAG_OUT_PORT);
		createEReference(dagOutPortEClass, DAG_OUT_PORT__SINKS);

		dagImmNodeEClass = createEClass(DAG_IMM_NODE);

		dagSymbolNodeEClass = createEClass(DAG_SYMBOL_NODE);
		createEAttribute(dagSymbolNodeEClass, DAG_SYMBOL_NODE__NAME);
		createEReference(dagSymbolNodeEClass, DAG_SYMBOL_NODE__SYMBOL);

		dagCallNodeEClass = createEClass(DAG_CALL_NODE);
		createEAttribute(dagCallNodeEClass, DAG_CALL_NODE__NAME);

		dagArrayValueNodeEClass = createEClass(DAG_ARRAY_VALUE_NODE);

		dagOpNodeEClass = createEClass(DAG_OP_NODE);
		createEAttribute(dagOpNodeEClass, DAG_OP_NODE__OPCODE);

		dagOutNodeEClass = createEClass(DAG_OUT_NODE);
		createEReference(dagOutNodeEClass, DAG_OUT_NODE__SYMBOL);

		dagControlEdgeEClass = createEClass(DAG_CONTROL_EDGE);
		createEAttribute(dagControlEdgeEClass, DAG_CONTROL_EDGE__TYPE);

		dagDataEdgeEClass = createEClass(DAG_DATA_EDGE);

		dagNumberedSymbolNodeEClass = createEClass(DAG_NUMBERED_SYMBOL_NODE);
		createEAttribute(dagNumberedSymbolNodeEClass, DAG_NUMBERED_SYMBOL_NODE__NUMBER);
		createEReference(dagNumberedSymbolNodeEClass, DAG_NUMBERED_SYMBOL_NODE__SYMBOL_DEFINITION);

		dagNumberedOutNodeEClass = createEClass(DAG_NUMBERED_OUT_NODE);
		createEAttribute(dagNumberedOutNodeEClass, DAG_NUMBERED_OUT_NODE__NUMBER);

		dagIntImmNodeEClass = createEClass(DAG_INT_IMM_NODE);
		createEAttribute(dagIntImmNodeEClass, DAG_INT_IMM_NODE__INT_VALUE);
		createEReference(dagIntImmNodeEClass, DAG_INT_IMM_NODE__VALUE);

		dagFloatImmNodeEClass = createEClass(DAG_FLOAT_IMM_NODE);
		createEAttribute(dagFloatImmNodeEClass, DAG_FLOAT_IMM_NODE__FLOAT_VALUE);
		createEReference(dagFloatImmNodeEClass, DAG_FLOAT_IMM_NODE__VALUE);

		dagStringImmNodeEClass = createEClass(DAG_STRING_IMM_NODE);
		createEAttribute(dagStringImmNodeEClass, DAG_STRING_IMM_NODE__STRING_VALUE);

		dagArrayNodeEClass = createEClass(DAG_ARRAY_NODE);

		dagSimpleArrayNodeEClass = createEClass(DAG_SIMPLE_ARRAY_NODE);

		dagFieldInstructionEClass = createEClass(DAG_FIELD_INSTRUCTION);
		createEReference(dagFieldInstructionEClass, DAG_FIELD_INSTRUCTION__FIELD);

		dagJumpNodeEClass = createEClass(DAG_JUMP_NODE);
		createEAttribute(dagJumpNodeEClass, DAG_JUMP_NODE__LABEL);

		dagPatternNodeEClass = createEClass(DAG_PATTERN_NODE);
		createEReference(dagPatternNodeEClass, DAG_PATTERN_NODE__PATTERN);
		createEReference(dagPatternNodeEClass, DAG_PATTERN_NODE__INPUTS_MAP);
		createEReference(dagPatternNodeEClass, DAG_PATTERN_NODE__OUTPUTS_MAP);

		inPortMapEClass = createEClass(IN_PORT_MAP);
		createEReference(inPortMapEClass, IN_PORT_MAP__KEY);
		createEReference(inPortMapEClass, IN_PORT_MAP__VALUE);

		outPortMapEClass = createEClass(OUT_PORT_MAP);
		createEReference(outPortMapEClass, OUT_PORT_MAP__KEY);
		createEReference(outPortMapEClass, OUT_PORT_MAP__VALUE);

		dagVectorNodeEClass = createEClass(DAG_VECTOR_NODE);

		dagVectorArrayAccessNodeEClass = createEClass(DAG_VECTOR_ARRAY_ACCESS_NODE);

		dagVectorOpNodeEClass = createEClass(DAG_VECTOR_OP_NODE);
		createEAttribute(dagVectorOpNodeEClass, DAG_VECTOR_OP_NODE__OPCODE);

		dagVectorShuffleNodeEClass = createEClass(DAG_VECTOR_SHUFFLE_NODE);
		createEAttribute(dagVectorShuffleNodeEClass, DAG_VECTOR_SHUFFLE_NODE__PERMUTATION);

		dagVectorExtractNodeEClass = createEClass(DAG_VECTOR_EXTRACT_NODE);
		createEAttribute(dagVectorExtractNodeEClass, DAG_VECTOR_EXTRACT_NODE__POS);

		dagVectorExpandNodeEClass = createEClass(DAG_VECTOR_EXPAND_NODE);
		createEAttribute(dagVectorExpandNodeEClass, DAG_VECTOR_EXPAND_NODE__PERMUTATION);

		dagVectorPackNodeEClass = createEClass(DAG_VECTOR_PACK_NODE);

		dagssaUseNodeEClass = createEClass(DAGSSA_USE_NODE);
		createEReference(dagssaUseNodeEClass, DAGSSA_USE_NODE__DEF);

		dagssaDefNodeEClass = createEClass(DAGSSA_DEF_NODE);
		createEReference(dagssaDefNodeEClass, DAGSSA_DEF_NODE__SSA_USES);

		dagPhiNodeEClass = createEClass(DAG_PHI_NODE);

		// Create enums
		dependencyTypeEEnum = createEEnum(DEPENDENCY_TYPE);
		dagOperatorEEnum = createEEnum(DAG_OPERATOR);

		// Create data types
		stringEDataType = createEDataType(STRING);
		intEDataType = createEDataType(INT);
		longEDataType = createEDataType(LONG);
		doubleEDataType = createEDataType(DOUBLE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		InstrsPackage theInstrsPackage = (InstrsPackage)EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI);
		AnnotationsPackage theAnnotationsPackage = (AnnotationsPackage)EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		dagVisitableEClass.getESuperTypes().add(theCorePackage.getGecosNode());
		dagInstructionEClass.getESuperTypes().add(theInstrsPackage.getInstruction());
		dagInstructionEClass.getESuperTypes().add(this.getDagVisitable());
		dagNodeEClass.getESuperTypes().add(theAnnotationsPackage.getAnnotatedElement());
		dagNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagNodeEClass.getESuperTypes().add(theCorePackage.getITypedElement());
		dagEdgeEClass.getESuperTypes().add(this.getDagVisitable());
		dagEdgeEClass.getESuperTypes().add(theAnnotationsPackage.getAnnotatedElement());
		dagPortEClass.getESuperTypes().add(this.getDagVisitable());
		dagInPortEClass.getESuperTypes().add(this.getDAGPort());
		dagInPortEClass.getESuperTypes().add(this.getDagVisitable());
		dagOutPortEClass.getESuperTypes().add(this.getDAGPort());
		dagOutPortEClass.getESuperTypes().add(this.getDagVisitable());
		dagImmNodeEClass.getESuperTypes().add(this.getDAGNode());
		dagImmNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagSymbolNodeEClass.getESuperTypes().add(this.getDAGNode());
		dagSymbolNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagCallNodeEClass.getESuperTypes().add(this.getDAGNode());
		dagCallNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagArrayValueNodeEClass.getESuperTypes().add(this.getDAGNode());
		dagArrayValueNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagOpNodeEClass.getESuperTypes().add(this.getDAGNode());
		dagOpNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagOutNodeEClass.getESuperTypes().add(this.getDAGNode());
		dagOutNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagControlEdgeEClass.getESuperTypes().add(this.getDAGEdge());
		dagControlEdgeEClass.getESuperTypes().add(this.getDagVisitable());
		dagDataEdgeEClass.getESuperTypes().add(this.getDAGEdge());
		dagDataEdgeEClass.getESuperTypes().add(this.getDagVisitable());
		dagNumberedSymbolNodeEClass.getESuperTypes().add(this.getDAGSymbolNode());
		dagNumberedSymbolNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagNumberedOutNodeEClass.getESuperTypes().add(this.getDAGOutNode());
		dagNumberedOutNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagIntImmNodeEClass.getESuperTypes().add(this.getDAGImmNode());
		dagIntImmNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagFloatImmNodeEClass.getESuperTypes().add(this.getDAGImmNode());
		dagFloatImmNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagStringImmNodeEClass.getESuperTypes().add(this.getDAGImmNode());
		dagStringImmNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagArrayNodeEClass.getESuperTypes().add(this.getDAGNode());
		dagArrayNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagSimpleArrayNodeEClass.getESuperTypes().add(this.getDAGSymbolNode());
		dagSimpleArrayNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagFieldInstructionEClass.getESuperTypes().add(this.getDAGNode());
		dagJumpNodeEClass.getESuperTypes().add(this.getDAGNode());
		dagJumpNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagPatternNodeEClass.getESuperTypes().add(this.getDAGOpNode());
		dagPatternNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagVectorNodeEClass.getESuperTypes().add(this.getDAGNode());
		dagVectorNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagVectorArrayAccessNodeEClass.getESuperTypes().add(this.getDAGVectorNode());
		dagVectorArrayAccessNodeEClass.getESuperTypes().add(this.getDAGSimpleArrayNode());
		dagVectorArrayAccessNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagVectorOpNodeEClass.getESuperTypes().add(this.getDAGVectorNode());
		dagVectorOpNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagVectorShuffleNodeEClass.getESuperTypes().add(this.getDAGVectorNode());
		dagVectorShuffleNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagVectorExtractNodeEClass.getESuperTypes().add(this.getDAGVectorNode());
		dagVectorExtractNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagVectorExpandNodeEClass.getESuperTypes().add(this.getDAGVectorNode());
		dagVectorExpandNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagVectorPackNodeEClass.getESuperTypes().add(this.getDAGVectorNode());
		dagVectorPackNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagssaUseNodeEClass.getESuperTypes().add(this.getDAGSymbolNode());
		dagssaUseNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagssaDefNodeEClass.getESuperTypes().add(this.getDAGOutNode());
		dagssaDefNodeEClass.getESuperTypes().add(this.getDagVisitable());
		dagPhiNodeEClass.getESuperTypes().add(this.getDAGNode());
		dagPhiNodeEClass.getESuperTypes().add(this.getDagVisitable());

		// Initialize classes and features; add operations and parameters
		initEClass(dagVisitorEClass, DagVisitor.class, "DagVisitor", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = addEOperation(dagVisitorEClass, null, "visitDAGInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGInstruction(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGInPort", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGInPort(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGOutPort", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGOutPort(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGSymbolNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGSymbolNode(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGCallNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGCallNode(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGArrayValueNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGArrayValueNode(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGOpNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGOpNode(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGOutNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGOutNode(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGControlEdge", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGControlEdge(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGDataEdge", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGDataEdge(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGNumberedSymbolNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGNumberedSymbolNode(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGNumberedOutNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGNumberedOutNode(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGIntImmNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGIntImmNode(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGFloatImmNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGFloatImmNode(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGArrayNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGArrayNode(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGSimpleArrayNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGSimpleArrayNode(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGJumpNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGJumpNode(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGPatternNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGPatternNode(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGVectorArrayAccessNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGVectorArrayAccessNode(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGStringImmNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGStringImmNode(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGVectorOpNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGVectorOpNode(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGVectorShuffleNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGVectorShuffleNode(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGVectorExtractNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGVectorExtractNode(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGVectorExpandNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGVectorExpandNode(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGVectorPackNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGVectorPackNode(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGSSAUseNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGSSAUseNode(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGSSADefNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGSSADefNode(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGPhiNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGPhiNode(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagVisitorEClass, null, "visitDAGFieldInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGFieldInstruction(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagVisitableEClass, DagVisitable.class, "DagVisitable", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(dagVisitableEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagInstructionEClass, DAGInstruction.class, "DAGInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDAGInstruction_Nodes(), this.getDAGNode(), this.getDAGNode_Parent(), "nodes", null, 0, -1, DAGInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDAGInstruction_Edges(), this.getDAGEdge(), this.getDAGEdge_Parent(), "edges", null, 0, -1, DAGInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDAGInstruction_Ident(), this.getint(), "ident", null, 0, 1, DAGInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDAGInstruction_SourceNodes(), this.getDAGNode(), null, "sourceNodes", null, 0, -1, DAGInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDAGInstruction_SinkNodes(), this.getDAGNode(), null, "sinkNodes", null, 0, -1, DAGInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagInstructionEClass, null, "connectNodes", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGNode(), "srcNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGNode(), "dstNode", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagInstructionEClass, null, "connectNodes", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGNode(), "srcNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGNode(), "dstNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getint(), "dstInportPosition", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagInstructionEClass, null, "connect", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGOutPort(), "srcPort", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGNode(), "dstNode", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagInstructionEClass, null, "replaceNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGNode(), "newNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGNode(), "oldNode", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagInstructionEClass, null, "insertNodeBetween", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGNode(), "newNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGNode(), "predNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGNode(), "succNode", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagInstructionEClass, null, "removeNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGNode(), "node", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagInstructionEClass, null, "removeNodeAndReconnect", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGNode(), "node", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagInstructionEClass, null, "disconnectNodes", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGNode(), "pred", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGNode(), "succ", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagInstructionEClass, null, "removeEdge", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDAGEdge(), "e", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagNodeEClass, DAGNode.class, "DAGNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDAGNode_Type(), theTypesPackage.getType(), null, "type", null, 0, 1, DAGNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDAGNode_Outputs(), this.getDAGOutPort(), null, "outputs", null, 0, -1, DAGNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDAGNode_Parent(), this.getDAGInstruction(), this.getDAGInstruction_Nodes(), "parent", null, 0, 1, DAGNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDAGNode_Inputs(), this.getDAGInPort(), null, "inputs", null, 0, -1, DAGNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDAGNode_Ident(), this.getint(), "ident", null, 0, 1, DAGNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagNodeEClass, this.getDAGOutPort(), "getDataOutputs", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagNodeEClass, this.getDAGOutPort(), "getControlOutputs", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagNodeEClass, this.getDAGInPort(), "getDataInputs", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagNodeEClass, this.getDAGInPort(), "getControlInputs", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagNodeEClass, this.getDAGEdge(), "getInputEdges", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagNodeEClass, this.getDAGDataEdge(), "getDataInputEdges", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagNodeEClass, this.getDAGControlEdge(), "getControlInputEdges", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagNodeEClass, this.getDAGEdge(), "getOutputEdges", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagNodeEClass, this.getDAGDataEdge(), "getDataOutputEdges", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagNodeEClass, this.getDAGControlEdge(), "getControlOutputEdges", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagNodeEClass, this.getDAGNode(), "getPredecessors", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagNodeEClass, this.getDAGNode(), "getDataPredecessors", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagNodeEClass, this.getDAGNode(), "getControlPredecessors", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagNodeEClass, this.getDAGNode(), "getSuccessors", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagNodeEClass, this.getDAGNode(), "getDataSuccessors", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagNodeEClass, this.getDAGNode(), "getControlSuccessors", 0, -1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagEdgeEClass, DAGEdge.class, "DAGEdge", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDAGEdge_Src(), this.getDAGOutPort(), this.getDAGOutPort_Sinks(), "src", null, 1, 1, DAGEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDAGEdge_Sink(), this.getDAGInPort(), this.getDAGInPort_Source(), "sink", null, 1, 1, DAGEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDAGEdge_Parent(), this.getDAGInstruction(), this.getDAGInstruction_Edges(), "parent", null, 0, 1, DAGEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagEdgeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagEdgeEClass, this.getDAGNode(), "getSinkNode", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagEdgeEClass, this.getDAGNode(), "getSourceNode", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagEdgeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagPortEClass, DAGPort.class, "DAGPort", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDAGPort_Ident(), this.getint(), "ident", null, 0, 1, DAGPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(dagPortEClass, this.getDAGNode(), "getParent", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagPortEClass, theEcorePackage.getEBoolean(), "isSetParent", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dagPortEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagInPortEClass, DAGInPort.class, "DAGInPort", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDAGInPort_Source(), this.getDAGEdge(), this.getDAGEdge_Sink(), "source", null, 0, 1, DAGInPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagInPortEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagInPortEClass, this.getDAGNode(), "getSourceNode", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagInPortEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagOutPortEClass, DAGOutPort.class, "DAGOutPort", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDAGOutPort_Sinks(), this.getDAGEdge(), this.getDAGEdge_Src(), "sinks", null, 0, -1, DAGOutPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagOutPortEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagOutPortEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagImmNodeEClass, DAGImmNode.class, "DAGImmNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(dagImmNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagSymbolNodeEClass, DAGSymbolNode.class, "DAGSymbolNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDAGSymbolNode_Name(), this.getString(), "name", null, 0, 1, DAGSymbolNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDAGSymbolNode_Symbol(), theCorePackage.getSymbol(), null, "symbol", null, 0, 1, DAGSymbolNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagSymbolNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagSymbolNodeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagCallNodeEClass, DAGCallNode.class, "DAGCallNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDAGCallNode_Name(), this.getString(), "name", null, 0, 1, DAGCallNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagCallNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagCallNodeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagArrayValueNodeEClass, DAGArrayValueNode.class, "DAGArrayValueNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(dagArrayValueNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagArrayValueNodeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagOpNodeEClass, DAGOpNode.class, "DAGOpNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDAGOpNode_Opcode(), this.getString(), "opcode", null, 0, 1, DAGOpNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagOpNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagOpNodeEClass, theEcorePackage.getEBoolean(), "isSet", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagOpNodeEClass, theEcorePackage.getEBoolean(), "isConvert", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagOutNodeEClass, DAGOutNode.class, "DAGOutNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDAGOutNode_Symbol(), theCorePackage.getSymbol(), null, "symbol", null, 0, 1, DAGOutNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagOutNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagOutNodeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagControlEdgeEClass, DAGControlEdge.class, "DAGControlEdge", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDAGControlEdge_Type(), this.getDependencyType(), "type", null, 0, 1, DAGControlEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagControlEdgeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagControlEdgeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagDataEdgeEClass, DAGDataEdge.class, "DAGDataEdge", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(dagDataEdgeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagDataEdgeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagNumberedSymbolNodeEClass, DAGNumberedSymbolNode.class, "DAGNumberedSymbolNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDAGNumberedSymbolNode_Number(), this.getint(), "number", null, 0, 1, DAGNumberedSymbolNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDAGNumberedSymbolNode_SymbolDefinition(), this.getDAGNumberedOutNode(), null, "symbolDefinition", null, 0, 1, DAGNumberedSymbolNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagNumberedSymbolNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagNumberedSymbolNodeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagNumberedOutNodeEClass, DAGNumberedOutNode.class, "DAGNumberedOutNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDAGNumberedOutNode_Number(), this.getint(), "number", null, 0, 1, DAGNumberedOutNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagNumberedOutNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagNumberedOutNodeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagIntImmNodeEClass, DAGIntImmNode.class, "DAGIntImmNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDAGIntImmNode_IntValue(), this.getlong(), "intValue", null, 0, 1, DAGIntImmNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDAGIntImmNode_Value(), theInstrsPackage.getIntInstruction(), null, "value", null, 0, 1, DAGIntImmNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagIntImmNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagIntImmNodeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagFloatImmNodeEClass, DAGFloatImmNode.class, "DAGFloatImmNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDAGFloatImmNode_FloatValue(), this.getdouble(), "floatValue", null, 0, 1, DAGFloatImmNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDAGFloatImmNode_Value(), theInstrsPackage.getFloatInstruction(), null, "value", null, 0, 1, DAGFloatImmNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagFloatImmNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagFloatImmNodeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagStringImmNodeEClass, DAGStringImmNode.class, "DAGStringImmNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDAGStringImmNode_StringValue(), this.getString(), "stringValue", null, 0, 1, DAGStringImmNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagStringImmNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagStringImmNodeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagArrayNodeEClass, DAGArrayNode.class, "DAGArrayNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(dagArrayNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagArrayNodeEClass, this.getDAGNode(), "getIndex", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagArrayNodeEClass, this.getDAGNode(), "getDest", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagArrayNodeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagSimpleArrayNodeEClass, DAGSimpleArrayNode.class, "DAGSimpleArrayNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(dagSimpleArrayNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagSimpleArrayNodeEClass, this.getDAGNode(), "getIndex", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagSimpleArrayNodeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagFieldInstructionEClass, DAGFieldInstruction.class, "DAGFieldInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDAGFieldInstruction_Field(), theTypesPackage.getField(), null, "field", null, 0, 1, DAGFieldInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagFieldInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagFieldInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagJumpNodeEClass, DAGJumpNode.class, "DAGJumpNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDAGJumpNode_Label(), this.getString(), "label", null, 0, 1, DAGJumpNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagJumpNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagPatternNodeEClass, DAGPatternNode.class, "DAGPatternNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDAGPatternNode_Pattern(), this.getDAGInstruction(), null, "pattern", null, 0, 1, DAGPatternNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDAGPatternNode_InputsMap(), this.getInPortMap(), null, "inputsMap", null, 0, -1, DAGPatternNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDAGPatternNode_OutputsMap(), this.getOutPortMap(), null, "outputsMap", null, 0, -1, DAGPatternNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagPatternNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(inPortMapEClass, Map.Entry.class, "InPortMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInPortMap_Key(), this.getDAGInPort(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInPortMap_Value(), this.getDAGInPort(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(outPortMapEClass, Map.Entry.class, "OutPortMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOutPortMap_Key(), this.getDAGOutPort(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOutPortMap_Value(), this.getDAGOutPort(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dagVectorNodeEClass, DAGVectorNode.class, "DAGVectorNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(dagVectorNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagVectorArrayAccessNodeEClass, DAGVectorArrayAccessNode.class, "DAGVectorArrayAccessNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(dagVectorArrayAccessNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagVectorArrayAccessNodeEClass, this.getDAGNode(), "getIndex", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagVectorArrayAccessNodeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagVectorOpNodeEClass, DAGVectorOpNode.class, "DAGVectorOpNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDAGVectorOpNode_Opcode(), this.getString(), "opcode", null, 0, 1, DAGVectorOpNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagVectorOpNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagVectorShuffleNodeEClass, DAGVectorShuffleNode.class, "DAGVectorShuffleNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDAGVectorShuffleNode_Permutation(), this.getint(), "permutation", "0", 0, -1, DAGVectorShuffleNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagVectorShuffleNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagVectorExtractNodeEClass, DAGVectorExtractNode.class, "DAGVectorExtractNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDAGVectorExtractNode_Pos(), this.getint(), "pos", "0", 0, 1, DAGVectorExtractNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagVectorExtractNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagVectorExtractNodeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagVectorExpandNodeEClass, DAGVectorExpandNode.class, "DAGVectorExpandNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDAGVectorExpandNode_Permutation(), this.getint(), "permutation", "0", 0, -1, DAGVectorExpandNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagVectorExpandNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagVectorPackNodeEClass, DAGVectorPackNode.class, "DAGVectorPackNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(dagVectorPackNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagVectorPackNodeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagssaUseNodeEClass, DAGSSAUseNode.class, "DAGSSAUseNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDAGSSAUseNode_Def(), this.getDAGSSADefNode(), null, "def", null, 0, 1, DAGSSAUseNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagssaUseNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagssaDefNodeEClass, DAGSSADefNode.class, "DAGSSADefNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDAGSSADefNode_SSAUses(), this.getDAGSSAUseNode(), null, "SSAUses", null, 0, -1, DAGSSADefNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(dagssaDefNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dagPhiNodeEClass, DAGPhiNode.class, "DAGPhiNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(dagPhiNodeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDagVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dagPhiNodeEClass, theInstrsPackage.getSSADefSymbol(), "getSSADefinitions", 0, -1, IS_UNIQUE, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(dependencyTypeEEnum, DependencyType.class, "DependencyType");
		addEEnumLiteral(dependencyTypeEEnum, DependencyType.RAW);
		addEEnumLiteral(dependencyTypeEEnum, DependencyType.WAW);
		addEEnumLiteral(dependencyTypeEEnum, DependencyType.WAR);

		initEEnum(dagOperatorEEnum, DAGOperator.class, "DAGOperator");
		addEEnumLiteral(dagOperatorEEnum, DAGOperator.CVT);
		addEEnumLiteral(dagOperatorEEnum, DAGOperator.RET);
		addEEnumLiteral(dagOperatorEEnum, DAGOperator.SET);
		addEEnumLiteral(dagOperatorEEnum, DAGOperator.INDIR);
		addEEnumLiteral(dagOperatorEEnum, DAGOperator.JUMP);

		// Initialize data types
		initEDataType(stringEDataType, String.class, "String", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(intEDataType, int.class, "int", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(longEDataType, long.class, "long", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(doubleEDataType, double.class, "double", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// gmf.link
		createGmfAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
		   });
	}

	/**
	 * Initializes the annotations for <b>gmf.link</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGmfAnnotations() {
		String source = "gmf.link";	
		addAnnotation
		  (dagEdgeEClass, 
		   source, 
		   new String[] {
			 "source", "src",
			 "target", "sink",
			 "style", "dot",
			 "width", "1"
		   });
	}

} //DagPackageImpl
