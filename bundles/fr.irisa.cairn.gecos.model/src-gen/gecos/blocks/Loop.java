/**
 */
package gecos.blocks;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Loop</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.blocks.BlocksPackage#getLoop()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface Loop extends Breakable, Continuable {
} // Loop
