package fr.irisa.cairn.gecos.model.tools.validation;

import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.Diagnostician;

public class GecosModelValidator {

	public static String PREFIX = "[GecosValidator] ";
	
	private static boolean VERBOSE=false;
	private static void vPrintln(Object o) { if (VERBOSE) System.out.println(PREFIX+(o.toString().replace("\n", "\n"+PREFIX))); }

	private static boolean debug = false;
	private static void debug(Object mess) { if (debug) System.out.println(PREFIX+mess.toString()); }
	
	private EList<EObject> allcontent;
	private Map<EObject,EObject> danglingObjectsRoot;
	private Map<EObject,EObject> danglingObjects;

	private int nbDanglingRef;
	private boolean throwException;

	public GecosModelValidator() {
		this(true);
	}
	public GecosModelValidator(boolean throwException) {
		this.throwException = throwException;
		allcontent = new BasicEList<EObject>();
		danglingObjectsRoot = new LinkedHashMap<EObject,EObject>();
		danglingObjects = new LinkedHashMap<EObject,EObject>();
		nbDanglingRef=0;
	}
	
	
	public void fill(EObject obj) {
		allcontent.add(obj);
		allcontent.addAll(obj.eContents());	
		for (EObject aobj : obj.eContents()) {
			fill(aobj);	
		}
	}
	
	public void checkRef(EObject obj) {
		for (EObject aobj : obj.eCrossReferences()) {
			if (!allcontent.contains(aobj.eContainer())) {
				nbDanglingRef++;
				findRootOfDanglingReference(obj,aobj);
			} 
		}
		for (EObject aobj : obj.eContents()) {
			checkRef(aobj);
		}
	}
	
	private void findRootOfDanglingReference(EObject obj, EObject ref) {
		EObject current = ref;
		while (current.eContainer()!=null) {
			current = current.eContainer();
		}
		danglingObjectsRoot.put(obj,current);
		danglingObjects.put(ref,obj);
	}

	public static void validateObject(EObject eObject) {
		Diagnostic diagnostic = Diagnostician.INSTANCE.validate(eObject);
	    if (!(diagnostic.getSeverity() == Diagnostic.OK)) {
	    	System.err.println(diagnostic);
	    } else {
	    	vPrintln(diagnostic.getMessage());
	    }
	}

	public boolean check(EObject object) {
		// default EMF Validator
		validateObject(object);
		
		vPrintln("1) Filling list");
		fill(object);
		vPrintln("	- model contains "+allcontent.size()+" Objects");
		vPrintln("2) Checking refs");
		checkRef(object);
		vPrintln("	- model has "+nbDanglingRef+" dangling references");
		boolean res = true;
		RuntimeException e = null;
		for (EObject ref : danglingObjects.keySet()) {
			res = false;
			EObject obj= danglingObjects.get(ref); 
			String mess = "- Object "+ref+" of Class "+ref.getClass().getSimpleName()+" referenced by "+obj+":"+obj.getClass().getSimpleName()+" is not contained in the model";
			debug(mess);	
			e = new RuntimeException(mess);
				
		}
		for (EObject obj : danglingObjectsRoot.keySet()) {
			debug("- Object "+obj+" of Class "+obj.getClass().getSimpleName()+" linked to external tree root "+danglingObjectsRoot.get(obj) );
		}
		if (!res && throwException)
			throw e;
		return res;
	}

}
