/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.tools.io;

import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

public class ReadProfileData {

	private String filename;

	public ReadProfileData(String filename) {
		this.filename = filename;
	}

	public IProfileData compute() {
		return readProfileData(filename);
	}

	private ProfileData readProfileData(String filename) {
		ProfileData result = new ProfileData();
		try {
			LineNumberReader reader = new LineNumberReader(
					new FileReader(filename));
			while (reader.ready()) {
				int value = Integer.parseInt(reader.readLine());
				if (value != 0)
					result.add(reader.getLineNumber() - 1, value);
			}
			reader.close();
		} catch (IOException e) {
			throw new RuntimeException("error while reading profile : "+e.getMessage());
		}
		return result;
	}
}
