#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

using namespace std;


#define DEBUG

static const char *inputFile;
static const char *outputFile;
static int nint;
static int fixToFloat;


/**
 * Input file format must be:
 *    - First line contains the number of dimensions (ND)
 *    - Next ND lines contain each the size of the corresponding dimension
 *    - Followed by 1 line per data value
 */


template<class FLOAT_TYPE, class FIX_TYPE>
static FLOAT_TYPE fix2float(FIX_TYPE inVal) {
	int wordlength = 8*sizeof(FIX_TYPE);
	int nfrac = wordlength - nint;
	FLOAT_TYPE one = pow(2, nfrac);

//	cout << "[Fix2Float] fixed-point type: << wordlength << "." << nint << "." << nfrac << endl;

	return ((FLOAT_TYPE)inVal) / one;
}

template<class FLOAT_TYPE, class FIX_TYPE>
static FIX_TYPE float2fix(FLOAT_TYPE inVal) {
	int wordlength = 8*sizeof(FIX_TYPE);
	int nfrac = wordlength - nint;
	FLOAT_TYPE one = pow(2, nfrac);

	FIX_TYPE outVal = (FIX_TYPE)(inVal * one);
//	printf("%d\n", outVal);

	return outVal; //TODO truncation
}

template<class FLOAT_TYPE, class FIX_TYPE>
static void fixSwitch(const char *SCAND_DESC) {
	ifstream infile(inputFile);
	if(infile.fail()) {
		cerr << "Failed to open input file ! " << inputFile << endl;
		exit(-2);
	}

	FILE *outfile = fopen(outputFile, "w");
	if(outfile == NULL) {
		cerr << "Failed to open output file ! " << outputFile << endl;
		exit(-2);
	}

	/* read header */
	//number of dimensions
	int nDims;
	infile >> nDims;
	fprintf(outfile, "%d\n", nDims);

#ifdef DEBUG
	cout << "nDims      : " << nDims << endl;
#endif

	//dimensions size
	int dimSizes[nDims];
	for(int i = 0 ; i < nDims; ++i) {
		infile >> dimSizes[i];
		fprintf(outfile, "%d\n", dimSizes[i]);
#ifdef DEBUG
		cout << "dim" << i << " size  : " << dimSizes[i] << endl;
#endif
	}

	/* Convert data */
	if(fixToFloat) { /* Fix to Float */
		cout << "[Fix2Float] fixed-point type: " << 8*sizeof(FIX_TYPE) << "." << nint << endl;
		FIX_TYPE inVal;
		FLOAT_TYPE outVal;
		while(infile >> inVal) {
			outVal = fix2float<FLOAT_TYPE, FIX_TYPE>(inVal);
			fprintf(outfile, "%f", outVal); fprintf(outfile, "\n");
		}
	}
	else { /* Float to Fix */
		cout << "[Float2Fix] fixed-point type: " << 8*sizeof(FIX_TYPE) << "." << nint << endl;
		FLOAT_TYPE inVal;
		FIX_TYPE outVal;
		while(infile >> inVal) {
			outVal = float2fix<FLOAT_TYPE, FIX_TYPE>(inVal);
			fprintf(outfile, SCAND_DESC, outVal); fprintf(outfile, "\n");
		}
	}

	infile.close();
	fclose(outfile);
}

template<class FLOAT_TYPE>
static void floatSwitch(string fixType)
{
	if     (fixType.compare("i8"  ) == 0)   fixSwitch<FLOAT_TYPE, int8_t  >("%d");
	else if(fixType.compare("ui8" ) == 0)   fixSwitch<FLOAT_TYPE, uint8_t >("%u");
	else if(fixType.compare("i16" ) == 0)   fixSwitch<FLOAT_TYPE, int16_t >("%d");
	else if(fixType.compare("ui16") == 0) 	fixSwitch<FLOAT_TYPE, uint16_t>("%u");
	else if(fixType.compare("i32" ) == 0)   fixSwitch<FLOAT_TYPE, int32_t >("%d");
	else if(fixType.compare("ui32") == 0)   fixSwitch<FLOAT_TYPE, uint32_t>("%u");

	else if(fixType.compare("int"   ) == 0) fixSwitch<FLOAT_TYPE, signed int    >("%d");
	else if(fixType.compare("short" ) == 0) fixSwitch<FLOAT_TYPE, signed short  >("%d");
	else if(fixType.compare("char"  ) == 0) fixSwitch<FLOAT_TYPE, signed char   >("%d");
	else if(fixType.compare("uint"  ) == 0) fixSwitch<FLOAT_TYPE, unsigned int  >("%u");
	else if(fixType.compare("ushort") == 0) fixSwitch<FLOAT_TYPE, unsigned short>("%u");
	else if(fixType.compare("uchar" ) == 0) fixSwitch<FLOAT_TYPE, unsigned char >("%u");
	else {
		cerr << "Unsupported fix_type argument ! " << fixType << endl;
		exit(-1);
	}
}

int main(int argc, const char **argv)
{
	if(argc < 7) {
		cerr << "Usage: " << argv[0] << " fix2float?(1/0) input_file output_file float_type(float/double) fix_type(<u>int/short/char/i32/i16/i8) nint" << endl;
		cerr << "\t float_type: float | double" << endl;
		cerr << "\t fix_type  : <u>int | short | char | i32 | i16 | i8" << endl;
		return -1;
	}

	fixToFloat  = atoi(argv[1]);
	inputFile	= argv[2];
	outputFile 	= argv[3];
	string floatType = string(argv[4]);
	string fixType   = string(argv[5]);
	nint 		= atoi(argv[6]);

#ifdef DEBUG
	cout << (fixToFloat? "Fix to Float:" : "Float to Fix:") << endl;
	cout << "Input File : " << inputFile << endl;
	cout << "Output File: " << outputFile << endl;
	cout << "Float Type : " << floatType << endl;
	cout << "Fix Type   : " << fixType << "," << nint << endl;
#endif
	if(floatType.compare("float") == 0)
		floatSwitch<float>(fixType);
	else if(floatType.compare("double") == 0)
		floatSwitch<double>(fixType);
	else {
		cerr << "Unsupported float_type argument ! " << floatType << endl;
		exit(-1);
	}

	return 0;
}
