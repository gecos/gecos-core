package fr.irisa.cairn.gecos.model.dag.adapter.impl;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGEdgeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGPortAdapter;
import gecos.dag.DAGEdge;

/**
 * implementation of a node adapter.
 * This graph adapt a DAGEdge
 * @author <your name>
 *
 */
public class DAGEdgeAdapterImpl extends fr.irisa.cairn.graph.implement.Edge
		implements
			IDAGEdgeAdapter {

	private final DAGEdge adapted;

	public DAGEdgeAdapterImpl(DAGEdge adapted, IDAGPortAdapter source,
			IDAGPortAdapter sink) {
		super(source, sink);
		this.adapted = adapted;
	}

	public DAGEdge getAdaptedEdge() {
		return this.adapted;
	}

	/**
	 * @model
	 */
	@Override
	public IDAGPortAdapter getSourcePort() {
		return (IDAGPortAdapter) super.getSourcePort();
	}

	/**
	 * @model
	 */
	@Override
	public IDAGPortAdapter getSinkPort() {
		return (IDAGPortAdapter) super.getSinkPort();
	}

}
