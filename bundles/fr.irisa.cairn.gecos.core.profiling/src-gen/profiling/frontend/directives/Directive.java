/**
 */
package profiling.frontend.directives;

import gecos.blocks.Block;

import gecos.instrs.CallInstruction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Directive</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link profiling.frontend.directives.Directive#getCall <em>Call</em>}</li>
 * </ul>
 *
 * @see profiling.frontend.directives.DirectivesPackage#getDirective()
 * @model abstract="true"
 * @generated
 */
public interface Directive extends EObject {
	/**
	 * Returns the value of the '<em><b>Call</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Call</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Call</em>' reference.
	 * @see #setCall(CallInstruction)
	 * @see profiling.frontend.directives.DirectivesPackage#getDirective_Call()
	 * @model
	 * @generated
	 */
	CallInstruction getCall();

	/**
	 * Sets the value of the '{@link profiling.frontend.directives.Directive#getCall <em>Call</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Call</em>' reference.
	 * @see #getCall()
	 * @generated
	 */
	void setCall(CallInstruction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void parse();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 * @generated
	 */
	Block createCodeBlock();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.annotations.FileLocationAnnotation%&gt; loc = this.getCall().getFileLocation();\n&lt;%gecos.instrs.CallInstruction%&gt; _call = this.getCall();\n&lt;%java.lang.String%&gt; _xifexpression = null;\nif ((loc != null))\n{\n\t&lt;%java.lang.String%&gt; _filename = loc.getFilename();\n\t&lt;%java.lang.String%&gt; _plus = (\" at \" + _filename);\n\t&lt;%java.lang.String%&gt; _plus_1 = (_plus + \":\");\n\tint _startingLine = loc.getStartingLine();\n\t_xifexpression = (_plus_1 + &lt;%java.lang.Integer%&gt;.valueOf(_startingLine));\n}\nreturn (_call + _xifexpression);'"
	 * @generated
	 */
	String printLocation();

} // Directive
