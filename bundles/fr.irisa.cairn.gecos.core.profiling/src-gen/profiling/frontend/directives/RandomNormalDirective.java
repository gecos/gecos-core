/**
 */
package profiling.frontend.directives;

import gecos.blocks.Block;

import gecos.instrs.Instruction;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Random Normal Directive</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * $random_uniform(mean, stddev [, seed])
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link profiling.frontend.directives.RandomNormalDirective#getNAME <em>NAME</em>}</li>
 *   <li>{@link profiling.frontend.directives.RandomNormalDirective#getMean <em>Mean</em>}</li>
 *   <li>{@link profiling.frontend.directives.RandomNormalDirective#getStddev <em>Stddev</em>}</li>
 *   <li>{@link profiling.frontend.directives.RandomNormalDirective#getSeed <em>Seed</em>}</li>
 * </ul>
 *
 * @see profiling.frontend.directives.DirectivesPackage#getRandomNormalDirective()
 * @model
 * @generated
 */
public interface RandomNormalDirective extends InjectorSourceDirective {
	/**
	 * Returns the value of the '<em><b>NAME</b></em>' attribute.
	 * The default value is <code>"$random_normal"</code>.
	 * The literals are from the enumeration {@link profiling.frontend.directives.DirectiveName}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>NAME</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>NAME</em>' attribute.
	 * @see profiling.frontend.directives.DirectiveName
	 * @see #setNAME(DirectiveName)
	 * @see profiling.frontend.directives.DirectivesPackage#getRandomNormalDirective_NAME()
	 * @model default="$random_normal" unique="false"
	 * @generated
	 */
	DirectiveName getNAME();

	/**
	 * Sets the value of the '{@link profiling.frontend.directives.RandomNormalDirective#getNAME <em>NAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>NAME</em>' attribute.
	 * @see profiling.frontend.directives.DirectiveName
	 * @see #getNAME()
	 * @generated
	 */
	void setNAME(DirectiveName value);

	/**
	 * Returns the value of the '<em><b>Mean</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mean</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mean</em>' reference.
	 * @see #setMean(Instruction)
	 * @see profiling.frontend.directives.DirectivesPackage#getRandomNormalDirective_Mean()
	 * @model
	 * @generated
	 */
	Instruction getMean();

	/**
	 * Sets the value of the '{@link profiling.frontend.directives.RandomNormalDirective#getMean <em>Mean</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mean</em>' reference.
	 * @see #getMean()
	 * @generated
	 */
	void setMean(Instruction value);

	/**
	 * Returns the value of the '<em><b>Stddev</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stddev</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stddev</em>' reference.
	 * @see #setStddev(Instruction)
	 * @see profiling.frontend.directives.DirectivesPackage#getRandomNormalDirective_Stddev()
	 * @model
	 * @generated
	 */
	Instruction getStddev();

	/**
	 * Sets the value of the '{@link profiling.frontend.directives.RandomNormalDirective#getStddev <em>Stddev</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Stddev</em>' reference.
	 * @see #getStddev()
	 * @generated
	 */
	void setStddev(Instruction value);

	/**
	 * Returns the value of the '<em><b>Seed</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Seed</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Seed</em>' reference.
	 * @see #setSeed(Instruction)
	 * @see profiling.frontend.directives.DirectivesPackage#getRandomNormalDirective_Seed()
	 * @model
	 * @generated
	 */
	Instruction getSeed();

	/**
	 * Sets the value of the '{@link profiling.frontend.directives.RandomNormalDirective#getSeed <em>Seed</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Seed</em>' reference.
	 * @see #getSeed()
	 * @generated
	 */
	void setSeed(Instruction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; args = this.getCall().getArgs();\nint _size = args.size();\nboolean _lessThan = (_size &lt; 2);\nif (_lessThan)\n{\n\t&lt;%java.lang.String%&gt; _printLocation = this.printLocation();\n\t&lt;%java.lang.String%&gt; _plus = (_printLocation + \"\\nMust have at least 2 arguments (mean, stddev)!\");\n\tthrow new &lt;%fr.irisa.cairn.gecos.core.profiling.frontend.InvalidDirective%&gt;(_plus);\n}\nthis.setMean(&lt;%java.util.Objects%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;requireNonNull(args.get(0)));\nthis.setStddev(&lt;%java.util.Objects%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;requireNonNull(args.get(1)));\nint _size_1 = args.size();\nboolean _greaterThan = (_size_1 &gt; 2);\nif (_greaterThan)\n{\n\tthis.setSeed(&lt;%java.util.Objects%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;requireNonNull(args.get(2)));\n}'"
	 * @generated
	 */
	void parse();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.core.ProcedureSet%&gt; ps = this.getCall().getContainingProcedureSet();\nfinal &lt;%gecos.blocks.BasicBlock%&gt; bb = &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory%&gt;.BBlock(\n\t&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory%&gt;.call(\n\t\t&lt;%fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport%&gt;.getNativeProc(ps, &lt;%fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport.NativeFunction%&gt;.INJECTOR_RANDOM_UNIFORN_DOUBLE), this.getMean(), this.getStddev()));\n&lt;%gecos.instrs.Instruction%&gt; _seed = this.getSeed();\nboolean _tripleNotEquals = (_seed != null);\nif (_tripleNotEquals)\n{\n\tbb.getInstructions().add(0, &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory%&gt;.call(\n\t\t&lt;%fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport%&gt;.getNativeProc(ps, &lt;%fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport.NativeFunction%&gt;.INJECTOR_RANDOM_SET_SEED), this.getSeed()));\n}\nreturn bb;'"
	 * @generated
	 */
	Block createCodeBlock();

} // RandomNormalDirective
