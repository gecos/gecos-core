/**
 */
package gecos.instrs.impl;

import gecos.core.Symbol;

import gecos.instrs.InstrsPackage;
import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;
import gecos.instrs.SSADefSymbol;
import gecos.instrs.SSAUseSymbol;

import gecos.types.Type;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SSA Use Symbol</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.impl.SSAUseSymbolImpl#getDef <em>Def</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SSAUseSymbolImpl extends NumberedSymbolInstructionImpl implements SSAUseSymbol {
	/**
	 * The cached value of the '{@link #getDef() <em>Def</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDef()
	 * @generated
	 * @ordered
	 */
	protected SSADefSymbol def;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SSAUseSymbolImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstrsPackage.eINSTANCE.getSSAUseSymbol();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SSADefSymbol getDef() {
		if (def != null && def.eIsProxy()) {
			InternalEObject oldDef = (InternalEObject)def;
			def = (SSADefSymbol)eResolveProxy(oldDef);
			if (def != oldDef) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InstrsPackage.SSA_USE_SYMBOL__DEF, oldDef, def));
			}
		}
		return def;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SSADefSymbol basicGetDef() {
		return def;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDef(SSADefSymbol newDef, NotificationChain msgs) {
		SSADefSymbol oldDef = def;
		def = newDef;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, InstrsPackage.SSA_USE_SYMBOL__DEF, oldDef, newDef);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDef(SSADefSymbol newDef) {
		if (newDef != def) {
			NotificationChain msgs = null;
			if (def != null)
				msgs = ((InternalEObject)def).eInverseRemove(this, InstrsPackage.SSA_DEF_SYMBOL__SSA_USES, SSADefSymbol.class, msgs);
			if (newDef != null)
				msgs = ((InternalEObject)newDef).eInverseAdd(this, InstrsPackage.SSA_DEF_SYMBOL__SSA_USES, SSADefSymbol.class, msgs);
			msgs = basicSetDef(newDef, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InstrsPackage.SSA_USE_SYMBOL__DEF, newDef, newDef));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final InstrsVisitor visitor) {
		visitor.visitSSAUseSymbol(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _elvis = null;
		Symbol _symbol = this.getSymbol();
		String _name = null;
		if (_symbol!=null) {
			_name=_symbol.getName();
		}
		if (_name != null) {
			_elvis = _name;
		} else {
			_elvis = "null";
		}
		String _plus = ("use(" + _elvis);
		String _plus_1 = (_plus + "_");
		int _number = this.getNumber();
		String _plus_2 = (_plus_1 + Integer.valueOf(_number));
		String _plus_3 = (_plus_2 + ":");
		Object _elvis_1 = null;
		Symbol _symbol_1 = this.getSymbol();
		Type _type = null;
		if (_symbol_1!=null) {
			_type=_symbol_1.getType();
		}
		if (_type != null) {
			_elvis_1 = _type;
		} else {
			_elvis_1 = "";
		}
		String _plus_4 = (_plus_3 + _elvis_1);
		return (_plus_4 + ")");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSame(final Instruction instr) {
		if ((instr instanceof SSAUseSymbol)) {
			boolean isSame = false;
			if (((this.getDef() == null) && (((SSAUseSymbol)instr).getDef() == null))) {
				isSame = true;
			}
			else {
				SSADefSymbol _def = this.getDef();
				boolean _tripleEquals = (_def == null);
				SSADefSymbol _def_1 = ((SSAUseSymbol)instr).getDef();
				boolean _tripleEquals_1 = (_def_1 == null);
				boolean _xor = (_tripleEquals ^ _tripleEquals_1);
				if (_xor) {
					return false;
				}
				else {
					isSame = this.getDef().isSame(((SSAUseSymbol)instr).getDef());
				}
			}
			return (super.isSame(instr) && isSame);
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InstrsPackage.SSA_USE_SYMBOL__DEF:
				if (def != null)
					msgs = ((InternalEObject)def).eInverseRemove(this, InstrsPackage.SSA_DEF_SYMBOL__SSA_USES, SSADefSymbol.class, msgs);
				return basicSetDef((SSADefSymbol)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InstrsPackage.SSA_USE_SYMBOL__DEF:
				return basicSetDef(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InstrsPackage.SSA_USE_SYMBOL__DEF:
				if (resolve) return getDef();
				return basicGetDef();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InstrsPackage.SSA_USE_SYMBOL__DEF:
				setDef((SSADefSymbol)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InstrsPackage.SSA_USE_SYMBOL__DEF:
				setDef((SSADefSymbol)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InstrsPackage.SSA_USE_SYMBOL__DEF:
				return def != null;
		}
		return super.eIsSet(featureID);
	}

} //SSAUseSymbolImpl
