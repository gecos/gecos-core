package fr.irisa.cairn.gecos.model.analysis.observers.control;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EContentAdapter;

public class ControlFlowObserver extends EContentAdapter  {

	public void notifyChanged(Notification notification) {
		super.notifyChanged(notification);
		if (notification!=null) {
			Object feature = notification.getFeature();
			if (feature!=null) {
				if (feature instanceof EStructuralFeature) {
					EStructuralFeature sfeature = (EStructuralFeature) feature;
					System.out.println("\t-> Notification received from "+sfeature.getName()+":"+sfeature.getEType().getName());
				} else {
					System.out.println("\t-> Notification received from "+feature+":"+feature.getClass().getSimpleName());
				}
			}
		}
	}

}
