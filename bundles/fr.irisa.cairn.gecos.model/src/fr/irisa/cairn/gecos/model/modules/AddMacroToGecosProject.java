package fr.irisa.cairn.gecos.model.modules;

import gecos.gecosproject.GecosProject;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;

@GSModule("Add Macro(s) definition(s) to a GecosProject.\n"
		+ "\nSee: 'CreateGecosProject' module.")
public class AddMacroToGecosProject {
	
	private GecosProject project;
	private Map<String,String> map;
	
	@GSModuleConstructor(
		"-arg1: GecosProject to which the Macro will be added.\n"
	  + "-arg2: a map of Macro <ID, definition> pairs.\n"
	  + "   e.g. AddMacroToGecosProject(proj, {\"DEBUG\" -> \"1\" , \"TRACE\" -> \"0\"})")
	public AddMacroToGecosProject(GecosProject project ,  Map<String,String> m) throws IOException {
		this.project=project;
		this.map = m;
	}
	
	@GSModuleConstructor(
		"-arg1: GecosProject to which the Macro will be added.\n"
	  + "-arg2: the Marcos ID."
	  + "-arg3: the Marcos definition.")
	public AddMacroToGecosProject(GecosProject project,  String id, String val) throws IOException {
		this.project=project;
		this.map = new LinkedHashMap<String, String>(1);
		this.map.put(id, val);
	}
	

	public void compute() {
		this.project.getMacros().putAll(map);
	}
}
