package fr.irisa.cairn.gecos.model.transforms.ssa;

import gecos.instrs.PhiInstruction;
import gecos.instrs.SSAUseSymbol;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;

class RenumberPhiSwitch extends RenumberSwitch {

	private int pos;

	public RenumberPhiSwitch(SSAAnalyser ssaAnalyser, int pos) {
		super(ssaAnalyser);
		this.pos = pos;
	}

	@Override
	public Object caseSetInstruction(SetInstruction set) {
		doSwitch(set.getSource());
		return this;
	}

	@Override
	public Object casePhiInstruction(PhiInstruction g) {
		SymbolInstruction sym = (SymbolInstruction) g.getChildren().get(pos);
		SSAUseSymbol use = makeSSAUse(sym);
//			int topVisibility = this.ssaAnalyser.topVisibility(sym.getSymbol());
//			SSAUseSymbol use = InstructionFactory.ssaUse(sym, topVisibility);
//			SSADefSymbol value = this.ssaAnalyser.findSSADefinition(use.getSymbol(), topVisibility);
//			if(value!=null) {
//				debug("Found "+value.getRoot()+" at <"+topVisibility+","+use.getSymbol()+"> for "+use+"\n");
//				use.setDef(value);
//			} else {
//				debug("Could not find definition for <"+topVisibility+","+use.getSymbol()+"> \n");
//			}

		debug("- Replacing "+sym+" by "+use+ " in "+g);
		g.replaceChild(sym, use);
		return this;
	}
}