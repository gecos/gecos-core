package fr.irisa.cairn.gecos.model.transforms.others;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.transforms.tools.GenericInstructionTransformation;
import gecos.blocks.Block;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;

@SuppressWarnings({"all"})
public class BooleanExpressionDistribution extends GenericInstructionTransformation {
	%include { sl.tom }

	%include { gecos_common.tom }
	%include { gecos_terminals.tom }
	%include { gecos_basic.tom }
	%include { gecos_logical.tom }
	%include { gecos_compare.tom }

	protected Instruction apply(Instruction instruction) throws VisitFailure{
		Instruction res = `InnermostId(BoolDistributivity()).visitLight(instruction, tom.mapping.GenericIntrospector.INSTANCE);
		return res;
	}
 
	%strategy BoolDistributivity() extends Identity() {
		visit Inst {
				and(InstL(x,or(InstL(y, z)))) -> { return `or(InstL(and(InstL(x, y)), and(InstL(x, z)))); }
				and(InstL(or(InstL(y, z)),x)) -> { return `or(InstL(and(InstL(y, x)), and(InstL(z, x)))); }  
		} 
	}
}