package fr.irisa.cairn.gecos.model.transforms.dag;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGNode;

public class SimplifyCvtNodes {

	private DAGInstruction dag;

	public SimplifyCvtNodes(DAGInstruction dag) {
		this.dag = dag;
	}

	public void compute() {
		processDag();
	}

	protected void processDag() {
		DAGUtils.topologicalSort(dag).stream()
			.filter(DAGUtils::isCVT)
			.filter(this::isUselessCvt)
			.forEach(dag::removeNodeAndReconnect);
	}
	
	private boolean isUselessCvt(DAGNode cvt) {
		_assert(cvt != null && DAGUtils.isCVT(cvt));
		EList<DAGNode> preds = cvt.getDataPredecessors();
		if(preds != null && preds.size() == 1) {
			TypeAnalyzer cvtType = new TypeAnalyzer(cvt.getType());
			TypeAnalyzer predType = new TypeAnalyzer(preds.get(0).getType());
			if(cvtType.isSimilar(predType))
				return true;
		}
		return false;
	}
	
	private static final void _assert(boolean b) {
		if(!b) throw new RuntimeException("assertion failed");
	}
	
}
