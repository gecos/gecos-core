package fr.irisa.cairn.gecos.model.transforms.procedures.separation;

import com.google.common.base.Objects;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import fr.irisa.r2d2.gecos.framework.GSModule;
import gecos.core.ISymbolUse;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;
import gecos.types.AliasType;
import gecos.types.ArrayType;
import gecos.types.FunctionType;
import gecos.types.PtrType;
import gecos.types.RecordType;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@GSModule("Move procedures to a new file based on the procedure name.")
@SuppressWarnings("all")
public class ProcedureSeparation {
  private GecosProject proj;
  
  private List<Procedure> procs;
  
  public ProcedureSeparation(final GecosProject proj) {
    this.proj = proj;
    this.procs = proj.listProcedures();
  }
  
  public ProcedureSeparation(final GecosProject proj, final List<Procedure> procs) {
    this.proj = proj;
    this.procs = procs;
  }
  
  public ProcedureSeparation(final GecosProject proj, final Procedure proc) {
    this(proj, CollectionLiterals.<Procedure>newArrayList(proc));
  }
  
  public void compute() {
    for (final Procedure proc : this.procs) {
      {
        EObject _eContainer = proc.getContainingProcedureSet().eContainer();
        final GecosSourceFile file = ((GecosSourceFile) _eContainer);
        String _string = file.toString();
        String _name = proc.getSymbol().getName();
        String _plus = (_name + ".c");
        boolean _notEquals = (!Objects.equal(_string, _plus));
        if (_notEquals) {
          this.moveProcToNewFile(proc);
        }
      }
    }
  }
  
  private void moveProcToNewFile(final Procedure proc) {
    final ProcedureSymbol procSym = proc.getSymbol();
    final ProcedureSet ps = proc.getContainingProcedureSet();
    String _name = procSym.getName();
    String _plus = (_name + ".c");
    final GecosSourceFile newFile = GecosUserCoreFactory.source(_plus, GecosUserCoreFactory.procedureSet());
    this.proj.getSources().add(newFile);
    final ProcedureSet newPS = newFile.getModel();
    final Function1<ISymbolUse, Boolean> _function = (ISymbolUse it) -> {
      return Boolean.valueOf((it != null));
    };
    final Function1<ISymbolUse, Symbol> _function_1 = (ISymbolUse it) -> {
      return it.getUsedSymbol();
    };
    final Function1<Symbol, Boolean> _function_2 = (Symbol it) -> {
      return Boolean.valueOf(this.isDeclaredOutside(it, proc));
    };
    final Set<Symbol> extSymRefs = IterableExtensions.<Symbol>toSet(IterableExtensions.<Symbol>filter(IterableExtensions.<Symbol>toSet(IterableExtensions.<ISymbolUse, Symbol>map(IterableExtensions.<ISymbolUse>filter(EMFUtils.<ISymbolUse>eAllContentsInstancesOf(proc.getBody(), ISymbolUse.class), _function), _function_1)), _function_2));
    Type _type = procSym.getType();
    _type.setInnermostStorageClass(StorageClassSpecifiers.NONE);
    Type _type_1 = procSym.getType();
    final ProcedureSymbol procSymRef = GecosUserCoreFactory.procSymbol(proc.getSymbolName(), ((FunctionType) _type_1));
    EMFUtils.substituteByNewObjectInContainer(proc.getSymbol(), procSymRef);
    final Function1<ISymbolUse, Boolean> _function_3 = (ISymbolUse it) -> {
      Symbol _usedSymbol = it.getUsedSymbol();
      return Boolean.valueOf(Objects.equal(_usedSymbol, procSym));
    };
    final Consumer<ISymbolUse> _function_4 = (ISymbolUse it) -> {
      it.setSymbol(procSymRef);
    };
    IterableExtensions.<ISymbolUse>filter(EMFUtils.<ISymbolUse>eAllContentsInstancesOf(ps, ISymbolUse.class), _function_3).forEach(_function_4);
    newPS.addProcedure(proc);
    for (final Symbol extSym : extSymRefs) {
      {
        Type _type_2 = extSym.getType();
        _type_2.setInnermostStorageClass(StorageClassSpecifiers.NONE);
        final Type copyType = this.copyTypeInScope(extSym.getType(), newPS.getScope());
        final Symbol copySym = GecosUserCoreFactory.symbol(extSym.getName(), copyType);
        newPS.addSymbol(copySym);
        Type _type_3 = copySym.getType();
        _type_3.setInnermostStorageClass(StorageClassSpecifiers.EXTERN);
      }
    }
    final Function1<Type, Boolean> _function_5 = (Type it) -> {
      return Boolean.valueOf(((it instanceof AliasType) || (it instanceof RecordType)));
    };
    final Consumer<Type> _function_6 = (Type it) -> {
      newPS.getScope().getTypes().add(it.<Type>copy());
    };
    IterableExtensions.<Type>toSet(IterableExtensions.<Type>filter(ps.getScope().getTypes(), _function_5)).forEach(_function_6);
  }
  
  private boolean isDeclaredOutside(final Symbol sym, final Procedure proc) {
    if ((sym == null)) {
      return false;
    }
    boolean _contains = proc.listParameters().contains(sym);
    if (_contains) {
      return false;
    }
    return proc.getScope().getParent().hasInScope(sym);
  }
  
  private Type _copyTypeInScope(final Type type, final Scope scope) {
    Type _xblockexpression = null;
    {
      final Type copy = type.<Type>copy();
      scope.getTypes().add(copy);
      _xblockexpression = copy;
    }
    return _xblockexpression;
  }
  
  private Type _copyTypeInScope(final ArrayType type, final Scope scope) {
    ArrayType _xblockexpression = null;
    {
      Type _copy = type.<Type>copy();
      final ArrayType copy = ((ArrayType) _copy);
      scope.getTypes().add(copy);
      Type _base = type.getBase();
      boolean _tripleNotEquals = (_base != null);
      if (_tripleNotEquals) {
        copy.setBase(this.copyTypeInScope(type.getBase(), scope));
      }
      _xblockexpression = copy;
    }
    return _xblockexpression;
  }
  
  private Type _copyTypeInScope(final PtrType type, final Scope scope) {
    ArrayType _xblockexpression = null;
    {
      Type _copy = type.<Type>copy();
      final ArrayType copy = ((ArrayType) _copy);
      scope.getTypes().add(copy);
      Type _base = type.getBase();
      boolean _tripleNotEquals = (_base != null);
      if (_tripleNotEquals) {
        copy.setBase(this.copyTypeInScope(type.getBase(), scope));
      }
      _xblockexpression = copy;
    }
    return _xblockexpression;
  }
  
  private Type _copyTypeInScope(final FunctionType type, final Scope scope) {
    FunctionType _xblockexpression = null;
    {
      Type _copy = type.<Type>copy();
      final FunctionType copy = ((FunctionType) _copy);
      scope.getTypes().add(copy);
      Type _returnType = type.getReturnType();
      boolean _tripleNotEquals = (_returnType != null);
      if (_tripleNotEquals) {
        copy.setReturnType(this.copyTypeInScope(type.getReturnType(), scope));
      }
      _xblockexpression = copy;
    }
    return _xblockexpression;
  }
  
  private Type copyTypeInScope(final Type type, final Scope scope) {
    if (type instanceof ArrayType) {
      return _copyTypeInScope((ArrayType)type, scope);
    } else if (type instanceof FunctionType) {
      return _copyTypeInScope((FunctionType)type, scope);
    } else if (type instanceof PtrType) {
      return _copyTypeInScope((PtrType)type, scope);
    } else if (type != null) {
      return _copyTypeInScope(type, scope);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(type, scope).toString());
    }
  }
}
