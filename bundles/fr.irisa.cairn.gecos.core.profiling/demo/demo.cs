debug(2);

sources = {
,"src-c/withMain.c"
,"src-c/withoutMain.c"
};

for source in sources do
	echo("-------------------------------------------------------");
	echo("- ProfileSymbolValues for " + source);
	echo("-------------------------------------------------------");

	p = CreateGecosProject("test");
	AddSourceToGecosProject(p, source);
	
	ProfileSymbolValues(p, "regen/"+basename(source));
done;