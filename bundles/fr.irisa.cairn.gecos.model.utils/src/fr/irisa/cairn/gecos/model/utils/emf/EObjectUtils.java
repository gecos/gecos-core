package fr.irisa.cairn.gecos.model.utils.emf;

import java.util.function.Predicate;

import org.eclipse.emf.ecore.EObject;

public class EObjectUtils {
	
	/**
	 * Returns the closest {@link EObject#eContainer() container object} up to {@code limit} (included) of the requested type. 
	 * If the given object is an instance of the requested type, then the object itself will be returned. 
	 * If no container object is of the requested type, then {@code null} will be returned.
	 * If limit is {@code null} or not in the container hierarchy of ele, the search continue until the root.
	 */
	public static <T extends EObject> T getContainerOfType(EObject ele, Class<T> type, EObject limit) {
		EObject container;
		for (container = ele; container != null && container != limit; container = container.eContainer())
			if (type.isInstance(container))
				return type.cast(container);

		if(container != null && type.isInstance(container))
			return type.cast(container);
		
		return null;
	}
	
	/**
	 * Returns the closest {@link EObject#eContainer() container object} up to {@code limit} (included) of the requested type
	 * that also passes the specified filter. 
	 * If the given object is an instance of the requested type, then the object itself will be returned. 
	 * If no container object is of the requested type, then {@code null} will be returned.
	 * If limit is {@code null} or not in the container hierarchy of ele, the search continue until the root.
	 */
	@SuppressWarnings("unchecked")
	public static <T extends EObject> T getContainerOfType(EObject ele, Class<T> type, EObject limit, Predicate<T> filter) {
		EObject container;
		for (container = ele; container != null && container != limit; container = container.eContainer())
			if (type.isInstance(container) && filter.test((T) container))
				return type.cast(container);

		if(container != null && type.isInstance(container) && filter.test((T) container))
			return type.cast(container);
		
		return null;
	}
	
}
