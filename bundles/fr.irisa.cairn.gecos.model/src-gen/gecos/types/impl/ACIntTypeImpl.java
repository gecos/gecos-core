/**
 */
package gecos.types.impl;

import gecos.types.ACIntType;
import gecos.types.ACType;
import gecos.types.BaseType;
import gecos.types.BoolType;
import gecos.types.FloatType;
import gecos.types.IntegerType;
import gecos.types.OverflowMode;
import gecos.types.ScalarType;
import gecos.types.Type;
import gecos.types.TypesPackage;
import gecos.types.TypesVisitor;
import gecos.types.VoidType;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AC Int Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.impl.ACIntTypeImpl#getSize <em>Size</em>}</li>
 *   <li>{@link gecos.types.impl.ACIntTypeImpl#isSigned <em>Signed</em>}</li>
 *   <li>{@link gecos.types.impl.ACIntTypeImpl#getBitwidth <em>Bitwidth</em>}</li>
 *   <li>{@link gecos.types.impl.ACIntTypeImpl#getOverflowMode <em>Overflow Mode</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ACIntTypeImpl extends ArrayTypeImpl implements ACIntType {
	/**
	 * The default value of the '{@link #getSize() <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSize()
	 * @generated
	 * @ordered
	 */
	protected static final long SIZE_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getSize() <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSize()
	 * @generated
	 * @ordered
	 */
	protected long size = SIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #isSigned() <em>Signed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSigned()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SIGNED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSigned() <em>Signed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSigned()
	 * @generated
	 * @ordered
	 */
	protected boolean signed = SIGNED_EDEFAULT;

	/**
	 * The default value of the '{@link #getBitwidth() <em>Bitwidth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBitwidth()
	 * @generated
	 * @ordered
	 */
	protected static final long BITWIDTH_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getBitwidth() <em>Bitwidth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBitwidth()
	 * @generated
	 * @ordered
	 */
	protected long bitwidth = BITWIDTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getOverflowMode() <em>Overflow Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverflowMode()
	 * @generated
	 * @ordered
	 */
	protected static final OverflowMode OVERFLOW_MODE_EDEFAULT = OverflowMode.AC_WRAP;

	/**
	 * The cached value of the '{@link #getOverflowMode() <em>Overflow Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverflowMode()
	 * @generated
	 * @ordered
	 */
	protected OverflowMode overflowMode = OVERFLOW_MODE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ACIntTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypesPackage.Literals.AC_INT_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getSize() {
		return size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSize(long newSize) {
		long oldSize = size;
		size = newSize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.AC_INT_TYPE__SIZE, oldSize, size));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSigned() {
		return signed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSigned(boolean newSigned) {
		boolean oldSigned = signed;
		signed = newSigned;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.AC_INT_TYPE__SIGNED, oldSigned, signed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getBitwidth() {
		return bitwidth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBitwidth(long newBitwidth) {
		long oldBitwidth = bitwidth;
		bitwidth = newBitwidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.AC_INT_TYPE__BITWIDTH, oldBitwidth, bitwidth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OverflowMode getOverflowMode() {
		return overflowMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOverflowMode(OverflowMode newOverflowMode) {
		OverflowMode oldOverflowMode = overflowMode;
		overflowMode = newOverflowMode == null ? OVERFLOW_MODE_EDEFAULT : newOverflowMode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.AC_INT_TYPE__OVERFLOW_MODE, oldOverflowMode, overflowMode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final TypesVisitor visitor) {
		visitor.visitACIntType(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		final StringBuffer result = new StringBuffer("ac_int<");
		result.append(this.getBitwidth());
		String _xifexpression = null;
		boolean _signed = this.getSigned();
		if (_signed) {
			_xifexpression = ",true";
		}
		else {
			_xifexpression = "false";
		}
		result.append(_xifexpression);
		int _value = this.getOverflowMode().getValue();
		boolean _notEquals = (_value != 0);
		if (_notEquals) {
			String _name = this.getOverflowMode().getName();
			String _plus = ("," + _name);
			result.append(_plus);
		}
		result.append(">");
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEqual(final Type other, final boolean ignoreStorageClass, final boolean ignoreConst, final boolean ignoreVolatile) {
		if ((this == other)) {
			return true;
		}
		if ((other instanceof ACIntType)) {
			return (((((ACIntType)other).getBitwidth() == this.getBitwidth()) && (((ACIntType)other).getOverflowMode() == this.getOverflowMode())) && super.isEqual(other, ignoreStorageClass, ignoreConst, ignoreVolatile));
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean getSigned() {
		return this.isSigned();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerType asInt() {
		if ((this instanceof IntegerType)) {
			return ((IntegerType) this);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FloatType asFloat() {
		if ((this instanceof FloatType)) {
			return ((FloatType) this);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BoolType asBool() {
		if ((this instanceof BoolType)) {
			return ((BoolType) this);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VoidType asVoid() {
		if ((this instanceof VoidType)) {
			return ((VoidType) this);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TypesPackage.AC_INT_TYPE__SIZE:
				return getSize();
			case TypesPackage.AC_INT_TYPE__SIGNED:
				return isSigned();
			case TypesPackage.AC_INT_TYPE__BITWIDTH:
				return getBitwidth();
			case TypesPackage.AC_INT_TYPE__OVERFLOW_MODE:
				return getOverflowMode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TypesPackage.AC_INT_TYPE__SIZE:
				setSize((Long)newValue);
				return;
			case TypesPackage.AC_INT_TYPE__SIGNED:
				setSigned((Boolean)newValue);
				return;
			case TypesPackage.AC_INT_TYPE__BITWIDTH:
				setBitwidth((Long)newValue);
				return;
			case TypesPackage.AC_INT_TYPE__OVERFLOW_MODE:
				setOverflowMode((OverflowMode)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TypesPackage.AC_INT_TYPE__SIZE:
				setSize(SIZE_EDEFAULT);
				return;
			case TypesPackage.AC_INT_TYPE__SIGNED:
				setSigned(SIGNED_EDEFAULT);
				return;
			case TypesPackage.AC_INT_TYPE__BITWIDTH:
				setBitwidth(BITWIDTH_EDEFAULT);
				return;
			case TypesPackage.AC_INT_TYPE__OVERFLOW_MODE:
				setOverflowMode(OVERFLOW_MODE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TypesPackage.AC_INT_TYPE__SIZE:
				return size != SIZE_EDEFAULT;
			case TypesPackage.AC_INT_TYPE__SIGNED:
				return signed != SIGNED_EDEFAULT;
			case TypesPackage.AC_INT_TYPE__BITWIDTH:
				return bitwidth != BITWIDTH_EDEFAULT;
			case TypesPackage.AC_INT_TYPE__OVERFLOW_MODE:
				return overflowMode != OVERFLOW_MODE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ScalarType.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == BaseType.class) {
			switch (derivedFeatureID) {
				case TypesPackage.AC_INT_TYPE__SIZE: return TypesPackage.BASE_TYPE__SIZE;
				default: return -1;
			}
		}
		if (baseClass == ACType.class) {
			switch (derivedFeatureID) {
				case TypesPackage.AC_INT_TYPE__SIGNED: return TypesPackage.AC_TYPE__SIGNED;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ScalarType.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == BaseType.class) {
			switch (baseFeatureID) {
				case TypesPackage.BASE_TYPE__SIZE: return TypesPackage.AC_INT_TYPE__SIZE;
				default: return -1;
			}
		}
		if (baseClass == ACType.class) {
			switch (baseFeatureID) {
				case TypesPackage.AC_TYPE__SIGNED: return TypesPackage.AC_INT_TYPE__SIGNED;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ACIntTypeImpl
