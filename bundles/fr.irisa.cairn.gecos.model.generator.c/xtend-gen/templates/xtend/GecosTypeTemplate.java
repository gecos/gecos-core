package templates.xtend;

import com.google.common.base.Objects;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableInstructionCGenerator;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableTypeCGenerator;
import fr.irisa.cairn.gecos.model.extensions.generators.IGecosCodeGenerator;
import gecos.instrs.Instruction;
import gecos.types.ACChannelType;
import gecos.types.ACComplexType;
import gecos.types.ACFixedType;
import gecos.types.ACFloatType;
import gecos.types.ACIntType;
import gecos.types.AliasType;
import gecos.types.ArrayType;
import gecos.types.BoolType;
import gecos.types.EnumType;
import gecos.types.Enumerator;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.FloatType;
import gecos.types.FunctionType;
import gecos.types.IntegerType;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.PtrType;
import gecos.types.QuantificationMode;
import gecos.types.RecordType;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import gecos.types.VoidType;
import java.util.Arrays;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import templates.xtend.GecosTemplate;

@SuppressWarnings("all")
public class GecosTypeTemplate extends GecosTemplate {
  public enum BitLevelDataType {
    SystemC,
    
    MentorAC,
    
    VivadoAP;
  }
  
  public enum CustomFloatDataType {
    AC_FLOAT,
    
    CT_FLOAT;
  }
  
  public static boolean ADD_ALIGN_ATTR_TO_ARRAYS = false;
  
  @Deprecated
  public static boolean UseSCFixedDataType = false;
  
  public static GecosTypeTemplate.BitLevelDataType bitLevelDataType = GecosTypeTemplate.BitLevelDataType.MentorAC;
  
  public static GecosTypeTemplate.CustomFloatDataType customFloatDataType = GecosTypeTemplate.CustomFloatDataType.AC_FLOAT;
  
  protected Object _generate(final ACChannelType acChannelType) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/* ACChannelType not yet implemented !!!*/");
    String _string = acChannelType.toString();
    _builder.append(_string);
    return _builder;
  }
  
  protected Object _generate(final ACComplexType acComplexType) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/* ACComplexType not yet implemented !!!*/");
    String _string = acComplexType.toString();
    _builder.append(_string);
    return _builder;
  }
  
  protected Object _generate(final ACFixedType acFixedType) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      buffer.append(this.generateTypeSpecifiers(acFixedType));
      if (GecosTypeTemplate.UseSCFixedDataType) {
        GecosTypeTemplate.bitLevelDataType = GecosTypeTemplate.BitLevelDataType.SystemC;
      }
      final GecosTypeTemplate.BitLevelDataType bitLevelDataType = GecosTypeTemplate.bitLevelDataType;
      if (bitLevelDataType != null) {
        switch (bitLevelDataType) {
          case SystemC:
            boolean _isSigned = acFixedType.isSigned();
            if (_isSigned) {
              StringConcatenation _builder = new StringConcatenation();
              _builder.append("sc_fixed<");
              long _bitwidth = acFixedType.getBitwidth();
              _builder.append(_bitwidth);
              _builder.append(", ");
              long _integer = acFixedType.getInteger();
              _builder.append(_integer);
              buffer.append(_builder);
            } else {
              StringConcatenation _builder_1 = new StringConcatenation();
              _builder_1.append("sc_ufixed<");
              long _bitwidth_1 = acFixedType.getBitwidth();
              _builder_1.append(_bitwidth_1);
              _builder_1.append(", ");
              long _integer_1 = acFixedType.getInteger();
              _builder_1.append(_integer_1);
              buffer.append(_builder_1);
            }
            QuantificationMode _quantificationMode = acFixedType.getQuantificationMode();
            boolean _equals = Objects.equal(_quantificationMode, QuantificationMode.AC_TRN);
            if (_equals) {
              StringConcatenation _builder_2 = new StringConcatenation();
              _builder_2.append(", SC_TRN");
              buffer.append(_builder_2);
            } else {
              StringConcatenation _builder_3 = new StringConcatenation();
              _builder_3.append(", SC_RND");
              buffer.append(_builder_3);
            }
            StringConcatenation _builder_4 = new StringConcatenation();
            _builder_4.append(", SC_SAT>");
            buffer.append(_builder_4);
            break;
          case VivadoAP:
            StringConcatenation _builder_5 = new StringConcatenation();
            _builder_5.append("ap_fixed<");
            long _bitwidth_2 = acFixedType.getBitwidth();
            _builder_5.append(_bitwidth_2);
            _builder_5.append(", ");
            long _integer_2 = acFixedType.getInteger();
            _builder_5.append(_integer_2);
            _builder_5.append(", ");
            int _xifexpression = (int) 0;
            boolean _isSigned_1 = acFixedType.isSigned();
            if (_isSigned_1) {
              _xifexpression = 1;
            } else {
              _xifexpression = 0;
            }
            _builder_5.append(_xifexpression);
            buffer.append(_builder_5);
            if ((Objects.equal(acFixedType.getQuantificationMode(), QuantificationMode.AC_TRN) && Objects.equal(acFixedType.getOverflowMode(), OverflowMode.AC_SAT))) {
              StringConcatenation _builder_6 = new StringConcatenation();
              _builder_6.append(">");
              buffer.append(_builder_6);
            } else {
              StringConcatenation _builder_7 = new StringConcatenation();
              _builder_7.append(", ");
              QuantificationMode _quantificationMode_1 = acFixedType.getQuantificationMode();
              _builder_7.append(_quantificationMode_1);
              _builder_7.append(", ");
              OverflowMode _overflowMode = acFixedType.getOverflowMode();
              _builder_7.append(_overflowMode);
              _builder_7.append(">");
              buffer.append(_builder_7);
            }
            break;
          case MentorAC:
            StringConcatenation _builder_8 = new StringConcatenation();
            _builder_8.append("ac_fixed<");
            long _bitwidth_3 = acFixedType.getBitwidth();
            _builder_8.append(_bitwidth_3);
            _builder_8.append(", ");
            long _integer_3 = acFixedType.getInteger();
            _builder_8.append(_integer_3);
            _builder_8.append(", ");
            int _xifexpression_1 = (int) 0;
            boolean _isSigned_2 = acFixedType.isSigned();
            if (_isSigned_2) {
              _xifexpression_1 = 1;
            } else {
              _xifexpression_1 = 0;
            }
            _builder_8.append(_xifexpression_1);
            buffer.append(_builder_8);
            if ((Objects.equal(acFixedType.getQuantificationMode(), QuantificationMode.AC_TRN) && Objects.equal(acFixedType.getOverflowMode(), OverflowMode.AC_SAT))) {
              StringConcatenation _builder_9 = new StringConcatenation();
              _builder_9.append(">");
              buffer.append(_builder_9);
            } else {
              StringConcatenation _builder_10 = new StringConcatenation();
              _builder_10.append(", ");
              QuantificationMode _quantificationMode_2 = acFixedType.getQuantificationMode();
              _builder_10.append(_quantificationMode_2);
              _builder_10.append(", ");
              OverflowMode _overflowMode_1 = acFixedType.getOverflowMode();
              _builder_10.append(_overflowMode_1);
              _builder_10.append(">");
              buffer.append(_builder_10);
            }
            break;
          default:
            StringConcatenation _builder_11 = new StringConcatenation();
            _builder_11.append("/* Unknown data type !!!*/");
            buffer.append(_builder_11);
            break;
        }
      } else {
        StringConcatenation _builder_11 = new StringConcatenation();
        _builder_11.append("/* Unknown data type !!!*/");
        buffer.append(_builder_11);
      }
      StringConcatenation _builder_12 = new StringConcatenation();
      String _string = buffer.toString();
      _builder_12.append(_string);
      _xblockexpression = _builder_12;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final ACFloatType acFloatType) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      buffer.append(this.generateTypeSpecifiers(acFloatType));
      CharSequence _switchResult = null;
      final GecosTypeTemplate.CustomFloatDataType customFloatDataType = GecosTypeTemplate.customFloatDataType;
      if (customFloatDataType != null) {
        switch (customFloatDataType) {
          case AC_FLOAT:
            CharSequence _xblockexpression_1 = null;
            {
              StringConcatenation _builder = new StringConcatenation();
              _builder.append("ac_float<");
              long _radix = acFloatType.getRadix();
              long _mantissa = acFloatType.getMantissa();
              long _plus = (_radix + _mantissa);
              _builder.append(_plus);
              _builder.append(", ");
              long _radix_1 = acFloatType.getRadix();
              _builder.append(_radix_1);
              _builder.append(", ");
              long _exponent = acFloatType.getExponent();
              _builder.append(_exponent);
              _builder.append(">");
              buffer.append(_builder);
              StringConcatenation _builder_1 = new StringConcatenation();
              String _string = buffer.toString();
              _builder_1.append(_string);
              _xblockexpression_1 = _builder_1;
            }
            _switchResult = _xblockexpression_1;
            break;
          case CT_FLOAT:
            StringConcatenation _builder = new StringConcatenation();
            _builder.append("ct_float<");
            long _exponent = acFloatType.getExponent();
            _builder.append(_exponent);
            _builder.append(", ");
            long _mantissa = acFloatType.getMantissa();
            _builder.append(_mantissa);
            _builder.append(", CT_RD>");
            _switchResult = buffer.append(_builder);
            break;
          default:
            StringConcatenation _builder_1 = new StringConcatenation();
            _builder_1.append("/* Unknown data type !!!*/");
            _switchResult = buffer.append(_builder_1);
            break;
        }
      } else {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("/* Unknown data type !!!*/");
        _switchResult = buffer.append(_builder_1);
      }
      _xblockexpression = _switchResult;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final ACIntType acIntType) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      buffer.append(this.generateTypeSpecifiers(acIntType));
      final GecosTypeTemplate.BitLevelDataType bitLevelDataType = GecosTypeTemplate.bitLevelDataType;
      if (bitLevelDataType != null) {
        switch (bitLevelDataType) {
          case SystemC:
            StringConcatenation _builder = new StringConcatenation();
            _builder.append("sc_int<");
            long _bitwidth = acIntType.getBitwidth();
            _builder.append(_bitwidth);
            _builder.append(", ");
            int _xifexpression = (int) 0;
            boolean _isSigned = acIntType.isSigned();
            if (_isSigned) {
              _xifexpression = 1;
            } else {
              _xifexpression = 0;
            }
            _builder.append(_xifexpression);
            _builder.append(">");
            buffer.append(_builder);
            break;
          case VivadoAP:
            boolean _isSigned_1 = acIntType.isSigned();
            if (_isSigned_1) {
              StringConcatenation _builder_1 = new StringConcatenation();
              _builder_1.append("ap_int<");
              long _bitwidth_1 = acIntType.getBitwidth();
              _builder_1.append(_bitwidth_1);
              _builder_1.append(">");
              buffer.append(_builder_1);
            } else {
              StringConcatenation _builder_2 = new StringConcatenation();
              _builder_2.append("ap_uint<");
              long _bitwidth_2 = acIntType.getBitwidth();
              _builder_2.append(_bitwidth_2);
              _builder_2.append(">");
              buffer.append(_builder_2);
            }
            break;
          case MentorAC:
            StringConcatenation _builder_3 = new StringConcatenation();
            _builder_3.append("ac_int<");
            long _bitwidth_3 = acIntType.getBitwidth();
            _builder_3.append(_bitwidth_3);
            _builder_3.append(", ");
            int _xifexpression_1 = (int) 0;
            boolean _isSigned_2 = acIntType.isSigned();
            if (_isSigned_2) {
              _xifexpression_1 = 1;
            } else {
              _xifexpression_1 = 0;
            }
            _builder_3.append(_xifexpression_1);
            _builder_3.append(">");
            buffer.append(_builder_3);
            break;
          default:
            StringConcatenation _builder_4 = new StringConcatenation();
            _builder_4.append("/* Unknown data type !!!*/");
            buffer.append(_builder_4);
            break;
        }
      } else {
        StringConcatenation _builder_4 = new StringConcatenation();
        _builder_4.append("/* Unknown data type !!!*/");
        buffer.append(_builder_4);
      }
      StringConcatenation _builder_5 = new StringConcatenation();
      _builder_5.append(buffer);
      _xblockexpression = _builder_5;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final AliasType aliasType) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      buffer.append(this.generateTypeSpecifiers(aliasType));
      StringConcatenation _builder = new StringConcatenation();
      String _name = aliasType.getName();
      _builder.append(_name);
      buffer.append(_builder);
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append(buffer);
      _xblockexpression = _builder_1;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final IntegerType t) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      buffer.append(this.generateTypeSpecifiers(t));
      SignModifiers _signModifier = t.getSignModifier();
      boolean _equals = Objects.equal(_signModifier, SignModifiers.SIGNED);
      if (_equals) {
        buffer.append("signed ");
      } else {
        SignModifiers _signModifier_1 = t.getSignModifier();
        boolean _equals_1 = Objects.equal(_signModifier_1, SignModifiers.UNSIGNED);
        if (_equals_1) {
          buffer.append("unsigned ");
        }
      }
      IntegerTypes _type = t.getType();
      if (_type != null) {
        switch (_type) {
          case CHAR:
            buffer.append("char");
            break;
          case SHORT:
            buffer.append("short");
            break;
          case INT:
            buffer.append("int");
            break;
          case LONG:
            buffer.append("long");
            break;
          case LONG_LONG:
            buffer.append("long long");
            break;
          default:
            IntegerTypes _type_1 = t.getType();
            String _plus = ("Unsupported IntegerType:" + _type_1);
            throw new RuntimeException(_plus);
        }
      } else {
        IntegerTypes _type_1 = t.getType();
        String _plus = ("Unsupported IntegerType:" + _type_1);
        throw new RuntimeException(_plus);
      }
      StringConcatenation _builder = new StringConcatenation();
      String _string = buffer.toString();
      _builder.append(_string);
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final FloatType t) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      buffer.append(this.generateTypeSpecifiers(t));
      FloatPrecisions _precision = t.getPrecision();
      if (_precision != null) {
        switch (_precision) {
          case SINGLE:
            buffer.append("float");
            break;
          case DOUBLE:
            buffer.append("double");
            break;
          case LONG_DOUBLE:
            buffer.append("long double");
            break;
          case HALF:
            buffer.append("/* cannot generate HALF floating-point type! */ float");
            break;
          default:
            FloatPrecisions _precision_1 = t.getPrecision();
            String _plus = ("Unsupported FloatType:" + _precision_1);
            throw new RuntimeException(_plus);
        }
      } else {
        FloatPrecisions _precision_1 = t.getPrecision();
        String _plus = ("Unsupported FloatType:" + _precision_1);
        throw new RuntimeException(_plus);
      }
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(buffer);
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final BoolType t) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      buffer.append(this.generateTypeSpecifiers(t));
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("_Bool");
      buffer.append(_builder);
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append(buffer);
      _xblockexpression = _builder_1;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final VoidType t) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      buffer.append(this.generateTypeSpecifiers(t));
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("void");
      buffer.append(_builder);
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append(buffer);
      _xblockexpression = _builder_1;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final Enumerator enumerator) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = enumerator.getName();
    _builder.append(_name);
    return _builder;
  }
  
  protected Object _generate(final EnumType enumType) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      buffer.append(this.generateTypeSpecifiers(enumType));
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("enum ");
      String _name = enumType.getName();
      _builder.append(_name);
      buffer.append(_builder);
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append(buffer);
      _xblockexpression = _builder_1;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final Field field) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = field.getName();
    _builder.append(_name);
    return _builder;
  }
  
  protected Object _generate(final FunctionType functionType) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      buffer.append(IGecosCodeGenerator.afterSymbolInformation);
      buffer.append("(");
      boolean first = true;
      EList<Type> _listParameters = functionType.listParameters();
      for (final Type s : _listParameters) {
        {
          if (first) {
            first = false;
          } else {
            StringConcatenation _builder = new StringConcatenation();
            _builder.append(", ");
            buffer.append(_builder);
          }
          IGecosCodeGenerator.afterSymbolInformation.setLength(0);
          StringConcatenation _builder_1 = new StringConcatenation();
          String _generate = ExtendableTypeCGenerator.eInstance.generate(s);
          _builder_1.append(_generate);
          buffer.append(_builder_1);
          buffer.append(IGecosCodeGenerator.afterSymbolInformation);
        }
      }
      boolean _isHasElipsis = functionType.isHasElipsis();
      if (_isHasElipsis) {
        if (first) {
          first = false;
        } else {
          StringConcatenation _builder = new StringConcatenation();
          _builder.append(", ");
          buffer.append(_builder);
        }
        buffer.append("...");
      }
      buffer.append(")");
      IGecosCodeGenerator.afterSymbolInformation.setLength(0);
      IGecosCodeGenerator.afterSymbolInformation.append(buffer);
      StringConcatenation _builder_1 = new StringConcatenation();
      String _generate = ExtendableTypeCGenerator.eInstance.generate(functionType.getReturnType());
      _builder_1.append(_generate);
      _xblockexpression = _builder_1;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final PtrType ptrType) {
    CharSequence _xblockexpression = null;
    {
      StringBuffer buf = new StringBuffer();
      boolean needParenthesis = ((ptrType.getBase() instanceof ArrayType) || (ptrType.getBase() instanceof FunctionType));
      if (needParenthesis) {
        IGecosCodeGenerator.afterSymbolInformation.append(")");
      }
      StringConcatenation _builder = new StringConcatenation();
      String _generate = ExtendableTypeCGenerator.eInstance.generate(ptrType.getBase());
      _builder.append(_generate);
      buf.append(_builder);
      if (needParenthesis) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append(" ");
        _builder_1.append("(*");
        buf.append(_builder_1);
      } else {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append(" ");
        _builder_2.append("*");
        buf.append(_builder_2);
      }
      boolean _isRestrict = ptrType.isRestrict();
      if (_isRestrict) {
        buf.append("restrict ");
      }
      buf.append(this.generateTypeSpecifiers(ptrType));
      int _length = buf.length();
      int _minus = (_length - 1);
      boolean _isWhitespace = Character.isWhitespace(buf.charAt(_minus));
      if (_isWhitespace) {
        int _length_1 = buf.length();
        int _minus_1 = (_length_1 - 1);
        buf.deleteCharAt(_minus_1);
      }
      StringConcatenation _builder_3 = new StringConcatenation();
      String _string = buf.toString();
      _builder_3.append(_string);
      _xblockexpression = _builder_3;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final ArrayType arrayType) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      Instruction _sizeExpr = arrayType.getSizeExpr();
      boolean _tripleEquals = (_sizeExpr == null);
      if (_tripleEquals) {
        IGecosCodeGenerator.afterSymbolInformation.append("[]");
      } else {
        final String value = ExtendableInstructionCGenerator.eInstance.generate(arrayType.getSizeExpr());
        IGecosCodeGenerator.afterSymbolInformation.append((("[" + value) + "]"));
      }
      buffer.append(ExtendableTypeCGenerator.eInstance.generate(arrayType.getBase()));
      if ((GecosTypeTemplate.ADD_ALIGN_ATTR_TO_ARRAYS && (!(arrayType.getBase() instanceof ArrayType)))) {
        buffer.append(" __ALIGN_ATT__");
      }
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(buffer);
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final RecordType recordType) {
    CharSequence _xblockexpression = null;
    {
      StringBuffer buffer = new StringBuffer();
      buffer.append(this.generateTypeSpecifiers(recordType));
      Kinds _kind = recordType.getKind();
      boolean _equals = Objects.equal(_kind, Kinds.STRUCT);
      if (_equals) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("struct ");
        String _name = recordType.getName();
        _builder.append(_name);
        buffer.append(_builder);
      } else {
        Kinds _kind_1 = recordType.getKind();
        boolean _equals_1 = Objects.equal(_kind_1, Kinds.UNION);
        if (_equals_1) {
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append("union ");
          String _name_1 = recordType.getName();
          _builder_1.append(_name_1);
          buffer.append(_builder_1);
        } else {
          StringConcatenation _builder_2 = new StringConcatenation();
          _builder_2.append("/* Unknown record type kind : ");
          Kinds _kind_2 = recordType.getKind();
          _builder_2.append(_kind_2);
          _builder_2.append(" */");
          buffer.append(_builder_2);
        }
      }
      StringConcatenation _builder_3 = new StringConcatenation();
      _builder_3.append(buffer);
      _xblockexpression = _builder_3;
    }
    return _xblockexpression;
  }
  
  /**
   * order: [Storage_class] [const] [volatile]
   * Not strictly important but used for testing.
   */
  public CharSequence generateTypeSpecifiers(final Type type) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      StorageClassSpecifiers _storageClass = type.getStorageClass();
      if (_storageClass != null) {
        switch (_storageClass) {
          case AUTO:
            StringConcatenation _builder = new StringConcatenation();
            _builder.append("auto ");
            buffer.append(_builder);
            break;
          case EXTERN:
            StringConcatenation _builder_1 = new StringConcatenation();
            _builder_1.append("extern ");
            buffer.append(_builder_1);
            break;
          case REGISTER:
            StringConcatenation _builder_2 = new StringConcatenation();
            _builder_2.append("register ");
            buffer.append(_builder_2);
            break;
          case STATIC:
            StringConcatenation _builder_3 = new StringConcatenation();
            _builder_3.append("static ");
            buffer.append(_builder_3);
            break;
          case NONE:
            break;
          default:
            break;
        }
      }
      boolean _isConstant = type.isConstant();
      if (_isConstant) {
        StringConcatenation _builder_4 = new StringConcatenation();
        _builder_4.append("const ");
        buffer.append(_builder_4);
      }
      boolean _isVolatile = type.isVolatile();
      if (_isVolatile) {
        StringConcatenation _builder_5 = new StringConcatenation();
        _builder_5.append("volatile ");
        buffer.append(_builder_5);
      }
      StringConcatenation _builder_6 = new StringConcatenation();
      _builder_6.append(buffer);
      _xblockexpression = _builder_6;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final EObject object) {
    return null;
  }
  
  public Object generate(final EObject acChannelType) {
    if (acChannelType instanceof ACChannelType) {
      return _generate((ACChannelType)acChannelType);
    } else if (acChannelType instanceof ACComplexType) {
      return _generate((ACComplexType)acChannelType);
    } else if (acChannelType instanceof ACFixedType) {
      return _generate((ACFixedType)acChannelType);
    } else if (acChannelType instanceof ACFloatType) {
      return _generate((ACFloatType)acChannelType);
    } else if (acChannelType instanceof ACIntType) {
      return _generate((ACIntType)acChannelType);
    } else if (acChannelType instanceof BoolType) {
      return _generate((BoolType)acChannelType);
    } else if (acChannelType instanceof FloatType) {
      return _generate((FloatType)acChannelType);
    } else if (acChannelType instanceof IntegerType) {
      return _generate((IntegerType)acChannelType);
    } else if (acChannelType instanceof VoidType) {
      return _generate((VoidType)acChannelType);
    } else if (acChannelType instanceof AliasType) {
      return _generate((AliasType)acChannelType);
    } else if (acChannelType instanceof ArrayType) {
      return _generate((ArrayType)acChannelType);
    } else if (acChannelType instanceof EnumType) {
      return _generate((EnumType)acChannelType);
    } else if (acChannelType instanceof FunctionType) {
      return _generate((FunctionType)acChannelType);
    } else if (acChannelType instanceof PtrType) {
      return _generate((PtrType)acChannelType);
    } else if (acChannelType instanceof RecordType) {
      return _generate((RecordType)acChannelType);
    } else if (acChannelType instanceof Enumerator) {
      return _generate((Enumerator)acChannelType);
    } else if (acChannelType instanceof Field) {
      return _generate((Field)acChannelType);
    } else if (acChannelType != null) {
      return _generate(acChannelType);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(acChannelType).toString());
    }
  }
}
