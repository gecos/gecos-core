/**
 */
package gecos.blocks.impl;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;

import fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksFactory;
import gecos.blocks.BlocksPackage;
import gecos.blocks.BlocksVisitor;
import gecos.blocks.ControlEdge;
import gecos.blocks.IfBlock;

import gecos.instrs.BranchType;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>If Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.blocks.impl.IfBlockImpl#getTestBlock <em>Test Block</em>}</li>
 *   <li>{@link gecos.blocks.impl.IfBlockImpl#getThenBlock <em>Then Block</em>}</li>
 *   <li>{@link gecos.blocks.impl.IfBlockImpl#getElseBlock <em>Else Block</em>}</li>
 *   <li>{@link gecos.blocks.impl.IfBlockImpl#getJumpTo <em>Jump To</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IfBlockImpl extends BlockImpl implements IfBlock {
	/**
	 * The cached value of the '{@link #getTestBlock() <em>Test Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestBlock()
	 * @generated
	 * @ordered
	 */
	protected Block testBlock;

	/**
	 * The cached value of the '{@link #getThenBlock() <em>Then Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThenBlock()
	 * @generated
	 * @ordered
	 */
	protected Block thenBlock;

	/**
	 * The cached value of the '{@link #getElseBlock() <em>Else Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElseBlock()
	 * @generated
	 * @ordered
	 */
	protected Block elseBlock;

	/**
	 * The default value of the '{@link #getJumpTo() <em>Jump To</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJumpTo()
	 * @generated
	 * @ordered
	 */
	protected static final String JUMP_TO_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getJumpTo() <em>Jump To</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJumpTo()
	 * @generated
	 * @ordered
	 */
	protected String jumpTo = JUMP_TO_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IfBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BlocksPackage.Literals.IF_BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block getTestBlock() {
		return testBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTestBlock(Block newTestBlock, NotificationChain msgs) {
		Block oldTestBlock = testBlock;
		testBlock = newTestBlock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BlocksPackage.IF_BLOCK__TEST_BLOCK, oldTestBlock, newTestBlock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTestBlock(Block newTestBlock) {
		if (newTestBlock != testBlock) {
			NotificationChain msgs = null;
			if (testBlock != null)
				msgs = ((InternalEObject)testBlock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.IF_BLOCK__TEST_BLOCK, null, msgs);
			if (newTestBlock != null)
				msgs = ((InternalEObject)newTestBlock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.IF_BLOCK__TEST_BLOCK, null, msgs);
			msgs = basicSetTestBlock(newTestBlock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.IF_BLOCK__TEST_BLOCK, newTestBlock, newTestBlock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block getThenBlock() {
		return thenBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetThenBlock(Block newThenBlock, NotificationChain msgs) {
		Block oldThenBlock = thenBlock;
		thenBlock = newThenBlock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BlocksPackage.IF_BLOCK__THEN_BLOCK, oldThenBlock, newThenBlock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setThenBlock(Block newThenBlock) {
		if (newThenBlock != thenBlock) {
			NotificationChain msgs = null;
			if (thenBlock != null)
				msgs = ((InternalEObject)thenBlock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.IF_BLOCK__THEN_BLOCK, null, msgs);
			if (newThenBlock != null)
				msgs = ((InternalEObject)newThenBlock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.IF_BLOCK__THEN_BLOCK, null, msgs);
			msgs = basicSetThenBlock(newThenBlock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.IF_BLOCK__THEN_BLOCK, newThenBlock, newThenBlock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block getElseBlock() {
		return elseBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElseBlock(Block newElseBlock, NotificationChain msgs) {
		Block oldElseBlock = elseBlock;
		elseBlock = newElseBlock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BlocksPackage.IF_BLOCK__ELSE_BLOCK, oldElseBlock, newElseBlock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElseBlock(Block newElseBlock) {
		if (newElseBlock != elseBlock) {
			NotificationChain msgs = null;
			if (elseBlock != null)
				msgs = ((InternalEObject)elseBlock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.IF_BLOCK__ELSE_BLOCK, null, msgs);
			if (newElseBlock != null)
				msgs = ((InternalEObject)newElseBlock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.IF_BLOCK__ELSE_BLOCK, null, msgs);
			msgs = basicSetElseBlock(newElseBlock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.IF_BLOCK__ELSE_BLOCK, newElseBlock, newElseBlock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getJumpTo() {
		return jumpTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setJumpTo(String newJumpTo) {
		String oldJumpTo = jumpTo;
		jumpTo = newJumpTo;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.IF_BLOCK__JUMP_TO, oldJumpTo, jumpTo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final BlocksVisitor visitor) {
		visitor.visitIfBlock(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block managedCopy(final BlockCopyManager mng) {
		final IfBlock ifblck = BlocksFactory.eINSTANCE.createIfBlock();
		ifblck.setTestBlock(this.getTestBlock().managedCopy(mng));
		ifblck.setThenBlock(this.getThenBlock().managedCopy(mng));
		Block _elseBlock = this.getElseBlock();
		boolean _tripleNotEquals = (_elseBlock != null);
		if (_tripleNotEquals) {
			ifblck.setElseBlock(this.getElseBlock().managedCopy(mng));
		}
		ifblck.setJumpTo(this.getJumpTo());
		int _number = this.getNumber();
		int _plus = (_number + 1000);
		ifblck.setNumber(_plus);
		GecosUserAnnotationFactory.copyAnnotations(this, ifblck);
		mng.link(this, ifblck);
		return ifblck;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _pathString = this.getPathString();
		String _plus = (_pathString + "(");
		Block _testBlock = this.getTestBlock();
		String _plus_1 = (_plus + _testBlock);
		String _plus_2 = (_plus_1 + ")");
		Block _thenBlock = this.getThenBlock();
		String res = (_plus_2 + _thenBlock);
		Block _elseBlock = this.getElseBlock();
		boolean _tripleNotEquals = (_elseBlock != null);
		if (_tripleNotEquals) {
			Block _elseBlock_1 = this.getElseBlock();
			String _plus_3 = ((res + " else ") + _elseBlock_1);
			res = _plus_3;
		}
		return res;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toShortString() {
		String _xifexpression = null;
		Block _elseBlock = this.getElseBlock();
		boolean _tripleNotEquals = (_elseBlock != null);
		if (_tripleNotEquals) {
			int _number = this.getNumber();
			_xifexpression = (("Else" + "_") + Integer.valueOf(_number));
		}
		return ("IfThen" + _xifexpression);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block simplifyBlock() {
		this.getThenBlock().simplifyBlock();
		Block _elseBlock = this.getElseBlock();
		boolean _tripleNotEquals = (_elseBlock != null);
		if (_tripleNotEquals) {
			this.getElseBlock().simplifyBlock();
		}
		return this;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeBlock(final Block bb) {
		Block _thenBlock = this.getThenBlock();
		boolean _tripleEquals = (_thenBlock == bb);
		if (_tripleEquals) {
			this.setThenBlock(null);
		}
		else {
			Block _elseBlock = this.getElseBlock();
			boolean _tripleEquals_1 = (_elseBlock == bb);
			if (_tripleEquals_1) {
				this.setElseBlock(null);
			}
			else {
				Block _testBlock = this.getTestBlock();
				boolean _tripleEquals_2 = (_testBlock == bb);
				if (_tripleEquals_2) {
					this.setTestBlock(null);
				}
				else {
					Block _thenBlock_1 = this.getThenBlock();
					boolean _tripleNotEquals = (_thenBlock_1 != null);
					if (_tripleNotEquals) {
						this.getThenBlock().removeBlock(bb);
					}
					Block _elseBlock_1 = this.getElseBlock();
					boolean _tripleNotEquals_1 = (_elseBlock_1 != null);
					if (_tripleNotEquals_1) {
						this.getElseBlock().removeBlock(bb);
					}
				}
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlEdge connectFromBasic(final BasicBlock dispatch, final BranchType cond) {
		Block _testBlock = this.getTestBlock();
		boolean _tripleNotEquals = (_testBlock != null);
		if (_tripleNotEquals) {
			return this.getTestBlock().connectFromBasic(dispatch, cond);
		}
		Block _thenBlock = this.getThenBlock();
		boolean _tripleNotEquals_1 = (_thenBlock != null);
		if (_tripleNotEquals_1) {
			return this.getThenBlock().connectFromBasic(dispatch, cond);
		}
		Block _elseBlock = this.getElseBlock();
		boolean _tripleNotEquals_2 = (_elseBlock != null);
		if (_tripleNotEquals_2) {
			return this.getElseBlock().connectFromBasic(dispatch, cond);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlEdge> connectTo(final Block to, final BranchType cond) {
		final BasicEList<ControlEdge> res = ECollections.<ControlEdge>newBasicEList();
		Block _thenBlock = this.getThenBlock();
		boolean _tripleNotEquals = (_thenBlock != null);
		if (_tripleNotEquals) {
			res.addAll(this.getThenBlock().connectTo(to, BranchType.UNCONDITIONAL));
		}
		else {
			Block _testBlock = this.getTestBlock();
			boolean _tripleNotEquals_1 = (_testBlock != null);
			if (_tripleNotEquals_1) {
				res.addAll(this.getTestBlock().connectTo(to, BranchType.IF_TRUE));
			}
		}
		Block _elseBlock = this.getElseBlock();
		boolean _tripleNotEquals_2 = (_elseBlock != null);
		if (_tripleNotEquals_2) {
			res.addAll(this.getElseBlock().connectTo(to, BranchType.UNCONDITIONAL));
		}
		else {
			Block _testBlock_1 = this.getTestBlock();
			boolean _tripleNotEquals_3 = (_testBlock_1 != null);
			if (_tripleNotEquals_3) {
				res.addAll(this.getTestBlock().connectTo(to, BranchType.IF_FALSE));
			}
		}
		return res;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void replace(final Block oldb, final Block newb) {
		Block _thenBlock = this.getThenBlock();
		boolean _tripleEquals = (oldb == _thenBlock);
		if (_tripleEquals) {
			this.setThenBlock(newb);
		}
		else {
			Block _elseBlock = this.getElseBlock();
			boolean _tripleEquals_1 = (oldb == _elseBlock);
			if (_tripleEquals_1) {
				this.setElseBlock(newb);
			}
			else {
				Block _testBlock = this.getTestBlock();
				boolean _tripleEquals_2 = (oldb == _testBlock);
				if (_tripleEquals_2) {
					this.setTestBlock(newb);
				}
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPathString() {
		return super.getPathString("IF");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BlocksPackage.IF_BLOCK__TEST_BLOCK:
				return basicSetTestBlock(null, msgs);
			case BlocksPackage.IF_BLOCK__THEN_BLOCK:
				return basicSetThenBlock(null, msgs);
			case BlocksPackage.IF_BLOCK__ELSE_BLOCK:
				return basicSetElseBlock(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BlocksPackage.IF_BLOCK__TEST_BLOCK:
				return getTestBlock();
			case BlocksPackage.IF_BLOCK__THEN_BLOCK:
				return getThenBlock();
			case BlocksPackage.IF_BLOCK__ELSE_BLOCK:
				return getElseBlock();
			case BlocksPackage.IF_BLOCK__JUMP_TO:
				return getJumpTo();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BlocksPackage.IF_BLOCK__TEST_BLOCK:
				setTestBlock((Block)newValue);
				return;
			case BlocksPackage.IF_BLOCK__THEN_BLOCK:
				setThenBlock((Block)newValue);
				return;
			case BlocksPackage.IF_BLOCK__ELSE_BLOCK:
				setElseBlock((Block)newValue);
				return;
			case BlocksPackage.IF_BLOCK__JUMP_TO:
				setJumpTo((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BlocksPackage.IF_BLOCK__TEST_BLOCK:
				setTestBlock((Block)null);
				return;
			case BlocksPackage.IF_BLOCK__THEN_BLOCK:
				setThenBlock((Block)null);
				return;
			case BlocksPackage.IF_BLOCK__ELSE_BLOCK:
				setElseBlock((Block)null);
				return;
			case BlocksPackage.IF_BLOCK__JUMP_TO:
				setJumpTo(JUMP_TO_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BlocksPackage.IF_BLOCK__TEST_BLOCK:
				return testBlock != null;
			case BlocksPackage.IF_BLOCK__THEN_BLOCK:
				return thenBlock != null;
			case BlocksPackage.IF_BLOCK__ELSE_BLOCK:
				return elseBlock != null;
			case BlocksPackage.IF_BLOCK__JUMP_TO:
				return JUMP_TO_EDEFAULT == null ? jumpTo != null : !JUMP_TO_EDEFAULT.equals(jumpTo);
		}
		return super.eIsSet(featureID);
	}

} //IfBlockImpl
