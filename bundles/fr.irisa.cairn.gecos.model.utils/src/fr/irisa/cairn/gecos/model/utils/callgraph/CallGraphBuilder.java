/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.utils.callgraph;

import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import fr.irisa.r2d2.gecos.framework.GSModule;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;
import gecos.instrs.CallInstruction;


@GSModule("\nReturn the CallGraph object corresponding\n"
		+ "to the specified GecosProject or ProcedureSet.\n"
		+ "\nSee: 'output' command to export the CallGraph into a DOT file.")
public class CallGraphBuilder extends BlockInstructionSwitch<Object> {

	private CallGraph graph;
	private CallGraphNode current;

	private ProcedureSet procset;
	private GecosProject project;

	public CallGraphBuilder() {
		this.graph = new CallGraph();
	}
	public CallGraphBuilder(ProcedureSet procset) {
		this();
		this.procset = procset;
	}

	public CallGraphBuilder(GecosProject project) {
		this();
		this.project= project;
	}

	public CallGraph compute() {
		if (project==null) {
			return computeProcedureSet(procset);
		} else {
			return computeProject(project);
		}
	}
	private CallGraph computeProject(GecosProject proj) {
		proj.listProcedures().forEach(p -> graph.add(new CallGraphNode(p)));
		proj.listProcedures().forEach(p -> {current = graph.get(p.getSymbol().getName()); doSwitch(p.getBody()); });
		
		return graph;
	}

	private CallGraph computeProcedureSet(ProcedureSet ps) {
		ps.listProcedures().forEach(p -> graph.add(new CallGraphNode(p)));
		ps.listProcedures().forEach(p -> {current = graph.get(p.getSymbol().getName()); doSwitch(p.getBody()); });
		
		return graph;
	}
	
	public void addCallTo(String name) {
		CallGraphNode to = graph.get(name);
		// forget calls to externals
		if (to != null)
			current.linkTo(to);
	}

	@Override
	public Object caseCallInstruction(CallInstruction g) {
		Object res =super.caseCallInstruction(g);
		String sym = g.getSymbolAddress();
		if (sym == null) {
			throw new RuntimeException("Indirect call not handled : " + g.getAddress());
		}
		addCallTo(sym);
		return res;
	}
}
