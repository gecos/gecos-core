package fr.irisa.cairn.gecos.model.utils.dotprinter;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksDefaultVisitor;
import gecos.blocks.BasicBlock;
import gecos.blocks.ControlEdge;
import gecos.blocks.DataEdge;
import gecos.instrs.SSAUseSymbol;

public class GecosDotEdgePrinter extends GecosBlocksDefaultVisitor {
	
	
	public GecosDotPrinter getGecosDotPrinter() {
		return gecosDotPrinter;
	}

	/**
	 * 
	 */
	private final GecosDotPrinter gecosDotPrinter;

	/**
	 * @param gecosDotPrinter
	 */
	GecosDotEdgePrinter(GecosDotPrinter gecosDotPrinter) { 
		this.gecosDotPrinter = gecosDotPrinter;
	}

	@Override
	public void visitBasicBlock(BasicBlock b) {
		for (ControlEdge edge : b.getOutEdges()) {

			int from = edge.getFrom().getNumber();
			if(edge.getTo() == null)
				continue;
			int to = edge.getTo().getNumber();
			String prefix= ("n" + from + " -> n" + to);
			
			switch (edge.getCond()) {
			case UNCONDITIONAL:
				this.gecosDotPrinter.out.print(prefix+"[color=\""+GecosDotPrinter.COLOR_COMPOSITE+"\" label=\"Jmp\"];\n");
				break;
			case IF_FALSE:
				this.gecosDotPrinter.out.print(prefix+"[color=\""+GecosDotPrinter.COLOR_LOOP+"\" label=\"BFalse\"];\n");
				break;
			case IF_TRUE:
				this.gecosDotPrinter.out.print(prefix+"[color=\""+GecosDotPrinter.COLOR_LOOP+"\" label=\"BTrue\"];\n");
				break;
			case DISPATCH:
				this.gecosDotPrinter.out.print(prefix+"[color=\""+GecosDotPrinter.COLOR_FOR+"\" label=\"Dispatch\"];\n");
				break;
			default:
				this.gecosDotPrinter.out.print(prefix+"[color=\""+GecosDotPrinter.COLOR_COMPOSITE+"\" label=\"Unkown edge type\"];\n");
			}
		}
		for (DataEdge edge : b.getUseEdges()) {
			int from = edge.getFrom().getNumber();
			if(edge.getTo() == null)
				continue;
			int to = edge.getTo().getNumber();
			
			//skip self edge
			if (from == to) continue;
			this.gecosDotPrinter.print("n"+from+" -> n"+to);
			this.gecosDotPrinter.out.print("[color=\""+GecosDotPrinter.COLOR_IF+"\" label=\"");
			List<String> symbols = new ArrayList<String>(edge.getUses().size()); 
			boolean first = true;
			for(SSAUseSymbol ssaUse  : edge.getUses()) {
				String name = ssaUse.getSymbol().getName()+"_"+ssaUse.getNumber();
				//code to avoid printing of the same name
				if (symbols.contains(name)) continue;
				if(first) {
					this.gecosDotPrinter.out.print(name);
					first=false;
				} else {
					this.gecosDotPrinter.out.print(","+name);
				}
				symbols.add(name);
			}
			this.gecosDotPrinter.out.print("\"];\n");
		}			
	}
	
	
}