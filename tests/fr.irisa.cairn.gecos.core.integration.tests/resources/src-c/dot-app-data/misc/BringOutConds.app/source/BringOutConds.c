#include <stdio.h>
#include "defs.h"
#include "data_size.h"

void BringOutConditionals(TYPE_0 in[M][N], TYPE_1 out[M][N]) {
	int i, j;

	for (i = 0; i < M; i++)
		if (N > 100) {
			if (M > 10)
				for (j = 0; j < 100; j++)
					out[i][j] = (in[i][j] + i + j) * 2;
		} else
			for (j = 0; j < N; j++)
				if (j < 10)
					out[i][j] = in[i][j] + 10;
				else
					out[i][j] = in[i][j] - 10;

}
