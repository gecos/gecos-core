#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

inline static int32_t EMX_min_i32_i32_i32(int32_t src1, int32_t src2);
inline static int32_t EMX_max_i32_i32_i32(int32_t src1, int32_t src2);
inline static uint32_t EMX_divround_u32_u32_u32(uint32_t src1, uint32_t src2);
void comparerow_less_1(bool *const res_data, int32_t A_data[3][5000], int32_t A_size[2], int32_t r1_data, int32_t r2_data);
void comparerow_less_2(bool *const res_data, int32_t A_data[3][5000], int32_t A_size[2], int32_t r1_data, int32_t r2_data);
void comparerow_less_3(bool *const res_data, int32_t A_data[3][5000], int32_t A_size[2], int32_t r1_data, int32_t r2_data);
void comparerow_less_4(bool *const res_data, int32_t A_data[3][5000], int32_t A_size[2], int32_t r1_data, int32_t r2_data);
void emx_sortrows_merge_bottomup_1(int32_t B_data[3][5000], int32_t B_size[2], int32_t A_data[3][5000], int32_t A_size[2]);
void emx_sortrows_merge_bottomup_2(int32_t B_data[3][5000], int32_t B_size[2], int32_t A_data[3][5000], int32_t A_size[2]);
void emx_sortrows_merge_bottomup_3(int32_t B_data[3][5000], int32_t B_size[2], int32_t A_data[3][5000], int32_t A_size[2]);
void emx_sortrows_merge_bottomup_4(int32_t B_data[3][5000], int32_t B_size[2], int32_t A_data[3][5000], int32_t A_size[2]);
void gsort_s_1(int32_t B_data[3][5000], int32_t B_size[2], int32_t A_data[3][5000], int32_t A_size[2]);
void gsort_s_2(int32_t B_data[3][5000], int32_t B_size[2], int32_t A_data[3][5000], int32_t A_size[2]);
void gsort_s_3(int32_t B_data[3][5000], int32_t B_size[2], int32_t A_data[3][5000], int32_t A_size[2]);
void gsort_s_4(int32_t B_data[3][5000], int32_t B_size[2], int32_t A_data[3][5000], int32_t A_size[2]);
void gauss_filter_3_3_1(int32_t filtered_data[320][240], int16_t array_data[320][240]);
void gauss_filter_3_3_2(int32_t filtered_data[320][240], int16_t array_data[320][240]);
void gauss_filter_3_3_3(int32_t filtered_data[320][240], int16_t array_data[320][240]);
void gauss_filter_3_3_4(int32_t filtered_data[320][240], int16_t array_data[320][240]);
//void interface_outdata(uint8_t img_data[4][640][480]);
void calc_gradient_1(uint16_t goodkeypoints_data[2][1000], int32_t goodkeypoints_size[2], int16_t patch_data[320][240]);
void calc_gradient_2(uint16_t goodkeypoints_data[2][1000], int32_t goodkeypoints_size[2], int16_t patch_data[320][240]);
void calc_gradient_3(uint16_t goodkeypoints_data[2][1000], int32_t goodkeypoints_size[2], int16_t patch_data[320][240]);
void calc_gradient_4(uint16_t goodkeypoints_data[2][1000], int32_t goodkeypoints_size[2], int16_t patch_data[320][240]);
void rgb2gray(uint8_t grey_data[640][480], uint8_t rgb_data[4][640][480]);
//void interface_indata(uint8_t img_data[4][640][480]);
void harris_image();
int main(int argc, char * * argv);


inline static int32_t EMX_min_i32_i32_i32(int32_t src1, int32_t src2) {
	return src1 > src2 ? src2 : src1;
}

inline static int32_t EMX_max_i32_i32_i32(int32_t src1, int32_t src2) {
	return src1 < src2 ? src2 : src1;
}

inline static uint32_t EMX_divround_u32_u32_u32(uint32_t src1, uint32_t src2) {
	uint32_t rnd = (src2 < 0 ? -src2 : src2) >> 1;
	
	rnd = src1 < 0 ? -rnd : rnd;
	return (src1 + rnd) / src2;
}

void comparerow_less_1(bool *const res_data, int32_t A_data[3][5000], int32_t A_size[2], int32_t r1_data, int32_t r2_data) {
	int32_t i1;
	
	*res_data = 0;
	for(i1 = 0; i1 <= 3 - 1; i1 = i1 + 1) {
		if (A_data[i1][r1_data] != A_data[i1][r2_data]) {
			*res_data = A_data[i1][r1_data] < A_data[i1][r2_data];
			break;
		}
	}
}

void comparerow_less_2(bool *const res_data, int32_t A_data[3][5000], int32_t A_size[2], int32_t r1_data, int32_t r2_data) {
	int32_t i1;
	
	*res_data = 0;
	for(i1 = 0; i1 <= 3 - 1; i1 = i1 + 1) {
		if (A_data[i1][r1_data] != A_data[i1][r2_data]) {
			*res_data = A_data[i1][r1_data] < A_data[i1][r2_data];
			break;
		}
	}
}

void comparerow_less_3(bool *const res_data, int32_t A_data[3][5000], int32_t A_size[2], int32_t r1_data, int32_t r2_data) {
	int32_t i1;
	
	*res_data = 0;
	for(i1 = 0; i1 <= 3 - 1; i1 = i1 + 1) {
		if (A_data[i1][r1_data] != A_data[i1][r2_data]) {
			*res_data = A_data[i1][r1_data] < A_data[i1][r2_data];
			break;
		}
	}
}

void comparerow_less_4(bool *const res_data, int32_t A_data[3][5000], int32_t A_size[2], int32_t r1_data, int32_t r2_data) {
	int32_t i1;
	
	*res_data = 0;
	for(i1 = 0; i1 <= 3 - 1; i1 = i1 + 1) {
		if (A_data[i1][r1_data] != A_data[i1][r2_data]) {
			*res_data = A_data[i1][r1_data] < A_data[i1][r2_data];
			break;
		}
	}
}

void emx_sortrows_merge_bottomup_1(int32_t B_data[3][5000], int32_t B_size[2], int32_t A_data[3][5000], int32_t A_size[2]) {
	bool call1_data;
	bool cmp_data;
	int32_t iEnd_data;
	int32_t iLeft_data;
	int32_t iRight_data;
	int32_t i_data;
	int32_t i1;
	int32_t i2;
	int32_t i3;
	int32_t i4;
	int32_t i5;
	int32_t i6;
	int32_t i7;
	int32_t i8;
	int32_t j_data;
	int32_t n_data;
	int32_t width_data;
	
	#pragma EMX_KILLVAR B_data
	for(i2 = 0; i2 <= 3 - 1; i2 = i2 + 1) {
		for(i1 = 0; i1 <= A_size[0] - 1; i1 = i1 + 1) {
			B_data[i2][i1] = A_data[i2][i1];
		}
	}
	B_size[0] = A_size[0];
	B_size[1] = 3;
	n_data = A_size[0];
	width_data = 1;
	while (width_data < n_data) {
		for(i3 = 0; i3 <= (n_data - 1) / (2*width_data) + 1 - 1; i3 = i3 + 1) {
			iLeft_data = i3 * (2*width_data);
			iRight_data = EMX_min_i32_i32_i32(i3 * (2*width_data) + width_data, n_data);
			iEnd_data = EMX_min_i32_i32_i32(i3 * (2*width_data) + 2*width_data, n_data);
			i_data = i3 * (2*width_data);
			j_data = iRight_data;
			for(i4 = 0; i4 <= iEnd_data - 1 - i3 * (2*width_data) + 1 - 1; i4 = i4 + 1) {
				comparerow_less_2(&call1_data, A_data, A_size, j_data, i_data);
				cmp_data = (i_data < iRight_data) && ((j_data >= iEnd_data) || ((int64_t)!call1_data ^ 1));
				if (cmp_data) {
					for(i5 = 0; i5 <= 3 - 1; i5 = i5 + 1) {
						B_data[i5][i4 + iLeft_data] = A_data[i5][i_data];
					}
					B_size[0] = EMX_max_i32_i32_i32(B_size[0], i4 + iLeft_data + 1);
					B_size[1] = 3;
					i_data = i_data + 1;
				} else {
					for(i6 = 0; i6 <= 3 - 1; i6 = i6 + 1) {
						B_data[i6][i4 + iLeft_data] = A_data[i6][j_data];
					}
					B_size[0] = EMX_max_i32_i32_i32(B_size[0], i4 + iLeft_data + 1);
					B_size[1] = 3;
					j_data = j_data + 1;
				}
			}
		}
		#pragma EMX_KILLVAR A_data
		for(i8 = 0; i8 <= 3 - 1; i8 = i8 + 1) {
			for(i7 = 0; i7 <= B_size[0] - 1; i7 = i7 + 1) {
				A_data[i8][i7] = B_data[i8][i7];
			}
		}
		A_size[0] = B_size[0];
		A_size[1] = 3;
		width_data = width_data*2;
	}
}

void emx_sortrows_merge_bottomup_2(int32_t B_data[3][5000], int32_t B_size[2], int32_t A_data[3][5000], int32_t A_size[2]) {
	bool call1_data;
	bool cmp_data;
	int32_t iEnd_data;
	int32_t iLeft_data;
	int32_t iRight_data;
	int32_t i_data;
	int32_t i1;
	int32_t i2;
	int32_t i3;
	int32_t i4;
	int32_t i5;
	int32_t i6;
	int32_t i7;
	int32_t i8;
	int32_t j_data;
	int32_t n_data;
	int32_t width_data;
	
	#pragma EMX_KILLVAR B_data
	for(i2 = 0; i2 <= 3 - 1; i2 = i2 + 1) {
		for(i1 = 0; i1 <= A_size[0] - 1; i1 = i1 + 1) {
			B_data[i2][i1] = A_data[i2][i1];
		}
	}
	B_size[0] = A_size[0];
	B_size[1] = 3;
	n_data = A_size[0];
	width_data = 1;
	while (width_data < n_data) {
		for(i3 = 0; i3 <= (n_data - 1) / (2*width_data) + 1 - 1; i3 = i3 + 1) {
			iLeft_data = i3 * (2*width_data);
			iRight_data = EMX_min_i32_i32_i32(i3 * (2*width_data) + width_data, n_data);
			iEnd_data = EMX_min_i32_i32_i32(i3 * (2*width_data) + 2*width_data, n_data);
			i_data = i3 * (2*width_data);
			j_data = iRight_data;
			for(i4 = 0; i4 <= iEnd_data - 1 - i3 * (2*width_data) + 1 - 1; i4 = i4 + 1) {
				comparerow_less_3(&call1_data, A_data, A_size, j_data, i_data);
				cmp_data = (i_data < iRight_data) && ((j_data >= iEnd_data) || ((int64_t)!call1_data ^ 1));
				if (cmp_data) {
					for(i5 = 0; i5 <= 3 - 1; i5 = i5 + 1) {
						B_data[i5][i4 + iLeft_data] = A_data[i5][i_data];
					}
					B_size[0] = EMX_max_i32_i32_i32(B_size[0], i4 + iLeft_data + 1);
					B_size[1] = 3;
					i_data = i_data + 1;
				} else {
					for(i6 = 0; i6 <= 3 - 1; i6 = i6 + 1) {
						B_data[i6][i4 + iLeft_data] = A_data[i6][j_data];
					}
					B_size[0] = EMX_max_i32_i32_i32(B_size[0], i4 + iLeft_data + 1);
					B_size[1] = 3;
					j_data = j_data + 1;
				}
			}
		}
		#pragma EMX_KILLVAR A_data
		for(i8 = 0; i8 <= 3 - 1; i8 = i8 + 1) {
			for(i7 = 0; i7 <= B_size[0] - 1; i7 = i7 + 1) {
				A_data[i8][i7] = B_data[i8][i7];
			}
		}
		A_size[0] = B_size[0];
		A_size[1] = 3;
		width_data = width_data*2;
	}
}

void emx_sortrows_merge_bottomup_3(int32_t B_data[3][5000], int32_t B_size[2], int32_t A_data[3][5000], int32_t A_size[2]) {
	bool call1_data;
	bool cmp_data;
	int32_t iEnd_data;
	int32_t iLeft_data;
	int32_t iRight_data;
	int32_t i_data;
	int32_t i1;
	int32_t i2;
	int32_t i3;
	int32_t i4;
	int32_t i5;
	int32_t i6;
	int32_t i7;
	int32_t i8;
	int32_t j_data;
	int32_t n_data;
	int32_t width_data;
	
	#pragma EMX_KILLVAR B_data
	for(i2 = 0; i2 <= 3 - 1; i2 = i2 + 1) {
		for(i1 = 0; i1 <= A_size[0] - 1; i1 = i1 + 1) {
			B_data[i2][i1] = A_data[i2][i1];
		}
	}
	B_size[0] = A_size[0];
	B_size[1] = 3;
	n_data = A_size[0];
	width_data = 1;
	while (width_data < n_data) {
		for(i3 = 0; i3 <= (n_data - 1) / (2*width_data) + 1 - 1; i3 = i3 + 1) {
			iLeft_data = i3 * (2*width_data);
			iRight_data = EMX_min_i32_i32_i32(i3 * (2*width_data) + width_data, n_data);
			iEnd_data = EMX_min_i32_i32_i32(i3 * (2*width_data) + 2*width_data, n_data);
			i_data = i3 * (2*width_data);
			j_data = iRight_data;
			for(i4 = 0; i4 <= iEnd_data - 1 - i3 * (2*width_data) + 1 - 1; i4 = i4 + 1) {
				comparerow_less_4(&call1_data, A_data, A_size, j_data, i_data);
				cmp_data = (i_data < iRight_data) && ((j_data >= iEnd_data) || ((int64_t)!call1_data ^ 1));
				if (cmp_data) {
					for(i5 = 0; i5 <= 3 - 1; i5 = i5 + 1) {
						B_data[i5][i4 + iLeft_data] = A_data[i5][i_data];
					}
					B_size[0] = EMX_max_i32_i32_i32(B_size[0], i4 + iLeft_data + 1);
					B_size[1] = 3;
					i_data = i_data + 1;
				} else {
					for(i6 = 0; i6 <= 3 - 1; i6 = i6 + 1) {
						B_data[i6][i4 + iLeft_data] = A_data[i6][j_data];
					}
					B_size[0] = EMX_max_i32_i32_i32(B_size[0], i4 + iLeft_data + 1);
					B_size[1] = 3;
					j_data = j_data + 1;
				}
			}
		}
		#pragma EMX_KILLVAR A_data
		for(i8 = 0; i8 <= 3 - 1; i8 = i8 + 1) {
			for(i7 = 0; i7 <= B_size[0] - 1; i7 = i7 + 1) {
				A_data[i8][i7] = B_data[i8][i7];
			}
		}
		A_size[0] = B_size[0];
		A_size[1] = 3;
		width_data = width_data*2;
	}
}

void emx_sortrows_merge_bottomup_4(int32_t B_data[3][5000], int32_t B_size[2], int32_t A_data[3][5000], int32_t A_size[2]) {
	bool call1_data;
	bool cmp_data;
	int32_t iEnd_data;
	int32_t iLeft_data;
	int32_t iRight_data;
	int32_t i_data;
	int32_t i1;
	int32_t i2;
	int32_t i3;
	int32_t i4;
	int32_t i5;
	int32_t i6;
	int32_t i7;
	int32_t i8;
	int32_t j_data;
	int32_t n_data;
	int32_t width_data;
	
	#pragma EMX_KILLVAR B_data
	for(i2 = 0; i2 <= 3 - 1; i2 = i2 + 1) {
		for(i1 = 0; i1 <= A_size[0] - 1; i1 = i1 + 1) {
			B_data[i2][i1] = A_data[i2][i1];
		}
	}
	B_size[0] = A_size[0];
	B_size[1] = 3;
	n_data = A_size[0];
	width_data = 1;
	while (width_data < n_data) {
		for(i3 = 0; i3 <= (n_data - 1) / (2*width_data) + 1 - 1; i3 = i3 + 1) {
			iLeft_data = i3 * (2*width_data);
			iRight_data = EMX_min_i32_i32_i32(i3 * (2*width_data) + width_data, n_data);
			iEnd_data = EMX_min_i32_i32_i32(i3 * (2*width_data) + 2*width_data, n_data);
			i_data = i3 * (2*width_data);
			j_data = iRight_data;
			for(i4 = 0; i4 <= iEnd_data - 1 - i3 * (2*width_data) + 1 - 1; i4 = i4 + 1) {
				comparerow_less_1(&call1_data, A_data, A_size, j_data, i_data);
				cmp_data = (i_data < iRight_data) && ((j_data >= iEnd_data) || ((int64_t)!call1_data ^ 1));
				if (cmp_data) {
					for(i5 = 0; i5 <= 3 - 1; i5 = i5 + 1) {
						B_data[i5][i4 + iLeft_data] = A_data[i5][i_data];
					}
					B_size[0] = EMX_max_i32_i32_i32(B_size[0], i4 + iLeft_data + 1);
					B_size[1] = 3;
					i_data = i_data + 1;
				} else {
					for(i6 = 0; i6 <= 3 - 1; i6 = i6 + 1) {
						B_data[i6][i4 + iLeft_data] = A_data[i6][j_data];
					}
					B_size[0] = EMX_max_i32_i32_i32(B_size[0], i4 + iLeft_data + 1);
					B_size[1] = 3;
					j_data = j_data + 1;
				}
			}
		}
		#pragma EMX_KILLVAR A_data
		for(i8 = 0; i8 <= 3 - 1; i8 = i8 + 1) {
			for(i7 = 0; i7 <= B_size[0] - 1; i7 = i7 + 1) {
				A_data[i8][i7] = B_data[i8][i7];
			}
		}
		A_size[0] = B_size[0];
		A_size[1] = 3;
		width_data = width_data*2;
	}
}

void gsort_s_1(int32_t B_data[3][5000], int32_t B_size[2], int32_t A_data[3][5000], int32_t A_size[2]) {
	static int32_t call1_data[3][5000];
	int32_t call1_size[2] = {0};
	int32_t i1;
	int32_t i2;
	
	memset(call1_data, 0.0, sizeof (int32_t[3][5000]));
	#pragma EMX_KILLVAR call1_data
	for(i2 = 0; i2 <= 3 - 1; i2 = i2 + 1) {
		for(i1 = 0; i1 <= A_size[0] - 1; i1 = i1 + 1) {
			call1_data[i2][i1] = A_data[i2][i1];
		}
	}
	call1_size[0] = A_size[0];
	call1_size[1] = 3;
	emx_sortrows_merge_bottomup_2(B_data, B_size, call1_data, call1_size);
}

void gsort_s_2(int32_t B_data[3][5000], int32_t B_size[2], int32_t A_data[3][5000], int32_t A_size[2]) {
	static int32_t call1_data[3][5000];
	int32_t call1_size[2] = {0};
	int32_t i1;
	int32_t i2;
	
	memset(call1_data, 0.0, sizeof (int32_t[3][5000]));
	#pragma EMX_KILLVAR call1_data
	for(i2 = 0; i2 <= 3 - 1; i2 = i2 + 1) {
		for(i1 = 0; i1 <= A_size[0] - 1; i1 = i1 + 1) {
			call1_data[i2][i1] = A_data[i2][i1];
		}
	}
	call1_size[0] = A_size[0];
	call1_size[1] = 3;
	emx_sortrows_merge_bottomup_3(B_data, B_size, call1_data, call1_size);
}

void gsort_s_3(int32_t B_data[3][5000], int32_t B_size[2], int32_t A_data[3][5000], int32_t A_size[2]) {
	static int32_t call1_data[3][5000];
	int32_t call1_size[2] = {0};
	int32_t i1;
	int32_t i2;
	
	memset(call1_data, 0.0, sizeof (int32_t[3][5000]));
	#pragma EMX_KILLVAR call1_data
	for(i2 = 0; i2 <= 3 - 1; i2 = i2 + 1) {
		for(i1 = 0; i1 <= A_size[0] - 1; i1 = i1 + 1) {
			call1_data[i2][i1] = A_data[i2][i1];
		}
	}
	call1_size[0] = A_size[0];
	call1_size[1] = 3;
	emx_sortrows_merge_bottomup_4(B_data, B_size, call1_data, call1_size);
}

void gsort_s_4(int32_t B_data[3][5000], int32_t B_size[2], int32_t A_data[3][5000], int32_t A_size[2]) {
	static int32_t call1_data[3][5000];
	int32_t call1_size[2] = {0};
	int32_t i1;
	int32_t i2;
	
	memset(call1_data, 0.0, sizeof (int32_t[3][5000]));
	#pragma EMX_KILLVAR call1_data
	for(i2 = 0; i2 <= 3 - 1; i2 = i2 + 1) {
		for(i1 = 0; i1 <= A_size[0] - 1; i1 = i1 + 1) {
			call1_data[i2][i1] = A_data[i2][i1];
		}
	}
	call1_size[0] = A_size[0];
	call1_size[1] = 3;
	emx_sortrows_merge_bottomup_1(B_data, B_size, call1_data, call1_size);
}

void gauss_filter_3_3_1(int32_t filtered_data[320][240], int16_t array_data[320][240]) {
	int32_t i1;
	int32_t i2;
	int32_t i3;
	int32_t i4;
	
	#pragma EMX_KILLVAR filtered_data
	for(i2 = 0; i2 <= 320 - 1; i2 = i2 + 1) {
		for(i1 = 0; i1 <= 240 - 1; i1 = i1 + 1) {
			filtered_data[i2][i1] = 0;
		}
	}
	for(i3 = 0; i3 <= 238 - 1; i3 = i3 + 1) {
		for(i4 = 0; i4 <= 318 - 1; i4 = i4 + 1) {
			filtered_data[i4 + 1][i3 + 1] = 4 * (int32_t)array_data[i4 + 1][i3 + 1] + 2 * ((int32_t)array_data[i4][i3 + 1] + (int32_t)array_data[i4 + 2][i3 + 1] + (int32_t)array_data[i4 + 1][i3 + 2] + (int32_t)array_data[i4 + 1][i3]) + (int32_t)array_data[i4 + 2][i3 + 2] + (int32_t)array_data[i4 + 2][i3] + (int32_t)array_data[i4][i3 + 2] + (int32_t)array_data[i4][i3];
			filtered_data[i4 + 1][i3 + 1] = filtered_data[i4 + 1][i3 + 1] / 16;
		}
	}
}

void gauss_filter_3_3_2(int32_t filtered_data[320][240], int16_t array_data[320][240]) {
	int32_t i1;
	int32_t i2;
	int32_t i3;
	int32_t i4;
	
	#pragma EMX_KILLVAR filtered_data
	for(i2 = 0; i2 <= 320 - 1; i2 = i2 + 1) {
		for(i1 = 0; i1 <= 240 - 1; i1 = i1 + 1) {
			filtered_data[i2][i1] = 0;
		}
	}
	for(i3 = 0; i3 <= 238 - 1; i3 = i3 + 1) {
		for(i4 = 0; i4 <= 318 - 1; i4 = i4 + 1) {
			filtered_data[i4 + 1][i3 + 1] = 4 * (int32_t)array_data[i4 + 1][i3 + 1] + 2 * ((int32_t)array_data[i4][i3 + 1] + (int32_t)array_data[i4 + 2][i3 + 1] + (int32_t)array_data[i4 + 1][i3 + 2] + (int32_t)array_data[i4 + 1][i3]) + (int32_t)array_data[i4 + 2][i3 + 2] + (int32_t)array_data[i4 + 2][i3] + (int32_t)array_data[i4][i3 + 2] + (int32_t)array_data[i4][i3];
			filtered_data[i4 + 1][i3 + 1] = filtered_data[i4 + 1][i3 + 1] / 16;
		}
	}
}

void gauss_filter_3_3_3(int32_t filtered_data[320][240], int16_t array_data[320][240]) {
	int32_t i1;
	int32_t i2;
	int32_t i3;
	int32_t i4;
	
	#pragma EMX_KILLVAR filtered_data
	for(i2 = 0; i2 <= 320 - 1; i2 = i2 + 1) {
		for(i1 = 0; i1 <= 240 - 1; i1 = i1 + 1) {
			filtered_data[i2][i1] = 0;
		}
	}
	for(i3 = 0; i3 <= 238 - 1; i3 = i3 + 1) {
		for(i4 = 0; i4 <= 318 - 1; i4 = i4 + 1) {
			filtered_data[i4 + 1][i3 + 1] = 4 * (int32_t)array_data[i4 + 1][i3 + 1] + 2 * ((int32_t)array_data[i4][i3 + 1] + (int32_t)array_data[i4 + 2][i3 + 1] + (int32_t)array_data[i4 + 1][i3 + 2] + (int32_t)array_data[i4 + 1][i3]) + (int32_t)array_data[i4 + 2][i3 + 2] + (int32_t)array_data[i4 + 2][i3] + (int32_t)array_data[i4][i3 + 2] + (int32_t)array_data[i4][i3];
			filtered_data[i4 + 1][i3 + 1] = filtered_data[i4 + 1][i3 + 1] / 16;
		}
	}
}

void gauss_filter_3_3_4(int32_t filtered_data[320][240], int16_t array_data[320][240]) {
	int32_t i1;
	int32_t i2;
	int32_t i3;
	int32_t i4;
	
	#pragma EMX_KILLVAR filtered_data
	for(i2 = 0; i2 <= 320 - 1; i2 = i2 + 1) {
		for(i1 = 0; i1 <= 240 - 1; i1 = i1 + 1) {
			filtered_data[i2][i1] = 0;
		}
	}
	for(i3 = 0; i3 <= 238 - 1; i3 = i3 + 1) {
		for(i4 = 0; i4 <= 318 - 1; i4 = i4 + 1) {
			filtered_data[i4 + 1][i3 + 1] = 4 * (int32_t)array_data[i4 + 1][i3 + 1] + 2 * ((int32_t)array_data[i4][i3 + 1] + (int32_t)array_data[i4 + 2][i3 + 1] + (int32_t)array_data[i4 + 1][i3 + 2] + (int32_t)array_data[i4 + 1][i3]) + (int32_t)array_data[i4 + 2][i3 + 2] + (int32_t)array_data[i4 + 2][i3] + (int32_t)array_data[i4][i3 + 2] + (int32_t)array_data[i4][i3];
			filtered_data[i4 + 1][i3 + 1] = filtered_data[i4 + 1][i3 + 1] / 16;
		}
	}
}

void calc_gradient_1(uint16_t goodkeypoints_data[2][1000], int32_t goodkeypoints_size[2], int16_t patch_data[320][240]) {
	static double H_data[320][240];
	double Hval_data;
	int32_t Ix_data;
	int32_t Iy_data;
	static int32_t call1_data[3][5000];
	int32_t call1_size[2] = {0};
	double detQ_data;
	static int32_t gf_data[320][240];
	double idx_data;
	int32_t i5;
	int32_t i6;
	int32_t i7;
	int32_t i8;
	int32_t i9;
	int32_t i10;
	int32_t i11;
	int32_t i12;
	int32_t i14;
	int32_t i15;
	int32_t i16;
	int32_t i17;
	int32_t keyp_data[1][1];
	static int32_t keypoints_data[3][5000];
	int32_t keypoints_size[2] = {0};
	static uint16_t kppatch_data[3][3][100];
	int32_t nkeyp_data;
	static int32_t sarr_data[3][5000];
	int32_t sarr_size[2] = {0};
	int32_t sumIxIy_data;
	int32_t sumIx2_data;
	int32_t sumIy2_data;
	uint16_t temp_data[3][3];
	double traceQ_data;
	
	memset(keypoints_data, 0.0, sizeof (int32_t[3][5000]));
	memset(sarr_data, 0.0, sizeof (int32_t[3][5000]));
	memset(call1_data, 0.0, sizeof (int32_t[3][5000]));
	#pragma EMX_KILLVAR keypoints_data
	keypoints_size[0] = 0;
	keypoints_size[1] = 3;
	#pragma EMX_KILLVAR goodkeypoints_data
	goodkeypoints_size[0] = 0;
	goodkeypoints_size[1] = 0;
	#pragma EMX_KILLVAR H_data
	for(i6 = 0; i6 <= 320 - 1; i6 = i6 + 1) {
		for(i5 = 0; i5 <= 240 - 1; i5 = i5 + 1) {
			H_data[i6][i5] = 0;
		}
	}
	#pragma test
	{
		gauss_filter_3_3_2(gf_data, patch_data);
		for(i7 = 0; i7 <= 236 - 1; i7 = i7 + 1) {
			for(i8 = 0; i8 <= 316 - 1; i8 = i8 + 1) {
				#pragma HLS PIPELINE II=13
				sumIx2_data = 0;
				sumIy2_data = 0;
				sumIxIy_data = 0;
				for(i9 = 0; i9 <= 3 - 1; i9 = i9 + 1) {
					for(i10 = 0; i10 <= 3 - 1; i10 = i10 + 1) {
						Ix_data = gf_data[i8 + 4 + (i10 - 1) - 1][i7 + 2 + (i9 - 1) - 1] - gf_data[i8 + 2 + (i10 - 1) - 1][i7 + 2 + (i9 - 1) - 1] + 2 * gf_data[i8 + 4 + (i10 - 1) - 1][i7 + 3 + (i9 - 1) - 1] - 2 * gf_data[i8 + 2 + (i10 - 1) - 1][i7 + 3 + (i9 - 1) - 1] + gf_data[i8 + 4 + (i10 - 1) - 1][i7 + 4 + (i9 - 1) - 1] - gf_data[i8 + 2 + (i10 - 1) - 1][i7 + 4 + (i9 - 1) - 1];
						Iy_data = gf_data[i8 + 2 + (i10 - 1) - 1][i7 + 4 + (i9 - 1) - 1] - gf_data[i8 + 2 + (i10 - 1) - 1][i7 + 2 + (i9 - 1) - 1] + 2 * gf_data[i8 + 3 + (i10 - 1) - 1][i7 + 4 + (i9 - 1) - 1] - 2 * gf_data[i8 + 3 + (i10 - 1) - 1][i7 + 2 + (i9 - 1) - 1] + gf_data[i8 + 4 + (i10 - 1) - 1][i7 + 4 + (i9 - 1) - 1] - gf_data[i8 + 4 + (i10 - 1) - 1][i7 + 2 + (i9 - 1) - 1];
						Ix_data = Ix_data / 8;
						Iy_data = Iy_data / 8;
						sumIx2_data = sumIx2_data + Ix_data*Ix_data;
						sumIy2_data = sumIy2_data + Iy_data*Iy_data;
						sumIxIy_data = sumIxIy_data + Ix_data*Iy_data;
					}
				}
				detQ_data = (double)(sumIx2_data*sumIy2_data) - (double)(sumIxIy_data*sumIxIy_data);
				traceQ_data = (double)(sumIx2_data + sumIy2_data);
				H_data[i8 + 2][i7 + 2] = detQ_data - 0.04 * (traceQ_data*traceQ_data);
			}
		}
		for(i11 = 0; i11 <= 234 - 1; i11 = i11 + 1) {
			for(i12 = 0; i12 <= 314 - 1; i12 = i12 + 1) {
				#pragma HLS PIPELINE II=5
				if ((int32_t)H_data[i12 + 3][i11 + 3] > 10000) {
					if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 2][i11 + 2]) {
						if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 3][i11 + 2]) {
							if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 4][i11 + 2]) {
								if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 2][i11 + 3]) {
									if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 4][i11 + 3]) {
										if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 2][i11 + 4]) {
											if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 3][i11 + 4]) {
												if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 4][i11 + 4]) {
													Hval_data = H_data[i12 + 3][i11 + 3];
													if (keypoints_size[0] < 5000) {
														idx_data = (double)keypoints_size[0];
														keypoints_data[0][(int32_t)(idx_data + 1.0) - 1] = (int32_t)Hval_data;
														keypoints_data[1][(int32_t)(idx_data + 1.0) - 1] = i11 + 4;
														keypoints_data[2][(int32_t)(idx_data + 1.0) - 1] = i12 + 4;
														keypoints_size[0] = EMX_max_i32_i32_i32(keypoints_size[0], (int32_t)(idx_data + 1.0));
														keypoints_size[1] = 3;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	nkeyp_data = keypoints_size[0];
	if (nkeyp_data > 0) {
		#pragma EMX_KILLVAR sarr_data
		gsort_s_2(call1_data, call1_size, keypoints_data, keypoints_size);
		for(i15 = 0; i15 <= 3 - 1; i15 = i15 + 1) {
			for(i14 = 0; i14 <= call1_size[0] - 1; i14 = i14 + 1) {
				sarr_data[i15][i14] = call1_data[i15][i14];
			}
		}
		sarr_size[0] = call1_size[0];
		sarr_size[1] = 3;
		if (nkeyp_data > 250) {
			nkeyp_data = 250;
		}
		#pragma EMX_KILLVAR goodkeypoints_data
		for(i17 = 0; i17 <= 2 - 1; i17 = i17 + 1) {
			for(i16 = 0; i16 <= nkeyp_data - 1; i16 = i16 + 1) {
				goodkeypoints_data[i17][i16] = (uint16_t)sarr_data[i17 + 1][i16];
			}
		}
		goodkeypoints_size[0] = nkeyp_data;
		goodkeypoints_size[1] = 2;
	}
}

void calc_gradient_2(uint16_t goodkeypoints_data[2][1000], int32_t goodkeypoints_size[2], int16_t patch_data[320][240]) {
	static double H_data[320][240];
	double Hval_data;
	int32_t Ix_data;
	int32_t Iy_data;
	static int32_t call1_data[3][5000];
	int32_t call1_size[2] = {0};
	double detQ_data;
	static int32_t gf_data[320][240];
	double idx_data;
	int32_t i5;
	int32_t i6;
	int32_t i7;
	int32_t i8;
	int32_t i9;
	int32_t i10;
	int32_t i11;
	int32_t i12;
	int32_t i14;
	int32_t i15;
	int32_t i16;
	int32_t i17;
	int32_t keyp_data[1][1];
	static int32_t keypoints_data[3][5000];
	int32_t keypoints_size[2] = {0};
	static uint16_t kppatch_data[3][3][100];
	int32_t nkeyp_data;
	static int32_t sarr_data[3][5000];
	int32_t sarr_size[2] = {0};
	int32_t sumIxIy_data;
	int32_t sumIx2_data;
	int32_t sumIy2_data;
	uint16_t temp_data[3][3];
	double traceQ_data;
	
	memset(keypoints_data, 0.0, sizeof (int32_t[3][5000]));
	memset(sarr_data, 0.0, sizeof (int32_t[3][5000]));
	memset(call1_data, 0.0, sizeof (int32_t[3][5000]));
	#pragma EMX_KILLVAR keypoints_data
	keypoints_size[0] = 0;
	keypoints_size[1] = 3;
	#pragma EMX_KILLVAR goodkeypoints_data
	goodkeypoints_size[0] = 0;
	goodkeypoints_size[1] = 0;
	#pragma EMX_KILLVAR H_data
	for(i6 = 0; i6 <= 320 - 1; i6 = i6 + 1) {
		for(i5 = 0; i5 <= 240 - 1; i5 = i5 + 1) {
			H_data[i6][i5] = 0;
		}
	}
	#pragma test
	{
		gauss_filter_3_3_3(gf_data, patch_data);
		for(i7 = 0; i7 <= 236 - 1; i7 = i7 + 1) {
			for(i8 = 0; i8 <= 316 - 1; i8 = i8 + 1) {
				#pragma HLS PIPELINE II=13
				sumIx2_data = 0;
				sumIy2_data = 0;
				sumIxIy_data = 0;
				for(i9 = 0; i9 <= 3 - 1; i9 = i9 + 1) {
					for(i10 = 0; i10 <= 3 - 1; i10 = i10 + 1) {
						Ix_data = gf_data[i8 + 4 + (i10 - 1) - 1][i7 + 2 + (i9 - 1) - 1] - gf_data[i8 + 2 + (i10 - 1) - 1][i7 + 2 + (i9 - 1) - 1] + 2 * gf_data[i8 + 4 + (i10 - 1) - 1][i7 + 3 + (i9 - 1) - 1] - 2 * gf_data[i8 + 2 + (i10 - 1) - 1][i7 + 3 + (i9 - 1) - 1] + gf_data[i8 + 4 + (i10 - 1) - 1][i7 + 4 + (i9 - 1) - 1] - gf_data[i8 + 2 + (i10 - 1) - 1][i7 + 4 + (i9 - 1) - 1];
						Iy_data = gf_data[i8 + 2 + (i10 - 1) - 1][i7 + 4 + (i9 - 1) - 1] - gf_data[i8 + 2 + (i10 - 1) - 1][i7 + 2 + (i9 - 1) - 1] + 2 * gf_data[i8 + 3 + (i10 - 1) - 1][i7 + 4 + (i9 - 1) - 1] - 2 * gf_data[i8 + 3 + (i10 - 1) - 1][i7 + 2 + (i9 - 1) - 1] + gf_data[i8 + 4 + (i10 - 1) - 1][i7 + 4 + (i9 - 1) - 1] - gf_data[i8 + 4 + (i10 - 1) - 1][i7 + 2 + (i9 - 1) - 1];
						Ix_data = Ix_data / 8;
						Iy_data = Iy_data / 8;
						sumIx2_data = sumIx2_data + Ix_data*Ix_data;
						sumIy2_data = sumIy2_data + Iy_data*Iy_data;
						sumIxIy_data = sumIxIy_data + Ix_data*Iy_data;
					}
				}
				detQ_data = (double)(sumIx2_data*sumIy2_data) - (double)(sumIxIy_data*sumIxIy_data);
				traceQ_data = (double)(sumIx2_data + sumIy2_data);
				H_data[i8 + 2][i7 + 2] = detQ_data - 0.04 * (traceQ_data*traceQ_data);
			}
		}
		for(i11 = 0; i11 <= 234 - 1; i11 = i11 + 1) {
			for(i12 = 0; i12 <= 314 - 1; i12 = i12 + 1) {
				#pragma HLS PIPELINE II=5
				if ((int32_t)H_data[i12 + 3][i11 + 3] > 10000) {
					if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 2][i11 + 2]) {
						if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 3][i11 + 2]) {
							if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 4][i11 + 2]) {
								if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 2][i11 + 3]) {
									if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 4][i11 + 3]) {
										if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 2][i11 + 4]) {
											if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 3][i11 + 4]) {
												if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 4][i11 + 4]) {
													Hval_data = H_data[i12 + 3][i11 + 3];
													if (keypoints_size[0] < 5000) {
														idx_data = (double)keypoints_size[0];
														keypoints_data[0][(int32_t)(idx_data + 1.0) - 1] = (int32_t)Hval_data;
														keypoints_data[1][(int32_t)(idx_data + 1.0) - 1] = i11 + 4;
														keypoints_data[2][(int32_t)(idx_data + 1.0) - 1] = i12 + 4;
														keypoints_size[0] = EMX_max_i32_i32_i32(keypoints_size[0], (int32_t)(idx_data + 1.0));
														keypoints_size[1] = 3;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	nkeyp_data = keypoints_size[0];
	if (nkeyp_data > 0) {
		#pragma EMX_KILLVAR sarr_data
		gsort_s_3(call1_data, call1_size, keypoints_data, keypoints_size);
		for(i15 = 0; i15 <= 3 - 1; i15 = i15 + 1) {
			for(i14 = 0; i14 <= call1_size[0] - 1; i14 = i14 + 1) {
				sarr_data[i15][i14] = call1_data[i15][i14];
			}
		}
		sarr_size[0] = call1_size[0];
		sarr_size[1] = 3;
		if (nkeyp_data > 250) {
			nkeyp_data = 250;
		}
		#pragma EMX_KILLVAR goodkeypoints_data
		for(i17 = 0; i17 <= 2 - 1; i17 = i17 + 1) {
			for(i16 = 0; i16 <= nkeyp_data - 1; i16 = i16 + 1) {
				goodkeypoints_data[i17][i16] = (uint16_t)sarr_data[i17 + 1][i16];
			}
		}
		goodkeypoints_size[0] = nkeyp_data;
		goodkeypoints_size[1] = 2;
	}
}

void calc_gradient_3(uint16_t goodkeypoints_data[2][1000], int32_t goodkeypoints_size[2], int16_t patch_data[320][240]) {
	static double H_data[320][240];
	double Hval_data;
	int32_t Ix_data;
	int32_t Iy_data;
	static int32_t call1_data[3][5000];
	int32_t call1_size[2] = {0};
	double detQ_data;
	static int32_t gf_data[320][240];
	double idx_data;
	int32_t i5;
	int32_t i6;
	int32_t i7;
	int32_t i8;
	int32_t i9;
	int32_t i10;
	int32_t i11;
	int32_t i12;
	int32_t i14;
	int32_t i15;
	int32_t i16;
	int32_t i17;
	int32_t keyp_data[1][1];
	static int32_t keypoints_data[3][5000];
	int32_t keypoints_size[2] = {0};
	static uint16_t kppatch_data[3][3][100];
	int32_t nkeyp_data;
	static int32_t sarr_data[3][5000];
	int32_t sarr_size[2] = {0};
	int32_t sumIxIy_data;
	int32_t sumIx2_data;
	int32_t sumIy2_data;
	uint16_t temp_data[3][3];
	double traceQ_data;
	
	memset(keypoints_data, 0.0, sizeof (int32_t[3][5000]));
	memset(sarr_data, 0.0, sizeof (int32_t[3][5000]));
	memset(call1_data, 0.0, sizeof (int32_t[3][5000]));
	#pragma EMX_KILLVAR keypoints_data
	keypoints_size[0] = 0;
	keypoints_size[1] = 3;
	#pragma EMX_KILLVAR goodkeypoints_data
	goodkeypoints_size[0] = 0;
	goodkeypoints_size[1] = 0;
	#pragma EMX_KILLVAR H_data
	for(i6 = 0; i6 <= 320 - 1; i6 = i6 + 1) {
		for(i5 = 0; i5 <= 240 - 1; i5 = i5 + 1) {
			H_data[i6][i5] = 0;
		}
	}
	#pragma test
	{
		gauss_filter_3_3_4(gf_data, patch_data);
		for(i7 = 0; i7 <= 236 - 1; i7 = i7 + 1) {
			for(i8 = 0; i8 <= 316 - 1; i8 = i8 + 1) {
				#pragma HLS PIPELINE II=13
				sumIx2_data = 0;
				sumIy2_data = 0;
				sumIxIy_data = 0;
				for(i9 = 0; i9 <= 3 - 1; i9 = i9 + 1) {
					for(i10 = 0; i10 <= 3 - 1; i10 = i10 + 1) {
						Ix_data = gf_data[i8 + 4 + (i10 - 1) - 1][i7 + 2 + (i9 - 1) - 1] - gf_data[i8 + 2 + (i10 - 1) - 1][i7 + 2 + (i9 - 1) - 1] + 2 * gf_data[i8 + 4 + (i10 - 1) - 1][i7 + 3 + (i9 - 1) - 1] - 2 * gf_data[i8 + 2 + (i10 - 1) - 1][i7 + 3 + (i9 - 1) - 1] + gf_data[i8 + 4 + (i10 - 1) - 1][i7 + 4 + (i9 - 1) - 1] - gf_data[i8 + 2 + (i10 - 1) - 1][i7 + 4 + (i9 - 1) - 1];
						Iy_data = gf_data[i8 + 2 + (i10 - 1) - 1][i7 + 4 + (i9 - 1) - 1] - gf_data[i8 + 2 + (i10 - 1) - 1][i7 + 2 + (i9 - 1) - 1] + 2 * gf_data[i8 + 3 + (i10 - 1) - 1][i7 + 4 + (i9 - 1) - 1] - 2 * gf_data[i8 + 3 + (i10 - 1) - 1][i7 + 2 + (i9 - 1) - 1] + gf_data[i8 + 4 + (i10 - 1) - 1][i7 + 4 + (i9 - 1) - 1] - gf_data[i8 + 4 + (i10 - 1) - 1][i7 + 2 + (i9 - 1) - 1];
						Ix_data = Ix_data / 8;
						Iy_data = Iy_data / 8;
						sumIx2_data = sumIx2_data + Ix_data*Ix_data;
						sumIy2_data = sumIy2_data + Iy_data*Iy_data;
						sumIxIy_data = sumIxIy_data + Ix_data*Iy_data;
					}
				}
				detQ_data = (double)(sumIx2_data*sumIy2_data) - (double)(sumIxIy_data*sumIxIy_data);
				traceQ_data = (double)(sumIx2_data + sumIy2_data);
				H_data[i8 + 2][i7 + 2] = detQ_data - 0.04 * (traceQ_data*traceQ_data);
			}
		}
		for(i11 = 0; i11 <= 234 - 1; i11 = i11 + 1) {
			for(i12 = 0; i12 <= 314 - 1; i12 = i12 + 1) {
				#pragma HLS PIPELINE II=5
				if ((int32_t)H_data[i12 + 3][i11 + 3] > 10000) {
					if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 2][i11 + 2]) {
						if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 3][i11 + 2]) {
							if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 4][i11 + 2]) {
								if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 2][i11 + 3]) {
									if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 4][i11 + 3]) {
										if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 2][i11 + 4]) {
											if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 3][i11 + 4]) {
												if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 4][i11 + 4]) {
													Hval_data = H_data[i12 + 3][i11 + 3];
													if (keypoints_size[0] < 5000) {
														idx_data = (double)keypoints_size[0];
														keypoints_data[0][(int32_t)(idx_data + 1.0) - 1] = (int32_t)Hval_data;
														keypoints_data[1][(int32_t)(idx_data + 1.0) - 1] = i11 + 4;
														keypoints_data[2][(int32_t)(idx_data + 1.0) - 1] = i12 + 4;
														keypoints_size[0] = EMX_max_i32_i32_i32(keypoints_size[0], (int32_t)(idx_data + 1.0));
														keypoints_size[1] = 3;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	nkeyp_data = keypoints_size[0];
	if (nkeyp_data > 0) {
		#pragma EMX_KILLVAR sarr_data
		gsort_s_4(call1_data, call1_size, keypoints_data, keypoints_size);
		for(i15 = 0; i15 <= 3 - 1; i15 = i15 + 1) {
			for(i14 = 0; i14 <= call1_size[0] - 1; i14 = i14 + 1) {
				sarr_data[i15][i14] = call1_data[i15][i14];
			}
		}
		sarr_size[0] = call1_size[0];
		sarr_size[1] = 3;
		if (nkeyp_data > 250) {
			nkeyp_data = 250;
		}
		#pragma EMX_KILLVAR goodkeypoints_data
		for(i17 = 0; i17 <= 2 - 1; i17 = i17 + 1) {
			for(i16 = 0; i16 <= nkeyp_data - 1; i16 = i16 + 1) {
				goodkeypoints_data[i17][i16] = (uint16_t)sarr_data[i17 + 1][i16];
			}
		}
		goodkeypoints_size[0] = nkeyp_data;
		goodkeypoints_size[1] = 2;
	}
}

void calc_gradient_4(uint16_t goodkeypoints_data[2][1000], int32_t goodkeypoints_size[2], int16_t patch_data[320][240]) {
	static double H_data[320][240];
	double Hval_data;
	int32_t Ix_data;
	int32_t Iy_data;
	static int32_t call1_data[3][5000];
	int32_t call1_size[2] = {0};
	double detQ_data;
	static int32_t gf_data[320][240];
	double idx_data;
	int32_t i5;
	int32_t i6;
	int32_t i7;
	int32_t i8;
	int32_t i9;
	int32_t i10;
	int32_t i11;
	int32_t i12;
	int32_t i14;
	int32_t i15;
	int32_t i16;
	int32_t i17;
	int32_t keyp_data[1][1];
	static int32_t keypoints_data[3][5000];
	int32_t keypoints_size[2] = {0};
	static uint16_t kppatch_data[3][3][100];
	int32_t nkeyp_data;
	static int32_t sarr_data[3][5000];
	int32_t sarr_size[2] = {0};
	int32_t sumIxIy_data;
	int32_t sumIx2_data;
	int32_t sumIy2_data;
	uint16_t temp_data[3][3];
	double traceQ_data;
	
	memset(keypoints_data, 0.0, sizeof (int32_t[3][5000]));
	memset(sarr_data, 0.0, sizeof (int32_t[3][5000]));
	memset(call1_data, 0.0, sizeof (int32_t[3][5000]));
	#pragma EMX_KILLVAR keypoints_data
	keypoints_size[0] = 0;
	keypoints_size[1] = 3;
	#pragma EMX_KILLVAR goodkeypoints_data
	goodkeypoints_size[0] = 0;
	goodkeypoints_size[1] = 0;
	#pragma EMX_KILLVAR H_data
	for(i6 = 0; i6 <= 320 - 1; i6 = i6 + 1) {
		for(i5 = 0; i5 <= 240 - 1; i5 = i5 + 1) {
			H_data[i6][i5] = 0;
		}
	}
	#pragma test
	{
		gauss_filter_3_3_1(gf_data, patch_data);
		for(i7 = 0; i7 <= 236 - 1; i7 = i7 + 1) {
			for(i8 = 0; i8 <= 316 - 1; i8 = i8 + 1) {
				#pragma HLS PIPELINE II=13
				sumIx2_data = 0;
				sumIy2_data = 0;
				sumIxIy_data = 0;
				for(i9 = 0; i9 <= 3 - 1; i9 = i9 + 1) {
					for(i10 = 0; i10 <= 3 - 1; i10 = i10 + 1) {
						Ix_data = gf_data[i8 + 4 + (i10 - 1) - 1][i7 + 2 + (i9 - 1) - 1] - gf_data[i8 + 2 + (i10 - 1) - 1][i7 + 2 + (i9 - 1) - 1] + 2 * gf_data[i8 + 4 + (i10 - 1) - 1][i7 + 3 + (i9 - 1) - 1] - 2 * gf_data[i8 + 2 + (i10 - 1) - 1][i7 + 3 + (i9 - 1) - 1] + gf_data[i8 + 4 + (i10 - 1) - 1][i7 + 4 + (i9 - 1) - 1] - gf_data[i8 + 2 + (i10 - 1) - 1][i7 + 4 + (i9 - 1) - 1];
						Iy_data = gf_data[i8 + 2 + (i10 - 1) - 1][i7 + 4 + (i9 - 1) - 1] - gf_data[i8 + 2 + (i10 - 1) - 1][i7 + 2 + (i9 - 1) - 1] + 2 * gf_data[i8 + 3 + (i10 - 1) - 1][i7 + 4 + (i9 - 1) - 1] - 2 * gf_data[i8 + 3 + (i10 - 1) - 1][i7 + 2 + (i9 - 1) - 1] + gf_data[i8 + 4 + (i10 - 1) - 1][i7 + 4 + (i9 - 1) - 1] - gf_data[i8 + 4 + (i10 - 1) - 1][i7 + 2 + (i9 - 1) - 1];
						Ix_data = Ix_data / 8;
						Iy_data = Iy_data / 8;
						sumIx2_data = sumIx2_data + Ix_data*Ix_data;
						sumIy2_data = sumIy2_data + Iy_data*Iy_data;
						sumIxIy_data = sumIxIy_data + Ix_data*Iy_data;
					}
				}
				detQ_data = (double)(sumIx2_data*sumIy2_data) - (double)(sumIxIy_data*sumIxIy_data);
				traceQ_data = (double)(sumIx2_data + sumIy2_data);
				H_data[i8 + 2][i7 + 2] = detQ_data - 0.04 * (traceQ_data*traceQ_data);
			}
		}
		for(i11 = 0; i11 <= 234 - 1; i11 = i11 + 1) {
			for(i12 = 0; i12 <= 314 - 1; i12 = i12 + 1) {
				#pragma HLS PIPELINE II=5
				if ((int32_t)H_data[i12 + 3][i11 + 3] > 10000) {
					if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 2][i11 + 2]) {
						if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 3][i11 + 2]) {
							if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 4][i11 + 2]) {
								if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 2][i11 + 3]) {
									if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 4][i11 + 3]) {
										if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 2][i11 + 4]) {
											if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 3][i11 + 4]) {
												if (H_data[i12 + 3][i11 + 3] > H_data[i12 + 4][i11 + 4]) {
													Hval_data = H_data[i12 + 3][i11 + 3];
													if (keypoints_size[0] < 5000) {
														idx_data = (double)keypoints_size[0];
														keypoints_data[0][(int32_t)(idx_data + 1.0) - 1] = (int32_t)Hval_data;
														keypoints_data[1][(int32_t)(idx_data + 1.0) - 1] = i11 + 4;
														keypoints_data[2][(int32_t)(idx_data + 1.0) - 1] = i12 + 4;
														keypoints_size[0] = EMX_max_i32_i32_i32(keypoints_size[0], (int32_t)(idx_data + 1.0));
														keypoints_size[1] = 3;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	nkeyp_data = keypoints_size[0];
	if (nkeyp_data > 0) {
		#pragma EMX_KILLVAR sarr_data
		gsort_s_1(call1_data, call1_size, keypoints_data, keypoints_size);
		for(i15 = 0; i15 <= 3 - 1; i15 = i15 + 1) {
			for(i14 = 0; i14 <= call1_size[0] - 1; i14 = i14 + 1) {
				sarr_data[i15][i14] = call1_data[i15][i14];
			}
		}
		sarr_size[0] = call1_size[0];
		sarr_size[1] = 3;
		if (nkeyp_data > 250) {
			nkeyp_data = 250;
		}
		#pragma EMX_KILLVAR goodkeypoints_data
		for(i17 = 0; i17 <= 2 - 1; i17 = i17 + 1) {
			for(i16 = 0; i16 <= nkeyp_data - 1; i16 = i16 + 1) {
				goodkeypoints_data[i17][i16] = (uint16_t)sarr_data[i17 + 1][i16];
			}
		}
		goodkeypoints_size[0] = nkeyp_data;
		goodkeypoints_size[1] = 2;
	}
}

void rgb2gray(uint8_t grey_data[640][480], uint8_t rgb_data[4][640][480]) {
	int32_t i1;
	int32_t i2;
	
	#pragma EMX_KILLVAR grey_data
	for(i2 = 0; i2 <= 640 - 1; i2 = i2 + 1) {
		for(i1 = 0; i1 <= 480 - 1; i1 = i1 + 1) {
			grey_data[i2][i1] = (uint8_t)EMX_divround_u32_u32_u32((uint32_t)rgb_data[0][i2][i1]*2126 + (uint32_t)rgb_data[1][i2][i1]*7152 + (uint32_t)rgb_data[2][i2][i1]*722, 10000);
		}
	}
}

void harris_image() {
	static int16_t Orig_I_data[640][480];
	static uint8_t Orig_I_gray_data[640][480];
	static uint8_t Orig_I_rgb_data[4][640][480];
	static uint8_t Orig_out_data[4][640][480];
	static int16_t call1_data[320][240];
	static int16_t call2[320][240];
	static int16_t call3[320][240];
	static int16_t call4[320][240];
	int32_t i1;
	int32_t i2;
	int32_t i3;
	int32_t i8;
	int32_t i9;
	int32_t i10;
	int32_t i11;
	int32_t i12;
	int32_t i13;
	int32_t i14;
	int32_t i15;
	int32_t i16;
	int32_t i17;
	int32_t i18;
	int32_t i19;
	int32_t i20;
	int32_t i21;
	int32_t i22;
	int32_t i23;
	int32_t i24;
	int32_t i27;
	int32_t i28;
	int32_t i29;
	int32_t i30;
	int32_t i31;
	int32_t i34;
	int32_t i35;
	int32_t i36;
	int32_t i37;
	int32_t i38;
	int32_t i40;
	int32_t i41;
	int32_t i42;
	int32_t i43;
	int32_t i44;
	static uint16_t keypoints_data[2][1000];
	int32_t keypoints_size[2] = {0};
	static uint16_t keypoints2_data[2][1000];
	static uint16_t keypoints2_data2[2][1000];
	static uint16_t keypoints2_data3[2][1000];
	static uint16_t keypoints2_data22[2][1000];
	int32_t keypoints2_size[2] = {0};
	int32_t keypoints2_size2[2] = {0};
	int32_t keypoints2_size3[2] = {0};
	int32_t keypoints2_size22[2] = {0};
	double numkp_data;
	int32_t tile_num_data[2];
	
	memset(keypoints_data, 0.0, sizeof (uint16_t[2][1000]));
	memset(keypoints2_data, 0.0, sizeof (uint16_t[2][1000]));
	memset(keypoints2_data2, 0.0, sizeof (uint16_t[2][1000]));
	memset(keypoints2_data3, 0.0, sizeof (uint16_t[2][1000]));
	memset(keypoints2_data22, 0.0, sizeof (uint16_t[2][1000]));
//	interface_indata(Orig_I_rgb_data);
	rgb2gray(Orig_I_gray_data, Orig_I_rgb_data);
	#pragma EMX_KILLVAR Orig_I_data
	for(i2 = 0; i2 <= 640 - 1; i2 = i2 + 1) {
		for(i1 = 0; i1 <= 480 - 1; i1 = i1 + 1) {
			Orig_I_data[i2][i1] = (int16_t)Orig_I_gray_data[i2][i1];
		}
	}
	#pragma EMX_KILLVAR tile_num_data
	for(i3 = 0; i3 <= 2 - 1; i3 = i3 + 1) {
		tile_num_data[i3] = 2;
	}
	#pragma EMX_DECISION 2 "main.harris_image.tilex" "Number of tiles (X)" "int" "select" 1 "#cpu"
	#pragma EMX_DECISION 2 "main.harris_image.tiley" "Number of tiles (Y)" "int" "select" 1 "#cpu"
	#pragma EMX_KILLVAR keypoints_data
	keypoints_size[0] = 0;
	keypoints_size[1] = 0;
	#pragma keep
	{
		#pragma HLS PIPELINE II=13
		#pragma EMX_KILLVAR call1_data
		for(i9 = 0; i9 <= 320 - 1; i9 = i9 + 1) {
			for(i8 = 0; i8 <= 240 - 1; i8 = i8 + 1) {
				call1_data[i9][i8] = Orig_I_data[i9][i8];
			}
		}
		calc_gradient_2(keypoints2_data, keypoints2_size, call1_data);
		for(i10 = 0; i10 <= keypoints2_size[0] - 1; i10 = i10 + 1) {
			keypoints2_data[0][i10] = (uint16_t)(uint32_t)keypoints2_data[0][i10] + 1 - 1;
			keypoints2_size[0] = EMX_max_i32_i32_i32(keypoints2_size[0], i10 + 1);
			keypoints2_size[1] = 2;
			keypoints2_data[1][i10] = (uint16_t)(uint32_t)keypoints2_data[1][i10] + 1 - 1;
			keypoints2_size[0] = EMX_max_i32_i32_i32(keypoints2_size[0], i10 + 1);
			keypoints2_size[1] = 2;
		}
		for(i12 = 0; i12 <= 2 - 1; i12 = i12 + 1) {
			for(i11 = keypoints_size[0]; i11 <= keypoints_size[0] + keypoints2_size[0] - 1; i11 = i11 + 1) {
				keypoints_data[i12][i11] = keypoints2_data[i12][i11 - keypoints_size[0]];
			}
		}
		keypoints_size[0] = keypoints_size[0] + keypoints2_size[0];
		keypoints_size[1] = 2;
		#pragma HLS PIPELINE II=13
		#pragma EMX_KILLVAR call2
		for(i28 = 0; i28 <= 320 - 1; i28 = i28 + 1) {
			for(i27 = 0; i27 <= 240 - 1; i27 = i27 + 1) {
				call2[i28][i27] = Orig_I_data[i28 + 320][i27];
			}
		}
		calc_gradient_3(keypoints2_data2, keypoints2_size2, call2);
		for(i29 = 0; i29 <= keypoints2_size2[0] - 1; i29 = i29 + 1) {
			keypoints2_data2[0][i29] = (uint16_t)(uint32_t)keypoints2_data2[0][i29] + 1 - 1;
			keypoints2_size2[0] = EMX_max_i32_i32_i32(keypoints2_size2[0], i29 + 1);
			keypoints2_size2[1] = 2;
			keypoints2_data2[1][i29] = (uint16_t)(uint32_t)keypoints2_data2[1][i29] + 321 - 1;
			keypoints2_size2[0] = EMX_max_i32_i32_i32(keypoints2_size2[0], i29 + 1);
			keypoints2_size2[1] = 2;
		}
		for(i31 = 0; i31 <= 2 - 1; i31 = i31 + 1) {
			for(i30 = keypoints_size[0]; i30 <= keypoints_size[0] + keypoints2_size2[0] - 1; i30 = i30 + 1) {
				keypoints_data[i31][i30] = keypoints2_data2[i31][i30 - keypoints_size[0]];
			}
		}
		keypoints_size[0] = keypoints_size[0] + keypoints2_size2[0];
		keypoints_size[1] = 2;
		#pragma HLS PIPELINE II=13
		#pragma EMX_KILLVAR call4
		for(i35 = 0; i35 <= 320 - 1; i35 = i35 + 1) {
			for(i34 = 0; i34 <= 240 - 1; i34 = i34 + 1) {
				call4[i35][i34] = Orig_I_data[i35][i34 + 240];
			}
		}
		calc_gradient_4(keypoints2_data3, keypoints2_size3, call4);
		for(i36 = 0; i36 <= keypoints2_size3[0] - 1; i36 = i36 + 1) {
			keypoints2_data3[0][i36] = (uint16_t)(uint32_t)keypoints2_data3[0][i36] + 241 - 1;
			keypoints2_size3[0] = EMX_max_i32_i32_i32(keypoints2_size3[0], i36 + 1);
			keypoints2_size3[1] = 2;
			keypoints2_data3[1][i36] = (uint16_t)(uint32_t)keypoints2_data3[1][i36] + 1 - 1;
			keypoints2_size3[0] = EMX_max_i32_i32_i32(keypoints2_size3[0], i36 + 1);
			keypoints2_size3[1] = 2;
		}
		for(i38 = 0; i38 <= 2 - 1; i38 = i38 + 1) {
			for(i37 = keypoints_size[0]; i37 <= keypoints_size[0] + keypoints2_size3[0] - 1; i37 = i37 + 1) {
				keypoints_data[i38][i37] = keypoints2_data3[i38][i37 - keypoints_size[0]];
			}
		}
		keypoints_size[0] = keypoints_size[0] + keypoints2_size3[0];
		keypoints_size[1] = 2;
		#pragma HLS PIPELINE II=13
		#pragma EMX_KILLVAR call3
		for(i41 = 0; i41 <= 320 - 1; i41 = i41 + 1) {
			for(i40 = 0; i40 <= 240 - 1; i40 = i40 + 1) {
				call3[i41][i40] = Orig_I_data[i41 + 320][i40 + 240];
			}
		}
		calc_gradient_1(keypoints2_data22, keypoints2_size22, call3);
		for(i42 = 0; i42 <= keypoints2_size22[0] - 1; i42 = i42 + 1) {
			keypoints2_data22[0][i42] = (uint16_t)(uint32_t)keypoints2_data22[0][i42] + 241 - 1;
			keypoints2_size22[0] = EMX_max_i32_i32_i32(keypoints2_size22[0], i42 + 1);
			keypoints2_size22[1] = 2;
			keypoints2_data22[1][i42] = (uint16_t)(uint32_t)keypoints2_data22[1][i42] + 321 - 1;
			keypoints2_size22[0] = EMX_max_i32_i32_i32(keypoints2_size22[0], i42 + 1);
			keypoints2_size22[1] = 2;
		}
		for(i44 = 0; i44 <= 2 - 1; i44 = i44 + 1) {
			for(i43 = keypoints_size[0]; i43 <= keypoints_size[0] + keypoints2_size22[0] - 1; i43 = i43 + 1) {
				keypoints_data[i44][i43] = keypoints2_data22[i44][i43 - keypoints_size[0]];
			}
		}
		keypoints_size[0] = keypoints_size[0] + keypoints2_size22[0];
		keypoints_size[1] = 2;
	}
	numkp_data = (double)keypoints_size[0];
	#pragma EMX_KILLVAR Orig_out_data
	for(i15 = 0; i15 <= 4 - 1; i15 = i15 + 1) {
		for(i14 = 0; i14 <= 640 - 1; i14 = i14 + 1) {
			for(i13 = 0; i13 <= 480 - 1; i13 = i13 + 1) {
				Orig_out_data[i15][i14][i13] = 0;
			}
		}
	}
	for(i17 = 0; i17 <= 640 - 1; i17 = i17 + 1) {
		for(i16 = 0; i16 <= 480 - 1; i16 = i16 + 1) {
			Orig_out_data[0][i17][i16] = Orig_I_gray_data[i17][i16];
		}
	}
	for(i19 = 0; i19 <= 640 - 1; i19 = i19 + 1) {
		for(i18 = 0; i18 <= 480 - 1; i18 = i18 + 1) {
			Orig_out_data[1][i19][i18] = Orig_I_gray_data[i19][i18];
		}
	}
	for(i21 = 0; i21 <= 640 - 1; i21 = i21 + 1) {
		for(i20 = 0; i20 <= 480 - 1; i20 = i20 + 1) {
			Orig_out_data[2][i21][i20] = Orig_I_gray_data[i21][i20];
		}
	}
	for(i22 = 0; i22 <= (int32_t)numkp_data - 1; i22 = i22 + 1) {
		for(i23 = 0; i23 <= 5 - 1; i23 = i23 + 1) {
			Orig_out_data[1][(int32_t)keypoints_data[1][i22] - 1][(int32_t)(uint16_t)(uint32_t)keypoints_data[0][i22] + ((int32_t)(uint16_t)(uint32_t)i23 - 2) - 1] = 255;
		}
		for(i24 = 0; i24 <= 5 - 1; i24 = i24 + 1) {
			Orig_out_data[1][(int32_t)(uint16_t)(uint32_t)keypoints_data[1][i22] + ((int32_t)(uint16_t)(uint32_t)i24 - 2) - 1][(int32_t)keypoints_data[0][i22] - 1] = 255;
		}
	}
//	interface_outdata(Orig_out_data);
}

int main(int argc, char * * argv) {
	harris_image();
	return 0;
}

