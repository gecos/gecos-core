/**
 */
package gecos.instrs;

import gecos.core.ISymbolUse;
import gecos.core.Symbol;

import gecos.types.ArrayType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Range Array Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.instrs.InstrsPackage#getRangeArrayInstruction()
 * @model
 * @generated
 */
public interface RangeArrayInstruction extends ArrayInstruction, ISymbolUse {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.core.Symbol%&gt; _xblockexpression = null;\n{\n\tfinal &lt;%gecos.instrs.Instruction%&gt; si = this.getDest();\n\t&lt;%gecos.core.Symbol%&gt; _xifexpression = null;\n\tif ((si instanceof &lt;%gecos.instrs.SymbolInstruction%&gt;))\n\t{\n\t\t_xifexpression = ((&lt;%gecos.instrs.SymbolInstruction%&gt;)si).getSymbol();\n\t}\n\telse\n\t{\n\t\t_xifexpression = null;\n\t}\n\t_xblockexpression = _xifexpression;\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	Symbol getSymbol();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model newSymbolUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.instrs.Instruction%&gt; si = this.getDest();\nif ((si instanceof &lt;%gecos.instrs.SymbolInstruction%&gt;))\n{\n\t((&lt;%gecos.instrs.SymbolInstruction%&gt;)si).setSymbol(newSymbol);\n}\nelse\n{\n\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(\"Not yet Implemented\");\n}'"
	 * @generated
	 */
	void setSymbol(Symbol newSymbol);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model newDestUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((newDest instanceof &lt;%gecos.instrs.SymbolInstruction%&gt;))\n{\n\tsuper.setDest(newDest);\n}\nelse\n{\n\t&lt;%java.lang.String%&gt; _simpleName = this.getClass().getSimpleName();\n\t&lt;%java.lang.String%&gt; _plus = (\"Illegal use of \" + _simpleName);\n\t&lt;%java.lang.String%&gt; _plus_1 = (_plus + \".setDest() : target must be a symbol not a compex instrcytion such as \");\n\t&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + newDest);\n\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus_2);\n}'"
	 * @generated
	 */
	void setDest(Instruction newDest);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitRangeArrayInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.core.Symbol%&gt; _symbol = this.getSymbol();\nboolean _tripleNotEquals = (_symbol != null);\nif (_tripleNotEquals)\n{\n\t&lt;%java.lang.String%&gt; _name = this.getSymbol().getName();\n\t&lt;%java.lang.String%&gt; _plus = (\"\" + _name);\n\tfinal &lt;%java.lang.StringBuffer%&gt; sb = new &lt;%java.lang.StringBuffer%&gt;(_plus);\n\ttry\n\t{\n\t\t&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; _index = this.getIndex();\n\t\tboolean _tripleNotEquals_1 = (_index != null);\n\t\tif (_tripleNotEquals_1)\n\t\t{\n\t\t\t&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; _index_1 = this.getIndex();\n\t\t\tfor (final &lt;%gecos.instrs.Instruction%&gt; i : _index_1)\n\t\t\t{\n\t\t\t\tif ((i != null))\n\t\t\t\t{\n\t\t\t\t\tsb.append(\"[\").append(i).append(\"]\");\n\t\t\t\t}\n\t\t\t\telse\n\t\t\t\t{\n\t\t\t\t\tsb.append(\"[null]\");\n\t\t\t\t}\n\t\t\t}\n\t\t}\n\t\tlong _size = this.getType().getSize();\n\t\t&lt;%java.lang.String%&gt; _plus_1 = (\":\" + &lt;%java.lang.Long%&gt;.valueOf(_size));\n\t\tsb.append(_plus_1);\n\t}\n\tcatch (final Throwable _t) {\n\t\tif (_t instanceof &lt;%java.lang.Exception%&gt;) {\n\t\t\tsb.append(\"[#error#]\");\n\t\t}\n\t\telse\n\t\t{\n\t\t\tthrow &lt;%org.eclipse.xtext.xbase.lib.Exceptions%&gt;.sneakyThrow(_t);\n\t\t}\n\t}\n\treturn sb.toString();\n}\nelse\n{\n\treturn super.toString();\n}'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.Type%&gt; _type = this.getSymbol().getType();\nif ((_type instanceof &lt;%gecos.types.ArrayType%&gt;))\n{\n\t&lt;%gecos.types.Type%&gt; _type_1 = this.getSymbol().getType();\n\treturn ((&lt;%gecos.types.ArrayType%&gt;) _type_1);\n}\nreturn null;'"
	 * @generated
	 */
	ArrayType getTargetArrayType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" iDataType="gecos.instrs.int" iUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((i &lt; 0))\n{\n\tthrow new &lt;%java.lang.RuntimeException%&gt;(\"Unsupported Operation\");\n}\nfinal &lt;%gecos.instrs.SimpleArrayInstruction%&gt; element0 = &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory%&gt;.array(this.getSymbol());\nfinal &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; _function = new &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;()\n{\n\tpublic void accept(final &lt;%gecos.instrs.Instruction%&gt; it)\n\t{\n\t\telement0.addIndex(it.copy());\n\t}\n};\nthis.getIndex().forEach(_function);\nif ((i == 0))\n{\n\treturn element0;\n}\nfinal &lt;%gecos.instrs.Instruction%&gt; lastIdx = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;last(element0.getIndex());\nelement0.getIndex().remove(lastIdx);\nelement0.getIndex().add(&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory%&gt;.add(lastIdx.copy(), &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory%&gt;.Int(i)));\nreturn element0;'"
	 * @generated
	 */
	SimpleArrayInstruction getAccessedElement(int i);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.RangeArrayInstruction%&gt;))\n{\n\treturn (super.isSame(instr) &amp;&amp; &lt;%com.google.common.base.Objects%&gt;.equal(this.getSymbol(), ((&lt;%gecos.instrs.RangeArrayInstruction%&gt;)instr).getSymbol()));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // RangeArrayInstruction
