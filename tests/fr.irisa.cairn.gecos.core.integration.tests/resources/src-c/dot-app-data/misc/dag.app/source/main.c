#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "tools.h"
#include "defs.h"
#include "data_size.h"

#if defined(__KAHRISMA__) //_kahrisma_
	extern void sim_timer_start(const char *);
	extern void sim_timer_stop(const char *);
	extern void sim_reset_stats();
#elif defined(__XENTIUM__)
#elif defined(__VEX__)
	#include <vex_simd.h>
//	extern void sim_ta_disable();	/* disable simulation */
//	extern void sim_ta_enable();
	extern unsigned long long get_sim_cycle_counter();
#elif defined(__ST240__)
	#include <platform.h>
	static volatile unsigned int *_st240_pmcr = (unsigned int *) LXPM_CR; //performance monitor Control Register
	static volatile unsigned long *_st240_pmclkr = (unsigned int *) LXPM_PCLK; //performance monitor CLocK Register
#else
	#include <time.h>
#endif


extern void dag(TYPE_0 in[MC][NC], TYPE_1 out[1]);


TYPE_0 in[M][N] = {0};
TYPE_1 out[M][N] = {0};

int main(int argc, const char **argv) {
	int i,j,a,b;
	char tmp[128];
	FILE *outfile, *infile;
	int _N, _M;
	unsigned long long time0;

	TYPE_0 tmpin[MC][NC];
	TYPE_1 tmpout;

#if defined(__VEX__)
	sim_ta_disable();
#endif

	/* check parameters */
	if(argc < 3) {
		fprintf(stderr, "Usage: %s input_file output_file\n", argv[0]);
		exit(-1);
	}

	infile = fopen(argv[1], "r");
	if(infile == NULL) {
		fprintf(stderr, "couldn't open file \"%s\"!\n", argv[1]);
		exit(-2);
	}

	outfile = fopen(argv[2], "w");
	if(outfile == NULL) {
		fprintf(stderr, "couldn't open file \"%s\"!\n", argv[2]);
		exit(-2);
	}

	/* init data */
	fgets(tmp, 128, infile);
	_N = atoi(tmp);
	fgets(tmp, 128, infile);
	_M = atoi(tmp);
	if(_N != N || _M != M) {
		fprintf(stderr, "nb of samples in '%s' is different from %d, %d\n", argv[1], M, N);
		exit(-3);
	}
	for(i=0; i<M; i++) {
		for(j=0; j<N; j++) {
			fgets(tmp, 128, infile);
			in[i][j] = (TYPE_0) (strcmp(TYPE2STR(TYPE_0), "float")? strtol(tmp, NULL, 10) : atof(tmp));
//			out[j] = 0;
		}
	}
	fclose(infile);


	/* 2- call function */
#if defined(__KAHRISMA__) //_kahrisma_
	sim_reset_stats();
	printf("\nSTATS RESET!\n");
	sim_timer_start("MY_KTIMER");
#elif defined(__XENTIUM__)
#elif defined(__VEX__)
	sim_ta_enable();
	time0 = get_sim_cycle_counter();
#elif defined(__ST240__)
//	bsp_pm_reset(); //reset performance monitor counters
//	bsp_pm_start(); // restart ..
//	time0 =bsp_pm_clock_get(); //get clock timer
	*_st240_pmcr = 0x2; /* disable and reset PM counter */
	*_st240_pmcr |= 1<<0; /* enable PM counter */
	time0 = *_st240_pmclkr;
#elif defined(CLOCK_PROCESS_CPUTIME_ID) //x86 & clock_gettime available
	struct timespec start, end;
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
#endif

	for(i=1; i<M-1; i++) {
		for(j=1; j<N-1; j++) {
			// init tmpin
			for(a=0; a<MC; a++)
				for(b=0; b<NC; b++)
					tmpin[a][b]= in[i-MC/2 + a][j-NC/2 + b];

			dag(tmpin, &tmpout);
			out[i][j] = tmpout;
		}
	}

#if defined(__KAHRISMA__)
	sim_timer_stop("MY_KTIMER");
#elif defined(__XENTIUM__)
#elif defined(__VEX__)
	sim_ta_disable();
	sim_ta_finish();
	printf("%llu\n", get_sim_cycle_counter() - time0);
#elif defined(__ST240__)
//	time1 = bsp_pm_clock_get();
	time1 = *_st240_pmclkr;
	bsp_pm_stop(); //stop performance monitor counters
	printf("%llu\n", time1 - time0);
#elif defined(CLOCK_PROCESS_CPUTIME_ID) //x86 & clock_gettime available
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);
	double duration = (double)(end.tv_sec-start.tv_sec) + ((double)(end.tv_nsec-start.tv_nsec)/1000000000);
	printf("Execution time = %lf sec\n\n", duration);
#endif

	/* output result */
	fprintf(outfile, "%d\n%d\n", M, N);
	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			fprintf(outfile, PRINT_ID(TYPE_1), out[i][j]);
			fprintf(outfile, "\n");
		}
	}
	fclose(outfile);

	return 0;
}
