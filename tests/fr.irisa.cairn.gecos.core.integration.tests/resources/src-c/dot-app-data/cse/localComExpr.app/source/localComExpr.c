/**
 * Tests for the transformation local common subexpression elimination
 */


#include <stdlib.h>
#include <stdio.h>

#define M  35
int var_g;

void g(int *cb) {
	(*cb)++;
}

void incr_int(int db) {
	db++;
}

void g2(int e[]) {
	e[0]++;
}

void incr_ptr(int *f) {
	f++;
}

void g3(int h) {
	var_g = var_g * var_g + h * h + 1;
}

int f(int i, int jb) {
	return (100 * (i + jb));
}

int f1(int x, int y, int z, int v, int w) {
	return (100 * (2 * x - 3 * y) - (4 * z + 5 * v + 6 * w));
}

int f2(int variable) {
	return (var_g + variable);
}

int f3(int variable2) {
	int r = rand();
	return (r + variable2);
}

/*********************************************************************************************
 *  In this cases (A.i), the addition should NOT be considered as Local Common-subexpression  *
 **********************************************************************************************/

void test_A (int c, int d, int *u, int e[4],int res[M][2], int opt_fct, int opt_ptr) {
	int i = 0;
	int _tmp1; // to test if the pass will generate a temporary variable having this name ! [SOLVED]
	int a,b;
	var_g = 1597 ;

	//*1*
	{	res[i][0] = *u + c;
		g(u);
		res[i][1] = *u + c;
	}
	//*2*
	i++;
	{	res[i][0] = e[0] + d;
		g2(e);
		res[i][1] = e[0] + d;
	}
	//*3*
	i++;
	{	res[i][0] = c + d;
		g(&c);
		res[i][1] = c + d;
	}
	//*4*
	i++;
	{	// tests Global variable case
		res[i][0] = c + var_g;
		g3(c);
		res[i][1] = c + var_g;
	}
	//*5*
	i++;
	{	res[i][0] = e[0] + d;
		e++;
		res[i][1] = e[0] + d;
	}
	//*6*
	i++;
	{	res[i][0] = *u + c;
		u++;
		res[i][1] = *u + c;
	}
	//*7*
	i++;
	{	int*p = &c; // an extra-pointer
		res[i][0] = c + d;
		*p = 10;
		res[i][1] = c + d;
	}
	//*8*
	i++;
	{	int*p = u; // an extra-pointer
		res[i][0] = *u + d;
		g(p);
		res[i][1] = *u + d;
	}
	//*9*
	i++;
	{	int*p = u; // an extra-pointer
		res[i][0] = *u + d;
		*p = *u + d;
		res[i][1] = *u + d;
	}
	//*10*
	i++;
	{	a = u + c;
		u++;
		b = u + c;
		res[i][0] = a-b;
	}
	//*11*
	i++;
	{	a = e + c;
		e++;
		b = e + c;
		res[i][0] = a-b;
	}
	//*12*
	i++;
	{ 	int*p = e; // an extra-pointer
		res[i][0] = e[0] + c;
		*p = e[0] + c;
		res[i][1] = e[0] + c;
	}
	//*13*
	i++;
	{	int*p = e; // an extra-pointer
		res[i][0] = e[0] + d;
		p[0] = e[0] + d;
		res[i][1] = e[0] + d;
	}
	//*14*
	i++;
	{	int*p = e; // an extra-pointer
		a = e[1] + c;
		(1 + p)[0] = e[1] + c;
		b = e[1] + c;
		res[i][0] = a!=b;
	}
	//*15*
	i++;
	{	int**p = &e; // an extra-pointer
		a = e[0] + c;
		(*p)[0] = e[0] + c;
		b = e[0] + c;
		res[i][0] = a!=b;
	}
	//*16*
	i++;
	{	int**p = &e; // an extra-pointer
		a = e[0] + d;
		*(p[0]) = e[0] + d;
		b = e[0] + d;
		res[i][0] = a!=b;
	}
	//*17*
	i++;
	{	int**p = &e; // an extra-pointer
		a = e[0] + d;
		g(*p);
		b = e[0] + d;
		res[i][0] = a!=b;
	}
	//*18*
	i++;
	{	int**p = &e; // an extra-pointer
		a = e[0] + d;
		g(p[0]);
		b = e[0] + d;
		res[i][0] = a!=b;
	}
	//*19*
	i++;
	{	int*p = e; // an extra-pointer
		a = e[1] + c;
		g(1 + p);
		b = e[1] + c;
		res[i][0] = a!=b;
	}
	//*20*
	i++;
	{	res[i][0] = c + var_g; // tests Global variable case
		var_g = d;
		res[i][1] = c + var_g;
	}
	//*21*
	i++;
	{	res[i][0] = c + d;
		c = 0;
		res[i][1] = c + d;
	}
	//*22*
	i++;
	{
		int*p = u;
		res[i][0] = *p + d;
		*p = *u + d;
		res[i][1] = *p + d;
	}
	//*23*
	i++;
	{
		int*p = u; // an extra-pointer
		res[i][0] = *p + d;
		g(p);
		res[i][1] = *p + d;
	}
	//*24*
	i++;
	{
		int*p = &c;
		res[i][0] = *p + d;
		*p = 66;
		res[i][1] = *p + d;
	}

	if (!opt_fct) { // this test make sense only if the transformation was called with function_option == false
		//*25*
		i++;
		{
			res[i][0] = f2(c) + d; // f2 uses the global variable var_g
			var_g = var_g + 1;
			res[i][1] = f2(c) + d;
		}

		//*26*
		i++;
		{
			res[i][0] = f2(c) + d; // f2 uses the global variable var_g (global pass)
			var_g = var_g + 1;
			res[i][1] = f2(c) + d;
		}

		//*27*
		i++;
		{
			res[i][0] = f3(c) + d; // f3 returns a random value
			res[i][1] = f3(c) + d;
		}
	}

	if (opt_ptr) { // this test make sense only if the transformation was called with pointer_option == true

		//*28*
		i++;
		{
			int*p = &c;
			res[i][0] = *p + d;
			c = 66;
			res[i][1] = *p + d;
		}
		//*29*
		i++;
		{
			int*p = u; // an extra-pointer
			res[i][0] = *p + d;
			g(u);
			res[i][1] = *p + d;
		}
		//*30*
		i++;
		{
			int*p = u;
			res[i][0] = *p + d;
			*u = *u + d;
			res[i][1] = *p + d;
		}
		//*31*
		i++;
		{
			int*p; //  an extra-pointer : test the conservative approach against pointers
			p = &c;
			a = c + d;
			*p = 10;
			b = c + d;
			res[i][0] = a;
			res[i][1] = b;
		}
		//*32*
		i++;
		{
			int*p; //  an extra-pointer : test the conservative approach against pointers
			p = &c;
			a = c + d;
			g(p);
			b = c + d;
			res[i][0] = a;
			res[i][1] = b;
		}
		//*33*
		i++;
		{
			int * p = &c;
			int * ptr;
			ptr = &c;
			a = *p + d;
			g(ptr);
			b = *p + d;
			res[i][0] = a;
			res[i][1] = b;
		}
		//*34*
		i++;
		{
			int * p;
			p = &c;
			a = *p + d;
			g(&c);
			b = *p + d;
			res[i][0] = a;
			res[i][1] = b;
		}
	}

	printf ("A : %d tests passed.\n",i+1);
}


/*****************************************************************************************
 *  In this cases (B.i), the addition should be considered as Local Common-subexpression  *
 ******************************************************************************************/

void test_B (int j, int h, int*p, int k[4],int resl[M][2]) {
	int i = 0;
	int *a, *b;
	var_g = 1597 ;

	//*1*
	{	resl[i][0] = j + h;
		incr_int(h);
		resl[i][1] = j + h;
	}
	//*2*
	i++;
	{	int var_g = 10; // tests global + local variables having the same name case
		resl[i][0] = j + var_g;
		g3(j);
		resl[i][1] = j + var_g;
	}
	//*3*
	i++;
	{	a = p + 1;
		incr_int(*p);
		b = p + 1;
		resl[i][0] = a-b;
	}
	//*4*
	i++;
	{	a = k + 1;
		incr_int(*k);
		b = k + 1;
		resl[i][0] = a-b;
	}
	//*5*
	i++;
	{	resl[i][0] = j + h;
		f1(1, j, j + h, 2, h);
		resl[i][1] = j + h;
		i++;
		resl[i][0] = h + 10;
		f(j + h, h + 10);
		resl[i][1] = h + 10;
	}
	//*6 + 7*
	i++;
	{	resl[i][0] = j + h;
		resl[i+1][0] = f1(5, j, j + h, h, 15) + 5;
		resl[i+1][1] = f1(5, j, j + h, h, 15) + 5;
		resl[i][1] = resl[i][0] + j;
		j = 5 + j;
		resl[i][0] = resl[i][0] + j;
	}
	//*8*
	i+=2;
	{	// * The pass did not consider "k[0] + 100" as a common expression because the approach is conservative with pointers
		resl[i][0] = k[0] + 100;
		incr_int(*k);
		resl[i][1] = k[0] + 100;
	}
	//*9*
	i++;
	{	resl[i][0] = *k + j;
		incr_ptr(k);
		resl[i][1] = *k + j;
	}
	//*10*
	i++;
	{	a = k + j;
		g2(k);
		b = k + j;
		resl[i][0] = a==b;
	}
	//*11*
	i++;
	{	a = &j + h;
		g(&j);
		b = &j + h;
		resl[i][0] = a==b;
	}
	//*12*
	i++;
	{	a = p + h;
		g(p);
		b = p + h;
		resl[i][0] = a==b;
	}
	//*13*
	i++;
	{	resl[i][0] = j + var_g; // tests Global variable case
		h++;
		resl[i][1] = j + var_g;
	}

	printf ("B : %d tests passed.\n",i+1);
}
