package fr.irisa.cairn.gecos.model.analysis.types;

import gecos.types.Type;

public abstract class AbstractMultiDimLevel implements TypeLevel {

	protected int nDims = 0;
	protected Type remainingType;
	
	/**
	 * @return number of dimensions in this level.
	 */
	public int getNDims() {
		return nDims;
	}

	public Type getRemainingType() {
		return remainingType;
	}
	
}
