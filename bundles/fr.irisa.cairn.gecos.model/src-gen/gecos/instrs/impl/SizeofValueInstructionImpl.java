/**
 */
package gecos.instrs.impl;

import gecos.instrs.InstrsPackage;
import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;
import gecos.instrs.SizeofValueInstruction;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sizeof Value Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SizeofValueInstructionImpl extends ExprComponentImpl implements SizeofValueInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SizeofValueInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstrsPackage.eINSTANCE.getSizeofValueInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final InstrsVisitor visitor) {
		visitor.visitSizeofValueInstruction(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		Instruction _expr = this.getExpr();
		String _plus = ("sizeof(" + _expr);
		return (_plus + ")");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSame(final Instruction instr) {
		if ((instr instanceof SizeofValueInstruction)) {
			boolean isSame = false;
			if (((this.getExpr() == null) && (((SizeofValueInstruction)instr).getExpr() == null))) {
				isSame = true;
			}
			else {
				Instruction _expr = this.getExpr();
				boolean _tripleEquals = (_expr == null);
				Instruction _expr_1 = ((SizeofValueInstruction)instr).getExpr();
				boolean _tripleEquals_1 = (_expr_1 == null);
				boolean _xor = (_tripleEquals ^ _tripleEquals_1);
				if (_xor) {
					return false;
				}
				else {
					isSame = this.getExpr().isSame(((SizeofValueInstruction)instr).getExpr());
				}
			}
			return (super.isSame(instr) && isSame);
		}
		return false;
	}

} //SizeofValueInstructionImpl
