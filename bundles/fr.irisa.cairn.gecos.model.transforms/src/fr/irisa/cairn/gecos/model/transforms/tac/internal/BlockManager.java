package fr.irisa.cairn.gecos.model.transforms.tac.internal;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.tools.switches.BasicBlockSwitch;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Scope;
import gecos.instrs.Instruction;

/**
 * Manage block. Create the new scope for each block. Manage the number of the temporary variable. Merge the new instructions into basic block.
 * NOT FULLY TESTED
 * 
 * @author Nicolas Simon
 * @date 30/10/12
 *
 */
public class BlockManager extends BasicBlockSwitch<Object> {
	private InstManager _instManager;
	private Scope _newScope; // New scope which contains the new symbol creating during computing of basic block instructions
	
	public BlockManager(){
		_instManager = new InstManager();
	}
	
	@Override
	public Object caseBasicBlock(BasicBlock b) {
		Map<Instruction, List<Instruction>> instToAdd = new HashMap<Instruction, List<Instruction>>();
		for(Instruction inst : b.getInstructions()){
			List<Instruction> instToAddList = _instManager.compute(_newScope, inst);
			instToAdd.put(inst, instToAddList);
		}
		
		for(Instruction key : instToAdd.keySet()){
			int pos = b.getInstructions().indexOf(key);
			if(pos >= 0){
				b.getInstructions().addAll(pos, instToAdd.get(key));
			}
			else{
				throw new RuntimeException("The last instruction representing the instruction to transform is not present into the current basic block");
			}
		}
		
		return null;
	}
	
	@Override
	public Object caseCompositeBlock(CompositeBlock b){
		Scope parentScope = _newScope;
		_newScope = GecosUserCoreFactory.scope();
		
		for(Block block : b.getChildren()){
			doSwitch(block);
		}
		
		b.getScope().mergeWith(_newScope);
		
		_newScope = parentScope;
		return null;
	}

	@Override
	public Object caseIfBlock(IfBlock b){
		if(b.getThenBlock() != null)
			doSwitch(b.getThenBlock());
		if(b.getElseBlock() != null)
			doSwitch(b.getElseBlock());
		return null;
	}
	
	@Override
	public Object caseForBlock(ForBlock b){
		if(b.getBodyBlock() != null)
			doSwitch(b.getBodyBlock());
		return null;
	}
	
	@Override
	public Object caseWhileBlock(WhileBlock b){
		if(b.getBodyBlock() != null)
			doSwitch(b.getBodyBlock());
		return null;
	}
	
	@Override
	public Object caseDoWhileBlock(DoWhileBlock b){
		if(b.getBodyBlock() != null)
			doSwitch(b.getBodyBlock());
		return null;
	}
	
}
