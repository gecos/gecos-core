/**
 */
package gecos.blocks.impl;

import com.google.common.base.Objects;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;

import fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;
import fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksFactory;
import gecos.blocks.BlocksPackage;
import gecos.blocks.BlocksVisitor;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ControlEdge;

import gecos.core.CorePackage;
import gecos.core.CoreVisitable;
import gecos.core.CoreVisitor;
import gecos.core.Scope;
import gecos.core.ScopeContainer;
import gecos.core.Symbol;

import gecos.instrs.BranchType;
import gecos.instrs.Instruction;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.xtext.xbase.lib.ExclusiveRange;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Composite Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.blocks.impl.CompositeBlockImpl#getScope <em>Scope</em>}</li>
 *   <li>{@link gecos.blocks.impl.CompositeBlockImpl#getChildren <em>Children</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CompositeBlockImpl extends BlockImpl implements CompositeBlock {
	/**
	 * The cached value of the '{@link #getScope() <em>Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScope()
	 * @generated
	 * @ordered
	 */
	protected Scope scope;

	/**
	 * The cached value of the '{@link #getChildren() <em>Children</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChildren()
	 * @generated
	 * @ordered
	 */
	protected EList<Block> children;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompositeBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BlocksPackage.Literals.COMPOSITE_BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scope getScope() {
		return scope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetScope(Scope newScope, NotificationChain msgs) {
		Scope oldScope = scope;
		scope = newScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BlocksPackage.COMPOSITE_BLOCK__SCOPE, oldScope, newScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScope(Scope newScope) {
		if (newScope != scope) {
			NotificationChain msgs = null;
			if (scope != null)
				msgs = ((InternalEObject)scope).eInverseRemove(this, CorePackage.SCOPE__CONTAINER, Scope.class, msgs);
			if (newScope != null)
				msgs = ((InternalEObject)newScope).eInverseAdd(this, CorePackage.SCOPE__CONTAINER, Scope.class, msgs);
			msgs = basicSetScope(newScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.COMPOSITE_BLOCK__SCOPE, newScope, newScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Block> getChildren() {
		if (children == null) {
			children = new EObjectContainmentEList<Block>(Block.class, this, BlocksPackage.COMPOSITE_BLOCK__CHILDREN);
		}
		return children;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final CoreVisitor visitor) {
		visitor.visitScopeContainer(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final BlocksVisitor visitor) {
		visitor.visitCompositeBlock(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addBlockBefore(final Block instr, final Block pos) {
		boolean _contains = this.getChildren().contains(pos);
		boolean _not = (!_contains);
		if (_not) {
			String _plus = (pos + " is not contained in ");
			String _plus_1 = (_plus + this);
			throw new RuntimeException(_plus_1);
		}
		final int i = this.getChildren().indexOf(pos);
		this.getChildren().add(i, instr);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addBlockAfter(final Block instr, final Block pos) {
		boolean _contains = this.getChildren().contains(pos);
		boolean _not = (!_contains);
		if (_not) {
			String _plus = (pos + " is not contained in ");
			String _plus_1 = (_plus + this);
			throw new RuntimeException(_plus_1);
		}
		final int i = this.getChildren().indexOf(pos);
		this.getChildren().add((i + 1), instr);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addChildren(final Block bi) {
		this.getChildren().add(bi);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block managedCopy(final BlockCopyManager mng) {
		final CompositeBlock newBlock = GecosUserBlockFactory.CompositeBlock();
		Scope _scope = this.getScope();
		boolean _tripleNotEquals = (_scope != null);
		if (_tripleNotEquals) {
			final Scope newScope = this.getScope().managedCopy(mng);
			newBlock.setScope(newScope);
		}
		newBlock.setNumber(this.getNumber());
		GecosUserAnnotationFactory.copyAnnotations(this, newBlock);
		EList<Block> _children = this.getChildren();
		for (final Block child : _children) {
			{
				final Block newchildBlock = child.managedCopy(mng);
				newBlock.getChildren().add(newchildBlock);
			}
		}
		mng.link(this, newBlock);
		return newBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void replace(final Block oldb, final Block newb) {
		final int index = this.getChildren().indexOf(oldb);
		if ((index >= 0)) {
			this.getChildren().set(index, newb);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block simplifyBlock() {
		final int size = this.getChildren().size();
		if ((size == 1)) {
			final Block child = this.getChildren().get(0);
			if (((child != null) && (child.eClass() == this.eClass()))) {
				final CompositeBlock cbChild = ((CompositeBlock) child);
				child.simplifyBlock();
				this.getScope().mergeWith(child.getScope());
				this.getChildren().addAll(cbChild.getChildren());
				this.getChildren().remove(child);
			}
		}
		return this;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block mergeWithComposite(final CompositeBlock block) {
		block.getScope().mergeWith(this.getScope());
		return block;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void mergeChildComposite(final CompositeBlock b) {
		final int pos = this.getChildren().indexOf(b);
		if ((pos == (-1))) {
			throw new RuntimeException("Cannot merge composite");
		}
		this.getScope().mergeWith(b.getScope());
		b.setScope(null);
		this.removeBlock(b);
		int _size = b.getChildren().size();
		ExclusiveRange _greaterThanDoubleDot = new ExclusiveRange(_size, 0, false);
		for (final Integer i : _greaterThanDoubleDot) {
			this.getChildren().add(pos, b.getChildren().get((i).intValue()));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean mergeChildren() {
		boolean changed = false;
		final Block[] children = ((Block[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(this.getChildren(), Block.class)).clone();
		BasicBlock prev = ((BasicBlock) null);
		BasicBlock lastChild = ((BasicBlock) null);
		for (final Block child : children) {
			if ((child instanceof BasicBlock)) {
				lastChild = ((BasicBlock)child);
				if ((prev != null)) {
					final BranchInstructionFinder il = new BranchInstructionFinder();
					il.doSwitch(child);
					boolean _containsLabels = il.containsLabels();
					if (_containsLabels) {
						prev = ((BasicBlock)child);
					}
					else {
						final BasicBlock bb = ((BasicBlock)child);
						Instruction[] _clone = ((Instruction[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(bb.getInstructions(), Instruction.class)).clone();
						for (final Instruction instr : _clone) {
							prev.addInstruction(instr);
						}
						this.removeBlock(child);
						changed = true;
					}
				}
				else {
					final BranchInstructionFinder ig = new BranchInstructionFinder();
					ig.doSwitch(child);
					if (((ig.containsGotos() || ig.containsBreaks()) || ig.containsContinues())) {
						prev = null;
					}
					else {
						prev = ((BasicBlock)child);
					}
				}
			}
			else {
				if (((!Objects.equal(prev, lastChild)) && (prev != null))) {
					if ((lastChild != null)) {
						prev.getOutEdges().clear();
						prev.getOutEdges().addAll(lastChild.getOutEdges());
						lastChild = null;
					}
				}
				prev = null;
			}
		}
		return changed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeBlock(final Block b) {
		this.getChildren().remove(b);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addBlock(final Block child) {
		this.addBlock(child, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlEdge> connectTo(final Block to, final BranchType cond) {
		boolean _isEmpty = this.getChildren().isEmpty();
		if (_isEmpty) {
			final BasicBlock basicBlock = BlocksFactory.eINSTANCE.createBasicBlock();
			this.addBlock(basicBlock);
		}
		final Block last = this.getLastChilld();
		return last.connectTo(to, BranchType.UNCONDITIONAL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block getLastChilld() {
		EList<Block> _children = this.getChildren();
		int _size = this.getChildren().size();
		int _minus = (_size - 1);
		return _children.get(_minus);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addBlock(final Block child, final boolean nolink) {
		if (((!nolink) && (this.getChildren().size() > 0))) {
			final Block prev = this.getLastChilld();
			prev.connectTo(child, BranchType.UNCONDITIONAL);
		}
		this.getChildren().add(child);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlEdge connectFromBasic(final BasicBlock from, final BranchType cond) {
		int _size = this.getChildren().size();
		boolean _equals = (_size == 0);
		if (_equals) {
			final BasicBlock basicBlock = BlocksFactory.eINSTANCE.createBasicBlock();
			this.addBlock(basicBlock);
		}
		final Block first = this.getChildren().get(0);
		return first.connectFromBasic(from, cond);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _pathString = this.getPathString();
		String _plus = (_pathString + "[0:");
		int _size = this.getChildren().size();
		int _minus = (_size - 1);
		String _plus_1 = (_plus + Integer.valueOf(_minus));
		return (_plus_1 + "]");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPathString() {
		return super.getPathString(this.toShortString());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toShortString() {
		int _number = this.getNumber();
		return ("CB" + Integer.valueOf(_number));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addSymbol(final Symbol symbol) {
		this.getScope().addSymbol(symbol);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeSymbol(final Symbol symbol) {
		this.getScope().removeSymbol(symbol);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BlocksPackage.COMPOSITE_BLOCK__SCOPE:
				if (scope != null)
					msgs = ((InternalEObject)scope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.COMPOSITE_BLOCK__SCOPE, null, msgs);
				return basicSetScope((Scope)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BlocksPackage.COMPOSITE_BLOCK__SCOPE:
				return basicSetScope(null, msgs);
			case BlocksPackage.COMPOSITE_BLOCK__CHILDREN:
				return ((InternalEList<?>)getChildren()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BlocksPackage.COMPOSITE_BLOCK__SCOPE:
				return getScope();
			case BlocksPackage.COMPOSITE_BLOCK__CHILDREN:
				return getChildren();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BlocksPackage.COMPOSITE_BLOCK__SCOPE:
				setScope((Scope)newValue);
				return;
			case BlocksPackage.COMPOSITE_BLOCK__CHILDREN:
				getChildren().clear();
				getChildren().addAll((Collection<? extends Block>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BlocksPackage.COMPOSITE_BLOCK__SCOPE:
				setScope((Scope)null);
				return;
			case BlocksPackage.COMPOSITE_BLOCK__CHILDREN:
				getChildren().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BlocksPackage.COMPOSITE_BLOCK__SCOPE:
				return scope != null;
			case BlocksPackage.COMPOSITE_BLOCK__CHILDREN:
				return children != null && !children.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == CoreVisitable.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == ScopeContainer.class) {
			switch (derivedFeatureID) {
				case BlocksPackage.COMPOSITE_BLOCK__SCOPE: return CorePackage.SCOPE_CONTAINER__SCOPE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == CoreVisitable.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == ScopeContainer.class) {
			switch (baseFeatureID) {
				case CorePackage.SCOPE_CONTAINER__SCOPE: return BlocksPackage.COMPOSITE_BLOCK__SCOPE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //CompositeBlockImpl
