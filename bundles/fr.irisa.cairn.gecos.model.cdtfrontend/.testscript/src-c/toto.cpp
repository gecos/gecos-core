#include <ap_int.h>
int nu(int n);
float toto(int n);

#define N 1
#define NPLUSONE N + 1

ap_int<7> api_x;
ap_int<N> api_y;
ap_int<N+1> api_z;
ap_int<NPLUSONE> api_a;
ap_int<N*2> api_b;


typedef ap_uint<32> FLOAT;
typedef ap_uint<24> mantissa;

int FP_to_accumulable(FLOAT in) {
	mantissa m;
	m = in.range(22, 0);
	return 1;
}
