/**
 */
package gecos.gecosproject.impl;

import gecos.gecosproject.GecosHeaderDirectory;
import gecos.gecosproject.GecosprojectPackage;
import gecos.gecosproject.ProjectVisitor;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Gecos Header Directory</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.gecosproject.impl.GecosHeaderDirectoryImpl#isIsStandardHeader <em>Is Standard Header</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GecosHeaderDirectoryImpl extends GecosFileImpl implements GecosHeaderDirectory {
	/**
	 * The default value of the '{@link #isIsStandardHeader() <em>Is Standard Header</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsStandardHeader()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_STANDARD_HEADER_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsStandardHeader() <em>Is Standard Header</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsStandardHeader()
	 * @generated
	 * @ordered
	 */
	protected boolean isStandardHeader = IS_STANDARD_HEADER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GecosHeaderDirectoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GecosprojectPackage.Literals.GECOS_HEADER_DIRECTORY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsStandardHeader() {
		return isStandardHeader;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsStandardHeader(boolean newIsStandardHeader) {
		boolean oldIsStandardHeader = isStandardHeader;
		isStandardHeader = newIsStandardHeader;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GecosprojectPackage.GECOS_HEADER_DIRECTORY__IS_STANDARD_HEADER, oldIsStandardHeader, isStandardHeader));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final ProjectVisitor visitor) {
		visitor.visitHeaderDirectory(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GecosprojectPackage.GECOS_HEADER_DIRECTORY__IS_STANDARD_HEADER:
				return isIsStandardHeader();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GecosprojectPackage.GECOS_HEADER_DIRECTORY__IS_STANDARD_HEADER:
				setIsStandardHeader((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GecosprojectPackage.GECOS_HEADER_DIRECTORY__IS_STANDARD_HEADER:
				setIsStandardHeader(IS_STANDARD_HEADER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GecosprojectPackage.GECOS_HEADER_DIRECTORY__IS_STANDARD_HEADER:
				return isStandardHeader != IS_STANDARD_HEADER_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (isStandardHeader: ");
		result.append(isStandardHeader);
		result.append(')');
		return result.toString();
	}

} //GecosHeaderDirectoryImpl
