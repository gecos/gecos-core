int i1;
short s1=1;
float r1=1, r2=2, r3;

enum E1 {x, y};
enum E2 {x=3, y=x+2};
enum E3 {x,y} e1=y, e2;
enum {x,y} e3=x; //nameless enum

typedef int T1, T2;
typedef enum {x,y} T3;

int F1(T1, T3);


int F1(T1 t1, T3 t3) {
	int a;
	double d;

	for (int i=0; i<10; i++) { // i in what scope ?
		i++;
		int j;
	}
}

int main() {
	return 0;
}
