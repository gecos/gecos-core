/**
 */
package gecos.instrs;

import gecos.dag.DAGInstruction;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visitor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.instrs.InstrsPackage#getInstrsVisitor()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface InstrsVisitor extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model gUnique="false"
	 * @generated
	 */
	void visitGenericInstruction(GenericInstruction g);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model aUnique="false"
	 * @generated
	 */
	void visitAddressInstruction(AddressInstruction a);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model aUnique="false"
	 * @generated
	 */
	void visitArrayValueInstruction(ArrayValueInstruction a);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model cUnique="false"
	 * @generated
	 */
	void visitCallInstruction(CallInstruction c);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model cUnique="false"
	 * @generated
	 */
	void visitConvertInstruction(ConvertInstruction c);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model sUnique="false"
	 * @generated
	 */
	void visitSizeofInstruction(SizeofInstruction s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model sUnique="false"
	 * @generated
	 */
	void visitSizeofValueInstruction(SizeofValueInstruction s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model sUnique="false"
	 * @generated
	 */
	void visitSizeofTypeInstruction(SizeofTypeInstruction s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model fUnique="false"
	 * @generated
	 */
	void visitFieldInstruction(FieldInstruction f);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model enumeratorUnique="false"
	 * @generated
	 */
	void visitEnumeratorInstruction(EnumeratorInstruction enumerator);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model fUnique="false"
	 * @generated
	 */
	void visitFloatInstruction(FloatInstruction f);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model iUnique="false"
	 * @generated
	 */
	void visitIntInstruction(IntInstruction i);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model iUnique="false"
	 * @generated
	 */
	void visitIndirInstruction(IndirInstruction i);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model lUnique="false"
	 * @generated
	 */
	void visitLabelInstruction(LabelInstruction l);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model rUnique="false"
	 * @generated
	 */
	void visitRangeInstruction(RangeInstruction r);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model rUnique="false"
	 * @generated
	 */
	void visitRetInstruction(RetInstruction r);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model sUnique="false"
	 * @generated
	 */
	void visitSetInstruction(SetInstruction s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model sUnique="false"
	 * @generated
	 */
	void visitSymbolInstruction(SymbolInstruction s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDummyInstruction(DummyInstruction d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model pUnique="false"
	 * @generated
	 */
	void visitPhiInstruction(PhiInstruction p);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nUnique="false"
	 * @generated
	 */
	void visitNumberedSymbolInstruction(NumberedSymbolInstruction n);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bUnique="false"
	 * @generated
	 */
	void visitBreakInstruction(BreakInstruction b);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model gUnique="false"
	 * @generated
	 */
	void visitGotoInstruction(GotoInstruction g);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model cUnique="false"
	 * @generated
	 */
	void visitContinueInstruction(ContinueInstruction c);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model cUnique="false"
	 * @generated
	 */
	void visitCondInstruction(CondInstruction c);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model aUnique="false"
	 * @generated
	 */
	void visitArrayInstruction(ArrayInstruction a);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model mUnique="false"
	 * @generated
	 */
	void visitMethodCallInstruction(MethodCallInstruction m);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model sUnique="false"
	 * @generated
	 */
	void visitSimpleArrayInstruction(SimpleArrayInstruction s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model sUnique="false"
	 * @generated
	 */
	void visitCaseInstruction(CaseInstruction s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model iUnique="false"
	 * @generated
	 */
	void visitDAGInstruction(DAGInstruction i);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model iUnique="false"
	 * @generated
	 */
	void visitSimdInstruction(SimdInstruction i);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model iUnique="false"
	 * @generated
	 */
	void visitRangeArrayInstruction(RangeArrayInstruction i);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model iUnique="false"
	 * @generated
	 */
	void visitSSAUseSymbol(SSAUseSymbol i);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model iUnique="false"
	 * @generated
	 */
	void visitSSADefSymbol(SSADefSymbol i);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model iUnique="false"
	 * @generated
	 */
	void visitStringInstruction(StringInstruction i);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model iUnique="false"
	 * @generated
	 */
	void visitSimdGenericInstruction(SimdGenericInstruction i);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model iUnique="false"
	 * @generated
	 */
	void visitSimdPackInstruction(SimdPackInstruction i);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model iUnique="false"
	 * @generated
	 */
	void visitSimdShuffleInstruction(SimdShuffleInstruction i);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model iUnique="false"
	 * @generated
	 */
	void visitSimdExtractInstruction(SimdExtractInstruction i);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model iUnique="false"
	 * @generated
	 */
	void visitSimdExpandInstruction(SimdExpandInstruction i);

} // InstrsVisitor
