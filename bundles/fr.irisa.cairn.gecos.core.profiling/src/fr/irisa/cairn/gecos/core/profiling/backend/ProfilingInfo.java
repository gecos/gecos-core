package fr.irisa.cairn.gecos.core.profiling.backend;

import static fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport.BACKEND_PROF_FILE_BIN_EXT;
import static fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport.BACKEND_PROF_FILE_TXT_EXT;
import static fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport.BACKEND_PROF_FILE_TXT_NB_HEADER_LINES;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.DoubleBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;

import com.google.common.base.Preconditions;

/**
 * @author aelmouss
 */
public class ProfilingInfo<T extends Number> {
	
	private HeaderInfo headerInfo;
	private GenericNumberFactory<T> factory;
	private List<T[]> snapshots; // snapshot (older-first) values[]
	private List<ValuesStats> snapshotStats;
	private ValuesStats allValuesStats;
	
	
	public static <E extends Number> ProfilingInfo<E> parseProfilingInfo(Path textFile) throws ProfilingBackendFailure {
		String filename = textFile.getFileName().toString();
		if(filename.endsWith(BACKEND_PROF_FILE_TXT_EXT)) {
			HeaderInfo header = readHeaderfromTxtFile(textFile);
			ProfilingInfo<E> vInfo = new ProfilingInfo<>(header);
			vInfo.readValuesfromTxtFile(textFile);
			return vInfo;
		} else if(filename.endsWith(BACKEND_PROF_FILE_BIN_EXT)) {
			return ParseFromBinaryFile.readfromBinFile(textFile);
		}
		throw new ProfilingBackendFailure("Unknown profiling file extension: " + textFile);
	}
	
	static class HeaderInfo {
		
		/**
		 * Symbol name
		 */
		String name;
		
		/**
		 * as returned by C++ typeid(TYPE).name()
		 */
		String elemTypeIdName;
		
		int uid;
		int elemSizeBytes;
		int nbSnapshots;
		int nbElemsPerCopy;
		
		/**
		 * The class that can be use to represent elements; determined from {@link #elemTypeIdName}.
		 */
		Class<? extends Number> elemClass;

		
		private void parseElementType() {
			switch (elemTypeIdName) {
			case "d": elemClass = Double.class; break;
			case "f": elemClass = Float.class; break;
			case "l": elemClass = Long.class; break;
			case "i": elemClass = Integer.class; break;
			case "s": elemClass = Short.class; break;
			default:
				InvalidProfilingFileFormat e = new InvalidProfilingFileFormat("Unknown Element Type ID name '" + elemTypeIdName + "'");
//				throw e;
				
				System.err.println("[WARNING] " +e.getMessage() + ". Continuing to parse data as 'double' !" + e.getStackTrace()[0]);
				elemClass = Double.class; 
				break;
			}
		}
		
		String print() {
			StringBuilder b = new StringBuilder();
			b.append("Name: ").append(name).append(" (UID = ").append(uid).append(")\n");
			b.append("Element Type Class : ").append(elemClass.getSimpleName()).append(" ("+elemTypeIdName + ")\n");
			b.append("Nb snapshots       : ").append(nbSnapshots).append("\n");
			return b.toString();
		}
	}
	
	
	protected ProfilingInfo(HeaderInfo header) {
		this.headerInfo = header;
		this.snapshots = new ArrayList<>();
		this.factory = new GenericNumberFactory<>(getElemClass());
	}
	
	public List<T[]> getSnapshots() {
		return snapshots;
	}
	
	public T[] getSnapshot(int i) {
		return snapshots.get(i);
	}
	
	public BufferedImage getSnapshotAsImage(int snapshotNumber, int height, int width, int scale) {
		T[] values = getSnapshot(snapshotNumber);
		
		BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
		WritableRaster img_data = img.getRaster();
		
//		img_data.setPixels(0, 0, width, height, Arrays.stream(values).mapToDouble(Number::doubleValue).map(d -> d*255).toArray());
		int c = 0;
		for(int x=0; x<height; x++) {
			for(int y=0; y<width; y++) {
				double pixel = values[c++].doubleValue() * scale;
				img_data.setPixel(x, y, new double[] {pixel});
			}
		}
		
		return img;
	}
	
	public int[] getSnapshotSizes() {
		return snapshots.stream()
			.mapToInt(s -> s.length)
			.toArray();
	}
	
	/**
	 * @return stream of all snapshot values in order [snapshot0.values ... snapshot1.values ...]
	 */
	public Stream<T> getAllSnapshotValues() {
		return snapshots.stream()
				.flatMap(Arrays::stream);
	}
	
	public List<ValuesStats> getSnapshotStats() {
		if(snapshotStats == null) {
			snapshotStats = snapshots.stream()
				.map(ValuesStats::of)
				.collect(toList());
		}
		return snapshotStats;
	}
	
	public ValuesStats getAllValuesStats() {
		if(allValuesStats == null)
			allValuesStats = ValuesStats.of(getAllSnapshotValues());
		return allValuesStats;
	}
	
	public String getName() {
		return headerInfo.name;
	}

	public String getElemTypeIdName() {
		return headerInfo.elemTypeIdName;
	}

	public int getUid() {
		return headerInfo.uid;
	}

	public int getElemSizeBytes() {
		return headerInfo.elemSizeBytes;
	}

	public int getNbSnapshots() {
		return headerInfo.nbSnapshots;
	}

	public int getNbElemsPerCopy() {
		return headerInfo.nbElemsPerCopy;
	}

	@SuppressWarnings("unchecked")
	public Class<T> getElemClass() {
		return (Class<T>) headerInfo.elemClass;
	}
	
	public String printHeader() {
		return headerInfo.print();
	}
	
	public String printValues() {
		return snapshots.stream()
			.map(e -> "snapshot " + snapshots.indexOf(e) + " :\n" + valuesToString(e))
			.collect(Collectors.joining("\n"));
	}
	
	private String valuesToString(T[] values) {
		String str = Arrays.stream(values)
				.map(n -> n.toString())
				.collect(Collectors.joining(", ", "\t[", "]\n"));
		str += ValuesStats.of(values).generateReport();
		return str;
	}
	
	
	// -------------------------------
	// Parse from binary file
	// -------------------------------
	
	private static class ParseFromBinaryFile {
		
		private static <E extends Number> ProfilingInfo<E> readfromBinFile(Path binFile) throws ProfilingBackendFailure {
			if(!binFile.toFile().isFile() || !binFile.toString().endsWith(BACKEND_PROF_FILE_BIN_EXT))
				throw new InvalidParameterException("Invalid variable profiling file: " + binFile);
			
			HeaderInfo info = new HeaderInfo();
			try(BufferedInputStream stream = new BufferedInputStream(new FileInputStream(binFile.toFile()))) {
				info.uid = readNextInt(stream);
				info.name = readNextString(stream);
				info.elemTypeIdName = readNextString(stream);
				info.elemSizeBytes = readNextInt(stream);
				info.nbSnapshots = readNextInt(stream);
				
				info.parseElementType(); //! not used
				
				Preconditions.checkState(info.elemClass.isAssignableFrom(Double.class), "Only supported for double datatype");
				
				ProfilingInfo<E> vInfo = new ProfilingInfo<>(info);
				
				// read snapshots
				for (int i = 0; i < info.nbSnapshots; i++) {
					E[] tmp = readNextSnapshot(stream);
					vInfo.snapshots.add(tmp);
				}
				
				return vInfo;
			} catch (Exception e) {
				throw new ProfilingBackendFailure("Failed to extract header information from simulation output file: " + binFile, e);
			}
		}
	
		private static int readNextInt(BufferedInputStream stream) throws IOException {
			ByteBuffer buf = ByteBuffer.allocate(Integer.SIZE/Byte.SIZE).order(ByteOrder.nativeOrder());
			stream.read(buf.array());
			return buf.getInt();
		}
		
		private static String readNextString(BufferedInputStream stream) throws IOException {
			int strLen = readNextInt(stream);
			byte[] tmp = new byte[strLen];
			stream.read(tmp);
			return IntStream.range(0, strLen).mapToObj(n -> String.valueOf((char)tmp[n])).collect(joining());
		}
		
		@SuppressWarnings("unchecked")
		protected static <E extends Number> E[] readNextSnapshot(BufferedInputStream stream) throws IOException {
			int nbValues = readNextInt(stream);
			
			ByteBuffer buf = ByteBuffer.allocate(Double.SIZE/Byte.SIZE * nbValues).order(ByteOrder.nativeOrder());
			stream.read(buf.array());
			
			DoubleBuffer doubleBuffer = buf.asDoubleBuffer();
			double[] values = new double[nbValues];
			doubleBuffer.get(values);
			
			/*
			 * !! This introduces a significant overhead
			 */
	//				E[] tmp = Arrays.stream(values)
	//						.mapToObj(String::valueOf)
	//						.map(vInfo.factory::valueOf)
	//						.toArray(vInfo.factory::createArray);
			
			E[] tmp = (E[]) Arrays.stream(values)
				.mapToObj(Double::valueOf)
				.toArray(Double[]::new);
			
			return tmp;
		}
	}
	
	// -------------------------------
	// Parse from Text file
	// -------------------------------
	
	private static final String SEPARATOR = ":";
	
	private static HeaderInfo readHeaderfromTxtFile(Path textFile) throws ProfilingBackendFailure {
		if(!textFile.toFile().isFile() || !textFile.toString().endsWith(BACKEND_PROF_FILE_TXT_EXT))
			throw new InvalidParameterException("Invalid variable profiling file: " + textFile);
		
		try {
			List<String> headerLines = Files.lines(textFile)
				.limit(BACKEND_PROF_FILE_TXT_NB_HEADER_LINES)
				.collect(Collectors.toList());
			
			if(headerLines.size() != BACKEND_PROF_FILE_TXT_NB_HEADER_LINES) {
				throw new InvalidProfilingFileFormat("Header section must have " +  BACKEND_PROF_FILE_TXT_NB_HEADER_LINES 
						+ " lines, but only " + headerLines.size() + " were found in simulation output file: " + textFile);
			}
			
			HeaderInfo info = new HeaderInfo();
			info.uid = parseInt(headerLines.get(0));
			info.name = parseString(headerLines.get(1));
			info.elemTypeIdName = parseString(headerLines.get(2));
			info.elemSizeBytes = parseInt(headerLines.get(3));
			info.nbSnapshots = parseInt(headerLines.get(4));
			
			info.parseElementType();
			return info;
		} catch (Exception e) {
			throw new ProfilingBackendFailure("Failed to extract header information from simulation output file: " + textFile, e);
		}
	}
	
	private void readValuesfromTxtFile(Path textFile) {
		Objects.requireNonNull(factory);
		
		List<String> lines;
		try {
			lines = Files.lines(textFile).sequential()
				.skip(BACKEND_PROF_FILE_TXT_NB_HEADER_LINES)
				.collect(Collectors.toList());
	
			int readIndex = 0;
			
			for(int snapshot = 0; snapshot < getNbSnapshots(); snapshot++) {
				readIndex += 2; //!! skip empty line separator + snapshot number line 
				
				int nbElem = parseInt(lines.get(readIndex));
				readIndex++;
				if(nbElem == 0) {
					new RuntimeException("Snapshot " + snapshot + " has zero elements!").printStackTrace();
				}
				
				Builder<T> builder = Stream.builder();
				for(int i = readIndex; i < readIndex+nbElem; i++) {
					T e = readElem(lines.get(i), i);
					builder.add(e);
				}
				readIndex += nbElem;
				
				T[] values = builder.build().toArray(factory::createArray);
				snapshots.add(values);
			}
		} catch (IOException e) {
			throw new RuntimeException("Failed to read variable info from file " + textFile, e);
		} catch (Exception e) {
			throw new InvalidProfilingFileFormat("Failed to parse file '" + textFile + "'", e);
		}
	}

	private T readElem(String line, int lineNumber) {
		try {
			String s = line.trim().replace("nan", "NaN"); //'nan' cannot be parsed
			return factory.valueOf(s);
		} catch (Exception e) {
			throw new InvalidProfilingFileFormat("Failed to parse element in '" + line + "' @ line " + lineNumber, e);
		}
	}

	private static Integer parseInt(String line) {
		try {
			return Integer.valueOf(line.split(SEPARATOR)[1].trim());
		} catch(Exception e) {
			throw new InvalidProfilingFileFormat("Line is not formated correctly! " + line, e);
		}
	}
	
	private static String parseString(String line) {
		try {
			return line.split(SEPARATOR)[1].trim();
		} catch(Exception e) {
			throw new InvalidProfilingFileFormat("Line is not formated correctly! " + line, e);
		}
	}
	
}
