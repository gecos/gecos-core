/**
 */
package gecos.dag;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Op Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.DAGOpNode#getOpcode <em>Opcode</em>}</li>
 * </ul>
 *
 * @see gecos.dag.DagPackage#getDAGOpNode()
 * @model
 * @generated
 */
public interface DAGOpNode extends DAGNode, DagVisitable {
	/**
	 * Returns the value of the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Opcode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Opcode</em>' attribute.
	 * @see #setOpcode(String)
	 * @see gecos.dag.DagPackage#getDAGOpNode_Opcode()
	 * @model unique="false" dataType="gecos.dag.String"
	 * @generated
	 */
	String getOpcode();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGOpNode#getOpcode <em>Opcode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Opcode</em>' attribute.
	 * @see #getOpcode()
	 * @generated
	 */
	void setOpcode(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGOpNode(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getOpcode().equals(&lt;%gecos.dag.DAGOperator%&gt;.SET.getLiteral());'"
	 * @generated
	 */
	boolean isSet();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getOpcode().equals(&lt;%gecos.dag.DAGOperator%&gt;.CVT.getLiteral());'"
	 * @generated
	 */
	boolean isConvert();

} // DAGOpNode
