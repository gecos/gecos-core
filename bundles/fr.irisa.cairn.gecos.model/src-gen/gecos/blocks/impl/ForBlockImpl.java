/**
 */
package gecos.blocks.impl;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;

import fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;
import fr.irisa.cairn.gecos.model.tools.utils.BlocksContinuesBreaksFinder;
import fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksFactory;
import gecos.blocks.BlocksPackage;
import gecos.blocks.BlocksVisitor;
import gecos.blocks.ControlEdge;
import gecos.blocks.ForBlock;

import gecos.instrs.BranchType;
import gecos.instrs.BreakInstruction;
import gecos.instrs.ContinueInstruction;
import gecos.instrs.Instruction;

import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>For Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.blocks.impl.ForBlockImpl#getInitBlock <em>Init Block</em>}</li>
 *   <li>{@link gecos.blocks.impl.ForBlockImpl#getTestBlock <em>Test Block</em>}</li>
 *   <li>{@link gecos.blocks.impl.ForBlockImpl#getStepBlock <em>Step Block</em>}</li>
 *   <li>{@link gecos.blocks.impl.ForBlockImpl#getBodyBlock <em>Body Block</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ForBlockImpl extends BlockImpl implements ForBlock {
	/**
	 * The cached value of the '{@link #getInitBlock() <em>Init Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitBlock()
	 * @generated
	 * @ordered
	 */
	protected BasicBlock initBlock;

	/**
	 * The cached value of the '{@link #getTestBlock() <em>Test Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestBlock()
	 * @generated
	 * @ordered
	 */
	protected BasicBlock testBlock;

	/**
	 * The cached value of the '{@link #getStepBlock() <em>Step Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStepBlock()
	 * @generated
	 * @ordered
	 */
	protected BasicBlock stepBlock;

	/**
	 * The cached value of the '{@link #getBodyBlock() <em>Body Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBodyBlock()
	 * @generated
	 * @ordered
	 */
	protected Block bodyBlock;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ForBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BlocksPackage.Literals.FOR_BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicBlock getInitBlock() {
		return initBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInitBlock(BasicBlock newInitBlock, NotificationChain msgs) {
		BasicBlock oldInitBlock = initBlock;
		initBlock = newInitBlock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BlocksPackage.FOR_BLOCK__INIT_BLOCK, oldInitBlock, newInitBlock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitBlock(BasicBlock newInitBlock) {
		if (newInitBlock != initBlock) {
			NotificationChain msgs = null;
			if (initBlock != null)
				msgs = ((InternalEObject)initBlock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.FOR_BLOCK__INIT_BLOCK, null, msgs);
			if (newInitBlock != null)
				msgs = ((InternalEObject)newInitBlock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.FOR_BLOCK__INIT_BLOCK, null, msgs);
			msgs = basicSetInitBlock(newInitBlock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.FOR_BLOCK__INIT_BLOCK, newInitBlock, newInitBlock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicBlock getTestBlock() {
		return testBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTestBlock(BasicBlock newTestBlock, NotificationChain msgs) {
		BasicBlock oldTestBlock = testBlock;
		testBlock = newTestBlock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BlocksPackage.FOR_BLOCK__TEST_BLOCK, oldTestBlock, newTestBlock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTestBlock(BasicBlock newTestBlock) {
		if (newTestBlock != testBlock) {
			NotificationChain msgs = null;
			if (testBlock != null)
				msgs = ((InternalEObject)testBlock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.FOR_BLOCK__TEST_BLOCK, null, msgs);
			if (newTestBlock != null)
				msgs = ((InternalEObject)newTestBlock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.FOR_BLOCK__TEST_BLOCK, null, msgs);
			msgs = basicSetTestBlock(newTestBlock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.FOR_BLOCK__TEST_BLOCK, newTestBlock, newTestBlock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicBlock getStepBlock() {
		return stepBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStepBlock(BasicBlock newStepBlock, NotificationChain msgs) {
		BasicBlock oldStepBlock = stepBlock;
		stepBlock = newStepBlock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BlocksPackage.FOR_BLOCK__STEP_BLOCK, oldStepBlock, newStepBlock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStepBlock(BasicBlock newStepBlock) {
		if (newStepBlock != stepBlock) {
			NotificationChain msgs = null;
			if (stepBlock != null)
				msgs = ((InternalEObject)stepBlock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.FOR_BLOCK__STEP_BLOCK, null, msgs);
			if (newStepBlock != null)
				msgs = ((InternalEObject)newStepBlock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.FOR_BLOCK__STEP_BLOCK, null, msgs);
			msgs = basicSetStepBlock(newStepBlock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.FOR_BLOCK__STEP_BLOCK, newStepBlock, newStepBlock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block getBodyBlock() {
		return bodyBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBodyBlock(Block newBodyBlock, NotificationChain msgs) {
		Block oldBodyBlock = bodyBlock;
		bodyBlock = newBodyBlock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BlocksPackage.FOR_BLOCK__BODY_BLOCK, oldBodyBlock, newBodyBlock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBodyBlock(Block newBodyBlock) {
		if (newBodyBlock != bodyBlock) {
			NotificationChain msgs = null;
			if (bodyBlock != null)
				msgs = ((InternalEObject)bodyBlock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.FOR_BLOCK__BODY_BLOCK, null, msgs);
			if (newBodyBlock != null)
				msgs = ((InternalEObject)newBodyBlock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.FOR_BLOCK__BODY_BLOCK, null, msgs);
			msgs = basicSetBodyBlock(newBodyBlock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.FOR_BLOCK__BODY_BLOCK, newBodyBlock, newBodyBlock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final BlocksVisitor visitor) {
		visitor.visitForBlock(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BasicBlock> listContinueBlocks() {
		final BlocksContinuesBreaksFinder finder = new BlocksContinuesBreaksFinder();
		finder.doSwitch(this.getBodyBlock());
		return ECollections.<BasicBlock>unmodifiableEList(finder.getContinues());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ContinueInstruction> listContinueInstructions() {
		final BranchInstructionFinder finder = new BranchInstructionFinder();
		finder.doSwitch(this.getBodyBlock());
		return ECollections.<ContinueInstruction>unmodifiableEList(finder.getContinues());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BasicBlock> listBreakBlocks() {
		final BlocksContinuesBreaksFinder finder = new BlocksContinuesBreaksFinder();
		finder.doSwitch(this.getBodyBlock());
		return ECollections.<BasicBlock>unmodifiableEList(finder.getBreaks());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BreakInstruction> listBreakInstructions() {
		final BranchInstructionFinder finder = new BranchInstructionFinder();
		finder.doSwitch(this.getBodyBlock());
		return ECollections.<BreakInstruction>unmodifiableEList(finder.getBreaks());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block managedCopy(final BlockCopyManager mng) {
		final ForBlock forBlock = BlocksFactory.eINSTANCE.createForBlock();
		Block _managedCopy = this.getInitBlock().managedCopy(mng);
		forBlock.setInitBlock(((BasicBlock) _managedCopy));
		Block _managedCopy_1 = this.getTestBlock().managedCopy(mng);
		forBlock.setTestBlock(((BasicBlock) _managedCopy_1));
		Block _managedCopy_2 = this.getStepBlock().managedCopy(mng);
		forBlock.setStepBlock(((BasicBlock) _managedCopy_2));
		Block _managedCopy_3 = this.getBodyBlock().managedCopy(mng);
		forBlock.setBodyBlock(((BasicBlock) _managedCopy_3));
		GecosUserAnnotationFactory.copyAnnotations(this, forBlock);
		mng.link(this, forBlock);
		return forBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void replace(final Block oldb, final Block newb) {
		Block _bodyBlock = this.getBodyBlock();
		boolean _tripleEquals = (oldb == _bodyBlock);
		if (_tripleEquals) {
			this.setBodyBlock(newb);
		}
		else {
			BasicBlock _initBlock = this.getInitBlock();
			boolean _tripleEquals_1 = (oldb == _initBlock);
			if (_tripleEquals_1) {
				this.setInitBlock(((BasicBlock) newb));
			}
			else {
				BasicBlock _stepBlock = this.getStepBlock();
				boolean _tripleEquals_2 = (oldb == _stepBlock);
				if (_tripleEquals_2) {
					this.setStepBlock(((BasicBlock) newb));
				}
				else {
					BasicBlock _testBlock = this.getTestBlock();
					boolean _tripleEquals_3 = (oldb == _testBlock);
					if (_tripleEquals_3) {
						this.setTestBlock(((BasicBlock) newb));
					}
				}
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block simplifyBlock() {
		this.getInitBlock().simplifyBlock();
		this.getStepBlock().simplifyBlock();
		this.getTestBlock().simplifyBlock();
		this.getBodyBlock().simplifyBlock();
		return this;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addBlockAfter(final Block insertedBlock, final Block existingBlock) {
		this.getBodyBlock().addBlockAfter(insertedBlock, existingBlock);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeBlock(final Block bb) {
		BasicBlock _testBlock = this.getTestBlock();
		boolean _tripleEquals = (_testBlock == bb);
		if (_tripleEquals) {
			this.setTestBlock(null);
		}
		else {
			BasicBlock _initBlock = this.getInitBlock();
			boolean _tripleEquals_1 = (_initBlock == bb);
			if (_tripleEquals_1) {
				this.setInitBlock(null);
			}
			else {
				BasicBlock _stepBlock = this.getStepBlock();
				boolean _tripleEquals_2 = (_stepBlock == bb);
				if (_tripleEquals_2) {
					this.setStepBlock(null);
				}
				else {
					Block _bodyBlock = this.getBodyBlock();
					boolean _tripleEquals_3 = (_bodyBlock == bb);
					if (_tripleEquals_3) {
						this.setBodyBlock(null);
					}
					else {
						Block _bodyBlock_1 = this.getBodyBlock();
						boolean _tripleNotEquals = (_bodyBlock_1 != null);
						if (_tripleNotEquals) {
							this.getBodyBlock().removeBlock(bb);
						}
					}
				}
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlEdge connectFromBasic(final BasicBlock dispatch, final BranchType cond) {
		BasicBlock _initBlock = this.getInitBlock();
		boolean _tripleNotEquals = (_initBlock != null);
		if (_tripleNotEquals) {
			return this.getInitBlock().connectFromBasic(dispatch, cond);
		}
		BasicBlock _testBlock = this.getTestBlock();
		boolean _tripleNotEquals_1 = (_testBlock != null);
		if (_tripleNotEquals_1) {
			return this.getTestBlock().connectFromBasic(dispatch, cond);
		}
		Block _bodyBlock = this.getBodyBlock();
		boolean _tripleNotEquals_2 = (_bodyBlock != null);
		if (_tripleNotEquals_2) {
			return this.getBodyBlock().connectFromBasic(dispatch, cond);
		}
		BasicBlock _stepBlock = this.getStepBlock();
		boolean _tripleNotEquals_3 = (_stepBlock != null);
		if (_tripleNotEquals_3) {
			return this.getStepBlock().connectFromBasic(dispatch, cond);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlEdge> connectTo(final Block to, final BranchType cond) {
		final BasicEList<ControlEdge> res = ECollections.<ControlEdge>newBasicEList();
		BasicBlock _testBlock = this.getTestBlock();
		boolean _tripleNotEquals = (_testBlock != null);
		if (_tripleNotEquals) {
			res.addAll(this.getTestBlock().connectTo(to, BranchType.IF_FALSE));
		}
		Block _bodyBlock = this.getBodyBlock();
		boolean _tripleNotEquals_1 = (_bodyBlock != null);
		if (_tripleNotEquals_1) {
			final BlocksContinuesBreaksFinder visitor = new BlocksContinuesBreaksFinder();
			visitor.doSwitch(this.getBodyBlock());
			List<BasicBlock> _breaks = visitor.getBreaks();
			for (final BasicBlock bb : _breaks) {
				res.addAll(bb.connectTo(to, BranchType.UNCONDITIONAL));
			}
		}
		else {
			BasicBlock _testBlock_1 = this.getTestBlock();
			boolean _tripleNotEquals_2 = (_testBlock_1 != null);
			if (_tripleNotEquals_2) {
				res.addAll(this.getTestBlock().connectTo(to, BranchType.IF_TRUE));
			}
		}
		return res;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toShortString() {
		int _number = this.getNumber();
		return ("For" + Integer.valueOf(_number));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _pathString = this.getPathString();
		int _number = this.getNumber();
		String _plus = (_pathString + Integer.valueOf(_number));
		String string = (_plus + "(");
		String _string = string;
		EList<Instruction> _instructions = this.getInitBlock().getInstructions();
		string = (_string + _instructions);
		String _string_1 = string;
		string = (_string_1 + ";");
		String _string_2 = string;
		EList<Instruction> _instructions_1 = this.getTestBlock().getInstructions();
		string = (_string_2 + _instructions_1);
		String _string_3 = string;
		string = (_string_3 + ";");
		String _string_4 = string;
		EList<Instruction> _instructions_2 = this.getStepBlock().getInstructions();
		string = (_string_4 + _instructions_2);
		String _string_5 = string;
		string = (_string_5 + ")");
		Block _bodyBlock = this.getBodyBlock();
		boolean _tripleNotEquals = (_bodyBlock != null);
		if (_tripleNotEquals) {
			String _string_6 = string;
			String _shortString = this.getBodyBlock().toShortString();
			String _plus_1 = ("{" + _shortString);
			String _plus_2 = (_plus_1 + "}");
			string = (_string_6 + _plus_2);
		}
		else {
			String _string_7 = string;
			string = (_string_7 + "null");
		}
		return string;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPathString() {
		return super.getPathString("FOR");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BlocksPackage.FOR_BLOCK__INIT_BLOCK:
				return basicSetInitBlock(null, msgs);
			case BlocksPackage.FOR_BLOCK__TEST_BLOCK:
				return basicSetTestBlock(null, msgs);
			case BlocksPackage.FOR_BLOCK__STEP_BLOCK:
				return basicSetStepBlock(null, msgs);
			case BlocksPackage.FOR_BLOCK__BODY_BLOCK:
				return basicSetBodyBlock(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BlocksPackage.FOR_BLOCK__INIT_BLOCK:
				return getInitBlock();
			case BlocksPackage.FOR_BLOCK__TEST_BLOCK:
				return getTestBlock();
			case BlocksPackage.FOR_BLOCK__STEP_BLOCK:
				return getStepBlock();
			case BlocksPackage.FOR_BLOCK__BODY_BLOCK:
				return getBodyBlock();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BlocksPackage.FOR_BLOCK__INIT_BLOCK:
				setInitBlock((BasicBlock)newValue);
				return;
			case BlocksPackage.FOR_BLOCK__TEST_BLOCK:
				setTestBlock((BasicBlock)newValue);
				return;
			case BlocksPackage.FOR_BLOCK__STEP_BLOCK:
				setStepBlock((BasicBlock)newValue);
				return;
			case BlocksPackage.FOR_BLOCK__BODY_BLOCK:
				setBodyBlock((Block)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BlocksPackage.FOR_BLOCK__INIT_BLOCK:
				setInitBlock((BasicBlock)null);
				return;
			case BlocksPackage.FOR_BLOCK__TEST_BLOCK:
				setTestBlock((BasicBlock)null);
				return;
			case BlocksPackage.FOR_BLOCK__STEP_BLOCK:
				setStepBlock((BasicBlock)null);
				return;
			case BlocksPackage.FOR_BLOCK__BODY_BLOCK:
				setBodyBlock((Block)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BlocksPackage.FOR_BLOCK__INIT_BLOCK:
				return initBlock != null;
			case BlocksPackage.FOR_BLOCK__TEST_BLOCK:
				return testBlock != null;
			case BlocksPackage.FOR_BLOCK__STEP_BLOCK:
				return stepBlock != null;
			case BlocksPackage.FOR_BLOCK__BODY_BLOCK:
				return bodyBlock != null;
		}
		return super.eIsSet(featureID);
	}

} //ForBlockImpl
