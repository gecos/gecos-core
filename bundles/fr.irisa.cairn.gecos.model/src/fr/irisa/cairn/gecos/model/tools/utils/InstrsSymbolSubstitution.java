package fr.irisa.cairn.gecos.model.tools.utils;

import java.util.LinkedHashMap;
import java.util.Map;

import fr.irisa.cairn.gecos.model.tools.switches.DefaultInstructionSwitch;
import gecos.core.Symbol;
import gecos.instrs.SymbolInstruction;

/**
 * 
 * @author amorvan
 * @generated NOT
 */
public class InstrsSymbolSubstitution extends DefaultInstructionSwitch<Object> {

	private Map<Symbol,Symbol> map;
	
	public InstrsSymbolSubstitution(Map<Symbol,Symbol> m) {
		this.map = m;
	}

	public InstrsSymbolSubstitution(Symbol olds,Symbol news) {
		this.map = new LinkedHashMap<Symbol,Symbol>();
		this.map.put(olds,news);
	}
	
	@Override
	public Object caseSymbolInstruction(SymbolInstruction i) {
		
		if (map.containsKey(i.getSymbol())) {
			i.setSymbol(map.get(i.getSymbol()));
		}
		
		return null;
	}
	
}
