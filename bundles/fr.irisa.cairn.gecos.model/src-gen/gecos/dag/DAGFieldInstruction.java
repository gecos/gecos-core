/**
 */
package gecos.dag;

import gecos.types.Field;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Field Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.DAGFieldInstruction#getField <em>Field</em>}</li>
 * </ul>
 *
 * @see gecos.dag.DagPackage#getDAGFieldInstruction()
 * @model
 * @generated
 */
public interface DAGFieldInstruction extends DAGNode {
	/**
	 * Returns the value of the '<em><b>Field</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Field</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Field</em>' reference.
	 * @see #setField(Field)
	 * @see gecos.dag.DagPackage#getDAGFieldInstruction_Field()
	 * @model
	 * @generated
	 */
	Field getField();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGFieldInstruction#getField <em>Field</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Field</em>' reference.
	 * @see #getField()
	 * @generated
	 */
	void setField(Field value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGFieldInstruction(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.dag.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getField().toString();'"
	 * @generated
	 */
	String toString();

} // DAGFieldInstruction
