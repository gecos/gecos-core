package fr.irisa.cairn.gecos.model.tools.visitors;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksVisitor;
import gecos.blocks.CaseBlock;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ControlEdge;
import gecos.blocks.DataEdge;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.FlowEdge;
import gecos.blocks.ForBlock;
import gecos.blocks.ForC99Block;
import gecos.blocks.IfBlock;
import gecos.blocks.SimpleForBlock;
import gecos.blocks.SwitchBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;

import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * BlocksDefaultVisitor is used to visit every kind of block of a procedure.
 * It's default behavior is to visit every block recursively until reaching a
 * {@link BasicBlock}.
 * 
 * 
 * @author anfloch
 */
public abstract class GecosBlocksDefaultVisitor extends EObjectImpl implements BlocksVisitor {


	public void visit(Procedure proc) {
		proc.getBody().accept(this);
	}

	public void visit(ProcedureSet ps) {
		for (Procedure p : ps.listProcedures()) {
			visit(p);
		}
	}

	public void visitBasicBlock(BasicBlock b) {
		visitBlock(b);
	}

	public void visitCaseBlock(CaseBlock c) {
		c.getBody().accept(this);
		
	}

	public void visitCompositeBlock(CompositeBlock c) {
		Block[] array = c.getChildren().toArray(new Block[c.getChildren().size()]);
		for (Block block : array) {
			block.accept(this);
		}
		
	}


	public void visitControlEdge(ControlEdge c) { }
	public void visitDataEdge(DataEdge c) { 	}
	public void visitFlowEdge(FlowEdge c) { 	}
	
	public void visitSimpleForBlock(SimpleForBlock f) {
		visitForBlock(f);
	}
	
	public void visitForC99Block(ForC99Block f) {
		visitForBlock(f);
	}


	public void visitForBlock(ForBlock f) {
		if (f.getInitBlock() != null)
			f.getInitBlock().accept(this);
		if (f.getTestBlock() != null)
			f.getTestBlock().accept(this);
		if (f.getStepBlock() != null)
			f.getStepBlock().accept(this);
		if (f.getBodyBlock() != null)
			f.getBodyBlock().accept(this);
		
	}

	public void visitIfBlock(IfBlock i) {
		if (i.getTestBlock() != null)
			i.getTestBlock().accept(this);
		if(i.getThenBlock()!=null)
			i.getThenBlock().accept(this);
		if (i.getElseBlock() != null)
			i.getElseBlock().accept(this);
		
	}

	public void visitLoopBlock(DoWhileBlock l) {
		if (l.getTestBlock() != null)
			l.getTestBlock().accept(this);
		if (l.getBodyBlock() != null)
			l.getBodyBlock().accept(this);
		
	}

	public void visitSwitchBlock(SwitchBlock s) {
		s.getDispatchBlock().accept(this);
		s.getBodyBlock().accept(this);
	}

	public void visitWhileBlock(WhileBlock w) {
		if (w.getTestBlock() != null)
			w.getTestBlock().accept(this);
		if (w.getBodyBlock() != null)
			w.getBodyBlock().accept(this);
		
	}

	protected void visitBlock(BasicBlock b) {
		if(b.getInEdges()!=null)
			for (ControlEdge e : b.getInEdges()) {
				e.accept(this);
			}
		
	}



}
