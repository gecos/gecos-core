/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.tools.visitors.simpleforloops;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.ForBlock;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.CondInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.instrs.SymbolInstruction;
import gecos.types.BaseType;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;
import tom.mapping.GenericIntrospector;

@SuppressWarnings("all")
public class FindConditionnalBound {

	%include { sl.tom }

	%include { gecos_common.tom }
	%include { gecos_terminals.tom }
	%include { gecos_basic.tom }
	%include { gecos_blocks.tom }
	%include { gecos_compare.tom }
	%include { gecos_control.tom }
	%include { gecos_arithmetic.tom }
	
	private ForBlock forBlock;
	private Instruction bound;

	public FindConditionnalBound(ForBlock forBlock, Symbol loopVariable) {
		this.forBlock = forBlock;
		try {
			`InnermostId(CheckEqualities()).visitLight(forBlock.getTestBlock(), GenericIntrospector.INSTANCE);
		} catch (VisitFailure e) {
		}
		findBound(loopVariable);
	}

	public Instruction getBound() {
		return bound;
	}

	%strategy CheckEqualities() extends Identity() {
		visit Inst {
			gt@gt(InstL(lhs, rhs)) -> {
				Instruction rhs = `rhs;
				Instruction lhs = `lhs;
				if ((rhs.getType() instanceof BaseType && ((BaseType)rhs.getType()).asFloat() != null) || (lhs.getType() instanceof BaseType && ((BaseType)lhs.getType()).asFloat() != null))
					throw new SimpleLoopException((`gt).getBasicBlock(), "Unable to find a bound");
				Instruction instruction = `ival(1);
				instruction.setType(`rhs.getType());
				instruction = `add(InstL(rhs, instruction));
				instruction.setType(`rhs.getType());
				instruction = `ge(InstL(lhs, instruction));
				instruction.setType(`gt.getType());
				return instruction;
			}
			lt@lt(InstL(lhs, rhs)) -> {
				Instruction rhs = `rhs;
				Instruction lhs = `lhs;
				if ((rhs.getType() instanceof BaseType && ((BaseType)rhs.getType()).asFloat() != null) || (lhs.getType() instanceof BaseType && ((BaseType)lhs.getType()).asFloat() != null))
					throw new SimpleLoopException((`lt).getBasicBlock(), "Unable to find a bound");
				Instruction instruction = `ival(1);
				instruction.setType(`rhs.getType());
				instruction = `sub(InstL(rhs, instruction));
				instruction.setType(`rhs.getType());
				instruction = `le(InstL(lhs, instruction));
				instruction.setType(`lt.getType());
				return instruction;
			}
		}
	}
	
	private void findBound(Symbol loopVariable) {
		BasicBlock condition = (BasicBlock)forBlock.getTestBlock();
		if (condition.getInstructionCount() == 0)
			throw new SimpleLoopException(forBlock, "Unable to find a bound");
		Instruction boundInstr = condition.getInstruction(0);
		%match(boundInstr) {
			brcond(generic) -> {
				if (checkOperator(`generic)) {
					GenericInstruction genericInstruction = (GenericInstruction)`generic;
					//checkDivision(genericInstruction);
					findBound(genericInstruction, loopVariable);
				}
			}
			g@generic(_,_) -> {
				if (checkOperator(`g)) {
					GenericInstruction genericInstruction = (GenericInstruction)`g;
					//checkDivision(genericInstruction);
					findBound(genericInstruction, loopVariable);
				}
			}
		}
		if (bound == null)
			throw new SimpleLoopException(forBlock, "Unable to find a bound");
	}

	private boolean checkOperator(Instruction instruction) {
		%match(instruction) {
			ge(_) -> {
				return true;
			}
			gt(_) -> {
				return true;
			}
			le(_) -> {
				return true;
			}
			lt(_) -> {
				return true;
			}
			eq(_) -> {
				return true;
			}
		}
		return false;
	}

	private void findBound(GenericInstruction genericInstruction, Symbol symbol) {
		Instruction lhs = genericInstruction.getOperand(0);
		Instruction rhs = genericInstruction.getOperand(1);
		boolean symbolInLhs = false;
		boolean symbolInRhs = false;
		try {
			`OutermostId(IsThereLoopIterator(symbol)).visitLight(lhs, GenericIntrospector.INSTANCE);
		} catch (VisitFailure e) {
			symbolInLhs = true;
		} catch (RuntimeException e) {
			symbolInLhs = true;
		}
		try {
			`OutermostId(IsThereLoopIterator(symbol)).visitLight(rhs, GenericIntrospector.INSTANCE);
		} catch (VisitFailure e) {
			symbolInRhs = true;
		} catch (RuntimeException e) {
			symbolInRhs = true;
		}
		if (symbolInLhs ^ symbolInRhs)
			manageLoopIterator(genericInstruction, symbol, symbolInLhs);
		else if (symbolInLhs)
			throw new SimpleLoopException(forBlock, "Unable to manage loop iterator on both side !");
		else
			throw new SimpleLoopException(forBlock, "Unable to manage loop without iterator !");
	}

	%strategy IsThereLoopIterator(Sym symbol) extends Identity(){
		visit Inst {
			symref(sym) -> {
				if (symbol.equals(`sym))
					throw new RuntimeException("LoopIterator found");
			}
		} 
	}

	private void manageLoopIterator(GenericInstruction genericInstruction, Symbol symbol, boolean symbolInLhs) {
		Instruction iterator = null;
		Instruction instruction = null;
		Instruction otherSide = null;
		if (symbolInLhs) {
			iterator = genericInstruction.getChildren().get(0);
			bound = genericInstruction.getChildren().get(1);
		} else {
			iterator = genericInstruction.getChildren().get(1);
			bound = genericInstruction.getChildren().get(0);
		}
		while (iterator != null && !(iterator instanceof SymbolInstruction)) {
			if (symbolInLhs) {
				instruction = genericInstruction.getChildren().get(0);
				otherSide = genericInstruction.getChildren().get(1);
			} else {
				instruction = genericInstruction.getChildren().get(1);
				otherSide = genericInstruction.getChildren().get(0);
			}
			%match (instruction) {
			add(InstL(lhs, rhs)) -> {
				try {
					`OutermostId(IsThereLoopIterator(symbol)).visitLight(`lhs, GenericIntrospector.INSTANCE);
					genericInstruction.getChildren().clear();
					genericInstruction.getChildren().add(0, `rhs);
					Instruction substract = `sub(InstL(otherSide, lhs));
					substract.setType(genericInstruction.getType());
					genericInstruction.getChildren().add(1, substract);
				} catch (VisitFailure e) {
					throw new SimpleLoopException(forBlock, "Unable to manage loop with an addition on iterator !");
				} catch (RuntimeException e) {
					try {
						`OutermostId(IsThereLoopIterator(symbol)).visitLight(`rhs, GenericIntrospector.INSTANCE);
						genericInstruction.getChildren().clear();
						genericInstruction.getChildren().add(0, `lhs);
						Instruction substract = `sub(InstL(otherSide, rhs));
						substract.setType(genericInstruction.getType());
						genericInstruction.getChildren().add(1, substract);
					} catch (VisitFailure visitFailure) {
						throw new SimpleLoopException(forBlock, "Unable to manage loop with an addition on iterator !");
					} catch (RuntimeException runtimeException) {
						throw new SimpleLoopException(forBlock, "Unable to manage loop with an iterator on both side of addition !");
					}
				}
				}
				sub(InstL(lhs, rhs)) -> {
					try {
						`OutermostId(IsThereLoopIterator(symbol)).visitLight(`lhs, GenericIntrospector.INSTANCE);
					genericInstruction.getChildren().clear();
					genericInstruction.getChildren().add(0, `rhs);
					Instruction addition = `add(InstL(otherSide, lhs));
						addition.setType(genericInstruction.getType());
						genericInstruction.getChildren().add(1, addition);
					} catch (VisitFailure e) {
						throw new SimpleLoopException(forBlock, "Unable to manage loop with a subtraction on iterator !");
					} catch (RuntimeException e) {
						try {
							`OutermostId(IsThereLoopIterator(symbol)).visitLight(`rhs, GenericIntrospector.INSTANCE);
						genericInstruction.getChildren().clear();
						genericInstruction.getChildren().add(0, `lhs);
						Instruction addition = `add(InstL(otherSide, rhs));
							addition.setType(genericInstruction.getType());
							genericInstruction.getChildren().add(1, addition);
						} catch (VisitFailure visitFailure) {
							throw new SimpleLoopException(forBlock, "Unable to manage loop with a subtraction on iterator !");
						} catch (RuntimeException runtimeException) {
							throw new SimpleLoopException(forBlock, "Unable to manage loop with an iterator on both side of subtraction !");
						}
					}
				}
				mul(InstL(lhs, rhs)) -> {
					try {
						`OutermostId(IsThereLoopIterator(symbol)).visitLight(`lhs, GenericIntrospector.INSTANCE);
					genericInstruction.getChildren().clear();
					genericInstruction.getChildren().add(0, `rhs);
					Instruction division = `div(InstL(otherSide, lhs));
						division.setType(genericInstruction.getType());
						genericInstruction.getChildren().add(1, division);
					} catch (VisitFailure e) {
						throw new SimpleLoopException(forBlock, "Unable to manage loop with a multiplication on iterator !");
					} catch (RuntimeException e) {
						try {
							`OutermostId(IsThereLoopIterator(symbol)).visitLight(`rhs, GenericIntrospector.INSTANCE);
						genericInstruction.getChildren().clear();
						genericInstruction.getChildren().add(0, `lhs);
						Instruction division = `div(InstL(otherSide, rhs));
							division.setType(genericInstruction.getType());
							genericInstruction.getChildren().add(1, division);
						} catch (VisitFailure visitFailure) {
							throw new SimpleLoopException(forBlock, "Unable to manage loop with a multiplication on iterator !");
						} catch (RuntimeException runtimeException) {
							throw new SimpleLoopException(forBlock, "Unable to manage loop with an iterator on both side of multiplication !");
						}
					}
				}
				div(InstL(lhs, rhs)) -> {
					try {
						`OutermostId(IsThereLoopIterator(symbol)).visitLight(`lhs, GenericIntrospector.INSTANCE);
					genericInstruction.getChildren().clear();
					genericInstruction.getChildren().add(0, `rhs);
					Instruction multiplication = `mul(InstL(otherSide, lhs));
						multiplication.setType(genericInstruction.getType());
						genericInstruction.getChildren().add(1, multiplication);
					} catch (VisitFailure e) {
						throw new SimpleLoopException(forBlock, "Unable to manage loop with a division on iterator !");
					} catch (RuntimeException e) {
						try {
							`OutermostId(IsThereLoopIterator(symbol)).visitLight(`rhs, GenericIntrospector.INSTANCE);
						genericInstruction.getChildren().clear();
						genericInstruction.getChildren().add(0, `lhs);
						Instruction multiplication = `mul(InstL(otherSide, rhs));
							multiplication.setType(genericInstruction.getType());
							genericInstruction.getChildren().add(1, multiplication);
						} catch (VisitFailure visitFailure) {
							throw new SimpleLoopException(forBlock, "Unable to manage loop with a division on iterator !");
						} catch (RuntimeException runtimeException) {
							throw new SimpleLoopException(forBlock, "Unable to manage loop with an iterator on both side of division !");
						}
					}
				}
			}
			iterator = genericInstruction.getChildren().get(0);
			bound = genericInstruction.getChildren().get(1);
			
		}
	}
}