/**
 */
package gecos.instrs.impl;

import gecos.instrs.InstrsPackage;
import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;
import gecos.instrs.SimdExtractInstruction;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simd Extract Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SimdExtractInstructionImpl extends SimdInstructionImpl implements SimdExtractInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimdExtractInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstrsPackage.eINSTANCE.getSimdExtractInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final InstrsVisitor visitor) {
		visitor.visitSimdExtractInstruction(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getSubwordIndex() {
		return this.getChild(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getVector() {
		return this.getChild(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubwordIndex(final Instruction subwordIndex) {
		int _size = this.getChildren().size();
		boolean _lessThan = (_size < 2);
		if (_lessThan) {
			this.getChildren().add(1, subwordIndex);
		}
		else {
			this.getChildren().set(1, subwordIndex);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVector(final Instruction vector) {
		int _size = this.getChildren().size();
		boolean _greaterThan = (_size > 0);
		if (_greaterThan) {
			this.getChildren().set(0, vector);
		}
		else {
			this.getChildren().add(0, vector);
		}
	}

} //SimdExtractInstructionImpl
