package fr.irisa.cairn.gecos.model.modules;

import gecos.annotations.IAnnotation;
import gecos.annotations.StringAnnotation;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;
import gecos.gecosproject.GecosprojectFactory;

import java.io.FileNotFoundException;
import java.io.IOException;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.r2d2.gecos.framework.GSModule;

@GSModule("Wrap 'SaveGecosProject' module to export a ProcedureSet.\n"
		+ "\nSee 'LoadGecosProject' module.")
public class SaveProcedureSet {
	ProcedureSet _ps;
	String _filename;
	
	public SaveProcedureSet(ProcedureSet ps, String filename){
		_ps = ps;
		_filename = filename;
	}
	
	public void compute() throws FileNotFoundException, IOException{
		if(_ps.eContainer().eContainer() != null && _ps.eContainer().eContainer() instanceof GecosProject){
			new SaveGecosProject((GecosProject)_ps.eContainer().eContainer(), _filename).compute();
		}
		else{
			GecosProject project = new CreateProject("_no_name_").compute();
			GecosSourceFile sourceFile = GecosprojectFactory.eINSTANCE.createGecosSourceFile();
			IAnnotation annotation = _ps.getAnnotation(GecosUserAnnotationFactory.FILE_NAME_ANOTATION_KEY);
			if (annotation != null)
				sourceFile.setName(((StringAnnotation)annotation).getContent());
			else
				sourceFile.setName("tmp_gecos_file.c");
			sourceFile.setModel(_ps);
			project.getSources().add(sourceFile);
		
			new SaveGecosProject(project, _filename).compute();
		}
	}
}
