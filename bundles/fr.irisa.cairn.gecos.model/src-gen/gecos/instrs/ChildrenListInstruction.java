/**
 */
package gecos.instrs;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Children List Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.ChildrenListInstruction#getChildren <em>Children</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getChildrenListInstruction()
 * @model abstract="true"
 * @generated
 */
public interface ChildrenListInstruction extends ComplexInstruction {
	/**
	 * Returns the value of the '<em><b>Children</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.instrs.Instruction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Children</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' containment reference list.
	 * @see gecos.instrs.InstrsPackage#getChildrenListInstruction_Children()
	 * @model containment="true"
	 * @generated
	 */
	EList<Instruction> getChildren();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;unmodifiableEList(this.getChildren());'"
	 * @generated
	 */
	EList<Instruction> listChildren();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model instUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getChildren().add(inst);'"
	 * @generated
	 */
	void addChild(Instruction inst);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model instUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getChildren().remove(inst);'"
	 * @generated
	 */
	void removeChild(Instruction inst);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.instrs.int" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getChildren().size();'"
	 * @generated
	 */
	int getChildrenCount();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" iDataType="gecos.instrs.int" iUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _childrenCount = this.getChildrenCount();\nboolean _lessEqualsThan = (_childrenCount &lt;= i);\nif (_lessEqualsThan)\n{\n\treturn null;\n}\nreturn this.getChildren().get(i);'"
	 * @generated
	 */
	Instruction getChild(int i);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.ChildrenListInstruction%&gt;))\n{\n\treturn super.isSame(instr);\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; _listChildren = this.listChildren();\n&lt;%java.lang.String%&gt; _plus = (\"ComplexInstruction[\" + _listChildren);\nreturn (_plus + \"]\");'"
	 * @generated
	 */
	String toString();

} // ChildrenListInstruction
