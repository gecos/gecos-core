package fr.irisa.cairn.gecos.model.analysis.tests;

import java.security.InvalidParameterException;

import org.junit.Assert;

import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import gecos.core.Scope;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import junit.framework.TestCase;

/**
 * @author aelmouss
 */
public class TypeAnalyzerTest extends TestCase {
	static final boolean VERBOSE = true;
	static final Scope scope = GecosUserCoreFactory.scope();

	@Override
	protected void setUp() throws Exception {
		GecosUserTypeFactory.setScope(scope);
	}
	
	@FunctionalInterface
	public interface Operation {
		public void run();
	}
	
	private static void assertException(Class<? extends Exception> expectedException, Operation op) {
		try {
			op.run();
		} catch(Exception e) {
			if(!expectedException.isInstance(e))
				fail("was expecting exception of type <" + expectedException + "> but caught: " + e);
			return;
		}
		fail("was expecting exception but none was caught");
	}

	@org.junit.Test
	public void test1() {
		// * int
		Type type = GecosUserTypeFactory.INT();
		type = GecosUserTypeFactory.PTR(type);
		
		TypeAnalyzer ta = new TypeAnalyzer(type);
		
		assertEquals("isPointer", ta.isPointer(), true);
		assertEquals("nbLevels", ta.getNbLevels(), 1);
		assertEquals("getPointer.nDim", ta.tryGetPointerLevel(0).getNDims(), 1);
		assertEquals("getPointer.isConst.", false, ta.tryGetPointerLevel(0).isConst(0));
		assertEquals("getBase ok", true, ta.getBase().isEqual(GecosUserTypeFactory.INT()));
		assertEquals("isInt", true, ta.getBaseLevel().isInt());
		assertEquals("isAlias", false, ta.getBaseLevel().isAlias());
		assertEquals("isBaseType", true, ta.getBaseLevel().isBaseType());
		assertEquals("isFixedPoint", false, ta.getBaseLevel().isFixedPoint());
		assertEquals("isFloat", false, ta.getBaseLevel().isFloat());
		assertEquals("isConst", false, ta.getBaseLevel().isConst());
		assertEquals("isStatic", false, ta.getBaseLevel().isStatic());
		assertEquals("isFunction", false, ta.getBaseLevel().isFunction());
		assertEquals("isRecord", false, ta.getBaseLevel().isRecord());
		assertEquals("isSimd", false, ta.getBaseLevel().isSimd());
		assertEquals("isVoid", false, ta.getBaseLevel().isVoid());
		
		assertEquals("getTotalNbDims", 1, ta.getTotalNbDims());
		assertException(InvalidParameterException.class, () -> ta.indexOf(ta.getLevelContainingDim(-1)));
		assertEquals("getLevelContainingDim0", 0, ta.indexOf(ta.getLevelContainingDim(0)));
		assertException(InvalidParameterException.class, () -> ta.indexOf(ta.getLevelContainingDim(1)));
		assertException(InvalidParameterException.class, () -> ta.tryGetDimSize(-1));
		assertEquals("tryGetDimSize0", null, ta.tryGetDimSize(0));
		assertException(InvalidParameterException.class, () -> ta.tryGetDimSize(1));
		
		if(VERBOSE) {
			System.out.println("\nType :" + type);
			System.out.println(ta);
		}
		
		Type type2 = GecosUserTypeFactory.INT();
		TypeAnalyzer ta2 = new TypeAnalyzer(ta.revertAllLevelsOn(type2));
		
		assertEquals("Revert/equals", true, ta2.equals(ta));
		assertEquals("Revert/isSimilar", true, ta2.isSimilar(ta));
	}
	
	@org.junit.Test
	public void test2() {
		// *(*(int32_t : int))
		Type type = GecosUserTypeFactory.INT();
		type = GecosUserTypeFactory.ALIAS(type, "int32_t");
		
		type = GecosUserTypeFactory.PTR(type);
		type = GecosUserTypeFactory.PTR(type);
		
		TypeAnalyzer ta = new TypeAnalyzer(type);
		
		assertEquals("isPointer", true, ta.isPointer());
		assertEquals("nbLevels", 1, ta.getNbLevels());
		assertEquals("getPointer.nDim", 2, ta.tryGetPointerLevel(0).getNDims());
		assertEquals("getPointer.isConst.", false, ta.tryGetPointerLevel(0).isConst(0));
		assertEquals("getPointer.isConst.", false, ta.tryGetPointerLevel(0).isConst(1));
		assertEquals("getBase", true, ta.getBase().isEqual(GecosUserTypeFactory.INT()));
		assertEquals("isInt", true, ta.getBaseLevel().isInt());
		assertEquals("isAlias", false, ta.getBaseLevel().isAlias()); //! the baseLevel base is 'int' i.e. not an alias
		assertEquals("getAlias", "int32_t", ta.getBaseLevel().getAlias().getName());
		assertEquals("isBaseType", true, ta.getBaseLevel().isBaseType());
		assertEquals("isFixedPoint", false, ta.getBaseLevel().isFixedPoint());
		assertEquals("isFloat", false, ta.getBaseLevel().isFloat());
		assertEquals("isConst", false, ta.getBaseLevel().isConst());
		assertEquals("isStatic", false, ta.getBaseLevel().isStatic());
		assertEquals("isFunction", false, ta.getBaseLevel().isFunction());
		assertEquals("isRecord", false, ta.getBaseLevel().isRecord());
		assertEquals("isSimd", false, ta.getBaseLevel().isSimd());
		assertEquals("isVoid", false, ta.getBaseLevel().isVoid());
		
		assertEquals("getTotalNbDims", 2, ta.getTotalNbDims());
		assertEquals("getLevelContainingDim0", 0, ta.indexOf(ta.getLevelContainingDim(0)));
		assertEquals("getLevelContainingDim1", 0, ta.indexOf(ta.getLevelContainingDim(1)));
		assertEquals("tryGetDimSize0", null, ta.tryGetDimSize(0));
		assertEquals("tryGetDimSize1", null, ta.tryGetDimSize(1));
		assertException(InvalidParameterException.class, () -> ta.tryGetDimSize(2));
		
		if(VERBOSE) {
			System.out.println("\nType :" + type);
			System.out.println(ta);
		}
		
		Type type2 = GecosUserTypeFactory.INT();
		TypeAnalyzer ta2 = new TypeAnalyzer(ta.revertAllLevelsOn(type2));
		
		assertEquals("Revert/equals", true, ta2.equals(ta));
		assertEquals("Revert/isSimilar", true, ta2.isSimilar(ta));
	}
	

	@org.junit.Test
	public void test3() {
		// [1](int)
		Type type = GecosUserTypeFactory.INT();
		
		type = GecosUserTypeFactory.ARRAY(type, 10);
		
		TypeAnalyzer ta = new TypeAnalyzer(type);
		
		assertEquals("isArray", true, ta.isArray());
		assertEquals("nbLevels", 1, ta.getNbLevels());
		assertEquals("getArray.nDim", 1, ta.tryGetArrayLevel(0).getNDims());
		assertEquals("getArray.getSize.", 10, ta.tryGetArrayLevel(0).tryGetSize(0).longValue());
		assertEquals("getBase ok", true, ta.getBase().isEqual(GecosUserTypeFactory.INT()));
		assertEquals("isInt", true, ta.getBaseLevel().isInt());
		assertEquals("isAlias", false, ta.getBaseLevel().isAlias());
		assertEquals("isBaseType", true, ta.getBaseLevel().isBaseType());
		assertEquals("isFixedPoint", false, ta.getBaseLevel().isFixedPoint());
		assertEquals("isFloat", false, ta.getBaseLevel().isFloat());
		assertEquals("isConst", false, ta.getBaseLevel().isConst());
		assertEquals("isStatic", false, ta.getBaseLevel().isStatic());
		assertEquals("isFunction", false, ta.getBaseLevel().isFunction());
		assertEquals("isRecord", false, ta.getBaseLevel().isRecord());
		assertEquals("isSimd", false, ta.getBaseLevel().isSimd());
		assertEquals("isVoid", false, ta.getBaseLevel().isVoid());
		
		assertEquals("getTotalNbDims", 1, ta.getTotalNbDims());
		assertEquals("getLevelContainingDim0", 0, ta.indexOf(ta.getLevelContainingDim(0)));
		assertException(InvalidParameterException.class, () -> ta.indexOf(ta.getLevelContainingDim(1)));
		assertEquals("tryGetDimSize0", 10, ta.tryGetDimSizeValue(0).longValue());
		
		if(VERBOSE) {
			System.out.println("\nType :" + type);
			System.out.println(ta);
		}
	}
	
	@org.junit.Test
	public void test4() {
		// [20][10](*(const int)
		Type type = GecosUserTypeFactory.INT();
		type.setConstant(true);
		Type constInt = type.copy();
		
		type = GecosUserTypeFactory.PTR(type);
		
		type = GecosUserTypeFactory.ARRAY(type, 10);
		type = GecosUserTypeFactory.ARRAY(type, 20);
		
		TypeAnalyzer ta = new TypeAnalyzer(type);
		
		assertEquals("isArray", true, ta.isArray());
		assertEquals("nbLevels", 2, ta.getNbLevels());
		assertEquals("getArray.nDim", 2, ta.tryGetArrayLevel(0).getNDims());
		assertEquals("getArray.getSize(0)", 20, ta.tryGetArrayLevel(0).tryGetSize(0).longValue());
		assertEquals("getArray.getSize(1)", 10, ta.tryGetArrayLevel(0).tryGetSize(1).longValue());
		
		assertEquals("getPointer.nDim", 1, ta.tryGetPointerLevel(1).getNDims());
		assertEquals("getPointer.isConst", false, ta.tryGetPointerLevel(1).isConst(0));
		
		assertEquals("getBase ok", true, ta.getBase().isEqual(constInt));
		assertEquals("isInt", true, ta.getBaseLevel().isInt());
		assertEquals("isAlias", false, ta.getBaseLevel().isAlias());
		assertEquals("isBaseType", true, ta.getBaseLevel().isBaseType());
		assertEquals("isFixedPoint", false, ta.getBaseLevel().isFixedPoint());
		assertEquals("isFloat", false, ta.getBaseLevel().isFloat());
		assertEquals("isConst", true, ta.getBaseLevel().isConst());
		assertEquals("isStatic", false, ta.getBaseLevel().isStatic());
		assertEquals("isFunction", false, ta.getBaseLevel().isFunction());
		assertEquals("isRecord", false, ta.getBaseLevel().isRecord());
		assertEquals("isSimd", false, ta.getBaseLevel().isSimd());
		assertEquals("isVoid", false, ta.getBaseLevel().isVoid());
		
		assertEquals("getTotalNbDims", 3, ta.getTotalNbDims());
		assertEquals("getLevelContainingDim0", 0, ta.indexOf(ta.getLevelContainingDim(0)));
		assertEquals("getLevelContainingDim1", 0, ta.indexOf(ta.getLevelContainingDim(1)));
		assertEquals("getLevelContainingDim2", 1, ta.indexOf(ta.getLevelContainingDim(2)));
		assertException(InvalidParameterException.class, () -> ta.indexOf(ta.getLevelContainingDim(3)));
		assertEquals("tryGetDimSize0", 20, ta.tryGetDimSizeValue(0).longValue());
		assertEquals("tryGetDimSize1", 10, ta.tryGetDimSizeValue(1).longValue());
		assertEquals("tryGetDimSize0", null, ta.tryGetDimSizeValue(2));
		assertException(InvalidParameterException.class, () -> ta.tryGetDimSizeValue(3));

		if(VERBOSE) {
			System.out.println("\nType :" + type);
			System.out.println(ta);
		}
		
		Type type2 = GecosUserTypeFactory.INT();
		TypeAnalyzer ta2 = new TypeAnalyzer(ta.revertAllLevelsOn(type2));
		
		assertEquals("Revert/equals", true, ta2.equals(ta));
		assertEquals("Revert/isSimilar", true, ta2.isSimilar(ta));
	}
	
	@org.junit.Test
	public void test5() {
		//* ([20][10] (const* const* * (static const (int32_t : int))))
		Type type = GecosUserTypeFactory.INT();
		type = GecosUserTypeFactory.ALIAS(type, "int32_t");
		type.setStorageClass(StorageClassSpecifiers.STATIC);
		type.setConstant(true);
		
		type = GecosUserTypeFactory.PTR(type);
		type = GecosUserTypeFactory.PTR(type);
		type.setConstant(true);
		type = GecosUserTypeFactory.PTR(type);
		type.setConstant(true);
		
		type = GecosUserTypeFactory.ARRAY(type, 10);
		type = GecosUserTypeFactory.ARRAY(type, 20);
		
		type = GecosUserTypeFactory.PTR(type);
		type.setConstant(true);
		
		TypeAnalyzer ta = new TypeAnalyzer(type);
		
		assertEquals("isArray", true, ta.isPointer());
		assertEquals("nbLevels", 3, ta.getNbLevels());
		
		assertEquals("getPointer(0).nDim", 1, ta.tryGetPointerLevel(0).getNDims());
		assertEquals("getPointer(0).isConst.", true, ta.tryGetPointerLevel(0).isConst(0));
		
		assertEquals("getArray(1).nDim", 2, ta.tryGetArrayLevel(1).getNDims());
		assertEquals("getArray(1).getSize(0).", 20, ta.tryGetArrayLevel(1).tryGetSize(0).longValue());
		assertEquals("getArray(1).getSize(1).", 10, ta.tryGetArrayLevel(1).tryGetSize(1).longValue());
		
		assertEquals("getPointer(2).nDim", 3, ta.tryGetPointerLevel(2).getNDims());
		assertEquals("getPointer(2).isConst(0).", true, ta.tryGetPointerLevel(2).isConst(0));
		assertEquals("getPointer(2).isConst(1).", true, ta.tryGetPointerLevel(2).isConst(1));
		assertEquals("getPointer(2).isConst(2).", false, ta.tryGetPointerLevel(2).isConst(2));
		
		assertEquals("getBase ok", true, ta.getBase().isEqual(GecosUserTypeFactory.INT()));
		assertEquals("isInt", true, ta.getBaseLevel().isInt());
		assertEquals("isAlias", false, ta.getBaseLevel().isAlias()); //! the baseLevel base is 'int' i.e. not an alias
		assertEquals("getAlias", "int32_t", ta.getBaseLevel().getAlias().getName());
		assertEquals("isBaseType", true, ta.getBaseLevel().isBaseType());
		assertEquals("isFixedPoint", false, ta.getBaseLevel().isFixedPoint());
		assertEquals("isFloat", false, ta.getBaseLevel().isFloat());
		assertEquals("isConst", true, ta.getBaseLevel().isConst());
		assertEquals("isStatic", true, ta.getBaseLevel().isStatic());
		assertEquals("isFunction", false, ta.getBaseLevel().isFunction());
		assertEquals("isRecord", false, ta.getBaseLevel().isRecord());
		assertEquals("isSimd", false, ta.getBaseLevel().isSimd());
		assertEquals("isVoid", false, ta.getBaseLevel().isVoid());
		
		assertEquals("getTotalNbDims", 6, ta.getTotalNbDims());
		assertEquals("getLevelContainingDim0", 0, ta.indexOf(ta.getLevelContainingDim(0)));
		assertEquals("getLevelContainingDim1", 1, ta.indexOf(ta.getLevelContainingDim(1)));
		assertEquals("getLevelContainingDim2", 1, ta.indexOf(ta.getLevelContainingDim(2)));
		assertEquals("getLevelContainingDim3", 2, ta.indexOf(ta.getLevelContainingDim(3)));
		assertEquals("getLevelContainingDim4", 2, ta.indexOf(ta.getLevelContainingDim(4)));
		assertEquals("getLevelContainingDim5", 2, ta.indexOf(ta.getLevelContainingDim(5)));
		assertException(InvalidParameterException.class, () -> ta.indexOf(ta.getLevelContainingDim(6)));
		assertEquals("tryGetDimSize0", null, ta.tryGetDimSizeValue(0));
		assertEquals("tryGetDimSize0", 20, ta.tryGetDimSizeValue(1).longValue());
		assertEquals("tryGetDimSize1", 10, ta.tryGetDimSizeValue(2).longValue());
		assertEquals("tryGetDimSize0", null, ta.tryGetDimSizeValue(3));
		assertEquals("tryGetDimSize0", null, ta.tryGetDimSizeValue(4));
		assertEquals("tryGetDimSize0", null, ta.tryGetDimSizeValue(5));
		assertException(InvalidParameterException.class, () -> ta.tryGetDimSizeValue(6));
		
		if(VERBOSE) {
			System.out.println("\nType :" + type);
			System.out.println(ta);
		}
		
		Type type2 = GecosUserTypeFactory.INT();
		TypeAnalyzer ta2 = new TypeAnalyzer(ta.revertAllLevelsOn(type2));
		
		assertEquals("Revert/equals", false, ta2.equals(ta));
		assertEquals("Revert/isSimilar", true, ta2.isSimilar(ta));
	}
	
}
