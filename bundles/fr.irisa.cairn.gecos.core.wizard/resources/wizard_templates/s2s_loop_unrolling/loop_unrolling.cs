project = CreateGecosProject("Unroll Project");
AddSourceToGecosProject(project,"./src/loop_unrolling.c");
CDTFrontend(project);

SaveGecosProject(project, "project_before.gecosproject");

#calls the ModelForUnroller on the whole project
ModelForUnroll(project);

SaveGecosProject(project, "project_after.gecosproject");

output(project, "c", "./src-gen");