/**
 */
package gecos.blocks;

import fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;

import gecos.instrs.BranchType;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>If Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.blocks.IfBlock#getTestBlock <em>Test Block</em>}</li>
 *   <li>{@link gecos.blocks.IfBlock#getThenBlock <em>Then Block</em>}</li>
 *   <li>{@link gecos.blocks.IfBlock#getElseBlock <em>Else Block</em>}</li>
 *   <li>{@link gecos.blocks.IfBlock#getJumpTo <em>Jump To</em>}</li>
 * </ul>
 *
 * @see gecos.blocks.BlocksPackage#getIfBlock()
 * @model annotation="gmf.node figure='rectangle' label='number' label.placement='external' label.icon='false'"
 * @generated
 */
public interface IfBlock extends Block, BlocksVisitable {
	/**
	 * Returns the value of the '<em><b>Test Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Block</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Block</em>' containment reference.
	 * @see #setTestBlock(Block)
	 * @see gecos.blocks.BlocksPackage#getIfBlock_TestBlock()
	 * @model containment="true"
	 *        annotation="gmf.compartment foo='bar'"
	 * @generated
	 */
	Block getTestBlock();

	/**
	 * Sets the value of the '{@link gecos.blocks.IfBlock#getTestBlock <em>Test Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Test Block</em>' containment reference.
	 * @see #getTestBlock()
	 * @generated
	 */
	void setTestBlock(Block value);

	/**
	 * Returns the value of the '<em><b>Then Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Then Block</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Then Block</em>' containment reference.
	 * @see #setThenBlock(Block)
	 * @see gecos.blocks.BlocksPackage#getIfBlock_ThenBlock()
	 * @model containment="true"
	 *        annotation="gmf.compartment foo='bar'"
	 * @generated
	 */
	Block getThenBlock();

	/**
	 * Sets the value of the '{@link gecos.blocks.IfBlock#getThenBlock <em>Then Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Then Block</em>' containment reference.
	 * @see #getThenBlock()
	 * @generated
	 */
	void setThenBlock(Block value);

	/**
	 * Returns the value of the '<em><b>Else Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Else Block</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Else Block</em>' containment reference.
	 * @see #setElseBlock(Block)
	 * @see gecos.blocks.BlocksPackage#getIfBlock_ElseBlock()
	 * @model containment="true"
	 *        annotation="gmf.compartment foo='bar'"
	 * @generated
	 */
	Block getElseBlock();

	/**
	 * Sets the value of the '{@link gecos.blocks.IfBlock#getElseBlock <em>Else Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Else Block</em>' containment reference.
	 * @see #getElseBlock()
	 * @generated
	 */
	void setElseBlock(Block value);

	/**
	 * Returns the value of the '<em><b>Jump To</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Jump To</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Jump To</em>' attribute.
	 * @see #setJumpTo(String)
	 * @see gecos.blocks.BlocksPackage#getIfBlock_JumpTo()
	 * @model unique="false" dataType="gecos.blocks.String"
	 * @generated
	 */
	String getJumpTo();

	/**
	 * Sets the value of the '{@link gecos.blocks.IfBlock#getJumpTo <em>Jump To</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Jump To</em>' attribute.
	 * @see #getJumpTo()
	 * @generated
	 */
	void setJumpTo(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitIfBlock(this);'"
	 * @generated
	 */
	void accept(BlocksVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" mngDataType="gecos.core.BlockCopyManager" mngUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.blocks.IfBlock%&gt; ifblck = &lt;%gecos.blocks.BlocksFactory%&gt;.eINSTANCE.createIfBlock();\nifblck.setTestBlock(this.getTestBlock().managedCopy(mng));\nifblck.setThenBlock(this.getThenBlock().managedCopy(mng));\n&lt;%gecos.blocks.Block%&gt; _elseBlock = this.getElseBlock();\nboolean _tripleNotEquals = (_elseBlock != null);\nif (_tripleNotEquals)\n{\n\tifblck.setElseBlock(this.getElseBlock().managedCopy(mng));\n}\nifblck.setJumpTo(this.getJumpTo());\nint _number = this.getNumber();\nint _plus = (_number + 1000);\nifblck.setNumber(_plus);\n&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory%&gt;.copyAnnotations(this, ifblck);\nmng.link(this, ifblck);\nreturn ifblck;'"
	 * @generated
	 */
	Block managedCopy(BlockCopyManager mng);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.blocks.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _pathString = this.getPathString();\n&lt;%java.lang.String%&gt; _plus = (_pathString + \"(\");\n&lt;%gecos.blocks.Block%&gt; _testBlock = this.getTestBlock();\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + _testBlock);\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + \")\");\n&lt;%gecos.blocks.Block%&gt; _thenBlock = this.getThenBlock();\n&lt;%java.lang.String%&gt; res = (_plus_2 + _thenBlock);\n&lt;%gecos.blocks.Block%&gt; _elseBlock = this.getElseBlock();\nboolean _tripleNotEquals = (_elseBlock != null);\nif (_tripleNotEquals)\n{\n\t&lt;%gecos.blocks.Block%&gt; _elseBlock_1 = this.getElseBlock();\n\t&lt;%java.lang.String%&gt; _plus_3 = ((res + \" else \") + _elseBlock_1);\n\tres = _plus_3;\n}\nreturn res;'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.blocks.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _xifexpression = null;\n&lt;%gecos.blocks.Block%&gt; _elseBlock = this.getElseBlock();\nboolean _tripleNotEquals = (_elseBlock != null);\nif (_tripleNotEquals)\n{\n\tint _number = this.getNumber();\n\t_xifexpression = ((\"Else\" + \"_\") + &lt;%java.lang.Integer%&gt;.valueOf(_number));\n}\nreturn (\"IfThen\" + _xifexpression);'"
	 * @generated
	 */
	String toShortString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getThenBlock().simplifyBlock();\n&lt;%gecos.blocks.Block%&gt; _elseBlock = this.getElseBlock();\nboolean _tripleNotEquals = (_elseBlock != null);\nif (_tripleNotEquals)\n{\n\tthis.getElseBlock().simplifyBlock();\n}\nreturn this;'"
	 * @generated
	 */
	Block simplifyBlock();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bbUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.Block%&gt; _thenBlock = this.getThenBlock();\nboolean _tripleEquals = (_thenBlock == bb);\nif (_tripleEquals)\n{\n\tthis.setThenBlock(null);\n}\nelse\n{\n\t&lt;%gecos.blocks.Block%&gt; _elseBlock = this.getElseBlock();\n\tboolean _tripleEquals_1 = (_elseBlock == bb);\n\tif (_tripleEquals_1)\n\t{\n\t\tthis.setElseBlock(null);\n\t}\n\telse\n\t{\n\t\t&lt;%gecos.blocks.Block%&gt; _testBlock = this.getTestBlock();\n\t\tboolean _tripleEquals_2 = (_testBlock == bb);\n\t\tif (_tripleEquals_2)\n\t\t{\n\t\t\tthis.setTestBlock(null);\n\t\t}\n\t\telse\n\t\t{\n\t\t\t&lt;%gecos.blocks.Block%&gt; _thenBlock_1 = this.getThenBlock();\n\t\t\tboolean _tripleNotEquals = (_thenBlock_1 != null);\n\t\t\tif (_tripleNotEquals)\n\t\t\t{\n\t\t\t\tthis.getThenBlock().removeBlock(bb);\n\t\t\t}\n\t\t\t&lt;%gecos.blocks.Block%&gt; _elseBlock_1 = this.getElseBlock();\n\t\t\tboolean _tripleNotEquals_1 = (_elseBlock_1 != null);\n\t\t\tif (_tripleNotEquals_1)\n\t\t\t{\n\t\t\t\tthis.getElseBlock().removeBlock(bb);\n\t\t\t}\n\t\t}\n\t}\n}'"
	 * @generated
	 */
	void removeBlock(Block bb);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" dispatchUnique="false" condUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.Block%&gt; _testBlock = this.getTestBlock();\nboolean _tripleNotEquals = (_testBlock != null);\nif (_tripleNotEquals)\n{\n\treturn this.getTestBlock().connectFromBasic(dispatch, cond);\n}\n&lt;%gecos.blocks.Block%&gt; _thenBlock = this.getThenBlock();\nboolean _tripleNotEquals_1 = (_thenBlock != null);\nif (_tripleNotEquals_1)\n{\n\treturn this.getThenBlock().connectFromBasic(dispatch, cond);\n}\n&lt;%gecos.blocks.Block%&gt; _elseBlock = this.getElseBlock();\nboolean _tripleNotEquals_2 = (_elseBlock != null);\nif (_tripleNotEquals_2)\n{\n\treturn this.getElseBlock().connectFromBasic(dispatch, cond);\n}\nreturn null;'"
	 * @generated
	 */
	ControlEdge connectFromBasic(BasicBlock dispatch, BranchType cond);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" toUnique="false" condUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%gecos.blocks.ControlEdge%&gt;&gt; res = &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.blocks.ControlEdge%&gt;&gt;newBasicEList();\n&lt;%gecos.blocks.Block%&gt; _thenBlock = this.getThenBlock();\nboolean _tripleNotEquals = (_thenBlock != null);\nif (_tripleNotEquals)\n{\n\tres.addAll(this.getThenBlock().connectTo(to, &lt;%gecos.instrs.BranchType%&gt;.UNCONDITIONAL));\n}\nelse\n{\n\t&lt;%gecos.blocks.Block%&gt; _testBlock = this.getTestBlock();\n\tboolean _tripleNotEquals_1 = (_testBlock != null);\n\tif (_tripleNotEquals_1)\n\t{\n\t\tres.addAll(this.getTestBlock().connectTo(to, &lt;%gecos.instrs.BranchType%&gt;.IF_TRUE));\n\t}\n}\n&lt;%gecos.blocks.Block%&gt; _elseBlock = this.getElseBlock();\nboolean _tripleNotEquals_2 = (_elseBlock != null);\nif (_tripleNotEquals_2)\n{\n\tres.addAll(this.getElseBlock().connectTo(to, &lt;%gecos.instrs.BranchType%&gt;.UNCONDITIONAL));\n}\nelse\n{\n\t&lt;%gecos.blocks.Block%&gt; _testBlock_1 = this.getTestBlock();\n\tboolean _tripleNotEquals_3 = (_testBlock_1 != null);\n\tif (_tripleNotEquals_3)\n\t{\n\t\tres.addAll(this.getTestBlock().connectTo(to, &lt;%gecos.instrs.BranchType%&gt;.IF_FALSE));\n\t}\n}\nreturn res;'"
	 * @generated
	 */
	EList<ControlEdge> connectTo(Block to, BranchType cond);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model oldbUnique="false" newbUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.Block%&gt; _thenBlock = this.getThenBlock();\nboolean _tripleEquals = (oldb == _thenBlock);\nif (_tripleEquals)\n{\n\tthis.setThenBlock(newb);\n}\nelse\n{\n\t&lt;%gecos.blocks.Block%&gt; _elseBlock = this.getElseBlock();\n\tboolean _tripleEquals_1 = (oldb == _elseBlock);\n\tif (_tripleEquals_1)\n\t{\n\t\tthis.setElseBlock(newb);\n\t}\n\telse\n\t{\n\t\t&lt;%gecos.blocks.Block%&gt; _testBlock = this.getTestBlock();\n\t\tboolean _tripleEquals_2 = (oldb == _testBlock);\n\t\tif (_tripleEquals_2)\n\t\t{\n\t\t\tthis.setTestBlock(newb);\n\t\t}\n\t}\n}'"
	 * @generated
	 */
	void replace(Block oldb, Block newb);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.blocks.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return super.getPathString(\"IF\");'"
	 * @generated
	 */
	String getPathString();

} // IfBlock
