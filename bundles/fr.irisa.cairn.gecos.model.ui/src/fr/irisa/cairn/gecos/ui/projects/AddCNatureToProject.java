package fr.irisa.cairn.gecos.ui.projects;

import org.eclipse.cdt.core.dom.IPDOMManager;
import org.eclipse.cdt.internal.core.pdom.indexer.IndexerPreferences;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

public class AddCNatureToProject  extends AbstractHandler {

	public static final String CNATURE = "org.eclipse.cdt.core.cnature";
	public static final boolean VERBOSE = true;
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
        ISelection selection = HandlerUtil.getCurrentSelection(event);
		final IProject project;

        if (selection instanceof IStructuredSelection) {
            IStructuredSelection structuredSelection = (IStructuredSelection) selection;
            Object firstElement = structuredSelection.getFirstElement();
            if (firstElement instanceof IProject) {
            	project = (IProject) firstElement;
            } else if (firstElement instanceof IJavaProject) {
            	IJavaProject jp = (IJavaProject) firstElement;
            	project = jp.getProject();
            } else {
            	throw new IllegalArgumentException("[GeCoS] ERROR. Unsupported first element of structured selection : "+firstElement.getClass().getSimpleName());
            }
        } else {
        	throw new IllegalArgumentException("[GeCoS] ERROR. Unsupported selection : "+selection.getClass().getSimpleName());
        }
		
		try {
			final IWorkspace ws = ResourcesPlugin.getWorkspace();
			ws.run(new IWorkspaceRunnable() {
				@Override
				public void run(IProgressMonitor monitor) throws CoreException {
					IProjectDescription description = project.getDescription();
					String[] natures = description.getNatureIds();
					for (String nature : natures) {
						if (nature.equals(CNATURE)) {
							if (VERBOSE) System.out.println("Project "+project.getName()+" already has C nature.");
							return;
						}
					}
					IndexerPreferences.set(project, IndexerPreferences.KEY_INDEXER_ID, IPDOMManager.ID_NO_INDEXER);
					String[] newNatures = new String[natures.length + 1];
					System.arraycopy(natures, 0, newNatures, 1, natures.length);
					newNatures[0] = CNATURE;
					description.setNatureIds(newNatures);
					project.setDescription(description, null);
					
//					ICConfigurationDescription prefCfg = CCorePlugin.getDefault().getPreferenceConfiguration();
//					ICProjectDescriptionManager mngr = CCorePlugin.getDefault().getProjectDescriptionManager();
//					ICProjectDescription projDes = mngr.createProjectDescription(project, false, false);
//					String genId = CDataUtil.genId("test");
//					String genId2 = CDataUtil.genId(null);
//					projDes.createConfiguration(genId2, genId,prefCfg);
//					mngr.setProjectDescription(project, projDes);
					
					project.refreshLocal(IResource.DEPTH_INFINITE, null);
					if (VERBOSE) System.out.println("Added C nature to project "+project.getName());
				}
			}, null);
			
		} catch (CoreException e) {
			e.printStackTrace(System.err);
			System.err.println("Could not add C nature to project "+project.getName());
		}
		return null;
	}
}