package fr.irisa.cairn.gecos.model.tools.utils;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.SimpleForBlock;
import gecos.blocks.SwitchBlock;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.types.Type;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class BlockCopyManager extends BlocksDefaultSwitch<Object> {
	
	Map<Symbol,Symbol> symMap;
	Map<Type,Type> typeMap;
	Map<Scope,Scope> scopeMap;
	Map<Block,Block> blkMap;

	public BlockCopyManager() {
		symMap  = new LinkedHashMap<Symbol, Symbol>();
		typeMap = new LinkedHashMap<Type, Type>();
		blkMap = new LinkedHashMap<Block,Block>();
		scopeMap = new LinkedHashMap<Scope, Scope>();
	}

	public void link(Symbol old, Symbol news) {
		symMap.put(old, news);
	}
	
	public void link(Block old, Block news) {
		blkMap.put(old, news);
	}

	public void link(Type old, Type news) {
		typeMap.put(old, news);
	}
	
	public void link(Scope old, Scope news) {
		scopeMap.put(old, news);
	}

	public void updateLocalSymbolReferences(Block b) {
		doSwitch(b);
	}
	
	@Override
	public Object caseCompositeBlock(CompositeBlock b) {
		for (Symbol s : b.getScope().getSymbols()) {
			if (s.getValue() != null) (new InstrsSymbolSubstitution(this.symMap)).doSwitch(s.getValue());
		}
		return super.caseCompositeBlock(b);
	}
	


	@Override
	public Object caseSwitchBlock(SwitchBlock b) {
		for(Entry<Long, Block> entry : b.getCases().entrySet()) {
			if(blkMap.containsValue(entry.getValue())) {
				entry.setValue(blkMap.get(entry.getValue()));
			}
		}
		return super.caseSwitchBlock(b);
	}

	@Override
	public Object caseBasicBlock(BasicBlock object) {
		(new InstrsSymbolSubstitution(this.symMap)).doSwitch(object);
		return null;
	}


	@Override
	public Object caseSimpleForBlock(SimpleForBlock object) {
		if(symMap.containsKey(object.getIterator())) {
			object.setIterator(symMap.get(object.getIterator()));
		}
		InstrsSymbolSubstitution updater = new InstrsSymbolSubstitution(this.symMap);
		updater.doSwitch(object.getLb());
		updater.doSwitch(object.getLb());
		updater.doSwitch(object.getStride());
		return super.caseForBlock(object);
	}

	
}	
	
