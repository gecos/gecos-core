/**
 */
package gecos.dag;

import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Pattern Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.DAGPatternNode#getPattern <em>Pattern</em>}</li>
 *   <li>{@link gecos.dag.DAGPatternNode#getInputsMap <em>Inputs Map</em>}</li>
 *   <li>{@link gecos.dag.DAGPatternNode#getOutputsMap <em>Outputs Map</em>}</li>
 * </ul>
 *
 * @see gecos.dag.DagPackage#getDAGPatternNode()
 * @model
 * @generated
 */
public interface DAGPatternNode extends DAGOpNode, DagVisitable {
	/**
	 * Returns the value of the '<em><b>Pattern</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pattern</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pattern</em>' containment reference.
	 * @see #setPattern(DAGInstruction)
	 * @see gecos.dag.DagPackage#getDAGPatternNode_Pattern()
	 * @model containment="true"
	 * @generated
	 */
	DAGInstruction getPattern();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGPatternNode#getPattern <em>Pattern</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pattern</em>' containment reference.
	 * @see #getPattern()
	 * @generated
	 */
	void setPattern(DAGInstruction value);

	/**
	 * Returns the value of the '<em><b>Inputs Map</b></em>' map.
	 * The key is of type {@link gecos.dag.DAGInPort},
	 * and the value is of type {@link gecos.dag.DAGInPort},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inputs Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inputs Map</em>' map.
	 * @see gecos.dag.DagPackage#getDAGPatternNode_InputsMap()
	 * @model mapType="gecos.dag.InPortMap&lt;gecos.dag.DAGInPort, gecos.dag.DAGInPort&gt;"
	 * @generated
	 */
	EMap<DAGInPort, DAGInPort> getInputsMap();

	/**
	 * Returns the value of the '<em><b>Outputs Map</b></em>' map.
	 * The key is of type {@link gecos.dag.DAGOutPort},
	 * and the value is of type {@link gecos.dag.DAGOutPort},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outputs Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outputs Map</em>' map.
	 * @see gecos.dag.DagPackage#getDAGPatternNode_OutputsMap()
	 * @model mapType="gecos.dag.OutPortMap&lt;gecos.dag.DAGOutPort, gecos.dag.DAGOutPort&gt;"
	 * @generated
	 */
	EMap<DAGOutPort, DAGOutPort> getOutputsMap();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGPatternNode(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

} // DAGPatternNode
