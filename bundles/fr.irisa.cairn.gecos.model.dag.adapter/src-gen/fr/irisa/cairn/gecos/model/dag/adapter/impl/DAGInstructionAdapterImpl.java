package fr.irisa.cairn.gecos.model.dag.adapter.impl;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IAdaptedContainer;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGEdgeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGInstructionAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGNodeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGPortAdapter;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;
import gecos.dag.DAGEdge;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGNode;
import gecos.dag.DAGPort;

/**
 * implementation of a graph adapter.
 * This graph adapt a DAGInstruction
 * @author <your name>
 *
 */
public class DAGInstructionAdapterImpl
		extends
			fr.irisa.cairn.graph.implement.Graph
		implements
			IDAGInstructionAdapter {

	private IAdaptedContainer container;
	private final DAGInstruction adapted;

	public DAGInstructionAdapterImpl(DAGInstruction adapted, boolean directed) {
		super(directed);
		this.adapted = adapted;
		this.container = new AdaptedContainerImpl();
	}

	public DAGInstruction getAdaptedGraph() {
		return this.adapted;
	}

	public IAdaptedContainer getContainer() {
		return this.container;
	}

	/**
	 * Add an edge to this graph
	 *
	 * @model default=""
	 * 
	 * @param e
	 *            an Edge
	 */
	@Override
	public void addEdge(IEdge edge) {
		super.addEdge(edge);

		if (edge instanceof IDAGEdgeAdapter) {
			this.container.addEdge((IDAGEdgeAdapter) edge);
		}

	}

	/**
	 * Add a node to this graph
	 *
	 * @model default=""
	 * 
	 * @param e
	 *            a node
	 */
	@Override
	public void addNode(INode node) {
		super.addNode(node);

		if (node instanceof IDAGNodeAdapter) {
			this.container.addNode((IDAGNodeAdapter) node);
		}
	}

	@Override
	public void removeEdge(IEdge edge) {
		super.removeEdge(edge);

		if (edge instanceof IDAGEdgeAdapter) {
			this.container.removeEdge((IDAGEdgeAdapter) edge);
		}

	}

	@Override
	public void removeNode(INode node) {
		super.removeNode(node);
		if (node instanceof IDAGNodeAdapter) {
			this.container.removeNode((IDAGNodeAdapter) node);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public java.util.Set<IDAGNodeAdapter> getNodes() {
		return (java.util.Set<IDAGNodeAdapter>) super.getNodes();
	}

	@SuppressWarnings("unchecked")
	@Override
	public java.util.Set<IDAGEdgeAdapter> getEdges() {
		return (java.util.Set<IDAGEdgeAdapter>) super.getEdges();
	}

	public IDAGInstructionAdapter getAdapterGraph(DAGInstruction graph) {
		IDAGInstructionAdapter graphAdapter;
		graphAdapter = this.container.getAdapterGraph(graph);
		return graphAdapter;
	}

	public IDAGNodeAdapter getAdapterNode(DAGNode node) {
		IDAGNodeAdapter nodeAdapter;
		nodeAdapter = this.container.getAdapterNode(node);
		return nodeAdapter;
	}

	public IDAGPortAdapter getAdapterPort(DAGPort port) {
		IDAGPortAdapter portAdapter;
		portAdapter = this.container.getAdapterPort(port);
		return portAdapter;
	}

	public IDAGEdgeAdapter getAdapterEdge(DAGEdge edge) {
		IDAGEdgeAdapter edgeAdapter;
		edgeAdapter = this.container.getAdapterEdge(edge);
		return edgeAdapter;
	}

}
