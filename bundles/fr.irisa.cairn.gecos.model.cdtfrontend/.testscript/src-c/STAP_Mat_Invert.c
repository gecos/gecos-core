typedef int* TS_DESC_2D[2];

extern float pivot[2];
extern float coef[2];
extern float inv[1000][1000][2];
extern float matinvptr[1000][1000][2];
void STAP_Mat_Invert(TS_DESC_2D mat, TS_DESC_2D matinv, int mat_ch_dim1,
		int mat_ch_dim2, int matinv_ch_dim1, int matinv_ch_dim2) {
	int i, j, k, re, im, l;
	for (i = 0; i < mat_ch_dim1; i++) {
		//recherche de pivot
		pivot[0] = inv[i][i][0]; //S0 par defaut le pivot est l'élément sur la diagonale
		pivot[1] = inv[i][i][1]; //S1

		for (j = i; j < 2 * mat_ch_dim2; j++) {
			re = inv[i][j][0]; //S2
			im = inv[i][j][1]; //S3
			inv[i][j][0] = (re * pivot[0] + im * pivot[1]) / (pivot[0] * pivot[0] + pivot[1] * pivot[1]); //S4
			inv[i][j][1] = (im * pivot[0] - re * pivot[1]) / (pivot[0] * pivot[0] + pivot[1] * pivot[1]); //S5
		} //(x+iy)/(a+ib)=((xa+yb)+i(ya-xb))/(a²+b²)

		for (k = 0; k < mat_ch_dim2; k++) {
			if (i > k || i < k) {
				coef[0] = inv[k][i][0]; //S6
				coef[1] = inv[k][i][1]; //S7 : S7[i, k] -> [0, i, 2, k, 1, 0, 0]
				for (l = i; l < 2 * mat_ch_dim2; l++) {
					inv[k][l][0] -= (coef[0] * inv[i][l][0] - coef[1] * inv[i][l][1]); //S8 -> S8[i, k, l] -> [0, i, 2, k, 1, l, 0]
					inv[k][l][1] -= (coef[0] * inv[i][l][1] + coef[1] * inv[i][l][0]); //S9
				}
			}
		}
	}

	//mettre le resultat dans matinvptr
	for (i = 0; i < matinv_ch_dim1; i++) {
		for (j = 0; j < matinv_ch_dim2; j++) {
			matinvptr[i][j][0] = inv[i][j + mat_ch_dim2][0]; //S10
			matinvptr[i][j][1] = inv[i][j + mat_ch_dim2][1]; //S11
		}
	}

}
