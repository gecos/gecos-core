#pragma gcs_array_flattening(2)
int y[3][4];
#pragma GCS_PURE_FUNCTION
int test1(int cv);

#pragma GCS_PURE_FUNCTION
int test1(int cv) {
	static int a;
	register int *x,c;
	int b;
	int i;
	int j;
	#pragma unroll
	for (i=0;i<16;i++){
		#pragma test1
		// This is a comment
		a +=x[i];
		#pragma test2
	}

	// This is a comment

	#pragma unroll
	for (i=0;i<3;i++) {
		#pragma unroll
		x[i];
		for (j=0;j<4;j++) {
			b+=y[i][j];
		}
	}
	return 0;
}

int test (int var){
	return (var+3);
}

/*ca c'est une fonction qui utilise une fontion en parametre*/
/*cette fonction met 3 comme variable d'entrée à la fonction entrée en parametre*/

int fonction (
#pragma GCS_PURE_FUNCTION
		int (*fonc)()
		){
return 0;
}

/*fonction principale*/
/*affiche le resultat a l'aide de printf ...*/

int main(){
/*printf ("\n");
printf ("resultat %i ...", fonction(test));
printf ("\n");*/
return 0;
}
