package fr.irisa.cairn.gecos.model.transforms.tac.internal;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.tools.switches.DefaultInstructionSwitch;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.RetInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.Type;

/**
 *  Visit the instruction and for each setInstruction, if the source is a composition of complex instruction, we transform this instruction.
 *  NOT FULLY TESTED
 * @author Nicolas Simon
 * @date 30/10/12
 *
 */
public class InstManager {
	private List<Instruction> _listInst; // List to stock new instruction created
	private InstructionSwitcher _switcher;
	private int _temporaryNumber = 0;
	private Scope _scopeToUse;
	
	
	public InstManager(){
		_switcher = new InstructionSwitcher();
	}
	
	public List<Instruction> getListInstruction(){
		return _listInst;
	}
	
	public List<Instruction> compute(Scope scope, Instruction inst){
		_listInst = new ArrayList<Instruction>();
		_scopeToUse = scope;
		_temporaryNumber = 0;
		_switcher.doSwitch(inst);
		
		return _listInst;
	}
	
	
	/**
	 * Create a new setInstruction based on one instruction. The destination is a new symbol created with a new name representing a temporary variable
	 * 
	 * @author Nicolas Simon
	 * @date 30/10/12
	 * @param g
	 * @return setInstruction
	 */
	private SetInstruction createNewTempSetInstruction(Instruction g){
		// Création du nom temporaire de la variable
		// Une vérification est faites pour choisir un nom de symbol non utilisé dans le scope courant
		Scope scopeCurrent = g.getBasicBlock().getScope();
		StringBuilder sBuilder = new StringBuilder();
		for(Symbol symb : scopeCurrent.lookupAllSymbols()){
			sBuilder.append(symb.getName());
		}
		Pattern pattern;
		Matcher matcher;
		String nameSymbol = "";
		do{
			nameSymbol += "_tmp";
			pattern = Pattern.compile(nameSymbol+"\\d+");
			matcher = pattern.matcher(sBuilder);
		}while(matcher.find());
		 
		
		Symbol destSymbol = findValidSymbolInScope(nameSymbol, g.getType());

		// Création du symbol et ajout de celui ci dans le scope temporaire
		SymbolInstruction dest = GecosUserInstructionFactory.symbref(destSymbol);
		return GecosUserInstructionFactory.set(dest, g.copy());
	}
	
	private Symbol findValidSymbolInScope(String name, Type type){
		Symbol possibleSymbol;
		String fullname;
		boolean createNewSymbol = false;
		do{
			fullname = name + _temporaryNumber++;
			possibleSymbol = null;
			
			// Find if in the current scope have a symbol with the same name
			for(Symbol sym : _scopeToUse.getSymbols()){
				if(sym.getName().equals(fullname)){
					possibleSymbol = sym;
					break;
				}
			}
			
			// If a symbol with a same name have been found, we check its type
			// If the type are compatible, we return this symbol
			// If not, we test the next temporary variable.
			// If the symbol not exist, we stop the loop and we create a new one
			if(possibleSymbol != null){			
				if(possibleSymbol.getType() == type){
					return possibleSymbol;
				}
			}
			else{
				createNewSymbol = true;
			}
			
		} while(!createNewSymbol);
		
		return GecosUserCoreFactory.symbol(fullname , type, _scopeToUse);
	}
	
	
	private class InstructionSwitcher extends DefaultInstructionSwitch<SetInstruction> {
		@Override
		public SetInstruction caseGenericInstruction(GenericInstruction g){	
			for(Instruction inst : g.getOperands()){
				SetInstruction o = doSwitch(inst);
				if(o != null){
					g.getOperands().set(g.getOperands().indexOf(inst), o.getDest().copy());
				}
			}
			
			if(g.getRoot() != g){
				SetInstruction setInst = createNewTempSetInstruction(g);
				_listInst.add(setInst);
				return setInst;
			}
			else{
				return null;
			}
		}
			
		@Override
		public SetInstruction caseArrayInstruction(ArrayInstruction g) {
			for(int i = 0 ; i < g.getIndex().size() ; i++){
				Instruction inst = g.getIndex().get(i);
				SetInstruction o = doSwitch(inst);
				if(o != null){
					g.replaceChild(inst, o.getDest().copy());
				}
			}
	
			if(g.getRoot() != g){
				SetInstruction setInst = createNewTempSetInstruction(g);
				_listInst.add(setInst);
				return setInst;
			}
			else{
				return null;
			}
		}
	
		@Override 
		public SetInstruction caseCallInstruction(CallInstruction g){
			for(int i = 0 ; i < g.getArgs().size() ; i++){
				Instruction inst = g.getArgs().get(i);
				SetInstruction o = doSwitch(inst);
				if(o != null){
					g.replaceChild(inst, o.getDest().copy());
				}
			}
			
			if(g.getRoot() != g){
				SetInstruction setInst = createNewTempSetInstruction(g);
				_listInst.add(setInst);
				return setInst;
			}
			else{
				return null;
			}
		}
		
		@Override
		public SetInstruction caseSetInstruction(SetInstruction g){
			SetInstruction o = doSwitch(g.getSource());
			if(o != null){
				g.setSource(o.getDest().copy());
			}	
			return null;
		}
		
		@Override
		public SetInstruction caseRetInstruction(RetInstruction g){
			if(g.getExpr() != null){
				SetInstruction o = doSwitch(g.getExpr());
				if(o != null){
					g.setExpr(o.getDest().copy());
				}
			}
			
			return null;	
		}	
	}
}
