#include "toto.h"

int toto_f1(int a, int b) {
	int res;
	res = a + b;
	return res;
}

int toto_f2(int a, int b) {
	int res;
	res = a * b;
	return res;
}

int main() {

	if(toto_f1(1,2) != 3)
		abort();

	if(toto_f2(1,2) != 2)
		abort();
}
