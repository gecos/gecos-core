#include <stdio.h>
#include <stdlib.h>

#define M  35

void test_A (int c, int d, int* uu, int e[2],int res[M][2], int opf, int optp	);
void test_B (int g, int h, int* u2, int k[2],int resl[M][2]);

int main(int argc, const char **argv) {
	FILE *outfile;
	int r1, r2, k;
	int t[4] = {25, 2, 56, 9};
	int optionFct;
	int optionPtr;

	int resA[M][2];
	int resB[M][2];

	/* check parameters */
	if (argc < 4) {
		fprintf(stderr, "Usage: %s output_file function_option pointer_option\n", argv[0]);
		exit(-1);
	}
	outfile = fopen(argv[1], "w");
	if (outfile == NULL) {
		fprintf(stderr, "couldn't open file \"%s\"!\n", argv[1]);
		exit(-2);
	}
	optionFct = atoi(argv[2]);
	optionPtr = atoi(argv[3]);

	for (k = 0; k < M; k++) {
		resA[k][0] = 0;
		resA[k][1] = 0;
	}

	for (k = 0; k < M; k++) {
		resB[k][0] = 0;
		resB[k][1] = 0;
	}

	r1 = M/2;
	r2 = M*5;


	/***********************************************************************************************
	 *  In these cases (A.i), the addition should NOT be considered as Local Common-subexpression  *
	 ***********************************************************************************************/

	test_A (r1, r2, &r1, t, resA, optionFct, optionPtr);

	/******************************************************************************************
	 *  In these cases (B.i), the addition should be considered as Local Common-subexpression  *
	 ******************************************************************************************/

	r1 = M/2;
	r2 = M*5;

	t[0] = 25;
	t[1] = 2;
	t[2] = 56;
	t[3] = 9;

	test_B (r1, r2, &r2, t, resB);

	for (k = 0; k < M; k++)
		fprintf(outfile, "A (%d) : %d %d \n", k+1, resA[k][0], resA[k][1]);

	for (k = 0; k < M; k++)
		fprintf(outfile, "B (%d) : %d %d \n", k+1, resB[k][0], resB[k][1]);

	fclose (outfile);

	return 0;
}
