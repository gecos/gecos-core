package fr.irisa.cairn.gecos.core.testframework.impl;

import static fr.irisa.cairn.gecos.core.testframework.impl.GSDefaultTestFlows.cxxFileDataTestFlow;
import static fr.irisa.cairn.gecos.core.testframework.impl.GSDefaultTestFlows.cxxFilesGroupDataTestFlow;
import static fr.irisa.cairn.gecos.core.testframework.impl.GSDefaultTestFlows.cxxMakefileDataTestFlow;
import static fr.irisa.cairn.gecos.testframework.utils.ThrowableMatchers.isAndHasCause;
import static org.hamcrest.CoreMatchers.instanceOf;

import fr.irisa.cairn.gecos.model.cdtfrontend.exception.CDTParserProblemException;
import fr.irisa.cairn.gecos.testframework.data.CxxFileData;
import fr.irisa.cairn.gecos.testframework.data.CxxFilesGroupData;
import fr.irisa.cairn.gecos.testframework.data.CxxMakefileData;
import fr.irisa.cairn.gecos.testframework.exceptions.CheckerFailure;
import fr.irisa.cairn.gecos.testframework.exceptions.CodegenFailure;
import fr.irisa.cairn.gecos.testframework.exceptions.CompilerFailure;
import fr.irisa.cairn.gecos.testframework.exceptions.CreationFailure;
import fr.irisa.cairn.gecos.testframework.exceptions.LinkerFailure;
import fr.irisa.cairn.gecos.testframework.exceptions.RunnerFailure;
import fr.irisa.cairn.gecos.testframework.s2s.TestFlowTemplate;

/**
 * A {@link TestFlowTemplate} implementation with default support 
 * for {@link CxxFileData}, {@link CxxFilesGroupData} and {@link CxxMakefileData}.
 * 
 * @see TestFlowTemplate
 * 
 * @author aelmouss
 */
public abstract class GecosS2STestTemplate<V extends GSProjectVersion> extends TestFlowTemplate<V> {
	
	/**
	 * If {@code true}, the test is skipped if a 
	 * {@link CDTParserProblemException} failure occur.
	 */
	protected boolean skipOnParsingFail;
	
	/**
	 * If {@code true}, register {@link CDTParserProblemException} as
	 * an Expected failure.
	 * In this case the test must fail with a {@link CDTParserProblemException}
	 * to be considered as successful.
	 */
	protected boolean expectParsingFail;
	
	protected boolean expectTransformFail;
	protected boolean expectCheckFail;
	protected boolean expectCodegenFail;
	protected boolean expectLinkFail;
	
	/**
	 * If {@code true}, register {@link CompilerFailure} as
	 * an Expected failure.
	 * In this case the test must fail with a {@link CompilerFailure}
	 * to be considered as successful.
	 */
	protected boolean expectCompileFail;
	
	/**
	 * If {@code true}, register {@link RunnerFailure} as
	 * an Expected failure.
	 * In this case the test must fail with a {@link RunnerFailure}
	 * to be considered as successful.
	 */
	protected boolean expectExecuteFail;

	
	@Override
	protected void configure() {
		super.configure();
		
		this.skipOnParsingFail = false;
		this.expectParsingFail = false;
		this.expectTransformFail = false;
		this.expectCheckFail = false;
		this.expectCodegenFail = false;
		this.expectLinkFail = false;
		this.expectCompileFail = false;
		this.expectExecuteFail = false;
		
		registerTestFlow(CxxFileData.class, cxxFileDataTestFlow());
		registerTestFlow(CxxFilesGroupData.class, cxxFilesGroupDataTestFlow(v -> "compile", v-> "run"));
		registerTestFlow(CxxMakefileData.class, cxxMakefileDataTestFlow(v -> "compile", v-> "run"));
	}
	
	@Override
	protected void beforeBuildTestFlow() {
		super.beforeBuildTestFlow();

		if(expectParsingFail)
			expectedExceptions.register(isAndHasCause(CreationFailure.class, CDTParserProblemException.class));
		if(skipOnParsingFail)
			skipOnExceptions.register(isAndHasCause(CreationFailure.class, CDTParserProblemException.class));
		if(expectTransformFail)
			expectedExceptions.register(instanceOf(CreationFailure.class));
		if(expectCheckFail)
			expectedExceptions.register(instanceOf(CheckerFailure.class));
		if(expectCodegenFail)
			expectedExceptions.register(instanceOf(CodegenFailure.class));
		if(expectCompileFail)
			expectedExceptions.register(instanceOf(CompilerFailure.class));
		if(expectLinkFail)
			expectedExceptions.register(instanceOf(LinkerFailure.class));
		if(expectExecuteFail)
			expectedExceptions.register(instanceOf(RunnerFailure.class));
	}
	
}
