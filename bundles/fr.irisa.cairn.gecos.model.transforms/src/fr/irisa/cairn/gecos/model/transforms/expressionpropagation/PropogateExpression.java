package fr.irisa.cairn.gecos.model.transforms.expressionpropagation;

import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import gecos.core.Procedure;
import gecos.gecosproject.GecosProject;
import gecos.instrs.Instruction;
import gecos.instrs.PhiInstruction;
import gecos.instrs.SSADefSymbol;
import gecos.instrs.SSAUseSymbol;
import gecos.instrs.SetInstruction;

/**
 * @author malle
 * This class purpose is to eliminate temporary scalar variables used in computation.
 * These temporary variables add WAW dependences when using memory based dependencies 
 * and prevent parallelization. This pass propagates the expressions to the use if the
 * temporary expression is used only once. we do not want to duplicate the computation
 * if it is used multiple times.
 */
public class PropogateExpression extends BlockInstructionSwitch<Object> {
	
	GecosProject proc;
	
	public PropogateExpression(GecosProject proc) {
		this.proc = proc;
	}
	
	public void compute() {
		for ( Procedure p : proc.listProcedures() )
			doSwitch(p);
	}
	
	public Object caseSSAUseSymbol(SSAUseSymbol object) {
		if ( !(object.getSymbol().getType().asBase() != null))
			return true;
		if ( object.getParent() instanceof PhiInstruction )
			return true;
		SSADefSymbol def = object.getDef();
		if ( def == null )
			return true;
		if ( !(def.getRoot() instanceof SetInstruction) ) {
			return true;
		}
		SetInstruction assign = (SetInstruction)def.getRoot();
		Instruction rhs =  assign.getSource();
		if ( rhs instanceof PhiInstruction ) {
			return true;
		}
		if ( def.getSSAUses().size() > 1 ) 
			return true;

	 	/* Actually propogate expression */ 
		object.getParent().replaceChild(object, rhs.copy());
		assign.getBasicBlock().removeInstruction(assign);
		return true;
	}

}