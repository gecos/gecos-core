package fr.irisa.cairn.gecos.model.modules;

import gecos.gecosproject.GecosFile;
import gecos.gecosproject.GecosHeaderDirectory;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;
import gecos.gecosproject.GecosprojectFactory;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;

@GSModule("Add source file(s) to a GecosProject.\n"
		+ "\nSee: 'CreateGecosProject' module.")
public class AddSourceToGecosProject {
	
	private static boolean debug = false;
	private static void debugln(Object o) { if (debug) System.out.println(o); }
	
	private static final List<String> sourceFileExtensionName = Arrays.asList(".c", ".cpp", ".m", ".sci", ".simple");
	
	private GecosProject project; 

	@GSModuleConstructor(
		"-arg1: GecosProject tp which sources will be added.\n"
	  + "-arg2: List of source file(s) path(s).")
	public AddSourceToGecosProject(GecosProject project ,  String... names) {
		this.project=project;
		for (String name : names) {
			try {
				File file = new File(new File(name).getCanonicalPath());
				if (file!=null && file.exists()) {
					if (file.isFile())
						addFile(file);
					else if ((file.isDirectory())) {
						addDirectory(file);
					}
				} else {
					throw new RuntimeException(" File " +file.getName()+ " does not exist");
				}
			}catch (Exception e) {
				throw new RuntimeException("Could not add file " +name+ "",e);
			}
		}
	}

	@GSModuleConstructor(
		"-arg1: GecosProject tp which sources will be added.\n"
	  + "-arg2: List of source file(s).")
	public AddSourceToGecosProject(GecosProject project ,  File... files) {
		this.project=project;
		for (File file : files) {	
			try {
				if (file!=null && file.exists()) {
					if (file.isFile())
						addFile(file);
					else if ((file.isDirectory())) {
						addDirectory(file);
					}
				} else {
					throw new IOException(
							" File " +file.getName()+ " does not exist");
				}
			}catch (Exception e) {
				throw new RuntimeException(
						"Could not add file " +file.getPath()+ "");
			}
		}
	}
	
	@GSModuleConstructor(
		"-arg1: GecosProject tp which sources will be added.\n"
	  + "-arg2: List of source file(s).")
	public AddSourceToGecosProject(GecosProject project ,  GecosFile... files) {
		this.project=project;
		for (GecosFile gecosFile : files) {
			if (gecosFile instanceof GecosSourceFile)
				this.project.getSources().add((GecosSourceFile) gecosFile);
			if (gecosFile instanceof GecosHeaderDirectory)
				this.project.getIncludes().add((GecosHeaderDirectory) gecosFile);
			
		}
	}

	private void addDirectory(File dir) {
		debugln("Visiting directory "+dir.getAbsolutePath());
		String[] children = dir.list();
		for (int i = 0; i < children.length; i++) {
			String filename= children[i];
			File file = new File(dir.getAbsolutePath()+File.separatorChar+filename);
			debugln("Visiting file "+file.getAbsolutePath());
			if (file.exists()) {
				if (file.isDirectory() && !file.getName().startsWith(".")) {
					addDirectory(file);
				} else if (file.isFile()) {
					addFile(file);
				}
			} else {
				System.err.println("Warning "+file.getAbsolutePath()+ " does not exist");
			}
		}
	}

	private void addFile(File file) {
		String fName = file.getName();
		if (isFileParsable(fName)) {
			GecosSourceFile sfile = GecosprojectFactory.eINSTANCE.createGecosSourceFile();
			sfile.setName(file.getAbsolutePath());
			project.getSources().add(sfile);
		} else {
			System.err.println("Filetype of "+fName+" not supported");
		}
	}
	
	private boolean isFileParsable(String fName) {
		for (int i = 0; i < sourceFileExtensionName.size(); i++) {
			if (fName.endsWith(sourceFileExtensionName.get(i)))
				return true;
		}
		return false;
	}

	public void compute() {
	}
}
