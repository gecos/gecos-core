#include "ProfilingEngine.h"

// not included in "ProfilingEngine.h" cause it significantly increases compilation time !
#include <random>
#include <map>
#include <vector>
#include <iostream>
#include <fstream>
#include <typeinfo>
#include <limits>
#include <float.h>
#include "io_png.h"
#include <stdint.h>


namespace profengine {


//-----------------------------------------------------------
// Variable
//-----------------------------------------------------------
	class Variable {
	protected:
		std::string m_name;
		int m_uid;

		Variable(std::string name, int uid) : m_name(name), m_uid(uid) {};

	public:
		virtual ~Variable() {};
		std::string getName() {
			return m_name;
		}
		int getUid() {
			return m_uid;
		}
		std::string toString() {
			return m_name + "_" + std::to_string(m_uid);
		}

		virtual void newSnapshot() = 0;
		virtual void writeTxt(std::fstream &fs) = 0;
		virtual void writeBin(std::fstream &fs) = 0;
    };


	template<typename T>
	class GenericVariable : public Variable {
	private:
		std::vector<T> current_snapshot;
		std::vector< std::vector<T> > snapshots; // [ snapshot values ]

	public:
		GenericVariable(std::string name, int uid):Variable(name, uid){};
		~GenericVariable(){}

		void addValue(T value) {
		#ifdef DEBUG
			   //std::cout << "  Pushing value to variable '" << toString() << "' = " << value <<std::endl;
		#endif
		   current_snapshot.push_back(value);
		}

		void newSnapshot() {
		#ifdef DEBUG
			std::cout << "Finish recording current copy values of variable '" << toString() << "'" << std::endl;
		#endif
			snapshots.push_back(current_snapshot);
			current_snapshot.clear();
		}

	   void writeTxt(std::fstream &fs) {
		   fs << "uid: " << m_uid << std::endl;
		   fs << "name: " << m_name << std::endl;
		   fs << "elemTypeIdName: " << typeid(T).name() << std::endl;
		   fs << "elemSizeBytes: " << sizeof(T) << std::endl;
		   fs << "nbSnapshots: " << snapshots.size() << std::endl;
		   fs << std::endl;

		   for(unsigned i = 0; i < snapshots.size(); i++) {
			   std::vector<T> values = snapshots[i];
			   fs << "snapshot: " << i << std::endl;
			   fs << "nbElems: " << values.size() << std::endl;
			   for(unsigned j = 0; j < values.size(); j++)
				   fs << values[j] << std::endl;
			   fs << std::endl;
		   }
	   }

	   void writeBin(std::fstream &fs) {
		   // write uid : int
		   fs.write((char*)&m_uid, sizeof(int));

		   // write name : int chars..
		   int size = m_name.size();
		   fs.write((char*)&size, sizeof(int));
		   fs.write(m_name.c_str(), size);

		   // write typeid name : int chars..
		   std::string typeidName = typeid(T).name();
		   size = typeidName.size();
		   fs.write((char*)&size, sizeof(int));
		   fs.write(typeidName.c_str(), size);

		   // write elem Size : int
		   size = sizeof(T);
		   fs.write((char*)&size, sizeof(int));

		   // write nb snaps : int
		   size = snapshots.size();
		   fs.write((char*)&size, sizeof(int));

		   for(unsigned i = 0; i < snapshots.size(); i++) {
			   std::vector<T> values = snapshots[i];
			   size = values.size();
			   fs.write((char*)&size, sizeof(int));
			   fs.write((char*)&(values[0]), size*sizeof(T));
		   }
	   }
	};


//-----------------------------------------------------------
// RandomGenerator
//-----------------------------------------------------------

	static std::default_random_engine *_RANDOM_GENERATOR = new std::default_random_engine(std::random_device()());

	class Generator {
	public:
		virtual ~Generator() {};
		virtual double generate() = 0;
	};

	class UniformRandomGenerator : public Generator {
	private:
		std::uniform_real_distribution<double> _distribution;
	public:
		UniformRandomGenerator(double minInclusive, double maxExclusive) : _distribution(minInclusive, maxExclusive) {}
		double generate() override {
			return _distribution(*_RANDOM_GENERATOR);
		}
	};

	class NormalRandomGenerator : public Generator {
	private:
		std::normal_distribution<double> _distribution;
	public:
		NormalRandomGenerator(double mean, double stddev) : _distribution(mean, stddev) {}
		double generate() override {
			return _distribution(*_RANDOM_GENERATOR);
		}
	};

	class FromFileGenerator : public Generator {
	private:
		std::string filePath;
		std::ifstream inputStream;
		int nbDims;
		int *sizes = NULL;
	public:
		FromFileGenerator(const char* inputPath) : filePath(inputPath) {
			inputStream.open(inputPath);
			inputStream >> nbDims;
			sizes = new int[nbDims];
			for(int i = 0; i < nbDims; i++) {
				inputStream >> sizes[i];
			}
		}
		~FromFileGenerator() override {
			delete [] sizes;
			sizes = NULL;
			inputStream.close();
		}
		double generate() override {
			double val;
			inputStream >> val;
			return val;
		}
	};

	// Phu 4/7/19
	class FromBinFileGenerator : public Generator {
	private:
		const char* inputPath;
		double *inputData;
		int nbDims;
		int *sizes = NULL;
		int count;
		double nbOfDims[1];
		double tmp[1];
		long unsigned length;
	public:
		FromBinFileGenerator(const char* inputPath) {
			this->inputPath = inputPath;
			length = 1;
			count = 0;

			FILE *fp;
			fp = fopen(inputPath , "rb" );

			if (fp==NULL) {
			    printf("Can not open the file: %s\n", inputPath);
			}

			if (fread(nbOfDims,sizeof(double), 1,fp) != 1) {
				printf("error :: problem occurred when reading file %s\n", inputPath);
				fclose(fp);
				exit(-1);
			}
			nbDims = (int)nbOfDims[0];
			sizes = new int[nbDims];
			for(int i = 0; i < nbDims; i++) {
				if (fread(tmp,sizeof(double),1,fp) != 1) {
					printf("error :: problem occurred when reading file %s\n", inputPath);
					fclose(fp);
					exit(-1);
				}
				sizes[i] = (int)tmp[0];
				length = length*sizes[i];
			}
			inputData = (double*)malloc(length*sizeof(double));
			if (fread(inputData, sizeof(double), length, fp) != (size_t)length) {
				printf("error :: problem occurred when reading file %s\n", inputPath);
				fclose(fp);
				exit(-1);
			}
			fclose(fp);
		}
		~FromBinFileGenerator() override {
			delete [] sizes;
			sizes = NULL;
			free(inputData);
		}
		double generate() override {
			double val = (double)inputData[count];
			count++;
			return val;
		}
	};
	// End Phu 4/7/19

	//Begin Added OS 7/3/19
	class FromRawFileGenerator : public Generator {
	private:
		const char* inputPath;
		float* inputData;
		int *sizes = NULL;
		int wh;
		int count;
		double max = FLT_MIN, max_norm = FLT_MIN;
		double min = FLT_MAX, min_norm = FLT_MAX;
	public:
		FromRawFileGenerator(const char* inputPath) {
			this->inputPath = inputPath;
			sizes = new int[2];

			int W=3968;
			int H=2976;
			long unsigned WH=W*H;
			FILE *fp;
			uint16_t *inputData_uint16_t = (uint16_t*) malloc(sizeof(uint16_t)*H*W);

		    fp = fopen(inputPath, "r");
		    if (!fp) {
		           printf("error :: %s not found\n", inputPath);
		           exit(-1);
		    }
			// read input raw image
		    if (fread(inputData_uint16_t, sizeof *inputData_uint16_t, WH, fp) != (size_t)WH) { // Intel arch is LE anyway
				printf("error :: problem occurred when reading image %s\n", inputPath);
				fclose(fp);
				exit(-1);
            }

			#ifdef DEBUG
			printf("reading image :: %s \n", inputPath);
			#endif
			fclose(fp);

			inputData = (float*) malloc(sizeof(float)*H*W);
			for (int i = 0; i < W*H; i++) {
				inputData[i] = (float) inputData_uint16_t[i];
				min = fmin(min, inputData[i]);
				max = fmax(max, inputData[i]);
				#ifdef DEBUG
				//if (i%1000 == 0) printf("input image (FromRawFileGenerator) :: %f\n", inputData[i]);
				#endif
			}
			#ifdef DEBUG
			printf("input image ::  max: %f min: %f\n", max, min);
			#endif

			free(inputData_uint16_t);

		    // variables
		    sizes[0] = W;
		    sizes[1] = H;
		    wh = sizes[0]*sizes[1];
		    count = 0;
		}
		~FromRawFileGenerator() override {
			delete [] sizes;
			sizes = NULL;
			free(inputData);
			#ifdef DEBUG
			printf("normalized input image ::  max_norm: %f min_norm: %f\n", max_norm, min_norm);
			#endif

		}
		double generate() override {
			double val = (double)inputData[count];
			//normalize to between 0-1
			val = val / 1024;
			min_norm = fmin(min_norm, val);
			max_norm = fmax(max_norm, val);
			#ifdef DEBUG
			//if (count%1000 == 0) printf("normalized read value :: %f \n", val);
			#endif
			count++;
			return val;
		}
	}; //End Added OS 7/3/19

	class FromPngFileGenerator : public Generator {
	private:
		const char* inputPath;
		float* inputData;
		int *sizes = NULL;
		int wh;
		int count;
		double max = FLT_MIN, max_norm = FLT_MIN; //Added OS 7/3/19
		double min = FLT_MAX, min_norm = FLT_MAX; //Added OS 7/3/19
	public:
		FromPngFileGenerator(const char* inputPath) {
			this->inputPath = inputPath;
			sizes = new int[2];

		    // read input
		    size_t nx,ny,nc;
		    inputData = NULL;
		    inputData = io_png_read_f32(inputPath, &nx, &ny, &nc);
		    if (!inputData) {
		        printf("error :: %s not found  or not a correct png image \n", inputPath);
		        exit(-1);
		    }

		    // variables
		    sizes[0] = (int) nx;
		    sizes[1] = (int) ny;
		    wh = sizes[0]*sizes[1];
		    count = 0;

		    //Begin Added OS 7/3/19
			for (int i = 0; i < wh; i++) {
				min = fmin(min, inputData[i]);
				max = fmax(max, inputData[i]);
				#ifdef DEBUG
				//if (i%1000 == 0) printf("input image (FromPngFileGenerator) :: %f\n", inputData[i]);
				#endif
			}
			#ifdef DEBUG
			printf("input image ::  max: %f min: %f\n", max, min);
			#endif
			//End Added OS 7/3/19
		}
		~FromPngFileGenerator() override {
			delete [] sizes;
			sizes = NULL;
			free(inputData);
			//Begin Added OS 7/3/19
			#ifdef DEBUG
			printf("normalized input image ::  max_norm: %f min_norm: %f\n", max_norm, min_norm);
			#endif
			//End Added OS 7/3/19
		}
		double generate() override {
			double val = (double)inputData[count];
			//normalize to between 0-1
			val = val / 255.0;
			//Begin Added OS 7/3/19
			min_norm = fmin(min_norm, val);
			max_norm = fmax(max_norm, val);
			#ifdef DEBUG
			//if (count%1000 == 0) printf("normalized read value :: %f \n", val);
			#endif
			//End Added OS 7/3/19
			count++;
			return val;
		}
	};

	class FromVarGenerator : public Generator {
	private:
		double _var;
	public:
		FromVarGenerator(double var) : _var(var) {}
		double generate() override {
			return _var;
		}
	};


//-----------------------------------------------------------
// ProfilingEngine
//-----------------------------------------------------------

	class VariablesMap {
	private:
		std::map<int, Variable*> internal_map;
	public:
		~VariablesMap() {
			for(auto const& entry : internal_map)
				delete entry.second;
		}

		friend class ProfilingEngine;
	};



	ProfilingEngine::ProfilingEngine() {
		m_values = new VariablesMap();
		generator = new UniformRandomGenerator(0.0, 1.0);
	}

	ProfilingEngine::~ProfilingEngine() {
		delete _RANDOM_GENERATOR;
		delete generator;
		delete m_values;
	}


//	template<typename T>
	void ProfilingEngine::pushValue(int uid, const char* name, _DOUBLE_TYPE_ value) {
		Variable* var = getVariable(uid);

		GenericVariable<_DOUBLE_TYPE_> *gvar;
		if(var == NULL) {
			gvar = new GenericVariable<_DOUBLE_TYPE_>(std::string(name), uid);
			saveVariable(uid, gvar);
		}
		else{
			gvar = dynamic_cast< GenericVariable<_DOUBLE_TYPE_>* >(var);
			if(gvar == NULL) {
				std::cerr <<"The variable '" << name << "' was saved earlier as a different element type" << std::endl;
				exit(1);
			}
		}

		gvar->addValue(value);
	}

	void ProfilingEngine::saveSnapshot(int uid) {
		Variable* var = getVariable(uid);
		if(var == NULL) {
			fprintf(stderr, "No variable with uid '%d' was registered!", uid);
			exit(1);
		}

		var->newSnapshot();
	}

	Variable* ProfilingEngine::getVariable(int uid) {
		return m_values->internal_map[uid];
	}

	void ProfilingEngine::saveVariable(int uid, Variable *variable) {
		if(m_values->internal_map[uid] != NULL){
			std::cerr << "Variable already created. Problem during the construction of the manager variable mapping" << std::endl;
			exit(1);
		}

		m_values->internal_map[uid] = variable;
	}

	void ProfilingEngine::writeTxt(std::string outputFolderPath) {
		std::fstream fs;
		std::string pathOutput;

		for(auto const& entry : m_values->internal_map) {
			Variable* var = entry.second;

			pathOutput = outputFolderPath +"/" + var->toString() + ".txt";
			fs.open(pathOutput.c_str(), std::ios::out);
			if(!fs.is_open()){
				std::cerr << "File to write simulation result can not be open: " << pathOutput << std::endl;
				exit(1);
			}

			var->writeTxt(fs);

			fs.close();
		}
	}

	void ProfilingEngine::writeBin(std::string outputFolderPath) {
		std::fstream fs;
		std::string pathOutput;

		for(auto const& entry : m_values->internal_map) {
			Variable* var = entry.second;

			pathOutput = outputFolderPath +"/" + var->toString() + ".bin";
			fs.open(pathOutput.c_str(), std::ios::out | std::ios::binary);
			if(!fs.is_open()){
				std::cerr << "File to write simulation result can not be open: " << pathOutput << std::endl;
				exit(1);
			}

			var->writeBin(fs);

			fs.close();
		}
	}


    void ProfilingEngine::setGeneratorUniformDoubleDistribution(double minInclusive, double maxExclusive) {
    	delete generator; // delete previous generator
    	generator = new UniformRandomGenerator(minInclusive, maxExclusive);
    }

    void ProfilingEngine::setGeneratorNormalDoubleDistribution(double mean, double stddev) {
    	delete generator; // delete previous generator
    	generator = new NormalRandomGenerator(mean, stddev);
    }

    void ProfilingEngine::setGeneratorFromFile(const char *filePath) {
    	delete generator; // delete previous generator

    	bool isPng = false;
    	bool isRaw = false; //Added OS 7/3/19
    	bool isBin = false; //Added Phu 4/7/19
    	{
    		size_t nl = strlen(filePath), el = strlen(".png"); //Added OS 7/3/19

        	isPng = nl >= el && !strcmp(filePath + nl - el, ".png"); //Modifed OS 7/3/19
        	isRaw = nl >= el && !strcmp(filePath + nl - el, ".raw"); //Added OS 7/3/19
        	isBin = nl >= el && !strcmp(filePath + nl - el, ".bin"); //Added Phu 4/7/19
    	}
    	if (isPng) {
    		generator = new FromPngFileGenerator(filePath);
    	}
    	else if (isRaw) { //Added OS 7/3/19
    		generator = new FromRawFileGenerator(filePath);
    	}
    	else if (isBin) { //Added Phu 4/7/19
    		generator = new FromBinFileGenerator(filePath);
    	}
		else {
    		generator = new FromFileGenerator(filePath);
    	}
    }

    void ProfilingEngine::setGeneratorFromVar(double var) {
    	delete generator; // delete previous generator
    	generator = new FromVarGenerator(var);
    }

//	 void ProfilingEngine::setGeneratorUniformIntDistribution(int minInclusive, int maxInclusive) {
//		std::uniform_int_distribution<int> _distribution(minInclusive, maxInclusive);
//	 }
//
//    void ProfilingEngine::setGeneratorNormalIntDistribution(double mean, double stddev) {
//    	std::normal_distribution<int> _distribution(mean, stddev);
//    }

	void ProfilingEngine::setGeneratorSeed(int seed) {
		delete _RANDOM_GENERATOR;
		_RANDOM_GENERATOR = new std::default_random_engine(seed);
	}

    double ProfilingEngine::generate() {
    	return generator->generate();
    }
}
