#include "ProfilingEngine.h"

// not included in "ProfilingEngine.h" cause it significantly increases compilation time !
#include <random>
#include <map>


namespace profengine {


//-----------------------------------------------------------
// Variable
//-----------------------------------------------------------
	std::string Variable::getName() {
		return m_name;
	}

	int Variable::getUid() {
		return m_uid;
	}

	std::string Variable::toString() {
		return m_name + "_" + std::to_string(m_uid);
	}


//-----------------------------------------------------------
// RandomGenerator
//-----------------------------------------------------------

	static std::default_random_engine *_GENERATOR = new std::default_random_engine(std::random_device()());

	class RandomGenerator {
	protected:

	public:
		virtual ~RandomGenerator() {};
		virtual double generate() = 0;
	};

	class UniformRandomGenerator : public RandomGenerator {
	private:
		std::uniform_real_distribution<double> _distribution;
	public:
		UniformRandomGenerator(double minInclusive, double maxExclusive) : _distribution(minInclusive, maxExclusive) {}
		double generate() {
			return _distribution(*_GENERATOR);
		}
	};

	class NormalRandomGenerator : public RandomGenerator {
	private:
		std::normal_distribution<double> _distribution;
	public:
		NormalRandomGenerator(double mean, double stddev) : _distribution(mean, stddev) {}
		double generate() {
			return _distribution(*_GENERATOR);
		}
	};



//-----------------------------------------------------------
// ProfilingEngine
//-----------------------------------------------------------

	class VariablesMap {
	private:
		std::map<int, Variable*> internal_map;
	public:
		~VariablesMap() {
			for(auto const& entry : internal_map)
				delete entry.second;
		}

		friend class ProfilingEngine;
	};



	ProfilingEngine::ProfilingEngine() {
		m_values = new VariablesMap();
		random_generator = new UniformRandomGenerator(0.0, 1.0);
	}

	ProfilingEngine::~ProfilingEngine() {
		delete _GENERATOR;
		delete random_generator;
		delete m_values;
	}

	Variable* ProfilingEngine::getVariable(int uid) {
		return m_values->internal_map[uid];
	}

	void ProfilingEngine::saveVariable(int uid, Variable *variable) {
		if(m_values->internal_map[uid] != NULL){
			std::cerr << "Variable already created. Problem during the construction of the manager variable mapping" << std::endl;
			exit(1);
		}

		m_values->internal_map[uid] = variable;
	}

	void ProfilingEngine::writeHuman(std::string outputFolderPath) {
		std::fstream fs;
		std::string pathOutput;

		for(auto const& entry : m_values->internal_map){
			Variable* var = entry.second;

			pathOutput = outputFolderPath +"/" + var->toString() + ".txt";
			fs.open(pathOutput.c_str(), std::fstream::out);
			if(!fs.is_open()){
				std::cerr << "File to write simulation result can not be open: " << pathOutput << std::endl;
				exit(1);
			}

			var->writeHuman(fs);

			fs.close();
		}
	}


    void ProfilingEngine::setGeneratorUniformDoubleDistribution(double minInclusive, double maxExclusive) {
    	delete random_generator; // delete previous generator
    	random_generator = new UniformRandomGenerator(minInclusive, maxExclusive);
    }

    void ProfilingEngine::setGeneratorNormalDoubleDistribution(double mean, double stddev) {
    	delete random_generator; // delete previous generator
    	random_generator = new NormalRandomGenerator(mean, stddev);
    }


//	 void ProfilingEngine::setGeneratorUniformIntDistribution(int minInclusive, int maxInclusive) {
//		std::uniform_int_distribution<int> _distribution(minInclusive, maxInclusive);
//	 }
//
//    void ProfilingEngine::setGeneratorNormalIntDistribution(double mean, double stddev) {
//    	std::normal_distribution<int> _distribution(mean, stddev);
//    }

	void ProfilingEngine::setGeneratorSeed(int seed) {
//		random_generator->setSeed(seed);
		_GENERATOR = new std::default_random_engine(seed);
	}

    double ProfilingEngine::generate() {
    	return random_generator->generate();
    }
}
