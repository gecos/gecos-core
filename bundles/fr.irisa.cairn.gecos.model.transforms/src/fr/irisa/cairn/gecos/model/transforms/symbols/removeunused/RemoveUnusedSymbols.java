package fr.irisa.cairn.gecos.model.transforms.symbols.removeunused;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import fr.irisa.cairn.gecos.model.tools.switches.SymbolSwitch;
import fr.irisa.cairn.gecos.model.transforms.symbols.removeunused.internal.AbstractGecosTransformModule;
import fr.irisa.r2d2.gecos.framework.GSModule;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.instrs.SymbolInstruction;

@GSModule("Removes all unused Symbols. That is all symbols \n"
		+ "which are never used in an instruction of the current ProcedureSet. \n"
		+ "When used with an identifier name as second argument, the pass also removes "
		+ "all the procedures/functions that are never called in the project. To make sure "
		+ "the main/top function is not removed, its name has to ben passed as parameter."
		+ "Note that the pass does not use the callgraph, and does not perform a real slicing "
		+ "of the program.")
public class RemoveUnusedSymbols extends AbstractGecosTransformModule {
	
	String mainFunc;
	
	public RemoveUnusedSymbols(GecosProject project) {
		super(project);
		this.mainFunc= "*";
	}

	public RemoveUnusedSymbols(GecosProject project, String mainFunc) {
		super(project);
		this.mainFunc= mainFunc;
	}

	public RemoveUnusedSymbols(ProcedureSet ps) {
		super(ps);
		this.mainFunc= "*";
	}

	@Override
	public void compute() {
		super.compute();
	}

	public void computeProcedureSet() {
		DeclaredSymbolAnalyzer decl = new DeclaredSymbolAnalyzer(ps);
		UsedSymbolAnalyzer used = new UsedSymbolAnalyzer(ps);
		List<Symbol> unusedList = decl.compute();
		unusedList.removeAll(used.compute());
		for(Symbol symbol: unusedList) {
			debug("Removing symbol "+symbol+ ", as it is not used in current ProcedureSet");
			Scope s = symbol.getContainingScope();
			if(s !=null) {
				if(symbol instanceof ProcedureSymbol) {
					System.out.println("Removing "+symbol);
					if(!(mainFunc.equals("*") || mainFunc.equals(symbol.getName()))) {
						s.getSymbols().remove(symbol);
					}
				}
			} else{
				debug("Symbol "+symbol.getName()+" has no attached scope !");
				//throw new UnsupportedOperationException("Symbol "+symbol.getName()+" has no attached scope !");
			}
		}
	}

	private static class UsedSymbolAnalyzer extends BlockInstructionSwitch<Object> { 
		List<Symbol> usedList;
		ProcedureSet ps;
		
		public UsedSymbolAnalyzer(ProcedureSet ps) {
			usedList = new ArrayList<Symbol>();
			this.ps=ps;
		}

		public List<Symbol> compute() {
			doSwitch(ps);
			return usedList;
		}
		
		@Override
		public Object caseSymbolInstruction(SymbolInstruction s) {
			usedList.add(s.getSymbol());
			return super.caseSymbolInstruction(s);
		}
	}

	private static class DeclaredSymbolAnalyzer extends SymbolSwitch<Object> {
		List<Symbol> declaredList;
		ProcedureSet ps;
		
		public DeclaredSymbolAnalyzer(ProcedureSet ps) {
			declaredList = new ArrayList<Symbol>();
			this.ps=ps;
		}

		public List<Symbol> compute() {
			doSwitch(ps);
			return declaredList;
		}

		@Override
		public Object caseSymbol(Symbol symbol) {
			declaredList.add(symbol);
			return super.caseSymbol(symbol);
		}
	}


	
	
}
