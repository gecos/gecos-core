/**
 */
package profiling.frontend.directives;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see profiling.frontend.directives.DirectivesPackage
 * @generated
 */
public interface DirectivesFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DirectivesFactory eINSTANCE = profiling.frontend.directives.impl.DirectivesFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Size Directive</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Size Directive</em>'.
	 * @generated
	 */
	SizeDirective createSizeDirective();

	/**
	 * Returns a new object of class '<em>Save Directive</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Save Directive</em>'.
	 * @generated
	 */
	SaveDirective createSaveDirective();

	/**
	 * Returns a new object of class '<em>Inject Directive</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Inject Directive</em>'.
	 * @generated
	 */
	InjectDirective createInjectDirective();

	/**
	 * Returns a new object of class '<em>From File Directive</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>From File Directive</em>'.
	 * @generated
	 */
	FromFileDirective createFromFileDirective();

	/**
	 * Returns a new object of class '<em>From Variable Directive</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>From Variable Directive</em>'.
	 * @generated
	 */
	FromVariableDirective createFromVariableDirective();

	/**
	 * Returns a new object of class '<em>Random Uniform Directive</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Random Uniform Directive</em>'.
	 * @generated
	 */
	RandomUniformDirective createRandomUniformDirective();

	/**
	 * Returns a new object of class '<em>Random Normal Directive</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Random Normal Directive</em>'.
	 * @generated
	 */
	RandomNormalDirective createRandomNormalDirective();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	DirectivesPackage getDirectivesPackage();

} //DirectivesFactory
