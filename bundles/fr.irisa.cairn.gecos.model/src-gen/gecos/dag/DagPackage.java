/**
 */
package gecos.dag;

import gecos.annotations.AnnotationsPackage;

import gecos.core.CorePackage;

import gecos.instrs.InstrsPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see gecos.dag.DagFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel modelName='Gecos' modelPluginClass='' creationCommands='false' creationIcons='false' interfaceNamePattern='' importerID='org.eclipse.emf.importer.ecore' operationReflection='false' basePackage='gecos'"
 * @generated
 */
public interface DagPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "dag";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.gecos.org/dag";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "dag";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DagPackage eINSTANCE = gecos.dag.impl.DagPackageImpl.init();

	/**
	 * The meta object id for the '{@link gecos.dag.DagVisitor <em>Visitor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.DagVisitor
	 * @see gecos.dag.impl.DagPackageImpl#getDagVisitor()
	 * @generated
	 */
	int DAG_VISITOR = 0;

	/**
	 * The number of structural features of the '<em>Visitor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VISITOR_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link gecos.dag.DagVisitable <em>Visitable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.DagVisitable
	 * @see gecos.dag.impl.DagPackageImpl#getDagVisitable()
	 * @generated
	 */
	int DAG_VISITABLE = 1;

	/**
	 * The number of structural features of the '<em>Visitable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VISITABLE_FEATURE_COUNT = CorePackage.GECOS_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGInstructionImpl <em>DAG Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGInstructionImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGInstruction()
	 * @generated
	 */
	int DAG_INSTRUCTION = 2;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_INSTRUCTION__ANNOTATIONS = InstrsPackage.INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_INSTRUCTION__INTERNAL_CACHED_TYPE = InstrsPackage.INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_INSTRUCTION__NODES = InstrsPackage.INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_INSTRUCTION__EDGES = InstrsPackage.INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_INSTRUCTION__IDENT = InstrsPackage.INSTRUCTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Source Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_INSTRUCTION__SOURCE_NODES = InstrsPackage.INSTRUCTION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Sink Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_INSTRUCTION__SINK_NODES = InstrsPackage.INSTRUCTION_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>DAG Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_INSTRUCTION_FEATURE_COUNT = InstrsPackage.INSTRUCTION_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGNodeImpl <em>DAG Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGNode()
	 * @generated
	 */
	int DAG_NODE = 3;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NODE__ANNOTATIONS = AnnotationsPackage.ANNOTATED_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NODE__TYPE = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NODE__OUTPUTS = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NODE__PARENT = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NODE__INPUTS = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NODE__IDENT = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>DAG Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NODE_FEATURE_COUNT = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGEdgeImpl <em>DAG Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGEdgeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGEdge()
	 * @generated
	 */
	int DAG_EDGE = 4;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_EDGE__ANNOTATIONS = DAG_VISITABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Src</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_EDGE__SRC = DAG_VISITABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Sink</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_EDGE__SINK = DAG_VISITABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_EDGE__PARENT = DAG_VISITABLE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>DAG Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_EDGE_FEATURE_COUNT = DAG_VISITABLE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGPortImpl <em>DAG Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGPortImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGPort()
	 * @generated
	 */
	int DAG_PORT = 5;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_PORT__IDENT = DAG_VISITABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>DAG Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_PORT_FEATURE_COUNT = DAG_VISITABLE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGInPortImpl <em>DAG In Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGInPortImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGInPort()
	 * @generated
	 */
	int DAG_IN_PORT = 6;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_IN_PORT__IDENT = DAG_PORT__IDENT;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_IN_PORT__SOURCE = DAG_PORT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>DAG In Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_IN_PORT_FEATURE_COUNT = DAG_PORT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGOutPortImpl <em>DAG Out Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGOutPortImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGOutPort()
	 * @generated
	 */
	int DAG_OUT_PORT = 7;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_OUT_PORT__IDENT = DAG_PORT__IDENT;

	/**
	 * The feature id for the '<em><b>Sinks</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_OUT_PORT__SINKS = DAG_PORT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>DAG Out Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_OUT_PORT_FEATURE_COUNT = DAG_PORT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGImmNodeImpl <em>DAG Imm Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGImmNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGImmNode()
	 * @generated
	 */
	int DAG_IMM_NODE = 8;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_IMM_NODE__ANNOTATIONS = DAG_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_IMM_NODE__TYPE = DAG_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_IMM_NODE__OUTPUTS = DAG_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_IMM_NODE__PARENT = DAG_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_IMM_NODE__INPUTS = DAG_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_IMM_NODE__IDENT = DAG_NODE__IDENT;

	/**
	 * The number of structural features of the '<em>DAG Imm Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_IMM_NODE_FEATURE_COUNT = DAG_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGSymbolNodeImpl <em>DAG Symbol Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGSymbolNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGSymbolNode()
	 * @generated
	 */
	int DAG_SYMBOL_NODE = 9;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_SYMBOL_NODE__ANNOTATIONS = DAG_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_SYMBOL_NODE__TYPE = DAG_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_SYMBOL_NODE__OUTPUTS = DAG_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_SYMBOL_NODE__PARENT = DAG_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_SYMBOL_NODE__INPUTS = DAG_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_SYMBOL_NODE__IDENT = DAG_NODE__IDENT;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_SYMBOL_NODE__NAME = DAG_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_SYMBOL_NODE__SYMBOL = DAG_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>DAG Symbol Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_SYMBOL_NODE_FEATURE_COUNT = DAG_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGCallNodeImpl <em>DAG Call Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGCallNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGCallNode()
	 * @generated
	 */
	int DAG_CALL_NODE = 10;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_CALL_NODE__ANNOTATIONS = DAG_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_CALL_NODE__TYPE = DAG_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_CALL_NODE__OUTPUTS = DAG_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_CALL_NODE__PARENT = DAG_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_CALL_NODE__INPUTS = DAG_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_CALL_NODE__IDENT = DAG_NODE__IDENT;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_CALL_NODE__NAME = DAG_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>DAG Call Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_CALL_NODE_FEATURE_COUNT = DAG_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGArrayValueNodeImpl <em>DAG Array Value Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGArrayValueNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGArrayValueNode()
	 * @generated
	 */
	int DAG_ARRAY_VALUE_NODE = 11;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_ARRAY_VALUE_NODE__ANNOTATIONS = DAG_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_ARRAY_VALUE_NODE__TYPE = DAG_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_ARRAY_VALUE_NODE__OUTPUTS = DAG_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_ARRAY_VALUE_NODE__PARENT = DAG_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_ARRAY_VALUE_NODE__INPUTS = DAG_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_ARRAY_VALUE_NODE__IDENT = DAG_NODE__IDENT;

	/**
	 * The number of structural features of the '<em>DAG Array Value Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_ARRAY_VALUE_NODE_FEATURE_COUNT = DAG_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGOpNodeImpl <em>DAG Op Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGOpNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGOpNode()
	 * @generated
	 */
	int DAG_OP_NODE = 12;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_OP_NODE__ANNOTATIONS = DAG_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_OP_NODE__TYPE = DAG_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_OP_NODE__OUTPUTS = DAG_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_OP_NODE__PARENT = DAG_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_OP_NODE__INPUTS = DAG_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_OP_NODE__IDENT = DAG_NODE__IDENT;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_OP_NODE__OPCODE = DAG_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>DAG Op Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_OP_NODE_FEATURE_COUNT = DAG_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGOutNodeImpl <em>DAG Out Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGOutNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGOutNode()
	 * @generated
	 */
	int DAG_OUT_NODE = 13;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_OUT_NODE__ANNOTATIONS = DAG_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_OUT_NODE__TYPE = DAG_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_OUT_NODE__OUTPUTS = DAG_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_OUT_NODE__PARENT = DAG_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_OUT_NODE__INPUTS = DAG_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_OUT_NODE__IDENT = DAG_NODE__IDENT;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_OUT_NODE__SYMBOL = DAG_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>DAG Out Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_OUT_NODE_FEATURE_COUNT = DAG_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGControlEdgeImpl <em>DAG Control Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGControlEdgeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGControlEdge()
	 * @generated
	 */
	int DAG_CONTROL_EDGE = 14;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_CONTROL_EDGE__ANNOTATIONS = DAG_EDGE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Src</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_CONTROL_EDGE__SRC = DAG_EDGE__SRC;

	/**
	 * The feature id for the '<em><b>Sink</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_CONTROL_EDGE__SINK = DAG_EDGE__SINK;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_CONTROL_EDGE__PARENT = DAG_EDGE__PARENT;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_CONTROL_EDGE__TYPE = DAG_EDGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>DAG Control Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_CONTROL_EDGE_FEATURE_COUNT = DAG_EDGE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGDataEdgeImpl <em>DAG Data Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGDataEdgeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGDataEdge()
	 * @generated
	 */
	int DAG_DATA_EDGE = 15;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_DATA_EDGE__ANNOTATIONS = DAG_EDGE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Src</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_DATA_EDGE__SRC = DAG_EDGE__SRC;

	/**
	 * The feature id for the '<em><b>Sink</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_DATA_EDGE__SINK = DAG_EDGE__SINK;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_DATA_EDGE__PARENT = DAG_EDGE__PARENT;

	/**
	 * The number of structural features of the '<em>DAG Data Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_DATA_EDGE_FEATURE_COUNT = DAG_EDGE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGNumberedSymbolNodeImpl <em>DAG Numbered Symbol Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGNumberedSymbolNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGNumberedSymbolNode()
	 * @generated
	 */
	int DAG_NUMBERED_SYMBOL_NODE = 16;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NUMBERED_SYMBOL_NODE__ANNOTATIONS = DAG_SYMBOL_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NUMBERED_SYMBOL_NODE__TYPE = DAG_SYMBOL_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NUMBERED_SYMBOL_NODE__OUTPUTS = DAG_SYMBOL_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NUMBERED_SYMBOL_NODE__PARENT = DAG_SYMBOL_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NUMBERED_SYMBOL_NODE__INPUTS = DAG_SYMBOL_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NUMBERED_SYMBOL_NODE__IDENT = DAG_SYMBOL_NODE__IDENT;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NUMBERED_SYMBOL_NODE__NAME = DAG_SYMBOL_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NUMBERED_SYMBOL_NODE__SYMBOL = DAG_SYMBOL_NODE__SYMBOL;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NUMBERED_SYMBOL_NODE__NUMBER = DAG_SYMBOL_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Symbol Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NUMBERED_SYMBOL_NODE__SYMBOL_DEFINITION = DAG_SYMBOL_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>DAG Numbered Symbol Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NUMBERED_SYMBOL_NODE_FEATURE_COUNT = DAG_SYMBOL_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGNumberedOutNodeImpl <em>DAG Numbered Out Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGNumberedOutNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGNumberedOutNode()
	 * @generated
	 */
	int DAG_NUMBERED_OUT_NODE = 17;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NUMBERED_OUT_NODE__ANNOTATIONS = DAG_OUT_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NUMBERED_OUT_NODE__TYPE = DAG_OUT_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NUMBERED_OUT_NODE__OUTPUTS = DAG_OUT_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NUMBERED_OUT_NODE__PARENT = DAG_OUT_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NUMBERED_OUT_NODE__INPUTS = DAG_OUT_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NUMBERED_OUT_NODE__IDENT = DAG_OUT_NODE__IDENT;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NUMBERED_OUT_NODE__SYMBOL = DAG_OUT_NODE__SYMBOL;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NUMBERED_OUT_NODE__NUMBER = DAG_OUT_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>DAG Numbered Out Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_NUMBERED_OUT_NODE_FEATURE_COUNT = DAG_OUT_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGIntImmNodeImpl <em>DAG Int Imm Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGIntImmNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGIntImmNode()
	 * @generated
	 */
	int DAG_INT_IMM_NODE = 18;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_INT_IMM_NODE__ANNOTATIONS = DAG_IMM_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_INT_IMM_NODE__TYPE = DAG_IMM_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_INT_IMM_NODE__OUTPUTS = DAG_IMM_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_INT_IMM_NODE__PARENT = DAG_IMM_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_INT_IMM_NODE__INPUTS = DAG_IMM_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_INT_IMM_NODE__IDENT = DAG_IMM_NODE__IDENT;

	/**
	 * The feature id for the '<em><b>Int Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_INT_IMM_NODE__INT_VALUE = DAG_IMM_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_INT_IMM_NODE__VALUE = DAG_IMM_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>DAG Int Imm Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_INT_IMM_NODE_FEATURE_COUNT = DAG_IMM_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGFloatImmNodeImpl <em>DAG Float Imm Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGFloatImmNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGFloatImmNode()
	 * @generated
	 */
	int DAG_FLOAT_IMM_NODE = 19;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_FLOAT_IMM_NODE__ANNOTATIONS = DAG_IMM_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_FLOAT_IMM_NODE__TYPE = DAG_IMM_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_FLOAT_IMM_NODE__OUTPUTS = DAG_IMM_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_FLOAT_IMM_NODE__PARENT = DAG_IMM_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_FLOAT_IMM_NODE__INPUTS = DAG_IMM_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_FLOAT_IMM_NODE__IDENT = DAG_IMM_NODE__IDENT;

	/**
	 * The feature id for the '<em><b>Float Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_FLOAT_IMM_NODE__FLOAT_VALUE = DAG_IMM_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_FLOAT_IMM_NODE__VALUE = DAG_IMM_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>DAG Float Imm Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_FLOAT_IMM_NODE_FEATURE_COUNT = DAG_IMM_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGStringImmNodeImpl <em>DAG String Imm Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGStringImmNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGStringImmNode()
	 * @generated
	 */
	int DAG_STRING_IMM_NODE = 20;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_STRING_IMM_NODE__ANNOTATIONS = DAG_IMM_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_STRING_IMM_NODE__TYPE = DAG_IMM_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_STRING_IMM_NODE__OUTPUTS = DAG_IMM_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_STRING_IMM_NODE__PARENT = DAG_IMM_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_STRING_IMM_NODE__INPUTS = DAG_IMM_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_STRING_IMM_NODE__IDENT = DAG_IMM_NODE__IDENT;

	/**
	 * The feature id for the '<em><b>String Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_STRING_IMM_NODE__STRING_VALUE = DAG_IMM_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>DAG String Imm Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_STRING_IMM_NODE_FEATURE_COUNT = DAG_IMM_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGArrayNodeImpl <em>DAG Array Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGArrayNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGArrayNode()
	 * @generated
	 */
	int DAG_ARRAY_NODE = 21;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_ARRAY_NODE__ANNOTATIONS = DAG_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_ARRAY_NODE__TYPE = DAG_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_ARRAY_NODE__OUTPUTS = DAG_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_ARRAY_NODE__PARENT = DAG_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_ARRAY_NODE__INPUTS = DAG_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_ARRAY_NODE__IDENT = DAG_NODE__IDENT;

	/**
	 * The number of structural features of the '<em>DAG Array Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_ARRAY_NODE_FEATURE_COUNT = DAG_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGSimpleArrayNodeImpl <em>DAG Simple Array Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGSimpleArrayNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGSimpleArrayNode()
	 * @generated
	 */
	int DAG_SIMPLE_ARRAY_NODE = 22;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_SIMPLE_ARRAY_NODE__ANNOTATIONS = DAG_SYMBOL_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_SIMPLE_ARRAY_NODE__TYPE = DAG_SYMBOL_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_SIMPLE_ARRAY_NODE__OUTPUTS = DAG_SYMBOL_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_SIMPLE_ARRAY_NODE__PARENT = DAG_SYMBOL_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_SIMPLE_ARRAY_NODE__INPUTS = DAG_SYMBOL_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_SIMPLE_ARRAY_NODE__IDENT = DAG_SYMBOL_NODE__IDENT;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_SIMPLE_ARRAY_NODE__NAME = DAG_SYMBOL_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_SIMPLE_ARRAY_NODE__SYMBOL = DAG_SYMBOL_NODE__SYMBOL;

	/**
	 * The number of structural features of the '<em>DAG Simple Array Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_SIMPLE_ARRAY_NODE_FEATURE_COUNT = DAG_SYMBOL_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGFieldInstructionImpl <em>DAG Field Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGFieldInstructionImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGFieldInstruction()
	 * @generated
	 */
	int DAG_FIELD_INSTRUCTION = 23;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_FIELD_INSTRUCTION__ANNOTATIONS = DAG_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_FIELD_INSTRUCTION__TYPE = DAG_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_FIELD_INSTRUCTION__OUTPUTS = DAG_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_FIELD_INSTRUCTION__PARENT = DAG_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_FIELD_INSTRUCTION__INPUTS = DAG_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_FIELD_INSTRUCTION__IDENT = DAG_NODE__IDENT;

	/**
	 * The feature id for the '<em><b>Field</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_FIELD_INSTRUCTION__FIELD = DAG_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>DAG Field Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_FIELD_INSTRUCTION_FEATURE_COUNT = DAG_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGJumpNodeImpl <em>DAG Jump Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGJumpNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGJumpNode()
	 * @generated
	 */
	int DAG_JUMP_NODE = 24;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_JUMP_NODE__ANNOTATIONS = DAG_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_JUMP_NODE__TYPE = DAG_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_JUMP_NODE__OUTPUTS = DAG_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_JUMP_NODE__PARENT = DAG_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_JUMP_NODE__INPUTS = DAG_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_JUMP_NODE__IDENT = DAG_NODE__IDENT;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_JUMP_NODE__LABEL = DAG_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>DAG Jump Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_JUMP_NODE_FEATURE_COUNT = DAG_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGPatternNodeImpl <em>DAG Pattern Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGPatternNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGPatternNode()
	 * @generated
	 */
	int DAG_PATTERN_NODE = 25;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_PATTERN_NODE__ANNOTATIONS = DAG_OP_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_PATTERN_NODE__TYPE = DAG_OP_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_PATTERN_NODE__OUTPUTS = DAG_OP_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_PATTERN_NODE__PARENT = DAG_OP_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_PATTERN_NODE__INPUTS = DAG_OP_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_PATTERN_NODE__IDENT = DAG_OP_NODE__IDENT;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_PATTERN_NODE__OPCODE = DAG_OP_NODE__OPCODE;

	/**
	 * The feature id for the '<em><b>Pattern</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_PATTERN_NODE__PATTERN = DAG_OP_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Inputs Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_PATTERN_NODE__INPUTS_MAP = DAG_OP_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Outputs Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_PATTERN_NODE__OUTPUTS_MAP = DAG_OP_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>DAG Pattern Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_PATTERN_NODE_FEATURE_COUNT = DAG_OP_NODE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.InPortMapImpl <em>In Port Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.InPortMapImpl
	 * @see gecos.dag.impl.DagPackageImpl#getInPortMap()
	 * @generated
	 */
	int IN_PORT_MAP = 26;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PORT_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PORT_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>In Port Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PORT_MAP_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.OutPortMapImpl <em>Out Port Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.OutPortMapImpl
	 * @see gecos.dag.impl.DagPackageImpl#getOutPortMap()
	 * @generated
	 */
	int OUT_PORT_MAP = 27;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_PORT_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_PORT_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Out Port Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_PORT_MAP_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGVectorNodeImpl <em>DAG Vector Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGVectorNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGVectorNode()
	 * @generated
	 */
	int DAG_VECTOR_NODE = 28;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_NODE__ANNOTATIONS = DAG_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_NODE__TYPE = DAG_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_NODE__OUTPUTS = DAG_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_NODE__PARENT = DAG_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_NODE__INPUTS = DAG_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_NODE__IDENT = DAG_NODE__IDENT;

	/**
	 * The number of structural features of the '<em>DAG Vector Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_NODE_FEATURE_COUNT = DAG_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGVectorArrayAccessNodeImpl <em>DAG Vector Array Access Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGVectorArrayAccessNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGVectorArrayAccessNode()
	 * @generated
	 */
	int DAG_VECTOR_ARRAY_ACCESS_NODE = 29;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_ARRAY_ACCESS_NODE__ANNOTATIONS = DAG_VECTOR_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_ARRAY_ACCESS_NODE__TYPE = DAG_VECTOR_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_ARRAY_ACCESS_NODE__OUTPUTS = DAG_VECTOR_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_ARRAY_ACCESS_NODE__PARENT = DAG_VECTOR_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_ARRAY_ACCESS_NODE__INPUTS = DAG_VECTOR_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_ARRAY_ACCESS_NODE__IDENT = DAG_VECTOR_NODE__IDENT;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_ARRAY_ACCESS_NODE__NAME = DAG_VECTOR_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_ARRAY_ACCESS_NODE__SYMBOL = DAG_VECTOR_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>DAG Vector Array Access Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_ARRAY_ACCESS_NODE_FEATURE_COUNT = DAG_VECTOR_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGVectorOpNodeImpl <em>DAG Vector Op Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGVectorOpNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGVectorOpNode()
	 * @generated
	 */
	int DAG_VECTOR_OP_NODE = 30;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_OP_NODE__ANNOTATIONS = DAG_VECTOR_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_OP_NODE__TYPE = DAG_VECTOR_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_OP_NODE__OUTPUTS = DAG_VECTOR_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_OP_NODE__PARENT = DAG_VECTOR_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_OP_NODE__INPUTS = DAG_VECTOR_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_OP_NODE__IDENT = DAG_VECTOR_NODE__IDENT;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_OP_NODE__OPCODE = DAG_VECTOR_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>DAG Vector Op Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_OP_NODE_FEATURE_COUNT = DAG_VECTOR_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGVectorShuffleNodeImpl <em>DAG Vector Shuffle Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGVectorShuffleNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGVectorShuffleNode()
	 * @generated
	 */
	int DAG_VECTOR_SHUFFLE_NODE = 31;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_SHUFFLE_NODE__ANNOTATIONS = DAG_VECTOR_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_SHUFFLE_NODE__TYPE = DAG_VECTOR_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_SHUFFLE_NODE__OUTPUTS = DAG_VECTOR_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_SHUFFLE_NODE__PARENT = DAG_VECTOR_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_SHUFFLE_NODE__INPUTS = DAG_VECTOR_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_SHUFFLE_NODE__IDENT = DAG_VECTOR_NODE__IDENT;

	/**
	 * The feature id for the '<em><b>Permutation</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_SHUFFLE_NODE__PERMUTATION = DAG_VECTOR_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>DAG Vector Shuffle Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_SHUFFLE_NODE_FEATURE_COUNT = DAG_VECTOR_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGVectorExtractNodeImpl <em>DAG Vector Extract Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGVectorExtractNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGVectorExtractNode()
	 * @generated
	 */
	int DAG_VECTOR_EXTRACT_NODE = 32;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_EXTRACT_NODE__ANNOTATIONS = DAG_VECTOR_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_EXTRACT_NODE__TYPE = DAG_VECTOR_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_EXTRACT_NODE__OUTPUTS = DAG_VECTOR_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_EXTRACT_NODE__PARENT = DAG_VECTOR_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_EXTRACT_NODE__INPUTS = DAG_VECTOR_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_EXTRACT_NODE__IDENT = DAG_VECTOR_NODE__IDENT;

	/**
	 * The feature id for the '<em><b>Pos</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_EXTRACT_NODE__POS = DAG_VECTOR_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>DAG Vector Extract Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_EXTRACT_NODE_FEATURE_COUNT = DAG_VECTOR_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGVectorExpandNodeImpl <em>DAG Vector Expand Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGVectorExpandNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGVectorExpandNode()
	 * @generated
	 */
	int DAG_VECTOR_EXPAND_NODE = 33;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_EXPAND_NODE__ANNOTATIONS = DAG_VECTOR_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_EXPAND_NODE__TYPE = DAG_VECTOR_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_EXPAND_NODE__OUTPUTS = DAG_VECTOR_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_EXPAND_NODE__PARENT = DAG_VECTOR_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_EXPAND_NODE__INPUTS = DAG_VECTOR_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_EXPAND_NODE__IDENT = DAG_VECTOR_NODE__IDENT;

	/**
	 * The feature id for the '<em><b>Permutation</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_EXPAND_NODE__PERMUTATION = DAG_VECTOR_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>DAG Vector Expand Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_EXPAND_NODE_FEATURE_COUNT = DAG_VECTOR_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGVectorPackNodeImpl <em>DAG Vector Pack Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGVectorPackNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGVectorPackNode()
	 * @generated
	 */
	int DAG_VECTOR_PACK_NODE = 34;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_PACK_NODE__ANNOTATIONS = DAG_VECTOR_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_PACK_NODE__TYPE = DAG_VECTOR_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_PACK_NODE__OUTPUTS = DAG_VECTOR_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_PACK_NODE__PARENT = DAG_VECTOR_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_PACK_NODE__INPUTS = DAG_VECTOR_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_PACK_NODE__IDENT = DAG_VECTOR_NODE__IDENT;

	/**
	 * The number of structural features of the '<em>DAG Vector Pack Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_VECTOR_PACK_NODE_FEATURE_COUNT = DAG_VECTOR_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGSSAUseNodeImpl <em>DAGSSA Use Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGSSAUseNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGSSAUseNode()
	 * @generated
	 */
	int DAGSSA_USE_NODE = 35;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAGSSA_USE_NODE__ANNOTATIONS = DAG_SYMBOL_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAGSSA_USE_NODE__TYPE = DAG_SYMBOL_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAGSSA_USE_NODE__OUTPUTS = DAG_SYMBOL_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAGSSA_USE_NODE__PARENT = DAG_SYMBOL_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAGSSA_USE_NODE__INPUTS = DAG_SYMBOL_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAGSSA_USE_NODE__IDENT = DAG_SYMBOL_NODE__IDENT;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAGSSA_USE_NODE__NAME = DAG_SYMBOL_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAGSSA_USE_NODE__SYMBOL = DAG_SYMBOL_NODE__SYMBOL;

	/**
	 * The feature id for the '<em><b>Def</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAGSSA_USE_NODE__DEF = DAG_SYMBOL_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>DAGSSA Use Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAGSSA_USE_NODE_FEATURE_COUNT = DAG_SYMBOL_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGSSADefNodeImpl <em>DAGSSA Def Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGSSADefNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGSSADefNode()
	 * @generated
	 */
	int DAGSSA_DEF_NODE = 36;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAGSSA_DEF_NODE__ANNOTATIONS = DAG_OUT_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAGSSA_DEF_NODE__TYPE = DAG_OUT_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAGSSA_DEF_NODE__OUTPUTS = DAG_OUT_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAGSSA_DEF_NODE__PARENT = DAG_OUT_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAGSSA_DEF_NODE__INPUTS = DAG_OUT_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAGSSA_DEF_NODE__IDENT = DAG_OUT_NODE__IDENT;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAGSSA_DEF_NODE__SYMBOL = DAG_OUT_NODE__SYMBOL;

	/**
	 * The feature id for the '<em><b>SSA Uses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAGSSA_DEF_NODE__SSA_USES = DAG_OUT_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>DAGSSA Def Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAGSSA_DEF_NODE_FEATURE_COUNT = DAG_OUT_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.dag.impl.DAGPhiNodeImpl <em>DAG Phi Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DAGPhiNodeImpl
	 * @see gecos.dag.impl.DagPackageImpl#getDAGPhiNode()
	 * @generated
	 */
	int DAG_PHI_NODE = 37;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_PHI_NODE__ANNOTATIONS = DAG_NODE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_PHI_NODE__TYPE = DAG_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_PHI_NODE__OUTPUTS = DAG_NODE__OUTPUTS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_PHI_NODE__PARENT = DAG_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_PHI_NODE__INPUTS = DAG_NODE__INPUTS;

	/**
	 * The feature id for the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_PHI_NODE__IDENT = DAG_NODE__IDENT;

	/**
	 * The number of structural features of the '<em>DAG Phi Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAG_PHI_NODE_FEATURE_COUNT = DAG_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.dag.DependencyType <em>Dependency Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.DependencyType
	 * @see gecos.dag.impl.DagPackageImpl#getDependencyType()
	 * @generated
	 */
	int DEPENDENCY_TYPE = 38;

	/**
	 * The meta object id for the '{@link gecos.dag.DAGOperator <em>DAG Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.DAGOperator
	 * @see gecos.dag.impl.DagPackageImpl#getDAGOperator()
	 * @generated
	 */
	int DAG_OPERATOR = 39;

	/**
	 * The meta object id for the '<em>String</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see gecos.dag.impl.DagPackageImpl#getString()
	 * @generated
	 */
	int STRING = 40;

	/**
	 * The meta object id for the '<em>int</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DagPackageImpl#getint()
	 * @generated
	 */
	int INT = 41;

	/**
	 * The meta object id for the '<em>long</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DagPackageImpl#getlong()
	 * @generated
	 */
	int LONG = 42;

	/**
	 * The meta object id for the '<em>double</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.dag.impl.DagPackageImpl#getdouble()
	 * @generated
	 */
	int DOUBLE = 43;


	/**
	 * Returns the meta object for class '{@link gecos.dag.DagVisitor <em>Visitor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visitor</em>'.
	 * @see gecos.dag.DagVisitor
	 * @generated
	 */
	EClass getDagVisitor();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DagVisitable <em>Visitable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visitable</em>'.
	 * @see gecos.dag.DagVisitable
	 * @generated
	 */
	EClass getDagVisitable();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGInstruction <em>DAG Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Instruction</em>'.
	 * @see gecos.dag.DAGInstruction
	 * @generated
	 */
	EClass getDAGInstruction();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.dag.DAGInstruction#getNodes <em>Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Nodes</em>'.
	 * @see gecos.dag.DAGInstruction#getNodes()
	 * @see #getDAGInstruction()
	 * @generated
	 */
	EReference getDAGInstruction_Nodes();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.dag.DAGInstruction#getEdges <em>Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Edges</em>'.
	 * @see gecos.dag.DAGInstruction#getEdges()
	 * @see #getDAGInstruction()
	 * @generated
	 */
	EReference getDAGInstruction_Edges();

	/**
	 * Returns the meta object for the attribute '{@link gecos.dag.DAGInstruction#getIdent <em>Ident</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ident</em>'.
	 * @see gecos.dag.DAGInstruction#getIdent()
	 * @see #getDAGInstruction()
	 * @generated
	 */
	EAttribute getDAGInstruction_Ident();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.dag.DAGInstruction#getSourceNodes <em>Source Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Source Nodes</em>'.
	 * @see gecos.dag.DAGInstruction#getSourceNodes()
	 * @see #getDAGInstruction()
	 * @generated
	 */
	EReference getDAGInstruction_SourceNodes();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.dag.DAGInstruction#getSinkNodes <em>Sink Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sink Nodes</em>'.
	 * @see gecos.dag.DAGInstruction#getSinkNodes()
	 * @see #getDAGInstruction()
	 * @generated
	 */
	EReference getDAGInstruction_SinkNodes();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGNode <em>DAG Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Node</em>'.
	 * @see gecos.dag.DAGNode
	 * @generated
	 */
	EClass getDAGNode();

	/**
	 * Returns the meta object for the reference '{@link gecos.dag.DAGNode#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see gecos.dag.DAGNode#getType()
	 * @see #getDAGNode()
	 * @generated
	 */
	EReference getDAGNode_Type();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.dag.DAGNode#getOutputs <em>Outputs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Outputs</em>'.
	 * @see gecos.dag.DAGNode#getOutputs()
	 * @see #getDAGNode()
	 * @generated
	 */
	EReference getDAGNode_Outputs();

	/**
	 * Returns the meta object for the container reference '{@link gecos.dag.DAGNode#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent</em>'.
	 * @see gecos.dag.DAGNode#getParent()
	 * @see #getDAGNode()
	 * @generated
	 */
	EReference getDAGNode_Parent();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.dag.DAGNode#getInputs <em>Inputs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Inputs</em>'.
	 * @see gecos.dag.DAGNode#getInputs()
	 * @see #getDAGNode()
	 * @generated
	 */
	EReference getDAGNode_Inputs();

	/**
	 * Returns the meta object for the attribute '{@link gecos.dag.DAGNode#getIdent <em>Ident</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ident</em>'.
	 * @see gecos.dag.DAGNode#getIdent()
	 * @see #getDAGNode()
	 * @generated
	 */
	EAttribute getDAGNode_Ident();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGEdge <em>DAG Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Edge</em>'.
	 * @see gecos.dag.DAGEdge
	 * @generated
	 */
	EClass getDAGEdge();

	/**
	 * Returns the meta object for the reference '{@link gecos.dag.DAGEdge#getSrc <em>Src</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Src</em>'.
	 * @see gecos.dag.DAGEdge#getSrc()
	 * @see #getDAGEdge()
	 * @generated
	 */
	EReference getDAGEdge_Src();

	/**
	 * Returns the meta object for the reference '{@link gecos.dag.DAGEdge#getSink <em>Sink</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sink</em>'.
	 * @see gecos.dag.DAGEdge#getSink()
	 * @see #getDAGEdge()
	 * @generated
	 */
	EReference getDAGEdge_Sink();

	/**
	 * Returns the meta object for the container reference '{@link gecos.dag.DAGEdge#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent</em>'.
	 * @see gecos.dag.DAGEdge#getParent()
	 * @see #getDAGEdge()
	 * @generated
	 */
	EReference getDAGEdge_Parent();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGPort <em>DAG Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Port</em>'.
	 * @see gecos.dag.DAGPort
	 * @generated
	 */
	EClass getDAGPort();

	/**
	 * Returns the meta object for the attribute '{@link gecos.dag.DAGPort#getIdent <em>Ident</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ident</em>'.
	 * @see gecos.dag.DAGPort#getIdent()
	 * @see #getDAGPort()
	 * @generated
	 */
	EAttribute getDAGPort_Ident();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGInPort <em>DAG In Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG In Port</em>'.
	 * @see gecos.dag.DAGInPort
	 * @generated
	 */
	EClass getDAGInPort();

	/**
	 * Returns the meta object for the reference '{@link gecos.dag.DAGInPort#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see gecos.dag.DAGInPort#getSource()
	 * @see #getDAGInPort()
	 * @generated
	 */
	EReference getDAGInPort_Source();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGOutPort <em>DAG Out Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Out Port</em>'.
	 * @see gecos.dag.DAGOutPort
	 * @generated
	 */
	EClass getDAGOutPort();

	/**
	 * Returns the meta object for the reference list '{@link gecos.dag.DAGOutPort#getSinks <em>Sinks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sinks</em>'.
	 * @see gecos.dag.DAGOutPort#getSinks()
	 * @see #getDAGOutPort()
	 * @generated
	 */
	EReference getDAGOutPort_Sinks();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGImmNode <em>DAG Imm Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Imm Node</em>'.
	 * @see gecos.dag.DAGImmNode
	 * @generated
	 */
	EClass getDAGImmNode();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGSymbolNode <em>DAG Symbol Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Symbol Node</em>'.
	 * @see gecos.dag.DAGSymbolNode
	 * @generated
	 */
	EClass getDAGSymbolNode();

	/**
	 * Returns the meta object for the attribute '{@link gecos.dag.DAGSymbolNode#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see gecos.dag.DAGSymbolNode#getName()
	 * @see #getDAGSymbolNode()
	 * @generated
	 */
	EAttribute getDAGSymbolNode_Name();

	/**
	 * Returns the meta object for the reference '{@link gecos.dag.DAGSymbolNode#getSymbol <em>Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Symbol</em>'.
	 * @see gecos.dag.DAGSymbolNode#getSymbol()
	 * @see #getDAGSymbolNode()
	 * @generated
	 */
	EReference getDAGSymbolNode_Symbol();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGCallNode <em>DAG Call Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Call Node</em>'.
	 * @see gecos.dag.DAGCallNode
	 * @generated
	 */
	EClass getDAGCallNode();

	/**
	 * Returns the meta object for the attribute '{@link gecos.dag.DAGCallNode#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see gecos.dag.DAGCallNode#getName()
	 * @see #getDAGCallNode()
	 * @generated
	 */
	EAttribute getDAGCallNode_Name();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGArrayValueNode <em>DAG Array Value Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Array Value Node</em>'.
	 * @see gecos.dag.DAGArrayValueNode
	 * @generated
	 */
	EClass getDAGArrayValueNode();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGOpNode <em>DAG Op Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Op Node</em>'.
	 * @see gecos.dag.DAGOpNode
	 * @generated
	 */
	EClass getDAGOpNode();

	/**
	 * Returns the meta object for the attribute '{@link gecos.dag.DAGOpNode#getOpcode <em>Opcode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Opcode</em>'.
	 * @see gecos.dag.DAGOpNode#getOpcode()
	 * @see #getDAGOpNode()
	 * @generated
	 */
	EAttribute getDAGOpNode_Opcode();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGOutNode <em>DAG Out Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Out Node</em>'.
	 * @see gecos.dag.DAGOutNode
	 * @generated
	 */
	EClass getDAGOutNode();

	/**
	 * Returns the meta object for the reference '{@link gecos.dag.DAGOutNode#getSymbol <em>Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Symbol</em>'.
	 * @see gecos.dag.DAGOutNode#getSymbol()
	 * @see #getDAGOutNode()
	 * @generated
	 */
	EReference getDAGOutNode_Symbol();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGControlEdge <em>DAG Control Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Control Edge</em>'.
	 * @see gecos.dag.DAGControlEdge
	 * @generated
	 */
	EClass getDAGControlEdge();

	/**
	 * Returns the meta object for the attribute '{@link gecos.dag.DAGControlEdge#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see gecos.dag.DAGControlEdge#getType()
	 * @see #getDAGControlEdge()
	 * @generated
	 */
	EAttribute getDAGControlEdge_Type();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGDataEdge <em>DAG Data Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Data Edge</em>'.
	 * @see gecos.dag.DAGDataEdge
	 * @generated
	 */
	EClass getDAGDataEdge();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGNumberedSymbolNode <em>DAG Numbered Symbol Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Numbered Symbol Node</em>'.
	 * @see gecos.dag.DAGNumberedSymbolNode
	 * @generated
	 */
	EClass getDAGNumberedSymbolNode();

	/**
	 * Returns the meta object for the attribute '{@link gecos.dag.DAGNumberedSymbolNode#getNumber <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number</em>'.
	 * @see gecos.dag.DAGNumberedSymbolNode#getNumber()
	 * @see #getDAGNumberedSymbolNode()
	 * @generated
	 */
	EAttribute getDAGNumberedSymbolNode_Number();

	/**
	 * Returns the meta object for the reference '{@link gecos.dag.DAGNumberedSymbolNode#getSymbolDefinition <em>Symbol Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Symbol Definition</em>'.
	 * @see gecos.dag.DAGNumberedSymbolNode#getSymbolDefinition()
	 * @see #getDAGNumberedSymbolNode()
	 * @generated
	 */
	EReference getDAGNumberedSymbolNode_SymbolDefinition();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGNumberedOutNode <em>DAG Numbered Out Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Numbered Out Node</em>'.
	 * @see gecos.dag.DAGNumberedOutNode
	 * @generated
	 */
	EClass getDAGNumberedOutNode();

	/**
	 * Returns the meta object for the attribute '{@link gecos.dag.DAGNumberedOutNode#getNumber <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number</em>'.
	 * @see gecos.dag.DAGNumberedOutNode#getNumber()
	 * @see #getDAGNumberedOutNode()
	 * @generated
	 */
	EAttribute getDAGNumberedOutNode_Number();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGIntImmNode <em>DAG Int Imm Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Int Imm Node</em>'.
	 * @see gecos.dag.DAGIntImmNode
	 * @generated
	 */
	EClass getDAGIntImmNode();

	/**
	 * Returns the meta object for the attribute '{@link gecos.dag.DAGIntImmNode#getIntValue <em>Int Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Int Value</em>'.
	 * @see gecos.dag.DAGIntImmNode#getIntValue()
	 * @see #getDAGIntImmNode()
	 * @generated
	 */
	EAttribute getDAGIntImmNode_IntValue();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.dag.DAGIntImmNode#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see gecos.dag.DAGIntImmNode#getValue()
	 * @see #getDAGIntImmNode()
	 * @generated
	 */
	EReference getDAGIntImmNode_Value();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGFloatImmNode <em>DAG Float Imm Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Float Imm Node</em>'.
	 * @see gecos.dag.DAGFloatImmNode
	 * @generated
	 */
	EClass getDAGFloatImmNode();

	/**
	 * Returns the meta object for the attribute '{@link gecos.dag.DAGFloatImmNode#getFloatValue <em>Float Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Float Value</em>'.
	 * @see gecos.dag.DAGFloatImmNode#getFloatValue()
	 * @see #getDAGFloatImmNode()
	 * @generated
	 */
	EAttribute getDAGFloatImmNode_FloatValue();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.dag.DAGFloatImmNode#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see gecos.dag.DAGFloatImmNode#getValue()
	 * @see #getDAGFloatImmNode()
	 * @generated
	 */
	EReference getDAGFloatImmNode_Value();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGStringImmNode <em>DAG String Imm Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG String Imm Node</em>'.
	 * @see gecos.dag.DAGStringImmNode
	 * @generated
	 */
	EClass getDAGStringImmNode();

	/**
	 * Returns the meta object for the attribute '{@link gecos.dag.DAGStringImmNode#getStringValue <em>String Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>String Value</em>'.
	 * @see gecos.dag.DAGStringImmNode#getStringValue()
	 * @see #getDAGStringImmNode()
	 * @generated
	 */
	EAttribute getDAGStringImmNode_StringValue();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGArrayNode <em>DAG Array Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Array Node</em>'.
	 * @see gecos.dag.DAGArrayNode
	 * @generated
	 */
	EClass getDAGArrayNode();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGSimpleArrayNode <em>DAG Simple Array Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Simple Array Node</em>'.
	 * @see gecos.dag.DAGSimpleArrayNode
	 * @generated
	 */
	EClass getDAGSimpleArrayNode();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGFieldInstruction <em>DAG Field Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Field Instruction</em>'.
	 * @see gecos.dag.DAGFieldInstruction
	 * @generated
	 */
	EClass getDAGFieldInstruction();

	/**
	 * Returns the meta object for the reference '{@link gecos.dag.DAGFieldInstruction#getField <em>Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Field</em>'.
	 * @see gecos.dag.DAGFieldInstruction#getField()
	 * @see #getDAGFieldInstruction()
	 * @generated
	 */
	EReference getDAGFieldInstruction_Field();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGJumpNode <em>DAG Jump Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Jump Node</em>'.
	 * @see gecos.dag.DAGJumpNode
	 * @generated
	 */
	EClass getDAGJumpNode();

	/**
	 * Returns the meta object for the attribute '{@link gecos.dag.DAGJumpNode#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see gecos.dag.DAGJumpNode#getLabel()
	 * @see #getDAGJumpNode()
	 * @generated
	 */
	EAttribute getDAGJumpNode_Label();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGPatternNode <em>DAG Pattern Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Pattern Node</em>'.
	 * @see gecos.dag.DAGPatternNode
	 * @generated
	 */
	EClass getDAGPatternNode();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.dag.DAGPatternNode#getPattern <em>Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Pattern</em>'.
	 * @see gecos.dag.DAGPatternNode#getPattern()
	 * @see #getDAGPatternNode()
	 * @generated
	 */
	EReference getDAGPatternNode_Pattern();

	/**
	 * Returns the meta object for the map '{@link gecos.dag.DAGPatternNode#getInputsMap <em>Inputs Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Inputs Map</em>'.
	 * @see gecos.dag.DAGPatternNode#getInputsMap()
	 * @see #getDAGPatternNode()
	 * @generated
	 */
	EReference getDAGPatternNode_InputsMap();

	/**
	 * Returns the meta object for the map '{@link gecos.dag.DAGPatternNode#getOutputsMap <em>Outputs Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Outputs Map</em>'.
	 * @see gecos.dag.DAGPatternNode#getOutputsMap()
	 * @see #getDAGPatternNode()
	 * @generated
	 */
	EReference getDAGPatternNode_OutputsMap();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>In Port Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>In Port Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="gecos.dag.DAGInPort"
	 *        valueType="gecos.dag.DAGInPort"
	 * @generated
	 */
	EClass getInPortMap();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getInPortMap()
	 * @generated
	 */
	EReference getInPortMap_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getInPortMap()
	 * @generated
	 */
	EReference getInPortMap_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Out Port Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Out Port Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="gecos.dag.DAGOutPort"
	 *        valueType="gecos.dag.DAGOutPort"
	 * @generated
	 */
	EClass getOutPortMap();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getOutPortMap()
	 * @generated
	 */
	EReference getOutPortMap_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getOutPortMap()
	 * @generated
	 */
	EReference getOutPortMap_Value();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGVectorNode <em>DAG Vector Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Vector Node</em>'.
	 * @see gecos.dag.DAGVectorNode
	 * @generated
	 */
	EClass getDAGVectorNode();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGVectorArrayAccessNode <em>DAG Vector Array Access Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Vector Array Access Node</em>'.
	 * @see gecos.dag.DAGVectorArrayAccessNode
	 * @generated
	 */
	EClass getDAGVectorArrayAccessNode();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGVectorOpNode <em>DAG Vector Op Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Vector Op Node</em>'.
	 * @see gecos.dag.DAGVectorOpNode
	 * @generated
	 */
	EClass getDAGVectorOpNode();

	/**
	 * Returns the meta object for the attribute '{@link gecos.dag.DAGVectorOpNode#getOpcode <em>Opcode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Opcode</em>'.
	 * @see gecos.dag.DAGVectorOpNode#getOpcode()
	 * @see #getDAGVectorOpNode()
	 * @generated
	 */
	EAttribute getDAGVectorOpNode_Opcode();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGVectorShuffleNode <em>DAG Vector Shuffle Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Vector Shuffle Node</em>'.
	 * @see gecos.dag.DAGVectorShuffleNode
	 * @generated
	 */
	EClass getDAGVectorShuffleNode();

	/**
	 * Returns the meta object for the attribute list '{@link gecos.dag.DAGVectorShuffleNode#getPermutation <em>Permutation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Permutation</em>'.
	 * @see gecos.dag.DAGVectorShuffleNode#getPermutation()
	 * @see #getDAGVectorShuffleNode()
	 * @generated
	 */
	EAttribute getDAGVectorShuffleNode_Permutation();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGVectorExtractNode <em>DAG Vector Extract Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Vector Extract Node</em>'.
	 * @see gecos.dag.DAGVectorExtractNode
	 * @generated
	 */
	EClass getDAGVectorExtractNode();

	/**
	 * Returns the meta object for the attribute '{@link gecos.dag.DAGVectorExtractNode#getPos <em>Pos</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Pos</em>'.
	 * @see gecos.dag.DAGVectorExtractNode#getPos()
	 * @see #getDAGVectorExtractNode()
	 * @generated
	 */
	EAttribute getDAGVectorExtractNode_Pos();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGVectorExpandNode <em>DAG Vector Expand Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Vector Expand Node</em>'.
	 * @see gecos.dag.DAGVectorExpandNode
	 * @generated
	 */
	EClass getDAGVectorExpandNode();

	/**
	 * Returns the meta object for the attribute list '{@link gecos.dag.DAGVectorExpandNode#getPermutation <em>Permutation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Permutation</em>'.
	 * @see gecos.dag.DAGVectorExpandNode#getPermutation()
	 * @see #getDAGVectorExpandNode()
	 * @generated
	 */
	EAttribute getDAGVectorExpandNode_Permutation();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGVectorPackNode <em>DAG Vector Pack Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Vector Pack Node</em>'.
	 * @see gecos.dag.DAGVectorPackNode
	 * @generated
	 */
	EClass getDAGVectorPackNode();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGSSAUseNode <em>DAGSSA Use Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAGSSA Use Node</em>'.
	 * @see gecos.dag.DAGSSAUseNode
	 * @generated
	 */
	EClass getDAGSSAUseNode();

	/**
	 * Returns the meta object for the reference '{@link gecos.dag.DAGSSAUseNode#getDef <em>Def</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Def</em>'.
	 * @see gecos.dag.DAGSSAUseNode#getDef()
	 * @see #getDAGSSAUseNode()
	 * @generated
	 */
	EReference getDAGSSAUseNode_Def();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGSSADefNode <em>DAGSSA Def Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAGSSA Def Node</em>'.
	 * @see gecos.dag.DAGSSADefNode
	 * @generated
	 */
	EClass getDAGSSADefNode();

	/**
	 * Returns the meta object for the reference list '{@link gecos.dag.DAGSSADefNode#getSSAUses <em>SSA Uses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>SSA Uses</em>'.
	 * @see gecos.dag.DAGSSADefNode#getSSAUses()
	 * @see #getDAGSSADefNode()
	 * @generated
	 */
	EReference getDAGSSADefNode_SSAUses();

	/**
	 * Returns the meta object for class '{@link gecos.dag.DAGPhiNode <em>DAG Phi Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DAG Phi Node</em>'.
	 * @see gecos.dag.DAGPhiNode
	 * @generated
	 */
	EClass getDAGPhiNode();

	/**
	 * Returns the meta object for enum '{@link gecos.dag.DependencyType <em>Dependency Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Dependency Type</em>'.
	 * @see gecos.dag.DependencyType
	 * @generated
	 */
	EEnum getDependencyType();

	/**
	 * Returns the meta object for enum '{@link gecos.dag.DAGOperator <em>DAG Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>DAG Operator</em>'.
	 * @see gecos.dag.DAGOperator
	 * @generated
	 */
	EEnum getDAGOperator();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>String</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getString();

	/**
	 * Returns the meta object for data type '<em>int</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>int</em>'.
	 * @model instanceClass="int"
	 * @generated
	 */
	EDataType getint();

	/**
	 * Returns the meta object for data type '<em>long</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>long</em>'.
	 * @model instanceClass="long"
	 * @generated
	 */
	EDataType getlong();

	/**
	 * Returns the meta object for data type '<em>double</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>double</em>'.
	 * @model instanceClass="double"
	 * @generated
	 */
	EDataType getdouble();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DagFactory getDagFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link gecos.dag.DagVisitor <em>Visitor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.DagVisitor
		 * @see gecos.dag.impl.DagPackageImpl#getDagVisitor()
		 * @generated
		 */
		EClass DAG_VISITOR = eINSTANCE.getDagVisitor();

		/**
		 * The meta object literal for the '{@link gecos.dag.DagVisitable <em>Visitable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.DagVisitable
		 * @see gecos.dag.impl.DagPackageImpl#getDagVisitable()
		 * @generated
		 */
		EClass DAG_VISITABLE = eINSTANCE.getDagVisitable();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGInstructionImpl <em>DAG Instruction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGInstructionImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGInstruction()
		 * @generated
		 */
		EClass DAG_INSTRUCTION = eINSTANCE.getDAGInstruction();

		/**
		 * The meta object literal for the '<em><b>Nodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAG_INSTRUCTION__NODES = eINSTANCE.getDAGInstruction_Nodes();

		/**
		 * The meta object literal for the '<em><b>Edges</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAG_INSTRUCTION__EDGES = eINSTANCE.getDAGInstruction_Edges();

		/**
		 * The meta object literal for the '<em><b>Ident</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DAG_INSTRUCTION__IDENT = eINSTANCE.getDAGInstruction_Ident();

		/**
		 * The meta object literal for the '<em><b>Source Nodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAG_INSTRUCTION__SOURCE_NODES = eINSTANCE.getDAGInstruction_SourceNodes();

		/**
		 * The meta object literal for the '<em><b>Sink Nodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAG_INSTRUCTION__SINK_NODES = eINSTANCE.getDAGInstruction_SinkNodes();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGNodeImpl <em>DAG Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGNode()
		 * @generated
		 */
		EClass DAG_NODE = eINSTANCE.getDAGNode();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAG_NODE__TYPE = eINSTANCE.getDAGNode_Type();

		/**
		 * The meta object literal for the '<em><b>Outputs</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAG_NODE__OUTPUTS = eINSTANCE.getDAGNode_Outputs();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAG_NODE__PARENT = eINSTANCE.getDAGNode_Parent();

		/**
		 * The meta object literal for the '<em><b>Inputs</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAG_NODE__INPUTS = eINSTANCE.getDAGNode_Inputs();

		/**
		 * The meta object literal for the '<em><b>Ident</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DAG_NODE__IDENT = eINSTANCE.getDAGNode_Ident();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGEdgeImpl <em>DAG Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGEdgeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGEdge()
		 * @generated
		 */
		EClass DAG_EDGE = eINSTANCE.getDAGEdge();

		/**
		 * The meta object literal for the '<em><b>Src</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAG_EDGE__SRC = eINSTANCE.getDAGEdge_Src();

		/**
		 * The meta object literal for the '<em><b>Sink</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAG_EDGE__SINK = eINSTANCE.getDAGEdge_Sink();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAG_EDGE__PARENT = eINSTANCE.getDAGEdge_Parent();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGPortImpl <em>DAG Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGPortImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGPort()
		 * @generated
		 */
		EClass DAG_PORT = eINSTANCE.getDAGPort();

		/**
		 * The meta object literal for the '<em><b>Ident</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DAG_PORT__IDENT = eINSTANCE.getDAGPort_Ident();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGInPortImpl <em>DAG In Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGInPortImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGInPort()
		 * @generated
		 */
		EClass DAG_IN_PORT = eINSTANCE.getDAGInPort();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAG_IN_PORT__SOURCE = eINSTANCE.getDAGInPort_Source();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGOutPortImpl <em>DAG Out Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGOutPortImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGOutPort()
		 * @generated
		 */
		EClass DAG_OUT_PORT = eINSTANCE.getDAGOutPort();

		/**
		 * The meta object literal for the '<em><b>Sinks</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAG_OUT_PORT__SINKS = eINSTANCE.getDAGOutPort_Sinks();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGImmNodeImpl <em>DAG Imm Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGImmNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGImmNode()
		 * @generated
		 */
		EClass DAG_IMM_NODE = eINSTANCE.getDAGImmNode();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGSymbolNodeImpl <em>DAG Symbol Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGSymbolNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGSymbolNode()
		 * @generated
		 */
		EClass DAG_SYMBOL_NODE = eINSTANCE.getDAGSymbolNode();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DAG_SYMBOL_NODE__NAME = eINSTANCE.getDAGSymbolNode_Name();

		/**
		 * The meta object literal for the '<em><b>Symbol</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAG_SYMBOL_NODE__SYMBOL = eINSTANCE.getDAGSymbolNode_Symbol();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGCallNodeImpl <em>DAG Call Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGCallNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGCallNode()
		 * @generated
		 */
		EClass DAG_CALL_NODE = eINSTANCE.getDAGCallNode();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DAG_CALL_NODE__NAME = eINSTANCE.getDAGCallNode_Name();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGArrayValueNodeImpl <em>DAG Array Value Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGArrayValueNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGArrayValueNode()
		 * @generated
		 */
		EClass DAG_ARRAY_VALUE_NODE = eINSTANCE.getDAGArrayValueNode();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGOpNodeImpl <em>DAG Op Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGOpNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGOpNode()
		 * @generated
		 */
		EClass DAG_OP_NODE = eINSTANCE.getDAGOpNode();

		/**
		 * The meta object literal for the '<em><b>Opcode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DAG_OP_NODE__OPCODE = eINSTANCE.getDAGOpNode_Opcode();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGOutNodeImpl <em>DAG Out Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGOutNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGOutNode()
		 * @generated
		 */
		EClass DAG_OUT_NODE = eINSTANCE.getDAGOutNode();

		/**
		 * The meta object literal for the '<em><b>Symbol</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAG_OUT_NODE__SYMBOL = eINSTANCE.getDAGOutNode_Symbol();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGControlEdgeImpl <em>DAG Control Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGControlEdgeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGControlEdge()
		 * @generated
		 */
		EClass DAG_CONTROL_EDGE = eINSTANCE.getDAGControlEdge();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DAG_CONTROL_EDGE__TYPE = eINSTANCE.getDAGControlEdge_Type();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGDataEdgeImpl <em>DAG Data Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGDataEdgeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGDataEdge()
		 * @generated
		 */
		EClass DAG_DATA_EDGE = eINSTANCE.getDAGDataEdge();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGNumberedSymbolNodeImpl <em>DAG Numbered Symbol Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGNumberedSymbolNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGNumberedSymbolNode()
		 * @generated
		 */
		EClass DAG_NUMBERED_SYMBOL_NODE = eINSTANCE.getDAGNumberedSymbolNode();

		/**
		 * The meta object literal for the '<em><b>Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DAG_NUMBERED_SYMBOL_NODE__NUMBER = eINSTANCE.getDAGNumberedSymbolNode_Number();

		/**
		 * The meta object literal for the '<em><b>Symbol Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAG_NUMBERED_SYMBOL_NODE__SYMBOL_DEFINITION = eINSTANCE.getDAGNumberedSymbolNode_SymbolDefinition();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGNumberedOutNodeImpl <em>DAG Numbered Out Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGNumberedOutNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGNumberedOutNode()
		 * @generated
		 */
		EClass DAG_NUMBERED_OUT_NODE = eINSTANCE.getDAGNumberedOutNode();

		/**
		 * The meta object literal for the '<em><b>Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DAG_NUMBERED_OUT_NODE__NUMBER = eINSTANCE.getDAGNumberedOutNode_Number();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGIntImmNodeImpl <em>DAG Int Imm Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGIntImmNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGIntImmNode()
		 * @generated
		 */
		EClass DAG_INT_IMM_NODE = eINSTANCE.getDAGIntImmNode();

		/**
		 * The meta object literal for the '<em><b>Int Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DAG_INT_IMM_NODE__INT_VALUE = eINSTANCE.getDAGIntImmNode_IntValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAG_INT_IMM_NODE__VALUE = eINSTANCE.getDAGIntImmNode_Value();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGFloatImmNodeImpl <em>DAG Float Imm Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGFloatImmNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGFloatImmNode()
		 * @generated
		 */
		EClass DAG_FLOAT_IMM_NODE = eINSTANCE.getDAGFloatImmNode();

		/**
		 * The meta object literal for the '<em><b>Float Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DAG_FLOAT_IMM_NODE__FLOAT_VALUE = eINSTANCE.getDAGFloatImmNode_FloatValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAG_FLOAT_IMM_NODE__VALUE = eINSTANCE.getDAGFloatImmNode_Value();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGStringImmNodeImpl <em>DAG String Imm Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGStringImmNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGStringImmNode()
		 * @generated
		 */
		EClass DAG_STRING_IMM_NODE = eINSTANCE.getDAGStringImmNode();

		/**
		 * The meta object literal for the '<em><b>String Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DAG_STRING_IMM_NODE__STRING_VALUE = eINSTANCE.getDAGStringImmNode_StringValue();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGArrayNodeImpl <em>DAG Array Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGArrayNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGArrayNode()
		 * @generated
		 */
		EClass DAG_ARRAY_NODE = eINSTANCE.getDAGArrayNode();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGSimpleArrayNodeImpl <em>DAG Simple Array Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGSimpleArrayNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGSimpleArrayNode()
		 * @generated
		 */
		EClass DAG_SIMPLE_ARRAY_NODE = eINSTANCE.getDAGSimpleArrayNode();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGFieldInstructionImpl <em>DAG Field Instruction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGFieldInstructionImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGFieldInstruction()
		 * @generated
		 */
		EClass DAG_FIELD_INSTRUCTION = eINSTANCE.getDAGFieldInstruction();

		/**
		 * The meta object literal for the '<em><b>Field</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAG_FIELD_INSTRUCTION__FIELD = eINSTANCE.getDAGFieldInstruction_Field();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGJumpNodeImpl <em>DAG Jump Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGJumpNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGJumpNode()
		 * @generated
		 */
		EClass DAG_JUMP_NODE = eINSTANCE.getDAGJumpNode();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DAG_JUMP_NODE__LABEL = eINSTANCE.getDAGJumpNode_Label();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGPatternNodeImpl <em>DAG Pattern Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGPatternNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGPatternNode()
		 * @generated
		 */
		EClass DAG_PATTERN_NODE = eINSTANCE.getDAGPatternNode();

		/**
		 * The meta object literal for the '<em><b>Pattern</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAG_PATTERN_NODE__PATTERN = eINSTANCE.getDAGPatternNode_Pattern();

		/**
		 * The meta object literal for the '<em><b>Inputs Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAG_PATTERN_NODE__INPUTS_MAP = eINSTANCE.getDAGPatternNode_InputsMap();

		/**
		 * The meta object literal for the '<em><b>Outputs Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAG_PATTERN_NODE__OUTPUTS_MAP = eINSTANCE.getDAGPatternNode_OutputsMap();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.InPortMapImpl <em>In Port Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.InPortMapImpl
		 * @see gecos.dag.impl.DagPackageImpl#getInPortMap()
		 * @generated
		 */
		EClass IN_PORT_MAP = eINSTANCE.getInPortMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IN_PORT_MAP__KEY = eINSTANCE.getInPortMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IN_PORT_MAP__VALUE = eINSTANCE.getInPortMap_Value();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.OutPortMapImpl <em>Out Port Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.OutPortMapImpl
		 * @see gecos.dag.impl.DagPackageImpl#getOutPortMap()
		 * @generated
		 */
		EClass OUT_PORT_MAP = eINSTANCE.getOutPortMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OUT_PORT_MAP__KEY = eINSTANCE.getOutPortMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OUT_PORT_MAP__VALUE = eINSTANCE.getOutPortMap_Value();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGVectorNodeImpl <em>DAG Vector Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGVectorNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGVectorNode()
		 * @generated
		 */
		EClass DAG_VECTOR_NODE = eINSTANCE.getDAGVectorNode();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGVectorArrayAccessNodeImpl <em>DAG Vector Array Access Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGVectorArrayAccessNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGVectorArrayAccessNode()
		 * @generated
		 */
		EClass DAG_VECTOR_ARRAY_ACCESS_NODE = eINSTANCE.getDAGVectorArrayAccessNode();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGVectorOpNodeImpl <em>DAG Vector Op Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGVectorOpNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGVectorOpNode()
		 * @generated
		 */
		EClass DAG_VECTOR_OP_NODE = eINSTANCE.getDAGVectorOpNode();

		/**
		 * The meta object literal for the '<em><b>Opcode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DAG_VECTOR_OP_NODE__OPCODE = eINSTANCE.getDAGVectorOpNode_Opcode();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGVectorShuffleNodeImpl <em>DAG Vector Shuffle Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGVectorShuffleNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGVectorShuffleNode()
		 * @generated
		 */
		EClass DAG_VECTOR_SHUFFLE_NODE = eINSTANCE.getDAGVectorShuffleNode();

		/**
		 * The meta object literal for the '<em><b>Permutation</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DAG_VECTOR_SHUFFLE_NODE__PERMUTATION = eINSTANCE.getDAGVectorShuffleNode_Permutation();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGVectorExtractNodeImpl <em>DAG Vector Extract Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGVectorExtractNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGVectorExtractNode()
		 * @generated
		 */
		EClass DAG_VECTOR_EXTRACT_NODE = eINSTANCE.getDAGVectorExtractNode();

		/**
		 * The meta object literal for the '<em><b>Pos</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DAG_VECTOR_EXTRACT_NODE__POS = eINSTANCE.getDAGVectorExtractNode_Pos();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGVectorExpandNodeImpl <em>DAG Vector Expand Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGVectorExpandNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGVectorExpandNode()
		 * @generated
		 */
		EClass DAG_VECTOR_EXPAND_NODE = eINSTANCE.getDAGVectorExpandNode();

		/**
		 * The meta object literal for the '<em><b>Permutation</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DAG_VECTOR_EXPAND_NODE__PERMUTATION = eINSTANCE.getDAGVectorExpandNode_Permutation();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGVectorPackNodeImpl <em>DAG Vector Pack Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGVectorPackNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGVectorPackNode()
		 * @generated
		 */
		EClass DAG_VECTOR_PACK_NODE = eINSTANCE.getDAGVectorPackNode();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGSSAUseNodeImpl <em>DAGSSA Use Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGSSAUseNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGSSAUseNode()
		 * @generated
		 */
		EClass DAGSSA_USE_NODE = eINSTANCE.getDAGSSAUseNode();

		/**
		 * The meta object literal for the '<em><b>Def</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAGSSA_USE_NODE__DEF = eINSTANCE.getDAGSSAUseNode_Def();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGSSADefNodeImpl <em>DAGSSA Def Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGSSADefNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGSSADefNode()
		 * @generated
		 */
		EClass DAGSSA_DEF_NODE = eINSTANCE.getDAGSSADefNode();

		/**
		 * The meta object literal for the '<em><b>SSA Uses</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAGSSA_DEF_NODE__SSA_USES = eINSTANCE.getDAGSSADefNode_SSAUses();

		/**
		 * The meta object literal for the '{@link gecos.dag.impl.DAGPhiNodeImpl <em>DAG Phi Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DAGPhiNodeImpl
		 * @see gecos.dag.impl.DagPackageImpl#getDAGPhiNode()
		 * @generated
		 */
		EClass DAG_PHI_NODE = eINSTANCE.getDAGPhiNode();

		/**
		 * The meta object literal for the '{@link gecos.dag.DependencyType <em>Dependency Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.DependencyType
		 * @see gecos.dag.impl.DagPackageImpl#getDependencyType()
		 * @generated
		 */
		EEnum DEPENDENCY_TYPE = eINSTANCE.getDependencyType();

		/**
		 * The meta object literal for the '{@link gecos.dag.DAGOperator <em>DAG Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.DAGOperator
		 * @see gecos.dag.impl.DagPackageImpl#getDAGOperator()
		 * @generated
		 */
		EEnum DAG_OPERATOR = eINSTANCE.getDAGOperator();

		/**
		 * The meta object literal for the '<em>String</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see gecos.dag.impl.DagPackageImpl#getString()
		 * @generated
		 */
		EDataType STRING = eINSTANCE.getString();

		/**
		 * The meta object literal for the '<em>int</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DagPackageImpl#getint()
		 * @generated
		 */
		EDataType INT = eINSTANCE.getint();

		/**
		 * The meta object literal for the '<em>long</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DagPackageImpl#getlong()
		 * @generated
		 */
		EDataType LONG = eINSTANCE.getlong();

		/**
		 * The meta object literal for the '<em>double</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.dag.impl.DagPackageImpl#getdouble()
		 * @generated
		 */
		EDataType DOUBLE = eINSTANCE.getdouble();

	}

} //DagPackage
