typedef int T;
T a;

int main() {
	typedef char T;
	T b;
	{
		typedef long long T;
		T c;

		if (sizeof(a) != sizeof(int))
			abort ();
		if (sizeof(b) != sizeof(char))
			abort ();
		if (sizeof(c) != sizeof(long long))
			abort ();
	}

	exit (0);
}
