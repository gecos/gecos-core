/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.factory.conversion;

import gecos.types.RecordType;

public class RecordConversionLine extends ConversionLine {

	/* ========================================================================
	 *        | char   | short  | int    | long   | longlong | float  | double
	 * ========================================================================
	 * char   | char   | short  | int    | long   | longlong | float  | double
	 * short  | short  | short  | int    | long   | longlong | float  | double
	 * int    | int    | int    | int    | long   | longlong | float  | double
	 * long   | long   | long   | long   | long   | longlong | float  | double
	 * longl  | longl  | longl  | longl  | longl  | longlong | float  | double
	 * float  | float  | float  | float  | float  | double   | float  | double
	 * double | double | double | double | double | double   | double | double
	 */
	private RecordType recordType;

	public RecordConversionLine(RecordType type) {
		recordType = type;
	}
	
	public RecordType getBaseType() {
		return recordType;
	}

	@Override
	public Object caseRecordType(RecordType t) {
		if(recordType.isEqual(t)) {
			return recordType;
		} 
		return null;
	}

}