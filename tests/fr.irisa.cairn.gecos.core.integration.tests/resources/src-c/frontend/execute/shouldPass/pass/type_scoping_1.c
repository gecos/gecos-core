#include <stdint.h>


void g(int8_t a) {
}

int main() {
	static int8_t b; //b must not share the same type as g:a. if it does a will be static and the code won't compile.

	return 0;
}
