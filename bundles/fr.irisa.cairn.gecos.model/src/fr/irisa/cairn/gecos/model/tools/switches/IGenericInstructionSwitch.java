package fr.irisa.cairn.gecos.model.tools.switches;

import gecos.instrs.GenericInstruction;

public interface IGenericInstructionSwitch<T> {

	public  T caseADD(GenericInstruction g);

	public  T caseSUB(GenericInstruction g);

	public  T caseMUL(GenericInstruction g);

	public  T caseSHL(GenericInstruction g);

	public  T caseSHR(GenericInstruction g);

	public  T caseDIV(GenericInstruction g);

	public  T caseMOD(GenericInstruction g);

	public  T caseOR(GenericInstruction g);

	public  T caseAND(GenericInstruction g);

	public  T caseXOR(GenericInstruction g);

	public  T caseARRAY(GenericInstruction g);

	public  T caseEQ(GenericInstruction g);

	public  T caseGT(GenericInstruction g);

	public  T caseGE(GenericInstruction g);

	public  T caseLT(GenericInstruction g);

	public  T caseLE(GenericInstruction g);

	public T caseMUX(GenericInstruction g);

}