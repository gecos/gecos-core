/**
 */
package gecos.dag;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Out Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.DAGOutPort#getSinks <em>Sinks</em>}</li>
 * </ul>
 *
 * @see gecos.dag.DagPackage#getDAGOutPort()
 * @model
 * @generated
 */
public interface DAGOutPort extends DAGPort, DagVisitable {
	/**
	 * Returns the value of the '<em><b>Sinks</b></em>' reference list.
	 * The list contents are of type {@link gecos.dag.DAGEdge}.
	 * It is bidirectional and its opposite is '{@link gecos.dag.DAGEdge#getSrc <em>Src</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sinks</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sinks</em>' reference list.
	 * @see gecos.dag.DagPackage#getDAGOutPort_Sinks()
	 * @see gecos.dag.DAGEdge#getSrc
	 * @model opposite="src"
	 * @generated
	 */
	EList<DAGEdge> getSinks();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGOutPort(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.dag.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.dag.DAGNode%&gt; _parent = this.getParent();\nboolean _tripleNotEquals = (_parent != null);\nif (_tripleNotEquals)\n{\n\t&lt;%gecos.dag.DAGNode%&gt; _parent_1 = this.getParent();\n\t&lt;%java.lang.String%&gt; _plus = (_parent_1 + \".Out[\");\n\tint _indexOf = this.getParent().getOutputs().indexOf(this);\n\t&lt;%java.lang.String%&gt; _plus_1 = (_plus + &lt;%java.lang.Integer%&gt;.valueOf(_indexOf));\n\treturn (_plus_1 + \"]\");\n}\nelse\n{\n\tint _ident = this.getIdent();\n\t&lt;%java.lang.String%&gt; _plus_2 = (\"?.Out[\" + &lt;%java.lang.Integer%&gt;.valueOf(_ident));\n\treturn (_plus_2 + \"]\");\n}'"
	 * @generated
	 */
	String toString();

} // DAGOutPort
