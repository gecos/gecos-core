package fr.irisa.cairn.gecos.model.tools.controlflow;

import java.util.List;

import org.eclipse.emf.common.util.BasicEList;

import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksDefaultVisitor;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.ControlEdge;

public class HierarchicalControlFlow extends GecosBlocksDefaultVisitor{

/**
 * This Helper class is used to build the list of all incoming/outgoing control 
 * flow edges from a Block hierarchy. The Algorithm simply collects all BB from the 
 * target block, and construct the list of edge pointing to BBs not belonging to the 
 * 
 * block hierarchy.  
 * @author steven derrien 
 * 
 */
	
	BasicEList<BasicBlock> bbs; 

	public HierarchicalControlFlow() {
		bbs = new BasicEList<BasicBlock>();
	}
	
	
	public List<ControlEdge> getAllIncomingEdges(Block b) {
		BasicEList<ControlEdge> res = new BasicEList<ControlEdge>();
		b.accept(this);
		for(BasicBlock bb : bbs) {
			for(ControlEdge e : bb.getInEdges()) {
				if(!bbs.contains(e.getFrom())) {
					res.add(e);
				}
			}
		}
		return res;
	}

	public List<ControlEdge> getAllOutcomingEdges(Block b) {
		BasicEList<ControlEdge> res = new BasicEList<ControlEdge>();
		b.accept(this);
		for(BasicBlock bb : bbs) {
			for(ControlEdge e : bb.getOutEdges()) {
				if(!bbs.contains(e.getTo())) {
					res.add(e);
				}
			}
		}
		return res;
	}

	@Override
	public void visitBasicBlock(BasicBlock b) {
		bbs.add(b);
	}

}
