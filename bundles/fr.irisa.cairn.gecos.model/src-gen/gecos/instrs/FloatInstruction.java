/**
 */
package gecos.instrs;

import gecos.types.Type;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Float Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.FloatInstruction#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getFloatInstruction()
 * @model
 * @generated
 */
public interface FloatInstruction extends ConstantInstruction {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(double)
	 * @see gecos.instrs.InstrsPackage#getFloatInstruction_Value()
	 * @model unique="false" dataType="gecos.instrs.double"
	 * @generated
	 */
	double getValue();

	/**
	 * Sets the value of the '{@link gecos.instrs.FloatInstruction#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(double value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitFloatInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%java.lang.String%&gt;.valueOf(this.getValue());'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return true;'"
	 * @generated
	 */
	boolean isConstant();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" objUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((obj instanceof &lt;%gecos.instrs.FloatInstruction%&gt;))\n{\n\tdouble _value = ((&lt;%gecos.instrs.FloatInstruction%&gt;)obj).getValue();\n\tdouble _value_1 = this.getValue();\n\treturn (_value == _value_1);\n}\nreturn false;'"
	 * @generated
	 */
	boolean equals(Object obj);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory%&gt;.FLOAT();'"
	 * @generated
	 */
	Type computeType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.FloatInstruction%&gt;))\n{\n\treturn (super.isSame(instr) &amp;&amp; (this.getValue() == ((&lt;%gecos.instrs.FloatInstruction%&gt;)instr).getValue()));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // FloatInstruction
