/**
 */
package gecos.instrs;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Comparison Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.ComparisonInstruction#getOperator <em>Operator</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getComparisonInstruction()
 * @model
 * @generated
 */
public interface ComparisonInstruction extends GenericInstruction {
	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link gecos.instrs.ComparisonOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see gecos.instrs.ComparisonOperator
	 * @see #setOperator(ComparisonOperator)
	 * @see gecos.instrs.InstrsPackage#getComparisonInstruction_Operator()
	 * @model unique="false"
	 * @generated
	 */
	ComparisonOperator getOperator();

	/**
	 * Sets the value of the '{@link gecos.instrs.ComparisonInstruction#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see gecos.instrs.ComparisonOperator
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(ComparisonOperator value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.ComparisonInstruction%&gt;))\n{\n\treturn (super.isSame(instr) &amp;&amp; (this.getOperator().getValue() == ((&lt;%gecos.instrs.ComparisonInstruction%&gt;)instr).getOperator().getValue()));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // ComparisonInstruction
