package fr.irisa.cairn.gecos.model.tools.queries;

import java.util.Stack;

import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch;
import gecos.blocks.ForBlock;
import gecos.core.Procedure;

public class BuildForBlockList extends BlocksDefaultSwitch<Object> {

	private int[] index;
	private ForBlock forBlock;
	private int offset = 0;
	Stack<Integer> pos = new Stack<Integer>();

	public BuildForBlockList(Procedure proc, int... index) {
		this.index = index;
	}

	public ForBlock compute() {
		return forBlock;
	}

	private void showCurrentPos(ForBlock b) {
		System.out.print("Position for " + b + " is ");
		for (Integer value : pos) {
			System.out.print("," + value);
		}
		System.out.println();
	}

	@Override
	public Object caseForBlock(ForBlock b) {
		Object res = null;
		showCurrentPos(b);
		if (index[pos.size()] == offset) {
			if (index.length == pos.size()) {
				System.out.print("Matching at ");
				showCurrentPos(b);
				return b;
			}
			pos.push(offset);
			offset = 0;
			res = super.caseForBlock(b);
			offset = pos.pop();
		}
		offset++;
		return res;
	}
}