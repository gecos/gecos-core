/**
 */
package gecos.instrs;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Convert Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.instrs.InstrsPackage#getConvertInstruction()
 * @model
 * @generated
 */
public interface ConvertInstruction extends ExprComponent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitConvertInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _elvis = null;\n&lt;%gecos.types.Type%&gt; _type = this.getType();\n&lt;%java.lang.String%&gt; _string = null;\nif (_type!=null)\n{\n\t_string=_type.toString();\n}\nif (_string != null)\n{\n\t_elvis = _string;\n} else\n{\n\t_elvis = \"?\";\n}\n&lt;%java.lang.String%&gt; _plus = (\"((\" + _elvis);\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \")\");\n&lt;%java.lang.String%&gt; _elvis_1 = null;\n&lt;%gecos.instrs.Instruction%&gt; _expr = this.getExpr();\n&lt;%java.lang.String%&gt; _string_1 = null;\nif (_expr!=null)\n{\n\t_string_1=_expr.toString();\n}\nif (_string_1 != null)\n{\n\t_elvis_1 = _string_1;\n} else\n{\n\t_elvis_1 = \"?\";\n}\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + _elvis_1);\nreturn (_plus_2 + \")\");'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.ConvertInstruction%&gt;))\n{\n\treturn super.isSame(instr);\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // ConvertInstruction
