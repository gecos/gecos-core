package fr.irisa.cairn.gecos.model.transforms.ssa;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.tools.switches.DefaultInstructionSwitch;
import gecos.blocks.BasicBlock;
import gecos.blocks.BlocksFactory;
import gecos.blocks.DataEdge;
import gecos.instrs.SSADefSymbol;
import gecos.instrs.SSAUseSymbol;
import gecos.instrs.SymbolInstruction;

abstract class RenumberSwitch extends DefaultInstructionSwitch<Object> {

	/**
	 * 
	 */
	protected final SSAAnalyser ssaAnalyser;
	protected BasicBlock currentBB;
	
	protected static final boolean VERBOSE = false;
	protected void debug(String string) {
		if (VERBOSE) System.out.println("[SSA Renumber] " + this.ssaAnalyser.indent + string);
	}

	/**
	 * @param ssaAnalyser
	 */
	protected RenumberSwitch(SSAAnalyser ssaAnalyser) {
		this.ssaAnalyser = ssaAnalyser;
	}

	@Override
	public void run(BasicBlock bb) {
		currentBB = bb;
		super.run(bb);
	}

	protected SSADefSymbol makeSSADefinition(SymbolInstruction symInstr) {
		int SSANumber = this.ssaAnalyser.addDefinition(symInstr.getSymbol());
		SSADefSymbol newsym = GecosUserInstructionFactory.ssaDef(symInstr,SSANumber);
		GecosUserAnnotationFactory.copyAnnotations(symInstr, newsym);
		newsym.eAdapters().addAll(symInstr.eAdapters());
		this.ssaAnalyser.registerSSADefinition(newsym);
		debug("  Adding <"+SSANumber+","+newsym+"> for "+symInstr.getSymbol()+" in "+symInstr.getRoot());
		return newsym;
	}

	protected SSAUseSymbol makeSSAUse(SymbolInstruction symInst) {
		int SSANumber = this.ssaAnalyser.topVisibility(symInst.getSymbol());
		
		SSAUseSymbol ssaUse = GecosUserInstructionFactory.ssaUse(symInst,SSANumber);
		GecosUserAnnotationFactory.copyAnnotations(symInst, ssaUse);
		ssaUse.eAdapters().addAll(symInst.eAdapters());
		
		if(SSANumber!=0) {
			SSADefSymbol ssaDef = this.ssaAnalyser.findSSADefinition(symInst.getSymbol(), SSANumber);
			if(ssaDef!=null) {
				debug("  Found "+ssaDef.getRoot()+" at <"+SSANumber+","+symInst+"> for "+ssaUse);
				ssaUse.setDef(ssaDef);
			} else {
				debug("  Could not find definition for <"+SSANumber+","+symInst+">");
			}
	
			debug("  - Reference "+symInst+" => "+ssaUse+" in "+symInst.eContainer());
			buildDataEdge(ssaUse);
			return ssaUse;
		} else if (SSANumber==0) {
			debug("  Found use of a uninitilazed (or parameter) variable "+symInst+" in "+symInst.getRoot());
			return ssaUse;
		}
		throw new UnsupportedOperationException("Not yet Implemented");
	}

	protected void buildDataEdge(SSAUseSymbol ssaUse) {
		SSADefSymbol ssaDef = ssaUse.getDef();
		if(ssaDef!=null) {
			BasicBlock defBBlock = ssaDef.getBasicBlock();
			if(defBBlock!=null) {
			DataEdge dataEdge = null;
			for(DataEdge de : defBBlock.getDefEdges()) {
				if(de.getTo()==currentBB) {
					de.getUses().add(ssaUse);
					return;
				}
			}

			dataEdge = BlocksFactory.eINSTANCE.createDataEdge();
			dataEdge.getUses().add(ssaUse);

			if(currentBB!=null ) {
				dataEdge.setTo(currentBB);
				defBBlock.getDefEdges().add(dataEdge);
			}
			}
		}
	}

}