/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.cdtfrontend.visitor;

import java.util.List;

import org.eclipse.cdt.core.dom.ast.IASTDeclSpecifier;
import org.eclipse.cdt.core.dom.ast.IASTDeclarator;
import org.eclipse.cdt.core.dom.ast.IASTParameterDeclaration;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTParameterDeclaration;

import fr.irisa.cairn.gecos.model.cdtfrontend.exception.CDTParserErrorException;
import fr.irisa.cairn.gecos.model.cdtfrontend.visitor.ASTDeclaratorVisitor.ParameterSymbolInfo;
import gecos.annotations.PragmaAnnotation;
import gecos.types.Type;

/**
 * @author mnaullet
 */
public class ASTParameterDeclarationVisitor {

	private CDTToGecosModel gecosModel;
	
	private ParameterSymbolInfo info;
	
	public ASTParameterDeclarationVisitor() {
	}
	
	public ParameterSymbolInfo getParameterSymbolInfo() {
		return info;
	}
	
	public void visit(CDTToGecosModel gecosModel, IASTParameterDeclaration parameterDeclaration) {
		this.gecosModel = gecosModel;
		if (parameterDeclaration instanceof ICPPASTParameterDeclaration)
			buildICPPASTParameterDeclaration((ICPPASTParameterDeclaration)parameterDeclaration);
		else
			buildIASTParameterDeclaration(parameterDeclaration);
	}
	
	private void buildICPPASTParameterDeclaration(ICPPASTParameterDeclaration parameterDeclaration) {
		buildIASTParameterDeclaration(parameterDeclaration);
	}
	
	private void buildIASTParameterDeclaration(IASTParameterDeclaration parameterDeclaration) {
		List<PragmaAnnotation> pragmaAnnotations = gecosModel.findAnnotation(parameterDeclaration.getFileLocation().getFileName(), parameterDeclaration.getFileLocation().getNodeOffset());
		
		IASTDeclSpecifier declSpecifier = parameterDeclaration.getDeclSpecifier();
		declSpecifier.accept(gecosModel);
		ASTDeclSpecifierVisitor declSpecifierVisitor = (ASTDeclSpecifierVisitor)gecosModel.getObject();
		Type type = declSpecifierVisitor.getType();
		if (type == null)
			throw new CDTParserErrorException(parameterDeclaration, "Could not determine type for "+parameterDeclaration.getRawSignature());

		IASTDeclarator declarator = parameterDeclaration.getDeclarator();
		declarator.accept(gecosModel);
		ASTDeclaratorVisitor declaratorVisitor = (ASTDeclaratorVisitor)gecosModel.getObject();
		
		String name = declaratorVisitor.getName();
		type = declaratorVisitor.buildType(type);
	
		info = new ParameterSymbolInfo(name, type, pragmaAnnotations);
	}

}