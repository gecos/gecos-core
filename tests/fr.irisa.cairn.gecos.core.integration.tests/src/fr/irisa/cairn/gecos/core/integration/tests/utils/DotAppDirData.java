package fr.irisa.cairn.gecos.core.integration.tests.utils;

import static fr.irisa.cairn.gecos.core.testframework.impl.GSOperators.customDataConvertor;
import static fr.irisa.cairn.gecos.core.testframework.impl.GSOperators.versionCopier;
import static fr.irisa.cairn.gecos.core.testframework.utils.GSProjectUtils.setProjectSourceFiles;
import static fr.irisa.cairn.gecos.testframework.s2s.Comparators.fileEquals;
import static fr.irisa.cairn.gecos.testframework.s2s.Operators.fileGenerator;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.CODEGEN;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.CONVERT;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.LINK;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.TRANSFORM;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.VERIFY;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.chain;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.convert;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.forEach;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.forEachPairWithFirst;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.transform;
import static fr.irisa.cairn.gecos.testframework.utils.OperationUtils.filter;
import static fr.irisa.cairn.gecos.testframework.utils.OperationUtils.join;
import static java.util.stream.Collectors.joining;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import fr.irisa.cairn.gecos.core.testframework.impl.GSDefaultTestFlows;
import fr.irisa.cairn.gecos.core.testframework.impl.GSProjectVersion;
import fr.irisa.cairn.gecos.testframework.data.AbstractCxxData;
import fr.irisa.cairn.gecos.testframework.exceptions.CodegenFailure;
import fr.irisa.cairn.gecos.testframework.exceptions.InvalidDataException;
import fr.irisa.cairn.gecos.testframework.model.IVersion;
import fr.irisa.cairn.gecos.testframework.model.IVersionFactory;
import fr.irisa.cairn.gecos.testframework.s2s.TestFlow;
import fr.irisa.cairn.gecos.testframework.stages.Stages;
import fr.irisa.cairn.gecos.testframework.utils.DataProviderUtils;

public class DotAppDirData extends AbstractCxxData {
	
	public static final String BUNDLE_DIRNAME_PATTERN = "^\\S+\\.app$";
	
	public static boolean isValidPath(Path file) {
		return file != null && Files.isDirectory(file) 
				&& file.getFileName().toString().matches(BUNDLE_DIRNAME_PATTERN);
	}

	public static DotAppDirData createFromPath(Path dir) throws InvalidDataException {
		if(!isValidPath(dir))
			throw new InvalidDataException("Not a valid data: " + dir);
	
		DotAppDirData data = new DotAppDirData();
		data.path = dir;
		data.srcFiles = DataProviderUtils.lookupFiles(dir, FileNamePatterns.CXX_SRC).collect(Collectors.toList());
		data.incDirs = DataProviderUtils.lookupDirsContainingFile(dir, FileNamePatterns.HEADERFILE_NAME_PATTERN).collect(Collectors.toList());
		data.makefile = DataProviderUtils.lookupFiles(dir, 1, FileNamePatterns.MAKEFILE).findFirst().orElse(null);
		return data;
	}

	
	
	/* *** Default Test flow *** */
	
	// !!! these should match the filenames specified in the makefile.
	public static final String MAKEFILE_CONFIG_NAME = "makefile.cfg";
	public static final String GEN_MAKEFILE_NAME = "makefile";
	public static final String OUTPUT_FILE_EXTENSION = ".x86.output";
	
	public static final String OUTPUT_FILE_ARG_KEY = "key::outputfile";
	
	
	public static TestFlow defaultDotAppDirDataTestFlow(
			Function<GSProjectVersion, String> makefileCompileTarget,
			Function<GSProjectVersion, String> makefileRunTarget) {
		IVersionFactory<GSProjectVersion> versionFactory = GSProjectVersion::new;
		Predicate<Path> sourceFilter = s -> !s.getFileName().toString().startsWith("main.");
		
		return GSDefaultTestFlows.cxxMakefileDataTestFlow(makefileCompileTarget, makefileRunTarget)
			.replaceStage(CONVERT, convert(customDataConvertor(versionFactory,
					(data, proj) -> setProjectSourceFiles(proj, filter(data.getSourceFiles(), sourceFilter)), 
					(data, v) -> v.setExternalSources(filter(data.getSourceFiles(), sourceFilter.negate())))))
			.replaceStage(TRANSFORM, transform(versionCopier(versionFactory, v -> 
					v.setExternalSources(v.getPrevious().getExternalSourceFiles()))))
			.addStageAfterLast(CODEGEN, chain(
					forEach(fileGenerator(DotAppDirData::getGenMakefile, DotAppDirData::generateMakefile)),
					forEach(v -> v.setMakefile(getGenMakefile(v)))))
			.addStageAfterLast(LINK, Stages.<GSProjectVersion>forEach(DotAppDirData::setOutputFile))
			.replaceStage(VERIFY, forEachPairWithFirst(fileEquals(DotAppDirData::getOutputFile)));
	}
	
	public static String defaultMakeCompileTarget(GSProjectVersion v) {
		return "compx BUILD_DIR=\"" + v.getOutputDir().toAbsolutePath() + "\"";
	}
	
	public static  String defaultMakeRunTarget(GSProjectVersion v) {
		return "runx BUILD_DIR=\"" + v.getOutputDir().toAbsolutePath() + "\"";
	}
	
	public static Path getOutputFile(IVersion v) {
		return v.getAnnotation(OUTPUT_FILE_ARG_KEY);
	}
	
	private static Path getGenMakefile(IVersion v) {
		return v.getOutputDir().resolve(GEN_MAKEFILE_NAME);
	}

	private static void setOutputFile(GSProjectVersion v) {
		v.addAnnotationIfNew(OUTPUT_FILE_ARG_KEY, 
				v.getOutputDir().resolve(v.getProject().getName()+OUTPUT_FILE_EXTENSION));
	}
	
	private static String generateMakefile(GSProjectVersion v) {
		Path rootMakefile = v.getMakefile();
		if(rootMakefile == null)
			throw new CodegenFailure("[CODEGEN] could not find root makefile for version " + v);
		
		String verDir = v.getOutputDir().toAbsolutePath().toString(); 
		String srcFiles = join(v.getGeneratedSourceFiles(), s -> s.toAbsolutePath().toString()) 
				+ " " + join(v.getExternalSourceFiles(), s -> s.toAbsolutePath().toString());
		
		return Stream.of(
			"## This Makefile was automatically generated by " + DotAppDirData.class.getName() + " ##",
			"",
			"INC_DIRS = " + verDir,
			"SRCS = " + srcFiles,
			"",
        	"-include " + MAKEFILE_CONFIG_NAME,
			"include " + rootMakefile.toAbsolutePath()
		).collect(joining("\n"));
	}

}
