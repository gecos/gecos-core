/**
 */
package profiling.frontend.directives;

import gecos.blocks.Block;

import gecos.core.Symbol;

import gecos.instrs.Instruction;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Save Directive</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link profiling.frontend.directives.SaveDirective#getNAME <em>NAME</em>}</li>
 *   <li>{@link profiling.frontend.directives.SaveDirective#getAddress <em>Address</em>}</li>
 *   <li>{@link profiling.frontend.directives.SaveDirective#getSizesOutermostFirst <em>Sizes Outermost First</em>}</li>
 * </ul>
 *
 * @see profiling.frontend.directives.DirectivesPackage#getSaveDirective()
 * @model
 * @generated
 */
public interface SaveDirective extends Directive {
	/**
	 * Returns the value of the '<em><b>NAME</b></em>' attribute.
	 * The default value is <code>"$save"</code>.
	 * The literals are from the enumeration {@link profiling.frontend.directives.DirectiveName}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>NAME</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>NAME</em>' attribute.
	 * @see profiling.frontend.directives.DirectiveName
	 * @see #setNAME(DirectiveName)
	 * @see profiling.frontend.directives.DirectivesPackage#getSaveDirective_NAME()
	 * @model default="$save" unique="false"
	 * @generated
	 */
	DirectiveName getNAME();

	/**
	 * Sets the value of the '{@link profiling.frontend.directives.SaveDirective#getNAME <em>NAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>NAME</em>' attribute.
	 * @see profiling.frontend.directives.DirectiveName
	 * @see #getNAME()
	 * @generated
	 */
	void setNAME(DirectiveName value);

	/**
	 * Returns the value of the '<em><b>Address</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Address</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Address</em>' reference.
	 * @see #setAddress(Symbol)
	 * @see profiling.frontend.directives.DirectivesPackage#getSaveDirective_Address()
	 * @model
	 * @generated
	 */
	Symbol getAddress();

	/**
	 * Sets the value of the '{@link profiling.frontend.directives.SaveDirective#getAddress <em>Address</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Address</em>' reference.
	 * @see #getAddress()
	 * @generated
	 */
	void setAddress(Symbol value);

	/**
	 * Returns the value of the '<em><b>Sizes Outermost First</b></em>' reference list.
	 * The list contents are of type {@link gecos.instrs.Instruction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sizes Outermost First</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sizes Outermost First</em>' reference list.
	 * @see profiling.frontend.directives.DirectivesPackage#getSaveDirective_SizesOutermostFirst()
	 * @model
	 * @generated
	 */
	EList<Instruction> getSizesOutermostFirst();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; args = this.getCall().getArgs();\nboolean _isEmpty = args.isEmpty();\nif (_isEmpty)\n{\n\t&lt;%java.lang.String%&gt; _printLocation = this.printLocation();\n\t&lt;%java.lang.String%&gt; _plus = (_printLocation + \"\\nMust have at least one argument (Symbol)!\");\n\tthrow new &lt;%fr.irisa.cairn.gecos.core.profiling.frontend.InvalidDirective%&gt;(_plus);\n}\n&lt;%gecos.core.Symbol%&gt; _xtrycatchfinallyexpression = null;\ntry\n{\n\t&lt;%gecos.instrs.Instruction%&gt; _get = args.get(0);\n\t&lt;%gecos.core.Symbol%&gt; _symbol = ((&lt;%gecos.instrs.SymbolInstruction%&gt;) _get).getSymbol();\n\t_xtrycatchfinallyexpression = _symbol;\n}\ncatch (final Throwable _t) {\n\tif (_t instanceof &lt;%java.lang.Exception%&gt;) {\n\t\t&lt;%java.lang.String%&gt; _printLocation_1 = this.printLocation();\n\t\t&lt;%java.lang.String%&gt; _plus_1 = (_printLocation_1 + \"\\nMust have a SymbolInstruction as first argument!\");\n\t\tthrow new &lt;%fr.irisa.cairn.gecos.core.profiling.frontend.InvalidDirective%&gt;(_plus_1);\n\t}\n\telse\n\t{\n\t\tthrow &lt;%org.eclipse.xtext.xbase.lib.Exceptions%&gt;.sneakyThrow(_t);\n\t}\n}\nthis.setAddress(_xtrycatchfinallyexpression);\n&lt;%gecos.types.Type%&gt; _type = this.getAddress().getType();\nfinal &lt;%fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer%&gt; ta = new &lt;%fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer%&gt;(_type, true);\nint _totalNbDims = ta.getTotalNbDims();\n&lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt; _doubleDotLessThan = new &lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt;(0, _totalNbDims, true);\nfor (final &lt;%java.lang.Integer%&gt; dim : _doubleDotLessThan)\n{\n\t{\n\t\t&lt;%gecos.instrs.Instruction%&gt; _xifexpression = null;\n\t\tint _size = args.size();\n\t\tboolean _greaterThan = (_size &gt; ((dim).intValue() + 1));\n\t\tif (_greaterThan)\n\t\t{\n\t\t\t_xifexpression = args.get((1 + (dim).intValue()));\n\t\t}\n\t\telse\n\t\t{\n\t\t\t_xifexpression = &lt;%fr.irisa.cairn.gecos.core.profiling.frontend.DirectivesProcessor%&gt;.getDimensionSize(this.getAddress(), (dim).intValue());\n\t\t}\n\t\tfinal &lt;%gecos.instrs.Instruction%&gt; sz = _xifexpression;\n\t\tif ((sz == null))\n\t\t{\n\t\t\t&lt;%java.lang.String%&gt; _printLocation_2 = this.printLocation();\n\t\t\t&lt;%java.lang.String%&gt; _plus_2 = (_printLocation_2 + \"\\nMust specify a size for dimension \\\'\");\n\t\t\t&lt;%java.lang.String%&gt; _plus_3 = (_plus_2 + dim);\n\t\t\t&lt;%java.lang.String%&gt; _plus_4 = (_plus_3 + \"\\\' which couldn\\\'t be determined automatically!\");\n\t\t\tthrow new &lt;%fr.irisa.cairn.gecos.core.profiling.frontend.InvalidDirective%&gt;(_plus_4);\n\t\t}\n\t\tthis.getSizesOutermostFirst().add(sz);\n\t}\n}'"
	 * @generated
	 */
	void parse();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.core.ProcedureSet%&gt; ps = this.getCall().getContainingProcedureSet();\nfinal int symbolUid = &lt;%fr.irisa.cairn.gecos.core.profiling.frontend.UidProvider%&gt;.setUID(this.getAddress());\nfinal &lt;%gecos.blocks.CompositeBlock%&gt; cb = &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory%&gt;.CompositeBlock();\n&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory%&gt;.comment(cb, this.getCall().toString());\n&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory%&gt;.setScope(cb.getScope());\nfinal &lt;%gecos.blocks.BasicBlock%&gt; innermostBody = &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory%&gt;.BBlock();\n&lt;%gecos.blocks.Block%&gt; currentBlock = ((&lt;%gecos.blocks.Block%&gt;) innermostBody);\nfinal &lt;%java.util.LinkedList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; indexesOutermostFirst = new &lt;%java.util.LinkedList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;();\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; _reverseView = &lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;reverseView(this.getSizesOutermostFirst());\nfor (final &lt;%gecos.instrs.Instruction%&gt; dimSize : _reverseView)\n{\n\t{\n\t\tfinal &lt;%gecos.core.Symbol%&gt; index = &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory%&gt;.symbol(\"i\", &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory%&gt;.INT(), cb.getScope());\n\t\tcb.getScope().makeUnique(index);\n\t\tindexesOutermostFirst.addFirst(&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory%&gt;.symbref(index));\n\t\tcurrentBlock = &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory%&gt;.For(index, 0, dimSize.copy(), 1, currentBlock);\n\t}\n}\nfinal &lt;%gecos.instrs.CallInstruction%&gt; pushValue = &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory%&gt;.call(\n\t&lt;%fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport%&gt;.getNativeProc(ps, &lt;%fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport.NativeFunction%&gt;.PUSH_VALUE), \n\t&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory%&gt;.Int(symbolUid), \n\t&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory%&gt;.string(this.getAddress().getName()), \n\t&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory%&gt;.array(this.getAddress(), indexesOutermostFirst));\ninnermostBody.addInstruction(pushValue);\ncb.addChildren(currentBlock);\nfinal &lt;%gecos.blocks.BasicBlock%&gt; finishBlock = &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory%&gt;.BBlock(\n\t&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory%&gt;.call(\n\t\t&lt;%fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport%&gt;.getNativeProc(ps, &lt;%fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport.NativeFunction%&gt;.SAVE_SNAPSHOT), \n\t\t&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory%&gt;.Int(symbolUid)));\ncb.addChildren(finishBlock);\nreturn cb;'"
	 * @generated
	 */
	Block createCodeBlock();

} // SaveDirective
