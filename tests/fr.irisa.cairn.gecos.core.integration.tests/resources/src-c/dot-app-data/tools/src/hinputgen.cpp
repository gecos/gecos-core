#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int main(int argc, const char **argv) {
	if(argc < 6) {
		printf("hinputgen M N float? min max\n");
		return -1;
	}
	int m = atoi(argv[1]);
	int n = atoi(argv[2]);
	int isfloat = atoi(argv[3]);
	int min = atoi(argv[4]);
	int max = atoi(argv[5]);
	int t = -1;
	if(argc >= 7) t = atoi(argv[6]);
	int i,j;

	srand(time(NULL));

	printf("#define M\t%d\n", m);
	printf("#define N\t%d\n", n);
	if(t != -1)printf("#define T\t%d\n", t);
	printf("\n#ifdef _MAIN_\n");
	printf("TYPE_0 in[M][N] = { ");
	for(i=0; i<(m*n)-1; i++) {
		if(isfloat)
			printf("%f,", (float)((rand() % (max + 1 - min)) + min));
		else
			printf("%d,", (int)((rand() % (max + 1 - min)) + min));
	}
	if(isfloat)
		printf("%f };\n", (float)((rand() % (max + 1 - min)) + min));
	else
		printf("%d };\n", (int)((rand() % (max + 1 - min)) + min));

	printf("#endif\n");

	return 0;
}
