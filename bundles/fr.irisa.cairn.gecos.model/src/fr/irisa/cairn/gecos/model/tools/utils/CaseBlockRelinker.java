package fr.irisa.cairn.gecos.model.tools.utils;

import gecos.annotations.ExtendedAnnotation;
import gecos.annotations.IAnnotation;
import gecos.blocks.Block;
import gecos.blocks.SwitchBlock;

import org.eclipse.emf.common.util.EMap;

/**
 * 
 * @generated NOT
 *
 */
public class CaseBlockRelinker extends BlocksDefaultSwitch<Object> {
	
	public final static String RELINKING_KEY = "caseLinking";
	
	SwitchBlock newCopy;
	
	public CaseBlockRelinker(SwitchBlock target) {
		newCopy=target;
	}
	
	public void relink() {
		doSwitch(newCopy);
	}

	@Override
	public Object caseBlock(Block elmt) {

		if (elmt.getAnnotations().containsKey(RELINKING_KEY)) {
			IAnnotation annotation = elmt.getAnnotation(RELINKING_KEY);
			if (annotation instanceof ExtendedAnnotation) {
				ExtendedAnnotation ext2 = (ExtendedAnnotation) annotation;
				SwitchBlock switchBlock = (SwitchBlock) ext2.getFields().get(0);
				Block copyCaseBlock = elmt;
				Block caseBlock = (Block) ext2.getFields().get(1);
				EMap<Long, Block> cases = switchBlock.getCases();
				
				for (Long key : cases.keySet()) {
					if (cases.get(key).equals(caseBlock)) {
					  newCopy.getCases().put(key, copyCaseBlock);
					  // remove annotations in the copy of the Switch Body  
					  elmt.getAnnotations().remove(ext2);
					  break;
					}
				}
			}
		} 
		return super.caseBlock(elmt);
	}

}
