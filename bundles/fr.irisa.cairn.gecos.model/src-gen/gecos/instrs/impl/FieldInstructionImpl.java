/**
 */
package gecos.instrs.impl;

import gecos.instrs.FieldInstruction;
import gecos.instrs.InstrsPackage;
import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;

import gecos.types.Field;
import gecos.types.Type;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Field Instruction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.impl.FieldInstructionImpl#getField <em>Field</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FieldInstructionImpl extends ExprComponentImpl implements FieldInstruction {
	/**
	 * The cached value of the '{@link #getField() <em>Field</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getField()
	 * @generated
	 * @ordered
	 */
	protected Field field;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FieldInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstrsPackage.eINSTANCE.getFieldInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Field getField() {
		return field;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setField(Field newField) {
		Field oldField = field;
		field = newField;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InstrsPackage.FIELD_INSTRUCTION__FIELD, oldField, field));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final InstrsVisitor visitor) {
		visitor.visitFieldInstruction(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _elvis = null;
		Type _type = this.getType();
		String _string = null;
		if (_type!=null) {
			_string=_type.toString();
		}
		if (_string != null) {
			_elvis = _string;
		} else {
			_elvis = "?";
		}
		String _plus = ("((" + _elvis);
		String _plus_1 = (_plus + ")");
		String _elvis_1 = null;
		Field _field = this.getField();
		String _string_1 = null;
		if (_field!=null) {
			_string_1=_field.toString();
		}
		if (_string_1 != null) {
			_elvis_1 = _string_1;
		} else {
			_elvis_1 = "?";
		}
		String _plus_2 = (_plus_1 + _elvis_1);
		return (_plus_2 + ")");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean equals(final Object obj) {
		if ((obj instanceof FieldInstruction)) {
			Field _field = ((FieldInstruction)obj).getField();
			boolean _tripleNotEquals = (_field != null);
			if (_tripleNotEquals) {
				return (super.equals(obj) && ((FieldInstruction)obj).getField().equals(this.getField()));
			}
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSame(final Instruction instr) {
		if ((instr instanceof FieldInstruction)) {
			boolean isSame = false;
			if (((((FieldInstruction)instr).getField() == null) && (this.getField() == null))) {
				isSame = true;
			}
			else {
				Field _field = ((FieldInstruction)instr).getField();
				boolean _tripleEquals = (_field == null);
				Field _field_1 = this.getField();
				boolean _tripleEquals_1 = (_field_1 == null);
				boolean _xor = (_tripleEquals ^ _tripleEquals_1);
				if (_xor) {
					return false;
				}
				else {
					isSame = ((FieldInstruction)instr).getField().isEqual(this.getField());
				}
			}
			return (isSame && super.isSame(instr));
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InstrsPackage.FIELD_INSTRUCTION__FIELD:
				return getField();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InstrsPackage.FIELD_INSTRUCTION__FIELD:
				setField((Field)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InstrsPackage.FIELD_INSTRUCTION__FIELD:
				setField((Field)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InstrsPackage.FIELD_INSTRUCTION__FIELD:
				return field != null;
		}
		return super.eIsSet(featureID);
	}

} //FieldInstructionImpl
