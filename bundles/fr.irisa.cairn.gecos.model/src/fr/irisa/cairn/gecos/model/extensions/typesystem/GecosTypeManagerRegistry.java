package fr.irisa.cairn.gecos.model.extensions.typesystem;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;

import fr.irisa.cairn.gecos.model.extensions.GecosRegistry;

/**
 * Register all the generators pointed by the <i>gecos.generators</i> extension
 * point.
 * 
 * @author Antoine Floc'h - Initial contribution and API
 * 
 */
final class GecosTypeManagerRegistry extends GecosRegistry {
	public final static GecosTypeManagerRegistry INSTANCE = new GecosTypeManagerRegistry();

	private final static String EXTENSION_POINT = "fr.irisa.cairn.gecos.model.GecosTypeSystem";
	private final static String EXTENSION_ATTRIBUTE = "extension";
	private final static String EXTENDED_ATTRIBUTE = "extended";

	private TypeSystemExtensionFactory extensionFactory;

	private GecosTypeManagerRegistry() {
		this.extensionFactory = new TypeSystemExtensionFactory();
	} 

	private List<Class<? extends ITypeSystemRuleSet>> getAllTypeSystemExtensionsClassesFor(
			ITypeSystemRuleSet extended) {
		List<Class<? extends ITypeSystemRuleSet>> extensions = new ArrayList<Class<? extends ITypeSystemRuleSet>>();
		for (IExtension e : getExtensions(EXTENSION_POINT)) {
			for (IConfigurationElement iConfigurationElement : e
					.getConfigurationElements()) {
				String extensionClassID = iConfigurationElement
						.getAttribute(EXTENSION_ATTRIBUTE);
				String extendedClassID = iConfigurationElement
						.getAttribute(EXTENDED_ATTRIBUTE);

				Class<ITypeSystemRuleSet> extendedType = getClass(
						extendedClassID, iConfigurationElement);
				if (extended.getClass() == extendedType) {
					Class<ITypeSystemRuleSet> extensionClass = getClass(
							extensionClassID, iConfigurationElement);
					extensions.add(extensionClass);
				}
			}
		}
		return extensions;
	}

	public List<ITypeSystemRuleSet> getAllTypeSystemExtensionsFor(
			ITypeSystemRuleSet extended) {
		List<ITypeSystemRuleSet> extensions = new ArrayList<ITypeSystemRuleSet>();
		List<Class<? extends ITypeSystemRuleSet>> allGeneratorExtensionsClassesFor = getAllTypeSystemExtensionsClassesFor(extended);
		for (Class<? extends ITypeSystemRuleSet> generatorClass : allGeneratorExtensionsClassesFor) {
			extensions.add(extensionFactory.buildGenerator(generatorClass));
		}
		return extensions;

	}
}