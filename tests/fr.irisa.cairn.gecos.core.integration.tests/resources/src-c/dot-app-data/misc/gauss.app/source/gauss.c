#include "defs.h"
#include "data_size.h"

#pragma MAIN_FUNC
void gauss(
#pragma DYNAMIC[0, 255]
	TYPE_0 in[M][N],
#pragma OUTPUT
	TYPE_1 out[M][N]){

	TYPE_1 tmp0[M][N] = {0};

	TYPE_0 coef[3] = {2, 1 , 4};


	int i, j;
	int c0;
	int c1;
	int c3;

	for(c0 = 0; c0 < M-1; c0 = c0 + 2) {
		for(c1 = 0; c1 < N; c1 = c1 + 2) {
			if ((c0 >= 2) && (c0 < M-3) && (c1 >= 2)) {
				#pragma __RLT_INSET__
				{
					tmp0[c0][c1] = (coef[0] * in[c0][c1] + coef[1] * in[c0 - 1][c1] + coef[1] * in[c0 + 1][c1]) / coef[2];
					out[c0][c1 - 1] = (coef[0] * tmp0[c0][c1 - 1] + coef[1] * tmp0[c0][c1 - 2] + coef[1] * tmp0[c0][c1]) / coef[2];
					tmp0[c0][c1 + 1] = (coef[0] * in[c0][c1 + 1] + coef[1] * in[c0 - 1][c1 + 1] + coef[1] * in[c0 + 1][c1 + 1]) / coef[2];
					out[c0][c1] = (coef[0] * tmp0[c0][c1] + coef[1] * tmp0[c0][c1 - 1] + coef[1] * tmp0[c0][c1 + 1]) / coef[2];
					tmp0[c0 + 1][c1] = (coef[0] * in[c0 + 1][c1] + coef[1] * in[c0][c1] + coef[1] * in[c0 + 2][c1]) / coef[2];
					out[c0 + 1][c1 - 1] = (coef[0] * tmp0[c0 + 1][c1 - 1] + coef[1] * tmp0[c0 + 1][c1 - 2] + coef[1] * tmp0[c0 + 1][c1]) / coef[2];
					tmp0[c0 + 1][c1 + 1] = (coef[0] * in[c0 + 1][c1 + 1] + coef[1] * in[c0][c1 + 1] + coef[1] * in[c0 + 2][c1 + 1]) / coef[2];
					out[c0 + 1][c1] = (coef[0] * tmp0[c0 + 1][c1] + coef[1] * tmp0[c0 + 1][c1 - 1] + coef[1] * tmp0[c0 + 1][c1 + 1]) / coef[2];
				}
			} else {
				if (c0 == M-2) {
					for(c3 = 0; c3 <= 1; c3 = c3 + 1) {
						tmp0[c0][c1 + c3] = (2 * in[c0][c1 + c3] + coef[1] * in[c0-1][c1 + c3] + coef[1] * in[c0+1][c1 + c3]) / coef[2];
						if (c1 + c3 >= 2) {
							out[c0][c1 + c3 - 1] = (2 * tmp0[c0][c1 + c3 - 1] + coef[1] * tmp0[c0][c1 + c3 - 2] + coef[1] * tmp0[c0][c1 + c3 + 0]) / coef[2];
						}
					}
				} else {
					if (c0 == 0) {
						for(c3 = 0; c3 <= 1; c3 = c3 + 1) {
							tmp0[1][c1 + c3] = (2 * in[1][c1 + c3] + coef[1] * in[0][c1 + c3] + coef[1] * in[2][c1 + c3]) / coef[2];
							if (c1 + c3 >= 2) {
								out[1][c1 + c3 - 1] = (2 * tmp0[1][c1 + c3 - 1] + coef[1] * tmp0[1][c1 + c3 - 2] + coef[1] * tmp0[1][c1 + c3 + 0]) / coef[2];
							}
						}
					} else {
						for(c3 = 0; c3 <= 1; c3 = c3 + 1) {
							tmp0[c0][c3] = (2 * in[c0][c3] + coef[1] * in[c0 - 1][c3] + coef[1] * in[c0 + 1][c3]) / coef[2];
						}
						for(c3 = 0; c3 <= 1; c3 = c3 + 1) {
							tmp0[c0 + 1][c3] = (2 * in[c0 + 1][c3] + coef[1] * in[c0][c3] + coef[1] * in[c0 + 2][c3]) / coef[2];
						}
					}
				}
			}
		}
	}
}
