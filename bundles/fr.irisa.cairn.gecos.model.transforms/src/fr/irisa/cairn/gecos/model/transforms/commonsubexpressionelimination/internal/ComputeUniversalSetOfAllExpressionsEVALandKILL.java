package fr.irisa.cairn.gecos.model.transforms.commonsubexpressionelimination.internal;

import java.util.ArrayList;
import java.util.StringTokenizer;

import fr.irisa.cairn.gecos.model.analysis.instructions.comparator.InstructionComparator;
import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDefaultVisitor;
import gecos.blocks.BasicBlock;
import gecos.blocks.CaseBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.ForC99Block;
import gecos.blocks.IfBlock;
import gecos.blocks.SimpleForBlock;
import gecos.blocks.SwitchBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Symbol;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
/**
 * @author imen
 */
public class ComputeUniversalSetOfAllExpressionsEVALandKILL extends GecosBlocksInstructionsDefaultVisitor {
	// Uexp = union of all evaluated expressions in all basic blocks
	private ArrayList<Instruction> Uexp;
	private ArrayList<String> Uexp_s;
	private int nbBB = 0;
	private ArrayList<ArrayList<Instruction>> ev;
	private ArrayList<ArrayList<String>> evalSymb; // Evaluated symbols for
													// each BB
	private ArrayList<Instruction> evalInstOfCurrentBB;
	private ArrayList<String> evalSymbcurrentBB;
	private ArrayList<String> ev_s;
	private ArrayList<Integer> BBId;
	private ArrayList<Integer> BBDepth;
	private ArrayList<BasicBlock> BasicB;
	private int depth;
	private boolean pointer_conservative;
	private boolean consider_function_call;
	
	public ArrayList<Integer> getBBDepth() {
		return this.BBDepth;
	}
	
	public ArrayList<BasicBlock> getBasicB() {
		return this.BasicB;
	}
	
	public ArrayList<Integer> getBBId() {
		return this.BBId;
	}
	
	public ArrayList<ArrayList<Instruction>> getev() {
		return this.ev;
	}

	public ArrayList<ArrayList<String>> getevalSymb() {
		return this.evalSymb;
	}

	public int getnbBB() {
		return this.nbBB;
	}

	public ArrayList<Instruction> getUexp() {
		return this.Uexp;
	}

	public ArrayList<String> getUexp_s() {
		return this.Uexp_s;
	}

	public ComputeUniversalSetOfAllExpressionsEVALandKILL(boolean bf, boolean bp) {
		super();
		this.consider_function_call = bf;
		this.pointer_conservative = bp;
		this.Uexp = new ArrayList<Instruction>();
		this.Uexp_s = new ArrayList<String>();
		nbBB = 0;
		this.ev = new ArrayList<ArrayList<Instruction>>();
		this.ev_s = new ArrayList<String>();
		this.evalSymb = new ArrayList<ArrayList<String>>();
		this.BBId = new ArrayList<Integer>();
		this.BBDepth = new ArrayList<Integer>();
		this.BasicB = new ArrayList<BasicBlock>();
		depth = 0;
	}

	@Override
	public void visitBasicBlock(BasicBlock b) {
		evalSymbcurrentBB = new ArrayList<String>();
		evalInstOfCurrentBB = new ArrayList<Instruction>();
		ev_s.clear();
		super.visitBasicBlock(b);
		nbBB++;
		ev.add(evalInstOfCurrentBB);
		evalSymb.add(evalSymbcurrentBB);
		BBId.add(b.getNumber());
		BBDepth.add(depth);
		BasicB.add(b);
	}

	private boolean strContainsAsToken(String str1, String str2) {
		StringTokenizer st = new StringTokenizer(str1, " ,()/*-+<>=!&|");
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (token.equals(str2))
				return true;
		}
		return false;
	}
	
	
	
	
	private boolean operandsModified(ArrayList<Instruction> listInstr, Instruction opr, Instruction opr2, boolean fctCall) {
		String s1 = InstructionComparator.instToString(opr);
		String s2 = InstructionComparator.instToString(opr2);
		
		if (!consider_function_call) {
			if ((opr instanceof CallInstruction) || (opr2 instanceof CallInstruction))
				return true;
		}
		
		boolean oprdModified =  false;
		for (Instruction dest : listInstr) {
			String d = InstructionComparator.instToString(dest);
			if (!fctCall) {
				if (dest instanceof SymbolInstruction) {
					TypeAnalyzer type_var = new TypeAnalyzer(((SymbolInstruction) dest).getSymbol().getType());
					if (type_var.isPointer()) {
						
						if (pointer_conservative)
							return true;
						
					//	System.out.println("dest :" +	InstructionComparator.instToString (dest) + "----" );
						/**
						 * case 1 
						 *    *u + .....;
						 *    u = ...; 
						 *    *u + .....;
						 */
						String instr_str = "*(" + d + ")";
						if (s1.equals(instr_str) || s2.equals(instr_str))
							return true;
					} else {
						if (type_var.isArray()) {
							
							if (pointer_conservative)
								return true;
							
							/**
							 * case 2 
							 *    u[] + .....; 
							 *    u = ... ; 
							 *    u[] + .....;
							 */	
							String instr_str = d + "[";
							if ((s1.startsWith(instr_str) && s1.endsWith("]")) 
									|| (s2.startsWith(instr_str) && s2.endsWith("]")))
								return true;						
						}
					}
				} else {
					/**
					 * case 3 
					 *     	p = &u;
					 *      u + .....;
					 *      p = ..;
					 *      u + .....;
					 */
					if (dest instanceof AddressInstruction) {
						
						if (pointer_conservative)
							return true;
						
						// System.out.println("dest :" +
						// InstructionComparator.instToString (dest) + "----" +
						// InstructionComparator.instToString(((AddressInstruction)dest).getAddress()));

						String instr_str = InstructionComparator.instToString(((AddressInstruction) dest).getAddress());

						
						if (!evalSymbcurrentBB.contains(instr_str)) {
							evalSymbcurrentBB.add(instr_str);
						}
						
						if (s1.equals(instr_str) || s2.equals(instr_str))
							return true;
						
						
						//***********
						
						String d_aux= InstructionComparator.instToString(((AddressInstruction) dest).getAddress());
						
						instr_str = "*(" + d_aux + ")";
						if (s1.equals(instr_str) || s2.equals(instr_str))
							return true;
						
						instr_str = d_aux + "[";
						if ((s1.startsWith(instr_str) && s1.endsWith("]")) 
								|| (s2.startsWith(instr_str) && s2.endsWith("]")))
							return true;
						
						//***********
					}
				}
				
				/** case 3
				 *      u + .....;
				 *      u = ... ;
				 *      u + .....;
				 */
				oprdModified = oprdModified || ( d.equals(s1) || d.equals(s2) );
				if (oprdModified)
					return true;
				
			} else {
				if (dest instanceof SymbolInstruction) {
					TypeAnalyzer type_var = new TypeAnalyzer(((SymbolInstruction) dest).getSymbol().getType());
					if (type_var.isPointer()) {
						if (!evalSymbcurrentBB.contains(d)) {
							evalSymbcurrentBB.add(d);
						}
						
						if (pointer_conservative)
							return true;
						
						/**
						 * case 1 
						 *        *u + .....; 
						 *        f(u); 
						 *        *u + .....;
						 */
						String instr_str = "*(" + d + ")";
						if (s1.equals(instr_str) || s2.equals(instr_str))
							return true;
					} else {
						if (type_var.isArray()) {
							if (!evalSymbcurrentBB.contains(d)) {
								evalSymbcurrentBB.add(d);
							}
							
							if (pointer_conservative)
								return true;
							
							// System.out.println("dest :" +
							// InstructionComparator.instToString (dest) +
							// "----" );
							/**
							 * case 2 
							 *        u[] + .....; 
							 *        f(u); 
							 *        u[] + .....;
							 */
							String instr_str = d + "[";
							if ((s1.startsWith(instr_str) && s1.endsWith("]"))
									|| (s2.startsWith(instr_str) && s2.endsWith("]")))
								return true;
						}
					}
				} else {
					/**
					 * case 3 
					 *        u + .....; 
					 *        f(&u); 
					 *        u + .....;
					 */
					if (dest instanceof AddressInstruction) {
						
						if (pointer_conservative)
							return true;
						
						// System.out.println("dest :" +
						// InstructionComparator.instToString (dest) + "----" +
						// InstructionComparator.instToString(((AddressInstruction)dest).getAddress()));

						String instr_str = InstructionComparator.instToString(((AddressInstruction) dest).getAddress());

						
						if (!evalSymbcurrentBB.contains(instr_str)) {
							evalSymbcurrentBB.add(instr_str);
						}
						
						if (s1.equals(instr_str) || s2.equals(instr_str))
							return true;
						
						//***********
						
						String d_aux= InstructionComparator.instToString(((AddressInstruction) dest).getAddress());
						
						instr_str = "*(" + d_aux + ")";
						if (s1.equals(instr_str) || s2.equals(instr_str))
							return true;
						
						instr_str = d_aux + "[";
						if ((s1.startsWith(instr_str) && s1.endsWith("]")) 
								|| (s2.startsWith(instr_str) && s2.endsWith("]")))
							return true;
						
						//***********
					}
				}
			}
		}
		return oprdModified;
	}
	

	@Override
	public void visitCallInstruction(CallInstruction c) {  
		super.visitCallInstruction(c);
		ArrayList<Instruction> l = new ArrayList<Instruction>();
		
		for (Instruction st : c.getArgs()) {   
			
			/**
			 * useful for cases like (*p15)[0] or *(p16[0]) -> need to get the
			 * name of the pointer
			 */
			while (st instanceof IndirInstruction)
				st = ((IndirInstruction) st).getAddress();

			while (st instanceof ArrayInstruction)
				st = ((ArrayInstruction) st).getDest();

			while (st instanceof IndirInstruction)
				st = ((IndirInstruction) st).getAddress();
						
			/**
			 * useful for cases like (1+p14)[0]
			 */

			ArrayList<SymbolInstruction> listSymbolsGI = null;

			if (st instanceof GenericInstruction) {
				SymbolsCounter countSymbols = new SymbolsCounter();
				st.accept(countSymbols);
				listSymbolsGI = countSymbols.getlistSymbsInst();
			} else {
				listSymbolsGI = new ArrayList<SymbolInstruction> ();
				if (st instanceof SymbolInstruction)
					listSymbolsGI.add((SymbolInstruction)st);
			}

			for (Instruction st2 : listSymbolsGI) {
				l.add(st2);
				if (st2 instanceof SymbolInstruction) {
					Symbol symbl = ((SymbolInstruction) st2).getSymbol();
					if (symbl.getValue() != null)
						l.add(symbl.getValue());
				}
			}
		}
		
		ArrayList<String> ev_s2 = new ArrayList<String>(ev_s);
		for (String str : ev_s2) {
			int i = ev_s.indexOf(str);
			
			Instruction ist = evalInstOfCurrentBB.get(i);
			if (ist instanceof GenericInstruction) {
				GenericInstruction gi = (GenericInstruction)ist;
				if(operandsModified(l, gi.getChild(0), gi.getChild(1), true)) {
					evalInstOfCurrentBB.remove(i);
					ev_s.remove(i);
				}
			}
		}
	}

	@Override
	public void visitSetInstruction(SetInstruction s) { 
		super.visitSetInstruction(s);
		Instruction instexpr = s.getSource();
		String dest = InstructionComparator.instToString(s.getDest()).trim();
		String expr = InstructionComparator.instToString(instexpr).trim();

		if (!evalSymbcurrentBB.contains(dest)) {
			evalSymbcurrentBB.add(dest);
		}

		if (strContainsAsToken(expr, dest)) {
			if (Uexp_s.contains(expr)) {
				Uexp.remove(instexpr);
				Uexp_s.remove(expr);
			}
		}
		
		Instruction st = s.getDest();
		
		ArrayList<Instruction> l = new ArrayList<Instruction>();
		
		/**
		 * useful for cases like (*p15)[0] or *(p16[0]) -> need to get the
		 * name of the pointer
		 */
		while (st instanceof IndirInstruction)
			st = ((IndirInstruction) st).getAddress();

		while (st instanceof ArrayInstruction)
			st = ((ArrayInstruction) st).getDest();

		while (st instanceof IndirInstruction)
			st = ((IndirInstruction) st).getAddress();
				
		/**
		 * useful for cases like (1+p14)[0]
		 */

		ArrayList<SymbolInstruction> listSymbolsGI = null;

		if (st instanceof GenericInstruction) {
			SymbolsCounter countSymbols = new SymbolsCounter();
			st.accept(countSymbols);
			listSymbolsGI = countSymbols.getlistSymbsInst();
		} else {
			listSymbolsGI = new ArrayList<SymbolInstruction> ();
			if (st instanceof SymbolInstruction)
				listSymbolsGI.add((SymbolInstruction)st);
		}

		for (Instruction st2 : listSymbolsGI) {
			l.add(st2);
			if (st2 instanceof SymbolInstruction) {
				Symbol symbl = ((SymbolInstruction) st2).getSymbol();
				if (symbl.getValue() != null)
					l.add(symbl.getValue());
			}
		}
		
		
		ArrayList<String> ev_s2 = new ArrayList<String>(ev_s);
		for (String str : ev_s2) {
			
			int i = ev_s.indexOf(str);
			
			Instruction ist = evalInstOfCurrentBB.get(i);
			if (ist instanceof GenericInstruction) {
				GenericInstruction gi = (GenericInstruction)ist;
				if(operandsModified(l, gi.getChild(0), gi.getChild(1), false)) {
					evalInstOfCurrentBB.remove(i);
					ev_s.remove(i);
				}
			}
		}
	}

	@Override
	public void visitGenericInstruction(GenericInstruction g) {
		super.visitGenericInstruction(g);
		String expr = InstructionComparator.instToString(g);
		if (!Uexp_s.contains(expr)) {
			Uexp.add(g);
			Uexp_s.add(expr);
		}

		if (!ev_s.contains(expr)) {
			evalInstOfCurrentBB.add(g);
			ev_s.add(expr);
		}
	}

	@Override
	public void visitCaseBlock(CaseBlock c) {
		depth ++;
		super.visitCaseBlock(c);
		depth --;
	}
	
	@Override
	public void visitSimpleForBlock(SimpleForBlock f) {
		depth ++;
		super.visitSimpleForBlock(f);
		depth --;
	}

	@Override
	public void visitForC99Block(ForC99Block f) {
		depth ++;
		super.visitForC99Block(f);
		depth --;
	}

	@Override
	public void visitForBlock(ForBlock f) {
		depth ++;
		super.visitForBlock(f);
		depth --;
	}

	@Override
	public void visitIfBlock(IfBlock i) {
		depth ++;
		super.visitIfBlock(i);
		depth --;
	}

	@Override
	public void visitLoopBlock(DoWhileBlock l) {
		depth ++;
		super.visitLoopBlock(l);
		depth --;
	}

	@Override
	public void visitSwitchBlock(SwitchBlock s) {
		depth ++;
		super.visitSwitchBlock(s);
		depth --;
	}

	@Override
	public void visitWhileBlock(WhileBlock w) {
		depth ++;
		super.visitWhileBlock(w);
		depth --;
	}
}