package fr.irisa.cairn.gecos.model.utils;

import static java.util.stream.Collectors.toList;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import gecos.gecosproject.GecosProject;

public class ProjectUtils {

	public static List<Path> getProjectSourcesRelativeTo(GecosProject project, Path dir) {
		return project.getSources().stream()
			.map(gf -> Paths.get(gf.getName()).getFileName())
			.map(dir::resolve)
			.collect(toList());
	}

}
