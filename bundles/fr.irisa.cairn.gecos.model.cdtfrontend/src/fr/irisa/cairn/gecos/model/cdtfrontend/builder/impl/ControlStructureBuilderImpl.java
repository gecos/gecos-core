package fr.irisa.cairn.gecos.model.cdtfrontend.builder.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.irisa.cairn.gecos.model.cdtfrontend.builder.ControlStructureBuilder;
import fr.irisa.cairn.gecos.model.cdtfrontend.builder.CoreBuilder;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksFactory;
import gecos.blocks.CaseBlock;
import gecos.blocks.CompositeBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.ForC99Block;
import gecos.blocks.IfBlock;
import gecos.blocks.SwitchBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Scope;
import gecos.instrs.Instruction;

public class ControlStructureBuilderImpl implements ControlStructureBuilder {

	private BlocksFactory factory = BlocksFactory.eINSTANCE;
	private CoreBuilder coreBuilder;
	private Map<Block, Boolean> casedBlock;
	private List<SwitchBlock> switchBlocks;
	private CaseBlock caseBlock;

	public ControlStructureBuilderImpl(CoreBuilder coreBuilder) {
		this.coreBuilder = coreBuilder;
		casedBlock = new HashMap<Block, Boolean>();
		switchBlocks = new ArrayList<SwitchBlock>();
	}

	public BasicBlock createBasicBlock(Instruction... instructions) {
		BasicBlock basicBlock = factory.createBasicBlock();
		setBlockToCaseBlock(basicBlock);
		for(Instruction instruction : instructions) {
			basicBlock.getInstructions().add(instruction);
		}
		return basicBlock;
	}

	public CaseBlock createCaseBlock(long i) {
		if (caseBlock != null) {
			BasicBlock basicBlock = createBasicBlock();
			((CompositeBlock)coreBuilder.getParentBlock()).getChildren().add(basicBlock);
		}
		caseBlock = factory.createCaseBlock();
		caseBlock.setValue(i);
		return caseBlock;
	}
	
	public boolean isCasedBlock(Block block) {
		return casedBlock.get(block).booleanValue();
	}

	public CompositeBlock createCompositeBlock(Scope scope) {
		CompositeBlock compositeBlock = GecosUserBlockFactory.CompositeBlock(scope);
		if (coreBuilder.getParentBlock() != null && coreBuilder.getParentBlock().getParent() != null) {
			if (coreBuilder.getParentBlock() instanceof CompositeBlock) {
				((CompositeBlock)coreBuilder.getParentBlock()).addChildren(compositeBlock);
			} else if (coreBuilder.getParentBlock() instanceof ForC99Block) {
				((ForC99Block)coreBuilder.getParentBlock()).setBodyBlock(compositeBlock);
			} else {
				System.out.println(coreBuilder.getParentBlock().getClass());
			}
		} else {
			CompositeBlock body = GecosUserBlockFactory.CompositeBlock(compositeBlock.getScope());
			compositeBlock.setScope(coreBuilder.createScope());
			body.addChildren(compositeBlock);
			coreBuilder.setBodyToCurrentProcedure(body);
		}
		setBlockToCaseBlock(compositeBlock);
		return compositeBlock;
	}

	public ForBlock createForBlock() {
		ForBlock forBlock = factory.createForBlock();
		setBlockToCaseBlock(forBlock);
		return forBlock;
	}
	
	public ForC99Block createForC99Block(Scope scope) {
		ForC99Block forC99Block = factory.createForC99Block();
		forC99Block.setScope(scope);
		setBlockToCaseBlock(forC99Block);
		return forC99Block;
	}
	
	public IfBlock createIfBlock() {
		IfBlock ifBlock = factory.createIfBlock();
		setBlockToCaseBlock(ifBlock);
		return ifBlock;
	}

	public DoWhileBlock createLoopBlock() {
		DoWhileBlock loopBlock = factory.createDoWhileBlock();
		setBlockToCaseBlock(loopBlock);
		return loopBlock;
	}

	public SwitchBlock createSwitchBlock() {
		SwitchBlock switchBlock = factory.createSwitchBlock();
		switchBlocks.add(switchBlock);
		setBlockToCaseBlock(switchBlock);
		return switchBlock;
	}

	public void switchBlockCreated() {
		if (!switchBlocks.isEmpty()) {
			if (caseBlock != null) {
				BasicBlock basicBlock = createBasicBlock();
				((CompositeBlock)switchBlocks.get(switchBlocks.size()-1).getBodyBlock()).getChildren().add(basicBlock);
			}
			switchBlocks.remove(switchBlocks.size()-1);
		}
	}

	public WhileBlock createWhileBlock() {
		WhileBlock whileBlock = factory.createWhileBlock();
		setBlockToCaseBlock(whileBlock);
		return whileBlock;
	}
	
	private void setBlockToCaseBlock(Block block) {
		if (caseBlock != null) {
			caseBlock.setBody(block);
			switchBlocks.get(switchBlocks.size()-1).getCases().put(caseBlock.getValue(), caseBlock.getBody());
			caseBlock = null;
			casedBlock.put(block, true);
		} else
			casedBlock.put(block, false);
	}
}