/**
 */
package gecos.core;

import gecos.types.Type;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Procedure Symbol</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.core.ProcedureSymbol#getKind <em>Kind</em>}</li>
 *   <li>{@link gecos.core.ProcedureSymbol#getProcedure <em>Procedure</em>}</li>
 * </ul>
 *
 * @see gecos.core.CorePackage#getProcedureSymbol()
 * @model
 * @generated
 */
public interface ProcedureSymbol extends Symbol, ScopeContainer {
	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see #setKind(int)
	 * @see gecos.core.CorePackage#getProcedureSymbol_Kind()
	 * @model unique="false" dataType="gecos.core.int"
	 * @generated
	 */
	int getKind();

	/**
	 * Sets the value of the '{@link gecos.core.ProcedureSymbol#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see #getKind()
	 * @generated
	 */
	void setKind(int value);

	/**
	 * Returns the value of the '<em><b>Procedure</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link gecos.core.Procedure#getSymbol <em>Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure</em>' containment reference.
	 * @see #setProcedure(Procedure)
	 * @see gecos.core.CorePackage#getProcedureSymbol_Procedure()
	 * @see gecos.core.Procedure#getSymbol
	 * @model opposite="symbol" containment="true"
	 * @generated
	 */
	Procedure getProcedure();

	/**
	 * Sets the value of the '{@link gecos.core.ProcedureSymbol#getProcedure <em>Procedure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Procedure</em>' containment reference.
	 * @see #getProcedure()
	 * @generated
	 */
	void setProcedure(Procedure value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.core.Symbol%&gt;, &lt;%gecos.core.ParameterSymbol%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.core.Symbol%&gt;, &lt;%gecos.core.ParameterSymbol%&gt;&gt;()\n{\n\tpublic &lt;%gecos.core.ParameterSymbol%&gt; apply(final &lt;%gecos.core.Symbol%&gt; it)\n\t{\n\t\treturn ((&lt;%gecos.core.ParameterSymbol%&gt;) it);\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.core.ParameterSymbol%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%gecos.core.Symbol%&gt;, &lt;%gecos.core.ParameterSymbol%&gt;&gt;map(this.getScope().getSymbols(), _function));'"
	 * @generated
	 */
	EList<ParameterSymbol> listParameters();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model newTypeUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if (((newType == null) || (newType instanceof &lt;%gecos.types.FunctionType%&gt;)))\n{\n\tsuper.setType(newType);\n}\nelse\n{\n\t&lt;%java.lang.String%&gt; _name = this.getName();\n\t&lt;%java.lang.String%&gt; _plus = (\"Attempting to set ProcedureSymbol \\\'\" + _name);\n\t&lt;%java.lang.String%&gt; _plus_1 = (_plus + \"\\\' type to a non FunctionType \\\'\");\n\t&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + newType);\n\t&lt;%java.lang.String%&gt; _plus_3 = (_plus_2 + \"\\\'\");\n\tthrow new &lt;%java.lang.IllegalArgumentException%&gt;(_plus_3);\n}'"
	 * @generated
	 */
	void setType(Type newType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.core.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.Type%&gt; _type = this.getType();\nboolean _tripleNotEquals = (_type != null);\nif (_tripleNotEquals)\n{\n\t&lt;%java.lang.String%&gt; _name = this.getName();\n\t&lt;%java.lang.String%&gt; _plus = (_name + \" \");\n\t&lt;%gecos.types.Type%&gt; _type_1 = this.getType();\n\treturn (_plus + _type_1);\n}\n&lt;%java.lang.String%&gt; _name_1 = this.getName();\n&lt;%java.lang.String%&gt; _plus_1 = (\"?:\" + _name_1);\nreturn (_plus_1 + \"(?)\");'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitProcedureSymbol(this);'"
	 * @generated
	 */
	void accept(CoreVisitor visitor);

} // ProcedureSymbol
