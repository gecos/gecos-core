/**
 */
package gecos.instrs;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simd Pack Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.instrs.InstrsPackage#getSimdPackInstruction()
 * @model
 * @generated
 */
public interface SimdPackInstruction extends SimdInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitSimdPackInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

} // SimdPackInstruction
