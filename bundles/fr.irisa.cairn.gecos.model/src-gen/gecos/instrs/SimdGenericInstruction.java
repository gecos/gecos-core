/**
 */
package gecos.instrs;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simd Generic Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.instrs.InstrsPackage#getSimdGenericInstruction()
 * @model
 * @generated
 */
public interface SimdGenericInstruction extends GenericInstruction, SimdInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitSimdGenericInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

} // SimdGenericInstruction
