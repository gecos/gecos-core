/**
 */
package gecos.dag;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Vector Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.dag.DagPackage#getDAGVectorNode()
 * @model abstract="true"
 * @generated
 */
public interface DAGVectorNode extends DAGNode, DagVisitable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 * @generated
	 */
	void accept(DagVisitor visitor);

} // DAGVectorNode
