/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.tools.visitors.simpleforloops;

import gecos.blocks.Block;

public class SimpleLoopException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2852067166815357260L;
	private Block block;


	public SimpleLoopException(Block b, String message) {
		super(message);
		this.block = b;
	}
	
	public Block getBlock() {
		return block;
	}
	
	public void setBlock(Block block) {
		this.block = block;
	}
}
