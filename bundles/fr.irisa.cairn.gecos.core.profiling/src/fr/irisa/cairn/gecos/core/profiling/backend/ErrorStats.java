package fr.irisa.cairn.gecos.core.profiling.backend;

import java.util.List;
import java.util.Objects;

/**
 * Compute statistical characteristics of the Error between original and
 * approximate values. The values are converted to {@link Double} !
 * 
 * <p>
 * The computation is done only once and cached for subsequent getter calls.
 * 
 * <pre>
 * Error = original - approximation
 * 
 * MSE (Mean Squared Error) 
 * 			= Power(Error)  
 * 			= sumSquared(Error) / N
 * 
 * Power(dB)= 10.log10(Power) = MSE (dB)
 * 
 * PSNR (dB) (Peak Signal-to-Noise Ration) 
 * 			= 10.log10(MAX^2(original) / MSE)
 * 			= 20.log10(MAX(original)) - PowerdB(error)
 * 			= - PowerdB(error) if MAX(original) = 1
 * </pre>
 * 
 * @see ValuesStats
 * 
 * @author aelmouss
 */
public class ErrorStats extends ValuesStats {

	protected ValuesStats signalStats;

	public static ErrorStats of(ValuesStats originalSignalStats, double[] errorValues) {
		ErrorStats errorStats = new ErrorStats();
		errorStats.signalStats = originalSignalStats;
		errorStats.compute(errorValues);
		return errorStats;
	}
	
	public static ErrorStats empty() {
		return of(ValuesStats.empty(), new double[] {});
	}

	public static <T extends Number> ErrorStats ofDiff(T[] original, T[] approximation) {
		return of(ValuesStats.of(original), diffValues(original, approximation));
	}
	
	/**
	 * Merge the provided errorStats into a single one. 
	 * The merge is done by reducing the errorStats, starting from
	 * an empty ErrorStats, using {@link ErrorStats#merge(ValuesStats, boolean)}.
	 * 
	 * If errorStats is empty an empty ErrorStats is returned.
	 * 
	 * @param errorStats
	 * @param takeMean if true take the MEAN of nbElements, sum and sumSquared. Otherwise, take their SUM.
	 * @return a new {@link ErrorStats}. Never {@code null}.
	 */
	public static ErrorStats merge(List<ErrorStats> errorStats, boolean takeMean) {
		Objects.requireNonNull(errorStats);
		return errorStats.stream().reduce(ErrorStats.empty(), (r, s) -> r.merge(s, takeMean));
	}
	

	public ValuesStats getOriginalSignalStats() {
		return signalStats;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ErrorStats merge(ValuesStats otherErroStats, boolean takeMean) {
		if (!(otherErroStats instanceof ErrorStats))
			throw new UnsupportedOperationException("Can only merge in ErrorStats object. "
					+ "Operation not supported for " + otherErroStats + " of type " + otherErroStats.getClass());
		ErrorStats other = (ErrorStats) otherErroStats;

		mergeEssentials(other, takeMean);
		signalStats.mergeEssentials(other.getOriginalSignalStats(), takeMean);

		updateComplex();
		signalStats.updateComplex();

		return this;
	}

	//TODO handle corner cases (divide by 0, NaN ...) 
	public double getPSNR() {
		// XXX should it rather use signalStats.getAbsoluteMax() ??
		return signalStats.getMax() * signalStats.getMax() / getPower();
	}

	public double getPSNRdB() {
		// XXX should it rather use signalStats.getAbsoluteMax() ??
		return 20 * Math.log10(signalStats.getMax()) - getPowerdB();
	}

	public double getSNR() {
		return signalStats.getPower() / getPower();
	}

	public double getSNRdB() {
		return signalStats.getPowerdB() - getPowerdB();
	}
}