package $packageName$.modules;

import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;

/**
 * GeCoS module implementation which replace addition/multiplication with multiplication/addition
 * in a GeCoS project and call a custom switch which detect addition/multiplication and use 
 * GeCoS factories to create new instruction and replace old one by new one.
 */
public class AdditionMultiplicationReplacerModule {
	private GecosProject _project;
	
	public AdditionMultiplicationReplacerModule(GecosProject project){
		_project = project;
	}
	
	/**
	 * Method called by the GeCoS framework when the script command defined in the extension is called
	 */
	public void compute(){
		AdditionMultiplicationReplacerSwitches switcher = new AdditionMultiplicationReplacerSwitches();
		for(ProcedureSet ps : _project.listProcedureSets())  // Iterate on each procedure set contained in a GeCoS project 
			for(Procedure p : ps.listProcedures()) // Iterate on each procedure contained in a procedure set
				switcher.doSwitch(p.getBody()); // Apply the switch on the body of a procedure
	}
	
	/**
	 * Implementation of a custom Switch replace addition/multiplication with multiplication/addition
	 * 
	 * This class extends BlockInstructionSwitch which is a Default switch implementation.
	 * BlockInstructionSwitch performs a depth-first traversal of the IR until a instruction is reached
	 */
	private class AdditionMultiplicationReplacerSwitches extends BlockInstructionSwitch<Object> {

		@Override
		public Object caseGenericInstruction(GenericInstruction g) {
			// Look up first the content of the g node
			super.caseGenericInstruction(g);
			
			// Arithmetic, Comparison and logicial operator are generic instruction.
			// The customization is done by the name and the number of operand
			if(g.getName().equals(ArithmeticOperator.ADD.getLiteral())){
				Instruction newInst = GecosUserInstructionFactory.mul(g.getOperand(0), g.getOperand(1)); // Create the new instruction
				// Test if the current generic instruction is contained in a instruction
				// getParent() return null if the instruction is not contained in an another instruction
				// otherwise, it return the parent instruction
				if(g.getParent() == null){
					g.getBasicBlock().replaceInstruction(g, newInst);
				}
				else{
					g.getParent().replaceChild(g, newInst); // Replace the instruction in the parent instuction by the new one
				}
			}
			else if(g.getName().equals(ArithmeticOperator.MUL.getLiteral())){
				Instruction newInst = GecosUserInstructionFactory.add(g.getOperand(0), g.getOperand(1));
				if(g.getParent() == null){
					g.getBasicBlock().replaceInstruction(g, newInst);
				}
				else{
					g.getParent().replaceChild(g, newInst);
				}
			}
	
			return null;
		}		
	}
}