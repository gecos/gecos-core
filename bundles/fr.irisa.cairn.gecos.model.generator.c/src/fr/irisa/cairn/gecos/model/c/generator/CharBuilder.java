package fr.irisa.cairn.gecos.model.c.generator;

public class CharBuilder {
	public static String buildCharLitteralFromIntInstruction(long value) {
		int intValue = (int) value;
		switch (intValue) {
		case '\0':
			return "\\0";
		case '\n':
			return "\\n";
		case '\"':
			return "\\\"";
		case '\t':
			return "\\r";
		case '\r':
			return "\\r";
		case '\'':
			return "\\\'";
		case '\\':
			return "\\";
		case '\b':
			return "\\b";
		case '\f':
			return "\\f";
		default:
			if (value < 0 || value > 255)
				throw new RuntimeException("Inconsistent value for a Char Litteral " + value);
			Character c = new Character((char) value);
			return c + "";
		}
	}
}
