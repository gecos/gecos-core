/**
 */
package gecos.types;

import gecos.instrs.Instruction;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enumerator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.Enumerator#getEnumType <em>Enum Type</em>}</li>
 *   <li>{@link gecos.types.Enumerator#getName <em>Name</em>}</li>
 *   <li>{@link gecos.types.Enumerator#getExpression <em>Expression</em>}</li>
 * </ul>
 *
 * @see gecos.types.TypesPackage#getEnumerator()
 * @model
 * @generated
 */
public interface Enumerator extends TypesVisitable {
	/**
	 * Returns the value of the '<em><b>Enum Type</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link gecos.types.EnumType#getEnumerators <em>Enumerators</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enum Type</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enum Type</em>' container reference.
	 * @see #setEnumType(EnumType)
	 * @see gecos.types.TypesPackage#getEnumerator_EnumType()
	 * @see gecos.types.EnumType#getEnumerators
	 * @model opposite="enumerators" transient="false"
	 * @generated
	 */
	EnumType getEnumType();

	/**
	 * Sets the value of the '{@link gecos.types.Enumerator#getEnumType <em>Enum Type</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enum Type</em>' container reference.
	 * @see #getEnumType()
	 * @generated
	 */
	void setEnumType(EnumType value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see gecos.types.TypesPackage#getEnumerator_Name()
	 * @model unique="false" dataType="gecos.types.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link gecos.types.Enumerator#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression</em>' containment reference.
	 * @see #setExpression(Instruction)
	 * @see gecos.types.TypesPackage#getEnumerator_Expression()
	 * @model containment="true"
	 * @generated
	 */
	Instruction getExpression();

	/**
	 * Sets the value of the '{@link gecos.types.Enumerator#getExpression <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression</em>' containment reference.
	 * @see #getExpression()
	 * @generated
	 */
	void setExpression(Instruction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitEnumerator(this);'"
	 * @generated
	 */
	void accept(TypesVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.boolean" unique="false" otherUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((this == other))\n{\n\treturn true;\n}\nreturn (&lt;%java.util.Objects%&gt;.equals(this.getName(), other.getName()) &amp;&amp; &lt;%java.util.Objects%&gt;.equals(this.getExpression(), other.getExpression()));'"
	 * @generated
	 */
	boolean isEqual(Enumerator other);

} // Enumerator
