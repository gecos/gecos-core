/**
 */
package gecos.types.impl;

import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.TypeSizes;

import gecos.instrs.Instruction;

import gecos.types.ACType;
import gecos.types.ArrayType;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import gecos.types.TypesPackage;
import gecos.types.TypesVisitor;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Array Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.impl.ArrayTypeImpl#getBase <em>Base</em>}</li>
 *   <li>{@link gecos.types.impl.ArrayTypeImpl#getLower <em>Lower</em>}</li>
 *   <li>{@link gecos.types.impl.ArrayTypeImpl#getUpper <em>Upper</em>}</li>
 *   <li>{@link gecos.types.impl.ArrayTypeImpl#getSizeExpr <em>Size Expr</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ArrayTypeImpl extends TypeImpl implements ArrayType {
	/**
	 * The cached value of the '{@link #getBase() <em>Base</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase()
	 * @generated
	 * @ordered
	 */
	protected Type base;

	/**
	 * The default value of the '{@link #getLower() <em>Lower</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLower()
	 * @generated
	 * @ordered
	 */
	protected static final long LOWER_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getLower() <em>Lower</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLower()
	 * @generated
	 * @ordered
	 */
	protected long lower = LOWER_EDEFAULT;

	/**
	 * The default value of the '{@link #getUpper() <em>Upper</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpper()
	 * @generated
	 * @ordered
	 */
	protected static final long UPPER_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getUpper() <em>Upper</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpper()
	 * @generated
	 * @ordered
	 */
	protected long upper = UPPER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSizeExpr() <em>Size Expr</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSizeExpr()
	 * @generated
	 * @ordered
	 */
	protected Instruction sizeExpr;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArrayTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypesPackage.Literals.ARRAY_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type getBase() {
		return base;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBase(Type newBase) {
		Type oldBase = base;
		base = newBase;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.ARRAY_TYPE__BASE, oldBase, base));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getLower() {
		return lower;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLower(long newLower) {
		long oldLower = lower;
		lower = newLower;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.ARRAY_TYPE__LOWER, oldLower, lower));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getUpper() {
		return upper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUpper(long newUpper) {
		long oldUpper = upper;
		upper = newUpper;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.ARRAY_TYPE__UPPER, oldUpper, upper));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getSizeExpr() {
		return sizeExpr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSizeExpr(Instruction newSizeExpr, NotificationChain msgs) {
		Instruction oldSizeExpr = sizeExpr;
		sizeExpr = newSizeExpr;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TypesPackage.ARRAY_TYPE__SIZE_EXPR, oldSizeExpr, newSizeExpr);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSizeExpr(Instruction newSizeExpr) {
		if (newSizeExpr != sizeExpr) {
			NotificationChain msgs = null;
			if (sizeExpr != null)
				msgs = ((InternalEObject)sizeExpr).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TypesPackage.ARRAY_TYPE__SIZE_EXPR, null, msgs);
			if (newSizeExpr != null)
				msgs = ((InternalEObject)newSizeExpr).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TypesPackage.ARRAY_TYPE__SIZE_EXPR, null, msgs);
			msgs = basicSetSizeExpr(newSizeExpr, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.ARRAY_TYPE__SIZE_EXPR, newSizeExpr, newSizeExpr));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final TypesVisitor visitor) {
		visitor.visitArrayType(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNbDims() {
		int depth = 1;
		Type tmp = this.getBase();
		while (((tmp instanceof ArrayType) && (!(tmp instanceof ACType)))) {
			{
				tmp = ((ArrayType) tmp).getBase();
				depth++;
			}
		}
		return depth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type getInnermostBase() {
		Type base = this.getBase();
		while (((base instanceof ArrayType) && (!(base instanceof ACType)))) {
			base = ((ArrayType) base).getBase();
		}
		return base;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInnermostBase(final Type innermostBase) {
		final Type base = this.getInnermostBase();
		EObject _eContainer = base.eContainer();
		boolean _not = (!(_eContainer instanceof ArrayType));
		if (_not) {
			throw new RuntimeException();
		}
		EObject _eContainer_1 = base.eContainer();
		((ArrayType) _eContainer_1).setBase(innermostBase);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		final StringBuffer str = new StringBuffer();
		str.append("[");
		Instruction _sizeExpr = this.getSizeExpr();
		boolean _tripleNotEquals = (_sizeExpr != null);
		if (_tripleNotEquals) {
			str.append(this.getSizeExpr());
		}
		str.append("](");
		Object _elvis = null;
		Type _base = this.getBase();
		if (_base != null) {
			_elvis = _base;
		} else {
			_elvis = "?";
		}
		str.append(_elvis);
		str.append(")");
		return str.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEqual(final Type other, final boolean ignoreStorageClass, final boolean ignoreConst, final boolean ignoreVolatile) {
		if ((other == this)) {
			return true;
		}
		if ((other instanceof ArrayType)) {
			final ArrayType arrayType = ((ArrayType)other);
			boolean sameBaseType = false;
			if (((arrayType.getBase() == null) && (this.getBase() == null))) {
				sameBaseType = true;
			}
			else {
				Type _base = arrayType.getBase();
				boolean _tripleEquals = (_base == null);
				Type _base_1 = this.getBase();
				boolean _tripleEquals_1 = (_base_1 == null);
				boolean _xor = (_tripleEquals ^ _tripleEquals_1);
				if (_xor) {
					return false;
				}
				else {
					sameBaseType = arrayType.getBase().isEqual(this.getBase(), ignoreStorageClass, ignoreConst, ignoreVolatile);
				}
			}
			boolean sameSizeExpr = false;
			if (((arrayType.getSizeExpr() == null) && (this.getSizeExpr() == null))) {
				sameSizeExpr = true;
			}
			else {
				Instruction _sizeExpr = arrayType.getSizeExpr();
				boolean _tripleEquals_2 = (_sizeExpr == null);
				Instruction _sizeExpr_1 = this.getSizeExpr();
				boolean _tripleEquals_3 = (_sizeExpr_1 == null);
				boolean _xor_1 = (_tripleEquals_2 ^ _tripleEquals_3);
				if (_xor_1) {
					return false;
				}
				else {
					sameSizeExpr = arrayType.getSizeExpr().isSame(this.getSizeExpr());
				}
			}
			return ((((sameBaseType && sameSizeExpr) && (this.getLower() == arrayType.getLower())) && (this.getUpper() == arrayType.getUpper())) && super.isEqual(arrayType, ignoreStorageClass, ignoreConst, ignoreVolatile));
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getSize() {
		return TypeSizes.defaultPointerSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instruction> getSizes() {
		final BasicEList<Instruction> sizes = new BasicEList<Instruction>();
		Type t = ((Type) this);
		while (((t instanceof ArrayType) && (!(t instanceof ACType)))) {
			{
				sizes.add(((ArrayType) t).getSizeExpr().copy());
				t = ((ArrayType) t).getBase();
			}
		}
		return sizes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInnermostStorageClass(final StorageClassSpecifiers innermostStorage) {
		Type _innermostBase = this.getInnermostBase();
		_innermostBase.setInnermostStorageClass(innermostStorage);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TypesPackage.ARRAY_TYPE__SIZE_EXPR:
				return basicSetSizeExpr(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TypesPackage.ARRAY_TYPE__BASE:
				return getBase();
			case TypesPackage.ARRAY_TYPE__LOWER:
				return getLower();
			case TypesPackage.ARRAY_TYPE__UPPER:
				return getUpper();
			case TypesPackage.ARRAY_TYPE__SIZE_EXPR:
				return getSizeExpr();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TypesPackage.ARRAY_TYPE__BASE:
				setBase((Type)newValue);
				return;
			case TypesPackage.ARRAY_TYPE__LOWER:
				setLower((Long)newValue);
				return;
			case TypesPackage.ARRAY_TYPE__UPPER:
				setUpper((Long)newValue);
				return;
			case TypesPackage.ARRAY_TYPE__SIZE_EXPR:
				setSizeExpr((Instruction)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TypesPackage.ARRAY_TYPE__BASE:
				setBase((Type)null);
				return;
			case TypesPackage.ARRAY_TYPE__LOWER:
				setLower(LOWER_EDEFAULT);
				return;
			case TypesPackage.ARRAY_TYPE__UPPER:
				setUpper(UPPER_EDEFAULT);
				return;
			case TypesPackage.ARRAY_TYPE__SIZE_EXPR:
				setSizeExpr((Instruction)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TypesPackage.ARRAY_TYPE__BASE:
				return base != null;
			case TypesPackage.ARRAY_TYPE__LOWER:
				return lower != LOWER_EDEFAULT;
			case TypesPackage.ARRAY_TYPE__UPPER:
				return upper != UPPER_EDEFAULT;
			case TypesPackage.ARRAY_TYPE__SIZE_EXPR:
				return sizeExpr != null;
		}
		return super.eIsSet(featureID);
	}

} //ArrayTypeImpl
