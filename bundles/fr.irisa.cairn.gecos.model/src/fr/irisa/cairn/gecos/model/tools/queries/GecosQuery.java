package fr.irisa.cairn.gecos.model.tools.queries;

import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ForBlock;
import gecos.core.CorePackage;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.Symbol;
import gecos.instrs.InstrsPackage;
import gecos.instrs.Instruction;
import gecos.instrs.SSADefSymbol;
import gecos.instrs.SSAUseSymbol;
import gecos.instrs.SetInstruction;
import gecos.types.Type;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

public class GecosQuery {

	static EClass symbolEClass = CorePackage.eINSTANCE.getSymbol();
	static EClass ssaDefEClass = InstrsPackage.eINSTANCE.getSSADefSymbol();
	static EClass ssaUseEClass = InstrsPackage.eINSTANCE.getSSAUseSymbol();
	static EClass procEClass = CorePackage.eINSTANCE.getProcedure();
	static EClass symbolInstrEClass = InstrsPackage.eINSTANCE.getSymbolInstruction();
	
	/* Returns all Procedure Objects contained in the current Object */
	public static EList<Procedure> findAllProceduresIn(EObject ps) {
		EList<Procedure> eAllContentsInstancesOf = EMFUtils.<Procedure>eAllContentsInstancesOf(ps, Procedure.class);
		return eAllContentsInstancesOf ;

	}

	public static EList<ForBlock> findAllForBlocksIn(EObject ps) {
		EList<ForBlock> eAllContentsInstancesOf = EMFUtils.<ForBlock>eAllContentsInstancesOf(ps, ForBlock.class);
		return eAllContentsInstancesOf ;
	}

	public static EList<BasicBlock> findAllBasicBlocksIn(EObject ps) {
		EList<BasicBlock> eAllContentsInstancesOf = EMFUtils.<BasicBlock>eAllContentsInstancesOf(ps, BasicBlock.class);
		return eAllContentsInstancesOf ;
	}
	
	public static EList<Block> findAllBlocksIn(EObject ps) {
		EList<Block> eAllContentsInstancesOf = EMFUtils.<Block>eAllContentsInstancesOf(ps, Block.class);
		return eAllContentsInstancesOf ;
	}
	
	public static EList<Symbol> findAllSymbolsIn(EObject ps) {
		EList<Symbol> eAllContentsInstancesOf = EMFUtils.<Symbol>eAllContentsInstancesOf(ps, Symbol.class);
		return eAllContentsInstancesOf ;
	}

	public static EList<Instruction> findAllInstructionsIn(EObject ps) {
		EList<Instruction> eAllContentsInstancesOf = EMFUtils.<Instruction>eAllContentsInstancesOf(ps, Instruction.class);
		return eAllContentsInstancesOf ;
	}

	public static class Instrs {

		// Any instruction modifying a the memory content is considered has having a side effect.
		public static boolean hasSideEffect(Instruction inst) {
			EList<SetInstruction> eList = EMFUtils.<SetInstruction>eAllContentsInstancesOf(inst, SetInstruction.class);
			return (eList.size()==0) ;
		}

		public static EList<SSADefSymbol> getAllSSADefintionsIn(Block ps) {
			EList<SSADefSymbol> eAllContentsInstancesOf = EMFUtils.<SSADefSymbol>eAllContentsInstancesOf(ps, SSADefSymbol.class);
			return eAllContentsInstancesOf ;
		}

		public static EList<SSAUseSymbol> getAllSSAUsesIn(Block ps) {
			EList<SSAUseSymbol> eAllContentsInstancesOf = EMFUtils.<SSAUseSymbol>eAllContentsInstancesOf(ps, SSAUseSymbol.class);
			return eAllContentsInstancesOf ;
		}
}

	public static class Types {

		public static boolean isGlobal(Type sym) {
			return sym.getContainingScope().getRoot().getTypes().contains(sym);
		}

		public static boolean isLocalFor(Symbol sym, Procedure proc) {
			return GecosQuery.Types.getAllLocalTypeDefinitionsIn(proc).contains(sym);
		}

		public static EList<Type> getAllLocalTypeDefinitionsIn(Procedure ps) {
			EList<Type> eAllContentsInstancesOf = EMFUtils.<Type>eAllContentsInstancesOf(ps, Type.class);
			return eAllContentsInstancesOf ;
		}

		public static EList<Type> getAllLocalTypeDefinitionsIn(Block ps) {
			EList<Type> eAllContentsInstancesOf = EMFUtils.<Type>eAllContentsInstancesOf(ps, Type.class);
			return eAllContentsInstancesOf ;
		}

		public static boolean isLocal(Type type) {
			throw new UnsupportedOperationException("Not yet implemented");
		}

	}

	public static class Symbols {
		
		public static boolean maybeTouchedIn(Symbol sym, Procedure ps) {
//			EObject root = EMFUtils.eRootContainer(ps);
			throw new UnsupportedOperationException("Not yet implemented");
		}

		public static boolean maybeTouchedIn(Symbol sym, Block ps) {
			throw new UnsupportedOperationException("Not yet implemented");
		}

		public static boolean mustbeTouchedIn(Symbol sym, Procedure ps) {
			throw new UnsupportedOperationException("Not yet implemented");
		}

		public static boolean mustbeTouchedIn(Symbol sym, Block ps) {
			throw new UnsupportedOperationException("Not yet implemented");
		}

		public static boolean isAliasedIn(Symbol sym, Procedure ps) {
			throw new UnsupportedOperationException("Not yet implemented");
		}

		public static boolean isAliasedIn(Symbol sym, ProcedureSet ps) {
			throw new UnsupportedOperationException("Not yet implemented");
		}

		public static boolean isGlobal(Symbol sym) {
			return sym.getContainingScope().getRoot().getSymbols().contains(sym);
		}

		public static boolean isLocalFor(Symbol sym, Procedure proc) {
			return GecosQuery.Symbols.getAllLocalSymbolDefinitionsIn(proc).contains(sym);
		}


		public static Procedure getProcedureOf(EObject o) {
			Procedure proc = EMFUtils.<Procedure>eContainerTypeSelect(o, Procedure.class);
			return proc ;
		}
	
		public static EList<Symbol> getAllLocalSymbolDefinitionsIn(Procedure ps) {
			EList<Symbol> eAllContentsInstancesOf = EMFUtils.<Symbol>eAllContentsInstancesOf(ps, Symbol.class);
			return eAllContentsInstancesOf ;
		}
		
		public static EList<Symbol> getAllLocalSymbolUseIn(Procedure ps) {
			EList<Symbol> eAllContentsInstancesOf = EMFUtils.<Symbol>eAllContentsInstancesOf(ps, Symbol.class);
			return eAllContentsInstancesOf ;
		}
		
		public static EList<Symbol> getAllInnerSymbolDefinitionsIn(CompositeBlock cb) {
			EList<Symbol> eAllContentsInstancesOf = EMFUtils.<Symbol>eAllContentsInstancesOf(cb, Symbol.class);
			return eAllContentsInstancesOf ;
		}
	}
}
