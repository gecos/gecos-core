/**
 */
package gecos.instrs;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Goto Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.GotoInstruction#getLabelInstruction <em>Label Instruction</em>}</li>
 *   <li>{@link gecos.instrs.GotoInstruction#getLabelName <em>Label Name</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getGotoInstruction()
 * @model
 * @generated
 */
public interface GotoInstruction extends BranchInstruction {
	/**
	 * Returns the value of the '<em><b>Label Instruction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label Instruction</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label Instruction</em>' reference.
	 * @see #setLabelInstruction(LabelInstruction)
	 * @see gecos.instrs.InstrsPackage#getGotoInstruction_LabelInstruction()
	 * @model
	 * @generated
	 */
	LabelInstruction getLabelInstruction();

	/**
	 * Sets the value of the '{@link gecos.instrs.GotoInstruction#getLabelInstruction <em>Label Instruction</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label Instruction</em>' reference.
	 * @see #getLabelInstruction()
	 * @generated
	 */
	void setLabelInstruction(LabelInstruction value);

	/**
	 * Returns the value of the '<em><b>Label Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label Name</em>' attribute.
	 * @see gecos.instrs.InstrsPackage#getGotoInstruction_LabelName()
	 * @model unique="false" dataType="gecos.instrs.String" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='&lt;%java.lang.String%&gt; _elvis = null;\n&lt;%gecos.instrs.LabelInstruction%&gt; _labelInstruction = this.getLabelInstruction();\n&lt;%java.lang.String%&gt; _name = null;\nif (_labelInstruction!=null)\n{\n\t_name=_labelInstruction.getName();\n}\nif (_name != null)\n{\n\t_elvis = _name;\n} else\n{\n\t_elvis = null;\n}\nreturn _elvis;'"
	 * @generated
	 */
	String getLabelName();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitGotoInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" objUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean sameBranchType = false;\nif ((obj instanceof &lt;%gecos.instrs.GotoInstruction%&gt;))\n{\n\tsameBranchType = true;\n}\nreturn (super.equals(obj) &amp;&amp; sameBranchType);'"
	 * @generated
	 */
	boolean equals(Object obj);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _labelName = this.getLabelName();\nreturn (\"goto \" + _labelName);'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.GotoInstruction%&gt;))\n{\n\tboolean isSameLabel = false;\n\tif (((this.getLabelInstruction() == null) &amp;&amp; (((&lt;%gecos.instrs.GotoInstruction%&gt;)instr).getLabelInstruction() == null)))\n\t{\n\t\tisSameLabel = true;\n\t}\n\telse\n\t{\n\t\t&lt;%gecos.instrs.LabelInstruction%&gt; _labelInstruction = this.getLabelInstruction();\n\t\tboolean _tripleEquals = (_labelInstruction == null);\n\t\t&lt;%gecos.instrs.LabelInstruction%&gt; _labelInstruction_1 = ((&lt;%gecos.instrs.GotoInstruction%&gt;)instr).getLabelInstruction();\n\t\tboolean _tripleEquals_1 = (_labelInstruction_1 == null);\n\t\tboolean _xor = (_tripleEquals ^ _tripleEquals_1);\n\t\tif (_xor)\n\t\t{\n\t\t\treturn false;\n\t\t}\n\t\telse\n\t\t{\n\t\t\tisSameLabel = this.getLabelInstruction().isSame(((&lt;%gecos.instrs.GotoInstruction%&gt;)instr).getLabelInstruction());\n\t\t}\n\t}\n\tboolean isSameLabelName = false;\n\tif (((this.getLabelName() == null) &amp;&amp; (((&lt;%gecos.instrs.GotoInstruction%&gt;)instr).getLabelName() == null)))\n\t{\n\t\tisSameLabelName = true;\n\t}\n\telse\n\t{\n\t\t&lt;%java.lang.String%&gt; _labelName = this.getLabelName();\n\t\tboolean _tripleEquals_2 = (_labelName == null);\n\t\t&lt;%java.lang.String%&gt; _labelName_1 = ((&lt;%gecos.instrs.GotoInstruction%&gt;)instr).getLabelName();\n\t\tboolean _tripleEquals_3 = (_labelName_1 == null);\n\t\tboolean _xor_1 = (_tripleEquals_2 ^ _tripleEquals_3);\n\t\tif (_xor_1)\n\t\t{\n\t\t\treturn false;\n\t\t}\n\t\telse\n\t\t{\n\t\t\tisSameLabelName = this.getLabelName().equals(((&lt;%gecos.instrs.GotoInstruction%&gt;)instr).getLabelName());\n\t\t}\n\t}\n\treturn ((super.isSame(instr) &amp;&amp; isSameLabel) &amp;&amp; isSameLabelName);\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // GotoInstruction
