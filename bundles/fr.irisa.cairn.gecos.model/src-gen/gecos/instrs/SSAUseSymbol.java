/**
 */
package gecos.instrs;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SSA Use Symbol</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.SSAUseSymbol#getDef <em>Def</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getSSAUseSymbol()
 * @model
 * @generated
 */
public interface SSAUseSymbol extends NumberedSymbolInstruction {
	/**
	 * Returns the value of the '<em><b>Def</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link gecos.instrs.SSADefSymbol#getSSAUses <em>SSA Uses</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Def</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Def</em>' reference.
	 * @see #setDef(SSADefSymbol)
	 * @see gecos.instrs.InstrsPackage#getSSAUseSymbol_Def()
	 * @see gecos.instrs.SSADefSymbol#getSSAUses
	 * @model opposite="SSAUses"
	 * @generated
	 */
	SSADefSymbol getDef();

	/**
	 * Sets the value of the '{@link gecos.instrs.SSAUseSymbol#getDef <em>Def</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Def</em>' reference.
	 * @see #getDef()
	 * @generated
	 */
	void setDef(SSADefSymbol value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitSSAUseSymbol(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _elvis = null;\n&lt;%gecos.core.Symbol%&gt; _symbol = this.getSymbol();\n&lt;%java.lang.String%&gt; _name = null;\nif (_symbol!=null)\n{\n\t_name=_symbol.getName();\n}\nif (_name != null)\n{\n\t_elvis = _name;\n} else\n{\n\t_elvis = \"null\";\n}\n&lt;%java.lang.String%&gt; _plus = (\"use(\" + _elvis);\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \"_\");\nint _number = this.getNumber();\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + &lt;%java.lang.Integer%&gt;.valueOf(_number));\n&lt;%java.lang.String%&gt; _plus_3 = (_plus_2 + \":\");\n&lt;%java.lang.Object%&gt; _elvis_1 = null;\n&lt;%gecos.core.Symbol%&gt; _symbol_1 = this.getSymbol();\n&lt;%gecos.types.Type%&gt; _type = null;\nif (_symbol_1!=null)\n{\n\t_type=_symbol_1.getType();\n}\nif (_type != null)\n{\n\t_elvis_1 = _type;\n} else\n{\n\t_elvis_1 = \"\";\n}\n&lt;%java.lang.String%&gt; _plus_4 = (_plus_3 + _elvis_1);\nreturn (_plus_4 + \")\");'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.SSAUseSymbol%&gt;))\n{\n\tboolean isSame = false;\n\tif (((this.getDef() == null) &amp;&amp; (((&lt;%gecos.instrs.SSAUseSymbol%&gt;)instr).getDef() == null)))\n\t{\n\t\tisSame = true;\n\t}\n\telse\n\t{\n\t\t&lt;%gecos.instrs.SSADefSymbol%&gt; _def = this.getDef();\n\t\tboolean _tripleEquals = (_def == null);\n\t\t&lt;%gecos.instrs.SSADefSymbol%&gt; _def_1 = ((&lt;%gecos.instrs.SSAUseSymbol%&gt;)instr).getDef();\n\t\tboolean _tripleEquals_1 = (_def_1 == null);\n\t\tboolean _xor = (_tripleEquals ^ _tripleEquals_1);\n\t\tif (_xor)\n\t\t{\n\t\t\treturn false;\n\t\t}\n\t\telse\n\t\t{\n\t\t\tisSame = this.getDef().isSame(((&lt;%gecos.instrs.SSAUseSymbol%&gt;)instr).getDef());\n\t\t}\n\t}\n\treturn (super.isSame(instr) &amp;&amp; isSame);\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // SSAUseSymbol
