typedef double T;

int main() {
	T res = 1;

	#pragma outline
	{
		res = 4;
	}

	if(res != 4) {
		abort();
	}

	return 0;
}
