/**
 */
package gecos.dag.impl;

import gecos.dag.DAGImmNode;
import gecos.dag.DagPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DAG Imm Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class DAGImmNodeImpl extends DAGNodeImpl implements DAGImmNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DAGImmNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DagPackage.Literals.DAG_IMM_NODE;
	}

} //DAGImmNodeImpl
