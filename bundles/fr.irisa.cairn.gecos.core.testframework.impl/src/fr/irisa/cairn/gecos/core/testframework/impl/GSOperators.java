package fr.irisa.cairn.gecos.core.testframework.impl;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

import fr.irisa.cairn.gecos.core.testframework.utils.GSProjectUtils;
import fr.irisa.cairn.gecos.model.cdtfrontend.CDTFrontEnd;
import fr.irisa.cairn.gecos.testframework.data.ICxxProjectData;
import fr.irisa.cairn.gecos.testframework.model.IVersionFactory;
import gecos.gecosproject.GecosProject;

public class GSOperators {

	private GSOperators() {}
	
	/**
	 * Create a new version using the provided factory, and:<ul>
	 * <li> set its project as a copy of the project of the specified version.
	 * <li> set its previous as the source version.
	 * <li> copy its makefile, external source files, timeout, enableStdRedirection
	 * <li> then, optionally apply the specified {@code afterVersionCreation}
	 * of the created (i.e. the copy) version.
	 * </ul>
	 * 
	 * @param versionFactory which is used to create the new version.
	 * the created project, otherwise do nothing.
	 * @param afterVersionCreation if not {@code null} accept
	 * the created version, otherwise do nothing.
	 * @return a function that when applied on a given version returns a copy of it.
	 */
	public static final <V extends GSProjectVersion> Function<V, V> versionCopier(
			IVersionFactory<V> versionFactory, Consumer<V> afterVersionCreation) {
		return src -> {
			GecosProject copyProject = src.getProject().copy();
			V copy = versionFactory.createVersion();
			copy.setProject(copyProject);
			copy.setPrevious(src);
			copy.setMakefile(src.getMakefile());
			copy.setExternalSources(src.getExternalSourceFiles());
			copy.setTimeout(src.getTimeout());
			copy.setEnableStdRedirection(src.isEnableStdRedirection());
			
			if(afterVersionCreation != null)
				afterVersionCreation.accept(copy);

			return copy;
		};
	}
	
	/**
	 * Create a new version using the provided factory, and:<ul>
	 * <li> set its project as a copy of the project of the specified version.
	 * <li> set its previous as the source version.
	 * </ul>
	 * 
	 * @param versionFactory which is used to create the new version.
	 * @return a function that when applied on a given version returns a copy of it.
	 */
	public static final <V extends GSProjectVersion> Function<V, V> versionCopier(
			IVersionFactory<V> versionFactory) {
		return versionCopier(versionFactory, null);
	}
	
	/**
	 * Create a function that when applied, on a given data, returns a new version.
	 * It performs the following:
	 * <ul>
	 * <li> Create a new {@link GecosProject} and initialize it from the data information
	 * (name, source files and include dirs).
	 * <li> Optionally, apply the specified {@code projectBeforeParsingInitializer} 
	 * on the data and the created project, to modify it if needed.
	 * <li> Parse the project with {@link CDTFrontEnd}.
	 * <li> Create a new version by applying the specified {@code versionFromProject}
	 * on the created project.
	 * </ul>
	 * 
	 * @param versionFactory which is used to create the new version.
	 * @param beforeParsing if not {@code null} accept the data and
	 * the created project, otherwise do nothing.
	 * @param afterVersionCreation if not {@code null} accept the data and
	 * the created version, otherwise do nothing.
	 * @return a function that when applied on a given data returns a new version.   
	 */
	public static final <D extends ICxxProjectData, V extends GSProjectVersion> Function<D, V> customDataConvertor(
			IVersionFactory<V> versionFactory,
			BiConsumer<D,GecosProject> beforeParsing, 
			BiConsumer<D,V> afterVersionCreation) {
		return data -> {
			GecosProject project = GSProjectUtils.createGecosProject(data.getName(), 
					data.getSourceFiles(), data.getIncludeDirs());
			
			if(beforeParsing != null)
				beforeParsing.accept(data, project);
			
			GSProjectUtils.parseGecosProject(project);
			
			V ver = versionFactory.createVersion();
			ver.setProject(project);
			ver.setName("Original");
			ver.setPrevious(null);
			ver.setMakefile(data.getMakefile());
			ver.setTimeout(Long.valueOf(20));
			ver.enableStdRedirection = false;
			
			if(afterVersionCreation != null)
				afterVersionCreation.accept(data, ver);
			return ver;
		};
	}
	
	/**
	 * Shorthand to {@link #customDataConvertor(IVersionFactory, BiConsumer, BiConsumer)}
	 * with no beforeParsing nor afterVersionCreation actions.
	 * 
	 * @param versionFactory which is used to create the new version.
	 * @return a function that when applied on a given data returns a new version.
	 */
	public static final <D extends ICxxProjectData, V extends GSProjectVersion> Function<D, V> defaultDataConvertor(
			IVersionFactory<V> versionFactory) {
		return customDataConvertor(versionFactory, null, null);
	}
	
}
