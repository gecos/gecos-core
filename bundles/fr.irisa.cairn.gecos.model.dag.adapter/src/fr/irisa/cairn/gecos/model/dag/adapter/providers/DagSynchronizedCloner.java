package fr.irisa.cairn.gecos.model.dag.adapter.providers;

import com.google.inject.Inject;

import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGEdgeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGInstructionAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGNodeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGPortAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.observers.IDagSynchronizationObserver;
import gecos.dag.DAGInstruction;

/**
 * A cloner that add an injected {@link IDagSynchronizationObserver} to maintain
 * consistency of adapted {@link DAGInstruction}.
 * 
 * @author Antoine Floc'h - Initial contribution and API
 * 
 */
public class DagSynchronizedCloner extends DagCloner {

	@Inject
	private IDagSynchronizationObserver update;

	public IDAGInstructionAdapter createGraph(boolean directed) {
		IDAGInstructionAdapter adapter = super.createGraph(directed);
		adapter.getObservers().add(update);
		return adapter;
	}

	@Override
	protected IDAGNodeAdapter basicNodeCopy(IDAGNodeAdapter n) {
		IDAGNodeAdapter copy = super.basicNodeCopy(n);
		copy.getObservers().add(update);
		return copy;
	}

	@Override
	protected IDAGEdgeAdapter basicEdgeCopy(IDAGEdgeAdapter e) {
		IDAGEdgeAdapter copy = super.basicEdgeCopy(e);
		copy.getObservers().add(update);
		return copy;
	}

	@Override
	protected IDAGPortAdapter basicPortCopy(IDAGPortAdapter p) {
		IDAGPortAdapter copy = super.basicPortCopy(p);
		copy.getObservers().add(update);
		return copy;
	}

}
