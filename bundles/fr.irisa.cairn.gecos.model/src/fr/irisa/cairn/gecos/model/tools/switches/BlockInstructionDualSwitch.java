package fr.irisa.cairn.gecos.model.tools.switches;

import fr.irisa.cairn.gecos.model.extensions.switchs.AdaptableInstrSwitch;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Procedure;
import gecos.instrs.AddressInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.FieldInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.PhiInstruction;
import gecos.instrs.RangeInstruction;
import gecos.instrs.RetInstruction;
import gecos.instrs.SetInstruction;

/**
 * A block and instruction switch managing two different generic return types.
 * 
 * @author antoine
 * 
 * @param <I> return type for instructions
 * @param <B> return type for blocks
 */
public class BlockInstructionDualSwitch<I, B> extends AdaptableInstrSwitch<I> {

	private BasicBlockVisitor bbvistor;
	private B lastBlockRes;

	public BlockInstructionDualSwitch() {
		bbvistor = new BasicBlockVisitor(this);
	}

	public B doSwitch(Block block) {
		bbvistor.doSwitch(block);
		return lastBlockRes;
	}

	public void doSwitch(Procedure proc) {
		bbvistor.doSwitch(proc);
	}

	public class BasicBlockVisitor extends BasicBlockSwitch<B> {

		BlockInstructionDualSwitch<I, B> single;

		public BasicBlockVisitor(
				BlockInstructionDualSwitch<I, B> dualBlockInstructionsSwitch) {
			this.single = dualBlockInstructionsSwitch;
		}

		@Override
		public B caseBasicBlock(BasicBlock b) {
			single.caseBasicBlock(b);
			return lastBlockRes;
		}

	}

	public B caseBasicBlock(BasicBlock b) {
		return bbvistor.doSwitch(b);
	}
	
	public B caseCompositeBlock(CompositeBlock b) {
		return bbvistor.doSwitch(b);
	}
	
	public B caseIfBlock(IfBlock b) {
		return bbvistor.doSwitch(b);
	}
	
	public B caseLoopBlock(DoWhileBlock b) {
		return bbvistor.doSwitch(b);
	}
	
	public B caseWhileBlock(WhileBlock b) {
		return bbvistor.doSwitch(b);
	}
	
	public B caseForBlock(ForBlock b) {
		return bbvistor.doSwitch(b);
	}

	public B caseSimpleForBlock(ForBlock b) {
		return bbvistor.doSwitch(b);
	}

	@Override
	public I caseGenericInstruction(GenericInstruction g) {
		for (int i = 0; i < g.getOperands().size(); ++i) {
			doSwitch(g.getOperand(i));
		}
		return null;
	}

	@Override
	public I caseCallInstruction(CallInstruction g) {
//		for (int i = 0; i < g.getChildrenCount(); ++i) {
//			doSwitch(g.getComponentAt(i));
//		}
		doSwitch(g.getAddress());
		for(Instruction arg : g.getArgs())
			doSwitch(arg);
		return null;
	}

	@Override
	public I caseSetInstruction(SetInstruction s) {
		doSwitch(s.getSource());
		doSwitch(s.getDest());
		return null;
	}

	@Override
	public I caseRetInstruction(RetInstruction g) {
		if (g.getExpr() != null)
			doSwitch(g.getExpr());
		return null;
	}

	@Override
	public I caseConvertInstruction(ConvertInstruction g) {
		doSwitch(g.getExpr());
		return null;
	}

	@Override
	public I caseRangeInstruction(RangeInstruction g) {
		doSwitch(g.getExpr());
		return null;
	}

	@Override
	public I caseIndirInstruction(IndirInstruction g) {
		doSwitch(g.getAddress());
		return null;
	}

	@Override
	public I caseAddressInstruction(AddressInstruction g) {
		doSwitch(g.getAddress());
		return null;
	}

	@Override
	public I caseFieldInstruction(FieldInstruction g) {
		doSwitch(g.getExpr());
		return null;
	}


	@Override
	public I casePhiInstruction(PhiInstruction object) {
		for (Instruction inst : object.getChildren()) {
			doSwitch(inst);
		}
		return null;
	}

}
