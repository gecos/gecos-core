

typedef struct s1_t s1;
typedef struct s2_t s2;
struct s2_t {
	s1* a;
	s2* b;
};
struct s1_t {
	s1* a;
	s2* b;
};






typedef int aaa, bbb, ccc;
aaa     int1;
bbb     int2;
ccc     int3;

typedef struct _matrix{
	int x;
	int y;
} *Matrix;

// GCC supports declaring typedefs before the struct declaration
typedef struct _Coordonnees Coordonnees;

struct _Coordonnees
{
    int x;
    int y;
    Coordonnees * toto;
};
union Example
{
    short a;
    long b;
    float c;
};

struct recursive_struct {
	int c;
	struct recursive_struct* next;
};

struct Personne
{
    char nom[100];
    char prenom[100];
    char adresse[1000];
};

enum Volume
{
	FAIBLE = 10, MOYEN = 50, FORT = 100, UNKNOWN
};

void initCoordonnees(Coordonnees *point, int x, int y) {
	point->x = x;
	point->y = y;
}

int main() {
	Coordonnees point = {0,0};
	struct Personne personne;
	enum Volume volume = FAIBLE;
	union Example example;
	initCoordonnees(&point, 1, 1);
	personne.nom[0] = 'T';
	personne.nom[1] = 'o';
	personne.nom[2] = 't';
	personne.nom[3] = 'o';
	example.c = 1.0;
	volume = MOYEN;
	volume = MOYEN;
	return 0;
}
