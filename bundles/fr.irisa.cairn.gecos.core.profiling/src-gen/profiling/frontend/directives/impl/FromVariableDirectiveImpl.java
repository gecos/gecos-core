/**
 */
package profiling.frontend.directives.impl;

import fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport;

import fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport.NativeFunction;

import fr.irisa.cairn.gecos.core.profiling.frontend.InvalidDirective;

import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;

import gecos.core.ProcedureSet;

import gecos.instrs.Instruction;

import java.util.Objects;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import profiling.frontend.directives.DirectiveName;
import profiling.frontend.directives.DirectivesPackage;
import profiling.frontend.directives.FromVariableDirective;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>From Variable Directive</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link profiling.frontend.directives.impl.FromVariableDirectiveImpl#getNAME <em>NAME</em>}</li>
 *   <li>{@link profiling.frontend.directives.impl.FromVariableDirectiveImpl#getInst <em>Inst</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FromVariableDirectiveImpl extends InjectorSourceDirectiveImpl implements FromVariableDirective {
	/**
	 * The default value of the '{@link #getNAME() <em>NAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNAME()
	 * @generated
	 * @ordered
	 */
	protected static final DirectiveName NAME_EDEFAULT = DirectiveName.FROM_VAR;

	/**
	 * The cached value of the '{@link #getNAME() <em>NAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNAME()
	 * @generated
	 * @ordered
	 */
	protected DirectiveName name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInst() <em>Inst</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInst()
	 * @generated
	 * @ordered
	 */
	protected Instruction inst;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FromVariableDirectiveImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DirectivesPackage.Literals.FROM_VARIABLE_DIRECTIVE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DirectiveName getNAME() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNAME(DirectiveName newNAME) {
		DirectiveName oldNAME = name;
		name = newNAME == null ? NAME_EDEFAULT : newNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DirectivesPackage.FROM_VARIABLE_DIRECTIVE__NAME, oldNAME, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getInst() {
		if (inst != null && inst.eIsProxy()) {
			InternalEObject oldInst = (InternalEObject)inst;
			inst = (Instruction)eResolveProxy(oldInst);
			if (inst != oldInst) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DirectivesPackage.FROM_VARIABLE_DIRECTIVE__INST, oldInst, inst));
			}
		}
		return inst;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction basicGetInst() {
		return inst;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInst(Instruction newInst) {
		Instruction oldInst = inst;
		inst = newInst;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DirectivesPackage.FROM_VARIABLE_DIRECTIVE__INST, oldInst, inst));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void parse() {
		final EList<Instruction> args = this.getCall().getArgs();
		int _size = args.size();
		boolean _lessThan = (_size < 1);
		if (_lessThan) {
			String _printLocation = this.printLocation();
			String _plus = (_printLocation + "\nMust have at least 1 arguments (instruction)!");
			throw new InvalidDirective(_plus);
		}
		this.setInst(Objects.<Instruction>requireNonNull(args.get(0)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block createCodeBlock() {
		BasicBlock _xblockexpression = null;
		{
			final ProcedureSet ps = this.getCall().getContainingProcedureSet();
			_xblockexpression = GecosUserBlockFactory.BBlock(
				GecosUserInstructionFactory.call(
					NativeProfilingEngineSupport.getNativeProc(ps, NativeFunction.INJECTOR_FROM_VAR), this.getInst()));
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DirectivesPackage.FROM_VARIABLE_DIRECTIVE__NAME:
				return getNAME();
			case DirectivesPackage.FROM_VARIABLE_DIRECTIVE__INST:
				if (resolve) return getInst();
				return basicGetInst();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DirectivesPackage.FROM_VARIABLE_DIRECTIVE__NAME:
				setNAME((DirectiveName)newValue);
				return;
			case DirectivesPackage.FROM_VARIABLE_DIRECTIVE__INST:
				setInst((Instruction)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DirectivesPackage.FROM_VARIABLE_DIRECTIVE__NAME:
				setNAME(NAME_EDEFAULT);
				return;
			case DirectivesPackage.FROM_VARIABLE_DIRECTIVE__INST:
				setInst((Instruction)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DirectivesPackage.FROM_VARIABLE_DIRECTIVE__NAME:
				return name != NAME_EDEFAULT;
			case DirectivesPackage.FROM_VARIABLE_DIRECTIVE__INST:
				return inst != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (NAME: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //FromVariableDirectiveImpl
