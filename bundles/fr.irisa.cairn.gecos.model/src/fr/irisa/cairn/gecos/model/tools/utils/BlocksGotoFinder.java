package fr.irisa.cairn.gecos.model.tools.utils;

import gecos.blocks.BasicBlock;
import gecos.instrs.Instruction;

import java.util.ArrayList;
import java.util.List;

/**
 * This visitor returns all the BB containing gotos inside the block.
 * 
 * @author amorvan
 * @generated NOT
 */
public class BlocksGotoFinder extends BlocksDefaultSwitch<Object> {
	
	private boolean containsGotos = false;
	public boolean containsGotos() { return containsGotos; }
	
	private List<BasicBlock> gotos = new ArrayList<BasicBlock>();
	public List<BasicBlock> getGotos() { return gotos; }

	@Override
	public Object caseBasicBlock(BasicBlock object) {
		BranchInstructionFinder visitor = new BranchInstructionFinder();
		for (Instruction i : object.getInstructions()) {
			visitor.doSwitch(i);
		}
		if (visitor.containsGotos()) {
			gotos.add(object);
			containsGotos = true;
		}
		return null;
	}
	
}
