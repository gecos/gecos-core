/**
 */
package gecos.instrs.impl;

import gecos.instrs.GotoInstruction;
import gecos.instrs.InstrsPackage;
import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;
import gecos.instrs.LabelInstruction;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Goto Instruction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.impl.GotoInstructionImpl#getLabelInstruction <em>Label Instruction</em>}</li>
 *   <li>{@link gecos.instrs.impl.GotoInstructionImpl#getLabelName <em>Label Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GotoInstructionImpl extends BranchInstructionImpl implements GotoInstruction {
	/**
	 * The cached value of the '{@link #getLabelInstruction() <em>Label Instruction</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabelInstruction()
	 * @generated
	 * @ordered
	 */
	protected LabelInstruction labelInstruction;

	/**
	 * The default value of the '{@link #getLabelName() <em>Label Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabelName()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_NAME_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GotoInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstrsPackage.eINSTANCE.getGotoInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LabelInstruction getLabelInstruction() {
		if (labelInstruction != null && labelInstruction.eIsProxy()) {
			InternalEObject oldLabelInstruction = (InternalEObject)labelInstruction;
			labelInstruction = (LabelInstruction)eResolveProxy(oldLabelInstruction);
			if (labelInstruction != oldLabelInstruction) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InstrsPackage.GOTO_INSTRUCTION__LABEL_INSTRUCTION, oldLabelInstruction, labelInstruction));
			}
		}
		return labelInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LabelInstruction basicGetLabelInstruction() {
		return labelInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabelInstruction(LabelInstruction newLabelInstruction) {
		LabelInstruction oldLabelInstruction = labelInstruction;
		labelInstruction = newLabelInstruction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InstrsPackage.GOTO_INSTRUCTION__LABEL_INSTRUCTION, oldLabelInstruction, labelInstruction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLabelName() {
		String _elvis = null;
		LabelInstruction _labelInstruction = this.getLabelInstruction();
		String _name = null;
		if (_labelInstruction!=null) {
			_name=_labelInstruction.getName();
		}
		if (_name != null) {
			_elvis = _name;
		} else {
			_elvis = null;
		}
		return _elvis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final InstrsVisitor visitor) {
		visitor.visitGotoInstruction(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean equals(final Object obj) {
		boolean sameBranchType = false;
		if ((obj instanceof GotoInstruction)) {
			sameBranchType = true;
		}
		return (super.equals(obj) && sameBranchType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _labelName = this.getLabelName();
		return ("goto " + _labelName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSame(final Instruction instr) {
		if ((instr instanceof GotoInstruction)) {
			boolean isSameLabel = false;
			if (((this.getLabelInstruction() == null) && (((GotoInstruction)instr).getLabelInstruction() == null))) {
				isSameLabel = true;
			}
			else {
				LabelInstruction _labelInstruction = this.getLabelInstruction();
				boolean _tripleEquals = (_labelInstruction == null);
				LabelInstruction _labelInstruction_1 = ((GotoInstruction)instr).getLabelInstruction();
				boolean _tripleEquals_1 = (_labelInstruction_1 == null);
				boolean _xor = (_tripleEquals ^ _tripleEquals_1);
				if (_xor) {
					return false;
				}
				else {
					isSameLabel = this.getLabelInstruction().isSame(((GotoInstruction)instr).getLabelInstruction());
				}
			}
			boolean isSameLabelName = false;
			if (((this.getLabelName() == null) && (((GotoInstruction)instr).getLabelName() == null))) {
				isSameLabelName = true;
			}
			else {
				String _labelName = this.getLabelName();
				boolean _tripleEquals_2 = (_labelName == null);
				String _labelName_1 = ((GotoInstruction)instr).getLabelName();
				boolean _tripleEquals_3 = (_labelName_1 == null);
				boolean _xor_1 = (_tripleEquals_2 ^ _tripleEquals_3);
				if (_xor_1) {
					return false;
				}
				else {
					isSameLabelName = this.getLabelName().equals(((GotoInstruction)instr).getLabelName());
				}
			}
			return ((super.isSame(instr) && isSameLabel) && isSameLabelName);
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InstrsPackage.GOTO_INSTRUCTION__LABEL_INSTRUCTION:
				if (resolve) return getLabelInstruction();
				return basicGetLabelInstruction();
			case InstrsPackage.GOTO_INSTRUCTION__LABEL_NAME:
				return getLabelName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InstrsPackage.GOTO_INSTRUCTION__LABEL_INSTRUCTION:
				setLabelInstruction((LabelInstruction)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InstrsPackage.GOTO_INSTRUCTION__LABEL_INSTRUCTION:
				setLabelInstruction((LabelInstruction)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InstrsPackage.GOTO_INSTRUCTION__LABEL_INSTRUCTION:
				return labelInstruction != null;
			case InstrsPackage.GOTO_INSTRUCTION__LABEL_NAME:
				return LABEL_NAME_EDEFAULT == null ? getLabelName() != null : !LABEL_NAME_EDEFAULT.equals(getLabelName());
		}
		return super.eIsSet(featureID);
	}

} //GotoInstructionImpl
