package fr.irisa.cairn.gecos.core.ui.examples;

import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

import fr.irisa.cairn.gecos.core.ui.Activator;
import fr.irisa.cairn.gecos.core.ui.examples.utils.BasicGecosExampleWizard;

public class GenerateIRFromC extends BasicGecosExampleWizard{	
	@Override
	public String getResourcesFolderToCopyIntoProject() {
		return "resources/wizard_templates/generatingir/";
	}

	@Override
	public String getPageName() {
		return "Generating GeCoS IR from C source code example";
	}

	@Override
	public String getPageDescription() {
		return "This wizard will create a example project for generating GeCoS IR from a c source code";
	}

	@Override
	protected Bundle getPluginBundle() {
		return Platform.getBundle(Activator.PLUGIN_ID);
	}
}
