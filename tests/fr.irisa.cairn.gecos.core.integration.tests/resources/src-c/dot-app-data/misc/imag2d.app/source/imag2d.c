#include "defs.h"
#include "data_size.h"


#pragma MAIN_FUNC
void imag2d(TYPE_0 in[MC][NC], TYPE_1 out[1]){

	TYPE_0 coef[MC][NC] = {	{1,	2,	1},
							{2,	4, 	2},
							{1,	2, 	1} };
	TYPE_0 sumCoef = 16;
	TYPE_1 acc[MC][NC] = {0};
	int i, j;

//	for(i = 0; i < MC; i++) {
//		for(j = 0; j < NC; j++) {
//			acc[i][j] = in[i][j] * coef[i][j];
//		}
//	}
#pragma __RLT_INSET__
	{
		acc[0][0] = (in[0][0] * coef[0][0]) / sumCoef;
		acc[0][1] = (in[0][1] * coef[0][1]) / sumCoef;
		acc[0][2] = (in[0][2] * coef[0][2]) / sumCoef;

		acc[1][0] = (in[1][0] * coef[1][0]) / sumCoef;
		acc[1][1] = (in[1][1] * coef[1][1]) / sumCoef;
		acc[1][2] = (in[1][2] * coef[1][2]) / sumCoef;

		acc[2][0] = (in[2][0] * coef[2][0]) / sumCoef;
		acc[2][1] = (in[2][1] * coef[2][1]) / sumCoef;
		acc[2][2] = (in[2][2] * coef[2][2]) / sumCoef;
	}

	out[0] = 0;
	for(i = 0; i < MC; i++) {
		for(j = 0; j < NC; j++) {
			out[0] += acc[i][j];
		}
	}

}
