package fr.irisa.cairn.gecos.model.dag.adapter.providers;

import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGEdgeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGNodeAdapter;
import fr.irisa.cairn.graph.providers.ILatencyProvider;

public class DagLatencyProvider implements
		ILatencyProvider<IDAGNodeAdapter, IDAGEdgeAdapter> {

	public int getNodeLatency(IDAGNodeAdapter n) {
		return 1;
	}

	public int getEdgeLatency(IDAGEdgeAdapter e) {
		return 0;
	}

}
