/**
 */
package gecos.types;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visitor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.types.TypesPackage#getTypesVisitor()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface TypesVisitor extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model fUnique="false"
	 * @generated
	 */
	void visitField(Field f);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model aUnique="false"
	 * @generated
	 */
	void visitArrayType(ArrayType a);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model fUnique="false"
	 * @generated
	 */
	void visitFunctionType(FunctionType f);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model pUnique="false"
	 * @generated
	 */
	void visitPtrType(PtrType p);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model rUnique="false"
	 * @generated
	 */
	void visitRecordType(RecordType r);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model aUnique="false"
	 * @generated
	 */
	void visitACFixedType(ACFixedType a);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model aUnique="false"
	 * @generated
	 */
	void visitACFloatType(ACFloatType a);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model aUnique="false"
	 * @generated
	 */
	void visitACIntType(ACIntType a);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model aUnique="false"
	 * @generated
	 */
	void visitACType(ACType a);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model aUnique="false"
	 * @generated
	 */
	void visitAliasType(AliasType a);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model enumTypeUnique="false"
	 * @generated
	 */
	void visitEnumType(EnumType enumType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model enumeratorUnique="false"
	 * @generated
	 */
	void visitEnumerator(Enumerator enumerator);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model stUnique="false"
	 * @generated
	 */
	void visitSimdType(SimdType st);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bUnique="false"
	 * @generated
	 */
	void visitVoidType(VoidType b);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bUnique="false"
	 * @generated
	 */
	void visitIntegerType(IntegerType b);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bUnique="false"
	 * @generated
	 */
	void visitBoolType(BoolType b);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bUnique="false"
	 * @generated
	 */
	void visitFloatType(FloatType b);

} // TypesVisitor
