/**
 */
package gecos.dag;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Numbered Symbol Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.DAGNumberedSymbolNode#getNumber <em>Number</em>}</li>
 *   <li>{@link gecos.dag.DAGNumberedSymbolNode#getSymbolDefinition <em>Symbol Definition</em>}</li>
 * </ul>
 *
 * @see gecos.dag.DagPackage#getDAGNumberedSymbolNode()
 * @model
 * @generated
 */
public interface DAGNumberedSymbolNode extends DAGSymbolNode, DagVisitable {
	/**
	 * Returns the value of the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number</em>' attribute.
	 * @see #setNumber(int)
	 * @see gecos.dag.DagPackage#getDAGNumberedSymbolNode_Number()
	 * @model unique="false" dataType="gecos.dag.int"
	 * @generated
	 */
	int getNumber();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGNumberedSymbolNode#getNumber <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number</em>' attribute.
	 * @see #getNumber()
	 * @generated
	 */
	void setNumber(int value);

	/**
	 * Returns the value of the '<em><b>Symbol Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Symbol Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbol Definition</em>' reference.
	 * @see #setSymbolDefinition(DAGNumberedOutNode)
	 * @see gecos.dag.DagPackage#getDAGNumberedSymbolNode_SymbolDefinition()
	 * @model
	 * @generated
	 */
	DAGNumberedOutNode getSymbolDefinition();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGNumberedSymbolNode#getSymbolDefinition <em>Symbol Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Symbol Definition</em>' reference.
	 * @see #getSymbolDefinition()
	 * @generated
	 */
	void setSymbolDefinition(DAGNumberedOutNode value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGNumberedSymbolNode(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.dag.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _elvis = null;\n&lt;%gecos.core.Symbol%&gt; _symbol = this.getSymbol();\n&lt;%java.lang.String%&gt; _name = null;\nif (_symbol!=null)\n{\n\t_name=_symbol.getName();\n}\nif (_name != null)\n{\n\t_elvis = _name;\n} else\n{\n\t_elvis = \"?\";\n}\n&lt;%java.lang.String%&gt; _plus = (_elvis + \"_\");\nint _number = this.getNumber();\nreturn (_plus + &lt;%java.lang.Integer%&gt;.valueOf(_number));'"
	 * @generated
	 */
	String toString();

} // DAGNumberedSymbolNode
