/**
 */
package gecos.blocks;

import gecos.annotations.AnnotationsPackage;

import gecos.core.CorePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see gecos.blocks.BlocksFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel modelName='Gecos' modelPluginClass='' creationCommands='false' creationIcons='false' interfaceNamePattern='' importerID='org.eclipse.emf.importer.ecore' operationReflection='false' basePackage='gecos'"
 * @generated
 */
public interface BlocksPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "blocks";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.gecos.org/blocks";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "blocks";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BlocksPackage eINSTANCE = gecos.blocks.impl.BlocksPackageImpl.init();

	/**
	 * The meta object id for the '{@link gecos.blocks.BlocksVisitor <em>Visitor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.blocks.BlocksVisitor
	 * @see gecos.blocks.impl.BlocksPackageImpl#getBlocksVisitor()
	 * @generated
	 */
	int BLOCKS_VISITOR = 0;

	/**
	 * The number of structural features of the '<em>Visitor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCKS_VISITOR_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link gecos.blocks.BlocksVisitable <em>Visitable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.blocks.BlocksVisitable
	 * @see gecos.blocks.impl.BlocksPackageImpl#getBlocksVisitable()
	 * @generated
	 */
	int BLOCKS_VISITABLE = 1;

	/**
	 * The number of structural features of the '<em>Visitable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCKS_VISITABLE_FEATURE_COUNT = CorePackage.GECOS_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.blocks.impl.BlockImpl <em>Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.blocks.impl.BlockImpl
	 * @see gecos.blocks.impl.BlocksPackageImpl#getBlock()
	 * @generated
	 */
	int BLOCK = 2;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__ANNOTATIONS = AnnotationsPackage.ANNOTATED_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__PARENT = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__NUMBER = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_FEATURE_COUNT = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.blocks.Breakable <em>Breakable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.blocks.Breakable
	 * @see gecos.blocks.impl.BlocksPackageImpl#getBreakable()
	 * @generated
	 */
	int BREAKABLE = 3;

	/**
	 * The number of structural features of the '<em>Breakable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAKABLE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link gecos.blocks.Continuable <em>Continuable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.blocks.Continuable
	 * @see gecos.blocks.impl.BlocksPackageImpl#getContinuable()
	 * @generated
	 */
	int CONTINUABLE = 4;

	/**
	 * The number of structural features of the '<em>Continuable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTINUABLE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link gecos.blocks.Loop <em>Loop</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.blocks.Loop
	 * @see gecos.blocks.impl.BlocksPackageImpl#getLoop()
	 * @generated
	 */
	int LOOP = 5;

	/**
	 * The number of structural features of the '<em>Loop</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_FEATURE_COUNT = BREAKABLE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.blocks.impl.BasicBlockImpl <em>Basic Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.blocks.impl.BasicBlockImpl
	 * @see gecos.blocks.impl.BlocksPackageImpl#getBasicBlock()
	 * @generated
	 */
	int BASIC_BLOCK = 6;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_BLOCK__ANNOTATIONS = BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_BLOCK__PARENT = BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_BLOCK__NUMBER = BLOCK__NUMBER;

	/**
	 * The feature id for the '<em><b>Instructions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_BLOCK__INSTRUCTIONS = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Def Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_BLOCK__DEF_EDGES = BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_BLOCK__OUT_EDGES = BLOCK_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Use Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_BLOCK__USE_EDGES = BLOCK_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_BLOCK__IN_EDGES = BLOCK_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_BLOCK__LABEL = BLOCK_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_BLOCK__FLAGS = BLOCK_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Basic Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 7;

	/**
	 * The meta object id for the '{@link gecos.blocks.impl.CompositeBlockImpl <em>Composite Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.blocks.impl.CompositeBlockImpl
	 * @see gecos.blocks.impl.BlocksPackageImpl#getCompositeBlock()
	 * @generated
	 */
	int COMPOSITE_BLOCK = 7;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_BLOCK__ANNOTATIONS = BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_BLOCK__PARENT = BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_BLOCK__NUMBER = BLOCK__NUMBER;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_BLOCK__SCOPE = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_BLOCK__CHILDREN = BLOCK_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Composite Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.blocks.FlowEdge <em>Flow Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.blocks.FlowEdge
	 * @see gecos.blocks.impl.BlocksPackageImpl#getFlowEdge()
	 * @generated
	 */
	int FLOW_EDGE = 10;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_EDGE__ANNOTATIONS = BLOCKS_VISITABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Flow Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_EDGE_FEATURE_COUNT = BLOCKS_VISITABLE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.blocks.impl.ControlEdgeImpl <em>Control Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.blocks.impl.ControlEdgeImpl
	 * @see gecos.blocks.impl.BlocksPackageImpl#getControlEdge()
	 * @generated
	 */
	int CONTROL_EDGE = 8;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_EDGE__ANNOTATIONS = FLOW_EDGE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Cond</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_EDGE__COND = FLOW_EDGE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_EDGE__FLAGS = FLOW_EDGE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>From</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_EDGE__FROM = FLOW_EDGE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_EDGE__TO = FLOW_EDGE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Control Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_EDGE_FEATURE_COUNT = FLOW_EDGE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link gecos.blocks.impl.DataEdgeImpl <em>Data Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.blocks.impl.DataEdgeImpl
	 * @see gecos.blocks.impl.BlocksPackageImpl#getDataEdge()
	 * @generated
	 */
	int DATA_EDGE = 9;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_EDGE__ANNOTATIONS = FLOW_EDGE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Uses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_EDGE__USES = FLOW_EDGE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_EDGE__TO = FLOW_EDGE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>From</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_EDGE__FROM = FLOW_EDGE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Data Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_EDGE_FEATURE_COUNT = FLOW_EDGE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link gecos.blocks.impl.IfBlockImpl <em>If Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.blocks.impl.IfBlockImpl
	 * @see gecos.blocks.impl.BlocksPackageImpl#getIfBlock()
	 * @generated
	 */
	int IF_BLOCK = 11;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_BLOCK__ANNOTATIONS = BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_BLOCK__PARENT = BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_BLOCK__NUMBER = BLOCK__NUMBER;

	/**
	 * The feature id for the '<em><b>Test Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_BLOCK__TEST_BLOCK = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Then Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_BLOCK__THEN_BLOCK = BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Else Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_BLOCK__ELSE_BLOCK = BLOCK_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Jump To</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_BLOCK__JUMP_TO = BLOCK_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>If Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link gecos.blocks.impl.WhileBlockImpl <em>While Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.blocks.impl.WhileBlockImpl
	 * @see gecos.blocks.impl.BlocksPackageImpl#getWhileBlock()
	 * @generated
	 */
	int WHILE_BLOCK = 12;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_BLOCK__ANNOTATIONS = BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_BLOCK__PARENT = BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_BLOCK__NUMBER = BLOCK__NUMBER;

	/**
	 * The feature id for the '<em><b>Test Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_BLOCK__TEST_BLOCK = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Body Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_BLOCK__BODY_BLOCK = BLOCK_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>While Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.blocks.impl.DoWhileBlockImpl <em>Do While Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.blocks.impl.DoWhileBlockImpl
	 * @see gecos.blocks.impl.BlocksPackageImpl#getDoWhileBlock()
	 * @generated
	 */
	int DO_WHILE_BLOCK = 13;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DO_WHILE_BLOCK__ANNOTATIONS = BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DO_WHILE_BLOCK__PARENT = BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DO_WHILE_BLOCK__NUMBER = BLOCK__NUMBER;

	/**
	 * The feature id for the '<em><b>Test Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DO_WHILE_BLOCK__TEST_BLOCK = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Body Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DO_WHILE_BLOCK__BODY_BLOCK = BLOCK_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Do While Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DO_WHILE_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.blocks.impl.ForBlockImpl <em>For Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.blocks.impl.ForBlockImpl
	 * @see gecos.blocks.impl.BlocksPackageImpl#getForBlock()
	 * @generated
	 */
	int FOR_BLOCK = 14;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_BLOCK__ANNOTATIONS = BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_BLOCK__PARENT = BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_BLOCK__NUMBER = BLOCK__NUMBER;

	/**
	 * The feature id for the '<em><b>Init Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_BLOCK__INIT_BLOCK = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Test Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_BLOCK__TEST_BLOCK = BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Step Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_BLOCK__STEP_BLOCK = BLOCK_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Body Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_BLOCK__BODY_BLOCK = BLOCK_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>For Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link gecos.blocks.impl.ForC99BlockImpl <em>For C99 Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.blocks.impl.ForC99BlockImpl
	 * @see gecos.blocks.impl.BlocksPackageImpl#getForC99Block()
	 * @generated
	 */
	int FOR_C99_BLOCK = 15;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_C99_BLOCK__ANNOTATIONS = FOR_BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_C99_BLOCK__PARENT = FOR_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_C99_BLOCK__NUMBER = FOR_BLOCK__NUMBER;

	/**
	 * The feature id for the '<em><b>Init Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_C99_BLOCK__INIT_BLOCK = FOR_BLOCK__INIT_BLOCK;

	/**
	 * The feature id for the '<em><b>Test Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_C99_BLOCK__TEST_BLOCK = FOR_BLOCK__TEST_BLOCK;

	/**
	 * The feature id for the '<em><b>Step Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_C99_BLOCK__STEP_BLOCK = FOR_BLOCK__STEP_BLOCK;

	/**
	 * The feature id for the '<em><b>Body Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_C99_BLOCK__BODY_BLOCK = FOR_BLOCK__BODY_BLOCK;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_C99_BLOCK__SCOPE = FOR_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>For C99 Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_C99_BLOCK_FEATURE_COUNT = FOR_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.blocks.impl.SimpleForBlockImpl <em>Simple For Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.blocks.impl.SimpleForBlockImpl
	 * @see gecos.blocks.impl.BlocksPackageImpl#getSimpleForBlock()
	 * @generated
	 */
	int SIMPLE_FOR_BLOCK = 16;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_FOR_BLOCK__ANNOTATIONS = FOR_C99_BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_FOR_BLOCK__PARENT = FOR_C99_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_FOR_BLOCK__NUMBER = FOR_C99_BLOCK__NUMBER;

	/**
	 * The feature id for the '<em><b>Init Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_FOR_BLOCK__INIT_BLOCK = FOR_C99_BLOCK__INIT_BLOCK;

	/**
	 * The feature id for the '<em><b>Test Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_FOR_BLOCK__TEST_BLOCK = FOR_C99_BLOCK__TEST_BLOCK;

	/**
	 * The feature id for the '<em><b>Step Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_FOR_BLOCK__STEP_BLOCK = FOR_C99_BLOCK__STEP_BLOCK;

	/**
	 * The feature id for the '<em><b>Body Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_FOR_BLOCK__BODY_BLOCK = FOR_C99_BLOCK__BODY_BLOCK;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_FOR_BLOCK__SCOPE = FOR_C99_BLOCK__SCOPE;

	/**
	 * The feature id for the '<em><b>Iterator</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_FOR_BLOCK__ITERATOR = FOR_C99_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Lb</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_FOR_BLOCK__LB = FOR_C99_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Ub</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_FOR_BLOCK__UB = FOR_C99_BLOCK_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Stride</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_FOR_BLOCK__STRIDE = FOR_C99_BLOCK_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Simple For Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_FOR_BLOCK_FEATURE_COUNT = FOR_C99_BLOCK_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link gecos.blocks.impl.SwitchBlockImpl <em>Switch Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.blocks.impl.SwitchBlockImpl
	 * @see gecos.blocks.impl.BlocksPackageImpl#getSwitchBlock()
	 * @generated
	 */
	int SWITCH_BLOCK = 17;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_BLOCK__ANNOTATIONS = BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_BLOCK__PARENT = BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_BLOCK__NUMBER = BLOCK__NUMBER;

	/**
	 * The feature id for the '<em><b>Body Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_BLOCK__BODY_BLOCK = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cases</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_BLOCK__CASES = BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Default Block</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_BLOCK__DEFAULT_BLOCK = BLOCK_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Dispatch Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_BLOCK__DISPATCH_BLOCK = BLOCK_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Switch Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link gecos.blocks.impl.CaseBlockImpl <em>Case Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.blocks.impl.CaseBlockImpl
	 * @see gecos.blocks.impl.BlocksPackageImpl#getCaseBlock()
	 * @generated
	 */
	int CASE_BLOCK = 18;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_BLOCK__ANNOTATIONS = BLOCK__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_BLOCK__PARENT = BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_BLOCK__NUMBER = BLOCK__NUMBER;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_BLOCK__VALUE = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_BLOCK__BODY = BLOCK_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Case Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.blocks.impl.CaseMapImpl <em>Case Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.blocks.impl.CaseMapImpl
	 * @see gecos.blocks.impl.BlocksPackageImpl#getCaseMap()
	 * @generated
	 */
	int CASE_MAP = 19;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Case Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_MAP_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '<em>String</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see gecos.blocks.impl.BlocksPackageImpl#getString()
	 * @generated
	 */
	int STRING = 20;

	/**
	 * The meta object id for the '<em>EE Long</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.Long
	 * @see gecos.blocks.impl.BlocksPackageImpl#getEELong()
	 * @generated
	 */
	int EE_LONG = 21;

	/**
	 * The meta object id for the '<em>int</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.blocks.impl.BlocksPackageImpl#getint()
	 * @generated
	 */
	int INT = 22;

	/**
	 * The meta object id for the '<em>long</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.blocks.impl.BlocksPackageImpl#getlong()
	 * @generated
	 */
	int LONG = 23;


	/**
	 * Returns the meta object for class '{@link gecos.blocks.BlocksVisitor <em>Visitor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visitor</em>'.
	 * @see gecos.blocks.BlocksVisitor
	 * @generated
	 */
	EClass getBlocksVisitor();

	/**
	 * Returns the meta object for class '{@link gecos.blocks.BlocksVisitable <em>Visitable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visitable</em>'.
	 * @see gecos.blocks.BlocksVisitable
	 * @generated
	 */
	EClass getBlocksVisitable();

	/**
	 * Returns the meta object for class '{@link gecos.blocks.Block <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Block</em>'.
	 * @see gecos.blocks.Block
	 * @generated
	 */
	EClass getBlock();

	/**
	 * Returns the meta object for the reference '{@link gecos.blocks.Block#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parent</em>'.
	 * @see gecos.blocks.Block#getParent()
	 * @see #getBlock()
	 * @generated
	 */
	EReference getBlock_Parent();

	/**
	 * Returns the meta object for the attribute '{@link gecos.blocks.Block#getNumber <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number</em>'.
	 * @see gecos.blocks.Block#getNumber()
	 * @see #getBlock()
	 * @generated
	 */
	EAttribute getBlock_Number();

	/**
	 * Returns the meta object for class '{@link gecos.blocks.Breakable <em>Breakable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Breakable</em>'.
	 * @see gecos.blocks.Breakable
	 * @generated
	 */
	EClass getBreakable();

	/**
	 * Returns the meta object for class '{@link gecos.blocks.Continuable <em>Continuable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Continuable</em>'.
	 * @see gecos.blocks.Continuable
	 * @generated
	 */
	EClass getContinuable();

	/**
	 * Returns the meta object for class '{@link gecos.blocks.Loop <em>Loop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Loop</em>'.
	 * @see gecos.blocks.Loop
	 * @generated
	 */
	EClass getLoop();

	/**
	 * Returns the meta object for class '{@link gecos.blocks.BasicBlock <em>Basic Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Basic Block</em>'.
	 * @see gecos.blocks.BasicBlock
	 * @generated
	 */
	EClass getBasicBlock();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.blocks.BasicBlock#getInstructions <em>Instructions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Instructions</em>'.
	 * @see gecos.blocks.BasicBlock#getInstructions()
	 * @see #getBasicBlock()
	 * @generated
	 */
	EReference getBasicBlock_Instructions();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.blocks.BasicBlock#getDefEdges <em>Def Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Def Edges</em>'.
	 * @see gecos.blocks.BasicBlock#getDefEdges()
	 * @see #getBasicBlock()
	 * @generated
	 */
	EReference getBasicBlock_DefEdges();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.blocks.BasicBlock#getOutEdges <em>Out Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Out Edges</em>'.
	 * @see gecos.blocks.BasicBlock#getOutEdges()
	 * @see #getBasicBlock()
	 * @generated
	 */
	EReference getBasicBlock_OutEdges();

	/**
	 * Returns the meta object for the reference list '{@link gecos.blocks.BasicBlock#getUseEdges <em>Use Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Use Edges</em>'.
	 * @see gecos.blocks.BasicBlock#getUseEdges()
	 * @see #getBasicBlock()
	 * @generated
	 */
	EReference getBasicBlock_UseEdges();

	/**
	 * Returns the meta object for the reference list '{@link gecos.blocks.BasicBlock#getInEdges <em>In Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>In Edges</em>'.
	 * @see gecos.blocks.BasicBlock#getInEdges()
	 * @see #getBasicBlock()
	 * @generated
	 */
	EReference getBasicBlock_InEdges();

	/**
	 * Returns the meta object for the attribute '{@link gecos.blocks.BasicBlock#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see gecos.blocks.BasicBlock#getLabel()
	 * @see #getBasicBlock()
	 * @generated
	 */
	EAttribute getBasicBlock_Label();

	/**
	 * Returns the meta object for the attribute '{@link gecos.blocks.BasicBlock#getFlags <em>Flags</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Flags</em>'.
	 * @see gecos.blocks.BasicBlock#getFlags()
	 * @see #getBasicBlock()
	 * @generated
	 */
	EAttribute getBasicBlock_Flags();

	/**
	 * Returns the meta object for class '{@link gecos.blocks.CompositeBlock <em>Composite Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composite Block</em>'.
	 * @see gecos.blocks.CompositeBlock
	 * @generated
	 */
	EClass getCompositeBlock();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.blocks.CompositeBlock#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Children</em>'.
	 * @see gecos.blocks.CompositeBlock#getChildren()
	 * @see #getCompositeBlock()
	 * @generated
	 */
	EReference getCompositeBlock_Children();

	/**
	 * Returns the meta object for class '{@link gecos.blocks.ControlEdge <em>Control Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Control Edge</em>'.
	 * @see gecos.blocks.ControlEdge
	 * @generated
	 */
	EClass getControlEdge();

	/**
	 * Returns the meta object for the attribute '{@link gecos.blocks.ControlEdge#getCond <em>Cond</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cond</em>'.
	 * @see gecos.blocks.ControlEdge#getCond()
	 * @see #getControlEdge()
	 * @generated
	 */
	EAttribute getControlEdge_Cond();

	/**
	 * Returns the meta object for the attribute '{@link gecos.blocks.ControlEdge#getFlags <em>Flags</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Flags</em>'.
	 * @see gecos.blocks.ControlEdge#getFlags()
	 * @see #getControlEdge()
	 * @generated
	 */
	EAttribute getControlEdge_Flags();

	/**
	 * Returns the meta object for the container reference '{@link gecos.blocks.ControlEdge#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>From</em>'.
	 * @see gecos.blocks.ControlEdge#getFrom()
	 * @see #getControlEdge()
	 * @generated
	 */
	EReference getControlEdge_From();

	/**
	 * Returns the meta object for the reference '{@link gecos.blocks.ControlEdge#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>To</em>'.
	 * @see gecos.blocks.ControlEdge#getTo()
	 * @see #getControlEdge()
	 * @generated
	 */
	EReference getControlEdge_To();

	/**
	 * Returns the meta object for class '{@link gecos.blocks.DataEdge <em>Data Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Edge</em>'.
	 * @see gecos.blocks.DataEdge
	 * @generated
	 */
	EClass getDataEdge();

	/**
	 * Returns the meta object for the reference list '{@link gecos.blocks.DataEdge#getUses <em>Uses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Uses</em>'.
	 * @see gecos.blocks.DataEdge#getUses()
	 * @see #getDataEdge()
	 * @generated
	 */
	EReference getDataEdge_Uses();

	/**
	 * Returns the meta object for the reference '{@link gecos.blocks.DataEdge#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>To</em>'.
	 * @see gecos.blocks.DataEdge#getTo()
	 * @see #getDataEdge()
	 * @generated
	 */
	EReference getDataEdge_To();

	/**
	 * Returns the meta object for the container reference '{@link gecos.blocks.DataEdge#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>From</em>'.
	 * @see gecos.blocks.DataEdge#getFrom()
	 * @see #getDataEdge()
	 * @generated
	 */
	EReference getDataEdge_From();

	/**
	 * Returns the meta object for class '{@link gecos.blocks.FlowEdge <em>Flow Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Flow Edge</em>'.
	 * @see gecos.blocks.FlowEdge
	 * @generated
	 */
	EClass getFlowEdge();

	/**
	 * Returns the meta object for class '{@link gecos.blocks.IfBlock <em>If Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>If Block</em>'.
	 * @see gecos.blocks.IfBlock
	 * @generated
	 */
	EClass getIfBlock();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.blocks.IfBlock#getTestBlock <em>Test Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Test Block</em>'.
	 * @see gecos.blocks.IfBlock#getTestBlock()
	 * @see #getIfBlock()
	 * @generated
	 */
	EReference getIfBlock_TestBlock();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.blocks.IfBlock#getThenBlock <em>Then Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Then Block</em>'.
	 * @see gecos.blocks.IfBlock#getThenBlock()
	 * @see #getIfBlock()
	 * @generated
	 */
	EReference getIfBlock_ThenBlock();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.blocks.IfBlock#getElseBlock <em>Else Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Else Block</em>'.
	 * @see gecos.blocks.IfBlock#getElseBlock()
	 * @see #getIfBlock()
	 * @generated
	 */
	EReference getIfBlock_ElseBlock();

	/**
	 * Returns the meta object for the attribute '{@link gecos.blocks.IfBlock#getJumpTo <em>Jump To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Jump To</em>'.
	 * @see gecos.blocks.IfBlock#getJumpTo()
	 * @see #getIfBlock()
	 * @generated
	 */
	EAttribute getIfBlock_JumpTo();

	/**
	 * Returns the meta object for class '{@link gecos.blocks.WhileBlock <em>While Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>While Block</em>'.
	 * @see gecos.blocks.WhileBlock
	 * @generated
	 */
	EClass getWhileBlock();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.blocks.WhileBlock#getTestBlock <em>Test Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Test Block</em>'.
	 * @see gecos.blocks.WhileBlock#getTestBlock()
	 * @see #getWhileBlock()
	 * @generated
	 */
	EReference getWhileBlock_TestBlock();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.blocks.WhileBlock#getBodyBlock <em>Body Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body Block</em>'.
	 * @see gecos.blocks.WhileBlock#getBodyBlock()
	 * @see #getWhileBlock()
	 * @generated
	 */
	EReference getWhileBlock_BodyBlock();

	/**
	 * Returns the meta object for class '{@link gecos.blocks.DoWhileBlock <em>Do While Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Do While Block</em>'.
	 * @see gecos.blocks.DoWhileBlock
	 * @generated
	 */
	EClass getDoWhileBlock();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.blocks.DoWhileBlock#getTestBlock <em>Test Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Test Block</em>'.
	 * @see gecos.blocks.DoWhileBlock#getTestBlock()
	 * @see #getDoWhileBlock()
	 * @generated
	 */
	EReference getDoWhileBlock_TestBlock();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.blocks.DoWhileBlock#getBodyBlock <em>Body Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body Block</em>'.
	 * @see gecos.blocks.DoWhileBlock#getBodyBlock()
	 * @see #getDoWhileBlock()
	 * @generated
	 */
	EReference getDoWhileBlock_BodyBlock();

	/**
	 * Returns the meta object for class '{@link gecos.blocks.ForBlock <em>For Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>For Block</em>'.
	 * @see gecos.blocks.ForBlock
	 * @generated
	 */
	EClass getForBlock();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.blocks.ForBlock#getInitBlock <em>Init Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Init Block</em>'.
	 * @see gecos.blocks.ForBlock#getInitBlock()
	 * @see #getForBlock()
	 * @generated
	 */
	EReference getForBlock_InitBlock();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.blocks.ForBlock#getTestBlock <em>Test Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Test Block</em>'.
	 * @see gecos.blocks.ForBlock#getTestBlock()
	 * @see #getForBlock()
	 * @generated
	 */
	EReference getForBlock_TestBlock();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.blocks.ForBlock#getStepBlock <em>Step Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Step Block</em>'.
	 * @see gecos.blocks.ForBlock#getStepBlock()
	 * @see #getForBlock()
	 * @generated
	 */
	EReference getForBlock_StepBlock();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.blocks.ForBlock#getBodyBlock <em>Body Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body Block</em>'.
	 * @see gecos.blocks.ForBlock#getBodyBlock()
	 * @see #getForBlock()
	 * @generated
	 */
	EReference getForBlock_BodyBlock();

	/**
	 * Returns the meta object for class '{@link gecos.blocks.ForC99Block <em>For C99 Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>For C99 Block</em>'.
	 * @see gecos.blocks.ForC99Block
	 * @generated
	 */
	EClass getForC99Block();

	/**
	 * Returns the meta object for class '{@link gecos.blocks.SimpleForBlock <em>Simple For Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple For Block</em>'.
	 * @see gecos.blocks.SimpleForBlock
	 * @generated
	 */
	EClass getSimpleForBlock();

	/**
	 * Returns the meta object for the reference '{@link gecos.blocks.SimpleForBlock#getIterator <em>Iterator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Iterator</em>'.
	 * @see gecos.blocks.SimpleForBlock#getIterator()
	 * @see #getSimpleForBlock()
	 * @generated
	 */
	EReference getSimpleForBlock_Iterator();

	/**
	 * Returns the meta object for the reference '{@link gecos.blocks.SimpleForBlock#getLb <em>Lb</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Lb</em>'.
	 * @see gecos.blocks.SimpleForBlock#getLb()
	 * @see #getSimpleForBlock()
	 * @generated
	 */
	EReference getSimpleForBlock_Lb();

	/**
	 * Returns the meta object for the reference '{@link gecos.blocks.SimpleForBlock#getUb <em>Ub</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ub</em>'.
	 * @see gecos.blocks.SimpleForBlock#getUb()
	 * @see #getSimpleForBlock()
	 * @generated
	 */
	EReference getSimpleForBlock_Ub();

	/**
	 * Returns the meta object for the reference '{@link gecos.blocks.SimpleForBlock#getStride <em>Stride</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Stride</em>'.
	 * @see gecos.blocks.SimpleForBlock#getStride()
	 * @see #getSimpleForBlock()
	 * @generated
	 */
	EReference getSimpleForBlock_Stride();

	/**
	 * Returns the meta object for class '{@link gecos.blocks.SwitchBlock <em>Switch Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Switch Block</em>'.
	 * @see gecos.blocks.SwitchBlock
	 * @generated
	 */
	EClass getSwitchBlock();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.blocks.SwitchBlock#getBodyBlock <em>Body Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body Block</em>'.
	 * @see gecos.blocks.SwitchBlock#getBodyBlock()
	 * @see #getSwitchBlock()
	 * @generated
	 */
	EReference getSwitchBlock_BodyBlock();

	/**
	 * Returns the meta object for the map '{@link gecos.blocks.SwitchBlock#getCases <em>Cases</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Cases</em>'.
	 * @see gecos.blocks.SwitchBlock#getCases()
	 * @see #getSwitchBlock()
	 * @generated
	 */
	EReference getSwitchBlock_Cases();

	/**
	 * Returns the meta object for the reference '{@link gecos.blocks.SwitchBlock#getDefaultBlock <em>Default Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Default Block</em>'.
	 * @see gecos.blocks.SwitchBlock#getDefaultBlock()
	 * @see #getSwitchBlock()
	 * @generated
	 */
	EReference getSwitchBlock_DefaultBlock();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.blocks.SwitchBlock#getDispatchBlock <em>Dispatch Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Dispatch Block</em>'.
	 * @see gecos.blocks.SwitchBlock#getDispatchBlock()
	 * @see #getSwitchBlock()
	 * @generated
	 */
	EReference getSwitchBlock_DispatchBlock();

	/**
	 * Returns the meta object for class '{@link gecos.blocks.CaseBlock <em>Case Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Case Block</em>'.
	 * @see gecos.blocks.CaseBlock
	 * @generated
	 */
	EClass getCaseBlock();

	/**
	 * Returns the meta object for the attribute '{@link gecos.blocks.CaseBlock#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see gecos.blocks.CaseBlock#getValue()
	 * @see #getCaseBlock()
	 * @generated
	 */
	EAttribute getCaseBlock_Value();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.blocks.CaseBlock#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body</em>'.
	 * @see gecos.blocks.CaseBlock#getBody()
	 * @see #getCaseBlock()
	 * @generated
	 */
	EReference getCaseBlock_Body();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Case Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Case Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyUnique="false" keyDataType="gecos.blocks.EELong"
	 *        valueType="gecos.blocks.Block" valueResolveProxies="false"
	 * @generated
	 */
	EClass getCaseMap();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getCaseMap()
	 * @generated
	 */
	EAttribute getCaseMap_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getCaseMap()
	 * @generated
	 */
	EReference getCaseMap_Value();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>String</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getString();

	/**
	 * Returns the meta object for data type '{@link java.lang.Long <em>EE Long</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>EE Long</em>'.
	 * @see java.lang.Long
	 * @model instanceClass="java.lang.Long"
	 * @generated
	 */
	EDataType getEELong();

	/**
	 * Returns the meta object for data type '<em>int</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>int</em>'.
	 * @model instanceClass="int"
	 * @generated
	 */
	EDataType getint();

	/**
	 * Returns the meta object for data type '<em>long</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>long</em>'.
	 * @model instanceClass="long"
	 * @generated
	 */
	EDataType getlong();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BlocksFactory getBlocksFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link gecos.blocks.BlocksVisitor <em>Visitor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.blocks.BlocksVisitor
		 * @see gecos.blocks.impl.BlocksPackageImpl#getBlocksVisitor()
		 * @generated
		 */
		EClass BLOCKS_VISITOR = eINSTANCE.getBlocksVisitor();

		/**
		 * The meta object literal for the '{@link gecos.blocks.BlocksVisitable <em>Visitable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.blocks.BlocksVisitable
		 * @see gecos.blocks.impl.BlocksPackageImpl#getBlocksVisitable()
		 * @generated
		 */
		EClass BLOCKS_VISITABLE = eINSTANCE.getBlocksVisitable();

		/**
		 * The meta object literal for the '{@link gecos.blocks.impl.BlockImpl <em>Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.blocks.impl.BlockImpl
		 * @see gecos.blocks.impl.BlocksPackageImpl#getBlock()
		 * @generated
		 */
		EClass BLOCK = eINSTANCE.getBlock();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK__PARENT = eINSTANCE.getBlock_Parent();

		/**
		 * The meta object literal for the '<em><b>Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLOCK__NUMBER = eINSTANCE.getBlock_Number();

		/**
		 * The meta object literal for the '{@link gecos.blocks.Breakable <em>Breakable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.blocks.Breakable
		 * @see gecos.blocks.impl.BlocksPackageImpl#getBreakable()
		 * @generated
		 */
		EClass BREAKABLE = eINSTANCE.getBreakable();

		/**
		 * The meta object literal for the '{@link gecos.blocks.Continuable <em>Continuable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.blocks.Continuable
		 * @see gecos.blocks.impl.BlocksPackageImpl#getContinuable()
		 * @generated
		 */
		EClass CONTINUABLE = eINSTANCE.getContinuable();

		/**
		 * The meta object literal for the '{@link gecos.blocks.Loop <em>Loop</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.blocks.Loop
		 * @see gecos.blocks.impl.BlocksPackageImpl#getLoop()
		 * @generated
		 */
		EClass LOOP = eINSTANCE.getLoop();

		/**
		 * The meta object literal for the '{@link gecos.blocks.impl.BasicBlockImpl <em>Basic Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.blocks.impl.BasicBlockImpl
		 * @see gecos.blocks.impl.BlocksPackageImpl#getBasicBlock()
		 * @generated
		 */
		EClass BASIC_BLOCK = eINSTANCE.getBasicBlock();

		/**
		 * The meta object literal for the '<em><b>Instructions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASIC_BLOCK__INSTRUCTIONS = eINSTANCE.getBasicBlock_Instructions();

		/**
		 * The meta object literal for the '<em><b>Def Edges</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASIC_BLOCK__DEF_EDGES = eINSTANCE.getBasicBlock_DefEdges();

		/**
		 * The meta object literal for the '<em><b>Out Edges</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASIC_BLOCK__OUT_EDGES = eINSTANCE.getBasicBlock_OutEdges();

		/**
		 * The meta object literal for the '<em><b>Use Edges</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASIC_BLOCK__USE_EDGES = eINSTANCE.getBasicBlock_UseEdges();

		/**
		 * The meta object literal for the '<em><b>In Edges</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASIC_BLOCK__IN_EDGES = eINSTANCE.getBasicBlock_InEdges();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASIC_BLOCK__LABEL = eINSTANCE.getBasicBlock_Label();

		/**
		 * The meta object literal for the '<em><b>Flags</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASIC_BLOCK__FLAGS = eINSTANCE.getBasicBlock_Flags();

		/**
		 * The meta object literal for the '{@link gecos.blocks.impl.CompositeBlockImpl <em>Composite Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.blocks.impl.CompositeBlockImpl
		 * @see gecos.blocks.impl.BlocksPackageImpl#getCompositeBlock()
		 * @generated
		 */
		EClass COMPOSITE_BLOCK = eINSTANCE.getCompositeBlock();

		/**
		 * The meta object literal for the '<em><b>Children</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_BLOCK__CHILDREN = eINSTANCE.getCompositeBlock_Children();

		/**
		 * The meta object literal for the '{@link gecos.blocks.impl.ControlEdgeImpl <em>Control Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.blocks.impl.ControlEdgeImpl
		 * @see gecos.blocks.impl.BlocksPackageImpl#getControlEdge()
		 * @generated
		 */
		EClass CONTROL_EDGE = eINSTANCE.getControlEdge();

		/**
		 * The meta object literal for the '<em><b>Cond</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTROL_EDGE__COND = eINSTANCE.getControlEdge_Cond();

		/**
		 * The meta object literal for the '<em><b>Flags</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTROL_EDGE__FLAGS = eINSTANCE.getControlEdge_Flags();

		/**
		 * The meta object literal for the '<em><b>From</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_EDGE__FROM = eINSTANCE.getControlEdge_From();

		/**
		 * The meta object literal for the '<em><b>To</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_EDGE__TO = eINSTANCE.getControlEdge_To();

		/**
		 * The meta object literal for the '{@link gecos.blocks.impl.DataEdgeImpl <em>Data Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.blocks.impl.DataEdgeImpl
		 * @see gecos.blocks.impl.BlocksPackageImpl#getDataEdge()
		 * @generated
		 */
		EClass DATA_EDGE = eINSTANCE.getDataEdge();

		/**
		 * The meta object literal for the '<em><b>Uses</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_EDGE__USES = eINSTANCE.getDataEdge_Uses();

		/**
		 * The meta object literal for the '<em><b>To</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_EDGE__TO = eINSTANCE.getDataEdge_To();

		/**
		 * The meta object literal for the '<em><b>From</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_EDGE__FROM = eINSTANCE.getDataEdge_From();

		/**
		 * The meta object literal for the '{@link gecos.blocks.FlowEdge <em>Flow Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.blocks.FlowEdge
		 * @see gecos.blocks.impl.BlocksPackageImpl#getFlowEdge()
		 * @generated
		 */
		EClass FLOW_EDGE = eINSTANCE.getFlowEdge();

		/**
		 * The meta object literal for the '{@link gecos.blocks.impl.IfBlockImpl <em>If Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.blocks.impl.IfBlockImpl
		 * @see gecos.blocks.impl.BlocksPackageImpl#getIfBlock()
		 * @generated
		 */
		EClass IF_BLOCK = eINSTANCE.getIfBlock();

		/**
		 * The meta object literal for the '<em><b>Test Block</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF_BLOCK__TEST_BLOCK = eINSTANCE.getIfBlock_TestBlock();

		/**
		 * The meta object literal for the '<em><b>Then Block</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF_BLOCK__THEN_BLOCK = eINSTANCE.getIfBlock_ThenBlock();

		/**
		 * The meta object literal for the '<em><b>Else Block</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF_BLOCK__ELSE_BLOCK = eINSTANCE.getIfBlock_ElseBlock();

		/**
		 * The meta object literal for the '<em><b>Jump To</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IF_BLOCK__JUMP_TO = eINSTANCE.getIfBlock_JumpTo();

		/**
		 * The meta object literal for the '{@link gecos.blocks.impl.WhileBlockImpl <em>While Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.blocks.impl.WhileBlockImpl
		 * @see gecos.blocks.impl.BlocksPackageImpl#getWhileBlock()
		 * @generated
		 */
		EClass WHILE_BLOCK = eINSTANCE.getWhileBlock();

		/**
		 * The meta object literal for the '<em><b>Test Block</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHILE_BLOCK__TEST_BLOCK = eINSTANCE.getWhileBlock_TestBlock();

		/**
		 * The meta object literal for the '<em><b>Body Block</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHILE_BLOCK__BODY_BLOCK = eINSTANCE.getWhileBlock_BodyBlock();

		/**
		 * The meta object literal for the '{@link gecos.blocks.impl.DoWhileBlockImpl <em>Do While Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.blocks.impl.DoWhileBlockImpl
		 * @see gecos.blocks.impl.BlocksPackageImpl#getDoWhileBlock()
		 * @generated
		 */
		EClass DO_WHILE_BLOCK = eINSTANCE.getDoWhileBlock();

		/**
		 * The meta object literal for the '<em><b>Test Block</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DO_WHILE_BLOCK__TEST_BLOCK = eINSTANCE.getDoWhileBlock_TestBlock();

		/**
		 * The meta object literal for the '<em><b>Body Block</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DO_WHILE_BLOCK__BODY_BLOCK = eINSTANCE.getDoWhileBlock_BodyBlock();

		/**
		 * The meta object literal for the '{@link gecos.blocks.impl.ForBlockImpl <em>For Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.blocks.impl.ForBlockImpl
		 * @see gecos.blocks.impl.BlocksPackageImpl#getForBlock()
		 * @generated
		 */
		EClass FOR_BLOCK = eINSTANCE.getForBlock();

		/**
		 * The meta object literal for the '<em><b>Init Block</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FOR_BLOCK__INIT_BLOCK = eINSTANCE.getForBlock_InitBlock();

		/**
		 * The meta object literal for the '<em><b>Test Block</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FOR_BLOCK__TEST_BLOCK = eINSTANCE.getForBlock_TestBlock();

		/**
		 * The meta object literal for the '<em><b>Step Block</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FOR_BLOCK__STEP_BLOCK = eINSTANCE.getForBlock_StepBlock();

		/**
		 * The meta object literal for the '<em><b>Body Block</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FOR_BLOCK__BODY_BLOCK = eINSTANCE.getForBlock_BodyBlock();

		/**
		 * The meta object literal for the '{@link gecos.blocks.impl.ForC99BlockImpl <em>For C99 Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.blocks.impl.ForC99BlockImpl
		 * @see gecos.blocks.impl.BlocksPackageImpl#getForC99Block()
		 * @generated
		 */
		EClass FOR_C99_BLOCK = eINSTANCE.getForC99Block();

		/**
		 * The meta object literal for the '{@link gecos.blocks.impl.SimpleForBlockImpl <em>Simple For Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.blocks.impl.SimpleForBlockImpl
		 * @see gecos.blocks.impl.BlocksPackageImpl#getSimpleForBlock()
		 * @generated
		 */
		EClass SIMPLE_FOR_BLOCK = eINSTANCE.getSimpleForBlock();

		/**
		 * The meta object literal for the '<em><b>Iterator</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMPLE_FOR_BLOCK__ITERATOR = eINSTANCE.getSimpleForBlock_Iterator();

		/**
		 * The meta object literal for the '<em><b>Lb</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMPLE_FOR_BLOCK__LB = eINSTANCE.getSimpleForBlock_Lb();

		/**
		 * The meta object literal for the '<em><b>Ub</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMPLE_FOR_BLOCK__UB = eINSTANCE.getSimpleForBlock_Ub();

		/**
		 * The meta object literal for the '<em><b>Stride</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMPLE_FOR_BLOCK__STRIDE = eINSTANCE.getSimpleForBlock_Stride();

		/**
		 * The meta object literal for the '{@link gecos.blocks.impl.SwitchBlockImpl <em>Switch Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.blocks.impl.SwitchBlockImpl
		 * @see gecos.blocks.impl.BlocksPackageImpl#getSwitchBlock()
		 * @generated
		 */
		EClass SWITCH_BLOCK = eINSTANCE.getSwitchBlock();

		/**
		 * The meta object literal for the '<em><b>Body Block</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SWITCH_BLOCK__BODY_BLOCK = eINSTANCE.getSwitchBlock_BodyBlock();

		/**
		 * The meta object literal for the '<em><b>Cases</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SWITCH_BLOCK__CASES = eINSTANCE.getSwitchBlock_Cases();

		/**
		 * The meta object literal for the '<em><b>Default Block</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SWITCH_BLOCK__DEFAULT_BLOCK = eINSTANCE.getSwitchBlock_DefaultBlock();

		/**
		 * The meta object literal for the '<em><b>Dispatch Block</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SWITCH_BLOCK__DISPATCH_BLOCK = eINSTANCE.getSwitchBlock_DispatchBlock();

		/**
		 * The meta object literal for the '{@link gecos.blocks.impl.CaseBlockImpl <em>Case Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.blocks.impl.CaseBlockImpl
		 * @see gecos.blocks.impl.BlocksPackageImpl#getCaseBlock()
		 * @generated
		 */
		EClass CASE_BLOCK = eINSTANCE.getCaseBlock();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CASE_BLOCK__VALUE = eINSTANCE.getCaseBlock_Value();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CASE_BLOCK__BODY = eINSTANCE.getCaseBlock_Body();

		/**
		 * The meta object literal for the '{@link gecos.blocks.impl.CaseMapImpl <em>Case Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.blocks.impl.CaseMapImpl
		 * @see gecos.blocks.impl.BlocksPackageImpl#getCaseMap()
		 * @generated
		 */
		EClass CASE_MAP = eINSTANCE.getCaseMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CASE_MAP__KEY = eINSTANCE.getCaseMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CASE_MAP__VALUE = eINSTANCE.getCaseMap_Value();

		/**
		 * The meta object literal for the '<em>String</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see gecos.blocks.impl.BlocksPackageImpl#getString()
		 * @generated
		 */
		EDataType STRING = eINSTANCE.getString();

		/**
		 * The meta object literal for the '<em>EE Long</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.Long
		 * @see gecos.blocks.impl.BlocksPackageImpl#getEELong()
		 * @generated
		 */
		EDataType EE_LONG = eINSTANCE.getEELong();

		/**
		 * The meta object literal for the '<em>int</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.blocks.impl.BlocksPackageImpl#getint()
		 * @generated
		 */
		EDataType INT = eINSTANCE.getint();

		/**
		 * The meta object literal for the '<em>long</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.blocks.impl.BlocksPackageImpl#getlong()
		 * @generated
		 */
		EDataType LONG = eINSTANCE.getlong();

	}

} //BlocksPackage
