/**
 */
package gecos.dag.impl;

import gecos.dag.DAGVectorPackNode;
import gecos.dag.DagPackage;
import gecos.dag.DagVisitor;

import gecos.types.Type;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DAG Vector Pack Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DAGVectorPackNodeImpl extends DAGVectorNodeImpl implements DAGVectorPackNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DAGVectorPackNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DagPackage.Literals.DAG_VECTOR_PACK_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final DagVisitor visitor) {
		visitor.visitDAGVectorPackNode(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		int _ident = this.getIdent();
		String _plus = ("Pack_" + Integer.valueOf(_ident));
		String _plus_1 = (_plus + "_");
		Type _type = this.getType();
		return (_plus_1 + _type);
	}

} //DAGVectorPackNodeImpl
