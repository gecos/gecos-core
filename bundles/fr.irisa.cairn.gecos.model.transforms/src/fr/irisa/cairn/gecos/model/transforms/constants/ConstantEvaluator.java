/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.transforms.constants;

import fr.irisa.cairn.gecos.model.analysis.instructions.evaluation.InstructionEvaluation;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.core.Procedure;
import gecos.core.Symbol;
import gecos.instrs.ArrayValueInstruction;
import gecos.instrs.CondInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.RangeInstruction;
import gecos.instrs.SymbolInstruction;

public class ConstantEvaluator extends BlockInstructionSwitch<Object> {

	private boolean changed;
	private Block block;

	private static boolean VERBOSE=false;

	private void debug(String mess) {
		if (VERBOSE) {
			System.out.println(mess);
		}
	}

	public ConstantEvaluator(Procedure proc) {
		
		this.block = proc.getBody();
	}

	public ConstantEvaluator(Block b) {
		this.block = b;
	}

	/**
	 * Compute the evaluator 1 time.
	 * @return true if IR changed, false otherwise.
	 */
	public boolean computeIterative() {
		changed = false;
		if (block.getContainingProcedureSet() != null && block.getContainingProcedureSet().getScope() != null)
			GecosUserTypeFactory.setScope(block.getContainingProcedureSet().getScope());
		else
			GecosUserTypeFactory.setScope(null);
		doSwitch(block);
		return changed;
	}

	@Override
	public Object caseBasicBlock(BasicBlock b) {
		debug("Visiting block "+b);
		Object object = super.caseBasicBlock(b);
		return object;
	}

	@Override
	public Object caseGenericInstruction(GenericInstruction g) {
		debug("[Generic] Visiting inst "+g);
		int size = g.getOperands().size();
		for (int i = 0; i < size; ++i)
			doSwitch(g.getOperand(i));

		//special process for mux : (cond)?resiftrue:resiffalse
		//in this case, the only operand that has to be constant 
		//is the condition.
		if (size == 3 && g.getName().equals("mux") && g.getOperand(0).isConstant()) {
			Instruction t = InstructionEvaluation.applyTernary(g);
			if (t != null) {
				if (g.getParent() == null) {
					replaceOldValueByNewValue(g,t);
				} else {
					replaceOldValueByNewValue(g,t);
				}
				changed = true;
			}

		} else { 

			for (int i = 0; i < size; ++i)
				if (!g.getOperand(i).isConstant()) {
					debug(g.getOperand(i)+ " is not a constant");
					return null;
				}

			debug("\tis a constant expression");

			// at this point children are constants
			if (size == 2 && !g.getName().equals("call")) {
				debug("simplifying "+g); 
				Instruction t = InstructionEvaluation.applyBinary(g);
				if (t != null) {
					if (g.getParent() == null) {
						if (g.getBasicBlock()!=null) {
							g.getBasicBlock().replaceInstruction(g,t);
						} else if (g.eContainer() instanceof Symbol){
							Symbol sym = (Symbol) g.eContainer();
							sym.setValue(t);
						} else {
							throw new RuntimeException("Unhandled container type "+g.eContainer());
						}
					} else {
						replaceOldValueByNewValue(g,t);
					}
					changed = true;
				}

			} else if (g.getName().equals("array")) {
				if (!(g.getOperand(0) instanceof ArrayValueInstruction))
					return null;
				ArrayValueInstruction value = (ArrayValueInstruction) g
				.getOperand(0);
				IntInstruction index = (IntInstruction) g.getOperand(1);
				if (index.getValue() < 0
						|| index.getValue() >= value.getChildren().size())
					throw new RuntimeException("bad array value index ");
				
				Instruction t = value.getChildren().get((int)index.getValue());
				if (g.getParent() == null) {
					replaceOldValueByNewValue(g,t);
				} else {
					replaceOldValueByNewValue(g,t);
				}
				
				changed = true;
			} else if (size == 1 && g.getOperand(0).isConstant()) {
				Instruction t = InstructionEvaluation.applyUnary(g);
				if (t != null) {
					if (g.getParent() == null) {
						replaceOldValueByNewValue(g,t);
					} else {
						replaceOldValueByNewValue(g,t);
					}
					changed = true;
				}
			}
		}
		return null;

	}

	@Override
	public Object caseConvertInstruction(ConvertInstruction c) {
		doSwitch(c.getExpr());
		return null;
	}
	
	@Override
	public Object caseCondInstruction(CondInstruction s) {
		if (s.getCond() != null) doSwitch(s.getCond());
		return null;
	}

	@Override
	public Object caseSymbolInstruction(SymbolInstruction s) {
		if (s.isConstant()) {
			Symbol sym = s.getSymbol();
			if (sym != null && sym.getValue() != null) {
				replaceOldValueByNewValue(s, sym.getValue());
			}
		}
		return null;
	}

	/*
	 * @Override public Object caseSet(SetInstruction set) {
	 * set.source().visit(this, arg); set.destination().visit(this, arg); }
	 */

	@Override
	public Object caseRangeInstruction(RangeInstruction range) {
		doSwitch(range.getExpr());
		if (range.getExpr().isConstant()) {
			Instruction t = InstructionEvaluation.applyRange(range);
			if (t != null) {
				replaceOldValueByNewValue(range, t);
				changed = true;
			}
		}
		return null;
	}

	public boolean isChanged() {
		return changed;
	}
	
	private void replaceOldValueByNewValue(Instruction oldValue, Instruction newValue) {
		oldValue.substituteWith(newValue);
	}
}