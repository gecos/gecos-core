package fr.irisa.cairn.gecos.model.cdtfrontend.initcdt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.cdt.core.CCorePlugin;
import org.eclipse.cdt.core.model.ICProject;
import org.eclipse.cdt.core.settings.model.CExternalSetting;
import org.eclipse.cdt.core.settings.model.CIncludePathEntry;
import org.eclipse.cdt.core.settings.model.CMacroEntry;
import org.eclipse.cdt.core.settings.model.ICConfigurationDescription;
import org.eclipse.cdt.core.settings.model.ICLanguageSettingEntry;
import org.eclipse.cdt.core.settings.model.ICProjectDescription;
import org.eclipse.cdt.core.settings.model.ICProjectDescriptionManager;
import org.eclipse.cdt.core.settings.model.ICSettingEntry;
import org.eclipse.cdt.core.settings.model.extension.CExternalSettingProvider;
import org.eclipse.cdt.core.settings.model.util.CDataUtil;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;

import fr.irisa.cairn.gecos.model.cdtfrontend.CDTFrontEnd;
import gecos.gecosproject.GecosHeaderDirectory;
import gecos.gecosproject.GecosProject;

public class GecosCDTSettingsProvider extends CExternalSettingProvider {
	
    /** The ID of this ExternalSettingProvider as specified in the plugin XML */
    public static final String ID = CDTFrontEnd.PLUGIN_ID + ".gecos_external_setting_provider"; //$NON-NLS-1$

    /**
     * Store data relative to each GeCoS project statically.
     */
	private final static LinkedHashMap<IProject, GecosProject> PROJECT_REGISTRY = new LinkedHashMap<>();
	private final static LinkedHashMap<GecosProject, IProject> PROJECT_REVERSE_REGISTRY = new LinkedHashMap<>();
    private final static Map<GecosProject, Map<String,String>> GECOS_MACRO_REGISTRY = new HashMap<GecosProject, Map<String,String>>();
    private final static Map<GecosProject, List<String>> GECOS_INCLUDES_REGISTRY = new HashMap<GecosProject, List<String>>();
	
    /**
     * Remove all data related to gProject
     */
	public static void deleteSettings(GecosProject gProject) {
		if (GECOS_MACRO_REGISTRY.containsKey(gProject)) {
			GECOS_MACRO_REGISTRY.get(gProject).clear();
			GECOS_MACRO_REGISTRY.remove(gProject);
		}

		if (GECOS_INCLUDES_REGISTRY.containsKey(gProject)) {
			GECOS_INCLUDES_REGISTRY.get(gProject).clear();
			GECOS_INCLUDES_REGISTRY.remove(gProject);
		}
		
		if (PROJECT_REVERSE_REGISTRY.containsKey(gProject)) {
			IProject project = PROJECT_REVERSE_REGISTRY.get(gProject);
			PROJECT_REGISTRY.remove(project);
			PROJECT_REVERSE_REGISTRY.remove(gProject);
		}
		
		// consistency check
//		if (PROJECT_REGISTRY.size() > 0 ||
//				PROJECT_REVERSE_REGISTRY.size() > 0 ||
//				GECOS_MACRO_REGISTRY.size() > 0 ||
//				GECOS_INCLUDES_REGISTRY.size() > 0) {
//			throw new RuntimeException("memory leak");
//		}
	}

	/**
	 * Register the project, build the includes and macros registry, and add
	 * this external setting provider to the CDT project.
	 * 
	 * @param cProject
	 * @param gProject
	 * @throws CoreException
	 */
	public static void setup(ICProject cProject, GecosProject gProject) throws CoreException {
		IProject project = cProject.getProject();
		
		/**
		 * register project
		 */
		PROJECT_REGISTRY.put(project, gProject);
		PROJECT_REVERSE_REGISTRY.put(gProject, project);
		
		/**
		 * register macros
		 */
		Map<String, String> projectMacros = new HashMap<>();
		projectMacros.putAll(CDTFrontEnd.GECOS_DEFAULT_MACROS);
//		projectMacros.putAll(gProject.getMacros());
		gProject.getMacros().forEach(e -> projectMacros.put(e.getKey(), e.getValue()));
		GECOS_MACRO_REGISTRY.put(gProject,projectMacros);
		
		/**
		 * register includes
		 */
		List<String> includes = new ArrayList<>(gProject.getIncludes().size());
		for (GecosHeaderDirectory dir : gProject.getIncludes()) 
			includes.add(dir.getName());
		GECOS_INCLUDES_REGISTRY.put(gProject, includes);
		
		/**
		 * add setting provider to the CDT Project
		 */
		ICProjectDescription des = CDTFrontEnd.cdtCoreModel.getProjectDescription(project);
		if (des == null) {
			String cfgProviderId = GecosCDTDataProvider.PROVIDER_ID;
			final String finalCfgProviderId = cfgProviderId;
			ICConfigurationDescription prefCfg = CCorePlugin.getDefault().getPreferenceConfiguration(finalCfgProviderId);
			if (prefCfg == null) 
				throw new NullPointerException();
			ICProjectDescriptionManager mngr = CCorePlugin.getDefault().getProjectDescriptionManager();
			ICProjectDescription projDes = mngr.createProjectDescription(project, false, false);
			String genId = CDataUtil.genId("test");
			String genId2 = CDataUtil.genId(null);
			projDes.createConfiguration(genId2, genId, prefCfg);
			mngr.setProjectDescription(project, projDes);
			des = projDes;
		}
		main: for (ICConfigurationDescription cfgDes : des.getConfigurations()) {
			String[] origESPI = cfgDes.getExternalSettingsProviderIds();
			for (String s : origESPI)
				if (s.equals(GecosCDTSettingsProvider.ID))
					continue main;
			String[] newESPI = new String[origESPI.length+1];
			System.arraycopy(origESPI, 0, newESPI, 0, origESPI.length);
			newESPI[origESPI.length] = GecosCDTSettingsProvider.ID;
			cfgDes.setExternalSettingsProviderIds(newESPI);
		}
		CDTFrontEnd.cdtCoreModel.setProjectDescription(project, des);
	}
	
	@Override
	public CExternalSetting[] getSettings(IProject project, ICConfigurationDescription cfg) {
		/**
		 * Builds the project settings using static values in the registry.
		 */
		if (PROJECT_REGISTRY.containsKey(project)) {
			//typical case : when using scripts (GecosProject is built before CDTProject)
			GecosProject gProject = PROJECT_REGISTRY.get(project);
			Map<String, String> macros = GECOS_MACRO_REGISTRY.get(gProject);
			List<String> includes = GECOS_INCLUDES_REGISTRY.get(gProject);
			
			CExternalSetting[] cExternalSettings = buildSettings(macros, includes);
			return cExternalSettings;
		} else {
			// if no project is associated, then return default settings
			// typical case : when using Codan (CDTProject is built before GecosProject)
			Map<String, String> macros = CDTFrontEnd.GECOS_DEFAULT_MACROS;
			List<String> includes = new ArrayList<>(0);
			
			CExternalSetting[] cExternalSettings = buildSettings(macros, includes);
			return cExternalSettings;
		}
	}

	private CExternalSetting[] buildSettings(Map<String, String> macros,
			List<String> includes) {
		ICLanguageSettingEntry[] res = new ICLanguageSettingEntry[macros.size()+includes.size()];
		int index = 0;
		for (Entry<String,String> e : macros.entrySet()) 
			res[index++] = new CMacroEntry(e.getKey(), e.getValue(),ICSettingEntry.MACRO);
		for (String inc : includes) 
			res[index++] = new CIncludePathEntry(inc,ICSettingEntry.INCLUDE_PATH);
		
		CExternalSetting[] cExternalSettings = new CExternalSetting[] { new CExternalSetting(null, null, null, res) };
		return cExternalSettings;
	}

}
