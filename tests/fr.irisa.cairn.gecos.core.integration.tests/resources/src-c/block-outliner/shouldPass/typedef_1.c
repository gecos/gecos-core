int main() {
	double res = 0;

	#pragma outline
	{
		typedef float T;
		T sum = 2.5 + 1.25;
		res += (sum*sum);
	} // res = 14.0625

	#pragma outline
	{
		typedef short T;
		T sum = 5;
		res += (sum*sum);
	} // res = 39.0625

	//-- test --
	if(res != 39.0625)
		abort();

	return 0;
}
