/**
 */
package gecos.instrs;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Array Value Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.instrs.InstrsPackage#getArrayValueInstruction()
 * @model
 * @generated
 */
public interface ArrayValueInstruction extends ChildrenListInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitArrayValueInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _join = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.join(this.listChildren(), \",\");\n&lt;%java.lang.String%&gt; _plus = (\"{\" + _join);\nreturn (_plus + \"}\");'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; _listChildren = this.listChildren();\nfor (final &lt;%gecos.instrs.Instruction%&gt; child : _listChildren)\n{\n\tboolean _isConstant = child.isConstant();\n\tboolean _not = (!_isConstant);\n\tif (_not)\n\t{\n\t\treturn false;\n\t}\n}\nreturn true;'"
	 * @generated
	 */
	boolean isConstant();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.ArrayValueInstruction%&gt;))\n{\n\treturn super.isSame(instr);\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // ArrayValueInstruction
