package fr.irisa.cairn.gecos.model.utils.dotprinter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.gecos.model.tools.utils.FixBlockNumbers;
import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDefaultVisitor;
import fr.irisa.cairn.gecos.model.utils.instructionprettyprinter.InstrsPrettyPrinter;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.SimpleForBlock;
import gecos.blocks.SwitchBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.Scope;
import gecos.core.ScopeContainer;
import gecos.core.Symbol;
import gecos.dag.DAGEdge;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGNode;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.LabelInstruction;

/**
 * Printer of {@link ProcedureSet} to graphviz DOT format. This printer can
 * easily be extended using the 'gecos.visitors' extension point.
 * 
 * @author antoine
 * 
 */
@GSModule("Export the IR into a DOT graph file.")
public class GecosDotPrinter extends GecosBlocksInstructionsDefaultVisitor {
	/**
	 * Output of the dot printer
	 */
	PrintStream out;
	private EObject target;
	private Procedure proc;
	private String path;
	boolean FIXBLOCKNUMBERS = false;
	enum Mode {NODE,EDGES};
	Mode mode = Mode.NODE;
	/**
	 * C generator of standard instructions
	 */
	private InstrsPrettyPrinter cGenerator = new InstrsPrettyPrinter(true);

	public InstrsPrettyPrinter getcGenerator() {
		return cGenerator;
	}

	public final static String COLOR_IF = "blue";
	public final static String COLOR_LOOP = "red";
	public final static String COLOR_WHILE = "purple";
	public final static String COLOR_FOR = "chartreuse3";
	public final static String COLOR_COMPOSITE = "gray";
	public final static String COLOR_DAG = "none";
	public final static String COLOR_BBDAG = "dodgerblue";

	private int tabulation;
	private GecosDotEdgePrinter edgeVisitor;

	@GSModuleConstructor(
		"-arg1: path of the output file.\n"
	  + "-arg2: Gecos object containing Procedures (e.g. ProcedureSet).\n"
	  + "   All of the contained procedures will be exported.")
	public GecosDotPrinter(String path, EObject target) {
		this.path = path;
		this.target = target;
	}

	@GSModuleConstructor(
			"-arg1: path of the output file.\n"
		  + "-arg2: Procedure object to export.")
	public GecosDotPrinter(String path, Procedure proc) {
		this.path = path;
		this.proc = proc;
	}

	@GSModuleConstructor(
		"-arg1: path of the output file.\n"
	  + "-arg2: Procedure object to export.\n"
	  + "-arg3: String of options. Supported options:\n"
	  + "   'FIXBLOCKNUMBERS' to fix block numbers first.")
	public GecosDotPrinter(String path, Procedure proc, String options) {
		this.path = path;
		this.proc = proc;
		FIXBLOCKNUMBERS = (options.contains("FIXBLOCKNUMBERS"));
	}

	public void compute() {
		edgeVisitor = new GecosDotEdgePrinter(this);
		if (target != null) {
			EList<Procedure> r = EMFUtils.<Procedure>eAllContentsFirstInstancesOf(target, Procedure.class);
			for(Procedure p : r) {
				if(FIXBLOCKNUMBERS) {
					FixBlockNumbers fdixer = new FixBlockNumbers(p);
					fdixer.compute();
				}
				visit(p);
			}
		} else if (proc != null) {
			
			if(FIXBLOCKNUMBERS) {
				FixBlockNumbers fdixer = new FixBlockNumbers(proc);
				fdixer.compute();
			}
			visit(proc);
		}
	}

	private void tabulate() {
		for (int i = 0; i < tabulation; i++) {
			out.print("\t");
		}
	}

	public void tabulateMore() {
		tabulation++;
	}

	public void tabulateLess() {
		tabulation--;
	}

	@Override
	public void visit(Procedure proc) {
		String file = path + java.io.File.separator + proc.getSymbol().getName() + ".dot";
		File f = new File(file);
		if (!f.exists()) {
			f.mkdirs();
			f.delete();
		}
		try {
			out = new PrintStream(new FileOutputStream(file));
			println("digraph G {");
			println("compound=true;");
			tabulateMore();
			println("node [shape=box];");
			mode=Mode.NODE;
			proc.getBody().accept(this);
			
			proc.getBody().accept(edgeVisitor);
			tabulateLess();
			println("}");
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public void print(String content) {
		tabulate();
		out.print(content);
	}

	public void printNoTabulation(String content) {
		out.print(content);
	}

	public void println(String content) {
		print(content + "\n");
	}

	@Override
	public void visitBasicBlock(BasicBlock b) {
		int number = b.getNumber();
		String bbLabel = "BB"+number;
		
		//pretty print Blocks init/test/step
		if (b.getParent() instanceof ForBlock) {
			if (b == ((ForBlock)b.getParent()).getInitBlock())
				bbLabel = bbLabel+" (loop init)";
			else if (b == ((ForBlock)b.getParent()).getTestBlock())
				bbLabel = bbLabel+" (loop test)";
			else if (b == ((ForBlock)b.getParent()).getStepBlock())
				bbLabel = bbLabel+" (loop step)";
		} else if (b.getParent() instanceof DoWhileBlock) {
			if (b == ((DoWhileBlock)b.getParent()).getTestBlock())
				bbLabel = bbLabel+" (loop test)";
		} else if (b.getParent() instanceof WhileBlock) {
			if (b == ((WhileBlock)b.getParent()).getTestBlock())
				bbLabel = bbLabel+" (loop test)";
		} else if (b.getParent() instanceof IfBlock) {
			if (b == ((IfBlock)b.getParent()).getTestBlock())
				bbLabel = bbLabel+" (if test)";
		} else if (b.getParent() instanceof SwitchBlock) {
			if (b == ((SwitchBlock)b.getParent()).getDispatchBlock())
				bbLabel = bbLabel+" (switch dispatch)";
		}
		
		
		if ( !hasDagInstruction(b)) {
			out.print("\n");
			print("n" + number + " [label=\""  + bbLabel);
			if (b.getInstructions().size() > 0)
				out.print("\\n");
			super.visitBasicBlock(b);
			out.println("\"];");
		} else {
			printClusterHeader(b, COLOR_BBDAG, "B");
			println("n"+b.getNumber()+" [shape=box,color="+COLOR_BBDAG+",fontcolor="+COLOR_BBDAG+",label="+bbLabel+"];");
			super.visitBasicBlock(b);
			printClusterTail();
		}
	}

	public boolean hasDagInstruction(BasicBlock bb) {
		return bb.getInstructionCount() > 0
				&& (bb.getInstruction(0) instanceof DAGInstruction)&&((DAGInstruction)bb.getInstruction(0)).getNodes().size()>0;
	}

	protected void printClusterHeader(Block b, String color) {
		printClusterHeader(b, color, "");
	}

	public void printClusterHeader(Block b, String color, String prefix) {
		println("subgraph cluster" + b.getNumber() + " {");
		tabulateMore();
		println("compound=true;");
		println("style=solid;");
		println("color=" + color + ";");
		String scope="";
		if(b instanceof ScopeContainer)
			scope= printScope(b.getScope());
		println("label=\"" + prefix + "B" + b.getNumber()
				+ scope + "\";");
	}

	public void printClusterTail() {
		tabulateLess();
		println("}");
	} 

	@Override
	public void visitCompositeBlock(CompositeBlock b) {
		println("subgraph cluster" + b.getNumber() + " {");
		tabulateMore();
		println("compound=true;");
		println("style=dashed;");
		println("color=" + COLOR_COMPOSITE + ";");
		println("label=\" Composite " + b.getNumber() + printScope(b.getScope()) + "\";");
		super.visitCompositeBlock(b);
		printClusterTail();
	}

	@Override
	public void visitIfBlock(IfBlock b) {
		printClusterHeader(b, COLOR_IF, "If:");
		super.visitIfBlock(b);
		printClusterTail();
	}

	@Override
	public void visitLoopBlock(DoWhileBlock b) {
		printClusterHeader(b, COLOR_LOOP, "Loop:");
		super.visitLoopBlock(b);
		printClusterTail();
	}

	@Override
	public void visitWhileBlock(WhileBlock b) {
		printClusterHeader(b, COLOR_WHILE, "While:");
		super.visitWhileBlock(b);
		printClusterTail();
	}

	@Override
	public void visitForBlock(ForBlock b) {
		printClusterHeader(b, COLOR_FOR, "For:");
		super.visitForBlock(b);
		printClusterTail();
	}

	@Override
	public void visitSimpleForBlock(SimpleForBlock b) {
		visitForBlock(b);
	}

//	protected void visitLabelInstruction(ComplexInstruction c) {
//		out.print(cGenerator.doSwitch(c) + ";\\n");
//	}
	
	@Override
	public void visitLabelInstruction(LabelInstruction l) {
		out.print("label: "+l.getName()+"\\n");
	}
	
	@Override
	protected void visitInstruction(Instruction c) {
		out.print(cGenerator.doSwitch(c) + ";\\n");
	}
	
	@Override
	protected void visitComplexInstruction(ComplexInstruction c) {
		// dot visit children of complex instructions since the C generator will
		// take carer of them
		visitInstruction(c);
	}

	/**
	 * Current id of the last {@link DAGInstruction}.
	 */
	int dottyClusterId;

	@Override
	public void visitDAGInstruction(DAGInstruction s) {

		if (s.getNodes().size() > 0) {
			println("subgraph clusterDag" + (++dottyClusterId) + " {");
			tabulateMore();
			println("color=" + COLOR_DAG + ";");
			println("label=\"\";");
			DAGDottyExport dagExport = new DAGDottyExport();
			for (DAGNode n : s.getNodes()) {
				println(dagExport.doSwitch(n));
			}
			for (DAGEdge e : s.getEdges()) {
				println(dagExport.doSwitch(e));
			}
			tabulateLess();
			println("}");
		}
	}

	private String printScope(Scope scope) {
		if (scope!=null) {
			StringBuffer buffer = new StringBuffer();
			buffer.append("(");
			boolean first = true;
			for (Symbol sym : scope.getSymbols()) {
				if (first) first = false;
				else buffer.append(",");
				buffer.append(sym.getName());
				if (sym.getValue() != null)
					buffer.append(" = "+cGenerator.doSwitch(sym.getValue()));
			}
			buffer.append(")");
			return buffer.toString();
		} else {
			return "";
		}
	}

}
