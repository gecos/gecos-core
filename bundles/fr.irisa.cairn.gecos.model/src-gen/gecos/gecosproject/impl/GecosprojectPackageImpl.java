/**
 */
package gecos.gecosproject.impl;

import gecos.annotations.AnnotationsPackage;

import gecos.blocks.BlocksPackage;

import gecos.core.CorePackage;

import gecos.dag.DagPackage;

import gecos.gecosproject.GecosFile;
import gecos.gecosproject.GecosHeaderDirectory;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;
import gecos.gecosproject.GecosprojectFactory;
import gecos.gecosproject.GecosprojectPackage;
import gecos.gecosproject.ProjectVisitable;
import gecos.gecosproject.ProjectVisitor;

import gecos.instrs.InstrsPackage;

import gecos.types.TypesPackage;

import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class GecosprojectPackageImpl extends EPackageImpl implements GecosprojectPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass projectVisitorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass projectVisitableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringToStringMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gecosProjectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gecosFileEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gecosSourceFileEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gecosHeaderDirectoryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType stringEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see gecos.gecosproject.GecosprojectPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private GecosprojectPackageImpl() {
		super(eNS_URI, GecosprojectFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link GecosprojectPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static GecosprojectPackage init() {
		if (isInited) return (GecosprojectPackage)EPackage.Registry.INSTANCE.getEPackage(GecosprojectPackage.eNS_URI);

		// Obtain or create and register package
		GecosprojectPackageImpl theGecosprojectPackage = (GecosprojectPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof GecosprojectPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new GecosprojectPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		CorePackage.eINSTANCE.eClass();
		AnnotationsPackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		InstrsPackage.eINSTANCE.eClass();
		BlocksPackage.eINSTANCE.eClass();
		DagPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theGecosprojectPackage.createPackageContents();

		// Initialize created meta-data
		theGecosprojectPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theGecosprojectPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(GecosprojectPackage.eNS_URI, theGecosprojectPackage);
		return theGecosprojectPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProjectVisitor() {
		return projectVisitorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProjectVisitable() {
		return projectVisitableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringToStringMap() {
		return stringToStringMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringToStringMap_Key() {
		return (EAttribute)stringToStringMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringToStringMap_Value() {
		return (EAttribute)stringToStringMapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGecosProject() {
		return gecosProjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGecosProject_Name() {
		return (EAttribute)gecosProjectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGecosProject_Includes() {
		return (EReference)gecosProjectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGecosProject_Sources() {
		return (EReference)gecosProjectEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGecosProject_Macros() {
		return (EReference)gecosProjectEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGecosFile() {
		return gecosFileEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGecosFile_Name() {
		return (EAttribute)gecosFileEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGecosSourceFile() {
		return gecosSourceFileEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGecosSourceFile_Model() {
		return (EReference)gecosSourceFileEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGecosHeaderDirectory() {
		return gecosHeaderDirectoryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGecosHeaderDirectory_IsStandardHeader() {
		return (EAttribute)gecosHeaderDirectoryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getString() {
		return stringEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GecosprojectFactory getGecosprojectFactory() {
		return (GecosprojectFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		projectVisitorEClass = createEClass(PROJECT_VISITOR);

		projectVisitableEClass = createEClass(PROJECT_VISITABLE);

		stringToStringMapEClass = createEClass(STRING_TO_STRING_MAP);
		createEAttribute(stringToStringMapEClass, STRING_TO_STRING_MAP__KEY);
		createEAttribute(stringToStringMapEClass, STRING_TO_STRING_MAP__VALUE);

		gecosProjectEClass = createEClass(GECOS_PROJECT);
		createEAttribute(gecosProjectEClass, GECOS_PROJECT__NAME);
		createEReference(gecosProjectEClass, GECOS_PROJECT__INCLUDES);
		createEReference(gecosProjectEClass, GECOS_PROJECT__SOURCES);
		createEReference(gecosProjectEClass, GECOS_PROJECT__MACROS);

		gecosFileEClass = createEClass(GECOS_FILE);
		createEAttribute(gecosFileEClass, GECOS_FILE__NAME);

		gecosSourceFileEClass = createEClass(GECOS_SOURCE_FILE);
		createEReference(gecosSourceFileEClass, GECOS_SOURCE_FILE__MODEL);

		gecosHeaderDirectoryEClass = createEClass(GECOS_HEADER_DIRECTORY);
		createEAttribute(gecosHeaderDirectoryEClass, GECOS_HEADER_DIRECTORY__IS_STANDARD_HEADER);

		// Create data types
		stringEDataType = createEDataType(STRING);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		AnnotationsPackage theAnnotationsPackage = (AnnotationsPackage)EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		projectVisitableEClass.getESuperTypes().add(theCorePackage.getGecosNode());
		gecosProjectEClass.getESuperTypes().add(this.getProjectVisitable());
		gecosProjectEClass.getESuperTypes().add(theAnnotationsPackage.getAnnotatedElement());
		gecosFileEClass.getESuperTypes().add(this.getProjectVisitable());
		gecosFileEClass.getESuperTypes().add(theAnnotationsPackage.getAnnotatedElement());
		gecosSourceFileEClass.getESuperTypes().add(this.getGecosFile());
		gecosHeaderDirectoryEClass.getESuperTypes().add(this.getGecosFile());

		// Initialize classes and features; add operations and parameters
		initEClass(projectVisitorEClass, ProjectVisitor.class, "ProjectVisitor", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = addEOperation(projectVisitorEClass, null, "visitProject", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getGecosProject(), "project", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(projectVisitorEClass, null, "visitSourceFile", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getGecosSourceFile(), "sourceFile", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(projectVisitorEClass, null, "visitHeaderDirectory", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getGecosHeaderDirectory(), "headerDirectory", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(projectVisitableEClass, ProjectVisitable.class, "ProjectVisitable", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(projectVisitableEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getProjectVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(stringToStringMapEClass, Map.Entry.class, "StringToStringMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringToStringMap_Key(), this.getString(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStringToStringMap_Value(), this.getString(), "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(gecosProjectEClass, GecosProject.class, "GecosProject", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGecosProject_Name(), this.getString(), "name", "gecos_project", 0, 1, GecosProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGecosProject_Includes(), this.getGecosHeaderDirectory(), null, "includes", null, 0, -1, GecosProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGecosProject_Sources(), this.getGecosSourceFile(), null, "sources", null, 0, -1, GecosProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGecosProject_Macros(), this.getStringToStringMap(), null, "macros", null, 0, -1, GecosProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(gecosProjectEClass, theCorePackage.getProcedureSet(), "listProcedureSets", 0, -1, IS_UNIQUE, IS_ORDERED);

		addEOperation(gecosProjectEClass, theCorePackage.getProcedure(), "listProcedures", 0, -1, IS_UNIQUE, IS_ORDERED);

		addEOperation(gecosProjectEClass, theEcorePackage.getEBoolean(), "isParsed", 1, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(gecosProjectEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getProjectVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(gecosProjectEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(gecosProjectEClass, this.getGecosProject(), "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(gecosFileEClass, GecosFile.class, "GecosFile", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGecosFile_Name(), this.getString(), "name", null, 0, 1, GecosFile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(gecosSourceFileEClass, GecosSourceFile.class, "GecosSourceFile", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGecosSourceFile_Model(), theCorePackage.getProcedureSet(), null, "model", null, 0, 1, GecosSourceFile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(gecosSourceFileEClass, theEcorePackage.getEBoolean(), "isParsed", 1, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(gecosSourceFileEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getProjectVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(gecosSourceFileEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(gecosHeaderDirectoryEClass, GecosHeaderDirectory.class, "GecosHeaderDirectory", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGecosHeaderDirectory_IsStandardHeader(), theEcorePackage.getEBoolean(), "isStandardHeader", null, 0, 1, GecosHeaderDirectory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(gecosHeaderDirectoryEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getProjectVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		// Initialize data types
		initEDataType(stringEDataType, String.class, "String", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
		   });
	}

} //GecosprojectPackageImpl
