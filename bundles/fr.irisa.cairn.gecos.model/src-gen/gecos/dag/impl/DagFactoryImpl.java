/**
 */
package gecos.dag.impl;

import gecos.dag.*;

import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DagFactoryImpl extends EFactoryImpl implements DagFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DagFactory init() {
		try {
			DagFactory theDagFactory = (DagFactory)EPackage.Registry.INSTANCE.getEFactory(DagPackage.eNS_URI);
			if (theDagFactory != null) {
				return theDagFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DagFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DagFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case DagPackage.DAG_INSTRUCTION: return createDAGInstruction();
			case DagPackage.DAG_IN_PORT: return createDAGInPort();
			case DagPackage.DAG_OUT_PORT: return createDAGOutPort();
			case DagPackage.DAG_SYMBOL_NODE: return createDAGSymbolNode();
			case DagPackage.DAG_CALL_NODE: return createDAGCallNode();
			case DagPackage.DAG_ARRAY_VALUE_NODE: return createDAGArrayValueNode();
			case DagPackage.DAG_OP_NODE: return createDAGOpNode();
			case DagPackage.DAG_OUT_NODE: return createDAGOutNode();
			case DagPackage.DAG_CONTROL_EDGE: return createDAGControlEdge();
			case DagPackage.DAG_DATA_EDGE: return createDAGDataEdge();
			case DagPackage.DAG_NUMBERED_SYMBOL_NODE: return createDAGNumberedSymbolNode();
			case DagPackage.DAG_NUMBERED_OUT_NODE: return createDAGNumberedOutNode();
			case DagPackage.DAG_INT_IMM_NODE: return createDAGIntImmNode();
			case DagPackage.DAG_FLOAT_IMM_NODE: return createDAGFloatImmNode();
			case DagPackage.DAG_STRING_IMM_NODE: return createDAGStringImmNode();
			case DagPackage.DAG_ARRAY_NODE: return createDAGArrayNode();
			case DagPackage.DAG_SIMPLE_ARRAY_NODE: return createDAGSimpleArrayNode();
			case DagPackage.DAG_FIELD_INSTRUCTION: return createDAGFieldInstruction();
			case DagPackage.DAG_JUMP_NODE: return createDAGJumpNode();
			case DagPackage.DAG_PATTERN_NODE: return createDAGPatternNode();
			case DagPackage.IN_PORT_MAP: return (EObject)createInPortMap();
			case DagPackage.OUT_PORT_MAP: return (EObject)createOutPortMap();
			case DagPackage.DAG_VECTOR_ARRAY_ACCESS_NODE: return createDAGVectorArrayAccessNode();
			case DagPackage.DAG_VECTOR_OP_NODE: return createDAGVectorOpNode();
			case DagPackage.DAG_VECTOR_SHUFFLE_NODE: return createDAGVectorShuffleNode();
			case DagPackage.DAG_VECTOR_EXTRACT_NODE: return createDAGVectorExtractNode();
			case DagPackage.DAG_VECTOR_EXPAND_NODE: return createDAGVectorExpandNode();
			case DagPackage.DAG_VECTOR_PACK_NODE: return createDAGVectorPackNode();
			case DagPackage.DAGSSA_USE_NODE: return createDAGSSAUseNode();
			case DagPackage.DAGSSA_DEF_NODE: return createDAGSSADefNode();
			case DagPackage.DAG_PHI_NODE: return createDAGPhiNode();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case DagPackage.DEPENDENCY_TYPE:
				return createDependencyTypeFromString(eDataType, initialValue);
			case DagPackage.DAG_OPERATOR:
				return createDAGOperatorFromString(eDataType, initialValue);
			case DagPackage.STRING:
				return createStringFromString(eDataType, initialValue);
			case DagPackage.INT:
				return createintFromString(eDataType, initialValue);
			case DagPackage.LONG:
				return createlongFromString(eDataType, initialValue);
			case DagPackage.DOUBLE:
				return createdoubleFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case DagPackage.DEPENDENCY_TYPE:
				return convertDependencyTypeToString(eDataType, instanceValue);
			case DagPackage.DAG_OPERATOR:
				return convertDAGOperatorToString(eDataType, instanceValue);
			case DagPackage.STRING:
				return convertStringToString(eDataType, instanceValue);
			case DagPackage.INT:
				return convertintToString(eDataType, instanceValue);
			case DagPackage.LONG:
				return convertlongToString(eDataType, instanceValue);
			case DagPackage.DOUBLE:
				return convertdoubleToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGInstruction createDAGInstruction() {
		DAGInstructionImpl dagInstruction = new DAGInstructionImpl();
		return dagInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGInPort createDAGInPort() {
		DAGInPortImpl dagInPort = new DAGInPortImpl();
		return dagInPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGOutPort createDAGOutPort() {
		DAGOutPortImpl dagOutPort = new DAGOutPortImpl();
		return dagOutPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGSymbolNode createDAGSymbolNode() {
		DAGSymbolNodeImpl dagSymbolNode = new DAGSymbolNodeImpl();
		return dagSymbolNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGCallNode createDAGCallNode() {
		DAGCallNodeImpl dagCallNode = new DAGCallNodeImpl();
		return dagCallNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGArrayValueNode createDAGArrayValueNode() {
		DAGArrayValueNodeImpl dagArrayValueNode = new DAGArrayValueNodeImpl();
		return dagArrayValueNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGOpNode createDAGOpNode() {
		DAGOpNodeImpl dagOpNode = new DAGOpNodeImpl();
		return dagOpNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGOutNode createDAGOutNode() {
		DAGOutNodeImpl dagOutNode = new DAGOutNodeImpl();
		return dagOutNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGControlEdge createDAGControlEdge() {
		DAGControlEdgeImpl dagControlEdge = new DAGControlEdgeImpl();
		return dagControlEdge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGDataEdge createDAGDataEdge() {
		DAGDataEdgeImpl dagDataEdge = new DAGDataEdgeImpl();
		return dagDataEdge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGNumberedSymbolNode createDAGNumberedSymbolNode() {
		DAGNumberedSymbolNodeImpl dagNumberedSymbolNode = new DAGNumberedSymbolNodeImpl();
		return dagNumberedSymbolNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGNumberedOutNode createDAGNumberedOutNode() {
		DAGNumberedOutNodeImpl dagNumberedOutNode = new DAGNumberedOutNodeImpl();
		return dagNumberedOutNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGIntImmNode createDAGIntImmNode() {
		DAGIntImmNodeImpl dagIntImmNode = new DAGIntImmNodeImpl();
		return dagIntImmNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGFloatImmNode createDAGFloatImmNode() {
		DAGFloatImmNodeImpl dagFloatImmNode = new DAGFloatImmNodeImpl();
		return dagFloatImmNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGStringImmNode createDAGStringImmNode() {
		DAGStringImmNodeImpl dagStringImmNode = new DAGStringImmNodeImpl();
		return dagStringImmNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGArrayNode createDAGArrayNode() {
		DAGArrayNodeImpl dagArrayNode = new DAGArrayNodeImpl();
		return dagArrayNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGSimpleArrayNode createDAGSimpleArrayNode() {
		DAGSimpleArrayNodeImpl dagSimpleArrayNode = new DAGSimpleArrayNodeImpl();
		return dagSimpleArrayNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGFieldInstruction createDAGFieldInstruction() {
		DAGFieldInstructionImpl dagFieldInstruction = new DAGFieldInstructionImpl();
		return dagFieldInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGJumpNode createDAGJumpNode() {
		DAGJumpNodeImpl dagJumpNode = new DAGJumpNodeImpl();
		return dagJumpNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGPatternNode createDAGPatternNode() {
		DAGPatternNodeImpl dagPatternNode = new DAGPatternNodeImpl();
		return dagPatternNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<DAGInPort, DAGInPort> createInPortMap() {
		InPortMapImpl inPortMap = new InPortMapImpl();
		return inPortMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<DAGOutPort, DAGOutPort> createOutPortMap() {
		OutPortMapImpl outPortMap = new OutPortMapImpl();
		return outPortMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGVectorArrayAccessNode createDAGVectorArrayAccessNode() {
		DAGVectorArrayAccessNodeImpl dagVectorArrayAccessNode = new DAGVectorArrayAccessNodeImpl();
		return dagVectorArrayAccessNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGVectorOpNode createDAGVectorOpNode() {
		DAGVectorOpNodeImpl dagVectorOpNode = new DAGVectorOpNodeImpl();
		return dagVectorOpNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGVectorShuffleNode createDAGVectorShuffleNode() {
		DAGVectorShuffleNodeImpl dagVectorShuffleNode = new DAGVectorShuffleNodeImpl();
		return dagVectorShuffleNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGVectorExtractNode createDAGVectorExtractNode() {
		DAGVectorExtractNodeImpl dagVectorExtractNode = new DAGVectorExtractNodeImpl();
		return dagVectorExtractNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGVectorExpandNode createDAGVectorExpandNode() {
		DAGVectorExpandNodeImpl dagVectorExpandNode = new DAGVectorExpandNodeImpl();
		return dagVectorExpandNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGVectorPackNode createDAGVectorPackNode() {
		DAGVectorPackNodeImpl dagVectorPackNode = new DAGVectorPackNodeImpl();
		return dagVectorPackNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGSSAUseNode createDAGSSAUseNode() {
		DAGSSAUseNodeImpl dagssaUseNode = new DAGSSAUseNodeImpl();
		return dagssaUseNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGSSADefNode createDAGSSADefNode() {
		DAGSSADefNodeImpl dagssaDefNode = new DAGSSADefNodeImpl();
		return dagssaDefNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGPhiNode createDAGPhiNode() {
		DAGPhiNodeImpl dagPhiNode = new DAGPhiNodeImpl();
		return dagPhiNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DependencyType createDependencyTypeFromString(EDataType eDataType, String initialValue) {
		DependencyType result = DependencyType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDependencyTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGOperator createDAGOperatorFromString(EDataType eDataType, String initialValue) {
		DAGOperator result = DAGOperator.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDAGOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createStringFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStringToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer createintFromString(EDataType eDataType, String initialValue) {
		return (Integer)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertintToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Long createlongFromString(EDataType eDataType, String initialValue) {
		return (Long)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertlongToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double createdoubleFromString(EDataType eDataType, String initialValue) {
		return (Double)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertdoubleToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DagPackage getDagPackage() {
		return (DagPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DagPackage getPackage() {
		return DagPackage.eINSTANCE;
	}

} //DagFactoryImpl
