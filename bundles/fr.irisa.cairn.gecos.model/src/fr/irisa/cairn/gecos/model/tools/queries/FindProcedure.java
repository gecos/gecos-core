/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.tools.queries;

import gecos.core.Procedure;
import gecos.core.ProcedureSet;

public class FindProcedure {
	
	private Procedure found;

	public FindProcedure(ProcedureSet procset, String name) {
		for (Procedure proc : procset.listProcedures()) {
			if(proc.getSymbol().getName().equals(name))
				this.found = proc;
		}
		if (found==null) {
			System.err.println("Warning : no procedure name matching "+name+" in "+procset);
		}
	}
	
	public Procedure compute() {
		return found;
	}

}
