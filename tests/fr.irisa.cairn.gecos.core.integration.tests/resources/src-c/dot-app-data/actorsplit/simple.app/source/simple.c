#include "defs.h"
#include "data_size.h"

void simple(TYPE_0 in[M][N], TYPE_1 out[M][N]){

	int i, j;

	for(i = 0; i < M; i++) {
		for(j = 1; j < N - 1; j++) {
			out[i][j] = 0.5*in[i][j] + 0.25*in[i][j-1] + 0.25*in[i][j+1];
		}
	}
}
