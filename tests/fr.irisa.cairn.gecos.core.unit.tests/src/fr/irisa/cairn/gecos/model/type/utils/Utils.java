package fr.irisa.cairn.gecos.model.type.utils;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import fr.irisa.r2d2.gecos.framework.utils.SystemExec;
import gecos.types.FunctionType;
import gecos.types.IntegerType;
import gecos.types.PtrType;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;

public class Utils {

	@FunctionalInterface
	interface TypeModifier extends Function<Type, Type> {};
	
	@FunctionalInterface
	interface SpecModifier extends Function<Specifier, Specifier> {}; 
	
	@FunctionalInterface
	interface DeclModifier extends Function<Declarator, Declarator> {};
	
	
	public static Declarator SDECL(String pointers, String identifier, String arraysOrparams) {
		return new SimpleDeclarator(pointers, identifier, arraysOrparams);
	}
	
	public static Declarator CDECL(String pointers, Declarator inner, String arraysOrparams) {
		return new ComplexDeclarator(pointers, inner, arraysOrparams);
	}
	
	
	public static Specifier SPEC(String spec) {
		return new Specifier(spec);
	}
	
	/**
	 * @param spec
	 * @param type
	 * @return
	 */
	public static TypeData TD(String spec, Type type) {
		return TD(SPEC(spec), null, type);
	}
	
	public static TypeData TD(Specifier specifiers, Declarator decl, Type type) {
		return new TypeData(specifiers, type, decl);
	}
	
	
	public static List<TypeData> modifySpec(List<TypeData> datas, SpecModifier specMod, TypeModifier typeModifier) {
		return modify(datas, specMod, d->d, typeModifier);
	}
	
	public static List<TypeData> modifyDecl(List<TypeData> datas, DeclModifier declMod, TypeModifier typeModifier) {
		return modify(datas, s->s, declMod, typeModifier);
	}
	
	/**
	 * Replace each data in {@code datas} with a new {@link TypeData} obtained by applying
	 * the specified modifier to data.
	 *  
	 * @param datas
	 * @param specMod
	 * @param declMod
	 * @param typeMod
	 * 
	 * @return list of modified data (newly created)
	 */
	public static List<TypeData> modify(List<TypeData> datas, SpecModifier specMod, DeclModifier declMod, TypeModifier typeMod) {
		List<TypeData> modified = datas.stream()
			.map(d -> modifdyData(d, specMod, declMod, typeMod))
			.collect(toList());
		return modified;
	}
	
	/**
	 * Create a new {@link TypeData} obtained by applying
	 * the specified modifier to {@code data}.
	 *  
	 * @param data
	 * @param specMod
	 * @param declMod
	 * @param typeMod
	 * 
	 * @return modified data (newly created)
	 */
	public static TypeData modifdyData(TypeData data, SpecModifier specMod, DeclModifier declMod, TypeModifier typeMod) {
		return TD(
			specMod.apply(data.getSpecifiers()),
			declMod.apply(data.getDeclarator()), 
			typeMod.apply(data.getType())
		);
	}
	
	@SafeVarargs
	public static <T> List<T> merge(List<T>... datas) {
		return Arrays.stream(datas).flatMap(l -> l.stream()).collect(toList());
	}
	
	public static <T> List<T> merge(Collection<List<T>> datas) {
		return datas.stream().flatMap(l -> l.stream()).collect(toList());
	}
	
	@SafeVarargs
	public static <T> List<T> merge(Stream<T>... streams) {
		return Arrays.stream(streams).flatMap(s -> s).collect(toList());
	}
	
	public static Object[][] toRaw(List<? extends Object> datas) {
		List<Object[]> list = datas.stream()
			.map(d -> new Object[]{d})
			.collect(toList());
		return list.toArray(new Object[list.size()][]);
	}
	
	/**
	 * @param datas
	 * @param number
	 * @param filterGccParsable
	 * @return A selection of {@code number} random elements from {@code datas}
	 * if {@code number} is positive, otherwise {@code datas} is passed through.
	 * The {@code datas} is first filtered to eliminate non-GCC parsable data, if {@code filterGccParsable} is set.
	 */
	public static List<TypeData> randomSelect(List<TypeData> datas, int number/*, boolean filterGccParsable*/) {
		if(number <= 0)
			return datas;
		
		Stream<TypeData> stream = datas.stream();
//		if(filterGccParsable)
//			stream = stream.filter(d -> isGccParsable(d));
		
		return stream
			.collect(collectingAndThen(toList(), l -> {Collections.shuffle(l); return l;}))
			.stream().limit(number)
			.collect(toList());
	}
	
	/**
	 * Requires gcc to be installed!
	 */
	public static boolean isGccParsable(TypeData data) {
		String cCode = data.getCCode();
		if(!cCode.endsWith(";"))
			cCode += ";";
		Path cFile = wrapInCFile(cCode);
		int res = SystemExec.shell("gcc -fsyntax-only " + cFile);
		return (res == SystemExec.successExitCode());
	}
	
	/**
	 * Writes the specified {@code code} to a newly created temporary .c file
	 * 
	 * @param code a valid c code
	 * @return the created .c file
	 */
	public static Path wrapInCFile(String code) {
		List<String> lines = Arrays.asList(code);
		try{
			File file = File.createTempFile("gecos_type_unitary_test", ".c");
			file.deleteOnExit();
			Path path = file.toPath();
			Files.write(path, lines, StandardOpenOption.WRITE);
			return path;
		} catch (Exception e){
			throw new RuntimeException("Error during generation of C file wrapper", e);
		}
	}

	
//TODO add these methods to a types Utility class--------------------------------------------------------------
	public static Type sttc(Type t) {
		t.setStorageClass(StorageClassSpecifiers.STATIC);
		return t;
	}
	
	public static Type ext(Type t) {
		t.setStorageClass(StorageClassSpecifiers.EXTERN);
		return t;
	}
	
	public static Type auto(Type t) {
		t.setStorageClass(StorageClassSpecifiers.AUTO);
		return t;
	}
	
	public static Type reg(Type t) {
		t.setStorageClass(StorageClassSpecifiers.REGISTER);
		return t;
	}
	
	public static Type cnst(Type t) {
		t.setConstant(true);
		return t;
	}
	
	public static Type voltile(Type t) {
		t.setVolatile(true);
		return t;
	}
	
	public static Type sign(IntegerType t) {
		t.setSignModifier(SignModifiers.SIGNED);
		return t;
	}
	
	public static Type unsign(IntegerType t) {
		t.setSignModifier(SignModifiers.UNSIGNED);
		return t;
	}
	
	public static Type restrct(Type t) {
		if(t instanceof PtrType)
			((PtrType) t).setRestrict(true);
		return t;
	}
	
	public static Type inline(Type t) {
		if(t instanceof FunctionType)
			((FunctionType) t).setInline(true);
		return t;
	}
	
}
