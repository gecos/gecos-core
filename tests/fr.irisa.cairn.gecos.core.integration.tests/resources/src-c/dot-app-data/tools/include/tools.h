#ifndef __TOOLS__
#define __TOOLS__

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>


#define MACRO_VAL(macro) 	#macro
#define TYPE2STR(type)		MACRO_VAL(type)
#define PRINT_ID(type)		(strcmp(TYPE2STR(type), "float")? "%d" : "%f")


//alloc2D(__TYPE__ ***in, int M, int N)
//#define alloc2D(in, M, N) { \
//	*in = (__TYPE__ **)malloc(M*sizeof(__TYPE__*)); \
//	int i; \
//	for (i = 0; i < M; i++) \
//		(*in)[i] = (__TYPE__*)malloc(N * sizeof(__TYPE__)); \
//}
#define alloc2D(in, M, N) { \
	*in = (__TYPE__ **)malloc(M*sizeof(__TYPE__*)); \
	__TYPE__ *data = (__TYPE__ *)malloc(M*N*sizeof(__TYPE__)); \
	int i; \
	for (i = 0; i < M; i++) \
		(*in)[i] = &(data[i*N]); \
}

//alloc2D(__TYPE__ **in, int M)
#define alloc1D(in, M) { \
	*in = (__TYPE__ *)malloc(M*sizeof(__TYPE__)); \
}

//init2D(__TYPE__ **array, int M, int N, int value)
#define init2D(array, M, N, value) { \
	int i,j; \
	for (i = 0; i < M; i++) \
		for (j = 0; j < N; j++) \
			array[i][j] = value; \
}

//readMat2D(__TYPE__ **array, const char *print_identifier, int M, int N, FILE *input)
//FIXME make sure the reading function is suitable for value's type (value out of range ?)
#define readMat2D(array, print_identifier, M, N, input) {\
	int i,j; \
	char tmp[128]; \
	char id[4]; \
	strncpy(id, print_identifier, 4); \
	for (i = 0; i < M; i++) \
		for (j = 0; j < N; j++) { \
			fgets(tmp, 128, input); \
			if(id[1] == 'd') \
				/*fscanf(input, "%d", &(array[i][j]));*/ \
				array[i][j] = (__TYPE__)strtoll(tmp, NULL, 10); \
			else \
				/*fscanf(input, "%f", &(array[i][j]));*/ \
				array[i][j] = (__TYPE__)atof(tmp); \
		} \
	}

//readMat1D(__TYPE__ *array, const char *print_identifier, int M, FILE *input)
//FIXME make sure the reading function is suitable for value's type (value out of range ?)
#define readMat1D(array, print_identifier, M, input) {\
	int i; \
	char tmp[128]; \
	char id[4]; \
	strncpy(id, print_identifier, 4); \
	for (i = 0; i < M; i++) { \
		fgets(tmp, 128, input); \
		if(id[1] == 'd') \
			/*fscanf(input, "%d", &(array[i][j]));*/ \
			array[i] = (__TYPE__)strtoll(tmp, NULL, 10); \
		else \
			/*fscanf(input, "%f", &(array[i][j]));*/ \
			array[i] = (__TYPE__)atof(tmp); \
	} \
}

//initFromFile2D(__TYPE__ ***array, const char *print_identifier, int *M, int *N, const char *file)
#define initFromFile2D(array, print_identifier, M, N, file) {\
	FILE *input_file = fopen(file, "r"); \
	if(input_file == NULL) { \
		printf("couldn't open input file '%s'!\n", file); \
		exit(-1); \
	} \
/*	fscanf(input_file, "%d", M); */ \
	char tmp[128]; \
	fgets(tmp, 128, input_file); \
	fgets(tmp, 128, input_file); \
	*M = atoi(tmp); \
/*	fscanf(input_file, "%d", N); */ \
	fgets(tmp, 128, input_file); \
	*N = atoi(tmp); \
	if(*M<0 || *N<0) { \
		printf("Array size negative!\n"); \
		exit(-1); \
	} \
	alloc2D(array , *M, *N); \
	readMat2D(*array, print_identifier, *M, *N, input_file); \
	fclose(input_file); \
}

//initFromFile1D(__TYPE__ **array, const char *print_identifier, int *M, const char *file)
#define initFromFile1DX(array, print_identifier, M, file) {\
	FILE *input_file = fopen(file, "r"); \
	if(input_file == NULL) { \
		printf("couldn't open input file '%s'!\n", file); \
		exit(-1); \
	} \
/*	fscanf(input_file, "%d", M); */ \
	char tmp[128]; \
	fgets(tmp, 128, input_file); \
	fgets(tmp, 128, input_file); \
	*M = atoi(tmp); \
	if(*M<0) { \
		printf("Array size negative!\n"); \
		exit(-1); \
	} \
	alloc1D(array , *M); \
	readMat1D(*array, print_identifier, *M, input_file); \
	fclose(input_file); \
}
#define initFromFile1D(array, print_identifier, M, file) {\
	FILE *input_file = fopen(file, "r"); \
	if(input_file == NULL) { \
		printf("couldn't open input file '%s'!\n", file); \
		exit(-1); \
	} \
/*	fscanf(input_file, "%d", M); */ \
	char tmp[128]; \
	fgets(tmp, 128, input_file); \
	*M = atoi(tmp); \
	if(*M<0) { \
		printf("Array size negative!\n"); \
		exit(-1); \
	} \
	alloc1D(array , *M); \
	readMat1D(*array, print_identifier, *M, input_file); \
	fclose(input_file); \
}

//copy2D(__TYPE__ **dst, __TYPE__ **src, int M, int N)
#define copy2D(dst, src, M, N) { \
	int i,j; \ \
	for (i = 0; i < M; i++) \
		for (j = 0; j < N; j++) \
			dst[i][j] = src[i][j]; \
}

//free2D(void **array, int M)
//#define free2D(array, M) { \
//	int i; \
//	for (i = 0; i < M; i++) \
//		free(array[i]); \
//	free(array); \
//}
#define free2D(array, M) { \
	free(array[0]); \
	free(array); \
}

//print2D(FILE *f, const char *print_identifier, __TYPE__ **array, int M, int N)
#define print2D(f, print_identifier, array, M, N) { \
	char id[4]; \
	strncpy(id, print_identifier, 4); \
	int i, j; \
	for (i = 0; i < M; i++) { \
		for (j = 0; j < N; j++) { \
			fprintf(f, id, array[i][j]); \
			fprintf(f, "\n"); \
		} \
		/*fprintf(f, " \n");*/ \
	} \
}

//print1D(FILE *f, const char *print_identifier, __TYPE__ *array, int M)
#define print1D(f, print_identifier, array, M) { \
	char id[4]; \
	strncpy(id, print_identifier, 4); \
	int i, j; \
	for (i = 0; i < M; i++) { \
		fprintf(f, id, array[i]); \
		fprintf(f, "\n"); \
	} \
}

//inputgen2D(__TYPE__ **array, int m, int n, int isfloat, int min, int max)
#define inputgen2D(array, m, n, isfloat, min, max) { \
	int i,j; \
	srand(time(NULL)); \
	for(i=0; i<m; i++) \
		for(j=0; j<n; j++) \
			array[i][j] = (__TYPE__)((rand() % (max + 1 - min)) + min); \
}


#endif //__TOOLS__
