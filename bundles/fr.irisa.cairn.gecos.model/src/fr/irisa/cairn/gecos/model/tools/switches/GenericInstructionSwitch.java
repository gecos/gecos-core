package fr.irisa.cairn.gecos.model.tools.switches;

import gecos.instrs.GenericInstruction;

import java.util.LinkedHashMap;
import java.util.Map;

public abstract class GenericInstructionSwitch<T> implements IGenericInstructionSwitch<T> {

	
	// FIXME : workaround to handle the lack of switch on Strings
	private final static Map<String, Integer> dispatch = new LinkedHashMap<String, Integer>();
	{
		dispatch.put("add", ADD);
		dispatch.put("sub", SUB);
		dispatch.put("mul", MUL);
		dispatch.put("shl", SHL);
		dispatch.put("shr", SHR);
		dispatch.put("div", DIV);
		dispatch.put("mod", MOD);
		dispatch.put("or", OR);
		dispatch.put("and", AND);
		dispatch.put("xor", XOR);
		dispatch.put("ne", NE);
		dispatch.put("eq", EQ);
		dispatch.put("gt", GT);
		dispatch.put("ge", GE);
		dispatch.put("geq", GE);
		dispatch.put("lt", LT);
		dispatch.put("le", LE);
		dispatch.put("leq", LE);
		dispatch.put("neg", NEG);
		dispatch.put("neq", NE);
		dispatch.put("lnot", LNOT);
		dispatch.put("mux", MUX);
	}

	public static final int ADD = 0;
	public static final int SUB = 1;
	public static final int MUL = 2;
	public static final int SHL = 3;
	public static final int SHR = 4;
	public static final int DIV = 5;
	public static final int MOD = 6;
	public static final int OR = 7;
	public static final int AND = 8;
	public static final int XOR = 9;
	public static final int ARRAY = 10;

	public static final int EQ = 12;
	public static final int GT = 13;
	public static final int GE = 14;
	public static final int LT = 15;
	public static final int LE = 16;

	public static final int NEG = 17;
	public static final int NE = 18;
	public static final int LNOT = 19;
	
	public static final int MUX = 20;
	
	public static final int UNKNOWN = -1;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.cairn.gecos.model.tools.switches.IGenericInstructionSwitch#caseADD
	 * (gecos.instrs.GenericInstruction)
	 */
	public T caseADD(GenericInstruction g) {
		return caseUnknownGeneric(g);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.cairn.gecos.model.tools.switches.IGenericInstructionSwitch#caseSUB
	 * (gecos.instrs.GenericInstruction)
	 */
	public T caseSUB(GenericInstruction g) {
		return caseUnknownGeneric(g);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.cairn.gecos.model.tools.switches.IGenericInstructionSwitch#caseMUL
	 * (gecos.instrs.GenericInstruction)
	 */
	public T caseMUL(GenericInstruction g) {
		return caseUnknownGeneric(g);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.cairn.gecos.model.tools.switches.IGenericInstructionSwitch#caseSHL
	 * (gecos.instrs.GenericInstruction)
	 */
	public T caseSHL(GenericInstruction g) {
		return caseUnknownGeneric(g);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.cairn.gecos.model.tools.switches.IGenericInstructionSwitch#caseSHR
	 * (gecos.instrs.GenericInstruction)
	 */
	public T caseSHR(GenericInstruction g) {
		return caseUnknownGeneric(g);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.cairn.gecos.model.tools.switches.IGenericInstructionSwitch#caseDIV
	 * (gecos.instrs.GenericInstruction)
	 */
	public T caseDIV(GenericInstruction g) {
		return caseUnknownGeneric(g);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.cairn.gecos.model.tools.switches.IGenericInstructionSwitch#caseMOD
	 * (gecos.instrs.GenericInstruction)
	 */
	public T caseMOD(GenericInstruction g) {
		return caseUnknownGeneric(g);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.cairn.gecos.model.tools.switches.IGenericInstructionSwitch#caseOR
	 * (gecos.instrs.GenericInstruction)
	 */
	public T caseOR(GenericInstruction g) {
		return caseUnknownGeneric(g);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.cairn.gecos.model.tools.switches.IGenericInstructionSwitch#caseAND
	 * (gecos.instrs.GenericInstruction)
	 */
	public T caseAND(GenericInstruction g) {
		return caseUnknownGeneric(g);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.cairn.gecos.model.tools.switches.IGenericInstructionSwitch#caseXOR
	 * (gecos.instrs.GenericInstruction)
	 */
	public T caseXOR(GenericInstruction g) {
		return caseUnknownGeneric(g);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.cairn.gecos.model.tools.switches.IGenericInstructionSwitch#caseARRAY
	 * (gecos.instrs.GenericInstruction)
	 */
	public T caseARRAY(GenericInstruction g) {
		return caseUnknownGeneric(g);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.cairn.gecos.model.tools.switches.IGenericInstructionSwitch#caseEQ
	 * (gecos.instrs.GenericInstruction)
	 */
	public T caseEQ(GenericInstruction g) {
		return caseUnknownGeneric(g);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.cairn.gecos.model.tools.switches.IGenericInstructionSwitch#caseGT
	 * (gecos.instrs.GenericInstruction)
	 */
	public T caseGT(GenericInstruction g) {
		return caseUnknownGeneric(g);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.cairn.gecos.model.tools.switches.IGenericInstructionSwitch#caseGE
	 * (gecos.instrs.GenericInstruction)
	 */
	public T caseGE(GenericInstruction g) {
		return caseUnknownGeneric(g);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.cairn.gecos.model.tools.switches.IGenericInstructionSwitch#caseLT
	 * (gecos.instrs.GenericInstruction)
	 */
	public T caseLT(GenericInstruction g) {
		return caseUnknownGeneric(g);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.cairn.gecos.model.tools.switches.IGenericInstructionSwitch#caseLE
	 * (gecos.instrs.GenericInstruction)
	 */
	public T caseLE(GenericInstruction g) {
		return caseUnknownGeneric(g);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.cairn.gecos.model.tools.switches.IGenericInstructionSwitch#caseNE
	 * (gecos.instrs.GenericInstruction)
	 */
	public T caseNEG(GenericInstruction g) {
		return caseUnknownGeneric(g);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.cairn.gecos.model.tools.switches.IGenericInstructionSwitch#caseNE
	 * (gecos.instrs.GenericInstruction)
	 */
	public T caseNE(GenericInstruction g) {
		return caseUnknownGeneric(g);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.cairn.gecos.model.tools.switches.IGenericInstructionSwitch#caseLE
	 * (gecos.instrs.GenericInstruction)
	 */
	public T caseLNOT(GenericInstruction g) {
		return caseUnknownGeneric(g);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.cairn.gecos.model.tools.switches.IGenericInstructionSwitch#caseLE
	 * (gecos.instrs.GenericInstruction)
	 */
	public T caseMUX(GenericInstruction g) {
		return caseUnknownGeneric(g);
	}

	public abstract T caseUnknownGeneric(GenericInstruction g);

	public T doSwitch(GenericInstruction inst) {
		if (dispatch.containsKey(inst.getName())) {
			int code = dispatch.get(inst.getName());
			switch (code) {
			case ADD:
				return caseADD(inst);
			case MUL:
				return caseMUL(inst);
			case SUB:
				return caseSUB(inst);
			case DIV:
				return caseDIV(inst);
			case MOD:
				return caseMOD(inst);
			case SHR:
				return caseSHR(inst);
			case SHL:
				return caseSHL(inst);
			case OR:
				return caseOR(inst);
			case AND:
				return caseAND(inst);
			case XOR:
				return caseXOR(inst);
			case ARRAY:
				return caseARRAY(inst);
			case EQ:
				return caseEQ(inst);
			case GT:
				return caseGT(inst);
			case GE:
				return caseGE(inst);
			case LT:
				return caseLT(inst);
			case LE:
				return caseLE(inst);
			case NEG:
				return caseNEG(inst);
			case NE:
				return caseNE(inst);
			case LNOT:
				return caseLNOT(inst);
			case MUX:
				return caseMUX(inst);
			default:
				throw new UnsupportedOperationException(
						"Unsupported instruction : " + inst.getName());
			}
		} else {
			return caseUnknownGeneric(inst);
		}

	}
}
