/**
 */
package gecos.instrs;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Range Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.RangeInstruction#getLow <em>Low</em>}</li>
 *   <li>{@link gecos.instrs.RangeInstruction#getHigh <em>High</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getRangeInstruction()
 * @model
 * @generated
 */
public interface RangeInstruction extends ExprComponent {
	/**
	 * Returns the value of the '<em><b>Low</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Low</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Low</em>' attribute.
	 * @see #setLow(long)
	 * @see gecos.instrs.InstrsPackage#getRangeInstruction_Low()
	 * @model unique="false" dataType="gecos.instrs.long"
	 * @generated
	 */
	long getLow();

	/**
	 * Sets the value of the '{@link gecos.instrs.RangeInstruction#getLow <em>Low</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Low</em>' attribute.
	 * @see #getLow()
	 * @generated
	 */
	void setLow(long value);

	/**
	 * Returns the value of the '<em><b>High</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>High</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>High</em>' attribute.
	 * @see #setHigh(long)
	 * @see gecos.instrs.InstrsPackage#getRangeInstruction_High()
	 * @model unique="false" dataType="gecos.instrs.long"
	 * @generated
	 */
	long getHigh();

	/**
	 * Sets the value of the '{@link gecos.instrs.RangeInstruction#getHigh <em>High</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>High</em>' attribute.
	 * @see #getHigh()
	 * @generated
	 */
	void setHigh(long value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitRangeInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _elvis = null;\n&lt;%gecos.instrs.Instruction%&gt; _expr = this.getExpr();\n&lt;%java.lang.String%&gt; _string = null;\nif (_expr!=null)\n{\n\t_string=_expr.toString();\n}\nif (_string != null)\n{\n\t_elvis = _string;\n} else\n{\n\t_elvis = \"?\";\n}\n&lt;%java.lang.String%&gt; _plus = (_elvis + \"{\");\nlong _high = this.getHigh();\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + &lt;%java.lang.Long%&gt;.valueOf(_high));\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + \":\");\nlong _low = this.getLow();\n&lt;%java.lang.String%&gt; _plus_3 = (_plus_2 + &lt;%java.lang.Long%&gt;.valueOf(_low));\nreturn (_plus_3 + \"}\");'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" objUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((obj instanceof &lt;%gecos.instrs.RangeInstruction%&gt;))\n{\n\treturn ((super.equals(obj) &amp;&amp; (((&lt;%gecos.instrs.RangeInstruction%&gt;)obj).getLow() == this.getLow())) &amp;&amp; (((&lt;%gecos.instrs.RangeInstruction%&gt;)obj).getHigh() == this.getHigh()));\n}\nreturn false;'"
	 * @generated
	 */
	boolean equals(Object obj);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.RangeInstruction%&gt;))\n{\n\treturn (((this.getLow() == ((&lt;%gecos.instrs.RangeInstruction%&gt;)instr).getLow()) &amp;&amp; (this.getHigh() == ((&lt;%gecos.instrs.RangeInstruction%&gt;)instr).getHigh())) &amp;&amp; super.isSame(instr));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // RangeInstruction
