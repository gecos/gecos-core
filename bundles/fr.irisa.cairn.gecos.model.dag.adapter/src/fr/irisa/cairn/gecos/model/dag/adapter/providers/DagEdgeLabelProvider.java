package fr.irisa.cairn.gecos.model.dag.adapter.providers;

import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGEdgeAdapter;
import fr.irisa.cairn.graph.implement.providers.EdgeLabelProvider;
import gecos.dag.DAGControlEdge;
import gecos.dag.DAGEdge;

public class DagEdgeLabelProvider extends EdgeLabelProvider<IDAGEdgeAdapter>{
	
	public static boolean SHOW_EDGE_LABEL = true;
	public static boolean SHOW_CONTROL_EDGE_TYPE = true;
	
	@Override
	public String getEdgeLabel(IDAGEdgeAdapter e) {
//		return super.getEdgeLabel(e);
		if(SHOW_EDGE_LABEL) {
			StringBuffer buf = new StringBuffer();
			DAGEdge edge = e.getAdaptedEdge();
			if(SHOW_CONTROL_EDGE_TYPE)
				if(edge instanceof DAGControlEdge)
					buf.append(((DAGControlEdge)edge).getType().toString()).append(" ");
			
			buf.append(edge.getSourceNode().getOutputs().indexOf(edge.getSrc()));
			buf.append(">").append(edge.getSinkNode().getInputs().indexOf(edge.getSink()));
			
			return buf.toString();
		}
		return "";
	}
}
