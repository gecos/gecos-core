package fr.irisa.cairn.gecos.core.transforms.tests;

import org.junit.Test;

import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.core.integration.tests.utils.DotAppDirData;
import fr.irisa.cairn.gecos.core.testframework.impl.GSProjectVersion;
import fr.irisa.cairn.gecos.core.testframework.impl.GecosS2STestTemplate;
import fr.irisa.cairn.gecos.model.transforms.commonsubexpressionelimination.GlobalCommonSubExpressionEliminationModule;
import fr.irisa.cairn.gecos.model.transforms.commonsubexpressionelimination.LocalCommonSubExpressionEliminationModule;
import fr.irisa.cairn.gecos.testframework.data.ICxxProjectData;
import fr.irisa.cairn.gecos.testframework.dataprovider.AddIntParams;
import fr.irisa.cairn.gecos.testframework.dataprovider.DataFromPathProvider;
import fr.irisa.cairn.gecos.testframework.dataprovider.ResourcesLocation;

public class ComExprElimIT extends GecosS2STestTemplate<GSProjectVersion> {
	
	private boolean function;
	private boolean pointer;
	
	@Override
	protected void configure() {
		super.configure();
		
		registerTestFlow(DotAppDirData.class, DotAppDirData.defaultDotAppDirDataTestFlow(DotAppDirData::defaultMakeCompileTarget,
				v -> DotAppDirData.defaultMakeRunTarget(v) + " OPTION_F=" + (function?1:0) + " OPTION_P=" + (pointer?1:0)));
	}

	
	@Test
	@UseDataProvider(location=DataFromPathProvider.class, value=DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation(value = "resources/src-c/dot-app-data/misc", dataClasses = DotAppDirData.class)
	@ResourcesLocation(value = "resources/src-c/dot-app-data/cse", dataClasses = DotAppDirData.class)
//	@ResourcesLocation(value = "resources/src-c/dot-app-data/idfix", dataClasses = DotAppDirData.class) //FIXME: substituting array subscripts even if not integers!
	@AddIntParams({0, 1}) // option_function
	@AddIntParams({0, 1}) // option_pointer
	public void localComSubExprFP (ICxxProjectData d, int option_function, int option_pointer) {
		function = option_function != 0;
		pointer = option_pointer != 0;
		runTest(d, v -> new LocalCommonSubExpressionEliminationModule(v.getProject(), 
				function, pointer).compute());
	}
	
	@Test
	@UseDataProvider(location=DataFromPathProvider.class, value=DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation(value = "resources/src-c/dot-app-data/misc", dataClasses = DotAppDirData.class)
	@ResourcesLocation(value = "resources/src-c/dot-app-data/cse", dataClasses = DotAppDirData.class)
//	@ResourcesLocation(value = "resources/src-c/dot-app-data/idfix", dataClasses = DotAppDirData.class) //FIXME: substituting array subscripts even if not integers! 
	@AddIntParams({0, 1})
	public void globalComExprElimP (ICxxProjectData d, int option_pointer) {
		pointer = option_pointer != 0;
		function = true;
		runTest(d, v -> new GlobalCommonSubExpressionEliminationModule(v.getProject(),
				function, pointer).compute());
	}
	
}
