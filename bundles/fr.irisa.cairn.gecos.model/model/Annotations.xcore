@GenModel(modelName="Gecos", modelPluginClass="",
	creationCommands="false", creationIcons="false", 
	interfaceNamePattern="",
	importerID="org.eclipse.emf.importer.ecore"
//	, editDirectory="/fr.irisa.cairn.gecos.model.edit/src"
//	, editorDirectory="/fr.irisa.cairn.gecos.model.editor/src"
//	, editorPluginID="fr.irisa.cairn.gecos.model.editor"
, operationReflection="false"
)
@Ecore(nsURI="http://www.gecos.org/annotations")
package gecos.annotations

import org.eclipse.emf.ecore.EObject
import gecos.core.GecosNode
import gecos.core.Symbol
import gecos.instrs.Instruction
import fr.irisa.cairn.gecos.model.tools.utils.GecosCopier

//!!! This is needed as of the current versions of xcore/xtext
// For some reason when using the default String (i.e. without the following)
// the corresponding EPackage attributes/.. are resolve to type java.lang.Object
// instead of java.lang.String.
// This causes a problem for Tom mapping generation for instance.
// The following fixes this problem. 
type String wraps String


abstract class IAnnotation extends GecosNode {
	op AnnotatedElement getAnnotatedElement() {
		if(eContainer !== null) {
			val containe = eContainer.eContainer
			if(containe instanceof AnnotatedElement)
				return containe
		}
		return null
	}
}

enum AnnotationKeys {
	PRAGMA_ANNOTATION_KEY as "#pragma"
	COMMENT_ANNOTATION_KEY as "#comment"
	SYMBOL_DEF_ANNOTATION_KEY as "SymbolDefinitionAnnotation"
	FILE_LOCATION_ANNOTATION_KEY as "gecos.core::FILE_LOCATION"
}

class StringAnnotation extends IAnnotation {
	String content
	
	op String toString() {
		content
	}
}

class PragmaAnnotation extends IAnnotation {
	unique String[] content
	
	op String toString() {
		content.join("#pragma {", ",", "}", ["\"" + it + "\""])
	}
	
	op <T extends Symbol> T copy() {
		val gecosCopier = new GecosCopier
		val symbol = gecosCopier.copy(this) as T
		gecosCopier.copyReferences
		return symbol
	}
}

class CommentAnnotation extends StringAnnotation {
}

class ExtendedAnnotation extends IAnnotation {
	contains EObject[] fields 
	refers EObject[] refs
}

enum LivenessInfo { def, used, alive}

class LivenessAnnotation extends IAnnotation {
	refers Symbol[] refs
	
}

class SymbolDefinitionAnnotation extends IAnnotation {
	refers Instruction symbolDefinition
}

class IASTAnnotation extends IAnnotation {
//	contains volatile EObject iastNode
	local refers EObject iastNode
}

class FileLocationAnnotation extends IAnnotation {
	String filename
	int startingLine
	int endingLine
	int nodeOffset
	int nodeLength
}

class StringToIAnnotationMap wraps java.util.Map.Entry {
	String key
	contains IAnnotation value
}

abstract class AnnotatedElement extends GecosNode {
	contains StringToIAnnotationMap[] annotations 
	
	op IAnnotation getAnnotation(String key) {
		return annotations.get(key);
	}
	
	op void setAnnotation(String key , IAnnotation annot) {
		annotations.put(key,annot);
	}

	/*
	 * @return the {@link PragmaAnnotation} attached to this object,
	 * {@code null} if not found.
	 */
	op PragmaAnnotation getPragma() {
		val annot = getAnnotation(AnnotationKeys.PRAGMA_ANNOTATION_KEY.literal)
		if (annot instanceof PragmaAnnotation)
			return annot
		return null
	}
	
	/*
	 * @return the {@link FileLocationAnnotation} attached to this object,
	 * {@code null} if not found.
	 */
	op FileLocationAnnotation getFileLocation() {
		val annot = getAnnotation(AnnotationKeys.FILE_LOCATION_ANNOTATION_KEY.literal)
		if (annot instanceof FileLocationAnnotation)
			return annot
		return null
	}
}

