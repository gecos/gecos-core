/**
 */
package gecos.dag.impl;

import gecos.dag.DAGEdge;
import gecos.dag.DAGNode;
import gecos.dag.DAGOutPort;
import gecos.dag.DagPackage;
import gecos.dag.DagVisitor;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DAG Out Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.impl.DAGOutPortImpl#getSinks <em>Sinks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DAGOutPortImpl extends DAGPortImpl implements DAGOutPort {
	/**
	 * The cached value of the '{@link #getSinks() <em>Sinks</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSinks()
	 * @generated
	 * @ordered
	 */
	protected EList<DAGEdge> sinks;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DAGOutPortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DagPackage.Literals.DAG_OUT_PORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGEdge> getSinks() {
		if (sinks == null) {
			sinks = new EObjectWithInverseResolvingEList<DAGEdge>(DAGEdge.class, this, DagPackage.DAG_OUT_PORT__SINKS, DagPackage.DAG_EDGE__SRC);
		}
		return sinks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final DagVisitor visitor) {
		visitor.visitDAGOutPort(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		DAGNode _parent = this.getParent();
		boolean _tripleNotEquals = (_parent != null);
		if (_tripleNotEquals) {
			DAGNode _parent_1 = this.getParent();
			String _plus = (_parent_1 + ".Out[");
			int _indexOf = this.getParent().getOutputs().indexOf(this);
			String _plus_1 = (_plus + Integer.valueOf(_indexOf));
			return (_plus_1 + "]");
		}
		else {
			int _ident = this.getIdent();
			String _plus_2 = ("?.Out[" + Integer.valueOf(_ident));
			return (_plus_2 + "]");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DagPackage.DAG_OUT_PORT__SINKS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSinks()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DagPackage.DAG_OUT_PORT__SINKS:
				return ((InternalEList<?>)getSinks()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DagPackage.DAG_OUT_PORT__SINKS:
				return getSinks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DagPackage.DAG_OUT_PORT__SINKS:
				getSinks().clear();
				getSinks().addAll((Collection<? extends DAGEdge>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DagPackage.DAG_OUT_PORT__SINKS:
				getSinks().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DagPackage.DAG_OUT_PORT__SINKS:
				return sinks != null && !sinks.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DAGOutPortImpl
