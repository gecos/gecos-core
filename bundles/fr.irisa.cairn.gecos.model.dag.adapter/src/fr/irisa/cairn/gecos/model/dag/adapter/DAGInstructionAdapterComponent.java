 package fr.irisa.cairn.gecos.model.dag.adapter;

import fr.irisa.cairn.gecos.model.dag.adapter.builder.DagAdapterSynchronizedFactory;
import fr.irisa.cairn.gecos.model.dag.adapter.module.AdapterBuilderModule;
import fr.irisa.cairn.gecos.model.dag.adapter.module.DagAdapterSynchronizedBuilderModule;
import fr.irisa.cairn.gecos.model.dag.adapter.module.DagAdapterSynchronizedFactoryModule;
import fr.irisa.cairn.gecos.model.dag.adapter.module.DagAnalysisProvidersModule;
import fr.irisa.cairn.gecos.model.dag.adapter.module.DagLabelProviderModule;
import fr.irisa.cairn.graph.guice.analysis.IGraphAnalysisService;
import fr.irisa.cairn.graph.guice.export.IGraphExportService;
import fr.irisa.cairn.graph.guice.modules.providersbinding.AnalysisProviderModule;
import fr.irisa.cairn.graph.guice.modules.providersbinding.LabelProviderModule;
import gecos.dag.DAGInstruction;

/**
 * DAG adapter component. Install DAG dedicated default providers and replace the builder module by
 * a {@link DagAdapterSynchronizedBuilderModule}.
 * 
 * @author Antoine Floc'h - Initial contribution and API
 * 
 */
public class DAGInstructionAdapterComponent extends AbstractDAGInstructionAdapterComponent {
	public final static DAGInstructionAdapterComponent INSTANCE = new DAGInstructionAdapterComponent();

	protected DAGInstructionAdapterComponent() {
		super();
	}

	@Override
	public void configure() {
		super.configure();
		replaceModule(AdapterBuilderModule.class,new DagAdapterSynchronizedBuilderModule());
		replaceModule(LabelProviderModule.class, new DagLabelProviderModule());
		replaceModule(AnalysisProviderModule.class,new DagAnalysisProvidersModule());
		installModule(new DagAdapterSynchronizedFactoryModule());
	}

	/**
	 * Get the injected factory service which maintain consistency between the
	 * adapter and the {@link DAGInstruction}.
	 * 
	 * @return
	 */
	public DagAdapterSynchronizedFactory getSynchronizedAdapterFactory() {
		return getService(DagAdapterSynchronizedFactory.class);
	}
	
	public IGraphAnalysisService getGraphAnalysisService() {
		return this.getService(IGraphAnalysisService.class);
	}

	public IGraphExportService getGraphExportService() {
		return this.getService(IGraphExportService.class);
	}
}
