package fr.irisa.cairn.gecos.model.transforms.others;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.transforms.tools.AbstractInstructionTransformation;
import fr.irisa.cairn.gecos.model.transforms.tools.GenericInstructionTransformation;
import gecos.blocks.Block;
import gecos.core.Procedure;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;

@SuppressWarnings({"all"})
public class MultiplicationDistribution extends GenericInstructionTransformation {
	%include { sl.tom }

	%include { gecos_common.tom }
	%include { gecos_terminals.tom }
	%include { gecos_basic.tom }
	%include { gecos_arithmetic.tom }
	%include { gecos_logical.tom }
	%include { gecos_compare.tom }
 
	protected Instruction apply(Instruction instruction) throws VisitFailure{
		Instruction adapter = `InnermostId(Distributivity()).visitLight(instruction, tom.mapping.GenericIntrospector.INSTANCE);
		return adapter;
	}
 
	%strategy Distributivity() extends Identity() {
		visit Inst {
				mul(InstL(add(InstL(x, y)), z)) -> { return `add(InstL(mul(InstL(x, z)), mul(InstL(y, z)))); }
				mul(InstL(x, add(InstL(y, z)))) -> { return `add(InstL(mul(InstL(x, y)), mul(InstL(x, z)))); }
		} 
	}

	public static class GecosDistributePass extends AbstractTomTransformationPass {

		public GecosDistributePass(Block block) {
			super(block);
		}
		public GecosDistributePass(Procedure p) {
			super(p);
		}

		@Override
		public void compute() {
			super.compute();
		}

		@Override
		protected AbstractInstructionTransformation getTransformation() {
			return new MultiplicationDistribution();
		}
	}
}