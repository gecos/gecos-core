/**
 */
package gecos.core;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISymbol Use</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.core.CorePackage#getISymbolUse()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ISymbolUse extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 * @generated
	 */
	Symbol getUsedSymbol();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model newSymbolUnique="false"
	 * @generated
	 */
	void setSymbol(Symbol newSymbol);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model originalSymbolUnique="false" newSymbolUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.core.Symbol%&gt; _usedSymbol = this.getUsedSymbol();\nboolean _tripleEquals = (_usedSymbol == originalSymbol);\nif (_tripleEquals)\n{\n\tthis.setSymbol(newSymbol);\n}\nelse\n{\n\tthrow new &lt;%java.lang.IllegalArgumentException%&gt;(\"Given Symbol is not used.\");\n}'"
	 * @generated
	 */
	void updateSymbol(Symbol originalSymbol, Symbol newSymbol);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.core.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getUsedSymbol().getName();'"
	 * @generated
	 */
	String getSymbolName();

} // ISymbolUse
