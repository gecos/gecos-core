/**
 */
package gecos.instrs.impl;

import gecos.instrs.CondInstruction;
import gecos.instrs.InstrsPackage;
import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cond Instruction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.impl.CondInstructionImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link gecos.instrs.impl.CondInstructionImpl#getCond <em>Cond</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CondInstructionImpl extends BranchInstructionImpl implements CondInstruction {
	/**
	 * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected String label = LABEL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCond() <em>Cond</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCond()
	 * @generated
	 * @ordered
	 */
	protected Instruction cond;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CondInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstrsPackage.eINSTANCE.getCondInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabel(String newLabel) {
		String oldLabel = label;
		label = newLabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InstrsPackage.COND_INSTRUCTION__LABEL, oldLabel, label));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getCond() {
		return cond;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCond(Instruction newCond, NotificationChain msgs) {
		Instruction oldCond = cond;
		cond = newCond;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, InstrsPackage.COND_INSTRUCTION__COND, oldCond, newCond);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCond(Instruction newCond) {
		if (newCond != cond) {
			NotificationChain msgs = null;
			if (cond != null)
				msgs = ((InternalEObject)cond).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - InstrsPackage.COND_INSTRUCTION__COND, null, msgs);
			if (newCond != null)
				msgs = ((InternalEObject)newCond).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - InstrsPackage.COND_INSTRUCTION__COND, null, msgs);
			msgs = basicSetCond(newCond, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InstrsPackage.COND_INSTRUCTION__COND, newCond, newCond));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instruction> listChildren() {
		return ECollections.<Instruction>unmodifiableEList(ECollections.<Instruction>asEList(this.getCond()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final InstrsVisitor visitor) {
		visitor.visitCondInstruction(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean equals(final Object obj) {
		boolean sameBranchType = false;
		if ((obj instanceof CondInstruction)) {
			sameBranchType = true;
		}
		return (super.equals(obj) && sameBranchType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _elvis = null;
		Instruction _cond = this.getCond();
		String _string = null;
		if (_cond!=null) {
			_string=_cond.toString();
		}
		if (_string != null) {
			_elvis = _string;
		} else {
			_elvis = "";
		}
		return _elvis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSame(final Instruction instr) {
		if ((instr instanceof CondInstruction)) {
			boolean isSameLabel = false;
			if (((((CondInstruction)instr).getLabel() == null) && (this.getLabel() == null))) {
				isSameLabel = true;
			}
			else {
				String _label = ((CondInstruction)instr).getLabel();
				boolean _tripleEquals = (_label == null);
				String _label_1 = this.getLabel();
				boolean _tripleEquals_1 = (_label_1 == null);
				boolean _xor = (_tripleEquals ^ _tripleEquals_1);
				if (_xor) {
					return false;
				}
				else {
					isSameLabel = ((CondInstruction)instr).getLabel().equals(this.getLabel());
				}
			}
			boolean isSameCond = false;
			if (((((CondInstruction)instr).getCond() == null) && (this.getCond() == null))) {
				isSameCond = true;
			}
			else {
				Instruction _cond = ((CondInstruction)instr).getCond();
				boolean _tripleEquals_2 = (_cond == null);
				Instruction _cond_1 = this.getCond();
				boolean _tripleEquals_3 = (_cond_1 == null);
				boolean _xor_1 = (_tripleEquals_2 ^ _tripleEquals_3);
				if (_xor_1) {
					return false;
				}
				else {
					isSameCond = ((CondInstruction)instr).getCond().isSame(this.getCond());
				}
			}
			return ((super.isSame(instr) && isSameCond) && isSameLabel);
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InstrsPackage.COND_INSTRUCTION__COND:
				return basicSetCond(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InstrsPackage.COND_INSTRUCTION__LABEL:
				return getLabel();
			case InstrsPackage.COND_INSTRUCTION__COND:
				return getCond();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InstrsPackage.COND_INSTRUCTION__LABEL:
				setLabel((String)newValue);
				return;
			case InstrsPackage.COND_INSTRUCTION__COND:
				setCond((Instruction)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InstrsPackage.COND_INSTRUCTION__LABEL:
				setLabel(LABEL_EDEFAULT);
				return;
			case InstrsPackage.COND_INSTRUCTION__COND:
				setCond((Instruction)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InstrsPackage.COND_INSTRUCTION__LABEL:
				return LABEL_EDEFAULT == null ? label != null : !LABEL_EDEFAULT.equals(label);
			case InstrsPackage.COND_INSTRUCTION__COND:
				return cond != null;
		}
		return super.eIsSet(featureID);
	}

} //CondInstructionImpl
