/**
 */
package gecos.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visitable</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.core.CorePackage#getCoreVisitable()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface CoreVisitable extends GecosNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 * @generated
	 */
	void accept(CoreVisitor visitor);

} // CoreVisitable
