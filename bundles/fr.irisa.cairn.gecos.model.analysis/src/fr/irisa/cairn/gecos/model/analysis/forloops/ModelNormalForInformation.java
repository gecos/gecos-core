/**
 * experimental
 */

package fr.irisa.cairn.gecos.model.analysis.forloops;

import fr.irisa.cairn.gecos.model.tools.switches.DefaultInstructionSwitch;
import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch;
import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDefaultVisitor;
import gecos.annotations.IAnnotation;
import gecos.annotations.PragmaAnnotation;
import gecos.annotations.util.AnnotationsSwitch;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.ForBlock;
import gecos.core.Symbol;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BreakInstruction;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.CondInstruction;
import gecos.instrs.ContinueInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * TODO : - make it work with DAGInstruction in step / init / test<br/>
 * note : a loop is valid for total unroll only if it is valid for partial
 * unroll<br/>
 * -
 * 
 * @author amorvan
 * 
 */
public class ModelNormalForInformation {
	
	public final static String GECOS_PRAGMA_PREFIX = "GCS_";
	public static enum Decision {UNROLLALL,UNROLLPARTIAL,NOUNROLL};

	private static final String PREFIX = "[NormalForInfos] ";
	private static final boolean debug = false;
	private static void debug(Object o) { if (debug) System.out.print(PREFIX + o.toString()); }
	private static void debugln(Object o) { debug(o.toString() + "\n"); }

	private static final List<String> stepOperators = Arrays.asList(new String [] {"mul","add","sub","div"});
	private static Map<String,String> reverseOperators; 
	
	{
		reverseOperators = new LinkedHashMap<String, String>();
		reverseOperators.put("eq", "neq");
		reverseOperators.put("neq", "eq");
		reverseOperators.put("lt", "ge");
		reverseOperators.put("le", "gt");
		reverseOperators.put("gt", "le");
		reverseOperators.put("ge", "lt");
	}

	private ForBlock forBlock;

	private int threshold = -1;
	private long unrollfactor = -1;
	private boolean pragmas = false;

	private boolean valid_unrollAll = false;
	private boolean valid_partialUnroll = false;

	private Decision decision = Decision.NOUNROLL;
	
	private boolean gotInit = false;
	private Symbol index = null;
	private Instruction init = null;

	private boolean gotTest = false;
	private String opTest = null;
	private Instruction stop = null;

	private boolean gotStep = false;
	private String opStep = null;
	private Instruction step = null;

	private long iterationCount = -1;

	private boolean hasPragma = false;
	private PragmaAnnotation annots = null;
	private boolean hasFactor = false;
	private boolean hasForce = false;
	private boolean force = false;
	private boolean unrollAll = false;

	private boolean containsBreakorContinue = false;
	/**
	 * Custom constructor for loop unroller. Do not use, except if you know what you do.
	 * @param forBlock
	 *            : Block to gets Infos
	 * @param th
	 *            : threshold (default = -1)
	 * @param f
	 *            : unrollFactor (default = -1)
	 * @param p
	 *            : care about pragmas (default = true)
	 */
	public ModelNormalForInformation(ForBlock forBlock, int th, int f, boolean p) {
		this.forBlock = forBlock;

		this.valid_unrollAll = false;
		this.valid_partialUnroll = false;

		this.threshold = th;
		this.unrollfactor = f;
		this.pragmas = p;

		this.hasPragma = false;
		this.annots = null;
		this.hasFactor = false;
		this.hasForce = false;
		this.force = false;
		this.unrollAll = false;

		this.compute();
	}

	/**
	 * Default constructor. 
	 * @param forBlock
	 */
	public ModelNormalForInformation(ForBlock forBlock) {
		// defaults values from ModelForUnroller
		this(forBlock, -1, -1, false);
	}

	/**
	 * process analysis of the loop
	 */
	private void compute() {
		this.processPragmas(this.forBlock);

		try {
			this.processInit(this.forBlock.getInitBlock());
			this.gotInit = true;
			this.processTest(this.forBlock.getTestBlock());
			this.gotTest = true;
			this.processStep(this.forBlock.getStepBlock());
			this.gotStep = true;
			this.computeIterationCount();
			
		} catch (Exception e) {
			debug(e.getMessage());
		}

		this.makeUnrollDecision();
	}

	/**
	 * Get init value
	 * @param b
	 */
	private void processInit(Block b) {
		BasicBlock init;
		try {
			init = (BasicBlock) b;
		} catch (Exception e) {
			throw new RuntimeException("Error : init block is not a basic block");
		}
		
		//init should be in the following form 
		// index = init_expression
		try {
			SetInstruction set = (SetInstruction) init.getInstruction(0);
			this.index = ((SymbolInstruction)set.getDest()).getSymbol();
			this.init = set.getSource();
		} catch (Exception e) {
			//Catch cast exception
			throw new RuntimeException ("InitCannot determine loop's initialization : bad loop syntax.");
		}
	}

	/**
	 * Get test value, and test operator
	 * @param b
	 */
	private void processTest(Block b) {
		BasicBlock test;
		try {
			test = (BasicBlock) b;
		} catch (Exception e) {
			throw new RuntimeException("Error : test block is not a basic block");
		}
		
		//test should be in the following form
		// index [testOp] test_expression
		try {
			CondInstruction cond = (CondInstruction) test.getInstruction(0);
			GenericInstruction g = (GenericInstruction) cond.getCond();
			if (g.getOperands().size() != 2) 
				throw new RuntimeException ("Cannot determine loop's test values : test is not a 2 operand instruction.");
			this.opTest = g.getName();
			if (!reverseOperators.keySet().contains(this.opTest)) 
				throw new RuntimeException ("Cannot determine loop's test values : unknown test operand : {"+this.opTest+"}.");
			if ((((SymbolInstruction)g.getOperand(0)).getSymbol()) != (this.getIterationIndex())) {
				throw new RuntimeException ("Cannot determine loop's test values : first operand is not index symbol.");
			}
			this.stop = g.getOperand(1);
		} catch (Exception e) {
			//Catch cast exception
			throw new RuntimeException ("testCannot determine loop's initialization : bad loop syntax.");
		}
	}

	/**
	 * Get step value (or expression), step operator
	 * @param b
	 */
	private void processStep(Block b) {
		BasicBlock step;
		try {
			step = (BasicBlock) b;
		} catch (Exception e) {
			throw new RuntimeException("Error : step block is not a basic block");
		}
		
		SetInstruction inst = (SetInstruction)step.getInstruction(0);
		
		//step may have 3 forms :
		// index [opstep0]
		// index [opstep1] step_expr
		// index = index [opstep2] step_expr
		//but the RemoveCSpecific transform (that is called in the front end)
		//removes the 2 first notations, and replace them by the last form. 
		//For example, i+=5 is the same as i = i + 5
		try {
			if (((SymbolInstruction)inst.getDest()).getSymbol() != this.getIterationIndex())
				throw new RuntimeException ("Cannot determine loop's step values : destination of step is not index symbol.");
			GenericInstruction expr = (GenericInstruction) inst.getSource();
			this.opStep = expr.getName();
			if (!stepOperators.contains(this.opStep)) 
				throw new RuntimeException ("Cannot determine loop's step values : unknown step operator {"+this.opStep+"}.");
			if (((SymbolInstruction)expr.getOperand(0)).getSymbol() != this.getIterationIndex()) 
				throw new RuntimeException ("Cannot determine loop's step values : first operand of step is not index symbol.");
			
			this.step = expr.getOperand(1); 
		} catch (Exception e) {
			//Catch cast exception
			throw new RuntimeException ("stepCannot determine loop's initialization : bad loop syntax.");
		}
	}

	//here we assumes that the loop tends to go till the test bound.
	private void computeIterationCount() {
		
		if (
				this.getStartValue() instanceof IntInstruction &&
				this.getStopValue() instanceof IntInstruction &&
				this.getStepValue() instanceof IntInstruction
		) {
			long start = ((IntInstruction) this.getStartValue()).getValue();
			long stop = ((IntInstruction) this.getStopValue()).getValue();
			long step = ((IntInstruction) this.getStepValue()).getValue();
			String opstep = this.getStepOp();
			String optest = this.getTestOp();
			
			if (opstep.equals(ArithmeticOperator.ADD.getLiteral())) {
				if (step == 0) return;
				this.iterationCount = Math.abs((stop - start) / step);
				if (optest.contains("e")) this.iterationCount++;
				
				
			} else if (opstep.equals(ArithmeticOperator.SUB.getLiteral())) {
				if (step == 0) return;
				this.iterationCount = Math.abs((start - stop ) / step); // Changes here
				if (optest.contains("e")) this.iterationCount++;
				
			} else if (opstep.equals(ArithmeticOperator.MUL.getLiteral())) { // Changes here
				if (step == 0 || start == 0) return;
				if (Math.abs(step) == 1) return;
				this.iterationCount = (long) Math.ceil(Math.log10(stop / start)/Math.log10(step)); // Changes here
				if (optest.contains("e")) this.iterationCount++;
				
			} else if (opstep.equals(ArithmeticOperator.DIV.getLiteral())) { // Changes here
				if (step == 0) return;debug(Math.log10(step) + "  div 4");
				if (Math.abs(step) == 1) return;
				if (stop == 0)	this.iterationCount = (long) Math.ceil(Math.log10(start)/Math.log10(step)) + 1;
				else 			this.iterationCount = (long) Math.ceil(Math.log10(start / stop)/Math.log10(step)); // Changes here
				if (optest.contains("e")) this.iterationCount++;
			} else {
				throw new RuntimeException("Error : unknown operator : {"+opstep+"}.");
			}
		} else {
			this.iterationCount = -1;
		}
	}
	
	/**
	 * determines what kind of unroll should be called depending of loops values
	 * & pragmas
	 */
	private void makeUnrollDecision() {
		
		long time = System.nanoTime();
		
		this.valid_partialUnroll = this.gotInit && this.gotStep && this.gotTest;
		if (!valid_partialUnroll) {
			this.decision = Decision.NOUNROLL; 
			return;
		}
		this.valid_unrollAll = this.valid_partialUnroll && (
				this.getStartValue() instanceof IntInstruction &&
				this.getStopValue() instanceof IntInstruction &&
				this.getStepValue() instanceof IntInstruction &&
				this.iterationCount > 0
		);
		
		FindIndexAlias iaf = new FindIndexAlias(this.index);
		this.forBlock.getBodyBlock().accept(iaf);
		
		boolean indexIsAliased = iaf.isAliased();
		
		boolean indexIsGlobal = this.index.getContainingScope().isGlobal();
		
		IndexInLHSFinder iilf = new IndexInLHSFinder(this.index);
		iilf.doSwitch(this.forBlock.getBodyBlock());
		boolean indexInLhs = iilf.lhs();
		
		FindBreakContinue bcbf = new FindBreakContinue();
		this.forBlock.getBodyBlock().accept(bcbf);
		
		boolean containsContinue = bcbf.containContinue;
		boolean containsBreak = bcbf.containBreak;
		
		this.containsBreakorContinue = containsBreak || containsContinue;
		
		boolean partialModeFinalValue = (hasPragma && hasFactor) || this.unrollfactor != -1;
		
		boolean itSUPth = (this.iterationCount != -1) && this.iterationCount >= threshold;
		boolean factorSUPit = (this.iterationCount != -1) && (unrollfactor >= this.iterationCount);
		if (factorSUPit) unrollfactor = iterationCount;
		boolean factorModIt = (this.iterationCount != -1) && (this.iterationCount / unrollfactor < 2);
				
		boolean nounroll = (
				(indexIsAliased) ||
				(indexIsGlobal) ||
				(indexInLhs) ||
				((valid_unrollAll) && (((IntInstruction)this.step).getValue() == 0)) ||
				(containsContinue) ||
				(pragmas && !hasPragma) ||	
				(hasPragma && hasForce && !force) ||
				(!valid_partialUnroll) ||
				(!partialModeFinalValue && !valid_unrollAll) ||
				(threshold != -1 && 
						(
							(!partialModeFinalValue && itSUPth) || 
							(partialModeFinalValue && !itSUPth) 
						)
				) || 
				(partialModeFinalValue && (unrollfactor < 2)) ||
				(!partialModeFinalValue && containsBreak)//*/
		);

		boolean unrollall = !nounroll && (
				unrollAll || 
				//this makes the unroller unroll all instead of partial when
				//the unroll factor is greater than the number of iterations
				(partialModeFinalValue && factorSUPit && !containsBreak) || 
				(!partialModeFinalValue) ||
				//this makes the unroller unroll all instead of partial when
				//the number of iterations after unrolling partially is only
				//one (and the end if the loop is outside loop with strip
				//mining).
				(partialModeFinalValue && factorModIt && !containsBreak) 
		);
		
		boolean partialunroll = !nounroll && !unrollall;
		
		if (unrollall) this.decision = Decision.UNROLLALL;
		else if (partialunroll) this.decision = Decision.UNROLLPARTIAL;
		else if (nounroll) this.decision = Decision.NOUNROLL;
		else throw new RuntimeException("Guru Meditation");
		
		time = (System.nanoTime() - time);
		//System.out.println("Making decision took : "+time+" ns");
		
	}

	public Decision getDecision() {
		return this.decision;
	}
	
	public long getFactor() {
		return unrollfactor;
	}
	
	/**
	 * return loop's index.
	 * 
	 * @return
	 */
	public Symbol getIterationIndex() {
		return this.index;
	}

	/**
	 * Return the number of iterations of the loop. If the bounds cannot be
	 * processed, it returns -1;
	 * 
	 * Useless?
	 * 
	 * @return
	 */
	public long getIterationCount() {
		return this.iterationCount;
	}//*/

	/**
	 * return the instruction corresponding to the initial value of the index.<br/>
	 * Note : It is an IntInstruction if full unroll mode is allowed.
	 * 
	 * @return
	 */
	public Instruction getStartValue() {
		return this.init;
	}

	/**
	 * Test an index' value. Returns true if its value is between
	 * getStartValue() and getStopValue(); <br/>
	 * Only works when bounds are known (if full unroll mode is allowed),
	 * otherwise returns false.
	 * 
	 * @param i
	 * @return
	 */
	public boolean testValue(long i) {
		String optest = this.opTest;
		long stopValue = 0;
		try {
			stopValue = ((IntInstruction)this.getStopValue()).getValue();
		} catch (Exception e) {
			return false;
		}
		if (optest.equals(ComparisonOperator.EQ.getLiteral())) {
			return i == stopValue;
		} else if (optest.equals(ComparisonOperator.NE.getLiteral())) {
			return i != stopValue;
		} else if (optest.equals(ComparisonOperator.GE.getLiteral())) {
			return i >= stopValue;
		} else if (optest.equals(ComparisonOperator.GT.getLiteral())) {
			return i > stopValue;
		} else if (optest.equals(ComparisonOperator.LE.getLiteral())) {
			return i <= stopValue;
		} else if (optest.equals(ComparisonOperator.LT.getLiteral())) {
			return i < stopValue;
		}
		throw new IllegalArgumentException("Error : unknown relational operator.");
	}

	/**
	 * Returns the instruction corresponding to the initial value of the index. <br/>
	 * Note : It is an IntInstruction if full unroll mode is allowed.
	 * 
	 * @return
	 */
	public Instruction getStopValue() {
		return this.stop;
	}

	/**
	 * Return the instruction corresponding to the value of the step of the
	 * loop. <br/>
	 * Note : It is an IntInstruction if full unroll mode is allowed.
	 * 
	 * @return
	 */
	public Instruction getStepValue() {
		return this.step;
	}

	/**
	 * Returns the operator used in the step expression
	 * 
	 * @return
	 */
	public String getStepOp() {
		return this.opStep;
	}

	/**
	 * Returns the operator used in the test expression
	 * 
	 * @return
	 */
	public String getTestOp() {
		return this.opTest;
	}
	
	/**
	 * Returns the reverse operator of the test expression.
	 * For example, if the operator is "<" (lower than), this
	 * method will return ">=" (greater or equals).
	 * @return
	 */
	public String getReversedTestOp() {
		return reverseOperators.get(this.opTest);
	}

	/**
	 * return the value of the iteration index at the iteration n
	 * @param l
	 * @return
	 */
	public long valueAtIteration(long l) {
		if (iterationCount < 1 || l > iterationCount) throw new RuntimeException("Error : cannot determine value at iteration.");
		
		long start = ((IntInstruction) init).getValue();
		long step = ((IntInstruction) this.step).getValue();
		long res = 0;
		if (opStep.equals(ArithmeticOperator.MUL.getLiteral())) {
			res = start * (step ^ l);
		} else if (opStep.equals(ArithmeticOperator.DIV.getLiteral())) {
			res = start / (step ^ l);debug(PREFIX + "div 2");
		} else if (opStep.equals(ArithmeticOperator.ADD.getLiteral()) || opStep.equals(ArithmeticOperator.SUB.getLiteral())) {
			res = start + step * l;
		} else {
			throw new RuntimeException("Error : Unsupported step operator.");
		}
		
		return res;
	}
	
	/**
	 * Return the value of v, after adding (or removing) the value of the step.
	 * It should be only used in the full unroll mode (return -1 otherwise);
	 * 
	 * @return
	 */
	public long incIndex(long i) {
		if (!this.valid_unrollAll) return -1;
		
		long step = ((IntInstruction) this.getStepValue()).getValue();
		String op = this.getStepOp();
		
		if(op.equals(ArithmeticOperator.ADD.getLiteral())) {
			if (Math.abs(step) < 1) throw new RuntimeException("Error : cannot unroll loop with infinite iterations.");
			return i + step;
		} else if (op.equals(ArithmeticOperator.SUB.getLiteral())) {
			if (Math.abs(step) < 1) throw new RuntimeException("Error : cannot unroll loop with infinite iterations.");
			return i - step;
		} else if (op.equals(ArithmeticOperator.MUL.getLiteral())) {
			if (Math.abs(i) < 1 || Math.abs(step) < 2) throw new RuntimeException("Error : cannot unroll loop with infinite iterations.");
			return i * step;
		} else if (op.equals(ArithmeticOperator.DIV.getLiteral())) {debug(PREFIX + "div 3");
			if (Math.abs(i) < 1 || Math.abs(step) < 2) throw new RuntimeException("Error : cannot unroll loop with infinite iterations.");
			return i / step;
		} else {
			throw new RuntimeException("Error : unknown operator for step.");
		}
	}

	// TODO : ajouter un incIndex pour partial unroll (qui multiplie par le
	// factor) et qui retourne une instruction deja toute faite
	
	/**
	 * Return true if the loop should be totally unrolled.
	 * 
	 * @return
	 */
	public boolean validFullUnroll() {
		return valid_unrollAll;
	}

	/**
	 * Return true if the loop should be partially unrolled
	 * 
	 * @return
	 */
	public boolean validPartialUnroll() {
		return valid_partialUnroll;
	}
	
	/**
	 * Return true if body contains break or continue related to this loop
	 */
	public boolean containsBreakOrContinue() {
		return containsBreakorContinue;
	}

	/**
	 * Visit forBlock's annotations, looking for #pragma
	 * 
	 * @param fb
	 */
	private void processPragmas(ForBlock fb) {
		ForUnrollPragmaVisitor fpv = new ForUnrollPragmaVisitor(fb);
		this.hasPragma = fpv.hasUnrollPragma();
		if (this.hasPragma) {
			this.annots = fpv.getPramas();
			this.hasFactor = fpv.hasFactor();
			if (this.hasFactor) {
				this.unrollfactor = fpv.getFactor();
			}
			this.hasForce = fpv.hasForce();
			if (this.hasForce) {
				this.force = fpv.getForce();
			}
			this.unrollAll = fpv.all();
		}
	}

	/**
	 * Return String representation of the loop
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(PREFIX + " -------------------- Infos About For-loop -------------------- \n");
		sb.append(PREFIX + "Mode = {threshold : " + this.threshold + " ; unroll factor : " + 
				this.unrollfactor + " ; pragmas : "	+ this.pragmas + "} \n");
		sb.append(PREFIX + this.forBlock + "\n");
		sb.append(PREFIX + "\t- index = "+this.getIterationIndex()+"\n");
		sb.append(PREFIX + "\t- init = "+this.getStartValue()+(this.getStartValue() instanceof IntInstruction?" (const)\n":"\n"));
		sb.append(PREFIX + "\t- stop = "+this.getStopValue()+(this.getStopValue() instanceof IntInstruction?" (const)\n":"\n"));
		sb.append(PREFIX + "\t- stop Operator = "+this.getTestOp()+"\n");
		
		sb.append(PREFIX + "\t- step = "+this.getStepValue()+(this.getStepValue() instanceof IntInstruction?" (const)\n":"\n"));
		sb.append(PREFIX + "\t- step Operator = "+this.getStepOp()+"\n");

		sb.append(PREFIX + "\t- valid for unroll : "+(this.valid_unrollAll?"ALL + PARTIAL":this.valid_partialUnroll?"PARTIAL":"NONE")+"\n");
		if (this.iterationCount > -1) sb.append(PREFIX + "\t- iteration count = "+this.getIterationCount()+"\n");
		else sb.append(PREFIX+"\t- unable to determine number of iterations"+"\n");
		sb.append(PREFIX + "\t- pragmas : ");
		if (this.hasPragma) {
			sb.append("\n");
			for (String s : this.annots.getContent()) sb.append(PREFIX+"\t\t> "+s+"\n");
			
		} else sb.append("none\n");

		sb.append(PREFIX + "\t- factor = "+unrollfactor+"\n");
		sb.append(PREFIX + "\t- threshold = "+threshold+"\n");
		sb.append(PREFIX + "\t- unroll decision : "+this.getDecision()+"\n");
		
		return sb.toString();
	}

	/**
	 * Static class to isolate research of #PRAGMA.
	 * 
	 * @author amorvan
	 * 
	 */
	private static class ForUnrollPragmaVisitor extends
	AnnotationsSwitch<Object> {

		private static final String UNROLL_STR_KEY = GECOS_PRAGMA_PREFIX+"UNROLL";
		private static final String UNROLLFORCE_STR_KEY = GECOS_PRAGMA_PREFIX+"UNROLL=";
		private static final String UNROLLFACTOR_STR_KEY = GECOS_PRAGMA_PREFIX+"UNROLLFACTOR=";
		private static final String UNROLLALL_STR_KEY = GECOS_PRAGMA_PREFIX+"UNROLLALL";

		private static final String PREFIX = "[ForPragmaVisitor] ";
		private static final boolean debug = false;

		private ForBlock forBlock;
		private boolean hasPragma;
		private PragmaAnnotation annotation;

		private boolean hasFactor;
		private int factor;

		private boolean hasForce;
		private boolean force;

		private boolean all;

		public ForUnrollPragmaVisitor(ForBlock block) {
			this.forBlock = block;

			// default values
			this.hasFactor = false;
			this.factor = -1;
			this.hasForce = false;
			this.force = false;
			this.all = false;

			this.hasPragma = false;
			this.annotation = null;

			// visit forblock's annotations
			Iterator<IAnnotation> ita = forBlock.getAnnotations().values()
			.iterator();
			while (ita.hasNext() && !this.hasPragma) {
				this.doSwitch(ita.next());
			}

			if (hasPragma
					&& (annotation == null || annotation.getContent().size() == 0))
				throw new RuntimeException("error");

			if (hasPragma)
				for (String str : annotation.getContent()) {
					str = str.toUpperCase(); // force upper case
					// look for force key
					if (str.startsWith(UNROLLFORCE_STR_KEY) && !hasForce) {
						hasForce = true;
						if (str.endsWith("YES")) {
							force = true;
						} else if (str.endsWith("NO")) {
							force = false;
						} else {
							hasForce = false;
							throw new UnsupportedOperationException(
							"Error : UNROLL=[YES or NO].");
						}
					}
					// look for factor key
					if (str.startsWith(UNROLLFACTOR_STR_KEY) && !hasFactor) {
						hasFactor = true;
						try {
							String strfactor = str
							.substring(UNROLLFACTOR_STR_KEY.length(),
									str.length());
							// debugln(strfactor);
							factor = Integer.valueOf(strfactor);
						} catch (Exception e) {
							throw new RuntimeException(
							"Error : unroll factor has to be a INTEGER.");
						}
					}
					// look for all key
					if (str.equals(UNROLLALL_STR_KEY) && !all) {
						all = true;
					}
					// look for other keys...
				}

			if (hasFactor && all)
				throw new RuntimeException(
				"Error (conflict) : pragma has an unroll factor and UNROLLALL directive.");
		}

		public boolean hasUnrollPragma() {
			return hasPragma;
		}

		public PragmaAnnotation getPramas() {
			return this.annotation;
		}

		public boolean hasFactor() {
			return hasFactor;
		}

		public int getFactor() {
			return factor;
		}

		public boolean hasForce() {
			return hasForce;
		}

		public boolean getForce() {
			return force;
		}

		public boolean all() {
			return all;
		}

		@Override
		public Object casePragmaAnnotation(PragmaAnnotation object) {
			if (object.getContent().size() != 0) {

				// enumeration of pragmas for debug only
				if (debug) {
					debug(PREFIX + "Non empty Pragma found {");
					for (String string : object.getContent()) {
						debug("\"" + string + "\"" + ",");
					}
					debugln("}");
				}

				// targeting unroll pragmas
				Iterator<String> it = object.getContent().iterator();
				while (it.hasNext() && !this.hasPragma) {
					this.hasPragma = it.next().toUpperCase().contains(
							UNROLL_STR_KEY);
				}
				if (this.hasPragma)
					this.annotation = object;
			}
			return null;
		}

	}

	private static class IndexInLHSFinder extends BlocksDefaultSwitch<Object> {
		
		private static class InstrsIndexInLHSFinder extends DefaultInstructionSwitch<Object> {
			
			private Symbol index;
			
			private boolean lhs = false;
			
			private boolean inLhs = false;
			
			public boolean lhs() { return inLhs; }
			
			public InstrsIndexInLHSFinder(Symbol i) {
				this.index = i;
				this.lhs = false;
				this.inLhs = false;
			}
			
			@Override
			public Object caseSetInstruction(SetInstruction object) {
				this.lhs = true;
				doSwitch(object.getDest());
				this.lhs = false;
				doSwitch(object.getSource());
				return null;
			}
			
			@Override
			public Object caseSymbolInstruction(SymbolInstruction object) {
				if (this.lhs && object.getSymbol() == index && (object.getParent() instanceof SetInstruction || object.getParent() instanceof AddressInstruction)) this.inLhs = true;
				return null;
			}
		}
		
		private Symbol index;
		private boolean lhs = false;
		public IndexInLHSFinder(Symbol i) {
			this.index = i;
			this.lhs = false;
		}
		
		@Override
		public Object caseBasicBlock(BasicBlock b) {
			if (this.lhs) return null;
			InstrsIndexInLHSFinder i = new InstrsIndexInLHSFinder(index);
			i.doSwitch(b);
			if (i.lhs()) this.lhs = true;
			return null;
		}
		
		public boolean lhs() {return this.lhs;}
		
	}

	private static class FindBreakContinue extends GecosBlocksInstructionsDefaultVisitor {

		public boolean containBreak = false;
		public boolean containContinue = false;
		
		@Override
		public void visitBreakInstruction(BreakInstruction b) {
			containBreak = true;
		}
		
		@Override
		public void visitContinueInstruction(ContinueInstruction c) {
			containContinue = true;
		}
	}
	

	private static class FindIndexAlias extends GecosBlocksInstructionsDefaultVisitor {
		private Symbol index;
		private boolean isAliased;
		public FindIndexAlias(Symbol i) {
			this.index = i;
			this.isAliased = false;
		}
		public boolean isAliased() { return this.isAliased; }
		
		public void visitAddressInstruction(AddressInstruction a) {
			Instruction i = a.getAddress();
			if (i instanceof SymbolInstruction && ((SymbolInstruction)i).getSymbol() == index) this.isAliased = true;
		}
	}
}
