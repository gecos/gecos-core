/**
 */
package gecos.blocks.impl;

import gecos.annotations.AnnotatedElement;
import gecos.annotations.AnnotationKeys;
import gecos.annotations.AnnotationsPackage;
import gecos.annotations.FileLocationAnnotation;
import gecos.annotations.IAnnotation;
import gecos.annotations.PragmaAnnotation;

import gecos.annotations.impl.StringToIAnnotationMapImpl;

import gecos.blocks.BasicBlock;
import gecos.blocks.BlocksPackage;
import gecos.blocks.BlocksVisitor;
import gecos.blocks.ControlEdge;

import gecos.instrs.BranchType;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Control Edge</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.blocks.impl.ControlEdgeImpl#getAnnotations <em>Annotations</em>}</li>
 *   <li>{@link gecos.blocks.impl.ControlEdgeImpl#getCond <em>Cond</em>}</li>
 *   <li>{@link gecos.blocks.impl.ControlEdgeImpl#getFlags <em>Flags</em>}</li>
 *   <li>{@link gecos.blocks.impl.ControlEdgeImpl#getFrom <em>From</em>}</li>
 *   <li>{@link gecos.blocks.impl.ControlEdgeImpl#getTo <em>To</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ControlEdgeImpl extends MinimalEObjectImpl.Container implements ControlEdge {
	/**
	 * The cached value of the '{@link #getAnnotations() <em>Annotations</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, IAnnotation> annotations;

	/**
	 * The default value of the '{@link #getCond() <em>Cond</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCond()
	 * @generated
	 * @ordered
	 */
	protected static final BranchType COND_EDEFAULT = BranchType.UNCONDITIONAL;

	/**
	 * The cached value of the '{@link #getCond() <em>Cond</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCond()
	 * @generated
	 * @ordered
	 */
	protected BranchType cond = COND_EDEFAULT;

	/**
	 * The default value of the '{@link #getFlags() <em>Flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFlags()
	 * @generated
	 * @ordered
	 */
	protected static final int FLAGS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getFlags() <em>Flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFlags()
	 * @generated
	 * @ordered
	 */
	protected int flags = FLAGS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTo() <em>To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTo()
	 * @generated
	 * @ordered
	 */
	protected BasicBlock to;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ControlEdgeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BlocksPackage.Literals.CONTROL_EDGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, IAnnotation> getAnnotations() {
		if (annotations == null) {
			annotations = new EcoreEMap<String,IAnnotation>(AnnotationsPackage.Literals.STRING_TO_IANNOTATION_MAP, StringToIAnnotationMapImpl.class, this, BlocksPackage.CONTROL_EDGE__ANNOTATIONS);
		}
		return annotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BranchType getCond() {
		return cond;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCond(BranchType newCond) {
		BranchType oldCond = cond;
		cond = newCond == null ? COND_EDEFAULT : newCond;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.CONTROL_EDGE__COND, oldCond, cond));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getFlags() {
		return flags;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFlags(int newFlags) {
		int oldFlags = flags;
		flags = newFlags;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.CONTROL_EDGE__FLAGS, oldFlags, flags));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicBlock getFrom() {
		if (eContainerFeatureID() != BlocksPackage.CONTROL_EDGE__FROM) return null;
		return (BasicBlock)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicBlock basicGetFrom() {
		if (eContainerFeatureID() != BlocksPackage.CONTROL_EDGE__FROM) return null;
		return (BasicBlock)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFrom(BasicBlock newFrom, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newFrom, BlocksPackage.CONTROL_EDGE__FROM, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFrom(BasicBlock newFrom) {
		if (newFrom != eInternalContainer() || (eContainerFeatureID() != BlocksPackage.CONTROL_EDGE__FROM && newFrom != null)) {
			if (EcoreUtil.isAncestor(this, newFrom))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newFrom != null)
				msgs = ((InternalEObject)newFrom).eInverseAdd(this, BlocksPackage.BASIC_BLOCK__OUT_EDGES, BasicBlock.class, msgs);
			msgs = basicSetFrom(newFrom, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.CONTROL_EDGE__FROM, newFrom, newFrom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicBlock getTo() {
		return to;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTo(BasicBlock newTo, NotificationChain msgs) {
		BasicBlock oldTo = to;
		to = newTo;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BlocksPackage.CONTROL_EDGE__TO, oldTo, newTo);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTo(BasicBlock newTo) {
		if (newTo != to) {
			NotificationChain msgs = null;
			if (to != null)
				msgs = ((InternalEObject)to).eInverseRemove(this, BlocksPackage.BASIC_BLOCK__IN_EDGES, BasicBlock.class, msgs);
			if (newTo != null)
				msgs = ((InternalEObject)newTo).eInverseAdd(this, BlocksPackage.BASIC_BLOCK__IN_EDGES, BasicBlock.class, msgs);
			msgs = basicSetTo(newTo, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.CONTROL_EDGE__TO, newTo, newTo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String fromString = "null";
		String toString = "null";
		BasicBlock _from = this.getFrom();
		boolean _tripleNotEquals = (_from != null);
		if (_tripleNotEquals) {
			fromString = this.getFrom().toShortString();
		}
		BasicBlock _to = this.getTo();
		boolean _tripleNotEquals_1 = (_to != null);
		if (_tripleNotEquals_1) {
			toString = this.getTo().toShortString();
		}
		BranchType _cond = this.getCond();
		String _plus = ("ControlEdge[" + _cond);
		String _plus_1 = (_plus + ", ");
		String _plus_2 = (_plus_1 + fromString);
		String _plus_3 = (_plus_2 + "->");
		String _plus_4 = (_plus_3 + toString);
		return (_plus_4 + "]");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final BlocksVisitor visitor) {
		visitor.visitControlEdge(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void disconnectFrom(final BasicBlock from) {
		BasicBlock _from = this.getFrom();
		boolean _tripleEquals = (_from == from);
		if (_tripleEquals) {
			this.setFrom(null);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void disconnect() {
		this.getFrom().getOutEdges().remove(this);
		this.setTo(null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFlag(final int f) {
		this.setFlags(f);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addFlag(final int f) {
		this.setFlags((this.getFlags() | f));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeFlag(final int f) {
		this.setFlags((this.getFlags() & (~f)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSet(final int f) {
		return ((this.getFlags() & f) != 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IAnnotation getAnnotation(final String key) {
		return this.getAnnotations().get(key);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnnotation(final String key, final IAnnotation annot) {
		this.getAnnotations().put(key, annot);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PragmaAnnotation getPragma() {
		final IAnnotation annot = this.getAnnotation(AnnotationKeys.PRAGMA_ANNOTATION_KEY.getLiteral());
		if ((annot instanceof PragmaAnnotation)) {
			return ((PragmaAnnotation)annot);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FileLocationAnnotation getFileLocation() {
		final IAnnotation annot = this.getAnnotation(AnnotationKeys.FILE_LOCATION_ANNOTATION_KEY.getLiteral());
		if ((annot instanceof FileLocationAnnotation)) {
			return ((FileLocationAnnotation)annot);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BlocksPackage.CONTROL_EDGE__FROM:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetFrom((BasicBlock)otherEnd, msgs);
			case BlocksPackage.CONTROL_EDGE__TO:
				if (to != null)
					msgs = ((InternalEObject)to).eInverseRemove(this, BlocksPackage.BASIC_BLOCK__IN_EDGES, BasicBlock.class, msgs);
				return basicSetTo((BasicBlock)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BlocksPackage.CONTROL_EDGE__ANNOTATIONS:
				return ((InternalEList<?>)getAnnotations()).basicRemove(otherEnd, msgs);
			case BlocksPackage.CONTROL_EDGE__FROM:
				return basicSetFrom(null, msgs);
			case BlocksPackage.CONTROL_EDGE__TO:
				return basicSetTo(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case BlocksPackage.CONTROL_EDGE__FROM:
				return eInternalContainer().eInverseRemove(this, BlocksPackage.BASIC_BLOCK__OUT_EDGES, BasicBlock.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BlocksPackage.CONTROL_EDGE__ANNOTATIONS:
				if (coreType) return getAnnotations();
				else return getAnnotations().map();
			case BlocksPackage.CONTROL_EDGE__COND:
				return getCond();
			case BlocksPackage.CONTROL_EDGE__FLAGS:
				return getFlags();
			case BlocksPackage.CONTROL_EDGE__FROM:
				if (resolve) return getFrom();
				return basicGetFrom();
			case BlocksPackage.CONTROL_EDGE__TO:
				return getTo();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BlocksPackage.CONTROL_EDGE__ANNOTATIONS:
				((EStructuralFeature.Setting)getAnnotations()).set(newValue);
				return;
			case BlocksPackage.CONTROL_EDGE__COND:
				setCond((BranchType)newValue);
				return;
			case BlocksPackage.CONTROL_EDGE__FLAGS:
				setFlags((Integer)newValue);
				return;
			case BlocksPackage.CONTROL_EDGE__FROM:
				setFrom((BasicBlock)newValue);
				return;
			case BlocksPackage.CONTROL_EDGE__TO:
				setTo((BasicBlock)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BlocksPackage.CONTROL_EDGE__ANNOTATIONS:
				getAnnotations().clear();
				return;
			case BlocksPackage.CONTROL_EDGE__COND:
				setCond(COND_EDEFAULT);
				return;
			case BlocksPackage.CONTROL_EDGE__FLAGS:
				setFlags(FLAGS_EDEFAULT);
				return;
			case BlocksPackage.CONTROL_EDGE__FROM:
				setFrom((BasicBlock)null);
				return;
			case BlocksPackage.CONTROL_EDGE__TO:
				setTo((BasicBlock)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BlocksPackage.CONTROL_EDGE__ANNOTATIONS:
				return annotations != null && !annotations.isEmpty();
			case BlocksPackage.CONTROL_EDGE__COND:
				return cond != COND_EDEFAULT;
			case BlocksPackage.CONTROL_EDGE__FLAGS:
				return flags != FLAGS_EDEFAULT;
			case BlocksPackage.CONTROL_EDGE__FROM:
				return basicGetFrom() != null;
			case BlocksPackage.CONTROL_EDGE__TO:
				return to != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == AnnotatedElement.class) {
			switch (derivedFeatureID) {
				case BlocksPackage.CONTROL_EDGE__ANNOTATIONS: return AnnotationsPackage.ANNOTATED_ELEMENT__ANNOTATIONS;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == AnnotatedElement.class) {
			switch (baseFeatureID) {
				case AnnotationsPackage.ANNOTATED_ELEMENT__ANNOTATIONS: return BlocksPackage.CONTROL_EDGE__ANNOTATIONS;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ControlEdgeImpl
