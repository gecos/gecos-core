/**
 */
package gecos.instrs.impl;

import com.google.common.base.Objects;

import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;

import gecos.core.Symbol;

import gecos.instrs.InstrsPackage;
import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;
import gecos.instrs.RangeArrayInstruction;
import gecos.instrs.SimpleArrayInstruction;
import gecos.instrs.SymbolInstruction;

import gecos.types.ArrayType;
import gecos.types.Type;

import java.util.function.Consumer;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Range Array Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RangeArrayInstructionImpl extends ArrayInstructionImpl implements RangeArrayInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RangeArrayInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstrsPackage.eINSTANCE.getRangeArrayInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol getSymbol() {
		Symbol _xblockexpression = null;
		{
			final Instruction si = this.getDest();
			Symbol _xifexpression = null;
			if ((si instanceof SymbolInstruction)) {
				_xifexpression = ((SymbolInstruction)si).getSymbol();
			}
			else {
				_xifexpression = null;
			}
			_xblockexpression = _xifexpression;
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSymbol(final Symbol newSymbol) {
		final Instruction si = this.getDest();
		if ((si instanceof SymbolInstruction)) {
			((SymbolInstruction)si).setSymbol(newSymbol);
		}
		else {
			throw new UnsupportedOperationException("Not yet Implemented");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDest(final Instruction newDest) {
		if ((newDest instanceof SymbolInstruction)) {
			super.setDest(newDest);
		}
		else {
			String _simpleName = this.getClass().getSimpleName();
			String _plus = ("Illegal use of " + _simpleName);
			String _plus_1 = (_plus + ".setDest() : target must be a symbol not a compex instrcytion such as ");
			String _plus_2 = (_plus_1 + newDest);
			throw new UnsupportedOperationException(_plus_2);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final InstrsVisitor visitor) {
		visitor.visitRangeArrayInstruction(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		Symbol _symbol = this.getSymbol();
		boolean _tripleNotEquals = (_symbol != null);
		if (_tripleNotEquals) {
			String _name = this.getSymbol().getName();
			String _plus = ("" + _name);
			final StringBuffer sb = new StringBuffer(_plus);
			try {
				EList<Instruction> _index = this.getIndex();
				boolean _tripleNotEquals_1 = (_index != null);
				if (_tripleNotEquals_1) {
					EList<Instruction> _index_1 = this.getIndex();
					for (final Instruction i : _index_1) {
						if ((i != null)) {
							sb.append("[").append(i).append("]");
						}
						else {
							sb.append("[null]");
						}
					}
				}
				long _size = this.getType().getSize();
				String _plus_1 = (":" + Long.valueOf(_size));
				sb.append(_plus_1);
			}
			catch (final Throwable _t) {
				if (_t instanceof Exception) {
					sb.append("[#error#]");
				}
				else {
					throw Exceptions.sneakyThrow(_t);
				}
			}
			return sb.toString();
		}
		else {
			return super.toString();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArrayType getTargetArrayType() {
		Type _type = this.getSymbol().getType();
		if ((_type instanceof ArrayType)) {
			Type _type_1 = this.getSymbol().getType();
			return ((ArrayType) _type_1);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleArrayInstruction getAccessedElement(final int i) {
		if ((i < 0)) {
			throw new RuntimeException("Unsupported Operation");
		}
		final SimpleArrayInstruction element0 = GecosUserInstructionFactory.array(this.getSymbol());
		final Consumer<Instruction> _function = new Consumer<Instruction>() {
			public void accept(final Instruction it) {
				element0.addIndex(it.copy());
			}
		};
		this.getIndex().forEach(_function);
		if ((i == 0)) {
			return element0;
		}
		final Instruction lastIdx = IterableExtensions.<Instruction>last(element0.getIndex());
		element0.getIndex().remove(lastIdx);
		element0.getIndex().add(GecosUserInstructionFactory.add(lastIdx.copy(), GecosUserInstructionFactory.Int(i)));
		return element0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSame(final Instruction instr) {
		if ((instr instanceof RangeArrayInstruction)) {
			return (super.isSame(instr) && Objects.equal(this.getSymbol(), ((RangeArrayInstruction)instr).getSymbol()));
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol getUsedSymbol() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void updateSymbol(final Symbol originalSymbol, final Symbol newSymbol) {
		Symbol _usedSymbol = this.getUsedSymbol();
		boolean _tripleEquals = (_usedSymbol == originalSymbol);
		if (_tripleEquals) {
			this.setSymbol(newSymbol);
		}
		else {
			throw new IllegalArgumentException("Given Symbol is not used.");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSymbolName() {
		return this.getUsedSymbol().getName();
	}

} //RangeArrayInstructionImpl
