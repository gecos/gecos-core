/**
 */
package gecos.gecosproject;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see gecos.gecosproject.GecosprojectPackage
 * @generated
 */
public interface GecosprojectFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GecosprojectFactory eINSTANCE = gecos.gecosproject.impl.GecosprojectFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Gecos Project</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Gecos Project</em>'.
	 * @generated
	 */
	GecosProject createGecosProject();

	/**
	 * Returns a new object of class '<em>Gecos Source File</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Gecos Source File</em>'.
	 * @generated
	 */
	GecosSourceFile createGecosSourceFile();

	/**
	 * Returns a new object of class '<em>Gecos Header Directory</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Gecos Header Directory</em>'.
	 * @generated
	 */
	GecosHeaderDirectory createGecosHeaderDirectory();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	GecosprojectPackage getGecosprojectPackage();

} //GecosprojectFactory
