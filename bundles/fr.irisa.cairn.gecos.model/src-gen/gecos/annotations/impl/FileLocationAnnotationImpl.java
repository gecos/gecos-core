/**
 */
package gecos.annotations.impl;

import gecos.annotations.AnnotationsPackage;
import gecos.annotations.FileLocationAnnotation;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>File Location Annotation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.annotations.impl.FileLocationAnnotationImpl#getFilename <em>Filename</em>}</li>
 *   <li>{@link gecos.annotations.impl.FileLocationAnnotationImpl#getStartingLine <em>Starting Line</em>}</li>
 *   <li>{@link gecos.annotations.impl.FileLocationAnnotationImpl#getEndingLine <em>Ending Line</em>}</li>
 *   <li>{@link gecos.annotations.impl.FileLocationAnnotationImpl#getNodeOffset <em>Node Offset</em>}</li>
 *   <li>{@link gecos.annotations.impl.FileLocationAnnotationImpl#getNodeLength <em>Node Length</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FileLocationAnnotationImpl extends IAnnotationImpl implements FileLocationAnnotation {
	/**
	 * The default value of the '{@link #getFilename() <em>Filename</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilename()
	 * @generated
	 * @ordered
	 */
	protected static final String FILENAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFilename() <em>Filename</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilename()
	 * @generated
	 * @ordered
	 */
	protected String filename = FILENAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getStartingLine() <em>Starting Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartingLine()
	 * @generated
	 * @ordered
	 */
	protected static final int STARTING_LINE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getStartingLine() <em>Starting Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartingLine()
	 * @generated
	 * @ordered
	 */
	protected int startingLine = STARTING_LINE_EDEFAULT;

	/**
	 * The default value of the '{@link #getEndingLine() <em>Ending Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndingLine()
	 * @generated
	 * @ordered
	 */
	protected static final int ENDING_LINE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getEndingLine() <em>Ending Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndingLine()
	 * @generated
	 * @ordered
	 */
	protected int endingLine = ENDING_LINE_EDEFAULT;

	/**
	 * The default value of the '{@link #getNodeOffset() <em>Node Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodeOffset()
	 * @generated
	 * @ordered
	 */
	protected static final int NODE_OFFSET_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNodeOffset() <em>Node Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodeOffset()
	 * @generated
	 * @ordered
	 */
	protected int nodeOffset = NODE_OFFSET_EDEFAULT;

	/**
	 * The default value of the '{@link #getNodeLength() <em>Node Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodeLength()
	 * @generated
	 * @ordered
	 */
	protected static final int NODE_LENGTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNodeLength() <em>Node Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodeLength()
	 * @generated
	 * @ordered
	 */
	protected int nodeLength = NODE_LENGTH_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FileLocationAnnotationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnnotationsPackage.Literals.FILE_LOCATION_ANNOTATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFilename(String newFilename) {
		String oldFilename = filename;
		filename = newFilename;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AnnotationsPackage.FILE_LOCATION_ANNOTATION__FILENAME, oldFilename, filename));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getStartingLine() {
		return startingLine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartingLine(int newStartingLine) {
		int oldStartingLine = startingLine;
		startingLine = newStartingLine;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AnnotationsPackage.FILE_LOCATION_ANNOTATION__STARTING_LINE, oldStartingLine, startingLine));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getEndingLine() {
		return endingLine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndingLine(int newEndingLine) {
		int oldEndingLine = endingLine;
		endingLine = newEndingLine;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AnnotationsPackage.FILE_LOCATION_ANNOTATION__ENDING_LINE, oldEndingLine, endingLine));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNodeOffset() {
		return nodeOffset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNodeOffset(int newNodeOffset) {
		int oldNodeOffset = nodeOffset;
		nodeOffset = newNodeOffset;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AnnotationsPackage.FILE_LOCATION_ANNOTATION__NODE_OFFSET, oldNodeOffset, nodeOffset));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNodeLength() {
		return nodeLength;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNodeLength(int newNodeLength) {
		int oldNodeLength = nodeLength;
		nodeLength = newNodeLength;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AnnotationsPackage.FILE_LOCATION_ANNOTATION__NODE_LENGTH, oldNodeLength, nodeLength));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__FILENAME:
				return getFilename();
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__STARTING_LINE:
				return getStartingLine();
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__ENDING_LINE:
				return getEndingLine();
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__NODE_OFFSET:
				return getNodeOffset();
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__NODE_LENGTH:
				return getNodeLength();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__FILENAME:
				setFilename((String)newValue);
				return;
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__STARTING_LINE:
				setStartingLine((Integer)newValue);
				return;
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__ENDING_LINE:
				setEndingLine((Integer)newValue);
				return;
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__NODE_OFFSET:
				setNodeOffset((Integer)newValue);
				return;
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__NODE_LENGTH:
				setNodeLength((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__FILENAME:
				setFilename(FILENAME_EDEFAULT);
				return;
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__STARTING_LINE:
				setStartingLine(STARTING_LINE_EDEFAULT);
				return;
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__ENDING_LINE:
				setEndingLine(ENDING_LINE_EDEFAULT);
				return;
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__NODE_OFFSET:
				setNodeOffset(NODE_OFFSET_EDEFAULT);
				return;
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__NODE_LENGTH:
				setNodeLength(NODE_LENGTH_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__FILENAME:
				return FILENAME_EDEFAULT == null ? filename != null : !FILENAME_EDEFAULT.equals(filename);
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__STARTING_LINE:
				return startingLine != STARTING_LINE_EDEFAULT;
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__ENDING_LINE:
				return endingLine != ENDING_LINE_EDEFAULT;
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__NODE_OFFSET:
				return nodeOffset != NODE_OFFSET_EDEFAULT;
			case AnnotationsPackage.FILE_LOCATION_ANNOTATION__NODE_LENGTH:
				return nodeLength != NODE_LENGTH_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (filename: ");
		result.append(filename);
		result.append(", startingLine: ");
		result.append(startingLine);
		result.append(", endingLine: ");
		result.append(endingLine);
		result.append(", nodeOffset: ");
		result.append(nodeOffset);
		result.append(", nodeLength: ");
		result.append(nodeLength);
		result.append(')');
		return result.toString();
	}

} //FileLocationAnnotationImpl
