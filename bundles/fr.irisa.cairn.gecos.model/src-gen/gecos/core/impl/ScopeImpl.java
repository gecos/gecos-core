/**
 */
package gecos.core.impl;

import com.google.common.base.Objects;

import com.google.common.collect.Iterables;

import fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;
import fr.irisa.cairn.gecos.model.tools.utils.GecosCopier;

import gecos.core.CoreFactory;
import gecos.core.CorePackage;
import gecos.core.CoreVisitor;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.ScopeContainer;
import gecos.core.Symbol;

import gecos.types.AliasType;
import gecos.types.EnumType;
import gecos.types.RecordType;
import gecos.types.Type;

import java.util.Collection;
import java.util.LinkedHashMap;

import java.util.function.Consumer;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Scope</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.core.impl.ScopeImpl#getTypes <em>Types</em>}</li>
 *   <li>{@link gecos.core.impl.ScopeImpl#getSymbols <em>Symbols</em>}</li>
 *   <li>{@link gecos.core.impl.ScopeImpl#getContainer <em>Container</em>}</li>
 *   <li>{@link gecos.core.impl.ScopeImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link gecos.core.impl.ScopeImpl#getChildren <em>Children</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScopeImpl extends MinimalEObjectImpl.Container implements Scope {
	/**
	 * The cached value of the '{@link #getTypes() <em>Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<Type> types;

	/**
	 * The cached value of the '{@link #getSymbols() <em>Symbols</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSymbols()
	 * @generated
	 * @ordered
	 */
	protected EList<Symbol> symbols;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScopeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.SCOPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Type> getTypes() {
		if (types == null) {
			types = new EObjectContainmentEList<Type>(Type.class, this, CorePackage.SCOPE__TYPES);
		}
		return types;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Symbol> getSymbols() {
		if (symbols == null) {
			symbols = new EObjectContainmentEList<Symbol>(Symbol.class, this, CorePackage.SCOPE__SYMBOLS);
		}
		return symbols;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopeContainer getContainer() {
		if (eContainerFeatureID() != CorePackage.SCOPE__CONTAINER) return null;
		return (ScopeContainer)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopeContainer basicGetContainer() {
		if (eContainerFeatureID() != CorePackage.SCOPE__CONTAINER) return null;
		return (ScopeContainer)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContainer(ScopeContainer newContainer, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newContainer, CorePackage.SCOPE__CONTAINER, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainer(ScopeContainer newContainer) {
		if (newContainer != eInternalContainer() || (eContainerFeatureID() != CorePackage.SCOPE__CONTAINER && newContainer != null)) {
			if (EcoreUtil.isAncestor(this, newContainer))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newContainer != null)
				msgs = ((InternalEObject)newContainer).eInverseAdd(this, CorePackage.SCOPE_CONTAINER__SCOPE, ScopeContainer.class, msgs);
			msgs = basicSetContainer(newContainer, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.SCOPE__CONTAINER, newContainer, newContainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scope getParent() {
		EObject current = this.eContainer();
		while ((current != null)) {
			EObject _eContainer = current.eContainer();
			boolean _tripleNotEquals = (_eContainer != null);
			if (_tripleNotEquals) {
				EObject _eContainer_1 = current.eContainer();
				if ((_eContainer_1 instanceof ScopeContainer)) {
					EObject _eContainer_2 = current.eContainer();
					final Scope scope = ((ScopeContainer) _eContainer_2).getScope();
					if ((scope != null)) {
						return scope;
					}
				}
				current = current.eContainer();
			}
			else {
				return null;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Scope> getChildren() {
		ScopeContainer _container = this.getContainer();
		EObject current = ((EObject) _container);
		if ((current != null)) {
			final TreeIterator<EObject> i = current.eAllContents();
			final BasicEList<Scope> res = new BasicEList<Scope>();
			while (i.hasNext()) {
				{
					current = i.next();
					if ((current instanceof ScopeContainer)) {
						res.add(((ScopeContainer)current).getScope());
						i.prune();
					}
				}
			}
			return res;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		ScopeContainer _container = this.getContainer();
		String _plus = ("Scope(" + _container);
		String _plus_1 = (_plus + ") {");
		String _join = IterableExtensions.join(this.getSymbols(), ", ");
		String _plus_2 = (_plus_1 + _join);
		return (_plus_2 + "}");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scope managedCopy(final BlockCopyManager manager) {
		final Scope scope = CoreFactory.eINSTANCE.createScope();
		EList<Symbol> _symbols = this.getSymbols();
		for (final Symbol sym : _symbols) {
			if ((!(sym instanceof ProcedureSymbol))) {
				final Symbol newSymbol = sym.<Symbol>copy();
				scope.getSymbols().add(newSymbol);
				manager.link(sym, newSymbol);
			}
		}
		EList<Type> _types = this.getTypes();
		for (final Type type : _types) {
			{
				final Type newType = type.<Type>copy();
				scope.getTypes().add(newType);
				manager.link(type, newType);
			}
		}
		manager.link(this, scope);
		return scope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scope copy() {
		final GecosCopier gecosCopier = new GecosCopier();
		EObject _copy = gecosCopier.copy(this);
		final Scope scope = ((Scope) _copy);
		gecosCopier.copyReferences();
		return scope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type lookupType(final String name) {
		EList<Type> _types = this.getTypes();
		for (final Type t : _types) {
			{
				if ((t instanceof RecordType)) {
					String _name = ((RecordType)t).getName();
					boolean _equals = Objects.equal(_name, name);
					if (_equals) {
						return t;
					}
				}
				if ((t instanceof EnumType)) {
					String _name_1 = ((EnumType)t).getName();
					boolean _equals_1 = Objects.equal(_name_1, name);
					if (_equals_1) {
						return t;
					}
				}
				if ((t instanceof AliasType)) {
					String _name_2 = ((AliasType)t).getName();
					boolean _equals_2 = Objects.equal(_name_2, name);
					if (_equals_2) {
						return t;
					}
				}
			}
		}
		Scope _parent = this.getParent();
		Type _lookupType = null;
		if (_parent!=null) {
			_lookupType=_parent.lookupType(name);
		}
		return _lookupType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type lookupAliasType(final String name) {
		Type _elvis = null;
		final Function1<AliasType, Boolean> _function = new Function1<AliasType, Boolean>() {
			public Boolean apply(final AliasType it) {
				String _name = it.getName();
				return Boolean.valueOf(Objects.equal(_name, name));
			}
		};
		AliasType _findFirst = IterableExtensions.<AliasType>findFirst(Iterables.<AliasType>filter(this.getTypes(), AliasType.class), _function);
		if (_findFirst != null) {
			_elvis = _findFirst;
		} else {
			Scope _parent = this.getParent();
			Type _lookupAliasType = null;
			if (_parent!=null) {
				_lookupAliasType=_parent.lookupAliasType(name);
			}
			_elvis = _lookupAliasType;
		}
		return _elvis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type lookupEnumType(final String name) {
		Type _elvis = null;
		final Function1<EnumType, Boolean> _function = new Function1<EnumType, Boolean>() {
			public Boolean apply(final EnumType it) {
				String _name = it.getName();
				return Boolean.valueOf(Objects.equal(_name, name));
			}
		};
		EnumType _findFirst = IterableExtensions.<EnumType>findFirst(Iterables.<EnumType>filter(this.getTypes(), EnumType.class), _function);
		if (_findFirst != null) {
			_elvis = _findFirst;
		} else {
			Scope _parent = this.getParent();
			Type _lookupEnumType = null;
			if (_parent!=null) {
				_lookupEnumType=_parent.lookupEnumType(name);
			}
			_elvis = _lookupEnumType;
		}
		return _elvis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type lookupRecordType(final String name) {
		Type _elvis = null;
		final Function1<RecordType, Boolean> _function = new Function1<RecordType, Boolean>() {
			public Boolean apply(final RecordType it) {
				String _name = it.getName();
				return Boolean.valueOf(Objects.equal(_name, name));
			}
		};
		RecordType _findFirst = IterableExtensions.<RecordType>findFirst(Iterables.<RecordType>filter(this.getTypes(), RecordType.class), _function);
		if (_findFirst != null) {
			_elvis = _findFirst;
		} else {
			Scope _parent = this.getParent();
			Type _lookupRecordType = null;
			if (_parent!=null) {
				_lookupRecordType=_parent.lookupRecordType(name);
			}
			_elvis = _lookupRecordType;
		}
		return _elvis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type lookup(final Type typ) {
		Type _elvis = null;
		final Function1<Type, Boolean> _function = new Function1<Type, Boolean>() {
			public Boolean apply(final Type it) {
				return Boolean.valueOf(it.isEqual(typ));
			}
		};
		Type _findFirst = IterableExtensions.<Type>findFirst(this.getTypes(), _function);
		if (_findFirst != null) {
			_elvis = _findFirst;
		} else {
			Scope _parent = this.getParent();
			Type _lookup = null;
			if (_parent!=null) {
				_lookup=_parent.lookup(typ);
			}
			_elvis = _lookup;
		}
		return _elvis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void makeUnique(final Symbol sym, final Scope ref) {
		Symbol _lookup = ref.lookup(sym.getName());
		boolean _tripleNotEquals = (_lookup != null);
		if (_tripleNotEquals) {
			final String name = sym.getName();
			int counter = 0;
			while ((ref.lookup((name + Integer.valueOf(counter))) != null)) {
				counter++;
			}
			sym.setName((name + Integer.valueOf(counter)));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void makeUnique(final Symbol sym) {
		this.makeUnique(sym, this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void mergeWith(final Scope scope) {
		if ((scope != this)) {
			final Function1<Symbol, Boolean> _function = new Function1<Symbol, Boolean>() {
				public Boolean apply(final Symbol it) {
					Symbol _lookup = ScopeImpl.this.lookup(it.getName());
					return Boolean.valueOf((_lookup != null));
				}
			};
			final Consumer<Symbol> _function_1 = new Consumer<Symbol>() {
				public void accept(final Symbol it) {
					ScopeImpl.this.makeUnique(it);
				}
			};
			IterableExtensions.<Symbol>filter(XcoreEListExtensions.<Symbol>immutableCopy(scope.getSymbols()), _function).forEach(_function_1);
			this.getSymbols().addAll(scope.getSymbols());
			this.getTypes().addAll(scope.getTypes());
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scope copy(Scope scope) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol lookup(final String name) {
		Symbol _elvis = null;
		final Function1<Symbol, Boolean> _function = new Function1<Symbol, Boolean>() {
			public Boolean apply(final Symbol it) {
				String _name = it.getName();
				return Boolean.valueOf(Objects.equal(_name, name));
			}
		};
		Symbol _findFirst = IterableExtensions.<Symbol>findFirst(this.getSymbols(), _function);
		if (_findFirst != null) {
			_elvis = _findFirst;
		} else {
			Scope _parent = this.getParent();
			Symbol _lookup = null;
			if (_parent!=null) {
				_lookup=_parent.lookup(name);
			}
			_elvis = _lookup;
		}
		return _elvis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean hasInScope(final Symbol symbol) {
		boolean _xifexpression = false;
		final Function1<Symbol, Boolean> _function = new Function1<Symbol, Boolean>() {
			public Boolean apply(final Symbol it) {
				return Boolean.valueOf(Objects.equal(it, symbol));
			}
		};
		boolean _exists = IterableExtensions.<Symbol>exists(this.getSymbols(), _function);
		if (_exists) {
			_xifexpression = true;
		}
		else {
			boolean _xifexpression_1 = false;
			Scope _parent = this.getParent();
			boolean _tripleNotEquals = (_parent != null);
			if (_tripleNotEquals) {
				_xifexpression_1 = this.getParent().hasInScope(symbol);
			}
			else {
				_xifexpression_1 = false;
			}
			_xifexpression = _xifexpression_1;
		}
		return _xifexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void moveUp(final Symbol sym) {
		Scope _parent = this.getParent();
		boolean _tripleNotEquals = (_parent != null);
		if (_tripleNotEquals) {
			this.getSymbols().remove(sym);
			this.getParent().getSymbols().add(sym);
		}
		else {
			throw new RuntimeException((("Cannot move Symbol " + sym) + " because there is no parent scope"));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scope getRoot() {
		Scope current = this;
		while ((current.getParent() != null)) {
			current = current.getParent();
		}
		return current;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addSymbol(final Symbol symbol) {
		this.getSymbols().add(symbol);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeSymbol(final Symbol symbol) {
		this.getSymbols().remove(symbol);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final CoreVisitor visitor) {
		visitor.visitScope(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isGlobal() {
		Scope _parent = this.getParent();
		return (_parent == null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scope findFirstCommonScopeWith(final Scope a) {
		final BasicEList<Scope> thisParents = new BasicEList<Scope>();
		Scope current = this;
		while ((current != null)) {
			{
				thisParents.add(current);
				current = current.getParent();
			}
		}
		current = a;
		while (((current != null) && (!thisParents.contains(current)))) {
			current = current.getParent();
		}
		return current;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Symbol> lookupAllSymbols() {
		final LinkedHashMap<String, Symbol> m = new LinkedHashMap<String, Symbol>();
		EList<Symbol> _symbols = this.getSymbols();
		for (final Symbol s : _symbols) {
			m.put(s.getName(), s);
		}
		Scope _parent = this.getParent();
		boolean _tripleNotEquals = (_parent != null);
		if (_tripleNotEquals) {
			final EList<Symbol> upperScopeSymbols = this.getParent().lookupAllSymbols();
			for (final Symbol s_1 : upperScopeSymbols) {
				boolean _containsKey = m.containsKey(s_1.getName());
				boolean _not = (!_containsKey);
				if (_not) {
					m.put(s_1.getName(), s_1);
				}
			}
		}
		return ECollections.<Symbol>unmodifiableEList(ECollections.<Symbol>asEList(((Symbol[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(m.values(), Symbol.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.SCOPE__CONTAINER:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetContainer((ScopeContainer)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.SCOPE__TYPES:
				return ((InternalEList<?>)getTypes()).basicRemove(otherEnd, msgs);
			case CorePackage.SCOPE__SYMBOLS:
				return ((InternalEList<?>)getSymbols()).basicRemove(otherEnd, msgs);
			case CorePackage.SCOPE__CONTAINER:
				return basicSetContainer(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CorePackage.SCOPE__CONTAINER:
				return eInternalContainer().eInverseRemove(this, CorePackage.SCOPE_CONTAINER__SCOPE, ScopeContainer.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CorePackage.SCOPE__TYPES:
				return getTypes();
			case CorePackage.SCOPE__SYMBOLS:
				return getSymbols();
			case CorePackage.SCOPE__CONTAINER:
				if (resolve) return getContainer();
				return basicGetContainer();
			case CorePackage.SCOPE__PARENT:
				return getParent();
			case CorePackage.SCOPE__CHILDREN:
				return getChildren();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CorePackage.SCOPE__TYPES:
				getTypes().clear();
				getTypes().addAll((Collection<? extends Type>)newValue);
				return;
			case CorePackage.SCOPE__SYMBOLS:
				getSymbols().clear();
				getSymbols().addAll((Collection<? extends Symbol>)newValue);
				return;
			case CorePackage.SCOPE__CONTAINER:
				setContainer((ScopeContainer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CorePackage.SCOPE__TYPES:
				getTypes().clear();
				return;
			case CorePackage.SCOPE__SYMBOLS:
				getSymbols().clear();
				return;
			case CorePackage.SCOPE__CONTAINER:
				setContainer((ScopeContainer)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CorePackage.SCOPE__TYPES:
				return types != null && !types.isEmpty();
			case CorePackage.SCOPE__SYMBOLS:
				return symbols != null && !symbols.isEmpty();
			case CorePackage.SCOPE__CONTAINER:
				return basicGetContainer() != null;
			case CorePackage.SCOPE__PARENT:
				return getParent() != null;
			case CorePackage.SCOPE__CHILDREN:
				return !getChildren().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ScopeImpl
