/**
 */
package gecos.types.impl;

import com.google.common.base.Objects;

import fr.irisa.cairn.gecos.model.tools.utils.GecosCopier;

import gecos.annotations.impl.AnnotatedElementImpl;

import gecos.core.Scope;

import gecos.types.AliasType;
import gecos.types.ArrayType;
import gecos.types.BaseType;
import gecos.types.EnumType;
import gecos.types.FunctionType;
import gecos.types.PtrType;
import gecos.types.RecordType;
import gecos.types.SimdType;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import gecos.types.TypesPackage;
import gecos.types.TypesVisitor;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.impl.TypeImpl#isConstant <em>Constant</em>}</li>
 *   <li>{@link gecos.types.impl.TypeImpl#isVolatile <em>Volatile</em>}</li>
 *   <li>{@link gecos.types.impl.TypeImpl#getStorageClass <em>Storage Class</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class TypeImpl extends AnnotatedElementImpl implements Type {
	/**
	 * The default value of the '{@link #isConstant() <em>Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConstant()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CONSTANT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isConstant() <em>Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConstant()
	 * @generated
	 * @ordered
	 */
	protected boolean constant = CONSTANT_EDEFAULT;

	/**
	 * The default value of the '{@link #isVolatile() <em>Volatile</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isVolatile()
	 * @generated
	 * @ordered
	 */
	protected static final boolean VOLATILE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isVolatile() <em>Volatile</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isVolatile()
	 * @generated
	 * @ordered
	 */
	protected boolean volatile_ = VOLATILE_EDEFAULT;

	/**
	 * The default value of the '{@link #getStorageClass() <em>Storage Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStorageClass()
	 * @generated
	 * @ordered
	 */
	protected static final StorageClassSpecifiers STORAGE_CLASS_EDEFAULT = StorageClassSpecifiers.NONE;

	/**
	 * The cached value of the '{@link #getStorageClass() <em>Storage Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStorageClass()
	 * @generated
	 * @ordered
	 */
	protected StorageClassSpecifiers storageClass = STORAGE_CLASS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypesPackage.Literals.TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isConstant() {
		return constant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstant(boolean newConstant) {
		boolean oldConstant = constant;
		constant = newConstant;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.TYPE__CONSTANT, oldConstant, constant));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isVolatile() {
		return volatile_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVolatile(boolean newVolatile) {
		boolean oldVolatile = volatile_;
		volatile_ = newVolatile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.TYPE__VOLATILE, oldVolatile, volatile_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StorageClassSpecifiers getStorageClass() {
		return storageClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStorageClass(StorageClassSpecifiers newStorageClass) {
		StorageClassSpecifiers oldStorageClass = storageClass;
		storageClass = newStorageClass == null ? STORAGE_CLASS_EDEFAULT : newStorageClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.TYPE__STORAGE_CLASS, oldStorageClass, storageClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInnermostStorageClass(final StorageClassSpecifiers innermostStorage) {
		this.setStorageClass(innermostStorage);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getSize() {
		String _simpleName = this.getClass().getSimpleName();
		String _plus = ("No default size for abstract type class (" + _simpleName);
		String _plus_1 = (_plus + ")");
		throw new UnsupportedOperationException(_plus_1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(TypesVisitor visitor) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scope getContainingScope() {
		EObject _eContainer = this.eContainer();
		if ((_eContainer instanceof Scope)) {
			EObject _eContainer_1 = this.eContainer();
			return ((Scope) _eContainer_1);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		final StringBuffer result = new StringBuffer();
		int _value = this.getStorageClass().getValue();
		boolean _notEquals = (_value != StorageClassSpecifiers.NONE_VALUE);
		if (_notEquals) {
			result.append(this.getStorageClass().getLiteral().toLowerCase()).append(" ");
		}
		boolean _isConstant = this.isConstant();
		if (_isConstant) {
			result.append("const ");
		}
		boolean _isVolatile = this.isVolatile();
		if (_isVolatile) {
			result.append("volatile ");
		}
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public <T extends Type> T copy() {
		final GecosCopier copier = new GecosCopier();
		EObject _copy = copier.copy(this);
		final T t = ((T) _copy);
		copier.copyReferences();
		return t;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void installOn(final Scope scope) {
		scope.getTypes().add(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isStatic() {
		StorageClassSpecifiers _storageClass = this.getStorageClass();
		return Objects.equal(_storageClass, StorageClassSpecifiers.STATIC);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRegister() {
		StorageClassSpecifiers _storageClass = this.getStorageClass();
		return Objects.equal(_storageClass, StorageClassSpecifiers.REGISTER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isExtern() {
		StorageClassSpecifiers _storageClass = this.getStorageClass();
		return Objects.equal(_storageClass, StorageClassSpecifiers.EXTERN);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAuto() {
		StorageClassSpecifiers _storageClass = this.getStorageClass();
		return Objects.equal(_storageClass, StorageClassSpecifiers.AUTO);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseType asBase() {
		if ((this instanceof BaseType)) {
			return ((BaseType) this);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArrayType asArray() {
		if ((this instanceof ArrayType)) {
			return ((ArrayType) this);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PtrType asPointer() {
		if ((this instanceof PtrType)) {
			return ((PtrType) this);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionType asFunction() {
		if ((this instanceof FunctionType)) {
			return ((FunctionType) this);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordType asRecord() {
		if ((this instanceof RecordType)) {
			return ((RecordType) this);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AliasType asAlias() {
		if ((this instanceof AliasType)) {
			return ((AliasType) this);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumType asEnum() {
		if ((this instanceof EnumType)) {
			return ((EnumType) this);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimdType asSimd() {
		if ((this instanceof SimdType)) {
			return ((SimdType) this);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isArray() {
		return (this instanceof ArrayType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isPointer() {
		return (this instanceof PtrType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEqual(final Type other) {
		return this.isEqual(other, false, false, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEqual(final Type other, final boolean ignoreStorageClass, final boolean ignoreConst, final boolean ignoreVolatile) {
		if ((this == other)) {
			return true;
		}
		return (((ignoreConst || (Boolean.valueOf(this.isConstant()) == Boolean.valueOf(other.isConstant()))) && (ignoreVolatile || (Boolean.valueOf(this.isVolatile()) == Boolean.valueOf(other.isVolatile())))) && (ignoreStorageClass || this.getStorageClass().equals(other.getStorageClass())));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSimilar(final Type other) {
		return this.isSimilar(other, false, false, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSimilar(final Type other, final boolean ignoreStorageClass, final boolean ignoreConst, final boolean ignoreVolatile) {
		boolean _equals = Objects.equal(other, this);
		if (_equals) {
			return true;
		}
		AliasType _asAlias = other.asAlias();
		boolean _tripleNotEquals = (_asAlias != null);
		if (_tripleNotEquals) {
			return other.isSimilar(this, ignoreStorageClass, ignoreConst, ignoreVolatile);
		}
		else {
			return this.isEqual(other, ignoreStorageClass, ignoreConst, ignoreVolatile);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TypesPackage.TYPE__CONSTANT:
				return isConstant();
			case TypesPackage.TYPE__VOLATILE:
				return isVolatile();
			case TypesPackage.TYPE__STORAGE_CLASS:
				return getStorageClass();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TypesPackage.TYPE__CONSTANT:
				setConstant((Boolean)newValue);
				return;
			case TypesPackage.TYPE__VOLATILE:
				setVolatile((Boolean)newValue);
				return;
			case TypesPackage.TYPE__STORAGE_CLASS:
				setStorageClass((StorageClassSpecifiers)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TypesPackage.TYPE__CONSTANT:
				setConstant(CONSTANT_EDEFAULT);
				return;
			case TypesPackage.TYPE__VOLATILE:
				setVolatile(VOLATILE_EDEFAULT);
				return;
			case TypesPackage.TYPE__STORAGE_CLASS:
				setStorageClass(STORAGE_CLASS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TypesPackage.TYPE__CONSTANT:
				return constant != CONSTANT_EDEFAULT;
			case TypesPackage.TYPE__VOLATILE:
				return volatile_ != VOLATILE_EDEFAULT;
			case TypesPackage.TYPE__STORAGE_CLASS:
				return storageClass != STORAGE_CLASS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //TypeImpl
