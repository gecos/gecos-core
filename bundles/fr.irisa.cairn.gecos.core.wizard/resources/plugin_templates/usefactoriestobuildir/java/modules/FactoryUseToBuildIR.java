package $packageName$.modules;

import java.util.ArrayList;

import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import gecos.blocks.BasicBlock;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.core.ParameterSymbol;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;
import gecos.types.FunctionType;


/**
 * C source code build 
 * 
 * void dummy(){
 * 	int i;
 * 	int a[10];
 * 	for(i = 0 ; i < 10 ; i = i + 1){
 * 		if(i < 5)
 * 			a[i] = 0;
 * 		else
 * 			a[i] = 1;
 * 	}
 * }
 */

/**
 * GeCoS module implementation build manually a GeCoS IR thanks to GeCoS factoris
 */
public class FactoryUseToBuildIR {

	public GecosProject compute(){
		GecosProject project = buildDummyProject();
		return project;
	}	
		
	private GecosProject buildDummyProject(){
		// Procedure set creation
		ProcedureSet ps = GecosUserCoreFactory.procedureSet();
		GecosUserTypeFactory.setScope(ps.getScope()); // Need to precise in which scope the new type are registered
		
		// Creation of the procedure called dummy and its body
		ProcedureSymbol psymbol = GecosUserCoreFactory.procSymbol("dummy", GecosUserTypeFactory.VOID(), new ArrayList<ParameterSymbol>());
		CompositeBlock mainblock = GecosUserBlockFactory.CompositeBlock(); // A composite block can contained any type of block and is a scope container
		GecosUserCoreFactory.proc(ps, psymbol, mainblock);
		
		// Fill the body block of the dummy procedure
		GecosUserTypeFactory.setScope(mainblock.getScope()); // Changing scope
		
		// Symbol creation and adding those symbol in the main block
		Symbol isymbol = GecosUserCoreFactory.symbol("i", GecosUserTypeFactory.INT());
		Symbol asymbol = GecosUserCoreFactory.symbol("i", GecosUserTypeFactory.ARRAY(GecosUserTypeFactory.INT(), GecosUserInstructionFactory.Int(10)));
		mainblock.addSymbol(isymbol);
		mainblock.addSymbol(asymbol);
		
		// Creation of the then block and else block and fill it with instruction
		// We just need to create basic block because this block just need contained some instruction
		BasicBlock thenBlock = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.set(
				GecosUserInstructionFactory.array(asymbol, GecosUserInstructionFactory.symbref(isymbol)),
				GecosUserInstructionFactory.Int(0)));
		BasicBlock elseBlock = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.set(
				GecosUserInstructionFactory.array(asymbol, GecosUserInstructionFactory.symbref(isymbol)),
				GecosUserInstructionFactory.Int(1)));
		
		// Creation of the if block and fill it with then block and else block
		IfBlock ifBlock = GecosUserBlockFactory.IfThenElse(
				GecosUserInstructionFactory.lt(GecosUserInstructionFactory.symbref(isymbol), GecosUserInstructionFactory.Int(5)), thenBlock, elseBlock);
		
		// Creation of the for block and fill it with if block
		ForBlock forBlock = GecosUserBlockFactory.For(isymbol, 0, 10, 1, ifBlock);
		
		// Add the for block into the main block
		mainblock.addChildren(forBlock);
		
		// Associate the procedure set to a Gecos source file with a name and create the gecos project fill with gecos source file
		GecosSourceFile sourcefile = GecosUserCoreFactory.source("dummy.c", ps);
		GecosProject project = GecosUserCoreFactory.projectSources("dummy_project", sourcefile);
		
		return project;
	}
}
