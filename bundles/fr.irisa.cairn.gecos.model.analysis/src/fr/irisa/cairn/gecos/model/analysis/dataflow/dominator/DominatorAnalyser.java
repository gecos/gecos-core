/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.analysis.dataflow.dominator;

import gecos.blocks.BasicBlock;
import gecos.blocks.ControlEdge;
import gecos.core.Procedure;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/** 
 *  Block Dominance Computation.
 * Mostly taken from Muchnick
 */

public class DominatorAnalyser {
	
	protected Procedure proc;
	protected Map<BasicBlock, List<BasicBlock>> dominators;
	protected Map<BasicBlock, BasicBlock> immediateDominators;
	protected DominatorTree dominatorTree;
	protected Map<BasicBlock, List<BasicBlock>> dominanceFrontiers;
	private boolean VERBOSE=false;


	public DominatorAnalyser(Procedure proc) {
		this.proc = proc;
	}

	public Map<BasicBlock, List<BasicBlock>> getDominators() {
		if (dominators == null)
			computeDominators();
		return dominators;
	}

	public Map<BasicBlock, BasicBlock> getImmediateDominators() {
		if (immediateDominators == null)
			computeImmediateDominators();
		return immediateDominators;
	}

	public DominatorTree getDominatorTree() {
		if (dominatorTree == null)
			computeDominatorTree();
		return dominatorTree;
	}

	public Map<BasicBlock, List<BasicBlock>> getDominanceFrontiers() {
		if (dominanceFrontiers == null)
			computeDominanceFrontiers();
		return dominanceFrontiers;
	}


	private void computeDominators() {

		/*
		 * Initialize the dominators~: start block
		 * is only dominated by itself, every other nodes
		 * is potentially dominated by every nodes.
		 */
		dominators = new LinkedHashMap<BasicBlock, List<BasicBlock>>();
		
		List<BasicBlock> N = proc.getBasicBlocks();
		for (BasicBlock bb : N) {
			
			List<BasicBlock> e;
			if (bb == proc.getStart()) {
				e = new ArrayList<BasicBlock>(1);
				e.add(proc.getStart());
			} else {
				e = new ArrayList<BasicBlock>(N);
//				e.addAll(N);
			}
			if (VERBOSE) System.out.println("Adding dominators "+e+" to "+bb);
//			debug("Adding dominators "+debug(e)+" to "+bb);
			dominators.put(bb, e);
		}

		if (VERBOSE) System.out.println("");

		boolean change = true;
		do {
			change = false;
			for (BasicBlock bb : N) {
				if (bb == proc.getStart())
					continue;
				if (VERBOSE) System.out.println("Building new List for "+bb);
				List<BasicBlock> T = new ArrayList<BasicBlock>(N);
				Iterator<ControlEdge> iter = bb.getInEdges().iterator();
				while (iter.hasNext()) {
					ControlEdge edge = iter.next();
					List<BasicBlock> List = dominators.get(edge.getFrom());
					T.retainAll(List);
					if (VERBOSE) 
						if (VERBOSE) System.out.println("\t- Analyzing edge "+edge+" -> dominator List is now "+T);
				}
				T.add(bb);
				if (! T.equals(dominators.get(bb))) {
					change = true;
					dominators.put(bb, T);
				}
			}
		} while (change);
	}

	/** Immediate Dominator.
	 *  Could use Tarjan algorithm
	 */
	public void computeImmediateDominators() {

		getDominators();

		// Step 1 : tmp = strict dominators
		Map<BasicBlock, List<BasicBlock>> tmp = new LinkedHashMap<BasicBlock, List<BasicBlock>>();
		List<BasicBlock> N = proc.getBasicBlocks();
		for (BasicBlock bb : N) {
			List<BasicBlock> List = dominators.get(bb);
			List<BasicBlock> s = new ArrayList<BasicBlock>(List);
			s.remove(bb);
			tmp.put(bb, s);
		}

		// Step 2 : tmp = nearest dominators
		for (BasicBlock n : N) {
			if (n == proc.getStart()) continue;

			// duplicate the List to prevent concurrent remove
			Iterator<BasicBlock> k = (new ArrayList<BasicBlock>(tmp.get(n))).iterator();
			while (k.hasNext()) {
				BasicBlock s = k.next();
				Iterator<BasicBlock> j = (tmp.get(n)).iterator();
				while (j.hasNext()) {
					BasicBlock t = j.next();
					if (t == s) continue;
	
					if ((tmp.get(s)).contains(t)) {
						j.remove();
					}
				}
			}
		}
		// Step 3 : take one of the nearest dominator
		immediateDominators = new LinkedHashMap<BasicBlock, BasicBlock>();
		for (BasicBlock b : tmp.keySet()) {
			List<BasicBlock> s = tmp.get(b);
			if (s.size() > 0) {
				BasicBlock[] t = s.toArray(new BasicBlock[s.size()]);
				immediateDominators.put(b, t[0]);
			}
		}
	}

	private void computeDominatorTree() {

		getImmediateDominators();

		Map<BasicBlock, DominatorTreeNode> nodes = new LinkedHashMap<BasicBlock, DominatorTreeNode>();
		for (BasicBlock b : proc.getBasicBlocks()) {
			nodes.put(b, new DominatorTreeNode(b));
		}

		for (BasicBlock to : immediateDominators.keySet()) {
			BasicBlock from = immediateDominators.get(to);
			nodes.get(from).addChild(nodes.get(to));
		}

		DominatorTreeNode root = nodes.get(proc.getStart());
		dominatorTree = new DominatorTree(nodes, root);
	}

	private void computeDominanceFrontiers() {

		getDominatorTree();

		dominanceFrontiers = new LinkedHashMap<BasicBlock, List<BasicBlock>>();

		PostOrderVisitor orderer = new PostOrderVisitor();
		BasicBlock[] P = orderer.process(dominatorTree.root());

		for (BasicBlock bb : P) {
			List<BasicBlock> df = new ArrayList<BasicBlock>();
			dominanceFrontiers.put(bb, df);

			Iterator<ControlEdge> j = bb.getOutEdges().iterator();
			while (j.hasNext()) {
				// XXX : Generate an exception
				ControlEdge edge = j.next();
				BasicBlock to = edge.getTo();
				assert(to!=null);
				BasicBlock basicBlock = immediateDominators.get(to);
				if(basicBlock==null) {
					//throw new RuntimeException("BB "+to+" reached by "+bb+" has no entry in immediateDominatorTable() for "+proc.getSymbol().getName());
					System.err.println("BB "+to+" reached by "+bb+" has no entry in immediateDominatorTable() for "+proc.getSymbol().getName());
					df.add(to);
				} else {
					if (! basicBlock.equals(bb)) {
						df.add(to);
					}
				}
			}

			for (DominatorTreeNode n : dominatorTree.node(bb).children()) {
				BasicBlock z = n.block();
				for (BasicBlock y : dominanceFrontiers.get(z)) {
					BasicBlock basicBlock = immediateDominators.get(y);
					if (basicBlock==null) {
						df.add(y);
					} else if (! basicBlock.equals(bb)) {
						df.add(y);
					}
				}
			}
		}
	}

	static void print(Map<BasicBlock, BasicBlock> t) {
		System.out.println("=========================");
		Iterator<BasicBlock> i = t.keySet().iterator();
		while(i.hasNext()) {
			BasicBlock b =  i.next();
			System.out.print("B"+b.getNumber()+" -> ");
			BasicBlock j = t.get(b);
			System.out.println("B"+j.getNumber());
		}
	}

	static void print2(Map<BasicBlock, ?> t) {
		System.out.println("=========================");
		Iterator<BasicBlock> i = t.keySet().iterator();
		while(i.hasNext()) {
			BasicBlock b =  i.next();
			System.out.print("B"+b.getNumber()+" -> {");
			Iterator<?> k = ((List<?>)t.get(b)).iterator();
			while (k.hasNext()) {
				BasicBlock j = (BasicBlock)k.next();
				System.out.print("B"+j.getNumber()+" ");
			}
			System.out.println("}");
		}
	}

	public boolean isVERBOSE() {
		return VERBOSE;
	}

	public void ListVERBOSE(boolean vERBOSE) {
		VERBOSE = vERBOSE;
	}
}
