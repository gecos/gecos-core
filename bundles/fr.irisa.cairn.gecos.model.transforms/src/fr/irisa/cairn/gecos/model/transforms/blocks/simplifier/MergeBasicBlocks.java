package fr.irisa.cairn.gecos.model.transforms.blocks.simplifier;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.tools.controlflow.BuildControlFlow;
import fr.irisa.cairn.gecos.model.tools.controlflow.ClearControlFlow;
import fr.irisa.cairn.gecos.model.tools.visitors.GecosDefaultVisitor;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.annotations.AnnotationKeys;
import gecos.annotations.PragmaAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.gecosproject.GecosProject;
import gecos.instrs.BranchInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.LabelInstruction;

/**
 * 
 * Merges consecutive BasicBlocks into one BasicBlock, if the branches allow it,
 * and removes empty BasicBlocks. Ignore Procedure.getStart() and
 * Procedure.getEnd() blocks. Also doesn't handle edges, therefore it is safer
 * to call {@link ClearControlFlow} before and {@link BuildControlFlow} after
 * this pass.
 * 
 * @author amorvan
 *
 */
public class MergeBasicBlocks extends GecosDefaultVisitor {
	
	private GecosProject proj;

	public MergeBasicBlocks() {
		super(false,false,false);
		this.proj = null;
	}
	
	public MergeBasicBlocks(GecosProject p) {
		super(false,false,false);
		this.proj = p;
	}
	
	public void compute() {
		if (proj != null)
			proj.accept(this);
	}
	
	public void visitCompositeBlock(CompositeBlock c) {
		super.visitCompositeBlock(c);
		
		EList<Block> children = c.getChildren();
		List<BasicBlock> toRemove = new ArrayList<>();
		BasicBlock previous = null;
		
		//iterate over all the basicBlocks of a composite block
		for (int i = 0; i < children.size(); i++) {
			Block block = children.get(i);
			if (block instanceof BasicBlock) {
				BasicBlock bb = (BasicBlock) block;

				// specific filter, like annotations
				if (filter(bb)) {
					previous = null;
					continue;
				}
				
				// remove the basic block if it is empty
				if (bb != null && bb.getInstructionCount() == 0) {
					toRemove.add(bb);
					continue;
				}
				
				// merge the previous eligible basic block with the current one
				// only if the first instruction is not a label
				if (previous != null && !(bb.getFirstInstruction() instanceof LabelInstruction)) {
					Instruction[] instrs = previous.getInstructions().toArray(new Instruction[previous.getInstructions().size()]);
					for (int j = instrs.length-1; j >= 0; j--) 
						bb.getInstructions().add(0, instrs[j]);
					GecosUserAnnotationFactory.mergePragmas(previous, bb);
					toRemove.add(previous);
				}
				
				// if there is no branch instruction, the current basic block is
				// eligible for merge 
				EList<BranchInstruction> branchs = EMFUtils.eAllContentsFirstInstancesOf(bb, BranchInstruction.class);
				if (branchs.size() == 0)
					previous= bb;
				else 
					previous = null;
			} else {
				// reset previous is the block is not a basic block
				previous = null;
			}
		}
		children.removeAll(toRemove);
	}

	private boolean filter(BasicBlock bb) {
		// check for ignore_merge pragma
		// note: usualy pragmas are attached to the instructions, not the
		// basicblock.
		PragmaAnnotation pragma = bb.getPragma();
		if (pragma != null)
			for (String s : pragma.getContent())
				if (s.contains("ignore_merge"))
					return true;
		
		// for now, the pass can only merge pragma annotation. If there are
		// other kind of annotations, skip the basic bloc;
		if (bb.getAnnotations().size() > ((bb.getAnnotations().contains(AnnotationKeys.PRAGMA_ANNOTATION_KEY.getLiteral()))?1:0))
			return true;
		
		//do not touch start/end basic blocks of procedures
		 if (bb.getContainingProcedure() != null && (bb == bb.getContainingProcedure().getStart() || bb == bb.getContainingProcedure().getEnd()))
			 return true;
		return false;
	}

}
