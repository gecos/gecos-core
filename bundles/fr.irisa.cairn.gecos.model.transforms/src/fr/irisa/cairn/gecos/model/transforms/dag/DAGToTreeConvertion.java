package fr.irisa.cairn.gecos.model.transforms.dag;

import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDefaultVisitor;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import gecos.blocks.BasicBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.dag.DAGInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.RetInstruction;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

/**
 * Transform all {@link DAGInstruction} into a sequence of tree instructions.
 * 
 * @author antoine
 * 
 */
@GSModule("Revert back the DAG representation, constructed by\n"
		+ "the 'GecosTreeToDAGIRConversion' module, into instructions Tree.")
public class DAGToTreeConvertion extends GecosBlocksInstructionsDefaultVisitor {
	
	private static final boolean DEBUG = false;
	
	private static final void debug(String string) {
		if (DEBUG) {
			System.out.println(string);
		}
	}

	private int maxDepth;
	protected DAGToTreeSequentialScan builder;
	protected EObject target;
	
	@GSModuleConstructor(
		"-arg1: Object to convert. It should be either \n"
	  + "   a GecosProject, ProcedureSet or a Procedure.\n"
	  + "-arg2: positive integer specifies the maximum Instruction depth\n"
	  + "   before making a cut.")
	public DAGToTreeConvertion(EObject target, int maxDepth) {
		this.maxDepth = maxDepth;
		this.target = target;
	}

	@GSModuleConstructor(
		"-arg1: Object to convert. It should be either \n"
	  + "   a GecosProject, ProcedureSet or a Procedure.\n"
	  + "\nDefault to Infinite depth (i.e. no implicit cut will be made).")
	public DAGToTreeConvertion(EObject target) {
		this(target, DAGToTreeSequentialScan.DEPTH_INFINITE);
	}

	//FIXME if target is a block.. Also avoid using EMFUtils
	public void compute() {
		List<Procedure> procs =  EMFUtils.<Procedure>eAllContentsFirstInstancesOf(target, Procedure.class);
		if(target instanceof Procedure)
			procs.add((Procedure) target);
		for(Procedure proc : procs) {
			visit(proc);
		}
	}

	public void visit(ProcedureSet ps) {
		for (Procedure p :new ArrayList<Procedure>(ps.listProcedures())) {
			visit(p);
		}
	}

	@Override
	public void visit(Procedure proc) {
		createInstructionBuilder(/*proc*/);
		super.visit(proc);
	}

	protected void createInstructionBuilder(/*Procedure proc*/) {
		builder = new DAGToTreeSequentialScan(maxDepth);
	}

	@Override
	public void visitBasicBlock(BasicBlock bb) {
		if(builder == null)
			createInstructionBuilder();
		
		if(DEBUG) debug("DAG2Tree basic block: " + bb);
		for (Instruction i : new ArrayList<Instruction>(bb.getInstructions())) {
			if (i instanceof DAGInstruction) {
				if(DEBUG) debug("   Convert dag: " + i);
				List<Instruction> instructions = builder.buildInstructionsList((DAGInstruction) i);
				int position = bb.getInstructions().indexOf(i);
				bb.removeInstruction(i);
				
				if(DEBUG) debug("   |-> instructions: ");
				RetInstruction returnInst = null;

				int j=0;
				for(Instruction inst : instructions) {
					if(DEBUG) debug("      " + inst);
					if(inst instanceof RetInstruction) {
						returnInst = (RetInstruction) inst;
						continue;
					}
					bb.getInstructions().add(position+j, inst);
//					bb.getInstructions().add(position+j, instructions.get(instructions.size()-1-j));
					j++;
				}
				
				/* XXX: assume that return instruction (if available) 
				 * is always the last instruction of the basic block
				 */
				if(returnInst != null)
					bb.getInstructions().add(position+j, returnInst);
			}
		}
	}
}
