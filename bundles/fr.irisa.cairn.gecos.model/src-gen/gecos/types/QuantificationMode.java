/**
 */
package gecos.types;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Quantification Mode</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see gecos.types.TypesPackage#getQuantificationMode()
 * @model
 * @generated
 */
public enum QuantificationMode implements Enumerator {
	/**
	 * The '<em><b>AC TRN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AC_TRN_VALUE
	 * @generated
	 * @ordered
	 */
	AC_TRN(0, "AC_TRN", "AC_TRN"),

	/**
	 * The '<em><b>AC TRN ZERO</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AC_TRN_ZERO_VALUE
	 * @generated
	 * @ordered
	 */
	AC_TRN_ZERO(1, "AC_TRN_ZERO", "AC_TRN_ZERO"),

	/**
	 * The '<em><b>AC RND</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AC_RND_VALUE
	 * @generated
	 * @ordered
	 */
	AC_RND(2, "AC_RND", "AC_RND"),

	/**
	 * The '<em><b>AC RND ZERO</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AC_RND_ZERO_VALUE
	 * @generated
	 * @ordered
	 */
	AC_RND_ZERO(3, "AC_RND_ZERO", "AC_RND_ZERO"),

	/**
	 * The '<em><b>AC RND INF</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AC_RND_INF_VALUE
	 * @generated
	 * @ordered
	 */
	AC_RND_INF(4, "AC_RND_INF", "AC_RND_INF"),

	/**
	 * The '<em><b>AC RND MIN INF</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AC_RND_MIN_INF_VALUE
	 * @generated
	 * @ordered
	 */
	AC_RND_MIN_INF(5, "AC_RND_MIN_INF", "AC_RND_MIN_INF"),

	/**
	 * The '<em><b>AC RND CONV</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AC_RND_CONV_VALUE
	 * @generated
	 * @ordered
	 */
	AC_RND_CONV(6, "AC_RND_CONV", "AC_RND_CONV");

	/**
	 * The '<em><b>AC TRN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>AC TRN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AC_TRN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int AC_TRN_VALUE = 0;

	/**
	 * The '<em><b>AC TRN ZERO</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>AC TRN ZERO</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AC_TRN_ZERO
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int AC_TRN_ZERO_VALUE = 1;

	/**
	 * The '<em><b>AC RND</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>AC RND</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AC_RND
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int AC_RND_VALUE = 2;

	/**
	 * The '<em><b>AC RND ZERO</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>AC RND ZERO</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AC_RND_ZERO
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int AC_RND_ZERO_VALUE = 3;

	/**
	 * The '<em><b>AC RND INF</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>AC RND INF</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AC_RND_INF
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int AC_RND_INF_VALUE = 4;

	/**
	 * The '<em><b>AC RND MIN INF</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>AC RND MIN INF</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AC_RND_MIN_INF
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int AC_RND_MIN_INF_VALUE = 5;

	/**
	 * The '<em><b>AC RND CONV</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>AC RND CONV</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AC_RND_CONV
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int AC_RND_CONV_VALUE = 6;

	/**
	 * An array of all the '<em><b>Quantification Mode</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final QuantificationMode[] VALUES_ARRAY =
		new QuantificationMode[] {
			AC_TRN,
			AC_TRN_ZERO,
			AC_RND,
			AC_RND_ZERO,
			AC_RND_INF,
			AC_RND_MIN_INF,
			AC_RND_CONV,
		};

	/**
	 * A public read-only list of all the '<em><b>Quantification Mode</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<QuantificationMode> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Quantification Mode</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static QuantificationMode get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			QuantificationMode result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Quantification Mode</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static QuantificationMode getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			QuantificationMode result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Quantification Mode</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static QuantificationMode get(int value) {
		switch (value) {
			case AC_TRN_VALUE: return AC_TRN;
			case AC_TRN_ZERO_VALUE: return AC_TRN_ZERO;
			case AC_RND_VALUE: return AC_RND;
			case AC_RND_ZERO_VALUE: return AC_RND_ZERO;
			case AC_RND_INF_VALUE: return AC_RND_INF;
			case AC_RND_MIN_INF_VALUE: return AC_RND_MIN_INF;
			case AC_RND_CONV_VALUE: return AC_RND_CONV;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private QuantificationMode(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //QuantificationMode
