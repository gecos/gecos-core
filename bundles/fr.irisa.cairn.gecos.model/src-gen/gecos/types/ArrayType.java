/**
 */
package gecos.types;

import gecos.instrs.Instruction;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Array Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.ArrayType#getBase <em>Base</em>}</li>
 *   <li>{@link gecos.types.ArrayType#getLower <em>Lower</em>}</li>
 *   <li>{@link gecos.types.ArrayType#getUpper <em>Upper</em>}</li>
 *   <li>{@link gecos.types.ArrayType#getSizeExpr <em>Size Expr</em>}</li>
 * </ul>
 *
 * @see gecos.types.TypesPackage#getArrayType()
 * @model
 * @generated
 */
public interface ArrayType extends Type, TypesVisitable {
	/**
	 * Returns the value of the '<em><b>Base</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base</em>' reference.
	 * @see #setBase(Type)
	 * @see gecos.types.TypesPackage#getArrayType_Base()
	 * @model resolveProxies="false"
	 * @generated
	 */
	Type getBase();

	/**
	 * Sets the value of the '{@link gecos.types.ArrayType#getBase <em>Base</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base</em>' reference.
	 * @see #getBase()
	 * @generated
	 */
	void setBase(Type value);

	/**
	 * Returns the value of the '<em><b>Lower</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lower</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lower</em>' attribute.
	 * @see #setLower(long)
	 * @see gecos.types.TypesPackage#getArrayType_Lower()
	 * @model unique="false" dataType="gecos.types.long"
	 * @generated
	 */
	long getLower();

	/**
	 * Sets the value of the '{@link gecos.types.ArrayType#getLower <em>Lower</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lower</em>' attribute.
	 * @see #getLower()
	 * @generated
	 */
	void setLower(long value);

	/**
	 * Returns the value of the '<em><b>Upper</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Upper</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Upper</em>' attribute.
	 * @see #setUpper(long)
	 * @see gecos.types.TypesPackage#getArrayType_Upper()
	 * @model unique="false" dataType="gecos.types.long"
	 * @generated
	 */
	long getUpper();

	/**
	 * Sets the value of the '{@link gecos.types.ArrayType#getUpper <em>Upper</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Upper</em>' attribute.
	 * @see #getUpper()
	 * @generated
	 */
	void setUpper(long value);

	/**
	 * Returns the value of the '<em><b>Size Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Size Expr</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Size Expr</em>' containment reference.
	 * @see #setSizeExpr(Instruction)
	 * @see gecos.types.TypesPackage#getArrayType_SizeExpr()
	 * @model containment="true"
	 * @generated
	 */
	Instruction getSizeExpr();

	/**
	 * Sets the value of the '{@link gecos.types.ArrayType#getSizeExpr <em>Size Expr</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Size Expr</em>' containment reference.
	 * @see #getSizeExpr()
	 * @generated
	 */
	void setSizeExpr(Instruction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitArrayType(this);'"
	 * @generated
	 */
	void accept(TypesVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return the number of {@link ArrayType} in the 'base' hierarchy.
	 * <pre>
	 * e.g.: A.getNbDims() = 1 for: A[1] or *(A[1]), and 2 for A[1][2] or ***(A[1][2])
	 * </pre>
	 * <!-- end-model-doc -->
	 * @model kind="operation" dataType="gecos.types.int" unique="false" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int depth = 1;\n&lt;%gecos.types.Type%&gt; tmp = this.getBase();\nwhile (((tmp instanceof &lt;%gecos.types.ArrayType%&gt;) &amp;&amp; (!(tmp instanceof &lt;%gecos.types.ACType%&gt;))))\n{\n\t{\n\t\ttmp = ((&lt;%gecos.types.ArrayType%&gt;) tmp).getBase();\n\t\tdepth++;\n\t}\n}\nreturn depth;'"
	 * @generated
	 */
	int getNbDims();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return the innermost type of this array.
	 * <pre>
	 * e.g: (int [][]).getInnermostBase() returns (int) whereas getBase() simply returns (int [])
	 * </pre>
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.Type%&gt; base = this.getBase();\nwhile (((base instanceof &lt;%gecos.types.ArrayType%&gt;) &amp;&amp; (!(base instanceof &lt;%gecos.types.ACType%&gt;))))\n{\n\tbase = ((&lt;%gecos.types.ArrayType%&gt;) base).getBase();\n}\nreturn base;'"
	 * @generated
	 */
	Type getInnermostBase();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Set the innermost type of this pointer.
	 * <pre>
	 * e.g: (int [][]).setInnermostBase(float) modifies this to (float [][])
	 * </pre>
	 * <!-- end-model-doc -->
	 * @model innermostBaseUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.types.Type%&gt; base = this.getInnermostBase();\n&lt;%org.eclipse.emf.ecore.EObject%&gt; _eContainer = base.eContainer();\nboolean _not = (!(_eContainer instanceof &lt;%gecos.types.ArrayType%&gt;));\nif (_not)\n{\n\tthrow new &lt;%java.lang.RuntimeException%&gt;();\n}\n&lt;%org.eclipse.emf.ecore.EObject%&gt; _eContainer_1 = base.eContainer();\n((&lt;%gecos.types.ArrayType%&gt;) _eContainer_1).setBase(innermostBase);'"
	 * @generated
	 */
	void setInnermostBase(Type innermostBase);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%java.lang.StringBuffer%&gt; str = new &lt;%java.lang.StringBuffer%&gt;();\nstr.append(\"[\");\n&lt;%gecos.instrs.Instruction%&gt; _sizeExpr = this.getSizeExpr();\nboolean _tripleNotEquals = (_sizeExpr != null);\nif (_tripleNotEquals)\n{\n\tstr.append(this.getSizeExpr());\n}\nstr.append(\"](\");\n&lt;%java.lang.Object%&gt; _elvis = null;\n&lt;%gecos.types.Type%&gt; _base = this.getBase();\nif (_base != null)\n{\n\t_elvis = _base;\n} else\n{\n\t_elvis = \"?\";\n}\nstr.append(_elvis);\nstr.append(\")\");\nreturn str.toString();'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.boolean" unique="false" otherUnique="false" ignoreStorageClassDataType="gecos.types.boolean" ignoreStorageClassUnique="false" ignoreConstDataType="gecos.types.boolean" ignoreConstUnique="false" ignoreVolatileDataType="gecos.types.boolean" ignoreVolatileUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((other == this))\n{\n\treturn true;\n}\nif ((other instanceof &lt;%gecos.types.ArrayType%&gt;))\n{\n\tfinal &lt;%gecos.types.ArrayType%&gt; arrayType = ((&lt;%gecos.types.ArrayType%&gt;)other);\n\tboolean sameBaseType = false;\n\tif (((arrayType.getBase() == null) &amp;&amp; (this.getBase() == null)))\n\t{\n\t\tsameBaseType = true;\n\t}\n\telse\n\t{\n\t\t&lt;%gecos.types.Type%&gt; _base = arrayType.getBase();\n\t\tboolean _tripleEquals = (_base == null);\n\t\t&lt;%gecos.types.Type%&gt; _base_1 = this.getBase();\n\t\tboolean _tripleEquals_1 = (_base_1 == null);\n\t\tboolean _xor = (_tripleEquals ^ _tripleEquals_1);\n\t\tif (_xor)\n\t\t{\n\t\t\treturn false;\n\t\t}\n\t\telse\n\t\t{\n\t\t\tsameBaseType = arrayType.getBase().isEqual(this.getBase(), ignoreStorageClass, ignoreConst, ignoreVolatile);\n\t\t}\n\t}\n\tboolean sameSizeExpr = false;\n\tif (((arrayType.getSizeExpr() == null) &amp;&amp; (this.getSizeExpr() == null)))\n\t{\n\t\tsameSizeExpr = true;\n\t}\n\telse\n\t{\n\t\t&lt;%gecos.instrs.Instruction%&gt; _sizeExpr = arrayType.getSizeExpr();\n\t\tboolean _tripleEquals_2 = (_sizeExpr == null);\n\t\t&lt;%gecos.instrs.Instruction%&gt; _sizeExpr_1 = this.getSizeExpr();\n\t\tboolean _tripleEquals_3 = (_sizeExpr_1 == null);\n\t\tboolean _xor_1 = (_tripleEquals_2 ^ _tripleEquals_3);\n\t\tif (_xor_1)\n\t\t{\n\t\t\treturn false;\n\t\t}\n\t\telse\n\t\t{\n\t\t\tsameSizeExpr = arrayType.getSizeExpr().isSame(this.getSizeExpr());\n\t\t}\n\t}\n\treturn ((((sameBaseType &amp;&amp; sameSizeExpr) &amp;&amp; (this.getLower() == arrayType.getLower())) &amp;&amp; (this.getUpper() == arrayType.getUpper())) &amp;&amp; super.isEqual(arrayType, ignoreStorageClass, ignoreConst, ignoreVolatile));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isEqual(Type other, boolean ignoreStorageClass, boolean ignoreConst, boolean ignoreVolatile);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.types.long" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.TypeSizes%&gt;.defaultPointerSize;'"
	 * @generated
	 */
	long getSize();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return list of copies of size instructions, outermost dimension first.
	 * <pre>
	 * e.g: getSizes(A[1][2][3]) returns [1, 2, 3]
	 * </pre>
	 * <!-- end-model-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; sizes = new &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;();\n&lt;%gecos.types.Type%&gt; t = ((&lt;%gecos.types.Type%&gt;) this);\nwhile (((t instanceof &lt;%gecos.types.ArrayType%&gt;) &amp;&amp; (!(t instanceof &lt;%gecos.types.ACType%&gt;))))\n{\n\t{\n\t\tsizes.add(((&lt;%gecos.types.ArrayType%&gt;) t).getSizeExpr().copy());\n\t\tt = ((&lt;%gecos.types.ArrayType%&gt;) t).getBase();\n\t}\n}\nreturn sizes;'"
	 * @generated
	 */
	EList<Instruction> getSizes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model innermostStorageUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.Type%&gt; _innermostBase = this.getInnermostBase();\n_innermostBase.setInnermostStorageClass(innermostStorage);'"
	 * @generated
	 */
	void setInnermostStorageClass(StorageClassSpecifiers innermostStorage);

} // ArrayType
