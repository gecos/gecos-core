package fr.irisa.cairn.gecos.model.analysis.profiling;

import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.BBlock;
import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.For;
//import static fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory.procSymbol;
import static fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory.symbol;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.Int;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.array;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.gt;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.lt;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.mux;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.set;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.sum;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.symbref;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.DOUBLE;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.FLOAT;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.INT;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.LONG;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.SHORT;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.setScope;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EMap;
import org.javatuples.Pair;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import gecos.annotations.IAnnotation;
import gecos.annotations.StringAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
//import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.instrs.CallInstruction;
//import gecos.instrs.CallInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.ArrayType;
import gecos.types.BaseType;
import gecos.types.IntegerType;
import gecos.types.ScalarType;
import gecos.types.Type;

//import org.eclipse.emf.ecore.EObject;

@GSModule(
	"VariableProfiler is used for: Generating a c file where some or all variables\n"
	+ " can be tracked during file's execution and their min/max values displayed\n"
	+ "\n*****************\n"
	+ "* How does it work ? *\n"
	+ "*****************\n"
	+ "- Call VariableProfiler(p) on the current project \"p\", followed by MergeCompositeBlock(p)\n"
	+ "  and finally CGenerator(p, \"../c_src_gen\").\n"
	+ "- Execute the new files to obtain the display of the variables minimum and maximum values in the form of\n"
	+ "		\"orignal_variable_name_min = ...\"\n"
	+ "		\"orignal_variable_name_max = ...\"\n"
	+ "- This plugin can make use of two pragmas, which have to be attached to the \"main\" procedure:\n"
	+ "  \"#pragma VARIABLE_TRACKING some_variable_names\"\n"
	+ "		 => specify that you want to keep watch of some specific variables only.\n"
	+ "  \"#pragma NO_OVERALL_ARRAY\"\n"
	+ "		 => specify that you want to obtain the value range for each cell of any array,\n"
	+ "			instead of the value range of the overall array (default).\n"
	+ "\n"
	+ "********************************\n"
	+ "* What are the requirements/limitations ? *\n"
	+ "********************************\n"
	+ "- As of today, this plugin does not handle pointers so it won't be able to profile them.\n"
	+ "- There MUST be a \"main\" function in the code (could be auto-generated in the future).\n"
	+ "- Variables with the same names will cause interferences with each other\n"
	+ "  so keep the names of the variables distinct.\n"
	+ "\n"
	+ "************\n"
	+ "* future work *\n"
	+ "************\n"
	+ "- patch limitations (see above)\n"
	+ "- Include the file execution + retrieval of the variables range + mapping results to original file\n"
	+ "  in the GeCoS plugin procedure\n"
	+ "\n"
	+ "T.L.\n"

)
public class VariableProfiler {
	
	public final static String PRAGMA_VAR_TRACKING = "VARIABLE_TRACKING";
	public final static String PRAGMA_NO_OVERALL_ARRAY = "NO_OVERALL_ARRAY";
	public final static String UPDATE_PROFILING_ANNOT_KEY = "updateProfiling";
	private GecosProject proj;
	
	
	@GSModuleConstructor("-arg1: The current GecosProject")
	public VariableProfiler(GecosProject proj) {
		this.proj = proj;		
	}

	public void compute(){
		
		boolean varTracking = false;
		boolean overall = true;
		List<String> trackingList = new ArrayList<String>();
		List<String> symbolName = new ArrayList<String>();
		List<Symbol> scalarSymbs = new ArrayList<Symbol>();//scalar symbol tracker
		List<String> toIgnore = new ArrayList<String>();
		Map<Symbol,Pair<Symbol, Symbol>> ArraySymbMap = new HashMap<Symbol,Pair<Symbol, Symbol>>();//array symbol tracker
		
		/************************* 			Pragma Handling 		*************************/
		
		for (ProcedureSet ps : proj.listProcedureSets()) { // Iterate on each procedure set contained in a GeCoS project 
			
			if(ps.findProcedure("main") == null){
				System.out.println("no main procedure found, skiping ");
				continue;
//				throw new RuntimeException("no main procedure found");
			}
			GecosUserAnnotationFactory.pragma(ps, GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION + "#include <stdio.h>");
			GecosUserAnnotationFactory.pragma(ps, GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION + "#include <limits.h>");
			GecosUserAnnotationFactory.pragma(ps, GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION + "#include <float.h>");
			initMinMaxSymbol(ps);
			
			
			//Procedure mainProc = ps.findProcedure("main");
			for (Procedure pr : ps.listProcedures()) {
				EMap<String, IAnnotation> tags = pr.getSymbol().getAnnotations();
	            for (String propId : tags.keySet()) {
	            	if(propId.equals("#pragma")){
	            		           	
		            	String tmp = tags.get(propId).toString();
		            	String firstDelims = "[\"]+";
		            	String[] firstTokens = tmp.split(firstDelims);
		            	for(String firstToken : firstTokens){
			            	String secondDelims = "[ ]+";
			            	String[] secondTokens = firstToken.split(secondDelims);
		            		if(secondTokens[0].equals(PRAGMA_VAR_TRACKING)){
		            			assert(secondTokens.length>=2);
		            			varTracking=true;
		            			for(int i=1; i<secondTokens.length;i++){
		            				trackingList.add(secondTokens[i]+"_"+pr.getSymbolName());
		            			}
		            		}else if (secondTokens[0].equals(PRAGMA_NO_OVERALL_ARRAY)){
		            			assert(secondTokens.length==1);
		            			overall = false;
		            			throw new RuntimeException("no_overall_array disabled for now - remove corresponding pragmas");
		            		}
		            	}
	            	}
	            }
			}
            //for (Procedure pr : ps.listProcedures()) {
            	callThroughProfiler(overall, trackingList, ps, ps.findProcedure("main"), varTracking, toIgnore);
            	System.out.println("done callThrough for a procedure set");
            //}
            
            /************************* min/max Symbol creation and scalar initialization *************************/
            
			for (Procedure pr : ps.listProcedures()) { // Iterate on each procedure contained in a procedure set
				System.out.println("Step 1");
				Scope root = ps.getScope(); 
				AssignmentFinderCustom afinder = new AssignmentFinderCustom(pr, AssignmentFinderCustom.SCALARS+AssignmentFinderCustom.ARRAYS);
				Map<Symbol, List<BasicBlock>> assign = afinder.compute();
				Symbol[] symList = assign.keySet().toArray(new Symbol[assign.keySet().size()]);
				for(Symbol symb : symList){
					boolean processSymb = varTracking? trackingList.contains(symb.getName()+"_"+pr.getSymbolName()):true;
					processSymb = toIgnore.contains(symb.getName())? false:processSymb;
					if (processSymb){
						if (ps.getScope().lookup(symb.getName()+"_"+pr.getSymbolName()+"_min") == null){
							declMinMax(overall, scalarSymbs, ArraySymbMap, pr, root, symb);
							symbolName.add(symb.getName()+"_"+pr.getSymbolName());	
						}
						Symbol min = root.lookup(symb.getName()+"_"+pr.getSymbolName()+"_min");
						Symbol max = root.lookup(symb.getName()+"_"+pr.getSymbolName()+"_max");
						
						if (min != null && min.getType() instanceof ScalarType && scalarSymbs != null && !scalarSymbs.contains(min) ){//for printing purposes
							scalarSymbs.add(min);
							scalarSymbs.add(max);
						} else if (min != null && min.getType() instanceof ArrayType && ArraySymbMap != null && !ArraySymbMap.containsKey(symb)){
							ArraySymbMap.put(symb, new Pair<Symbol, Symbol>(min, max));
						}
					}
				}	
			}
			System.out.println("Step 2");
		}
		
		/************************* min/max arrays initialization (if any) *************************/
		
		if (ArraySymbMap.size()>0){//min_ and max_ arrays initialization for-loops generation 
			for (ProcedureSet ps : proj.listProcedureSets()) {
				Procedure pr = ps.findProcedure("main");
				if(pr == null){
					System.out.println("No main function found, skiping");
					continue;
//					throw new RuntimeException("No main function found");
				}
				System.out.println("Step 3");
				for (Symbol arraySymb : ArraySymbMap.keySet()){
					
					ArrayType currentArrayType = (ArrayType) arraySymb.getType();
					Type innermostBase = currentArrayType.getInnermostBase();
					if(innermostBase.asBase() == null) 
						throw new RuntimeException("Not yet supported for " + arraySymb);
					
					BaseType arrayBase = innermostBase.asBase();
					CompositeBlock prBody = getBody(pr);
					
					List<Instruction> arraySizes = currentArrayType.getSizes(); // (1,2,3) for array A[1][2][3]
					List<Instruction> arrayIterators = new ArrayList<Instruction>(); //container of the iterators of the generated for-loops
					int dimNumber = arraySizes.size();
					Symbol iterSymbol;
					
					for (int i = 0; i<dimNumber; i++){ //actual creation of the iterators
						String iterName = "ar_it_"+(i);
						iterSymbol = prBody.getScope().lookup(iterName);
						if (iterSymbol == null){
							iterSymbol = symbol(iterName, INT());
							prBody.getScope().addSymbol(iterSymbol);
						}
						arrayIterators.add(symbref(iterSymbol));
					}
									
					Symbol minSymb = ArraySymbMap.get(arraySymb).getValue0();
					Symbol maxSymb = ArraySymbMap.get(arraySymb).getValue1();
					
					List<Instruction> arrayIteratorsTmp = new ArrayList<Instruction>();
					for (Instruction iters : arrayIterators){
						arrayIteratorsTmp.add(iters.copy());
					}
					//ArrayType arrayTy = (ArrayType) arraySymb.getType();
					//ScalarType arrayBase = (ScalarType) arrayTy.getScalarBaseType();
					
					
					Instruction maxValue=typeToMinMaxValue(arrayBase, false, pr);
					Instruction minValue=typeToMinMaxValue(arrayBase, true, pr);
					
					
					Instruction maxInit = set(array(symbref(maxSymb), arrayIterators), maxValue);
					Instruction minInit = set(array(symbref(minSymb), arrayIteratorsTmp), minValue);
					List<Instruction> initArrays = new ArrayList<Instruction>();
					
					initArrays.add(minInit);
					initArrays.add(maxInit);
					Block forBlock = BBlock(initArrays);// inner "for" body
					
					for (int i = dimNumber-1; i>=0; i--){ //generation of nested initialization for-loops
						Instruction initIter = set(arrayIterators.get(i).copy(), Int(0));
						Instruction testIter = lt(arrayIterators.get(i).copy(), arraySizes.get(dimNumber-i-1));
						Instruction IncrIter = sum(arrayIterators.get(i).copy(), Int(1));
						Instruction stepIter = set(arrayIterators.get(i).copy(),IncrIter);
						forBlock = For(BBlock(initIter), BBlock(testIter), BBlock(stepIter), forBlock.copy());//inner "for"(s) is(are) the body of outer "for"(s) 						
					}
					
					prBody.addChildren(forBlock);
					prBody.getChildren().move(0, forBlock);
				}
			}
		}
		
		for (ProcedureSet ps : proj.listProcedureSets()) {
			
			setScope(ps.getScope());
			System.out.println("Step 4");
			/************************* call updates creation **********************************/
			
			for (Procedure pr : ps.listProcedures()){
				CallFinder cFinder = new CallFinder(pr);
		    	List<CallInstruction> callList = cFinder.compute();
		    	for (CallInstruction call : callList ){
		    		StringAnnotation annot = (StringAnnotation) call.getAnnotation(UPDATE_PROFILING_ANNOT_KEY);
		    		System.out.println("pr = "+pr.getSymbolName()+", call = "+call.toString()+" annot : "+ (annot!=null));
		    		if (annot != null && annot.getContent().compareTo("")!=0){
			    		String msg = annot.getContent();
			    		System.out.println("msg = " + msg);
			    		String delim = "[|]+";
			    		String buffer1[] = msg.split(delim);
			    		for (String elem : buffer1){
			    			System.out.println("elem = " + elem);
			    			
			    			String delim2 = "[:]+";
			    			String buffer2[] = elem.split(delim2);
			    			int index = Integer.parseInt(buffer2[1]);
			    			System.out.println("index = " + index);
			    			Instruction param =  call.getArgs().get(Integer.parseInt(buffer2[1])).copy();
			    			
			    			SymbolInstruction arg;
			    			if (param instanceof SymbolInstruction && (overall || ((SymbolInstruction) param).getSymbol().getType().asBase()!=null)){
			    				
				    			arg = (SymbolInstruction) param.copy();
				    			Symbol argMinSymb = ps.getScope().lookup(arg.getSymbolName()+"_"+pr.getSymbolName()+"_min");
				    			Symbol argMaxSymb = ps.getScope().lookup(arg.getSymbolName()+"_"+pr.getSymbolName()+"_max");
				    			if (argMinSymb==null || argMaxSymb==null){
				    				declMinMax(overall, scalarSymbs, ArraySymbMap, pr, ps.getScope(), arg.getSymbol());
				    				argMinSymb = ps.getScope().lookup(arg.getSymbolName()+"_"+pr.getSymbolName()+"_min");
					    			argMaxSymb = ps.getScope().lookup(arg.getSymbolName()+"_"+pr.getSymbolName()+"_max");
				    			}
				    			Symbol srcMinSymb = ps.getScope().lookup(buffer2[0]+"_min");
				    			Symbol srcMaxSymb = ps.getScope().lookup(buffer2[0]+"_max");
				    			Instruction srcMin = symbref(srcMinSymb);
				    			Instruction srcMax = symbref(srcMaxSymb);
				    			Instruction argMin = symbref(argMinSymb);
				    			Instruction argMax = symbref(argMaxSymb);
				    			Instruction condMin = lt(argMin.copy(), srcMin.copy());
				    			Instruction condMax = gt(argMax.copy(), srcMax.copy());
				    			Instruction muxMin = mux(condMin, argMin.copy(), srcMin.copy());
				    			Instruction muxMax = mux(condMax, argMax.copy(), srcMax.copy());
				    			SetInstruction setMin = set(argMin.copy(), muxMin.copy());
				    			SetInstruction setMax = set(argMax, muxMax);
				    			BasicBlock b = call.getBlock();
				    			if (b == null){
				    				throw new RuntimeException("call basic block not found");
				    			}
				    			b.insertInstructionAfter(setMin, call.getRoot());
				    			b.insertInstructionAfter(setMax, call.getRoot());
			    			}else if (param instanceof SymbolInstruction && !overall && ((SymbolInstruction) param).getSymbol().getType().asArray()!=null){
			    				throw new RuntimeException("no_overall_array not handled yet for function calls return-tracking");
			    			}
			    		}
		    		}
		    		call.setAnnotation(UPDATE_PROFILING_ANNOT_KEY, null);
		    	}
        	}
			/************************* min/max assignment duplication *************************/
			
			for (Procedure pr : ps.listProcedures()) {
				System.out.println("POY");
				AssignmentDuplicator duplicator = new AssignmentDuplicator(pr,symbolName, overall);
				duplicator.compute();
			}
			
			/************************* min/max display generation for scalars *************************/
			
			Procedure pr = ps.findProcedure("main");
			if(pr == null){
				System.out.println("No main function found, skiping");
				continue;
//				throw new RuntimeException("No main function found");
			}
			VariablePrinter varPrinter = new VariablePrinter(pr, scalarSymbs, ArraySymbMap);
			varPrinter.compute();
		}
	}

	private void callThroughProfiler(boolean overall, List<String> trackingList,
			ProcedureSet ps, Procedure pr, boolean varTracking, List<String> toIgnore) {
		System.out.println("callThroughProfiler with pr = "+pr.getSymbolName());
		List<String> symbolName = new ArrayList<String>();
		CallFinder cFinder = new CallFinder(pr);
		List<CallInstruction> callList = cFinder.compute();
		for (CallInstruction call : callList ){
			ProcedureSymbol callProcSymb = (ProcedureSymbol) call.getProcedureSymbol();
			Procedure callProc = callProcSymb.getProcedure();
			if (callProc !=null && callProc.getContainingProcedureSet() == ps){
				List<Instruction> args = call.getArgs();
				List<Integer> indexArgs = new ArrayList<Integer>();
				List<String> toAnnot = new ArrayList<String>();
				for (int i = 0; i<args.size(); i++){
					if (args.get(i) instanceof SymbolInstruction){
						SymbolInstruction argSymbInstr = (SymbolInstruction) args.get(i);
						Symbol argSymb = argSymbInstr.getSymbol();
						for (String tmpstr : trackingList)
							System.out.println(tmpstr);
						System.out.println("argSymb =  "+argSymb.getName());
						if (trackingList.contains(argSymb.getName()+"_"+pr.getSymbolName()) || !varTracking){
							
							//générer var tempo pour tracker argument
							List<ParameterSymbol> paramsList = callProc.listParameters();
							Symbol paramSymb = paramsList.get(i);
							if (ps.getScope().lookup(paramSymb.getName()+"_"+callProc.getSymbolName()+"_min") == null){
								declMinMax(overall, null, null, callProc, ps.getScope(), paramSymb);
								symbolName.add(paramsList.get(i).getName()+"_"+callProc.getSymbolName());
								toIgnore.add(paramSymb.getName()+"_"+callProc.getSymbolName()+"_min");
								toIgnore.add(paramSymb.getName()+"_"+callProc.getSymbolName()+"_max");
							}
							indexArgs.add(i);
							toAnnot.add(paramSymb.getName()+"_"+callProc.getSymbolName());
							if (varTracking)
								trackingList.add(paramSymb.getName()+"_"+callProc.getSymbolName());
							System.out.println("start nested call");
							callThroughProfiler(overall, trackingList, callProc.getContainingProcedureSet(), callProc, varTracking, toIgnore);
							System.out.println("return from nested call");
							if (varTracking)
								trackingList.remove(trackingList.size()-1);
							//générer le code après le call pour propager çà.
						}
					} 
				}
				//faire le tracking de la var dans la proc
				AssignmentDuplicator duplicator = new AssignmentDuplicator(callProc, symbolName, overall);
				duplicator.compute();
				String oldMsg;
				
				StringAnnotation msg = (StringAnnotation) call.getAnnotation(UPDATE_PROFILING_ANNOT_KEY);
				if(msg!=null)
					oldMsg = msg.getContent();
				else
					oldMsg="";
				
				if (oldMsg.compareTo("")==0){				
					for (int i = 0; i<toAnnot.size(); i++){
						String symName = toAnnot.get(i);
							if (oldMsg.compareTo("") == 0)
								oldMsg = symName+":"+indexArgs.get(i);
							else
								oldMsg = oldMsg + "|" + symName+":"+indexArgs.get(i);
					}
				}
				else{
					for (int i = 0; i<toAnnot.size(); i++){
						String symName = toAnnot.get(i);
							if (!oldMsg.contains(symName))
								oldMsg = oldMsg + "|" + symName+":"+indexArgs.get(i);
					}
				}
				msg = GecosUserAnnotationFactory.STRING(oldMsg);
				call.setAnnotation(UPDATE_PROFILING_ANNOT_KEY, msg);
				symbolName.clear();
			}
			
		}
	}

	private void declMinMax(boolean overall, List<Symbol> scalarSymbs, Map<Symbol, Pair<Symbol, Symbol>> ArraySymbMap,
			Procedure pr, Scope root, Symbol symb) {
		Instruction value_min=null;
		Instruction value_max=null;
		Symbol min = symbol(symb.getName()+"_"+pr.getSymbolName()+"_min", symb.getType());
		Symbol max = symbol(symb.getName()+"_"+pr.getSymbolName()+"_max", symb.getType());
		if (symb.getType().isPointer())
			throw new RuntimeException("Pointers not handled");
		if (symb.getValue()==null){//not initialised symbol
			
			if (symb.getType().asBase() != null){//scalar symbol
				
				BaseType t = symb.getType().asBase();
				
				value_min = typeToMinMaxValue(t, true, pr);
				value_max = typeToMinMaxValue(t, false, pr);
				
				
			}else if (symb.getType().isArray()){//array symbol tracking
				if(overall){
					ArrayType currentArrayType = symb.getType().asArray();
					
					Type innermostBase = currentArrayType.getInnermostBase();
					if(innermostBase.asBase() == null) 
						throw new RuntimeException("Not yet supported for " + currentArrayType);
					
					BaseType arrayBase = innermostBase.asBase();
					
					min = symbol(symb.getName()+"_"+pr.getSymbolName()+"_min", arrayBase);
					max = symbol(symb.getName()+"_"+pr.getSymbolName()+"_max", arrayBase);
					
					value_min = typeToMinMaxValue(arrayBase, true, pr);
					value_max = typeToMinMaxValue(arrayBase, false, pr);
					

				}else{
					value_min = null;
					value_max = null;
					if (ArraySymbMap != null)
						
						ArraySymbMap.put(symb, new Pair<Symbol, Symbol>(min, max));	
				}
			}else{//struct and ... ?
				value_min = null;
				value_max = null;
			}
			
		}else{//initialized already (scalars only)

			value_min = symb.getValue().copy();
			value_max = symb.getValue().copy();
		}
		if (min.getType() instanceof ScalarType && scalarSymbs != null){//scalar symbol tracking
			scalarSymbs.add(min);
			scalarSymbs.add(max);
		}
		min.setValue(value_min);
		max.setValue(value_max);

		root.addSymbol(min);
		root.addSymbol(max);
	}
	
	private void initMinMaxSymbol(ProcedureSet ps){
		Symbol shrt_min = symbol("SHRT_MIN", SHORT());
		Symbol shrt_max = symbol("SHRT_MAX", SHORT());
		Symbol int_min = symbol("INT_MIN", INT());
		Symbol int_max = symbol("INT_MAX", INT());
		Symbol long_min = symbol("LONG_MIN", LONG());
		Symbol long_max = symbol("LONG_MAX", LONG());
		Symbol float_min = symbol("FLT_MIN", FLOAT());
		Symbol float_max = symbol("FLT_MAX", FLOAT());
		Symbol double_min = symbol("DBL_MIN", DOUBLE());
		Symbol double_max = symbol("DBL_MAX", DOUBLE());
		ps.getScope().getSymbols().add(shrt_min);
		ps.getScope().getSymbols().add(shrt_max);
		ps.getScope().getSymbols().add(int_min);
		ps.getScope().getSymbols().add(int_max);
		ps.getScope().getSymbols().add(long_min);
		ps.getScope().getSymbols().add(long_max);
		ps.getScope().getSymbols().add(float_min);
		ps.getScope().getSymbols().add(float_max);
		ps.getScope().getSymbols().add(double_min);
		ps.getScope().getSymbols().add(double_max);
		

		GecosUserAnnotationFactory.pragma(shrt_min, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(shrt_max, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(int_min, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(int_max, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(long_min, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(long_max, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(float_min, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(float_max, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(double_min, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(double_max, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
	}
	
	private Instruction typeToMinMaxValue (BaseType t, boolean isMax, Procedure pr){
		String symName = null;
		
		if(t.asInt() != null) {
			IntegerType it = t.asInt();
			symName = it.isShort() ? "SHRT" :
				it.isLong() ? "LONG" :
				"INT";
		}
		else if(t.asFloat() != null) {
			symName = t.asFloat().isSingle() ? "FLT" : "DBL";
		}
		
		if(symName == null)
			return null;
		
		symName += isMax ? "_MAX" : "_MIN";
		
		return symbref(pr.getContainingProcedureSet().getScope().lookup(symName));
	}
	
	
	private CompositeBlock getBody(Procedure pr) {
		Block prBody = pr.getBody().getChildren().get(1);
		if (!(prBody instanceof CompositeBlock))
			throw new RuntimeException("get body : first block not composite block");
		
		return (CompositeBlock) prBody;
	}
	
}