/**
 */
package gecos.dag;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAGSSA Def Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.DAGSSADefNode#getSSAUses <em>SSA Uses</em>}</li>
 * </ul>
 *
 * @see gecos.dag.DagPackage#getDAGSSADefNode()
 * @model
 * @generated
 */
public interface DAGSSADefNode extends DAGOutNode, DagVisitable {
	/**
	 * Returns the value of the '<em><b>SSA Uses</b></em>' reference list.
	 * The list contents are of type {@link gecos.dag.DAGSSAUseNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SSA Uses</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SSA Uses</em>' reference list.
	 * @see gecos.dag.DagPackage#getDAGSSADefNode_SSAUses()
	 * @model
	 * @generated
	 */
	EList<DAGSSAUseNode> getSSAUses();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGSSADefNode(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

} // DAGSSADefNode
