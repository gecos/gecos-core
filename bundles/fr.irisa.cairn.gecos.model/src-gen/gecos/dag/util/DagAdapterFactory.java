/**
 */
package gecos.dag.util;

import gecos.annotations.AnnotatedElement;

import gecos.core.GecosNode;
import gecos.core.ITypedElement;

import gecos.dag.*;

import gecos.instrs.InstrsVisitable;
import gecos.instrs.Instruction;

import java.util.Map;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see gecos.dag.DagPackage
 * @generated
 */
public class DagAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DagPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DagAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = DagPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DagSwitch<Adapter> modelSwitch =
		new DagSwitch<Adapter>() {
			@Override
			public Adapter caseDagVisitor(DagVisitor object) {
				return createDagVisitorAdapter();
			}
			@Override
			public Adapter caseDagVisitable(DagVisitable object) {
				return createDagVisitableAdapter();
			}
			@Override
			public Adapter caseDAGInstruction(DAGInstruction object) {
				return createDAGInstructionAdapter();
			}
			@Override
			public Adapter caseDAGNode(DAGNode object) {
				return createDAGNodeAdapter();
			}
			@Override
			public Adapter caseDAGEdge(DAGEdge object) {
				return createDAGEdgeAdapter();
			}
			@Override
			public Adapter caseDAGPort(DAGPort object) {
				return createDAGPortAdapter();
			}
			@Override
			public Adapter caseDAGInPort(DAGInPort object) {
				return createDAGInPortAdapter();
			}
			@Override
			public Adapter caseDAGOutPort(DAGOutPort object) {
				return createDAGOutPortAdapter();
			}
			@Override
			public Adapter caseDAGImmNode(DAGImmNode object) {
				return createDAGImmNodeAdapter();
			}
			@Override
			public Adapter caseDAGSymbolNode(DAGSymbolNode object) {
				return createDAGSymbolNodeAdapter();
			}
			@Override
			public Adapter caseDAGCallNode(DAGCallNode object) {
				return createDAGCallNodeAdapter();
			}
			@Override
			public Adapter caseDAGArrayValueNode(DAGArrayValueNode object) {
				return createDAGArrayValueNodeAdapter();
			}
			@Override
			public Adapter caseDAGOpNode(DAGOpNode object) {
				return createDAGOpNodeAdapter();
			}
			@Override
			public Adapter caseDAGOutNode(DAGOutNode object) {
				return createDAGOutNodeAdapter();
			}
			@Override
			public Adapter caseDAGControlEdge(DAGControlEdge object) {
				return createDAGControlEdgeAdapter();
			}
			@Override
			public Adapter caseDAGDataEdge(DAGDataEdge object) {
				return createDAGDataEdgeAdapter();
			}
			@Override
			public Adapter caseDAGNumberedSymbolNode(DAGNumberedSymbolNode object) {
				return createDAGNumberedSymbolNodeAdapter();
			}
			@Override
			public Adapter caseDAGNumberedOutNode(DAGNumberedOutNode object) {
				return createDAGNumberedOutNodeAdapter();
			}
			@Override
			public Adapter caseDAGIntImmNode(DAGIntImmNode object) {
				return createDAGIntImmNodeAdapter();
			}
			@Override
			public Adapter caseDAGFloatImmNode(DAGFloatImmNode object) {
				return createDAGFloatImmNodeAdapter();
			}
			@Override
			public Adapter caseDAGStringImmNode(DAGStringImmNode object) {
				return createDAGStringImmNodeAdapter();
			}
			@Override
			public Adapter caseDAGArrayNode(DAGArrayNode object) {
				return createDAGArrayNodeAdapter();
			}
			@Override
			public Adapter caseDAGSimpleArrayNode(DAGSimpleArrayNode object) {
				return createDAGSimpleArrayNodeAdapter();
			}
			@Override
			public Adapter caseDAGFieldInstruction(DAGFieldInstruction object) {
				return createDAGFieldInstructionAdapter();
			}
			@Override
			public Adapter caseDAGJumpNode(DAGJumpNode object) {
				return createDAGJumpNodeAdapter();
			}
			@Override
			public Adapter caseDAGPatternNode(DAGPatternNode object) {
				return createDAGPatternNodeAdapter();
			}
			@Override
			public Adapter caseInPortMap(Map.Entry<DAGInPort, DAGInPort> object) {
				return createInPortMapAdapter();
			}
			@Override
			public Adapter caseOutPortMap(Map.Entry<DAGOutPort, DAGOutPort> object) {
				return createOutPortMapAdapter();
			}
			@Override
			public Adapter caseDAGVectorNode(DAGVectorNode object) {
				return createDAGVectorNodeAdapter();
			}
			@Override
			public Adapter caseDAGVectorArrayAccessNode(DAGVectorArrayAccessNode object) {
				return createDAGVectorArrayAccessNodeAdapter();
			}
			@Override
			public Adapter caseDAGVectorOpNode(DAGVectorOpNode object) {
				return createDAGVectorOpNodeAdapter();
			}
			@Override
			public Adapter caseDAGVectorShuffleNode(DAGVectorShuffleNode object) {
				return createDAGVectorShuffleNodeAdapter();
			}
			@Override
			public Adapter caseDAGVectorExtractNode(DAGVectorExtractNode object) {
				return createDAGVectorExtractNodeAdapter();
			}
			@Override
			public Adapter caseDAGVectorExpandNode(DAGVectorExpandNode object) {
				return createDAGVectorExpandNodeAdapter();
			}
			@Override
			public Adapter caseDAGVectorPackNode(DAGVectorPackNode object) {
				return createDAGVectorPackNodeAdapter();
			}
			@Override
			public Adapter caseDAGSSAUseNode(DAGSSAUseNode object) {
				return createDAGSSAUseNodeAdapter();
			}
			@Override
			public Adapter caseDAGSSADefNode(DAGSSADefNode object) {
				return createDAGSSADefNodeAdapter();
			}
			@Override
			public Adapter caseDAGPhiNode(DAGPhiNode object) {
				return createDAGPhiNodeAdapter();
			}
			@Override
			public Adapter caseGecosNode(GecosNode object) {
				return createGecosNodeAdapter();
			}
			@Override
			public Adapter caseAnnotatedElement(AnnotatedElement object) {
				return createAnnotatedElementAdapter();
			}
			@Override
			public Adapter caseInstrsVisitable(InstrsVisitable object) {
				return createInstrsVisitableAdapter();
			}
			@Override
			public Adapter caseITypedElement(ITypedElement object) {
				return createITypedElementAdapter();
			}
			@Override
			public Adapter caseInstruction(Instruction object) {
				return createInstructionAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DagVisitor <em>Visitor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DagVisitor
	 * @generated
	 */
	public Adapter createDagVisitorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DagVisitable <em>Visitable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DagVisitable
	 * @generated
	 */
	public Adapter createDagVisitableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGInstruction <em>DAG Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGInstruction
	 * @generated
	 */
	public Adapter createDAGInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGNode <em>DAG Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGNode
	 * @generated
	 */
	public Adapter createDAGNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGEdge <em>DAG Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGEdge
	 * @generated
	 */
	public Adapter createDAGEdgeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGPort <em>DAG Port</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGPort
	 * @generated
	 */
	public Adapter createDAGPortAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGInPort <em>DAG In Port</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGInPort
	 * @generated
	 */
	public Adapter createDAGInPortAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGOutPort <em>DAG Out Port</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGOutPort
	 * @generated
	 */
	public Adapter createDAGOutPortAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGImmNode <em>DAG Imm Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGImmNode
	 * @generated
	 */
	public Adapter createDAGImmNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGSymbolNode <em>DAG Symbol Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGSymbolNode
	 * @generated
	 */
	public Adapter createDAGSymbolNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGCallNode <em>DAG Call Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGCallNode
	 * @generated
	 */
	public Adapter createDAGCallNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGArrayValueNode <em>DAG Array Value Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGArrayValueNode
	 * @generated
	 */
	public Adapter createDAGArrayValueNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGOpNode <em>DAG Op Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGOpNode
	 * @generated
	 */
	public Adapter createDAGOpNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGOutNode <em>DAG Out Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGOutNode
	 * @generated
	 */
	public Adapter createDAGOutNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGControlEdge <em>DAG Control Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGControlEdge
	 * @generated
	 */
	public Adapter createDAGControlEdgeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGDataEdge <em>DAG Data Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGDataEdge
	 * @generated
	 */
	public Adapter createDAGDataEdgeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGNumberedSymbolNode <em>DAG Numbered Symbol Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGNumberedSymbolNode
	 * @generated
	 */
	public Adapter createDAGNumberedSymbolNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGNumberedOutNode <em>DAG Numbered Out Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGNumberedOutNode
	 * @generated
	 */
	public Adapter createDAGNumberedOutNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGIntImmNode <em>DAG Int Imm Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGIntImmNode
	 * @generated
	 */
	public Adapter createDAGIntImmNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGFloatImmNode <em>DAG Float Imm Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGFloatImmNode
	 * @generated
	 */
	public Adapter createDAGFloatImmNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGStringImmNode <em>DAG String Imm Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGStringImmNode
	 * @generated
	 */
	public Adapter createDAGStringImmNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGArrayNode <em>DAG Array Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGArrayNode
	 * @generated
	 */
	public Adapter createDAGArrayNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGSimpleArrayNode <em>DAG Simple Array Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGSimpleArrayNode
	 * @generated
	 */
	public Adapter createDAGSimpleArrayNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGFieldInstruction <em>DAG Field Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGFieldInstruction
	 * @generated
	 */
	public Adapter createDAGFieldInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGJumpNode <em>DAG Jump Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGJumpNode
	 * @generated
	 */
	public Adapter createDAGJumpNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGPatternNode <em>DAG Pattern Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGPatternNode
	 * @generated
	 */
	public Adapter createDAGPatternNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>In Port Map</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createInPortMapAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Out Port Map</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createOutPortMapAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGVectorNode <em>DAG Vector Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGVectorNode
	 * @generated
	 */
	public Adapter createDAGVectorNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGVectorArrayAccessNode <em>DAG Vector Array Access Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGVectorArrayAccessNode
	 * @generated
	 */
	public Adapter createDAGVectorArrayAccessNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGVectorOpNode <em>DAG Vector Op Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGVectorOpNode
	 * @generated
	 */
	public Adapter createDAGVectorOpNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGVectorShuffleNode <em>DAG Vector Shuffle Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGVectorShuffleNode
	 * @generated
	 */
	public Adapter createDAGVectorShuffleNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGVectorExtractNode <em>DAG Vector Extract Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGVectorExtractNode
	 * @generated
	 */
	public Adapter createDAGVectorExtractNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGVectorExpandNode <em>DAG Vector Expand Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGVectorExpandNode
	 * @generated
	 */
	public Adapter createDAGVectorExpandNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGVectorPackNode <em>DAG Vector Pack Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGVectorPackNode
	 * @generated
	 */
	public Adapter createDAGVectorPackNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGSSAUseNode <em>DAGSSA Use Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGSSAUseNode
	 * @generated
	 */
	public Adapter createDAGSSAUseNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGSSADefNode <em>DAGSSA Def Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGSSADefNode
	 * @generated
	 */
	public Adapter createDAGSSADefNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.dag.DAGPhiNode <em>DAG Phi Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.dag.DAGPhiNode
	 * @generated
	 */
	public Adapter createDAGPhiNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.core.GecosNode <em>Gecos Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.core.GecosNode
	 * @generated
	 */
	public Adapter createGecosNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.annotations.AnnotatedElement <em>Annotated Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.annotations.AnnotatedElement
	 * @generated
	 */
	public Adapter createAnnotatedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.InstrsVisitable <em>Visitable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.InstrsVisitable
	 * @generated
	 */
	public Adapter createInstrsVisitableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.core.ITypedElement <em>ITyped Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.core.ITypedElement
	 * @generated
	 */
	public Adapter createITypedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link gecos.instrs.Instruction <em>Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see gecos.instrs.Instruction
	 * @generated
	 */
	public Adapter createInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //DagAdapterFactory
