/**
 */
package gecos.annotations;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Comment Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.annotations.AnnotationsPackage#getCommentAnnotation()
 * @model
 * @generated
 */
public interface CommentAnnotation extends StringAnnotation {
} // CommentAnnotation
