/**
 */
package gecos.dag;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Array Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.dag.DagPackage#getDAGArrayNode()
 * @model
 * @generated
 */
public interface DAGArrayNode extends DAGNode, DagVisitable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGArrayNode(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getPredecessors().get(1);'"
	 * @generated
	 */
	DAGNode getIndex();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getPredecessors().get(0);'"
	 * @generated
	 */
	DAGNode getDest();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.dag.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _ident = this.getIdent();\n&lt;%java.lang.String%&gt; _plus = (\"ArrayNode_\" + &lt;%java.lang.Integer%&gt;.valueOf(_ident));\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \":\");\n&lt;%gecos.types.Type%&gt; _type = this.getType();\nreturn (_plus_1 + _type);'"
	 * @generated
	 */
	String toString();

} // DAGArrayNode
