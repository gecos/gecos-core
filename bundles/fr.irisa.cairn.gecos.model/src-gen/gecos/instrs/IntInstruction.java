/**
 */
package gecos.instrs;

import gecos.types.Type;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Int Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.IntInstruction#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getIntInstruction()
 * @model
 * @generated
 */
public interface IntInstruction extends ConstantInstruction {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(long)
	 * @see gecos.instrs.InstrsPackage#getIntInstruction_Value()
	 * @model unique="false" dataType="gecos.instrs.long"
	 * @generated
	 */
	long getValue();

	/**
	 * Sets the value of the '{@link gecos.instrs.IntInstruction#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(long value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitIntInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%java.lang.String%&gt;.valueOf(this.getValue());'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return true;'"
	 * @generated
	 */
	boolean isConstant();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" objectUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return ((object instanceof &lt;%gecos.instrs.IntInstruction%&gt;) &amp;&amp; \n\t(((&lt;%gecos.instrs.IntInstruction%&gt;) object).getValue() == this.getValue()));'"
	 * @generated
	 */
	boolean equals(Object object);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory%&gt;.INT();'"
	 * @generated
	 */
	Type computeType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _xifexpression = false;\nif ((instr instanceof &lt;%gecos.instrs.IntInstruction%&gt;))\n{\n\t_xifexpression = (super.isSame(instr) &amp;&amp; (this.getValue() == ((&lt;%gecos.instrs.IntInstruction%&gt;)instr).getValue()));\n}\nelse\n{\n\t_xifexpression = false;\n}\nreturn _xifexpression;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // IntInstruction
