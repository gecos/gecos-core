package fr.irisa.cairn.gecos.model.utils.annotations;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.eclipse.emf.ecore.EObject;

import com.google.common.collect.Streams;

import gecos.annotations.AnnotatedElement;
import gecos.annotations.PragmaAnnotation;

public class PragmaUtils {
	
//	@FunctionalInterface
//	public interface PragmaDefinition {
//		public boolean isPragmaContent(String pragmaContent);
//	}
	
	/**
	 * @param pragmaContent a string
	 * @return first space-separated char sequence in the pragmaContent
	 */
	public static String getPragmaName(String pragmaContent) {
		return pragmaContent.split("\\s+")[0].trim();
	}
	
	/**
	 * @param pragmaContent a string
	 * @return the specified pragmaContent skipping the pragma name.
	 */
	public static String getPragmaArgs(String pragmaContent) {
		return pragmaContent.replaceFirst(getPragmaName(pragmaContent), "").trim();
	}
	
	/**
	 * @param obj
	 * @param pargmaContentMatcher to match contents from the obj's pragma annotation
	 * @return stream of matching pragma contents. Never {@code null}.
	 */
	public static Stream<String> findPragmaContent(AnnotatedElement obj, Predicate<String> pargmaContentMatcher) {
		PragmaAnnotation pragma = obj.getPragma();
		if(pragma == null)
			return Stream.empty();
		return findPragmaContent(pragma, pargmaContentMatcher);
	}
	
	/**
	 * @param pragma annotation
	 * @param pargmaContentMatcher to match contents from the pragma
	 * @return list of matching pragma contents. Never {@code null}.
	 */
	public static Stream<String> findPragmaContent(PragmaAnnotation pragma, Predicate<String> pargmaContentMatcher) {
		Objects.requireNonNull(pragma);
		Objects.requireNonNull(pargmaContentMatcher);
		return pragma.getContent().stream()
				.filter(pargmaContentMatcher);
	}
	
	public static <T extends AnnotatedElement> Map<T, List<String>> findAllContentsWithPragma(EObject inRoot, Class<T> instanceOf, 
			Predicate<String> pargmaContentMatcher) {
		return Streams.stream(inRoot.eAllContents())
			.filter(PragmaAnnotation.class::isInstance)
			.map(PragmaAnnotation.class::cast)
			.filter(Objects::nonNull)
			.filter(p -> instanceOf.isInstance(p.getAnnotatedElement()))
			.filter(p -> p.getContent().stream().filter(pargmaContentMatcher).findAny().isPresent())
			.collect(toMap(
						p -> instanceOf.cast(p.getAnnotatedElement()), 
						p -> findPragmaContent(p, pargmaContentMatcher).collect(toList())));
	}
	
//	public static void removePragma(PragmaDefinition pragmaDef, AnnotatedElement fromElt) {
//		PragmaAnnotation pg = fromElt.getPragma();
//		if(pragmaDef.isInstance(pg))
//			fromElt.getAnnotations().remove(pg.eContainer()); //FIXME multiple pragmas are fused !!
//	}
	
}
