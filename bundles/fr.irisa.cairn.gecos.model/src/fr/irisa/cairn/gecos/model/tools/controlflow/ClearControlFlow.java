package fr.irisa.cairn.gecos.model.tools.controlflow;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;

public class ClearControlFlow extends BlocksDefaultSwitch<Object> {

	private List<Procedure> lProc = null;

	public enum MODE {
		USEDEF, CFG, ALL
	}

	public MODE mode = MODE.CFG;

	private ClearControlFlow() {
	};

	public ClearControlFlow(Procedure proc) {
		(lProc = new ArrayList<Procedure>(1)).add(proc);
	}

	public ClearControlFlow(ProcedureSet ps) {
		lProc = ps.listProcedures();
	}

	public ClearControlFlow(GecosProject project) {
		lProc = new ArrayList<Procedure>();
		for (ProcedureSet ps : project.listProcedureSets()) {
			lProc.addAll(ps.listProcedures());
		}
	}

	public static void clearCF(Procedure proc) {
		ClearControlFlow clearControlFlow = new ClearControlFlow(proc);
		clearControlFlow.compute();
	}


	public static void clearCF(Procedure proc,MODE mode) {
		ClearControlFlow cCF = new ClearControlFlow(proc);
		cCF.mode=mode;
		cCF.compute();
	}


	public static void clearCF(ProcedureSet ps,MODE mode) {
		ClearControlFlow cCF = new ClearControlFlow(ps);
		cCF.mode=mode;
		cCF.compute();
	}

	public static void clearCF(Block b) {
		(new ClearControlFlow()).doSwitch(b);
	}
	
	public static void clearCF(EObject targetAST) {
		EList<Block> topLvlBlocks = EMFUtils.eAllContentsFirstInstancesOf(targetAST, Block.class);
		for (Block b : topLvlBlocks)
			clearCF(b);
	}

	public void compute() {
		for (Procedure proc : lProc) {
			doSwitch(proc);
		}
	}

	@Override
	public Object caseBasicBlock(BasicBlock b) {
		switch (mode) {
		case CFG:
			b.getInEdges().clear();
			b.getOutEdges().clear();
			break;
		case USEDEF:
			b.getUseEdges().clear();
			b.getDefEdges().clear();
			break;
		case ALL:
			b.getInEdges().clear();
			b.getOutEdges().clear();
			b.getUseEdges().clear();
			b.getDefEdges().clear();
			break;

		default:
			break;
		}
		return null;
	}
}
