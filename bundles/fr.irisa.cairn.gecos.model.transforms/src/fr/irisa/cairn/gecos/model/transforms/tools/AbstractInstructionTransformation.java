package fr.irisa.cairn.gecos.model.transforms.tools;

import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionDualSwitch;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.instrs.Instruction;
import tom.library.sl.VisitFailure;

/**
 * An abstract class for TOM instructions transformations. Original instructions
 * aren't modified.
 * 
 * @author antoine
 * 
 */
public abstract class AbstractInstructionTransformation {
	public boolean VERBOSE = false;

	protected InstructionTransformSwitch tSwitch = new InstructionTransformSwitch();

	protected Procedure currentProcedure =null;
	protected Block currentBlock  =null;

	/**
	 * Replace all procedure's instructions by transformed ones.
	 * 
	 * @param proc
	 *            analyzed procedure
	 */
	public void transform(ProcedureSet ps) {
		for(Procedure proc : ps.listProcedures()) {
			transform(proc);
		}
	}

	/**
	 * Replace all procedure's instructions by transformed ones.
	 * 
	 * @param proc
	 *            analyzed procedure
	 */
	public void transform(Procedure proc) {
		currentProcedure=proc;
		tSwitch.doSwitch(proc.getBody());
	}

	/**
	 * Replace all block's instructions by transformed ones.
	 * 
	 * @param block
	 *            analyzed block
	 */
	public void transform(Block block) {
		currentBlock=block;
		tSwitch.doSwitch(block);
	}

	/**
	 * Compute the transformation of an instruction. Original instruction isn't
	 * modified.
	 * 
	 * @param original
	 *            instruction to transform
	 * @return A new transformed instruction or the original instruction if
	 *         transformation has no effect.
	 */
	public Instruction transform(Instruction original) {
		return tSwitch.doSwitch(original);
	}

	/**
	 * Abstract method giving TOM transformation behavior.
	 * 
	 * @param instruction
	 * @return
	 * @throws VisitFailure
	 */
	protected abstract Instruction apply(Instruction instruction)
			throws VisitFailure;

	/**
	 * Nested transformation switch. Default behavior is to return transformed
	 * instruction.
	 * 
	 * @author antoine
	 * 
	 */
	protected class InstructionTransformSwitch extends
			BlockInstructionDualSwitch<Instruction, Object> {

		@Override
		public Object caseBasicBlock(BasicBlock b) {
			for (Instruction inst : b.getInstructions()) {
				// FIXME : was
				Instruction transformed = doTrans(inst);
				//Instruction transformed = doSwitch(inst);
				if (transformed != inst) {
					int pos = b.getInstructions().indexOf(inst);
					b.getInstructions().set(pos, transformed);
				}
			}
			return new Object();
		}

		@Override
		public Instruction caseInstruction(Instruction original) {
			return doTrans(original);
		}

		// @Override
		// public Instruction caseCondInstruction(CondInstruction object) {
		// //XXX: dirty (should be a child in adapter)
		// Instruction transCond = doSwitch(object.getCond());
		// if(transCond != object.getCond()){
		// CondInstruction copy = (CondInstruction) object.copy();
		// copy.setCond(transCond);
		// return copy;
		// }
		// return object;
		// }

		protected Instruction doTrans(Instruction original) {
			try {
				Instruction newInst = apply(original);
				if (newInst != original) {
					if (VERBOSE)
						System.out.println(original + "=>" + newInst);
				} else {
					if (VERBOSE)
						System.out.println(original	+ " was not transformed");
				}
				return newInst;
			} catch (VisitFailure e) {
				return original;
			}
		}
	}

	public abstract static class AbstractTomTransformationPass {
		private AbstractInstructionTransformation transformation;
		private Block block;
		private Procedure proc;

		public AbstractTomTransformationPass(Block block) {
			this.block = block;
		}

		public AbstractTomTransformationPass(Procedure proc) {
			this.proc = proc;
		}

		protected abstract AbstractInstructionTransformation getTransformation();

		public void compute() {
			transformation = getTransformation();
			if (proc != null)
				transformation.transform(proc);
			else if (block != null) {
				transformation.transform(block);
			}
		}
	}
}
