/**
 */
package gecos.gecosproject;

import gecos.annotations.AnnotatedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Gecos File</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.gecosproject.GecosFile#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see gecos.gecosproject.GecosprojectPackage#getGecosFile()
 * @model abstract="true"
 * @generated
 */
public interface GecosFile extends ProjectVisitable, AnnotatedElement {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see gecos.gecosproject.GecosprojectPackage#getGecosFile_Name()
	 * @model unique="false" dataType="gecos.gecosproject.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link gecos.gecosproject.GecosFile#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // GecosFile
