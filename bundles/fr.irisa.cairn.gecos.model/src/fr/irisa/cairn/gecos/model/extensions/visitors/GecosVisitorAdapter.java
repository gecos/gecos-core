package fr.irisa.cairn.gecos.model.extensions.visitors;

import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDefaultVisitor;

/**
 * Abstract class for all visitors adapters. A visitor adapter is a
 * {@link GecosBlocksInstructionsDefaultVisitor} with a reference on the
 * adaptable visitor.
 * 
 * @author Antoine Floc'h - Initial contribution and API
 * 
 * @param <Visitor>
 *            Adaptable visitor type
 */
public abstract class GecosVisitorAdapter<Visitor> extends
		GecosBlocksInstructionsDefaultVisitor {
	protected Visitor visitor;

	public GecosVisitorAdapter(Visitor visitor) {
		this.visitor = visitor;
	}

	/**
	 * Get the adaptable visitor of this adapter
	 * 
	 * @return
	 */
	public Visitor getVisitor() {
		return visitor;
	}

}
