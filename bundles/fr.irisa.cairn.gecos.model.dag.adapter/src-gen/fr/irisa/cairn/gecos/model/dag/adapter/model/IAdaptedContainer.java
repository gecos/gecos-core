package fr.irisa.cairn.gecos.model.dag.adapter.model;

import gecos.dag.DAGEdge;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGNode;
import gecos.dag.DAGPort;

/**
 * Interface of the adapted container.
 * Provides methods to find the corresponding adapter to an adapted object.
 * 
 * @author <your name>
 *
 */
public interface IAdaptedContainer {

	public IAdaptedContainer getParent();

	public void setParent(IAdaptedContainer parent);

	public IAdaptedContainer getRoot();

	public IDAGInstructionAdapter getAdapterGraph(DAGInstruction graph);

	public IDAGNodeAdapter getAdapterNode(DAGNode node);

	public IDAGPortAdapter getAdapterPort(DAGPort port);

	public IDAGEdgeAdapter getAdapterEdge(DAGEdge edge);

	public void addGraph(IDAGInstructionAdapter adapter);

	public void addNode(IDAGNodeAdapter adapter);

	public void addPort(IDAGPortAdapter adapter);

	public void addEdge(IDAGEdgeAdapter adapter);

	public void removeGraph(DAGInstruction adapted);

	public void removeNode(DAGNode adapted);

	public void removePort(DAGPort adapted);

	public void removeEdge(DAGEdge adapted);

	public void removeGraph(IDAGInstructionAdapter adapter);

	public void removeNode(IDAGNodeAdapter adapter);

	public void removePort(IDAGPortAdapter adapter);

	public void removeEdge(IDAGEdgeAdapter adapter);

	public boolean containGraph(DAGInstruction adapted);

	public boolean containNode(DAGNode adapted);

	public boolean containPort(DAGPort adapted);

	public boolean containEdge(DAGEdge adapted);

}
