/**
 */
package profiling.frontend.directives;

import gecos.blocks.Block;

import gecos.core.Symbol;

import gecos.instrs.Instruction;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Size Directive</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link profiling.frontend.directives.SizeDirective#getNAME <em>NAME</em>}</li>
 *   <li>{@link profiling.frontend.directives.SizeDirective#getAddress <em>Address</em>}</li>
 *   <li>{@link profiling.frontend.directives.SizeDirective#getSizesOutermostFirst <em>Sizes Outermost First</em>}</li>
 * </ul>
 *
 * @see profiling.frontend.directives.DirectivesPackage#getSizeDirective()
 * @model
 * @generated
 */
public interface SizeDirective extends Directive {
	/**
	 * Returns the value of the '<em><b>NAME</b></em>' attribute.
	 * The default value is <code>"$size"</code>.
	 * The literals are from the enumeration {@link profiling.frontend.directives.DirectiveName}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>NAME</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>NAME</em>' attribute.
	 * @see profiling.frontend.directives.DirectiveName
	 * @see #setNAME(DirectiveName)
	 * @see profiling.frontend.directives.DirectivesPackage#getSizeDirective_NAME()
	 * @model default="$size" unique="false"
	 * @generated
	 */
	DirectiveName getNAME();

	/**
	 * Sets the value of the '{@link profiling.frontend.directives.SizeDirective#getNAME <em>NAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>NAME</em>' attribute.
	 * @see profiling.frontend.directives.DirectiveName
	 * @see #getNAME()
	 * @generated
	 */
	void setNAME(DirectiveName value);

	/**
	 * Returns the value of the '<em><b>Address</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Address</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Address</em>' reference.
	 * @see #setAddress(Symbol)
	 * @see profiling.frontend.directives.DirectivesPackage#getSizeDirective_Address()
	 * @model
	 * @generated
	 */
	Symbol getAddress();

	/**
	 * Sets the value of the '{@link profiling.frontend.directives.SizeDirective#getAddress <em>Address</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Address</em>' reference.
	 * @see #getAddress()
	 * @generated
	 */
	void setAddress(Symbol value);

	/**
	 * Returns the value of the '<em><b>Sizes Outermost First</b></em>' reference list.
	 * The list contents are of type {@link gecos.instrs.Instruction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sizes Outermost First</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sizes Outermost First</em>' reference list.
	 * @see profiling.frontend.directives.DirectivesPackage#getSizeDirective_SizesOutermostFirst()
	 * @model
	 * @generated
	 */
	EList<Instruction> getSizesOutermostFirst();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; args = this.getCall().getArgs();\nboolean _isEmpty = args.isEmpty();\nif (_isEmpty)\n{\n\t&lt;%java.lang.String%&gt; _printLocation = this.printLocation();\n\t&lt;%java.lang.String%&gt; _plus = (_printLocation + \"\\nMust have at least one argument (Symbol)!\");\n\tthrow new &lt;%fr.irisa.cairn.gecos.core.profiling.frontend.InvalidDirective%&gt;(_plus);\n}\n&lt;%gecos.core.Symbol%&gt; _xtrycatchfinallyexpression = null;\ntry\n{\n\t&lt;%gecos.instrs.Instruction%&gt; _get = args.get(0);\n\t&lt;%gecos.core.Symbol%&gt; _symbol = ((&lt;%gecos.instrs.SymbolInstruction%&gt;) _get).getSymbol();\n\t_xtrycatchfinallyexpression = _symbol;\n}\ncatch (final Throwable _t) {\n\tif (_t instanceof &lt;%java.lang.Exception%&gt;) {\n\t\t&lt;%java.lang.String%&gt; _printLocation_1 = this.printLocation();\n\t\t&lt;%java.lang.String%&gt; _plus_1 = (_printLocation_1 + \"\\nMust have a SymbolInstruction as first argument!\");\n\t\tthrow new &lt;%fr.irisa.cairn.gecos.core.profiling.frontend.InvalidDirective%&gt;(_plus_1);\n\t}\n\telse\n\t{\n\t\tthrow &lt;%org.eclipse.xtext.xbase.lib.Exceptions%&gt;.sneakyThrow(_t);\n\t}\n}\nfinal &lt;%gecos.core.Symbol%&gt; address = _xtrycatchfinallyexpression;\n&lt;%gecos.types.Type%&gt; _type = address.getType();\nfinal &lt;%fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer%&gt; ta = new &lt;%fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer%&gt;(_type, true);\nint _totalNbDims = ta.getTotalNbDims();\n&lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt; _doubleDotLessThan = new &lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt;(0, _totalNbDims, true);\nfor (final &lt;%java.lang.Integer%&gt; dim : _doubleDotLessThan)\n{\n\t{\n\t\t&lt;%gecos.instrs.Instruction%&gt; _xifexpression = null;\n\t\tint _size = args.size();\n\t\tboolean _greaterThan = (_size &gt; ((dim).intValue() + 1));\n\t\tif (_greaterThan)\n\t\t{\n\t\t\t_xifexpression = args.get((1 + (dim).intValue()));\n\t\t}\n\t\telse\n\t\t{\n\t\t\t_xifexpression = ta.tryGetDimSize((dim).intValue());\n\t\t}\n\t\tfinal &lt;%gecos.instrs.Instruction%&gt; sz = _xifexpression;\n\t\tif ((sz == null))\n\t\t{\n\t\t\t&lt;%java.lang.String%&gt; _printLocation_2 = this.printLocation();\n\t\t\t&lt;%java.lang.String%&gt; _plus_2 = (_printLocation_2 + \"\\nMust specify a size for dimension \\\'\");\n\t\t\t&lt;%java.lang.String%&gt; _plus_3 = (_plus_2 + dim);\n\t\t\t&lt;%java.lang.String%&gt; _plus_4 = (_plus_3 + \"\\\' which couldn\\\'t be determined automatically!\");\n\t\t\tthrow new &lt;%fr.irisa.cairn.gecos.core.profiling.frontend.InvalidDirective%&gt;(_plus_4);\n\t\t}\n\t\tthis.getSizesOutermostFirst().add(sz);\n\t}\n}\naddress.getAnnotations().put(&lt;%fr.irisa.cairn.gecos.core.profiling.frontend.DirectivesProcessor%&gt;.SIZE_ANNOTATION_KEY, &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory%&gt;.extendedRefers(((&lt;%org.eclipse.emf.ecore.EObject%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(this.getSizesOutermostFirst(), &lt;%org.eclipse.emf.ecore.EObject%&gt;.class))));'"
	 * @generated
	 */
	void parse();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory%&gt;.BBlock();'"
	 * @generated
	 */
	Block createCodeBlock();

} // SizeDirective
