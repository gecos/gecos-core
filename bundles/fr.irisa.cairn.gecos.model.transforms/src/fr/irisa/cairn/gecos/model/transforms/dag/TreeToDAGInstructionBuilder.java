package fr.irisa.cairn.gecos.model.transforms.dag;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.gecos.model.analysis.instructions.comparator.FuzzyBoolean;
import fr.irisa.cairn.gecos.model.analysis.instructions.comparator.InstructionComparator;
import fr.irisa.cairn.gecos.model.dag.adapter.DAGInstructionAdapterComponent;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGInstructionAdapter;
import fr.irisa.cairn.gecos.model.factory.GecosUserDagFactory;
import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDefaultVisitor;
import fr.irisa.cairn.gecos.model.transforms.dag.internal.DAGAnnotationBuilder;
import gecos.blocks.BasicBlock;
import gecos.core.Symbol;
import gecos.dag.DAGCallNode;
import gecos.dag.DAGControlEdge;
import gecos.dag.DAGDataEdge;
import gecos.dag.DAGInPort;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGNode;
import gecos.dag.DAGNumberedSymbolNode;
import gecos.dag.DAGOpNode;
import gecos.dag.DAGOperator;
import gecos.dag.DAGOutNode;
import gecos.dag.DAGOutPort;
import gecos.dag.DAGSimpleArrayNode;
import gecos.dag.DAGSymbolNode;
import gecos.dag.DependencyType;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.ArrayValueInstruction;
import gecos.instrs.BreakInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.CondInstruction;
import gecos.instrs.ContinueInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.DummyInstruction;
import gecos.instrs.FieldInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.GotoInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.NumberedSymbolInstruction;
import gecos.instrs.PhiInstruction;
import gecos.instrs.RangeInstruction;
import gecos.instrs.RetInstruction;
import gecos.instrs.SSADefSymbol;
import gecos.instrs.SSAUseSymbol;
import gecos.instrs.SetInstruction;
import gecos.instrs.SimpleArrayInstruction;
import gecos.instrs.StringInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.BaseType;
import gecos.types.Type;

public class TreeToDAGInstructionBuilder extends BlockInstructionSwitch<Object> {

	private static final boolean EXPERIMENTAL = true;
	private static final boolean DEBUG = false;
	private static final boolean DEBUG_GRAPH = false;

	private static int DEBUG_INDEX = 1;
	private static final void debug(String str) {
		if(DEBUG) {
			if(DEBUG_INDEX < 3) {
				for(int i=0; i<DEBUG_INDEX; i++)
					System.out.print("   ");
				System.out.println(str);
			}
		}
	}
	private static final void incINDEX() {
		if(DEBUG) DEBUG_INDEX ++;
	}
	private static final void decINDEX() {
		if(DEBUG) DEBUG_INDEX --;
	}
	
	private static int FILEN = 0;
	private static final void debug_graph(DAGInstruction dag) {
		if(DEBUG_GRAPH) {
			if(!dag.getNodes().isEmpty()) {
				IDAGInstructionAdapter dagAdapter = DAGInstructionAdapterComponent.INSTANCE.build(dag);
				DAGInstructionAdapterComponent.INSTANCE.getGraphExportService().export("dag_"+FILEN+++".dot", dagAdapter);
			}
		}
	}
	
	
	public DAGInstruction dagInst;
	private Map<Symbol, Integer> lastSSAWriteNumber;
	private Map<Symbol, DAGNode> symbolDefNodeMapping;
	private Map<Symbol, DAGNode> symbolReDefNodeMapping;
	protected Stack<DAGNode> nodeStack;
	// private DAGNode lastSideEffect;
	private int id = 0;
	private MemoryDependenciesManager memoryDependenciesManager;
	private VariableOutputDependencyManager outputDependencyManager;

	
	public TreeToDAGInstructionBuilder() {
		this.nodeStack = new Stack<DAGNode>();
		this.lastSSAWriteNumber = new LinkedHashMap<Symbol, Integer>();
		this.symbolDefNodeMapping = new LinkedHashMap<Symbol, DAGNode>();
		this.symbolReDefNodeMapping = new LinkedHashMap<Symbol, DAGNode>(); //XXX not used
	}
	
	public DAGInstruction build(List<Instruction> instrs) {
		if(DEBUG) debug("InstructionBuilder build instructions");
		incINDEX();
		
		dagInst = GecosUserDagFactory.createDAGInstruction();
		memoryDependenciesManager = new MemoryDependenciesManager();
		outputDependencyManager = new VariableOutputDependencyManager();
		returnNode = null;
		symbolDefNodeMapping.clear();
		symbolReDefNodeMapping.clear();
		// lastSideEffect = null;
		for (Instruction inst : instrs) {
			if(DEBUG) debug("InstructionBuilder build: " + inst);
			incINDEX();
			
			nodeStack.clear();
			doSwitch(inst);
			
			decINDEX();
			if(DEBUG) debug("|-> dag: " + dagInst);
		}
		decINDEX();
		
		if(DEBUG) debug("|-> symbolDefNodeMapping: " + symbolDefNodeMapping);
		
		Map<Symbol, DAGOutNode> outputsMap = outputChangedSymbols();
		if(DEBUG) debug("|-> outputsMap: " + outputsMap);

		Set<DAGNode> lastNodes = new LinkedHashSet<DAGNode>(outputsMap.values());
		if(DEBUG) debug("|-> lastNodes: " + lastNodes);
		
		if(DEBUG) debug("|-> outputs: " + outputDependencyManager.outputs);
		if(DEBUG) debug("|-> WARs: " + memoryDependenciesManager.WARs);
		
//		for(DAGSimpleArrayNode from : memoryDependenciesManager.WARs.keySet()) {
//			DAGSimpleArrayNode to = memoryDependenciesManager.WARs.get(from);
//			DAGNode succ = to.getDataSuccessors().get(0);
//			if(succ instanceof DAGOpNode && ((DAGOpNode)succ).getOpcode() == DagIntructionsTypes.SET) { //src is a write
//				DAGOutPort outport = UserDagFactory.createDAGOutputPort(from);
//				DAGInPort inport = UserDagFactory.createDAGInputPort(succ);
//				instruction.getEdges().add(UserDagFactory.createDAGControlEdge(inport, outport, DependencyType.WAR));
//			}
//		}debug_graph
		
		/*
		 * XXX this adds a non-existing dependency between some outputs!
		 * Instead, we should add the dependency between the dependent nodes.
		 * E.g: x = a+1; 
		 * 		a = b;
		 * The dependency here is a WAR on "a". However the following adds a WAW x -> a.
		 */
		// Manage order between outputs
		for (Symbol output : outputsMap.keySet()) {
			List<VariableOutputDependency> predecessors = outputDependencyManager.findLexicalDependencies(output);
			DAGOutNode outputNode = outputsMap.get(output);
			if (predecessors.size() > 0) {
				for (VariableOutputDependency pred : predecessors) {
					DAGNode predOutputNode = outputsMap.get(pred.output);
					// Output dependency is not on a DAG out node
					// symbol(pointers, arrays, etc.)
					if (predOutputNode == null)
						predOutputNode = pred.node;
					makeControlEdge(predOutputNode, outputNode);
					lastNodes.remove(predOutputNode);
				}
			}
		}
		if (returnNode != null) {
			for (DAGNode lastNode : lastNodes) {
				makeControlEdge(lastNode, returnNode);
			}
		}
		
		debug_graph(dagInst);
		
		return dagInst;
	}

	public DAGInstruction build(BasicBlock b) {
		build(b.getInstructions());
		b.getInstructions().clear();
		b.getInstructions().add(dagInst);
		return dagInst;
	}

	public DAGInstruction getInstruction() {
		return dagInst;
	}

	public Stack<DAGNode> getNodeStack() {
		return nodeStack;
	}

	/**
	 * Create a new data edge.
	 * 
	 * @param from
	 *            source node
	 * @param out
	 *            source output port number
	 * @param to
	 *            destination node
	 * @param in
	 *            destination port number
	 * @return
	 */
	public DAGDataEdge makeDataEdge(DAGNode from, int out, DAGNode to, int in) {
		if (out >= from.getOutputs().size()) {
			throw new RuntimeException("Node " + from
					+ " has not enough output ports (" + (out + 1)
					+ " required, " + from.getOutputs().size() + " found)");
		}
		if (in >= to.getInputs().size()) {
			throw new RuntimeException("Node " + to
					+ " has not enough input ports (" + (in + 1)
					+ " required, " + from.getInputs().size() + " found)");
		}
		DAGOutPort outport = from.getOutputs().get(out);
		outport.setIdent(out);

		DAGInPort inport = to.getInputs().get(in);
		inport.setIdent(in);
		if (inport == null || outport == null) {
			throw new UnsupportedOperationException("null port");
		}
		return GecosUserDagFactory.createDAGDataEdge(inport, outport);
	}

	private void makeControlEdge(DAGNode from, DAGNode to) {
		assert (to.eContainer() instanceof DAGInstruction);
		assert (from.eContainer() instanceof DAGInstruction);
		if (!from.getSuccessors().contains(to)) {
			DAGOutPort outport = GecosUserDagFactory.createDAGOutputPort(from);
			outport.setIdent(from.getOutputs().size() - 1);
			DAGInPort inport = GecosUserDagFactory.createDAGInputPort(to);
			inport.setIdent(to.getInputs().size() - 1);
			DAGControlEdge createDAGControlEdge = GecosUserDagFactory.createDAGControlEdge(inport, outport, DependencyType.WAW); //XXX this may be a fake dependency 
			DAGInstruction instr = (DAGInstruction) from.eContainer();
			instr.getEdges().add(createDAGControlEdge);
		}
	}

	DAGControlEdge makeControlEdge(DAGNode from, int out, DAGNode to,
			int in) {
		if (out >= from.getOutputs().size()) {
			throw new RuntimeException("Node " + from
					+ " has not enough output ports (" + (out + 1)
					+ " required, " + from.getOutputs().size() + " found)");
		}
		if (in >= to.getInputs().size()) {
			throw new RuntimeException("Node " + to
					+ " has not enough input ports (" + (in + 1)
					+ " required, " + from.getInputs().size() + " found)");
		}
		DAGOutPort outport = from.getOutputs().get(out);
		DAGInPort inport = to.getInputs().get(in);

		return GecosUserDagFactory.createDAGControlEdge(inport, outport, DependencyType.RAW);
	}

	private Map<Symbol, DAGOutNode> outputChangedSymbols() {
		Iterator<Symbol> i = symbolDefNodeMapping.keySet().iterator();
		Map<Symbol, DAGOutNode> outputsMap = new LinkedHashMap<Symbol, DAGOutNode>();
		while (i.hasNext()) {
			Symbol symbol = i.next();

			DAGOutNode out = null;
			Integer number = lastSSAWriteNumber.get(symbol);
			DAGNode node = getModifiedNode(symbol);
			if (node != null) {
				if (number != null) {
					/* we are in SSA mode */
					out = GecosUserDagFactory.createDAGNumberedOutNode(dagInst,
							symbol, number, dagInst.getType());
				} else {
					out = GecosUserDagFactory.createDAGOutNode(dagInst, symbol);
				}
				GecosUserDagFactory.createDAGInputPort(out);
				dagInst.getNodes().add(out);
				dagInst.getEdges().add(makeDataEdge(node, 0, out, 0));
				outputsMap.put(symbol, out);
			}

		}
		return outputsMap;
	}

	/**
	 * Get the last value of a symbol.
	 * 
	 * @param s
	 * @return null if symbol hasn't been modified
	 */
	private DAGNode getModifiedNode(Symbol s) {
		DAGNode node = symbolDefNodeMapping.get(s);
		if (node instanceof DAGSymbolNode) {
			Symbol value = ((DAGSymbolNode) node).getSymbol();
			if (value != s)
				return node;
			else
				return null;
		}
		return node;
	}

	private List<Symbol> currentSymbols = new UniqueEList<Symbol>();

	private boolean inLHS = false;
	private int indirLevel = 0;
//	private boolean isIndex = false;

	@Override
	public Object caseSSADefSymbol(SSADefSymbol s) {
		Symbol symbol = s.getSymbol();
		DAGNode dagSymbolNode = symbolDefNodeMapping.get(symbol);

		if (dagSymbolNode != null
				&& dagSymbolNode instanceof DAGNumberedSymbolNode) {
			DAGNumberedSymbolNode nnode = (DAGNumberedSymbolNode) dagSymbolNode;
			if (nnode.getNumber() < s.getNumber()) {
				nnode.setNumber(s.getNumber());
				symbolDefNodeMapping.put(s.getSymbol(), nnode);
			}
		}
		if (dagSymbolNode == null) {
			dagSymbolNode = GecosUserDagFactory.createDAGNumberedSymbolNode(
					dagInst, s.getSymbol(), s.getNumber());
			symbolDefNodeMapping.put(s.getSymbol(), dagSymbolNode);
		}

		dagInst.getNodes().add(dagSymbolNode);
		currentSymbols.add(symbol);
		nodeStack.push(dagSymbolNode);

		if (indirLevel == 0) {
			MemoryAccess m = new ScalarAccess(s, dagSymbolNode, true);
			memoryDependenciesManager.manage(m);
		}
		return dagSymbolNode;
	}

	@Override
	public Object caseSSAUseSymbol(SSAUseSymbol s) {
		Symbol symbol = s.getSymbol();

		DAGNode dagSymbolNode = symbolDefNodeMapping.get(symbol);

		if (dagSymbolNode != null
				&& dagSymbolNode instanceof DAGNumberedSymbolNode) {
			DAGNumberedSymbolNode nnode = (DAGNumberedSymbolNode) dagSymbolNode;
			if (nnode.getNumber() != s.getNumber()) {
				dagSymbolNode = GecosUserDagFactory.createDAGNumberedSymbolNode(
						dagInst, s.getSymbol(), s.getNumber());
				symbolDefNodeMapping.put(s.getSymbol(), dagSymbolNode);
			}
		}
		if (dagSymbolNode == null) {
			dagSymbolNode = GecosUserDagFactory.createDAGNumberedSymbolNode(
					dagInst, s.getSymbol(), s.getNumber());
			symbolDefNodeMapping.put(s.getSymbol(), dagSymbolNode);
		}

		dagInst.getNodes().add(dagSymbolNode);
		currentSymbols.add(symbol);
		nodeStack.push(dagSymbolNode);

		if (indirLevel == 0) {
			MemoryAccess m = new ScalarAccess(s, dagSymbolNode, false);
			memoryDependenciesManager.manage(m);
		}
		return dagSymbolNode;
	}

	@Override
	public Object caseNumberedSymbolInstruction(NumberedSymbolInstruction s) {
		DAGNode symbol = symbolDefNodeMapping.get(s.getSymbol());
		if (symbol == null) {
			symbol = GecosUserDagFactory.createDAGNumberedSymbolNode(dagInst,
					s.getSymbol(), s.getNumber());
			symbolDefNodeMapping.put(s.getSymbol(), symbol);
			dagInst.getNodes().add(symbol);
		} else {
			System.out.println("symbol " + symbol
					+ " already present in symbolMapping map");
		}
		nodeStack.push(symbol);
		return symbol;
	}

	@Override
	public Object caseSymbolInstruction(SymbolInstruction s) {
		Symbol symbol = s.getSymbol();
		DAGNode manageSymbol = manageSymbol(symbol);
		if (indirLevel == 0) {
			if (inLHS) {
				MemoryAccess m = new ScalarAccess(s, manageSymbol, true);
				memoryDependenciesManager.manage(m);

			} else {
				MemoryAccess m = new ScalarAccess(s, manageSymbol, false);
				memoryDependenciesManager.manage(m);
			}
		}
		return manageSymbol;
	}

	public DAGNode manageSymbol(Symbol symbol) {
		DAGNode dagSymbolNode = symbolDefNodeMapping.get(symbol);
		if (dagSymbolNode == null) {
			dagSymbolNode = GecosUserDagFactory.createDAGSymbolNode(dagInst,
					symbol);
			symbolDefNodeMapping.put(symbol, dagSymbolNode);
			symbolReDefNodeMapping.put(symbol, dagSymbolNode);
			dagInst.getNodes().add(dagSymbolNode);
		}
//		if(!isIndex)
			currentSymbols.add(symbol);
		nodeStack.push(dagSymbolNode);
		return dagSymbolNode;
	}

	@Override
	public Object caseSetInstruction(SetInstruction s) {
		SetInstructionManager manager = new SetInstructionManager(s);
		manager.manage();
		return s;
	}

	/**
	 * Class that manage Set instructions in function of their destination type.
	 * 
	 * @author Antoine Floc'h - Initial contribution and API
	 * 
	 */
	public class SetInstructionManager extends
			GecosBlocksInstructionsDefaultVisitor {
		private SetInstruction setInstruction;
		private DAGNode setNode;

		public SetInstructionManager(SetInstruction setInstruction) {
			this.setInstruction = setInstruction;
		}

		public Stack<DAGNode> getNodeStack() {
			return nodeStack;
		}

		public void manage() {
			inLHS = false;
			doSwitch(setInstruction.getSource());
			inLHS = true;
			setInstruction.getDest().accept(this);
		}

		public MemoryDependenciesManager getMemoryDependenciesManager() {
			return memoryDependenciesManager;
		}

		public VariableOutputDependencyManager getOutputDependencyManager() {
			return outputDependencyManager;
		}

		@Override
		public void visitSSADefSymbol(SSADefSymbol s) {
			super.visitSSADefSymbol(s);
			lastSSAWriteNumber.put(s.getSymbol(), s.getNumber());
		}

		@Override
		public void visitSymbolInstruction(SymbolInstruction s) {
			DAGNode node = nodeStack.pop();
			symbolDefNodeMapping.put(s.getSymbol(), node);
			outputDependencyManager.notifyOutput(s.getSymbol(), node, new ArrayList<Symbol>(currentSymbols));
			currentSymbols.clear();
		}

		@Override
		public void visitSimpleArrayInstruction(SimpleArrayInstruction s) {
			super.visitSimpleArrayInstruction(s);
			if (isWriteAccess(s)) {
				outputDependencyManager.notifyOutput(s.getSymbol(), setNode, new ArrayList<Symbol>(currentSymbols));
				currentSymbols.clear();
			}
		}

		@Override
		protected void visitComplexInstruction(ComplexInstruction c) {
			visitInstruction(c);
		}

		@Override
		public void visitInstruction(Instruction s) {
			doSwitch(s);
			setNode = createSetNode();
			dagInst.getNodes().add(setNode);
			if (nodeStack.size() < 2) {
				throw new RuntimeException(
						"ModelDagbuilder error : instruction " + setInstruction
								+ " has no childs in Node stacks");
			}
			DAGDataEdge edge1 = makeDataEdge((DAGNode) nodeStack.pop(), 0, setNode, 0);
			DAGDataEdge edge2 = makeDataEdge((DAGNode) nodeStack.pop(), 0, setNode, 1);
			dagInst.getEdges().add(edge1);
			dagInst.getEdges().add(edge2);
		}

		private DAGNode createSetNode() {
			DAGNode node = GecosUserDagFactory.createDAGOPNode(dagInst, DagIntructionsTypes.SET, setInstruction.getType());
			node.setIdent(id++);
			GecosUserDagFactory.createDAGOutputPort(node);
			GecosUserDagFactory.createDAGInputPort(node);
			GecosUserDagFactory.createDAGInputPort(node);
			DAGAnnotationBuilder annotBuilder = new DAGAnnotationBuilder();
			annotBuilder.buildDAGAnnotation(setInstruction, node);
			return node;
		}
	}

	@Override
	public Object caseIntInstruction(IntInstruction i) {
		DAGNode imm = GecosUserDagFactory.createDAGIntImmediateNode(dagInst, i);
		GecosUserDagFactory.createDAGOutputPort(imm);
		dagInst.getNodes().add(imm);
		nodeStack.push(imm);
		return imm;
	}

	@Override
	public Object caseArrayValueInstruction(ArrayValueInstruction i) {
		DAGNode imm = GecosUserDagFactory.createDAGArrayValueNode(dagInst, i);
		dagInst.getNodes().add(imm);
		nodeStack.push(imm);
		return imm;
	}

	@Override
	public Object caseArrayInstruction(ArrayInstruction object) {
		throw new UnsupportedOperationException(
				"Support for ArrayInstruction in DAGBuilder is not yet complete.");
	}

	@Override
	public Object caseSimpleArrayInstruction(SimpleArrayInstruction object) {
//		Object index = null;

//		isIndex  = true;
		for (Instruction instr : object.getIndex()) {
//			index = 
					doSwitch(instr);
		}
//		isIndex = false;

		// *** buildDAGOpnode
		DAGSimpleArrayNode node = GecosUserDagFactory.createDAGSimpleArrayNode(dagInst, object);
		node.setIdent(id++);
//		currentSymbols.add(node.getSymbol());

		DAGAnnotationBuilder annotBuilder = new DAGAnnotationBuilder();
		annotBuilder.buildDAGAnnotation(object, node);

		dagInst.getNodes().add(node);

		int nbDims = object.getIndex().size();
		for (int i = 0; i < nbDims; i++) {
			DAGNode indexnode = (DAGNode) nodeStack.pop();
			DAGDataEdge makeDataEdge = makeDataEdge(indexnode, 0, node, i);
			dagInst.getEdges().add(makeDataEdge); // connect
		}
		nodeStack.push(node);

		// Manage memory dependencies
		boolean write = isWriteAccess(object);
		MemoryAccess m = new ArrayAccess(object, node, write);
		memoryDependenciesManager.manage(m);
		return node;
	}

	@Override
	public Object caseGenericInstruction(GenericInstruction inst) {
		super.caseGenericInstruction(inst);
		buildDAGOpNode(inst, inst.getName());
		return inst;
	}

	private void buildDAGOpNode(ComplexInstruction inst, String name) {
		// pred/succ

		DAGNode node = GecosUserDagFactory.createDAGOPNode(dagInst, name,
				inst.getType());
		node.setIdent(id++);
		Type type = inst.getType();
		if (type == null)
			throw new UnsupportedOperationException("Null type for " + inst);
		node.setType(type);
		DAGAnnotationBuilder annotBuilder = new DAGAnnotationBuilder();
		annotBuilder.buildDAGAnnotation(inst, node);

		dagInst.getNodes().add(node);

		GecosUserDagFactory.createDAGOutputPort(node);

		// connect inputs
		for (int i = inst.getChildrenCount() - 1; i >= 0; --i) {
			GecosUserDagFactory.createDAGInputPort(node);
		}

		for (int i = inst.getChildrenCount() - 1; i >= 0; --i) {
			DAGNode source = (DAGNode) nodeStack.pop();
			dagInst.getEdges().add(makeDataEdge(source, 0, node, i));
		}
		nodeStack.push(node);
	}

	@Override
	public Object caseCallInstruction(CallInstruction inst) {

		super.caseCallInstruction(inst);
		boolean isBasic = (inst.getType() instanceof BaseType);

		// FIXME : was boolean hasOutput = ! isBasic ||
		// ((BaseType)inst.getType()).getType().name() != BaseType.VOID;
		boolean hasOutput = !isBasic
				|| ((BaseType) inst.getType()).asVoid() == null;
		DAGCallNode node = GecosUserDagFactory.createDAGCallNode(dagInst,
				inst.getSymbolAddress(), inst.getType(), hasOutput);
		dagInst.getNodes().add(node);

		if (hasOutput) {
			GecosUserDagFactory.createDAGOutputPort(node);
		}

		bindDAGNodeInputs(inst.listChildren(), node);
		/*
		 * if (lastCall != null) {
		 * instruction.getEdges().add(UserDagFactory.createDAGControlEdge
		 * (lastCall, node)); } lastCall = node;
		 */
		return node;
	}

	public void bindDAGNodeInputs(List<Instruction> inst, DAGCallNode node) {
		for (int i = inst.size() - 1; i >= 0; --i) {
			GecosUserDagFactory.createDAGInputPort(node);
		}

		for (int i = inst.size() - 1; i >= 0; --i) {
			DAGNode child = (DAGNode) nodeStack.pop();
			dagInst.getEdges().add(makeDataEdge(child, 0, node, i));
		}
		nodeStack.push(node);
	}

	@Override
	public Object caseIndirInstruction(IndirInstruction i) {
		if (!EXPERIMENTAL) {
			throw new UnsupportedOperationException(
					"Support for indirection in DAGBuilder is not yet complete.\nIf you still want to play (at your own risk), set the constant EXPERIMENTAL to true in TreeToDAGInstructionBuilder");
		} else {
			indirLevel++;
			super.caseIndirInstruction(i);
			indirLevel--;
			DAGNode node = GecosUserDagFactory.createDAGOPNode(dagInst,
					DagIntructionsTypes.INDIR, i.getType());
			GecosUserDagFactory.createDAGOutputPort(node);
			GecosUserDagFactory.createDAGInputPort(node);
			dagInst.getNodes().add(node);
			dagInst.getEdges().add(
					makeDataEdge((DAGNode) nodeStack.pop(), 0, node, 0));
			nodeStack.push(node);

			// Manage memory dependencies
//			boolean write = 
					isWriteAccess(i);
			// MemoryAccess m = new IndirAccess(i, node, write);
			// memoryDependenciesManager.manage(m);

			return node;
		}
	}

	private boolean isWriteAccess(Instruction object) {
		EObject eContainer = object.eContainer();
		if (eContainer instanceof SetInstruction) {
			if (((SetInstruction) eContainer).getDest() == object)
				return true;
		}
		return false;
	}

	@Override
	public Object caseBreakInstruction(BreakInstruction object) {
		// FIXME
		DAGNode node = GecosUserDagFactory.createDAGOPNode(dagInst,
				DagIntructionsTypes.JUMP, object.getType());
		dagInst.getNodes().add(node);
		GecosUserDagFactory.createDAGInputPort(node);

		nodeStack.push(node);
		return node;
	}

	@Override
	public Object caseContinueInstruction(ContinueInstruction object) {
		// FIXME
		DAGNode node = GecosUserDagFactory.createDAGOPNode(dagInst,
				DagIntructionsTypes.JUMP, object.getType());
		dagInst.getNodes().add(node);
		GecosUserDagFactory.createDAGInputPort(node);

		nodeStack.push(node);
		return node;
	}

	@Override
	public Object caseGotoInstruction(GotoInstruction object) {
		// FIXME
		DAGNode node = GecosUserDagFactory.createDAGOPNode(dagInst,
				DagIntructionsTypes.JUMP, object.getType());
		dagInst.getNodes().add(node);
		GecosUserDagFactory.createDAGInputPort(node);

		nodeStack.push(node);
		return node;
	}

	@Override
	public Object caseCondInstruction(CondInstruction g) {
		// FIXME
		DAGNode node = GecosUserDagFactory.createDAGOPNode(dagInst,
				DagIntructionsTypes.JUMP, g.getType());
		dagInst.getNodes().add(node);
		GecosUserDagFactory.createDAGInputPort(node);
		if (g.getCond() != null) {
			doSwitch(g.getCond());
			dagInst.getEdges().add(
					makeDataEdge((DAGNode) nodeStack.pop(), 0, node, 0));
		}
		nodeStack.push(node);
		return node;
	}

	@Override
	public Object caseConvertInstruction(ConvertInstruction conv) {
		DAGNode node = GecosUserDagFactory.createDAGOPNode(dagInst,
				DagIntructionsTypes.CVT, conv.getType());
		node.setType(conv.getType());
		GecosUserDagFactory.createDAGOutputPort(node);
		GecosUserDagFactory.createDAGInputPort(node);

		dagInst.getNodes().add(node);
		doSwitch(conv.getExpr());
		dagInst.getEdges().add(
				makeDataEdge((DAGNode) nodeStack.pop(), 0, node, 0));
		nodeStack.push(node);
		return node;
	}

	@Override
	public Object caseRangeInstruction(RangeInstruction g) {
		DAGNode node = GecosUserDagFactory
				.createDAGOPNode(dagInst,
						DagIntructionsTypes.RANGE(g.getLow(), g.getHigh()),
						g.getType());
		dagInst.getNodes().add(node);
		GecosUserDagFactory.createDAGOutputPort(node);
		GecosUserDagFactory.createDAGInputPort(node);

		doSwitch(g.getExpr());
		dagInst.getEdges().add(
				makeDataEdge((DAGNode) nodeStack.pop(), 0, node, 0));
		nodeStack.push(node);
		return node;
	}

	@Override
	public Object caseAddressInstruction(AddressInstruction inst) {
		super.caseAddressInstruction(inst);
		buildDAGOpNode(inst, "address");
		return nodeStack.peek();
	}

	@Override
	public Object caseFieldInstruction(FieldInstruction g) {
		super.caseFieldInstruction(g);
		buildDAGOpNode(g, "field");
		return nodeStack.peek();
	}

	@Override
	public Object caseFloatInstruction(FloatInstruction floatInstruction) {
		DAGNode imm = GecosUserDagFactory.createDAGFloatImmediateNode(dagInst,
				floatInstruction);
		GecosUserDagFactory.createDAGOutputPort(imm);
		dagInst.getNodes().add(imm);
		nodeStack.push(imm);
		return imm;
	}

	@Override
	public Object casePhiInstruction(PhiInstruction inst) {
		super.casePhiInstruction(inst);
		buildDAGOpNode(inst, "phy");
		return nodeStack.peek();
	}

	private DAGNode returnNode;
	@Override
	public Object caseRetInstruction(RetInstruction g) {
		super.caseRetInstruction(g);
		if (g.getExpr() != null && !(g.getExpr() instanceof DummyInstruction)) {
			buildDAGOpNode(g, "ret");
			// only one return node by DAGInstruction
			returnNode = nodeStack.peek();
		}
		return true;
	}
	
	@Override
	public Object caseDummyInstruction(DummyInstruction object) {
		//prune switch
		return true;
	}
	
	@Override
	public Object caseStringInstruction(StringInstruction i) { 
		DAGNode str = GecosUserDagFactory.createDAGStringImmNode(dagInst, i.getValue());
		GecosUserDagFactory.createDAGOutputPort(str);
		dagInst.getNodes().add(str);
		nodeStack.push(str);
		return str;
	}
	
	@Override
	public Object caseInstruction(Instruction object) {
		throw new UnsupportedOperationException("Support for " + object + ":"
				+ object.getClass().getSimpleName()
				+ " in DAGBuilder not yet implemented");
	}

	// @Override
	// public Object caseLabelInstruction(LabelInstruction object) {
	// System.err.println("Ignoring LabelInstructions");
	// return nodeStack.peek();
	// }

	public static class DagIntructionsTypes {
		public final static String SET = DAGOperator.SET.getLiteral();    
		public final static String CVT = DAGOperator.CVT.getLiteral();    
		public final static String INDIR = DAGOperator.INDIR.getLiteral();
		public final static String JUMP = DAGOperator.JUMP.getLiteral();

		public static String RANGE(long low, long high) {
			StringBuffer sb = new StringBuffer("RANGE(");
			sb.append(high).append(":").append(low).append(")");
			return sb.toString();
		}
	}

	private static abstract class MemoryAccess {
		private final boolean write;
		private final DAGNode node;
		private final Instruction instr;

		public MemoryAccess(Instruction instr, DAGNode node, boolean write) {
			this.write = write;
			this.node = node;
			this.instr = instr;
		}

		public boolean isWrite() {
			return write;
		}

		public abstract FuzzyBoolean accessToSameLocation(MemoryAccess a);

		public DAGNode getNode() {
			return node;
		}

		public Instruction getInstr() {
			return instr;
		}

	}

	private static class ArrayAccess extends MemoryAccess {

		private final Symbol array;

		public ArrayAccess(SimpleArrayInstruction instr,
				DAGSimpleArrayNode node, boolean write) {
			super(instr, node, write);
			array = node.getSymbol();
		}

		/**
		 * @return YES/MAYBE if both access the same array and their indexes are either equivalent (YES)
		 * or maybe equivalent (MAYBE). NO otherwise
		 * 
		 */
		public FuzzyBoolean accessToSameLocation(MemoryAccess a) {
			if(DEBUG) debug("mayAccessToSameLocation as \"" + a + "\" ?");
			
			if (a instanceof ArrayAccess) {
				ArrayAccess a2 = (ArrayAccess) a;
				if (array == a2.array)
					return haveSameIndices(a2);
				
				return FuzzyBoolean.NO;
			} else //??
				return a.accessToSameLocation(this);
		}

		/**
		 * Use the InstructionComparator to compare indexes starting from the outermost dimension.
		 * 
		 * @param a
		 * @return  YES if all indexes are equivalent.
		 * 			NO if either both arrays have different number of indexes or if the first non equivalent (YES) indexes are strictly different (NO)  
		 * 			MAYBE if if the first non equivalent (YES) indexes are maybe different (MAYBE) 
		 * 
		 */
		protected FuzzyBoolean haveSameIndices(ArrayAccess a) {

			EList<Instruction> index = getInstr().getIndex();
			EList<Instruction> index2 = a.getInstr().getIndex();
//			assert (index.size() == index2.size());
			if(index.size() != index2.size())
				return FuzzyBoolean.NO;

			for (int i = 0; i < index.size(); i++) {
				Instruction idx1 = index.get(i);
				Instruction idx2 = index2.get(i);
				
//				FuzzyBoolean res = IsInstructionEquivalent.isEquivalent(idx1, idx2);
				FuzzyBoolean res = InstructionComparator.isEquivalent(idx1, idx2);
				if(res != FuzzyBoolean.YES)
					return res;
			}
			
			return FuzzyBoolean.YES;
		}

		@Override
		public DAGSimpleArrayNode getNode() {
			return (DAGSimpleArrayNode) super.getNode();
		}

		public SimpleArrayInstruction getInstr() {
			return (SimpleArrayInstruction) super.getInstr();
		}
		
		@Override
		public String toString() {
			return "ArrayAccess: " + array;
		}
	}

	private static class IndirAccess extends MemoryAccess {

		public IndirAccess(Instruction instr, DAGNode node, boolean write) {
			super(instr, node, write);
		}

		public FuzzyBoolean accessToSameLocation(MemoryAccess a) {
			// Instruction ia = a.getInstr().getRoot();
			// Instruction ib = this.getInstr().getRoot();
			// BasicBlock basicBlock = a.getInstr().getRoot().getBasicBlock();
			// if(a.write ) {
			// if(basicBlock.getInstructions().indexOf(ia)<basicBlock.getInstructions().indexOf(ib))
			// {
			// return true;
			// }
			// } else if(this.isWrite()){
			// if(basicBlock.getInstructions().indexOf(ia)>basicBlock.getInstructions().indexOf(ib))
			// {
			// return true;
			// }
			// }
			return FuzzyBoolean.YES;
		}
		
		@Override
		public String toString() {
			return "IndirAccess: ";
		}
	}

	private static class ScalarAccess extends MemoryAccess {
		private Symbol symbol;

		public ScalarAccess(SymbolInstruction instr, DAGNode node, boolean write) {
			super(instr, node, write);
			this.symbol = instr.getSymbol();
		}

		public FuzzyBoolean accessToSameLocation(MemoryAccess a) {
			if (a instanceof ScalarAccess) {
				if (((ScalarAccess) a).symbol == symbol)
					return FuzzyBoolean.YES;
			}
			if (a instanceof IndirAccess) {
				return FuzzyBoolean.YES;
			}
			return FuzzyBoolean.NO;
		}

		@Override
		public String toString() {
			return "ScalarAccess: " + symbol;
		}
	}

	public static class VariableOutputDependencyManager {
		private List<VariableOutputDependency> outputs = new ArrayList<TreeToDAGInstructionBuilder.VariableOutputDependency>();
		private Set<Symbol> symbols = new LinkedHashSet<Symbol>();

		public void notifyOutput(Symbol sym, DAGNode node, List<Symbol> inputs) {
			this.outputs.add(new VariableOutputDependency(sym, node, inputs));
			this.symbols.add(sym);
			this.symbols.addAll(inputs);
		}

		public VariableOutputDependency getLastOutputDependencyFor(Symbol s) {
			for (int i = outputs.size() - 1; i >= 0; --i) {
				VariableOutputDependency o = outputs.get(i);
				if (o.output == s)
					return o;
			}
			return null;
		}

		//
		// public List<Symbol> findOutputPredecessors(Symbol s) {
		// VariableOutputDependency o = getLastOutputDependencyFor(s);
		// int lastWritePositionForS = outputs.indexOf(o);
		// List<Symbol> predecessors = new UniqueEList<Symbol>();
		// if (o != null) {
		// for (Symbol testedSym : symbols) {
		// if (testedSym != s) {
		// VariableOutputDependency lastForSym =
		// getLastOutputDependencyFor(testedSym);
		// if (lastForSym != null) {
		// int lastWritePosForTestedSym = outputs
		// .indexOf(lastForSym);
		// if (lastWritePosForTestedSym <= lastWritePositionForS
		// && o.isDependent(lastForSym))
		// predecessors.add(testedSym);
		// }
		// }
		// }
		// }
		// return predecessors;
		// }

		public List<VariableOutputDependency> findLexicalDependencies(Symbol s) {
			VariableOutputDependency o = getLastOutputDependencyFor(s);
			int lastWritePositionForS = outputs.indexOf(o);
			List<VariableOutputDependency> predecessors = new UniqueEList<VariableOutputDependency>();
			if (o != null) {
				for (Symbol testedSym : symbols) {
					if (testedSym != s) {
						VariableOutputDependency lastForSym = getLastOutputDependencyFor(testedSym);
						if (lastForSym != null) {
							int lastWritePosForTestedSym = outputs
									.indexOf(lastForSym);
							if (lastWritePosForTestedSym <= lastWritePositionForS
									&& o.isDependent(lastForSym))
								predecessors.add(lastForSym);
						}
					}
				}
			}
			return predecessors;
		}
	}

	private static class VariableOutputDependency {
		private Symbol output;
		private List<Symbol> inputs;
		private DAGNode node;

		public VariableOutputDependency(Symbol output, DAGNode node,
				List<Symbol> inputs) {
			this.node = node;
			this.output = output;
			this.inputs = inputs;

		}

		public boolean isDependent(VariableOutputDependency other) {
			for (Symbol in : other.inputs) {
				if (in == output)
					return true;
			}
			return false;
		}

		@Override
		public String toString() {
			return "VariableOutputDependency: " + output + "(" + node + ")" + " = " + inputs;
		}
		
	}

	/**
	 * Manage memory dependencies for a {@link DAGInstruction}. Create control
	 * edges for each WAR/WAW/RAW dependency.
	 * 
	 * @author Antoine Floc'h - Initial contribution and API
	 * 
	 */
	private static class MemoryDependenciesManager {
		private List<MemoryAccess> writes = new ArrayList<TreeToDAGInstructionBuilder.MemoryAccess>();
		private List<MemoryAccess> reads = new ArrayList<TreeToDAGInstructionBuilder.MemoryAccess>();
		private static final boolean arrayDependenciesOnSETNode = false;
		private Map<DAGSimpleArrayNode, DAGSimpleArrayNode> WARs = new LinkedHashMap<DAGSimpleArrayNode, DAGSimpleArrayNode>();

		public void manage(MemoryAccess a) {
			if (a.isWrite()) {
				// WAR dependencies
				List<Entry<MemoryAccess, Boolean>> lastReads = findLastReads(a);
				for (Entry<MemoryAccess, Boolean> entry : lastReads) {
					MemoryAccess r = entry.getKey();
					boolean isMust = entry.getValue();
					
					manageDependency(r.getNode(), a.getNode(), DependencyType.WAR, isMust);
					if(isMust)
						reads.remove(r); //XXX check this
				}
				
				// WAW dependencies
				List<Entry<MemoryAccess, Boolean>> lastWrites = findLastWrites(a);
				for (Entry<MemoryAccess, Boolean> entry : lastWrites) {
					MemoryAccess w = entry.getKey();
					boolean isMust = entry.getValue();
					
					manageDependency(w.getNode(), a.getNode(), DependencyType.WAW, isMust);
					
					/* remove w from writes only if it was a YES but not when it's MAYBE. this will 
					 * ensure that in case of MAY dependency w (in addition to a - with which w have
					 * a may WAW-) will be still considered for later accesses (there is still a 
					 * chance to find MUST dependencies with w)
					 */
					if(isMust)
						writes.remove(w); 
				}
				
				writes.add(a);
			} else {
				// RAW dependencies
				List<Entry<MemoryAccess, Boolean>> lastWrites = findLastWrites(a);
				for (Entry<MemoryAccess, Boolean> entry : lastWrites) {
					MemoryAccess w = entry.getKey();
					boolean isMust = entry.getValue();
					
					manageDependency(w.getNode(), a.getNode(), DependencyType.RAW, isMust);
					
					//writes.remove(w); //XXX check in case of non ScalarAccess
				}
				
				reads.add(a);
			}			
		}

		/**
		 * 
		 * @param a
		 * @return list of lastWrites; to each a boolean is associated indicating if:
		 * must dependency => true
		 * may dependency => false 
		 */
		private List<Entry<MemoryAccess, Boolean>> findLastWrites(MemoryAccess a) {
			if(DEBUG) debug("findLastWrites of: " + a);
			incINDEX();
			
			List<Entry<MemoryAccess, Boolean>> lastWrites = new ArrayList<Entry<MemoryAccess, Boolean>>(); 
			for (MemoryAccess w : writes) {
				FuzzyBoolean res = a.accessToSameLocation(w);
				if(res != FuzzyBoolean.NO) {
					lastWrites.add(new SimpleEntry<MemoryAccess, Boolean>(w, (res == FuzzyBoolean.YES)));
				}
			}
			
			decINDEX();
			if(DEBUG) debug("|->" + lastWrites);
			return lastWrites;
		}

		/**
		 * 
		 * @param a
		 * @return list of lastReads; to each a boolean (isMust) is associated indicating if:
		 * must dependency => true
		 * may dependency => false 
		 */
		private List<Entry<MemoryAccess, Boolean>> findLastReads(MemoryAccess a) {
			if(DEBUG) debug("FindLastReads of: " + a + " -> " + a.getInstr() + " -> " + a.getInstr().getRoot());
			incINDEX();
			
			List<Entry<MemoryAccess, Boolean>> lastReads = new ArrayList<Entry<MemoryAccess, Boolean>>();
			for (MemoryAccess r : reads) {
				FuzzyBoolean res = a.accessToSameLocation(r);
				if(res != FuzzyBoolean.NO) {
					lastReads.add(new SimpleEntry<MemoryAccess, Boolean>(r, (res == FuzzyBoolean.YES)));
				}
			}
			
			decINDEX();
			if(DEBUG) debug("|->" + lastReads);
			return lastReads;
		}

		private void manageDependency(DAGNode from, DAGNode to, DependencyType type, boolean isMust) {
			//TODO add isMust information to the newly created ControlEdge: add isMust to ControlEdge in gecos model
			assert (to.eContainer() instanceof DAGInstruction);
			assert (from.eContainer() instanceof DAGInstruction);
			if (from != to) {
				if (!from.getSuccessors().contains(to)) {
					DAGOutPort outport = GecosUserDagFactory.createDAGOutputPort(from);
					outport.setIdent(from.getOutputs().size() - 1);
					DAGInPort inport = GecosUserDagFactory.createDAGInputPort(to);
					inport.setIdent(to.getInputs().size() - 1);
					DAGControlEdge createDAGControlEdge = GecosUserDagFactory.createDAGControlEdge(inport, outport, type);
					DAGInstruction instr = (DAGInstruction) from.eContainer();
					instr.getEdges().add(createDAGControlEdge);
					
					if(arrayDependenciesOnSETNode) {
						/* Also add dependency from SET node -> to node when from is a write (Array Access) */
						if((from instanceof DAGSimpleArrayNode) && (to instanceof DAGSimpleArrayNode)) {
							if(type == DependencyType.RAW || type == DependencyType.WAW) {
								DAGNode fromSetNode = from.getDataSuccessors().get(0);
								if(fromSetNode instanceof DAGOpNode && ((DAGOpNode)fromSetNode).getOpcode() == DagIntructionsTypes.SET) { //src is a write
									outport = GecosUserDagFactory.createDAGOutputPort(fromSetNode);
									inport = GecosUserDagFactory.createDAGInputPort(to);
									instr.getEdges().add(GecosUserDagFactory.createDAGControlEdge(inport, outport, type));
								}
							}
							else if(type == DependencyType.WAR) {
//								if(to.getDataSuccessors().size() > 0) {
//									DAGNode succ = to.getDataSuccessors().get(0);
//									if(succ instanceof DAGOpNode && ((DAGOpNode)succ).getOpcode() == DagIntructionsTypes.SET) { //src is a write
//										outport = UserDagFactory.createDAGOutputPort(from);
//										inport = UserDagFactory.createDAGInputPort(succ);
//										instr.getEdges().add(UserDagFactory.createDAGControlEdge(inport, outport, type));
//									}
//								}
//								else
//									System.err.println(from + " -> " + to);
								WARs.put((DAGSimpleArrayNode)from, (DAGSimpleArrayNode)to);
							}
						}
					}
				}
			}
		}
	}

}
