/**
 */
package gecos.instrs.impl;

import gecos.core.Symbol;

import gecos.instrs.InstrsPackage;
import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;
import gecos.instrs.SSADefSymbol;
import gecos.instrs.SSAUseSymbol;

import gecos.types.Type;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SSA Def Symbol</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.impl.SSADefSymbolImpl#getSSAUses <em>SSA Uses</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SSADefSymbolImpl extends NumberedSymbolInstructionImpl implements SSADefSymbol {
	/**
	 * The cached value of the '{@link #getSSAUses() <em>SSA Uses</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSSAUses()
	 * @generated
	 * @ordered
	 */
	protected EList<SSAUseSymbol> ssaUses;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SSADefSymbolImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstrsPackage.eINSTANCE.getSSADefSymbol();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SSAUseSymbol> getSSAUses() {
		if (ssaUses == null) {
			ssaUses = new EObjectWithInverseResolvingEList<SSAUseSymbol>(SSAUseSymbol.class, this, InstrsPackage.SSA_DEF_SYMBOL__SSA_USES, InstrsPackage.SSA_USE_SYMBOL__DEF);
		}
		return ssaUses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final InstrsVisitor visitor) {
		visitor.visitSSADefSymbol(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _elvis = null;
		Symbol _symbol = this.getSymbol();
		String _name = null;
		if (_symbol!=null) {
			_name=_symbol.getName();
		}
		if (_name != null) {
			_elvis = _name;
		} else {
			_elvis = "null";
		}
		String _plus = ("def(" + _elvis);
		String _plus_1 = (_plus + "_");
		int _number = this.getNumber();
		String _plus_2 = (_plus_1 + Integer.valueOf(_number));
		String _plus_3 = (_plus_2 + ":");
		Object _elvis_1 = null;
		Symbol _symbol_1 = this.getSymbol();
		Type _type = null;
		if (_symbol_1!=null) {
			_type=_symbol_1.getType();
		}
		if (_type != null) {
			_elvis_1 = _type;
		} else {
			_elvis_1 = "";
		}
		String _plus_4 = (_plus_3 + _elvis_1);
		return (_plus_4 + ")");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSame(final Instruction instr) {
		if ((instr instanceof SSADefSymbol)) {
			return super.isSame(instr);
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InstrsPackage.SSA_DEF_SYMBOL__SSA_USES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSSAUses()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case InstrsPackage.SSA_DEF_SYMBOL__SSA_USES:
				return ((InternalEList<?>)getSSAUses()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InstrsPackage.SSA_DEF_SYMBOL__SSA_USES:
				return getSSAUses();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InstrsPackage.SSA_DEF_SYMBOL__SSA_USES:
				getSSAUses().clear();
				getSSAUses().addAll((Collection<? extends SSAUseSymbol>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InstrsPackage.SSA_DEF_SYMBOL__SSA_USES:
				getSSAUses().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InstrsPackage.SSA_DEF_SYMBOL__SSA_USES:
				return ssaUses != null && !ssaUses.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SSADefSymbolImpl
