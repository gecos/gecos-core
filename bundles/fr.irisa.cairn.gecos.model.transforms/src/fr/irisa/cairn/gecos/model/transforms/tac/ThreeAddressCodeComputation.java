package fr.irisa.cairn.gecos.model.transforms.tac;

import fr.irisa.cairn.gecos.model.transforms.tac.internal.BlockManager;
import fr.irisa.r2d2.gecos.framework.GSModule;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;


/**
 * Pass that transform each basic block instructions into the three address code form (TAC form)  
 * Basic block contained into control flow is not transform (ex : for test block)
 * 
 * @author Nicolas Simon
 * @date 30/10/12
 *
 */
@GSModule("Performs Three Address Code transformation.\n"
		+ "Basic blocks used in control structures are not converted\n"
		+ "(e.g. IF's test block or FOR's init/test/step blocks.")
public class ThreeAddressCodeComputation {

	private ProcedureSet _ps;	
	private BlockManager _blockManager;
	private GecosProject _proj;
	
	public ThreeAddressCodeComputation(ProcedureSet ps){
		_ps = ps;
		_blockManager = new BlockManager();
	}

	public ThreeAddressCodeComputation(GecosProject proj){
		_proj = proj;
		_blockManager = new BlockManager();
	}
	
	private void computeProcedureSet(ProcedureSet  ps){
		for(Procedure p : ps.listProcedures()){
			_blockManager.doSwitch(p.getBody());
		}
	}

	public void compute(){
		if(_proj!=null) {
			for (ProcedureSet _ps :_proj.listProcedureSets())
				computeProcedureSet(_ps);
			
		}
		else if(_ps != null) {
			computeProcedureSet(_ps);
		}
		else{
			throw new RuntimeException("Project or ProcedureSet given are null!");
		}
	}
}
