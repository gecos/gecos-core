#include <stdio.h>

#define M 150
#define N 10

void testGlobal_CSE(int a, int b, int res[N], int f[M], int g[M], int opt_ptr) {
	int c, d, e, i, j;

	{
		c = a + b;
		d = a * c;
		//a = 10;
		e = d * d;
		i = 1;
	}
	do {
		f[i] = a + b;
		c = c * 2;
		if (c > d)
			g[i] = a * c;
		else
			g[i] = d * d;
		 i = i + 1;
	} while (i < M) ;


	res[0] = c;
	res[1] = d;
	res[2] = e;

	{
		//c = M-1;

		for (i = 0; i < 3; i++)
			res[3+i] = M - 1;

		for (i = 0; i < M; i++)
			if (M > 100)
				for (j = 0; j < 100; j++)
					res[4] = b + c;
			else
				for (j = 0; j < M; j++)
					res[4] = M - 1;

		res[5] = M - 1;
	}


	if (!opt_ptr) { // this test make sense only if the transformation was called with pointer_option == false
		int*ptr;

		{
			int*p; //  an extra-pointer : test the conservative approach Vs pointers
			p = &c;
			res[6] = c + d;
			*p = 10;
			res[7] = c + d;
		}
		{
			res[8] = c + d;
		}
		ptr = &c;
		*ptr = 999;
		{
			res[9] = c + d;
		}
	}
}

