package fr.irisa.cairn.gecos.model.transforms.loops.unroller;

import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.BBlock;
import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.CompositeBlock;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.Int;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.set;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.symbref;

import java.security.InvalidParameterException;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import com.google.common.collect.Lists;

import fr.irisa.cairn.gecos.model.analysis.forloops.ModelNormalForInformation;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.tools.controlflow.BuildControlFlow;
import fr.irisa.cairn.gecos.model.tools.controlflow.ClearControlFlow;
import fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;
import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch;
import fr.irisa.cairn.gecos.model.tools.utils.BlocksEraser;
import fr.irisa.cairn.gecos.model.tools.utils.FixBlockNumbers;
import fr.irisa.cairn.gecos.model.tools.utils.InstrsSubstitution;
import fr.irisa.cairn.gecos.model.tools.visitors.simpleforloops.RemoveSimpleForBlock;
import fr.irisa.cairn.gecos.model.transforms.blocks.simplifier.BlockSimplifier;
import fr.irisa.cairn.gecos.model.transforms.blocks.simplifier.ModelBlockSimplifier;
import fr.irisa.cairn.gecos.model.transforms.constants.ConstantEvaluator;
import fr.irisa.cairn.gecos.model.transforms.constants.ModelConstantPropagatorIterative;
import fr.irisa.cairn.gecos.model.transforms.deadcodeelimination.ModelDeadCodeEliminator;
import fr.irisa.cairn.gecos.model.transforms.loops.unroller.ModelForUnroller.ModelForUnrollerCore.ForUnrollException;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksFactory;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.core.CoreFactory;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;
import gecos.instrs.BreakInstruction;
import gecos.instrs.CondInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.SetInstruction;

/**
 * 
 * This unroller does not ensures legal unroll if the index's value is changed during 
 * evaluation of the body. It does not unroll if : <br/>
 *  - loop bounds and step are not well written (see doc); <br/>
 *  - transformation may be illegal : <br/>
 *    * index is global; <br/> 
 *    * index is addressed in the body; <br/>
 *    * loop contains continue instruction; <br/>
 *    * loop contains break when trying to unroll fully; <br/>
 *  
 *  TODO : support ForC99 
 *  
 * @author amorvan
 *
 */
@GSModule("Partial or Full Loop Unrolling inside the specified Gecos Object.\n"
		+ "It does not ensures legal unroll if the index's value is changed\n"
		+ "during evaluation of the loop body.\n"
		+ "\nIt does not unroll if:\n"
		+ "   - loop bounds and step are not well written ?.\n"
		+ "   - transformation may be illegal :\n"
		+ "       * index is global\n"
		+ "       * index is addressed in the body\n"
		+ "       * loop contains continue instruction\n"
		+ "       * loop contains break when trying to unroll fully\n"
		+ "\nIt also rebuilds the control flow, evaluates constants,"
		+ "removes dead code, and fix block numbers.\n")
public class ModelForUnroller extends BlocksDefaultSwitch<Object> {

	
	/******************* STATIC VALUES *********************/
	
	
	private static final boolean debug=false;
	private static void debug(Object o) { if (debug) System.out.print(o.toString()); }
	private static void debugln(Object o) {  debug(o.toString()+"\n"); }
	private static final String PREFIX = "[UNROLL] ";

	/* value for differents modes (has to be a power of 2) */
	public final static int MODE_FULL 	= 0;
	public final static int MODE_THRESH = 1;
	public final static int MODE_PART	= 2;
	public final static int MODE_PRAGMA = 4;
	
	public static final int MODE_PART_PRAGMA  		= MODE_PART   + MODE_PRAGMA;
	public static final int MODE_THESH_PART   		= MODE_THRESH + MODE_PART;
	public static final int MODE_THESH_PRAGMA 		= MODE_THRESH + MODE_PRAGMA;
	public static final int MODE_THESH_PART_PRAGMA 	= MODE_THESH_PART + MODE_PRAGMA;

	/* default Parameter values */
	private static final int DEFAULT_MODE 				= MODE_PRAGMA;
	private static final int DEFAULT_UNROLLFACTOR 		= 4;
	private static final int DEFAULT_THRESHOLD 			= 128;
	private static final int DEFAULT_THRESHOLD_PARTIAL 	= 0;
	private static final int DEFAULT_NULL_PARAMETER 	= -1;

	
//	private final static Map<Integer,Integer> defaultParam1 = new LinkedHashMap<Integer, Integer>();
//	private final static Map<Integer,Integer> defaultParam2 = new LinkedHashMap<Integer, Integer>();
//
//	static {
//		// Mode FULL takes no parameters
//		defaultParam1.put(MODE_FULL, 	DEFAULT_NULL_PARAMETER); 
//		defaultParam2.put(MODE_FULL,	DEFAULT_NULL_PARAMETER);
//		
//		// Mode PRAGMA takes no parameters
//		defaultParam1.put(MODE_PRAGMA, 	DEFAULT_NULL_PARAMETER);
//		defaultParam2.put(MODE_PRAGMA,	DEFAULT_NULL_PARAMETER);
//		
//		// Mode THRESHOLD takes only 1 parameter: the THRESHOLD value
//		defaultParam1.put(MODE_THRESH, 	DEFAULT_THRESHOLD);
//		defaultParam2.put(MODE_THRESH,	DEFAULT_NULL_PARAMETER);
//		
//		// Mode PARTIAL takes only 1 parameter: the UNROLL factor
//		defaultParam1.put(MODE_PART, 	DEFAULT_UNROLLFACTOR);
//		defaultParam2.put(MODE_PART,	DEFAULT_NULL_PARAMETER);
//		
//		// Mode THRESHOLD PRAGMA takes only 1 parameter: the THRESHOLD value
//		defaultParam1.put(MODE_THESH_PRAGMA,	DEFAULT_THRESHOLD);
//		defaultParam2.put(MODE_THESH_PRAGMA,	DEFAULT_NULL_PARAMETER);
//		
//		// Mode PARTIAL PRAGMA takes only 1 parameter: the UNROLL Factor
//		defaultParam1.put(MODE_PART_PRAGMA,	DEFAULT_UNROLLFACTOR);
//		defaultParam2.put(MODE_PART_PRAGMA,	DEFAULT_NULL_PARAMETER);
//		
//		// Mode THRESHOLD PARTIAL takes 2 parameters: the THRESHOLD value and UNROLL factor
//		defaultParam1.put(MODE_THESH_PART,	DEFAULT_THRESHOLD_PARTIAL); 
//		defaultParam2.put(MODE_THESH_PART,	DEFAULT_UNROLLFACTOR);
//		
//		// Mode THRESHOLD PARTIAL PRAGMA takes 2 parameters: the THRESHOLD value and UNROLL factor		
//		defaultParam1.put(MODE_THESH_PART_PRAGMA,	DEFAULT_THRESHOLD_PARTIAL);
//		defaultParam2.put(MODE_THESH_PART_PRAGMA,	DEFAULT_UNROLLFACTOR);
//		
//	}

	// following are the possible optimizations
	//private final static boolean[] opti = new boolean[] {false,false,false,true};
	private final static boolean[] opti = new boolean[] {
		/* 1 Clear CF		 */ // cannot be disabled
		/* 2 ConstantEval	 */ true,
		/* 3 Unroll			 */ // cannot be disabled
		/* 4 ConstantEval	 */ true,
		/* 5 DeadCode		 */ true,
		/* 6 Simplify		 */ true,
		/* 7 Rebuild CF		 */ // cannot be disabled
		/* 8 FixNumbers		 */ // cannot be disabled
		};
	
	/******************* NON-STATIC VALUES *********************/
	
	
	//mode for the current unroll
	private boolean thresholdMode = false;
	private boolean partialMode = false;
	private boolean pragmaMode = false;

	//default values of the current unroll
	private int threshold = DEFAULT_NULL_PARAMETER;
	private int unrollFactor = DEFAULT_NULL_PARAMETER;
	
	//the unroller will try to unroll each loop in each procedure of the following list. 
	private List<Procedure> lProc;


	/******************* CONSTRUCTORS *********************/
	
	
	@GSModuleConstructor(
			"-arg1: Gecos object on which the transformation will be applied.\n"
		  + "   Should be either a GecosProject, ProcedureSet or Procedure.\n"
		  + "-arg2: select the Unrolling mode. Supported values:\n"
		  + "   * 0: FULL unroll.\n"
		  + "   * 1: FULL unroll only if the loop iteration count is \n"
		  + "      less than (<) the specified THREASHOLD.\n"
		  + "   * 2: PARTIAL unroll by the specified UNROLL FACTOR.\n"
		  + "   * 3: PARTIAL unroll by the specified UNROLL FACTOR only if\n"
		  + "      the loop iteration count is >= the specified THRESHOLD.\n"
		  + "   * 4+M: Only unroll the PRAGMA annotated for loops.\n"
		  + "      The unroll mode is 'M' with the specified parameters as \n"
		  + "      defaults, unless explicitly specified using paragmas.\n"
		  + "      Supported Pargmas are:\n"
		  + "          -'GCS_UNROLL': unroll with default mode ('M').\n"
		  + "          -'GCS_UNROLLALL': unroll with mode FULL instead of 'M'.\n"
		  + "          -'GCS_UNROLL=<YES/NO>': if NO then do not unroll, else same as GCS_UNROLL ?\n"
		  + "          -'GCS_UNROLLFACTOR=<value>': override the default UNROLL FACTOR.\n"
		  + "-arg3: UNROLL FACTOR. Ignored for modes 0, 1, 4 and 5.\n"
		  + "-arg4: THRESHOLD value. Ignored for modes 0, 2, 4 and 6.")
	public ModelForUnroller(EObject obj, int mode, int unrollFactor, int threshold) {
		if(obj instanceof GecosProject)
			this.lProc = ((GecosProject) obj).listProcedures();
		else if(obj instanceof ProcedureSet)
			this.lProc = ((ProcedureSet)obj).listProcedures();
		else if(obj instanceof Procedure)
			this.lProc = Lists.<Procedure>newArrayList((Procedure) obj);
		else
			throw new InvalidParameterException("ModelForUnroller can Only GecosProject, "
					+ "ProcedureSet and Procedure are supported.");
		
		init(mode, unrollFactor, threshold);
	}
	
	@GSModuleConstructor("Same as full constructor. Use default UNROLL FACTOR 4.\n"
			+ "Use default THRESHOLD 128 for all modes except \n"
			+ "modes '3' and '7' for which 0 is the default.")
	public ModelForUnroller(EObject obj, int mode) {
		this(obj, mode, -1, -1); //use defaults
	}
	
	@GSModuleConstructor("Same as full constructor. Use default mode: PRAGMA_MODE.")
	public ModelForUnroller(EObject obj) {
		this(obj, DEFAULT_MODE);
	}
	
	
//	//constructors for single procedures
//	public ModelForUnroller(Procedure proc) {
//		this(proc,DEFAULT_MODE);
//	}
//
//	public ModelForUnroller(Procedure proc, int mode) {
//		this(proc,mode,defaultParam1.get(mode));
//	}
//
//	public ModelForUnroller(Procedure proc, int mode, int param1) {
//		this(proc,mode,param1,defaultParam2.get(mode));
//	}
//
//	public ModelForUnroller(Procedure proc, int mode, int param1, int param2) {
//		(this.lProc = new ArrayList<Procedure>(1)).add(proc);
//		init(mode,param1,param2);
//	}
//
//	//constructors for procedure set
//	public ModelForUnroller(ProcedureSet ps) {
//		this(ps,DEFAULT_MODE);
//	}
//
//	public ModelForUnroller(ProcedureSet ps, int mode) {
//		this(ps,mode,defaultParam1.get(mode));
//	}
//
//	public ModelForUnroller(ProcedureSet ps, int mode, int unrollFactor) {
//		this(ps,mode,unrollFactor,defaultParam2.get(mode));
//	}
//
//	public ModelForUnroller(ProcedureSet ps, int mode, int unrollFactor, int threshold) {
//		this.lProc = ps.listProcedures();
//		init(mode,unrollFactor,threshold);
//	}
//
//	//constructor for whole gecos projects
//	public ModelForUnroller(GecosProject proj) {
//		this(proj,DEFAULT_MODE);
//	}
//
//	public ModelForUnroller(GecosProject proj, int mode) {
//		this(proj,mode,DEFAULT_NULL_PARAMETER);
//	}
//
//	public ModelForUnroller(GecosProject proj, int mode, int param1) {
//		this(proj,mode,param1,DEFAULT_NULL_PARAMETER);
//	}
//
//	public ModelForUnroller(GecosProject proj, int mode, int param1, int param2) {
//		this.lProc = new ArrayList<Procedure>();
//		for (GecosSourceFile file : proj.getSources()) {
//			if (file.getModel() == null) {
//				debug("Error : Calling For unroller, but project has not been built. Exiting.");
//				System.exit(-1);
//			}
//			this.lProc.addAll(file.getModel().listProcedures());
//		}
//		init(mode, param1, param2);
//	}
	
	/**
	 * @deprecated old constructor entry
	 */
	@Deprecated
	public ModelForUnroller(Procedure proc, String mode) {
		this(proc,
				mode.equals("UNROLLWITHTHRESHOLD")? MODE_THRESH:
				mode.equals("UNROLLALLPRAGMAS")? MODE_PRAGMA:
					MODE_FULL);
	}

	
	/******************* METHODS *********************/
	
	
	/**
	 * initialize non-static values depending on mode & parameters.
	 */
	private void init(int m, int unrollFactor, int threshold) {
		// init mode
		thresholdMode = (m & MODE_THRESH) == MODE_THRESH;
		partialMode = (m & MODE_PART) == MODE_PART;
		pragmaMode = (m & MODE_PRAGMA) == MODE_PRAGMA;

		// init values
		this.unrollFactor = !partialMode ? DEFAULT_NULL_PARAMETER :
							(unrollFactor < 0 ? DEFAULT_UNROLLFACTOR : unrollFactor);
		this.threshold = !thresholdMode ? DEFAULT_NULL_PARAMETER :
						 (threshold >= 0 ? threshold :
							 (partialMode ? DEFAULT_THRESHOLD_PARTIAL : DEFAULT_THRESHOLD));
	}
	
	/**
	 * browse the list of procedure, and call some specific processes (optimization, unroll, ...)
	 */
	public void compute() {
		debugln(PREFIX+"start : "+this.toString());
		long time = System.currentTimeMillis(); 
		debugln(PREFIX+"---------------------------------------------------------------------------------");
		for (Procedure proc : lProc) {
			if (proc == null) {
				debug("Error : ModelForUnroll cannont find procedure.");
				break;
			}

			ProcedureSet containingProcedureSet = proc.getContainingProcedureSet();
			Scope scope = containingProcedureSet.getScope();
			if (scope == null)
				throw new RuntimeException();
			GecosUserTypeFactory.setScope(scope);
			
			if (debug) {
				String fileprocedure = "";
				if (proc.eContainer() != null && proc.eContainer() instanceof ProcedureSet) {
					ProcedureSet ps = (ProcedureSet) proc.eContainer();
					if ((ps.eContainer() != null)  && ps.eContainer() instanceof GecosSourceFile) {
							GecosSourceFile src = (GecosSourceFile) ps.eContainer();
							fileprocedure += src+" / ";
					}
				}
				fileprocedure += proc.getSymbol().getName();
				debugln(PREFIX+" ******** "+fileprocedure+" ******** ");
			}

			debugln(PREFIX+"1- clear control flow");
			//delete all control edges : it allows easy manipulation on model
			ClearControlFlow mccf = new ClearControlFlow(proc);
			mccf.compute();
			
			new RemoveSimpleForBlock(proc).compute();

			if (opti[0]) {
				//evaluate constants
				debugln(PREFIX+"2- propagate & evaluate constants");
				ModelConstantPropagatorIterative constPropagator = new ModelConstantPropagatorIterative(proc);
				while (constPropagator.computeIterative());
				ConstantEvaluator ce = new ConstantEvaluator(proc);
				while (ce.computeIterative());
			}

			//look for loops to unroll
			debugln(PREFIX+"3- look for loops to unroll");
			Block body = proc.getBody();
			doSwitch(body);

			if (opti[1]) {
				//evaluate constants 
				debugln(PREFIX+"4- evaluate constants");
				ConstantEvaluator ce = new ConstantEvaluator(proc);
				while (ce.computeIterative());
			}

			if (opti[2]) {
				//remove dead code
				debugln(PREFIX+"5- remove dead code");
				(new ModelDeadCodeEliminator(proc)).compute();
			}

			if (opti[3]) {
				//simplify blocks
				debugln(PREFIX+"6- simplify blocks");
				ModelBlockSimplifier bs = new ModelBlockSimplifier(proc);
				bs.compute();
			}

			//rebuild control edges
			debugln(PREFIX+"7- rebuild control flow");
			BuildControlFlow mbcf = new BuildControlFlow(proc);
			mbcf.compute();

			//fix block numbers (since copying a block copies its number)
			debugln(PREFIX+"8- fix block numbers");
			FixBlockNumbers.fix(proc);

		}
		time = (System.currentTimeMillis() - time);
		debugln(PREFIX+"end, time = "+time+" ms");
	} //compute

	/**
	 * When a for block is found, then try to unroll (depending on non-static values).
	 * If it has been unrolled, then replace it in its parents.
	 */
	@Override
	public Object caseForBlock(ForBlock forBlock) {
		
		// 1 - check parent
		EObject parent = forBlock.eContainer();
		
		// 2 - get infos 
		ModelNormalForInformation infos = new ModelNormalForInformation(forBlock,threshold,unrollFactor,pragmaMode);
		debug(infos);
		
		// 3 - switch depending on infos
		Block unrolledBody = null;

		try {
			switch(infos.getDecision()) {
			case UNROLLALL:
				unrolledBody = ModelForUnrollerCore.unroll(forBlock,infos);
				break;
			case UNROLLPARTIAL:
				unrolledBody = ModelForUnrollerCore.partialUnroll(forBlock,infos,infos.getFactor());
				break;
			case NOUNROLL:
				doSwitch(forBlock.getBodyBlock());
				return null;
			default:
				throw new RuntimeException("Error : unknown case");
			}
		} catch (ForUnrollException e) {
			String msg = "ForUnroll error : "+e.type;
			throw new RuntimeException(msg);
		}
		
		if (unrolledBody == null) throw new RuntimeException("Error : unroll return null block.");

		// 4- insert unrolled body inside parent's block
		debugln(PREFIX+"\t >> replace old forblock by unrolled body");
		
		if (!(parent instanceof Block)) throw new RuntimeException("Error : parent has to be a Block.");
		if ((parent instanceof BasicBlock)) throw new RuntimeException("Error : parent cannot be BasicBlock.");

		if (unrolledBody instanceof CompositeBlock) ((Block)parent).getScope().mergeWith(unrolledBody.getScope());
		((Block)parent).replace(forBlock,unrolledBody);
		(new BlocksEraser()).doSwitch(forBlock);
		
		// 5 - simplify unrolledBody
		debugln(PREFIX+"\t >> simplify unrolled body");
		(new ModelBlockSimplifier(unrolledBody)).compute();

		// 6- evaluate constants
		debugln(PREFIX+"\t >> evaluate unrolled body's constants");
		ConstantEvaluator ce = new ConstantEvaluator(unrolledBody);
		while (ce.computeIterative());


		// 7- unroll children
		debugln(PREFIX+"\t >> unroll children");
		if (unrolledBody instanceof ForBlock) {
			doSwitch(((ForBlock) unrolledBody).getBodyBlock());
		} else if (unrolledBody instanceof CompositeBlock) {
			doSwitch(unrolledBody);
		}
		
		//add lcl: simplify BB result of unroll
		if( unrolledBody instanceof CompositeBlock){
			BlockSimplifier.simplify((CompositeBlock) unrolledBody);			
		}
		if(parent instanceof CompositeBlock){
			BlockSimplifier.simplify((CompositeBlock) parent);
		}
		
		return null;
	} // caseForBlock

	/**
	 * toString().
	 */
	public String toString() {
		int mode = ((thresholdMode? MODE_THRESH:0) + 
					(partialMode? MODE_PART:0) +
					(pragmaMode? MODE_PRAGMA:0));

		String strmode = "unroll all";
		switch (mode) {
		case 0: strmode = "unroll all";	break;
		case 1: strmode = "unroll with threshold"; break;
		case 2: strmode = "partial"; break;
		case 4: strmode = "unroll only pragma"; break;
		case 3: strmode = "threshold+partial"; break;
		case 5: strmode = "threshold+pragma"; break;
		case 6: strmode = "partial+pragma"; break;
		case 7: strmode = "threshold+partial+pragma"; break;
		default: strmode = "unroll all";
		}

		return "ForUnroller : {mode = "+mode+" ("+strmode+"); threshold = "+threshold+"; unroll factor = "+unrollFactor+"}";
	} // toString

	
	/******************* SUB-CLASSES *********************/
	
	
	/**
	 * Static class to isolate unroll process. <br/>
	 * If you want to use it from your own class, have a look at 
	 * the ModelNormalForInformation class.
	 * @author amorvan
	 *
	 */
	public static class ModelForUnrollerCore {

		public static class ForUnrollException extends Exception {
			private static final long serialVersionUID = 1L;
			public static enum ErrorType {
				FORINITISNOTBASICBLOCK,
			}
			public ErrorType type;
			public ForUnrollException(ErrorType type) {
				this.type = type;
			}
		}

		private static final String PREFIX = "[UNROLL_CORE] ";
		public static Block unroll(ForBlock forBlock) throws ForUnrollException {
			return unroll(forBlock,new ModelNormalForInformation(forBlock));
		}
		
		/**
		 * use this one if you already processed NormalForInformations in order to 
		 * reduce execution's time.
		 * @param forBlock
		 * @param loopInfos
		 * @return
		 * @throws ForUnrollException
		 */
		public static Block unroll(ForBlock forBlock, ModelNormalForInformation loopInfos) throws ForUnrollException {
			debugln(PREFIX+"unroll all start");
			Block res = null;
			
			Block body = forBlock.getBodyBlock();

			//Create result composite block. It will contain all the copies of for's body.
			CompositeBlock unrolledBody = CompositeBlock();

			//initialize scope of the unrolled body
			Scope newScope = CoreFactory.eINSTANCE.createScope();
			//newScope.setParent(forBlock.getScope().getParent());
			unrolledBody.setScope(newScope);

			
			/*
			 * if body's scope is different from parent's scope (meaning it has
			 * its own scope), then merge with unrolled body. Be careful : we
			 * have to manage declarations with initializations. We manage this
			 * by creating a BasicBlock with SetInstructions, and add a copy 
			 * of this block at each iteration of the loop.
			 */
			List<SetInstruction> lInit = new LinkedList<SetInstruction>();
			if (forBlock.getScope() != body.getScope()) {
				Scope scope = body.getScope();
				for (Symbol s : scope.getSymbols()) {
					if (s.getValue() != null) {
						lInit.add(set(symbref(s),s.getValue().copy()));
						s.setValue(null);
					}
				}
				unrolledBody.getScope().mergeWith(scope);
			}
			BasicBlock init = (lInit.size() < 1)?null:(BBlock(lInit.toArray(new Instruction[lInit.size()])));

			Symbol loopIndex = loopInfos.getIterationIndex();
			debugln(PREFIX+"begin copy");
			long i;
			long startValue = ((IntInstruction)loopInfos.getStartValue()).getValue();
			for (i = startValue ; loopInfos.testValue(i); i = loopInfos.incIndex(i)) {
				
				//prepare Instuction for index substitution
				 Instruction inst = Int(i);

				//if init block (that contains set instructions for declarations
				// with initializations) is not null, then add it another time.
				if (init != null) {
					Block copyInit = init.copy();
					//replace each occurrence of index symbol by its current value.
					copyInit.accept(new InstrsSubstitution(loopIndex, inst));
					unrolledBody.addChildren(copyInit);
				}
				
				// replicate the loop body block for indice i
				Block bi = body.copy();
				bi.setNumber((int)(i+1));

				//replace each occurrence of index symbol by its current value.
				bi.accept(new InstrsSubstitution(loopIndex, inst));
				
				// add replicated body into new unrolled body
				unrolledBody.addChildren(bi);
			}

			debugln(PREFIX+"copy ends, create stop block");
			//at the end of the loop, we have to set into index its last value
			BasicBlock seti = BBlock(set(symbref(loopIndex),Int(i))); 
			unrolledBody.addChildren(seti);

			debugln(PREFIX+"unroll all ends");
			res = unrolledBody;
			return res;
		}

		public static Block partialUnroll(ForBlock forBlock, int unrollFactor) {
			return partialUnroll(forBlock,new ModelNormalForInformation(forBlock),unrollFactor);
		}
		
		/**
		 * use this one if you already processed NormalForInformations in order to reduce time.
		 * @param forBlock
		 * @param loopInfos
		 * @param unrollFactor
		 * @return
		 */
		public static Block partialUnroll(ForBlock forBlock, ModelNormalForInformation loopInfos,long unrollFactor) {
			debug("Warning : Partial unroller highly unstable.");
			debugln(PREFIX+"unroll partial start");
			Block res = null;

			//copy init & test block (they do not change)
			BasicBlock initBlock = (BasicBlock) forBlock.getInitBlock().copy();
			BasicBlock testBlock = (BasicBlock) forBlock.getTestBlock().copy();
			
			//create step expression & block
			GenericInstruction s1 = GecosUserInstructionFactory.mul(loopInfos.getStepValue().copy(), Int(unrollFactor));
//			GenericInstruction s1 = InstrsFactory.eINSTANCE.createGenericInstruction();
//			s1.setName("mul");
//			s1.addOperand(loopInfos.getStepValue().copy());
//			s1.addOperand(Int(unrollFactor));
			
			GenericInstruction s2 = GecosUserInstructionFactory.generic(loopInfos.getStepOp(),loopInfos.getIterationIndex().getType(), symbref(loopInfos.getIterationIndex()),s1);
//			GenericInstruction s2 = InstrsFactory.eINSTANCE.createGenericInstruction();
//			s2.setName(loopInfos.getStepOp());
//			s2.addOperand(symbref(loopInfos.getIterationIndex()));
//			s2.addOperand(s1);
			
			SetInstruction s3 = GecosUserInstructionFactory.set(loopInfos.getIterationIndex(), s2);
//			SetInstruction s3 = InstrsFactory.eINSTANCE.createSetInstruction();
//			s3.setDest(symbref(loopInfos.getIterationIndex()));
//			s3.setSource(s2);
			BasicBlock stepBlock = BlocksFactory.eINSTANCE.createBasicBlock();
			stepBlock.getInstructions().add(s3);
			
			//use local variable for body
			Block body = forBlock.getBodyBlock();

			//initialize the new body
			CompositeBlock unrolledBody = CompositeBlock();

			//initialize scope of the unrolled body
			Scope newScope = CoreFactory.eINSTANCE.createScope();
			//newScope.setParent(forBlock.getScope());
			unrolledBody.setScope(newScope);


			/*
			 * if body's scope is different from parent's scope (meaning it has
			 * its own scope), then merge with unrolled body. Be careful : we
			 * have to manage declarations with initializations. We manage this
			 * by creating a BasicBlock with SetInstructions, and add a copy 
			 * of this block at each iteration of the loop.
			 */
			List<SetInstruction> lInit = new LinkedList<SetInstruction>();
			if (forBlock.getScope() != body.getScope()) {
				Scope scope = body.getScope();
				for (Symbol s : scope.getSymbols()) {
					if (s.getValue() != null) {
						lInit.add(set(symbref(s),s.getValue().copy()));
						s.setValue(null);
					}
				}
				unrolledBody.getScope().mergeWith(scope);
			}
			BasicBlock init = (lInit.size() < 1)?null:(BBlock(lInit.toArray(new Instruction[lInit.size()])));
			Symbol loopIndex = loopInfos.getIterationIndex();
			
			// strip mining when bounds are known instead of add a break.
			boolean stripmining = true;
			boolean removeControl = stripmining && loopInfos.getIterationCount() > 0 && !loopInfos.containsBreakOrContinue();
			
			for(int i = 0; i < unrollFactor; i++) {

				//prepare Instruction for index substitution
				IntInstruction factorInst = Int(i); 
				
				GenericInstruction exprTimeFactor = GecosUserInstructionFactory.mul(factorInst, loopInfos.getStepValue().copy());
//				GenericInstruction exprTimeFactor = InstrsFactory.eINSTANCE.createGenericInstruction();
//				exprTimeFactor.setName("mul");
//				exprTimeFactor.addOperand(factorInst);
//				exprTimeFactor.addOperand(loopInfos.getStepValue().copy());
				
				GenericInstruction indexOpExpr = GecosUserInstructionFactory.generic(
						loopInfos.getStepOp(),
						loopInfos.getIterationIndex().getType(),
						symbref(loopInfos.getIterationIndex()),
						exprTimeFactor
						);
//				GenericInstruction indexOpExpr = InstrsFactory.eINSTANCE.createGenericInstruction();
//				indexOpExpr.setName(loopInfos.getStepOp());
//				indexOpExpr.addOperand(symbref(loopInfos.getIterationIndex()));
//				indexOpExpr.addOperand(exprTimeFactor);
				
				Instruction inst = indexOpExpr;

				//if init block (that contains set instructions for declarations
				// with initializations) is not null, then add it another time.
				if (init != null) {
					Block copyInit = init.copy();
					//replace each occurrence of index symbol by its current value.
					copyInit.accept(new InstrsSubstitution(loopIndex, inst));
					unrolledBody.addChildren(copyInit);
				}
				
				// replicate the loop body block for indice i
				Block bi = body.copy();
				bi.setNumber(i+1);

				//replace each occurrence of index symbol by its current value.
				bi.accept(new InstrsSubstitution(loopIndex, inst));
				
				// add replicated body into new unrolled body
				unrolledBody.addChildren(bi);
				
				//add control if block to break execution if bound is reached
				if (!removeControl && (i != unrollFactor - 1) && ((loopInfos.getIterationCount() < 1) || (loopInfos.getIterationCount() >= 1 && (loopInfos.getIterationCount() % unrollFactor == i + 1)))) {
					//build break block
					BreakInstruction breakInst = GecosUserInstructionFactory.breakInst();
					BasicBlock breakBB = BlocksFactory.eINSTANCE.createBasicBlock();
					breakBB.addInstruction(breakInst);
					
					//build cond instruction & block
					GenericInstruction exprTimeFactorB = GecosUserInstructionFactory.mul(Int(i+1),loopInfos.getStepValue().copy());
					GenericInstruction indexOpExprB = GecosUserInstructionFactory.generic(
							loopInfos.getStepOp(), 
							loopInfos.getIterationIndex().getType(), 
							symbref(loopInfos.getIterationIndex()),
							exprTimeFactorB);
//					GenericInstruction indexOpExprB = InstrsFactory.eINSTANCE.createGenericInstruction();
//					indexOpExprB.setName(loopInfos.getStepOp());
//					indexOpExprB.addOperand(symbref(loopInfos.getIterationIndex()));
//					indexOpExprB.addOperand(exprTimeFactorB);

					GenericInstruction testValue = GecosUserInstructionFactory.generic(
							loopInfos.getReversedTestOp(),
							indexOpExprB.getType(),
							indexOpExprB,
							loopInfos.getStopValue().copy()
							);
//					GenericInstruction testValue = InstrsFactory.eINSTANCE.createGenericInstruction();
//					testValue.setName(loopInfos.getReversedTestOp());
//					testValue.addOperand(indexOpExprB);
//					testValue.addOperand(loopInfos.getStopValue().copy());

					String label = "BBL"+forBlock.getNumber()+i;
					CondInstruction condInst = GecosUserInstructionFactory.condBranch(testValue, label);
//					CondInstruction condInst = InstrsFactory.eINSTANCE.createCondInstruction();
//					condInst.setCond(testValue);
					
					BasicBlock condBB = BlocksFactory.eINSTANCE.createBasicBlock();
					condBB.addInstruction(condInst);
					
					IfBlock ifb = BlocksFactory.eINSTANCE.createIfBlock();
					ifb.setTestBlock(condBB);
					ifb.setThenBlock(breakBB);
					ifb.setJumpTo(label);
					
					unrolledBody.addChildren(ifb);
				}
			}
			
			//create new for block with new blocks
			ForBlock newFor = BlocksFactory.eINSTANCE.createForBlock();
			newFor.setInitBlock(initBlock);
			newFor.setTestBlock(testBlock);
			newFor.setStepBlock(stepBlock);
			newFor.setBodyBlock(unrolledBody);
			res = newFor;
			
			if (removeControl) {
				/*
				 * FIXME Partial unroller doesn't set the correct upper bound,
				 * doesn't strip mine correctly, and doesn't set the correct
				 * value for the iterator after the strip mine.
				 */
				
				//FIXME : following only works when iteration starts at 0...
				long nbCopyAtEnd = loopInfos.getIterationCount() % unrollFactor;
				long newEnd = (loopInfos.getIterationCount()) - (nbCopyAtEnd);
				
				long test = ((IntInstruction)loopInfos.getStartValue()).getValue() ; 
				for (int i = 0; i < newEnd-nbCopyAtEnd; i++) {
						test = loopInfos.incIndex(test);
				}
				
				//replace test value by the new end
				((GenericInstruction)((CondInstruction)((BasicBlock) testBlock).getInstructions().get(0)).getCond()).getOperands().set(1, Int(test));
				
				CompositeBlock newRes = GecosUserBlockFactory.CompositeBlock();
				newRes.setNumber(0);
				
//				Scope parentScope = unrolledBody.getScope().getParent();
				Scope nscope = unrolledBody.getScope().managedCopy(new BlockCopyManager()); 
				newRes.setScope(nscope);
				newRes.addChildren(newFor);
				
				int addOne = (loopInfos.getTestOp().contains("e"))?1:0;
				long value = 0;
				for (int i = 0; i < nbCopyAtEnd; i++) {
					value = loopInfos.valueAtIteration(newEnd + (i + addOne));
					Instruction inst = Int(value);
					
					if (init != null) {
						Block copyInit = init.copy();
						//replace each occurrence of index symbol by its current value.
						copyInit.accept(new InstrsSubstitution(loopIndex, inst));
						unrolledBody.addChildren(copyInit);
					}
					Block bi = body.copy();
					bi.accept(new InstrsSubstitution(loopIndex, inst));
					newRes.addChildren(bi);
				}
				
				//BasicBlock seti = BBlock(set(symbref(loopIndex),Int(value),loopIndex.getType())); 
				long finalValue;
				for (	finalValue = ((IntInstruction)loopInfos.getStartValue()).getValue() ; 
						loopInfos.testValue(finalValue); 
						finalValue = loopInfos.incIndex(finalValue)) ;
				BasicBlock seti = BBlock(set(symbref(loopIndex),Int(finalValue))); 
				newRes.addChildren(seti);
				
				res = newRes;
			}
			
			debugln(PREFIX+"unroll partial ends");
			return res;
		}
		
	}

	
}
