package fr.irisa.cairn.gecos.model.transforms.bringOutCond.internal;

import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksDefaultVisitor;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.IfBlock;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.Instruction;
import gecos.types.IntegerType;
import gecos.types.Type;

/**
 * 
 * This pass brings the test out from if constructs.
 * 
 * @author Imen Fassi
 * @date 15/06/2016
 */

public class BringOutConditionals extends GecosBlocksDefaultVisitor {

	private int counter = 0;

	public BringOutConditionals() {
		super();
	}

	private int incr_counter() {
		counter++;
		return counter;
	}
	
	@Override
	public void visitIfBlock(IfBlock i) {
		GecosUserTypeFactory.setScope(i.getScope());
		
		boolean delete = true;
		Block parent = i.getParent();
		Scope scp;
		CompositeBlock cb;
		BasicBlock newb = GecosUserBlockFactory.BBlock();

		if (parent instanceof CompositeBlock) {
			scp = i.getParent().getScope();
			cb = (CompositeBlock) parent;
			int idx = cb.getChildren().indexOf(i);
			if (idx > 0) {
				Block pbb = cb.getChildren().get(idx - 1);
				if (pbb instanceof BasicBlock) {
					newb = (BasicBlock) pbb;
					delete = false;
				} else
					cb.addBlockBefore(newb, i);
			}
			if (idx == 0)
				cb.addBlockBefore(newb, i);
		} else {
			BasicBlock aux = GecosUserBlockFactory.BBlock();
			parent.replace(i, aux);
			cb = GecosUserBlockFactory.CompositeBlock(newb, i);
			scp = cb.getScope();
			parent.replace(aux, cb);
		}

		if (i.getTestBlock() != null) {
			if (i.getTestBlock() instanceof BasicBlock) {
				BasicBlock bbcond = (BasicBlock) i.getTestBlock();
				if (bbcond.getInstructionCount() == 1) {

					Instruction instr = bbcond.getFirstInstruction();

					/**
					 * Create a symbol with the appropriate type
					 */
					Symbol symb = null;
					String name = new_temp(scp);

					Type type = instr.getType();
//					TypeAnalyzer type_var = new TypeAnalyzer(type);
//					if (type_var.getBaseLevel().isInt()) {
//						BaseType base = type_var.getBaseLevel().getBaseAsInt();
//						if (!base.isSigned() && base.getSize() == 1)
//							type = GecosUserTypeFactory.INT();
//					}
					if(type.asBase() != null) {
						IntegerType asInt = type.asBase().asInt();
						if(asInt != null && !asInt.getSigned() && asInt.getSize() == 1)
							type = GecosUserTypeFactory.INT();
					}

					symb = GecosUserCoreFactory.symbol(name, type, scp);

					/**
					 * Create the SymbolInstruction
					 */
					Instruction symbInstrTMP2 = GecosUserInstructionFactory.symbref(symb);
					/**
					 * replace the test by the SymbolInstruction
					 */
					bbcond.replaceInstruction(instr, symbInstrTMP2);

					/**
					 * Create the Symbol Instruction
					 */
					Instruction symbInstrTMP = GecosUserInstructionFactory.symbref(symb);

					/**
					 * Create a new SetInstruction
					 */
					Instruction setInstTEMP = GecosUserInstructionFactory.set(symbInstrTMP, instr);

					newb.addInstruction(setInstTEMP);

					if (i.getElseBlock() != null) {

						parent = i.getParent();
						Block newThenBlock = i.getElseBlock();
						i.setElseBlock(null);

						Instruction predicat = GecosUserInstructionFactory.lnot(GecosUserInstructionFactory.symbref(symb));
						
						IfBlock ifb = GecosUserBlockFactory.IfThen(predicat, newThenBlock);

						parent.addBlockAfter(ifb, i);

						newThenBlock.accept(this);
					}
				}

			} else if (delete)
				cb.removeBlock(newb);

		}

		if (i.getThenBlock() != null)
			i.getThenBlock().accept(this);
	}

	/**
	 * returns a new temporary name and verifies if The symbol is already used
	 * or not
	 */
	private String new_temp(Scope scope) {
		String name;
		boolean ok;
		if (scope != null) {
			do {
				name = "_Predicate_" + incr_counter();
				ok = true;
				for (Symbol s : scope.getSymbols()) {
					if (s.getName().equals(name)) {
						ok = false;
						break;
					}
				}
			} while (ok == false);
		} else
			name = "_Predicate_" + incr_counter();
		return name;
	}
}
