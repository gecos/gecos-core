package fr.irisa.cairn.gecos.model.dag.adapter.module;

import fr.irisa.cairn.gecos.model.dag.adapter.providers.DagDotAttributsProvider;
import fr.irisa.cairn.gecos.model.dag.adapter.providers.DagEdgeLabelProvider;
import fr.irisa.cairn.gecos.model.dag.adapter.providers.DagLabelProvider;
import fr.irisa.cairn.graph.guice.modules.providersbinding.LabelProviderModule;
import fr.irisa.cairn.graph.providers.IAttributsProvider;
import fr.irisa.cairn.graph.providers.IEdgeLabelProvider;
import fr.irisa.cairn.graph.providers.INodeLabelProvider;

public class DagLabelProviderModule extends LabelProviderModule {

	protected void bindIAttributeProvider() {
		this.bind(IAttributsProvider.class).toInstance(new DagDotAttributsProvider());
	}

	protected void bindINodeLabelProvider() {
		this.bind(INodeLabelProvider.class).to(DagLabelProvider.class);
	}
	
	@Override
	protected void bindIEdgeLabelProvider() {
		this.bind(IEdgeLabelProvider.class).to(DagEdgeLabelProvider.class);
	}

}
