/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.tools.switches;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.gecos.model.extensions.switchs.IAdaptableSwitch;
import fr.irisa.cairn.gecos.model.extensions.switchs.SwitchDelegate;
import gecos.blocks.BasicBlock;
import gecos.instrs.Instruction;

public class AdaptableDefaultInstructionSwitch<T> extends DefaultInstructionSwitch<T> implements IAdaptableSwitch<T> {

	private SwitchDelegate<T> delegate = new SwitchDelegate<T>();

	public SwitchDelegate<T> getDelegate() {
		return delegate;
	}

	@Override
	public T doSwitch(EObject o) {
		T res = delegate.doSwitch(o, this);
		if (res == null) {
			res = super.doSwitch(o);
		}
		return res;
	}
	@SuppressWarnings("unchecked")
	public Object getAdapter(@SuppressWarnings("rawtypes") Class cls) {
		if (cls.isAssignableFrom(getClass()))
			return this;
		return Platform.getAdapterManager().loadAdapter(this, cls.getName());
	}
	
	public void run(BasicBlock bb) {
		for (Instruction i : bb.getInstructions()) {
			doSwitch(i);
		}
	}	
}
