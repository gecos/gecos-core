package fr.irisa.cairn.gecos.model.extensions.switchs;

import java.lang.reflect.Constructor;

/**
 * Factory for the switch adapters. A switch adapter has to be constructed with
 * only one parameter which is the adaptable switch instance.
 * <br><br><b>Example:</b>
 * <code>
 * 	public MySwitchAdapter(AdaptableSwitch sw)
 * </code>
 * 
 * @author Antoine Floc'h - Initial contribution and API
 * 
 */
public class SwitchAdapterFactory {

	public Object buildAdapter(Class<? extends Object> adapterClass,
			Object adaptable) {

		Constructor<Object> constructor = getDefaultConstructor(adapterClass,
				adaptable.getClass());
		try {
			Object adapter = constructor.newInstance(adaptable);
			return adapter;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Constructor<Object> getDefaultConstructor(Class adapterClass,
			Class adaptableClass) {
		Constructor<Object>[] declaredConstructors = (Constructor<Object>[]) adapterClass
				.getDeclaredConstructors();
		for (Constructor<Object> constructor : declaredConstructors) {
			Class<?>[] parameterTypes = constructor.getParameterTypes();
			if (parameterTypes.length == 1) {
				if (parameterTypes[0] == adaptableClass) {
					return constructor;
				}
			}
		}
		throw new IllegalArgumentException(
				"No matching constructor in the adapter ("
						+ adapterClass.getCanonicalName() + ")");
	}

}
