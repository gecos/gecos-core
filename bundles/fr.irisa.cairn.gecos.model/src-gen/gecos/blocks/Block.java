/**
 */
package gecos.blocks;

import fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;

import gecos.annotations.AnnotatedElement;

import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.Scope;

import gecos.instrs.BranchType;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.blocks.Block#getParent <em>Parent</em>}</li>
 *   <li>{@link gecos.blocks.Block#getNumber <em>Number</em>}</li>
 * </ul>
 *
 * @see gecos.blocks.BlocksPackage#getBlock()
 * @model abstract="true"
 *        annotation="gmf.node figure='rectangle' label='number' label.placement='external' label.icon='false'"
 * @generated
 */
public interface Block extends AnnotatedElement, BlocksVisitable {
	/**
	 * Returns the value of the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' reference.
	 * @see gecos.blocks.BlocksPackage#getBlock_Parent()
	 * @model resolveProxies="false" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='&lt;%org.eclipse.emf.ecore.EObject%&gt; _eContainer = this.eContainer();\nif ((_eContainer instanceof &lt;%gecos.blocks.Block%&gt;))\n{\n\t&lt;%org.eclipse.emf.ecore.EObject%&gt; _eContainer_1 = this.eContainer();\n\treturn ((&lt;%gecos.blocks.Block%&gt;) _eContainer_1);\n}\nreturn null;'"
	 * @generated
	 */
	Block getParent();

	/**
	 * Returns the value of the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This attribute does not have any semantic in the model.
	 * For a renumbering of the Blocks, see {@link FixBlockNumbers}.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Number</em>' attribute.
	 * @see #setNumber(int)
	 * @see gecos.blocks.BlocksPackage#getBlock_Number()
	 * @model unique="false" dataType="gecos.blocks.int"
	 * @generated
	 */
	int getNumber();

	/**
	 * Sets the value of the '{@link gecos.blocks.Block#getNumber <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number</em>' attribute.
	 * @see #getNumber()
	 * @generated
	 */
	void setNumber(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this;'"
	 * @generated
	 */
	Block simplifyBlock();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.blocks.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getPathString(this.toShortString());'"
	 * @generated
	 */
	String getPathString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.blocks.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _eIsProxy = this.eIsProxy();\nif (_eIsProxy)\n{\n\treturn super.toString();\n}\n&lt;%java.lang.String%&gt; _string = super.toString();\nfinal &lt;%java.lang.StringBuffer%&gt; result = new &lt;%java.lang.StringBuffer%&gt;(_string);\nresult.append(\" (number: \");\nresult.append(this.getNumber());\nresult.append(\")\");\nreturn result.toString();'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Parent block is not a CompositeBlock, so a new CompositeBlock have to be
	 * created
	 * 
	 * @param insertedBlock block to be added
	 * @param existingBlock insertion point block
	 * <!-- end-model-doc -->
	 * @model insertedBlockUnique="false" existingBlockUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.blocks.Block%&gt; parent = this.getParent();\nfinal &lt;%gecos.blocks.CompositeBlock%&gt; compositeBlock = &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory%&gt;.CompositeBlock();\nparent.replace(existingBlock, compositeBlock);\ncompositeBlock.getChildren().add(0, existingBlock);\ncompositeBlock.getChildren().add(1, insertedBlock);'"
	 * @generated
	 */
	void addBlockAfter(Block insertedBlock, Block existingBlock);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _simpleName = this.getClass().getSimpleName();\n&lt;%java.lang.String%&gt; _plus = (\"Non applicable to an object of class \" + _simpleName);\nthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus);'"
	 * @generated
	 */
	void mergeChildComposite(CompositeBlock b);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model toUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _simpleName = this.getClass().getSimpleName();\n&lt;%java.lang.String%&gt; _plus = (\"Non applicable to an object of class \" + _simpleName);\nthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus);'"
	 * @generated
	 */
	void mergeWithBasic(BasicBlock to);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Remove the block bb from the children (cf. implementation for specific blocks).
	 * If there are no children, or bb does not appear in children, then it does nothing. <br/>
	 * Note : this method does not remove the block from the memory. When using
	 * it, you may have a look at the scope management. <br/>
	 * To properly delete a Block from the memory (if needed), use the BlocksEraser class.
	 * @param bb
	 * 		block that has to be removed
	 * <!-- end-model-doc -->
	 * @model bbUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _simpleName = this.getClass().getSimpleName();\n&lt;%java.lang.String%&gt; _plus = (\"Non applicable to an object of class \" + _simpleName);\nthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus);'"
	 * @generated
	 */
	void removeBlock(Block bb);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Replace the block oldBlock in the children (cf. implementation for specific blocks) by newBlock.
	 * If there are no children, or oldBlock does not appear in children, then it does nothing.
	 * <br>
	 * Note : this method does not remove the old block from the memory. When using
	 * it, you may have a look at the scope management.
	 * <br>
	 * To properly delete the oldBlock from the memory (if needed), use the BlocksEraser class.
	 * 
	 * @param bb block that has to be removed
	 * <!-- end-model-doc -->
	 * @model oldBlockUnique="false" newBlockUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _simpleName = this.getClass().getSimpleName();\n&lt;%java.lang.String%&gt; _plus = (\"Non applicable to an object of class \" + _simpleName);\nthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus);'"
	 * @generated
	 */
	void replace(Block oldBlock, Block newBlock);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * <p>
	 * Connect {@code this} to the Block {@code to} with {@code cond} as {@link BranchType}.
	 * Does not consider break/continue/return statements.
	 * </p>
	 * <p><ul>
	 * <li> {@link BasicBlock}: call to connectFromBasic
	 * <li> {@link CompositeBlock}: connect last child (UNCONDITIONAL); create an empty BasicBlock if CompositeBlock is empty.
	 * <li> {@link CaseBlock}: connect body (UNCONDITIONAL); if body is null, no edge created.
	 * <li> {@link ForBlock} (and ForC99 + SimpleFor): connect test (IF_FALSE); if test is null, no edge created.
	 * <li> {@link DoWhileBlock}: connect test (IF_FALSE); if test is null, no edge created.
	 * <li> {@link WhileBlock}: connect test (IF_FALSE); if test is null, no edge created.
	 * <li> {@link SwitchBlock}: connect body (UNCONDITIONAL); if body is null, connect dispatch (UNCONDITIONAL); if dispatch is null, no edge created
	 * <li> {@link IfBlock}: connect then and else (UNCONDITIONAL); if then is null, connect test (IF_TRUE); if else is null, connect test (IF_FALSE); if test is also null, no edge created.
	 * </ul></p>
	 * <!-- end-model-doc -->
	 * @model toUnique="false" condUnique="false"
	 * @generated
	 */
	EList<ControlEdge> connectTo(Block to, BranchType cond);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" fromUnique="false" condUnique="false"
	 * @generated
	 */
	ControlEdge connectFromBasic(BasicBlock from, BranchType cond);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Copy a Block and use its parent scope information.
	 * @param bb block that has to be removed
	 * <!-- end-model-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%fr.irisa.cairn.gecos.model.tools.utils.GecosCopier%&gt; gecosCopier = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.GecosCopier%&gt;();\n&lt;%org.eclipse.emf.ecore.EObject%&gt; _copy = gecosCopier.copy(this);\nfinal T block = ((T) _copy);\ngecosCopier.copyReferences();\nreturn block;'"
	 * @generated
	 */
	<T extends Block> T copy();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" cblockUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='cblock.getScope().mergeWith(this.getScope());\nreturn cblock;'"
	 * @generated
	 */
	Block mergeWithComposite(CompositeBlock cblock);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @deprecated use MergeCompositeBlocks from transforms plugin
	 * <!-- end-model-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='throw new &lt;%java.lang.UnsupportedOperationException%&gt;();'"
	 * @generated
	 */
	boolean mergeChildren();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((this instanceof &lt;%gecos.core.ScopeContainer%&gt;))\n{\n\tfinal &lt;%gecos.core.Scope%&gt; res = ((&lt;%gecos.core.ScopeContainer%&gt;) this).getScope();\n\tif ((res != null))\n\t{\n\t\treturn res;\n\t}\n}\n&lt;%gecos.blocks.Block%&gt; _parent = this.getParent();\n&lt;%gecos.core.Scope%&gt; _scope = null;\nif (_parent!=null)\n{\n\t_scope=_parent.getScope();\n}\nreturn _scope;'"
	 * @generated
	 */
	Scope getScope();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 * @generated
	 */
	void accept(BlocksVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.ecore.EObject%&gt; containr = this.eContainer();\nwhile ((containr != null))\n{\n\tif ((containr instanceof &lt;%gecos.core.Procedure%&gt;))\n\t{\n\t\treturn ((&lt;%gecos.core.Procedure%&gt;)containr);\n\t}\n\telse\n\t{\n\t\tcontainr = containr.eContainer();\n\t}\n}\nreturn null;'"
	 * @generated
	 */
	Procedure getContainingProcedure();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.core.Procedure%&gt; _containingProcedure = this.getContainingProcedure();\n&lt;%gecos.core.ProcedureSet%&gt; _containingProcedureSet = null;\nif (_containingProcedure!=null)\n{\n\t_containingProcedureSet=_containingProcedure.getContainingProcedureSet();\n}\nreturn _containingProcedureSet;'"
	 * @generated
	 */
	ProcedureSet getContainingProcedureSet();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.blocks.String" unique="false" nodeNameDataType="gecos.blocks.String" nodeNameUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.blocks.Block%&gt; parent = this.getParent();\ntry\n{\n\tif ((parent == null))\n\t{\n\t\t&lt;%gecos.core.Procedure%&gt; _containingProcedure = this.getContainingProcedure();\n\t\tboolean _tripleNotEquals = (_containingProcedure != null);\n\t\tif (_tripleNotEquals)\n\t\t{\n\t\t\tfinal &lt;%gecos.core.ProcedureSymbol%&gt; symbol = this.getContainingProcedure().getSymbol();\n\t\t\tif ((symbol != null))\n\t\t\t{\n\t\t\t\t&lt;%java.lang.String%&gt; _name = symbol.getName();\n\t\t\t\t&lt;%java.lang.String%&gt; _plus = (\"/proc:\" + _name);\n\t\t\t\t&lt;%java.lang.String%&gt; _plus_1 = (_plus + \"/\");\n\t\t\t\treturn (_plus_1 + nodeName);\n\t\t\t}\n\t\t\telse\n\t\t\t{\n\t\t\t\treturn (\"/??/\" + nodeName);\n\t\t\t}\n\t\t}\n\t\telse\n\t\t{\n\t\t\treturn (\"/\" + nodeName);\n\t\t}\n\t}\n\telse\n\t{\n\t\tfinal int indexOf = parent.eContents().indexOf(this);\n\t\t&lt;%java.lang.String%&gt; _pathString = parent.getPathString();\n\t\t&lt;%java.lang.String%&gt; _plus_2 = (_pathString + \"/\");\n\t\t&lt;%java.lang.String%&gt; _plus_3 = (_plus_2 + nodeName);\n\t\t&lt;%java.lang.String%&gt; _plus_4 = (_plus_3 + \"#\");\n\t\treturn (_plus_4 + &lt;%java.lang.Integer%&gt;.valueOf(indexOf));\n\t}\n}\ncatch (final Throwable _t) {\n\tif (_t instanceof &lt;%java.lang.Exception%&gt;) {\n\t\treturn (\"/exception_\" + nodeName);\n\t}\n\telse\n\t{\n\t\tthrow &lt;%org.eclipse.xtext.xbase.lib.Exceptions%&gt;.sneakyThrow(_t);\n\t}\n}'"
	 * @generated
	 */
	String getPathString(String nodeName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.ecore.EObject%&gt; _eContainer = this.eContainer();\nreturn (_eContainer instanceof &lt;%gecos.core.Procedure%&gt;);'"
	 * @generated
	 */
	boolean isRootBlock();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @deprecated use {@link #getContainingProcedure()} instead
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getContainingProcedure();'"
	 * @generated
	 */
	Procedure getRootProcedure();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" mngDataType="gecos.core.BlockCopyManager" mngUnique="false"
	 * @generated
	 */
	Block managedCopy(BlockCopyManager mng);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.blocks.String" unique="false"
	 * @generated
	 */
	String toShortString();

} // Block
