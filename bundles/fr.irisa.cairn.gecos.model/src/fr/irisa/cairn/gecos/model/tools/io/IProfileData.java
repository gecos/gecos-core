/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.tools.io;

public interface IProfileData {
	
	/**
	 * Get the profile data of a basic block.
	 * 
	 * @param block
	 * @return
	 */
	int get(int block);
	
	/**
	 * Add the profile data of a basic block.
	 * 
	 * @param block
	 * @return
	 */
	public void add(int block, int count);

}
