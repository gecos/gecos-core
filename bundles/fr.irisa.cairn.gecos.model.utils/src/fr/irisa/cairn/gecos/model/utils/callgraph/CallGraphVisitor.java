package fr.irisa.cairn.gecos.model.utils.callgraph;

public abstract class CallGraphVisitor {

	public abstract Object visitCallGraph(CallGraph callGraph) ;
	public abstract Object visitCallGraphNode(CallGraphNode callGraphNode) ;
	public abstract Object visitCallGraphEdge(CallGraphEdge callGraphEdge) ;

}
