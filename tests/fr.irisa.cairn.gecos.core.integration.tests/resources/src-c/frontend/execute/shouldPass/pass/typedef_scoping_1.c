typedef int T;
T a;

int main() {
	typedef char T;
	T b;

	if (sizeof(a) != sizeof(int))
		abort ();
	if (sizeof(b) != sizeof(char))
		abort ();

	exit (0);
}
