/**
 */
package gecos.instrs.impl;

import gecos.instrs.ArrayValueInstruction;
import gecos.instrs.InstrsPackage;
import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Array Value Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ArrayValueInstructionImpl extends ChildrenListInstructionImpl implements ArrayValueInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArrayValueInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstrsPackage.eINSTANCE.getArrayValueInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final InstrsVisitor visitor) {
		visitor.visitArrayValueInstruction(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _join = IterableExtensions.join(this.listChildren(), ",");
		String _plus = ("{" + _join);
		return (_plus + "}");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isConstant() {
		EList<Instruction> _listChildren = this.listChildren();
		for (final Instruction child : _listChildren) {
			boolean _isConstant = child.isConstant();
			boolean _not = (!_isConstant);
			if (_not) {
				return false;
			}
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSame(final Instruction instr) {
		if ((instr instanceof ArrayValueInstruction)) {
			return super.isSame(instr);
		}
		return false;
	}

} //ArrayValueInstructionImpl
