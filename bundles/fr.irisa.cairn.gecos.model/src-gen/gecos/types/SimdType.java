/**
 */
package gecos.types;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simd Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.SimdType#getSubwordType <em>Subword Type</em>}</li>
 *   <li>{@link gecos.types.SimdType#getSize <em>Size</em>}</li>
 * </ul>
 *
 * @see gecos.types.TypesPackage#getSimdType()
 * @model
 * @generated
 */
public interface SimdType extends Type, TypesVisitable {
	/**
	 * Returns the value of the '<em><b>Subword Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subword Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subword Type</em>' reference.
	 * @see #setSubwordType(Type)
	 * @see gecos.types.TypesPackage#getSimdType_SubwordType()
	 * @model resolveProxies="false"
	 * @generated
	 */
	Type getSubwordType();

	/**
	 * Sets the value of the '{@link gecos.types.SimdType#getSubwordType <em>Subword Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subword Type</em>' reference.
	 * @see #getSubwordType()
	 * @generated
	 */
	void setSubwordType(Type value);

	/**
	 * Returns the value of the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Size</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Size</em>' attribute.
	 * @see #setSize(long)
	 * @see gecos.types.TypesPackage#getSimdType_Size()
	 * @model unique="false" dataType="gecos.types.long"
	 * @generated
	 */
	long getSize();

	/**
	 * Sets the value of the '{@link gecos.types.SimdType#getSize <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Size</em>' attribute.
	 * @see #getSize()
	 * @generated
	 */
	void setSize(long value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitSimdType(this);'"
	 * @generated
	 */
	void accept(TypesVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _string = this.getSubwordType().toString();\n&lt;%java.lang.String%&gt; _plus = (_string + \"&lt;\");\nlong _size = this.getSize();\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + &lt;%java.lang.Long%&gt;.valueOf(_size));\nreturn (_plus_1 + \"&gt;\");'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.boolean" unique="false" otherUnique="false" ignoreStorageClassDataType="gecos.types.boolean" ignoreStorageClassUnique="false" ignoreConstDataType="gecos.types.boolean" ignoreConstUnique="false" ignoreVolatileDataType="gecos.types.boolean" ignoreVolatileUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((other == this))\n{\n\treturn true;\n}\nif ((other instanceof &lt;%gecos.types.SimdType%&gt;))\n{\n\tfinal &lt;%gecos.types.SimdType%&gt; vt = ((&lt;%gecos.types.SimdType%&gt;)other);\n\treturn (((this.getSize() == vt.getSize()) &amp;&amp; this.getSubwordType().isEqual(vt.getSubwordType(), ignoreStorageClass, ignoreConst, ignoreVolatile)) &amp;&amp; super.isEqual(other, ignoreStorageClass, ignoreConst, ignoreVolatile));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isEqual(Type other, boolean ignoreStorageClass, boolean ignoreConst, boolean ignoreVolatile);

} // SimdType
