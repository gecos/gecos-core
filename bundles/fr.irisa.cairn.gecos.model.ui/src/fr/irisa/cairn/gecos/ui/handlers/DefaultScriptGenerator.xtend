package fr.irisa.cairn.gecos.ui.handlers;

class DefaultScriptGenerator {
	def generate()'''
#------------------------------------------------------------------------ 
#       ______     ______     _____
#      / ____/__  / ____/___ / ___/
#     / / __/ _ \/ /   / __ \\__ \ 
#    / /_/ /  __/ /___/ /_/ /__/ / 
#    \____/\___/\____/\____/____/  --Generic Compiler Suite--
#                              	   (c) INRIA-IRISA 2011-2014
#------------------------------------------------------------------------  
#
# This is a default GeCoS compiler script. It is called when selecting 
# 'compile' in the Gecos menu available in the C editor. 
#
#
# Parameter : edited C file path
#

#Create a new GeCoS project and add the C source file
project = CreateGecosProject("project");
AddSourceToGecosProject(project,$1);

#Build the procedure set using CDT
CDTFrontend(project);

#Add your compilation passes here
for file in project do
	ps = GetProcedureSet(file);
	#outputs a dotty representing the CDFG for ps
	output(ps);
done;
	'''
}