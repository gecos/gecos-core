/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.cdtfrontend.visitor;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.cdt.core.dom.ast.IASTASMDeclaration;
import org.eclipse.cdt.core.dom.ast.IASTCompositeTypeSpecifier;
import org.eclipse.cdt.core.dom.ast.IASTDeclaration;
import org.eclipse.cdt.core.dom.ast.IASTDeclarator;
import org.eclipse.cdt.core.dom.ast.IASTFunctionDefinition;
import org.eclipse.cdt.core.dom.ast.IASTProblemDeclaration;
import org.eclipse.cdt.core.dom.ast.IASTSimpleDeclaration;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTExplicitTemplateInstantiation;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTLinkageSpecification;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTNamespaceAlias;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTNamespaceDefinition;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTTemplateDeclaration;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTTemplateSpecialization;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTUsingDeclaration;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTUsingDirective;

import fr.irisa.cairn.gecos.model.cdtfrontend.exception.CDTParserErrorException;
import fr.irisa.cairn.gecos.model.cdtfrontend.exception.CDTParserNotYetSupportedException;
import fr.irisa.cairn.gecos.model.cdtfrontend.exception.CDTParserProblemException;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import gecos.annotations.PragmaAnnotation;
import gecos.core.Procedure;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.Instruction;
import gecos.types.AliasType;
import gecos.types.Field;
import gecos.types.FunctionType;
import gecos.types.Type;

/**
 * @author mnaullet
 * 
 * TODO (aelmouss) need to check for incomplete types ... 
 */
public class ASTDeclarationVisitor {

	private CDTToGecosModel gecosModel;
	private Symbol symbol;
	private AliasType aliasType;
	private List<Field> fields;
	private Map<String, Instruction> declarations;

	public ASTDeclarationVisitor() {
		declarations = new LinkedHashMap<String, Instruction>();
	}

	public void visit(CDTToGecosModel gecosModel, IASTDeclaration declaration) {
		this.gecosModel = gecosModel;
		// find the correct declaration and implements it in the Gecos Model
		if (declaration instanceof IASTASMDeclaration)
			buildIASTASMDeclaration((IASTASMDeclaration)declaration);
		else if (declaration instanceof IASTFunctionDefinition)
			buildIASTFunctionDefinition((IASTFunctionDefinition)declaration);
		else if (declaration instanceof IASTProblemDeclaration)
			buildIASTProblemDeclaration((IASTProblemDeclaration)declaration);
		else if (declaration instanceof IASTSimpleDeclaration)
			buildIASTSimpleDeclaration((IASTSimpleDeclaration)declaration);
		else if (declaration instanceof ICPPASTExplicitTemplateInstantiation)
			buildICPPASTExplicitTemplateInstantiation((ICPPASTExplicitTemplateInstantiation)declaration);
		else if (declaration instanceof ICPPASTLinkageSpecification)
			buildICPPASTLinkageSpecification((ICPPASTLinkageSpecification)declaration);
		else if (declaration instanceof ICPPASTNamespaceAlias)
			buildICPPASTNamespaceAlias((ICPPASTNamespaceAlias)declaration);
		else if (declaration instanceof ICPPASTNamespaceDefinition)
			buildICPPASTNamespaceDefinition((ICPPASTNamespaceDefinition)declaration);
		else if (declaration instanceof ICPPASTTemplateDeclaration)
			buildICPPASTTemplateDeclaration((ICPPASTTemplateDeclaration)declaration);
		else if (declaration instanceof ICPPASTTemplateSpecialization)
			buildICPPASTTemplateSpecialization((ICPPASTTemplateSpecialization)declaration);
		else if (declaration instanceof ICPPASTUsingDeclaration)
			buildICPPASTUsingDeclaration((ICPPASTUsingDeclaration)declaration);
		else if (declaration instanceof ICPPASTUsingDirective)
			buildICPPASTUsingDirective((ICPPASTUsingDirective)declaration);
		else
			notYetSupported(declaration);
		
		if (!gecosModel.visitingInclude())
			gecosModel.addIASTAnnotation(symbol, declaration);
		
		gecosModel.addFileLocationAnnotation(symbol, declaration.getFileLocation());
	}
	
	public List<Field> getFields() {
		return fields;
	}
	
	public Map<String, Instruction> getDeclarations() {
		return declarations;
	}
	
	public Symbol getSymbol() {
		return symbol;
	}

	/** 
	 * Case of Procedure definition with a body (implementation)
	 * @param declaration
	 */
	private void buildIASTFunctionDefinition(IASTFunctionDefinition declaration) {
		List<PragmaAnnotation> pragmaAnnotations = gecosModel.findAnnotation(declaration.getFileLocation().getFileName(), declaration.getFileLocation().getNodeOffset());

		/* visit/build declaration specifier */
		declaration.getDeclSpecifier().accept(gecosModel);
		ASTDeclSpecifierVisitor declSpecifierVisitor = (ASTDeclSpecifierVisitor)gecosModel.getObject();
		
		/* visit/build declarator before adding the ProcedureSymbol Scope so that all types would be added in the parent scope */
		IASTDeclarator outermostDeclarator = declaration.getDeclarator();
		while(outermostDeclarator.getParent() != declaration && outermostDeclarator.getParent() instanceof IASTDeclarator)
			outermostDeclarator = (IASTDeclarator) outermostDeclarator.getParent();
		outermostDeclarator.accept(gecosModel);
		ASTDeclaratorVisitor declaratorVisitor = (ASTDeclaratorVisitor)gecosModel.getObject();

		assert(declaratorVisitor.getInnermost().isFunction());
		String name = declaratorVisitor.getName();
		FunctionType funcType = declaratorVisitor.buildType(declSpecifierVisitor.getType()).asFunction();
		if(declaration.getDeclSpecifier().isInline())
			funcType.setInline(true); //XXX untested
		
		/* checking for errors... */
		if(funcType.isHasElipsis() && funcType.listParameters().isEmpty())
			throw new CDTParserErrorException(declaration, "ISO C requires a named argument before '...'");
		Symbol _existingSymbol = gecosModel.getGecosCoreContextedFactory().findSymbol(name);
		if (_existingSymbol != null) {
			if(_existingSymbol instanceof ProcedureSymbol) {
				if(((ProcedureSymbol) _existingSymbol).getProcedure() != null)
					throw new CDTParserErrorException(declaration, "redefinition of '" + name + "'");
				if(! (((ProcedureSymbol) _existingSymbol).listParameters().isEmpty() && !funcType.isHasElipsis()))
					if(!_existingSymbol.getType().isSimilar(funcType, true, true, true))
						throw new CDTParserErrorException(declaration, "conflicting types for '" + name + "'");
			} else if(gecosModel.getGecosCoreContextedFactory().isInCurrentScope(_existingSymbol))
				throw new CDTParserErrorException(declaration, "'" + name + "' redeclared as different kind of symbol");
		}
		
		/* create ProcedureSymbol scope and move all ParameterSymbols inside it */ 
		Scope procParamsScope = gecosModel.getGecosCoreContextedFactory().createScope();
		procParamsScope.getSymbols().addAll(declaratorVisitor.getInnermost().getParameterSymbols()); // innermost: in case of e.g.: function that returns a pointer to function
		
		/* Exit out the ProcedureSymbol scope so that the created ProcedureSymbol would be added in the parent scope */
		gecosModel.getGecosCoreContextedFactory().goToUpperScope();
		
		ProcedureSymbol procSym = gecosModel.getGecosCoreContextedFactory().addProcedureSymbol(name, funcType, procParamsScope);
		
		if (gecosModel.visitingInclude())
			gecosModel.ignore(procSym);
		else {
			Procedure procedure = gecosModel.getGecosCoreContextedFactory().addProcedure(procSym);
//			gecosModel.addFileLocationAnnotation(procedure, declaration.getFileLocation());
			
			Symbol procSymbol = gecosModel.getGecosCoreContextedFactory().getSymbol(name);
			if (procSym != procSymbol)
				throw new RuntimeException("Error");
			
			/* visit/build procedure's body */
			declaration.getBody().accept(gecosModel);
			gecosModel.getGecosCoreContextedFactory().goToUpperScope();
			gecosModel.getGecosCoreContextedFactory().goToUpperScope();
		}
		GecosUserAnnotationFactory.pragma(procSym, pragmaAnnotations);
	}

	/**
	 * SimpleDeclaration = [DECL_SPEC]+ DECLARATOR [= INITIALIZER] [, DECLARATOR [= INITIALIZER]]*;
	 * <li>DECL_SPEC = STOR | SPEC | QUAL <ul>
	 *    <li>STOR (storage class specifier) = [extern | static | auto | register | typedef]
	 *    <li>SPEC (type specifier) = [void | char | short | int | long | float | double | signed | unsigned | enum_spec | union_spec | struct_spec ]
	 *    <li>QUAL (type qualifier) = [const | volatile]
	 * </ul>   
	 * <li>DECLARATOR = [POINTER] identifier | (DECL) [ARRAY_MOD | FUNC]* <ul>
	 *    <li>ARRAY_MOD = '['[size]']'
	 *    <li>FUNC = '('[params]')'
	 * </ul>
	 * 
	 * e.g.: static const int (* const (* x)[2]) (int), *y;
	 * <li> DECL_SPEC = static const int
	 * <li> first DECLARATOR: (* const (* x)[2]) (int) : this is a nested declarator declaring 'x' as 
	 * a pointer to array to const pointer to function that takes int and returns DECL_SPEC.
	 * <li> second DECLARATOR: *y : this a non-nested declarator declaring 'y' as a pointer to DECL_SPEC.
	 * 
	 * @param declaration
	 */
	private void buildIASTSimpleDeclaration(IASTSimpleDeclaration declaration) {
		List<PragmaAnnotation> pragmaAnnotations = gecosModel.findAnnotation(declaration.getFileLocation().getFileName(), declaration.getFileLocation().getNodeOffset());
		
		/* visit/build declaration specifier */
		declaration.getDeclSpecifier().accept(gecosModel);
		ASTDeclSpecifierVisitor declSpecifierVisitor = (ASTDeclSpecifierVisitor)gecosModel.getObject();
		Type typeSpecifier = declSpecifierVisitor.getType();
		boolean isTypeDef = declSpecifierVisitor.isTypedef();
		
		boolean isInRecord = declaration.getParent() instanceof IASTCompositeTypeSpecifier;
		if(isInRecord)
			fields = new ArrayList<>(declaration.getDeclarators().length);

		/* visit declarators: each represent a symbol, an alias or a record field */
		for(int i = 0; i < declaration.getDeclarators().length; i++) {
			IASTDeclarator declarator = declaration.getDeclarators()[i];
			declarator.accept(gecosModel);
			ASTDeclaratorVisitor declaratorVisitor = (ASTDeclaratorVisitor)gecosModel.getObject();
			
			String name = declaratorVisitor.getName();
			Type type = declaratorVisitor.buildType(typeSpecifier);
			
			/* 
			 * In case this declaration is a typedef: 
			 * - the referenced type must be already defined in the current scope hierarchy.
			 * - if the CURRENT scope already contains a AliasType or Symbol with the same name => error.
			 * - otherwise, create a new AliasType in the CURRENT scope.
			 */
			if (isTypeDef) {
				if(! gecosModel.getGecosCoreContextedFactory().isDefined(type))
					throw new CDTParserErrorException(declarator, "aliasing an unknown type ["+type+"] by " + name);
				if (gecosModel.getGecosCoreContextedFactory().findAliasTypeInCurrentScope(name) != null)
					throw new CDTParserErrorException(declarator, "redefinition of '"+name+"'");
				if (gecosModel.getGecosCoreContextedFactory().findSymbolInCurrentScope(name) != null)
					throw new CDTParserErrorException(declarator, "redefinition of '"+name+"' as different kind of symbol");
				
				aliasType = GecosUserTypeFactory.ALIAS(type, name); //should be in current scope
				GecosUserAnnotationFactory.pragma(aliasType, pragmaAnnotations);
				if (gecosModel.visitingInclude())
					gecosModel.ignore(aliasType);
			}
			
			/* 
			 * In case this declaration is a record (struct, union,...) field:
			 * create/add the corresponding field.
			 */
			else if (isInRecord) {
				if (declaratorVisitor.getBitSize() != -1 && declaratorVisitor.getBitSize() > type.getSize())
					throw new CDTParserErrorException(declarator, "The size of the bitfield cannot exced the size of the type declaration.");
				
				fields.add(GecosUserTypeFactory.FIELD(name, type, declaratorVisitor.getBitSize()));
			}
			
			/* In case this declaration is a Procedure Symbol:
			 * - if a ProcedureSymbol with the same name already exists in the scope hierarchy:
			 *     - if different prototypes => error, ignore otherwise.
			 * - if a Symbol with the same name exists in the CURRENT scope => error.
			 * - else, create the a new ProcedureSymbol and add it to the CURRENT scope.
			 */
			else if (declaratorVisitor.getInnermost().isFunction()) {
				FunctionType funcType = type.asFunction();
				assert(funcType != null);
				if(declaration.getDeclSpecifier().isInline())
					funcType.setInline(true); //XXX untested

				if(funcType.isHasElipsis() && funcType.listParameters().isEmpty())
					throw new CDTParserErrorException(declarator, "ISO C requires a named argument before '...'");
				
				Symbol existingSymbol = gecosModel.getGecosCoreContextedFactory().findSymbol(declaratorVisitor.getName());
				if (existingSymbol != null) {
					if(existingSymbol instanceof ProcedureSymbol) {
						if(!existingSymbol.getType().isSimilar(type, true, true, true))
							throw new CDTParserErrorException(declarator, "conflicting types for '" + name + "'");
					} else if(gecosModel.getGecosCoreContextedFactory().isInCurrentScope(existingSymbol))
						throw new CDTParserErrorException(declarator, "redefinition of '"+name+"' as different kind of symbol");
					
					symbol = existingSymbol;
				} else {
					/* create ProcedureSymbol scope and move all ParameterSymbols inside it
					 * NOTE:  this is important to isolate Parameter symbols from the parent Scope
					 *    to avoid conflicts: e.g.: int foo(int A); int A() {};
					 *    in this case if the parameter A was placed in the parent scope, it will 
					 *    mistakenly conflict with the function declaration A(). 
					 */ 
					Scope procParamsScope = gecosModel.getGecosCoreContextedFactory().createScope();
					procParamsScope.getSymbols().addAll(declaratorVisitor.getInnermost().getParameterSymbols()); // innermost: in case of e.g.: function that returns a pointer to function
					
					/* Exit out the ProcedureSymbol scope so that the created ProcedureSymbol would be added in the parent scope */
					gecosModel.getGecosCoreContextedFactory().goToUpperScope();
					symbol = gecosModel.getGecosCoreContextedFactory().addProcedureSymbol(name, funcType, procParamsScope);
					GecosUserAnnotationFactory.pragma(symbol, pragmaAnnotations);
					if (gecosModel.visitingInclude() && symbol != null) {
						gecosModel.ignore(symbol);
						GecosUserAnnotationFactory.pragma(symbol,GecosUserAnnotationFactory.HEADER_SYMBOL_ANNOTATION);
					}
				}
			}
				
			/*
			 * In case this declaration declares a Symbol:
			 * - if a Alias with the same name already exists in the CURRENT scope => error
			 * - if a Symbol with the same name already exists in the CURRENT scope:
			 *    - if CURRENT scope is NOT Global => error.
			 *    - else if both symbols have intializers or have conflicting COMPLETE types => error
			 *        - FIXME in case INCOMPLETE types: conflicting types may not be an error (e.g. in global scope: int a[]; int a[10];)
			 *    - otherwise, continue with existing Symbol.
			 * - else create a new Symbol in the CURRENT scope.
			 */
			else {
				Instruction value = declaratorVisitor.getInitializer();
				
				if (gecosModel.getGecosCoreContextedFactory().findAliasTypeInCurrentScope(name) != null)
					throw new CDTParserErrorException(declarator, "redefinition of '"+name+"' as different kind of symbol");
				
				Symbol existingSymInCurrentScope = gecosModel.getGecosCoreContextedFactory().findSymbolInCurrentScope(name);
				
				// check for errors
				if(existingSymInCurrentScope != null) {
					if(!gecosModel.getGecosCoreContextedFactory().isInGlobalScope(existingSymInCurrentScope))
						throw new CDTParserErrorException(declarator, "redefinition of '" + name + "'");
					else {
						if(existingSymInCurrentScope.getValue() != null && value != null)
							throw new CDTParserErrorException(declarator, "redefinition of '" + name + "'");
//						if(!existingSymbol.getType().isSimilar(type, type.isExtern(), false, false))
						if(!existingSymInCurrentScope.getType().isSimilar(type, true, true, true)) //XXX
							throw new CDTParserErrorException(declarator, "redefinition of '" + name + "' with a different type: '" + type + "' vs '" + existingSymInCurrentScope.getType() + "'");
					}
				}
				
				try {
					if(existingSymInCurrentScope != null) {
						symbol = existingSymInCurrentScope;
						if(symbol.getValue() == null && value != null)
							symbol.setValue(value);
//						if(!symbol.getType().isSimilar(type, false, false, false)) {
							symbol.setType(type);
//						}
						gecosModel.removeIgnore(symbol);
					} else {
						symbol = gecosModel.getGecosCoreContextedFactory().addSymbolToCurrentScope(name, type, value);
					}
					
					if (gecosModel.visitingInclude() && symbol != null) {
						gecosModel.ignore(symbol);
						GecosUserAnnotationFactory.pragma(symbol,GecosUserAnnotationFactory.HEADER_SYMBOL_ANNOTATION);
					}
					GecosUserAnnotationFactory.pragma(symbol, pragmaAnnotations);
				}catch (CDTParserProblemException e) {
					throw new CDTParserProblemException(declarator, e.getMessage());
				}
			}
		}
	}
	
	private void buildIASTProblemDeclaration(IASTProblemDeclaration declaration) {
		// XXX : as of 15 Oct. 2014, CDT considers .h files as C++ headers, thus 
		// when trying to define a "bool" type, it fails. This hack overcomes
		// this issue.
		if (declaration.getRawSignature().equals("typedef unsigned char bool;")) {
			Type t = GecosUserTypeFactory.ALIAS(GecosUserTypeFactory.UCHAR(), "bool");
			if (gecosModel.visitingInclude()) {
				gecosModel.ignore(t);
			}
			return;
		}
		// throw a parser problem when CDT wasn't able to parse a declartion.
		throw new CDTParserErrorException(declaration, "\""+ declaration.getRawSignature() + "\" : " + declaration.getProblem().getMessage());
	}

	private void buildIASTASMDeclaration(IASTASMDeclaration declaration) {
		notYetSupported(declaration);
	}
	
	private void buildICPPASTExplicitTemplateInstantiation(ICPPASTExplicitTemplateInstantiation declaration) {
		notYetSupported(declaration);
	}

	private void buildICPPASTLinkageSpecification(ICPPASTLinkageSpecification declaration) {
		notYetSupported(declaration);
	}

	private void buildICPPASTNamespaceAlias(ICPPASTNamespaceAlias declaration) {
		notYetSupported(declaration);
	}

	private void buildICPPASTNamespaceDefinition(ICPPASTNamespaceDefinition declaration) {
		notYetSupported(declaration);
	}

	private void buildICPPASTTemplateDeclaration(ICPPASTTemplateDeclaration declaration) {
		notYetSupported(declaration);
	}

	private void buildICPPASTTemplateSpecialization(ICPPASTTemplateSpecialization declaration) {
		notYetSupported(declaration);
	}

	private void buildICPPASTUsingDeclaration(ICPPASTUsingDeclaration declaration) {
		notYetSupported(declaration);
	}

	private void buildICPPASTUsingDirective(ICPPASTUsingDirective declaration) {
		notYetSupported(declaration);
	}
	
	private static void notYetSupported(IASTDeclaration declaration) {
//		CDTParserProblemException exception = new CDTParserProblemException(declaration, "Support for "+declaration.getClass().getSimpleName()+ ":" +declaration+ " is not yet implemented ");
//		if(CDTFrontEnd.ignoreUnsupported()) {
//			String warningMsg =  "** WARNING: Error being ignored! This may result into incorrect internal representation! **\n";
//			System.err.println(warningMsg + exception.getMessage() + "\n" + exception.getStackTrace()[0]);
//		}
//		else 
//			throw exception;
		
		throw new CDTParserNotYetSupportedException(declaration, "Support for "+declaration.getClass().getSimpleName()+ ":" +declaration+ " is not yet implemented ");
	}
	
}