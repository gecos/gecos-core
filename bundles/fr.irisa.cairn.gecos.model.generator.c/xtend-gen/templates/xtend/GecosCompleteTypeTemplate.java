package templates.xtend;

import com.google.common.base.Objects;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableCompleteTypeCGenerator;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableInstructionCGenerator;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableTypeCGenerator;
import fr.irisa.cairn.gecos.model.extensions.generators.IGecosCodeGenerator;
import gecos.instrs.Instruction;
import gecos.types.ACChannelType;
import gecos.types.ACComplexType;
import gecos.types.ACFixedType;
import gecos.types.ACFloatType;
import gecos.types.ACIntType;
import gecos.types.AliasType;
import gecos.types.ArrayType;
import gecos.types.BoolType;
import gecos.types.EnumType;
import gecos.types.Enumerator;
import gecos.types.Field;
import gecos.types.FloatType;
import gecos.types.FunctionType;
import gecos.types.IntegerType;
import gecos.types.Kinds;
import gecos.types.PtrType;
import gecos.types.RecordType;
import gecos.types.VoidType;
import java.util.Arrays;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import templates.xtend.GecosTypeTemplate;

@SuppressWarnings("all")
public class GecosCompleteTypeTemplate extends GecosTypeTemplate {
  @Override
  protected Object _generate(final AliasType aliasType) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer generated = new StringBuffer();
      String _generate = ExtendableTypeCGenerator.eInstance.generate(aliasType.getAlias());
      String _plus = (_generate + " ");
      String _name = aliasType.getName();
      String _plus_1 = (_plus + _name);
      generated.append(_plus_1);
      generated.append(IGecosCodeGenerator.afterSymbolInformation);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("typedef ");
      String _string = generated.toString();
      _builder.append(_string);
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  @Override
  protected Object _generate(final ArrayType arrayType) {
    CharSequence _xblockexpression = null;
    {
      final String generated = ExtendableCompleteTypeCGenerator.eInstance.generate(arrayType.getBase());
      final String value = ExtendableInstructionCGenerator.eInstance.generate(arrayType.getSizeExpr());
      boolean _equals = Objects.equal(value, Integer.valueOf(0));
      if (_equals) {
        IGecosCodeGenerator.afterSymbolInformation.append("[]");
      } else {
        IGecosCodeGenerator.afterSymbolInformation.append((("[" + value) + "]"));
      }
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(generated);
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  @Override
  protected Object _generate(final Enumerator enumerator) {
    CharSequence _xifexpression = null;
    Instruction _expression = enumerator.getExpression();
    boolean _tripleEquals = (_expression == null);
    if (_tripleEquals) {
      StringConcatenation _builder = new StringConcatenation();
      String _name = enumerator.getName();
      _builder.append(_name);
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      String _name_1 = enumerator.getName();
      _builder_1.append(_name_1);
      _builder_1.append(" = ");
      String _generate = ExtendableInstructionCGenerator.eInstance.generate(enumerator.getExpression());
      _builder_1.append(_generate);
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  @Override
  protected Object _generate(final EnumType enumType) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("enum ");
      String _name = enumType.getName();
      _builder.append(_name);
      _builder.append(" {");
      _builder.newLineIfNotEmpty();
      buffer.append(_builder);
      EList<Enumerator> _enumerators = enumType.getEnumerators();
      for (final Enumerator enumerator : _enumerators) {
        EList<Enumerator> _enumerators_1 = enumType.getEnumerators();
        int _size = enumType.getEnumerators().size();
        int _minus = (_size - 1);
        boolean _equals = enumerator.equals(_enumerators_1.get(_minus));
        if (_equals) {
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append("\t");
          String _generate = ExtendableCompleteTypeCGenerator.eInstance.generate(enumerator);
          _builder_1.append(_generate, "\t");
          _builder_1.newLineIfNotEmpty();
          buffer.append(_builder_1);
        } else {
          StringConcatenation _builder_2 = new StringConcatenation();
          _builder_2.append("\t");
          String _generate_1 = ExtendableCompleteTypeCGenerator.eInstance.generate(enumerator);
          _builder_2.append(_generate_1, "\t");
          _builder_2.append(",");
          _builder_2.newLineIfNotEmpty();
          buffer.append(_builder_2);
        }
      }
      StringConcatenation _builder_3 = new StringConcatenation();
      _builder_3.append("}");
      buffer.append(_builder_3);
      StringConcatenation _builder_4 = new StringConcatenation();
      _builder_4.append(buffer);
      _xblockexpression = _builder_4;
    }
    return _xblockexpression;
  }
  
  @Override
  protected Object _generate(final Field field) {
    CharSequence _xblockexpression = null;
    {
      StringBuffer buffer = new StringBuffer();
      long _bitwidth = field.getBitwidth();
      boolean _notEquals = (_bitwidth != (-1));
      if (_notEquals) {
        long _bitwidth_1 = field.getBitwidth();
        String _plus = (" : " + Long.valueOf(_bitwidth_1));
        buffer.append(_plus);
      }
      StringConcatenation _builder = new StringConcatenation();
      String _generate = ExtendableTypeCGenerator.eInstance.generate(field.getType());
      _builder.append(_generate);
      _builder.append(" ");
      String _name = field.getName();
      _builder.append(_name);
      String _string = IGecosCodeGenerator.afterSymbolInformation.toString();
      _builder.append(_string);
      _builder.append(buffer);
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  @Override
  protected Object _generate(final FunctionType functionType) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      buffer.append(IGecosCodeGenerator.afterSymbolInformation);
      buffer.append("(");
      int i = 0;
      while ((i < (functionType.listParameters().size() - 1))) {
        {
          IGecosCodeGenerator.afterSymbolInformation.setLength(0);
          StringConcatenation _builder = new StringConcatenation();
          String _generate = ExtendableCompleteTypeCGenerator.eInstance.generate(functionType.listParameters().get(i));
          _builder.append(_generate);
          _builder.append(", ");
          buffer.append(_builder);
          buffer.append(IGecosCodeGenerator.afterSymbolInformation);
          i = (i + 1);
        }
      }
      int _size = functionType.listParameters().size();
      boolean _lessThan = (i < _size);
      if (_lessThan) {
        IGecosCodeGenerator.afterSymbolInformation.setLength(0);
        StringConcatenation _builder = new StringConcatenation();
        String _generate = ExtendableCompleteTypeCGenerator.eInstance.generate(functionType.listParameters().get(i));
        _builder.append(_generate);
        buffer.append(_builder);
        buffer.append(IGecosCodeGenerator.afterSymbolInformation);
      }
      buffer.append(")");
      IGecosCodeGenerator.afterSymbolInformation.setLength(0);
      IGecosCodeGenerator.afterSymbolInformation.append(buffer);
      StringConcatenation _builder_1 = new StringConcatenation();
      String _generate_1 = ExtendableCompleteTypeCGenerator.eInstance.generate(functionType.getReturnType());
      _builder_1.append(_generate_1);
      _xblockexpression = _builder_1;
    }
    return _xblockexpression;
  }
  
  @Override
  protected Object _generate(final RecordType recordType) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      Kinds _kind = recordType.getKind();
      boolean _equals = Objects.equal(_kind, Kinds.UNION);
      if (_equals) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("union ");
        buffer.append(_builder);
      } else {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("struct ");
        buffer.append(_builder_1);
      }
      StringConcatenation _builder_2 = new StringConcatenation();
      String _name = recordType.getName();
      _builder_2.append(_name);
      _builder_2.append(" {");
      _builder_2.newLineIfNotEmpty();
      buffer.append(_builder_2);
      EList<Field> _fields = recordType.getFields();
      for (final Field field : _fields) {
        {
          IGecosCodeGenerator.afterSymbolInformation.setLength(0);
          StringConcatenation _builder_3 = new StringConcatenation();
          _builder_3.append("\t");
          String _generate = ExtendableCompleteTypeCGenerator.eInstance.generate(field);
          _builder_3.append(_generate, "\t");
          _builder_3.append(";");
          _builder_3.newLineIfNotEmpty();
          buffer.append(_builder_3);
          IGecosCodeGenerator.afterSymbolInformation.setLength(0);
        }
      }
      StringConcatenation _builder_3 = new StringConcatenation();
      _builder_3.append("}");
      buffer.append(_builder_3);
      StringConcatenation _builder_4 = new StringConcatenation();
      _builder_4.append(buffer);
      _xblockexpression = _builder_4;
    }
    return _xblockexpression;
  }
  
  public Object generate(final EObject aliasType) {
    if (aliasType instanceof ACChannelType) {
      return _generate((ACChannelType)aliasType);
    } else if (aliasType instanceof ACComplexType) {
      return _generate((ACComplexType)aliasType);
    } else if (aliasType instanceof ACFixedType) {
      return _generate((ACFixedType)aliasType);
    } else if (aliasType instanceof ACFloatType) {
      return _generate((ACFloatType)aliasType);
    } else if (aliasType instanceof ACIntType) {
      return _generate((ACIntType)aliasType);
    } else if (aliasType instanceof BoolType) {
      return _generate((BoolType)aliasType);
    } else if (aliasType instanceof FloatType) {
      return _generate((FloatType)aliasType);
    } else if (aliasType instanceof IntegerType) {
      return _generate((IntegerType)aliasType);
    } else if (aliasType instanceof VoidType) {
      return _generate((VoidType)aliasType);
    } else if (aliasType instanceof AliasType) {
      return _generate((AliasType)aliasType);
    } else if (aliasType instanceof ArrayType) {
      return _generate((ArrayType)aliasType);
    } else if (aliasType instanceof EnumType) {
      return _generate((EnumType)aliasType);
    } else if (aliasType instanceof FunctionType) {
      return _generate((FunctionType)aliasType);
    } else if (aliasType instanceof PtrType) {
      return _generate((PtrType)aliasType);
    } else if (aliasType instanceof RecordType) {
      return _generate((RecordType)aliasType);
    } else if (aliasType instanceof Enumerator) {
      return _generate((Enumerator)aliasType);
    } else if (aliasType instanceof Field) {
      return _generate((Field)aliasType);
    } else if (aliasType != null) {
      return _generate(aliasType);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(aliasType).toString());
    }
  }
}
