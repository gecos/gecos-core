/**
 */
package gecos.dag;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.DAGPort#getIdent <em>Ident</em>}</li>
 * </ul>
 *
 * @see gecos.dag.DagPackage#getDAGPort()
 * @model abstract="true"
 * @generated
 */
public interface DAGPort extends DagVisitable {
	/**
	 * Returns the value of the '<em><b>Ident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ident</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ident</em>' attribute.
	 * @see #setIdent(int)
	 * @see gecos.dag.DagPackage#getDAGPort_Ident()
	 * @model unique="false" dataType="gecos.dag.int"
	 * @generated
	 */
	int getIdent();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGPort#getIdent <em>Ident</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ident</em>' attribute.
	 * @see #getIdent()
	 * @generated
	 */
	void setIdent(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _isSetParent = this.isSetParent();\nif (_isSetParent)\n{\n\t&lt;%org.eclipse.emf.ecore.EObject%&gt; _eContainer = this.eContainer();\n\treturn ((&lt;%gecos.dag.DAGNode%&gt;) _eContainer);\n}\nelse\n{\n\treturn null;\n}'"
	 * @generated
	 */
	DAGNode getParent();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return ((this.eContainer() != null) &amp;&amp; (this.eContainer() instanceof &lt;%gecos.dag.DAGNode%&gt;));'"
	 * @generated
	 */
	boolean isSetParent();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 * @generated
	 */
	void accept(DagVisitor visitor);

} // DAGPort
