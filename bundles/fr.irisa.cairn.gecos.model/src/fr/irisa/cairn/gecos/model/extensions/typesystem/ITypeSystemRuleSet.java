package fr.irisa.cairn.gecos.model.extensions.typesystem;

import gecos.instrs.Instruction;
import gecos.types.Type;

public interface ITypeSystemRuleSet {
	
	public Type computeType(Instruction o);

}