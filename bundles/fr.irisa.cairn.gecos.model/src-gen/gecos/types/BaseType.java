/**
 */
package gecos.types;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.BaseType#getSize <em>Size</em>}</li>
 * </ul>
 *
 * @see gecos.types.TypesPackage#getBaseType()
 * @model abstract="true"
 * @generated
 */
public interface BaseType extends ScalarType, TypesVisitable {
	/**
	 * Returns the value of the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Size</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Size</em>' attribute.
	 * @see #setSize(long)
	 * @see gecos.types.TypesPackage#getBaseType_Size()
	 * @model unique="false" dataType="gecos.types.long"
	 * @generated
	 */
	long getSize();

	/**
	 * Sets the value of the '{@link gecos.types.BaseType#getSize <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Size</em>' attribute.
	 * @see #getSize()
	 * @generated
	 */
	void setSize(long value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 * @generated
	 */
	void accept(TypesVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((this instanceof &lt;%gecos.types.IntegerType%&gt;))\n{\n\treturn ((&lt;%gecos.types.IntegerType%&gt;) this);\n}\nreturn null;'"
	 * @generated
	 */
	IntegerType asInt();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((this instanceof &lt;%gecos.types.FloatType%&gt;))\n{\n\treturn ((&lt;%gecos.types.FloatType%&gt;) this);\n}\nreturn null;'"
	 * @generated
	 */
	FloatType asFloat();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((this instanceof &lt;%gecos.types.BoolType%&gt;))\n{\n\treturn ((&lt;%gecos.types.BoolType%&gt;) this);\n}\nreturn null;'"
	 * @generated
	 */
	BoolType asBool();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((this instanceof &lt;%gecos.types.VoidType%&gt;))\n{\n\treturn ((&lt;%gecos.types.VoidType%&gt;) this);\n}\nreturn null;'"
	 * @generated
	 */
	VoidType asVoid();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return super.toString();'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.boolean" unique="false" otherUnique="false" ignoreStorageClassDataType="gecos.types.boolean" ignoreStorageClassUnique="false" ignoreConstDataType="gecos.types.boolean" ignoreConstUnique="false" ignoreVolatileDataType="gecos.types.boolean" ignoreVolatileUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((this == other))\n{\n\treturn true;\n}\nif ((other instanceof &lt;%gecos.types.BaseType%&gt;))\n{\n\tfinal &lt;%gecos.types.BaseType%&gt; base = ((&lt;%gecos.types.BaseType%&gt;)other);\n\treturn ((base.getSize() == this.getSize()) &amp;&amp; super.isEqual(other, ignoreStorageClass, ignoreConst, ignoreVolatile));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isEqual(Type other, boolean ignoreStorageClass, boolean ignoreConst, boolean ignoreVolatile);

} // BaseType
