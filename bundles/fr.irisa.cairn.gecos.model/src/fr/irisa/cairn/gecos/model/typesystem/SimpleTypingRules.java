package fr.irisa.cairn.gecos.model.typesystem;

import java.util.Arrays;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.extensions.typesystem.ITypeSystemRuleSet;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import gecos.blocks.Block;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.InstrsPackage;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.PhiInstruction;
import gecos.instrs.ReductionOperator;
import gecos.instrs.RetInstruction;
import gecos.instrs.SSADefSymbol;
import gecos.instrs.SSAUseSymbol;
import gecos.instrs.SelectOperator;
import gecos.instrs.SetInstruction;
import gecos.instrs.StringInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.ArrayType;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.FloatType;
import gecos.types.IntegerType;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.PtrType;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import gecos.types.TypesPackage;

@SuppressWarnings("all")
public class SimpleTypingRules implements ITypeSystemRuleSet {


private static boolean tom_equal_term_Strategy(Object t1, Object t2) {
return  (t1.equals(t2)) ;
}
private static boolean tom_is_sort_Strategy(Object t) {
return  (t instanceof tom.library.sl.Strategy) ;
}
private static boolean tom_equal_term_Position(Object t1, Object t2) {
return  (t1.equals(t2)) ;
}
private static boolean tom_is_sort_Position(Object t) {
return  (t instanceof tom.library.sl.Position) ;
}
private static boolean tom_equal_term_int(int t1, int t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_int(int t) {
return  true ;
}
private static boolean tom_equal_term_char(char t1, char t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_char(char t) {
return  true ;
}
private static boolean tom_equal_term_String(String t1, String t2) {
return  t1.equals(t2) ;
}
private static boolean tom_is_sort_String(String t) {
return  t instanceof String ;
}
private static  tom.library.sl.Strategy  tom_make_mu( tom.library.sl.Strategy  var,  tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.Mu(var,v) );
}
private static  tom.library.sl.Strategy  tom_make_MuVar( String  name) { 
return ( new tom.library.sl.MuVar(name) );
}
private static  tom.library.sl.Strategy  tom_make_Identity() { 
return ( new tom.library.sl.Identity() );
}
private static  tom.library.sl.Strategy  tom_make_One( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.One(v) );
}
private static  tom.library.sl.Strategy  tom_make_All( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.All(v) );
}
private static  tom.library.sl.Strategy  tom_make_Fail() { 
return ( new tom.library.sl.Fail() );
}
private static boolean tom_is_fun_sym_Sequence( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.Sequence );
}
private static  tom.library.sl.Strategy  tom_empty_list_Sequence() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_Sequence( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.Sequence.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Sequence.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Sequence.THEN) );
}
private static boolean tom_is_empty_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( t == null );
}

  private static   tom.library.sl.Strategy  tom_append_list_Sequence( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 == null )) {
      return l2;
    } else if(( l2 == null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.Sequence )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.THEN) ) == null )) {
        return  tom.library.sl.Sequence.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.Sequence.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.FIRST) ),tom_append_list_Sequence(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.Sequence.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_Sequence( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end == null ) ||  (end.equals(tom_empty_list_Sequence())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.Sequence.make(((( begin instanceof tom.library.sl.Sequence ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Sequence.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_Sequence(((( begin instanceof tom.library.sl.Sequence ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Sequence.THEN) ):tom_empty_list_Sequence()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_Choice( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.Choice );
}
private static  tom.library.sl.Strategy  tom_empty_list_Choice() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_Choice( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.Choice.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Choice.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Choice.THEN) );
}
private static boolean tom_is_empty_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( t ==null );
}

  private static   tom.library.sl.Strategy  tom_append_list_Choice( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 ==null )) {
      return l2;
    } else if(( l2 ==null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.Choice )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.THEN) ) ==null )) {
        return  tom.library.sl.Choice.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.Choice.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.FIRST) ),tom_append_list_Choice(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.Choice.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_Choice( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end ==null ) ||  (end.equals(tom_empty_list_Choice())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.Choice.make(((( begin instanceof tom.library.sl.Choice ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Choice.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_Choice(((( begin instanceof tom.library.sl.Choice ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Choice.THEN) ):tom_empty_list_Choice()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_SequenceId( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.SequenceId );
}
private static  tom.library.sl.Strategy  tom_empty_list_SequenceId() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_SequenceId( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.SequenceId.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.SequenceId.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.SequenceId.THEN) );
}
private static boolean tom_is_empty_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( t == null );
}

  private static   tom.library.sl.Strategy  tom_append_list_SequenceId( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 == null )) {
      return l2;
    } else if(( l2 == null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.SequenceId )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.THEN) ) == null )) {
        return  tom.library.sl.SequenceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.SequenceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.FIRST) ),tom_append_list_SequenceId(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.SequenceId.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_SequenceId( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end == null ) ||  (end.equals(tom_empty_list_SequenceId())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.SequenceId.make(((( begin instanceof tom.library.sl.SequenceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.SequenceId.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_SequenceId(((( begin instanceof tom.library.sl.SequenceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.SequenceId.THEN) ):tom_empty_list_SequenceId()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_ChoiceId( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.ChoiceId );
}
private static  tom.library.sl.Strategy  tom_empty_list_ChoiceId() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_ChoiceId( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.ChoiceId.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.ChoiceId.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.ChoiceId.THEN) );
}
private static boolean tom_is_empty_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( t ==null );
}

  private static   tom.library.sl.Strategy  tom_append_list_ChoiceId( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 ==null )) {
      return l2;
    } else if(( l2 ==null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.ChoiceId )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.THEN) ) ==null )) {
        return  tom.library.sl.ChoiceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.ChoiceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.FIRST) ),tom_append_list_ChoiceId(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.ChoiceId.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_ChoiceId( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end ==null ) ||  (end.equals(tom_empty_list_ChoiceId())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.ChoiceId.make(((( begin instanceof tom.library.sl.ChoiceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.ChoiceId.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_ChoiceId(((( begin instanceof tom.library.sl.ChoiceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.ChoiceId.THEN) ):tom_empty_list_ChoiceId()),end,tail)) ;
  }
  private static  tom.library.sl.Strategy  tom_make_OneId( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.OneId(v) );
}
private static  tom.library.sl.Strategy  tom_make_AllSeq( tom.library.sl.Strategy  s) { 
return ( new tom.library.sl.AllSeq(s) );
}
private static  tom.library.sl.Strategy  tom_make_AUCtl( tom.library.sl.Strategy  s1,  tom.library.sl.Strategy  s2) { 
return ( 
tom_make_mu(tom_make_MuVar("x"),tom_cons_list_Choice(s2,tom_cons_list_Choice(tom_cons_list_Sequence(tom_cons_list_Sequence(s1,tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("x")),tom_empty_list_Sequence())),tom_cons_list_Sequence(tom_make_One(tom_make_Identity()),tom_empty_list_Sequence())),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_EUCtl( tom.library.sl.Strategy  s1,  tom.library.sl.Strategy  s2) { 
return ( 
tom_make_mu(tom_make_MuVar("x"),tom_cons_list_Choice(s2,tom_cons_list_Choice(tom_cons_list_Sequence(s1,tom_cons_list_Sequence(tom_make_One(tom_make_MuVar("x")),tom_empty_list_Sequence())),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_Try( tom.library.sl.Strategy  s) { 
return ( 
tom_cons_list_Choice(s,tom_cons_list_Choice(tom_make_Identity(),tom_empty_list_Choice())))
;
}
private static  tom.library.sl.Strategy  tom_make_Repeat( tom.library.sl.Strategy  s) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Choice(tom_cons_list_Sequence(s,tom_cons_list_Sequence(tom_make_MuVar("_x"),tom_empty_list_Sequence())),tom_cons_list_Choice(tom_make_Identity(),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_TopDown( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Sequence(v,tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("_x")),tom_empty_list_Sequence()))))
;
}
private static  tom.library.sl.Strategy  tom_make_OnceTopDown( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Choice(v,tom_cons_list_Choice(tom_make_One(tom_make_MuVar("_x")),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_RepeatId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_SequenceId(v,tom_cons_list_SequenceId(tom_make_MuVar("_x"),tom_empty_list_SequenceId()))))
;
}
private static  tom.library.sl.Strategy  tom_make_OnceTopDownId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_ChoiceId(v,tom_cons_list_ChoiceId(tom_make_OneId(tom_make_MuVar("_x")),tom_empty_list_ChoiceId()))))
;
}
private static boolean tom_equal_term_long(long t1, long t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_long(long t) {
return  true ;
}
private static boolean tom_equal_term_double(double t1, double t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_double(double t) {
return  true ;
}
private static boolean tom_equal_term_boolean(boolean t1, boolean t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_boolean(boolean t) {
return  true ;
}
private static boolean tom_equal_term_float(float t1, float t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_float(float t) {
return  true ;
}


private static <O> EList<O> enforce(EList l) {
return l;
}

private static <O> EList<O> append(O e,EList<O> l) {
l.add(e);
return l;
}
private static boolean tom_equal_term_EELong(Object l1, Object l2) {
return l1.equals(l2);
}
private static boolean tom_is_sort_EELong(Object t) {
return t instanceof java.lang.Long;
}
private static boolean tom_equal_term_BlockCopyManager(Object l1, Object l2) {
return l1.equals(l2);
}
private static boolean tom_is_sort_BlockCopyManager(Object t) {
return t instanceof fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;
}
private static boolean tom_equal_term_DependencyType(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_DependencyType(Object t) {
return t instanceof DependencyType;
}
private static boolean tom_equal_term_DAGOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_DAGOperator(Object t) {
return t instanceof DAGOperator;
}
private static boolean tom_equal_term_ArithmeticOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ArithmeticOperator(Object t) {
return t instanceof ArithmeticOperator;
}
private static boolean tom_equal_term_ComparisonOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ComparisonOperator(Object t) {
return t instanceof ComparisonOperator;
}
private static boolean tom_equal_term_LogicalOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_LogicalOperator(Object t) {
return t instanceof LogicalOperator;
}
private static boolean tom_equal_term_BitwiseOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_BitwiseOperator(Object t) {
return t instanceof BitwiseOperator;
}
private static boolean tom_equal_term_SelectOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_SelectOperator(Object t) {
return t instanceof SelectOperator;
}
private static boolean tom_equal_term_ReductionOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ReductionOperator(Object t) {
return t instanceof ReductionOperator;
}
private static boolean tom_equal_term_BranchType(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_BranchType(Object t) {
return t instanceof BranchType;
}
private static boolean tom_equal_term_StorageClassSpecifiers(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_StorageClassSpecifiers(Object t) {
return t instanceof StorageClassSpecifiers;
}
private static boolean tom_equal_term_Kinds(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_Kinds(Object t) {
return t instanceof Kinds;
}
private static boolean tom_equal_term_IntegerTypes(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_IntegerTypes(Object t) {
return t instanceof IntegerTypes;
}
private static boolean tom_equal_term_SignModifiers(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_SignModifiers(Object t) {
return t instanceof SignModifiers;
}
private static boolean tom_equal_term_FloatPrecisions(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_FloatPrecisions(Object t) {
return t instanceof FloatPrecisions;
}
private static boolean tom_equal_term_OverflowMode(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_OverflowMode(Object t) {
return t instanceof OverflowMode;
}
private static boolean tom_equal_term_QuantificationMode(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_QuantificationMode(Object t) {
return t instanceof QuantificationMode;
}
private static boolean tom_equal_term_Inst(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Inst(Object t) {
return t instanceof Instruction;
}
private static boolean tom_equal_term_Blk(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Blk(Object t) {
return t instanceof Block;
}
private static boolean tom_equal_term_Sym(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Sym(Object t) {
return t instanceof Symbol;
}
private static boolean tom_equal_term_SymL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_SymL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Symbol>)t).size() == 0 
    	|| (((EList<Symbol>)t).size()>0 && ((EList<Symbol>)t).get(0) instanceof Symbol));
}
private static boolean tom_equal_term_Type(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Type(Object t) {
return t instanceof Type;
}
private static boolean tom_equal_term_TypeL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_TypeL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Type>)t).size() == 0 
    	|| (((EList<Type>)t).size()>0 && ((EList<Type>)t).get(0) instanceof Type));
}
private static boolean tom_equal_term_Field(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Field(Object t) {
return t instanceof Field;
}
private static boolean tom_equal_term_FieldL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_FieldL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Field>)t).size() == 0 
    	|| (((EList<Field>)t).size()>0 && ((EList<Field>)t).get(0) instanceof Field));
}
private static boolean tom_equal_term_InstL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_InstL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Instruction>)t).size() == 0 
    	|| (((EList<Instruction>)t).size()>0 && ((EList<Instruction>)t).get(0) instanceof Instruction));
}
private static boolean tom_is_fun_sym_InstL( EList<Instruction>  t) {
return  t instanceof EList<?> &&
 		(((EList<Instruction>)t).size() == 0 
 		|| (((EList<Instruction>)t).size()>0 && ((EList<Instruction>)t).get(0) instanceof Instruction));
}
private static  EList<Instruction>  tom_empty_array_InstL(int n) { 
return  new BasicEList<Instruction>(n) ;
}
private static  EList<Instruction>  tom_cons_array_InstL(Instruction e,  EList<Instruction>  l) { 
return  append(e,l) ;
}
private static Instruction tom_get_element_InstL_InstL( EList<Instruction>  l, int n) {
return  l.get(n) ;
}
private static int tom_get_size_InstL_InstL( EList<Instruction>  l) {
return  l.size() ;
}

  private static   EList<Instruction>  tom_get_slice_InstL( EList<Instruction>  subject, int begin, int end) {
     EList<Instruction>  result =  new BasicEList<Instruction>(end-begin) ;
    while(begin!=end) {
      result =  append( subject.get(begin) ,result) ;
      begin++;
    }
    return result;
  }

  private static   EList<Instruction>  tom_append_array_InstL( EList<Instruction>  l2,  EList<Instruction>  l1) {
    int size1 =  l1.size() ;
    int size2 =  l2.size() ;
    int index;
     EList<Instruction>  result =  new BasicEList<Instruction>(size1+size2) ;
    index=size1;
    while(index >0) {
      result =  append( l1.get(size1-index) ,result) ;
      index--;
    }

    index=size2;
    while(index > 0) {
      result =  append( l2.get(size2-index) ,result) ;
      index--;
    }
    return result;
  }private static boolean tom_equal_term_BlkL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_BlkL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Block>)t).size() == 0 
    	|| (((EList<Block>)t).size()>0 && ((EList<Block>)t).get(0) instanceof Block));
}
private static boolean tom_equal_term_Node(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Node(Object t) {
return t instanceof DAGNode;
}
private static boolean tom_equal_term_NodeL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_NodeL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<DAGNode>)t).size() == 0 
    	|| (((EList<DAGNode>)t).size()>0 && ((EList<DAGNode>)t).get(0) instanceof DAGNode));
}
private static boolean tom_is_fun_sym_symref(Instruction t) {
return t instanceof SymbolInstruction;
}
private static Symbol tom_get_slot_symref_symbol(Instruction t) {
return ((SymbolInstruction)t).getSymbol();
}
private static boolean tom_is_fun_sym_ival(Instruction t) {
return t instanceof IntInstruction;
}
private static  long  tom_get_slot_ival_value(Instruction t) {
return ((IntInstruction)t).getValue();
}
private static boolean tom_is_fun_sym_fval(Instruction t) {
return t instanceof FloatInstruction;
}
private static  double  tom_get_slot_fval_value(Instruction t) {
return ((FloatInstruction)t).getValue();
}
private static boolean tom_is_fun_sym_strval(Instruction t) {
return t instanceof StringInstruction;
}
private static  String  tom_get_slot_strval_value(Instruction t) {
return ((StringInstruction)t).getValue();
}
private static boolean tom_is_fun_sym_generic(Instruction t) {
return t instanceof GenericInstruction;
}
private static  String  tom_get_slot_generic_name(Instruction t) {
return ((GenericInstruction)t).getName();
}
private static  EList<Instruction>  tom_get_slot_generic_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_set(Instruction t) {
return t instanceof SetInstruction;
}
private static Instruction tom_get_slot_set_dest(Instruction t) {
return ((SetInstruction)t).getDest();
}
private static Instruction tom_get_slot_set_source(Instruction t) {
return ((SetInstruction)t).getSource();
}
private static boolean tom_is_fun_sym_array(Instruction t) {
return t instanceof ArrayInstruction;
}
private static Instruction tom_get_slot_array_dest(Instruction t) {
return ((ArrayInstruction)t).getDest();
}
private static  EList<Instruction>  tom_get_slot_array_index(Instruction t) {
return enforce(((ArrayInstruction)t).getIndex());
}
private static boolean tom_is_fun_sym_address(Instruction t) {
return t instanceof AddressInstruction;
}
private static Instruction tom_get_slot_address_address(Instruction t) {
return ((AddressInstruction)t).getAddress();
}
private static boolean tom_is_fun_sym_indir(Instruction t) {
return t instanceof IndirInstruction;
}
private static Instruction tom_get_slot_indir_address(Instruction t) {
return ((IndirInstruction)t).getAddress();
}
private static boolean tom_is_fun_sym_ret(Instruction t) {
return t instanceof RetInstruction;
}
private static Instruction tom_get_slot_ret_expr(Instruction t) {
return ((RetInstruction)t).getExpr();
}
private static boolean tom_is_fun_sym_phi(Instruction t) {
return t instanceof PhiInstruction;
}
private static  EList<Instruction>  tom_get_slot_phi_children(Instruction t) {
return enforce(((PhiInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_ssaDef(Instruction t) {
return t instanceof SSADefSymbol;
}
private static Symbol tom_get_slot_ssaDef_symbol(Instruction t) {
return ((SSADefSymbol)t).getSymbol();
}
private static  int  tom_get_slot_ssaDef_number(Instruction t) {
return ((SSADefSymbol)t).getNumber();
}
private static boolean tom_is_fun_sym_ssaUse(Instruction t) {
return t instanceof SSAUseSymbol;
}
private static Symbol tom_get_slot_ssaUse_symbol(Instruction t) {
return ((SSAUseSymbol)t).getSymbol();
}
private static  int  tom_get_slot_ssaUse_number(Instruction t) {
return ((SSAUseSymbol)t).getNumber();
}

private static boolean tom_is_fun_sym_floatType(Type t) {
return t instanceof FloatType;
}
private static FloatPrecisions tom_get_slot_floatType_precision(Type t) {
return ((FloatType)t).getPrecision();
}
private static boolean tom_is_fun_sym_intType(Type t) {
return t instanceof IntegerType;
}
private static Type tom_make_intType(SignModifiers _signModifier, IntegerTypes _type) { 
return fr.irisa.cairn.gecos.model.tom.gecos.internal.GecosTomFactory.createIntType(_signModifier, _type);
}
private static SignModifiers tom_get_slot_intType_signModifier(Type t) {
return ((IntegerType)t).getSignModifier();
}
private static IntegerTypes tom_get_slot_intType_type(Type t) {
return ((IntegerType)t).getType();
}
private static boolean tom_is_fun_sym_ptrType(Type t) {
return t instanceof PtrType;
}
private static Type tom_get_slot_ptrType_base(Type t) {
return ((PtrType)t).getBase();
}
private static boolean tom_is_fun_sym_arrayType(Type t) {
return t instanceof ArrayType;
}
private static Type tom_get_slot_arrayType_base(Type t) {
return ((ArrayType)t).getBase();
}
private static  long  tom_get_slot_arrayType_lower(Type t) {
return ((ArrayType)t).getLower();
}
private static  long  tom_get_slot_arrayType_upper(Type t) {
return ((ArrayType)t).getUpper();
}
private static Instruction tom_get_slot_arrayType_sizeExpr(Type t) {
return ((ArrayType)t).getSizeExpr();
}


protected static void debug(String mess) {
if(VERBOSE) System.out.println(mess);
}

protected static void debug(int n, String mess) {  
debug("Rule "+n+" : "+mess); 
}

public static final boolean VERBOSE = false;

public SimpleTypingRules() {

}

public boolean isExternalType(Type type) {
if(type!=null)
return (type.eClass().getEPackage()!=TypesPackage.eINSTANCE) ;
else
return true;
}

public boolean usesExternalTypes(Instruction... instrs) {
return usesExternalTypes(Arrays.asList(instrs));
}

public boolean usesExternalTypes(List<Instruction> instrs) {
for(Instruction instr : instrs) {
if(isExternalType(instr.getType())) {
System.out.println("External type "+instr.getType()+" in "+instr);
return true;
}
}
return false; 
}


@Override
public Type computeType(Instruction o) {

if(o.eClass().getEPackage()!=InstrsPackage.eINSTANCE)
return null;
Type res = tomType(o);
return res;
}


public Type tomAddSubMulDivType(String n, Type t1, Type t2) {
Type res = null;

{
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_floatType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_floatType(((Type)((Type)t2)))) {

res = GecosUserTypeFactory.DOUBLE();

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_floatType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_ptrType(((Type)((Type)t2)))) {

// invalid
res = null;

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_floatType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_intType(((Type)((Type)t2)))) {

res = GecosUserTypeFactory.DOUBLE();

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_ptrType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_floatType(((Type)((Type)t2)))) {

// invalid
res = null;

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_ptrType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_ptrType(((Type)((Type)t2)))) {
// TODO : check
if (n.equals("add")) {
res = GecosUserTypeFactory.INT();
}
else {
res = null;
}

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_ptrType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_intType(((Type)((Type)t2)))) {

PtrType ptr = (PtrType) 
t1;
res = GecosUserTypeFactory.PTR(ptr.getBase());

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_intType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_floatType(((Type)((Type)t2)))) {

res = GecosUserTypeFactory.DOUBLE();

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_intType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_ptrType(((Type)((Type)t2)))) {

res = GecosUserTypeFactory.INT();

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_intType(((Type)((Type)t1)))) {
SignModifiers tom_signedA=tom_get_slot_intType_signModifier(((Type)t1));
IntegerTypes tom_sizeA=tom_get_slot_intType_type(((Type)t1));
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_intType(((Type)((Type)t2)))) {
SignModifiers tom_signedB=tom_get_slot_intType_signModifier(((Type)t2));
IntegerTypes tom_sizeB=tom_get_slot_intType_type(((Type)t2));

if ((n == "add")||(n == "sub")) {
res = 
tom_make_intType((tom_signedA== SignModifiers.SIGNED||tom_signedB==SignModifiers.SIGNED?SignModifiers.SIGNED:SignModifiers.UNSIGNED),tom_sizeA.getValue()>tom_sizeB.getValue()?tom_sizeA:tom_sizeB);
((IntegerType)res).setSize(Math.max(t1.getSize(), t2.getSize()) + 1);
}
else {
res = 
tom_make_intType((tom_signedA== SignModifiers.SIGNED||tom_signedB==SignModifiers.SIGNED?SignModifiers.SIGNED:SignModifiers.UNSIGNED),tom_sizeA.getValue()>tom_sizeB.getValue()?tom_sizeA:tom_sizeB);
((IntegerType)res).setSize(t1.getSize() + t2.getSize());
}

}
}
}
}
}
}
}
}

return res;
}

public Type tomNegType(Type t1) {
Type res = null;

{
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_floatType(((Type)((Type)t1)))) {

res = t1;

}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_intType(((Type)((Type)t1)))) {

res = t1;

}
}
}
}
}

return res;
}

public Type tomType(Instruction o) {

{
{
if (tom_is_sort_Inst(o)) {
if (tom_is_sort_Inst(((Instruction)o))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)o)))) {
 EList<Instruction>  tomMatch3_2=tom_get_slot_generic_children(((Instruction)o));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch3_2))) {
 EList<Instruction>  tom_children=tomMatch3_2;


if(usesExternalTypes(
tom_children)) {
debug(0,"Non standard types in "+(
tom_children));
return null;
} 

}
}
}
}
}
{
if (tom_is_sort_Inst(o)) {
if (tom_is_sort_Inst(((Instruction)o))) {
if (tom_is_fun_sym_set(((Instruction)((Instruction)o)))) {
Instruction tom_dst=tom_get_slot_set_dest(((Instruction)o));
Instruction tom_src=tom_get_slot_set_source(((Instruction)o));

//

if(usesExternalTypes(
tom_dst, 
tom_src)) { 				
return null;
}
// TODO : here we should check that the types of src and dst are consistent 
Type typeD = 
tom_dst.getType();
Type typeS = 
tom_src.getType();

{
{
if (tom_is_sort_Type(typeD)) {
if (tom_is_sort_Type(((Type)typeD))) {
if (tom_is_fun_sym_ptrType(((Type)((Type)typeD)))) {
if (tom_is_sort_Type(typeS)) {
if (tom_is_sort_Type(((Type)typeS))) {
if (tom_is_fun_sym_floatType(((Type)((Type)typeS)))) {

debug(20,"Set operation "+o+" invalid typing : dest pointer, src float");
return null;

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(typeD)) {
if (tom_is_sort_Type(((Type)typeD))) {
if (tom_is_fun_sym_floatType(((Type)((Type)typeD)))) {
if (tom_is_sort_Type(typeS)) {
if (tom_is_sort_Type(((Type)typeS))) {
if (tom_is_fun_sym_ptrType(((Type)((Type)typeS)))) {

debug(21,"Set operation "+o+" invalid typing : dest float, src pointer");
return null;

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(typeD)) {
if (tom_is_sort_Type(((Type)typeD))) {
if (tom_is_fun_sym_intType(((Type)((Type)typeD)))) {
if (tom_is_sort_Type(typeS)) {
if (tom_is_sort_Type(((Type)typeS))) {
if (tom_is_fun_sym_floatType(((Type)((Type)typeS)))) {

debug(22,"Set operation "+o+" is not advised : src int, dest float");

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(typeD)) {
if (tom_is_sort_Type(((Type)typeD))) {
if (tom_is_fun_sym_floatType(((Type)((Type)typeD)))) {
if (tom_is_sort_Type(typeS)) {
if (tom_is_sort_Type(((Type)typeS))) {
if (tom_is_fun_sym_intType(((Type)((Type)typeS)))) {

debug(23,"Set operation "+o+" is not advised : src float, dest int");

}
}
}
}
}
}
}
}

debug(1,"Set instruction " + o + " : dest " + typeD + " / src " + typeS);
return (
tom_dst).getType();

}
}
}
}
{
if (tom_is_sort_Inst(o)) {
if (tom_is_sort_Inst(((Instruction)o))) {
if (tom_is_fun_sym_ret(((Instruction)((Instruction)o)))) {
Instruction tom_expr=tom_get_slot_ret_expr(((Instruction)o));

if(usesExternalTypes(
tom_expr)) {				
return null;
}
debug(1,"Return instruction "+o+" typed as "+(
tom_expr).getType());
return (
tom_expr).getType();

}
}
}
}
{
if (tom_is_sort_Inst(o)) {
if (tom_is_sort_Inst(((Instruction)o))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)o)))) {
 EList<Instruction>  tomMatch3_19=tom_get_slot_generic_children(((Instruction)o));
 String  tom_name=tom_get_slot_generic_name(((Instruction)o));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch3_19))) {
int tomMatch3_23=0;
if (!(tomMatch3_23 >= tom_get_size_InstL_InstL(tomMatch3_19))) {
Instruction tom_op1=tom_get_element_InstL_InstL(tomMatch3_19,tomMatch3_23);
if (tomMatch3_23 + 1 >= tom_get_size_InstL_InstL(tomMatch3_19)) {
{
{
if ( true ) {
if ( true ) {
if (tom_equal_term_String("neg", (( String )tom_name))) {

Type t1 = 
tom_op1.getType();
Type res = tomNegType(t1);
if (res != null) {debug(2,"Unary neg "+o+" resolved to "+res);}
else {debug(3,"Unary neg "+o+" not resolved");}
return res;

}
}
}
}
{
if ( true ) {
if ( true ) {
if (tom_equal_term_String("not", (( String )tom_name))) {

debug(4,"Unary not operation "+o);
// only INT() & result in BOOL()
Type t1 = 
tom_op1.getType();

{
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_intType(((Type)((Type)t1)))) {
return GecosUserTypeFactory.BOOL(); 
}
}
}
}
}

return null;

}
}
}
}
}


}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(o)) {
if (tom_is_sort_Inst(((Instruction)o))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)o)))) {
 EList<Instruction>  tomMatch3_27=tom_get_slot_generic_children(((Instruction)o));
 String  tom_name=tom_get_slot_generic_name(((Instruction)o));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch3_27))) {
int tomMatch3_31=0;
if (!(tomMatch3_31 >= tom_get_size_InstL_InstL(tomMatch3_27))) {
Instruction tom_op1=tom_get_element_InstL_InstL(tomMatch3_27,tomMatch3_31);
int tomMatch3_32=tomMatch3_31 + 1;
if (!(tomMatch3_32 >= tom_get_size_InstL_InstL(tomMatch3_27))) {
Instruction tom_op2=tom_get_element_InstL_InstL(tomMatch3_27,tomMatch3_32);
if (tomMatch3_32 + 1 >= tom_get_size_InstL_InstL(tomMatch3_27)) {
// Binary operations

{
{
if ( true ) {
boolean tomMatch7_6= false ;
 String  tomMatch7_4= "" ;
 String  tomMatch7_5= "" ;
 String  tomMatch7_2= "" ;
 String  tomMatch7_3= "" ;
if ( true ) {
if (tom_equal_term_String("add", (( String )tom_name))) {
{
tomMatch7_6= true ;
tomMatch7_2=(( String )tom_name);
}
} else {
if ( true ) {
if (tom_equal_term_String("sub", (( String )tom_name))) {
{
tomMatch7_6= true ;
tomMatch7_3=(( String )tom_name);
}
} else {
if ( true ) {
if (tom_equal_term_String("mul", (( String )tom_name))) {
{
tomMatch7_6= true ;
tomMatch7_4=(( String )tom_name);
}
} else {
if ( true ) {
if (tom_equal_term_String("div", (( String )tom_name))) {
{
tomMatch7_6= true ;
tomMatch7_5=(( String )tom_name);
}
}
}
}
}
}
}
}
}
if (tomMatch7_6) {

String n = 
tom_name;
Type res = null;
Type t1 = 
tom_op1.getType();
Type t2 = 
tom_op2.getType();


{
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_floatType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_floatType(((Type)((Type)t2)))) {

res = GecosUserTypeFactory.DOUBLE();

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_floatType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_ptrType(((Type)((Type)t2)))) {

// invalid
debug(5,"Binary operation "+o+" is badly typed (float + pointer)");
res = null;

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_floatType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_intType(((Type)((Type)t2)))) {

res = GecosUserTypeFactory.DOUBLE();

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_ptrType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_floatType(((Type)((Type)t2)))) {

// invalid
debug(6,"Binary operation "+o+" is badly typed (pointer + float)");
res = null;

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_ptrType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_ptrType(((Type)((Type)t2)))) {

PtrType ptr = (PtrType) 
t1;
res = GecosUserTypeFactory.PTR(ptr.getBase());

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_ptrType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_intType(((Type)((Type)t2)))) {

PtrType ptr = (PtrType) 
t1;
res = GecosUserTypeFactory.PTR(ptr.getBase());

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_intType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_floatType(((Type)((Type)t2)))) {

res = GecosUserTypeFactory.DOUBLE();

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_intType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_ptrType(((Type)((Type)t2)))) {

res = GecosUserTypeFactory.INT();

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_intType(((Type)((Type)t1)))) {
IntegerTypes tom_sizeA=tom_get_slot_intType_type(((Type)t1));
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_intType(((Type)((Type)t2)))) {
IntegerTypes tom_sizeB=tom_get_slot_intType_type(((Type)t2));

IntegerTypes size = 
tom_sizeA.getValue()>
tom_sizeB.getValue()?
tom_sizeA:
tom_sizeB;
if (size == IntegerTypes.CHAR ||size == IntegerTypes.SHORT) {
res = GecosUserTypeFactory.SHORT();
}
else if (size == IntegerTypes.INT) {
res = GecosUserTypeFactory.INT();
}
else if (size == IntegerTypes.LONG) {
res = GecosUserTypeFactory.LONG();
}
else {
res = GecosUserTypeFactory.LONGLONG();
}

}
}
}
}
}
}
}
}

res = tomAddSubMulDivType(n, t1, t2);

if (res != null) {
debug(7,"Binary operation "+o+" type is "+res);
}
return res; 

}
}
}
{
if ( true ) {
boolean tomMatch7_12= false ;
 String  tomMatch7_10= "" ;
 String  tomMatch7_11= "" ;
 String  tomMatch7_9= "" ;
if ( true ) {
if (tom_equal_term_String("and", (( String )tom_name))) {
{
tomMatch7_12= true ;
tomMatch7_9=(( String )tom_name);
}
} else {
if ( true ) {
if (tom_equal_term_String("or", (( String )tom_name))) {
{
tomMatch7_12= true ;
tomMatch7_10=(( String )tom_name);
}
} else {
if ( true ) {
if (tom_equal_term_String("xor", (( String )tom_name))) {
{
tomMatch7_12= true ;
tomMatch7_11=(( String )tom_name);
}
}
}
}
}
}
}
if (tomMatch7_12) {

debug(8,"Bitwise logic operation "+o);
// only INT() & result in BOOL()
Type t1 = 
tom_op1.getType();
Type t2 = 
tom_op2.getType();

{
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_intType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_intType(((Type)((Type)t2)))) {
return GecosUserTypeFactory.BOOL();
}
}
}
}
}
}
}
}

debug(19,"Bitwise logic op "+o+" badly typed");

}
}
}
{
if ( true ) {
boolean tomMatch7_17= false ;
 String  tomMatch7_15= "" ;
 String  tomMatch7_16= "" ;
if ( true ) {
if (tom_equal_term_String("land", (( String )tom_name))) {
{
tomMatch7_17= true ;
tomMatch7_15=(( String )tom_name);
}
} else {
if ( true ) {
if (tom_equal_term_String("lor", (( String )tom_name))) {
{
tomMatch7_17= true ;
tomMatch7_16=(( String )tom_name);
}
}
}
}
}
if (tomMatch7_17) {

debug(9,"Logical operation "+o);
// only INT() & result in INT(max(a,b))
Type t1 = 
tom_op1.getType();
Type t2 = 
tom_op2.getType();

{
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_intType(((Type)((Type)t1)))) {
IntegerTypes tom_sizeA=tom_get_slot_intType_type(((Type)t1));
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_intType(((Type)((Type)t2)))) {
IntegerTypes tom_sizeB=tom_get_slot_intType_type(((Type)t2));


IntegerTypes size = 
tom_sizeA.getValue()>
tom_sizeB.getValue()?
tom_sizeA:
tom_sizeB;
if (size == IntegerTypes.CHAR ||size == IntegerTypes.SHORT) {
return GecosUserTypeFactory.SHORT();
}
else if (size == IntegerTypes.INT) {
return GecosUserTypeFactory.INT();
}
else if (size == IntegerTypes.LONG) {
return GecosUserTypeFactory.LONG();
}
else {
return GecosUserTypeFactory.LONGLONG();
}

}
}
}
}
}
}
}
}

debug(20,"Logic op "+o+" badly typed");

}
}
}
{
if ( true ) {
boolean tomMatch7_28= false ;
 String  tomMatch7_20= "" ;
 String  tomMatch7_23= "" ;
 String  tomMatch7_24= "" ;
 String  tomMatch7_21= "" ;
 String  tomMatch7_26= "" ;
 String  tomMatch7_27= "" ;
 String  tomMatch7_22= "" ;
 String  tomMatch7_25= "" ;
if ( true ) {
if (tom_equal_term_String("geq", (( String )tom_name))) {
{
tomMatch7_28= true ;
tomMatch7_20=(( String )tom_name);
}
} else {
if ( true ) {
if (tom_equal_term_String("ge", (( String )tom_name))) {
{
tomMatch7_28= true ;
tomMatch7_21=(( String )tom_name);
}
} else {
if ( true ) {
if (tom_equal_term_String("gt", (( String )tom_name))) {
{
tomMatch7_28= true ;
tomMatch7_22=(( String )tom_name);
}
} else {
if ( true ) {
if (tom_equal_term_String("leq", (( String )tom_name))) {
{
tomMatch7_28= true ;
tomMatch7_23=(( String )tom_name);
}
} else {
if ( true ) {
if (tom_equal_term_String("le", (( String )tom_name))) {
{
tomMatch7_28= true ;
tomMatch7_24=(( String )tom_name);
}
} else {
if ( true ) {
if (tom_equal_term_String("lt", (( String )tom_name))) {
{
tomMatch7_28= true ;
tomMatch7_25=(( String )tom_name);
}
} else {
if ( true ) {
if (tom_equal_term_String("eq", (( String )tom_name))) {
{
tomMatch7_28= true ;
tomMatch7_26=(( String )tom_name);
}
} else {
if ( true ) {
if (tom_equal_term_String("neq", (( String )tom_name))) {
{
tomMatch7_28= true ;
tomMatch7_27=(( String )tom_name);
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
if (tomMatch7_28) {

Type t1 = 
tom_op1.getType();
Type t2 = 
tom_op2.getType();
Type res = null;

{
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_intType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_intType(((Type)((Type)t2)))) {



res = GecosUserTypeFactory.BOOL();

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_intType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_floatType(((Type)((Type)t2)))) {

res = GecosUserTypeFactory.BOOL();

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_intType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_ptrType(((Type)((Type)t2)))) {

// warning in gcc
res = GecosUserTypeFactory.BOOL();

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_floatType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_intType(((Type)((Type)t2)))) {

res = GecosUserTypeFactory.BOOL();

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_floatType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_floatType(((Type)((Type)t2)))) {

res = GecosUserTypeFactory.BOOL();

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_floatType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_ptrType(((Type)((Type)t2)))) {

// invalid
debug(17,"Invalid comparison "+o+" between float and pointer");

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_ptrType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_intType(((Type)((Type)t2)))) {

// warning in gcc
res = GecosUserTypeFactory.BOOL();

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_ptrType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_floatType(((Type)((Type)t2)))) {

// invalid
debug(18,"Invalid comparison "+o+" between pointer and float");

}
}
}
}
}
}
}
{
if (tom_is_sort_Type(t1)) {
if (tom_is_sort_Type(((Type)t1))) {
if (tom_is_fun_sym_ptrType(((Type)((Type)t1)))) {
if (tom_is_sort_Type(t2)) {
if (tom_is_sort_Type(((Type)t2))) {
if (tom_is_fun_sym_ptrType(((Type)((Type)t2)))) {

res = GecosUserTypeFactory.BOOL();

}
}
}
}
}
}
}
}

if (res != null) {
debug(10,"Comparison operation "+o+" typed as "+res);
}
else {
debug(11,"Comparison operation "+o+" not resolved");
}
return res;

}
}
}
}


}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(o)) {
if (tom_is_sort_Inst(((Instruction)o))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)o)))) {
 EList<Instruction>  tomMatch3_36=tom_get_slot_generic_children(((Instruction)o));
 String  tom_name=tom_get_slot_generic_name(((Instruction)o));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch3_36))) {
int tomMatch3_40=0;
if (!(tomMatch3_40 >= tom_get_size_InstL_InstL(tomMatch3_36))) {
int tomMatch3_41=tomMatch3_40 + 1;
if (!(tomMatch3_41 >= tom_get_size_InstL_InstL(tomMatch3_36))) {
int tomMatch3_42=tomMatch3_41 + 1;
if (!(tomMatch3_42 >= tom_get_size_InstL_InstL(tomMatch3_36))) {
if (tomMatch3_42 + 1 >= tom_get_size_InstL_InstL(tomMatch3_36)) {
{
{
if ( true ) {
if ( true ) {
if (tom_equal_term_String("mux", (( String )tom_name))) {

// Float(), Ptr, INT() & result in INT(max(a,b))

}
}
}
}
}


}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(o)) {
if (tom_is_sort_Inst(((Instruction)o))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)o)))) {
 EList<Instruction>  tomMatch3_46=tom_get_slot_generic_children(((Instruction)o));
 String  tom_name=tom_get_slot_generic_name(((Instruction)o));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch3_46))) {
int tomMatch3_50=0;
if (!(tomMatch3_50 >= tom_get_size_InstL_InstL(tomMatch3_46))) {
int tomMatch3_51=tomMatch3_50 + 1;
if (!(tomMatch3_51 >= tom_get_size_InstL_InstL(tomMatch3_46))) {
if (!(tomMatch3_51 + 1 >= tom_get_size_InstL_InstL(tomMatch3_46))) {
{
{
if ( true ) {
if ( true ) {
if (tom_equal_term_String("sum", (( String )tom_name))) {

// Float(), Ptr, INT() & result in INT(max(a,b))

}
}
}
}
}


}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(o)) {
if (tom_is_sort_Inst(((Instruction)o))) {
if (tom_is_fun_sym_indir(((Instruction)((Instruction)o)))) {

Type t= (
tom_get_slot_indir_address(((Instruction)o))).getType(); 

{
{
if (tom_is_sort_Type(t)) {
if (tom_is_sort_Type(((Type)t))) {
if (tom_is_fun_sym_ptrType(((Type)((Type)t)))) {

// Do something
return (
tom_get_slot_ptrType_base(((Type)t)));

}
}
}
}
{
if (tom_is_sort_Type(t)) {
if (tom_is_sort_Type(((Type)t))) {
if (tom_is_fun_sym_arrayType(((Type)((Type)t)))) {

// Do something
return (
tom_get_slot_arrayType_base(((Type)t)));

}
}
}
}
{
if (tom_is_sort_Type(t)) {

System.err.println("Indirection on neither a popinter or array "+(
((Instruction)o)));
return null;

}
}
}


}
}
}
}
{
if (tom_is_sort_Inst(o)) {
if (tom_is_sort_Inst(((Instruction)o))) {
if (tom_is_fun_sym_address(((Instruction)((Instruction)o)))) {
Instruction tom_lvalue=tom_get_slot_address_address(((Instruction)o));
{
{
if (tom_is_sort_Inst(tom_lvalue)) {
if (tom_is_sort_Inst(((Instruction)tom_lvalue))) {
if (tom_is_fun_sym_array(((Instruction)((Instruction)tom_lvalue)))) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tom_get_slot_array_index(((Instruction)tom_lvalue))))) {

return GecosUserTypeFactory.PTR((
tom_get_slot_array_dest(((Instruction)tom_lvalue))).getType());

}
}
}
}
}
{
if (tom_is_sort_Inst(tom_lvalue)) {
if (tom_is_sort_Inst(((Instruction)tom_lvalue))) {
if (tom_is_fun_sym_symref(((Instruction)((Instruction)tom_lvalue)))) {

Symbol sym = (
tom_get_slot_symref_symbol(((Instruction)tom_lvalue)));
return GecosUserTypeFactory.PTR(sym.getType());


}
}
}
}
}



}
}
}
}
{
if (tom_is_sort_Inst(o)) {
if (tom_is_sort_Inst(((Instruction)o))) {
if (tom_is_fun_sym_array(((Instruction)((Instruction)o)))) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tom_get_slot_array_index(((Instruction)o))))) {

Type t= (
tom_get_slot_array_dest(((Instruction)o))).getType();

{
{
if (tom_is_sort_Type(t)) {
if (tom_is_sort_Type(((Type)t))) {
if (tom_is_fun_sym_ptrType(((Type)((Type)t)))) {

// Do something
return (
tom_get_slot_ptrType_base(((Type)t)));

}
}
}
}
{
if (tom_is_sort_Type(t)) {
if (tom_is_sort_Type(((Type)t))) {
if (tom_is_fun_sym_arrayType(((Type)((Type)t)))) {

// Do something
return (
tom_get_slot_arrayType_base(((Type)t)));

}
}
}
}
{
if (tom_is_sort_Type(t)) {

System.err.println("Array access on neither an array or pointer "+(
((Instruction)o)));
return null;

}
}
}


}
}
}
}
}
{
if (tom_is_sort_Inst(o)) {
if (tom_is_sort_Inst(((Instruction)o))) {
if (tom_is_fun_sym_ssaDef(((Instruction)((Instruction)o)))) {

return  null;  

}
}
}
}
{
if (tom_is_sort_Inst(o)) {
if (tom_is_sort_Inst(((Instruction)o))) {
if (tom_is_fun_sym_ssaUse(((Instruction)((Instruction)o)))) {

return  null;  

}
}
}
}
{
if (tom_is_sort_Inst(o)) {
if (tom_is_sort_Inst(((Instruction)o))) {
if (tom_is_fun_sym_phi(((Instruction)((Instruction)o)))) {
 EList<Instruction>  tomMatch3_82=tom_get_slot_phi_children(((Instruction)o));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch3_82))) {
if (!(0 >= tom_get_size_InstL_InstL(tomMatch3_82))) {

return null;  

}
}
}
}
}
}
{
if (tom_is_sort_Inst(o)) {
if (tom_is_sort_Inst(((Instruction)o))) {
if (tom_is_fun_sym_symref(((Instruction)((Instruction)o)))) {
Symbol tom_sym=tom_get_slot_symref_symbol(((Instruction)o));

if((
tom_sym)!=null) {
Type type= (
tom_sym).getType();
} else {
return null;
}
Type type= (
tom_sym).getType();
debug(12,"Symbol instruction "+o+" with Type "+type);
return type;  

}
}
}
}
{
if (tom_is_sort_Inst(o)) {
if (tom_is_sort_Inst(((Instruction)o))) {
if (tom_is_fun_sym_ival(((Instruction)((Instruction)o)))) {

debug(13,"TypeResolver -> IntInstruction : "+(
tom_get_slot_ival_value(((Instruction)o))));
return GecosUserTypeFactory.INT();  

}
}
}
}
{
if (tom_is_sort_Inst(o)) {
if (tom_is_sort_Inst(((Instruction)o))) {
if (tom_is_fun_sym_fval(((Instruction)((Instruction)o)))) {

debug(14,"TypeResolver -> FloatInstruction : "+(
tom_get_slot_fval_value(((Instruction)o))));
return GecosUserTypeFactory.DOUBLE();  				


}
}
}
}
{
if (tom_is_sort_Inst(o)) {
if (tom_is_sort_Inst(((Instruction)o))) {
if (tom_is_fun_sym_strval(((Instruction)((Instruction)o)))) {
 String  tom_a=tom_get_slot_strval_value(((Instruction)o));

((StringInstruction)(
((Instruction)o))).setValue(
tom_a);
if(
tom_a.equals("true")|(
tom_a.equals("false"))){
debug(15,"TypeResolver -> BoolInstruction "+(
tom_a));
return GecosUserTypeFactory.BOOL();
} else {
debug(16,"TypeResolver -> StringLitteralInstruction "+(
tom_a));
return GecosUserTypeFactory.STRING((
tom_a).length());
}


}
}
}
}
{
if (tom_is_sort_Inst(o)) {



}
}
}

return null;

}

}
