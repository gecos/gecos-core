/**
 * 
 */
package fr.irisa.cairn.gecos.model.cdtfrontend.builder.impl;

import java.util.Stack;

import fr.irisa.cairn.gecos.model.cdtfrontend.builder.ControlStructureBuilder;
import fr.irisa.cairn.gecos.model.cdtfrontend.builder.CoreBuilder;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.Instruction;
import gecos.types.AliasType;
import gecos.types.EnumType;
import gecos.types.Enumerator;
import gecos.types.FunctionType;
import gecos.types.RecordType;
import gecos.types.Type;


/**
 * @author mnaullet
 */
public class CoreBuilderImpl implements CoreBuilder {

	private ControlStructureBuilder controlStructureBuilder;
	
	private ProcedureSet procedureSet;
	private Procedure procedure;
	private Stack<Scope> scopes;
	private Block parentBlock;
	
	private Scope globalScope() {
		return scopes.firstElement();
	}
	
	private Scope currentScope() {
		return scopes.peek();
	}
	
	public CoreBuilderImpl() {
		procedureSet = GecosUserCoreFactory.procedureSet();
		GecosUserTypeFactory.setScope(procedureSet.getScope());
		scopes = new Stack<Scope>();
		scopes.push(procedureSet.getScope());
		
		controlStructureBuilder = new ControlStructureBuilderImpl(this);
	}

	public Procedure addProcedure(ProcedureSymbol procedureSymbol) {
		procedure = GecosUserCoreFactory.proc(procedureSymbol);
		procedureSet.addProcedure(procedure);
		return procedure;
	}
	
	public ParameterSymbol createParameterSymbol(String name, Type type) {
		ParameterSymbol paramSymbol = GecosUserCoreFactory.paramSymbol(name, type);
		currentScope().addSymbol(paramSymbol);
		return paramSymbol;
	}
	
	public void setBodyToCurrentProcedure(CompositeBlock compositeBlock) {
		setBody(procedure, compositeBlock);
	}
	
	public Symbol addSymbolToCurrentScope(String name, Type type, Instruction instruction) {
		return GecosUserCoreFactory.symbol(name, type, currentScope(), instruction);
	}
	
//	public ProcedureSymbol addProcedureSymbol(String procedureName, Type returnType, List<ParameterSymbol> parameterSymbols, boolean hasElipsis) {
//		ProcedureSymbol symbol = (ProcedureSymbol)findSymbol(procedureName);
//		if (symbol == null) {
//			symbol = GecosUserCoreFactory.procSymbol(procedureName, returnType, parameterSymbols, hasElipsis);
////			globalScope().getSymbols().add(symbol);
//			currentScope().getSymbols().add(symbol);
//		} else {
//			//update arguments
//			Scope scope = symbol.getScope();
//			scope.getSymbols().clear();
//			for (Symbol s : parameterSymbols)
//				scope.addSymbol(s);
//		}
//		return symbol;
//	}
	
	/**
	 * In case a {@link ProcedureSymbol} with the same name already exists in the current scope hierarchy, then
	 * the existing ProcedureSymbol is returned after replacing its scope by {@code params}. Otherwise
	 * a new {@link ProcedureSymbol} is created a added to the CURRENT scope.
	 * <br><br>
	 * NOTE: this assumes that the caller of this method has already checked for errors in case a similar
	 * {@link Symbol} already exists.
	 */
	public ProcedureSymbol addProcedureSymbol(String procedureName, FunctionType funcType, Scope params) {
		Symbol symbol = findSymbol(procedureName);
		if(symbol instanceof ProcedureSymbol) {
			((ProcedureSymbol) symbol).setScope(params);
			//XXX should we also replace the type ?
		} 
		else {
			symbol = GecosUserCoreFactory.procSymbol(procedureName, funcType, params);
			currentScope().getSymbols().add(symbol);
		}
		
		return (ProcedureSymbol) symbol;
	}
	
	public ProcedureSymbol addProcedureSymbol(String procedureName, FunctionType funcType) {
		Symbol symbol = findSymbol(procedureName);
		if(symbol instanceof ProcedureSymbol) {
			//XXX should we replace the type ?
		} 
		else {
			symbol = GecosUserCoreFactory.procSymbol(procedureName, funcType);
			currentScope().getSymbols().add(symbol);
		}
		
		return (ProcedureSymbol) symbol;
	}
	
	public Scope createScope() {
		Scope scope = GecosUserCoreFactory.scope();
		scopes.push(scope);
		GecosUserTypeFactory.setScope(scope); //XXX ???
		return scope;
	}
	
	public void goToUpperScope() {
		if (scopes.size() > 1) {
			scopes.pop();
			GecosUserTypeFactory.setScope(currentScope()); //XXX ???
		}
	}

	public Enumerator findEnumerator(String name) {
		Scope scope = currentScope();
		while (scope != null) {
			for (Type type : scope.getTypes()) {
				if (type instanceof EnumType) {
					Enumerator enumerator = findEnumerator((EnumType)type, name);
					if (enumerator != null)
						return enumerator;
				}
			}
			scope = scope.getParent();
		}
		return null;
	}

	public Enumerator findEnumerator(EnumType enumType, String name) {
		for (Enumerator enumerator : enumType.getEnumerators())
			if (enumerator.getName().equals(name))
				return enumerator;
		return null;
	}

	public Symbol findSymbol(String name) {
		return currentScope().lookup(name);
	}
	
	public Symbol findSymbolInCurrentScope(String name) {
		return currentScope().getSymbols().stream()
				.filter(s -> s.getName().equals(name))
				.findFirst().orElse(null);
	}

	public Type findType(String name) {
		Type type = currentScope().lookupType(name);
		if (type != null)
			return type;
		else
			return null;
	}

	public AliasType findAliasType(String name) {
		Type type = currentScope().lookupAliasType(name);
		if (type != null && type instanceof AliasType)
			return (AliasType)type;
		else
			return null;
	}
	
	public AliasType findAliasTypeInCurrentScope(String name) {
		AliasType alias = currentScope().getTypes().stream()
			.filter(t -> t instanceof AliasType)
			.map(t -> (AliasType)t)
			.filter(t -> t.getName().equals(name))
			.findFirst().orElse(null);
		return alias;
	}

	public EnumType findEnumType(String name) {
		Type type = currentScope().lookupEnumType(name);
		if (type != null && type instanceof EnumType)
			return (EnumType)type;
		else
			return null;
	}

	public RecordType findRecordType(String name) {
		Type type = currentScope().lookupRecordType(name);
		if (type != null && type instanceof RecordType)
			return (RecordType)type;
		else
			return null;
	}

	public ControlStructureBuilder getControlStructureBuilder() {
		return controlStructureBuilder;
	}

	public ProcedureSet getProcedureSet() {
		return procedureSet;
	}

	public Symbol getSymbol(String name) {
		return currentScope().lookup(name);
	}

	public Block getParentBlock() {
		return parentBlock;
	}
	
	public void setParentBlock(Block block) {
		parentBlock = block;
	}

	private void setBody(Procedure procedure, CompositeBlock compositeBlock) {
		BasicBlock bb1 = controlStructureBuilder.createBasicBlock();
		BasicBlock bb2 = controlStructureBuilder.createBasicBlock();
		compositeBlock.getChildren().add(0, bb1);
		compositeBlock.getChildren().add(bb2);
		procedure.setBody(compositeBlock);
		procedure.setStart(bb1);
		procedure.setEnd(bb2);
	}

	@Override
	public boolean isDefined(Type type) {
		return currentScope().lookup(type) != null;
	}

	@Override
	public boolean isInCurrentScope(Symbol symbol) {
		return symbol.getContainingScope() == currentScope();
	}
	
	@Override
	public boolean isInGlobalScope(Symbol symbol) {
		return symbol.getContainingScope() == globalScope();
	}
	
	@Override
	public boolean isInCurrentScope(Type type) {
		return type.getContainingScope() == currentScope();
	}

	@Override
	public void removeFromCurrentScope(Type type) {
		currentScope().getTypes().remove(type);
	}

	@Override
	public void addToCurrentScope(Type type) {
		currentScope().getTypes().add(type);
	}
}