package fr.irisa.cairn.gecos.model.factory;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import fr.irisa.cairn.gecos.model.factory.conversion.ConversionMatrix;
import gecos.core.ProcedureSymbol;
import gecos.core.Symbol;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.ArrayValueInstruction;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BreakInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.CaseInstruction;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.CondInstruction;
import gecos.instrs.ContinueInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.DummyInstruction;
import gecos.instrs.EnumeratorInstruction;
import gecos.instrs.FieldInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.GotoInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.InstrsFactory;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.LabelInstruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.MethodCallInstruction;
import gecos.instrs.NumberedSymbolInstruction;
import gecos.instrs.PhiInstruction;
import gecos.instrs.RangeInstruction;
import gecos.instrs.ReductionOperator;
import gecos.instrs.RetInstruction;
import gecos.instrs.SSADefSymbol;
import gecos.instrs.SSAUseSymbol;
import gecos.instrs.SelectOperator;
import gecos.instrs.SetInstruction;
import gecos.instrs.SimdExpandInstruction;
import gecos.instrs.SimdExtractInstruction;
import gecos.instrs.SimdGenericInstruction;
import gecos.instrs.SimdPackInstruction;
import gecos.instrs.SimdShuffleInstruction;
import gecos.instrs.SimpleArrayInstruction;
import gecos.instrs.SizeofTypeInstruction;
import gecos.instrs.SizeofValueInstruction;
import gecos.instrs.StringInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.AliasType;
import gecos.types.ArrayType;
import gecos.types.BaseType;
import gecos.types.Enumerator;
import gecos.types.Field;
import gecos.types.FunctionType;
import gecos.types.PtrType;
import gecos.types.RecordType;
import gecos.types.Type;
import gecos.types.VoidType;

public class GecosUserInstructionFactory {
	
	static private InstrsFactory factory = InstrsFactory.eINSTANCE;
	
	private GecosUserInstructionFactory() {}

	public static GenericInstruction add(Instruction a, Instruction b) {
		checkNoVoid(a, b);
		return generic(ArithmeticOperator.ADD.getLiteral(), ConversionMatrix.getCommon(a.getType(), b.getType()), a, b);
	}

	/**
	 * @deprecated use {@link #add(Instruction, Instruction)}
	 */
	@Deprecated
	public static GenericInstruction add(Instruction op1, Instruction op2, Type t) {
		return add(op1,op2);
	}

	public static AddressInstruction address(Instruction i) {
		AddressInstruction res = factory.createAddressInstruction();
		res.setAddress(i);
		res.setType(i.getType());
		return res;
	}

	public static GenericInstruction and(Instruction i1, Instruction i2) {
		return generic(BitwiseOperator.AND.getLiteral(), ConversionMatrix.getCommon(i1.getType(), i2.getType()), i1, i2);
	}
	
	
	public static ArrayInstruction array(Instruction dest,  List<Instruction> indexes) {
		return array(dest,indexes.toArray(new Instruction[indexes.size()]));
	}
	
	public static ArrayInstruction array(Instruction dest, Instruction... indexes) {
		ArrayInstruction res = factory.createArrayInstruction();
		checkNoVoid(indexes);
		res.setDest(dest);
		for (Instruction instruction : indexes) {
			//note : ArrayInstruction.getIndex() returns a copy of the list
			res.addIndex(instruction);
			res.setType(instruction.getType());
		}
		return res;
	}
	
	public static SimpleArrayInstruction array(Symbol dest, Instruction... indexes) {
		SimpleArrayInstruction res = factory.createSimpleArrayInstruction();
		res.setDest(symbref(dest));
		res.setType(dest.getType());
		checkNoVoid(indexes);
		for (Instruction index : indexes) {
			//note : SimpleArrayInstruction.getIndex() returns a copy of the list
			res.addIndex(index);
			if (res.getType() instanceof PtrType)
				res.setType(((PtrType) res.getType()).getBase());
			else if (res.getType() instanceof ArrayType)
				res.setType(((ArrayType) res.getType()).getBase());
		}
		return res;
	}

	public static SimpleArrayInstruction array(Symbol dest, List<Instruction> indexes) {
		return array(dest,indexes.toArray(new Instruction[indexes.size()]));
	}
	
	public static ArrayValueInstruction arrayValue(Instruction... values) {
		ArrayValueInstruction arrayVal = factory.createArrayValueInstruction();
		for (int i = 0; i < values.length; i++) {
			arrayVal.getChildren().add(values[i]);
			arrayVal.setType(values[i].getType());
		}
		return arrayVal;
	}

	/**
	 * @deprecated use {@link #condBranch(Instruction, String)}
	 */
	@Deprecated
	public static CondInstruction branch(Instruction cond, String label) {
		return condBranch(cond, label);
	}

	public static BreakInstruction breakInst() {
		BreakInstruction branchInstruction = factory.createBreakInstruction();
		branchInstruction.setType(GecosUserTypeFactory.VOID());
		return branchInstruction;
	}
	
	public static CallInstruction call(Symbol address, Instruction... args) {
		return call(symbref(address), args);
	}
	
	public static CallInstruction call(Instruction address, Instruction... args) {
		return call(address, Arrays.asList(args));
	}
	
	public static CallInstruction call(Instruction address, List<Instruction> args) {
		CallInstruction call = factory.createCallInstruction();
		call.setAddress(address);
		Type retType = GecosUserTypeFactory.VOID();
		if (address.isSymbol()) {
			SymbolInstruction syminst = (SymbolInstruction) address;
			Symbol symbol = syminst.getSymbol();
			if (symbol instanceof ProcedureSymbol) {
				FunctionType type = (FunctionType) syminst.getSymbol().getType();
				retType = type.getReturnType();
			}
		}
		call.setType(retType);
		for (int i = 0; i < args.size(); i++) {
			Instruction instruction = args.get(i);
			call.addArg(instruction);
		}
		return call;
	}

	public static CallInstruction call(ProcedureSymbol psSym, Instruction... args) {
		return call(symbref(psSym),args);
	}

	public static CallInstruction call(ProcedureSymbol psSym, List<Instruction> args) {
		return call(psSym,args.toArray(new Instruction[args.size()]));
	}

	public static CaseInstruction caseInst(Instruction buildExpression) {
		CaseInstruction caseInst = factory.createCaseInstruction();
		caseInst.setExpr(buildExpression);
		caseInst.setType(buildExpression.getType());
		return caseInst;
	}

	/**
	 * @param type
	 * @param value
	 * @return
	 */
	public static ConvertInstruction cast(Type type, Instruction value) {
//		if (value.getType() == type) return value;
		
		ConvertInstruction conv = factory.createConvertInstruction();
		
//		while (type instanceof StaticType || type instanceof ConstType) {
//			if (type instanceof StaticType) type = ((StaticType)type).getBase();
//			if (type instanceof ConstType) type = ((ConstType)type).getBase();
//		}
		
		conv.setType(type);
		conv.setExpr(value);
		return conv;
	}

	private static void checkNoVoid(Instruction... insts) {
		for (Instruction instruction : insts)
			if (instruction != null && instruction.getType() instanceof VoidType)
				throw new IllegalArgumentException("Operation not allowed on void type");
	}

	public static CondInstruction condBranch(Instruction cond, String label) {
		CondInstruction res = factory.createCondInstruction();
		res.setCond(cond);
		res.setLabel(label);
		res.setType(cond.getType());
		return res;
	}

	public static ContinueInstruction continueInst() {
		ContinueInstruction branchInstruction = factory.createContinueInstruction();
		branchInstruction.setType(GecosUserTypeFactory.VOID());
		return branchInstruction;
	}

	/**
	 * @deprecated use {@link #cast(Type, Instruction)} 
	 */
	@Deprecated
	public static Instruction createConvertInstruction(Type type, Instruction instruction) {
		return cast(type,instruction);
	}

	public static EnumeratorInstruction enumorator(Enumerator enumerator) {
		EnumeratorInstruction enumeratorInstruction = factory.createEnumeratorInstruction();
		enumeratorInstruction.setEnumerator(enumerator);
		enumeratorInstruction.setType((Type)enumerator.eContainer());
		return enumeratorInstruction;
	}

	public static GenericInstruction div(Instruction a, Instruction b) {
		checkNoVoid(a, b);
		return generic(ArithmeticOperator.DIV.getLiteral(), ConversionMatrix.getCommon(a.getType(), b.getType()), a, b);
	}

	public static DummyInstruction dummy() {
		DummyInstruction dummyInstruction = factory.createDummyInstruction();
		dummyInstruction.setType(GecosUserTypeFactory.VOID());
		return dummyInstruction;
	}

	public static GenericInstruction eq(Instruction a, Instruction b) {
		checkNoVoid(a, b);
		return generic(ComparisonOperator.EQ.getLiteral(), GecosUserTypeFactory.BOOL(), a, b);
	}

	/**
	 * @deprecated use {@link #eq(Instruction, Instruction)} 
	 */
	@Deprecated
	public static GenericInstruction eq(Instruction op1, Instruction op2, Type t) {
		return eq(op1,op2);
	}

	public static FieldInstruction field(Instruction a, Field name) {
		FieldInstruction fieldInst = factory.createFieldInstruction();
		fieldInst.setExpr(a);
		fieldInst.setField(name);
		fieldInst.setType(name.getType());
		return fieldInst;
	}

	public static FloatInstruction Float(double a) {
		FloatInstruction inst = factory.createFloatInstruction();
		inst.setValue(a);
		inst.setType(GecosUserTypeFactory.FLOAT());
		return inst;
	}

	public static GenericInstruction ge(Instruction a, Instruction b) {
		checkNoVoid(a, b);
		return generic(ComparisonOperator.GE.getLiteral(), GecosUserTypeFactory.BOOL(), a, b);
	}

	/**
	 * @deprecated use {@link #ge(Instruction, Instruction)} 
	 */
	@Deprecated
	public static GenericInstruction ge(Instruction a, Instruction b, Type type)  {
		return ge(a,b);
	}
	
	public static LabelInstruction label(String label) {
		LabelInstruction res = factory.createLabelInstruction();
		res.setName(label);
		res.setType(GecosUserTypeFactory.VOID());
		return res;
	}
	
	public static GenericInstruction generic(String string, Type type, Instruction... operands) {

		GenericInstruction res = factory.createGenericInstruction();
		res.setName(string);
		for (int i = 0; i < operands.length; i++) {
			res.getOperands().add(operands[i]);
		}
		res.setType(type);
		return res;
	}

	public static char getEscapedChar(char c) {
		switch (c) {
		case 'b' :
			return '\b';
		case 'f' :
			return '\f';
		case 'n' :
			return '\n';
		case 'r' :
			return '\r';
		case 't' :
			return '\t';
		case '"' :
			return '\"';
		case '\'' :
			return '\'';
		case '\\' :
			return '\\';
		}
		return c;
	}
	
	public static GotoInstruction gotoInst(LabelInstruction label) {
		GotoInstruction gotoInstruction = factory.createGotoInstruction();
		gotoInstruction.setLabelInstruction(label);
		gotoInstruction.setType(GecosUserTypeFactory.VOID());
		return gotoInstruction;
	}

	public static GenericInstruction gt(Instruction a, Instruction b) {
		checkNoVoid(a, b);
		return generic(ComparisonOperator.GT.getLiteral(), GecosUserTypeFactory.BOOL(), a, b);
	}

	/**
	 * @deprecated use {@link #gt(Instruction, Instruction)} 
	 */
	@Deprecated
	public static GenericInstruction gt(Instruction op1, Instruction op2, Type int2) {
		return gt(op1,op2);
	}

	public static IndirInstruction indir(Instruction address) {
		Type t = null;
		if (address.getType() instanceof PtrType) {
			PtrType ptrType = (PtrType) address.getType();
			t = (ptrType.getBase());
		} else if (address.getType() instanceof ArrayType) {
			t = (((ArrayType)address.getType()).getBase());
		} else if (address.getType() instanceof FunctionType) {
			t = (address.getType());
		} else if (address.getType() instanceof AliasType) {
			Type type = address.getType();
			while (type instanceof AliasType)
				type = ((AliasType)type).getAlias();
			if (type instanceof PtrType) {
				PtrType ptrType = (PtrType) type;
				t = (ptrType.getBase());
			} else if (type instanceof FunctionType) {
				t = (type);
			} else if (type instanceof RecordType) {
				t = type;
			} else if (type instanceof BaseType) {
				t = type;
			} else {
				throw new UnsupportedOperationException("Not yet implemented");
			}
//		} else if (address.getType() instanceof RegisterType) {
//			t = (((RegisterType)address.getType()).getBase());
		} else if (address.getType() instanceof BaseType) {
			t = (address.getType());
		} else {
			throw new UnsupportedOperationException("Not yet implemented");
		}
		return indir(t,address);
	}

	public static IndirInstruction indir(Type type, Instruction address)  {
		IndirInstruction res = factory.createIndirInstruction();
		res.setAddress(address);
		res.setType(type);
		return res;
	}

	public static IntInstruction Int(long l) {
		IntInstruction inst = factory.createIntInstruction();
		inst.setValue(l);
		inst.setType(GecosUserTypeFactory.INT());
		return inst;
	}
	
	public static GenericInstruction land(Instruction a, Instruction b) {
		checkNoVoid(a, b);
		return generic(LogicalOperator.AND.getLiteral(), GecosUserTypeFactory.BOOL(), a, b);
	}

	public static GenericInstruction le(Instruction a, Instruction b) {
		checkNoVoid(a, b);
		return generic(ComparisonOperator.LE.getLiteral(), GecosUserTypeFactory.BOOL(), a, b);
	}

	/**
	 * @deprecated use {@link #le(Instruction, Instruction)} 
	 */
	@Deprecated
	public static GenericInstruction le(Instruction a, Instruction b, Type type)  {
		return le(a,b);
	}
	
	public static GenericInstruction le(Symbol op1, Instruction op2) {
		return le(symbref(op1),op2);
	}
	

	/**
	 * @deprecated use {@link #le(Instruction, Instruction)} 
	 */
	@Deprecated
	public static GenericInstruction leq(Instruction a, Instruction b)  {
		return le(a,b);
	}

	/**
	 * @deprecated use {@link #le(Instruction, Instruction)} 
	 */
	@Deprecated
	public static GenericInstruction leq(Instruction op1, Instruction op2, Type t) {
		return le(op1,op2);
	}

	/**
	 * @deprecated use {@link #le(Symbol, Instruction)} 
	 */
	@Deprecated
	public static GenericInstruction leq(Symbol op1, Instruction op2) {
		return le(op1,op2);
	}

	public static GenericInstruction lnot(Instruction build) {
		return generic(LogicalOperator.NOT.getLiteral(), GecosUserTypeFactory.BOOL(), build);
	}

	public static GenericInstruction lor(Instruction i1, Instruction i2) {
		return generic(LogicalOperator.OR.getLiteral(), GecosUserTypeFactory.BOOL(), i1, i2);
	}
	

	public static GenericInstruction lt(Instruction a, Instruction b) {
		checkNoVoid(a, b);
		return generic(ComparisonOperator.LT.getLiteral(), GecosUserTypeFactory.BOOL(), a, b);
	}

	/**
	 * @deprecated use {@link #lt(Instruction, Instruction)} 
	 */
	@Deprecated
	public static GenericInstruction lt(Instruction op1, Instruction op2, Type t) {
		return lt(op1,op2);
	}

	public static MethodCallInstruction methodCallInstruction(String methodName, Instruction receiver, Instruction... args) {		
		MethodCallInstruction methodCallInstruction = factory.createMethodCallInstruction();
		methodCallInstruction.setType(GecosUserTypeFactory.VOID());
		methodCallInstruction.setName(methodName);
		methodCallInstruction.setAddress(receiver);
		for(Instruction arg : args){
			methodCallInstruction.addArg(arg);
		}
		return methodCallInstruction;
	}
	
	public static MethodCallInstruction methodCallInstruction(String methodName, Instruction receiver, List<Instruction> args) {		
		return methodCallInstruction(methodName, receiver, args.toArray(new Instruction[args.size()]));
	}

	public static GenericInstruction mod(Instruction a, Instruction b) {
		checkNoVoid(a, b);
		return generic(ArithmeticOperator.MOD.getLiteral(), ConversionMatrix.getCommon(a.getType(), b.getType()), a, b);
	}

	public static GenericInstruction mul(Instruction a, Instruction b) {
		checkNoVoid(a, b);
		return generic(ArithmeticOperator.MUL.getLiteral(), ConversionMatrix.getCommon(a.getType(), b.getType()), a, b);
	}

	public static GenericInstruction mux(Instruction cond, Instruction trueVal, Instruction falseVal){
		Type common = ConversionMatrix.getCommon(trueVal.getType(), falseVal.getType());
		GenericInstruction res = GecosUserInstructionFactory.generic(SelectOperator.MUX.getLiteral(), common, cond, trueVal, falseVal);
		return res;
	}

	public static GenericInstruction ne(Instruction a, Instruction b) {
		checkNoVoid(a, b);
		return generic(ComparisonOperator.NE.getLiteral(), GecosUserTypeFactory.BOOL(), a, b);
	}

	public static GenericInstruction neg(Instruction build) {
		return generic(ArithmeticOperator.NEG.getLiteral(), build.getType(), build);
	}

	/**
	 * @deprecated use {@link #ne(Instruction, Instruction)} 
	 */
	@Deprecated
	public static GenericInstruction neq(Instruction op1, Instruction op2, Type t) {
		return ne(op1,op2);
	}
	
	public static GenericInstruction not(Instruction build) {
		return generic(BitwiseOperator.NOT.getLiteral(), build.getType(), build);
	}
	
	public static NumberedSymbolInstruction numberedSymbolInstruction(Symbol sym, int topVisibility) {
		NumberedSymbolInstruction nSymInst = factory.createNumberedSymbolInstruction();
		nSymInst.setSymbol(sym);
		nSymInst.setNumber(topVisibility);
		nSymInst.setType(sym.getType());
		return nSymInst;
	}

	public static NumberedSymbolInstruction numberedSymbolInstruction(SymbolInstruction sym, int topVisibility) {
		return numberedSymbolInstruction(sym.getSymbol(), topVisibility);
	}

	public static GenericInstruction or(Instruction i1, Instruction i2) {
		return generic(BitwiseOperator.OR.getLiteral(), ConversionMatrix.getCommon(i1.getType(), i2.getType()), i1, i2);
	}

	public static PhiInstruction phi(Type type)  {
		PhiInstruction createPhiInstruction = factory.createPhiInstruction();
		createPhiInstruction.setType(type);
		return createPhiInstruction;
	}
	
	public static RangeInstruction range(Instruction inst, long low, long high) {
		return range(inst.getType(),inst,low,high);
	}
	
	public static RangeInstruction range(Type type, Instruction inst, long low, long high)  {
		RangeInstruction  res =factory.createRangeInstruction();
		res.setExpr(inst);
		res.setHigh(high); 
		res.setLow(low); 
		res.setType(type);
		return res;
	}

	public static RetInstruction ret(Instruction buildExpression) {
		RetInstruction ret = factory.createRetInstruction();
		ret.setExpr(buildExpression);
		ret.setType(buildExpression.getType());
		return ret;
	}

	public static GenericInstruction sequence(Instruction ... instrs) {
		if (instrs.length == 0) throw new IllegalArgumentException();
		GenericInstruction genericInstruction = generic("sequence",instrs[instrs.length-1].getType(),instrs);
		return genericInstruction;
	}

	public static GenericInstruction sequence(List<Instruction> instrs) {
		if (instrs.size() == 0) throw new IllegalArgumentException();
		return sequence(instrs.toArray(new Instruction[instrs.size()]));
	}

	public static SetInstruction set(Instruction dest, Instruction value)  {
		if(dest.getType()==null) {
			throw new UnsupportedOperationException("LHS " + dest + ":" + dest.getClass().getSimpleName() + " has no type");
		} else {
			/*
			 * Do not add casts here !
			 */
			SetInstruction inst = factory.createSetInstruction();
			inst.setDest(dest);
			inst.setType(dest.getType());
			inst.setSource(value);
			return inst;
		}
	}

	/**
	 * @deprecated use {@link #set(Instruction, Instruction)}
	 */
	@Deprecated
	public static SetInstruction set(Instruction dest, Instruction value, Type t) {
		return set(dest,value);
	}

	public static SetInstruction set(Symbol dest, Instruction value) {
		return set(symbref(dest),value);
	}


	public static SetInstruction set(Symbol sym, long value)  {
		if(sym.getType() instanceof BaseType) {
			return set(sym,Int(value));
		}
		throw new UnsupportedOperationException("Type mitsmatch ");
	}

	public static SetInstruction set(Symbol sym, int value)  {
		return set(sym,(long)value);
	}

	public static GenericInstruction shl(Instruction inst, long amount) {
//		if(amount < 0) throw new UnsupportedOperationException("");
		return shl(inst, Int(amount));
	}
	
	public static GenericInstruction shl(Instruction inst, Instruction amount) {
		checkNoVoid(inst, amount);
		return generic(BitwiseOperator.SHL.getLiteral(), inst.getType(), inst, amount);
	}

	public static GenericInstruction shr(Instruction inst, long amount) {
//		if(amount < 0) throw new UnsupportedOperationException("");
		return shr(inst, Int(amount));
	}
	
	public static GenericInstruction shr(Instruction inst, Instruction amount) {
		checkNoVoid(inst, amount);
		return generic(BitwiseOperator.SHR.getLiteral(), inst.getType(), inst, amount);
	}
	
	public static SimdGenericInstruction simdOp(String opCode, Type type, Instruction... operands) {
		SimdGenericInstruction simd = factory.createSimdGenericInstruction();
		simd.setName(opCode);
		simd.setType(type);
		for (int i=0; i<operands.length; i++) {
			simd.getOperands().add(operands[i]);
		}
		return simd;
	}
	
	public static SimdExtractInstruction simdExtract(Instruction vector, Instruction subwordPos, Type type) {
		SimdExtractInstruction simd = factory.createSimdExtractInstruction();
		simd.setType(type);
		simd.setVector(vector);
		simd.setSubwordIndex(subwordPos);
		return simd;
	}
	
	public static SimdPackInstruction simdPack(Type type, Instruction... operands) {
		SimdPackInstruction simd = factory.createSimdPackInstruction();
		simd.setType(type);
		for (int i=0; i<operands.length; i++) {
			simd.getChildren().add(operands[i]);
		}
		return simd;
	}
	
	public static SimdShuffleInstruction simdShuffle(Collection<Integer> permutation, Type type, Instruction... operands) {
		SimdShuffleInstruction simd = factory.createSimdShuffleInstruction();
		simd.getPermutation().clear();
		simd.getPermutation().addAll(permutation);
		simd.setType(type);
		for (int i=0; i<operands.length; i++) {
			simd.getChildren().add(operands[i]);
		}
		return simd;
	}
	
	public static SimdExpandInstruction simdExpand(Collection<Integer> permutation, Type type, Instruction... operands) {
		SimdExpandInstruction simd = factory.createSimdExpandInstruction();
		simd.getPermutation().clear();
		simd.getPermutation().addAll(permutation);
		simd.setType(type);
		for (int i=0; i<operands.length; i++) {
			simd.getChildren().add(operands[i]);
		}
		return simd;
	}

	public static SizeofValueInstruction sizeOf(Instruction target) {
		SizeofValueInstruction sizeofValueInstruction = factory.createSizeofValueInstruction();
		sizeofValueInstruction.setType(GecosUserTypeFactory.UINT()); //FIXME sizeof return a size_t which may be different than uint (on 64-bit targets for instance)
		sizeofValueInstruction.setExpr(target);
		return sizeofValueInstruction;
	}
	
	public static SizeofTypeInstruction sizeOf(Type target) {
		SizeofTypeInstruction sizeofTypeInstruction = factory.createSizeofTypeInstruction();
		sizeofTypeInstruction.setType(GecosUserTypeFactory.UINT()); //FIXME sizeof return a size_t which may be different than uint (on 64-bit targets for instance)
		sizeofTypeInstruction.setTargetType(target);
		return sizeofTypeInstruction;
	}

	public static SSADefSymbol ssaDef(SymbolInstruction sym,int nb) {
		SSADefSymbol nSymInst = factory.createSSADefSymbol();
		nSymInst.setSymbol(sym.getSymbol());
		nSymInst.setNumber(nb);
		return nSymInst;
	}

	public static SSAUseSymbol ssaUse(SymbolInstruction sym,int nb) {
		SSAUseSymbol nSymInst = factory.createSSAUseSymbol();
		nSymInst.setSymbol(sym.getSymbol());
		nSymInst.setNumber(nb);
		return nSymInst;
	}

	public static SSAUseSymbol ssaUse(Symbol sym,int nb) {
		SSAUseSymbol nSymInst = factory.createSSAUseSymbol();
		nSymInst.setSymbol(sym);
		nSymInst.setNumber(nb);
		return nSymInst;
	}
	public static SSAUseSymbol ssaUse(SSADefSymbol def, Type t) {
		SSAUseSymbol nSymInst = factory.createSSAUseSymbol();
		nSymInst.setDef(def);
		nSymInst.setSymbol(def.getSymbol());
		nSymInst.setNumber(def.getNumber());
		nSymInst.setType(def.getType());
		
		return nSymInst;
	}

	public static StringInstruction string(char[] chars) {
		String str = new String(chars);
		return string(str);
	}

	public static StringInstruction string(String str){
		StringInstruction inst = factory.createStringInstruction();
		inst.setValue(str);
		BaseType createCHAR = GecosUserTypeFactory.UCHAR();
		PtrType createPOINTER = GecosUserTypeFactory.PTR(createCHAR);
		inst.setType(createPOINTER);
		return inst;
	}

	/**
	 * @deprecated use {@link #string(String)}
	 */
	@Deprecated
	public static StringInstruction String(String str){
		return string(str);
	}
	
	public static GenericInstruction sub(Instruction a, Instruction b) {
		checkNoVoid(a, b);
		return generic(ArithmeticOperator.SUB.getLiteral(), ConversionMatrix.getCommon(a.getType(), b.getType()), a, b);
	}

	public static GenericInstruction sum(Instruction... a) {
		checkNoVoid(a);
		if (a.length < 2) {
			throw new IllegalArgumentException("sum(...) expects >=2 arguments, only " + a.length + " was found");
		}
		GenericInstruction inst = add(a[0], a[1]);
		for (int i = 2; i < a.length; i++) {
			inst = add(inst, a[i]);
		}
		return inst;
	}

	private static GenericInstruction reduction(String literal, Instruction ... a) {
		if (a.length < 2) {
			throw new IllegalArgumentException(literal+"(...) expects >=2 arguments, only " + a.length + " was found");
		}
		checkNoVoid(a);
		GenericInstruction inst = generic(literal, a[0].getType(), a);
		return inst;
	}

	public static SymbolInstruction symbref(Symbol sym)  {
		SymbolInstruction  res =factory.createSymbolInstruction();
		res.setSymbol(sym);
		res.setType(sym.getType());
		return res;
	}

	public static GenericInstruction xor(Instruction i1, Instruction i2) {
		return generic(BitwiseOperator.XOR.getLiteral(), ConversionMatrix.getCommon(i1.getType(), i2.getType()), i1, i2);
	}

	public static GenericInstruction max(Instruction ... a) {
		return reduction(ReductionOperator.MAX.getLiteral(), a);
	}

	public static GenericInstruction min(Instruction ... a) {
		return reduction(ReductionOperator.MIN.getLiteral(), a);
	}

	public static GenericInstruction ellipses(Instruction ... a) {
		return reduction("ellipses", a);
	}

	public static GenericInstruction _throw(Instruction a) {
		return generic("throw", GecosUserTypeFactory.VOID(), a);
	}

}
