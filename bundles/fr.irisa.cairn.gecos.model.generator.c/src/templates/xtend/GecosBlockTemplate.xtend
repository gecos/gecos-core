package templates.xtend

import fr.irisa.cairn.gecos.model.c.generator.ExtendableAnnotationCGenerator
import fr.irisa.cairn.gecos.model.c.generator.ExtendableBlockCGenerator
import fr.irisa.cairn.gecos.model.c.generator.ExtendableCoreCGenerator
import fr.irisa.cairn.gecos.model.c.generator.ExtendableInstructionCGenerator
import fr.irisa.cairn.gecos.model.c.generator.ExtendableTypeCGenerator
import gecos.annotations.AnnotatedElement
import gecos.annotations.CommentAnnotation
import gecos.annotations.IAnnotation
import gecos.blocks.BasicBlock
import gecos.blocks.Block
import gecos.blocks.CompositeBlock
import gecos.blocks.DoWhileBlock
import gecos.blocks.ForBlock
import gecos.blocks.ForC99Block
import gecos.blocks.IfBlock
import gecos.blocks.SimpleForBlock
import gecos.blocks.SwitchBlock
import gecos.blocks.WhileBlock
import gecos.core.Procedure
import gecos.core.Scope
import gecos.core.Symbol
import gecos.instrs.DummyInstruction
import gecos.instrs.Instruction
import gecos.instrs.LabelInstruction
import org.eclipse.emf.common.util.EMap
import org.eclipse.emf.ecore.EObject

class GecosBlockTemplate extends GecosTemplate {

	String comma
	String jump
	EMap<Long, Block> caseMaps
	Block defaultBlock

	static GecosBlockTemplate singleton;
	
	new () {
		singleton = this;
	}
	def static getInstance() {
		singleton
	}
	def dispatch generate(BasicBlock basicBlock) {
		
		val buffer = new StringBuffer()
		buffer.append('''
		''')
		
		buffer.append('''«generateAnnotations(basicBlock)»''')
		var needBraces = false; 
		if (basicBlock.pragma !== null && !basicBlock.pragma.content.empty) {
			needBraces = true;
			buffer.append('''{
				''')
		}	
		
		for (Instruction instruction : basicBlock.instructions) {
			if (instruction.pragma !== null && !instruction.pragma.content.empty)
				buffer.append('''«ExtendableAnnotationCGenerator::eInstance.generate(instruction.pragma)»''')
			buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(instruction)»''')
			if (!(instruction instanceof DummyInstruction || instruction instanceof LabelInstruction))
				buffer.append('''«comma»''')
			if (instruction instanceof LabelInstruction && addLabelComma(instruction as LabelInstruction, basicBlock)) 
				buffer.append('''«comma»''')
			if (!(instruction instanceof DummyInstruction))
				buffer.append('''«jump»''')
		}
		
		if(needBraces)
			buffer.append('''
			}
			''')
		
		'''«buffer»'''
	}
	
	def addLabelComma(LabelInstruction instruction, BasicBlock block) {
		var b1 = block.instructions.indexOf(instruction) == (block.instructionCount -1)
		var b2 = block.parent instanceof CompositeBlock && ((block.parent as CompositeBlock).children.indexOf(block) == (block.parent as CompositeBlock).children.size-1);
		return (b1 && b2)
	}

	def dispatch generate(CompositeBlock compositeBlock) {
		comma = ";"
		jump = "
"
//		val gecosCoreTemplate = new GecosCoreTemplate()
        if (compositeBlock.eContainer instanceof Procedure &&
            (compositeBlock.children.size > 1 && compositeBlock.children.get(1) instanceof CompositeBlock)) {
            '''«generateAnnotations(compositeBlock)»«ExtendableBlockCGenerator::eInstance.generate(compositeBlock.children.get(1))»'''
		} else {
			val buffer = new StringBuffer()
			buffer.append('''«printCase(compositeBlock)»«generateAnnotations(compositeBlock)»{
				''')
			if (compositeBlock.scope !== null /* && compositeBlock.scope.symbols.size > 0*/ ) {
				val scopeGen = ExtendableCoreCGenerator::eInstance.generate(compositeBlock.scope)
//				val scopeGen = gecosCoreTemplate.generate(compositeBlock.scope)
				if(scopeGen.length > 0) 
					buffer.append('''	«scopeGen»''')
			}
			for (Block block : compositeBlock.children) {
				val charSequence = ExtendableBlockCGenerator::eInstance.generate(block)
				if (!charSequence.toString.equals(""))
					buffer.append('''	«printCase(block)»«charSequence»''')
			}
			buffer.append('''}''')
			if (compositeBlock.eContainer instanceof CompositeBlock && !(compositeBlock.eContainer.eContainer instanceof Procedure))
				buffer.append(jump)
			'''«buffer.toString»'''
		}
	}

	def dispatch generate(ForBlock forBlock) {
		comma = ""
		jump = ""
		val buffer = new StringBuffer()
		buffer.append('''
		''')
		buffer.append(generateAnnotations(forBlock))
		buffer.append('''for(«ExtendableBlockCGenerator::eInstance.generate(forBlock.initBlock)»; «ExtendableBlockCGenerator::eInstance.generate(forBlock.testBlock)»; «ExtendableBlockCGenerator::eInstance.generate(forBlock.stepBlock)»)''')
		comma = ";"
		jump = "
"
		if (forBlock.bodyBlock instanceof CompositeBlock) {
			buffer.append(''' ''')
			buffer.append('''«ExtendableBlockCGenerator::eInstance.generate(forBlock.bodyBlock)»''')
		}else if(forBlock.bodyBlock instanceof BasicBlock && (forBlock.bodyBlock as BasicBlock).instructionCount > 1) {
			buffer.append(''' {
				''')
			buffer.append('''	«ExtendableBlockCGenerator::eInstance.generate(forBlock.bodyBlock)»''')
			buffer.append('''
			}''')		
		}else if (forBlock.bodyBlock !== null)
			buffer.append('''
			
				«ExtendableBlockCGenerator::eInstance.generate(forBlock.bodyBlock)»''')
		else
			//body block is null
			buffer.append("/* null body block */;");
		'''«buffer»
		'''
	}

	def dispatch generate(ForC99Block forC99Block) {
		comma = ""
		jump = ""
		val buffer = new StringBuffer()
		buffer.append('''«generateAnnotations(forC99Block)»for(''')
		buffer.append('''«printForScope(forC99Block.scope)»; ''')
		buffer.append('''«ExtendableBlockCGenerator::eInstance.generate(forC99Block.testBlock)»; «ExtendableBlockCGenerator::eInstance.generate(forC99Block.stepBlock)»)''')
		comma = ";"
		jump = "
"

		if (forC99Block.bodyBlock instanceof CompositeBlock) {
			buffer.append(''' ''')
			buffer.append('''«ExtendableBlockCGenerator::eInstance.generate(forC99Block.bodyBlock)»''')
		}else if(forC99Block.bodyBlock instanceof BasicBlock && (forC99Block.bodyBlock as BasicBlock).instructionCount > 1) {
			buffer.append(''' {
				''')
			buffer.append('''	«ExtendableBlockCGenerator::eInstance.generate(forC99Block.bodyBlock)»''')
			buffer.append('''
			}''')		
		}else if (forC99Block.bodyBlock !== null)
			buffer.append('''
			
				«ExtendableBlockCGenerator::eInstance.generate(forC99Block.bodyBlock)»''')
		else
			//body block is null
			buffer.append("/* null body block */;");
		'''«buffer»
		'''
	}

	def dispatch generate(IfBlock ifBlock) {
		comma = ""
		jump = ""
		val buffer = new StringBuffer()
		buffer.append('''
		«generateAnnotations(ifBlock)»if («ExtendableBlockCGenerator::eInstance.generate(ifBlock.testBlock)»)''')
		comma = ";"
		jump = "
"
		if (ifBlock.thenBlock instanceof CompositeBlock) {
			buffer.append(''' ''')
			buffer.append('''«ExtendableBlockCGenerator::eInstance.generate(ifBlock.thenBlock)»''')
			if (ifBlock.elseBlock !== null)
				buffer.append(''' ''')
		}else if(ifBlock.thenBlock instanceof BasicBlock && (ifBlock.thenBlock as BasicBlock).instructionCount > 1) {
			buffer.append(''' {
				''')
			buffer.append('''	«ExtendableBlockCGenerator::eInstance.generate(ifBlock.thenBlock)»''')
			buffer.append('''
			}''')
			if (ifBlock.elseBlock !== null)
				buffer.append(''' ''')
		}else {
			buffer.append("
")
			buffer.append('''	«ExtendableBlockCGenerator::eInstance.generate(ifBlock.thenBlock)»''')
		}
		
		if (ifBlock.elseBlock !== null) {
			if (ifBlock.elseBlock instanceof CompositeBlock) {
				buffer.append('''else ''')
				buffer.append('''«ExtendableBlockCGenerator::eInstance.generate(ifBlock.elseBlock)»''')
			}else if(ifBlock.elseBlock instanceof BasicBlock && (ifBlock.elseBlock as BasicBlock).instructionCount > 1) {
				buffer.append('''else {
					''')
				buffer.append('''	«ExtendableBlockCGenerator::eInstance.generate(ifBlock.elseBlock)»''')
				buffer.append('''
				}''')		
			}else {
				buffer.append('''else
				''')
				buffer.append('''	«ExtendableBlockCGenerator::eInstance.generate(ifBlock.elseBlock)»''')
			}
		}
		'''«buffer.toString»
		'''
	}
	//FIXME if the bodyBlock is a basicBlock with more than 1 instruction then add {}
	def dispatch generate(DoWhileBlock loopBlock) {
		val buffer = new StringBuffer()
		buffer.append('''
		«generateAnnotations(loopBlock)»do «ExtendableBlockCGenerator::eInstance.generate(loopBlock.getBodyBlock)» while (''')
		comma = ""
		jump = ""
		if (loopBlock.getTestBlock === null) {
			buffer.append('''1 /* warning: test block is null */''')
		} else {
			buffer.append('''«ExtendableBlockCGenerator::eInstance.generate(loopBlock.getTestBlock)»''')
		}
		buffer.append(''');
		''')
		comma = ";"
		jump = "
"
		'''«buffer»'''
	}
 
	def dispatch generate(SimpleForBlock simpleForBlock) {
		comma = ""
		jump = ""
		val buffer = new StringBuffer()
		buffer.append('''«generateAnnotations(simpleForBlock)»for(''')
		if ((simpleForBlock.initBlock as BasicBlock).instructionCount == 0)
			buffer.append('''«printForScope(simpleForBlock.scope)»; ''')
		else
			buffer.append('''«ExtendableBlockCGenerator::eInstance.generate(simpleForBlock.initBlock)»; ''')
		buffer.append('''«ExtendableBlockCGenerator::eInstance.generate(simpleForBlock.testBlock)»; «ExtendableBlockCGenerator::eInstance.generate(simpleForBlock.stepBlock)»)''')
		comma = ";"
		jump = "
"
		if (simpleForBlock.bodyBlock instanceof CompositeBlock) {
			buffer.append(''' ''')
			buffer.append('''«ExtendableBlockCGenerator::eInstance.generate(simpleForBlock.bodyBlock)»''')
		}else if(simpleForBlock.bodyBlock instanceof BasicBlock && (simpleForBlock.bodyBlock as BasicBlock).instructionCount == 1) {
					buffer.append('''
			
				«ExtendableBlockCGenerator::eInstance.generate(simpleForBlock.bodyBlock)»''')		
		}else if (simpleForBlock.bodyBlock !== null){				
				buffer.append(''' {
				''')
			buffer.append('''	«ExtendableBlockCGenerator::eInstance.generate(simpleForBlock.bodyBlock)»''')
			buffer.append('''
			}''')		
		} else
			//body block is null
			buffer.append("/* null body block */;");
		'''«buffer»
		'''
	}

	def dispatch generate(SwitchBlock switchBlock) {
		comma = ""
		jump = ""
		val oldCaseMaps = caseMaps
		val oldDefaultBlock = defaultBlock
		val buffer = new StringBuffer()
		caseMaps = switchBlock.cases
		defaultBlock = switchBlock.defaultBlock
		buffer.append('''«generateAnnotations(switchBlock)»switch («ExtendableBlockCGenerator::eInstance.generate(switchBlock.dispatchBlock)»)''')
		buffer.append('''«ExtendableBlockCGenerator::eInstance.generate(switchBlock.bodyBlock)»
		''')
		caseMaps = oldCaseMaps 
		defaultBlock = oldDefaultBlock
		'''«buffer»'''
	}

	def dispatch generate(WhileBlock whileBlock) {
		comma = ""
		jump = ""
		val buffer = new StringBuffer()
		buffer.append('''«generateAnnotations(whileBlock)»while (''')
		if (whileBlock.testBlock === null) {
			buffer.append('''1 /* warning: test block is null */''')
		} else {
			buffer.append('''«ExtendableBlockCGenerator::eInstance.generate(whileBlock.testBlock)»''')
		}
		buffer.append(''')''')
		
		comma = ";"
		jump = "
"
		if(whileBlock.bodyBlock instanceof BasicBlock && (whileBlock.bodyBlock as BasicBlock).instructionCount > 1) {
			buffer.append(''' {
				''')
			buffer.append('''	«ExtendableBlockCGenerator::eInstance.generate(whileBlock.bodyBlock)»''')
			buffer.append('''
			}''')		
		}
		else if (!(whileBlock.bodyBlock instanceof BasicBlock && ((whileBlock.bodyBlock as BasicBlock).instructions.size == 0))) {
			buffer.append(''' ''')
			buffer.append('''«ExtendableBlockCGenerator::eInstance.generate(whileBlock.bodyBlock)»
			''')
		} else
			buffer.append(''';
			''')

		'''«buffer»'''
	}

	def dispatch generate(EObject object) {
		null
	}
	
	def printCase(Block block) {
		val buffer = new StringBuffer()
		if (caseMaps !== null && caseMaps.containsValue(block))
			for(Long l : caseMaps.keySet) {
				if (block == caseMaps.get(l)) {
					buffer.append('''case «l» :
					''')
				}
			}
		if (defaultBlock !== null && block == defaultBlock)
			buffer.append('''	default :
			''')
		'''«buffer»'''
	}
	
	def generateAnnotations(AnnotatedElement annotatedElement) {
		val res = new StringBuffer();
		for (IAnnotation a : annotatedElement.annotations.values) {
			if (a instanceof CommentAnnotation) {
				res.append("/* "+(a as CommentAnnotation).content+" */\n")
			}
		}
		if (annotatedElement.pragma !== null && !annotatedElement.pragma.content.empty) 
			res.append(ExtendableAnnotationCGenerator::eInstance.generate(annotatedElement.pragma))
		'''«res»'''
	}
	
	def printForScope(Scope scope) {
		val buffer = new StringBuffer()
		for(Symbol symbol : scope.symbols) {
			if (symbol.equals(scope.symbols.get(0)))
				buffer.append('''«ExtendableTypeCGenerator::eInstance.generate(symbol.type)»''')
			buffer.append(''' «symbol.name»''')
			if (symbol.value !== null)
				buffer.append(''' = «ExtendableInstructionCGenerator::eInstance.generate(symbol.value)»''')
			if (!symbol.equals(scope.symbols.get(scope.symbols.size - 1)))
				buffer.append(''',''')
		}
		buffer.append('''''')
	}
}