package fr.irisa.cairn.gecos.model.tools.utils;

import gecos.blocks.Block;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.r2d2.gecos.framework.GSModule;

@GSModule("Fixes incorrectly numbered blocks in the IR to ensure that\n"
		+ "each block has its own unique number.\n"
		+ "For instance, this is important when trying to output a CDFG into a dotty file.")
public class FixBlockNumbers {
	
	private List<Procedure> l;
	
	public FixBlockNumbers(GecosProject ps) {
		l = ps.listProcedures();
	}

	public FixBlockNumbers(ProcedureSet ps) {
		l = ps.listProcedures();
	}
	
	public FixBlockNumbers(Procedure p) {
		(l = new ArrayList<Procedure>(1)).add(p);
	}
	
	public void compute() {
		for (Procedure p : l) new BlockNumberFixer().fix(p);
	}
	
	public static void fix(ProcedureSet ps) { new FixBlockNumbers(ps).compute(); }
	public static void fix(Procedure p) { new FixBlockNumbers(p).compute(); }
	
	public class BlockNumberFixer extends BlocksDefaultSwitch<Object> {
		
		int count;
		
		protected BlockNumberFixer() {
			count = 0;
		}
		
		protected void fix(Procedure p) { 
			doSwitch(p); 
		}
		
		@Override
		public Object caseBlock(Block object) {
			object.setNumber(count);
			count++;
			return null;
		}

		
	}

}
