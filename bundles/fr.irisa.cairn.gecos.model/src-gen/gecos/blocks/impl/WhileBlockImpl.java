/**
 */
package gecos.blocks.impl;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;

import fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;
import fr.irisa.cairn.gecos.model.tools.utils.BlocksContinuesBreaksFinder;
import fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksFactory;
import gecos.blocks.BlocksPackage;
import gecos.blocks.BlocksVisitor;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ControlEdge;
import gecos.blocks.WhileBlock;

import gecos.instrs.BranchType;
import gecos.instrs.BreakInstruction;
import gecos.instrs.ContinueInstruction;

import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>While Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.blocks.impl.WhileBlockImpl#getTestBlock <em>Test Block</em>}</li>
 *   <li>{@link gecos.blocks.impl.WhileBlockImpl#getBodyBlock <em>Body Block</em>}</li>
 * </ul>
 *
 * @generated
 */
public class WhileBlockImpl extends BlockImpl implements WhileBlock {
	/**
	 * The cached value of the '{@link #getTestBlock() <em>Test Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestBlock()
	 * @generated
	 * @ordered
	 */
	protected Block testBlock;

	/**
	 * The cached value of the '{@link #getBodyBlock() <em>Body Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBodyBlock()
	 * @generated
	 * @ordered
	 */
	protected Block bodyBlock;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WhileBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BlocksPackage.Literals.WHILE_BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block getTestBlock() {
		return testBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTestBlock(Block newTestBlock, NotificationChain msgs) {
		Block oldTestBlock = testBlock;
		testBlock = newTestBlock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BlocksPackage.WHILE_BLOCK__TEST_BLOCK, oldTestBlock, newTestBlock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTestBlock(Block newTestBlock) {
		if (newTestBlock != testBlock) {
			NotificationChain msgs = null;
			if (testBlock != null)
				msgs = ((InternalEObject)testBlock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.WHILE_BLOCK__TEST_BLOCK, null, msgs);
			if (newTestBlock != null)
				msgs = ((InternalEObject)newTestBlock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.WHILE_BLOCK__TEST_BLOCK, null, msgs);
			msgs = basicSetTestBlock(newTestBlock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.WHILE_BLOCK__TEST_BLOCK, newTestBlock, newTestBlock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block getBodyBlock() {
		return bodyBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBodyBlock(Block newBodyBlock, NotificationChain msgs) {
		Block oldBodyBlock = bodyBlock;
		bodyBlock = newBodyBlock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BlocksPackage.WHILE_BLOCK__BODY_BLOCK, oldBodyBlock, newBodyBlock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBodyBlock(Block newBodyBlock) {
		if (newBodyBlock != bodyBlock) {
			NotificationChain msgs = null;
			if (bodyBlock != null)
				msgs = ((InternalEObject)bodyBlock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.WHILE_BLOCK__BODY_BLOCK, null, msgs);
			if (newBodyBlock != null)
				msgs = ((InternalEObject)newBodyBlock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.WHILE_BLOCK__BODY_BLOCK, null, msgs);
			msgs = basicSetBodyBlock(newBodyBlock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.WHILE_BLOCK__BODY_BLOCK, newBodyBlock, newBodyBlock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final BlocksVisitor visitor) {
		visitor.visitWhileBlock(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BasicBlock> listContinueBlocks() {
		final BlocksContinuesBreaksFinder finder = new BlocksContinuesBreaksFinder();
		finder.doSwitch(this.getBodyBlock());
		return ECollections.<BasicBlock>unmodifiableEList(finder.getContinues());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ContinueInstruction> listContinueInstructions() {
		final BranchInstructionFinder finder = new BranchInstructionFinder();
		finder.doSwitch(this.getBodyBlock());
		return ECollections.<ContinueInstruction>unmodifiableEList(finder.getContinues());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BasicBlock> listBreakBlocks() {
		final BlocksContinuesBreaksFinder finder = new BlocksContinuesBreaksFinder();
		finder.doSwitch(this.getBodyBlock());
		return ECollections.<BasicBlock>unmodifiableEList(finder.getBreaks());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BreakInstruction> listBreakInstructions() {
		final BranchInstructionFinder finder = new BranchInstructionFinder();
		finder.doSwitch(this.getBodyBlock());
		return ECollections.<BreakInstruction>unmodifiableEList(finder.getBreaks());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block managedCopy(final BlockCopyManager mng) {
		final WhileBlock whileBlock = BlocksFactory.eINSTANCE.createWhileBlock();
		whileBlock.setTestBlock(this.getTestBlock().managedCopy(mng));
		whileBlock.setBodyBlock(this.getBodyBlock().managedCopy(mng));
		GecosUserAnnotationFactory.copyAnnotations(this, whileBlock);
		mng.link(this, whileBlock);
		return whileBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void mergeWithBasic(final BasicBlock to) {
		throw new UnsupportedOperationException("Not yet implemented");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block mergeWithComposite(final CompositeBlock compositeBlock) {
		throw new UnsupportedOperationException("Not yet implemented");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block simplifyBlock() {
		throw new UnsupportedOperationException("Not yet implemented");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addBlockAfter(final Block a, final Block b) {
		throw new UnsupportedOperationException("Not yet implemented");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeBlock(final Block bb) {
		Block _testBlock = this.getTestBlock();
		boolean _tripleEquals = (_testBlock == bb);
		if (_tripleEquals) {
			this.setTestBlock(null);
		}
		else {
			Block _bodyBlock = this.getBodyBlock();
			boolean _tripleEquals_1 = (_bodyBlock == bb);
			if (_tripleEquals_1) {
				this.setBodyBlock(null);
			}
			else {
				Block _bodyBlock_1 = this.getBodyBlock();
				boolean _tripleNotEquals = (_bodyBlock_1 != null);
				if (_tripleNotEquals) {
					this.getBodyBlock().removeBlock(bb);
				}
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void mergeChildComposite(final CompositeBlock b) {
		throw new UnsupportedOperationException("Not yet implemented");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlEdge connectFromBasic(final BasicBlock dispatch, final BranchType cond) {
		return this.getTestBlock().connectFromBasic(dispatch, cond);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlEdge> connectTo(final Block next, final BranchType cond) {
		final EList<ControlEdge> res = this.getTestBlock().connectTo(next, BranchType.IF_FALSE);
		final BlocksContinuesBreaksFinder visitor = new BlocksContinuesBreaksFinder();
		visitor.doSwitch(this.getBodyBlock());
		List<BasicBlock> _breaks = visitor.getBreaks();
		for (final BasicBlock bb : _breaks) {
			{
				ControlEdge[] _clone = ((ControlEdge[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(bb.getOutEdges(), ControlEdge.class)).clone();
				for (final ControlEdge e : _clone) {
					{
						e.setFrom(null);
						e.setTo(null);
					}
				}
				bb.getOutEdges().clear();
				res.addAll(bb.connectTo(next, BranchType.UNCONDITIONAL));
			}
		}
		return res;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void replace(final Block oldb, final Block newb) {
		Block _testBlock = this.getTestBlock();
		boolean _tripleEquals = (oldb == _testBlock);
		if (_tripleEquals) {
			this.setTestBlock(newb);
		}
		else {
			Block _bodyBlock = this.getBodyBlock();
			boolean _tripleEquals_1 = (oldb == _bodyBlock);
			if (_tripleEquals_1) {
				this.setBodyBlock(newb);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPathString() {
		return super.getPathString(this.toShortString());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toShortString() {
		int _number = this.getNumber();
		return ("While_" + Integer.valueOf(_number));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BlocksPackage.WHILE_BLOCK__TEST_BLOCK:
				return basicSetTestBlock(null, msgs);
			case BlocksPackage.WHILE_BLOCK__BODY_BLOCK:
				return basicSetBodyBlock(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BlocksPackage.WHILE_BLOCK__TEST_BLOCK:
				return getTestBlock();
			case BlocksPackage.WHILE_BLOCK__BODY_BLOCK:
				return getBodyBlock();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BlocksPackage.WHILE_BLOCK__TEST_BLOCK:
				setTestBlock((Block)newValue);
				return;
			case BlocksPackage.WHILE_BLOCK__BODY_BLOCK:
				setBodyBlock((Block)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BlocksPackage.WHILE_BLOCK__TEST_BLOCK:
				setTestBlock((Block)null);
				return;
			case BlocksPackage.WHILE_BLOCK__BODY_BLOCK:
				setBodyBlock((Block)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BlocksPackage.WHILE_BLOCK__TEST_BLOCK:
				return testBlock != null;
			case BlocksPackage.WHILE_BLOCK__BODY_BLOCK:
				return bodyBlock != null;
		}
		return super.eIsSet(featureID);
	}

} //WhileBlockImpl
