/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.cdtfrontend.visitor;

import java.util.List;
import java.util.Map;

import org.eclipse.cdt.core.dom.ast.IASTBreakStatement;
import org.eclipse.cdt.core.dom.ast.IASTCaseStatement;
import org.eclipse.cdt.core.dom.ast.IASTCompoundStatement;
import org.eclipse.cdt.core.dom.ast.IASTContinueStatement;
import org.eclipse.cdt.core.dom.ast.IASTDeclarationStatement;
import org.eclipse.cdt.core.dom.ast.IASTDefaultStatement;
import org.eclipse.cdt.core.dom.ast.IASTDoStatement;
import org.eclipse.cdt.core.dom.ast.IASTExpression;
import org.eclipse.cdt.core.dom.ast.IASTExpressionStatement;
import org.eclipse.cdt.core.dom.ast.IASTForStatement;
import org.eclipse.cdt.core.dom.ast.IASTGotoStatement;
import org.eclipse.cdt.core.dom.ast.IASTIfStatement;
import org.eclipse.cdt.core.dom.ast.IASTLabelStatement;
import org.eclipse.cdt.core.dom.ast.IASTNullStatement;
import org.eclipse.cdt.core.dom.ast.IASTProblemStatement;
import org.eclipse.cdt.core.dom.ast.IASTReturnStatement;
import org.eclipse.cdt.core.dom.ast.IASTStatement;
import org.eclipse.cdt.core.dom.ast.IASTSwitchStatement;
import org.eclipse.cdt.core.dom.ast.IASTWhileStatement;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTCatchHandler;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTForStatement;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTIfStatement;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTSwitchStatement;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTTryBlockStatement;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTWhileStatement;

import fr.irisa.cairn.gecos.model.cdtfrontend.CDTFrontEnd;
import fr.irisa.cairn.gecos.model.cdtfrontend.exception.CDTParserErrorException;
import fr.irisa.cairn.gecos.model.cdtfrontend.exception.CDTParserNotYetSupportedException;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.tools.visitors.simpleforloops.SimpleLoop;
import gecos.annotations.AnnotatedElement;
import gecos.annotations.PragmaAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksFactory;
import gecos.blocks.CompositeBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.ForC99Block;
import gecos.blocks.IfBlock;
import gecos.blocks.SwitchBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.BreakInstruction;
import gecos.instrs.CondInstruction;
import gecos.instrs.ContinueInstruction;
import gecos.instrs.GotoInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.SymbolInstruction;

//see Scope creation for different Blocks

/**
 * @author mnaullet
 * 
 * FIXME: Post Increment/Decrement operations are not handled correctly in many cases !!
 */
public class ASTStatementVisitor {
	
	private static final String labelName = "_gecos_label";
	//amorvan : reset the counter when building a new project (see CDTFrontEnd)
	public static int labelCounter = 0;

	private CDTToGecosModel gecosModel;
	private List<Instruction> beforeInstructions;
	private List<Instruction> afterInstructions;
	private Map<String, Instruction> declarations;

	private Block block;
	private static int blockNumber;

	public ASTStatementVisitor() {
	}

	public void visit(CDTToGecosModel gecosModel, IASTStatement statement) {
		this.gecosModel = gecosModel;
		if (statement instanceof IASTBreakStatement)
			buildIASTBreakStatement((IASTBreakStatement)statement);
		else if (statement instanceof IASTCaseStatement)
			buildIASTCaseStatement((IASTCaseStatement)statement);
		else if (statement instanceof IASTCompoundStatement)
			buildIASTCompoundStatement((IASTCompoundStatement)statement);
		else if (statement instanceof IASTContinueStatement)
			buildIASTContinueStatement((IASTContinueStatement)statement);
		else if (statement instanceof IASTDeclarationStatement)
			buildIASTDeclarationStatement((IASTDeclarationStatement)statement);
		else if (statement instanceof IASTDefaultStatement)
			buildIASTDefaultStatement((IASTDefaultStatement)statement);
		else if (statement instanceof IASTDoStatement)
			buildIASTDoStatement((IASTDoStatement)statement);
		else if (statement instanceof IASTExpressionStatement)
			buildIASTExpressionStatement((IASTExpressionStatement)statement);
		else if (statement instanceof IASTForStatement)
			if (statement instanceof ICPPASTForStatement)
				buildICPPASTForStatement((ICPPASTForStatement)statement);
			else
				buildIASTForStatement((IASTForStatement)statement);
		else if (statement instanceof IASTGotoStatement)
			buildIASTGotoStatement((IASTGotoStatement)statement);
		else if (statement instanceof IASTIfStatement)
			if (statement instanceof ICPPASTIfStatement)
				buildICPPASTIfStatement((ICPPASTIfStatement)statement);
			else
				buildIASTIfStatement((IASTIfStatement)statement);
		else if (statement instanceof IASTLabelStatement)
			buildIASTLabelStatement((IASTLabelStatement)statement);
		else if (statement instanceof IASTNullStatement)
			buildIASTNullStatement((IASTNullStatement)statement);
		else if (statement instanceof IASTProblemStatement)
			buildIASTProblemStatement((IASTProblemStatement)statement);
		else if (statement instanceof IASTReturnStatement)
			buildIASTReturnStatement((IASTReturnStatement)statement);
		else if (statement instanceof IASTSwitchStatement)
			if (statement instanceof ICPPASTSwitchStatement)
				buildICPPASTSwitchStatement((ICPPASTSwitchStatement)statement);
			else
				buildIASTSwitchStatement((IASTSwitchStatement)statement);
		else if (statement instanceof IASTWhileStatement)
			if (statement instanceof ICPPASTWhileStatement)
				buildICPPASTWhileStatement((ICPPASTWhileStatement)statement);
			else
				buildIASTWhileStatement((IASTWhileStatement)statement);
		else if (statement instanceof ICPPASTCatchHandler)
			buildICPPASTCatchHandler((ICPPASTCatchHandler)statement);
		else if (statement instanceof ICPPASTTryBlockStatement)
			buildICPPASTTryBlockStatement((ICPPASTTryBlockStatement)statement);
	
		if (!gecosModel.visitingInclude())
			gecosModel.addIASTAnnotation(block, statement);
		
		gecosModel.addFileLocationAnnotation(block, statement.getFileLocation());
	}

	public Block getBlock() {
		return block;
	}
	
	public Map<String, Instruction> getDeclarations() {
		return declarations;
	}

	private void buildIASTBreakStatement(IASTBreakStatement statement) {
		if (gecosModel.visitingInclude())
			notYetSupported(statement);
		BasicBlock basicBlock = gecosModel.getGecosControlStructureFactory().createBasicBlock();
		basicBlock.addInstruction(GecosUserInstructionFactory.breakInst());
		block = basicBlock;
	}

	private void buildIASTCaseStatement(IASTCaseStatement statement) {
		if (gecosModel.visitingInclude())
			notYetSupported(statement);
		statement.getExpression().accept(gecosModel);
		ASTExpressionVisitor expressionVisitor = (ASTExpressionVisitor)gecosModel.getObject();
		if (expressionVisitor.getInstruction() instanceof IntInstruction)
			gecosModel.getGecosControlStructureFactory().createCaseBlock(((IntInstruction)expressionVisitor.getInstruction()).getValue());
		else
			gecosModel.getGecosControlStructureFactory().createCaseBlock(0);
		block = null;
	}

	private void buildIASTCompoundStatement(IASTCompoundStatement statement) {
		if (gecosModel.visitingInclude())
			notYetSupported(statement);
		//XXX - push()
		Scope scope = gecosModel.getGecosCoreContextedFactory().createScope();
		CompositeBlock compositeBlock = gecosModel.getGecosControlStructureFactory().createCompositeBlock(scope);
		compositeBlock.setNumber(blockNumber++);
		Block parentBlock = gecosModel.getGecosCoreContextedFactory().getParentBlock();
		Block previousBlock = null;
		gecosModel.getGecosCoreContextedFactory().setParentBlock(compositeBlock);
		addPragma(compositeBlock, statement);
		for(IASTStatement stat : statement.getStatements()) {
			stat.accept(gecosModel);
			Block blockToBeAdded = ((ASTStatementVisitor)gecosModel.getObject()).getBlock();
			if (blockToBeAdded != null) {
				if (compositeBlock.getChildren().size() > 0)
					previousBlock = compositeBlock.getChildren().get(compositeBlock.getChildren().size() - 1);
				boolean add = false;
				boolean previousIsBasic = previousBlock != null && previousBlock instanceof BasicBlock;
				boolean blockIsBasic = blockToBeAdded instanceof BasicBlock;
				if (blockIsBasic && previousIsBasic) {
					boolean blockIsNotCase = !(gecosModel.getGecosControlStructureFactory().isCasedBlock(blockToBeAdded));
					boolean hasMoreThanOneInstr = ((BasicBlock)previousBlock).getInstructionCount() > 0;
					if (blockIsNotCase && hasMoreThanOneInstr) {
						Instruction instruction = ((BasicBlock)previousBlock).getInstruction(((BasicBlock)previousBlock).getInstructionCount()-1);
						add = !(instruction instanceof GotoInstruction) && !(instruction instanceof ContinueInstruction) && !(instruction instanceof BreakInstruction);						
					}
				}
				if (add)
					((BasicBlock)previousBlock).getInstructions().addAll(((BasicBlock)blockToBeAdded).getInstructions());
				else
					compositeBlock.addChildren(blockToBeAdded);
			}
		}
		//XXX - pop()
		gecosModel.getGecosCoreContextedFactory().goToUpperScope();
		gecosModel.getGecosCoreContextedFactory().setParentBlock(parentBlock);
		addDummyInstruction(compositeBlock, statement);
//		gecosModel.addBlock(parentBlock, statement);
		this.block = compositeBlock;
	}

	private void buildIASTContinueStatement(IASTContinueStatement statement) {
		if (gecosModel.visitingInclude())
			notYetSupported(statement);
		BasicBlock basicBlock = gecosModel.getGecosControlStructureFactory().createBasicBlock();
		basicBlock.addInstruction(GecosUserInstructionFactory.continueInst());
		block = basicBlock;
	}

	private void buildIASTDeclarationStatement(IASTDeclarationStatement statement) {
		if (gecosModel.visitingInclude())
			notYetSupported(statement);
		statement.getDeclaration().accept(gecosModel);
		declarations = ((ASTDeclarationVisitor)gecosModel.getObject()).getDeclarations();
	}

	private void buildIASTDefaultStatement(IASTDefaultStatement statement) {
		if (gecosModel.visitingInclude())
			notYetSupported(statement);
		BasicBlock basicBlock = gecosModel.getGecosControlStructureFactory().createBasicBlock();
		block = basicBlock;
	}

	private void buildIASTDoStatement(IASTDoStatement statement) {
		if (gecosModel.visitingInclude())
			notYetSupported(statement);
		DoWhileBlock loopBlock = gecosModel.getGecosControlStructureFactory().createLoopBlock();
		addPragma(loopBlock, statement);
		statement.getCondition().accept(gecosModel);
		Instruction test = ((ASTExpressionVisitor)gecosModel.getObject()).getInstruction();
		beforeInstructions = ((ASTExpressionVisitor)gecosModel.getObject()).getBeforeInstructions();
		afterInstructions = ((ASTExpressionVisitor)gecosModel.getObject()).getAfterInstructions();
		loopBlock.setTestBlock(gecosModel.getGecosControlStructureFactory().createBasicBlock(test));
		statement.getBody().accept(gecosModel);
		Block block = ((ASTStatementVisitor)gecosModel.getObject()).getBlock();
		if (!(block instanceof CompositeBlock)) {
			block = GecosUserBlockFactory.CompositeBlock(block);
		} 
		loopBlock.setBodyBlock(block);
		setBeforeAfterInstruction((CompositeBlock)loopBlock.getBodyBlock());
		this.block = loopBlock;
		buildTest(loopBlock.getTestBlock(), loopBlock.getBodyBlock());
	}

	private void buildIASTExpressionStatement(IASTExpressionStatement statement) {
		if (gecosModel.visitingInclude())
			notYetSupported(statement);
		statement.getExpression().accept(gecosModel);
		ASTExpressionVisitor expressionVisitor = (ASTExpressionVisitor)gecosModel.getObject();
		BasicBlock basicBlock = gecosModel.getGecosControlStructureFactory().createBasicBlock();
		addPragma(expressionVisitor.getInstruction(), statement);
		beforeInstructions = expressionVisitor.getBeforeInstructions();
		afterInstructions = expressionVisitor.getAfterInstructions();
		if (beforeInstructions != null)
			for(int i = 0; i < beforeInstructions.size(); i++)
				basicBlock.addInstruction(beforeInstructions.get(i));
//		if (!(expressionVisitor.getInstruction() instanceof FieldInstruction)
//				&& ! (expressionVisitor.getInstruction() instanceof SymbolInstruction))
			basicBlock.addInstruction(expressionVisitor.getInstruction());
		if (afterInstructions != null)
			for(int i = 0; i < afterInstructions.size(); i++)
				basicBlock.addInstruction(afterInstructions.get(i));
		block = basicBlock;
	}

	private void buildIASTForStatement(IASTForStatement statement) {
		if (gecosModel.visitingInclude())
			notYetSupported(statement);
		ForBlock forBlock = gecosModel.getGecosControlStructureFactory().createForBlock();
		Block parentBlock = null;
		IASTStatement initializerStatement = statement.getInitializerStatement();
		BasicBlock init = null;
		if (initializerStatement!=null) {
			if (initializerStatement instanceof IASTDeclarationStatement) {
				//ForC99Block
				parentBlock = gecosModel.getGecosCoreContextedFactory().getParentBlock();
				//XXX - push()
				Scope scope = gecosModel.getGecosCoreContextedFactory().createScope();
				forBlock = gecosModel.getGecosControlStructureFactory().createForC99Block(scope);
				((CompositeBlock)parentBlock).addChildren(forBlock);
				gecosModel.getGecosCoreContextedFactory().setParentBlock(forBlock);
			}
			addPragma(forBlock, statement);
			initializerStatement.accept(gecosModel);
			init = (BasicBlock)((ASTStatementVisitor)gecosModel.getObject()).getBlock();
			declarations = ((ASTStatementVisitor)gecosModel.getObject()).getDeclarations();
			if (declarations != null) {
				init = GecosUserBlockFactory.BBlock();
				for (String string : declarations.keySet()) {
					Symbol symbol = gecosModel.getGecosCoreContextedFactory().getSymbol(string);
					if(symbol==null) {
						throw new RuntimeException("Parse Error : the identifier \""+string + "\" is not declared !");
					}
					SymbolInstruction symbolInstruction = GecosUserInstructionFactory.symbref(symbol);
					init.addInstruction(GecosUserInstructionFactory.set(symbolInstruction, declarations.get(string)));
				}
			}
		}
		if (init == null){
			init = GecosUserBlockFactory.BBlock();
		}

		// FIXME : there may not be 
		IASTExpression conditionExpression = statement.getConditionExpression();
		BasicBlock test = GecosUserBlockFactory.BBlock();
		if (conditionExpression!=null) {
			conditionExpression.accept(gecosModel);
			Instruction conditionnal = ((ASTExpressionVisitor)gecosModel.getObject()).getInstruction();
			test.addInstruction(conditionnal);
		}

		BasicBlock step = gecosModel.getGecosControlStructureFactory().createBasicBlock();
		IASTExpression iterationExpression = statement.getIterationExpression();
		if(iterationExpression!=null) {
			/* FIXME: e.g. i=i++ -1; */
			iterationExpression.accept(gecosModel);
			Instruction s = ((ASTExpressionVisitor)gecosModel.getObject()).getInstruction();
			if(s!=null) {
				beforeInstructions = ((ASTExpressionVisitor)gecosModel.getObject()).getBeforeInstructions();
				afterInstructions = ((ASTExpressionVisitor)gecosModel.getObject()).getAfterInstructions();
				setBeforeAfterInstruction(step, s);
			} else {
				throw new UnsupportedOperationException("Maxime must fix this corner case for "+iterationExpression);
			}
		}
		forBlock.setInitBlock(init);
		forBlock.setTestBlock(test);
		forBlock.setStepBlock(step);
		statement.getBody().accept(gecosModel);
		if (forBlock instanceof ForC99Block) {
			//XXX - pop()
			gecosModel.getGecosCoreContextedFactory().goToUpperScope();
			gecosModel.getGecosCoreContextedFactory().setParentBlock(parentBlock);
		}
		forBlock.setBodyBlock(((ASTStatementVisitor)gecosModel.getObject()).getBlock());
		addDummyInstruction(forBlock.getBodyBlock(), statement);
		
		if(this.gecosModel.isGenerateSimpleForBlock()) {
			ForBlock simpleForBlock = SimpleLoop.eInstance.getForScop(forBlock);
			if (forBlock != simpleForBlock) {
				if (forBlock instanceof ForC99Block)
					((CompositeBlock)parentBlock).removeBlock(forBlock);
				else
					parentBlock = gecosModel.getGecosCoreContextedFactory().getParentBlock();
				
				((CompositeBlock)parentBlock).addChildren(simpleForBlock);
				forBlock = simpleForBlock;
			}
		}
		this.block = forBlock;
		buildTest(forBlock.getTestBlock(), forBlock.getBodyBlock());
	}

	private void buildICPPASTForStatement(ICPPASTForStatement statement) {
		buildIASTForStatement(statement);
	}

	private void buildIASTGotoStatement(IASTGotoStatement statement) {
		if (gecosModel.visitingInclude())
			notYetSupported(statement);
		statement.getName().accept(gecosModel);
		String name = ((ASTNameVisitor)gecosModel.getObject()).getName();
		BasicBlock basicBlock= gecosModel.getGecosControlStructureFactory().createBasicBlock();
		basicBlock.addInstruction(GecosUserInstructionFactory.gotoInst(gecosModel.getLabelInstruction(name)));
		block = basicBlock;
	}

	private void buildIASTIfStatement(IASTIfStatement statement) {
		if (gecosModel.visitingInclude())
			notYetSupported(statement);
		IfBlock ifBlock = gecosModel.getGecosControlStructureFactory().createIfBlock();
		addPragma(ifBlock, statement);
		statement.getConditionExpression().accept(gecosModel);
		ifBlock.setTestBlock(gecosModel.getGecosControlStructureFactory().createBasicBlock(((ASTExpressionVisitor)gecosModel.getObject()).getInstruction()));
		statement.getThenClause().accept(gecosModel);
		ifBlock.setThenBlock(((ASTStatementVisitor)gecosModel.getObject()).getBlock());
		if (statement.getElseClause() != null) {
			statement.getElseClause().accept(gecosModel);
			ifBlock.setElseBlock(((ASTStatementVisitor)gecosModel.getObject()).getBlock());
		}
		buildTest(ifBlock.getTestBlock(), ifBlock.getElseBlock());
		block = ifBlock;
//		gecosModel.addBlock(block, statement);

	}

	private void buildICPPASTIfStatement(ICPPASTIfStatement statement) {
		buildIASTIfStatement(statement);
	}

	private void buildIASTLabelStatement(IASTLabelStatement statement) {
		if (gecosModel.visitingInclude())
			notYetSupported(statement);
		statement.getName().accept(gecosModel);
		String name = ((ASTNameVisitor)gecosModel.getObject()).getName();
		BasicBlock basicBlock= gecosModel.getGecosControlStructureFactory().createBasicBlock();
		basicBlock.addInstruction(gecosModel.getLabelInstruction(name));
		//FIXME process desynchronizing !!
		if (gecosModel.getGecosCoreContextedFactory().getParentBlock() != null && gecosModel.getGecosCoreContextedFactory().getParentBlock() instanceof CompositeBlock) {
			((CompositeBlock)gecosModel.getGecosCoreContextedFactory().getParentBlock()).addChildren(basicBlock);
		}
		statement.getNestedStatement().accept(gecosModel);
		block = ((ASTStatementVisitor)gecosModel.getObject()).getBlock();
	}

	private void buildIASTNullStatement(IASTNullStatement statement) {
		if (gecosModel.visitingInclude())
			notYetSupported(statement);
	}

	private void buildIASTProblemStatement(IASTProblemStatement statement) {
		throw new CDTParserErrorException(statement, "Parse error in the file " + statement.getContainingFilename() + " at line  : " + statement.getFileLocation().getStartingLineNumber() + 
				"\n\tCDT could not parse \"" + statement.getRawSignature() + "\" : " + statement.getProblem().getMessage());
	}

	private void buildIASTReturnStatement(IASTReturnStatement statement) {
		if (gecosModel.visitingInclude())
			notYetSupported(statement);
		BasicBlock basicBlock = BlocksFactory.eINSTANCE.createBasicBlock();
		Instruction instruction = null;
		if (statement.getReturnValue() != null) {
			statement.getReturnValue().accept(gecosModel);
			instruction = ((ASTExpressionVisitor)gecosModel.getObject()).getInstruction();
		} else if (statement.getReturnArgument() != null) {
			statement.getReturnArgument().accept(gecosModel);
			instruction = ((ASTInitializerVisitor)gecosModel.getObject()).getInstruction();
		} else {
			instruction = GecosUserInstructionFactory.dummy();
		}
		instruction = GecosUserInstructionFactory.ret(instruction);
		List<PragmaAnnotation> findAnnotation = gecosModel.findAnnotation(statement.getFileLocation().getFileName(), statement.getFileLocation().getNodeOffset());
		GecosUserAnnotationFactory.pragma(instruction, findAnnotation);
		if (gecosModel.getGecosCoreContextedFactory().getParentBlock() != null && gecosModel.getGecosCoreContextedFactory().getParentBlock() instanceof CompositeBlock && !(statement.getParent() instanceof IASTIfStatement)) {
			CompositeBlock compositeBlock = (CompositeBlock)gecosModel.getGecosCoreContextedFactory().getParentBlock();
			if (!compositeBlock.getChildren().isEmpty() && compositeBlock.getChildren().get(compositeBlock.getChildren().size()-1) instanceof BasicBlock)
				((BasicBlock)compositeBlock.getChildren().get(compositeBlock.getChildren().size()-1)).addInstruction(instruction);
			else{
				basicBlock.addInstruction(instruction);
				((CompositeBlock)gecosModel.getGecosCoreContextedFactory().getParentBlock()).addChildren(basicBlock);
			}
		} else {
			basicBlock.addInstruction(instruction);
			block = basicBlock;
		}
	}

	private void buildIASTSwitchStatement(IASTSwitchStatement statement) {
		if (gecosModel.visitingInclude())
			notYetSupported(statement);
		SwitchBlock switchBlock = gecosModel.getGecosControlStructureFactory().createSwitchBlock();
		statement.getControllerExpression().accept(gecosModel);
		ASTExpressionVisitor expressionVisitor = (ASTExpressionVisitor)gecosModel.getObject();
		switchBlock.setDispatchBlock(gecosModel.getGecosControlStructureFactory().createBasicBlock(expressionVisitor.getInstruction()));
		statement.getBody().accept(gecosModel);
		switchBlock.setBodyBlock(((ASTStatementVisitor)gecosModel.getObject()).getBlock());
		gecosModel.getGecosControlStructureFactory().switchBlockCreated();
		block = switchBlock;
	}

	private void buildICPPASTSwitchStatement(ICPPASTSwitchStatement statement) {
		buildIASTSwitchStatement(statement);
	}

	private void buildIASTWhileStatement(IASTWhileStatement statement) {
		if (gecosModel.visitingInclude())
			notYetSupported(statement);
		WhileBlock whileBlock = gecosModel.getGecosControlStructureFactory().createWhileBlock();
		addPragma(whileBlock, statement);
		statement.getCondition().accept(gecosModel);
		whileBlock.setTestBlock(gecosModel.getGecosControlStructureFactory().createBasicBlock(((ASTExpressionVisitor)gecosModel.getObject()).getInstruction()));
		beforeInstructions = ((ASTExpressionVisitor)gecosModel.getObject()).getBeforeInstructions();
		afterInstructions = ((ASTExpressionVisitor)gecosModel.getObject()).getAfterInstructions();
		if (statement.getBody() instanceof IASTNullStatement)
			whileBlock.setBodyBlock(gecosModel.getGecosControlStructureFactory().createBasicBlock());
		else {
			statement.getBody().accept(gecosModel);
			Block block = ((ASTStatementVisitor)gecosModel.getObject()).getBlock();
			if (!(block instanceof CompositeBlock)) {
				block = GecosUserBlockFactory.CompositeBlock(block);
			}
			whileBlock.setBodyBlock(block);
			setBeforeAfterInstruction((CompositeBlock)whileBlock.getBodyBlock());
			buildTest(whileBlock.getTestBlock(), whileBlock.getBodyBlock());
		}
		addDummyInstruction(whileBlock, statement);
		block = whileBlock;
	}

	private void buildICPPASTWhileStatement(ICPPASTWhileStatement statement) {
		buildIASTWhileStatement(statement);
	}

	private void buildICPPASTCatchHandler(ICPPASTCatchHandler statement) {
		notYetSupported(statement);
	}

	private void buildICPPASTTryBlockStatement(ICPPASTTryBlockStatement statement) {
		notYetSupported(statement);
	}

	private void addPragma(AnnotatedElement annotatedElement, IASTStatement statement) {
		int nodeOffset = statement.getFileLocation().getNodeOffset();
		if (statement instanceof IASTExpressionStatement)
			nodeOffset = nodeOffset + statement.getFileLocation().getNodeLength();
		
		List<PragmaAnnotation> findAnnotation = gecosModel.findAnnotation(statement.getFileLocation().getFileName(), nodeOffset);
		GecosUserAnnotationFactory.pragma(annotatedElement, findAnnotation);
//		gecosModel.getGecosAnnotationFactory().addPragmaAnnotationsTo(, findAnnotation);
	}

	private void addDummyInstruction(Block body, IASTStatement statement) {
		List<PragmaAnnotation> pragmaAnnotations = gecosModel.findAnnotation(statement.getFileLocation().getFileName(), statement.getFileLocation().getNodeOffset()+statement.getFileLocation().getNodeLength());
		if (!pragmaAnnotations.isEmpty()) {
			Instruction instruction = GecosUserInstructionFactory.dummy();
			GecosUserAnnotationFactory.pragma(instruction, pragmaAnnotations);
			if (body instanceof CompositeBlock) {
				BasicBlock basicBlock = gecosModel.getGecosControlStructureFactory().createBasicBlock(instruction);
				((CompositeBlock)body).addChildren(basicBlock);
			} else if (body instanceof BasicBlock) {
				((BasicBlock)body).addInstruction(instruction);
			}
		}
	}

	private void setBeforeAfterInstruction(CompositeBlock body) {
		BasicBlock basicBlock;
		if (beforeInstructions != null) {
			basicBlock = gecosModel.getGecosControlStructureFactory().createBasicBlock();
			for(int i = 0; i < beforeInstructions.size(); i++)
				basicBlock.addInstruction(beforeInstructions.get(i));
			if (body.getChildren().isEmpty())
				body.getChildren().add(basicBlock);
			else
				body.getChildren().add(0, basicBlock);
			beforeInstructions = null;
		}
		if (afterInstructions != null) {
			basicBlock = gecosModel.getGecosControlStructureFactory().createBasicBlock();
			for(int i = 0; i < afterInstructions.size(); i++)
				basicBlock.addInstruction(afterInstructions.get(i));
			body.getChildren().add(basicBlock);
			afterInstructions = null;
		}
	}

	private void setBeforeAfterInstruction(BasicBlock basicBlock, Instruction instruction) {
		if (beforeInstructions == null && afterInstructions == null)
			basicBlock.addInstruction(instruction);
		if (beforeInstructions != null) {
			for(int i = 0; i < beforeInstructions.size(); i++)
				basicBlock.addInstruction(beforeInstructions.get(i));
			beforeInstructions = null;
		}
		if (afterInstructions != null) {
			for(int i = 0; i < afterInstructions.size(); i++)
				basicBlock.addInstruction(afterInstructions.get(i));
			afterInstructions = null;
		}
	}

	//FIXME no need of labelBlock
	private void buildTest(Block testBlock, Block labelBlock) {
		if (testBlock instanceof BasicBlock) {
			if (!((BasicBlock)testBlock).getInstructions().isEmpty()) {
				Instruction cond = ((BasicBlock)testBlock).getFirstInstruction();
				CondInstruction condInstruction = GecosUserInstructionFactory.condBranch(cond, labelName + labelCounter++);
				((BasicBlock)testBlock).getInstructions().clear();
				((BasicBlock)testBlock).addInstruction(condInstruction);
			}
		}
		else
			if (CDTFrontEnd.VERBOSE) 
				System.err.println("Unsupported configuration : " + testBlock.getClass());
	}
	
	private static void notYetSupported(IASTStatement statement) {
		throw new CDTParserNotYetSupportedException(statement, "Support for "+statement.getClass().getSimpleName()+ ":" +statement.getRawSignature()+ " not yet implemented ");
	}
	
}