/**
 */
package gecos.blocks;

import gecos.instrs.BranchType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Control Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.blocks.ControlEdge#getCond <em>Cond</em>}</li>
 *   <li>{@link gecos.blocks.ControlEdge#getFlags <em>Flags</em>}</li>
 *   <li>{@link gecos.blocks.ControlEdge#getFrom <em>From</em>}</li>
 *   <li>{@link gecos.blocks.ControlEdge#getTo <em>To</em>}</li>
 * </ul>
 *
 * @see gecos.blocks.BlocksPackage#getControlEdge()
 * @model annotation="gmf.link source='from' target='to' style='dot' width='1'"
 * @generated
 */
public interface ControlEdge extends FlowEdge {
	/**
	 * Returns the value of the '<em><b>Cond</b></em>' attribute.
	 * The literals are from the enumeration {@link gecos.instrs.BranchType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cond</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cond</em>' attribute.
	 * @see gecos.instrs.BranchType
	 * @see #setCond(BranchType)
	 * @see gecos.blocks.BlocksPackage#getControlEdge_Cond()
	 * @model unique="false"
	 * @generated
	 */
	BranchType getCond();

	/**
	 * Sets the value of the '{@link gecos.blocks.ControlEdge#getCond <em>Cond</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cond</em>' attribute.
	 * @see gecos.instrs.BranchType
	 * @see #getCond()
	 * @generated
	 */
	void setCond(BranchType value);

	/**
	 * Returns the value of the '<em><b>Flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Flags</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Flags</em>' attribute.
	 * @see #setFlags(int)
	 * @see gecos.blocks.BlocksPackage#getControlEdge_Flags()
	 * @model unique="false" dataType="gecos.blocks.int"
	 * @generated
	 */
	int getFlags();

	/**
	 * Sets the value of the '{@link gecos.blocks.ControlEdge#getFlags <em>Flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Flags</em>' attribute.
	 * @see #getFlags()
	 * @generated
	 */
	void setFlags(int value);

	/**
	 * Returns the value of the '<em><b>From</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link gecos.blocks.BasicBlock#getOutEdges <em>Out Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From</em>' container reference.
	 * @see #setFrom(BasicBlock)
	 * @see gecos.blocks.BlocksPackage#getControlEdge_From()
	 * @see gecos.blocks.BasicBlock#getOutEdges
	 * @model opposite="outEdges" transient="false"
	 * @generated
	 */
	BasicBlock getFrom();

	/**
	 * Sets the value of the '{@link gecos.blocks.ControlEdge#getFrom <em>From</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From</em>' container reference.
	 * @see #getFrom()
	 * @generated
	 */
	void setFrom(BasicBlock value);

	/**
	 * Returns the value of the '<em><b>To</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link gecos.blocks.BasicBlock#getInEdges <em>In Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To</em>' reference.
	 * @see #setTo(BasicBlock)
	 * @see gecos.blocks.BlocksPackage#getControlEdge_To()
	 * @see gecos.blocks.BasicBlock#getInEdges
	 * @model opposite="inEdges" resolveProxies="false"
	 * @generated
	 */
	BasicBlock getTo();

	/**
	 * Sets the value of the '{@link gecos.blocks.ControlEdge#getTo <em>To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To</em>' reference.
	 * @see #getTo()
	 * @generated
	 */
	void setTo(BasicBlock value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.blocks.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; fromString = \"null\";\n&lt;%java.lang.String%&gt; toString = \"null\";\n&lt;%gecos.blocks.BasicBlock%&gt; _from = this.getFrom();\nboolean _tripleNotEquals = (_from != null);\nif (_tripleNotEquals)\n{\n\tfromString = this.getFrom().toShortString();\n}\n&lt;%gecos.blocks.BasicBlock%&gt; _to = this.getTo();\nboolean _tripleNotEquals_1 = (_to != null);\nif (_tripleNotEquals_1)\n{\n\ttoString = this.getTo().toShortString();\n}\n&lt;%gecos.instrs.BranchType%&gt; _cond = this.getCond();\n&lt;%java.lang.String%&gt; _plus = (\"ControlEdge[\" + _cond);\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \", \");\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + fromString);\n&lt;%java.lang.String%&gt; _plus_3 = (_plus_2 + \"-&gt;\");\n&lt;%java.lang.String%&gt; _plus_4 = (_plus_3 + toString);\nreturn (_plus_4 + \"]\");'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitControlEdge(this);'"
	 * @generated
	 */
	void accept(BlocksVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model fromUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.BasicBlock%&gt; _from = this.getFrom();\nboolean _tripleEquals = (_from == from);\nif (_tripleEquals)\n{\n\tthis.setFrom(null);\n}'"
	 * @generated
	 */
	void disconnectFrom(BasicBlock from);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getFrom().getOutEdges().remove(this);\nthis.setTo(null);'"
	 * @generated
	 */
	void disconnect();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model fDataType="gecos.blocks.int" fUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.setFlags(f);'"
	 * @generated
	 */
	void setFlag(int f);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model fDataType="gecos.blocks.int" fUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.setFlags((this.getFlags() | f));'"
	 * @generated
	 */
	void addFlag(int f);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model fDataType="gecos.blocks.int" fUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.setFlags((this.getFlags() &amp; (~f)));'"
	 * @generated
	 */
	void removeFlag(int f);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" fDataType="gecos.blocks.int" fUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return ((this.getFlags() &amp; f) != 0);'"
	 * @generated
	 */
	boolean isSet(int f);

} // ControlEdge
