package test;

import fr.irisa.cairn.gecos.model.dag.adapter.DAGInstructionAdapterComponent;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGInstructionAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGNodeAdapter;
import fr.irisa.cairn.graph.analysis.GraphAnalysis;
import fr.irisa.cairn.graph.guice.GraphComponent;
import gecos.dag.DAGInstruction;

import java.util.List;

public class Example {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DAGInstruction ex = buildExample();
		IDAGInstructionAdapter adapter = DAGInstructionAdapterComponent.INSTANCE.build(ex);
		
		//Simple IGraph algorithm call: topological sort of nodes
		List<IDAGNodeAdapter> topological = GraphAnalysis.topologicalAlapSort(adapter);
		System.out.println(topological);
		
		//Example of algorithm call using injected providers: critical path using injected ILatencyProvider 
		//If no custom provider module has been defined, it's using IGraph default latency provider
		int cp = GraphComponent.getINSTANCE().getGraphAnalysisService().criticalPathLength(adapter);
		System.out.println("Critical path = "+cp);
	}
	
	public static DAGInstruction buildExample(){
		//Build your own example here
		throw new RuntimeException("You need to build your own adapted example.");
	}

}
