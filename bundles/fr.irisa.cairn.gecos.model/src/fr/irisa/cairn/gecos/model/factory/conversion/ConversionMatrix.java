/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.factory.conversion;

import gecos.types.AliasType;
import gecos.types.ArrayType;
import gecos.types.BaseType;
import gecos.types.BoolType;
import gecos.types.EnumType;
import gecos.types.FloatType;
import gecos.types.IntegerType;
import gecos.types.PtrType;
import gecos.types.RecordType;
import gecos.types.Type;
import gecos.types.TypesPackage;
import gecos.types.VoidType;
import gecos.types.util.TypesSwitch;

public class ConversionMatrix extends TypesSwitch<Object> {

	/* ========================================================================
	 *        | char   | short  | int    | long   | longlong | float  | double
	 * ========================================================================
	 * char   | char   | short  | int    | long   | longlong | float  | double
	 * short  | short  | short  | int    | long   | longlong | float  | double
	 * int    | int    | int    | int    | long   | longlong | float  | double
	 * long   | long   | long   | long   | long   | longlong | float  | double
	 * longl  | longl  | longl  | longl  | longl  | longlong | float  | double
	 * float  | float  | float  | float  | float  | double   | float  | double
	 * double | double | double | double | double | double   | double | double
	 */
	
	private static boolean isExtendedType(Type t) {
		return t.eClass().getEPackage()!=TypesPackage.eINSTANCE;
	}
	public static Type getCommon(Type l, Type r) {
		if(l==null || r==null) {
			return null;
		}
		if (l.isEqual(r)) {
			return l;
		} else if (isExtendedType(l) || isExtendedType(r)) {
			// FIXME Leyton
			throw new UnsupportedOperationException("Not support for Extended Types ("+r+","+l+")");
		} else if (l instanceof EnumType && r instanceof BaseType){
			return r;
		} else if (l instanceof BaseType && r instanceof EnumType){
			return l;
		} else if (l instanceof AliasType && r instanceof AliasType) {
			return getCommon(((AliasType)l).getAlias(), ((AliasType)r).getAlias());
		} else if (l instanceof AliasType) {
			return getCommon(((AliasType)l).getAlias(), r);
		} else if (r instanceof AliasType) {
			return getCommon(l, ((AliasType)r).getAlias());
//		} else if (l instanceof ConstType && r instanceof ConstType) {
//			return getCommon(((ConstType)l).getBase(), ((ConstType)r).getBase());
//		} else if (l instanceof ConstType) {
//			return getCommon(((ConstType)l).getBase(), r);
//		} else if (r instanceof ConstType) {
//			return getCommon(l, ((ConstType)r).getBase());
//		} else if (l instanceof VolatileType && r instanceof VolatileType) {
//			return getCommon(((VolatileType)l).getBase(), ((VolatileType)r).getBase());
//		} else if (l instanceof VolatileType) {
//			return getCommon(((VolatileType)l).getBase(), r);
//		} else if (r instanceof VolatileType) {
//			return getCommon(l, ((VolatileType)r).getBase());
		} else if (l instanceof BaseType && r instanceof BaseType) {
			return new ConversionVisitor().getCommonBaseType(l, r);
		} else if (l instanceof BaseType) {
			Type t = findBaseType(r);
			return new ConversionVisitor().getCommonBaseType(t, l);
		} else if (r instanceof BaseType) {
			Type t = findBaseType(l);
			return new ConversionVisitor().getCommonBaseType(t, r);
		} else if (l instanceof ArrayType && r instanceof PtrType) {
			return r;
		} else if (l instanceof PtrType && r instanceof ArrayType) {
			return l;
		} else if (l instanceof PtrType && r instanceof PtrType) {
			return l;
		} else if (l instanceof PtrType && isTypeInt32(r)) {
			return l;
		} else if (r instanceof PtrType && isTypeInt32(l)) {
			return r;
		} else if (l instanceof PtrType) {
			Type t = findBaseType(r);
			return getCommon(l, t);
		} else if (r instanceof PtrType) {
			Type t = findBaseType(l);
			return getCommon(t, r);
//		} else if ((r instanceof StaticType || r instanceof RegisterType) && (l instanceof StaticType || l instanceof RegisterType)) {
//			return r;
		} else {
			throw new RuntimeException("Could not find common type for "+l+" and "+r);
		}
	}
	
	private static class ConversionVisitor extends TypesSwitch<Object> {

		protected ConversionLine currentLine;
		@Override
		public Object caseArrayType(ArrayType t) {
			currentLine = new ArrayConversionLine(t);
			return null;
		}
		
		@Override
		public Object casePtrType(PtrType t) {
			currentLine = new PtrConversionLine(t);
			return null;
		}
	
		@Override
		public Object caseIntegerType(IntegerType t) {
			currentLine= new BaseConversionLine(t);
			return null;
		}
		
		@Override
		public Object caseFloatType(FloatType t) {
			currentLine= new BaseConversionLine(t);
			return null;
		}
		
		@Override
		public Object caseBoolType(BoolType t) {
			currentLine= new BaseConversionLine(t);
			return null;
		}
		
		@Override
		public Object caseVoidType(VoidType t) {
			currentLine= new BaseConversionLine(t);
			return null;
		}
		
		@Override
		public Object caseRecordType(RecordType t) {
			currentLine= new RecordConversionLine(t);
			return null;
		}
	
//		public void setConversionLine(ConversionLine line) {
//			currentLine = line;
//		}
	
		private Type getCommonBaseType(Type l, Type r) {
			currentLine = null;
			doSwitch(l);
			if (currentLine == null)
				throw new RuntimeException("Could not find common type for "+l+" and "+r);
			return currentLine.getCommonType(r);
		}
	}
	private static Type findBaseType(Type t) {
//		if (t instanceof PtrType)
//			return ((PtrType)t).getBase();
//		if (t instanceof ArrayType)
//			return ((ArrayType)t).getBase();
		while (!(t instanceof BaseType) && !(t instanceof PtrType) && !(t instanceof ArrayType) && !(t instanceof EnumType)) {
			if (t instanceof AliasType)
				t = ((AliasType)t).getAlias();
//			else if (t instanceof ConstType)
//				t = ((ConstType)t).getBase();
//			else if (t instanceof RegisterType)
//				t = ((RegisterType)t).getBase();
//			else if (t instanceof StaticType)
//				t = ((StaticType)t).getBase();
			else
				throw new RuntimeException("Could not find BaseType for "+t+" ("+t.getClass().getCanonicalName()+")");
		}
		return t;
	}
	
	private static boolean isTypeInt32(Type type) {
		return type instanceof IntegerType && (((IntegerType)type).getSize() == 32);
	}
}