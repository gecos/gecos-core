/**
 */
package gecos.dag.impl;

import fr.irisa.cairn.gecos.model.factory.GecosUserDagFactory;

import gecos.dag.DAGDataEdge;
import gecos.dag.DAGEdge;
import gecos.dag.DAGInPort;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGNode;
import gecos.dag.DAGOutPort;
import gecos.dag.DagPackage;
import gecos.dag.DagVisitor;

import gecos.instrs.InstrsVisitor;

import gecos.instrs.impl.InstructionImpl;

import java.util.Collection;

import java.util.function.Consumer;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DAG Instruction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.impl.DAGInstructionImpl#getNodes <em>Nodes</em>}</li>
 *   <li>{@link gecos.dag.impl.DAGInstructionImpl#getEdges <em>Edges</em>}</li>
 *   <li>{@link gecos.dag.impl.DAGInstructionImpl#getIdent <em>Ident</em>}</li>
 *   <li>{@link gecos.dag.impl.DAGInstructionImpl#getSourceNodes <em>Source Nodes</em>}</li>
 *   <li>{@link gecos.dag.impl.DAGInstructionImpl#getSinkNodes <em>Sink Nodes</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DAGInstructionImpl extends InstructionImpl implements DAGInstruction {
	/**
	 * The cached value of the '{@link #getNodes() <em>Nodes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<DAGNode> nodes;

	/**
	 * The cached value of the '{@link #getEdges() <em>Edges</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<DAGEdge> edges;

	/**
	 * The default value of the '{@link #getIdent() <em>Ident</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdent()
	 * @generated
	 * @ordered
	 */
	protected static final int IDENT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getIdent() <em>Ident</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdent()
	 * @generated
	 * @ordered
	 */
	protected int ident = IDENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSourceNodes() <em>Source Nodes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<DAGNode> sourceNodes;

	/**
	 * The cached value of the '{@link #getSinkNodes() <em>Sink Nodes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSinkNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<DAGNode> sinkNodes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DAGInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DagPackage.Literals.DAG_INSTRUCTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGNode> getNodes() {
		if (nodes == null) {
			nodes = new EObjectContainmentWithInverseEList<DAGNode>(DAGNode.class, this, DagPackage.DAG_INSTRUCTION__NODES, DagPackage.DAG_NODE__PARENT);
		}
		return nodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGEdge> getEdges() {
		if (edges == null) {
			edges = new EObjectContainmentWithInverseEList<DAGEdge>(DAGEdge.class, this, DagPackage.DAG_INSTRUCTION__EDGES, DagPackage.DAG_EDGE__PARENT);
		}
		return edges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getIdent() {
		return ident;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdent(int newIdent) {
		int oldIdent = ident;
		ident = newIdent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DagPackage.DAG_INSTRUCTION__IDENT, oldIdent, ident));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGNode> getSourceNodes() {
		if (sourceNodes == null) {
			sourceNodes = new EObjectContainmentEList<DAGNode>(DAGNode.class, this, DagPackage.DAG_INSTRUCTION__SOURCE_NODES);
		}
		return sourceNodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGNode> getSinkNodes() {
		if (sinkNodes == null) {
			sinkNodes = new EObjectContainmentEList<DAGNode>(DAGNode.class, this, DagPackage.DAG_INSTRUCTION__SINK_NODES);
		}
		return sinkNodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final DagVisitor visitor) {
		visitor.visitDAGInstruction(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final InstrsVisitor visitor) {
		visitor.visitDAGInstruction(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		int _ident = this.getIdent();
		String _plus = ("DAG_" + Integer.valueOf(_ident));
		EList<DAGNode> _nodes = this.getNodes();
		return (_plus + _nodes);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void connectNodes(final DAGNode srcNode, final DAGNode dstNode) {
		this.connectNodes(srcNode, dstNode, (-1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void connectNodes(final DAGNode srcNode, final DAGNode dstNode, final int dstInportPosition) {
		DAGOutPort _xifexpression = null;
		if (((srcNode.getOutputs().size() > 0) && (srcNode.getOutputs().get(0) != null))) {
			_xifexpression = srcNode.getOutputs().get(0);
		}
		else {
			_xifexpression = GecosUserDagFactory.createDAGOutputPort(srcNode);
		}
		final DAGOutPort srcPort = _xifexpression;
		final DAGInPort sinkPort = GecosUserDagFactory.createDAGInputPort(dstNode);
		if (((dstInportPosition >= 0) && (dstInportPosition < dstNode.getInputs().size()))) {
			dstNode.getInputs().move(dstInportPosition, sinkPort);
		}
		this.getEdges().add(GecosUserDagFactory.createDAGDataEdge(sinkPort, srcPort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void connect(final DAGOutPort srcPort, final DAGNode dstNode) {
		final DAGInPort sinkPort = GecosUserDagFactory.createDAGInputPort(dstNode);
		this.getEdges().add(GecosUserDagFactory.createDAGDataEdge(sinkPort, srcPort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void replaceNode(final DAGNode newNode, final DAGNode oldNode) {
		newNode.getInputs().addAll(oldNode.getInputs());
		newNode.getOutputs().addAll(oldNode.getOutputs());
		final EList<DAGNode> nodes = this.getNodes();
		nodes.add(newNode);
		nodes.remove(oldNode);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void insertNodeBetween(final DAGNode newNode, final DAGNode predNode, final DAGNode succNode) {
		final Function1<DAGEdge, Boolean> _function = new Function1<DAGEdge, Boolean>() {
			public Boolean apply(final DAGEdge it) {
				DAGNode _sinkNode = it.getSinkNode();
				return Boolean.valueOf((_sinkNode == succNode));
			}
		};
		final EList<DAGEdge> edgesBetween = ECollections.<DAGEdge>asEList(((DAGEdge[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<DAGEdge>filter(predNode.getOutputEdges(), _function), DAGEdge.class)));
		int edgeBetweenFirstPosition = (-1);
		boolean _isEmpty = edgesBetween.isEmpty();
		boolean _not = (!_isEmpty);
		if (_not) {
			edgeBetweenFirstPosition = succNode.getInputEdges().indexOf(edgesBetween.get(0));
			final Consumer<DAGEdge> _function_1 = new Consumer<DAGEdge>() {
				public void accept(final DAGEdge it) {
					DAGInstructionImpl.this.removeEdge(it);
				}
			};
			edgesBetween.forEach(_function_1);
		}
		this.connectNodes(predNode, newNode);
		this.connectNodes(newNode, succNode, edgeBetweenFirstPosition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeNode(final DAGNode node) {
		if ((node == null)) {
			return;
		}
		final Consumer<DAGEdge> _function = new Consumer<DAGEdge>() {
			public void accept(final DAGEdge it) {
				DAGInstructionImpl.this.removeEdge(it);
			}
		};
		node.getInputEdges().forEach(_function);
		final Consumer<DAGEdge> _function_1 = new Consumer<DAGEdge>() {
			public void accept(final DAGEdge it) {
				DAGInstructionImpl.this.removeEdge(it);
			}
		};
		node.getOutputEdges().forEach(_function_1);
		this.getNodes().remove(node);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeNodeAndReconnect(final DAGNode node) {
		final EList<DAGNode> dataPreds = node.getDataPredecessors();
		int _size = dataPreds.size();
		boolean _notEquals = (_size != 1);
		if (_notEquals) {
			throw new RuntimeException("This operation is only possible for nodes with ONLY 1 data predecessor!");
		}
		final DAGNode predNode = dataPreds.get(0);
		predNode.getOutputs().addAll(node.getOutputs());
		this.removeNode(node);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void disconnectNodes(final DAGNode pred, final DAGNode succ) {
		final Consumer<DAGDataEdge> _function = new Consumer<DAGDataEdge>() {
			public void accept(final DAGDataEdge it) {
				DAGInstructionImpl.this.removeEdge(it);
			}
		};
		succ.getDataInputEdges().forEach(_function);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeEdge(final DAGEdge e) {
		if ((e == null)) {
			return;
		}
		DAGOutPort srcPort = e.getSrc();
		if (((srcPort != null) && (srcPort.getSinks().size() == 1))) {
			DAGNode _parent = srcPort.getParent();
			boolean _tripleNotEquals = (_parent != null);
			if (_tripleNotEquals) {
				srcPort.getParent().getOutputs().remove(srcPort);
			}
		}
		DAGNode sinkNode = e.getSinkNode();
		if ((sinkNode != null)) {
			sinkNode.getInputs().remove(e.getSink());
		}
		this.getEdges().remove(e);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DagPackage.DAG_INSTRUCTION__NODES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getNodes()).basicAdd(otherEnd, msgs);
			case DagPackage.DAG_INSTRUCTION__EDGES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getEdges()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DagPackage.DAG_INSTRUCTION__NODES:
				return ((InternalEList<?>)getNodes()).basicRemove(otherEnd, msgs);
			case DagPackage.DAG_INSTRUCTION__EDGES:
				return ((InternalEList<?>)getEdges()).basicRemove(otherEnd, msgs);
			case DagPackage.DAG_INSTRUCTION__SOURCE_NODES:
				return ((InternalEList<?>)getSourceNodes()).basicRemove(otherEnd, msgs);
			case DagPackage.DAG_INSTRUCTION__SINK_NODES:
				return ((InternalEList<?>)getSinkNodes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DagPackage.DAG_INSTRUCTION__NODES:
				return getNodes();
			case DagPackage.DAG_INSTRUCTION__EDGES:
				return getEdges();
			case DagPackage.DAG_INSTRUCTION__IDENT:
				return getIdent();
			case DagPackage.DAG_INSTRUCTION__SOURCE_NODES:
				return getSourceNodes();
			case DagPackage.DAG_INSTRUCTION__SINK_NODES:
				return getSinkNodes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DagPackage.DAG_INSTRUCTION__NODES:
				getNodes().clear();
				getNodes().addAll((Collection<? extends DAGNode>)newValue);
				return;
			case DagPackage.DAG_INSTRUCTION__EDGES:
				getEdges().clear();
				getEdges().addAll((Collection<? extends DAGEdge>)newValue);
				return;
			case DagPackage.DAG_INSTRUCTION__IDENT:
				setIdent((Integer)newValue);
				return;
			case DagPackage.DAG_INSTRUCTION__SOURCE_NODES:
				getSourceNodes().clear();
				getSourceNodes().addAll((Collection<? extends DAGNode>)newValue);
				return;
			case DagPackage.DAG_INSTRUCTION__SINK_NODES:
				getSinkNodes().clear();
				getSinkNodes().addAll((Collection<? extends DAGNode>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DagPackage.DAG_INSTRUCTION__NODES:
				getNodes().clear();
				return;
			case DagPackage.DAG_INSTRUCTION__EDGES:
				getEdges().clear();
				return;
			case DagPackage.DAG_INSTRUCTION__IDENT:
				setIdent(IDENT_EDEFAULT);
				return;
			case DagPackage.DAG_INSTRUCTION__SOURCE_NODES:
				getSourceNodes().clear();
				return;
			case DagPackage.DAG_INSTRUCTION__SINK_NODES:
				getSinkNodes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DagPackage.DAG_INSTRUCTION__NODES:
				return nodes != null && !nodes.isEmpty();
			case DagPackage.DAG_INSTRUCTION__EDGES:
				return edges != null && !edges.isEmpty();
			case DagPackage.DAG_INSTRUCTION__IDENT:
				return ident != IDENT_EDEFAULT;
			case DagPackage.DAG_INSTRUCTION__SOURCE_NODES:
				return sourceNodes != null && !sourceNodes.isEmpty();
			case DagPackage.DAG_INSTRUCTION__SINK_NODES:
				return sinkNodes != null && !sinkNodes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DAGInstructionImpl
