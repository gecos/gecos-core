package fr.irisa.cairn.gecos.model.includes.std;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.CodeSource;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ComputeIncludesPath {
	
	public static final boolean debug = false;
	public static final String TMP_DIR_PREFIX = "gecos_std_includes_";
	public static final String INCLUDES_FOLDER = "includes";
	
	public static List<String> getPaths() {
		ComputeIncludesPath c = new ComputeIncludesPath(INCLUDES_FOLDER);
		List<String> compute = c.compute();
		return compute;
	}
	
	
	private File destOnFileSystem; // destination folder on the filesystem (use /tmp/TMP_DIR_PREFIX* by default)
	private String rootFolder; //root folder, in the Plug-in resource, containing include files to copy
	private List<String> paths; // list of path to use as include folders (i.e. -Ipath)
	
	/**
	 * Filter URLs
	 * @param url
	 * @return
	 */
	private boolean accept(URL url) {
		return (!url.toString().contains(".svn")
				&& !url.toString().endsWith(".java")
				&& !url.toString().endsWith(".class")
				&& url.toString().contains(
						(url.toString().contains(".jar!")?".jar!":"")+ 
						"/" + rootFolder + "/"));
	}

	/*
	 * Constructors
	 */
	private ComputeIncludesPath(String rootFolder) {
		try {
			destOnFileSystem = File.createTempFile("gecos_std_includes_", "");
		} catch (IOException e) {
			throw new RuntimeException("Could not create temporary dir.",e);
		}
		destOnFileSystem.delete();
		destOnFileSystem.mkdir();
		
		this.rootFolder=rootFolder;
		paths = new ArrayList<>();
		
		// '/inc' becomes 'inc'
		while (this.rootFolder.startsWith(File.separator))
			this.rootFolder=this.rootFolder.substring(1);
	}
	
	int level;
	private List<String> compute() {
		level = 0;
		CodeSource src = ComputeIncludesPath.class.getProtectionDomain().getCodeSource();
		try {
			URL location = src.getLocation();
			URI uri = location.toURI();
			File p = new File(uri).getCanonicalFile();
			if (p.isDirectory()) {
				if (debug) 
					System.out.println("Using plugin source folder : "+p.getAbsolutePath()+File.separator+rootFolder);
				destOnFileSystem.delete();
				for (String s : new File(p.getAbsolutePath()+File.separator+rootFolder).list())
					this.paths.add(p.getAbsolutePath()+File.separator+rootFolder+File.separator+s);
			} else {
				if (debug) 
					System.out.println("copying "+uri+"/"+rootFolder+" to "+destOnFileSystem.getAbsolutePath());
				
				iterateEntry(destOnFileSystem, p);
				
				//jar browsing does not follow file hierarchy.
				//therefore deleteOnExit has to be called afterward 
				deleteOnExit(destOnFileSystem);
			}
			return this.paths;
		} catch (IOException | URISyntaxException e) {
			throw new RuntimeException("Could not copy resources",e);
		}
	}

	private void deleteOnExit(File f) {
		f.deleteOnExit();
		if (f.isDirectory()) {
			for (File child : f.listFiles())
				deleteOnExit(child);
		}
	}

	private void iterateEntry(File dest, File p)
			throws MalformedURLException, IOException {
		if (p.isDirectory()) {
			for (File f : p.listFiles()) {
				iterateFileSystem(dest,f);
			}
		} else if (p.isFile() && p.getName().toLowerCase().endsWith(".jar")) {
			iterateJarFile(p);
		} else {
			throw new UnsupportedOperationException("Do not support "+p);
		}
	}
	
	/**
	 * Copy data from the plug-in resource to filePath on the file system
	 * @param filePath absolute path to the destination file
	 * @param data URL to the plug-in resource data to copy
	 * @throws IOException
	 */
	private void makefile(String filePath, URL data) throws IOException {
		if (debug) System.out.println(" >> copy "+filePath);

		// Copy resource to filesystem in a temp folder with a unique name		
		InputStream inputStreamForFileToCopy = data.openStream();
		File copiedFile = new File(filePath);
		FileOutputStream outputStream = new FileOutputStream(copiedFile);
		byte[] array = new byte[8192];
		int read = 0;
		while ((read = inputStreamForFileToCopy.read(array)) > 0)
			outputStream.write(array, 0, read);
		outputStream.close();
		inputStreamForFileToCopy.close();
	}
	
	private File makeDir(String path) {
		if (debug) System.out.println("mkdir "+path+" (lvl = "+level+")");
		if (level == 1) this.paths.add(path);
		File newDir = new File(path);
		newDir.mkdir();
		return newDir;
	}
	
	
	private void iterateFileSystem(File parent, File r) throws MalformedURLException, IOException {
		String path = parent.getAbsolutePath()+File.separator+r.getName();
		URL url = r.toURI().toURL();
		if (!accept(url)) return;
		
		if (r.isDirectory()) {
			File newDir = makeDir(path);
			level++;
			for (File file : r.listFiles()) {
				iterateFileSystem(newDir, file);
			}
			level--;
		} else
			makefile(path, url);
	}
	
	private void iterateJarFile(File r) throws MalformedURLException, IOException {
		JarFile jFile = new JarFile(r);
		for (Enumeration<JarEntry> je = jFile.entries(); je.hasMoreElements();) {
			JarEntry j = je.nextElement();
			URL u = new URL("jar", "", r.toURI() + "!/" + j.getName());
			String path = destOnFileSystem.getAbsolutePath()+File.separator+j.getName();
			String line = j.getName();
			level = line.length() - line.replace("/", "").length() - 1;
			if (!accept(u)) continue;
			if (j.isDirectory()) {
				makeDir(path);
			} else {
				makefile(path,u);
			}
		}
		jFile.close();
	}
}