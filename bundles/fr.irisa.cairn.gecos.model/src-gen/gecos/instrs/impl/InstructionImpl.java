/**
 */
package gecos.instrs.impl;

import fr.irisa.cairn.gecos.model.tools.utils.GecosCopier;

import gecos.annotations.impl.AnnotatedElementImpl;

import gecos.blocks.BasicBlock;

import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.Scope;

import gecos.instrs.ComplexInstruction;
import gecos.instrs.InstrsPackage;
import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;

import gecos.types.Type;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import org.eclipse.xtext.xbase.lib.Exceptions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Instruction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.impl.InstructionImpl#get___internal_cached___type <em>internal cached type</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class InstructionImpl extends AnnotatedElementImpl implements Instruction {
	/**
	 * The cached value of the '{@link #get___internal_cached___type() <em>internal cached type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #get___internal_cached___type()
	 * @generated
	 * @ordered
	 */
	protected Type ___internal_cached___type;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstrsPackage.eINSTANCE.getInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type get___internal_cached___type() {
		return ___internal_cached___type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void set___internal_cached___type(Type new___internal_cached___type) {
		Type old___internal_cached___type = ___internal_cached___type;
		___internal_cached___type = new___internal_cached___type;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InstrsPackage.INSTRUCTION__INTERNAL_CACHED_TYPE, old___internal_cached___type, ___internal_cached___type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexInstruction getParent() {
		EObject cont = this.eContainer();
		while (((!(cont instanceof ComplexInstruction)) && (cont instanceof Instruction))) {
			cont = cont.eContainer();
		}
		if ((!(cont instanceof ComplexInstruction))) {
			return null;
		}
		return ((ComplexInstruction) cont);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type getType() {
		Type ____internal_cached___type = this.get___internal_cached___type();
		boolean _tripleEquals = (____internal_cached___type == null);
		if (_tripleEquals) {
			try {
				final Type t = this.computeType();
				if ((t == null)) {
					return null;
				}
				if (((this.getBasicBlock() != null) && (this.getBasicBlock().getScope() != null))) {
					final Scope scope = this.getBasicBlock().getScope();
					final Type existingType = scope.lookup(t);
					if ((existingType == null)) {
						this.setType(t);
						scope.getTypes().add(t);
					}
					else {
						this.setType(existingType);
					}
				}
				else {
					return t;
				}
			}
			catch (final Throwable _t) {
				if (_t instanceof Exception) {
					return null;
				}
				else {
					throw Exceptions.sneakyThrow(_t);
				}
			}
		}
		return this.get___internal_cached___type();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(final Type t) {
		this.set___internal_cached___type(t);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type computeType() {
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSame(final Instruction instr) {
		boolean isSame = false;
		if (((instr.getType() == null) && (this.getType() == null))) {
			isSame = true;
		}
		else {
			Type _type = instr.getType();
			boolean _tripleEquals = (_type == null);
			Type _type_1 = this.getType();
			boolean _tripleEquals_1 = (_type_1 == null);
			boolean _xor = (_tripleEquals ^ _tripleEquals_1);
			if (_xor) {
				return false;
			}
			else {
				isSame = instr.getType().isEqual(this.getType());
			}
		}
		return isSame;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final InstrsVisitor visitor) {
		String _simpleName = this.getClass().getSimpleName();
		String _plus = (_simpleName + " not supported by ");
		String _simpleName_1 = visitor.getClass().getSimpleName();
		String _plus_1 = (_plus + _simpleName_1);
		throw new UnsupportedOperationException(_plus_1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void replaceChild(final Instruction s, final Instruction value) {
		throw new UnsupportedOperationException(("Not applicable to " + s));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction copy() {
		final GecosCopier copier = new GecosCopier();
		EObject _copy = copier.copy(this);
		final Instruction instruction = ((Instruction) _copy);
		copier.copyReferences();
		return instruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Procedure getContainingProcedure() {
		EObject container = this.eContainer();
		while ((container != null)) {
			if ((container instanceof Procedure)) {
				return ((Procedure)container);
			}
			else {
				container = container.eContainer();
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureSet getContainingProcedureSet() {
		Procedure _containingProcedure = this.getContainingProcedure();
		ProcedureSet _containingProcedureSet = null;
		if (_containingProcedure!=null) {
			_containingProcedureSet=_containingProcedure.getContainingProcedureSet();
		}
		return _containingProcedureSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getRoot() {
		ComplexInstruction _parent = this.getParent();
		boolean _tripleNotEquals = (_parent != null);
		if (_tripleNotEquals) {
			return this.getParent().getRoot();
		}
		return this;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicBlock getBasicBlock() {
		EObject container = this.eContainer();
		while ((container != null)) {
			{
				if ((container instanceof BasicBlock)) {
					return ((BasicBlock)container);
				}
				container = container.eContainer();
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isConstant() {
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isUnconditionalBranch() {
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRet() {
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isBranch() {
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSet() {
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSymbol() {
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void substituteWith(final Instruction inst) {
		ComplexInstruction _parent = this.getParent();
		boolean _tripleNotEquals = (_parent != null);
		if (_tripleNotEquals) {
			this.getParent().replaceChild(this, inst);
		}
		else {
			if (((this.getBasicBlock() != null) && this.getBasicBlock().getInstructions().contains(this))) {
				this.getBasicBlock().replaceInstruction(this, inst);
			}
			else {
				EObject _eContainer = this.eContainer();
				boolean _tripleNotEquals_1 = (_eContainer != null);
				if (_tripleNotEquals_1) {
					final EObject container = this.eContainer();
					final EStructuralFeature feature = this.eContainingFeature();
					boolean _isMany = feature.isMany();
					if (_isMany) {
						Object _eGet = container.eGet(feature);
						final EList<EObject> list = ((EList<EObject>) _eGet);
						boolean _contains = list.contains(this);
						boolean _not = (!_contains);
						if (_not) {
							throw new RuntimeException((((("Cannot find [" + this) + "] in the structural features of [") + container) + "]."));
						}
						list.set(list.indexOf(this), inst);
					}
					else {
						container.eSet(feature, inst);
					}
				}
				else {
					throw new UnsupportedOperationException((((("Cannot substitute [" + this) + "] with [") + inst) + "] as this latter one is not contained."));
				}
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void remove() {
		EObject _eContainer = this.eContainer();
		boolean _tripleEquals = (_eContainer == null);
		if (_tripleEquals) {
			throw new UnsupportedOperationException((("Cannot remove [" + this) + "] as it is not contained."));
		}
		EcoreUtil.remove(((EObject) this));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InstrsPackage.INSTRUCTION__INTERNAL_CACHED_TYPE:
				return get___internal_cached___type();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InstrsPackage.INSTRUCTION__INTERNAL_CACHED_TYPE:
				set___internal_cached___type((Type)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InstrsPackage.INSTRUCTION__INTERNAL_CACHED_TYPE:
				set___internal_cached___type((Type)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InstrsPackage.INSTRUCTION__INTERNAL_CACHED_TYPE:
				return ___internal_cached___type != null;
		}
		return super.eIsSet(featureID);
	}

} //InstructionImpl
