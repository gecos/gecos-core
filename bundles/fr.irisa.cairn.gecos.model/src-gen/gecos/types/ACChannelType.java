/**
 */
package gecos.types;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AC Channel Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.ACChannelType#getBaseType <em>Base Type</em>}</li>
 * </ul>
 *
 * @see gecos.types.TypesPackage#getACChannelType()
 * @model
 * @generated
 */
public interface ACChannelType extends ACType, TypesVisitable {
	/**
	 * Returns the value of the '<em><b>Base Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Type</em>' reference.
	 * @see #setBaseType(Type)
	 * @see gecos.types.TypesPackage#getACChannelType_BaseType()
	 * @model
	 * @generated
	 */
	Type getBaseType();

	/**
	 * Sets the value of the '{@link gecos.types.ACChannelType#getBaseType <em>Base Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Type</em>' reference.
	 * @see #getBaseType()
	 * @generated
	 */
	void setBaseType(Type value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.boolean" unique="false" otherUnique="false" ignoreStorageClassDataType="gecos.types.boolean" ignoreStorageClassUnique="false" ignoreConstDataType="gecos.types.boolean" ignoreConstUnique="false" ignoreVolatileDataType="gecos.types.boolean" ignoreVolatileUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((this == other))\n{\n\treturn true;\n}\nif ((other instanceof &lt;%gecos.types.ACChannelType%&gt;))\n{\n\treturn (this.getBaseType().isEqual(((&lt;%gecos.types.ACChannelType%&gt;)other).getBaseType(), ignoreStorageClass, ignoreConst, ignoreVolatile) &amp;&amp; super.isEqual(other, ignoreStorageClass, ignoreConst, ignoreVolatile));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isEqual(Type other, boolean ignoreStorageClass, boolean ignoreConst, boolean ignoreVolatile);

} // ACChannelType
