package fr.irisa.cairn.gecos.model.extensions.switchs;

import org.eclipse.emf.ecore.EObject;

/**
 * An adaptable switch has a {@link SwitchDelegate} in charge of the delegation
 * of the doSwitch function to the registered switch adapters. A switch has to
 * implement this interface in order to use the adapter framework
 * (<i>gecos.switches</i> extension point).
 * 
 * @author Antoine Floc'h - Initial contribution and API
 * 
 * @param <T>
 */
public interface IAdaptableSwitch<T> {
	SwitchDelegate<T> getDelegate();
	T doSwitch(EObject o);
}
