/**
 */
package gecos.instrs;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see gecos.instrs.InstrsPackage
 * @generated
 */
public interface InstrsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	InstrsFactory eINSTANCE = gecos.instrs.impl.InstrsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Dummy Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dummy Instruction</em>'.
	 * @generated
	 */
	DummyInstruction createDummyInstruction();

	/**
	 * Returns a new object of class '<em>Label Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Label Instruction</em>'.
	 * @generated
	 */
	LabelInstruction createLabelInstruction();

	/**
	 * Returns a new object of class '<em>Symbol Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Symbol Instruction</em>'.
	 * @generated
	 */
	SymbolInstruction createSymbolInstruction();

	/**
	 * Returns a new object of class '<em>Numbered Symbol Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Numbered Symbol Instruction</em>'.
	 * @generated
	 */
	NumberedSymbolInstruction createNumberedSymbolInstruction();

	/**
	 * Returns a new object of class '<em>SSA Def Symbol</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SSA Def Symbol</em>'.
	 * @generated
	 */
	SSADefSymbol createSSADefSymbol();

	/**
	 * Returns a new object of class '<em>SSA Use Symbol</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SSA Use Symbol</em>'.
	 * @generated
	 */
	SSAUseSymbol createSSAUseSymbol();

	/**
	 * Returns a new object of class '<em>Sizeof Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sizeof Instruction</em>'.
	 * @generated
	 */
	SizeofInstruction createSizeofInstruction();

	/**
	 * Returns a new object of class '<em>Sizeof Type Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sizeof Type Instruction</em>'.
	 * @generated
	 */
	SizeofTypeInstruction createSizeofTypeInstruction();

	/**
	 * Returns a new object of class '<em>Sizeof Value Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sizeof Value Instruction</em>'.
	 * @generated
	 */
	SizeofValueInstruction createSizeofValueInstruction();

	/**
	 * Returns a new object of class '<em>Int Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Int Instruction</em>'.
	 * @generated
	 */
	IntInstruction createIntInstruction();

	/**
	 * Returns a new object of class '<em>Float Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Float Instruction</em>'.
	 * @generated
	 */
	FloatInstruction createFloatInstruction();

	/**
	 * Returns a new object of class '<em>String Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>String Instruction</em>'.
	 * @generated
	 */
	StringInstruction createStringInstruction();

	/**
	 * Returns a new object of class '<em>Array Value Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Array Value Instruction</em>'.
	 * @generated
	 */
	ArrayValueInstruction createArrayValueInstruction();

	/**
	 * Returns a new object of class '<em>Phi Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Phi Instruction</em>'.
	 * @generated
	 */
	PhiInstruction createPhiInstruction();

	/**
	 * Returns a new object of class '<em>Set Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Set Instruction</em>'.
	 * @generated
	 */
	SetInstruction createSetInstruction();

	/**
	 * Returns a new object of class '<em>Address Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Address Instruction</em>'.
	 * @generated
	 */
	AddressInstruction createAddressInstruction();

	/**
	 * Returns a new object of class '<em>Indir Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Indir Instruction</em>'.
	 * @generated
	 */
	IndirInstruction createIndirInstruction();

	/**
	 * Returns a new object of class '<em>Call Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Call Instruction</em>'.
	 * @generated
	 */
	CallInstruction createCallInstruction();

	/**
	 * Returns a new object of class '<em>Method Call Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Method Call Instruction</em>'.
	 * @generated
	 */
	MethodCallInstruction createMethodCallInstruction();

	/**
	 * Returns a new object of class '<em>Array Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Array Instruction</em>'.
	 * @generated
	 */
	ArrayInstruction createArrayInstruction();

	/**
	 * Returns a new object of class '<em>Simple Array Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simple Array Instruction</em>'.
	 * @generated
	 */
	SimpleArrayInstruction createSimpleArrayInstruction();

	/**
	 * Returns a new object of class '<em>Range Array Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Range Array Instruction</em>'.
	 * @generated
	 */
	RangeArrayInstruction createRangeArrayInstruction();

	/**
	 * Returns a new object of class '<em>Cond Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cond Instruction</em>'.
	 * @generated
	 */
	CondInstruction createCondInstruction();

	/**
	 * Returns a new object of class '<em>Break Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Break Instruction</em>'.
	 * @generated
	 */
	BreakInstruction createBreakInstruction();

	/**
	 * Returns a new object of class '<em>Continue Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Continue Instruction</em>'.
	 * @generated
	 */
	ContinueInstruction createContinueInstruction();

	/**
	 * Returns a new object of class '<em>Goto Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Goto Instruction</em>'.
	 * @generated
	 */
	GotoInstruction createGotoInstruction();

	/**
	 * Returns a new object of class '<em>Ret Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ret Instruction</em>'.
	 * @generated
	 */
	RetInstruction createRetInstruction();

	/**
	 * Returns a new object of class '<em>Convert Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Convert Instruction</em>'.
	 * @generated
	 */
	ConvertInstruction createConvertInstruction();

	/**
	 * Returns a new object of class '<em>Field Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Field Instruction</em>'.
	 * @generated
	 */
	FieldInstruction createFieldInstruction();

	/**
	 * Returns a new object of class '<em>Enumerator Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Enumerator Instruction</em>'.
	 * @generated
	 */
	EnumeratorInstruction createEnumeratorInstruction();

	/**
	 * Returns a new object of class '<em>Range Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Range Instruction</em>'.
	 * @generated
	 */
	RangeInstruction createRangeInstruction();

	/**
	 * Returns a new object of class '<em>Case Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Case Instruction</em>'.
	 * @generated
	 */
	CaseInstruction createCaseInstruction();

	/**
	 * Returns a new object of class '<em>Generic Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Generic Instruction</em>'.
	 * @generated
	 */
	GenericInstruction createGenericInstruction();

	/**
	 * Returns a new object of class '<em>Arithmetic Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Arithmetic Instruction</em>'.
	 * @generated
	 */
	ArithmeticInstruction createArithmeticInstruction();

	/**
	 * Returns a new object of class '<em>Comparison Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Comparison Instruction</em>'.
	 * @generated
	 */
	ComparisonInstruction createComparisonInstruction();

	/**
	 * Returns a new object of class '<em>Logical Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Logical Instruction</em>'.
	 * @generated
	 */
	LogicalInstruction createLogicalInstruction();

	/**
	 * Returns a new object of class '<em>Bitwise Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Bitwise Instruction</em>'.
	 * @generated
	 */
	BitwiseInstruction createBitwiseInstruction();

	/**
	 * Returns a new object of class '<em>Simd Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simd Instruction</em>'.
	 * @generated
	 */
	SimdInstruction createSimdInstruction();

	/**
	 * Returns a new object of class '<em>Simd Generic Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simd Generic Instruction</em>'.
	 * @generated
	 */
	SimdGenericInstruction createSimdGenericInstruction();

	/**
	 * Returns a new object of class '<em>Simd Pack Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simd Pack Instruction</em>'.
	 * @generated
	 */
	SimdPackInstruction createSimdPackInstruction();

	/**
	 * Returns a new object of class '<em>Simd Extract Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simd Extract Instruction</em>'.
	 * @generated
	 */
	SimdExtractInstruction createSimdExtractInstruction();

	/**
	 * Returns a new object of class '<em>Simd Shuffle Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simd Shuffle Instruction</em>'.
	 * @generated
	 */
	SimdShuffleInstruction createSimdShuffleInstruction();

	/**
	 * Returns a new object of class '<em>Simd Expand Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simd Expand Instruction</em>'.
	 * @generated
	 */
	SimdExpandInstruction createSimdExpandInstruction();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	InstrsPackage getInstrsPackage();

} //InstrsFactory
