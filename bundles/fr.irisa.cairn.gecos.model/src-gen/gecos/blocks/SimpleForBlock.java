/**
 */
package gecos.blocks;

import fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;

import gecos.core.Symbol;

import gecos.instrs.Instruction;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple For Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.blocks.SimpleForBlock#getIterator <em>Iterator</em>}</li>
 *   <li>{@link gecos.blocks.SimpleForBlock#getLb <em>Lb</em>}</li>
 *   <li>{@link gecos.blocks.SimpleForBlock#getUb <em>Ub</em>}</li>
 *   <li>{@link gecos.blocks.SimpleForBlock#getStride <em>Stride</em>}</li>
 * </ul>
 *
 * @see gecos.blocks.BlocksPackage#getSimpleForBlock()
 * @model
 * @generated
 */
public interface SimpleForBlock extends ForC99Block {
	/**
	 * Returns the value of the '<em><b>Iterator</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iterator</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iterator</em>' reference.
	 * @see #isSetIterator()
	 * @see #unsetIterator()
	 * @see #setIterator(Symbol)
	 * @see gecos.blocks.BlocksPackage#getSimpleForBlock_Iterator()
	 * @model unsettable="true"
	 * @generated
	 */
	Symbol getIterator();

	/**
	 * Sets the value of the '{@link gecos.blocks.SimpleForBlock#getIterator <em>Iterator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iterator</em>' reference.
	 * @see #isSetIterator()
	 * @see #unsetIterator()
	 * @see #getIterator()
	 * @generated
	 */
	void setIterator(Symbol value);

	/**
	 * Unsets the value of the '{@link gecos.blocks.SimpleForBlock#getIterator <em>Iterator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIterator()
	 * @see #getIterator()
	 * @see #setIterator(Symbol)
	 * @generated
	 */
	void unsetIterator();

	/**
	 * Returns whether the value of the '{@link gecos.blocks.SimpleForBlock#getIterator <em>Iterator</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Iterator</em>' reference is set.
	 * @see #unsetIterator()
	 * @see #getIterator()
	 * @see #setIterator(Symbol)
	 * @generated
	 */
	boolean isSetIterator();

	/**
	 * Returns the value of the '<em><b>Lb</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lb</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lb</em>' reference.
	 * @see #isSetLb()
	 * @see #unsetLb()
	 * @see #setLb(Instruction)
	 * @see gecos.blocks.BlocksPackage#getSimpleForBlock_Lb()
	 * @model unsettable="true"
	 * @generated
	 */
	Instruction getLb();

	/**
	 * Sets the value of the '{@link gecos.blocks.SimpleForBlock#getLb <em>Lb</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lb</em>' reference.
	 * @see #isSetLb()
	 * @see #unsetLb()
	 * @see #getLb()
	 * @generated
	 */
	void setLb(Instruction value);

	/**
	 * Unsets the value of the '{@link gecos.blocks.SimpleForBlock#getLb <em>Lb</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLb()
	 * @see #getLb()
	 * @see #setLb(Instruction)
	 * @generated
	 */
	void unsetLb();

	/**
	 * Returns whether the value of the '{@link gecos.blocks.SimpleForBlock#getLb <em>Lb</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Lb</em>' reference is set.
	 * @see #unsetLb()
	 * @see #getLb()
	 * @see #setLb(Instruction)
	 * @generated
	 */
	boolean isSetLb();

	/**
	 * Returns the value of the '<em><b>Ub</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ub</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ub</em>' reference.
	 * @see #isSetUb()
	 * @see #unsetUb()
	 * @see #setUb(Instruction)
	 * @see gecos.blocks.BlocksPackage#getSimpleForBlock_Ub()
	 * @model unsettable="true"
	 * @generated
	 */
	Instruction getUb();

	/**
	 * Sets the value of the '{@link gecos.blocks.SimpleForBlock#getUb <em>Ub</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ub</em>' reference.
	 * @see #isSetUb()
	 * @see #unsetUb()
	 * @see #getUb()
	 * @generated
	 */
	void setUb(Instruction value);

	/**
	 * Unsets the value of the '{@link gecos.blocks.SimpleForBlock#getUb <em>Ub</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetUb()
	 * @see #getUb()
	 * @see #setUb(Instruction)
	 * @generated
	 */
	void unsetUb();

	/**
	 * Returns whether the value of the '{@link gecos.blocks.SimpleForBlock#getUb <em>Ub</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Ub</em>' reference is set.
	 * @see #unsetUb()
	 * @see #getUb()
	 * @see #setUb(Instruction)
	 * @generated
	 */
	boolean isSetUb();

	/**
	 * Returns the value of the '<em><b>Stride</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stride</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stride</em>' reference.
	 * @see #isSetStride()
	 * @see #unsetStride()
	 * @see #setStride(Instruction)
	 * @see gecos.blocks.BlocksPackage#getSimpleForBlock_Stride()
	 * @model unsettable="true"
	 * @generated
	 */
	Instruction getStride();

	/**
	 * Sets the value of the '{@link gecos.blocks.SimpleForBlock#getStride <em>Stride</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Stride</em>' reference.
	 * @see #isSetStride()
	 * @see #unsetStride()
	 * @see #getStride()
	 * @generated
	 */
	void setStride(Instruction value);

	/**
	 * Unsets the value of the '{@link gecos.blocks.SimpleForBlock#getStride <em>Stride</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetStride()
	 * @see #getStride()
	 * @see #setStride(Instruction)
	 * @generated
	 */
	void unsetStride();

	/**
	 * Returns whether the value of the '{@link gecos.blocks.SimpleForBlock#getStride <em>Stride</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Stride</em>' reference is set.
	 * @see #unsetStride()
	 * @see #getStride()
	 * @see #setStride(Instruction)
	 * @generated
	 */
	boolean isSetStride();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" mngDataType="gecos.core.BlockCopyManager" mngUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return super.managedCopy(mng);'"
	 * @generated
	 */
	Block managedCopy(BlockCopyManager mng);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitSimpleForBlock(this);'"
	 * @generated
	 */
	void accept(BlocksVisitor visitor);

} // SimpleForBlock
