package fr.irisa.cairn.gecos.model.tools.controlflow;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.gecos.model.tools.utils.BlocksContinuesBreaksFinder;
import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch;
import fr.irisa.cairn.gecos.model.tools.utils.BlocksReturnFinder;
import fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import fr.irisa.r2d2.gecos.framework.GSModule;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CaseBlock;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ControlEdge;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.SwitchBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;
import gecos.instrs.BranchType;
import gecos.instrs.GotoInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.LabelInstruction;

/**
 * 
 * @author akumar : initial implementation
 * @author mnaullet : major rewriting
 * @author amorvan : fixes
 *
 */
@GSModule("Analyzes the IR and builds its associated control flow.\n"
		+ "\nSee: 'RemoveControlFlow' module.")
public class BuildControlFlow extends BlocksDefaultSwitch<Object> {
	
	private List<Procedure> lProc = null;

	private BuildControlFlow() {}

	public BuildControlFlow(GecosProject project) {
		lProc = new ArrayList<Procedure>();
		for (ProcedureSet ps : project.listProcedureSets()) {
			lProc.addAll(ps.listProcedures());
		}
	}
	
	public BuildControlFlow(Procedure proc) {
		(lProc = new ArrayList<Procedure>(1)).add(proc);
	}

	public BuildControlFlow(ProcedureSet ps) {
		lProc = ps.listProcedures();
	}

	public static void buildCF(Procedure proc) {
		(new BuildControlFlow(proc)).compute(); 
	}
	public static void buildCF(ProcedureSet ps) {
		(new BuildControlFlow(ps)).compute(); 
	}
	public static void buildCF(Block b) {
		(new BuildControlFlow()).doSwitch(b); 
	}
	public static void buildCF(EObject targetAST) {
		EList<Block> topLvlBlocks = EMFUtils.eAllContentsFirstInstancesOf(targetAST, Block.class);
		for (Block b : topLvlBlocks)
			buildCF(b);
	}

	public void compute() {
		for (Procedure proc : lProc) {
			
			doSwitch(proc.getBody());
			
			//do not connect the body to the end since the last statement of the body
			//contains at least implicit return, that is contained inside
			//BlocksReturnFinder().getReturns().
			
			//proc.getBody().connectTo(proc.getEnd(), BranchType.UNCONDITIONAL);
			
			BlocksReturnFinder brf = new BlocksReturnFinder();
			brf.doSwitch(proc.getBody());
			for (BasicBlock bb : brf.getReturns()) {
				for (ControlEdge c : (ControlEdge[]) bb.getOutEdges().toArray()) {
					c.setTo(null);
					c.setFrom(null);
				}
				bb.connectTo(proc.getEnd(), BranchType.UNCONDITIONAL);
			}
		}
	}

	@Override
	public Object caseBasicBlock(BasicBlock b) {
		//breaks and continues have already been managed in loops / switches
		BranchInstructionFinder gotoFinder = new BranchInstructionFinder();
		EList<Instruction> instructions = b.getInstructions();
		for(Instruction ins: instructions){
			gotoFinder.doSwitch(ins);
		}


		if (gotoFinder.containsGotos()) {

			for (ControlEdge e : b.getOutEdges().toArray(new ControlEdge[b.getOutEdges().size()])) {
				e.setFrom(null);
				e.setTo(null);
			}
			b.getOutEdges().clear();
			List<GotoInstruction> gotoInstructions = gotoFinder.getGotos();
			for (GotoInstruction branch : gotoInstructions) {
				LabelInstruction labelIns = branch.getLabelInstruction();
				if (labelIns != null) {
					if (labelIns.getBasicBlock() != null) {
						//System.out.println("[GOTO] connect "+b+" to "+labelIns.getBasicBlock());
						b.connectTo(labelIns.getBasicBlock(), BranchType.UNCONDITIONAL);
					}else {
						throw new RuntimeException("Error : LabelInstruction ["+labelIns+"] is not contained in a BasicBlock.");
					}
				} else {
					throw new RuntimeException("Error : BranchInstruction ["+branch+"] does not have LabelInstruction.");
				}
			}
		}
		
		return null;
	}

	@Override
	public Object caseCompositeBlock(CompositeBlock b) {
		List<Block> children = b.getChildren();
		Block prev = null;
		for (Block child : children) {
			if (prev != null)
				prev.connectTo(child, BranchType.UNCONDITIONAL);
			doSwitch(child);
			prev = child;
			if (prev instanceof BasicBlock && ((BasicBlock) prev).getInstructionCount() > 0 && ((BasicBlock) prev).getLastInstruction() instanceof GotoInstruction)
				prev = null;
		}
		return null;
	}

	@Override
	public Object caseIfBlock(IfBlock b) {
		Block thenBlock = b.getThenBlock();
		if (thenBlock != null) {
			b.getTestBlock().connectTo(thenBlock, BranchType.IF_TRUE);
			doSwitch(thenBlock);
		}
		Block elseBlock = b.getElseBlock();
		if (elseBlock != null) {
			b.getTestBlock().connectTo(elseBlock,BranchType.IF_FALSE);
			doSwitch(elseBlock);
		}
		return null;
	}
	
	@Override
	public Object caseForBlock(ForBlock b) {

		Block bodyBlock = b.getBodyBlock();
		
		doSwitch(bodyBlock);
		
		b.getInitBlock().connectTo(b.getTestBlock(), BranchType.UNCONDITIONAL);
		if (b.getBodyBlock() == null) 
			b.getTestBlock().connectTo(b.getStepBlock(), BranchType.IF_TRUE);
		else
			b.getTestBlock().connectTo(b.getBodyBlock(), BranchType.IF_TRUE);
		b.getStepBlock().connectTo(b.getTestBlock(), BranchType.UNCONDITIONAL);

		if (b.getBodyBlock() != null) 
			b.getBodyBlock().connectTo(b.getStepBlock(), BranchType.UNCONDITIONAL);
		
		BlocksContinuesBreaksFinder visitor = new BlocksContinuesBreaksFinder();
		if (b.getBodyBlock() != null) 
			visitor.doSwitch(b.getBodyBlock());
		for (BasicBlock bb : visitor.getContinues()) {
			for (ControlEdge e : (ControlEdge[]) bb.getOutEdges().toArray()) {
				e.setFrom(null);
				e.setTo(null);
			}
			bb.connectTo(b.getStepBlock(), BranchType.UNCONDITIONAL);
		}

		return null;
	}

	@Override
	public Object caseDoWhileBlock(DoWhileBlock b) {
		doSwitch(b.getBodyBlock());
		
		b.getBodyBlock().connectTo(b.getTestBlock(), BranchType.UNCONDITIONAL);
		b.getTestBlock().connectTo(b.getBodyBlock(), BranchType.IF_TRUE);

		BlocksContinuesBreaksFinder visitor = new BlocksContinuesBreaksFinder();
		visitor.doSwitch(b.getBodyBlock());
		for (BasicBlock bb : visitor.getContinues()) {
			for (ControlEdge e : (ControlEdge[]) bb.getOutEdges().toArray()) {
				e.setFrom(null);
				e.setTo(null);
			}
			bb.connectTo(b.getTestBlock(), BranchType.UNCONDITIONAL);
		}
		
		return null;
	}

	@Override
	public Object caseWhileBlock(WhileBlock b) {
		doSwitch(b.getBodyBlock());
		
		b.getBodyBlock().connectTo(b.getTestBlock(), BranchType.UNCONDITIONAL);
		b.getTestBlock().connectTo(b.getBodyBlock(), BranchType.IF_TRUE);
		
		BlocksContinuesBreaksFinder visitor = new BlocksContinuesBreaksFinder();
		visitor.doSwitch(b.getBodyBlock());
		for (BasicBlock bb : visitor.getContinues()) {
			for (ControlEdge e : (ControlEdge[]) bb.getOutEdges().toArray()) {
				e.setFrom(null);
				e.setTo(null);
			}
			bb.connectTo(b.getTestBlock(), BranchType.UNCONDITIONAL);
		}
		
		return null;
	}

	@Override
	public Object caseCaseBlock(CaseBlock b) {
		doSwitch(b.getBody());
		return null;
	}

	@Override
	public Object caseSwitchBlock(SwitchBlock b) {
		
		Block dispatch = b.getDispatchBlock();
		
		for (Block c : b.getCases().values()) {
			dispatch.connectTo(c, BranchType.DISPATCH);
		}
		doSwitch(b.getBodyBlock());
		
		if (b.getDefaultBlock() != null) {
			dispatch.connectTo(b.getDefaultBlock(), BranchType.DISPATCH);
			//b.getBody().connectTo(b.getDefault(), BranchType.UNCONDITIONAL);
		}
		
		return null;
	}
}
