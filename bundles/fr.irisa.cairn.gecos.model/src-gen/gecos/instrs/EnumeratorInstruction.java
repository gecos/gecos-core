/**
 */
package gecos.instrs;

import gecos.types.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enumerator Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.EnumeratorInstruction#getEnumerator <em>Enumerator</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getEnumeratorInstruction()
 * @model
 * @generated
 */
public interface EnumeratorInstruction extends Instruction {
	/**
	 * Returns the value of the '<em><b>Enumerator</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumerator</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumerator</em>' reference.
	 * @see #setEnumerator(Enumerator)
	 * @see gecos.instrs.InstrsPackage#getEnumeratorInstruction_Enumerator()
	 * @model resolveProxies="false"
	 * @generated
	 */
	Enumerator getEnumerator();

	/**
	 * Sets the value of the '{@link gecos.instrs.EnumeratorInstruction#getEnumerator <em>Enumerator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enumerator</em>' reference.
	 * @see #getEnumerator()
	 * @generated
	 */
	void setEnumerator(Enumerator value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitEnumeratorInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.EnumeratorInstruction%&gt;))\n{\n\tboolean isSame = false;\n\tif (((((&lt;%gecos.instrs.EnumeratorInstruction%&gt;)instr).getEnumerator() == null) &amp;&amp; (this.getEnumerator() == null)))\n\t{\n\t\tisSame = true;\n\t}\n\telse\n\t{\n\t\t&lt;%gecos.types.Enumerator%&gt; _enumerator = ((&lt;%gecos.instrs.EnumeratorInstruction%&gt;)instr).getEnumerator();\n\t\tboolean _tripleEquals = (_enumerator == null);\n\t\t&lt;%gecos.types.Enumerator%&gt; _enumerator_1 = this.getEnumerator();\n\t\tboolean _tripleEquals_1 = (_enumerator_1 == null);\n\t\tboolean _xor = (_tripleEquals ^ _tripleEquals_1);\n\t\tif (_xor)\n\t\t{\n\t\t\treturn false;\n\t\t}\n\t\telse\n\t\t{\n\t\t\tisSame = ((&lt;%gecos.instrs.EnumeratorInstruction%&gt;)instr).getEnumerator().isEqual(this.getEnumerator());\n\t\t}\n\t}\n\treturn (isSame &amp;&amp; super.isSame(instr));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // EnumeratorInstruction
