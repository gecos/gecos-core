package fr.irisa.cairn.gecos.model.dag.adapter.builder;

import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGEdgeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGInstructionAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGNodeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGPortAdapter;
import gecos.dag.DAGEdge;
import gecos.dag.DAGInPort;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGNode;
import gecos.dag.DAGOutPort;
import gecos.dag.DAGPort;

import java.util.HashSet;
import java.util.Set;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Builder of the graph adapter.
 *
 * @author <your name>
 *
 */
@Singleton
public class AdapterBuilder {
	@Inject
	AdapterFactory factory;

	//private Set<DAGInstruction> visitedsGraph;
	private Set<DAGNode> visitedsNode;
	private Set<DAGPort> visitedsPort;
	private Set<DAGEdge> visitedsEdge;

	@Inject
	protected AdapterBuilder() {

		//	this.visitedsGraph = new HashSet<DAGInstruction>();
		initialize();
	}

	private void initialize() {
		this.visitedsNode = new HashSet<DAGNode>();
		this.visitedsPort = new HashSet<DAGPort>();
		this.visitedsEdge = new HashSet<DAGEdge>();
	}

	public IDAGInstructionAdapter buildGraphAdapter(DAGInstruction adapted) {
		initialize();
		IDAGInstructionAdapter graphAdapter = factory.createGraphAdapter(
				adapted, true);

		initGraphAdapter(graphAdapter, adapted);

		;
		return graphAdapter;
	}

	private void initGraphAdapter(IDAGInstructionAdapter graphAdapter,
			DAGInstruction adapted) {

		for (DAGNode n : adapted.getNodes()) {

			IDAGInstructionAdapter container = graphAdapter;

			buildNodeAdapter(container, n);
		}

		;

		for (DAGEdge e : adapted.getEdges()) {

			IDAGInstructionAdapter container = graphAdapter;

			buildInternalEdge(container, e);
		}

	}

	public void buildNodeAdapter(IDAGInstructionAdapter graphAdapter,
			DAGNode adapted) {

		IDAGNodeAdapter nodeAdapter = null;
		if (!this.visitedsNode.contains(adapted)) {
			this.visitedsNode.add(adapted);
			nodeAdapter = factory.createNodeAdapter(adapted);
			graphAdapter.addNode(nodeAdapter);

			initNodeAdapter(graphAdapter, nodeAdapter, adapted);

		}
	}

	private void initNodeAdapter(IDAGInstructionAdapter graphAdapter,
			IDAGNodeAdapter nodeAdapter, DAGNode adapted) {

		for (DAGInPort p : adapted.getInputs()) {

			IDAGNodeAdapter container = nodeAdapter;

			buildInPortAdapter(container, p);
		}

		;

		for (DAGOutPort p : adapted.getOutputs()) {

			IDAGNodeAdapter container = nodeAdapter;

			buildOutPortAdapter(container, p);
		}

	}

	public void buildOutPortAdapter(IDAGNodeAdapter nodeAdapter, DAGPort adapted) {

		if (!this.visitedsPort.contains(adapted)) {
			this.visitedsPort.add(adapted);

			IDAGPortAdapter portAdapter = factory.createPortAdapter(adapted);
			nodeAdapter.addOutputPort(portAdapter);

			initPortAdapter(nodeAdapter, portAdapter, adapted);

		}

	}

	public void buildInPortAdapter(IDAGNodeAdapter nodeAdapter, DAGPort adapted) {

		if (!this.visitedsPort.contains(adapted)) {
			this.visitedsPort.add(adapted);

			IDAGPortAdapter portAdapter = factory.createPortAdapter(adapted);
			nodeAdapter.addInputPort(portAdapter);

			initPortAdapter(nodeAdapter, portAdapter, adapted);

		}

	}

	private void initPortAdapter(IDAGNodeAdapter nodeAdapter,
			IDAGPortAdapter portAdapter, DAGPort adapted) {

	}

	private void buildEdgeAdapter(IDAGInstructionAdapter graphAdapter,
			DAGEdge adapted) {

		IDAGEdgeAdapter edgeAdapter = null;
		initEdgeAdapter(graphAdapter, edgeAdapter, adapted);

		IDAGPortAdapter sourceAdapter = graphAdapter.getAdapterPort(adapted
				.getSrc()); //sourceExpression
		IDAGPortAdapter sinkAdapter = graphAdapter.getAdapterPort(adapted
				.getSink()); //sourceExpression

		edgeAdapter = factory.connect(adapted, sourceAdapter, sinkAdapter);

	}

	private void buildInternalEdge(IDAGInstructionAdapter graphAdapter,
			DAGEdge adapted) {

		if (!this.visitedsEdge.contains(adapted)) {
			this.visitedsEdge.add(adapted);
			buildEdgeAdapter(graphAdapter, adapted);
		}

	}

	private void initEdgeAdapter(IDAGInstructionAdapter graphAdapter,
			IDAGEdgeAdapter edgeAdapter, DAGEdge adapted) {

	}

}
