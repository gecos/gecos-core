package fr.irisa.cairn.gecos.model.type.utils;

import static java.util.stream.Collectors.joining;

import java.util.stream.Stream;

public class SimpleDeclarator extends Declarator {

	String identifier;

	public SimpleDeclarator(String pointers, String identifier, String arrays_params) {
		super(pointers, arrays_params);
		this.identifier = identifier;
	}
	
	@Override
	public String getIdentifier() {
		return identifier;
	}
	
	@Override
	public String getCCode() {
		return Stream.of(
			pointers,
			identifier,
			arrays_params
		).sequential().filter(s -> s!= null && !s.isEmpty())
		.collect(joining(" "));
	}
	
}
