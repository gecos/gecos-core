#include <stdio.h>
#define M	128
#define N	128

#ifndef _min_
#define _min_(a, b)       ((a) < (b) ? (a) : (b))
#endif
#ifndef _max_
#define _max_(a, b)       ((a) < (b) ? (b) : (a))
#endif

void testInstToQuad(int in[N][M], int out[N][M]) {

	int i, j, a;

	// Loop nest without '{' and '}'

	for (i = 0; i < _min_(N, M); i++)
		out[i][i] = in[_min_(N,M) - 1 ][_min_(N,M) - 2];

	//*******

	a=M-1;

	for (i = 0; i < N; i++)
		if (M > 100)
			for (j = 0; j < 100; j++)
				out[i][j] = (in[i][j] + i + j) * 2;
		else
			for (j = 0; j < M; j++)
				out[i][j] = (in[i][j] + i + j) * 2;

	//*******

	a=M-1;

	// Loop nest with '{' and '}'
	for (i = 0; i < N; i++) {
		a = 10;
		for (j = 0; j < M - a; j++) {
			out[i][j] = (in[i][j] + i + j) * 2;
		}
	}

	//*******

	// if test
	if (M % 2 == 0)
		if (N % 2 == 0)
			out[N - 2][M - 2] = (M + N) / 2;

	//*******

	// while test
	i = 1;
	while (i < N - 1) {
		j = 2;
		while (j < M - 2) {
			out[i][j] = (in[i - 1][j - 2] - in[i - 1][j - 1] * 2 + i * j) * 2
					+ in[i][j] * (in[i + 1][j + 1] - in[i + 1][j + 2]);
			j++;
		}
		i++;
	}

	//*******

	while (i < 500)
		i += (100-50);


	//*******

	// do while test
	i = 1;
	do {
		j = 2;
		do {
			out[i][j] = (in[i - 1][j - 2] - in[i - 1][j - 1] * 2 + i * j) * 2
					+ in[i][j] * (in[i + 1][j + 1] - in[i + 1][j + 2]);
			j++;
		} while (j < M - 2);
		i++;
	} while (i < N - 1);

}

