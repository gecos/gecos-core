package fr.irisa.cairn.gecos.model.c.generator;

import templates.xtend.GecosTypeTemplate;
import fr.irisa.cairn.gecos.model.extensions.generators.ExtendableGenerator;
import fr.irisa.cairn.gecos.model.extensions.generators.IGeneratorDispatchStrategy;

public class ExtendableTypeCGenerator extends ExtendableGenerator {

	public static ExtendableTypeCGenerator eInstance = new ExtendableTypeCGenerator();

	public ExtendableTypeCGenerator() {
		super(new GecosTypeTemplate());
	}
	
	public ExtendableTypeCGenerator(IGeneratorDispatchStrategy dispatchStrategy) {
		super(dispatchStrategy, new GecosTypeTemplate());
	}
}