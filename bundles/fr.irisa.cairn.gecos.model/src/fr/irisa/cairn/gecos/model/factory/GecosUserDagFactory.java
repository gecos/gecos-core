package fr.irisa.cairn.gecos.model.factory;

import gecos.core.Symbol;
import gecos.dag.DAGArrayValueNode;
import gecos.dag.DAGCallNode;
import gecos.dag.DAGControlEdge;
import gecos.dag.DAGDataEdge;
import gecos.dag.DAGEdge;
import gecos.dag.DAGFieldInstruction;
import gecos.dag.DAGFloatImmNode;
import gecos.dag.DAGInPort;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGIntImmNode;
import gecos.dag.DAGNode;
import gecos.dag.DAGNumberedOutNode;
import gecos.dag.DAGNumberedSymbolNode;
import gecos.dag.DAGOpNode;
import gecos.dag.DAGOutNode;
import gecos.dag.DAGOutPort;
import gecos.dag.DAGPatternNode;
import gecos.dag.DAGPhiNode;
import gecos.dag.DAGSSADefNode;
import gecos.dag.DAGSSAUseNode;
import gecos.dag.DAGSimpleArrayNode;
import gecos.dag.DAGStringImmNode;
import gecos.dag.DAGSymbolNode;
import gecos.dag.DAGVectorExpandNode;
import gecos.dag.DAGVectorExtractNode;
import gecos.dag.DAGVectorOpNode;
import gecos.dag.DAGVectorPackNode;
import gecos.dag.DAGVectorShuffleNode;
import gecos.dag.DagFactory;
import gecos.dag.DependencyType;
import gecos.instrs.ArrayValueInstruction;
import gecos.instrs.FieldInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.SimpleArrayInstruction;
import gecos.types.Type;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

public class GecosUserDagFactory {

	private static DagFactory facto = DagFactory.eINSTANCE;

	private GecosUserDagFactory() {}

	public static DAGOpNode createDAGOPNode(DAGInstruction instruction,
			String string, Type type) {
		DAGOpNode opNode = facto.createDAGOpNode();
		opNode.setOpcode(string);
		if (type == null) {
			throw new UnsupportedOperationException(
					"Cannot create DAGNode "+string+" with null type ");
		}
		opNode.setType(type);
		instruction.getNodes().add(opNode);

		return opNode;
	}
	
	public static DAGVectorOpNode createDAGVectorOPNode(DAGInstruction dag, String opcode, Type type) {
		DAGVectorOpNode vop = facto.createDAGVectorOpNode();
		vop.setOpcode(opcode);
		if (type == null) {
			throw new UnsupportedOperationException("Cannot create DAGNode "+opcode+" with null type ");
		}
		vop.setType(type);
		dag.getNodes().add(vop);
		return vop;
	}
	
	public static DAGVectorShuffleNode createDAGVectorShuffleNode(DAGInstruction dag, Collection<Integer> permutation, Type type) {
		DAGVectorShuffleNode vop = facto.createDAGVectorShuffleNode();
		if (type == null) {
			throw new UnsupportedOperationException("Cannot create DAGShuffleNode with null type ");
		}
		//TODO check permutation validity
		vop.setType(type);
		vop.getPermutation().clear();
		vop.getPermutation().addAll(permutation);
		dag.getNodes().add(vop);
		return vop;
	}
	
	public static DAGVectorExpandNode createDAGVectorExpandNode(DAGInstruction dag, Collection<Integer> permutation, Type type) {
		DAGVectorExpandNode vop = facto.createDAGVectorExpandNode();
		if (type == null) {
			throw new UnsupportedOperationException("Cannot create DAGExpandNode with null type ");
		}
		//TODO check permutation validity
		vop.setType(type);
		vop.getPermutation().clear();
		vop.getPermutation().addAll(permutation);
		dag.getNodes().add(vop);
		return vop;
	}
	
	public static DAGVectorExtractNode createDAGVectorExtractNode(DAGInstruction dag, int pos, Type type) {
		DAGVectorExtractNode vop = facto.createDAGVectorExtractNode();
		if (type == null) {
			throw new UnsupportedOperationException("Cannot create DAGVectorExtractNode with null type ");
		}
		//TODO check pos validity
		vop.setType(type);
		vop.setPos(pos);
		dag.getNodes().add(vop);
		return vop;
	}
	
	public static DAGVectorPackNode createDAGVectorPackNode(DAGInstruction dag, Type type) {
		DAGVectorPackNode vop = facto.createDAGVectorPackNode();
		if (type == null) {
			throw new UnsupportedOperationException("Cannot create DAGVectorPackNode with null type ");
		}
		vop.setType(type);
		dag.getNodes().add(vop);
		return vop;
	}
	
	public static DAGIntImmNode createDAGIntImmediateNode(DAGInstruction instruction, long i) {
		IntInstruction inst = GecosUserInstructionFactory.Int(i);
		return createDAGIntImmediateNode(instruction, inst);
	}

	public static DAGIntImmNode createDAGIntImmediateNode(DAGInstruction instruction,
			IntInstruction i) {
		DAGIntImmNode intNode = facto.createDAGIntImmNode();
		intNode.setValue((IntInstruction) i.copy());
		instruction.getNodes().add(intNode);
		intNode.setType(i.getType());
		intNode.setValue((IntInstruction) i.copy());
		intNode.setIntValue(i.getValue());
		return intNode;
	}

	public static DAGFloatImmNode createDAGFloatImmediateNode(DAGInstruction instruction, double i) {
		FloatInstruction inst = GecosUserInstructionFactory.Float(i);
		return createDAGFloatImmediateNode(instruction, inst);
	}
	
	public static DAGFloatImmNode createDAGFloatImmediateNode(
			DAGInstruction instruction, FloatInstruction i) {
		DAGFloatImmNode floatNode = facto.createDAGFloatImmNode();
		floatNode.setValue((FloatInstruction) i.copy());
		instruction.getNodes().add(floatNode);
		floatNode.setType(i.getType());
		floatNode.setValue((FloatInstruction) i.copy());
		floatNode.setFloatValue(i.getValue());

		return floatNode;
	}
	
	public static DAGStringImmNode createDAGStringImmNode(DAGInstruction instruction, String value) {
		DAGStringImmNode stringNode = facto.createDAGStringImmNode();
		stringNode.setType(GecosUserTypeFactory.STRING(value.length()+1));
		stringNode.setStringValue(value);
		instruction.getNodes().add(stringNode);

		return stringNode;
	}
	
	public static DAGFieldInstruction createDAGFieldNode(DAGInstruction instruction, FieldInstruction field){
		DAGFieldInstruction fieldNode = facto.createDAGFieldInstruction();
		createDAGOutputPort(fieldNode);
		instruction.getNodes().add(fieldNode);
		fieldNode.setField(field.getField());
		fieldNode.setType(field.getType());
		return fieldNode;
		
	}

	public static DAGDataEdge createDAGDataEdge(DAGInPort sink, DAGOutPort src) {
		DAGDataEdge createDAGDataEdge = facto.createDAGDataEdge();
		createDAGDataEdge.setSink(sink);
		createDAGDataEdge.setSrc(src);
		return createDAGDataEdge;
	}

	public static DAGControlEdge createDAGControlEdge(DAGInPort sink,DAGOutPort source, DependencyType type) {
		DAGControlEdge createDAGControlEdge = facto.createDAGControlEdge();
		createDAGControlEdge.setSink(sink);
		createDAGControlEdge.setSrc(source);
		createDAGControlEdge.setType(type);
		return createDAGControlEdge;
	}
	
	public static DAGNode createDAGArrayValueNode(DAGInstruction instruction,
			ArrayValueInstruction i) {

		 DAGArrayValueNode createDAGArrayValueNode = facto.createDAGArrayValueNode();
		  createDAGArrayValueNode.setType(i.getType());
		 instruction.getNodes().add(createDAGArrayValueNode);
		 createDAGOutputPort(createDAGArrayValueNode);
		return createDAGArrayValueNode;
	}

	
	
	public static DAGCallNode createDAGCallNode(DAGInstruction instruction,
			String symbolAddress, Type type, boolean hasOutput) {
		DAGCallNode callNode = facto.createDAGCallNode();
		// FIXME createDAGCallNode.
		createDAGOutputPort(callNode);
		instruction.getNodes().add(callNode);
		callNode.setType(type);
		callNode.setName(symbolAddress);
		return callNode;

	}

	public static DAGSymbolNode createDAGSymbolNode(DAGInstruction instruction,
			Symbol symbol) {
		DAGSymbolNode dagSymNode = facto.createDAGSymbolNode();
		createDAGOutputPort(dagSymNode);
		dagSymNode.setSymbol(symbol);
		instruction.getNodes().add(dagSymNode);
		dagSymNode.setType(symbol.getType());
		return dagSymNode;
	}

	public static DAGSimpleArrayNode createDAGSimpleArrayNode(DAGInstruction dag, SimpleArrayInstruction instr) {
		DAGSimpleArrayNode node = createDAGSimpleArrayNode(dag, instr.getSymbol(), instr.getType());
		for (int i = 0; i < instr.getIndex().size(); i++)
			createDAGInputPort(node);
		return node;
	}
	
	public static DAGSimpleArrayNode createDAGSimpleArrayNode(DAGInstruction dag, Symbol symbol, Type type) {
		DAGSimpleArrayNode node = facto.createDAGSimpleArrayNode();
		createDAGOutputPort(node);
		node.setType(type);
		node.setSymbol(symbol);
		dag.getNodes().add(node);
		return node;
	}

	public static DAGNumberedSymbolNode createDAGNumberedSymbolNode(
			DAGInstruction instruction,
			Symbol sym, int number) {
		DAGNumberedSymbolNode numSymNode = facto.createDAGNumberedSymbolNode();
		numSymNode.setNumber(number);
		createDAGOutputPort(numSymNode);
		numSymNode.setSymbol(sym);
		instruction.getNodes().add(numSymNode);
		numSymNode.setType(sym.getType());
		/// FIXME Inter BasicBlock dependencies should be handled here
		//numSymNode.setSymbolDefinition(null);
		return numSymNode;
	}

	public static DAGOutPort createDAGOutputPort(DAGNode dagNode) {
		DAGOutPort createDAGOutPort = facto.createDAGOutPort();
		dagNode.getOutputs().add(createDAGOutPort);
		createDAGOutPort.setIdent(dagNode.getOutputs().size()-1);
		return createDAGOutPort;
	}

	public static DAGInPort createDAGInputPort(DAGNode dagNode) {
		DAGInPort createDAGInPort = facto.createDAGInPort();
		dagNode.getInputs().add(createDAGInPort);
		createDAGInPort.setIdent(dagNode.getInputs().size()-1);
		return createDAGInPort;
	}

	public static DAGOutNode createDAGOutNode(DAGInstruction instruction,
			Symbol symbol) {
		DAGOutNode createDAGOutNode = facto.createDAGOutNode();
		createDAGOutNode.setSymbol(symbol);
		createDAGOutNode.setType(symbol.getType());
		instruction.getNodes().add(createDAGOutNode);
		return createDAGOutNode;
	}

	public static DAGNumberedOutNode createDAGNumberedOutNode(DAGInstruction instruction,
			Symbol symbol, int number, Type t) {
		DAGNumberedOutNode createDAGNumberedOutNode = facto
				.createDAGNumberedOutNode();
		createDAGNumberedOutNode.setSymbol(symbol);
		createDAGNumberedOutNode.setNumber(number);
		createDAGNumberedOutNode.setType(t);
			

		instruction.getNodes().add(createDAGNumberedOutNode);
		return createDAGNumberedOutNode;
	}

	public static DAGInstruction createDAGInstruction() {
		DAGInstruction createDAGInstruction = facto.createDAGInstruction();
		return createDAGInstruction;
	}
	
	public static DAGPatternNode createDAGPatternNode() {
		DAGPatternNode l = DagFactory.eINSTANCE.createDAGPatternNode();
		DAGInstruction i = DagFactory.eINSTANCE.createDAGInstruction();
		l.setPattern(i);
		l.setOpcode("pattern");
		return l;
	}
	
	public static DAGSSAUseNode createDAGSSAUseNode(DAGSSADefNode def){
		DAGSSAUseNode useNode = DagFactory.eINSTANCE.createDAGSSAUseNode();
		useNode.setSymbol(def.getSymbol());
		useNode.setDef(def);
		def.getSSAUses().add(useNode);
		return useNode;		
	}
	
	public static DAGSSADefNode createDAGSSADefNode(Symbol sym){
		DAGSSADefNode defNode = DagFactory.eINSTANCE.createDAGSSADefNode();
		defNode.setSymbol(sym);
		return defNode;		
	}
	
	public static DAGPhiNode createDAGPhiNode(){
		DAGPhiNode phiNode = DagFactory.eINSTANCE.createDAGPhiNode();
		return phiNode;		
	}
	
	public static void fillUpPatternNode(DAGPatternNode pattern,EList<DAGNode> nodes) {
		DAGInstruction parent  = pattern.getParent();
		DAGInstruction dag = pattern.getPattern();
		dag.getNodes().addAll(nodes); 
		
		
		Set<DAGNode> set = new LinkedHashSet<DAGNode>(); 
		for(DAGNode node : nodes) {
			set.add(node);
		}
		// list clone to avoid concurrent modification exception
		for(DAGEdge edge : new BasicEList<DAGEdge>(parent.getEdges())) {
			DAGNode sink =edge.getSinkNode();
			DAGNode src = edge.getSourceNode();
			if (set.contains(sink) && set.contains(src)) {
				// Internal edge
				dag.getEdges().add(edge);
			} else if (set.contains(sink)){
				// Incoming edge
				DAGInPort oldInport = edge.getSink();
				DAGInPort newInputPort = GecosUserDagFactory.createDAGInputPort(pattern);
				edge.setSink(newInputPort);
				pattern.getInputsMap().put(newInputPort, oldInport);
				
			} else if (set.contains(src)) {
				// Outgoing Edge
				DAGOutPort oldOutport = edge.getSrc();
				DAGOutPort newOutputPort = GecosUserDagFactory.createDAGOutputPort(pattern);
				edge.setSrc(newOutputPort);
				pattern.getOutputsMap().put(newOutputPort, oldOutport);				
			}
				
		}		
		// check if the resulting DAG is cyclic
		// FIXME : TODO
		
	}

	public static void reconnectIncomingEdge(DAGEdge e, DAGPatternNode pattern,DAGNode node) {
		DAGInPort sink = e.getSink();
		DAGInPort src = pattern.getInputsMap().get(sink);
		pattern.getInputsMap().remove(sink);
		e.setSink(src);
	}
		public static void addNodeToPattern(DAGPatternNode pattern,DAGNode node) {
		DAGInstruction parent= pattern.getParent();
		if (node.getParent()== parent) {
			Set<DAGNode> set = new LinkedHashSet<DAGNode>(); 
			for(DAGNode _node : pattern.getPattern().getNodes()) {
				set.add(_node);
			}
		
			// list clone to avoid concurrent modification exception
			for(DAGEdge edge : new BasicEList<DAGEdge>(parent.getEdges())) {
				DAGNode sink =edge.getSinkNode();
				DAGNode src = edge.getSourceNode();
				if (src==node && sink==pattern){
					// Incoming edge
					DAGInPort oldInport = edge.getSink();
					DAGInPort newInputPort = GecosUserDagFactory.createDAGInputPort(pattern);
					edge.setSink(newInputPort);
					pattern.getInputsMap().put(newInputPort, oldInport);
					
				} else if (sink==node && src==pattern) {
					// Outgoing Edge
					DAGOutPort oldOutport = edge.getSrc();
					DAGOutPort newOutputPort = GecosUserDagFactory.createDAGOutputPort(pattern);
					edge.setSrc(newOutputPort);
					pattern.getOutputsMap().put(newOutputPort, oldOutport);				
				}
					
			}		
		// check if the resulting DAG is cyclic
		}
		
	}
		
}
