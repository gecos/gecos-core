package fr.irisa.cairn.gecos.model.utils.dotprinter;

import fr.irisa.cairn.gecos.model.extensions.switchs.AdaptableDAGSwitch;
import gecos.dag.DAGArrayValueNode;
import gecos.dag.DAGCallNode;
import gecos.dag.DAGControlEdge;
import gecos.dag.DAGDataEdge;
import gecos.dag.DAGEdge;
import gecos.dag.DAGFloatImmNode;
import gecos.dag.DAGInPort;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGIntImmNode;
import gecos.dag.DAGNode;
import gecos.dag.DAGNumberedOutNode;
import gecos.dag.DAGNumberedSymbolNode;
import gecos.dag.DAGOpNode;
import gecos.dag.DAGOutNode;
import gecos.dag.DAGOutPort;
import gecos.dag.DAGPatternNode;
import gecos.dag.DAGPhiNode;
import gecos.dag.DAGSSADefNode;
import gecos.dag.DAGSSAUseNode;
import gecos.dag.DAGSimpleArrayNode;
import gecos.dag.DAGSymbolNode;

public class DAGDottyExport extends AdaptableDAGSwitch<String> {

	public int unique;

	public String makeNodeID(int unique, DAGNode node) {
		if(node instanceof DAGPatternNode) {
			return "cluster_n_" + unique + "_" + node.hashCode();			
		} else {
			return "n_" + unique + "_" + node.hashCode();
		}
	}


	@Override
	public String caseDAGInstruction(DAGInstruction object) {
		
		StringBuffer sb = new StringBuffer();
		sb.append("digraph DAG_"+object.getBasicBlock().getNumber()+" { \n");
		for(DAGNode node: object.getNodes()){
			sb.append(doSwitch(node) + "\n");
		}
		for(DAGEdge edge: object.getEdges()){
			sb.append(doSwitch(edge) + "\n");
		}
		sb.append("}\n");
		return sb.toString();
	}


	@Override
	public String caseDAGArrayValueNode(DAGArrayValueNode node) {
		return makeNodeID(unique, node) + " [shape=\"box\",label=\""
				+ node.toString() + "\"];";
	}


	@Override
	public String caseDAGPatternNode(DAGPatternNode _node) {
		;
		StringBuffer sb = new StringBuffer();
		sb.append("// hello world\n");
		sb.append("subgraph cluster_"+makeNodeID(unique, _node) +" { \n");
		sb.append("compound=true;\n");
		sb.append("label=\"Pattern_"+_node.getIdent()+"\";\n");
		sb.append("color=red;\n");
		sb.append("style=solid;\n");

		for(DAGNode node: _node.getPattern().getNodes()){
			sb.append(doSwitch(node) + "\n");
		}
		for(DAGEdge edge: _node.getPattern().getEdges()){
			sb.append(doSwitch(edge) + "\n");
		}
		sb.append("}\n");
		return sb.toString();
		
	}


	@Override
	public String caseDAGNode(DAGNode node) {
		return makeNodeID(unique, node) + " [shape=\"box\",label=\""
				+ node.toString() + "\"];";
	}

	@Override
	public String caseDAGCallNode(DAGCallNode node) {
		return makeNodeID(unique, node) + " [shape=\"box\",label=\""
				+ node.getName() + "(...)" + "\"];";
	}

	@Override
	public String caseDAGControlEdge(DAGControlEdge inst) {
		return makeNodeID(unique, getSrcNode(inst.getSrc())) + " -> "
				+ makeNodeID(unique, getSinkNode(inst.getSink()))
				+ "[label=\""+inst.getType()+"\",style=dotted]" + ";";
	}

	private DAGNode getSinkNode(DAGInPort e) {
		DAGNode parent = e.getParent();
		if (parent instanceof DAGPatternNode) {
			DAGInPort dagInPort = ((DAGPatternNode)parent).getInputsMap().get(e);
			return getSinkNode(dagInPort);
		} else {
			return parent; 
		}
	}
	
	private DAGNode getSrcNode(DAGOutPort e) {
		DAGNode parent = e.getParent();
		if (parent instanceof DAGPatternNode) {
			DAGOutPort dagInPort = ((DAGPatternNode)parent).getOutputsMap().get(e);
			return getSrcNode(dagInPort);
		} else {
			return parent; 
		}
	}

	@Override
	public String caseDAGDataEdge(DAGDataEdge inst) {
		return makeNodeID(unique, getSrcNode(inst.getSrc())) + " -> "
				+ makeNodeID(unique, getSinkNode(inst.getSink()))
				+ "[style=filled]" + ";";
	}

	@Override
	public String caseDAGFloatImmNode(DAGFloatImmNode String) {
		return makeNodeID(unique, String) + " [shape=\"box\",label=\""
				+ String.getFloatValue()+ "\"];";
	}

	@Override
	public String caseDAGIntImmNode(DAGIntImmNode String) {
		return makeNodeID(unique, String) + " [shape=\"invhouse\",label=\""
				+ String.getValue().getValue() + "\"];";
	}

	@Override
	public String caseDAGOpNode(DAGOpNode String) {
		String opcode = String.getOpcode();
		if (opcode.equals("add"))
			opcode = "+";
		else if (opcode.equals("mul"))
			opcode = "X";
		else if (opcode.equals("div"))
			opcode = "/";
		else if (opcode.equals("sub"))
			opcode = "-";
		else if (opcode.equals("shl"))
			opcode = "<<";
		else if (opcode.equals("shr"))
			opcode = ">>";
		else if (opcode.equals("INDIR"))
			opcode = "*()";
		else if (opcode.equals("SET"))
			opcode = ":=";
		else if (opcode.equals("or"))
			opcode = "||";
		else if (opcode.equals("and"))
			opcode = "&&";
		else if (opcode.equals("xor"))
			opcode = "^";
		else if (opcode.equals("neg"))
			opcode = "-";
		else if (opcode.equals("CVT"))
			opcode = "cvt\\n" + String.getType().toString().toLowerCase();
		return makeNodeID(unique, String) + "[shape=\"circle\",label=\""
				+ opcode + "\"];";
	}

	@Override
	public String caseDAGOutNode(DAGOutNode node) {
		return makeNodeID(unique, node)
				+ "[shape=\"house\",style=filled,fillcolor=grey,label=\""
				+ node.getSymbol().getName() + "\"];";
	}
	@Override
	public String caseDAGNumberedOutNode(DAGNumberedOutNode node) {
		return makeNodeID(unique, node)
				+ "[shape=\"house\",style=filled,fillcolor=grey,label=\""
				+ node.getSymbol().getName() + "_" + node.getNumber()
				+ "\"];";
	}
	@Override
	public String caseDAGSymbolNode(DAGSymbolNode String) {
		return makeNodeID(unique, String)
				+ "[shape=\"invhouse\",style=filled,fillcolor=grey,label=\""
				+ String.getSymbol().getName() + "\"];";
	}

	@Override
	public String caseDAGSimpleArrayNode(DAGSimpleArrayNode String) {
		return makeNodeID(unique, String) + "[shape=\"box3d\",label=\""
				+ String.getSymbol().getName() + "[]\"];";
	}

	@Override
	public String caseDAGNumberedSymbolNode(DAGNumberedSymbolNode String) {
		return makeNodeID(unique, String)
				+ "[shape=\"invhouse\",style=filled,fillcolor=grey,label=\""
				+ String.getSymbol().getName() + "_" + String.getNumber()+ "\"];";

	}
	
	@Override
	public String caseDAGSSADefNode(DAGSSADefNode node){
		return makeNodeID(unique, node) 
				+ "[shape=\"house\",style=filled,fillcolor=grey,label=\""
				+ node.getSymbol().getName() + "\"];";
	}
	
	@Override
	public String caseDAGSSAUseNode(DAGSSAUseNode node){
		return makeNodeID(unique, node) 
				+ "[shape=\"invhouse\",style=filled,fillcolor=grey,label=\""
				+ node.getSymbol().getName() + "\"];";
	}
	
	@Override
	public String caseDAGPhiNode(DAGPhiNode node){
		return makeNodeID(unique, node) 
				+ "[shape=\"circle\",style=filled,fillcolor=grey,label=\""
				+ "phi"+ "\"];";

	}



}
