/**
 */
package profiling.frontend.directives;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see profiling.frontend.directives.DirectivesFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel operationReflection='false' basePackage='profiling.frontend'"
 * @generated
 */
public interface DirectivesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "directives";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.gecos.org/profiling/frontend/directives";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "directives";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DirectivesPackage eINSTANCE = profiling.frontend.directives.impl.DirectivesPackageImpl.init();

	/**
	 * The meta object id for the '{@link profiling.frontend.directives.impl.DirectiveImpl <em>Directive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see profiling.frontend.directives.impl.DirectiveImpl
	 * @see profiling.frontend.directives.impl.DirectivesPackageImpl#getDirective()
	 * @generated
	 */
	int DIRECTIVE = 0;

	/**
	 * The feature id for the '<em><b>Call</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECTIVE__CALL = 0;

	/**
	 * The number of structural features of the '<em>Directive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECTIVE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link profiling.frontend.directives.impl.SizeDirectiveImpl <em>Size Directive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see profiling.frontend.directives.impl.SizeDirectiveImpl
	 * @see profiling.frontend.directives.impl.DirectivesPackageImpl#getSizeDirective()
	 * @generated
	 */
	int SIZE_DIRECTIVE = 1;

	/**
	 * The feature id for the '<em><b>Call</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZE_DIRECTIVE__CALL = DIRECTIVE__CALL;

	/**
	 * The feature id for the '<em><b>NAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZE_DIRECTIVE__NAME = DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Address</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZE_DIRECTIVE__ADDRESS = DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Sizes Outermost First</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZE_DIRECTIVE__SIZES_OUTERMOST_FIRST = DIRECTIVE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Size Directive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZE_DIRECTIVE_FEATURE_COUNT = DIRECTIVE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link profiling.frontend.directives.impl.SaveDirectiveImpl <em>Save Directive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see profiling.frontend.directives.impl.SaveDirectiveImpl
	 * @see profiling.frontend.directives.impl.DirectivesPackageImpl#getSaveDirective()
	 * @generated
	 */
	int SAVE_DIRECTIVE = 2;

	/**
	 * The feature id for the '<em><b>Call</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAVE_DIRECTIVE__CALL = DIRECTIVE__CALL;

	/**
	 * The feature id for the '<em><b>NAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAVE_DIRECTIVE__NAME = DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Address</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAVE_DIRECTIVE__ADDRESS = DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Sizes Outermost First</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAVE_DIRECTIVE__SIZES_OUTERMOST_FIRST = DIRECTIVE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Save Directive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAVE_DIRECTIVE_FEATURE_COUNT = DIRECTIVE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link profiling.frontend.directives.impl.InjectDirectiveImpl <em>Inject Directive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see profiling.frontend.directives.impl.InjectDirectiveImpl
	 * @see profiling.frontend.directives.impl.DirectivesPackageImpl#getInjectDirective()
	 * @generated
	 */
	int INJECT_DIRECTIVE = 3;

	/**
	 * The feature id for the '<em><b>Call</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INJECT_DIRECTIVE__CALL = DIRECTIVE__CALL;

	/**
	 * The feature id for the '<em><b>NAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INJECT_DIRECTIVE__NAME = DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Address</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INJECT_DIRECTIVE__ADDRESS = DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Source Directive</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INJECT_DIRECTIVE__SOURCE_DIRECTIVE = DIRECTIVE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Sizes Outermost First</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INJECT_DIRECTIVE__SIZES_OUTERMOST_FIRST = DIRECTIVE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Inject Directive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INJECT_DIRECTIVE_FEATURE_COUNT = DIRECTIVE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link profiling.frontend.directives.impl.InjectorSourceDirectiveImpl <em>Injector Source Directive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see profiling.frontend.directives.impl.InjectorSourceDirectiveImpl
	 * @see profiling.frontend.directives.impl.DirectivesPackageImpl#getInjectorSourceDirective()
	 * @generated
	 */
	int INJECTOR_SOURCE_DIRECTIVE = 4;

	/**
	 * The feature id for the '<em><b>Call</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INJECTOR_SOURCE_DIRECTIVE__CALL = DIRECTIVE__CALL;

	/**
	 * The number of structural features of the '<em>Injector Source Directive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INJECTOR_SOURCE_DIRECTIVE_FEATURE_COUNT = DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link profiling.frontend.directives.impl.FromFileDirectiveImpl <em>From File Directive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see profiling.frontend.directives.impl.FromFileDirectiveImpl
	 * @see profiling.frontend.directives.impl.DirectivesPackageImpl#getFromFileDirective()
	 * @generated
	 */
	int FROM_FILE_DIRECTIVE = 5;

	/**
	 * The feature id for the '<em><b>Call</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_FILE_DIRECTIVE__CALL = INJECTOR_SOURCE_DIRECTIVE__CALL;

	/**
	 * The feature id for the '<em><b>NAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_FILE_DIRECTIVE__NAME = INJECTOR_SOURCE_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>File Path</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_FILE_DIRECTIVE__FILE_PATH = INJECTOR_SOURCE_DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>From File Directive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_FILE_DIRECTIVE_FEATURE_COUNT = INJECTOR_SOURCE_DIRECTIVE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link profiling.frontend.directives.impl.FromVariableDirectiveImpl <em>From Variable Directive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see profiling.frontend.directives.impl.FromVariableDirectiveImpl
	 * @see profiling.frontend.directives.impl.DirectivesPackageImpl#getFromVariableDirective()
	 * @generated
	 */
	int FROM_VARIABLE_DIRECTIVE = 6;

	/**
	 * The feature id for the '<em><b>Call</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_VARIABLE_DIRECTIVE__CALL = INJECTOR_SOURCE_DIRECTIVE__CALL;

	/**
	 * The feature id for the '<em><b>NAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_VARIABLE_DIRECTIVE__NAME = INJECTOR_SOURCE_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Inst</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_VARIABLE_DIRECTIVE__INST = INJECTOR_SOURCE_DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>From Variable Directive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_VARIABLE_DIRECTIVE_FEATURE_COUNT = INJECTOR_SOURCE_DIRECTIVE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link profiling.frontend.directives.impl.RandomUniformDirectiveImpl <em>Random Uniform Directive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see profiling.frontend.directives.impl.RandomUniformDirectiveImpl
	 * @see profiling.frontend.directives.impl.DirectivesPackageImpl#getRandomUniformDirective()
	 * @generated
	 */
	int RANDOM_UNIFORM_DIRECTIVE = 7;

	/**
	 * The feature id for the '<em><b>Call</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_UNIFORM_DIRECTIVE__CALL = INJECTOR_SOURCE_DIRECTIVE__CALL;

	/**
	 * The feature id for the '<em><b>NAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_UNIFORM_DIRECTIVE__NAME = INJECTOR_SOURCE_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Min</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_UNIFORM_DIRECTIVE__MIN = INJECTOR_SOURCE_DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Max</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_UNIFORM_DIRECTIVE__MAX = INJECTOR_SOURCE_DIRECTIVE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Seed</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_UNIFORM_DIRECTIVE__SEED = INJECTOR_SOURCE_DIRECTIVE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Random Uniform Directive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_UNIFORM_DIRECTIVE_FEATURE_COUNT = INJECTOR_SOURCE_DIRECTIVE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link profiling.frontend.directives.impl.RandomNormalDirectiveImpl <em>Random Normal Directive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see profiling.frontend.directives.impl.RandomNormalDirectiveImpl
	 * @see profiling.frontend.directives.impl.DirectivesPackageImpl#getRandomNormalDirective()
	 * @generated
	 */
	int RANDOM_NORMAL_DIRECTIVE = 8;

	/**
	 * The feature id for the '<em><b>Call</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_NORMAL_DIRECTIVE__CALL = INJECTOR_SOURCE_DIRECTIVE__CALL;

	/**
	 * The feature id for the '<em><b>NAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_NORMAL_DIRECTIVE__NAME = INJECTOR_SOURCE_DIRECTIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Mean</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_NORMAL_DIRECTIVE__MEAN = INJECTOR_SOURCE_DIRECTIVE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Stddev</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_NORMAL_DIRECTIVE__STDDEV = INJECTOR_SOURCE_DIRECTIVE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Seed</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_NORMAL_DIRECTIVE__SEED = INJECTOR_SOURCE_DIRECTIVE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Random Normal Directive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_NORMAL_DIRECTIVE_FEATURE_COUNT = INJECTOR_SOURCE_DIRECTIVE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link profiling.frontend.directives.DirectiveName <em>Directive Name</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see profiling.frontend.directives.DirectiveName
	 * @see profiling.frontend.directives.impl.DirectivesPackageImpl#getDirectiveName()
	 * @generated
	 */
	int DIRECTIVE_NAME = 9;


	/**
	 * Returns the meta object for class '{@link profiling.frontend.directives.Directive <em>Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Directive</em>'.
	 * @see profiling.frontend.directives.Directive
	 * @generated
	 */
	EClass getDirective();

	/**
	 * Returns the meta object for the reference '{@link profiling.frontend.directives.Directive#getCall <em>Call</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Call</em>'.
	 * @see profiling.frontend.directives.Directive#getCall()
	 * @see #getDirective()
	 * @generated
	 */
	EReference getDirective_Call();

	/**
	 * Returns the meta object for class '{@link profiling.frontend.directives.SizeDirective <em>Size Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Size Directive</em>'.
	 * @see profiling.frontend.directives.SizeDirective
	 * @generated
	 */
	EClass getSizeDirective();

	/**
	 * Returns the meta object for the attribute '{@link profiling.frontend.directives.SizeDirective#getNAME <em>NAME</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>NAME</em>'.
	 * @see profiling.frontend.directives.SizeDirective#getNAME()
	 * @see #getSizeDirective()
	 * @generated
	 */
	EAttribute getSizeDirective_NAME();

	/**
	 * Returns the meta object for the reference '{@link profiling.frontend.directives.SizeDirective#getAddress <em>Address</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Address</em>'.
	 * @see profiling.frontend.directives.SizeDirective#getAddress()
	 * @see #getSizeDirective()
	 * @generated
	 */
	EReference getSizeDirective_Address();

	/**
	 * Returns the meta object for the reference list '{@link profiling.frontend.directives.SizeDirective#getSizesOutermostFirst <em>Sizes Outermost First</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sizes Outermost First</em>'.
	 * @see profiling.frontend.directives.SizeDirective#getSizesOutermostFirst()
	 * @see #getSizeDirective()
	 * @generated
	 */
	EReference getSizeDirective_SizesOutermostFirst();

	/**
	 * Returns the meta object for class '{@link profiling.frontend.directives.SaveDirective <em>Save Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Save Directive</em>'.
	 * @see profiling.frontend.directives.SaveDirective
	 * @generated
	 */
	EClass getSaveDirective();

	/**
	 * Returns the meta object for the attribute '{@link profiling.frontend.directives.SaveDirective#getNAME <em>NAME</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>NAME</em>'.
	 * @see profiling.frontend.directives.SaveDirective#getNAME()
	 * @see #getSaveDirective()
	 * @generated
	 */
	EAttribute getSaveDirective_NAME();

	/**
	 * Returns the meta object for the reference '{@link profiling.frontend.directives.SaveDirective#getAddress <em>Address</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Address</em>'.
	 * @see profiling.frontend.directives.SaveDirective#getAddress()
	 * @see #getSaveDirective()
	 * @generated
	 */
	EReference getSaveDirective_Address();

	/**
	 * Returns the meta object for the reference list '{@link profiling.frontend.directives.SaveDirective#getSizesOutermostFirst <em>Sizes Outermost First</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sizes Outermost First</em>'.
	 * @see profiling.frontend.directives.SaveDirective#getSizesOutermostFirst()
	 * @see #getSaveDirective()
	 * @generated
	 */
	EReference getSaveDirective_SizesOutermostFirst();

	/**
	 * Returns the meta object for class '{@link profiling.frontend.directives.InjectDirective <em>Inject Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inject Directive</em>'.
	 * @see profiling.frontend.directives.InjectDirective
	 * @generated
	 */
	EClass getInjectDirective();

	/**
	 * Returns the meta object for the attribute '{@link profiling.frontend.directives.InjectDirective#getNAME <em>NAME</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>NAME</em>'.
	 * @see profiling.frontend.directives.InjectDirective#getNAME()
	 * @see #getInjectDirective()
	 * @generated
	 */
	EAttribute getInjectDirective_NAME();

	/**
	 * Returns the meta object for the reference '{@link profiling.frontend.directives.InjectDirective#getAddress <em>Address</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Address</em>'.
	 * @see profiling.frontend.directives.InjectDirective#getAddress()
	 * @see #getInjectDirective()
	 * @generated
	 */
	EReference getInjectDirective_Address();

	/**
	 * Returns the meta object for the containment reference '{@link profiling.frontend.directives.InjectDirective#getSourceDirective <em>Source Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Source Directive</em>'.
	 * @see profiling.frontend.directives.InjectDirective#getSourceDirective()
	 * @see #getInjectDirective()
	 * @generated
	 */
	EReference getInjectDirective_SourceDirective();

	/**
	 * Returns the meta object for the reference list '{@link profiling.frontend.directives.InjectDirective#getSizesOutermostFirst <em>Sizes Outermost First</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sizes Outermost First</em>'.
	 * @see profiling.frontend.directives.InjectDirective#getSizesOutermostFirst()
	 * @see #getInjectDirective()
	 * @generated
	 */
	EReference getInjectDirective_SizesOutermostFirst();

	/**
	 * Returns the meta object for class '{@link profiling.frontend.directives.InjectorSourceDirective <em>Injector Source Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Injector Source Directive</em>'.
	 * @see profiling.frontend.directives.InjectorSourceDirective
	 * @generated
	 */
	EClass getInjectorSourceDirective();

	/**
	 * Returns the meta object for class '{@link profiling.frontend.directives.FromFileDirective <em>From File Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>From File Directive</em>'.
	 * @see profiling.frontend.directives.FromFileDirective
	 * @generated
	 */
	EClass getFromFileDirective();

	/**
	 * Returns the meta object for the attribute '{@link profiling.frontend.directives.FromFileDirective#getNAME <em>NAME</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>NAME</em>'.
	 * @see profiling.frontend.directives.FromFileDirective#getNAME()
	 * @see #getFromFileDirective()
	 * @generated
	 */
	EAttribute getFromFileDirective_NAME();

	/**
	 * Returns the meta object for the reference '{@link profiling.frontend.directives.FromFileDirective#getFilePath <em>File Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>File Path</em>'.
	 * @see profiling.frontend.directives.FromFileDirective#getFilePath()
	 * @see #getFromFileDirective()
	 * @generated
	 */
	EReference getFromFileDirective_FilePath();

	/**
	 * Returns the meta object for class '{@link profiling.frontend.directives.FromVariableDirective <em>From Variable Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>From Variable Directive</em>'.
	 * @see profiling.frontend.directives.FromVariableDirective
	 * @generated
	 */
	EClass getFromVariableDirective();

	/**
	 * Returns the meta object for the attribute '{@link profiling.frontend.directives.FromVariableDirective#getNAME <em>NAME</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>NAME</em>'.
	 * @see profiling.frontend.directives.FromVariableDirective#getNAME()
	 * @see #getFromVariableDirective()
	 * @generated
	 */
	EAttribute getFromVariableDirective_NAME();

	/**
	 * Returns the meta object for the reference '{@link profiling.frontend.directives.FromVariableDirective#getInst <em>Inst</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Inst</em>'.
	 * @see profiling.frontend.directives.FromVariableDirective#getInst()
	 * @see #getFromVariableDirective()
	 * @generated
	 */
	EReference getFromVariableDirective_Inst();

	/**
	 * Returns the meta object for class '{@link profiling.frontend.directives.RandomUniformDirective <em>Random Uniform Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Random Uniform Directive</em>'.
	 * @see profiling.frontend.directives.RandomUniformDirective
	 * @generated
	 */
	EClass getRandomUniformDirective();

	/**
	 * Returns the meta object for the attribute '{@link profiling.frontend.directives.RandomUniformDirective#getNAME <em>NAME</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>NAME</em>'.
	 * @see profiling.frontend.directives.RandomUniformDirective#getNAME()
	 * @see #getRandomUniformDirective()
	 * @generated
	 */
	EAttribute getRandomUniformDirective_NAME();

	/**
	 * Returns the meta object for the reference '{@link profiling.frontend.directives.RandomUniformDirective#getMin <em>Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Min</em>'.
	 * @see profiling.frontend.directives.RandomUniformDirective#getMin()
	 * @see #getRandomUniformDirective()
	 * @generated
	 */
	EReference getRandomUniformDirective_Min();

	/**
	 * Returns the meta object for the reference '{@link profiling.frontend.directives.RandomUniformDirective#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Max</em>'.
	 * @see profiling.frontend.directives.RandomUniformDirective#getMax()
	 * @see #getRandomUniformDirective()
	 * @generated
	 */
	EReference getRandomUniformDirective_Max();

	/**
	 * Returns the meta object for the reference '{@link profiling.frontend.directives.RandomUniformDirective#getSeed <em>Seed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Seed</em>'.
	 * @see profiling.frontend.directives.RandomUniformDirective#getSeed()
	 * @see #getRandomUniformDirective()
	 * @generated
	 */
	EReference getRandomUniformDirective_Seed();

	/**
	 * Returns the meta object for class '{@link profiling.frontend.directives.RandomNormalDirective <em>Random Normal Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Random Normal Directive</em>'.
	 * @see profiling.frontend.directives.RandomNormalDirective
	 * @generated
	 */
	EClass getRandomNormalDirective();

	/**
	 * Returns the meta object for the attribute '{@link profiling.frontend.directives.RandomNormalDirective#getNAME <em>NAME</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>NAME</em>'.
	 * @see profiling.frontend.directives.RandomNormalDirective#getNAME()
	 * @see #getRandomNormalDirective()
	 * @generated
	 */
	EAttribute getRandomNormalDirective_NAME();

	/**
	 * Returns the meta object for the reference '{@link profiling.frontend.directives.RandomNormalDirective#getMean <em>Mean</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Mean</em>'.
	 * @see profiling.frontend.directives.RandomNormalDirective#getMean()
	 * @see #getRandomNormalDirective()
	 * @generated
	 */
	EReference getRandomNormalDirective_Mean();

	/**
	 * Returns the meta object for the reference '{@link profiling.frontend.directives.RandomNormalDirective#getStddev <em>Stddev</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Stddev</em>'.
	 * @see profiling.frontend.directives.RandomNormalDirective#getStddev()
	 * @see #getRandomNormalDirective()
	 * @generated
	 */
	EReference getRandomNormalDirective_Stddev();

	/**
	 * Returns the meta object for the reference '{@link profiling.frontend.directives.RandomNormalDirective#getSeed <em>Seed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Seed</em>'.
	 * @see profiling.frontend.directives.RandomNormalDirective#getSeed()
	 * @see #getRandomNormalDirective()
	 * @generated
	 */
	EReference getRandomNormalDirective_Seed();

	/**
	 * Returns the meta object for enum '{@link profiling.frontend.directives.DirectiveName <em>Directive Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Directive Name</em>'.
	 * @see profiling.frontend.directives.DirectiveName
	 * @generated
	 */
	EEnum getDirectiveName();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DirectivesFactory getDirectivesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link profiling.frontend.directives.impl.DirectiveImpl <em>Directive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see profiling.frontend.directives.impl.DirectiveImpl
		 * @see profiling.frontend.directives.impl.DirectivesPackageImpl#getDirective()
		 * @generated
		 */
		EClass DIRECTIVE = eINSTANCE.getDirective();

		/**
		 * The meta object literal for the '<em><b>Call</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIRECTIVE__CALL = eINSTANCE.getDirective_Call();

		/**
		 * The meta object literal for the '{@link profiling.frontend.directives.impl.SizeDirectiveImpl <em>Size Directive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see profiling.frontend.directives.impl.SizeDirectiveImpl
		 * @see profiling.frontend.directives.impl.DirectivesPackageImpl#getSizeDirective()
		 * @generated
		 */
		EClass SIZE_DIRECTIVE = eINSTANCE.getSizeDirective();

		/**
		 * The meta object literal for the '<em><b>NAME</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIZE_DIRECTIVE__NAME = eINSTANCE.getSizeDirective_NAME();

		/**
		 * The meta object literal for the '<em><b>Address</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIZE_DIRECTIVE__ADDRESS = eINSTANCE.getSizeDirective_Address();

		/**
		 * The meta object literal for the '<em><b>Sizes Outermost First</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIZE_DIRECTIVE__SIZES_OUTERMOST_FIRST = eINSTANCE.getSizeDirective_SizesOutermostFirst();

		/**
		 * The meta object literal for the '{@link profiling.frontend.directives.impl.SaveDirectiveImpl <em>Save Directive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see profiling.frontend.directives.impl.SaveDirectiveImpl
		 * @see profiling.frontend.directives.impl.DirectivesPackageImpl#getSaveDirective()
		 * @generated
		 */
		EClass SAVE_DIRECTIVE = eINSTANCE.getSaveDirective();

		/**
		 * The meta object literal for the '<em><b>NAME</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SAVE_DIRECTIVE__NAME = eINSTANCE.getSaveDirective_NAME();

		/**
		 * The meta object literal for the '<em><b>Address</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SAVE_DIRECTIVE__ADDRESS = eINSTANCE.getSaveDirective_Address();

		/**
		 * The meta object literal for the '<em><b>Sizes Outermost First</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SAVE_DIRECTIVE__SIZES_OUTERMOST_FIRST = eINSTANCE.getSaveDirective_SizesOutermostFirst();

		/**
		 * The meta object literal for the '{@link profiling.frontend.directives.impl.InjectDirectiveImpl <em>Inject Directive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see profiling.frontend.directives.impl.InjectDirectiveImpl
		 * @see profiling.frontend.directives.impl.DirectivesPackageImpl#getInjectDirective()
		 * @generated
		 */
		EClass INJECT_DIRECTIVE = eINSTANCE.getInjectDirective();

		/**
		 * The meta object literal for the '<em><b>NAME</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INJECT_DIRECTIVE__NAME = eINSTANCE.getInjectDirective_NAME();

		/**
		 * The meta object literal for the '<em><b>Address</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INJECT_DIRECTIVE__ADDRESS = eINSTANCE.getInjectDirective_Address();

		/**
		 * The meta object literal for the '<em><b>Source Directive</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INJECT_DIRECTIVE__SOURCE_DIRECTIVE = eINSTANCE.getInjectDirective_SourceDirective();

		/**
		 * The meta object literal for the '<em><b>Sizes Outermost First</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INJECT_DIRECTIVE__SIZES_OUTERMOST_FIRST = eINSTANCE.getInjectDirective_SizesOutermostFirst();

		/**
		 * The meta object literal for the '{@link profiling.frontend.directives.impl.InjectorSourceDirectiveImpl <em>Injector Source Directive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see profiling.frontend.directives.impl.InjectorSourceDirectiveImpl
		 * @see profiling.frontend.directives.impl.DirectivesPackageImpl#getInjectorSourceDirective()
		 * @generated
		 */
		EClass INJECTOR_SOURCE_DIRECTIVE = eINSTANCE.getInjectorSourceDirective();

		/**
		 * The meta object literal for the '{@link profiling.frontend.directives.impl.FromFileDirectiveImpl <em>From File Directive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see profiling.frontend.directives.impl.FromFileDirectiveImpl
		 * @see profiling.frontend.directives.impl.DirectivesPackageImpl#getFromFileDirective()
		 * @generated
		 */
		EClass FROM_FILE_DIRECTIVE = eINSTANCE.getFromFileDirective();

		/**
		 * The meta object literal for the '<em><b>NAME</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FROM_FILE_DIRECTIVE__NAME = eINSTANCE.getFromFileDirective_NAME();

		/**
		 * The meta object literal for the '<em><b>File Path</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FROM_FILE_DIRECTIVE__FILE_PATH = eINSTANCE.getFromFileDirective_FilePath();

		/**
		 * The meta object literal for the '{@link profiling.frontend.directives.impl.FromVariableDirectiveImpl <em>From Variable Directive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see profiling.frontend.directives.impl.FromVariableDirectiveImpl
		 * @see profiling.frontend.directives.impl.DirectivesPackageImpl#getFromVariableDirective()
		 * @generated
		 */
		EClass FROM_VARIABLE_DIRECTIVE = eINSTANCE.getFromVariableDirective();

		/**
		 * The meta object literal for the '<em><b>NAME</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FROM_VARIABLE_DIRECTIVE__NAME = eINSTANCE.getFromVariableDirective_NAME();

		/**
		 * The meta object literal for the '<em><b>Inst</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FROM_VARIABLE_DIRECTIVE__INST = eINSTANCE.getFromVariableDirective_Inst();

		/**
		 * The meta object literal for the '{@link profiling.frontend.directives.impl.RandomUniformDirectiveImpl <em>Random Uniform Directive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see profiling.frontend.directives.impl.RandomUniformDirectiveImpl
		 * @see profiling.frontend.directives.impl.DirectivesPackageImpl#getRandomUniformDirective()
		 * @generated
		 */
		EClass RANDOM_UNIFORM_DIRECTIVE = eINSTANCE.getRandomUniformDirective();

		/**
		 * The meta object literal for the '<em><b>NAME</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RANDOM_UNIFORM_DIRECTIVE__NAME = eINSTANCE.getRandomUniformDirective_NAME();

		/**
		 * The meta object literal for the '<em><b>Min</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RANDOM_UNIFORM_DIRECTIVE__MIN = eINSTANCE.getRandomUniformDirective_Min();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RANDOM_UNIFORM_DIRECTIVE__MAX = eINSTANCE.getRandomUniformDirective_Max();

		/**
		 * The meta object literal for the '<em><b>Seed</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RANDOM_UNIFORM_DIRECTIVE__SEED = eINSTANCE.getRandomUniformDirective_Seed();

		/**
		 * The meta object literal for the '{@link profiling.frontend.directives.impl.RandomNormalDirectiveImpl <em>Random Normal Directive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see profiling.frontend.directives.impl.RandomNormalDirectiveImpl
		 * @see profiling.frontend.directives.impl.DirectivesPackageImpl#getRandomNormalDirective()
		 * @generated
		 */
		EClass RANDOM_NORMAL_DIRECTIVE = eINSTANCE.getRandomNormalDirective();

		/**
		 * The meta object literal for the '<em><b>NAME</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RANDOM_NORMAL_DIRECTIVE__NAME = eINSTANCE.getRandomNormalDirective_NAME();

		/**
		 * The meta object literal for the '<em><b>Mean</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RANDOM_NORMAL_DIRECTIVE__MEAN = eINSTANCE.getRandomNormalDirective_Mean();

		/**
		 * The meta object literal for the '<em><b>Stddev</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RANDOM_NORMAL_DIRECTIVE__STDDEV = eINSTANCE.getRandomNormalDirective_Stddev();

		/**
		 * The meta object literal for the '<em><b>Seed</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RANDOM_NORMAL_DIRECTIVE__SEED = eINSTANCE.getRandomNormalDirective_Seed();

		/**
		 * The meta object literal for the '{@link profiling.frontend.directives.DirectiveName <em>Directive Name</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see profiling.frontend.directives.DirectiveName
		 * @see profiling.frontend.directives.impl.DirectivesPackageImpl#getDirectiveName()
		 * @generated
		 */
		EEnum DIRECTIVE_NAME = eINSTANCE.getDirectiveName();

	}

} //DirectivesPackage
