package fr.irisa.cairn.gecos.model.factory;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;

import gecos.annotations.AnnotatedElement;
import gecos.annotations.AnnotationKeys;
import gecos.annotations.AnnotationsFactory;
import gecos.annotations.CommentAnnotation;
import gecos.annotations.ExtendedAnnotation;
import gecos.annotations.FileLocationAnnotation;
import gecos.annotations.IAnnotation;
import gecos.annotations.PragmaAnnotation;
import gecos.annotations.StringAnnotation;

public class GecosUserAnnotationFactory {
	
	//XXX was in ProcedureSet
	public static final String FILE_NAME_ANOTATION_KEY = "original_file_base_name";

	//TODO: were in PragmaAnnotation:
	//used by [CDTFrontEnd, CGenerator]
	public static final String CODEGEN_IGNORE_ANNOTATION = "S2S4HLS:IGNORE";
	public static final String CODEGEN_PRINT_ANNOTATION = "S2S4HLS:MODULE:PRINT:";
	public static final String HEADER_SYMBOL_ANNOTATION = "HEADER_SYMBOL";
	public static final String CODEGEN_COPYFILE_ANNOTATION = "_CODEGEN.COPYFILE_";
	
	
	//TODO: were in SymbolDefinitionAnnotation
//	public static final String ANNOTATION_KEY = "SymbolDefinitionAnnotation";
	
	//XXX were in IntInstruction: 
	//used by [CDTFrontEnd]
	public static final String PRAGMA_HEX_LITTERAL = "HEX_LITTERAL";
	public static final String PRAGMA_OCT_LITTERAL = "OCT_LITTERAL"; 
	public static final String PRAGMA_BIN_LITTERAL = "BIN_LITTERAL";
	public static final String PRAGMA_CHAR_LITTERAL = "CHAR_LITTERAL";

	public static final String PRAGMA_PURE_FUNCTION = "GCS_PURE_FUNCTION";

	
	private GecosUserAnnotationFactory() {}
	
	public static PragmaAnnotation createPragma(String... strings) {
		PragmaAnnotation pragmaAnnotation = AnnotationsFactory.eINSTANCE.createPragmaAnnotation();
		for(String string : strings) {
			pragmaAnnotation.getContent().add(string);
		}
		return pragmaAnnotation;
	}


	public static void pragma(AnnotatedElement annotatedElement, List<PragmaAnnotation> pragmaAnnotations) {
		for(int j = 0; j < pragmaAnnotations.size(); j++) {
			PragmaAnnotation pragma = annotatedElement.getPragma();
			if (pragma == null)
				annotatedElement.setAnnotation(AnnotationKeys.PRAGMA_ANNOTATION_KEY.getLiteral(), pragmaAnnotations.get(j));
			else {
				pragma.getContent().add(pragmaAnnotations.get(j).getContent().get(0));
			}
		}
	}
	
	public static void pragma(AnnotatedElement elmt, String... content) {
		PragmaAnnotation pragma = elmt.getPragma();
		if (pragma == null) {
			pragma = createPragma(content);
			elmt.setAnnotation(AnnotationKeys.PRAGMA_ANNOTATION_KEY.getLiteral(), pragma);
		} else {
			for (String string : content) {
				pragma.getContent().add(string);
			}
		}
	}
	
	public static void mergePragmas(AnnotatedElement origElmt, AnnotatedElement newElmt) {
		PragmaAnnotation pragma = newElmt.getPragma();
		
		//1. get a merged collection of the content with uniqueness 
		Set<String> contents = new LinkedHashSet<>();
		if (pragma != null)
			contents.addAll(pragma.getContent());
		if (origElmt.getPragma() != null)
			contents.addAll(origElmt.getPragma().getContent());

		//2. override contents of existing pragma, or create new one
		if (contents.size() > 0) {
			if (pragma != null) {
				pragma.getContent().clear();
				pragma.getContent().addAll(contents);
			} else {
				pragma = createPragma(contents.toArray(new String[contents.size()]));
				newElmt.setAnnotation(AnnotationKeys.PRAGMA_ANNOTATION_KEY.getLiteral(),pragma);
			}
		}
	}

	/**
	 * Appends the comment to the existing annotation if it exists, or create a new one.
	 * @param elmt
	 * @param comment
	 */
	public static void comment(AnnotatedElement elmt, String comment) {
		CommentAnnotation annotation = (CommentAnnotation) elmt.getAnnotation(AnnotationKeys.COMMENT_ANNOTATION_KEY.getLiteral());
		if (annotation == null) {
			annotation = comment(comment);
		} else {
			String content = annotation.getContent();
			content += "; "+comment;
			annotation.setContent(content);
		}
		elmt.setAnnotation(AnnotationKeys.COMMENT_ANNOTATION_KEY.getLiteral(), annotation);
	}
	
	public static CommentAnnotation comment(String string) {
		CommentAnnotation comment = AnnotationsFactory.eINSTANCE.createCommentAnnotation();
		comment.setContent(string);
		return comment;
	}
	
	public static StringAnnotation STRING(String string) {
		StringAnnotation annot = AnnotationsFactory.eINSTANCE.createStringAnnotation();
		annot.setContent(string);
		return annot;
	}
	
	/**
	 * Create a {@link FileLocationAnnotation} with the specified parameters
	 * and add it to the {@link AnnotatedElement} {@code e}.
	 * 
	 * @param e
	 * @param fileName
	 * @param startLine
	 * @param endLine
	 * @param nodeLength
	 * @param nodeOffset
	 */
	public static void fileLocation(AnnotatedElement e, String fileName, int startLine, int endLine, int nodeLength, int nodeOffset) {
		FileLocationAnnotation annotation = fileLocation(fileName, startLine, endLine, nodeLength, nodeOffset);
		e.setAnnotation(AnnotationKeys.FILE_LOCATION_ANNOTATION_KEY.getLiteral(), annotation);
	}
	
	public static FileLocationAnnotation fileLocation(String fileName, int startLine, int endLine, int nodeLength, int nodeOffset) {
		FileLocationAnnotation annot = AnnotationsFactory.eINSTANCE.createFileLocationAnnotation();
		annot.setFilename(fileName);
		annot.setStartingLine(startLine);
		annot.setEndingLine(endLine);
		annot.setNodeLength(nodeLength);
		annot.setNodeOffset(nodeOffset);
		return annot;
	}
	
	public static ExtendedAnnotation extendedContains(EObject... fieldsToContain) {
		ExtendedAnnotation annot = AnnotationsFactory.eINSTANCE.createExtendedAnnotation();
		EList<EObject> fields = annot.getFields();
		Arrays.stream(fieldsToContain)
			.filter(Objects::nonNull)
			.forEach(fields::add);
		return annot;
	}
	
	public static ExtendedAnnotation extendedRefers(EObject... references) {
		ExtendedAnnotation annot = AnnotationsFactory.eINSTANCE.createExtendedAnnotation();
		EList<EObject> fields = annot.getRefs();
		Arrays.stream(references)
			.filter(Objects::nonNull)
			.forEach(fields::add);
		return annot;
	}

	public static void copyAnnotations(AnnotatedElement src, AnnotatedElement target) {
		if (target != null)
			for (String key : src.getAnnotations().keySet()) {
				IAnnotation annotation = src.getAnnotations().get(key);
				IAnnotation copy = (IAnnotation) (new Copier()).copy(annotation);
				target.getAnnotations().put(key, copy);
			}
	}
}
