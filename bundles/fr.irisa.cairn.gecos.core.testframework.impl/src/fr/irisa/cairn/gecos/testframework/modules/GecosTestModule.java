package fr.irisa.cairn.gecos.testframework.modules;


import static fr.irisa.cairn.gecos.testframework.stages.Stages.forEach;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.forEachPairWithFirst;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.onlyIf;
import static java.lang.System.out;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.gecos.core.testframework.impl.GSCheckers;
import fr.irisa.cairn.gecos.core.testframework.impl.GSGenerators;
import fr.irisa.cairn.gecos.core.testframework.impl.GSProjectVersion;
import fr.irisa.cairn.gecos.core.testframework.utils.GSProjectUtils;
import fr.irisa.cairn.gecos.testframework.model.AbstractTestTemplate;
import fr.irisa.cairn.gecos.testframework.model.ITestStage;
import fr.irisa.cairn.gecos.testframework.s2s.Comparators;
import fr.irisa.cairn.gecos.testframework.s2s.Compilers;
import fr.irisa.cairn.gecos.testframework.s2s.Linkers;
import fr.irisa.cairn.gecos.testframework.s2s.Operators;
import fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow;
import fr.irisa.cairn.gecos.testframework.stages.SequenceStage;
import fr.irisa.cairn.gecos.testframework.stages.Stages;
import fr.irisa.r2d2.gecos.framework.GSArg;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import fr.irisa.r2d2.gecos.framework.utils.FileUtils;
import gecos.gecosproject.GecosProject;

@GSModule("This module allow testing a Gecos transformation within a script.\n"
		+ "It performs the following steps, depending on the specified options:\n"
		+ "   - Generate C code for of the specified original (orig) and transformed (mod) projects.\n"
		+ "   - Compile the generated codes.\n"
		+ "   - Link each version of the code to generate an executable.\n"
		+ "   - Execute both 'orig' and 'mod'.\n"
		+ "   - Compare their outputs.")
public class GecosTestModule extends AbstractTestTemplate<GSProjectVersion> {

	public enum TestMode { // exclusive i.e. cannot be combined
		CODEGEN("CODEGEN"),		// generate code only
		COMPILE("COMPILE"), 	// + compile
		EXECUTE("EXECUTE"),		// + link and execute
		AUTO("AUTO");			// automatically decide if COMPILE or EXECUTE
		
		TestMode(String v) {
			this.value = v;
		}
		String value;
	}
	
	public enum CompareMode { // can be combined (except NONE)
		EXIT("EXIT"),		// check the exit code: if non-zero fails //TODO should fail if exit codes are different regardless of their values. 
		STDOUT("STDOUT"); 	// compare outputs on stdout
//		FILES("FILES");		// compare output files (should be specified)
		
		CompareMode(String v) {
			this.value = v;
		}
		String value;
	}

	private List<GecosProject> projects;
	private TestMode testMode;
	private CompareMode compareMode;
	private Boolean compareCode;
	private Long timeout;
	
	private SequenceStage comparators = Stages.chain();
	private boolean doCompile;
	private boolean doLink;
	
	
	@GSModuleConstructor(args={
		@GSArg(name="projects", info="List of GecosProjects to be tested."),
		@GSArg(name="testMode", info="String specifying the Test mode. It can be one of the following:\n"
			+"   'CODEGEN': generate code only (orig and mod).\n"
			+"   'COMPILE': 'CODEGEN' + compile (orig and mod).\n"
			+"   'EXECUTE': 'COMPILE' + link and execute (orig and mod).\n"
			+"   ['AUTO'] : 'EXECUTE' if a 'main' procedure is available in both versions, otherwise 'COMPILE'."),
		@GSArg(name="compareMode", info="String specifying the Comparison mode when 'EXECUTE'. Supported values are:"
			+"   ['EXIT'] : Check the exit code of the 'EXECUTE' commands: "
			+"      if non-zero the test is considered a failure.\n"
//			+"   'FILES'  : (TODO) 'EXIT' + compare output files.\n"
			+"   'STDOUT' : 'EXIT' + compare outputs on stdout."),
		@GSArg(name="compareCode", info="if true the source code files of both versions are compared."),
		@GSArg(name="codegenDir", info="code generation root directory.")
	})
	public GecosTestModule(List<GecosProject> projects, String testMode, String compareMode, Boolean compareCode, String codegenDir) { 
		//TODO add options to specify external sources, incDirs, output files ...
		this.projects = projects;
		this.testMode = TestMode.valueOf(testMode);
		this.compareMode = CompareMode.valueOf(compareMode);
		this.compareCode = compareCode;
		this.timeout = null; //TODO add parameter to specify timeout
		this.testMethodOutputDir = Paths.get(codegenDir);
		if(cleanMethodOutputDirBefore && Files.exists(testMethodOutputDir))
			FileUtils.deleteRecursive(testMethodOutputDir.toFile());
		
		//TODO add an option to specify an entry-point procedure, in case no main is available,
		// to automatically generate a main function that calls it with generated inputs..
	}
	
	public void compute() {
		if(projects == null || projects.isEmpty()) {
			System.out.println("[GecosTestModule][Abort] The specified projects list cannot be null or empty!");
			return;
		}
		
		this.versions = createVersions();
		
		setup();
		
		ITestStage testFlow = buildTestFlow();
		runTestFlow(testFlow);
		
		out.println("[GecosTestModule] Test succeded.");
	}
	
	@Override
	protected ITestStage buildTestFlow() {
		
		return S2STestFlow.builder()
			.then(o -> out.println("[GecosTestModule] Starting test flow"))
			.then(o -> out.println("[GecosTestModule] Checking for Dangling references..."))
			.check(forEach(GSCheckers.danglingRef()))
			.then(o -> out.println("[GecosTestModule] Generating C code..."))
			.codegen(forEach(GSGenerators.cGenerator(false)))
			.then(onlyIf(o -> doCompile, o -> out.println("[GecosTestModule] Compiling generated code...")))
			.compile(forEach(Compilers.gcc()).onlyIf(o -> doCompile))
			.then(onlyIf(o -> doLink, o -> out.println("[GecosTestModule] Linking generated code...")))
			.link(forEach(Linkers.gcc()).onlyIf(o -> doLink))
			.then(onlyIf(o -> doLink, o -> out.println("[GecosTestModule] Running generated code...")))
			.run(forEach(Operators.defaultExecutorNoArgs()).onlyIf(o -> doLink))
			.verify(comparators)
			.then(o -> out.println("[GecosTestModule] Finished."))
			.build();
	}
	
	private void setup() {
		if(testMode.equals(TestMode.AUTO))
			testMode = versions.stream().allMatch(GSProjectVersion::hasMainProcedure) ?
				TestMode.EXECUTE:
				TestMode.COMPILE;
		
		doCompile = true;
		doLink = true;
		
		if(compareCode) comparators = comparators
			.then(o -> out.println("[GecosTestModule] Comparing generated source code..."))
			.then(forEachPairWithFirst(Comparators.sourceCodeEquals()));
		
		switch (testMode) {
		case CODEGEN:
			doCompile = false;
			doLink = false;
			break;
		case COMPILE:
			doLink = false;
			break;
		case EXECUTE:
			setExecCompareMode();
			break;
		default:
			throw new RuntimeException("Test mode '"+testMode+"' is not supported.");
		}
	}
	
	private void setExecCompareMode() {
		switch (compareMode) {
		case STDOUT:
			comparators = comparators
				.then(o -> out.println("[GecosTestModule] Comparing stdout files..."))
				.then(forEachPairWithFirst(Comparators.stdoutEquals()));
			break;
//		case FILES:
//			comparators.then(forEachPairWithFirst(Comparators.fileEquals(v -> {
//				if(v.isRoot())
//					return null; //TODO
//				return null; //TODO
//			})));
//			break;
		case EXIT:
		default:
			break;
		}
	}
	
	private List<GSProjectVersion> createVersions() {
		List<GSProjectVersion> versions = new ArrayList<>();
		GSProjectVersion previous = null;
		for(int i=0; i < projects.size(); i++) {
			GecosProject project = projects.get(i);
			try {
				GSProjectUtils.parseGecosProject(project);
			} catch (Exception e) {
				throw new RuntimeException("[GecosTestModule][ERROR] Failed to create Version from project: " + project + "\n", e);
			}
			GSProjectVersion modVer = createVersion(project, "ver-"+i, previous);
			versions.add(modVer);
			previous = modVer;
		}
		return versions;
	}
	
	protected static final String EXECUTABLE_EXT = ".out";
	protected static final String STDOUT_EXT = ".stdout";
	protected static final String STDERR_EXT = ".stderr";
	
	private GSProjectVersion createVersion(GecosProject project, String name, GSProjectVersion previous) {
		GSProjectVersion v = new GSProjectVersion();
		v.setProject(project);
		v.setName(name);
		v.setPrevious(previous);
		v.setTimeout(timeout);
		v.setCodegenDir(testMethodOutputDir.resolve(name));
		v.setExecutable(v.getOutputDir().resolve(v.getName() + EXECUTABLE_EXT));
		v.setStdoutFile(v.getOutputDir().resolve(v.getName() + STDOUT_EXT));
		v.setStderrFile(v.getOutputDir().resolve(v.getName() + STDERR_EXT));
		return v;
	}

}
