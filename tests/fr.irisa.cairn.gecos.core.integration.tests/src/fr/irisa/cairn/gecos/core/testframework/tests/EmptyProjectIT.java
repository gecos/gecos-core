package fr.irisa.cairn.gecos.core.testframework.tests;

import static fr.irisa.cairn.gecos.testframework.s2s.Comparators.regularFileComparator;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.forEachPairWithFirst;

import org.eclipse.emf.common.util.EList;
import org.junit.Assume;
import org.junit.Test;

import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.core.integration.tests.utils.DotAppDirData;
import fr.irisa.cairn.gecos.core.testframework.impl.GSProjectVersion;
import fr.irisa.cairn.gecos.core.testframework.impl.GecosS2STestTemplate;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.testframework.data.ICxxProjectData;
import fr.irisa.cairn.gecos.testframework.dataprovider.DataFromPathProvider;
import fr.irisa.cairn.gecos.testframework.dataprovider.ResourcesLocation;
import fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName;
import fr.irisa.cairn.gecos.testframework.stages.Stages;
import fr.irisa.r2d2.gecos.framework.utils.FileUtils;
import gecos.blocks.CompositeBlock;
import gecos.core.Procedure;

/**
 * Transform the original project into an empty one.
 * The aim is to test the makefile chain. All tests should fail 
 * (compile but produce different outputs fail)
 * 
 * @author aelmouss
 */
public class EmptyProjectIT extends GecosS2STestTemplate<GSProjectVersion> {
	
	@Override
	protected void configure() {
		super.configure();
		
		registerTestFlow(DotAppDirData.class, DotAppDirData.defaultDotAppDirDataTestFlow(
				DotAppDirData::defaultMakeCompileTarget, DotAppDirData::defaultMakeRunTarget)
			// The outputs should be different.
			// !!!: they may still be identical in case the computation endup producing the same output,
			// but in general this should not be the case. We can skip the following assertion.
			.replaceStage(S2SStageName.VERIFY, forEachPairWithFirst(regularFileComparator(DotAppDirData::getOutputFile,
						(f1, f2) -> ! FileUtils.areFilesEquals(f1.toFile(), f2.toFile())))));
	}
	
	/**
	 * NOTE: Use only data providers supplying applications producing an output file !
	 * Assumes that project does not have only a 'main' procedure. The main is skipped.
	 */
	@Test
	@UseDataProvider(location=DataFromPathProvider.class, value=DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation(value = "resources/src-c/dot-app-data/misc", dataClasses = DotAppDirData.class)
	@ResourcesLocation(value = "resources/src-c/dot-app-data/cse", dataClasses = DotAppDirData.class)
	@ResourcesLocation(value = "resources/src-c/dot-app-data/idfix", dataClasses = DotAppDirData.class)
	public void testMakefile(ICxxProjectData d) {
		findTestFlow(d.getClass()).replaceStage(S2SStageName.CHECK, Stages.NOP);
		
		runTest(d, v -> {
			EList<Procedure> procs = v.getProject().listProcedures();
			int nProcs = procs.size();
			
			Assume.assumeFalse(nProcs == 1 && procs.get(0).getSymbolName().equals("main"));
				
			procs.stream().filter(p -> !p.getSymbolName().equals("main"))
				.forEach(p -> {
					CompositeBlock body = p.getBody();
					body.replace(body.getChildren().get(1), GecosUserBlockFactory.CompositeBlock());
				});
		});
	}
	
}
