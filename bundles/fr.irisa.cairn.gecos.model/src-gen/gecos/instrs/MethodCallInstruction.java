/**
 */
package gecos.instrs;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Method Call Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.MethodCallInstruction#getArgs <em>Args</em>}</li>
 *   <li>{@link gecos.instrs.MethodCallInstruction#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getMethodCallInstruction()
 * @model
 * @generated
 */
public interface MethodCallInstruction extends AddressComponent {
	/**
	 * Returns the value of the '<em><b>Args</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.instrs.Instruction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Args</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Args</em>' containment reference list.
	 * @see gecos.instrs.InstrsPackage#getMethodCallInstruction_Args()
	 * @model containment="true"
	 * @generated
	 */
	EList<Instruction> getArgs();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see gecos.instrs.InstrsPackage#getMethodCallInstruction_Name()
	 * @model unique="false" dataType="gecos.instrs.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link gecos.instrs.MethodCallInstruction#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitMethodCallInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; _xblockexpression = null;\n{\n\tfinal &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; list = new &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;();\n\tlist.add(this.getAddress());\n\tlist.addAll(this.getArgs());\n\t_xblockexpression = &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;unmodifiableEList(list);\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	EList<Instruction> listChildren();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model argUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getArgs().add(arg);'"
	 * @generated
	 */
	void addArg(Instruction arg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.MethodCallInstruction%&gt;))\n{\n\treturn (&lt;%com.google.common.base.Objects%&gt;.equal(this.getName(), ((&lt;%gecos.instrs.MethodCallInstruction%&gt;)instr).getName()) &amp;&amp; super.isSame(instr));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // MethodCallInstruction
