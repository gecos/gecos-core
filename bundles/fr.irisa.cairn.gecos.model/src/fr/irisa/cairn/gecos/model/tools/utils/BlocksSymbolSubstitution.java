package fr.irisa.cairn.gecos.model.tools.utils;

import gecos.blocks.BasicBlock;
import gecos.blocks.CompositeBlock;
import gecos.core.Symbol;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 
 * @author amorvan
 * @generated NOT
 */
public class BlocksSymbolSubstitution extends BlocksDefaultSwitch<Object> {

	private Map<Symbol,Symbol> map;
	
	public BlocksSymbolSubstitution(Map<Symbol,Symbol> m) {
		this.map = m;
	}

	public BlocksSymbolSubstitution(Symbol olds,Symbol news) {
		this.map = new LinkedHashMap<Symbol,Symbol>();
		this.map.put(olds,news);
	}
	
	@Override
	public Object caseCompositeBlock(CompositeBlock b) {
		for (Symbol s : b.getScope().getSymbols()) {
			if (s.getValue() != null) (new InstrsSymbolSubstitution(this.map)).doSwitch(s.getValue());
		}
		return super.caseCompositeBlock(b);
	}
	
	@Override
	public Object caseBasicBlock(BasicBlock object) {
		(new InstrsSymbolSubstitution(this.map)).doSwitch(object);
		return null;
	}
}
