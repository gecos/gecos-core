/**
 */
package gecos.instrs;

import gecos.types.Type;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Array Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.ArrayInstruction#getDest <em>Dest</em>}</li>
 *   <li>{@link gecos.instrs.ArrayInstruction#getIndex <em>Index</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getArrayInstruction()
 * @model
 * @generated
 */
public interface ArrayInstruction extends ComponentInstruction {
	/**
	 * Returns the value of the '<em><b>Dest</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dest</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dest</em>' containment reference.
	 * @see #setDest(Instruction)
	 * @see gecos.instrs.InstrsPackage#getArrayInstruction_Dest()
	 * @model containment="true"
	 * @generated
	 */
	Instruction getDest();

	/**
	 * Sets the value of the '{@link gecos.instrs.ArrayInstruction#getDest <em>Dest</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dest</em>' containment reference.
	 * @see #getDest()
	 * @generated
	 */
	void setDest(Instruction value);

	/**
	 * Returns the value of the '<em><b>Index</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.instrs.Instruction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Index</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Index</em>' containment reference list.
	 * @see gecos.instrs.InstrsPackage#getArrayInstruction_Index()
	 * @model containment="true"
	 * @generated
	 */
	EList<Instruction> getIndex();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; _xblockexpression = null;\n{\n\tfinal &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; list = new &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;();\n\tlist.add(this.getDest());\n\tlist.addAll(this.getIndex());\n\t_xblockexpression = &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;unmodifiableEList(list);\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	EList<Instruction> listChildren();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model idxUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getIndex().add(idx);'"
	 * @generated
	 */
	void addIndex(Instruction idx);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model iDataType="gecos.instrs.int" iUnique="false" idxUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getIndex().add(i, idx);'"
	 * @generated
	 */
	void addIndexAt(int i, Instruction idx);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitArrayInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return super.computeType();'"
	 * @generated
	 */
	Type computeType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean sym = true;\nfinal &lt;%java.lang.StringBuffer%&gt; sb = new &lt;%java.lang.StringBuffer%&gt;();\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; _listChildren = this.listChildren();\nfor (final &lt;%gecos.instrs.Instruction%&gt; i : _listChildren)\n{\n\tif (sym)\n\t{\n\t\tsb.append(i);\n\t\tsym = false;\n\t}\n\telse\n\t{\n\t\tif ((i != null))\n\t\t{\n\t\t\tsb.append(\"[\").append(i).append(\"]\");\n\t\t}\n\t\telse\n\t\t{\n\t\t\tsb.append(\"[null]\");\n\t\t}\n\t}\n}\nreturn sb.toString();'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.ArrayInstruction%&gt;))\n{\n\treturn super.isSame(instr);\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // ArrayInstruction
