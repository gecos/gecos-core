package fr.irisa.cairn.gecos.core.cdtfrontend.tests;

import org.junit.Test;

import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.core.testframework.impl.GSProjectVersion;
import fr.irisa.cairn.gecos.core.testframework.impl.GecosS2STestTemplate;
import fr.irisa.cairn.gecos.model.cdtfrontend.CDTFrontEnd;
import fr.irisa.cairn.gecos.testframework.data.CxxFileData;
import fr.irisa.cairn.gecos.testframework.data.ICxxProjectData;
import fr.irisa.cairn.gecos.testframework.dataprovider.DataFromPathProvider;
import fr.irisa.cairn.gecos.testframework.dataprovider.ResourcesLocation;
import fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName;
import fr.irisa.cairn.gecos.testframework.stages.Stages;

/**
 * Tests C front-end {@link CDTFrontEnd}.
 * 
 * <p> This only tests if the original code can be parsed i.e. test flow
 * stops just before transform.
 * 
 * @author aelmouss
 */
public class MiscCompileIT extends GecosS2STestTemplate<GSProjectVersion> {
	
	@Override
	protected void configure() {
		super.configure();
		
		findTestFlow(CxxFileData.class)
			.replaceStage(S2SStageName.TRANSFORM, Stages.STOP);
		
		expectParsingFail = true; //expecting failure to parse original project
	}
	
	
	/**
	 * All tests are expected to fail to Parse original project.
	 */
	@Test
	@UseDataProvider(location=DataFromPathProvider.class, value=DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation("resources/src-c/frontend/compile/shouldFail/fail")//should fail and do fail => test succeeds
	public void testPass(ICxxProjectData d) {
		runTest(d, p -> {}); 
	}
	
	/**
	 * These test should fail to parse but currently they do not: they represent bugs!
	 */
	@Test
	@UseDataProvider(location=DataFromPathProvider.class, value=DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation("resources/src-c/frontend/compile/shouldFail/pass") //should fail but pass => test fails (bug)
	public void testBugs(ICxxProjectData d) {
		runTest(d, p -> {}); 
	}
	
}
