/**
 */
package gecos.instrs;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simd Extract Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.instrs.InstrsPackage#getSimdExtractInstruction()
 * @model
 * @generated
 */
public interface SimdExtractInstruction extends SimdInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitSimdExtractInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getChild(1);'"
	 * @generated
	 */
	Instruction getSubwordIndex();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getChild(0);'"
	 * @generated
	 */
	Instruction getVector();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model subwordIndexUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _size = this.getChildren().size();\nboolean _lessThan = (_size &lt; 2);\nif (_lessThan)\n{\n\tthis.getChildren().add(1, subwordIndex);\n}\nelse\n{\n\tthis.getChildren().set(1, subwordIndex);\n}'"
	 * @generated
	 */
	void setSubwordIndex(Instruction subwordIndex);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model vectorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _size = this.getChildren().size();\nboolean _greaterThan = (_size &gt; 0);\nif (_greaterThan)\n{\n\tthis.getChildren().set(0, vector);\n}\nelse\n{\n\tthis.getChildren().add(0, vector);\n}'"
	 * @generated
	 */
	void setVector(Instruction vector);

} // SimdExtractInstruction
