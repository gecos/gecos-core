package fr.irisa.cairn.gecos.model.transforms.arrays.flattening;

import fr.irisa.cairn.gecos.model.tools.switches.SymbolSwitch;
import fr.irisa.cairn.gecos.model.transforms.arrays.flattening.internal.ArrayFlattener;
import fr.irisa.r2d2.gecos.framework.GSModule;
import gecos.annotations.AnnotationKeys;
import gecos.annotations.PragmaAnnotation;
import gecos.core.ProcedureSet;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;
import gecos.types.ArrayType;
import gecos.types.Type;

@GSModule("If used with a single argument, it will performs Array Flattening "
		+ "on Symbols tagged with a'gcs_array_flattening(int depth)' #pragma directive.\n"
		+ " The depth parameter indicates the depth of the scalarization "
		+ "(eg a depth of 1 applied to a2D array willl flatten only one dimension of the array)."
		+ "\nIf used with two argments (second one being an integer), \n"
		+ "it will trye to flatten all arrays in the program.")
public class ModelArrayFlattening extends SymbolSwitch<Object> {


	public static boolean debug = false;
	public static final String GCS_ARRAY_FLATTENING = "gcs_array_flattening";
	private ProcedureSet ps;
	private GecosProject project;
	private boolean flattenAllArrays;


	public ModelArrayFlattening(ProcedureSet ps, int scalarizeAllArray) {
		super();
		this.ps =ps;
		flattenAllArrays=true;
	}
	
	public ModelArrayFlattening(GecosProject project, int scalarizeAllArray) {
		super();
		this.project =project;
		flattenAllArrays=true;
	}

	public ModelArrayFlattening(GecosProject project) {
		super();
		this.project =project;
		flattenAllArrays= false;
	}


	public ModelArrayFlattening(ProcedureSet ps) {
		super();
		this.ps =ps;
		flattenAllArrays=false;
	}

	public void compute() {
		if (project!=null) {
			for(GecosSourceFile file : project.getSources()) {
				ps = file.getModel();
				doSwitch(ps);
			}
		} else {
			doSwitch(ps);
		}
	}

	@Override
	public Object caseSymbol(Symbol object) {
		Type type = object.getType();
		if (type!=null) {
			if(type.asArray() != null && object.eContainer()!=null) {
				checkScalarizePragma(object);
			}
		} else {
			throw new UnsupportedOperationException("Symbol "+object+ " has no type");
		}
		return true;
	}

	/**
	 * 	Function which determine dimension of array (ie: number of lines)
	 * 	and set arrayType to last element base type
	 * @param array : array to evaluate
	 * @param arrayType : type to set to last base type
	 * @return dimension of array
	 */
	private long getDimension(Symbol array) {
		long ret = 0;
		Type type = array.getType();

		if (type instanceof ArrayType) {
			ret= ((ArrayType)type).getUpper();
		}
		if (debug)
			System.out.println("found dimension: " + ret);

		return ret;
	}

	private void checkScalarizePragma(Symbol symbol) {
		if(this.flattenAllArrays){
			try {
				long depth = getDimension(symbol);
				
				ArrayFlattener flatenner = new ArrayFlattener(ps, symbol, depth);
				flatenner.transform();
			} catch (NumberFormatException e) {
				throw new RuntimeException("error in flatenner");
			}
		}
		else{
			PragmaAnnotation pragma = symbol.getPragma();
			if (pragma!=null) {
				String res = "";
				for (String string : pragma.getContent()) {
					//Pattern p = Pattern.compile("scalarize(\\d+)");
					//Pattern p = Pattern.compile("scalarize(\\d+)");
					///Matcher matcher = p.matcher(string);
					//boolean scalarize = matcher.find(); 
					if (string.contains("scalarize(")) {
						throw new UnsupportedOperationException("scalarize(int depth) pragma are deprecated, please use instead "+GCS_ARRAY_FLATTENING+"(int depth)");
					} else if (string.contains(GCS_ARRAY_FLATTENING.toUpperCase()))
						throw new UnsupportedOperationException(GCS_ARRAY_FLATTENING.toUpperCase() + "(int depth) pragma are deprecated, please use instead " + GCS_ARRAY_FLATTENING + "(int depth)");
					if(string.contains(GCS_ARRAY_FLATTENING+"(")) {
						res=string;
					}
				}
				if (debug)
					System.out.println("Found Scalarize pragma for symbol "+symbol);
				int start = res.lastIndexOf("(");
				int end = res.lastIndexOf(")");
				String value = start >= 0 && end >= 0 ? res.substring(start+1, end) : "-1";
				long depth = -1;
				if(value.equals("all")) {
					depth = getDimension(symbol);
				} else {
					try {
						depth= Integer.parseInt(value);	
					} catch (NumberFormatException e) {
						throw new RuntimeException("Pragma does not enforce syntax GCS_FLATTEN(INT) : '"+res+"'");
					}
				}
				pragma.getContent().remove(res);
				if (pragma.getContent().size()==0) {
					symbol.getAnnotations().removeKey(AnnotationKeys.PRAGMA_ANNOTATION_KEY.getLiteral());
				}
				ArrayFlattener flatenner = new ArrayFlattener(ps, symbol, depth);
				flatenner.transform();
			}
		}
	}

}
