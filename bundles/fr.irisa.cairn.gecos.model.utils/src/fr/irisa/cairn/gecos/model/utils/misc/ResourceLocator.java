package fr.irisa.cairn.gecos.model.utils.misc;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.URIUtil;
import org.osgi.framework.FrameworkUtil;


/**
 * Duplicate of fr.irisa.cairn.gecos.testframework.utils/ResourceLocator.java
 */
public class ResourceLocator {

	private static final Path DEFAULT_EXTRRACT_PATH = Paths.get("target/resources-locator_extracted-jars");

	private Path extractedJarsLocation = DEFAULT_EXTRRACT_PATH;

	/**
	 * If true: if 'resourcesPlugin' jar already exists in TMP_RESOURCES_LOCATION then reuse it (i.e. do not extract jar again)
	 * If false: always extract jar, if directory already exists override it.
	 */
	private boolean reuseExtractedJars = true;
	
	
	private String resourcesBundleName;
	private String resourcesRootDir;  // relative to the resourcesBundle location
	private Path resourceDir;
	

	/**
	 * Use the bundle that contains the specified {@code classInResourcesPlugin} as 
	 * the resources bundle.
	 * 
	 * @param classInResourcesBundle class located in the resources bundle (used only to locate the bundle).
	 * @param resourcesRootDir path of the directory containing resources
	 *   relative to the resources bundle location.
	 */
	public ResourceLocator(Class<?> classInResourcesBundle, String resourcesRootDir) {
		this(FrameworkUtil.getBundle(classInResourcesBundle).getSymbolicName(), resourcesRootDir);
		//TODO try use classInResourcesBundle.getClassLoader() instead
	}
	
	/**
	 * Creates a {@link ResourceLocator} that tries to locate resources in
	 * '{@code plugin_directory/<resourcesRootDir>}'.
	 * <br>
	 * 'plugin_directory' is located using URL "{@code platform:/plugin/<resourcesPluginName>}".
	 * 
	 * @param resourcesPluginName plugin where the resources are located 
	 *   (e.g. fr.irisa.cairn.gecos.testframework.resources).
	 * @param resourcesRootDir path of the directory containing resources
	 *   relative to the resources bundle location.
	 *   Can be empty.
	 */
	public ResourceLocator(String resourcesPluginName, String resourcesRootDir) {
		this.resourcesBundleName = resourcesPluginName;
		this.resourcesRootDir = resourcesRootDir;
	}
	
	public Path getExtractedJarsLocation() {
		return extractedJarsLocation;
	}
	public void setExtractedJarsLocation(Path extractedJarsLocation) {
		this.extractedJarsLocation = extractedJarsLocation;
	}
	public boolean isReuseExtractedJars() {
		return reuseExtractedJars;
	}
	
	/**
	 * In case the resource bundle is located in a jar, the resourcesRootDirectory is extracted
	 * from this jar to the location specified by {@link #getExtractedJarsLocation()} only if
	 * {@code reuseExtractedJars} is set to false or if such directory does not already exist.
	 * 
	 * @param reuseExtractedJars If false always extract jar entry to the extraction location,
	 * (i.e. if directory already exists override it). Otherwise, reuse it if it already exists. 
	 */
	public void setReuseExtractedJars(boolean reuseExtractedJars) {
		this.reuseExtractedJars = reuseExtractedJars;
	}
	
	/**
	 * Tries to locate the specified resources path relative to
	 * this {@link FileLocator} @{code resourcesPluginName} directory.
	 * <br>
	 * It first tries to resolve the bundle location using "platform:/plugin/..plugin_name.."
	 * URL.
	 * <br>
	 * If the located bundle is a jar, it extracts its content to a temporarily location 
	 * TMP ({@value #extractedJarsLocation})
	 * and returns a new {@link Path} pointing to {@code TMP/relativePath}.
	 * 
	 * @param relativePath resource path considered as relative to the root resources bundle path.
	 * 
	 * @return the located resolved path location.
	 */
	public Path locate(String relativePath) { //TODO consider relativePath as relative to resourcesRelativeDir
		if(resourceDir == null)
			resolveResourcesDir();
		
		return resourceDir.resolve(relativePath);
	}
	
	//FIXME this only works when running as an eclipse application
	private void resolveResourcesDir() {
		URI bundleUri; try {
			bundleUri = FileLocator.resolve(new URL("platform:/plugin/" + resourcesBundleName)).toURI();
		} catch(Exception e) {
			throw new RuntimeException("Couldn't resolve location of bundle '" + resourcesBundleName + "'!\n"
					+ "Make sure the bundle name is correct and,\n"
					+ "That it is added as dependecy in the manifest of the bundle that is using it !!!", e);
		}
		
		if(URIUtil.isFileURI(bundleUri))
			resourceDir = URIUtil.toFile(bundleUri).toPath();
		else if(isJarUri(bundleUri)) {
			resourceDir = extractedJarsLocation.resolve(resourcesBundleName);
			
			// if resourcesPlugin jar already extracted in TMP_RESOURCES_LOCATION then reuse it
			if(reuseExtractedJars && Files.isDirectory(resourceDir)) {
			} else
				extractJar(bundleUri, resourceDir);
		} else
			throw new RuntimeException("Not yet supported: " + bundleUri);
	}

	private static boolean isJarUri(URI bundleUri) {
		return bundleUri.getScheme().startsWith("jar");
	}

	//TODO only extract this.resourcesRelativeDir
	public static void extractJar(URI bundleUri, Path toDir) {
		try {
			JarFile jar = uriToJarFile(bundleUri);
			Enumeration<JarEntry> enumEntries = jar.entries();
			while (enumEntries.hasMoreElements()) {
				JarEntry jarEntry = enumEntries.nextElement();
			    Path f = toDir.resolve(jarEntry.getName());
			    extractJarEntry(jar, jarEntry, f);
			}
			jar.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}	
	}

	private static JarFile uriToJarFile(URI bundleUri) throws IOException {
		String[] splits = bundleUri.getSchemeSpecificPart().split("!");
		String path = splits[0];
		if(path.startsWith("file:"))
			path = path.replaceFirst("file:", "");
		
		return new JarFile(path);
	}

	private static void extractJarEntry(JarFile jar, JarEntry jarEntry, Path to) throws IOException {
		if (jarEntry.isDirectory())
			Files.createDirectories(to);
	    else {
	    	try (
				InputStream is = jar.getInputStream(jarEntry);
				OutputStream fos = Files.newOutputStream(to);
	    	){	
	    		while (is.available() > 0) fos.write(is.read());
	    	}
	    }
	}
	
}
