package fr.irisa.cairn.gecos.model.modules;

import fr.irisa.r2d2.gecos.framework.GSArg;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import gecos.gecosproject.GecosProject;

@GSModule("Return a self-contained copy of a GecosProject.")
public class CopyProject {
	
	private GecosProject project;

	@GSModuleConstructor(args = {
			@GSArg(name = "project", info = "The GecosProject to be copied.") 
	})
	public CopyProject(GecosProject project) {
		this.project = project;
	}

	public GecosProject compute() {
//		return EcoreTools.<GecosProject>copy(project); //XXX
		return project.copy();
	}

}
