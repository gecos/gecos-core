package fr.irisa.cairn.gecos.model.tools.visitors;

import gecos.dag.DAGArrayNode;
import gecos.dag.DAGArrayValueNode;
import gecos.dag.DAGCallNode;
import gecos.dag.DAGControlEdge;
import gecos.dag.DAGDataEdge;
import gecos.dag.DAGFieldInstruction;
import gecos.dag.DAGFloatImmNode;
import gecos.dag.DAGInPort;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGIntImmNode;
import gecos.dag.DAGJumpNode;
import gecos.dag.DAGNumberedOutNode;
import gecos.dag.DAGNumberedSymbolNode;
import gecos.dag.DAGOpNode;
import gecos.dag.DAGOutNode;
import gecos.dag.DAGOutPort;
import gecos.dag.DAGPatternNode;
import gecos.dag.DAGPhiNode;
import gecos.dag.DAGSSADefNode;
import gecos.dag.DAGSSAUseNode;
import gecos.dag.DAGSimpleArrayNode;
import gecos.dag.DAGStringImmNode;
import gecos.dag.DAGSymbolNode;
import gecos.dag.DAGVectorArrayAccessNode;
import gecos.dag.DAGVectorExpandNode;
import gecos.dag.DAGVectorExtractNode;
import gecos.dag.DAGVectorOpNode;
import gecos.dag.DAGVectorPackNode;
import gecos.dag.DAGVectorShuffleNode;
import gecos.dag.DagVisitor;

import org.eclipse.emf.ecore.impl.EObjectImpl;

public abstract class GecosDagDefaultVisitor extends EObjectImpl implements
		DagVisitor {

	public void visitDAGInstruction(DAGInstruction d) {
		// TODO Auto-generated method stub

	}

	public void visitDAGInPort(DAGInPort d) {
		// TODO Auto-generated method stub

	}

	public void visitDAGOutPort(DAGOutPort d) {
		// TODO Auto-generated method stub

	}

	public void visitDAGSymbolNode(DAGSymbolNode d) {
		// TODO Auto-generated method stub

	}

	public void visitDAGCallNode(DAGCallNode d) {
		// TODO Auto-generated method stub

	}

	public void visitDAGArrayValueNode(DAGArrayValueNode d) {
		// TODO Auto-generated method stub

	}

	public void visitDAGOpNode(DAGOpNode d) {
		// TODO Auto-generated method stub

	}

	public void visitDAGOutNode(DAGOutNode d) {
		// TODO Auto-generated method stub

	}

	public void visitDAGControlEdge(DAGControlEdge d) {
		// TODO Auto-generated method stub

	}

	public void visitDAGDataEdge(DAGDataEdge d) {
		// TODO Auto-generated method stub

	}

	public void visitDAGNumberedSymbolNode(DAGNumberedSymbolNode d) {
		// TODO Auto-generated method stub

	}

	public void visitDAGNumberedOutNode(DAGNumberedOutNode d) {
		// TODO Auto-generated method stub

	}

	public void visitDAGIntImmNode(DAGIntImmNode d) {
		// TODO Auto-generated method stub

	}

	public void visitDAGFloatImmNode(DAGFloatImmNode d) {
		// TODO Auto-generated method stub

	}

	public void visitDAGArrayNode(DAGArrayNode d) {
		// TODO Auto-generated method stub

	}

	public void visitDAGSimpleArrayNode(DAGSimpleArrayNode d) {
		// TODO Auto-generated method stub

	}

	public void visitDAGJumpNode(DAGJumpNode d) {
		// TODO Auto-generated method stub

	}
	
	public void visitDAGPatternNode(DAGPatternNode d) {
		
	}
	
	@Override
	public void visitDAGStringImmNode(DAGStringImmNode d) {
		// TODO Auto-generated method stub
		
	}

	public void visitDAGVectorArrayAccessNode(DAGVectorArrayAccessNode d) {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void visitDAGVectorOpNode(DAGVectorOpNode d) {
		// TODO Auto-generated method stub
	}

	@Override
	public void visitDAGVectorShuffleNode(DAGVectorShuffleNode d) {
		// TODO Auto-generated method stub
	}

	@Override
	public void visitDAGVectorExtractNode(DAGVectorExtractNode d) {
		// TODO Auto-generated method stub
	}

	@Override
	public void visitDAGVectorPackNode(DAGVectorPackNode d) {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void visitDAGVectorExpandNode(DAGVectorExpandNode d) {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void visitDAGSSAUseNode(DAGSSAUseNode d) {
		
	}

	@Override
	public void visitDAGSSADefNode(DAGSSADefNode d) {
		
	}

	@Override
	public void visitDAGPhiNode(DAGPhiNode d) {
		
	}

	@Override
	public void visitDAGFieldInstruction(DAGFieldInstruction d) {
		// TODO Auto-generated method stub
		
	}
	
	
	
}
