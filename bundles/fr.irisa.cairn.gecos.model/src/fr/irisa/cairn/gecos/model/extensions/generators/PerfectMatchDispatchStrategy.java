/**
 * 
 */
package fr.irisa.cairn.gecos.model.extensions.generators;

import java.util.List;

/**
 * Strategy that select the first compatible generator extension.
 * @author mnaullet
 */
public class PerfectMatchDispatchStrategy implements IGeneratorDispatchStrategy {

	public String dispatch(List<IGecosCodeGenerator> generators, Object o) {
		if (o == null) return "/* null object */";
		/*
		 * Try to find a perfect match method to generate "o"
		 */
		IGecosCodeGenerator gecosCodeGenerator = findPerfectMatchGenerator(generators, o);
		if (gecosCodeGenerator != null) {
			return gecosCodeGenerator.generate(o);
		}
		/*
		 * Find finally a suitable generator
		 */
		for (IGecosCodeGenerator iGecosCodeGenerator : generators) {
			String generated = iGecosCodeGenerator.generate(o);
			if(generated!=null)
				return generated;
		}
		System.err.println("Warning : could not generate code for object of " + o.getClass());
		return "/* Could not find code generator for object of " + o.getClass() + " */";
	}

	private IGecosCodeGenerator findPerfectMatchGenerator (List<IGecosCodeGenerator> generators, Object o) {
		for (IGecosCodeGenerator iGecosCodeGenerator : generators) {
			for (int i = 0; i < o.getClass().getInterfaces().length; i++)
				try {
					iGecosCodeGenerator.getClass().getDeclaredMethod("_generate", o.getClass().getInterfaces()[i]);
					return iGecosCodeGenerator;
				} catch (SecurityException e) {
				} catch (NoSuchMethodException e) {
				}
		}
		return null;
	}
}