package fr.irisa.cairn.gecos.model.dag.adapter.builder;

import java.util.Comparator;

import com.google.inject.Inject;

import fr.irisa.cairn.gecos.model.dag.adapter.impl.DAGCollapsedNodeAdapterImpl;
import fr.irisa.cairn.gecos.model.dag.adapter.impl.DAGEdgeAdapterImpl;
import fr.irisa.cairn.gecos.model.dag.adapter.impl.DAGInstructionAdapterImpl;
import fr.irisa.cairn.gecos.model.dag.adapter.impl.DAGNodeAdapterImpl;
import fr.irisa.cairn.gecos.model.dag.adapter.impl.DAGPortAdapterImpl;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGCollapsedNodeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGEdgeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGInstructionAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGNodeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGPortAdapter;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import gecos.dag.DAGEdge;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGNode;
import gecos.dag.DAGPort;

/**
 * Factory of the graph adapter. Nodes, edges and ports may be ordered if 
 * an adapted Comparator is injected.

 * @author <your name>
 *
 */
public class AdapterFactory {

	@Inject
	Comparator<INode> nodeComparator;
	@Inject
	Comparator<IEdge> edgeComparator;
	@Inject
	Comparator<IPort> portComparator;

	@Inject
	protected AdapterFactory() {
	}

	public IDAGInstructionAdapter createGraphAdapter(DAGInstruction adapted,
			boolean directed) {
		DAGInstructionAdapterImpl graph = new DAGInstructionAdapterImpl(
				adapted, directed);
		if (this.nodeComparator != null) {
			graph.setNodeComparator(this.nodeComparator);
		}
		if (this.edgeComparator != null) {
			graph.setEdgeComparator(this.edgeComparator);
		}
		return graph;
	}

	public IDAGNodeAdapter createNodeAdapter(DAGNode adapted) {
		DAGNodeAdapterImpl node = new DAGNodeAdapterImpl(adapted);
		if (this.portComparator != null) {
			node.setPortComparator(portComparator);
		}
		return node;
	}

	public IDAGCollapsedNodeAdapter createCollapsedNodeAdapter(DAGNode adapted) {
		DAGCollapsedNodeAdapterImpl node = new DAGCollapsedNodeAdapterImpl(adapted);
		if (this.portComparator != null) {
			node.setPortComparator(portComparator);
		}
		return node;
	}
	
	public IDAGPortAdapter createPortAdapter(DAGPort adapted) {
		DAGPortAdapterImpl port = new DAGPortAdapterImpl(adapted);;
		return port;
	}

	public IDAGEdgeAdapter createEdgeAdapter(DAGEdge adapted,
			IDAGPortAdapter source, IDAGPortAdapter sink) {
		return new DAGEdgeAdapterImpl(adapted, source, sink);
	}

	/**
	 * Connect two ports and add the link to the nodes' graphs
	 * 
	 * @param source
	 * @param sink
	 * @return the created edge
	 */
	public IDAGEdgeAdapter connect(DAGEdge adapted, IDAGPortAdapter source,
			IDAGPortAdapter sink) {
		if(source == null || adapted == null || sink == null) {
			return null; //XXX
		}
		IDAGEdgeAdapter e = createEdgeAdapter(adapted, source, sink);
		IDAGInstructionAdapter gSource = source.getNode().getGraph();
		if (sink != null) {
			gSource.addEdge(e);
			IDAGInstructionAdapter gSink = sink.getNode().getGraph();
			if (gSource != gSink) {
				gSink.addEdge(e);
			}
		}
		return e;
	}

}
