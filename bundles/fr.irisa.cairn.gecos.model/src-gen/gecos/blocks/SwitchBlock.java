/**
 */
package gecos.blocks;

import fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;

import gecos.instrs.BranchType;
import gecos.instrs.BreakInstruction;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Switch Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.blocks.SwitchBlock#getBodyBlock <em>Body Block</em>}</li>
 *   <li>{@link gecos.blocks.SwitchBlock#getCases <em>Cases</em>}</li>
 *   <li>{@link gecos.blocks.SwitchBlock#getDefaultBlock <em>Default Block</em>}</li>
 *   <li>{@link gecos.blocks.SwitchBlock#getDispatchBlock <em>Dispatch Block</em>}</li>
 * </ul>
 *
 * @see gecos.blocks.BlocksPackage#getSwitchBlock()
 * @model annotation="http://www.eclipse.org/ocl/examples/OCL differentValues='self.cases-&gt;forAll(a,b | a.value=b.value implies a=b) '"
 *        annotation="gmf.node figure='rectangle' label='number' label.placement='external' label.icon='false'"
 * @generated
 */
public interface SwitchBlock extends Block, BlocksVisitable, Breakable {
	/**
	 * Returns the value of the '<em><b>Body Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body Block</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body Block</em>' containment reference.
	 * @see #setBodyBlock(Block)
	 * @see gecos.blocks.BlocksPackage#getSwitchBlock_BodyBlock()
	 * @model containment="true"
	 *        annotation="gmf.compartment foo='bar'"
	 * @generated
	 */
	Block getBodyBlock();

	/**
	 * Sets the value of the '{@link gecos.blocks.SwitchBlock#getBodyBlock <em>Body Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body Block</em>' containment reference.
	 * @see #getBodyBlock()
	 * @generated
	 */
	void setBodyBlock(Block value);

	/**
	 * Returns the value of the '<em><b>Cases</b></em>' map.
	 * The key is of type {@link java.lang.Long},
	 * and the value is of type {@link gecos.blocks.Block},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cases</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cases</em>' map.
	 * @see gecos.blocks.BlocksPackage#getSwitchBlock_Cases()
	 * @model mapType="gecos.blocks.CaseMap&lt;gecos.blocks.EELong, gecos.blocks.Block&gt;"
	 *        annotation="gmf.compartment foo='bar'"
	 * @generated
	 */
	EMap<Long, Block> getCases();

	/**
	 * Returns the value of the '<em><b>Default Block</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Block</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Block</em>' reference.
	 * @see #setDefaultBlock(Block)
	 * @see gecos.blocks.BlocksPackage#getSwitchBlock_DefaultBlock()
	 * @model resolveProxies="false"
	 *        annotation="gmf.compartment foo='bar'"
	 * @generated
	 */
	Block getDefaultBlock();

	/**
	 * Sets the value of the '{@link gecos.blocks.SwitchBlock#getDefaultBlock <em>Default Block</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Block</em>' reference.
	 * @see #getDefaultBlock()
	 * @generated
	 */
	void setDefaultBlock(Block value);

	/**
	 * Returns the value of the '<em><b>Dispatch Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dispatch Block</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dispatch Block</em>' containment reference.
	 * @see #setDispatchBlock(BasicBlock)
	 * @see gecos.blocks.BlocksPackage#getSwitchBlock_DispatchBlock()
	 * @model containment="true"
	 *        annotation="gmf.compartment foo='bar'"
	 * @generated
	 */
	BasicBlock getDispatchBlock();

	/**
	 * Sets the value of the '{@link gecos.blocks.SwitchBlock#getDispatchBlock <em>Dispatch Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dispatch Block</em>' containment reference.
	 * @see #getDispatchBlock()
	 * @generated
	 */
	void setDispatchBlock(BasicBlock value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitSwitchBlock(this);'"
	 * @generated
	 */
	void accept(BlocksVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%fr.irisa.cairn.gecos.model.tools.utils.BlocksContinuesBreaksFinder%&gt; finder = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.BlocksContinuesBreaksFinder%&gt;();\nfinder.doSwitch(this.getBodyBlock());\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt;unmodifiableEList(finder.getBreaks());'"
	 * @generated
	 */
	EList<BasicBlock> listBreakBlocks();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder%&gt; finder = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder%&gt;();\nfinder.doSwitch(this.getBodyBlock());\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.instrs.BreakInstruction%&gt;&gt;unmodifiableEList(finder.getBreaks());'"
	 * @generated
	 */
	EList<BreakInstruction> listBreakInstructions();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" dispatchUnique="false" condUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getDispatchBlock().connectFromBasic(dispatch, cond);'"
	 * @generated
	 */
	ControlEdge connectFromBasic(BasicBlock dispatch, BranchType cond);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" nextUnique="false" condUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%gecos.blocks.ControlEdge%&gt;&gt; res = &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.blocks.ControlEdge%&gt;&gt;newBasicEList();\n&lt;%gecos.blocks.Block%&gt; _defaultBlock = this.getDefaultBlock();\nboolean _tripleNotEquals = (_defaultBlock != null);\nif (_tripleNotEquals)\n{\n\tres.addAll(this.getDefaultBlock().connectTo(next, &lt;%gecos.instrs.BranchType%&gt;.UNCONDITIONAL));\n}\nelse\n{\n\tres.addAll(this.getDispatchBlock().connectTo(next, &lt;%gecos.instrs.BranchType%&gt;.IF_FALSE));\n\tres.addAll(this.getBodyBlock().connectTo(next, &lt;%gecos.instrs.BranchType%&gt;.UNCONDITIONAL));\n}\nfinal &lt;%fr.irisa.cairn.gecos.model.tools.utils.BlocksContinuesBreaksFinder%&gt; visitor = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.BlocksContinuesBreaksFinder%&gt;();\nvisitor.doSwitch(this.getBodyBlock());\n&lt;%java.util.List%&gt;&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt; _breaks = visitor.getBreaks();\nfor (final &lt;%gecos.blocks.BasicBlock%&gt; bb : _breaks)\n{\n\t{\n\t\t&lt;%gecos.blocks.ControlEdge%&gt;[] _clone = ((&lt;%gecos.blocks.ControlEdge%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(bb.getOutEdges(), &lt;%gecos.blocks.ControlEdge%&gt;.class)).clone();\n\t\tfor (final &lt;%gecos.blocks.ControlEdge%&gt; e : _clone)\n\t\t{\n\t\t\t{\n\t\t\t\te.setFrom(null);\n\t\t\t\te.setTo(null);\n\t\t\t}\n\t\t}\n\t\tbb.getOutEdges().clear();\n\t\tres.addAll(bb.connectTo(next, &lt;%gecos.instrs.BranchType%&gt;.UNCONDITIONAL));\n\t}\n}\nreturn res;'"
	 * @generated
	 */
	EList<ControlEdge> connectTo(Block next, BranchType cond);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.Block%&gt; _defaultBlock = this.getDefaultBlock();\nboolean _tripleNotEquals = (_defaultBlock != null);\nif (_tripleNotEquals)\n{\n\tthis.getDefaultBlock().simplifyBlock();\n}\n&lt;%gecos.blocks.Block%&gt; _bodyBlock = this.getBodyBlock();\nboolean _tripleNotEquals_1 = (_bodyBlock != null);\nif (_tripleNotEquals_1)\n{\n\tthis.getBodyBlock().simplifyBlock();\n}\n&lt;%gecos.blocks.Block%&gt; _defaultBlock_1 = this.getDefaultBlock();\nboolean _tripleNotEquals_2 = (_defaultBlock_1 != null);\nif (_tripleNotEquals_2)\n{\n\tthis.getDefaultBlock().simplifyBlock();\n}\nreturn this;'"
	 * @generated
	 */
	Block simplifyBlock();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model oldbUnique="false" newbUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.Block%&gt; _defaultBlock = this.getDefaultBlock();\nboolean _tripleEquals = (oldb == _defaultBlock);\nif (_tripleEquals)\n{\n\tthis.setDefaultBlock(newb);\n}\nelse\n{\n\tif (((this.getDispatchBlock() == oldb) &amp;&amp; (newb instanceof &lt;%gecos.blocks.BasicBlock%&gt;)))\n\t{\n\t\tthis.setDispatchBlock(((&lt;%gecos.blocks.BasicBlock%&gt;) newb));\n\t}\n\telse\n\t{\n\t\t&lt;%gecos.blocks.Block%&gt; _bodyBlock = this.getBodyBlock();\n\t\tboolean _tripleNotEquals = (_bodyBlock != null);\n\t\tif (_tripleNotEquals)\n\t\t{\n\t\t\t&lt;%gecos.blocks.Block%&gt; _bodyBlock_1 = this.getBodyBlock();\n\t\t\tboolean _tripleEquals_1 = (oldb == _bodyBlock_1);\n\t\t\tif (_tripleEquals_1)\n\t\t\t{\n\t\t\t\tthis.setBodyBlock(newb);\n\t\t\t\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(\"Not yet implemented\");\n\t\t\t}\n\t\t}\n\t}\n}'"
	 * @generated
	 */
	void replace(Block oldb, Block newb);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bbUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.blocks.BasicBlock%&gt; _dispatchBlock = this.getDispatchBlock();\nboolean _tripleEquals = (_dispatchBlock == bb);\nif (_tripleEquals)\n{\n\tthis.setDispatchBlock(null);\n}\nelse\n{\n\t&lt;%gecos.blocks.Block%&gt; _bodyBlock = this.getBodyBlock();\n\tboolean _tripleEquals_1 = (_bodyBlock == bb);\n\tif (_tripleEquals_1)\n\t{\n\t\tthis.setBodyBlock(null);\n\t}\n\telse\n\t{\n\t\t&lt;%gecos.blocks.Block%&gt; _defaultBlock = this.getDefaultBlock();\n\t\tboolean _tripleEquals_2 = (_defaultBlock == bb);\n\t\tif (_tripleEquals_2)\n\t\t{\n\t\t\tthis.setDefaultBlock(null);\n\t\t}\n\t\telse\n\t\t{\n\t\t\t&lt;%gecos.blocks.Block%&gt; _bodyBlock_1 = this.getBodyBlock();\n\t\t\tboolean _tripleNotEquals = (_bodyBlock_1 != null);\n\t\t\tif (_tripleNotEquals)\n\t\t\t{\n\t\t\t\tthis.getBodyBlock().removeBlock(bb);\n\t\t\t}\n\t\t}\n\t}\n}'"
	 * @generated
	 */
	void removeBlock(Block bb);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" mngDataType="gecos.core.BlockCopyManager" mngUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.blocks.SwitchBlock%&gt; switchBlock = &lt;%gecos.blocks.BlocksFactory%&gt;.eINSTANCE.createSwitchBlock();\nfinal &lt;%gecos.blocks.Block%&gt; copy = this.getBodyBlock().managedCopy(mng);\nswitchBlock.setBodyBlock(copy);\n&lt;%gecos.blocks.Block%&gt; _bodyBlock = this.getBodyBlock();\nfinal &lt;%gecos.blocks.CompositeBlock%&gt; cb = ((&lt;%gecos.blocks.CompositeBlock%&gt;) _bodyBlock);\n&lt;%gecos.blocks.Block%&gt; _managedCopy = this.getDispatchBlock().managedCopy(mng);\nfinal &lt;%gecos.blocks.BasicBlock%&gt; copy2 = ((&lt;%gecos.blocks.BasicBlock%&gt;) _managedCopy);\ncb.getChildren().add(copy2);\nswitchBlock.setDispatchBlock(copy2);\nfinal &lt;%gecos.blocks.Block%&gt; copy3 = this.getDefaultBlock().managedCopy(mng);\ncb.getChildren().add(copy3);\nswitchBlock.setDefaultBlock(copy3);\n&lt;%java.util.Set%&gt;&lt;&lt;%java.lang.Long%&gt;&gt; _keySet = this.getCases().keySet();\nfor (final &lt;%java.lang.Long%&gt; key : _keySet)\n{\n\t{\n\t\tfinal &lt;%gecos.blocks.Block%&gt; caseBranch = this.getCases().get(key);\n\t\tfinal &lt;%gecos.annotations.ExtendedAnnotation%&gt; ext = &lt;%gecos.annotations.AnnotationsFactory%&gt;.eINSTANCE.createExtendedAnnotation();\n\t\text.getFields().add(this);\n\t\text.getFields().add(caseBranch);\n\t\tcaseBranch.getAnnotations().put(&lt;%fr.irisa.cairn.gecos.model.tools.utils.CaseBlockRelinker%&gt;.RELINKING_KEY, ext);\n\t}\n}\nfinal &lt;%fr.irisa.cairn.gecos.model.tools.utils.CaseBlockRelinker%&gt; relinker = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.CaseBlockRelinker%&gt;(switchBlock);\nrelinker.relink();\n&lt;%java.util.Set%&gt;&lt;&lt;%java.lang.Long%&gt;&gt; _keySet_1 = this.getCases().keySet();\nfor (final &lt;%java.lang.Long%&gt; key_1 : _keySet_1)\n{\n\t{\n\t\tfinal &lt;%gecos.blocks.Block%&gt; caseBranch = this.getCases().get(key_1);\n\t\tfinal &lt;%gecos.annotations.IAnnotation%&gt; annot = caseBranch.getAnnotation(&lt;%fr.irisa.cairn.gecos.model.tools.utils.CaseBlockRelinker%&gt;.RELINKING_KEY);\n\t\tcaseBranch.getAnnotations().remove(annot);\n\t}\n}\nint _number = this.getNumber();\nint _plus = (_number + 1000);\nswitchBlock.setNumber(_plus);\n&lt;%fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory%&gt;.copyAnnotations(this, switchBlock);\nmng.link(this, switchBlock);\nreturn switchBlock;'"
	 * @generated
	 */
	Block managedCopy(BlockCopyManager mng);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.blocks.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _number = this.getNumber();\nreturn (\"Switch_\" + &lt;%java.lang.Integer%&gt;.valueOf(_number));'"
	 * @generated
	 */
	String toShortString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.blocks.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _pathString = this.getPathString();\n&lt;%java.lang.String%&gt; _plus = (_pathString + \"(\");\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; _instructions = this.getDispatchBlock().getInstructions();\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + _instructions);\nreturn (_plus_1 + \")\");'"
	 * @generated
	 */
	String toString();

} // SwitchBlock
