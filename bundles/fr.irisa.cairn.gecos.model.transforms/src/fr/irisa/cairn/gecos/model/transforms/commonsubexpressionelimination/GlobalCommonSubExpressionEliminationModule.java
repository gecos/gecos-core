package fr.irisa.cairn.gecos.model.transforms.commonsubexpressionelimination;

import java.util.ArrayList;
import java.util.StringTokenizer;

import fr.irisa.cairn.gecos.model.analysis.instructions.comparator.InstructionComparator;
import fr.irisa.cairn.gecos.model.tools.controlflow.BuildControlFlow;
import fr.irisa.cairn.gecos.model.tools.controlflow.ClearControlFlow;
import fr.irisa.cairn.gecos.model.transforms.commonsubexpressionelimination.internal.AvailableExpressionIN;
import fr.irisa.cairn.gecos.model.transforms.commonsubexpressionelimination.internal.ComputeUniversalSetOfAllExpressionsEVALandKILL;
import fr.irisa.cairn.gecos.model.transforms.commonsubexpressionelimination.internal.GlobalCommonSubExpressionElimination;
import fr.irisa.r2d2.gecos.framework.GSModule;
import gecos.blocks.BasicBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;
import gecos.instrs.Instruction;

/**
 * This transformation does not Consider pointers 
 * 
 * @author imen fassi
 * @date 25/04/2016
 */

@GSModule("This Module eliminates the Global Common-Subexpressions in the code. It does not Consider pointers.")

public class GlobalCommonSubExpressionEliminationModule {
	private GecosProject _project;
	/**
	 * if you want to consider expressions, having a function call as an
	 * operand, set consider_function_call to true or false otherwise.
	 */
	private boolean consider_function_call;
	/**
	 * if you want to be conservative regarding pointer, set
	 * pointer_conservative to true.
	 */
	private boolean pointer_conservative;

	public GlobalCommonSubExpressionEliminationModule(GecosProject project, boolean bf, boolean cp) {
		this._project = project;
		this.consider_function_call = bf;
		this.pointer_conservative = cp;
	}

	public void compute() {
		
		new TransformInstructionsIntoQuadruplesModule(_project).compute();
		new LocalCommonSubExpressionEliminationModule(_project, consider_function_call, pointer_conservative).compute();		
		
//		System.out.println("[info] The Global Common SubExpression Elimination transformation supposes that the" 
//				+ "\n       Local Common SubExpression Elimination transformation is done."
//				+ "\n       (Call LocalCommonSubExpressionElimination before running this Pass.)");
		ArrayList<Instruction> Uexp;
		ArrayList<ArrayList<String>> evalSymbBB;
		int nBB = 0;
		ArrayList<ArrayList<Instruction>> EVAL ;
		ArrayList<ArrayList<Instruction>> KILL ;
		ArrayList<ArrayList<Instruction>> AEin ;
		ArrayList<ArrayList<Instruction>> AEout ;
		ArrayList<Integer> bb_ID ;
		ArrayList<Integer> bbDepth ;

		for (ProcedureSet ps : _project.listProcedureSets()) {
			for (Procedure pr : ps.listProcedures()) {
				/**
				 * Prepare the inputs
				 */
				ComputeUniversalSetOfAllExpressionsEVALandKILL uExp = new ComputeUniversalSetOfAllExpressionsEVALandKILL(consider_function_call, pointer_conservative);
				pr.getBody().accept(uExp);
				Uexp = uExp.getUexp();    // the universal set of all expressions
				nBB = uExp.getnbBB();     // number basic blocks
				EVAL = uExp.getev();      // EVAL : evaluated expr for each BB
				evalSymbBB = uExp.getevalSymb(); //KILL: killed expr for each BB
				bb_ID = uExp.getBBId();
				bbDepth = uExp.getBBDepth();
				ArrayList<BasicBlock> basicBlock = uExp.getBasicB();
				
				//System.out.println("BB identifiers  : " + bb_ID);
				//System.out.println("BB Depth  : " + bbDepth);
				
				/**
				 * Compute KILL(i) : The set of all expressions evaluated in
				 * other blocks such that one or more of their operand are
				 * assigned in block i, or evaluated in block i and subsequently
				 * have an operand assigned in block i.
				 */

				KILL = new ArrayList<ArrayList<Instruction>>();

				for (int i = 0; i < nBB; i++) {
					ArrayList<Instruction> killI = computeKilledExpr(i, nBB, evalSymbBB.get(i), EVAL);
					KILL.add(killI);
				}

				//System.out.println("[info] nBB (number of basic blocks) : " + nBB);
				//System.out.println("[info] Uexp (Universal Set Of All Expressions) : " + Uexp);
				
				/**
				 * Iterate until no changes in AEin and AEout
				 */
				
				AEin = new ArrayList<ArrayList<Instruction>>(nBB);
				AEout = new ArrayList<ArrayList<Instruction>>(nBB);

				// initialize all AEin to Uexp
				for (int i = 0; i < nBB; i++) {
					AEin.add(i, new ArrayList<Instruction>(Uexp));
					AEout.add(i, new ArrayList<Instruction>(Uexp));
				}
				
				//int it = 0;
				AvailableExpressionIN _AEIN;
				do {
					_AEIN = new AvailableExpressionIN (EVAL, KILL, bb_ID, AEin, AEout);	
					pr.getBody().accept(_AEIN);
					AEin = _AEIN.getAEin() ;
					AEout = _AEIN.getAEout();
					//it++;
				} while (_AEIN.getmodifDone());
				
				//System.out.println("[info] Number Of Iterations Consumed in Computing the Available Expressions IN and OUT : "+it);
				/*
				int i=0;
				for(ArrayList<Instruction> in : AEout) {
					 System.out.println(" AEout ["+bb_ID.get(i)+"] : "+in);
					 i++;
				}
				*/
				GlobalCommonSubExpressionElimination GCSE = new GlobalCommonSubExpressionElimination(AEin, basicBlock, bbDepth);
				GCSE.Global_CSE();
			}
		}
		
		ClearControlFlow ccf = new ClearControlFlow(_project);
		ccf.compute();
		BuildControlFlow bcf = new BuildControlFlow(_project);
		bcf.compute();
		
//		System.out.println("[info] GlobalCommonSubExpressionElimination: DONE");
	}

	private boolean strContainsAsToken(String str1, String str2) {
		StringTokenizer st = new StringTokenizer(str1, " ,()/*-+<>=!&|");
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (token.equals(str2))
				return true;
		}
		return false;
	}
	


	private boolean strContainsAsTokenArrayRef(String s1, String s2) {
		
		String str2 = s2 + "[";
		StringTokenizer st = new StringTokenizer(s1, " ,()/*-+<>=!&|");
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (token.startsWith(str2) && token.endsWith("]")) {
				return true;
			}
		}
		return false;
	}
	

	
	/**
	 * Cas pointeur ou tableau
	 * 
	 */
	private ArrayList<Instruction> computeKilledExpr(int index, int nBB, ArrayList<String> symbolI,
			ArrayList<ArrayList<Instruction>> eVAL) {
		ArrayList<Instruction> killedexpr = new ArrayList<Instruction>();
		ArrayList<String> killedexpr_s = new ArrayList<String>();
		ArrayList<Instruction> evalI;
	
		for (String symbl : symbolI) {
			String sb = "*("+symbl+")";
			for (int j = 0; j < index; j++) {
				evalI = eVAL.get(j);
				for (Instruction inst : evalI) {
					String inststr = InstructionComparator.instToString(inst);
					if (strContainsAsToken(inststr, symbl) || strContainsAsToken(inststr, sb) || strContainsAsTokenArrayRef(inststr, symbl))
						if (!killedexpr_s.contains(inststr)) {
							killedexpr.add(inst);
							killedexpr_s.add(inststr);
						}
				}
			}
			for (int j = index + 1; j < nBB; j++) {
				evalI = eVAL.get(j);
				for (Instruction inst : evalI) {
					String inststr = InstructionComparator.instToString(inst);
					if (strContainsAsToken(inststr, symbl) || strContainsAsToken(inststr, sb) || strContainsAsTokenArrayRef(inststr, symbl))
						if (!killedexpr_s.contains(inststr)) {
							killedexpr.add(inst);
							killedexpr_s.add(inststr);
						}
				}
			}
		}
		return killedexpr;
	}
}
