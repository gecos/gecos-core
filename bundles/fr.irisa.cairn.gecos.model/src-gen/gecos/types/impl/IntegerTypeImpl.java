/**
 */
package gecos.types.impl;

import com.google.common.base.Objects;

import gecos.types.IntegerType;
import gecos.types.IntegerTypes;
import gecos.types.SignModifiers;
import gecos.types.Type;
import gecos.types.TypesPackage;
import gecos.types.TypesVisitor;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Integer Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.impl.IntegerTypeImpl#getSignModifier <em>Sign Modifier</em>}</li>
 *   <li>{@link gecos.types.impl.IntegerTypeImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IntegerTypeImpl extends BaseTypeImpl implements IntegerType {
	/**
	 * The default value of the '{@link #getSignModifier() <em>Sign Modifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignModifier()
	 * @generated
	 * @ordered
	 */
	protected static final SignModifiers SIGN_MODIFIER_EDEFAULT = SignModifiers.NONE;

	/**
	 * The cached value of the '{@link #getSignModifier() <em>Sign Modifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignModifier()
	 * @generated
	 * @ordered
	 */
	protected SignModifiers signModifier = SIGN_MODIFIER_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final IntegerTypes TYPE_EDEFAULT = IntegerTypes.INT;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected IntegerTypes type = TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IntegerTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypesPackage.Literals.INTEGER_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignModifiers getSignModifier() {
		return signModifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSignModifier(SignModifiers newSignModifier) {
		SignModifiers oldSignModifier = signModifier;
		signModifier = newSignModifier == null ? SIGN_MODIFIER_EDEFAULT : newSignModifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.INTEGER_TYPE__SIGN_MODIFIER, oldSignModifier, signModifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerTypes getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(IntegerTypes newType) {
		IntegerTypes oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.INTEGER_TYPE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final TypesVisitor visitor) {
		visitor.visitIntegerType(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean getSigned() {
		SignModifiers _signModifier = this.getSignModifier();
		return (!Objects.equal(_signModifier, SignModifiers.UNSIGNED));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isChar() {
		IntegerTypes _type = this.getType();
		return Objects.equal(_type, IntegerTypes.CHAR);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isShort() {
		IntegerTypes _type = this.getType();
		return Objects.equal(_type, IntegerTypes.SHORT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isLong() {
		IntegerTypes _type = this.getType();
		return Objects.equal(_type, IntegerTypes.LONG);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isLongLong() {
		IntegerTypes _type = this.getType();
		return Objects.equal(_type, IntegerTypes.LONG_LONG);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEqual(final Type other, final boolean ignoreStorageClass, final boolean ignoreConst, final boolean ignoreVolatile) {
		if ((other == this)) {
			return true;
		}
		if ((other instanceof IntegerType)) {
			return ((((IntegerType)other).getType().equals(this.getType()) && (((IntegerType)other).getSignModifier().getValue() == this.getSignModifier().getValue())) && super.isEqual(other, ignoreStorageClass, ignoreConst, ignoreVolatile));
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _string = super.toString();
		final StringBuffer result = new StringBuffer(_string);
		int _value = this.getSignModifier().getValue();
		boolean _tripleNotEquals = (_value != SignModifiers.NONE_VALUE);
		if (_tripleNotEquals) {
			result.append(this.getSignModifier().getLiteral().toLowerCase()).append(" ");
		}
		result.append(this.getType().getLiteral().toLowerCase());
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TypesPackage.INTEGER_TYPE__SIGN_MODIFIER:
				return getSignModifier();
			case TypesPackage.INTEGER_TYPE__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TypesPackage.INTEGER_TYPE__SIGN_MODIFIER:
				setSignModifier((SignModifiers)newValue);
				return;
			case TypesPackage.INTEGER_TYPE__TYPE:
				setType((IntegerTypes)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TypesPackage.INTEGER_TYPE__SIGN_MODIFIER:
				setSignModifier(SIGN_MODIFIER_EDEFAULT);
				return;
			case TypesPackage.INTEGER_TYPE__TYPE:
				setType(TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TypesPackage.INTEGER_TYPE__SIGN_MODIFIER:
				return signModifier != SIGN_MODIFIER_EDEFAULT;
			case TypesPackage.INTEGER_TYPE__TYPE:
				return type != TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //IntegerTypeImpl
