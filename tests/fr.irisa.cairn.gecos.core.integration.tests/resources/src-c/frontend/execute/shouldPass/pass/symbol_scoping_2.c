int a = 1;

int main() {
	{
		int a = 2;
		{
			int a = 3;

			if (a != 3)
				abort ();
		}

		if (a != 2)
			abort ();
	}
	if (a != 1)
		abort ();

	exit (0);
}
