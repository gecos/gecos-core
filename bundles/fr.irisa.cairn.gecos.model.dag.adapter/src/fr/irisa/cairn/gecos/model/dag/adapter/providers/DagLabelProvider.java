package fr.irisa.cairn.gecos.model.dag.adapter.providers;

import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGNodeAdapter;
import fr.irisa.cairn.graph.ISubGraphNode;
import fr.irisa.cairn.graph.implement.providers.NodeLabelProvider;
import gecos.dag.DAGCallNode;
import gecos.dag.DAGFloatImmNode;
import gecos.dag.DAGIntImmNode;
import gecos.dag.DAGNode;
import gecos.dag.DAGNumberedOutNode;
import gecos.dag.DAGNumberedSymbolNode;
import gecos.dag.DAGOpNode;
import gecos.dag.DAGOutNode;
import gecos.dag.DAGSimpleArrayNode;
import gecos.dag.DAGSymbolNode;
import gecos.dag.DAGVectorArrayAccessNode;
import gecos.dag.DAGVectorExpandNode;
import gecos.dag.DAGVectorExtractNode;
import gecos.dag.DAGVectorOpNode;
import gecos.dag.DAGVectorPackNode;
import gecos.dag.DAGVectorShuffleNode;
import gecos.dag.util.DagSwitch;

public class DagLabelProvider extends NodeLabelProvider<IDAGNodeAdapter, ISubGraphNode> {
	
	public static boolean SHOW_NODE_ANNOTATIONS = false;
	public static boolean SHOW_NODE_TYPE = false;
	public static int NODE_LABEL_MAX_LINE_SIZE = 20; //if label size is bigger split into multiple lines

	@Override
	public String getNodeLabel(IDAGNodeAdapter n) {
		StringBuffer label = new StringBuffer();
		DAGNode node = n.getAdaptedNode();
		label.append(DagLabel.INSTANCE.doSwitch(node));
		
		for(int offset = NODE_LABEL_MAX_LINE_SIZE; offset < label.length(); offset += NODE_LABEL_MAX_LINE_SIZE) {
			label.insert(offset, "\n");
		}
		if(SHOW_NODE_TYPE) 
			label.append("\n").append(node.getType());
		if(SHOW_NODE_ANNOTATIONS)
			for(String a : node.getAnnotations().keySet())
				label.append("\n").append(a);
		return label.toString();
	}

	private static class DagLabel extends DagSwitch<String> {
		public final static DagLabel INSTANCE = new DagLabel();

		private DagLabel() {

		}

		@Override
		public String caseDAGNode(DAGNode node) {
			return node.toString();
		}

		@Override
		public String caseDAGCallNode(DAGCallNode node) {
			return node.getName() + "()";
		}

		@Override
		public String caseDAGFloatImmNode(DAGFloatImmNode n) {
			return "" + n.getValue().getValue();
		}

		@Override
		public String caseDAGIntImmNode(DAGIntImmNode n) {
//			return "" + n.getIntValue();
			StringBuffer label = new StringBuffer(""+n.getIntValue());
			return label.toString();
		}

		@Override
		public String caseDAGOpNode(DAGOpNode node) {
			StringBuffer buf = new StringBuffer();
			buf.append(node.getOpcode()).append("_").append(node.getIdent());
			return buf.toString();
			
//			String opcode = String.getOpcodede();			
//			return opcode;
//			if (opcode.equals("add"))
//				opcode = "+";
//			else if (opcode.equals("mul"))
//				opcode = "*";
//			else if (opcode.equals("div"))
//				opcode = "/";
//			else if (opcode.equals("sub"))
//				opcode = "-";
//			else if (opcode.equals("shl"))
//				opcode = "<<";
//			else if (opcode.equals("shr"))
//				opcode = ">>";
//			else if (opcode.equals("INDIR"))
//				opcode = "*()";
//			else if (opcode.equals("SET"))
//				opcode = ":=";
//			else if (opcode.equals("or"))
//				opcode = "||";
//			else if (opcode.equals("and"))
//				opcode = "&&";
//			else if (opcode.equals("xor"))
//				opcode = "^";
//			else if (opcode.equals("neg"))
//				opcode = "-";
//			else if (opcode.equals("CVT"))
//				opcode = "cvt\\n"
//						+ String.getTypeNode().toString().toLowerCase();
//			return opcode;
		}
		
		@Override
		public String caseDAGOutNode(DAGOutNode node) {
			return node.getSymbol().getName();
		}

		@Override
		public String caseDAGSymbolNode(DAGSymbolNode node) {
			return node.getSymbol().getName();
		}

		@Override
		public String caseDAGSimpleArrayNode(DAGSimpleArrayNode node) {
//			return node.getSymbol().getName();
			
			StringBuffer buf = new StringBuffer();
//			buf.append(node.getSymbol().getName()).append("_").append(node.getIdent());
			buf.append(node.toString());
			return buf.toString();
		}

		@Override
		public String caseDAGNumberedSymbolNode(DAGNumberedSymbolNode node) {
			return node.getSymbol().getName() + "_" + node.getNumber();
		}

		@Override
		public String caseDAGNumberedOutNode(DAGNumberedOutNode node) {
			return node.getSymbol().getName() + "_" + node.getNumber();
		}
		
		@Override
		public String caseDAGVectorArrayAccessNode(DAGVectorArrayAccessNode node) {
			StringBuffer buf = new StringBuffer();
			buf.append(node.getSymbol().getName()).append("_").append(node.getIdent());
			return buf.toString();
		}
		
		@Override
		public String caseDAGVectorOpNode(DAGVectorOpNode node) {
			StringBuffer buf = new StringBuffer();
			buf.append(node.getOpcode()).append("_").append(node.getIdent());
			return buf.toString();
		}

		@Override
		public String caseDAGVectorExtractNode(DAGVectorExtractNode node) {
			StringBuffer buf = new StringBuffer();
			buf.append("ext_").append(node.getIdent()).append("@").append(node.getPos());
			return buf.toString();
		}
		
		@Override
		public String caseDAGVectorExpandNode(DAGVectorExpandNode node) {
			StringBuffer buf = new StringBuffer();
			buf.append("expand_").append(node.getIdent()).append(node.getPermutation().toString());
			return buf.toString();
		}

		@Override
		public String caseDAGVectorPackNode(DAGVectorPackNode node) {
			StringBuffer buf = new StringBuffer();
			buf.append("pack_").append(node.getIdent());
			return buf.toString();
		}

		@Override
		public String caseDAGVectorShuffleNode(DAGVectorShuffleNode node) {
			StringBuffer buf = new StringBuffer();
			buf.append("shuffle_").append(node.getIdent()).append(node.getPermutation().toString());
			return buf.toString();
		}

	}

}
