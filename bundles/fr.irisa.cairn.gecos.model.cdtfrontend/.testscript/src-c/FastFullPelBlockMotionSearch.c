typedef int* imgpel[1000][1000];
extern int block_sad[1000];
extern int BlockSAD[100][10][5][100][100];
extern int pos_00[1000][1000],offset_x,offset_y;
extern int block_index;
extern int GlobalMem[1000000];
extern int spiral_search_x[100],spiral_search_y[100];

int shift(int,int);
int MV_COST_SMP(int,int,int,short,short);

int                                                   //  ==> minimum motion cost after search
FastFullPelBlockMotionSearch (imgpel*   orig_pic,     // <--  not used
                              short     ref,          // <--  reference frame (0... or -1 (backward))
                              int       list,
                              int       pic_pix_x,    // <--  absolute x-coordinate of regarded AxB block
                              int       pic_pix_y,    // <--  absolute y-coordinate of regarded AxB block
                              int       blocktype,    // <--  block type (1-16x16 ... 7-4x4)
                              short     pred_mv_x,    // <--  motion vector predictor (x) in sub-pel units
                              short     pred_mv_y,    // <--  motion vector predictor (y) in sub-pel units
                              short    mv_x,         //  --> motion vector (x) - in pel units
                              short    mv_y,         //  --> motion vector (y) - in pel units
                              int       search_range, // <--  1-d search range in pel units
                              int       min_mcost,    // <--  minimum motion cost (cost for center or huge value)
                              int       lambda_factor)       // <--  lagrangian parameter for determining motion cost
{


int max_pos,best_pos,pos;
int input_rdopt;
int mcost,affine;
int cand_x,cand_y;
max_pos       = (2*search_range+1)*(2*search_range+1);              // number of search positions
best_pos      = 0;                                                  // position with minimum motion cost
  


  //===== cost for (0,0)-vector: it is done before, because MVCost can be negative =====
  if (input_rdopt != 0)
  {
    mcost = block_sad[pos_00[list][ref]] + MV_COST_SMP (lambda_factor, 0, 0, pred_mv_x, pred_mv_y);

//    if (mcost < min_mcost)

    if(affine > 0)
    {
      min_mcost = mcost;
      best_pos  = pos_00[list][ref];
    }
  }

  //===== loop over all search positions =====
  for (pos=0; pos<max_pos; pos++)
  {
    //--- check residual cost ---
    if (BlockSAD[list][ref][blocktype][block_index][pos] < min_mcost)
    {
      //--- get motion vector cost ---
      cand_x = shift((offset_x + spiral_search_x[pos]),2);
      cand_y = shift((offset_y + spiral_search_y[pos]),2);
      mcost  = BlockSAD[list][ref][blocktype][block_index][pos];
      mcost += MV_COST_SMP (lambda_factor, cand_x, cand_y, pred_mv_x, pred_mv_y);

      //--- check motion cost ---
      if (mcost < min_mcost)
      {
        min_mcost = mcost;
        best_pos  = pos;
      }
    }

  }

  //===== set best motion vector and return minimum motion cost =====
  GlobalMem[mv_x] = offset_x + spiral_search_x[best_pos];
  GlobalMem[mv_y] = offset_y + spiral_search_y[best_pos];

  return(min_mcost);
}
