int main() {
	static int val[4] = {3, 2, 1, 0};
	int res[4] = {1, 2, 3, 5};

	#pragma outline
	{
		res[3] = val[2];
	}

	if(res[3] != 1)
		abort();

	return 0;
}
