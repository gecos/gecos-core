/**
 */
package gecos.blocks.impl;

import gecos.annotations.AnnotationsPackage;

import gecos.annotations.impl.AnnotationsPackageImpl;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksFactory;
import gecos.blocks.BlocksPackage;
import gecos.blocks.BlocksVisitable;
import gecos.blocks.BlocksVisitor;
import gecos.blocks.Breakable;
import gecos.blocks.CaseBlock;
import gecos.blocks.CompositeBlock;
import gecos.blocks.Continuable;
import gecos.blocks.ControlEdge;
import gecos.blocks.DataEdge;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.FlowEdge;
import gecos.blocks.ForBlock;
import gecos.blocks.ForC99Block;
import gecos.blocks.IfBlock;
import gecos.blocks.Loop;
import gecos.blocks.SimpleForBlock;
import gecos.blocks.SwitchBlock;
import gecos.blocks.WhileBlock;

import gecos.core.CorePackage;

import gecos.core.impl.CorePackageImpl;

import gecos.dag.DagPackage;

import gecos.dag.impl.DagPackageImpl;

import gecos.instrs.InstrsPackage;

import gecos.instrs.impl.InstrsPackageImpl;

import gecos.types.TypesPackage;

import gecos.types.impl.TypesPackageImpl;

import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.ETypeParameter;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BlocksPackageImpl extends EPackageImpl implements BlocksPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blocksVisitorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blocksVisitableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass breakableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass continuableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass loopEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass basicBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compositeBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass controlEdgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataEdgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass flowEdgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ifBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass whileBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass doWhileBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass forBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass forC99BlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simpleForBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass switchBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass caseBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass caseMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType stringEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType eeLongEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType intEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType longEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see gecos.blocks.BlocksPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private BlocksPackageImpl() {
		super(eNS_URI, BlocksFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link BlocksPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static BlocksPackage init() {
		if (isInited) return (BlocksPackage)EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI);

		// Obtain or create and register package
		BlocksPackageImpl theBlocksPackage = (BlocksPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof BlocksPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new BlocksPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
		AnnotationsPackageImpl theAnnotationsPackage = (AnnotationsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI) instanceof AnnotationsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI) : AnnotationsPackage.eINSTANCE);
		InstrsPackageImpl theInstrsPackage = (InstrsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI) instanceof InstrsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI) : InstrsPackage.eINSTANCE);
		TypesPackageImpl theTypesPackage = (TypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI) instanceof TypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI) : TypesPackage.eINSTANCE);
		DagPackageImpl theDagPackage = (DagPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DagPackage.eNS_URI) instanceof DagPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DagPackage.eNS_URI) : DagPackage.eINSTANCE);

		// Create package meta-data objects
		theBlocksPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theAnnotationsPackage.createPackageContents();
		theInstrsPackage.createPackageContents();
		theTypesPackage.createPackageContents();
		theDagPackage.createPackageContents();

		// Initialize created meta-data
		theBlocksPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theAnnotationsPackage.initializePackageContents();
		theInstrsPackage.initializePackageContents();
		theTypesPackage.initializePackageContents();
		theDagPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theBlocksPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(BlocksPackage.eNS_URI, theBlocksPackage);
		return theBlocksPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlocksVisitor() {
		return blocksVisitorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlocksVisitable() {
		return blocksVisitableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlock() {
		return blockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlock_Parent() {
		return (EReference)blockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlock_Number() {
		return (EAttribute)blockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBreakable() {
		return breakableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getContinuable() {
		return continuableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLoop() {
		return loopEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBasicBlock() {
		return basicBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBasicBlock_Instructions() {
		return (EReference)basicBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBasicBlock_DefEdges() {
		return (EReference)basicBlockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBasicBlock_OutEdges() {
		return (EReference)basicBlockEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBasicBlock_UseEdges() {
		return (EReference)basicBlockEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBasicBlock_InEdges() {
		return (EReference)basicBlockEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBasicBlock_Label() {
		return (EAttribute)basicBlockEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBasicBlock_Flags() {
		return (EAttribute)basicBlockEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCompositeBlock() {
		return compositeBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCompositeBlock_Children() {
		return (EReference)compositeBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getControlEdge() {
		return controlEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getControlEdge_Cond() {
		return (EAttribute)controlEdgeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getControlEdge_Flags() {
		return (EAttribute)controlEdgeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getControlEdge_From() {
		return (EReference)controlEdgeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getControlEdge_To() {
		return (EReference)controlEdgeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataEdge() {
		return dataEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataEdge_Uses() {
		return (EReference)dataEdgeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataEdge_To() {
		return (EReference)dataEdgeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataEdge_From() {
		return (EReference)dataEdgeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFlowEdge() {
		return flowEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIfBlock() {
		return ifBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIfBlock_TestBlock() {
		return (EReference)ifBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIfBlock_ThenBlock() {
		return (EReference)ifBlockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIfBlock_ElseBlock() {
		return (EReference)ifBlockEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIfBlock_JumpTo() {
		return (EAttribute)ifBlockEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWhileBlock() {
		return whileBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWhileBlock_TestBlock() {
		return (EReference)whileBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWhileBlock_BodyBlock() {
		return (EReference)whileBlockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDoWhileBlock() {
		return doWhileBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDoWhileBlock_TestBlock() {
		return (EReference)doWhileBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDoWhileBlock_BodyBlock() {
		return (EReference)doWhileBlockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getForBlock() {
		return forBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getForBlock_InitBlock() {
		return (EReference)forBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getForBlock_TestBlock() {
		return (EReference)forBlockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getForBlock_StepBlock() {
		return (EReference)forBlockEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getForBlock_BodyBlock() {
		return (EReference)forBlockEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getForC99Block() {
		return forC99BlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimpleForBlock() {
		return simpleForBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimpleForBlock_Iterator() {
		return (EReference)simpleForBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimpleForBlock_Lb() {
		return (EReference)simpleForBlockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimpleForBlock_Ub() {
		return (EReference)simpleForBlockEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimpleForBlock_Stride() {
		return (EReference)simpleForBlockEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSwitchBlock() {
		return switchBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSwitchBlock_BodyBlock() {
		return (EReference)switchBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSwitchBlock_Cases() {
		return (EReference)switchBlockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSwitchBlock_DefaultBlock() {
		return (EReference)switchBlockEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSwitchBlock_DispatchBlock() {
		return (EReference)switchBlockEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCaseBlock() {
		return caseBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCaseBlock_Value() {
		return (EAttribute)caseBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCaseBlock_Body() {
		return (EReference)caseBlockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCaseMap() {
		return caseMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCaseMap_Key() {
		return (EAttribute)caseMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCaseMap_Value() {
		return (EReference)caseMapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getString() {
		return stringEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEELong() {
		return eeLongEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getint() {
		return intEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getlong() {
		return longEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlocksFactory getBlocksFactory() {
		return (BlocksFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		blocksVisitorEClass = createEClass(BLOCKS_VISITOR);

		blocksVisitableEClass = createEClass(BLOCKS_VISITABLE);

		blockEClass = createEClass(BLOCK);
		createEReference(blockEClass, BLOCK__PARENT);
		createEAttribute(blockEClass, BLOCK__NUMBER);

		breakableEClass = createEClass(BREAKABLE);

		continuableEClass = createEClass(CONTINUABLE);

		loopEClass = createEClass(LOOP);

		basicBlockEClass = createEClass(BASIC_BLOCK);
		createEReference(basicBlockEClass, BASIC_BLOCK__INSTRUCTIONS);
		createEReference(basicBlockEClass, BASIC_BLOCK__DEF_EDGES);
		createEReference(basicBlockEClass, BASIC_BLOCK__OUT_EDGES);
		createEReference(basicBlockEClass, BASIC_BLOCK__USE_EDGES);
		createEReference(basicBlockEClass, BASIC_BLOCK__IN_EDGES);
		createEAttribute(basicBlockEClass, BASIC_BLOCK__LABEL);
		createEAttribute(basicBlockEClass, BASIC_BLOCK__FLAGS);

		compositeBlockEClass = createEClass(COMPOSITE_BLOCK);
		createEReference(compositeBlockEClass, COMPOSITE_BLOCK__CHILDREN);

		controlEdgeEClass = createEClass(CONTROL_EDGE);
		createEAttribute(controlEdgeEClass, CONTROL_EDGE__COND);
		createEAttribute(controlEdgeEClass, CONTROL_EDGE__FLAGS);
		createEReference(controlEdgeEClass, CONTROL_EDGE__FROM);
		createEReference(controlEdgeEClass, CONTROL_EDGE__TO);

		dataEdgeEClass = createEClass(DATA_EDGE);
		createEReference(dataEdgeEClass, DATA_EDGE__USES);
		createEReference(dataEdgeEClass, DATA_EDGE__TO);
		createEReference(dataEdgeEClass, DATA_EDGE__FROM);

		flowEdgeEClass = createEClass(FLOW_EDGE);

		ifBlockEClass = createEClass(IF_BLOCK);
		createEReference(ifBlockEClass, IF_BLOCK__TEST_BLOCK);
		createEReference(ifBlockEClass, IF_BLOCK__THEN_BLOCK);
		createEReference(ifBlockEClass, IF_BLOCK__ELSE_BLOCK);
		createEAttribute(ifBlockEClass, IF_BLOCK__JUMP_TO);

		whileBlockEClass = createEClass(WHILE_BLOCK);
		createEReference(whileBlockEClass, WHILE_BLOCK__TEST_BLOCK);
		createEReference(whileBlockEClass, WHILE_BLOCK__BODY_BLOCK);

		doWhileBlockEClass = createEClass(DO_WHILE_BLOCK);
		createEReference(doWhileBlockEClass, DO_WHILE_BLOCK__TEST_BLOCK);
		createEReference(doWhileBlockEClass, DO_WHILE_BLOCK__BODY_BLOCK);

		forBlockEClass = createEClass(FOR_BLOCK);
		createEReference(forBlockEClass, FOR_BLOCK__INIT_BLOCK);
		createEReference(forBlockEClass, FOR_BLOCK__TEST_BLOCK);
		createEReference(forBlockEClass, FOR_BLOCK__STEP_BLOCK);
		createEReference(forBlockEClass, FOR_BLOCK__BODY_BLOCK);

		forC99BlockEClass = createEClass(FOR_C99_BLOCK);

		simpleForBlockEClass = createEClass(SIMPLE_FOR_BLOCK);
		createEReference(simpleForBlockEClass, SIMPLE_FOR_BLOCK__ITERATOR);
		createEReference(simpleForBlockEClass, SIMPLE_FOR_BLOCK__LB);
		createEReference(simpleForBlockEClass, SIMPLE_FOR_BLOCK__UB);
		createEReference(simpleForBlockEClass, SIMPLE_FOR_BLOCK__STRIDE);

		switchBlockEClass = createEClass(SWITCH_BLOCK);
		createEReference(switchBlockEClass, SWITCH_BLOCK__BODY_BLOCK);
		createEReference(switchBlockEClass, SWITCH_BLOCK__CASES);
		createEReference(switchBlockEClass, SWITCH_BLOCK__DEFAULT_BLOCK);
		createEReference(switchBlockEClass, SWITCH_BLOCK__DISPATCH_BLOCK);

		caseBlockEClass = createEClass(CASE_BLOCK);
		createEAttribute(caseBlockEClass, CASE_BLOCK__VALUE);
		createEReference(caseBlockEClass, CASE_BLOCK__BODY);

		caseMapEClass = createEClass(CASE_MAP);
		createEAttribute(caseMapEClass, CASE_MAP__KEY);
		createEReference(caseMapEClass, CASE_MAP__VALUE);

		// Create data types
		stringEDataType = createEDataType(STRING);
		eeLongEDataType = createEDataType(EE_LONG);
		intEDataType = createEDataType(INT);
		longEDataType = createEDataType(LONG);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		AnnotationsPackage theAnnotationsPackage = (AnnotationsPackage)EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI);
		InstrsPackage theInstrsPackage = (InstrsPackage)EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		blocksVisitableEClass.getESuperTypes().add(theCorePackage.getGecosNode());
		blockEClass.getESuperTypes().add(theAnnotationsPackage.getAnnotatedElement());
		blockEClass.getESuperTypes().add(this.getBlocksVisitable());
		loopEClass.getESuperTypes().add(this.getBreakable());
		loopEClass.getESuperTypes().add(this.getContinuable());
		basicBlockEClass.getESuperTypes().add(this.getBlock());
		basicBlockEClass.getESuperTypes().add(this.getBlocksVisitable());
		compositeBlockEClass.getESuperTypes().add(this.getBlock());
		compositeBlockEClass.getESuperTypes().add(theCorePackage.getScopeContainer());
		compositeBlockEClass.getESuperTypes().add(this.getBlocksVisitable());
		controlEdgeEClass.getESuperTypes().add(this.getFlowEdge());
		dataEdgeEClass.getESuperTypes().add(this.getFlowEdge());
		flowEdgeEClass.getESuperTypes().add(this.getBlocksVisitable());
		flowEdgeEClass.getESuperTypes().add(theAnnotationsPackage.getAnnotatedElement());
		ifBlockEClass.getESuperTypes().add(this.getBlock());
		ifBlockEClass.getESuperTypes().add(this.getBlocksVisitable());
		whileBlockEClass.getESuperTypes().add(this.getBlock());
		whileBlockEClass.getESuperTypes().add(this.getBlocksVisitable());
		whileBlockEClass.getESuperTypes().add(this.getLoop());
		doWhileBlockEClass.getESuperTypes().add(this.getBlock());
		doWhileBlockEClass.getESuperTypes().add(this.getBlocksVisitable());
		doWhileBlockEClass.getESuperTypes().add(this.getLoop());
		forBlockEClass.getESuperTypes().add(this.getBlock());
		forBlockEClass.getESuperTypes().add(this.getBlocksVisitable());
		forBlockEClass.getESuperTypes().add(this.getLoop());
		forC99BlockEClass.getESuperTypes().add(this.getForBlock());
		forC99BlockEClass.getESuperTypes().add(theCorePackage.getScopeContainer());
		forC99BlockEClass.getESuperTypes().add(this.getBlocksVisitable());
		simpleForBlockEClass.getESuperTypes().add(this.getForC99Block());
		switchBlockEClass.getESuperTypes().add(this.getBlock());
		switchBlockEClass.getESuperTypes().add(this.getBlocksVisitable());
		switchBlockEClass.getESuperTypes().add(this.getBreakable());
		caseBlockEClass.getESuperTypes().add(this.getBlock());
		caseBlockEClass.getESuperTypes().add(this.getBlocksVisitable());

		// Initialize classes and features; add operations and parameters
		initEClass(blocksVisitorEClass, BlocksVisitor.class, "BlocksVisitor", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = addEOperation(blocksVisitorEClass, null, "visitBasicBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBasicBlock(), "b", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(blocksVisitorEClass, null, "visitCompositeBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCompositeBlock(), "c", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(blocksVisitorEClass, null, "visitFlowEdge", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getFlowEdge(), "c", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(blocksVisitorEClass, null, "visitControlEdge", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getControlEdge(), "c", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(blocksVisitorEClass, null, "visitDataEdge", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDataEdge(), "c", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(blocksVisitorEClass, null, "visitForBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getForBlock(), "f", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(blocksVisitorEClass, null, "visitIfBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIfBlock(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(blocksVisitorEClass, null, "visitWhileBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getWhileBlock(), "w", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(blocksVisitorEClass, null, "visitLoopBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDoWhileBlock(), "l", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(blocksVisitorEClass, null, "visitSwitchBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSwitchBlock(), "s", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(blocksVisitorEClass, null, "visitCaseBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCaseBlock(), "c", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(blocksVisitorEClass, null, "visitForC99Block", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getForC99Block(), "f", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(blocksVisitorEClass, null, "visitSimpleForBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSimpleForBlock(), "f", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(blocksVisitableEClass, BlocksVisitable.class, "BlocksVisitable", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(blocksVisitableEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlocksVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(blockEClass, Block.class, "Block", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBlock_Parent(), this.getBlock(), null, "parent", null, 0, 1, Block.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlock_Number(), this.getint(), "number", null, 0, 1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(blockEClass, this.getBlock(), "simplifyBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(blockEClass, this.getString(), "getPathString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(blockEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(blockEClass, null, "addBlockAfter", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "insertedBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "existingBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(blockEClass, null, "mergeChildComposite", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCompositeBlock(), "b", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(blockEClass, null, "mergeWithBasic", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBasicBlock(), "to", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(blockEClass, null, "removeBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "bb", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(blockEClass, null, "replace", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "oldBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "newBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(blockEClass, this.getControlEdge(), "connectTo", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "to", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getBranchType(), "cond", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(blockEClass, this.getControlEdge(), "connectFromBasic", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBasicBlock(), "from", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getBranchType(), "cond", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(blockEClass, null, "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		ETypeParameter t1 = addETypeParameter(op, "T");
		EGenericType g1 = createEGenericType(this.getBlock());
		t1.getEBounds().add(g1);
		g1 = createEGenericType(t1);
		initEOperation(op, g1);

		op = addEOperation(blockEClass, this.getBlock(), "mergeWithComposite", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCompositeBlock(), "cblock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(blockEClass, theEcorePackage.getEBoolean(), "mergeChildren", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(blockEClass, theCorePackage.getScope(), "getScope", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(blockEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlocksVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(blockEClass, theCorePackage.getProcedure(), "getContainingProcedure", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(blockEClass, theCorePackage.getProcedureSet(), "getContainingProcedureSet", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(blockEClass, this.getString(), "getPathString", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getString(), "nodeName", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(blockEClass, theEcorePackage.getEBoolean(), "isRootBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(blockEClass, theCorePackage.getProcedure(), "getRootProcedure", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(blockEClass, this.getBlock(), "managedCopy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getBlockCopyManager(), "mng", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(blockEClass, this.getString(), "toShortString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(breakableEClass, Breakable.class, "Breakable", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(breakableEClass, this.getBasicBlock(), "listBreakBlocks", 0, -1, IS_UNIQUE, IS_ORDERED);

		addEOperation(breakableEClass, theInstrsPackage.getBreakInstruction(), "listBreakInstructions", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEClass(continuableEClass, Continuable.class, "Continuable", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(continuableEClass, this.getBasicBlock(), "listContinueBlocks", 0, -1, IS_UNIQUE, IS_ORDERED);

		addEOperation(continuableEClass, theInstrsPackage.getContinueInstruction(), "listContinueInstructions", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEClass(loopEClass, Loop.class, "Loop", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(basicBlockEClass, BasicBlock.class, "BasicBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBasicBlock_Instructions(), theInstrsPackage.getInstruction(), null, "instructions", null, 0, -1, BasicBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBasicBlock_DefEdges(), this.getDataEdge(), this.getDataEdge_From(), "defEdges", null, 0, -1, BasicBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBasicBlock_OutEdges(), this.getControlEdge(), this.getControlEdge_From(), "outEdges", null, 0, -1, BasicBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBasicBlock_UseEdges(), this.getDataEdge(), this.getDataEdge_To(), "useEdges", null, 0, -1, BasicBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBasicBlock_InEdges(), this.getControlEdge(), this.getControlEdge_To(), "inEdges", null, 0, -1, BasicBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBasicBlock_Label(), this.getString(), "label", "", 0, 1, BasicBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBasicBlock_Flags(), this.getint(), "flags", null, 0, 1, BasicBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(basicBlockEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(basicBlockEClass, this.getString(), "toShortString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(basicBlockEClass, this.getBasicBlock(), "getPredecessors", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(basicBlockEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlocksVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(basicBlockEClass, null, "clearOutEdges", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(basicBlockEClass, this.getBasicBlock(), "getSuccessors", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(basicBlockEClass, null, "removeInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getInstruction(), "s", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(basicBlockEClass, this.getString(), "contentString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(basicBlockEClass, null, "mergeWithBasic", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBasicBlock(), "b", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(basicBlockEClass, this.getBasicBlock(), "splitAt", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getInstruction(), "g", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(basicBlockEClass, null, "prependInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(basicBlockEClass, theCorePackage.getScope(), "getAssociatedScope", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(basicBlockEClass, this.getControlEdge(), "connectFromBasic", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBasicBlock(), "from", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getBranchType(), "cond", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(basicBlockEClass, this.getControlEdge(), "connectTo", 0, -1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "to", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getBranchType(), "cond", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(basicBlockEClass, null, "addInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(basicBlockEClass, theInstrsPackage.getInstruction(), "getInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getint(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(basicBlockEClass, null, "insertInstructionBefore", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getInstruction(), "pos", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(basicBlockEClass, null, "insertInstructionAfter", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getInstruction(), "pos", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(basicBlockEClass, null, "replaceInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getInstruction(), "oldi", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getInstruction(), "newi", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(basicBlockEClass, null, "removeAllInstructions", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(basicBlockEClass, theInstrsPackage.getInstruction(), "lastInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(basicBlockEClass, theEcorePackage.getEBoolean(), "isConnectedTo", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBasicBlock(), "b", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(basicBlockEClass, null, "disconnectFromBasic", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBasicBlock(), "from", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(basicBlockEClass, null, "disconnect", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(basicBlockEClass, this.getint(), "whichPredecessor", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBasicBlock(), "b", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(basicBlockEClass, null, "setFlag", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getint(), "f", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(basicBlockEClass, null, "addFlag", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getint(), "f", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(basicBlockEClass, null, "removeFlag", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getint(), "f", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(basicBlockEClass, theEcorePackage.getEBoolean(), "isSet", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getint(), "f", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(basicBlockEClass, theInstrsPackage.getInstruction(), "getFirstInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(basicBlockEClass, theInstrsPackage.getInstruction(), "getLastInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(basicBlockEClass, this.getint(), "getInstructionCount", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(basicBlockEClass, this.getString(), "getPathString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(basicBlockEClass, this.getBlock(), "managedCopy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getBlockCopyManager(), "mng", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(basicBlockEClass, theEcorePackage.getEBoolean(), "isDAG", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(compositeBlockEClass, CompositeBlock.class, "CompositeBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCompositeBlock_Children(), this.getBlock(), null, "children", null, 0, -1, CompositeBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(compositeBlockEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getCoreVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(compositeBlockEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlocksVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(compositeBlockEClass, null, "addBlockBefore", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "pos", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(compositeBlockEClass, null, "addBlockAfter", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "pos", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(compositeBlockEClass, null, "addChildren", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "bi", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(compositeBlockEClass, this.getBlock(), "managedCopy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getBlockCopyManager(), "mng", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(compositeBlockEClass, null, "replace", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "oldb", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "newb", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(compositeBlockEClass, this.getBlock(), "simplifyBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(compositeBlockEClass, this.getBlock(), "mergeWithComposite", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCompositeBlock(), "block", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(compositeBlockEClass, null, "mergeChildComposite", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCompositeBlock(), "b", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(compositeBlockEClass, theEcorePackage.getEBoolean(), "mergeChildren", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(compositeBlockEClass, null, "removeBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "b", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(compositeBlockEClass, null, "addBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "child", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(compositeBlockEClass, this.getControlEdge(), "connectTo", 0, -1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "to", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getBranchType(), "cond", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(compositeBlockEClass, this.getBlock(), "getLastChilld", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(compositeBlockEClass, null, "addBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "child", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEBoolean(), "nolink", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(compositeBlockEClass, this.getControlEdge(), "connectFromBasic", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBasicBlock(), "from", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getBranchType(), "cond", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(compositeBlockEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(compositeBlockEClass, this.getString(), "getPathString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(compositeBlockEClass, this.getString(), "toShortString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(controlEdgeEClass, ControlEdge.class, "ControlEdge", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getControlEdge_Cond(), theInstrsPackage.getBranchType(), "cond", null, 0, 1, ControlEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getControlEdge_Flags(), this.getint(), "flags", null, 0, 1, ControlEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getControlEdge_From(), this.getBasicBlock(), this.getBasicBlock_OutEdges(), "from", null, 0, 1, ControlEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getControlEdge_To(), this.getBasicBlock(), this.getBasicBlock_InEdges(), "to", null, 0, 1, ControlEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(controlEdgeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(controlEdgeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlocksVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(controlEdgeEClass, null, "disconnectFrom", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBasicBlock(), "from", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(controlEdgeEClass, null, "disconnect", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(controlEdgeEClass, null, "setFlag", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getint(), "f", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(controlEdgeEClass, null, "addFlag", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getint(), "f", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(controlEdgeEClass, null, "removeFlag", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getint(), "f", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(controlEdgeEClass, theEcorePackage.getEBoolean(), "isSet", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getint(), "f", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dataEdgeEClass, DataEdge.class, "DataEdge", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataEdge_Uses(), theInstrsPackage.getSSAUseSymbol(), null, "uses", null, 0, -1, DataEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataEdge_To(), this.getBasicBlock(), this.getBasicBlock_UseEdges(), "to", null, 0, 1, DataEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataEdge_From(), this.getBasicBlock(), this.getBasicBlock_DefEdges(), "from", null, 0, 1, DataEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(dataEdgeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlocksVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(flowEdgeEClass, FlowEdge.class, "FlowEdge", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(flowEdgeEClass, this.getBasicBlock(), "getFrom", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(flowEdgeEClass, this.getBasicBlock(), "getTo", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(flowEdgeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlocksVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(ifBlockEClass, IfBlock.class, "IfBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIfBlock_TestBlock(), this.getBlock(), null, "testBlock", null, 0, 1, IfBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIfBlock_ThenBlock(), this.getBlock(), null, "thenBlock", null, 0, 1, IfBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIfBlock_ElseBlock(), this.getBlock(), null, "elseBlock", null, 0, 1, IfBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIfBlock_JumpTo(), this.getString(), "jumpTo", null, 0, 1, IfBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(ifBlockEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlocksVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ifBlockEClass, this.getBlock(), "managedCopy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getBlockCopyManager(), "mng", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(ifBlockEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(ifBlockEClass, this.getString(), "toShortString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(ifBlockEClass, this.getBlock(), "simplifyBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ifBlockEClass, null, "removeBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "bb", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ifBlockEClass, this.getControlEdge(), "connectFromBasic", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBasicBlock(), "dispatch", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getBranchType(), "cond", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ifBlockEClass, this.getControlEdge(), "connectTo", 0, -1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "to", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getBranchType(), "cond", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ifBlockEClass, null, "replace", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "oldb", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "newb", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(ifBlockEClass, this.getString(), "getPathString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(whileBlockEClass, WhileBlock.class, "WhileBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getWhileBlock_TestBlock(), this.getBlock(), null, "testBlock", null, 0, 1, WhileBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWhileBlock_BodyBlock(), this.getBlock(), null, "bodyBlock", null, 0, 1, WhileBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(whileBlockEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlocksVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(whileBlockEClass, this.getBasicBlock(), "listContinueBlocks", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(whileBlockEClass, theInstrsPackage.getContinueInstruction(), "listContinueInstructions", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(whileBlockEClass, this.getBasicBlock(), "listBreakBlocks", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(whileBlockEClass, theInstrsPackage.getBreakInstruction(), "listBreakInstructions", 0, -1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(whileBlockEClass, this.getBlock(), "managedCopy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getBlockCopyManager(), "mng", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(whileBlockEClass, null, "mergeWithBasic", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBasicBlock(), "to", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(whileBlockEClass, this.getBlock(), "mergeWithComposite", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCompositeBlock(), "compositeBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(whileBlockEClass, this.getBlock(), "simplifyBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(whileBlockEClass, null, "addBlockAfter", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "a", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "b", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(whileBlockEClass, null, "removeBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "bb", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(whileBlockEClass, null, "mergeChildComposite", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCompositeBlock(), "b", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(whileBlockEClass, this.getControlEdge(), "connectFromBasic", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBasicBlock(), "dispatch", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getBranchType(), "cond", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(whileBlockEClass, this.getControlEdge(), "connectTo", 0, -1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "next", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getBranchType(), "cond", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(whileBlockEClass, null, "replace", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "oldb", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "newb", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(whileBlockEClass, this.getString(), "getPathString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(whileBlockEClass, this.getString(), "toShortString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(doWhileBlockEClass, DoWhileBlock.class, "DoWhileBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDoWhileBlock_TestBlock(), this.getBlock(), null, "testBlock", null, 0, 1, DoWhileBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDoWhileBlock_BodyBlock(), this.getBlock(), null, "bodyBlock", null, 0, 1, DoWhileBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(doWhileBlockEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlocksVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(doWhileBlockEClass, this.getBasicBlock(), "listContinueBlocks", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(doWhileBlockEClass, theInstrsPackage.getContinueInstruction(), "listContinueInstructions", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(doWhileBlockEClass, this.getBasicBlock(), "listBreakBlocks", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(doWhileBlockEClass, theInstrsPackage.getBreakInstruction(), "listBreakInstructions", 0, -1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(doWhileBlockEClass, null, "addBlockAfter", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "a", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "b", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(doWhileBlockEClass, null, "removeBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "bb", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(doWhileBlockEClass, null, "mergeChildComposite", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCompositeBlock(), "b", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(doWhileBlockEClass, this.getControlEdge(), "connectFromBasic", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBasicBlock(), "dispatch", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getBranchType(), "cond", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(doWhileBlockEClass, this.getControlEdge(), "connectTo", 0, -1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "to", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getBranchType(), "cond", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(doWhileBlockEClass, this.getBlock(), "managedCopy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getBlockCopyManager(), "mng", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(doWhileBlockEClass, null, "replace", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "oldb", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "newb", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(doWhileBlockEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(doWhileBlockEClass, this.getString(), "toShortString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(forBlockEClass, ForBlock.class, "ForBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getForBlock_InitBlock(), this.getBasicBlock(), null, "initBlock", null, 0, 1, ForBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getForBlock_TestBlock(), this.getBasicBlock(), null, "testBlock", null, 0, 1, ForBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getForBlock_StepBlock(), this.getBasicBlock(), null, "stepBlock", null, 0, 1, ForBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getForBlock_BodyBlock(), this.getBlock(), null, "bodyBlock", null, 0, 1, ForBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(forBlockEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlocksVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(forBlockEClass, this.getBasicBlock(), "listContinueBlocks", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(forBlockEClass, theInstrsPackage.getContinueInstruction(), "listContinueInstructions", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(forBlockEClass, this.getBasicBlock(), "listBreakBlocks", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(forBlockEClass, theInstrsPackage.getBreakInstruction(), "listBreakInstructions", 0, -1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(forBlockEClass, this.getBlock(), "managedCopy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getBlockCopyManager(), "mng", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(forBlockEClass, null, "replace", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "oldb", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "newb", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(forBlockEClass, this.getBlock(), "simplifyBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(forBlockEClass, null, "addBlockAfter", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "insertedBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "existingBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(forBlockEClass, null, "removeBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "bb", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(forBlockEClass, this.getControlEdge(), "connectFromBasic", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBasicBlock(), "dispatch", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getBranchType(), "cond", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(forBlockEClass, this.getControlEdge(), "connectTo", 0, -1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "to", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getBranchType(), "cond", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(forBlockEClass, this.getString(), "toShortString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(forBlockEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(forBlockEClass, this.getString(), "getPathString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(forC99BlockEClass, ForC99Block.class, "ForC99Block", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(forC99BlockEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlocksVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(forC99BlockEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getCoreVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(simpleForBlockEClass, SimpleForBlock.class, "SimpleForBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSimpleForBlock_Iterator(), theCorePackage.getSymbol(), null, "iterator", null, 0, 1, SimpleForBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimpleForBlock_Lb(), theInstrsPackage.getInstruction(), null, "lb", null, 0, 1, SimpleForBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimpleForBlock_Ub(), theInstrsPackage.getInstruction(), null, "ub", null, 0, 1, SimpleForBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimpleForBlock_Stride(), theInstrsPackage.getInstruction(), null, "stride", null, 0, 1, SimpleForBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(simpleForBlockEClass, this.getBlock(), "managedCopy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getBlockCopyManager(), "mng", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(simpleForBlockEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlocksVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(switchBlockEClass, SwitchBlock.class, "SwitchBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSwitchBlock_BodyBlock(), this.getBlock(), null, "bodyBlock", null, 0, 1, SwitchBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSwitchBlock_Cases(), this.getCaseMap(), null, "cases", null, 0, -1, SwitchBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSwitchBlock_DefaultBlock(), this.getBlock(), null, "defaultBlock", null, 0, 1, SwitchBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSwitchBlock_DispatchBlock(), this.getBasicBlock(), null, "dispatchBlock", null, 0, 1, SwitchBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(switchBlockEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlocksVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(switchBlockEClass, this.getBasicBlock(), "listBreakBlocks", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(switchBlockEClass, theInstrsPackage.getBreakInstruction(), "listBreakInstructions", 0, -1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(switchBlockEClass, this.getControlEdge(), "connectFromBasic", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBasicBlock(), "dispatch", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getBranchType(), "cond", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(switchBlockEClass, this.getControlEdge(), "connectTo", 0, -1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "next", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getBranchType(), "cond", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(switchBlockEClass, this.getBlock(), "simplifyBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(switchBlockEClass, null, "replace", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "oldb", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "newb", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(switchBlockEClass, null, "removeBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlock(), "bb", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(switchBlockEClass, this.getBlock(), "managedCopy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getBlockCopyManager(), "mng", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(switchBlockEClass, this.getString(), "toShortString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(switchBlockEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(caseBlockEClass, CaseBlock.class, "CaseBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCaseBlock_Value(), this.getlong(), "value", "0", 1, 1, CaseBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCaseBlock_Body(), this.getBlock(), null, "body", null, 0, 1, CaseBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(caseBlockEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlocksVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(caseMapEClass, Map.Entry.class, "CaseMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCaseMap_Key(), this.getEELong(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCaseMap_Value(), this.getBlock(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize data types
		initEDataType(stringEDataType, String.class, "String", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(eeLongEDataType, Long.class, "EELong", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(intEDataType, int.class, "int", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(longEDataType, long.class, "long", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// gmf.node
		createGmfAnnotations();
		// gmf.compartment
		createGmf_1Annotations();
		// gmf.link
		createGmf_2Annotations();
		// http://www.eclipse.org/ocl/examples/OCL
		createOCLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
		   });
	}

	/**
	 * Initializes the annotations for <b>gmf.node</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGmfAnnotations() {
		String source = "gmf.node";	
		addAnnotation
		  (blockEClass, 
		   source, 
		   new String[] {
			 "figure", "rectangle",
			 "label", "number",
			 "label.placement", "external",
			 "label.icon", "false"
		   });	
		addAnnotation
		  (basicBlockEClass, 
		   source, 
		   new String[] {
			 "figure", "rectangle",
			 "label", "number",
			 "label.placement", "external",
			 "label.icon", "false"
		   });	
		addAnnotation
		  (compositeBlockEClass, 
		   source, 
		   new String[] {
			 "figure", "rectangle",
			 "label", "number",
			 "label.placement", "external",
			 "label.icon", "false"
		   });	
		addAnnotation
		  (ifBlockEClass, 
		   source, 
		   new String[] {
			 "figure", "rectangle",
			 "label", "number",
			 "label.placement", "external",
			 "label.icon", "false"
		   });	
		addAnnotation
		  (whileBlockEClass, 
		   source, 
		   new String[] {
			 "figure", "rectangle",
			 "label", "number",
			 "label.placement", "external",
			 "label.icon", "false"
		   });	
		addAnnotation
		  (doWhileBlockEClass, 
		   source, 
		   new String[] {
			 "figure", "rectangle",
			 "label", "number",
			 "label.placement", "external",
			 "label.icon", "false"
		   });	
		addAnnotation
		  (forBlockEClass, 
		   source, 
		   new String[] {
			 "figure", "rectangle",
			 "label", "number",
			 "label.placement", "external",
			 "label.icon", "false"
		   });	
		addAnnotation
		  (switchBlockEClass, 
		   source, 
		   new String[] {
			 "figure", "rectangle",
			 "label", "number",
			 "label.placement", "external",
			 "label.icon", "false"
		   });	
		addAnnotation
		  (caseBlockEClass, 
		   source, 
		   new String[] {
			 "figure", "rectangle",
			 "label", "number",
			 "label.placement", "external",
			 "label.icon", "false"
		   });
	}

	/**
	 * Initializes the annotations for <b>gmf.compartment</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGmf_1Annotations() {
		String source = "gmf.compartment";	
		addAnnotation
		  (getCompositeBlock_Children(), 
		   source, 
		   new String[] {
			 "foo", "bar"
		   });	
		addAnnotation
		  (getIfBlock_TestBlock(), 
		   source, 
		   new String[] {
			 "foo", "bar"
		   });	
		addAnnotation
		  (getIfBlock_ThenBlock(), 
		   source, 
		   new String[] {
			 "foo", "bar"
		   });	
		addAnnotation
		  (getIfBlock_ElseBlock(), 
		   source, 
		   new String[] {
			 "foo", "bar"
		   });	
		addAnnotation
		  (getWhileBlock_TestBlock(), 
		   source, 
		   new String[] {
			 "foo", "bar"
		   });	
		addAnnotation
		  (getWhileBlock_BodyBlock(), 
		   source, 
		   new String[] {
			 "foo", "bar"
		   });	
		addAnnotation
		  (getDoWhileBlock_TestBlock(), 
		   source, 
		   new String[] {
			 "foo", "bar"
		   });	
		addAnnotation
		  (getDoWhileBlock_BodyBlock(), 
		   source, 
		   new String[] {
			 "foo", "bar"
		   });	
		addAnnotation
		  (getForBlock_InitBlock(), 
		   source, 
		   new String[] {
			 "foo", "bar"
		   });	
		addAnnotation
		  (getForBlock_TestBlock(), 
		   source, 
		   new String[] {
			 "foo", "bar"
		   });	
		addAnnotation
		  (getForBlock_StepBlock(), 
		   source, 
		   new String[] {
			 "foo", "bar"
		   });	
		addAnnotation
		  (getForBlock_BodyBlock(), 
		   source, 
		   new String[] {
			 "foo", "bar"
		   });	
		addAnnotation
		  (getSwitchBlock_BodyBlock(), 
		   source, 
		   new String[] {
			 "foo", "bar"
		   });	
		addAnnotation
		  (getSwitchBlock_Cases(), 
		   source, 
		   new String[] {
			 "foo", "bar"
		   });	
		addAnnotation
		  (getSwitchBlock_DefaultBlock(), 
		   source, 
		   new String[] {
			 "foo", "bar"
		   });	
		addAnnotation
		  (getSwitchBlock_DispatchBlock(), 
		   source, 
		   new String[] {
			 "foo", "bar"
		   });	
		addAnnotation
		  (getCaseBlock_Body(), 
		   source, 
		   new String[] {
			 "foo", "bar"
		   });
	}

	/**
	 * Initializes the annotations for <b>gmf.link</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGmf_2Annotations() {
		String source = "gmf.link";	
		addAnnotation
		  (controlEdgeEClass, 
		   source, 
		   new String[] {
			 "source", "from",
			 "target", "to",
			 "style", "dot",
			 "width", "1"
		   });	
		addAnnotation
		  (dataEdgeEClass, 
		   source, 
		   new String[] {
			 "source", "from",
			 "target", "to",
			 "style", "dot",
			 "width", "1"
		   });	
		addAnnotation
		  (flowEdgeEClass, 
		   source, 
		   new String[] {
			 "source", "from",
			 "target", "to",
			 "style", "dot",
			 "width", "1"
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/ocl/examples/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.eclipse.org/ocl/examples/OCL";	
		addAnnotation
		  (switchBlockEClass, 
		   source, 
		   new String[] {
			 "differentValues", "self.cases->forAll(a,b | a.value=b.value implies a=b) "
		   });
	}

} //BlocksPackageImpl
