/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.tools.io;

import java.util.LinkedHashMap;
import java.util.Map;

public class ProfileData implements IProfileData {

	private Map<Integer, Integer> counts;

	public ProfileData() {
		counts = new LinkedHashMap<Integer, Integer>();
	}
	
	public ProfileData compute() {
		return this;
	}

	public void add(int block, int count) {
		counts.put(new Integer(block), new Integer(count));
	}

	public int get(int block) {
		Integer value = (Integer) counts.get(new Integer(block));
		if (value == null) return 0;
		return value.intValue();
	}
}
