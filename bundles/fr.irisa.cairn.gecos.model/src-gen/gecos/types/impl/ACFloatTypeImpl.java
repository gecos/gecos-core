/**
 */
package gecos.types.impl;

import gecos.types.ACFloatType;
import gecos.types.Type;
import gecos.types.TypesPackage;
import gecos.types.TypesVisitor;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AC Float Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.impl.ACFloatTypeImpl#getExponent <em>Exponent</em>}</li>
 *   <li>{@link gecos.types.impl.ACFloatTypeImpl#getMantissa <em>Mantissa</em>}</li>
 *   <li>{@link gecos.types.impl.ACFloatTypeImpl#getRadix <em>Radix</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ACFloatTypeImpl extends ACTypeImpl implements ACFloatType {
	/**
	 * The default value of the '{@link #getExponent() <em>Exponent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExponent()
	 * @generated
	 * @ordered
	 */
	protected static final long EXPONENT_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getExponent() <em>Exponent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExponent()
	 * @generated
	 * @ordered
	 */
	protected long exponent = EXPONENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getMantissa() <em>Mantissa</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMantissa()
	 * @generated
	 * @ordered
	 */
	protected static final long MANTISSA_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getMantissa() <em>Mantissa</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMantissa()
	 * @generated
	 * @ordered
	 */
	protected long mantissa = MANTISSA_EDEFAULT;

	/**
	 * The default value of the '{@link #getRadix() <em>Radix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRadix()
	 * @generated
	 * @ordered
	 */
	protected static final long RADIX_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getRadix() <em>Radix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRadix()
	 * @generated
	 * @ordered
	 */
	protected long radix = RADIX_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ACFloatTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypesPackage.Literals.AC_FLOAT_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getExponent() {
		return exponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExponent(long newExponent) {
		long oldExponent = exponent;
		exponent = newExponent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.AC_FLOAT_TYPE__EXPONENT, oldExponent, exponent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getMantissa() {
		return mantissa;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMantissa(long newMantissa) {
		long oldMantissa = mantissa;
		mantissa = newMantissa;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.AC_FLOAT_TYPE__MANTISSA, oldMantissa, mantissa));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getRadix() {
		return radix;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRadix(long newRadix) {
		long oldRadix = radix;
		radix = newRadix;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.AC_FLOAT_TYPE__RADIX, oldRadix, radix));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final TypesVisitor visitor) {
		visitor.visitACFloatType(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEqual(final Type other, final boolean ignoreStorageClass, final boolean ignoreConst, final boolean ignoreVolatile) {
		if ((other == this)) {
			return true;
		}
		if ((other instanceof ACFloatType)) {
			return ((((((ACFloatType)other).getMantissa() == this.getMantissa()) && (((ACFloatType)other).getExponent() == this.getExponent())) && (((ACFloatType)other).getRadix() == this.getRadix())) && super.isEqual(other, ignoreStorageClass, ignoreConst, ignoreVolatile));
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		final StringBuffer result = new StringBuffer("ac_float<");
		long _exponent = this.getExponent();
		String _plus = (Long.valueOf(_exponent) + ",");
		result.append(_plus);
		long _mantissa = this.getMantissa();
		String _plus_1 = (Long.valueOf(_mantissa) + ",");
		result.append(_plus_1);
		result.append(this.getRadix());
		result.append(">");
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TypesPackage.AC_FLOAT_TYPE__EXPONENT:
				return getExponent();
			case TypesPackage.AC_FLOAT_TYPE__MANTISSA:
				return getMantissa();
			case TypesPackage.AC_FLOAT_TYPE__RADIX:
				return getRadix();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TypesPackage.AC_FLOAT_TYPE__EXPONENT:
				setExponent((Long)newValue);
				return;
			case TypesPackage.AC_FLOAT_TYPE__MANTISSA:
				setMantissa((Long)newValue);
				return;
			case TypesPackage.AC_FLOAT_TYPE__RADIX:
				setRadix((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TypesPackage.AC_FLOAT_TYPE__EXPONENT:
				setExponent(EXPONENT_EDEFAULT);
				return;
			case TypesPackage.AC_FLOAT_TYPE__MANTISSA:
				setMantissa(MANTISSA_EDEFAULT);
				return;
			case TypesPackage.AC_FLOAT_TYPE__RADIX:
				setRadix(RADIX_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TypesPackage.AC_FLOAT_TYPE__EXPONENT:
				return exponent != EXPONENT_EDEFAULT;
			case TypesPackage.AC_FLOAT_TYPE__MANTISSA:
				return mantissa != MANTISSA_EDEFAULT;
			case TypesPackage.AC_FLOAT_TYPE__RADIX:
				return radix != RADIX_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //ACFloatTypeImpl
