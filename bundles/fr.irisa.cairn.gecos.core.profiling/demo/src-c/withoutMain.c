#define TYPE_0 float
#define N 125


TYPE_0 C1[3] = {0.31, 0.46, 0.33};


void conv(TYPE_0 in[N], TYPE_0 *out, int idx) {
	$size(out, N);
	$inject(in, $random_uniform(-10, 20, 20));

	TYPE_0 c[3] = {0.2, 0.35, 0.45};

	int i;
	if(idx%2) {
		for (i = 1; i < N-1; i++)
			out[i] = c[0]*in[i-1] + c[1]*in[i] + c[2]*in[i+1];
	} else {
		for (i = 1; i < N-1; i++)
			out[i] = C1[0]*in[i-1] + C1[1]*in[i] + C1[2]*in[i+1];
	}

	$save(out);//, N); // size is optional since $size(out) was specified
}

