/**
 */
package gecos.types;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AC Float Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.ACFloatType#getExponent <em>Exponent</em>}</li>
 *   <li>{@link gecos.types.ACFloatType#getMantissa <em>Mantissa</em>}</li>
 *   <li>{@link gecos.types.ACFloatType#getRadix <em>Radix</em>}</li>
 * </ul>
 *
 * @see gecos.types.TypesPackage#getACFloatType()
 * @model
 * @generated
 */
public interface ACFloatType extends ACType, TypesVisitable {
	/**
	 * Returns the value of the '<em><b>Exponent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exponent</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exponent</em>' attribute.
	 * @see #setExponent(long)
	 * @see gecos.types.TypesPackage#getACFloatType_Exponent()
	 * @model unique="false" dataType="gecos.types.long"
	 * @generated
	 */
	long getExponent();

	/**
	 * Sets the value of the '{@link gecos.types.ACFloatType#getExponent <em>Exponent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exponent</em>' attribute.
	 * @see #getExponent()
	 * @generated
	 */
	void setExponent(long value);

	/**
	 * Returns the value of the '<em><b>Mantissa</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mantissa</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mantissa</em>' attribute.
	 * @see #setMantissa(long)
	 * @see gecos.types.TypesPackage#getACFloatType_Mantissa()
	 * @model unique="false" dataType="gecos.types.long"
	 * @generated
	 */
	long getMantissa();

	/**
	 * Sets the value of the '{@link gecos.types.ACFloatType#getMantissa <em>Mantissa</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mantissa</em>' attribute.
	 * @see #getMantissa()
	 * @generated
	 */
	void setMantissa(long value);

	/**
	 * Returns the value of the '<em><b>Radix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Radix</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Radix</em>' attribute.
	 * @see #setRadix(long)
	 * @see gecos.types.TypesPackage#getACFloatType_Radix()
	 * @model unique="false" dataType="gecos.types.long"
	 * @generated
	 */
	long getRadix();

	/**
	 * Sets the value of the '{@link gecos.types.ACFloatType#getRadix <em>Radix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Radix</em>' attribute.
	 * @see #getRadix()
	 * @generated
	 */
	void setRadix(long value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitACFloatType(this);'"
	 * @generated
	 */
	void accept(TypesVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.boolean" unique="false" otherUnique="false" ignoreStorageClassDataType="gecos.types.boolean" ignoreStorageClassUnique="false" ignoreConstDataType="gecos.types.boolean" ignoreConstUnique="false" ignoreVolatileDataType="gecos.types.boolean" ignoreVolatileUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((other == this))\n{\n\treturn true;\n}\nif ((other instanceof &lt;%gecos.types.ACFloatType%&gt;))\n{\n\treturn ((((((&lt;%gecos.types.ACFloatType%&gt;)other).getMantissa() == this.getMantissa()) &amp;&amp; (((&lt;%gecos.types.ACFloatType%&gt;)other).getExponent() == this.getExponent())) &amp;&amp; (((&lt;%gecos.types.ACFloatType%&gt;)other).getRadix() == this.getRadix())) &amp;&amp; super.isEqual(other, ignoreStorageClass, ignoreConst, ignoreVolatile));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isEqual(Type other, boolean ignoreStorageClass, boolean ignoreConst, boolean ignoreVolatile);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%java.lang.StringBuffer%&gt; result = new &lt;%java.lang.StringBuffer%&gt;(\"ac_float&lt;\");\nlong _exponent = this.getExponent();\n&lt;%java.lang.String%&gt; _plus = (&lt;%java.lang.Long%&gt;.valueOf(_exponent) + \",\");\nresult.append(_plus);\nlong _mantissa = this.getMantissa();\n&lt;%java.lang.String%&gt; _plus_1 = (&lt;%java.lang.Long%&gt;.valueOf(_mantissa) + \",\");\nresult.append(_plus_1);\nresult.append(this.getRadix());\nresult.append(\"&gt;\");\nreturn result.toString();'"
	 * @generated
	 */
	String toString();

} // ACFloatType
