/*
 Copyright (C) 2006 Pedro Felzenszwalb

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <common.h>

unsigned char** mat_malloc(int width, int height) {
	unsigned char** line = (unsigned char**) malloc(width
			* sizeof(unsigned char *));
	{
		int i;
		for (i = 0; i < width; i++) {
			line[i] = (unsigned char*) malloc(height * sizeof(unsigned char));
		}
	}
	return line;
}

float*** cube_malloc(int width, int height, int value) {
	float*** line = (float***) malloc(width * sizeof(float **));
	int i, j, v;
	for (i = 0; i < width; i++) {
		line[i] = (float **) malloc(height * sizeof(float *));
		for (j = 0; j < height; j++) {
			line[i][j] = (float *) malloc(value * sizeof(float));
			for (v = 0; v < value; v++) {
				line[i][j][v] = 0.0;
			}
		}
	}
	return line;
}

float**** hypercube_malloc(int level, int width, int height, int value) {
	float**** line = (float****) malloc(level * sizeof(float ***));
	int i, j, k, v;
	for (i = 0; i < level; i++) {
		line[i] = (float ***) malloc(width * sizeof(float **));
		for (j = 0; j < width; j++) {
			line[i][j] = (float **) malloc(height * sizeof(float *));
			for (k = 0; k < height; k++) {
				line[i][j][k] = (float *) malloc(value * sizeof(float));
				for (v = 0; v < value; v++) {
					line[i][j][k][v] = 0.0;
				}
			}
		}
	}
	return line;
}

