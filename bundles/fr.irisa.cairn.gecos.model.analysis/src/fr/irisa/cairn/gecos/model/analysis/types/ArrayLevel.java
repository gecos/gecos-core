package fr.irisa.cairn.gecos.model.analysis.types;

import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.types.ArrayType;
import gecos.types.BaseType;
import gecos.types.Type;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

/**
 * Represent a N dimensional array type by: N and the sizes of each dimension
 * 
 * @author aelmouss
 */
public class ArrayLevel extends AbstractMultiDimLevel {
	
	private List<Instruction> sizes = new ArrayList<>(); //outermost dimension First
	
	private ArrayLevel() {}
	
	public static ArrayLevel constructFrom(Type type, TypeAnalyzer typeAnalyzer) {
		Type skipAliases = typeAnalyzer.skipAliases(type);
		if(!checkType(skipAliases))
			return null;
		
		ArrayLevel level = new ArrayLevel();

		while (checkType(skipAliases)) {
			type = skipAliases;
			
			level.nDims++;
			
			Instruction sizeExpr = ((ArrayType)type).getSizeExpr();
			if(sizeExpr != null)
				level.sizes.add(sizeExpr.copy());
			else 
				level.sizes.add(null);
			
			type = ((ArrayType)type).getBase();
			skipAliases = typeAnalyzer.skipAliases(type);
		}
		
		level.remainingType = type;
		return level;
	}
	
	private static boolean checkType(Type type) {
		return (type instanceof ArrayType) && !(type instanceof BaseType);
	}
	
	@Override
	public Type revertOn(Type type) {
		for(int i = nDims-1; i >= 0; i--)
			type = GecosUserTypeFactory.ARRAY(type, sizes.get(i));
		
		return type;
	}
	
	/**
	 * @return list of Copies of array size instructions in outermost-dimension-first order.
	 * (e.g.: getSizes(int[5][3][1]) = [5, 3, 1])
	 */
	public List<Instruction> getSizes() {
		return sizes;
	}
	
	/**
	 * @param dim array dimension (in outermost dimension first order)
	 * @return the size of dimension {@code dim}
	 * @throws InvalidParameterException when {@code dim} is out of range
	 */
	public Instruction getSize(int dim) {
		if(dim < 0 || dim >= sizes.size())
			throw new InvalidParameterException("dim (=" + dim + ") is out of range! (max=" + (sizes.size()-1) + ")");
		
		return sizes.get(dim);
	}
	
	/**
	 * @param dim array dimension (in outermost dimension first order)
	 * @return the size of dimension {@code dim} if it's given explicitly (as IntInstruction), null otherwise.
	 * @throws InvalidParameterException when {@code dim} is out of range
	 */
	public Long tryGetSize(int dim) {
		Instruction size = getSize(dim);
		if(size instanceof IntInstruction)
			return ((IntInstruction)size).getValue();
		
		return null;
	}
	
	@Override
	public String toString() {
		return "Array " + nDims + " dimensions " + sizes;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ArrayLevel))
			return false;
		
		ArrayLevel other = (ArrayLevel)obj;
		if(other.getNDims() != this.getNDims())
			return false;
		
		for(int i = 0; i < this.getNDims(); ++i)
			if(!other.getSize(i).isSame(this.getSize(i)))
				return false;

		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + nDims;
		result = prime * result + ((sizes == null) ? 0 : sizes.hashCode());
		return result;
	}

	@Override
	public boolean isSimilar(TypeLevel obj) {
		return this.equals(obj);
	}
	
}
