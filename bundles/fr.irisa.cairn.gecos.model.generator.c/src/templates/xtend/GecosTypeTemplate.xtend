package templates.xtend

import fr.irisa.cairn.gecos.model.c.generator.ExtendableInstructionCGenerator
import fr.irisa.cairn.gecos.model.c.generator.ExtendableTypeCGenerator
import gecos.types.ACChannelType
import gecos.types.ACComplexType
import gecos.types.ACFixedType
import gecos.types.ACFloatType
import gecos.types.ACIntType
import gecos.types.AliasType
import gecos.types.ArrayType
import gecos.types.BoolType
import gecos.types.EnumType
import gecos.types.Enumerator
import gecos.types.Field
import gecos.types.FloatType
import gecos.types.FunctionType
import gecos.types.IntegerType
import gecos.types.Kinds
import gecos.types.OverflowMode
import gecos.types.PtrType
import gecos.types.QuantificationMode
import gecos.types.RecordType
import gecos.types.SignModifiers
import gecos.types.Type
import gecos.types.VoidType
import org.eclipse.emf.ecore.EObject

class GecosTypeTemplate extends GecosTemplate {
	
	public static boolean ADD_ALIGN_ATTR_TO_ARRAYS = false; //XXX hack
	//static public boolean UseSCFixedDataType = false;

	@Deprecated
	static public boolean UseSCFixedDataType = false;
	
	static enum BitLevelDataType {SystemC, MentorAC, VivadoAP}
	static enum CustomFloatDataType {AC_FLOAT, CT_FLOAT}
	
	static public BitLevelDataType bitLevelDataType = BitLevelDataType.MentorAC;
	static public CustomFloatDataType customFloatDataType = CustomFloatDataType.AC_FLOAT;
	
	
	def dispatch generate(ACChannelType acChannelType) {
		'''/* ACChannelType not yet implemented !!!*/«acChannelType.toString»'''
	}
	
	def dispatch generate(ACComplexType acComplexType) {
		'''/* ACComplexType not yet implemented !!!*/«acComplexType.toString»'''
	}
	
	def dispatch generate(ACFixedType acFixedType) {
		val buffer = new StringBuffer()
		buffer.append(generateTypeSpecifiers(acFixedType));
		
		//FIXME : should be removed by Nicolas
		if (GecosTypeTemplate.UseSCFixedDataType) {
			bitLevelDataType=BitLevelDataType.SystemC;
		}
		
		switch(bitLevelDataType) {
			case SystemC :{
				if(acFixedType.isSigned){
					buffer.append('''sc_fixed<«acFixedType.bitwidth», «acFixedType.integer»''')
				} else{
					buffer.append('''sc_ufixed<«acFixedType.bitwidth», «acFixedType.integer»''')
				}
				if (acFixedType.quantificationMode == QuantificationMode::AC_TRN){
					buffer.append(''', SC_TRN''')					
				} else {
					buffer.append(''', SC_RND''')			
				}
				buffer.append(''', SC_SAT>''')	
			} 
			case VivadoAP: {
				buffer.append('''ap_fixed<«acFixedType.bitwidth», «acFixedType.integer», «if (acFixedType.isSigned) 1 else 0»''')
				if (acFixedType.quantificationMode == QuantificationMode::AC_TRN && acFixedType.overflowMode == OverflowMode::AC_SAT)
					buffer.append('''>''')
				else
					buffer.append(''', «acFixedType.quantificationMode», «acFixedType.overflowMode»>''')	
				
			}
			case MentorAC: {
				buffer.append('''ac_fixed<«acFixedType.bitwidth», «acFixedType.integer», «if (acFixedType.isSigned) 1 else 0»''')
				if (acFixedType.quantificationMode == QuantificationMode::AC_TRN && acFixedType.overflowMode == OverflowMode::AC_SAT)
					buffer.append('''>''')
				else
					buffer.append(''', «acFixedType.quantificationMode», «acFixedType.overflowMode»>''')	
				
			}
			default:
				buffer.append('''/* Unknown data type !!!*/''')
		}
		'''«buffer.toString»'''
	}
	
	def dispatch generate(ACFloatType acFloatType) {
		val buffer = new StringBuffer
			buffer.append(generateTypeSpecifiers(acFloatType));
	
		switch(customFloatDataType) {
			case AC_FLOAT: {
				buffer.append('''ac_float<«acFloatType.radix + acFloatType.mantissa», «acFloatType.radix», «acFloatType.exponent»>''')
				'''«buffer.toString»'''
			}
			case CT_FLOAT :{
				buffer.append('''ct_float<«acFloatType.exponent», «acFloatType.mantissa», CT_RD>''') //TODO add support for specifying rounding mode
			}
			default:
				buffer.append('''/* Unknown data type !!!*/''')
		}
	}
	  
	def dispatch generate(ACIntType acIntType) {
		val buffer = new StringBuffer
		buffer.append(generateTypeSpecifiers(acIntType));
		switch(bitLevelDataType) {
			case SystemC :{
				buffer.append('''sc_int<«acIntType.bitwidth», «if (acIntType.isSigned) 1 else 0»>''')
			}
			case VivadoAP : {
				if (acIntType.isSigned) {
					buffer.append('''ap_int<«acIntType.bitwidth»>''')
				} else {
					buffer.append('''ap_uint<«acIntType.bitwidth»>''')
				}
			}
			case MentorAC :{
				buffer.append('''ac_int<«acIntType.bitwidth», «if (acIntType.isSigned) 1 else 0»>''')
			}
			default:
				buffer.append('''/* Unknown data type !!!*/''')
		}
		
		'''«buffer»'''
	}
	
	def dispatch generate(AliasType aliasType) {
		val buffer = new StringBuffer
		buffer.append(generateTypeSpecifiers(aliasType));
		buffer.append('''«aliasType.name»''')
		'''«buffer»'''
	}

	def dispatch generate(IntegerType t) {
		val buffer = new StringBuffer()
		
		buffer.append(generateTypeSpecifiers(t));
		
		if(t.signModifier == SignModifiers.SIGNED)
			buffer.append("signed ")
		else if(t.signModifier == SignModifiers.UNSIGNED)
			buffer.append("unsigned ")
			
		switch(t.type) {
			case CHAR: buffer.append("char")
			case SHORT: buffer.append("short")
			case INT: buffer.append("int")
			case LONG: buffer.append("long")
			case LONG_LONG: buffer.append("long long")
			default: throw new RuntimeException("Unsupported IntegerType:" + t.type)
		}
		
		'''«buffer.toString»'''
	}
	
	def dispatch generate(FloatType t) {
		val buffer = new StringBuffer
		buffer.append(generateTypeSpecifiers(t));
		
		switch(t.precision) {
			case SINGLE: buffer.append("float")
			case DOUBLE: buffer.append("double")
			case LONG_DOUBLE: buffer.append("long double")
			case HALF: buffer.append("/* cannot generate HALF floating-point type! */ float")
			default: throw new RuntimeException("Unsupported FloatType:" + t.precision)
		}
		'''«buffer»'''
	}
	
	def dispatch generate(BoolType t) {
		val buffer = new StringBuffer
		buffer.append(generateTypeSpecifiers(t));
		buffer.append('''_Bool''') //XXX if >=c99 else int
		'''«buffer»'''
	}
	
	def dispatch generate(VoidType t) {
		val buffer = new StringBuffer
		buffer.append(generateTypeSpecifiers(t));
		buffer.append('''void''')
		'''«buffer»'''
	}
	
	def dispatch generate(Enumerator enumerator) {
		'''«enumerator.name»'''
	}
	
	def dispatch generate(EnumType enumType) {
		val buffer = new StringBuffer
		buffer.append(generateTypeSpecifiers(enumType));
		buffer.append('''enum «enumType.name»''')
		'''«buffer»'''
	}
	
	def dispatch generate(Field field) {
		'''«field.name»'''
	}
	
	def dispatch generate(FunctionType functionType) {
		val StringBuffer buffer = new StringBuffer
		buffer.append(afterSymbolInformation)
		buffer.append("(")
		
		var first = true
		for (Type s : functionType.listParameters) {
			if (first) first = false
			else buffer.append(''', ''')
				
			afterSymbolInformation.setLength(0)
			buffer.append('''«ExtendableTypeCGenerator::eInstance.generate(s)»''')
			buffer.append(afterSymbolInformation)
		}
		
		if (functionType.hasElipsis) {
			if (first) first = false
			else buffer.append(''', ''')
			// ISO C requires a named argument before elipsis
			buffer.append("...")
		}
		buffer.append(")")
		afterSymbolInformation.setLength(0)
		afterSymbolInformation.append(buffer)
		'''«ExtendableTypeCGenerator::eInstance.generate(functionType.returnType)»'''
	}
	
	def dispatch generate(PtrType ptrType) {
		var StringBuffer buf = new StringBuffer()
		
		var needParenthesis = (ptrType.base instanceof ArrayType || ptrType.base instanceof FunctionType)
		
		if(needParenthesis) afterSymbolInformation.append(")")
		
		buf.append('''«ExtendableTypeCGenerator::eInstance.generate(ptrType.base)»''')
		
		if(needParenthesis) buf.append(''' (*''')
		else buf.append(''' *''')
		
		if(ptrType.isRestrict) buf.append("restrict ")
		buf.append(generateTypeSpecifiers(ptrType));
		
		if(Character.isWhitespace(buf.charAt(buf.length -1)))
			buf.deleteCharAt(buf.length -1)
		
		'''«buf.toString»'''
	}
	
	def dispatch generate(ArrayType arrayType) {
		val buffer = new StringBuffer()
		
		if (arrayType.sizeExpr === null)
			afterSymbolInformation.append("[]")
		else {
			val value = ExtendableInstructionCGenerator::eInstance.generate(arrayType.sizeExpr)
			afterSymbolInformation.append("[" + value + "]")
		}
		
		buffer.append(ExtendableTypeCGenerator::eInstance.generate(arrayType.base))
		
		if(ADD_ALIGN_ATTR_TO_ARRAYS && !(arrayType.base instanceof ArrayType)) //XXX
			buffer.append(" __ALIGN_ATT__");
			
		'''«buffer»'''
	}
	
	def dispatch generate(RecordType recordType) {
		var StringBuffer buffer = new StringBuffer()
		buffer.append(generateTypeSpecifiers(recordType));
		
		if (recordType.kind == Kinds.STRUCT) {
			buffer.append('''struct «recordType.name»''')
		} else if (recordType.kind == Kinds.UNION) {
			buffer.append('''union «recordType.name»''')
		} else {
			buffer.append('''/* Unknown record type kind : «recordType.kind» */''')
		}
		'''«buffer»'''
	}
	
	/**
	 * order: [Storage_class] [const] [volatile]
	 * Not strictly important but used for testing.
	 */
	def generateTypeSpecifiers(Type type) {
		val StringBuffer buffer = new StringBuffer
		switch(type.storageClass) {
			case AUTO:     { buffer.append('''auto ''') }
			case EXTERN:   { buffer.append('''extern ''') }
			case REGISTER: { buffer.append('''register ''') }
			case STATIC:   { buffer.append('''static ''') }
			case NONE: {}
		}
		if(type.isConstant)
			buffer.append('''const ''')
		if(type.isVolatile)
			buffer.append('''volatile ''')
			
		'''«buffer»'''
	}

	def dispatch generate(EObject object) {
		null
	}
}