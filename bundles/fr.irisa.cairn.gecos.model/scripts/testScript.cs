debug(1);
echo("ici");
 
dir="src/";

projectname = "projetAutomatisation";
cFile = "sha.c";

echo("	- etape 0 : creation du Gecos projet");

#initialize project
project = CreateGecosProject(projectname);

#add files to the projects (sourdes and includes)
AddSourceToGecosProject(project,"./src/");
AddIncDirToGecosProject(project,"./include/");

#build project
GecosProjectBuilder(project);
	
#get procedure set from source file name
modelps = FindProcedureSet(project,cFile);
echo("done project");

SaveModelAsXML(modelps, cFile +".gecos");
#ModelDottyExport("fir3", modelps);

ModelDAGBuilder(modelps);       
SaveModelAsXML(modelps, "test_chen.dag.debug.gecos");
ModelDAGDottyExport(modelps, "testDotDagps");