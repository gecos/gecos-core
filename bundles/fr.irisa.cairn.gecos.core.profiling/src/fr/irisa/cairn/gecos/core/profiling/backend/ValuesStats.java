package fr.irisa.cairn.gecos.core.profiling.backend;

import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.stream.Stream;


/**
 * Compute statistical characteristics of Number values.
 * The values are converted to {@link Double} !
 * 
 * <p> The computation is done only once and cached for subsequent getter calls.
 * 
 * <pre>
 * Mean = sum(val)/N
 * 
 * Variance = sum((val - mean)^2) / N 
 *          = sum(val^2)/N + sum(mean^2)/N -2.mean.sum(val)/N
 *          = sumSquared/N + mean^2 -2.mean^2 
 *          = sumSquared/N - mean^2
 *          
 * Deviation = sqrt(Variance)
 * 
 * Energy = sumSquared = sum(val^2)
 * 
 * Power = Energy / N
 *       = sumSquared / N
 *       = deviation^2 + mean^2
 *       = variance + mean^2
 * 
 * Power(dB) = 10.log10(Power)
 * 
 * RMS = sqrt(Power)
 * </pre>
 * 
 * @author aelmouss
 */
public class ValuesStats {

	// Essentials
	protected double nbElements;
	protected double min;
	protected double max;
	protected double sum;
	protected double sumSquared;
	
	// Complex i.e. computed from Essentials
	protected double mean;
	protected double variance;
	protected double deviation;
	protected double power;
	protected double powerdB;
	protected double rms;
	protected double energy;
	
	
	public static ValuesStats of(double[] values) {
		return new ValuesStats().compute(values);
	}
	
	public static ValuesStats empty() {
		return of(new double[] {});
	}
	
	public static <T extends Number> ValuesStats of(Stream<T> values) {
		return of(values.mapToDouble(v -> Double.valueOf(v.toString())).toArray());
	}
	
	public static <T extends Number> ValuesStats of(T[] values) {
		return of(Arrays.stream(values));
	}

	public static <T extends Number> ValuesStats ofDiff(T[] first, T[] second) {
		return of(diffValues(first, second));
	}
	
	public static <T extends Number> double[] diffValues(T[] first, T[] second) {
		if(first.length != second.length)
			throw new InvalidParameterException("Number of samples differs! first has " 
					+ first.length + " and second has " + second.length);
		
		double[] diffValues = new double[second.length];
		for (int i = 0; i < second.length ; i++) {
			diffValues[i] = first[i].doubleValue() - second[i].doubleValue();
		}
		return diffValues;
	}
	
	protected ValuesStats compute(double[] values) {
		computeEssentials(values);
		updateComplex();
		return this;
	}

	protected void computeEssentials(double[] values) {
		nbElements = values.length;
		min = Double.MAX_VALUE;
		max = Double.MIN_VALUE;
		sum = 0;
		sumSquared = 0;
		for (double val : values) {
			min = Math.min(val, min);
			max = Math.max(val, max);
			if (val != 0) {
				sum += val;
				sumSquared += val * val;
			}
		}
	}
	
	//TODO handle corner cases (divide by 0, NaN ...)
	protected void updateComplex() {
		mean = sum / nbElements;
		energy = sumSquared;
		power = sumSquared/nbElements;
		variance = power - (mean * mean);
		
		deviation = Math.sqrt(variance);		
		powerdB = 10 * Math.log10(power);
		rms = Math.sqrt(power);
	}

	/**
	 * Update {@code this} depending on <code>takeMean</code>: <ul>
	 * <li> add in nbElements, sum and sumSquared from {@code other} if takeMean is false,
	 * otherwise, take their mean values between <code>this</code> an <code>other</code>.
	 * <li> update min/max as the min/max between <code>this</code> an <code>other</code>.
	 * <li> and then recomputing other attributes.
	 * </ul>
	 * @param other stats
	 * @param takeMean if true take the MEAN between <code>this</code> and <code>other</code>'s nbElements, sum and sumSquared.
	 * Otherwise, take the SUM of <code>this</code> and <code>other</code>'s nbElements, sum and sumSquared.
	 * Take the min/max between <code>this</code> an <code>other</code> regardless of the value of <code>takeMean</code>.
	 * @return this
	 */
	public ValuesStats merge(ValuesStats other, boolean takeMean) {
		mergeEssentials(other, takeMean);
		updateComplex();
		return this;
	}

	protected void mergeEssentials(ValuesStats other, boolean takeMean) {
		nbElements += other.nbElements;
		sumSquared += other.sumSquared;
		sum += other.sum;
		
		if(takeMean) {
			nbElements /= 2.;
			sumSquared /= 2.;
			sum /= 2.;
		}
		
		min = Math.min(min, other.min);
		max = Math.max(max, other.max);
	}
	
	public double getNbElements() {
		return nbElements;
	}
	
	public double getMin() {
		return min;
	}
	
	public double getMax() {
		return max;
	}
	
	public double getMean() {
		return mean;
	}

	public double getVariance() {
		return variance;
	}

	public double getDeviation() {
		return deviation;
	}
	
	public double getEnergy() {
		return energy;
	}
	
	public double getPower() {
		return power;
	}

	public double getPowerdB() {
		return powerdB;
	}

	public double getAbsoluteMax() {
		return Math.max(Math.abs(max), Math.abs(min));
	}
	
	public double getRms() {
		return rms;
	}
	
	public String generateReport() {
		StringBuilder sb = new StringBuilder();
		sb.append("\tNumber of samples  = ").append(getNbElements()  ).append("\n");
		sb.append("\tMean     = ").append(getMean())
			.append(", Min = ").append(getMin())
			.append(", Max = ").append(getMax())
			.append(", AbsMax = ").append(getAbsoluteMax()).append("\n");
		sb.append("\tVariance = ").append(getVariance())
			.append(", Deviation = ").append(getDeviation()).append("\n");
		sb.append("\tPower    = ").append(getPower()).append(" = ").append(getPowerdB()).append(" dB\n");
		
		return sb.toString();
	}
	
}