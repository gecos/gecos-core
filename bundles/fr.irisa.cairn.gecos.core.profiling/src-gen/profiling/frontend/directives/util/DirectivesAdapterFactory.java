/**
 */
package profiling.frontend.directives.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import profiling.frontend.directives.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see profiling.frontend.directives.DirectivesPackage
 * @generated
 */
public class DirectivesAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DirectivesPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DirectivesAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = DirectivesPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DirectivesSwitch<Adapter> modelSwitch =
		new DirectivesSwitch<Adapter>() {
			@Override
			public Adapter caseDirective(Directive object) {
				return createDirectiveAdapter();
			}
			@Override
			public Adapter caseSizeDirective(SizeDirective object) {
				return createSizeDirectiveAdapter();
			}
			@Override
			public Adapter caseSaveDirective(SaveDirective object) {
				return createSaveDirectiveAdapter();
			}
			@Override
			public Adapter caseInjectDirective(InjectDirective object) {
				return createInjectDirectiveAdapter();
			}
			@Override
			public Adapter caseInjectorSourceDirective(InjectorSourceDirective object) {
				return createInjectorSourceDirectiveAdapter();
			}
			@Override
			public Adapter caseFromFileDirective(FromFileDirective object) {
				return createFromFileDirectiveAdapter();
			}
			@Override
			public Adapter caseFromVariableDirective(FromVariableDirective object) {
				return createFromVariableDirectiveAdapter();
			}
			@Override
			public Adapter caseRandomUniformDirective(RandomUniformDirective object) {
				return createRandomUniformDirectiveAdapter();
			}
			@Override
			public Adapter caseRandomNormalDirective(RandomNormalDirective object) {
				return createRandomNormalDirectiveAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link profiling.frontend.directives.Directive <em>Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see profiling.frontend.directives.Directive
	 * @generated
	 */
	public Adapter createDirectiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link profiling.frontend.directives.SizeDirective <em>Size Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see profiling.frontend.directives.SizeDirective
	 * @generated
	 */
	public Adapter createSizeDirectiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link profiling.frontend.directives.SaveDirective <em>Save Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see profiling.frontend.directives.SaveDirective
	 * @generated
	 */
	public Adapter createSaveDirectiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link profiling.frontend.directives.InjectDirective <em>Inject Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see profiling.frontend.directives.InjectDirective
	 * @generated
	 */
	public Adapter createInjectDirectiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link profiling.frontend.directives.InjectorSourceDirective <em>Injector Source Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see profiling.frontend.directives.InjectorSourceDirective
	 * @generated
	 */
	public Adapter createInjectorSourceDirectiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link profiling.frontend.directives.FromFileDirective <em>From File Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see profiling.frontend.directives.FromFileDirective
	 * @generated
	 */
	public Adapter createFromFileDirectiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link profiling.frontend.directives.FromVariableDirective <em>From Variable Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see profiling.frontend.directives.FromVariableDirective
	 * @generated
	 */
	public Adapter createFromVariableDirectiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link profiling.frontend.directives.RandomUniformDirective <em>Random Uniform Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see profiling.frontend.directives.RandomUniformDirective
	 * @generated
	 */
	public Adapter createRandomUniformDirectiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link profiling.frontend.directives.RandomNormalDirective <em>Random Normal Directive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see profiling.frontend.directives.RandomNormalDirective
	 * @generated
	 */
	public Adapter createRandomNormalDirectiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //DirectivesAdapterFactory
