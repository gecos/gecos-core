package fr.irisa.cairn.gecos.core.profiling;

import static java.util.stream.Collectors.toList;

import java.io.ByteArrayOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Iterables;

import fr.irisa.cairn.gecos.core.profiling.backend.MainProcedureCreator;
import fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport;
import fr.irisa.cairn.gecos.core.profiling.backend.ProfilingBackendFailure;
import fr.irisa.cairn.gecos.core.profiling.backend.ProfilingInfo;
import fr.irisa.cairn.gecos.core.profiling.frontend.DirectivesProcessor;
import fr.irisa.cairn.gecos.model.c.generator.XtendCGenerator;
import fr.irisa.cairn.gecos.model.utils.ProjectUtils;
import fr.irisa.cairn.gecos.model.utils.misc.LoggingUtils;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;

/**
 * @author aelmouss
 */
public class SimulationBasedProfiler {

	private GecosProject project;
	private boolean verbose;
	private boolean keepProfilingOutputs;
	
	private Map<Integer, Symbol> instrumentedSymbols;
	private List<Path> generatedProfiledSourceFiles;
	private List<Path> projectIncludeDirs;
	
	private Logger logger;
	private GecosProject projectCopyWithNoDirectives;

	public SimulationBasedProfiler(GecosProject project, boolean verbose, boolean keepProfilingOutputs, Logger logger) {
		this.project = project;
		this.verbose = verbose;
		this.logger = logger;
		this.keepProfilingOutputs = keepProfilingOutputs;
		if(logger == null)
			this.logger = LoggingUtils.useStdout(Logger.getAnonymousLogger(), Level.INFO);
	}
	
	public SimulationBasedProfiler(GecosProject project, boolean verbose, boolean keepProfilingOutputs) {
		this(project, verbose, keepProfilingOutputs, null);
	}
	
	public SimulationBasedProfiler(GecosProject project, boolean verbose) {
		this(project, verbose, false, null);
	}
	
	public SimulationBasedProfiler(GecosProject project) {
		this(project, false, false, null);
	}

	public GecosProject insertProfiling() {
		return insertProfiling(1);
	}
	
	/**
	 * Insert profiling instructions in the specified project by inspecting the $ directives.
	 * @param nbSimulation nb of times the target function will be called in the main (loop).
	 * @return a copy of the original project in which $ directives have been removed 
	 */
	public GecosProject insertProfiling(int nbSimulation) {
		logger.info("                - Instrument code");
		Stopwatch w = Stopwatch.createStarted();

		/** add/replace main **/
		new MainProcedureCreator(project).generate();
		
		DirectivesProcessor instrumentationProcessor = new DirectivesProcessor(project);
		this.projectCopyWithNoDirectives = instrumentationProcessor.compute();
		this.instrumentedSymbols = instrumentationProcessor.getSavedSymbols();

		
		logger.info("                - Code instumentation finished in " + w);
		return projectCopyWithNoDirectives;
	}
	
	/**
	 * @return a copy of the original project in which $ directives have been removed 
	 */
	public GecosProject getProjectCopyWithNoDirectives() {
		return projectCopyWithNoDirectives;
	}
	
	public void generateProfiledCode(Path codegenDir) {
		new XtendCGenerator(project, codegenDir.toString()).compute();
		this.generatedProfiledSourceFiles = ProjectUtils.getProjectSourcesRelativeTo(project, codegenDir);
		
		// original project include dirs (&& macros ??)
		this.projectIncludeDirs = project.getIncludes().stream()
				.filter(d -> !d.isIsStandardHeader())
				.map(d -> d.getName()).map(Paths::get)
				.collect(toList());
	}
	
	/**
	 * @throws ProfilingBackendFailure if compilation, execution or extraction fails
	 */
	public Map<Symbol, ProfilingInfo<? extends Number>> profile(Path outDir, List<Path> incDirs,
			List<Path> libDirs, List<String> libs, List<String> headers, Map<String, String> macros, int nbSimus, int initSeedValue, String ... arguments)
					throws ProfilingBackendFailure {
		logger.fine("                - Compiling instrumented code");
		Stopwatch w = Stopwatch.createStarted();
		compile(outDir, incDirs, headers, libDirs, libs, macros);
		logger.fine("                - Compilation finished in " + w);
		logger.fine("                - Executing instrumented code");

		String[] args = new String[arguments.length+2];
		System.arraycopy(arguments, 0, args, 2, arguments.length);
		args[0] = Integer.toString(nbSimus);
		args[1] = Integer.toString(initSeedValue);

		execute(outDir, args);
		logger.fine("                - Execution finished in " + w);
		logger.fine("                - Collecting profiled values");
		w = Stopwatch.createStarted();
		Map<Symbol, ProfilingInfo<? extends Number>> profiledSymbols = retrieveProfilingInfo(outDir);
		
		if (!keepProfilingOutputs)
			 NativeProfilingEngineSupport.removeProfilingOutputs(outDir);
		if (outDir.toFile().list().length == 0) {
			outDir.toFile().delete();
		}
		
		logger.fine("                - Collecting finished in " + w);
		return profiledSymbols;
	}
	

	public Map<Symbol, ProfilingInfo<? extends Number>> profile(Path outDir, List<Path> incDirs,
			List<Path> libDirs, List<String> libs, List<String> headers, Map<String, String> macros, String ... arguments)
					throws ProfilingBackendFailure {
		return profile(outDir, incDirs, libDirs, libs, headers, macros, 1, -1, arguments);
	}

	/**
	 * @param buildDir directory where the executable will be generated at
	 * @param incDirs can be {@code null}.
	 * @param headers can be {@code null}.
	 * @param libDirs can be {@code null}.
	 * @param libs can be {@code null}.
	 * @param macros can be {@code null}.
	 * @throws ProfilingBackendFailure if compilation fails.
	 */
	protected void compile(Path buildDir, List<Path> incDirs, List<String> headers, List<Path> libDirs, 
			List<String> libs, Map<String, String> macros) throws ProfilingBackendFailure {
		
		ByteArrayOutputStream stdout = new ByteArrayOutputStream();
		ByteArrayOutputStream stderr = new ByteArrayOutputStream();
		
		try {
			Iterable<Path> allIncDirs = incDirs == null ? projectIncludeDirs : Iterables.concat(projectIncludeDirs, incDirs);
			NativeProfilingEngineSupport.compile(generatedProfiledSourceFiles, buildDir, 
					allIncDirs, headers, libDirs, libs, macros, stdout, stderr, logger);
		} catch (ProfilingBackendFailure e) {
			throw e;
		} finally {
			if(stdout.size() > 0)
				logger.fine(() -> stdout.toString());
			if(stderr.size() > 0)
				logger.severe(() -> stderr.toString());
		}
	}

	/**
	 * @throws ProfilingBackendFailure if execution fails
	 */
	protected void execute(Path executableDir, String ... arguments) throws ProfilingBackendFailure {
		ByteArrayOutputStream stdout = new ByteArrayOutputStream();
		ByteArrayOutputStream stderr = new ByteArrayOutputStream();
		
		try {
			NativeProfilingEngineSupport.execute(executableDir, stdout, stderr, logger, arguments);
		} catch (ProfilingBackendFailure e) {
			throw e;
		} finally {
			if(stdout.size() > 0)
				logger.fine(() -> stdout.toString());
			if(stderr.size() > 0)
				logger.severe(() -> stderr.toString());
		}
	}
	
	protected Map<Symbol, ProfilingInfo<? extends Number>> retrieveProfilingInfo(Path executableDir) throws ProfilingBackendFailure {
		Map<Symbol, ProfilingInfo<? extends Number>> profiledSymbols = NativeProfilingEngineSupport.retrieveProfilingInfo(executableDir, instrumentedSymbols);

		if(verbose) {
			profiledSymbols.entrySet().stream()
				.map(e -> "=======    Profiling Info for Symbol '" +  e.getKey() + "'   =======\n"
						+ e.getValue().printHeader()
						+ e.getValue().printValues())
				.forEach(logger::info);
		}
		
		return profiledSymbols;
	}
		
}
