/**
 */
package gecos.dag.impl;

import gecos.dag.DAGVectorShuffleNode;
import gecos.dag.DagPackage;
import gecos.dag.DagVisitor;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EDataTypeEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DAG Vector Shuffle Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.impl.DAGVectorShuffleNodeImpl#getPermutation <em>Permutation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DAGVectorShuffleNodeImpl extends DAGVectorNodeImpl implements DAGVectorShuffleNode {
	/**
	 * The cached value of the '{@link #getPermutation() <em>Permutation</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPermutation()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> permutation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DAGVectorShuffleNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DagPackage.Literals.DAG_VECTOR_SHUFFLE_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getPermutation() {
		if (permutation == null) {
			permutation = new EDataTypeEList<Integer>(Integer.class, this, DagPackage.DAG_VECTOR_SHUFFLE_NODE__PERMUTATION);
		}
		return permutation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final DagVisitor visitor) {
		visitor.visitDAGVectorShuffleNode(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DagPackage.DAG_VECTOR_SHUFFLE_NODE__PERMUTATION:
				return getPermutation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DagPackage.DAG_VECTOR_SHUFFLE_NODE__PERMUTATION:
				getPermutation().clear();
				getPermutation().addAll((Collection<? extends Integer>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DagPackage.DAG_VECTOR_SHUFFLE_NODE__PERMUTATION:
				getPermutation().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DagPackage.DAG_VECTOR_SHUFFLE_NODE__PERMUTATION:
				return permutation != null && !permutation.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (permutation: ");
		result.append(permutation);
		result.append(')');
		return result.toString();
	}

} //DAGVectorShuffleNodeImpl
