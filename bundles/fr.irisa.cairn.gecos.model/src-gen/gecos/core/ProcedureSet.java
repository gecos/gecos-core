/**
 */
package gecos.core;

import gecos.annotations.AnnotatedElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Procedure Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.core.ProcedureSet#getUses <em>Uses</em>}</li>
 * </ul>
 *
 * @see gecos.core.CorePackage#getProcedureSet()
 * @model annotation="gmf.diagram foo='bar'"
 * @generated
 */
public interface ProcedureSet extends ScopeContainer, AnnotatedElement, CoreVisitable {
	/**
	 * Returns the value of the '<em><b>Uses</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.core.ISymbolUse}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uses</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uses</em>' containment reference list.
	 * @see gecos.core.CorePackage#getProcedureSet_Uses()
	 * @model containment="true" changeable="false"
	 * @generated
	 */
	EList<ISymbolUse> getUses();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" nameDataType="gecos.core.String" nameUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.core.Procedure%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.core.Procedure%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%gecos.core.Procedure%&gt; p)\n\t{\n\t\t&lt;%java.lang.String%&gt; _name = p.getSymbol().getName();\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf(&lt;%com.google.common.base.Objects%&gt;.equal(_name, name));\n\t}\n};\nreturn &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.core.Procedure%&gt;&gt;findFirst(this.listProcedures(), _function);'"
	 * @generated
	 */
	Procedure findProcedure(String name);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return an unmodifiable view to the list of (non-null) {@link Procedure}s
	 * <!-- end-model-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.core.ProcedureSymbol%&gt;, &lt;%gecos.core.Procedure%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.core.ProcedureSymbol%&gt;, &lt;%gecos.core.Procedure%&gt;&gt;()\n{\n\tpublic &lt;%gecos.core.Procedure%&gt; apply(final &lt;%gecos.core.ProcedureSymbol%&gt; ps)\n\t{\n\t\treturn ps.getProcedure();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.core.Procedure%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.core.Procedure%&gt;&gt;asEList(((&lt;%gecos.core.Procedure%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.core.Procedure%&gt;&gt;filterNull(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.core.ProcedureSymbol%&gt;, &lt;%gecos.core.Procedure%&gt;&gt;map(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%gecos.core.ProcedureSymbol%&gt;&gt;filter(this.getScope().getSymbols(), &lt;%gecos.core.ProcedureSymbol%&gt;.class), _function)), &lt;%gecos.core.Procedure%&gt;.class))));'"
	 * @generated
	 */
	EList<Procedure> listProcedures();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model procUnique="false" procRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.core.ProcedureSymbol%&gt; symbol = proc.getSymbol();\nif ((symbol == null))\n{\n\tthrow new &lt;%java.lang.IllegalArgumentException%&gt;(\"Cannot add a procedure without symbol.\");\n}\nthis.addSymbol(symbol);'"
	 * @generated
	 */
	void addProcedure(Procedure proc);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model procsMany="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.core.Procedure%&gt;&gt; _function = new &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.core.Procedure%&gt;&gt;()\n{\n\tpublic void accept(final &lt;%gecos.core.Procedure%&gt; it)\n\t{\n\t\t&lt;%this%&gt;.addProcedure(it);\n\t}\n};\nprocs.forEach(_function);'"
	 * @generated
	 */
	void addAllProcedures(EList<Procedure> procs);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model procUnique="false" procRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.core.ProcedureSymbol%&gt; symbol = proc.getSymbol();\nif ((symbol == null))\n{\n\tthrow new &lt;%java.lang.IllegalArgumentException%&gt;(\"Cannot remove a procedure without symbol.\");\n}\nthis.removeSymbol(symbol);'"
	 * @generated
	 */
	void removeProcedure(Procedure proc);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.core.Procedure%&gt;, &lt;%gecos.core.ProcedureSymbol%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.core.Procedure%&gt;, &lt;%gecos.core.ProcedureSymbol%&gt;&gt;()\n{\n\tpublic &lt;%gecos.core.ProcedureSymbol%&gt; apply(final &lt;%gecos.core.Procedure%&gt; it)\n\t{\n\t\treturn it.getSymbol();\n\t}\n};\nfinal &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.core.ProcedureSymbol%&gt;&gt; _function_1 = new &lt;%java.util.function.Consumer%&gt;&lt;&lt;%gecos.core.ProcedureSymbol%&gt;&gt;()\n{\n\tpublic void accept(final &lt;%gecos.core.ProcedureSymbol%&gt; it)\n\t{\n\t\t&lt;%this%&gt;.removeSymbol(it);\n\t}\n};\n&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%gecos.core.Procedure%&gt;, &lt;%gecos.core.ProcedureSymbol%&gt;&gt;map(this.listProcedures(), _function).forEach(_function_1);'"
	 * @generated
	 */
	void removeAllProcedures();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.core.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return \"ProcedureSet\";'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitProcedureSet(this);'"
	 * @generated
	 */
	void accept(CoreVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%fr.irisa.cairn.gecos.model.tools.utils.GecosCopier%&gt; gecosCopier = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.GecosCopier%&gt;();\n&lt;%org.eclipse.emf.ecore.EObject%&gt; _copy = gecosCopier.copy(this);\nfinal &lt;%gecos.core.ProcedureSet%&gt; procedureSet = ((&lt;%gecos.core.ProcedureSet%&gt;) _copy);\ngecosCopier.copyReferences();\nreturn procedureSet;'"
	 * @generated
	 */
	ProcedureSet copy();

} // ProcedureSet
