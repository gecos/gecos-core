/**
 */
package gecos.dag.util;

import gecos.annotations.AnnotatedElement;

import gecos.core.GecosNode;
import gecos.core.ITypedElement;

import gecos.dag.*;

import gecos.instrs.InstrsVisitable;
import gecos.instrs.Instruction;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see gecos.dag.DagPackage
 * @generated
 */
public class DagSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DagPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DagSwitch() {
		if (modelPackage == null) {
			modelPackage = DagPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case DagPackage.DAG_VISITOR: {
				DagVisitor dagVisitor = (DagVisitor)theEObject;
				T result = caseDagVisitor(dagVisitor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_VISITABLE: {
				DagVisitable dagVisitable = (DagVisitable)theEObject;
				T result = caseDagVisitable(dagVisitable);
				if (result == null) result = caseGecosNode(dagVisitable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_INSTRUCTION: {
				DAGInstruction dagInstruction = (DAGInstruction)theEObject;
				T result = caseDAGInstruction(dagInstruction);
				if (result == null) result = caseInstruction(dagInstruction);
				if (result == null) result = caseDagVisitable(dagInstruction);
				if (result == null) result = caseAnnotatedElement(dagInstruction);
				if (result == null) result = caseInstrsVisitable(dagInstruction);
				if (result == null) result = caseITypedElement(dagInstruction);
				if (result == null) result = caseGecosNode(dagInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_NODE: {
				DAGNode dagNode = (DAGNode)theEObject;
				T result = caseDAGNode(dagNode);
				if (result == null) result = caseAnnotatedElement(dagNode);
				if (result == null) result = caseDagVisitable(dagNode);
				if (result == null) result = caseITypedElement(dagNode);
				if (result == null) result = caseGecosNode(dagNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_EDGE: {
				DAGEdge dagEdge = (DAGEdge)theEObject;
				T result = caseDAGEdge(dagEdge);
				if (result == null) result = caseDagVisitable(dagEdge);
				if (result == null) result = caseAnnotatedElement(dagEdge);
				if (result == null) result = caseGecosNode(dagEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_PORT: {
				DAGPort dagPort = (DAGPort)theEObject;
				T result = caseDAGPort(dagPort);
				if (result == null) result = caseDagVisitable(dagPort);
				if (result == null) result = caseGecosNode(dagPort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_IN_PORT: {
				DAGInPort dagInPort = (DAGInPort)theEObject;
				T result = caseDAGInPort(dagInPort);
				if (result == null) result = caseDAGPort(dagInPort);
				if (result == null) result = caseDagVisitable(dagInPort);
				if (result == null) result = caseGecosNode(dagInPort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_OUT_PORT: {
				DAGOutPort dagOutPort = (DAGOutPort)theEObject;
				T result = caseDAGOutPort(dagOutPort);
				if (result == null) result = caseDAGPort(dagOutPort);
				if (result == null) result = caseDagVisitable(dagOutPort);
				if (result == null) result = caseGecosNode(dagOutPort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_IMM_NODE: {
				DAGImmNode dagImmNode = (DAGImmNode)theEObject;
				T result = caseDAGImmNode(dagImmNode);
				if (result == null) result = caseDAGNode(dagImmNode);
				if (result == null) result = caseAnnotatedElement(dagImmNode);
				if (result == null) result = caseDagVisitable(dagImmNode);
				if (result == null) result = caseITypedElement(dagImmNode);
				if (result == null) result = caseGecosNode(dagImmNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_SYMBOL_NODE: {
				DAGSymbolNode dagSymbolNode = (DAGSymbolNode)theEObject;
				T result = caseDAGSymbolNode(dagSymbolNode);
				if (result == null) result = caseDAGNode(dagSymbolNode);
				if (result == null) result = caseAnnotatedElement(dagSymbolNode);
				if (result == null) result = caseDagVisitable(dagSymbolNode);
				if (result == null) result = caseITypedElement(dagSymbolNode);
				if (result == null) result = caseGecosNode(dagSymbolNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_CALL_NODE: {
				DAGCallNode dagCallNode = (DAGCallNode)theEObject;
				T result = caseDAGCallNode(dagCallNode);
				if (result == null) result = caseDAGNode(dagCallNode);
				if (result == null) result = caseAnnotatedElement(dagCallNode);
				if (result == null) result = caseDagVisitable(dagCallNode);
				if (result == null) result = caseITypedElement(dagCallNode);
				if (result == null) result = caseGecosNode(dagCallNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_ARRAY_VALUE_NODE: {
				DAGArrayValueNode dagArrayValueNode = (DAGArrayValueNode)theEObject;
				T result = caseDAGArrayValueNode(dagArrayValueNode);
				if (result == null) result = caseDAGNode(dagArrayValueNode);
				if (result == null) result = caseAnnotatedElement(dagArrayValueNode);
				if (result == null) result = caseDagVisitable(dagArrayValueNode);
				if (result == null) result = caseITypedElement(dagArrayValueNode);
				if (result == null) result = caseGecosNode(dagArrayValueNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_OP_NODE: {
				DAGOpNode dagOpNode = (DAGOpNode)theEObject;
				T result = caseDAGOpNode(dagOpNode);
				if (result == null) result = caseDAGNode(dagOpNode);
				if (result == null) result = caseAnnotatedElement(dagOpNode);
				if (result == null) result = caseDagVisitable(dagOpNode);
				if (result == null) result = caseITypedElement(dagOpNode);
				if (result == null) result = caseGecosNode(dagOpNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_OUT_NODE: {
				DAGOutNode dagOutNode = (DAGOutNode)theEObject;
				T result = caseDAGOutNode(dagOutNode);
				if (result == null) result = caseDAGNode(dagOutNode);
				if (result == null) result = caseAnnotatedElement(dagOutNode);
				if (result == null) result = caseDagVisitable(dagOutNode);
				if (result == null) result = caseITypedElement(dagOutNode);
				if (result == null) result = caseGecosNode(dagOutNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_CONTROL_EDGE: {
				DAGControlEdge dagControlEdge = (DAGControlEdge)theEObject;
				T result = caseDAGControlEdge(dagControlEdge);
				if (result == null) result = caseDAGEdge(dagControlEdge);
				if (result == null) result = caseDagVisitable(dagControlEdge);
				if (result == null) result = caseAnnotatedElement(dagControlEdge);
				if (result == null) result = caseGecosNode(dagControlEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_DATA_EDGE: {
				DAGDataEdge dagDataEdge = (DAGDataEdge)theEObject;
				T result = caseDAGDataEdge(dagDataEdge);
				if (result == null) result = caseDAGEdge(dagDataEdge);
				if (result == null) result = caseDagVisitable(dagDataEdge);
				if (result == null) result = caseAnnotatedElement(dagDataEdge);
				if (result == null) result = caseGecosNode(dagDataEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_NUMBERED_SYMBOL_NODE: {
				DAGNumberedSymbolNode dagNumberedSymbolNode = (DAGNumberedSymbolNode)theEObject;
				T result = caseDAGNumberedSymbolNode(dagNumberedSymbolNode);
				if (result == null) result = caseDAGSymbolNode(dagNumberedSymbolNode);
				if (result == null) result = caseDAGNode(dagNumberedSymbolNode);
				if (result == null) result = caseAnnotatedElement(dagNumberedSymbolNode);
				if (result == null) result = caseDagVisitable(dagNumberedSymbolNode);
				if (result == null) result = caseITypedElement(dagNumberedSymbolNode);
				if (result == null) result = caseGecosNode(dagNumberedSymbolNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_NUMBERED_OUT_NODE: {
				DAGNumberedOutNode dagNumberedOutNode = (DAGNumberedOutNode)theEObject;
				T result = caseDAGNumberedOutNode(dagNumberedOutNode);
				if (result == null) result = caseDAGOutNode(dagNumberedOutNode);
				if (result == null) result = caseDAGNode(dagNumberedOutNode);
				if (result == null) result = caseAnnotatedElement(dagNumberedOutNode);
				if (result == null) result = caseDagVisitable(dagNumberedOutNode);
				if (result == null) result = caseITypedElement(dagNumberedOutNode);
				if (result == null) result = caseGecosNode(dagNumberedOutNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_INT_IMM_NODE: {
				DAGIntImmNode dagIntImmNode = (DAGIntImmNode)theEObject;
				T result = caseDAGIntImmNode(dagIntImmNode);
				if (result == null) result = caseDAGImmNode(dagIntImmNode);
				if (result == null) result = caseDAGNode(dagIntImmNode);
				if (result == null) result = caseAnnotatedElement(dagIntImmNode);
				if (result == null) result = caseDagVisitable(dagIntImmNode);
				if (result == null) result = caseITypedElement(dagIntImmNode);
				if (result == null) result = caseGecosNode(dagIntImmNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_FLOAT_IMM_NODE: {
				DAGFloatImmNode dagFloatImmNode = (DAGFloatImmNode)theEObject;
				T result = caseDAGFloatImmNode(dagFloatImmNode);
				if (result == null) result = caseDAGImmNode(dagFloatImmNode);
				if (result == null) result = caseDAGNode(dagFloatImmNode);
				if (result == null) result = caseAnnotatedElement(dagFloatImmNode);
				if (result == null) result = caseDagVisitable(dagFloatImmNode);
				if (result == null) result = caseITypedElement(dagFloatImmNode);
				if (result == null) result = caseGecosNode(dagFloatImmNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_STRING_IMM_NODE: {
				DAGStringImmNode dagStringImmNode = (DAGStringImmNode)theEObject;
				T result = caseDAGStringImmNode(dagStringImmNode);
				if (result == null) result = caseDAGImmNode(dagStringImmNode);
				if (result == null) result = caseDAGNode(dagStringImmNode);
				if (result == null) result = caseAnnotatedElement(dagStringImmNode);
				if (result == null) result = caseDagVisitable(dagStringImmNode);
				if (result == null) result = caseITypedElement(dagStringImmNode);
				if (result == null) result = caseGecosNode(dagStringImmNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_ARRAY_NODE: {
				DAGArrayNode dagArrayNode = (DAGArrayNode)theEObject;
				T result = caseDAGArrayNode(dagArrayNode);
				if (result == null) result = caseDAGNode(dagArrayNode);
				if (result == null) result = caseAnnotatedElement(dagArrayNode);
				if (result == null) result = caseDagVisitable(dagArrayNode);
				if (result == null) result = caseITypedElement(dagArrayNode);
				if (result == null) result = caseGecosNode(dagArrayNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_SIMPLE_ARRAY_NODE: {
				DAGSimpleArrayNode dagSimpleArrayNode = (DAGSimpleArrayNode)theEObject;
				T result = caseDAGSimpleArrayNode(dagSimpleArrayNode);
				if (result == null) result = caseDAGSymbolNode(dagSimpleArrayNode);
				if (result == null) result = caseDAGNode(dagSimpleArrayNode);
				if (result == null) result = caseAnnotatedElement(dagSimpleArrayNode);
				if (result == null) result = caseDagVisitable(dagSimpleArrayNode);
				if (result == null) result = caseITypedElement(dagSimpleArrayNode);
				if (result == null) result = caseGecosNode(dagSimpleArrayNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_FIELD_INSTRUCTION: {
				DAGFieldInstruction dagFieldInstruction = (DAGFieldInstruction)theEObject;
				T result = caseDAGFieldInstruction(dagFieldInstruction);
				if (result == null) result = caseDAGNode(dagFieldInstruction);
				if (result == null) result = caseAnnotatedElement(dagFieldInstruction);
				if (result == null) result = caseDagVisitable(dagFieldInstruction);
				if (result == null) result = caseITypedElement(dagFieldInstruction);
				if (result == null) result = caseGecosNode(dagFieldInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_JUMP_NODE: {
				DAGJumpNode dagJumpNode = (DAGJumpNode)theEObject;
				T result = caseDAGJumpNode(dagJumpNode);
				if (result == null) result = caseDAGNode(dagJumpNode);
				if (result == null) result = caseAnnotatedElement(dagJumpNode);
				if (result == null) result = caseDagVisitable(dagJumpNode);
				if (result == null) result = caseITypedElement(dagJumpNode);
				if (result == null) result = caseGecosNode(dagJumpNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_PATTERN_NODE: {
				DAGPatternNode dagPatternNode = (DAGPatternNode)theEObject;
				T result = caseDAGPatternNode(dagPatternNode);
				if (result == null) result = caseDAGOpNode(dagPatternNode);
				if (result == null) result = caseDAGNode(dagPatternNode);
				if (result == null) result = caseAnnotatedElement(dagPatternNode);
				if (result == null) result = caseDagVisitable(dagPatternNode);
				if (result == null) result = caseITypedElement(dagPatternNode);
				if (result == null) result = caseGecosNode(dagPatternNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.IN_PORT_MAP: {
				@SuppressWarnings("unchecked") Map.Entry<DAGInPort, DAGInPort> inPortMap = (Map.Entry<DAGInPort, DAGInPort>)theEObject;
				T result = caseInPortMap(inPortMap);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.OUT_PORT_MAP: {
				@SuppressWarnings("unchecked") Map.Entry<DAGOutPort, DAGOutPort> outPortMap = (Map.Entry<DAGOutPort, DAGOutPort>)theEObject;
				T result = caseOutPortMap(outPortMap);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_VECTOR_NODE: {
				DAGVectorNode dagVectorNode = (DAGVectorNode)theEObject;
				T result = caseDAGVectorNode(dagVectorNode);
				if (result == null) result = caseDAGNode(dagVectorNode);
				if (result == null) result = caseAnnotatedElement(dagVectorNode);
				if (result == null) result = caseDagVisitable(dagVectorNode);
				if (result == null) result = caseITypedElement(dagVectorNode);
				if (result == null) result = caseGecosNode(dagVectorNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_VECTOR_ARRAY_ACCESS_NODE: {
				DAGVectorArrayAccessNode dagVectorArrayAccessNode = (DAGVectorArrayAccessNode)theEObject;
				T result = caseDAGVectorArrayAccessNode(dagVectorArrayAccessNode);
				if (result == null) result = caseDAGVectorNode(dagVectorArrayAccessNode);
				if (result == null) result = caseDAGSimpleArrayNode(dagVectorArrayAccessNode);
				if (result == null) result = caseDAGSymbolNode(dagVectorArrayAccessNode);
				if (result == null) result = caseDAGNode(dagVectorArrayAccessNode);
				if (result == null) result = caseAnnotatedElement(dagVectorArrayAccessNode);
				if (result == null) result = caseDagVisitable(dagVectorArrayAccessNode);
				if (result == null) result = caseITypedElement(dagVectorArrayAccessNode);
				if (result == null) result = caseGecosNode(dagVectorArrayAccessNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_VECTOR_OP_NODE: {
				DAGVectorOpNode dagVectorOpNode = (DAGVectorOpNode)theEObject;
				T result = caseDAGVectorOpNode(dagVectorOpNode);
				if (result == null) result = caseDAGVectorNode(dagVectorOpNode);
				if (result == null) result = caseDAGNode(dagVectorOpNode);
				if (result == null) result = caseAnnotatedElement(dagVectorOpNode);
				if (result == null) result = caseDagVisitable(dagVectorOpNode);
				if (result == null) result = caseITypedElement(dagVectorOpNode);
				if (result == null) result = caseGecosNode(dagVectorOpNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_VECTOR_SHUFFLE_NODE: {
				DAGVectorShuffleNode dagVectorShuffleNode = (DAGVectorShuffleNode)theEObject;
				T result = caseDAGVectorShuffleNode(dagVectorShuffleNode);
				if (result == null) result = caseDAGVectorNode(dagVectorShuffleNode);
				if (result == null) result = caseDAGNode(dagVectorShuffleNode);
				if (result == null) result = caseAnnotatedElement(dagVectorShuffleNode);
				if (result == null) result = caseDagVisitable(dagVectorShuffleNode);
				if (result == null) result = caseITypedElement(dagVectorShuffleNode);
				if (result == null) result = caseGecosNode(dagVectorShuffleNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_VECTOR_EXTRACT_NODE: {
				DAGVectorExtractNode dagVectorExtractNode = (DAGVectorExtractNode)theEObject;
				T result = caseDAGVectorExtractNode(dagVectorExtractNode);
				if (result == null) result = caseDAGVectorNode(dagVectorExtractNode);
				if (result == null) result = caseDAGNode(dagVectorExtractNode);
				if (result == null) result = caseAnnotatedElement(dagVectorExtractNode);
				if (result == null) result = caseDagVisitable(dagVectorExtractNode);
				if (result == null) result = caseITypedElement(dagVectorExtractNode);
				if (result == null) result = caseGecosNode(dagVectorExtractNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_VECTOR_EXPAND_NODE: {
				DAGVectorExpandNode dagVectorExpandNode = (DAGVectorExpandNode)theEObject;
				T result = caseDAGVectorExpandNode(dagVectorExpandNode);
				if (result == null) result = caseDAGVectorNode(dagVectorExpandNode);
				if (result == null) result = caseDAGNode(dagVectorExpandNode);
				if (result == null) result = caseAnnotatedElement(dagVectorExpandNode);
				if (result == null) result = caseDagVisitable(dagVectorExpandNode);
				if (result == null) result = caseITypedElement(dagVectorExpandNode);
				if (result == null) result = caseGecosNode(dagVectorExpandNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_VECTOR_PACK_NODE: {
				DAGVectorPackNode dagVectorPackNode = (DAGVectorPackNode)theEObject;
				T result = caseDAGVectorPackNode(dagVectorPackNode);
				if (result == null) result = caseDAGVectorNode(dagVectorPackNode);
				if (result == null) result = caseDAGNode(dagVectorPackNode);
				if (result == null) result = caseAnnotatedElement(dagVectorPackNode);
				if (result == null) result = caseDagVisitable(dagVectorPackNode);
				if (result == null) result = caseITypedElement(dagVectorPackNode);
				if (result == null) result = caseGecosNode(dagVectorPackNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAGSSA_USE_NODE: {
				DAGSSAUseNode dagssaUseNode = (DAGSSAUseNode)theEObject;
				T result = caseDAGSSAUseNode(dagssaUseNode);
				if (result == null) result = caseDAGSymbolNode(dagssaUseNode);
				if (result == null) result = caseDAGNode(dagssaUseNode);
				if (result == null) result = caseAnnotatedElement(dagssaUseNode);
				if (result == null) result = caseDagVisitable(dagssaUseNode);
				if (result == null) result = caseITypedElement(dagssaUseNode);
				if (result == null) result = caseGecosNode(dagssaUseNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAGSSA_DEF_NODE: {
				DAGSSADefNode dagssaDefNode = (DAGSSADefNode)theEObject;
				T result = caseDAGSSADefNode(dagssaDefNode);
				if (result == null) result = caseDAGOutNode(dagssaDefNode);
				if (result == null) result = caseDAGNode(dagssaDefNode);
				if (result == null) result = caseAnnotatedElement(dagssaDefNode);
				if (result == null) result = caseDagVisitable(dagssaDefNode);
				if (result == null) result = caseITypedElement(dagssaDefNode);
				if (result == null) result = caseGecosNode(dagssaDefNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DagPackage.DAG_PHI_NODE: {
				DAGPhiNode dagPhiNode = (DAGPhiNode)theEObject;
				T result = caseDAGPhiNode(dagPhiNode);
				if (result == null) result = caseDAGNode(dagPhiNode);
				if (result == null) result = caseAnnotatedElement(dagPhiNode);
				if (result == null) result = caseDagVisitable(dagPhiNode);
				if (result == null) result = caseITypedElement(dagPhiNode);
				if (result == null) result = caseGecosNode(dagPhiNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visitor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visitor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDagVisitor(DagVisitor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDagVisitable(DagVisitable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGInstruction(DAGInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGNode(DAGNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGEdge(DAGEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGPort(DAGPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG In Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG In Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGInPort(DAGInPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Out Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Out Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGOutPort(DAGOutPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Imm Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Imm Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGImmNode(DAGImmNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Symbol Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Symbol Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGSymbolNode(DAGSymbolNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Call Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Call Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGCallNode(DAGCallNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Array Value Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Array Value Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGArrayValueNode(DAGArrayValueNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Op Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Op Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGOpNode(DAGOpNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Out Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Out Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGOutNode(DAGOutNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Control Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Control Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGControlEdge(DAGControlEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Data Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Data Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGDataEdge(DAGDataEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Numbered Symbol Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Numbered Symbol Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGNumberedSymbolNode(DAGNumberedSymbolNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Numbered Out Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Numbered Out Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGNumberedOutNode(DAGNumberedOutNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Int Imm Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Int Imm Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGIntImmNode(DAGIntImmNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Float Imm Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Float Imm Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGFloatImmNode(DAGFloatImmNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG String Imm Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG String Imm Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGStringImmNode(DAGStringImmNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Array Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Array Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGArrayNode(DAGArrayNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Simple Array Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Simple Array Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGSimpleArrayNode(DAGSimpleArrayNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Field Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Field Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGFieldInstruction(DAGFieldInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Jump Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Jump Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGJumpNode(DAGJumpNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Pattern Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Pattern Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGPatternNode(DAGPatternNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>In Port Map</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>In Port Map</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInPortMap(Map.Entry<DAGInPort, DAGInPort> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Out Port Map</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Out Port Map</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOutPortMap(Map.Entry<DAGOutPort, DAGOutPort> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Vector Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Vector Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGVectorNode(DAGVectorNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Vector Array Access Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Vector Array Access Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGVectorArrayAccessNode(DAGVectorArrayAccessNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Vector Op Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Vector Op Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGVectorOpNode(DAGVectorOpNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Vector Shuffle Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Vector Shuffle Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGVectorShuffleNode(DAGVectorShuffleNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Vector Extract Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Vector Extract Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGVectorExtractNode(DAGVectorExtractNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Vector Expand Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Vector Expand Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGVectorExpandNode(DAGVectorExpandNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Vector Pack Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Vector Pack Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGVectorPackNode(DAGVectorPackNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAGSSA Use Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAGSSA Use Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGSSAUseNode(DAGSSAUseNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAGSSA Def Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAGSSA Def Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGSSADefNode(DAGSSADefNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DAG Phi Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DAG Phi Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDAGPhiNode(DAGPhiNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Gecos Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Gecos Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGecosNode(GecosNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotatedElement(AnnotatedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInstrsVisitable(InstrsVisitable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ITyped Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ITyped Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseITypedElement(ITypedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInstruction(Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //DagSwitch
