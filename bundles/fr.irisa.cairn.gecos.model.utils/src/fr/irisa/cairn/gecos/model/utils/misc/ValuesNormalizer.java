package fr.irisa.cairn.gecos.model.utils.misc;

import static java.util.stream.Collectors.toList;

import java.util.DoubleSummaryStatistics;
import java.util.List;

public class ValuesNormalizer {

	private double min;
	private double range;
	
	public ValuesNormalizer(double min, double max) {
		this.min = min;
		this.range = max - min;
	}
	
	public ValuesNormalizer(List<Double> values) {
		DoubleSummaryStatistics valuesStats = values.stream().mapToDouble(Double::doubleValue).summaryStatistics();
		this.min = valuesStats.getMin();
		this.range = valuesStats.getMax() - min;
	}
	
	public double getRange() {
		return range;
	}

	public void setRange(double range) {
		this.range = range;
	}

	public double getMin() {
		return min;
	}

	public void setMin(double min) {
		this.min = min;
	}

	public double getMax() {
		return range + min;
	}

	public List<Double> normalize(List<Double> values) {
		if(range == 0)
			return values;
		return values.stream().map(this::normalize).collect(toList());
	}

	public double normalize(Double v) {
		return (v - min) / range;
	}

	public static double normalize(double min, double range, Double v) {
		return (v-min) / range;
	}

}
