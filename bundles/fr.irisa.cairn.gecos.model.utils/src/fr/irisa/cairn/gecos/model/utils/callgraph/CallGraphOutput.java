package fr.irisa.cairn.gecos.model.utils.callgraph;

import java.io.FileNotFoundException;

import fr.irisa.cairn.gecos.model.extensions.outputs.GecosOutput;

public class CallGraphOutput extends GecosOutput {

	public void print(CallGraph g){
		try {
			CallGraphDottyExport export = new CallGraphDottyExport(g, file);
			export.compute();
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
}
