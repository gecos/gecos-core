package fr.irisa.cairn.gecos.core.profiling.frontend;

import static fr.irisa.cairn.gecos.model.utils.annotations.PragmaUtils.findPragmaContent;
import static fr.irisa.cairn.gecos.model.utils.annotations.PragmaUtils.getPragmaName;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.collect.Streams;

import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.tools.controlflow.BuildControlFlow;
import fr.irisa.cairn.gecos.model.tools.controlflow.ClearControlFlow;
import fr.irisa.cairn.gecos.model.transforms.blocks.simplifier.MergeCompositeBlocks;
import fr.irisa.cairn.gecos.model.utils.annotations.AnnotationUtils;
import gecos.annotations.ExtendedAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.instrs.CallInstruction;
import gecos.instrs.Instruction;
import profiling.frontend.directives.Directive;
import profiling.frontend.directives.DirectiveName;
import profiling.frontend.directives.DirectivesFactory;
import profiling.frontend.directives.InjectDirective;
import profiling.frontend.directives.SaveDirective;
import profiling.frontend.directives.SizeDirective;

/**
 * @author aelmouss
 */
public class DirectivesProcessor {

	private static final String TARGET_FUNC_PRAGMA_NAME = "MAIN_FUNC";
	public static final String SIZE_ANNOTATION_KEY = "DirectivesProcessor:size";
	
	/**
	 * Try to find the target (entry point) procedure.
	 * In case only one procedure is available in ps, it is considered as the target.
	 * Otherwise, it looks for a procedure with pragma {@value #TARGET_FUNC_PRAGMA_NAME} annotation.
	 * 
	 * @return the target procedure, never {@code null}.
	 */
	public static Procedure getTargetProcedure(List<Procedure> procs) {
		Procedure target = null;
		if(procs.size() == 1)
			target = procs.get(0);
		else {
			target = procs.stream()
				.filter(p -> findPragmaContent(p.getSymbol(), c -> 
						getPragmaName(c).equalsIgnoreCase(TARGET_FUNC_PRAGMA_NAME)).findAny().isPresent())
				.findFirst().orElse(null);
		}
		if(target == null)
			throw new RuntimeException("No Target procedure was found! "
					+ "It must be annotated with '#pragma " + TARGET_FUNC_PRAGMA_NAME 
					+ "' in case multiple procedures are present.");
		
		return target;
	}
	
	private static List<Instruction> getSizesFromAnnotation(Symbol s) {
		ExtendedAnnotation a = AnnotationUtils.getAnnotation(s, SIZE_ANNOTATION_KEY);
		if(a == null)
			return null;
		return a.getRefs().stream()
				.map(Instruction.class::cast)
				.collect(toList());
	}
	
	/**
	 * Try to get the symbol dimension sizes (in outermost-first order)
	 * from its type, if available, otherwise try to find it from a corresponding
	 * $size directive if any was specified. Otherwise it throws a {@link RuntimeException}.
	 * @param s symbol
	 * @return symbol dimension sizes in outermost-first order.
	 * @throws RuntimeException if size was not found for one or more dimensions.
	 */
	public static List<Instruction> getDimensionSizes(Symbol s) {
		List<Instruction> sizesFromAnnotation = DirectivesProcessor.getSizesFromAnnotation(s);
		TypeAnalyzer analyzer = new TypeAnalyzer(s.getType(), true);
		
		List<Instruction> sizesOutermostFirst = new ArrayList<>();
		for(int dim = 0; dim < analyzer.getTotalNbDims(); dim++) { //outermost-first order
			Instruction sz = analyzer.tryGetDimSize(dim);
			if(sz == null && sizesFromAnnotation != null)
				sz = sizesFromAnnotation.get(dim);
			if(sz == null) 
				throw new RuntimeException("Symbol type does not provide size for dimension '" + dim + "' and no $size directive was provided: " + s
						+ "\nMust specify size using directive '$size("+s.getName()+", outermost-first comma-separated dimension sizes)' "
						+ ", or change symbol type to add size information.");
			sizesOutermostFirst.add(sz);
		}
		
		return sizesOutermostFirst;
	}
	
	/**
	 * @param dim outermost-first order (i.e. 0 corresponds to outermost dimension) 
	 * @return the size of the dimension specified by {@code dim} if found, {@code null} otherwise.
	 */
	public static Instruction getDimensionSize(Symbol s, int dim) {
		List<Instruction> sizesFromAnnotation = DirectivesProcessor.getSizesFromAnnotation(s);
		TypeAnalyzer analyzer = new TypeAnalyzer(s.getType(), true);
		Instruction sz = analyzer.tryGetDimSize(dim);
		if(sz == null && sizesFromAnnotation != null)
			sz = sizesFromAnnotation.get(dim);
		return sz;
	}
	
	
	//-------------------------------------------------------------------------------
	
	private static final DirectivesFactory factory = DirectivesFactory.eINSTANCE;
	
	
	private static final DirectiveName[] ROOT_DIRECTIVES = new DirectiveName[] { 
			DirectiveName.SIZE, 
			DirectiveName.SAVE, 
			DirectiveName.INJECT };
	
	public static Directive createDirective(CallInstruction call) {
		DirectiveName name = DirectiveName.get(getCallProcedureName(call).toLowerCase());
		switch (name) {
		case SIZE:
			SizeDirective size = factory.createSizeDirective();
			size.setCall(call);
			return size;
		case SAVE:
			SaveDirective save = factory.createSaveDirective();
			save.setCall(call);
			return save;
		case INJECT:
			InjectDirective d = factory.createInjectDirective();
			d.setCall(call);
			return d;	
		default:
			throw new IllegalArgumentException("Unknown directive " + call);
		}
	}
	
	private static Stream<String> getDirectiveNames() {
		return Arrays.stream(ROOT_DIRECTIVES)
			.map(DirectiveName::getLiteral);
	}
	
	private static List<Directive> findDirectives(Procedure p) {
		Predicate<CallInstruction> predicate = call -> getDirectiveNames()
				.filter(getCallProcedureName(call)::equalsIgnoreCase)
				.findAny().isPresent();
		
		return Streams.stream(p.eAllContents()) // EMFUtils.eAllContentsInstancesOf(p, CallInstruction.class).stream()
			.filter(CallInstruction.class::isInstance)
			.map(CallInstruction.class::cast)
			.filter(predicate)
			.map(DirectivesProcessor::createDirective)
			.collect(toList());
	}

	private static String getCallProcedureName(CallInstruction call) {
		return call.getProcedureSymbol().getName();
	}
	
	//-------------------------------------------------------------------------------
	
	
	private Map<Integer, Symbol> savedSymbols; //<uid, symbol>

	private EObject root;
	private List<ProcedureSet> procedureSets;
	private GecosProject originalProjectCopy;
	
	
	public DirectivesProcessor(GecosProject project) {
		this.originalProjectCopy = project.copy();
		this.root = project;
		this.savedSymbols = new LinkedHashMap<>();
		this.procedureSets = project.listProcedureSets();
	}
	
//	public DirectivesProcessor(ProcedureSet ps) {
//		this.root = ps;
//		this.procedureSets = Arrays.asList(ps);
//		this.savedSymbols = new LinkedHashMap<>();
//	}
	
	public Map<Integer, Symbol> getSavedSymbols() {
		return savedSymbols;
	}
	
	/**
	 * @return a copy of the original project in which $ directives have been removed.
	 */
	public GecosProject compute() {
		/* process directives */ 
		procedureSets.forEach(this::replaceDirectives);

		if(root instanceof GecosProject) {
			GecosProject project = (GecosProject) root;
			new MergeCompositeBlocks(project).compute();
			new ClearControlFlow(project).compute();
			new BuildControlFlow(project).compute();
		} else {
			ProcedureSet procedureSet = (ProcedureSet) root;
			new MergeCompositeBlocks().visit(procedureSet);
			new ClearControlFlow(procedureSet).compute();
			new BuildControlFlow(procedureSet).compute();
		}
		
		/* remove directives in a copy of the project */
		originalProjectCopy.listProcedures().stream()
			.flatMap(p -> findDirectives(p).stream())
			.forEach(d -> d.getCall().remove());
		
		return originalProjectCopy;
	}
	
	private void replaceDirectives(ProcedureSet ps) {
		ps.listProcedures().stream()
			.flatMap(p -> findDirectives(p).stream())
			.peek(Directive::parse)
			.forEach(this::replaceDirective);
	}
	
	private void replaceDirective(Directive directive) {
		CallInstruction call = directive.getCall();
		Block codeBlock = directive.createCodeBlock();
		
		BasicBlock directiveBlck = call.getBasicBlock();
		if(directiveBlck.getInstructionCount() == 1) {
			EcoreUtil.replace(directiveBlck, codeBlock);
		} else {
			directiveBlck.splitAt(call);
			directiveBlck.addBlockAfter(codeBlock, directiveBlck);
		}
		
		//XXX
		if(directive instanceof SaveDirective) {
			Symbol address = ((SaveDirective) directive).getAddress();
			savedSymbols.put(UidProvider.getUID(address), address);
		}
	}

}