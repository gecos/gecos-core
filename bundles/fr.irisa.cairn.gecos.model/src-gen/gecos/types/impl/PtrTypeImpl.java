/**
 */
package gecos.types.impl;

import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.TypeSizes;

import gecos.types.PtrType;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import gecos.types.TypesPackage;
import gecos.types.TypesVisitor;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ptr Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.impl.PtrTypeImpl#getBase <em>Base</em>}</li>
 *   <li>{@link gecos.types.impl.PtrTypeImpl#isRestrict <em>Restrict</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PtrTypeImpl extends TypeImpl implements PtrType {
	/**
	 * The cached value of the '{@link #getBase() <em>Base</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase()
	 * @generated
	 * @ordered
	 */
	protected Type base;

	/**
	 * The default value of the '{@link #isRestrict() <em>Restrict</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRestrict()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RESTRICT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isRestrict() <em>Restrict</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRestrict()
	 * @generated
	 * @ordered
	 */
	protected boolean restrict = RESTRICT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PtrTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypesPackage.Literals.PTR_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type getBase() {
		return base;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBase(Type newBase) {
		Type oldBase = base;
		base = newBase;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.PTR_TYPE__BASE, oldBase, base));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRestrict() {
		return restrict;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRestrict(boolean newRestrict) {
		boolean oldRestrict = restrict;
		restrict = newRestrict;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.PTR_TYPE__RESTRICT, oldRestrict, restrict));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final TypesVisitor visitor) {
		visitor.visitPtrType(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNbDims() {
		int depth = 1;
		Type tmp = this.getBase();
		while ((tmp instanceof PtrType)) {
			{
				this.setBase(((PtrType)tmp).getBase());
				depth++;
			}
		}
		return depth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type getInnermostBase() {
		Type tmp = this.getBase();
		while ((tmp instanceof PtrType)) {
			tmp = ((PtrType)tmp).getBase();
		}
		return this.getBase();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInnermostBase(final Type innermostBase) {
		Type tmp = innermostBase;
		EObject _eContainer = tmp.eContainer();
		boolean _not = (!(_eContainer instanceof PtrType));
		if (_not) {
			throw new RuntimeException();
		}
		EObject _eContainer_1 = this.getBase().eContainer();
		((PtrType) _eContainer_1).setBase(innermostBase);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEqual(final Type other, final boolean ignoreStorageClass, final boolean ignoreConst, final boolean ignoreVolatile) {
		if ((other == this)) {
			return true;
		}
		if ((other instanceof PtrType)) {
			final PtrType ptrtype = ((PtrType)other);
			Type _base = ptrtype.getBase();
			boolean _tripleNotEquals = (_base != null);
			Type _base_1 = this.getBase();
			boolean _tripleNotEquals_1 = (_base_1 != null);
			boolean _xor = (_tripleNotEquals ^ _tripleNotEquals_1);
			if (_xor) {
				return false;
			}
			if (((ptrtype.getBase() != null) && (this.getBase() != null))) {
				boolean _isEqual = ptrtype.getBase().isEqual(this.getBase(), ignoreStorageClass, ignoreConst, ignoreVolatile);
				boolean _not = (!_isEqual);
				if (_not) {
					return false;
				}
			}
			return ((Boolean.valueOf(this.isRestrict()) == Boolean.valueOf(ptrtype.isRestrict())) && super.isEqual(ptrtype, ignoreStorageClass, ignoreConst, ignoreVolatile));
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getSize() {
		return TypeSizes.defaultPointerSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _xifexpression = null;
		boolean _isRestrict = this.isRestrict();
		if (_isRestrict) {
			_xifexpression = "restrict ";
		}
		else {
			_xifexpression = "";
		}
		String _plus = ("*" + _xifexpression);
		String _xifexpression_1 = null;
		boolean _isConstant = this.isConstant();
		if (_isConstant) {
			_xifexpression_1 = "const ";
		}
		else {
			_xifexpression_1 = "";
		}
		String _plus_1 = (_plus + _xifexpression_1);
		String _xifexpression_2 = null;
		boolean _isVolatile = this.isVolatile();
		if (_isVolatile) {
			_xifexpression_2 = "volatile ";
		}
		else {
			_xifexpression_2 = "";
		}
		String _plus_2 = (_plus_1 + _xifexpression_2);
		String _plus_3 = (_plus_2 + "(");
		String _elvis = null;
		Type _base = this.getBase();
		String _string = null;
		if (_base!=null) {
			_string=_base.toString();
		}
		if (_string != null) {
			_elvis = _string;
		} else {
			_elvis = "?";
		}
		String _plus_4 = (_plus_3 + _elvis);
		return (_plus_4 + ")");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInnermostStorageClass(final StorageClassSpecifiers innermostStorage) {
		Type _innermostBase = this.getInnermostBase();
		_innermostBase.setInnermostStorageClass(innermostStorage);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TypesPackage.PTR_TYPE__BASE:
				return getBase();
			case TypesPackage.PTR_TYPE__RESTRICT:
				return isRestrict();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TypesPackage.PTR_TYPE__BASE:
				setBase((Type)newValue);
				return;
			case TypesPackage.PTR_TYPE__RESTRICT:
				setRestrict((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TypesPackage.PTR_TYPE__BASE:
				setBase((Type)null);
				return;
			case TypesPackage.PTR_TYPE__RESTRICT:
				setRestrict(RESTRICT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TypesPackage.PTR_TYPE__BASE:
				return base != null;
			case TypesPackage.PTR_TYPE__RESTRICT:
				return restrict != RESTRICT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //PtrTypeImpl
