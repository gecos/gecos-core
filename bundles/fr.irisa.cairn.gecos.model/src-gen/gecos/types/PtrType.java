/**
 */
package gecos.types;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ptr Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.PtrType#getBase <em>Base</em>}</li>
 *   <li>{@link gecos.types.PtrType#isRestrict <em>Restrict</em>}</li>
 * </ul>
 *
 * @see gecos.types.TypesPackage#getPtrType()
 * @model
 * @generated
 */
public interface PtrType extends Type, TypesVisitable {
	/**
	 * Returns the value of the '<em><b>Base</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base</em>' reference.
	 * @see #setBase(Type)
	 * @see gecos.types.TypesPackage#getPtrType_Base()
	 * @model resolveProxies="false"
	 * @generated
	 */
	Type getBase();

	/**
	 * Sets the value of the '{@link gecos.types.PtrType#getBase <em>Base</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base</em>' reference.
	 * @see #getBase()
	 * @generated
	 */
	void setBase(Type value);

	/**
	 * Returns the value of the '<em><b>Restrict</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Restrict</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Restrict</em>' attribute.
	 * @see #setRestrict(boolean)
	 * @see gecos.types.TypesPackage#getPtrType_Restrict()
	 * @model default="false" unique="false" dataType="gecos.types.boolean"
	 * @generated
	 */
	boolean isRestrict();

	/**
	 * Sets the value of the '{@link gecos.types.PtrType#isRestrict <em>Restrict</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Restrict</em>' attribute.
	 * @see #isRestrict()
	 * @generated
	 */
	void setRestrict(boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitPtrType(this);'"
	 * @generated
	 */
	void accept(TypesVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return the number of {@link PtrType} in the 'base' hierarchy.
	 * <pre>
	 * e.g.: A.getNbDims() = 1 for: *A or (*A)[1], and 2 for **A or (*(**A)[1])[2]
	 * </pre>
	 * <!-- end-model-doc -->
	 * @model kind="operation" dataType="gecos.types.int" unique="false" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int depth = 1;\n&lt;%gecos.types.Type%&gt; tmp = this.getBase();\nwhile ((tmp instanceof &lt;%gecos.types.PtrType%&gt;))\n{\n\t{\n\t\tthis.setBase(((&lt;%gecos.types.PtrType%&gt;)tmp).getBase());\n\t\tdepth++;\n\t}\n}\nreturn depth;'"
	 * @generated
	 */
	int getNbDims();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return the innermost type of this pointer.
	 * <pre>
	 * e.g: (int ***).getInnermostBase() should return (int),
	 * whereas getBase() simply returns (int **)
	 * </pre>
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.Type%&gt; tmp = this.getBase();\nwhile ((tmp instanceof &lt;%gecos.types.PtrType%&gt;))\n{\n\ttmp = ((&lt;%gecos.types.PtrType%&gt;)tmp).getBase();\n}\nreturn this.getBase();'"
	 * @generated
	 */
	Type getInnermostBase();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Set the innermost type of this pointer.
	 * <pre>
	 * e.g: (int ***).setInnermostBase(float) modifies this to (float ***))
	 * </pre>
	 * <!-- end-model-doc -->
	 * @model innermostBaseUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.Type%&gt; tmp = innermostBase;\n&lt;%org.eclipse.emf.ecore.EObject%&gt; _eContainer = tmp.eContainer();\nboolean _not = (!(_eContainer instanceof &lt;%gecos.types.PtrType%&gt;));\nif (_not)\n{\n\tthrow new &lt;%java.lang.RuntimeException%&gt;();\n}\n&lt;%org.eclipse.emf.ecore.EObject%&gt; _eContainer_1 = this.getBase().eContainer();\n((&lt;%gecos.types.PtrType%&gt;) _eContainer_1).setBase(innermostBase);'"
	 * @generated
	 */
	void setInnermostBase(Type innermostBase);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.boolean" unique="false" otherUnique="false" ignoreStorageClassDataType="gecos.types.boolean" ignoreStorageClassUnique="false" ignoreConstDataType="gecos.types.boolean" ignoreConstUnique="false" ignoreVolatileDataType="gecos.types.boolean" ignoreVolatileUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((other == this))\n{\n\treturn true;\n}\nif ((other instanceof &lt;%gecos.types.PtrType%&gt;))\n{\n\tfinal &lt;%gecos.types.PtrType%&gt; ptrtype = ((&lt;%gecos.types.PtrType%&gt;)other);\n\t&lt;%gecos.types.Type%&gt; _base = ptrtype.getBase();\n\tboolean _tripleNotEquals = (_base != null);\n\t&lt;%gecos.types.Type%&gt; _base_1 = this.getBase();\n\tboolean _tripleNotEquals_1 = (_base_1 != null);\n\tboolean _xor = (_tripleNotEquals ^ _tripleNotEquals_1);\n\tif (_xor)\n\t{\n\t\treturn false;\n\t}\n\tif (((ptrtype.getBase() != null) &amp;&amp; (this.getBase() != null)))\n\t{\n\t\tboolean _isEqual = ptrtype.getBase().isEqual(this.getBase(), ignoreStorageClass, ignoreConst, ignoreVolatile);\n\t\tboolean _not = (!_isEqual);\n\t\tif (_not)\n\t\t{\n\t\t\treturn false;\n\t\t}\n\t}\n\treturn ((&lt;%java.lang.Boolean%&gt;.valueOf(this.isRestrict()) == &lt;%java.lang.Boolean%&gt;.valueOf(ptrtype.isRestrict())) &amp;&amp; super.isEqual(ptrtype, ignoreStorageClass, ignoreConst, ignoreVolatile));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isEqual(Type other, boolean ignoreStorageClass, boolean ignoreConst, boolean ignoreVolatile);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.types.long" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.TypeSizes%&gt;.defaultPointerSize;'"
	 * @generated
	 */
	long getSize();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _xifexpression = null;\nboolean _isRestrict = this.isRestrict();\nif (_isRestrict)\n{\n\t_xifexpression = \"restrict \";\n}\nelse\n{\n\t_xifexpression = \"\";\n}\n&lt;%java.lang.String%&gt; _plus = (\"*\" + _xifexpression);\n&lt;%java.lang.String%&gt; _xifexpression_1 = null;\nboolean _isConstant = this.isConstant();\nif (_isConstant)\n{\n\t_xifexpression_1 = \"const \";\n}\nelse\n{\n\t_xifexpression_1 = \"\";\n}\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + _xifexpression_1);\n&lt;%java.lang.String%&gt; _xifexpression_2 = null;\nboolean _isVolatile = this.isVolatile();\nif (_isVolatile)\n{\n\t_xifexpression_2 = \"volatile \";\n}\nelse\n{\n\t_xifexpression_2 = \"\";\n}\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + _xifexpression_2);\n&lt;%java.lang.String%&gt; _plus_3 = (_plus_2 + \"(\");\n&lt;%java.lang.String%&gt; _elvis = null;\n&lt;%gecos.types.Type%&gt; _base = this.getBase();\n&lt;%java.lang.String%&gt; _string = null;\nif (_base!=null)\n{\n\t_string=_base.toString();\n}\nif (_string != null)\n{\n\t_elvis = _string;\n} else\n{\n\t_elvis = \"?\";\n}\n&lt;%java.lang.String%&gt; _plus_4 = (_plus_3 + _elvis);\nreturn (_plus_4 + \")\");'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model innermostStorageUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.Type%&gt; _innermostBase = this.getInnermostBase();\n_innermostBase.setInnermostStorageClass(innermostStorage);'"
	 * @generated
	 */
	void setInnermostStorageClass(StorageClassSpecifiers innermostStorage);

} // PtrType
