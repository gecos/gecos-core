package fr.irisa.cairn.gecos.core.profiling.backend;

public class InvalidProfilingFileFormat extends RuntimeException {

	private static final long serialVersionUID = -6054237598855067816L;

	public InvalidProfilingFileFormat(String msg) {
		super(msg);
	}
	
	public InvalidProfilingFileFormat(String msg, Exception e) {
		super(msg, e);
	}
}
