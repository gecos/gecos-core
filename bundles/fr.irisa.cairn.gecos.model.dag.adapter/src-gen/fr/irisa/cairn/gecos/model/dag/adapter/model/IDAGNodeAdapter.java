package fr.irisa.cairn.gecos.model.dag.adapter.model;

import gecos.dag.DAGNode;

import java.util.List;

/**
 * Interface of a node adapter.
 * This node adapt a DAGNode
 * @author <your name>
 *
 */
public interface IDAGNodeAdapter
		extends
			fr.irisa.cairn.graph.INode,
			fr.irisa.cairn.graph.observer.IObservable {

	/**
	 * Get the adapted node.
	 *
	 * @return the adapted node
	 */
	DAGNode getAdaptedNode();

	/**
	 * Get the graph containing the node.
	 * 
	 * @model default=""
	 * 
	 * @return the node graph or <code>null</code> if graph hasn't been set
	 */
	public IDAGInstructionAdapter getGraph();

	/**
	 * Get a list of all incoming edges to the node.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	List<IDAGEdgeAdapter> getIncomingEdges();

	/**
	 * Get a list of all outgoing edges from the node.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	List<IDAGEdgeAdapter> getOutgoingEdges();

	/**
	 * Get the list of all input ports.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	List<IDAGPortAdapter> getInputPorts();

	/**
	 * Get the list of all output ports.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	List<IDAGPortAdapter> getOutputPorts();

	/**
	 * Get predecessors of this node in its graph.
	 * 
	 * @model default=""
	 */
	List<IDAGNodeAdapter> getPredecessors();

	/**
	 * Get successors of this node in its graph.
	 * 
	 * @model default=""
	 */
	List<IDAGNodeAdapter> getSuccessors();

}
