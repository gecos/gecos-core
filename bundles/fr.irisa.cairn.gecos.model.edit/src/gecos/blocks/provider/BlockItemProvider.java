/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package gecos.blocks.provider;

import gecos.annotations.provider.AnnotatedElementItemProvider;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksPackage;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.WhileBlock;

import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link gecos.blocks.Block} object.
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * @generated
 */
public class BlockItemProvider extends AnnotatedElementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public BlockItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addParentPropertyDescriptor(object);
			addNumberPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Number feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addNumberPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Block_number_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Block_number_feature", "_UI_Block_type"),
				 BlocksPackage.Literals.BLOCK__NUMBER,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Parent feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addParentPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Block_parent_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Block_parent_feature", "_UI_Block_type"),
				 BlocksPackage.Literals.BLOCK__PARENT,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean shouldComposeCreationImage() {
		return true;
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	@Override
	public String getText(Object object) {
		Block block = (Block) object;
		Object parent = block.getParent();
		String label = "";
		try {
		if (parent != null)
			if (parent instanceof IfBlock) {
				if (((IfBlock) parent).getTestBlock()==block) {
					label = "Cond ";
				} else if (((IfBlock) parent).getThenBlock()==block) {
					label = "Then ";
				} else if (((IfBlock) parent).getElseBlock()==block) {
					label = "Else ";
				}
			} else if (parent instanceof ForBlock) {
				BasicBlock initBlock = ((ForBlock) parent).getInitBlock();
				BasicBlock testBlock = ((ForBlock) parent).getTestBlock();
				BasicBlock stepBlock = ((ForBlock) parent).getStepBlock();
				if (initBlock!=null && initBlock==block) {
					label = "Init ";
				} else if (testBlock!=null && testBlock==block) {
					label = "Test ";
				} else if (stepBlock!=null && stepBlock==block) {
					label = "Step ";
				} else if (((ForBlock) parent).getBodyBlock().equals(block)) {
					label = "Body ";
				}
			} else if (parent instanceof WhileBlock) {
				if (((WhileBlock) parent).getTestBlock()==(block)) {
					label = "Condition ";
				} else if (((WhileBlock) parent).getBodyBlock()==(block)) {
					label = "Body ";
				}
			} else if (parent instanceof DoWhileBlock) {
				if (((DoWhileBlock) parent).getTestBlock()==(block)) {
					label = "Test ";
				} else if (((DoWhileBlock) parent).getBodyBlock()==(block)) {
					label = "Body ";
				}
			}
		} catch (NullPointerException e) {
			label = "NPE error";
		}
		return label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Block.class)) {
			case BlocksPackage.BLOCK__NUMBER:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * Return the resource locator for this item provider's resources. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return gecos.blocks.provider.GecosEditPlugin.INSTANCE;
	}

}
