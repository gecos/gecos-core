package fr.irisa.cairn.gecos.model.tools.utils;

import gecos.blocks.BasicBlock;
import gecos.instrs.Instruction;

import java.util.ArrayList;
import java.util.List;

/**
 * This visitor returns all the BB containing labels inside the block.
 * 
 * @author amorvan
 * @generated NOT
 */
public class BlocksLabelFinder extends BlocksDefaultSwitch<Object> {

	private boolean containsLabels = false;
	public boolean containsLabels() { return containsLabels; }
	
	private List<BasicBlock> labels = new ArrayList<BasicBlock>();
	public List<BasicBlock> getLabels() { return labels; }

	@Override
	public Object caseBasicBlock(BasicBlock object) {
		BranchInstructionFinder visitor = new BranchInstructionFinder();
		for (Instruction i : object.getInstructions()) {
			visitor.doSwitch(i);
		}
		if (visitor.containsLabels()) {
			labels.add(object);
			containsLabels = true;
		}
		return null;
	}
	
}
