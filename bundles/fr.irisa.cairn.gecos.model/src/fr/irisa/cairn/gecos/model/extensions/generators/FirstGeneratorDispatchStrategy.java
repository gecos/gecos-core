package fr.irisa.cairn.gecos.model.extensions.generators;

import java.util.List;

/**
 * Strategy that select the first compatible generator extension.
 * @author mnaullet
 *
 */
public final class FirstGeneratorDispatchStrategy implements IGeneratorDispatchStrategy{

	public String dispatch(List<IGecosCodeGenerator> generators, Object o) {
		for (IGecosCodeGenerator iGecosCodeGenerator : generators) {
			String generated = iGecosCodeGenerator.generate(o);
			if(generated!=null)
				return generated;
		}
		System.err.println("Warning : could not generate code for object of " + o.getClass());
		return "/* Could not find code generator for object of " + o.getClass() + " */";
	}
}