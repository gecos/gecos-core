struct s1{
	int x[1];
};

int main() {
	struct s1{
		int x[10];
	};

	{
		struct s1{
			int x[20];
		};

		if (sizeof(struct s1) != 20*sizeof(int))
			abort ();
	}

	if (sizeof(struct s1) != 10*sizeof(int))
		abort ();

	exit (0);
}
