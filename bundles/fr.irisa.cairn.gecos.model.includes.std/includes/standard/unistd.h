#ifndef _UNISTD_H
#define _UNISTD_H

int unlink(const char *file);
int getopt(int argc, char * const argv[], const char *optstring);
extern char *optarg;
extern int optind, opterr, optopt;

#endif
