#include <stdio.h>

void testLocalComExpr (int a, int b, int m, int n, int j) {
  int c,d,e,f,g,h,k ;

  c = a + b ;
  d = m && n ;
  e = b + d ;
  f = a + b ;
  g = -b ;
  h = b + a ;
  a = j + a ;
  k = m && n ;
  j = b + d ;
  c = a + b ;
  a = -b ;
}
