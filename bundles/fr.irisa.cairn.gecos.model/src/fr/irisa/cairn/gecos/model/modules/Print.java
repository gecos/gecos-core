package fr.irisa.cairn.gecos.model.modules;

import gecos.core.Procedure;
import gecos.gecosproject.GecosHeaderDirectory;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;

import java.util.Map.Entry;

import fr.irisa.r2d2.gecos.framework.GSModule;

@GSModule("Prints information about the specified input on the standard output.")
public class Print {

	private Object arg;

	public Print (GecosProject proj) {
		this.arg = proj;
	}

	public Print (Procedure proc) { 
		this.arg = proc; 
	}
	
	public Print (GecosSourceFile inArg) {
		this.arg = inArg;
	}

	public Print (int inArg) {
		arg = inArg;
	}

	public void compute() {
		if (arg instanceof GecosProject) {
			GecosProject project = (GecosProject) arg;
			System.out.println("Project name : "+project);
			System.out.println("Includes dir : ");
			for (GecosHeaderDirectory d : project.getIncludes()) {
				System.out.println("\t"+d.getName());
			}
			System.out.println("Sources files : ");
			for (GecosSourceFile f : project.getSources()) {
				System.out.print("\t"+f.getName());
				if (f.getModel() != null) {
					System.out.print(" >> Procedures : "+f.getModel().listProcedures());
				}
				System.out.println();
			}
			System.out.println("User defined macros : ");
			for (Entry<String,String> e : project.getMacros().entrySet()) {
				System.out.println("\t"+e.getKey()+" = "+e.getValue());
			}
			return;
		}

		if (arg instanceof GecosSourceFile) {
			GecosSourceFile f = (GecosSourceFile) arg;
			System.out.println(f.getName());
			return;
		}

		if (arg instanceof Integer) {
			int i = (Integer) arg;
			System.out.println(i);
			return;
		}
		
		if (arg instanceof Procedure) {
			System.out.println(arg);
		}
	}
}
