/**
 */
package gecos.instrs;

import gecos.core.ISymbolUse;
import gecos.core.Symbol;

import gecos.types.Type;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Symbol Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.SymbolInstruction#getSymbol <em>Symbol</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getSymbolInstruction()
 * @model
 * @generated
 */
public interface SymbolInstruction extends Instruction, ISymbolUse {
	/**
	 * Returns the value of the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Symbol</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbol</em>' reference.
	 * @see #setSymbol(Symbol)
	 * @see gecos.instrs.InstrsPackage#getSymbolInstruction_Symbol()
	 * @model resolveProxies="false"
	 * @generated
	 */
	Symbol getSymbol();

	/**
	 * Sets the value of the '{@link gecos.instrs.SymbolInstruction#getSymbol <em>Symbol</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Symbol</em>' reference.
	 * @see #getSymbol()
	 * @generated
	 */
	void setSymbol(Symbol value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitSymbolInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getSymbol();'"
	 * @generated
	 */
	Symbol getUsedSymbol();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; sym = \"(sym=?)\";\n&lt;%gecos.core.Symbol%&gt; _symbol = this.getSymbol();\nboolean _tripleNotEquals = (_symbol != null);\nif (_tripleNotEquals)\n{\n\tsym = this.getSymbol().getName();\n}\nreturn sym;'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return false;'"
	 * @generated
	 */
	boolean isConstant();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return true;'"
	 * @generated
	 */
	boolean isSymbol();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.core.Symbol%&gt; _symbol = this.getSymbol();\nboolean _tripleNotEquals = (_symbol != null);\nif (_tripleNotEquals)\n{\n\treturn this.getSymbol().getType();\n}\nreturn null;'"
	 * @generated
	 */
	Type computeType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.SymbolInstruction%&gt;))\n{\n\treturn (super.isSame(instr) &amp;&amp; (((&lt;%gecos.instrs.SymbolInstruction%&gt;)instr).getSymbol() == this.getSymbol()));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" objUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return ((obj instanceof &lt;%gecos.instrs.SymbolInstruction%&gt;) &amp;&amp; (((&lt;%gecos.instrs.SymbolInstruction%&gt;) obj).getSymbol() == this.getSymbol()));'"
	 * @generated
	 */
	boolean equals(Object obj);

} // SymbolInstruction
