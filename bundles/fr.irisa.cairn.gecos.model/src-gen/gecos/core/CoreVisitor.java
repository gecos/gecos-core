/**
 */
package gecos.core;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visitor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.core.CorePackage#getCoreVisitor()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface CoreVisitor extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model procedureUnique="false"
	 * @generated
	 */
	void visitProcedure(Procedure procedure);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model procedureSetUnique="false"
	 * @generated
	 */
	void visitProcedureSet(ProcedureSet procedureSet);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model scopeContainerUnique="false"
	 * @generated
	 */
	void visitScopeContainer(ScopeContainer scopeContainer);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model scopeUnique="false"
	 * @generated
	 */
	void visitScope(Scope scope);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model symbolUnique="false"
	 * @generated
	 */
	void visitSymbol(Symbol symbol);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model paramSymbolUnique="false"
	 * @generated
	 */
	void visitParameterSymbol(ParameterSymbol paramSymbol);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model procedureSymbolUnique="false"
	 * @generated
	 */
	void visitProcedureSymbol(ProcedureSymbol procedureSymbol);

} // CoreVisitor
