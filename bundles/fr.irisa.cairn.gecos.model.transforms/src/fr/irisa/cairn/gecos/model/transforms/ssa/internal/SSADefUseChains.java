package fr.irisa.cairn.gecos.model.transforms.ssa.internal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;


import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import gecos.core.Procedure;
import gecos.instrs.SSADefSymbol;
import gecos.instrs.SSAUseSymbol;
import gecos.instrs.SetInstruction;

public class SSADefUseChains {

	HashMap<SSADefSymbol, List<SSAUseSymbol>> defUseMap;

	private class SSADefUseCollector extends BlockInstructionSwitch {
		
		List<SSADefSymbol> allSSADefs= new ArrayList<SSADefSymbol>(); 	
		List<SSAUseSymbol> allSSAUses= new ArrayList<SSAUseSymbol>();
		
		public SSADefUseCollector(Procedure p) {
			this.doSwitch(p);
		}
		public List<SSADefSymbol> getAllSSADefs() {
			return allSSADefs;
		}
		public List<SSAUseSymbol> getAllSSAUses() {
			return allSSAUses;
		}
		
		@Override
		public Object caseSSADefSymbol(SSADefSymbol object) {
			allSSADefs.add(object);
			return null;
		}
		
		@Override
		public Object caseSSAUseSymbol(SSAUseSymbol object) {
			allSSAUses.add(object);
			return null;
		}
	}

	public static SSADefSymbol getSink(SSAUseSymbol use) {
		if(use.getRoot() instanceof SetInstruction) {
			SetInstruction set = (SetInstruction) use.getRoot();
			if(set.getDest() instanceof SSADefSymbol) {
				return (SSADefSymbol) set.getDest();
			}
		}
		return null;
	}

	public SSADefUseChains(Procedure p) {
		SSADefUseCollector collector = new SSADefUseCollector(p) ;
		for (SSAUseSymbol use : collector.allSSAUses) {
			SSADefSymbol def = use.getDef();
			if(!defUseMap.containsKey(def)) {
				defUseMap.put(def,new ArrayList<SSAUseSymbol>());
			}
			defUseMap.get(def).add(use);
		}
	}
	
	public List<SSAUseSymbol> getUses(SSADefSymbol def) {
		return defUseMap.get(def);
	}
	
	public List<SSAUseSymbol> getAllIndirectUses(SSADefSymbol def) {
		HashSet<SSAUseSymbol> s = new HashSet<SSAUseSymbol>();
		return _getAllIndirectUses(s,def);
	}
	
	public List<SSAUseSymbol> _getAllIndirectUses(HashSet<SSAUseSymbol> s, SSADefSymbol def) {
		List<SSAUseSymbol> uses = getUses(def);
		s.addAll(uses);
		for (SSAUseSymbol use : uses) {
			SSADefSymbol sink = getSink(use);
			if(sink!=null) {
				_getAllIndirectUses(s, sink);
			}
		}
		return new ArrayList<SSAUseSymbol>(s);
	}

	
		
}
