/**
 */
package gecos.core;

import gecos.annotations.AnnotatedElement;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Procedure</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.core.Procedure#getStart <em>Start</em>}</li>
 *   <li>{@link gecos.core.Procedure#getBody <em>Body</em>}</li>
 *   <li>{@link gecos.core.Procedure#getEnd <em>End</em>}</li>
 *   <li>{@link gecos.core.Procedure#getSymbol <em>Symbol</em>}</li>
 *   <li>{@link gecos.core.Procedure#getContainingProcedureSet <em>Containing Procedure Set</em>}</li>
 * </ul>
 *
 * @see gecos.core.CorePackage#getProcedure()
 * @model annotation="gmf.node figure='rectangle'"
 * @generated
 */
public interface Procedure extends AnnotatedElement, CoreVisitable, ISymbolUse {
	/**
	 * Returns the value of the '<em><b>Start</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start</em>' reference.
	 * @see #setStart(BasicBlock)
	 * @see gecos.core.CorePackage#getProcedure_Start()
	 * @model resolveProxies="false"
	 *        annotation="gmf.compartment foo='bar'"
	 * @generated
	 */
	BasicBlock getStart();

	/**
	 * Sets the value of the '{@link gecos.core.Procedure#getStart <em>Start</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start</em>' reference.
	 * @see #getStart()
	 * @generated
	 */
	void setStart(BasicBlock value);

	/**
	 * Returns the value of the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body</em>' containment reference.
	 * @see #setBody(CompositeBlock)
	 * @see gecos.core.CorePackage#getProcedure_Body()
	 * @model containment="true"
	 *        annotation="gmf.compartment foo='bar'"
	 * @generated
	 */
	CompositeBlock getBody();

	/**
	 * Sets the value of the '{@link gecos.core.Procedure#getBody <em>Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body</em>' containment reference.
	 * @see #getBody()
	 * @generated
	 */
	void setBody(CompositeBlock value);

	/**
	 * Returns the value of the '<em><b>End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End</em>' reference.
	 * @see #setEnd(BasicBlock)
	 * @see gecos.core.CorePackage#getProcedure_End()
	 * @model resolveProxies="false"
	 *        annotation="gmf.compartment foo='bar'"
	 * @generated
	 */
	BasicBlock getEnd();

	/**
	 * Sets the value of the '{@link gecos.core.Procedure#getEnd <em>End</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End</em>' reference.
	 * @see #getEnd()
	 * @generated
	 */
	void setEnd(BasicBlock value);

	/**
	 * Returns the value of the '<em><b>Symbol</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link gecos.core.ProcedureSymbol#getProcedure <em>Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Symbol</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbol</em>' container reference.
	 * @see #setSymbol(ProcedureSymbol)
	 * @see gecos.core.CorePackage#getProcedure_Symbol()
	 * @see gecos.core.ProcedureSymbol#getProcedure
	 * @model opposite="procedure" transient="false"
	 * @generated
	 */
	ProcedureSymbol getSymbol();

	/**
	 * Sets the value of the '{@link gecos.core.Procedure#getSymbol <em>Symbol</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Symbol</em>' container reference.
	 * @see #getSymbol()
	 * @generated
	 */
	void setSymbol(ProcedureSymbol value);

	/**
	 * Returns the value of the '<em><b>Containing Procedure Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Procedure Set</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Procedure Set</em>' reference.
	 * @see gecos.core.CorePackage#getProcedure_ContainingProcedureSet()
	 * @model resolveProxies="false" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='if ((((this.getSymbol() != null) &amp;&amp; (this.getSymbol().getContainingScope() != null)) &amp;&amp; (this.getSymbol().getContainingScope().getContainer() instanceof &lt;%gecos.core.ProcedureSet%&gt;)))\n{\n\t&lt;%gecos.core.ScopeContainer%&gt; _container = this.getSymbol().getContainingScope().getContainer();\n\treturn ((&lt;%gecos.core.ProcedureSet%&gt;) _container);\n}\nreturn null;'"
	 * @generated
	 */
	ProcedureSet getContainingProcedureSet();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getSymbol().listParameters();'"
	 * @generated
	 */
	EList<ParameterSymbol> listParameters();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%fr.irisa.cairn.tools.ecore.query.EMFUtils%&gt;.&lt;&lt;%gecos.blocks.BasicBlock%&gt;&gt;eAllContentsFirstInstancesOf(this, &lt;%gecos.blocks.BasicBlock%&gt;.class);'"
	 * @generated
	 */
	EList<BasicBlock> getBasicBlocks();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitProcedure(this);'"
	 * @generated
	 */
	void accept(CoreVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.core.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _valueOf = &lt;%java.lang.String%&gt;.valueOf(this.getSymbol());\nreturn (\"Procedure: \" + _valueOf);'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getSymbol().getScope();'"
	 * @generated
	 */
	Scope getScope();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Search a {@link Block} in the whole procedure and replace it by another
	 * one.
	 * 
	 * @param oldb replaced block
	 * @param newb new block
	 * <!-- end-model-doc -->
	 * @model oldBlkUnique="false" newBlkUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%fr.irisa.cairn.gecos.model.tools.utils.BlocksUpdate%&gt; update = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.BlocksUpdate%&gt;(oldBlk, newBlk);\nupdate.doSwitch(this);\n&lt;%gecos.blocks.BasicBlock%&gt; _start = this.getStart();\nboolean _tripleEquals = (_start == oldBlk);\nif (_tripleEquals)\n{\n\tthis.setStart(((&lt;%gecos.blocks.BasicBlock%&gt;) newBlk));\n}\nelse\n{\n\t&lt;%gecos.blocks.BasicBlock%&gt; _end = this.getEnd();\n\tboolean _tripleEquals_1 = (_end == oldBlk);\n\tif (_tripleEquals_1)\n\t{\n\t\tthis.setEnd(((&lt;%gecos.blocks.BasicBlock%&gt;) newBlk));\n\t}\n\telse\n\t{\n\t\t&lt;%gecos.blocks.CompositeBlock%&gt; _body = this.getBody();\n\t\tboolean _tripleEquals_2 = (_body == oldBlk);\n\t\tif (_tripleEquals_2)\n\t\t{\n\t\t\tthis.setBody(((&lt;%gecos.blocks.CompositeBlock%&gt;) newBlk));\n\t\t}\n\t\telse\n\t\t{\n\t\t\tthis.getBody().replace(oldBlk, newBlk);\n\t\t}\n\t}\n}'"
	 * @generated
	 */
	void replaceBlock(Block oldBlk, Block newBlk);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%fr.irisa.cairn.gecos.model.tools.utils.GecosCopier%&gt; gecosCopier = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.GecosCopier%&gt;();\n&lt;%org.eclipse.emf.ecore.EObject%&gt; _copy = gecosCopier.copy(this);\nfinal &lt;%gecos.core.Procedure%&gt; procedure = ((&lt;%gecos.core.Procedure%&gt;) _copy);\ngecosCopier.copyReferences();\nreturn procedure;'"
	 * @generated
	 */
	Procedure copy();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getSymbol();'"
	 * @generated
	 */
	Symbol getUsedSymbol();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model newSymbolUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((newSymbol instanceof &lt;%gecos.core.ProcedureSymbol%&gt;))\n{\n\t((&lt;%gecos.core.ProcedureSymbol%&gt;)newSymbol).setProcedure(this);\n}\nelse\n{\n\tthrow new &lt;%java.lang.IllegalArgumentException%&gt;();\n}'"
	 * @generated
	 */
	void setSymbol(Symbol newSymbol);

} // Procedure
