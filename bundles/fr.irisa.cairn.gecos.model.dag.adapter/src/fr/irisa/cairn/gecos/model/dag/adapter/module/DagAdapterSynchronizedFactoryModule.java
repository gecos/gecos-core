package fr.irisa.cairn.gecos.model.dag.adapter.module;

import com.google.inject.Singleton;

import fr.irisa.cairn.componentElement.ServiceModule;
import fr.irisa.cairn.gecos.model.dag.adapter.builder.DagAdapterSynchronizedFactory;


public class DagAdapterSynchronizedFactoryModule extends ServiceModule {
	
	protected void bindSynchronizedFactory(){
		this.bind(DagAdapterSynchronizedFactory.class).in(Singleton.class);
	}
	
	@Override
	protected void configure() {
		bindSynchronizedFactory();
	}

}
