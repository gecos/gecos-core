package fr.irisa.cairn.gecos.model.transforms.procedures.outlining;

import java.security.InvalidParameterException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.gecos.model.transforms.ssa.SSAAnalyser;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import fr.irisa.r2d2.gecos.framework.GSArg;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import gecos.annotations.AnnotatedElement;
import gecos.annotations.PragmaAnnotation;
import gecos.blocks.Block;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;


//?? does it make sense to make procedureSymbole a Declaration ? parametrScope? does scopping remain consistent when inlining/outlining ??
@GSModule("Outlines blocks annotated with an outline pragma.")
public class ProcedureOutlining {
	
	static class PragmaDefinition {
		String name;

		public PragmaDefinition(String name) {
			this.name = name;
		}
		
		public static String getPragmaName(PragmaAnnotation pragma) {
			return pragma.getContent().get(0);
		}
		
		public static List<String> getPragmaArgs(PragmaAnnotation pragma) {
			EList<String> content = pragma.getContent();
			return content.subList(1, content.size());
		}
		
		public boolean isInstance(PragmaAnnotation pragma) {
			return pragma != null && Objects.equals(getPragmaName(pragma), name); //FIXME multiple pragmas are fused !!
		}
	}
	
	public static class AnnotationsUtils {
		public static <T extends EObject> List<T> findAnnotatedContents(EObject inRoot, PragmaDefinition pragmaDef, Class<T> instanceOf) {
			EList<PragmaAnnotation> pragmaAnnotations = EMFUtils.<PragmaAnnotation>eAllContentsInstancesOf(inRoot, PragmaAnnotation.class);
			return pragmaAnnotations.stream()
				.filter(Objects::nonNull)
				.filter(pragmaDef::isInstance)
				.map(PragmaAnnotation::getAnnotatedElement)
				.filter(instanceOf::isInstance)
				.map(instanceOf::cast)
				.collect(Collectors.toList());
		}
		
		public static void removePragma(PragmaDefinition pragmaDef, AnnotatedElement fromElt) {
			PragmaAnnotation pg = fromElt.getPragma();
			if(pragmaDef.isInstance(pg))
				fromElt.getAnnotations().remove(pg.eContainer()); //FIXME multiple pragmas are fused !!
		}
	}
	
	
	////////////////////////////////////////////////////
	
	static final PragmaDefinition BLOCK_OUTLINE_PRAGMA = new PragmaDefinition("outline");

	private EObject root;
	private boolean outlineInNewFile;
	
	@GSModuleConstructor(args = {
		@GSArg(name = "proj", info = "Project to inspect for outlining.")
	})	
	public ProcedureOutlining(GecosProject proj) {
		this(proj, false);
	}
	
	@GSModuleConstructor(args = {
		@GSArg(name = "proj", info = "Project to inspect for outlining."),
		@GSArg(name = "outlineInNewFile", info = "Whether to outline in a separate file.")
	})
	public ProcedureOutlining(GecosProject proj, boolean outlineInNewFile) {
		this.root = proj;
		this.outlineInNewFile = outlineInNewFile;
	}
	
	@GSModuleConstructor(args = {
		@GSArg(name = "ps", info = "Procedure set to inspect for outlining.")
	})
	public ProcedureOutlining(ProcedureSet ps) {
		this.root = ps;
		this.outlineInNewFile = false;
	}	
	
	public void compute() {
		List<Block> blocksToOutline = AnnotationsUtils.<Block>findAnnotatedContents(root, BLOCK_OUTLINE_PRAGMA, Block.class);
		
		for (Block blockToOutline: blocksToOutline) {
			if (blockToOutline.getContainingProcedure().getAnnotation(SSAAnalyser.SSATAG) == null) {
				throw new InvalidParameterException("Cannot outline a block which isn't in SSA form.");
			}
		}
		
		//TODO in case there are embedded outline blocks, start by the innermost first.
		
		blocksToOutline.forEach(b -> new BlockOutliner(b, (GecosProject)root, outlineInNewFile).outline());
	}
	
}
