/**
 */
package gecos.instrs;

import gecos.blocks.BasicBlock;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Complex Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.instrs.InstrsPackage#getComplexInstruction()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ComplexInstruction extends Instruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This returns an unmodifiable, ordered list of the contained children.
	 * The returned list is never {@code null}, but it might contain {@code null} elements.
	 * 
	 * The index (order) of an element matches that given by {@link #indexOfChild(Instruction)}
	 * and can be used to manipulate it using {@link #removeChildAt(int)} and {@link #getChildAt(int)}.
	 * 
	 * @return an Unmodifiable, ordered list of contained children, never {@code null}.
	 * <!-- end-model-doc -->
	 * @model unique="false"
	 * @generated
	 */
	EList<Instruction> listChildren();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This is equivalent to {@code this.listChildren()} with filtering of all {@code null} elements.
	 * <br>
	 * Note: the order of elements in the returned list is meaningless!
	 * 
	 * @return an Unmodifiable list of valid (i.e. non-null) contained children, never {@code null}.
	 * <!-- end-model-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;asEList(((&lt;%gecos.instrs.Instruction%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;filterNull(this.listChildren()), &lt;%gecos.instrs.Instruction%&gt;.class))));'"
	 * @generated
	 */
	EList<Instruction> listValidChildren();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Note: The returned value might be different from {@code this.listChildren().indexOf(child)}
	 * since this method uses '==' operator instead of 'equals()'!
	 * 
	 * @return the index of the first occurrence of the specified {@code child} (using '==')
	 * in the list of children obtained by {@link #listChildren()}, or -1 if none was found.
	 * <!-- end-model-doc -->
	 * @model dataType="gecos.instrs.int" unique="false" childUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _size = this.listChildren().size();\n&lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt; _doubleDotLessThan = new &lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt;(0, _size, true);\nfor (final &lt;%java.lang.Integer%&gt; i : _doubleDotLessThan)\n{\n\t&lt;%gecos.instrs.Instruction%&gt; _get = this.listChildren().get((i).intValue());\n\tboolean _tripleEquals = (_get == child);\n\tif (_tripleEquals)\n\t{\n\t\treturn (i).intValue();\n\t}\n}\nreturn (-1);'"
	 * @generated
	 */
	int indexOfChild(Instruction child);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This is equivalent to {@code this.listChildren().get(index)}.
	 * 
	 * @return the child at the specified {@code index}, it might be {@code null}.
	 * 
	 * @throws IndexOutOfBoundsException if the index is out of range (index < 0 || index >= size())
	 * <!-- end-model-doc -->
	 * @model unique="false" indexDataType="gecos.instrs.int" indexUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.listChildren().get(index);'"
	 * @generated
	 */
	Instruction getChildAt(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This is equivalent to {@code this.removeChild(getChildAt(index))}
	 * <!-- end-model-doc -->
	 * @model indexDataType="gecos.instrs.int" indexUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.removeChild(this.getChildAt(index));'"
	 * @generated
	 */
	void removeChildAt(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Removes (or Unset) the specified {@code instr} in case it is contained by {@code this}.
	 * Do nothing otherwise.
	 * <br>
	 * Notes:
	 * <li> In case the containing feature is not many-valued (i.e. not a EList), it is
	 * unset (i.e. {@code container.eUnset(feature)}.
	 * <li> Calling {@code this.listChildren().size()} after this operation might return the same value,
	 * but {@code this.listValidChildren().size()} must decrease by one.
	 * <!-- end-model-doc -->
	 * @model instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.ecore.EObject%&gt; _eContainer = instr.eContainer();\nboolean _tripleEquals = (_eContainer == this);\nif (_tripleEquals)\n{\n\t&lt;%org.eclipse.emf.ecore.util.EcoreUtil%&gt;.remove(((&lt;%org.eclipse.emf.ecore.EObject%&gt;) instr));\n}'"
	 * @generated
	 */
	void removeChild(Instruction instr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model oldInstrUnique="false" newInstrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.ecore.EObject%&gt; _eContainer = oldInstr.eContainer();\nboolean _tripleNotEquals = (_eContainer != this);\nif (_tripleNotEquals)\n{\n\tthrow new &lt;%java.lang.IllegalArgumentException%&gt;(((((\"[\" + oldInstr) + \"] is not contained by [\") + this) + \"]\"));\n}\n&lt;%org.eclipse.emf.ecore.util.EcoreUtil%&gt;.replace(((&lt;%org.eclipse.emf.ecore.EObject%&gt;) oldInstr), newInstr);'"
	 * @generated
	 */
	void replaceChild(Instruction oldInstr, Instruction newInstr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This is equivalent to {@code this.listChildren().size()}.
	 * <!-- end-model-doc -->
	 * @model kind="operation" dataType="gecos.instrs.int" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.listChildren().size();'"
	 * @generated
	 */
	int getChildrenCount();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return the {@link BasicBlock} that contains the root (see {@link #getRoot())
	 * of {@code this} instruction, null if not found.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.ecore.EObject%&gt; c = this.getRoot().eContainer();\nif ((c instanceof &lt;%gecos.blocks.BasicBlock%&gt;))\n{\n\treturn ((&lt;%gecos.blocks.BasicBlock%&gt;)c);\n}\nreturn null;'"
	 * @generated
	 */
	BasicBlock getBlock();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @deprecated use {@link #getRoot()} instead
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getRoot();'"
	 * @generated
	 */
	Instruction getRootInstuction();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.ComplexInstruction%&gt;))\n{\n\tint _size = this.listChildren().size();\n\tint _size_1 = ((&lt;%gecos.instrs.ComplexInstruction%&gt;)instr).listChildren().size();\n\tboolean _notEquals = (_size != _size_1);\n\tif (_notEquals)\n\t{\n\t\treturn false;\n\t}\n\tint _size_2 = this.listChildren().size();\n\tfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%java.lang.Integer%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%java.lang.Integer%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n\t{\n\t\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%java.lang.Integer%&gt; it)\n\t\t{\n\t\t\tfinal &lt;%gecos.instrs.Instruction%&gt; c1 = &lt;%this%&gt;.listChildren().get((it).intValue());\n\t\t\tfinal &lt;%gecos.instrs.Instruction%&gt; c2 = ((&lt;%gecos.instrs.ComplexInstruction%&gt;)instr).listChildren().get((it).intValue());\n\t\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf((((c1 == null) &amp;&amp; (c2 == null)) || c1.isSame(c2)));\n\t\t}\n\t};\n\tfinal boolean sameChildrens = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%java.lang.Integer%&gt;&gt;forall(new &lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt;(0, _size_2, true), _function);\n\treturn (sameChildrens &amp;&amp; super.isSame(instr));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // ComplexInstruction
