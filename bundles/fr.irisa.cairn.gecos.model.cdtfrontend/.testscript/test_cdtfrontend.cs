debug(1);

echo("start CDT Frontend test");
CDTUseGecosStandardIncludes();

#sources = FindFiles("src-c/",".c");
sources = {"src-c/toto.cpp"};
#sources = {"src-c/test.c"};


for src in sources do

	echo(" - testing "+src);
	proj = CreateGecosProject("test incs");
	AddSourceToGecosProject(proj,src);
	CDTFrontend(proj);
	SetBitAccurateBackend("VivadoAP");
	CGenerator(proj,"regen/");
	SaveGecosProject(proj,"project_"+src);
	GecosDottyExport("./dots/",proj); 
	GecosTreeToDAGIRConversion(proj);
	GecosDottyExport("./dots_dag/",proj); 
done;

echo("end CDT Frontend test");
