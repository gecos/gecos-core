/**
 */
package gecos.annotations;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IAST Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.annotations.IASTAnnotation#getIastNode <em>Iast Node</em>}</li>
 * </ul>
 *
 * @see gecos.annotations.AnnotationsPackage#getIASTAnnotation()
 * @model
 * @generated
 */
public interface IASTAnnotation extends IAnnotation {
	/**
	 * Returns the value of the '<em><b>Iast Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iast Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iast Node</em>' reference.
	 * @see #setIastNode(EObject)
	 * @see gecos.annotations.AnnotationsPackage#getIASTAnnotation_IastNode()
	 * @model resolveProxies="false"
	 * @generated
	 */
	EObject getIastNode();

	/**
	 * Sets the value of the '{@link gecos.annotations.IASTAnnotation#getIastNode <em>Iast Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iast Node</em>' reference.
	 * @see #getIastNode()
	 * @generated
	 */
	void setIastNode(EObject value);

} // IASTAnnotation
