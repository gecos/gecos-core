package fr.irisa.cairn.gecos.core.cdtfrontend.tests;

import org.junit.Test;

import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.core.testframework.impl.GSProjectVersion;
import fr.irisa.cairn.gecos.core.testframework.impl.GecosS2STestTemplate;
import fr.irisa.cairn.gecos.model.cdtfrontend.CDTFrontEnd;
import fr.irisa.cairn.gecos.testframework.data.ICxxProjectData;
import fr.irisa.cairn.gecos.testframework.dataprovider.DataFromPathProvider;
import fr.irisa.cairn.gecos.testframework.dataprovider.ResourcesLocation;
import fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName;
import fr.irisa.cairn.gecos.testframework.stages.Stages;
import gecos.gecosproject.GecosProject;

/**
 * Tests C front-end {@link CDTFrontEnd} and the C code generator.
 * 
 * @author aelmouss
 */
public class GccCompileIT extends GecosS2STestTemplate<GSProjectVersion> {
	
	/**
	 * The project is the same in both versions,
	 * however for the original version the code generator does not generate 
	 * code from the {@link GecosProject} but rather copy original sources.
	 */
	@Test
	@UseDataProvider(location=DataFromPathProvider.class, value=DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation(bundleName="fr.irisa.cairn.gecos.testframework.resources", value = "src-c/single/gcc/compile/pass/fast", limit = 30)
//	@UseProviderWithMulti(SDP_single_gcc_compile_pass_slow.class)
//	@UseProviderWithMulti(SDP_single_gcc_compile_fail.class)
	public void gccCompile(ICxxProjectData d) {
		/**
		 * TODO we should only skip on "not-yet-supported" exception in CDTFrontend
		 * instead of all CDTParserProblemException this way we only skip
		 * the code that fails to parse because it is not yet supported, while
		 * code that fails for other reasons should be reported as Failure.
		 */
		skipOnParsingFail = true; //skip tests that cannot be parsed.  
		
		//stop before link stage
		findTestFlow(d.getClass()).replaceStage(S2SStageName.LINK, Stages.STOP);
		
		runTest(d, p -> {}); 
	}
	
}
