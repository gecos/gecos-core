package fr.irisa.cairn.gecos.model.analysis.types;

import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import gecos.types.ACFixedType;
import gecos.types.AliasType;
import gecos.types.BaseType;
import gecos.types.BoolType;
import gecos.types.FloatType;
import gecos.types.FunctionType;
import gecos.types.IntegerType;
import gecos.types.PtrType;
import gecos.types.RecordType;
import gecos.types.SimdType;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import gecos.types.VoidType;

/**
 * Represent the base level of a {@link Type}. 
 * That is the outermost non-pointer, non-array type
 * ! It considers only one Aliasing level.
 * 
 * @author aelmouss
 */
public class BaseLevel implements TypeLevel {
	
	private Type base = null;
	private AliasType alias = null;
	private boolean isConst = false;
	private boolean isStatic = false;
	//TODO add isRegister and isVolatile ...
	
	
	private BaseLevel() {};
	
	public static BaseLevel constructFrom(Type type, TypeAnalyzer typeAnalyzer) {
//		type = typeAnalyzer.skipAliases(type); //TODO
		
		BaseLevel level = new BaseLevel();
		
//		if(type instanceof StaticType) {
//			level.isStatic = true;
//			type = ((StaticType) type).getBase();
//		}
//		if(type instanceof ConstType) {
//			level.isConst = true;
//			type = ((ConstType) type).getBase();
//		}
//		if(type instanceof StaticType) {
//			level.isStatic = true;
//			type = ((StaticType) type).getBase();
//		}
		
		level.isConst = type.isConstant(); //XXX
		level.isStatic = type.getStorageClass().equals(StorageClassSpecifiers.STATIC);
		
		if(type instanceof AliasType) {
			level.alias = ((AliasType) type);
			type = ((AliasType) type).getAlias();
		}
		
		level.base = type;
		
		return level;
	}

	@Override
	public Type revertOn(Type type) {
		if(isStatic)
			type.setStorageClass(StorageClassSpecifiers.STATIC);
		if(isConst)
			type.setConstant(true);
		return type;
	}
	
	public Type getBase() {
		return base;
	}
	
	public AliasType getAlias() {
		return alias;
	}
	
	/**
	 * @return the {@link Type} represented by this {@link TypeLevel} 
	 * without considering static and const qualifiers.
	 */
	public Type skipStaticConst() {
		if(alias != null)
			return alias;
		return base;
	}
	
	/**
	 * @return true if BASE is {@link AliasType} (i.e. this {@link BaseLevel} is an alias of alias).
	 * <br>NOTE: this is different from ({@code getAlias() != null})
	 */
	public boolean isAlias() {
		return base instanceof AliasType;
	}
	
	public boolean isConst() {
		return isConst;
	}
	
	public boolean isStatic() {
		return isStatic;
	}
	
	public boolean isRecord() {
		return base instanceof RecordType;
	}
	
	/**
	 * @return true if BASE is {@link BaseType}.
	 */
	public boolean isBaseType() {
		return base instanceof BaseType;
	}
	
//	public boolean isSignedBaseType() {
//		return isBaseType() && ((BaseType)base).isSigned();
//	}
	
	public boolean isVoid() {
		return isVoidType(base);
	}
	
	public boolean isBool(){
		return base instanceof BoolType;
	}
	
	public boolean isInt() {
		return base instanceof IntegerType;
	}
	
	public boolean isInt64() {
		return isInt() && ((BaseType)base).getSize() == 64;
	}
	
	public boolean isInt32() {
		return isInt() && ((BaseType)base).getSize() == 32;
	}
	
	public boolean isInt16() {
		return isInt() && ((BaseType)base).getSize() == 16;
	}
	
	public boolean isInt8() {
		return isInt() && ((BaseType)base).getSize() == 8;
	}
	
	public boolean isFloat() {
		return base instanceof FloatType;
	}
	
	public boolean isFloatSingle() {
		return isFloat() && ((BaseType)base).getSize() == GecosUserTypeFactory.TypeSizes.defaultFloatSize;
	}
	
	public boolean isFloatDouble() {
		return isFloat() && ((BaseType)base).getSize() == GecosUserTypeFactory.TypeSizes.defaultDoubleSize;
	}
	
	
	public boolean isFixedPoint() {
		return (base instanceof ACFixedType);
	}
	
	/**
	 * @return true if BASE is {@link FunctionType}.
	 */
	public boolean isFunction() {
		return (base instanceof FunctionType);
	}
	
	/**
	 * @return true if BASE is {@link SimdType}.
	 */
	public boolean isSimd() {
		return (base instanceof SimdType);
	}
	
	/**
	 * @return true if BASE is {@link PtrType}.
	 * i.e. this {@link BaseLevel} is an alias to {@link PtrType}.
	 */
	public boolean isPointer() {
		return (base instanceof PtrType);
	}
	
	static private boolean isVoidType(Type type) {
		return type instanceof VoidType;
	}
	
	public IntegerType getBaseAsInt() throws ClassCastException {
		return (IntegerType) base;
	}
	
	public FloatType getBaseAsFloat() throws ClassCastException {
		return (FloatType) base;
	}
	
	public ACFixedType getBaseAsFixedPoint() throws ClassCastException {
		return (ACFixedType) base;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(isConst()? "const " : "");
		sb.append(isStatic()? "static " : "");
		sb.append(alias == null ? "" : getAlias().getName());
		sb.append(" " + getBase());
		return sb.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof BaseLevel))
			return false;
		
		BaseLevel other = (BaseLevel) obj;
		Type _base = alias != null ? alias : base;
	
		return 	isConst == other.isConst() &&
				isStatic == other.isStatic() &&
				_base.isEqual(other.getBase());
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alias == null) ? 0 : alias.hashCode());
		result = prime * result + ((base == null) ? 0 : base.hashCode());
		result = prime * result + (isConst ? 1231 : 1237);
		result = prime * result + (isStatic ? 1231 : 1237);
		return result;
	}
	
	@Override
	public boolean isSimilar(TypeLevel obj) {
		if(!(obj instanceof BaseLevel))
			return false;
		
		BaseLevel other = (BaseLevel) obj;
		return base.isEqual(other.getBase(), true, true, true);
	}

}
