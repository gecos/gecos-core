package fr.irisa.cairn.gecos.ui.handlers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;

import fr.irisa.r2d2.gecos.framework.utils.GecosScriptLauncher;

/**
 * Handler that serialize the current procedure set
 * 
 * @author antoine
 * 
 */
public class GecosSerializerHandler extends AbstactGecosCFileHandler {

	@Override
	public void handle(IFile file) {
		String scriptFileName = ".serializer-script.cs";
		if (file != null) {
			String scriptFile = file.getProject().getLocation().toString() + "/" + scriptFileName;
			if (!(new File(scriptFile).exists())) {
				try {
					PrintStream printStream = new PrintStream(new File(scriptFile));
					printStream.append(getDefaultScript());
					printStream.close();
				} catch (FileNotFoundException e) {
					throw new RuntimeException(e);
				}
			}
			String osString = file.getRawLocation().toOSString();
			GecosScriptLauncher.launch(scriptFile, true, true, osString);
		}

		
		final IProject p = file.getProject();
		try {
			p.getWorkspace().run(new IWorkspaceRunnable() {
				@Override
				public void run(IProgressMonitor monitor) throws CoreException {
					p.refreshLocal(IResource.DEPTH_INFINITE, monitor);
				}
			}, new NullProgressMonitor());
		} catch (CoreException e) {}
	}

	private CharSequence getDefaultScript() {
		SerializerScriptGenerator generator = new SerializerScriptGenerator();
		return generator.generate();
	}


}
