#ifndef __PROFILING_ENGINE_H__
#define __PROFILING_ENGINE_H__

#include <string>
#include <cstring>
#include <stdlib.h>


namespace profengine {

	class Variable;
	class Generator;
	class VariablesMap;

    class ProfilingEngine {
	private:
		VariablesMap *m_values; //saved variables <uid, variable>
		Generator *generator;

		typedef double _DOUBLE_TYPE_;

	public:
		ProfilingEngine();
		~ProfilingEngine();

		void pushValue(int uid, const char* name, _DOUBLE_TYPE_ value);
		void saveSnapshot(int uid);
		Variable* getVariable(int uid);
		void saveVariable(int uid, Variable *variable);
		void writeTxt(std::string outputFolderPath);
		void writeBin(std::string outputFolderPath);
		void setGeneratorSeed(int seed);
		void setGeneratorUniformIntDistribution(int minInclusive, int maxInclusive);
		void setGeneratorUniformDoubleDistribution(double minInclusive, double maxExclusive);
		void setGeneratorNormalIntDistribution(double mean, double stddev);
		void setGeneratorNormalDoubleDistribution(double mean, double stddev);
		void setGeneratorFromFile(const char *filePath);
		void setGeneratorFromVar(double var);
//		void setGeneratorFromVar(double *var);
		double generate();
    };

}

//----------------------------------------------------------------------------------------
//  Collect values API
//----------------------------------------------------------------------------------------


static profengine::ProfilingEngine *__internal_sim_manager;

void ProfilingEngine_start() {
	__internal_sim_manager = new profengine::ProfilingEngine;
}

void ProfilingEngine_start_seeded(const int seed) {
	__internal_sim_manager = new profengine::ProfilingEngine;
	if (seed >= 0)
		__internal_sim_manager->setGeneratorSeed(seed);
}

void ProfilingEngine_finalize(const char* outputFolderPath) {
#ifdef _WRITE_TXT_
	__internal_sim_manager->writeTxt(std::string(outputFolderPath));
#else
	__internal_sim_manager->writeBin(std::string(outputFolderPath));
#endif
	delete __internal_sim_manager;
}

//void ProfilingEngine_pushValue(int uid, const char* name, T value) {
//	__internal_sim_manager->pushValue(uid, name, value);
//}

#define ProfilingEngine_pushValue(uid, name, value) \
	__internal_sim_manager->pushValue(uid, name, value); //FIXME !!!! the results are different if we add an explicit cast to double or not !!!!


#define ProfilingEngine_saveSnapshot(/*int*/ uid) \
	__internal_sim_manager->saveSnapshot(uid);
	

//----------------------------------------------------------------------------------------
//  Inject values API
//----------------------------------------------------------------------------------------

#define ProfilingEngine_initInjector_fromVar(var)  \
	__internal_sim_manager->setGeneratorFromVar(var)

#define ProfilingEngine_initInjector_fromFile(filePath) \
	__internal_sim_manager->setGeneratorFromFile(filePath)

#define ProfilingEngine_initInjector_RandomSeed(newSeed) \
	__internal_sim_manager->setGeneratorSeed(newSeed);

#define ProfilingEngine_initInjector_RandomUniformInt(minInclusive, maxInclusive) \
	__internal_sim_manager->setGeneratorUniformIntDistribution(minInclusive, maxInclusive);

#define ProfilingEngine_initInjector_RandomUniformDouble(minInclusive, maxInclusive) \
	__internal_sim_manager->setGeneratorUniformDoubleDistribution(minInclusive, maxInclusive);

#define ProfilingEngine_initInjector_RandomNormalInt(mean, stddev) \
	__internal_sim_manager->setGeneratorNormalIntDistribution(mean, stddev);

#define ProfilingEngine_initInjector_RandomNormalDouble(mean, stddev) \
	__internal_sim_manager->setGeneratorNormalDoubleDistribution(mean, stddev);

#define ProfilingEngine_injectValue() \
	__internal_sim_manager->generate()


#endif // __PROFILING_ENGINE_H__
