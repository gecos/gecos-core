package fr.irisa.cairn.gecos.model.tools.utils;

import java.util.LinkedHashMap;
import java.util.Map;

import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDefaultVisitor;
import gecos.core.Symbol;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.SymbolInstruction;

/**
 * not tested : replace Symbol as Key by Instruction as Value.
 * @author amorvan
 * @generated NOT
 */
public class InstrsSubstitution extends GecosBlocksInstructionsDefaultVisitor {
	private Map<Symbol,Instruction> map;
	
	public InstrsSubstitution(Map<Symbol,Instruction> m) {
		this.map = m;
	}

	public InstrsSubstitution(Symbol olds,Instruction news) {
		this.map = new LinkedHashMap<Symbol,Instruction>();
		this.map.put(olds,news);
	}
	
	@Override
	public void visitSymbolInstruction(SymbolInstruction i) {
		
		if (map.containsKey(i.getSymbol())) {
//			System.out.println("replace >> "+i+" by "+map.get(i.getSymbol())+" (type : "+map.get(i.getSymbol()).getClass().getSimpleName()+")");
			ComplexInstruction parent = (ComplexInstruction) i.eContainer();
			if (parent!=null) {
				parent.replaceChild(i, map.get(i.getSymbol()).copy());
			} else {
				
			}
		}
	}


}
