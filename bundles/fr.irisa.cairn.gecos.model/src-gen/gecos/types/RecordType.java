/**
 */
package gecos.types;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Record Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.RecordType#getFields <em>Fields</em>}</li>
 *   <li>{@link gecos.types.RecordType#getKind <em>Kind</em>}</li>
 *   <li>{@link gecos.types.RecordType#isDefined <em>Defined</em>}</li>
 *   <li>{@link gecos.types.RecordType#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see gecos.types.TypesPackage#getRecordType()
 * @model
 * @generated
 */
public interface RecordType extends Type, TypesVisitable {
	/**
	 * Returns the value of the '<em><b>Fields</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.types.Field}.
	 * It is bidirectional and its opposite is '{@link gecos.types.Field#getRecord <em>Record</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fields</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fields</em>' containment reference list.
	 * @see gecos.types.TypesPackage#getRecordType_Fields()
	 * @see gecos.types.Field#getRecord
	 * @model opposite="record" containment="true"
	 * @generated
	 */
	EList<Field> getFields();

	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link gecos.types.Kinds}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see gecos.types.Kinds
	 * @see #setKind(Kinds)
	 * @see gecos.types.TypesPackage#getRecordType_Kind()
	 * @model unique="false"
	 * @generated
	 */
	Kinds getKind();

	/**
	 * Sets the value of the '{@link gecos.types.RecordType#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see gecos.types.Kinds
	 * @see #getKind()
	 * @generated
	 */
	void setKind(Kinds value);

	/**
	 * Returns the value of the '<em><b>Defined</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defined</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defined</em>' attribute.
	 * @see #setDefined(boolean)
	 * @see gecos.types.TypesPackage#getRecordType_Defined()
	 * @model unique="false" dataType="gecos.types.boolean"
	 * @generated
	 */
	boolean isDefined();

	/**
	 * Sets the value of the '{@link gecos.types.RecordType#isDefined <em>Defined</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defined</em>' attribute.
	 * @see #isDefined()
	 * @generated
	 */
	void setDefined(boolean value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * The default value is <code>"anonymous"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see gecos.types.TypesPackage#getRecordType_Name()
	 * @model default="anonymous" unique="false" dataType="gecos.types.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link gecos.types.RecordType#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.types.long" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.Long%&gt; _switchResult = null;\n&lt;%gecos.types.Kinds%&gt; _kind = this.getKind();\nif (_kind != null)\n{\n\tswitch (_kind)\n\t{\n\t\tcase UNION:\n\t\t\tfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.types.Field%&gt;, &lt;%java.lang.Long%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.types.Field%&gt;, &lt;%java.lang.Long%&gt;&gt;()\n\t\t\t{\n\t\t\t\tpublic &lt;%java.lang.Long%&gt; apply(final &lt;%gecos.types.Field%&gt; it)\n\t\t\t\t{\n\t\t\t\t\treturn &lt;%java.lang.Long%&gt;.valueOf(it.getType().getSize());\n\t\t\t\t}\n\t\t\t};\n\t\t\t_switchResult = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%java.lang.Long%&gt;&gt;max(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%gecos.types.Field%&gt;, &lt;%java.lang.Long%&gt;&gt;map(this.getFields(), _function));\n\t\t\tbreak;\n\t\tcase STRUCT:\n\t\t\tfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.types.Field%&gt;, &lt;%java.lang.Long%&gt;&gt; _function_1 = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.types.Field%&gt;, &lt;%java.lang.Long%&gt;&gt;()\n\t\t\t{\n\t\t\t\tpublic &lt;%java.lang.Long%&gt; apply(final &lt;%gecos.types.Field%&gt; it)\n\t\t\t\t{\n\t\t\t\t\tlong _xifexpression = (long) 0;\n\t\t\t\t\tlong _bitwidth = it.getBitwidth();\n\t\t\t\t\tboolean _equals = (_bitwidth == (-1));\n\t\t\t\t\tif (_equals)\n\t\t\t\t\t{\n\t\t\t\t\t\t_xifexpression = it.getType().getSize();\n\t\t\t\t\t}\n\t\t\t\t\telse\n\t\t\t\t\t{\n\t\t\t\t\t\t_xifexpression = it.getBitwidth();\n\t\t\t\t\t}\n\t\t\t\t\treturn &lt;%java.lang.Long%&gt;.valueOf(_xifexpression);\n\t\t\t\t}\n\t\t\t};\n\t\t\tfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function2%&gt;&lt;&lt;%java.lang.Long%&gt;, &lt;%java.lang.Long%&gt;, &lt;%java.lang.Long%&gt;&gt; _function_2 = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function2%&gt;&lt;&lt;%java.lang.Long%&gt;, &lt;%java.lang.Long%&gt;, &lt;%java.lang.Long%&gt;&gt;()\n\t\t\t{\n\t\t\t\tpublic &lt;%java.lang.Long%&gt; apply(final &lt;%java.lang.Long%&gt; s1, final &lt;%java.lang.Long%&gt; s2)\n\t\t\t\t{\n\t\t\t\t\treturn &lt;%java.lang.Long%&gt;.valueOf(((s1).longValue() + (s2).longValue()));\n\t\t\t\t}\n\t\t\t};\n\t\t\t_switchResult = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%java.lang.Long%&gt;&gt;reduce(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%gecos.types.Field%&gt;, &lt;%java.lang.Long%&gt;&gt;map(this.getFields(), _function_1), _function_2);\n\t\t\tbreak;\n\t\tdefault:\n\t\t\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(\"Not implemented yet\");\n\t}\n}\nelse\n{\n\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(\"Not implemented yet\");\n}\nreturn (_switchResult).longValue();'"
	 * @generated
	 */
	long getSize();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" keyDataType="gecos.types.String" keyUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.types.Field%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.types.Field%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%gecos.types.Field%&gt; it)\n\t{\n\t\t&lt;%java.lang.String%&gt; _name = it.getName();\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf(&lt;%com.google.common.base.Objects%&gt;.equal(_name, key));\n\t}\n};\nreturn &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.types.Field%&gt;&gt;findFirst(this.getFields(), _function);'"
	 * @generated
	 */
	Field findField(String key);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitRecordType(this);'"
	 * @generated
	 */
	void accept(TypesVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.boolean" unique="false" otherUnique="false" ignoreStorageClassDataType="gecos.types.boolean" ignoreStorageClassUnique="false" ignoreConstDataType="gecos.types.boolean" ignoreConstUnique="false" ignoreVolatileDataType="gecos.types.boolean" ignoreVolatileUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((other == this))\n{\n\treturn true;\n}\nif ((other instanceof &lt;%gecos.types.RecordType%&gt;))\n{\n\tfinal &lt;%gecos.types.RecordType%&gt; rtype = ((&lt;%gecos.types.RecordType%&gt;)other);\n\tint _size = this.getFields().size();\n\tint _size_1 = rtype.getFields().size();\n\tboolean _tripleEquals = (_size == _size_1);\n\tif (_tripleEquals)\n\t{\n\t\tint _size_2 = this.getFields().size();\n\t\t&lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt; _doubleDotLessThan = new &lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt;(0, _size_2, true);\n\t\tfor (final &lt;%java.lang.Integer%&gt; i : _doubleDotLessThan)\n\t\t{\n\t\t\t{\n\t\t\t\t&lt;%gecos.types.Field%&gt; _get = rtype.getFields().get((i).intValue());\n\t\t\t\tboolean _tripleNotEquals = (_get != null);\n\t\t\t\t&lt;%gecos.types.Field%&gt; _get_1 = this.getFields().get((i).intValue());\n\t\t\t\tboolean _tripleNotEquals_1 = (_get_1 != null);\n\t\t\t\tboolean _xor = (_tripleNotEquals ^ _tripleNotEquals_1);\n\t\t\t\tif (_xor)\n\t\t\t\t{\n\t\t\t\t\treturn false;\n\t\t\t\t}\n\t\t\t\tif (((rtype.getFields().get((i).intValue()) != null) &amp;&amp; (this.getFields().get((i).intValue()) != null)))\n\t\t\t\t{\n\t\t\t\t\tboolean _isEqual = rtype.getFields().get((i).intValue()).isEqual(this.getFields().get((i).intValue()));\n\t\t\t\t\tboolean _not = (!_isEqual);\n\t\t\t\t\tif (_not)\n\t\t\t\t\t{\n\t\t\t\t\t\treturn false;\n\t\t\t\t\t}\n\t\t\t\t}\n\t\t\t}\n\t\t}\n\t}\n\treturn (((this.getKind().equals(rtype.getKind()) &amp;&amp; (&lt;%java.lang.Boolean%&gt;.valueOf(this.isDefined()) == &lt;%java.lang.Boolean%&gt;.valueOf(rtype.isDefined()))) &amp;&amp; this.getName().equals(rtype.getName())) &amp;&amp; super.isEqual(rtype, ignoreStorageClass, ignoreConst, ignoreVolatile));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isEqual(Type other, boolean ignoreStorageClass, boolean ignoreConst, boolean ignoreVolatile);

} // RecordType
