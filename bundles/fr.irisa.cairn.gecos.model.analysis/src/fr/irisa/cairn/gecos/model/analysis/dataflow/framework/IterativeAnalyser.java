/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.analysis.dataflow.framework;

import gecos.blocks.BasicBlock;
import gecos.core.Procedure;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

/** IterativeAnalyser. classic iteratrive algorithm
 * to compute properties from a dataflow. 
 *
 * for each block b :
 * v_new(b) = \sum_{n\in next(b)} compute(b, n)
 */


public abstract class IterativeAnalyser {


	private static boolean VERBOSE = false;

	protected static void debug(String mess) {
		if (VERBOSE) System.out.println( mess);
	}
	
	public static void setDebug(boolean vERBOSE) {
		VERBOSE = vERBOSE;
	}
	
	public static boolean isDebug() {
		return VERBOSE;
	}

	private Procedure proc;
	private Map<BasicBlock,ILattice> values;
	private BasicEList<BasicBlock> workList;

	public IterativeAnalyser(Procedure proc) {
		this.proc = proc;
		values = new LinkedHashMap<BasicBlock, ILattice>();
		workList = new BasicEList<BasicBlock>();
	} 
	
	protected void initialize() {
		Iterator<BasicBlock> i = proc.getBasicBlocks().iterator();
		
		while (i.hasNext()) {
			BasicBlock b = i.next();
			if(b==null) {
				throw new RuntimeException("Error : null basic block found !");
			}
			workList.add(b);
			setLattice(b, newBottomLattice());
		}
	}

	public Procedure procedure() { return proc; }

	public void compute() {

		//System.out.println(this.getClass().getSimpleName()+": START .compute()");
		ILattice totalEffect;
		while(! workList.isEmpty()) {
			
			BasicBlock b = (BasicBlock) workList.remove(0);
			if (isDebug()) debug("Current is BB"+b.getNumber());
			totalEffect = newBottomLattice();
			Iterator<BasicBlock> iter = nextBlocks(b).iterator();

			while(iter.hasNext()) {
				BasicBlock next = (BasicBlock) iter.next();
				if (next==null) {
					System.out.println("error");
				}
				if (isDebug()) debug("	Next is BB"+next.getNumber()+" Lattice "+totalEffect);
				totalEffect.merge(compute(next, getLattice(next)));
				if (isDebug()) debug("	=> "+totalEffect+"\n");

			}
			
			if (isDebug()) debug("	- Total effect is "+totalEffect);
			//if had effect, add immediate predecessors to workList.
			if (! getLattice(b).equals(totalEffect)) {
				if (isDebug()) debug("not same => had effect");
				setLattice(b, totalEffect);
				iter = reverseNextBlocks(b).iterator();
				while(iter.hasNext()) {
					BasicBlock pred = (BasicBlock) iter.next();
					workList.add(pred);
				}
			}
			hook(b);
		}
		//System.out.println(this.getClass().getSimpleName()+": START .compute()");
	}

	// only for debug purpose
	protected void hook(BasicBlock b) { }

	public ILattice getLattice(BasicBlock b) {
		return (ILattice) values.get(b);
	}

	protected void setLattice(BasicBlock b, ILattice l) {
		values.put(b, l);
	}

	protected abstract EList<BasicBlock> nextBlocks(BasicBlock b);
	protected abstract EList<BasicBlock> reverseNextBlocks(BasicBlock b);
	protected abstract ILattice newBottomLattice();
	protected abstract ILattice compute(BasicBlock b, ILattice in);
}
