package fr.irisa.cairn.gecos.model.modules;

import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosprojectPackage;

import java.io.File;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;

@GSModule("Load a GecosProject from a serialized '.gecosproject' file.\n"
		+ "\nSee: 'SaveGecosProject' module.")
public class LoadGecosProject {

	String filename;
	
	@GSModuleConstructor("-arg1: file path of the '.gecosproject' file.")
	public LoadGecosProject(String filename) {
		this.filename=filename;
	}
	
	public GecosProject compute() throws RuntimeException {
		// Create a resource set to hold the resources.
		//
		ResourceSet resourceSet = new ResourceSetImpl();

		// Register the appropriate resource factory to handle all file extensions.
		//
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put
		(Resource.Factory.Registry.DEFAULT_EXTENSION, 
				new XMIResourceFactoryImpl());

		// Register the package to ensure it is available during loading.
		//
		resourceSet.getPackageRegistry().put
		(GecosprojectPackage.eNS_URI, 
				GecosprojectPackage.eINSTANCE);

		// If there are no arguments, emit an appropriate usage message.
		//
		File file = new File(filename);
		URI uri = URI.createFileURI(file.getAbsolutePath());

		// Demand load resource for this file.
		//
		Resource resource = resourceSet.getResource(uri, true);
		//System.out.println("Loaded " + uri);

		// Validate the contents of the loaded resource.
		//
		EList<EObject> contents = resource.getContents();
		return (GecosProject) contents.get(0);
	}


	
}
