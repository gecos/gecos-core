package fr.irisa.cairn.gecos.model.instruction.tests;

import static org.junit.Assert.assertTrue;

import org.eclipse.emf.common.util.EList;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.model.instructions.utils.InstructionDataProvider;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.Instruction;

/**
 * 
 * @author aelmouss
 */
@RunWith(DataProviderRunner.class)
public class TestComplexInstructions {

	@Test
	@UseDataProvider(location=InstructionDataProvider.class, value="provideComplexInstructions")
	public void testBasic(ComplexInstruction inst) {
		System.out.println(inst);
		
		//test copy/issame
		Instruction copy = inst.copy();
		assertTrue("copyisSame", copy != null && copy != inst && copy.isSame(inst));
				
		EList<Instruction> children = inst.listChildren();
		System.out.println(children);
		
		//test getChildrenCount
		assertTrue("getChildrenCount", inst.getChildrenCount() == children.size());
		
		for(Instruction c : children) {
			if(c == null) {
				System.err.println("Found a null child!");
				continue;
			}
			
			//test getParent
			assertTrue("getParent()", c.getParent() == inst);
			
			//test indexOfChild
			int index = inst.indexOfChild(c);
			assertTrue("indexOfChild()", index >= 0 && index < children.size());
			
			//test getChildAt
			assertTrue("getChildAt()", inst.getChildAt(index) == c);
			
			//test getRoot
			assertTrue("getRoot()", inst.getRoot() == c.getRoot());
			
			//test replaceChild
			Instruction ccopy = c.copy();
			inst.replaceChild(c, ccopy);
			assertTrue("copyisSame", ccopy != null && c != ccopy && ccopy.isSame(c));
			assertTrue("replaceChild()", inst.indexOfChild(c) < 0 && inst.getChildAt(index) == ccopy);
			
			//test removeChild
//			inst.removeChild(c); //this should have no impact
			int nbValid = inst.listValidChildren().size();
			assertTrue("listValidChildren()", nbValid > 0);
			inst.removeChild(ccopy);
			assertTrue("removeChild()indexOfChild", inst.indexOfChild(ccopy) < 0);
			assertTrue("removeChild()listValidChildren", inst.listValidChildren().size() == nbValid - 1);
			System.out.println("inst.listChildren().size() = " + inst.listChildren().size()); //may the remain the same as before !
		}
		
		System.out.println(inst); // by now all children should have been removed
		assertTrue("listValidChildren", inst.listValidChildren().isEmpty());
	}
	
}
