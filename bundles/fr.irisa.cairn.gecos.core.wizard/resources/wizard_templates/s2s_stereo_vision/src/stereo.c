/*
 Copyright (C) 2006 Pedro Felzenszwalb

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <common.h>
#include <math.h>


/* convolve image with gaussian filter */
int i = 0;
// computation of data costs

//extern float abs(float);

void comp_data(float ***data, unsigned char **img1, unsigned char **img2,
		unsigned char **out1, unsigned char **out2, int width, int height) {

	unsigned char **tmp;

	smooth(img1, out1, width, height);
	smooth(img2, out2, width, height);
	save_pgm("smooth1.pgm", width, height, out1);
	save_pgm("smooth2.pgm", width, height, out2);

	tmp = mat_malloc(width, height);
	for (int x = 0; x < VALUES - 1; x++) {
		for (int y = 0; y < height; y++) {
			for (int v = 0; v < VALUES; v++) {
				data[x][y][v] = (LAMBDA * 255); // ~ 170
			}
		}
	}
	for (int x = VALUES - 1; x < width; x++) {
		for (int y = 0; y < height; y++) {
			tmp[x][y] = 255;
			for (int v = 0; v < VALUES; v++) {
				float val = abs(out1[x][y] - out2[x - v][y]);
				data[x][y][v] = LAMBDA * min(val, DATA_K);
				tmp[x][y] = min(tmp[x][y], data[x][y][v]);
			}
		}
	}

	save_pgm("disp", width, height, tmp);

	// printf("%d",i);
}


// compute message
#pragma gcs_inline=yes
void msg(float s1[VALUES], float s2[VALUES], float s3[VALUES],
		float s4[VALUES], float dst[VALUES]) {
	int value, q;
	float val;
	// aggregate and find min
	float minimum = INF;

	for (value = 0; value < VALUES; value++) {
		dst[value] = s1[value] + s2[value] + s3[value] + s4[value];
		if (dst[value] < minimum)
			minimum = dst[value];
	}

#pragma gcs_unroll
	for (q = 1; q < VALUES; q++) {
		float prev = dst[q - 1] + 1.0F;
		if (prev < dst[q])
			dst[q] = prev;
	}

#pragma gcs_unroll
	for (q = VALUES - 2; q >= 0; q--) {
		float prev = dst[q + 1] + 1.0F;
		if (prev < dst[q])
			dst[q] = prev;
	}

	// truncate
	minimum += DISC_K;
	for (value = 0; value < VALUES; value++)
		if (minimum < dst[value])
			dst[value] = minimum;

	// normalize
	val = 0;
	for (value = 0; value < VALUES; value++)
		val += dst[value];

	val /= VALUES;
	for (value = 0; value < VALUES; value++)
		dst[value] -= val;
}

// Belief propagation using checkerboard update scheme
void bp_cb(float*** u, float ***d, float ***l, float ***r, float ***data,
		int width, int height) {
	int x, y, t;
	for (t = 0; t < ITER; t++) {
		printf("\tIteration t=%u\n", t);
		fflush(stdout);
		for (y = 1; y < height - 1; y++) {
			for (x = ((y + t) % 2) + 1; x < width - 1; x += 2) {
				msg(u[x][y + 1], l[x + 1][y], r[x - 1][y], data[x][y], u[x][y]);
				msg(d[x][y - 1], l[x + 1][y], r[x - 1][y], data[x][y], d[x][y]);
				msg(u[x][y + 1], d[x][y - 1], r[x - 1][y], data[x][y], r[x][y]);
				msg(u[x][y + 1], d[x][y - 1], l[x + 1][y], data[x][y], l[x][y]);
			}
		}
	}
}

//
// Single scale belief propagation for image restoration
//
void stereo_ms(float ****data, float ****u, float ****d, float ****l,
		float ****r, int width, int height) {

	int x, y, i, v, value;
	unsigned char** out;
	int cur_width;
	int cur_height;
	char name[128];

	out = mat_malloc(MAX_WIDTH, MAX_HEIGHT);
	/*
	 *  Data pyramid
	 */
	for (i = 1; i <= LEVELS; i++) {
		int old_width = width;
		int old_height = height;
		//		int new_width = (int)ceil(old_width/2.0);
		//		int new_height = (int)ceil(old_height/2.0);

		//		assert(new_width >= 1);
		//		assert(new_height >= 1);
		for (y = 0; y < old_height; y++) {
			for (x = 0; x < old_width; x++) {
				for (value = 0; value < VALUES; value++) {
					data[i][x / 2][y / 2][value] += data[i - 1][x][y][value];
				}
			}
		}
	}

	cur_width = width >> LEVELS;
	cur_height = height >> LEVELS;

	// Run belief propagation from coarse to fine

	output(out, u[0], d[0], l[0], r[0], data[0], width, height);

	sprintf(name, "%s_%u.pgm", "res/res", 0);
	save_pgm(name, width, height, out);

	for (i = LEVELS - 1; i >= 0; i--) {
		cur_width = cur_width * 2;
		cur_height = cur_height * 2;
		printf("LEVEL %u : %u x %u\n", i, cur_width, cur_height);
		fflush(stdout);

		for (y = 0; y < cur_height; y++) {
			for (x = 0; x < cur_width; x++) {
				for (v = 0; v < VALUES; v++) {
					u[i][x][y][v] = u[i + 1][x / 2][y / 2][v];
					d[i][x][y][v] = d[i + 1][x / 2][y / 2][v];
					l[i][x][y][v] = l[i + 1][x / 2][y / 2][v];
					r[i][x][y][v] = r[i + 1][x / 2][y / 2][v];
				}
			}
		}


		bp_cb(u[i], d[i], l[i], r[i], data[i], cur_width, cur_height);

		output(out, u[i], d[i], l[i], r[i], data[i], width, height);
		sprintf(name, "%s_%u.pgm", "res/res", i);
		save_pgm(name, width, height, out);

	}

}

// generate output from current messages
void output(unsigned char ** out, float*** u, float*** d, float*** l,
		float*** r, float*** data, int width, int height) {
	int x, y, value;
	for (y = 1; y < height - 1; y++) {
		for (x = 1; x < width - 1; x++) {
			// keep track of best value for current pixel
			int best_score = 0;
			float best_val = INF;
			for (value = 0; value < VALUES; value++) {
				float score = u[x][y + 1][value] + d[x][y - 1][value]
						+ l[x + 1][y][value] + r[x - 1][y][value]
						+ data[x][y][value];
				if (score < best_val) {
					best_val = score;
					best_score = value;
				}
			}
			out[x][y] = best_score * SCALE;
		}
	}
}

int main(int argc, char **argv) {

	int width1, height1;
	int height2, width2;

	unsigned char** img1 = mat_malloc(MAX_WIDTH, MAX_HEIGHT);
	unsigned char** img2 = mat_malloc(MAX_WIDTH, MAX_HEIGHT);
	unsigned char** out1 = mat_malloc(MAX_WIDTH, MAX_HEIGHT);
	unsigned char** out2 = mat_malloc(MAX_WIDTH, MAX_HEIGHT);
	float ****data, ****l, ****r, ****u, ****d;

	if (argc != 3) {
		fprintf(stderr, "usage: %s left.pgm right.pgm", argv[0]);
		exit(1);
	}

	// load input
	load_pgm(argv[1], &width1, &height1, img1);
	load_pgm(argv[2], &width2, &height2, img2);

	printf("Images dimension %u x %u ", width1, height1);

	assert(width1 == width2);
	assert(height1 == height2);

	data = hypercube_malloc(LEVELS + 1, width1, height1, VALUES);
	u = hypercube_malloc(LEVELS + 1, width1, height1, VALUES);
	r = hypercube_malloc(LEVELS + 1, width1, height1, VALUES);
	l = hypercube_malloc(LEVELS + 1, width1, height1, VALUES);
	d = hypercube_malloc(LEVELS + 1, width1, height1, VALUES);

#pragma gcs_inline
	comp_data(data[0], img1, img2, out1, out2, width1, height1);

	// compute disparities
	stereo_ms(data, u, d, l, r, width1, height1);

	return 0;
}
