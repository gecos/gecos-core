/**
 */
package gecos.dag;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visitor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.dag.DagPackage#getDagVisitor()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface DagVisitor extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGInstruction(DAGInstruction d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGInPort(DAGInPort d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGOutPort(DAGOutPort d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGSymbolNode(DAGSymbolNode d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGCallNode(DAGCallNode d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGArrayValueNode(DAGArrayValueNode d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGOpNode(DAGOpNode d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGOutNode(DAGOutNode d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGControlEdge(DAGControlEdge d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGDataEdge(DAGDataEdge d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGNumberedSymbolNode(DAGNumberedSymbolNode d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGNumberedOutNode(DAGNumberedOutNode d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGIntImmNode(DAGIntImmNode d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGFloatImmNode(DAGFloatImmNode d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGArrayNode(DAGArrayNode d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGSimpleArrayNode(DAGSimpleArrayNode d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGJumpNode(DAGJumpNode d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGPatternNode(DAGPatternNode d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGVectorArrayAccessNode(DAGVectorArrayAccessNode d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGStringImmNode(DAGStringImmNode d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGVectorOpNode(DAGVectorOpNode d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGVectorShuffleNode(DAGVectorShuffleNode d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGVectorExtractNode(DAGVectorExtractNode d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGVectorExpandNode(DAGVectorExpandNode d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGVectorPackNode(DAGVectorPackNode d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGSSAUseNode(DAGSSAUseNode d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGSSADefNode(DAGSSADefNode d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGPhiNode(DAGPhiNode d);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dUnique="false"
	 * @generated
	 */
	void visitDAGFieldInstruction(DAGFieldInstruction d);

} // DagVisitor
