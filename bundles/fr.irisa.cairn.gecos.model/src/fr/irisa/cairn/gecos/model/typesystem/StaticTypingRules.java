package fr.irisa.cairn.gecos.model.typesystem;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.extensions.typesystem.ITypeSystemRuleSet;
import gecos.blocks.Block;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.InstrsPackage;
import gecos.instrs.Instruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.PhiInstruction;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SSADefSymbol;
import gecos.instrs.SSAUseSymbol;
import gecos.instrs.SelectOperator;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;

@SuppressWarnings("all")
public class StaticTypingRules implements ITypeSystemRuleSet {


private static boolean tom_equal_term_Strategy(Object t1, Object t2) {
return  (t1.equals(t2)) ;
}
private static boolean tom_is_sort_Strategy(Object t) {
return  (t instanceof tom.library.sl.Strategy) ;
}
private static boolean tom_equal_term_Position(Object t1, Object t2) {
return  (t1.equals(t2)) ;
}
private static boolean tom_is_sort_Position(Object t) {
return  (t instanceof tom.library.sl.Position) ;
}
private static boolean tom_equal_term_int(int t1, int t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_int(int t) {
return  true ;
}
private static boolean tom_equal_term_char(char t1, char t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_char(char t) {
return  true ;
}
private static boolean tom_equal_term_String(String t1, String t2) {
return  t1.equals(t2) ;
}
private static boolean tom_is_sort_String(String t) {
return  t instanceof String ;
}
private static  tom.library.sl.Strategy  tom_make_mu( tom.library.sl.Strategy  var,  tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.Mu(var,v) );
}
private static  tom.library.sl.Strategy  tom_make_MuVar( String  name) { 
return ( new tom.library.sl.MuVar(name) );
}
private static  tom.library.sl.Strategy  tom_make_Identity() { 
return ( new tom.library.sl.Identity() );
}
private static  tom.library.sl.Strategy  tom_make_One( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.One(v) );
}
private static  tom.library.sl.Strategy  tom_make_All( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.All(v) );
}
private static  tom.library.sl.Strategy  tom_make_Fail() { 
return ( new tom.library.sl.Fail() );
}
private static boolean tom_is_fun_sym_Sequence( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.Sequence );
}
private static  tom.library.sl.Strategy  tom_empty_list_Sequence() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_Sequence( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.Sequence.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Sequence.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Sequence.THEN) );
}
private static boolean tom_is_empty_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( t == null );
}

  private static   tom.library.sl.Strategy  tom_append_list_Sequence( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 == null )) {
      return l2;
    } else if(( l2 == null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.Sequence )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.THEN) ) == null )) {
        return  tom.library.sl.Sequence.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.Sequence.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.FIRST) ),tom_append_list_Sequence(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.Sequence.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_Sequence( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end == null ) ||  (end.equals(tom_empty_list_Sequence())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.Sequence.make(((( begin instanceof tom.library.sl.Sequence ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Sequence.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_Sequence(((( begin instanceof tom.library.sl.Sequence ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Sequence.THEN) ):tom_empty_list_Sequence()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_Choice( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.Choice );
}
private static  tom.library.sl.Strategy  tom_empty_list_Choice() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_Choice( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.Choice.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Choice.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Choice.THEN) );
}
private static boolean tom_is_empty_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( t ==null );
}

  private static   tom.library.sl.Strategy  tom_append_list_Choice( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 ==null )) {
      return l2;
    } else if(( l2 ==null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.Choice )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.THEN) ) ==null )) {
        return  tom.library.sl.Choice.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.Choice.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.FIRST) ),tom_append_list_Choice(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.Choice.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_Choice( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end ==null ) ||  (end.equals(tom_empty_list_Choice())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.Choice.make(((( begin instanceof tom.library.sl.Choice ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Choice.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_Choice(((( begin instanceof tom.library.sl.Choice ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Choice.THEN) ):tom_empty_list_Choice()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_SequenceId( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.SequenceId );
}
private static  tom.library.sl.Strategy  tom_empty_list_SequenceId() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_SequenceId( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.SequenceId.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.SequenceId.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.SequenceId.THEN) );
}
private static boolean tom_is_empty_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( t == null );
}

  private static   tom.library.sl.Strategy  tom_append_list_SequenceId( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 == null )) {
      return l2;
    } else if(( l2 == null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.SequenceId )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.THEN) ) == null )) {
        return  tom.library.sl.SequenceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.SequenceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.FIRST) ),tom_append_list_SequenceId(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.SequenceId.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_SequenceId( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end == null ) ||  (end.equals(tom_empty_list_SequenceId())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.SequenceId.make(((( begin instanceof tom.library.sl.SequenceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.SequenceId.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_SequenceId(((( begin instanceof tom.library.sl.SequenceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.SequenceId.THEN) ):tom_empty_list_SequenceId()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_ChoiceId( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.ChoiceId );
}
private static  tom.library.sl.Strategy  tom_empty_list_ChoiceId() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_ChoiceId( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.ChoiceId.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.ChoiceId.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.ChoiceId.THEN) );
}
private static boolean tom_is_empty_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( t ==null );
}

  private static   tom.library.sl.Strategy  tom_append_list_ChoiceId( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 ==null )) {
      return l2;
    } else if(( l2 ==null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.ChoiceId )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.THEN) ) ==null )) {
        return  tom.library.sl.ChoiceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.ChoiceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.FIRST) ),tom_append_list_ChoiceId(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.ChoiceId.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_ChoiceId( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end ==null ) ||  (end.equals(tom_empty_list_ChoiceId())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.ChoiceId.make(((( begin instanceof tom.library.sl.ChoiceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.ChoiceId.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_ChoiceId(((( begin instanceof tom.library.sl.ChoiceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.ChoiceId.THEN) ):tom_empty_list_ChoiceId()),end,tail)) ;
  }
  private static  tom.library.sl.Strategy  tom_make_OneId( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.OneId(v) );
}
private static  tom.library.sl.Strategy  tom_make_AllSeq( tom.library.sl.Strategy  s) { 
return ( new tom.library.sl.AllSeq(s) );
}
private static  tom.library.sl.Strategy  tom_make_AUCtl( tom.library.sl.Strategy  s1,  tom.library.sl.Strategy  s2) { 
return ( 
tom_make_mu(tom_make_MuVar("x"),tom_cons_list_Choice(s2,tom_cons_list_Choice(tom_cons_list_Sequence(tom_cons_list_Sequence(s1,tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("x")),tom_empty_list_Sequence())),tom_cons_list_Sequence(tom_make_One(tom_make_Identity()),tom_empty_list_Sequence())),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_EUCtl( tom.library.sl.Strategy  s1,  tom.library.sl.Strategy  s2) { 
return ( 
tom_make_mu(tom_make_MuVar("x"),tom_cons_list_Choice(s2,tom_cons_list_Choice(tom_cons_list_Sequence(s1,tom_cons_list_Sequence(tom_make_One(tom_make_MuVar("x")),tom_empty_list_Sequence())),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_Try( tom.library.sl.Strategy  s) { 
return ( 
tom_cons_list_Choice(s,tom_cons_list_Choice(tom_make_Identity(),tom_empty_list_Choice())))
;
}
private static  tom.library.sl.Strategy  tom_make_Repeat( tom.library.sl.Strategy  s) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Choice(tom_cons_list_Sequence(s,tom_cons_list_Sequence(tom_make_MuVar("_x"),tom_empty_list_Sequence())),tom_cons_list_Choice(tom_make_Identity(),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_TopDown( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Sequence(v,tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("_x")),tom_empty_list_Sequence()))))
;
}
private static  tom.library.sl.Strategy  tom_make_OnceTopDown( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Choice(v,tom_cons_list_Choice(tom_make_One(tom_make_MuVar("_x")),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_RepeatId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_SequenceId(v,tom_cons_list_SequenceId(tom_make_MuVar("_x"),tom_empty_list_SequenceId()))))
;
}
private static  tom.library.sl.Strategy  tom_make_OnceTopDownId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_ChoiceId(v,tom_cons_list_ChoiceId(tom_make_OneId(tom_make_MuVar("_x")),tom_empty_list_ChoiceId()))))
;
}
private static boolean tom_equal_term_long(long t1, long t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_long(long t) {
return  true ;
}
private static boolean tom_equal_term_double(double t1, double t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_double(double t) {
return  true ;
}
private static boolean tom_equal_term_boolean(boolean t1, boolean t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_boolean(boolean t) {
return  true ;
}
private static boolean tom_equal_term_float(float t1, float t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_float(float t) {
return  true ;
}


private static <O> EList<O> enforce(EList l) {
return l;
}

private static <O> EList<O> append(O e,EList<O> l) {
l.add(e);
return l;
}
private static boolean tom_equal_term_EELong(Object l1, Object l2) {
return l1.equals(l2);
}
private static boolean tom_is_sort_EELong(Object t) {
return t instanceof java.lang.Long;
}
private static boolean tom_equal_term_BlockCopyManager(Object l1, Object l2) {
return l1.equals(l2);
}
private static boolean tom_is_sort_BlockCopyManager(Object t) {
return t instanceof fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;
}
private static boolean tom_equal_term_DependencyType(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_DependencyType(Object t) {
return t instanceof DependencyType;
}
private static boolean tom_equal_term_DAGOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_DAGOperator(Object t) {
return t instanceof DAGOperator;
}
private static boolean tom_equal_term_ArithmeticOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ArithmeticOperator(Object t) {
return t instanceof ArithmeticOperator;
}
private static boolean tom_equal_term_ComparisonOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ComparisonOperator(Object t) {
return t instanceof ComparisonOperator;
}
private static boolean tom_equal_term_LogicalOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_LogicalOperator(Object t) {
return t instanceof LogicalOperator;
}
private static boolean tom_equal_term_BitwiseOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_BitwiseOperator(Object t) {
return t instanceof BitwiseOperator;
}
private static boolean tom_equal_term_SelectOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_SelectOperator(Object t) {
return t instanceof SelectOperator;
}
private static boolean tom_equal_term_ReductionOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ReductionOperator(Object t) {
return t instanceof ReductionOperator;
}
private static boolean tom_equal_term_BranchType(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_BranchType(Object t) {
return t instanceof BranchType;
}
private static boolean tom_equal_term_StorageClassSpecifiers(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_StorageClassSpecifiers(Object t) {
return t instanceof StorageClassSpecifiers;
}
private static boolean tom_equal_term_Kinds(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_Kinds(Object t) {
return t instanceof Kinds;
}
private static boolean tom_equal_term_IntegerTypes(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_IntegerTypes(Object t) {
return t instanceof IntegerTypes;
}
private static boolean tom_equal_term_SignModifiers(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_SignModifiers(Object t) {
return t instanceof SignModifiers;
}
private static boolean tom_equal_term_FloatPrecisions(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_FloatPrecisions(Object t) {
return t instanceof FloatPrecisions;
}
private static boolean tom_equal_term_OverflowMode(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_OverflowMode(Object t) {
return t instanceof OverflowMode;
}
private static boolean tom_equal_term_QuantificationMode(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_QuantificationMode(Object t) {
return t instanceof QuantificationMode;
}
private static boolean tom_equal_term_Inst(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Inst(Object t) {
return t instanceof Instruction;
}
private static boolean tom_equal_term_Blk(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Blk(Object t) {
return t instanceof Block;
}
private static boolean tom_equal_term_Sym(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Sym(Object t) {
return t instanceof Symbol;
}
private static boolean tom_equal_term_SymL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_SymL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Symbol>)t).size() == 0 
    	|| (((EList<Symbol>)t).size()>0 && ((EList<Symbol>)t).get(0) instanceof Symbol));
}
private static boolean tom_equal_term_Type(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Type(Object t) {
return t instanceof Type;
}
private static boolean tom_equal_term_TypeL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_TypeL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Type>)t).size() == 0 
    	|| (((EList<Type>)t).size()>0 && ((EList<Type>)t).get(0) instanceof Type));
}
private static boolean tom_equal_term_Field(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Field(Object t) {
return t instanceof Field;
}
private static boolean tom_equal_term_FieldL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_FieldL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Field>)t).size() == 0 
    	|| (((EList<Field>)t).size()>0 && ((EList<Field>)t).get(0) instanceof Field));
}
private static boolean tom_equal_term_InstL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_InstL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Instruction>)t).size() == 0 
    	|| (((EList<Instruction>)t).size()>0 && ((EList<Instruction>)t).get(0) instanceof Instruction));
}
private static boolean tom_is_fun_sym_InstL( EList<Instruction>  t) {
return  t instanceof EList<?> &&
 		(((EList<Instruction>)t).size() == 0 
 		|| (((EList<Instruction>)t).size()>0 && ((EList<Instruction>)t).get(0) instanceof Instruction));
}
private static  EList<Instruction>  tom_empty_array_InstL(int n) { 
return  new BasicEList<Instruction>(n) ;
}
private static  EList<Instruction>  tom_cons_array_InstL(Instruction e,  EList<Instruction>  l) { 
return  append(e,l) ;
}
private static Instruction tom_get_element_InstL_InstL( EList<Instruction>  l, int n) {
return  l.get(n) ;
}
private static int tom_get_size_InstL_InstL( EList<Instruction>  l) {
return  l.size() ;
}

  private static   EList<Instruction>  tom_get_slice_InstL( EList<Instruction>  subject, int begin, int end) {
     EList<Instruction>  result =  new BasicEList<Instruction>(end-begin) ;
    while(begin!=end) {
      result =  append( subject.get(begin) ,result) ;
      begin++;
    }
    return result;
  }

  private static   EList<Instruction>  tom_append_array_InstL( EList<Instruction>  l2,  EList<Instruction>  l1) {
    int size1 =  l1.size() ;
    int size2 =  l2.size() ;
    int index;
     EList<Instruction>  result =  new BasicEList<Instruction>(size1+size2) ;
    index=size1;
    while(index >0) {
      result =  append( l1.get(size1-index) ,result) ;
      index--;
    }

    index=size2;
    while(index > 0) {
      result =  append( l2.get(size2-index) ,result) ;
      index--;
    }
    return result;
  }private static boolean tom_equal_term_BlkL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_BlkL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Block>)t).size() == 0 
    	|| (((EList<Block>)t).size()>0 && ((EList<Block>)t).get(0) instanceof Block));
}
private static boolean tom_equal_term_Node(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Node(Object t) {
return t instanceof DAGNode;
}
private static boolean tom_equal_term_NodeL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_NodeL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<DAGNode>)t).size() == 0 
    	|| (((EList<DAGNode>)t).size()>0 && ((EList<DAGNode>)t).get(0) instanceof DAGNode));
}
private static boolean tom_is_fun_sym_symref(Instruction t) {
return t instanceof SymbolInstruction;
}
private static Symbol tom_get_slot_symref_symbol(Instruction t) {
return ((SymbolInstruction)t).getSymbol();
}
private static boolean tom_is_fun_sym_set(Instruction t) {
return t instanceof SetInstruction;
}
private static Instruction tom_get_slot_set_dest(Instruction t) {
return ((SetInstruction)t).getDest();
}
private static Instruction tom_get_slot_set_source(Instruction t) {
return ((SetInstruction)t).getSource();
}
private static boolean tom_is_fun_sym_phi(Instruction t) {
return t instanceof PhiInstruction;
}
private static  EList<Instruction>  tom_get_slot_phi_children(Instruction t) {
return enforce(((PhiInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_ssaDef(Instruction t) {
return t instanceof SSADefSymbol;
}
private static Symbol tom_get_slot_ssaDef_symbol(Instruction t) {
return ((SSADefSymbol)t).getSymbol();
}
private static  int  tom_get_slot_ssaDef_number(Instruction t) {
return ((SSADefSymbol)t).getNumber();
}
private static boolean tom_is_fun_sym_ssaUse(Instruction t) {
return t instanceof SSAUseSymbol;
}
private static Symbol tom_get_slot_ssaUse_symbol(Instruction t) {
return ((SSAUseSymbol)t).getSymbol();
}
private static  int  tom_get_slot_ssaUse_number(Instruction t) {
return ((SSAUseSymbol)t).getNumber();
}



protected static void debug(String mess) {
if(VERBOSE) System.out.println(mess);
}

protected static void debug(int n, String mess) {  
debug("Rule "+n+" : "+mess); 
}

public static final boolean VERBOSE = true;

public StaticTypingRules() {

}



@Override
public Type computeType(Instruction o) {
if(o.eClass().getEPackage()!=InstrsPackage.eINSTANCE)
return null;
Type res = tomType(o);
return res;
}


public Type tomType(Instruction o) {

{
{
if (tom_is_sort_Inst(o)) {
if (tom_is_sort_Inst(((Instruction)o))) {
if (tom_is_fun_sym_ssaDef(((Instruction)((Instruction)o)))) {

Type type= (
tom_get_slot_ssaDef_symbol(((Instruction)o))).getType();
debug(0,"SSA  "+o+" with Type "+type);
return type;  

}
}
}
}
{
if (tom_is_sort_Inst(o)) {
if (tom_is_sort_Inst(((Instruction)o))) {
if (tom_is_fun_sym_ssaUse(((Instruction)((Instruction)o)))) {

Type type= (
tom_get_slot_ssaUse_symbol(((Instruction)o))).getType();
debug(0,"SSA "+o+" with Type "+type);
return type;  

}
}
}
}
{
if (tom_is_sort_Inst(o)) {
if (tom_is_sort_Inst(((Instruction)o))) {
if (tom_is_fun_sym_set(((Instruction)((Instruction)o)))) {

Type type= (
tom_get_slot_set_dest(((Instruction)o))).getType();
debug(0,"SET "+o+" with Type "+type);
return type;  

}
}
}
}
{
if (tom_is_sort_Inst(o)) {
if (tom_is_sort_Inst(((Instruction)o))) {
if (tom_is_fun_sym_phi(((Instruction)((Instruction)o)))) {
 EList<Instruction>  tomMatch1_16=tom_get_slot_phi_children(((Instruction)o));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_16))) {
int tomMatch1_20=0;
if (!(tomMatch1_20 >= tom_get_size_InstL_InstL(tomMatch1_16))) {

Type type= (
tom_get_element_InstL_InstL(tomMatch1_16,tomMatch1_20)).getType();
debug(0,"Phi node "+o+" with Type "+type);
return type;  

}
}
}
}
}
}
{
if (tom_is_sort_Inst(o)) {
if (tom_is_sort_Inst(((Instruction)o))) {
if (tom_is_fun_sym_symref(((Instruction)((Instruction)o)))) {

Type type= (
tom_get_slot_symref_symbol(((Instruction)o))).getType();
debug(0,"Symbol instruction "+o+" with Type "+type);
return type;  

}
}
}
}
}

return null;

}

}
