package fr.irisa.cairn.gecos.model.transforms.blocks.simplifier;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch;
import fr.irisa.cairn.gecos.model.tools.utils.BlocksEraser;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CaseBlock;
import gecos.blocks.CompositeBlock;
import gecos.blocks.SwitchBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.Scope;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;

/**
 * This visitor reduce the level of CompositeBlock, and try to merge
 * BasicBlocks that do not have branch instructions. <br/>
 * It does not take care of ControlFlow. ModelBuildControlFlow and
 * ModelClearControlFlow may be useful.
 * @author amorvan
 *
 */
public class ModelBlockSimplifier extends BlocksDefaultSwitch<Object> {

	/*
	 * what it does?
	 *   - reduce composite block complexity 
	 *   - merge basic blocks when it is possible
	 *   - remove Composite block by its child (if it has only one)
	 */
	private static final boolean debug=false;
	private static void debug(Object o) { if (debug) System.out.print(o.toString()); }
	private static void debugln(Object o) {  debug(o.toString()+"\n"); }
	private static final String PREFIX = "[BLOCK SIMPLIFER] ";
	
	private List<Block> lBlock;
	
	public ModelBlockSimplifier(Block b) {
		(lBlock = new ArrayList<Block>(1)).add(b);
	}
	
	public ModelBlockSimplifier(Procedure proc) {
		(lBlock = new ArrayList<Block>(1)).add(proc.getBody());
	}

	public ModelBlockSimplifier(ProcedureSet ps) {
		lBlock = new ArrayList<Block>();
		for (Procedure proc : ps.listProcedures()) 
			lBlock.add(proc.getBody());
	}

	public ModelBlockSimplifier(GecosProject project) {
		lBlock = new ArrayList<Block>();
		for (GecosSourceFile f : project.getSources()) {
			if (f.getModel() == null) throw new RuntimeException("Error : project is not fully built.");
			for (Procedure proc : f.getModel().listProcedures())
				lBlock.add(proc.getBody());
		}
	}
	
	public void compute() {
		//*
		for (Block b : lBlock) {
			doSwitch(b);
		}//*/
	}
	
	@Override
	public Object caseCompositeBlock(CompositeBlock b) {
		super.caseCompositeBlock(b);
		if (b.getParent() == null) {
			return null;
		} else if (b.getParent() instanceof CaseBlock) {
			//XXX : exception for CaseBlock
			super.caseCompositeBlock(b);
			return null;
		}else {
			debugln(PREFIX+"CompositeBlock found - trying to simplify");

			
			Block[] children = b.getChildren().toArray(new Block[b.getChildren().size()]);
			for (Block child : children) {
				if (child instanceof CompositeBlock) {
					// if one child is a compositeBlock, then merge it with b;
					b.mergeChildComposite((CompositeBlock) child);
				}
			}

			//after merging all the CompositeBlocks, merge basicBlocks if possible 
			b.mergeChildren();
			
			//finally replace composite block by its child (if it has only one, and no declarations)
			if (b.getChildren().size() == 1 && b.getScope().getSymbols().size() == 0 && !(b.eContainer().eContainer() instanceof Procedure)) {

				//except if it is a BasicBlock with more than 1 instruction
				if (b.getChildren().get(0) instanceof BasicBlock ) {
					BasicBlock child = (BasicBlock) b.getChildren().get(0);
					if (child.getInstructionCount() > 1) return null;
				}
				
				Scope scope = b.getScope();
				if (scope != null) {
					Scope parent = scope.getParent();
					if (parent != null) {
						parent.mergeWith(scope);
					}
				}
				b.getParent().replace(b, b.getChildren().get(0));
				(new BlocksEraser()).doSwitch(b);
			}
			
		}
		
		return null;
	}
	
	/**
	 * ignores Switches for the moment (IR is not fine ... 
	 * and also copy & connects methods).
	 * 
	 * @author amorvan
	 */
	@Override
	public Object caseSwitchBlock(SwitchBlock b) {
		return null;
	}
	
}
