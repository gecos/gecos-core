package fr.irisa.cairn.gecos.model.transforms.tools;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import tom.library.sl.VisitFailure;

public abstract class AbstractBlockTransformation {
	public static final boolean VERBOSE = false;

	protected static void debug(String mess) {
		if(VERBOSE) System.out.println(mess);
	}

	protected static void debug(int n, String mess) {
		debug("Rule "+n+" : "+mess);
	}
	/**
	 * Replace procedure's blocks by transformed ones.
	 * 
	 * @param proc
	 *            analyzed procedure
	 */
	public void transform(Procedure proc) {
		BasicBlock start = (BasicBlock) transform(proc.getStart());
		BasicBlock end = (BasicBlock) transform(proc.getEnd());
		CompositeBlock body = (CompositeBlock)transform(proc.getBody());
		if (start != proc.getStart()) {
			proc.setStart(start);
		}
		if (end != proc.getEnd()) {
			proc.setEnd(end);
		}
		if (body != proc.getBody()) {
			proc.setBody(body);
		}
	}
	/**
	 * Replace procedure's blocks by transformed ones.
	 * 
	 * @param proc
	 *            analyzed procedure
	 */

	public void transform(ProcedureSet ps) {
		for(Procedure p : ps.listProcedures()) {
			transform(p);
		}
	}

	/**
	 * Transform a block. If block has been modified then original is replaced
	 * by transformed one in its container.
	 * 
	 * @param b
	 * @return
	 */
	public Block transform(Block b) {
		try {
			if(b!=null) {
				Block trans = apply(b);
				if (trans != b) {
					debug(b + "=>" + trans);
					if (b.getParent() != null) {
						b.getParent().replace(b, trans);
					}
					return trans;
				} else {
					debug(b + "was not been transformed");
					return b;
				}
			} else {
				debug(" null block : skipping trasnformation");
				return b;

			}
		} catch (VisitFailure e) {
			return b;
		}
	}

	protected abstract Block apply(Block block) throws VisitFailure;
}
