/**
 */
package gecos.types;

import gecos.annotations.AnnotationsPackage;

import gecos.core.CorePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see gecos.types.TypesFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel importerID='org.eclipse.emf.importer.ecore' operationReflection='false' basePackage='gecos'"
 * @generated
 */
public interface TypesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "types";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.gecos.org/types";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "types";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TypesPackage eINSTANCE = gecos.types.impl.TypesPackageImpl.init();

	/**
	 * The meta object id for the '{@link gecos.types.TypesVisitable <em>Visitable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.TypesVisitable
	 * @see gecos.types.impl.TypesPackageImpl#getTypesVisitable()
	 * @generated
	 */
	int TYPES_VISITABLE = 0;

	/**
	 * The number of structural features of the '<em>Visitable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPES_VISITABLE_FEATURE_COUNT = CorePackage.GECOS_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.types.impl.FieldImpl <em>Field</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.FieldImpl
	 * @see gecos.types.impl.TypesPackageImpl#getField()
	 * @generated
	 */
	int FIELD = 1;

	/**
	 * The feature id for the '<em><b>Record</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__RECORD = TYPES_VISITABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__NAME = TYPES_VISITABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__TYPE = TYPES_VISITABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__OFFSET = TYPES_VISITABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Bitwidth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__BITWIDTH = TYPES_VISITABLE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_FEATURE_COUNT = TYPES_VISITABLE_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link gecos.types.impl.EnumeratorImpl <em>Enumerator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.EnumeratorImpl
	 * @see gecos.types.impl.TypesPackageImpl#getEnumerator()
	 * @generated
	 */
	int ENUMERATOR = 2;

	/**
	 * The feature id for the '<em><b>Enum Type</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATOR__ENUM_TYPE = TYPES_VISITABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATOR__NAME = TYPES_VISITABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATOR__EXPRESSION = TYPES_VISITABLE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Enumerator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATOR_FEATURE_COUNT = TYPES_VISITABLE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link gecos.types.impl.TypeImpl <em>Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.TypeImpl
	 * @see gecos.types.impl.TypesPackageImpl#getType()
	 * @generated
	 */
	int TYPE = 3;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__ANNOTATIONS = AnnotationsPackage.ANNOTATED_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__CONSTANT = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__VOLATILE = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__STORAGE_CLASS = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_FEATURE_COUNT = AnnotationsPackage.ANNOTATED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link gecos.types.impl.ArrayTypeImpl <em>Array Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.ArrayTypeImpl
	 * @see gecos.types.impl.TypesPackageImpl#getArrayType()
	 * @generated
	 */
	int ARRAY_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_TYPE__ANNOTATIONS = TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_TYPE__CONSTANT = TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_TYPE__VOLATILE = TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_TYPE__STORAGE_CLASS = TYPE__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Base</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_TYPE__BASE = TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Lower</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_TYPE__LOWER = TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Upper</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_TYPE__UPPER = TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Size Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_TYPE__SIZE_EXPR = TYPE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Array Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link gecos.types.impl.PtrTypeImpl <em>Ptr Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.PtrTypeImpl
	 * @see gecos.types.impl.TypesPackageImpl#getPtrType()
	 * @generated
	 */
	int PTR_TYPE = 5;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PTR_TYPE__ANNOTATIONS = TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PTR_TYPE__CONSTANT = TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PTR_TYPE__VOLATILE = TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PTR_TYPE__STORAGE_CLASS = TYPE__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Base</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PTR_TYPE__BASE = TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Restrict</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PTR_TYPE__RESTRICT = TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Ptr Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PTR_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.types.impl.__Internal_ParameterTypeWrapperImpl <em>Internal Parameter Type Wrapper</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.__Internal_ParameterTypeWrapperImpl
	 * @see gecos.types.impl.TypesPackageImpl#get__Internal_ParameterTypeWrapper()
	 * @generated
	 */
	int _INTERNAL_PARAMETER_TYPE_WRAPPER = 6;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int _INTERNAL_PARAMETER_TYPE_WRAPPER__PARAMETER = 0;

	/**
	 * The number of structural features of the '<em>Internal Parameter Type Wrapper</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int _INTERNAL_PARAMETER_TYPE_WRAPPER_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link gecos.types.impl.FunctionTypeImpl <em>Function Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.FunctionTypeImpl
	 * @see gecos.types.impl.TypesPackageImpl#getFunctionType()
	 * @generated
	 */
	int FUNCTION_TYPE = 7;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_TYPE__ANNOTATIONS = TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_TYPE__CONSTANT = TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_TYPE__VOLATILE = TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_TYPE__STORAGE_CLASS = TYPE__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_TYPE__RETURN_TYPE = TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>internal parameter wrapers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_TYPE__INTERNAL_PARAMETER_WRAPERS = TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Has Elipsis</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_TYPE__HAS_ELIPSIS = TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Inline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_TYPE__INLINE = TYPE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Noreturn</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_TYPE__NORETURN = TYPE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Function Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link gecos.types.impl.RecordTypeImpl <em>Record Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.RecordTypeImpl
	 * @see gecos.types.impl.TypesPackageImpl#getRecordType()
	 * @generated
	 */
	int RECORD_TYPE = 8;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_TYPE__ANNOTATIONS = TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_TYPE__CONSTANT = TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_TYPE__VOLATILE = TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_TYPE__STORAGE_CLASS = TYPE__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Fields</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_TYPE__FIELDS = TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_TYPE__KIND = TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Defined</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_TYPE__DEFINED = TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_TYPE__NAME = TYPE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Record Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link gecos.types.impl.AliasTypeImpl <em>Alias Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.AliasTypeImpl
	 * @see gecos.types.impl.TypesPackageImpl#getAliasType()
	 * @generated
	 */
	int ALIAS_TYPE = 9;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALIAS_TYPE__ANNOTATIONS = TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALIAS_TYPE__CONSTANT = TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALIAS_TYPE__VOLATILE = TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALIAS_TYPE__STORAGE_CLASS = TYPE__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Alias</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALIAS_TYPE__ALIAS = TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALIAS_TYPE__NAME = TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Alias Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALIAS_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.types.impl.EnumTypeImpl <em>Enum Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.EnumTypeImpl
	 * @see gecos.types.impl.TypesPackageImpl#getEnumType()
	 * @generated
	 */
	int ENUM_TYPE = 10;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_TYPE__ANNOTATIONS = TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_TYPE__CONSTANT = TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_TYPE__VOLATILE = TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_TYPE__STORAGE_CLASS = TYPE__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_TYPE__NAME = TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Enumerators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_TYPE__ENUMERATORS = TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Enum Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.types.impl.SimdTypeImpl <em>Simd Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.SimdTypeImpl
	 * @see gecos.types.impl.TypesPackageImpl#getSimdType()
	 * @generated
	 */
	int SIMD_TYPE = 11;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_TYPE__ANNOTATIONS = TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_TYPE__CONSTANT = TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_TYPE__VOLATILE = TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_TYPE__STORAGE_CLASS = TYPE__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Subword Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_TYPE__SUBWORD_TYPE = TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_TYPE__SIZE = TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Simd Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMD_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.types.impl.UndefinedTypeImpl <em>Undefined Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.UndefinedTypeImpl
	 * @see gecos.types.impl.TypesPackageImpl#getUndefinedType()
	 * @generated
	 */
	int UNDEFINED_TYPE = 12;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNDEFINED_TYPE__ANNOTATIONS = TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNDEFINED_TYPE__CONSTANT = TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNDEFINED_TYPE__VOLATILE = TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNDEFINED_TYPE__STORAGE_CLASS = TYPE__STORAGE_CLASS;

	/**
	 * The number of structural features of the '<em>Undefined Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNDEFINED_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.types.impl.ScalarTypeImpl <em>Scalar Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.ScalarTypeImpl
	 * @see gecos.types.impl.TypesPackageImpl#getScalarType()
	 * @generated
	 */
	int SCALAR_TYPE = 13;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALAR_TYPE__ANNOTATIONS = TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALAR_TYPE__CONSTANT = TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALAR_TYPE__VOLATILE = TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALAR_TYPE__STORAGE_CLASS = TYPE__STORAGE_CLASS;

	/**
	 * The number of structural features of the '<em>Scalar Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALAR_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.types.impl.BaseTypeImpl <em>Base Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.BaseTypeImpl
	 * @see gecos.types.impl.TypesPackageImpl#getBaseType()
	 * @generated
	 */
	int BASE_TYPE = 14;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_TYPE__ANNOTATIONS = SCALAR_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_TYPE__CONSTANT = SCALAR_TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_TYPE__VOLATILE = SCALAR_TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_TYPE__STORAGE_CLASS = SCALAR_TYPE__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_TYPE__SIZE = SCALAR_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Base Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_TYPE_FEATURE_COUNT = SCALAR_TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.types.impl.IntegerTypeImpl <em>Integer Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.IntegerTypeImpl
	 * @see gecos.types.impl.TypesPackageImpl#getIntegerType()
	 * @generated
	 */
	int INTEGER_TYPE = 15;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_TYPE__ANNOTATIONS = BASE_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_TYPE__CONSTANT = BASE_TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_TYPE__VOLATILE = BASE_TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_TYPE__STORAGE_CLASS = BASE_TYPE__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_TYPE__SIZE = BASE_TYPE__SIZE;

	/**
	 * The feature id for the '<em><b>Sign Modifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_TYPE__SIGN_MODIFIER = BASE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_TYPE__TYPE = BASE_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Integer Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_TYPE_FEATURE_COUNT = BASE_TYPE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.types.impl.FloatTypeImpl <em>Float Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.FloatTypeImpl
	 * @see gecos.types.impl.TypesPackageImpl#getFloatType()
	 * @generated
	 */
	int FLOAT_TYPE = 16;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_TYPE__ANNOTATIONS = BASE_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_TYPE__CONSTANT = BASE_TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_TYPE__VOLATILE = BASE_TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_TYPE__STORAGE_CLASS = BASE_TYPE__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_TYPE__SIZE = BASE_TYPE__SIZE;

	/**
	 * The feature id for the '<em><b>Precision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_TYPE__PRECISION = BASE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Float Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_TYPE_FEATURE_COUNT = BASE_TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.types.impl.BoolTypeImpl <em>Bool Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.BoolTypeImpl
	 * @see gecos.types.impl.TypesPackageImpl#getBoolType()
	 * @generated
	 */
	int BOOL_TYPE = 17;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOL_TYPE__ANNOTATIONS = BASE_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOL_TYPE__CONSTANT = BASE_TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOL_TYPE__VOLATILE = BASE_TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOL_TYPE__STORAGE_CLASS = BASE_TYPE__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOL_TYPE__SIZE = BASE_TYPE__SIZE;

	/**
	 * The number of structural features of the '<em>Bool Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOL_TYPE_FEATURE_COUNT = BASE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.types.impl.VoidTypeImpl <em>Void Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.VoidTypeImpl
	 * @see gecos.types.impl.TypesPackageImpl#getVoidType()
	 * @generated
	 */
	int VOID_TYPE = 18;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOID_TYPE__ANNOTATIONS = BASE_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOID_TYPE__CONSTANT = BASE_TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOID_TYPE__VOLATILE = BASE_TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOID_TYPE__STORAGE_CLASS = BASE_TYPE__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOID_TYPE__SIZE = BASE_TYPE__SIZE;

	/**
	 * The number of structural features of the '<em>Void Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOID_TYPE_FEATURE_COUNT = BASE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.types.impl.ACTypeImpl <em>AC Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.ACTypeImpl
	 * @see gecos.types.impl.TypesPackageImpl#getACType()
	 * @generated
	 */
	int AC_TYPE = 19;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_TYPE__ANNOTATIONS = BASE_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_TYPE__CONSTANT = BASE_TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_TYPE__VOLATILE = BASE_TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_TYPE__STORAGE_CLASS = BASE_TYPE__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_TYPE__SIZE = BASE_TYPE__SIZE;

	/**
	 * The feature id for the '<em><b>Signed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_TYPE__SIGNED = BASE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>AC Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_TYPE_FEATURE_COUNT = BASE_TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.types.impl.ACIntTypeImpl <em>AC Int Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.ACIntTypeImpl
	 * @see gecos.types.impl.TypesPackageImpl#getACIntType()
	 * @generated
	 */
	int AC_INT_TYPE = 20;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_INT_TYPE__ANNOTATIONS = ARRAY_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_INT_TYPE__CONSTANT = ARRAY_TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_INT_TYPE__VOLATILE = ARRAY_TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_INT_TYPE__STORAGE_CLASS = ARRAY_TYPE__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Base</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_INT_TYPE__BASE = ARRAY_TYPE__BASE;

	/**
	 * The feature id for the '<em><b>Lower</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_INT_TYPE__LOWER = ARRAY_TYPE__LOWER;

	/**
	 * The feature id for the '<em><b>Upper</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_INT_TYPE__UPPER = ARRAY_TYPE__UPPER;

	/**
	 * The feature id for the '<em><b>Size Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_INT_TYPE__SIZE_EXPR = ARRAY_TYPE__SIZE_EXPR;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_INT_TYPE__SIZE = ARRAY_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Signed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_INT_TYPE__SIGNED = ARRAY_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Bitwidth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_INT_TYPE__BITWIDTH = ARRAY_TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Overflow Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_INT_TYPE__OVERFLOW_MODE = ARRAY_TYPE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>AC Int Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_INT_TYPE_FEATURE_COUNT = ARRAY_TYPE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link gecos.types.impl.ACFixedTypeImpl <em>AC Fixed Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.ACFixedTypeImpl
	 * @see gecos.types.impl.TypesPackageImpl#getACFixedType()
	 * @generated
	 */
	int AC_FIXED_TYPE = 21;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FIXED_TYPE__ANNOTATIONS = ARRAY_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FIXED_TYPE__CONSTANT = ARRAY_TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FIXED_TYPE__VOLATILE = ARRAY_TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FIXED_TYPE__STORAGE_CLASS = ARRAY_TYPE__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Base</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FIXED_TYPE__BASE = ARRAY_TYPE__BASE;

	/**
	 * The feature id for the '<em><b>Lower</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FIXED_TYPE__LOWER = ARRAY_TYPE__LOWER;

	/**
	 * The feature id for the '<em><b>Upper</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FIXED_TYPE__UPPER = ARRAY_TYPE__UPPER;

	/**
	 * The feature id for the '<em><b>Size Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FIXED_TYPE__SIZE_EXPR = ARRAY_TYPE__SIZE_EXPR;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FIXED_TYPE__SIZE = ARRAY_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Signed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FIXED_TYPE__SIGNED = ARRAY_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Bitwidth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FIXED_TYPE__BITWIDTH = ARRAY_TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FIXED_TYPE__MODE = ARRAY_TYPE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Fraction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FIXED_TYPE__FRACTION = ARRAY_TYPE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Integer</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FIXED_TYPE__INTEGER = ARRAY_TYPE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Quantification Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FIXED_TYPE__QUANTIFICATION_MODE = ARRAY_TYPE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Overflow Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FIXED_TYPE__OVERFLOW_MODE = ARRAY_TYPE_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>AC Fixed Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FIXED_TYPE_FEATURE_COUNT = ARRAY_TYPE_FEATURE_COUNT + 8;

	/**
	 * The meta object id for the '{@link gecos.types.impl.ACFloatTypeImpl <em>AC Float Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.ACFloatTypeImpl
	 * @see gecos.types.impl.TypesPackageImpl#getACFloatType()
	 * @generated
	 */
	int AC_FLOAT_TYPE = 22;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FLOAT_TYPE__ANNOTATIONS = AC_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FLOAT_TYPE__CONSTANT = AC_TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FLOAT_TYPE__VOLATILE = AC_TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FLOAT_TYPE__STORAGE_CLASS = AC_TYPE__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FLOAT_TYPE__SIZE = AC_TYPE__SIZE;

	/**
	 * The feature id for the '<em><b>Signed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FLOAT_TYPE__SIGNED = AC_TYPE__SIGNED;

	/**
	 * The feature id for the '<em><b>Exponent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FLOAT_TYPE__EXPONENT = AC_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Mantissa</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FLOAT_TYPE__MANTISSA = AC_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Radix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FLOAT_TYPE__RADIX = AC_TYPE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>AC Float Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_FLOAT_TYPE_FEATURE_COUNT = AC_TYPE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link gecos.types.impl.ACComplexTypeImpl <em>AC Complex Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.ACComplexTypeImpl
	 * @see gecos.types.impl.TypesPackageImpl#getACComplexType()
	 * @generated
	 */
	int AC_COMPLEX_TYPE = 23;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_COMPLEX_TYPE__ANNOTATIONS = AC_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_COMPLEX_TYPE__CONSTANT = AC_TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_COMPLEX_TYPE__VOLATILE = AC_TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_COMPLEX_TYPE__STORAGE_CLASS = AC_TYPE__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_COMPLEX_TYPE__SIZE = AC_TYPE__SIZE;

	/**
	 * The feature id for the '<em><b>Signed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_COMPLEX_TYPE__SIGNED = AC_TYPE__SIGNED;

	/**
	 * The feature id for the '<em><b>Base Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_COMPLEX_TYPE__BASE_TYPE = AC_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>AC Complex Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_COMPLEX_TYPE_FEATURE_COUNT = AC_TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.types.impl.ACChannelTypeImpl <em>AC Channel Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.ACChannelTypeImpl
	 * @see gecos.types.impl.TypesPackageImpl#getACChannelType()
	 * @generated
	 */
	int AC_CHANNEL_TYPE = 24;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_CHANNEL_TYPE__ANNOTATIONS = AC_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_CHANNEL_TYPE__CONSTANT = AC_TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_CHANNEL_TYPE__VOLATILE = AC_TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_CHANNEL_TYPE__STORAGE_CLASS = AC_TYPE__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_CHANNEL_TYPE__SIZE = AC_TYPE__SIZE;

	/**
	 * The feature id for the '<em><b>Signed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_CHANNEL_TYPE__SIGNED = AC_TYPE__SIGNED;

	/**
	 * The feature id for the '<em><b>Base Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_CHANNEL_TYPE__BASE_TYPE = AC_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>AC Channel Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_CHANNEL_TYPE_FEATURE_COUNT = AC_TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.types.TypesVisitor <em>Visitor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.TypesVisitor
	 * @see gecos.types.impl.TypesPackageImpl#getTypesVisitor()
	 * @generated
	 */
	int TYPES_VISITOR = 25;

	/**
	 * The number of structural features of the '<em>Visitor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPES_VISITOR_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link gecos.types.StorageClassSpecifiers <em>Storage Class Specifiers</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.StorageClassSpecifiers
	 * @see gecos.types.impl.TypesPackageImpl#getStorageClassSpecifiers()
	 * @generated
	 */
	int STORAGE_CLASS_SPECIFIERS = 26;

	/**
	 * The meta object id for the '{@link gecos.types.Kinds <em>Kinds</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.Kinds
	 * @see gecos.types.impl.TypesPackageImpl#getKinds()
	 * @generated
	 */
	int KINDS = 27;

	/**
	 * The meta object id for the '{@link gecos.types.IntegerTypes <em>Integer Types</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.IntegerTypes
	 * @see gecos.types.impl.TypesPackageImpl#getIntegerTypes()
	 * @generated
	 */
	int INTEGER_TYPES = 28;

	/**
	 * The meta object id for the '{@link gecos.types.SignModifiers <em>Sign Modifiers</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.SignModifiers
	 * @see gecos.types.impl.TypesPackageImpl#getSignModifiers()
	 * @generated
	 */
	int SIGN_MODIFIERS = 29;

	/**
	 * The meta object id for the '{@link gecos.types.FloatPrecisions <em>Float Precisions</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.FloatPrecisions
	 * @see gecos.types.impl.TypesPackageImpl#getFloatPrecisions()
	 * @generated
	 */
	int FLOAT_PRECISIONS = 30;

	/**
	 * The meta object id for the '{@link gecos.types.OverflowMode <em>Overflow Mode</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.OverflowMode
	 * @see gecos.types.impl.TypesPackageImpl#getOverflowMode()
	 * @generated
	 */
	int OVERFLOW_MODE = 31;

	/**
	 * The meta object id for the '{@link gecos.types.QuantificationMode <em>Quantification Mode</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.QuantificationMode
	 * @see gecos.types.impl.TypesPackageImpl#getQuantificationMode()
	 * @generated
	 */
	int QUANTIFICATION_MODE = 32;

	/**
	 * The meta object id for the '<em>String</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see gecos.types.impl.TypesPackageImpl#getString()
	 * @generated
	 */
	int STRING = 33;

	/**
	 * The meta object id for the '<em>long</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.TypesPackageImpl#getlong()
	 * @generated
	 */
	int LONG = 34;

	/**
	 * The meta object id for the '<em>int</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.TypesPackageImpl#getint()
	 * @generated
	 */
	int INT = 35;

	/**
	 * The meta object id for the '<em>boolean</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.types.impl.TypesPackageImpl#getboolean()
	 * @generated
	 */
	int BOOLEAN = 36;


	/**
	 * Returns the meta object for class '{@link gecos.types.TypesVisitable <em>Visitable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visitable</em>'.
	 * @see gecos.types.TypesVisitable
	 * @generated
	 */
	EClass getTypesVisitable();

	/**
	 * Returns the meta object for class '{@link gecos.types.Field <em>Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Field</em>'.
	 * @see gecos.types.Field
	 * @generated
	 */
	EClass getField();

	/**
	 * Returns the meta object for the container reference '{@link gecos.types.Field#getRecord <em>Record</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Record</em>'.
	 * @see gecos.types.Field#getRecord()
	 * @see #getField()
	 * @generated
	 */
	EReference getField_Record();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.Field#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see gecos.types.Field#getName()
	 * @see #getField()
	 * @generated
	 */
	EAttribute getField_Name();

	/**
	 * Returns the meta object for the reference '{@link gecos.types.Field#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see gecos.types.Field#getType()
	 * @see #getField()
	 * @generated
	 */
	EReference getField_Type();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.Field#getOffset <em>Offset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Offset</em>'.
	 * @see gecos.types.Field#getOffset()
	 * @see #getField()
	 * @generated
	 */
	EAttribute getField_Offset();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.Field#getBitwidth <em>Bitwidth</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bitwidth</em>'.
	 * @see gecos.types.Field#getBitwidth()
	 * @see #getField()
	 * @generated
	 */
	EAttribute getField_Bitwidth();

	/**
	 * Returns the meta object for class '{@link gecos.types.Enumerator <em>Enumerator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enumerator</em>'.
	 * @see gecos.types.Enumerator
	 * @generated
	 */
	EClass getEnumerator();

	/**
	 * Returns the meta object for the container reference '{@link gecos.types.Enumerator#getEnumType <em>Enum Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Enum Type</em>'.
	 * @see gecos.types.Enumerator#getEnumType()
	 * @see #getEnumerator()
	 * @generated
	 */
	EReference getEnumerator_EnumType();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.Enumerator#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see gecos.types.Enumerator#getName()
	 * @see #getEnumerator()
	 * @generated
	 */
	EAttribute getEnumerator_Name();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.types.Enumerator#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see gecos.types.Enumerator#getExpression()
	 * @see #getEnumerator()
	 * @generated
	 */
	EReference getEnumerator_Expression();

	/**
	 * Returns the meta object for class '{@link gecos.types.Type <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type</em>'.
	 * @see gecos.types.Type
	 * @generated
	 */
	EClass getType();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.Type#isConstant <em>Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Constant</em>'.
	 * @see gecos.types.Type#isConstant()
	 * @see #getType()
	 * @generated
	 */
	EAttribute getType_Constant();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.Type#isVolatile <em>Volatile</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Volatile</em>'.
	 * @see gecos.types.Type#isVolatile()
	 * @see #getType()
	 * @generated
	 */
	EAttribute getType_Volatile();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.Type#getStorageClass <em>Storage Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Storage Class</em>'.
	 * @see gecos.types.Type#getStorageClass()
	 * @see #getType()
	 * @generated
	 */
	EAttribute getType_StorageClass();

	/**
	 * Returns the meta object for class '{@link gecos.types.ArrayType <em>Array Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Array Type</em>'.
	 * @see gecos.types.ArrayType
	 * @generated
	 */
	EClass getArrayType();

	/**
	 * Returns the meta object for the reference '{@link gecos.types.ArrayType#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base</em>'.
	 * @see gecos.types.ArrayType#getBase()
	 * @see #getArrayType()
	 * @generated
	 */
	EReference getArrayType_Base();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.ArrayType#getLower <em>Lower</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lower</em>'.
	 * @see gecos.types.ArrayType#getLower()
	 * @see #getArrayType()
	 * @generated
	 */
	EAttribute getArrayType_Lower();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.ArrayType#getUpper <em>Upper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Upper</em>'.
	 * @see gecos.types.ArrayType#getUpper()
	 * @see #getArrayType()
	 * @generated
	 */
	EAttribute getArrayType_Upper();

	/**
	 * Returns the meta object for the containment reference '{@link gecos.types.ArrayType#getSizeExpr <em>Size Expr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Size Expr</em>'.
	 * @see gecos.types.ArrayType#getSizeExpr()
	 * @see #getArrayType()
	 * @generated
	 */
	EReference getArrayType_SizeExpr();

	/**
	 * Returns the meta object for class '{@link gecos.types.PtrType <em>Ptr Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ptr Type</em>'.
	 * @see gecos.types.PtrType
	 * @generated
	 */
	EClass getPtrType();

	/**
	 * Returns the meta object for the reference '{@link gecos.types.PtrType#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base</em>'.
	 * @see gecos.types.PtrType#getBase()
	 * @see #getPtrType()
	 * @generated
	 */
	EReference getPtrType_Base();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.PtrType#isRestrict <em>Restrict</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Restrict</em>'.
	 * @see gecos.types.PtrType#isRestrict()
	 * @see #getPtrType()
	 * @generated
	 */
	EAttribute getPtrType_Restrict();

	/**
	 * Returns the meta object for class '{@link gecos.types.__Internal_ParameterTypeWrapper <em>Internal Parameter Type Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Internal Parameter Type Wrapper</em>'.
	 * @see gecos.types.__Internal_ParameterTypeWrapper
	 * @generated
	 */
	EClass get__Internal_ParameterTypeWrapper();

	/**
	 * Returns the meta object for the reference '{@link gecos.types.__Internal_ParameterTypeWrapper#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parameter</em>'.
	 * @see gecos.types.__Internal_ParameterTypeWrapper#getParameter()
	 * @see #get__Internal_ParameterTypeWrapper()
	 * @generated
	 */
	EReference get__Internal_ParameterTypeWrapper_Parameter();

	/**
	 * Returns the meta object for class '{@link gecos.types.FunctionType <em>Function Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function Type</em>'.
	 * @see gecos.types.FunctionType
	 * @generated
	 */
	EClass getFunctionType();

	/**
	 * Returns the meta object for the reference '{@link gecos.types.FunctionType#getReturnType <em>Return Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Return Type</em>'.
	 * @see gecos.types.FunctionType#getReturnType()
	 * @see #getFunctionType()
	 * @generated
	 */
	EReference getFunctionType_ReturnType();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.types.FunctionType#get__internal_parameter_wrapers <em>internal parameter wrapers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>internal parameter wrapers</em>'.
	 * @see gecos.types.FunctionType#get__internal_parameter_wrapers()
	 * @see #getFunctionType()
	 * @generated
	 */
	EReference getFunctionType___internal_parameter_wrapers();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.FunctionType#isHasElipsis <em>Has Elipsis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Elipsis</em>'.
	 * @see gecos.types.FunctionType#isHasElipsis()
	 * @see #getFunctionType()
	 * @generated
	 */
	EAttribute getFunctionType_HasElipsis();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.FunctionType#isInline <em>Inline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Inline</em>'.
	 * @see gecos.types.FunctionType#isInline()
	 * @see #getFunctionType()
	 * @generated
	 */
	EAttribute getFunctionType_Inline();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.FunctionType#isNoreturn <em>Noreturn</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Noreturn</em>'.
	 * @see gecos.types.FunctionType#isNoreturn()
	 * @see #getFunctionType()
	 * @generated
	 */
	EAttribute getFunctionType_Noreturn();

	/**
	 * Returns the meta object for class '{@link gecos.types.RecordType <em>Record Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Record Type</em>'.
	 * @see gecos.types.RecordType
	 * @generated
	 */
	EClass getRecordType();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.types.RecordType#getFields <em>Fields</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fields</em>'.
	 * @see gecos.types.RecordType#getFields()
	 * @see #getRecordType()
	 * @generated
	 */
	EReference getRecordType_Fields();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.RecordType#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see gecos.types.RecordType#getKind()
	 * @see #getRecordType()
	 * @generated
	 */
	EAttribute getRecordType_Kind();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.RecordType#isDefined <em>Defined</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Defined</em>'.
	 * @see gecos.types.RecordType#isDefined()
	 * @see #getRecordType()
	 * @generated
	 */
	EAttribute getRecordType_Defined();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.RecordType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see gecos.types.RecordType#getName()
	 * @see #getRecordType()
	 * @generated
	 */
	EAttribute getRecordType_Name();

	/**
	 * Returns the meta object for class '{@link gecos.types.AliasType <em>Alias Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alias Type</em>'.
	 * @see gecos.types.AliasType
	 * @generated
	 */
	EClass getAliasType();

	/**
	 * Returns the meta object for the reference '{@link gecos.types.AliasType#getAlias <em>Alias</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Alias</em>'.
	 * @see gecos.types.AliasType#getAlias()
	 * @see #getAliasType()
	 * @generated
	 */
	EReference getAliasType_Alias();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.AliasType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see gecos.types.AliasType#getName()
	 * @see #getAliasType()
	 * @generated
	 */
	EAttribute getAliasType_Name();

	/**
	 * Returns the meta object for class '{@link gecos.types.EnumType <em>Enum Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enum Type</em>'.
	 * @see gecos.types.EnumType
	 * @generated
	 */
	EClass getEnumType();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.EnumType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see gecos.types.EnumType#getName()
	 * @see #getEnumType()
	 * @generated
	 */
	EAttribute getEnumType_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.types.EnumType#getEnumerators <em>Enumerators</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Enumerators</em>'.
	 * @see gecos.types.EnumType#getEnumerators()
	 * @see #getEnumType()
	 * @generated
	 */
	EReference getEnumType_Enumerators();

	/**
	 * Returns the meta object for class '{@link gecos.types.SimdType <em>Simd Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simd Type</em>'.
	 * @see gecos.types.SimdType
	 * @generated
	 */
	EClass getSimdType();

	/**
	 * Returns the meta object for the reference '{@link gecos.types.SimdType#getSubwordType <em>Subword Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Subword Type</em>'.
	 * @see gecos.types.SimdType#getSubwordType()
	 * @see #getSimdType()
	 * @generated
	 */
	EReference getSimdType_SubwordType();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.SimdType#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size</em>'.
	 * @see gecos.types.SimdType#getSize()
	 * @see #getSimdType()
	 * @generated
	 */
	EAttribute getSimdType_Size();

	/**
	 * Returns the meta object for class '{@link gecos.types.UndefinedType <em>Undefined Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Undefined Type</em>'.
	 * @see gecos.types.UndefinedType
	 * @generated
	 */
	EClass getUndefinedType();

	/**
	 * Returns the meta object for class '{@link gecos.types.ScalarType <em>Scalar Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scalar Type</em>'.
	 * @see gecos.types.ScalarType
	 * @generated
	 */
	EClass getScalarType();

	/**
	 * Returns the meta object for class '{@link gecos.types.BaseType <em>Base Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Type</em>'.
	 * @see gecos.types.BaseType
	 * @generated
	 */
	EClass getBaseType();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.BaseType#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size</em>'.
	 * @see gecos.types.BaseType#getSize()
	 * @see #getBaseType()
	 * @generated
	 */
	EAttribute getBaseType_Size();

	/**
	 * Returns the meta object for class '{@link gecos.types.IntegerType <em>Integer Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer Type</em>'.
	 * @see gecos.types.IntegerType
	 * @generated
	 */
	EClass getIntegerType();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.IntegerType#getSignModifier <em>Sign Modifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sign Modifier</em>'.
	 * @see gecos.types.IntegerType#getSignModifier()
	 * @see #getIntegerType()
	 * @generated
	 */
	EAttribute getIntegerType_SignModifier();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.IntegerType#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see gecos.types.IntegerType#getType()
	 * @see #getIntegerType()
	 * @generated
	 */
	EAttribute getIntegerType_Type();

	/**
	 * Returns the meta object for class '{@link gecos.types.FloatType <em>Float Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Float Type</em>'.
	 * @see gecos.types.FloatType
	 * @generated
	 */
	EClass getFloatType();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.FloatType#getPrecision <em>Precision</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Precision</em>'.
	 * @see gecos.types.FloatType#getPrecision()
	 * @see #getFloatType()
	 * @generated
	 */
	EAttribute getFloatType_Precision();

	/**
	 * Returns the meta object for class '{@link gecos.types.BoolType <em>Bool Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bool Type</em>'.
	 * @see gecos.types.BoolType
	 * @generated
	 */
	EClass getBoolType();

	/**
	 * Returns the meta object for class '{@link gecos.types.VoidType <em>Void Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Void Type</em>'.
	 * @see gecos.types.VoidType
	 * @generated
	 */
	EClass getVoidType();

	/**
	 * Returns the meta object for class '{@link gecos.types.ACType <em>AC Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AC Type</em>'.
	 * @see gecos.types.ACType
	 * @generated
	 */
	EClass getACType();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.ACType#isSigned <em>Signed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Signed</em>'.
	 * @see gecos.types.ACType#isSigned()
	 * @see #getACType()
	 * @generated
	 */
	EAttribute getACType_Signed();

	/**
	 * Returns the meta object for class '{@link gecos.types.ACIntType <em>AC Int Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AC Int Type</em>'.
	 * @see gecos.types.ACIntType
	 * @generated
	 */
	EClass getACIntType();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.ACIntType#getBitwidth <em>Bitwidth</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bitwidth</em>'.
	 * @see gecos.types.ACIntType#getBitwidth()
	 * @see #getACIntType()
	 * @generated
	 */
	EAttribute getACIntType_Bitwidth();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.ACIntType#getOverflowMode <em>Overflow Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Overflow Mode</em>'.
	 * @see gecos.types.ACIntType#getOverflowMode()
	 * @see #getACIntType()
	 * @generated
	 */
	EAttribute getACIntType_OverflowMode();

	/**
	 * Returns the meta object for class '{@link gecos.types.ACFixedType <em>AC Fixed Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AC Fixed Type</em>'.
	 * @see gecos.types.ACFixedType
	 * @generated
	 */
	EClass getACFixedType();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.ACFixedType#getBitwidth <em>Bitwidth</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bitwidth</em>'.
	 * @see gecos.types.ACFixedType#getBitwidth()
	 * @see #getACFixedType()
	 * @generated
	 */
	EAttribute getACFixedType_Bitwidth();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.ACFixedType#getMode <em>Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mode</em>'.
	 * @see gecos.types.ACFixedType#getMode()
	 * @see #getACFixedType()
	 * @generated
	 */
	EAttribute getACFixedType_Mode();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.ACFixedType#getFraction <em>Fraction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fraction</em>'.
	 * @see gecos.types.ACFixedType#getFraction()
	 * @see #getACFixedType()
	 * @generated
	 */
	EAttribute getACFixedType_Fraction();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.ACFixedType#getInteger <em>Integer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Integer</em>'.
	 * @see gecos.types.ACFixedType#getInteger()
	 * @see #getACFixedType()
	 * @generated
	 */
	EAttribute getACFixedType_Integer();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.ACFixedType#getQuantificationMode <em>Quantification Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantification Mode</em>'.
	 * @see gecos.types.ACFixedType#getQuantificationMode()
	 * @see #getACFixedType()
	 * @generated
	 */
	EAttribute getACFixedType_QuantificationMode();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.ACFixedType#getOverflowMode <em>Overflow Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Overflow Mode</em>'.
	 * @see gecos.types.ACFixedType#getOverflowMode()
	 * @see #getACFixedType()
	 * @generated
	 */
	EAttribute getACFixedType_OverflowMode();

	/**
	 * Returns the meta object for class '{@link gecos.types.ACFloatType <em>AC Float Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AC Float Type</em>'.
	 * @see gecos.types.ACFloatType
	 * @generated
	 */
	EClass getACFloatType();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.ACFloatType#getExponent <em>Exponent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Exponent</em>'.
	 * @see gecos.types.ACFloatType#getExponent()
	 * @see #getACFloatType()
	 * @generated
	 */
	EAttribute getACFloatType_Exponent();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.ACFloatType#getMantissa <em>Mantissa</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mantissa</em>'.
	 * @see gecos.types.ACFloatType#getMantissa()
	 * @see #getACFloatType()
	 * @generated
	 */
	EAttribute getACFloatType_Mantissa();

	/**
	 * Returns the meta object for the attribute '{@link gecos.types.ACFloatType#getRadix <em>Radix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Radix</em>'.
	 * @see gecos.types.ACFloatType#getRadix()
	 * @see #getACFloatType()
	 * @generated
	 */
	EAttribute getACFloatType_Radix();

	/**
	 * Returns the meta object for class '{@link gecos.types.ACComplexType <em>AC Complex Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AC Complex Type</em>'.
	 * @see gecos.types.ACComplexType
	 * @generated
	 */
	EClass getACComplexType();

	/**
	 * Returns the meta object for the reference '{@link gecos.types.ACComplexType#getBaseType <em>Base Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Type</em>'.
	 * @see gecos.types.ACComplexType#getBaseType()
	 * @see #getACComplexType()
	 * @generated
	 */
	EReference getACComplexType_BaseType();

	/**
	 * Returns the meta object for class '{@link gecos.types.ACChannelType <em>AC Channel Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AC Channel Type</em>'.
	 * @see gecos.types.ACChannelType
	 * @generated
	 */
	EClass getACChannelType();

	/**
	 * Returns the meta object for the reference '{@link gecos.types.ACChannelType#getBaseType <em>Base Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Type</em>'.
	 * @see gecos.types.ACChannelType#getBaseType()
	 * @see #getACChannelType()
	 * @generated
	 */
	EReference getACChannelType_BaseType();

	/**
	 * Returns the meta object for class '{@link gecos.types.TypesVisitor <em>Visitor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visitor</em>'.
	 * @see gecos.types.TypesVisitor
	 * @generated
	 */
	EClass getTypesVisitor();

	/**
	 * Returns the meta object for enum '{@link gecos.types.StorageClassSpecifiers <em>Storage Class Specifiers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Storage Class Specifiers</em>'.
	 * @see gecos.types.StorageClassSpecifiers
	 * @generated
	 */
	EEnum getStorageClassSpecifiers();

	/**
	 * Returns the meta object for enum '{@link gecos.types.Kinds <em>Kinds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Kinds</em>'.
	 * @see gecos.types.Kinds
	 * @generated
	 */
	EEnum getKinds();

	/**
	 * Returns the meta object for enum '{@link gecos.types.IntegerTypes <em>Integer Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Integer Types</em>'.
	 * @see gecos.types.IntegerTypes
	 * @generated
	 */
	EEnum getIntegerTypes();

	/**
	 * Returns the meta object for enum '{@link gecos.types.SignModifiers <em>Sign Modifiers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Sign Modifiers</em>'.
	 * @see gecos.types.SignModifiers
	 * @generated
	 */
	EEnum getSignModifiers();

	/**
	 * Returns the meta object for enum '{@link gecos.types.FloatPrecisions <em>Float Precisions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Float Precisions</em>'.
	 * @see gecos.types.FloatPrecisions
	 * @generated
	 */
	EEnum getFloatPrecisions();

	/**
	 * Returns the meta object for enum '{@link gecos.types.OverflowMode <em>Overflow Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Overflow Mode</em>'.
	 * @see gecos.types.OverflowMode
	 * @generated
	 */
	EEnum getOverflowMode();

	/**
	 * Returns the meta object for enum '{@link gecos.types.QuantificationMode <em>Quantification Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Quantification Mode</em>'.
	 * @see gecos.types.QuantificationMode
	 * @generated
	 */
	EEnum getQuantificationMode();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>String</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getString();

	/**
	 * Returns the meta object for data type '<em>long</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>long</em>'.
	 * @model instanceClass="long"
	 * @generated
	 */
	EDataType getlong();

	/**
	 * Returns the meta object for data type '<em>int</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>int</em>'.
	 * @model instanceClass="int"
	 * @generated
	 */
	EDataType getint();

	/**
	 * Returns the meta object for data type '<em>boolean</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>boolean</em>'.
	 * @model instanceClass="boolean"
	 * @generated
	 */
	EDataType getboolean();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TypesFactory getTypesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link gecos.types.TypesVisitable <em>Visitable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.TypesVisitable
		 * @see gecos.types.impl.TypesPackageImpl#getTypesVisitable()
		 * @generated
		 */
		EClass TYPES_VISITABLE = eINSTANCE.getTypesVisitable();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.FieldImpl <em>Field</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.FieldImpl
		 * @see gecos.types.impl.TypesPackageImpl#getField()
		 * @generated
		 */
		EClass FIELD = eINSTANCE.getField();

		/**
		 * The meta object literal for the '<em><b>Record</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIELD__RECORD = eINSTANCE.getField_Record();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD__NAME = eINSTANCE.getField_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIELD__TYPE = eINSTANCE.getField_Type();

		/**
		 * The meta object literal for the '<em><b>Offset</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD__OFFSET = eINSTANCE.getField_Offset();

		/**
		 * The meta object literal for the '<em><b>Bitwidth</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD__BITWIDTH = eINSTANCE.getField_Bitwidth();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.EnumeratorImpl <em>Enumerator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.EnumeratorImpl
		 * @see gecos.types.impl.TypesPackageImpl#getEnumerator()
		 * @generated
		 */
		EClass ENUMERATOR = eINSTANCE.getEnumerator();

		/**
		 * The meta object literal for the '<em><b>Enum Type</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENUMERATOR__ENUM_TYPE = eINSTANCE.getEnumerator_EnumType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENUMERATOR__NAME = eINSTANCE.getEnumerator_Name();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENUMERATOR__EXPRESSION = eINSTANCE.getEnumerator_Expression();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.TypeImpl <em>Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.TypeImpl
		 * @see gecos.types.impl.TypesPackageImpl#getType()
		 * @generated
		 */
		EClass TYPE = eINSTANCE.getType();

		/**
		 * The meta object literal for the '<em><b>Constant</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TYPE__CONSTANT = eINSTANCE.getType_Constant();

		/**
		 * The meta object literal for the '<em><b>Volatile</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TYPE__VOLATILE = eINSTANCE.getType_Volatile();

		/**
		 * The meta object literal for the '<em><b>Storage Class</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TYPE__STORAGE_CLASS = eINSTANCE.getType_StorageClass();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.ArrayTypeImpl <em>Array Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.ArrayTypeImpl
		 * @see gecos.types.impl.TypesPackageImpl#getArrayType()
		 * @generated
		 */
		EClass ARRAY_TYPE = eINSTANCE.getArrayType();

		/**
		 * The meta object literal for the '<em><b>Base</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARRAY_TYPE__BASE = eINSTANCE.getArrayType_Base();

		/**
		 * The meta object literal for the '<em><b>Lower</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARRAY_TYPE__LOWER = eINSTANCE.getArrayType_Lower();

		/**
		 * The meta object literal for the '<em><b>Upper</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARRAY_TYPE__UPPER = eINSTANCE.getArrayType_Upper();

		/**
		 * The meta object literal for the '<em><b>Size Expr</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARRAY_TYPE__SIZE_EXPR = eINSTANCE.getArrayType_SizeExpr();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.PtrTypeImpl <em>Ptr Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.PtrTypeImpl
		 * @see gecos.types.impl.TypesPackageImpl#getPtrType()
		 * @generated
		 */
		EClass PTR_TYPE = eINSTANCE.getPtrType();

		/**
		 * The meta object literal for the '<em><b>Base</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PTR_TYPE__BASE = eINSTANCE.getPtrType_Base();

		/**
		 * The meta object literal for the '<em><b>Restrict</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PTR_TYPE__RESTRICT = eINSTANCE.getPtrType_Restrict();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.__Internal_ParameterTypeWrapperImpl <em>Internal Parameter Type Wrapper</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.__Internal_ParameterTypeWrapperImpl
		 * @see gecos.types.impl.TypesPackageImpl#get__Internal_ParameterTypeWrapper()
		 * @generated
		 */
		EClass _INTERNAL_PARAMETER_TYPE_WRAPPER = eINSTANCE.get__Internal_ParameterTypeWrapper();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference _INTERNAL_PARAMETER_TYPE_WRAPPER__PARAMETER = eINSTANCE.get__Internal_ParameterTypeWrapper_Parameter();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.FunctionTypeImpl <em>Function Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.FunctionTypeImpl
		 * @see gecos.types.impl.TypesPackageImpl#getFunctionType()
		 * @generated
		 */
		EClass FUNCTION_TYPE = eINSTANCE.getFunctionType();

		/**
		 * The meta object literal for the '<em><b>Return Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_TYPE__RETURN_TYPE = eINSTANCE.getFunctionType_ReturnType();

		/**
		 * The meta object literal for the '<em><b>internal parameter wrapers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_TYPE__INTERNAL_PARAMETER_WRAPERS = eINSTANCE.getFunctionType___internal_parameter_wrapers();

		/**
		 * The meta object literal for the '<em><b>Has Elipsis</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTION_TYPE__HAS_ELIPSIS = eINSTANCE.getFunctionType_HasElipsis();

		/**
		 * The meta object literal for the '<em><b>Inline</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTION_TYPE__INLINE = eINSTANCE.getFunctionType_Inline();

		/**
		 * The meta object literal for the '<em><b>Noreturn</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTION_TYPE__NORETURN = eINSTANCE.getFunctionType_Noreturn();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.RecordTypeImpl <em>Record Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.RecordTypeImpl
		 * @see gecos.types.impl.TypesPackageImpl#getRecordType()
		 * @generated
		 */
		EClass RECORD_TYPE = eINSTANCE.getRecordType();

		/**
		 * The meta object literal for the '<em><b>Fields</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RECORD_TYPE__FIELDS = eINSTANCE.getRecordType_Fields();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RECORD_TYPE__KIND = eINSTANCE.getRecordType_Kind();

		/**
		 * The meta object literal for the '<em><b>Defined</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RECORD_TYPE__DEFINED = eINSTANCE.getRecordType_Defined();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RECORD_TYPE__NAME = eINSTANCE.getRecordType_Name();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.AliasTypeImpl <em>Alias Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.AliasTypeImpl
		 * @see gecos.types.impl.TypesPackageImpl#getAliasType()
		 * @generated
		 */
		EClass ALIAS_TYPE = eINSTANCE.getAliasType();

		/**
		 * The meta object literal for the '<em><b>Alias</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALIAS_TYPE__ALIAS = eINSTANCE.getAliasType_Alias();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ALIAS_TYPE__NAME = eINSTANCE.getAliasType_Name();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.EnumTypeImpl <em>Enum Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.EnumTypeImpl
		 * @see gecos.types.impl.TypesPackageImpl#getEnumType()
		 * @generated
		 */
		EClass ENUM_TYPE = eINSTANCE.getEnumType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENUM_TYPE__NAME = eINSTANCE.getEnumType_Name();

		/**
		 * The meta object literal for the '<em><b>Enumerators</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENUM_TYPE__ENUMERATORS = eINSTANCE.getEnumType_Enumerators();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.SimdTypeImpl <em>Simd Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.SimdTypeImpl
		 * @see gecos.types.impl.TypesPackageImpl#getSimdType()
		 * @generated
		 */
		EClass SIMD_TYPE = eINSTANCE.getSimdType();

		/**
		 * The meta object literal for the '<em><b>Subword Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMD_TYPE__SUBWORD_TYPE = eINSTANCE.getSimdType_SubwordType();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMD_TYPE__SIZE = eINSTANCE.getSimdType_Size();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.UndefinedTypeImpl <em>Undefined Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.UndefinedTypeImpl
		 * @see gecos.types.impl.TypesPackageImpl#getUndefinedType()
		 * @generated
		 */
		EClass UNDEFINED_TYPE = eINSTANCE.getUndefinedType();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.ScalarTypeImpl <em>Scalar Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.ScalarTypeImpl
		 * @see gecos.types.impl.TypesPackageImpl#getScalarType()
		 * @generated
		 */
		EClass SCALAR_TYPE = eINSTANCE.getScalarType();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.BaseTypeImpl <em>Base Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.BaseTypeImpl
		 * @see gecos.types.impl.TypesPackageImpl#getBaseType()
		 * @generated
		 */
		EClass BASE_TYPE = eINSTANCE.getBaseType();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_TYPE__SIZE = eINSTANCE.getBaseType_Size();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.IntegerTypeImpl <em>Integer Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.IntegerTypeImpl
		 * @see gecos.types.impl.TypesPackageImpl#getIntegerType()
		 * @generated
		 */
		EClass INTEGER_TYPE = eINSTANCE.getIntegerType();

		/**
		 * The meta object literal for the '<em><b>Sign Modifier</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_TYPE__SIGN_MODIFIER = eINSTANCE.getIntegerType_SignModifier();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_TYPE__TYPE = eINSTANCE.getIntegerType_Type();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.FloatTypeImpl <em>Float Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.FloatTypeImpl
		 * @see gecos.types.impl.TypesPackageImpl#getFloatType()
		 * @generated
		 */
		EClass FLOAT_TYPE = eINSTANCE.getFloatType();

		/**
		 * The meta object literal for the '<em><b>Precision</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FLOAT_TYPE__PRECISION = eINSTANCE.getFloatType_Precision();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.BoolTypeImpl <em>Bool Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.BoolTypeImpl
		 * @see gecos.types.impl.TypesPackageImpl#getBoolType()
		 * @generated
		 */
		EClass BOOL_TYPE = eINSTANCE.getBoolType();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.VoidTypeImpl <em>Void Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.VoidTypeImpl
		 * @see gecos.types.impl.TypesPackageImpl#getVoidType()
		 * @generated
		 */
		EClass VOID_TYPE = eINSTANCE.getVoidType();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.ACTypeImpl <em>AC Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.ACTypeImpl
		 * @see gecos.types.impl.TypesPackageImpl#getACType()
		 * @generated
		 */
		EClass AC_TYPE = eINSTANCE.getACType();

		/**
		 * The meta object literal for the '<em><b>Signed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AC_TYPE__SIGNED = eINSTANCE.getACType_Signed();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.ACIntTypeImpl <em>AC Int Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.ACIntTypeImpl
		 * @see gecos.types.impl.TypesPackageImpl#getACIntType()
		 * @generated
		 */
		EClass AC_INT_TYPE = eINSTANCE.getACIntType();

		/**
		 * The meta object literal for the '<em><b>Bitwidth</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AC_INT_TYPE__BITWIDTH = eINSTANCE.getACIntType_Bitwidth();

		/**
		 * The meta object literal for the '<em><b>Overflow Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AC_INT_TYPE__OVERFLOW_MODE = eINSTANCE.getACIntType_OverflowMode();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.ACFixedTypeImpl <em>AC Fixed Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.ACFixedTypeImpl
		 * @see gecos.types.impl.TypesPackageImpl#getACFixedType()
		 * @generated
		 */
		EClass AC_FIXED_TYPE = eINSTANCE.getACFixedType();

		/**
		 * The meta object literal for the '<em><b>Bitwidth</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AC_FIXED_TYPE__BITWIDTH = eINSTANCE.getACFixedType_Bitwidth();

		/**
		 * The meta object literal for the '<em><b>Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AC_FIXED_TYPE__MODE = eINSTANCE.getACFixedType_Mode();

		/**
		 * The meta object literal for the '<em><b>Fraction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AC_FIXED_TYPE__FRACTION = eINSTANCE.getACFixedType_Fraction();

		/**
		 * The meta object literal for the '<em><b>Integer</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AC_FIXED_TYPE__INTEGER = eINSTANCE.getACFixedType_Integer();

		/**
		 * The meta object literal for the '<em><b>Quantification Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AC_FIXED_TYPE__QUANTIFICATION_MODE = eINSTANCE.getACFixedType_QuantificationMode();

		/**
		 * The meta object literal for the '<em><b>Overflow Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AC_FIXED_TYPE__OVERFLOW_MODE = eINSTANCE.getACFixedType_OverflowMode();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.ACFloatTypeImpl <em>AC Float Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.ACFloatTypeImpl
		 * @see gecos.types.impl.TypesPackageImpl#getACFloatType()
		 * @generated
		 */
		EClass AC_FLOAT_TYPE = eINSTANCE.getACFloatType();

		/**
		 * The meta object literal for the '<em><b>Exponent</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AC_FLOAT_TYPE__EXPONENT = eINSTANCE.getACFloatType_Exponent();

		/**
		 * The meta object literal for the '<em><b>Mantissa</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AC_FLOAT_TYPE__MANTISSA = eINSTANCE.getACFloatType_Mantissa();

		/**
		 * The meta object literal for the '<em><b>Radix</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AC_FLOAT_TYPE__RADIX = eINSTANCE.getACFloatType_Radix();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.ACComplexTypeImpl <em>AC Complex Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.ACComplexTypeImpl
		 * @see gecos.types.impl.TypesPackageImpl#getACComplexType()
		 * @generated
		 */
		EClass AC_COMPLEX_TYPE = eINSTANCE.getACComplexType();

		/**
		 * The meta object literal for the '<em><b>Base Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AC_COMPLEX_TYPE__BASE_TYPE = eINSTANCE.getACComplexType_BaseType();

		/**
		 * The meta object literal for the '{@link gecos.types.impl.ACChannelTypeImpl <em>AC Channel Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.ACChannelTypeImpl
		 * @see gecos.types.impl.TypesPackageImpl#getACChannelType()
		 * @generated
		 */
		EClass AC_CHANNEL_TYPE = eINSTANCE.getACChannelType();

		/**
		 * The meta object literal for the '<em><b>Base Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AC_CHANNEL_TYPE__BASE_TYPE = eINSTANCE.getACChannelType_BaseType();

		/**
		 * The meta object literal for the '{@link gecos.types.TypesVisitor <em>Visitor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.TypesVisitor
		 * @see gecos.types.impl.TypesPackageImpl#getTypesVisitor()
		 * @generated
		 */
		EClass TYPES_VISITOR = eINSTANCE.getTypesVisitor();

		/**
		 * The meta object literal for the '{@link gecos.types.StorageClassSpecifiers <em>Storage Class Specifiers</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.StorageClassSpecifiers
		 * @see gecos.types.impl.TypesPackageImpl#getStorageClassSpecifiers()
		 * @generated
		 */
		EEnum STORAGE_CLASS_SPECIFIERS = eINSTANCE.getStorageClassSpecifiers();

		/**
		 * The meta object literal for the '{@link gecos.types.Kinds <em>Kinds</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.Kinds
		 * @see gecos.types.impl.TypesPackageImpl#getKinds()
		 * @generated
		 */
		EEnum KINDS = eINSTANCE.getKinds();

		/**
		 * The meta object literal for the '{@link gecos.types.IntegerTypes <em>Integer Types</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.IntegerTypes
		 * @see gecos.types.impl.TypesPackageImpl#getIntegerTypes()
		 * @generated
		 */
		EEnum INTEGER_TYPES = eINSTANCE.getIntegerTypes();

		/**
		 * The meta object literal for the '{@link gecos.types.SignModifiers <em>Sign Modifiers</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.SignModifiers
		 * @see gecos.types.impl.TypesPackageImpl#getSignModifiers()
		 * @generated
		 */
		EEnum SIGN_MODIFIERS = eINSTANCE.getSignModifiers();

		/**
		 * The meta object literal for the '{@link gecos.types.FloatPrecisions <em>Float Precisions</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.FloatPrecisions
		 * @see gecos.types.impl.TypesPackageImpl#getFloatPrecisions()
		 * @generated
		 */
		EEnum FLOAT_PRECISIONS = eINSTANCE.getFloatPrecisions();

		/**
		 * The meta object literal for the '{@link gecos.types.OverflowMode <em>Overflow Mode</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.OverflowMode
		 * @see gecos.types.impl.TypesPackageImpl#getOverflowMode()
		 * @generated
		 */
		EEnum OVERFLOW_MODE = eINSTANCE.getOverflowMode();

		/**
		 * The meta object literal for the '{@link gecos.types.QuantificationMode <em>Quantification Mode</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.QuantificationMode
		 * @see gecos.types.impl.TypesPackageImpl#getQuantificationMode()
		 * @generated
		 */
		EEnum QUANTIFICATION_MODE = eINSTANCE.getQuantificationMode();

		/**
		 * The meta object literal for the '<em>String</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see gecos.types.impl.TypesPackageImpl#getString()
		 * @generated
		 */
		EDataType STRING = eINSTANCE.getString();

		/**
		 * The meta object literal for the '<em>long</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.TypesPackageImpl#getlong()
		 * @generated
		 */
		EDataType LONG = eINSTANCE.getlong();

		/**
		 * The meta object literal for the '<em>int</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.TypesPackageImpl#getint()
		 * @generated
		 */
		EDataType INT = eINSTANCE.getint();

		/**
		 * The meta object literal for the '<em>boolean</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.types.impl.TypesPackageImpl#getboolean()
		 * @generated
		 */
		EDataType BOOLEAN = eINSTANCE.getboolean();

	}

} //TypesPackage
