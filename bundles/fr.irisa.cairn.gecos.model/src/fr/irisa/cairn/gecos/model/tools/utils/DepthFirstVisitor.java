/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.tools.utils;
import java.util.LinkedHashSet;
import java.util.Set;

public class DepthFirstVisitor {

	private Set<IGraphNode> visited;

	public DepthFirstVisitor() {
		reset();
	}
		
	public void reset() {
		visited = new LinkedHashSet<IGraphNode>();
	}

	public void visit(IGraphNode node) {
		visited.add(node);
		processBefore(node);
		for (int i = 0; i < node.size(); ++i) {
			if (! visited.contains(node.successorAt(i))) {
				processSuccessorBefore(node);
				visit(node.successorAt(i));
				processSuccessorAfter(node);
			}
		}
		processAfter(node);
	}

	public void processBefore(IGraphNode node) { }
	public void processAfter(IGraphNode node) { }
	public void processSuccessorBefore(IGraphNode node) { }
	public void processSuccessorAfter(IGraphNode node) { }
}
