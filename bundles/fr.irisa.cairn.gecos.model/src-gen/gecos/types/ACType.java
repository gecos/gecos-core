/**
 */
package gecos.types;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AC Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.ACType#isSigned <em>Signed</em>}</li>
 * </ul>
 *
 * @see gecos.types.TypesPackage#getACType()
 * @model
 * @generated
 */
public interface ACType extends BaseType, TypesVisitable {
	/**
	 * Returns the value of the '<em><b>Signed</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signed</em>' attribute.
	 * @see #setSigned(boolean)
	 * @see gecos.types.TypesPackage#getACType_Signed()
	 * @model default="false" unique="false" dataType="gecos.types.boolean"
	 * @generated
	 */
	boolean isSigned();

	/**
	 * Sets the value of the '{@link gecos.types.ACType#isSigned <em>Signed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signed</em>' attribute.
	 * @see #isSigned()
	 * @generated
	 */
	void setSigned(boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitACType(this);'"
	 * @generated
	 */
	void accept(TypesVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.types.boolean" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.isSigned();'"
	 * @generated
	 */
	boolean getSigned();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.boolean" unique="false" otherUnique="false" ignoreStorageClassDataType="gecos.types.boolean" ignoreStorageClassUnique="false" ignoreConstDataType="gecos.types.boolean" ignoreConstUnique="false" ignoreVolatileDataType="gecos.types.boolean" ignoreVolatileUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((this == other))\n{\n\treturn true;\n}\nif ((other instanceof &lt;%gecos.types.ACType%&gt;))\n{\n\treturn ((&lt;%java.lang.Boolean%&gt;.valueOf(this.getSigned()) == &lt;%java.lang.Boolean%&gt;.valueOf(((&lt;%gecos.types.ACType%&gt;)other).getSigned())) &amp;&amp; super.isEqual(other, ignoreStorageClass, ignoreConst, ignoreVolatile));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isEqual(Type other, boolean ignoreStorageClass, boolean ignoreConst, boolean ignoreVolatile);

} // ACType
