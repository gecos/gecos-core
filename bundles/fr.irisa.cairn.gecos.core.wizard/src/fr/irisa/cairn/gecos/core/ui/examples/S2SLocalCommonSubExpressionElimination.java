package fr.irisa.cairn.gecos.core.ui.examples;

import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

import fr.irisa.cairn.gecos.core.ui.Activator;
import fr.irisa.cairn.gecos.core.ui.examples.utils.BasicGecosExampleWizard;

public class S2SLocalCommonSubExpressionElimination extends BasicGecosExampleWizard {
	@Override
	public String getResourcesFolderToCopyIntoProject() {
		return "resources/wizard_templates/s2s_local_com_subexpr_elim/";
	}

	@Override
	public String getPageName() {
		return "Local Common Sub-Expressions Elimination Example";
	}

	@Override
	public String getPageDescription() {
		return "This wizard will create an example project for local common sub-expressions Elimination.";
	}

	@Override
	protected Bundle getPluginBundle() {
		return Platform.getBundle(Activator.PLUGIN_ID);
	}

}
