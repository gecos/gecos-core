/**
 */
package gecos.types.impl;

import gecos.types.BoolType;
import gecos.types.Type;
import gecos.types.TypesPackage;
import gecos.types.TypesVisitor;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bool Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BoolTypeImpl extends BaseTypeImpl implements BoolType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BoolTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypesPackage.Literals.BOOL_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final TypesVisitor visitor) {
		visitor.visitBoolType(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEqual(final Type other, final boolean ignoreStorageClass, final boolean ignoreConst, final boolean ignoreVolatile) {
		boolean _xblockexpression = false;
		{
			if ((other == this)) {
				return true;
			}
			_xblockexpression = ((other instanceof BoolType) && super.isEqual(other, ignoreStorageClass, ignoreConst, ignoreVolatile));
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _string = super.toString();
		return (_string + "bool");
	}

} //BoolTypeImpl
