/**
 */
package gecos.instrs;

import gecos.core.ISymbolUse;
import gecos.core.Symbol;

import gecos.types.ArrayType;
import gecos.types.Type;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple Array Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.instrs.InstrsPackage#getSimpleArrayInstruction()
 * @model
 * @generated
 */
public interface SimpleArrayInstruction extends ArrayInstruction, ISymbolUse {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.core.Symbol%&gt; _xblockexpression = null;\n{\n\tfinal &lt;%gecos.instrs.Instruction%&gt; si = this.getDest();\n\t&lt;%gecos.core.Symbol%&gt; _xifexpression = null;\n\tif ((si instanceof &lt;%gecos.instrs.SymbolInstruction%&gt;))\n\t{\n\t\t_xifexpression = ((&lt;%gecos.instrs.SymbolInstruction%&gt;)si).getSymbol();\n\t}\n\telse\n\t{\n\t\t_xifexpression = null;\n\t}\n\t_xblockexpression = _xifexpression;\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	Symbol getSymbol();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model newSymbolUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.instrs.Instruction%&gt; si = this.getDest();\nif ((si instanceof &lt;%gecos.instrs.SymbolInstruction%&gt;))\n{\n\t((&lt;%gecos.instrs.SymbolInstruction%&gt;)si).setSymbol(newSymbol);\n}\nelse\n{\n\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(\"Not yet Implemented\");\n}'"
	 * @generated
	 */
	void setSymbol(Symbol newSymbol);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model newDestUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((newDest instanceof &lt;%gecos.instrs.SymbolInstruction%&gt;))\n{\n\tsuper.setDest(newDest);\n}\nelse\n{\n\t&lt;%java.lang.String%&gt; _simpleName = this.getClass().getSimpleName();\n\t&lt;%java.lang.String%&gt; _plus = (\"Illegal use of \" + _simpleName);\n\t&lt;%java.lang.String%&gt; _plus_1 = (_plus + \".setDest() : target must be a symbol not a compex instrcytion such as \");\n\t&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + newDest);\n\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus_2);\n}'"
	 * @generated
	 */
	void setDest(Instruction newDest);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.core.Symbol%&gt; _symbol = this.getSymbol();\nboolean _tripleEquals = (_symbol == null);\nif (_tripleEquals)\n{\n\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(((\"SimpleArrayInstruction \" + this) + \" doesn\\\'t have any symbol!\"));\n}\nint nb_index = 0;\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; _index = this.getIndex();\nboolean _tripleNotEquals = (_index != null);\nif (_tripleNotEquals)\n{\n\tnb_index = this.getIndex().size();\n}\nif ((nb_index == 0))\n{\n\treturn this.getSymbol().getType();\n}\n&lt;%gecos.types.Type%&gt; _type = this.getSymbol().getType();\nif ((_type instanceof &lt;%gecos.types.ArrayType%&gt;))\n{\n\t&lt;%gecos.types.Type%&gt; _type_1 = this.getSymbol().getType();\n\tfinal &lt;%gecos.types.ArrayType%&gt; symType = ((&lt;%gecos.types.ArrayType%&gt;) _type_1);\n\tfinal int nb_dim = symType.getNbDims();\n\tif ((nb_index &gt; nb_dim))\n\t{\n\t\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(((((\"Accessing \" + &lt;%java.lang.Integer%&gt;.valueOf(nb_dim)) + \" dimensions Array using \") + &lt;%java.lang.Integer%&gt;.valueOf(nb_index)) + \" indexes!\"));\n\t}\n\t&lt;%gecos.types.Type%&gt; base = symType.getBase();\n\t&lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt; _doubleDotLessThan = new &lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt;(1, nb_index, true);\n\tfor (final &lt;%java.lang.Integer%&gt; i : _doubleDotLessThan)\n\t{\n\t\tbase = ((&lt;%gecos.types.ArrayType%&gt;) base).getBase();\n\t}\n\treturn base;\n}\nelse\n{\n\t&lt;%gecos.types.Type%&gt; _type_2 = this.getSymbol().getType();\n\tif ((_type_2 instanceof &lt;%gecos.types.PtrType%&gt;))\n\t{\n\t\t&lt;%gecos.types.Type%&gt; _type_3 = this.getSymbol().getType();\n\t\tfinal &lt;%gecos.types.PtrType%&gt; symType_1 = ((&lt;%gecos.types.PtrType%&gt;) _type_3);\n\t\tfinal int nb_dim_1 = symType_1.getNbDims();\n\t\tif ((nb_index &gt; nb_dim_1))\n\t\t{\n\t\t\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(((((\"Accessing \" + &lt;%java.lang.Integer%&gt;.valueOf(nb_dim_1)) + \" dimensions Array using \") + &lt;%java.lang.Integer%&gt;.valueOf(nb_index)) + \" indexes!\"));\n\t\t}\n\t\t&lt;%gecos.types.Type%&gt; base_1 = symType_1.getBase();\n\t\t&lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt; _doubleDotLessThan_1 = new &lt;%org.eclipse.xtext.xbase.lib.ExclusiveRange%&gt;(1, nb_index, true);\n\t\tfor (final &lt;%java.lang.Integer%&gt; i_1 : _doubleDotLessThan_1)\n\t\t{\n\t\t\tbase_1 = ((&lt;%gecos.types.PtrType%&gt;) base_1).getBase();\n\t\t}\n\t\treturn base_1;\n\t}\n}\n&lt;%gecos.types.Type%&gt; _type_4 = this.getSymbol().getType();\n&lt;%java.lang.String%&gt; _plus = (((\"SimpleArrayInstruction \" + this) + \" with symbol type \") + _type_4);\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \" unhandeled !\");\nthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus_1);'"
	 * @generated
	 */
	Type computeType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitSimpleArrayInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getSymbol();'"
	 * @generated
	 */
	Symbol getUsedSymbol();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.core.Symbol%&gt; _symbol = this.getSymbol();\nboolean _tripleNotEquals = (_symbol != null);\nif (_tripleNotEquals)\n{\n\t&lt;%java.lang.String%&gt; _name = this.getSymbol().getName();\n\t&lt;%java.lang.String%&gt; _plus = (\"\" + _name);\n\tfinal &lt;%java.lang.StringBuffer%&gt; sb = new &lt;%java.lang.StringBuffer%&gt;(_plus);\n\ttry\n\t{\n\t\t&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; _index = this.getIndex();\n\t\tboolean _tripleNotEquals_1 = (_index != null);\n\t\tif (_tripleNotEquals_1)\n\t\t{\n\t\t\t&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.instrs.Instruction%&gt;&gt; _index_1 = this.getIndex();\n\t\t\tfor (final &lt;%gecos.instrs.Instruction%&gt; i : _index_1)\n\t\t\t{\n\t\t\t\tif ((i != null))\n\t\t\t\t{\n\t\t\t\t\tsb.append(\"[\").append(i).append(\"]\");\n\t\t\t\t}\n\t\t\t\telse\n\t\t\t\t{\n\t\t\t\t\tsb.append(\"[null]\");\n\t\t\t\t}\n\t\t\t}\n\t\t}\n\t}\n\tcatch (final Throwable _t) {\n\t\tif (_t instanceof &lt;%java.lang.Exception%&gt;) {\n\t\t\tsb.append(\"[#error#]\");\n\t\t}\n\t\telse\n\t\t{\n\t\t\tthrow &lt;%org.eclipse.xtext.xbase.lib.Exceptions%&gt;.sneakyThrow(_t);\n\t\t}\n\t}\n\treturn sb.toString();\n}\nelse\n{\n\treturn super.toString();\n}'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.Type%&gt; _type = this.getSymbol().getType();\nif ((_type instanceof &lt;%gecos.types.ArrayType%&gt;))\n{\n\t&lt;%gecos.types.Type%&gt; _type_1 = this.getSymbol().getType();\n\treturn ((&lt;%gecos.types.ArrayType%&gt;) _type_1);\n}\nreturn null;'"
	 * @generated
	 */
	ArrayType getTargetArrayType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.SimpleArrayInstruction%&gt;))\n{\n\treturn (super.isSame(instr) &amp;&amp; (this.getSymbol() == ((&lt;%gecos.instrs.SimpleArrayInstruction%&gt;)instr).getSymbol()));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // SimpleArrayInstruction
