/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.analysis.dataflow.dominator;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.gecos.model.tools.utils.IGraphNode;
import gecos.blocks.BasicBlock;

public class DominatorTreeNode implements IGraphNode {

	private DominatorTreeNode parent;
	private BasicBlock block;
	private List<DominatorTreeNode> children;

	public DominatorTreeNode(BasicBlock block) {
		this.children = new ArrayList<DominatorTreeNode>();
		this.block = block;
		this.parent = null;
	}

	public BasicBlock block() {
		return block;
	}

	public DominatorTreeNode parent() {
		return parent;
	}

	public void addChild(DominatorTreeNode child) {
		children.add(child);
		child.parent = this;
	}

	public List<DominatorTreeNode> children() {
		return children;
	}

	// IGraphNode interface
	public int size() {
		return children.size();
	}

	public IGraphNode successorAt(int i) {
		return (IGraphNode) children.get(i);
	}

	@Override
	public String toString() {
		return "DomTreeNode[" + block + "]";
	}

}
