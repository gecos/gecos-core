#ifndef _STDIO_H
#define _STDIO_H

#include <stdlib.h>

#define __inline__ inline
#define __extension__
#define __const const
#define __restrict
#define __attribute__

//int* NULL;
//#define EOF (-1)
int EOF=-1;

#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2

#include <stddef.h>

#include <stdarg.h>


typedef struct TFILE {
	int dummy;
} FILE;
typedef int fpos_t;

extern FILE *stdin;
extern FILE *stdout;
extern FILE *stderr;

extern void clearerr  (FILE *f);
extern int fclose  (FILE *f);
extern int feof  (FILE *f);
extern int ferror  (FILE *f);
extern int fflush  (FILE *f);
extern int fgetc  (FILE * f);
extern int fgetpos  (FILE* fp, fpos_t *pos);
extern char* fgets  (char*, int, FILE*);
extern FILE* fopen  (char*,  char*);
extern int fprintf  (FILE*,  char* format, ...);
extern int fputc  (int, FILE*);
extern int fputs  ( char *str, FILE *fp);
extern size_t fread  (void*, size_t, size_t, FILE*);
extern FILE* freopen  ( char*,  char*, FILE*);
extern int fscanf  (FILE *fp,  char* format, ...);
extern int fseek  (FILE* fp, long int offset, int whence);
extern int fsetpos  (FILE* fp,  fpos_t *pos);
extern long int ftell  (FILE* fp);
extern size_t fwrite  ( void*, size_t, size_t, FILE*);
extern int getc  (FILE *);
extern int getchar  (void);
extern char* gets  (char*);
extern void perror  ( char *);
extern int printf  ( char* format, ...);
extern int putc  (int, FILE *);
extern int putchar  (int);
extern int puts  ( char *str);
extern int remove  ( char*);
extern int rename  ( char* _old,  char* _new);
extern void rewind  (FILE*);
extern int scanf  ( char* format, ...);
extern void setbuf  (FILE*, char*);
extern void setlinebuf  (FILE*);
extern void setbuffer  (FILE*, char*, int);
extern int setvbuf  (FILE*, char*, int mode, size_t size);
extern int sprintf  (char*,  char* format, ...);
extern int sscanf  ( char* string,  char* format, ...);
extern FILE* tmpfile  (void);
extern char* tmpnam  (char*);

extern int ungetc  (int c, FILE* fp);
extern int vfprintf  (FILE *fp, char  *fmt0, ...);
extern int vprintf  (char  *fmt, ...);

extern int vsprintf  (char* string,  char* format, ...);
extern int fcloseall  (void);

extern int asprintf  (char **, const char *, ...);
extern int vasprintf  (char **, const char *, ...);

extern char* getenv(char* str);

FILE *popen(const char *command, const char *mode);
int pclose(FILE *stream);
//extern void exit(...);


#endif 
