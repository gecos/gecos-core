package fr.irisa.cairn.gecos.model.dag.adapter.observers;

import java.util.Map.Entry;

import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGCollapsedNodeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGEdgeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGInstructionAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGNodeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGPortAdapter;
import fr.irisa.cairn.graph.implement.CollapsedNode;
import fr.irisa.cairn.graph.observer.AbstractGraphStructureObserver;
import fr.irisa.cairn.graph.observer.IModificationNotification;
import gecos.dag.DAGInPort;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGNode;
import gecos.dag.DAGOutPort;
import gecos.dag.DAGPatternNode;

/**
 * An observer that keep a synchronization between the
 * {@link IDAGInstructionAdapter} and the {@link DAGInstruction}. It catch
 * structural {@link IModificationNotification} thrown by the graph to update
 * the adapted {@link DAGInstruction}.
 * 
 * @author Antoine Floc'h - Initial contribution and API
 * 
 */
public class DagAdapterSynchronizationObserver extends
		AbstractGraphStructureObserver implements IDagSynchronizationObserver {

	@Override
	protected void notifyAddNode(IModificationNotification notif) {
		IDAGInstructionAdapter graph = (IDAGInstructionAdapter) notif
				.getNotifier();
		IDAGNodeAdapter node = (IDAGNodeAdapter) notif.getFeature();
		graph.getAdaptedGraph().getNodes().add(node.getAdaptedNode());

		// Management of DAG collapsed node
		if (node instanceof IDAGCollapsedNodeAdapter
				&& !node.getObservers().contains(this)) {
			CollapsedNode cnode = (CollapsedNode) node;
			IDAGInstructionAdapter nestedAdapter = (IDAGInstructionAdapter) cnode
					.getSubgraph();
			node.getObservers().add(this);
			cnode.getSubgraph().addObserver(this);
			DAGPatternNode patternNode = (DAGPatternNode) node.getAdaptedNode();
			patternNode.setPattern(nestedAdapter.getAdaptedGraph());
		}
	}

	@Override
	protected void notifyRemoveNode(IModificationNotification notif) {
		IDAGInstructionAdapter graph = (IDAGInstructionAdapter) notif
				.getNotifier();
		IDAGNodeAdapter node = (IDAGNodeAdapter) notif.getFeature();
		graph.getAdaptedGraph().getNodes().remove(node.getAdaptedNode());
	}

	@Override
	protected void notifyDisconnectEdge(IModificationNotification notif) {
		IDAGPortAdapter padapter = (IDAGPortAdapter) notif.getNotifier();
		IDAGEdgeAdapter edge = (IDAGEdgeAdapter) notif.getFeature();
		if (padapter.getAdaptedPort() instanceof DAGInPort) {
			DAGInPort pin = (DAGInPort) padapter.getAdaptedPort();
			DAGNode node = pin.getParent();
			if (node != null)
				node.getPredecessors().remove(
						pin.getSource().getSrc().getParent());

		} else {
			DAGOutPort pout = (DAGOutPort) padapter.getAdaptedPort();
			DAGNode node = pout.getParent();
			if (node != null) {
				if (edge.getSinkPort() != null)
					node.getSuccessors().remove(
							edge.getSinkPort().getNode().getAdaptedNode());
			}

		}

	}

	@Override
	protected void notifyAddEdge(IModificationNotification notif) {
		try{
			IDAGInstructionAdapter graph = (IDAGInstructionAdapter) notif
					.getNotifier();
			IDAGEdgeAdapter edge = (IDAGEdgeAdapter) notif.getFeature();
			graph.getAdaptedGraph().getEdges().add(edge.getAdaptedEdge());
		} catch(Exception e) {
//			System.err.println("WARRNING: DAGAdapter notifyAddOutputPort");
		}

	}

	// Edges
	protected void notifyReconnectEdgeSource(IModificationNotification notif) {
		IDAGPortAdapter port = (IDAGPortAdapter) notif.getFeature();
		if (port != null) {
			IDAGEdgeAdapter edge = (IDAGEdgeAdapter) notif.getNotifier();
			edge.getAdaptedEdge().setSrc((DAGOutPort) port.getAdaptedPort());
		}
	}

	protected void notifyReconnectEdgeSink(IModificationNotification notif) {
		IDAGPortAdapter port = (IDAGPortAdapter) notif.getFeature();
		if (port != null) {
			IDAGEdgeAdapter edge = (IDAGEdgeAdapter) notif.getNotifier();
			edge.getAdaptedEdge().setSink((DAGInPort) port.getAdaptedPort());
			DAGInPort inport = (DAGInPort) port.getAdaptedPort();
			inport.setSource(edge.getAdaptedEdge());
		}
	}

	// @Override
	// protected void notifyConnectEdge(IModificationNotification notif) {
	// throw new DagAdapterCoherencyException(
	// "Not yet implemented: DAGInstruction is no more synchronized with the adapter:"
	// + notif);
	// }
	//
	// @Override
	// protected void notifyDisconnectEdge(IModificationNotification notif) {
	// throw new DagAdapterCoherencyException(
	// "Not yet implemented: DAGInstruction is no more synchronized with the adapter:"
	// + notif);
	// }

	@Override
	protected void notifyRemoveEdge(IModificationNotification notif) {
		IDAGInstructionAdapter graph = (IDAGInstructionAdapter) notif
				.getNotifier();
		IDAGEdgeAdapter edge = (IDAGEdgeAdapter) notif.getFeature();
		graph.getAdaptedGraph().getEdges().remove(edge.getAdaptedEdge());

		// DAGInPort sinkPort = edge.getAdaptedEdge().getSink();
		// DAGOutPort sourcePort = edge.getAdaptedEdge().getSrc();
		//
		// DAGNode dest = sinkPort.getParent();
		// dest.getInputs().remove(sinkPort);
		//
		// DAGNode source = sourcePort.getParent();
		// sourcePort.getSinks().remove(edge.getAdaptedEdge());
		//
		// if(sourcePort!=null && sourcePort.getParent()!=null)
		// source.getSuccessors().remove(dest);
		// dest.getPredecessors().remove(source);

		//
		// boolean connected = false;
		// for(DAGOutPort p: source.getOutputs()){
		// for(DAGEdge e:p.getSinks()){
		// if(e.getSink().getParent()==dest)
		// connected = true;
		// }
		// }
		// if(!connected){

		// }
	}

	// Node structural updates
	@Override
	protected void notifyAddInputPort(IModificationNotification notif) {
		try {
			IDAGPortAdapter port = (IDAGPortAdapter) notif.getFeature();
			if (port != null) {
				IDAGNodeAdapter node = (IDAGNodeAdapter) notif.getNotifier();
				node.getAdaptedNode().getInputs()
						.add((DAGInPort) port.getAdaptedPort());
			}
		} catch(Exception e) {
//			System.err.println("WARRNING: DAGAdapter notifyAddInputPort");
		}
	}

	@Override
	protected void notifyAddOutputPort(IModificationNotification notif) {
		try {
			IDAGPortAdapter port = (IDAGPortAdapter) notif.getFeature();
			if (port != null) {
				IDAGNodeAdapter node = (IDAGNodeAdapter) notif.getNotifier();
				node.getAdaptedNode().getOutputs()
						.add((DAGOutPort) port.getAdaptedPort());
			}
		} catch(Exception e) {
//			System.err.println("WARRNING: DAGAdapter notifyAddOutputPort");
		}
	}

	@Override
	protected void notifyRemoveInputPort(IModificationNotification notif) {
		IDAGPortAdapter port = (IDAGPortAdapter) notif.getFeature();
		if (port != null) {
			IDAGNodeAdapter node = (IDAGNodeAdapter) notif.getNotifier();
			node.getAdaptedNode().getInputs()
					.remove((DAGInPort) port.getAdaptedPort());
		}
	}

	@Override
	protected void notifyRemoveOutputPort(IModificationNotification notif) {
		IDAGPortAdapter port = (IDAGPortAdapter) notif.getFeature();
		if (port != null) {
			IDAGNodeAdapter node = (IDAGNodeAdapter) notif.getNotifier();
			node.getAdaptedNode().getOutputs()
					.remove((DAGOutPort) port.getAdaptedPort());
		}
	}

	@Override
	protected void notifySetSubgraphContent(IModificationNotification notif) {
		IDAGCollapsedNodeAdapter node = (IDAGCollapsedNodeAdapter) notif
				.getNotifier();
		IDAGInstructionAdapter pattern = (IDAGInstructionAdapter) notif
				.getFeature();
		DAGPatternNode pnode = (DAGPatternNode) node.getAdaptedNode();
		pnode.setPattern(pattern.getAdaptedGraph());
	}

	@Override
	protected void notifyMapInput(IModificationNotification notif) {
		@SuppressWarnings("unchecked")
		Entry<IDAGPortAdapter, IDAGPortAdapter> entry = (Entry<IDAGPortAdapter, IDAGPortAdapter>) notif
				.getFeature();
		IDAGCollapsedNodeAdapter cnode = (IDAGCollapsedNodeAdapter) notif
				.getNotifier();
		DAGPatternNode pnode = (DAGPatternNode) cnode.getAdaptedNode();
		pnode.getInputsMap().put((DAGInPort) entry.getKey().getAdaptedPort(),
				(DAGInPort) entry.getValue().getAdaptedPort());
		super.notifyMapInput(notif);
	}
	
	@Override
	protected void notifyMapOutput(IModificationNotification notif) {
		@SuppressWarnings("unchecked")
		Entry<IDAGPortAdapter, IDAGPortAdapter> entry = (Entry<IDAGPortAdapter, IDAGPortAdapter>) notif
				.getFeature();
		IDAGCollapsedNodeAdapter cnode = (IDAGCollapsedNodeAdapter) notif
				.getNotifier();
		DAGPatternNode pnode = (DAGPatternNode) cnode.getAdaptedNode();
		pnode.getOutputsMap().put((DAGOutPort) entry.getKey().getAdaptedPort(),
				(DAGOutPort) entry.getValue().getAdaptedPort());
		super.notifyMapInput(notif);
	}

}
