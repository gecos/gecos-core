/*******************************************************************************
* Copyright (c) 2012 Universite de Rennes 1 / Inria.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the FreeBSD License v1.0
* which accompanies this distribution, and is available at
* http://www.freebsd.org/copyright/freebsd-license.html
*
* Contributors:
*    DERRIEN Steven - initial API and implementation
*    MORVAN Antoine - initial API and implementation
*    NAULLET Maxime - initial API and implementation
*******************************************************************************/
package fr.irisa.cairn.gecos.model.tools.visitors.simpleforloops;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.ForBlock;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.CondInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.instrs.SymbolInstruction;
import gecos.types.BaseType;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;
import tom.mapping.GenericIntrospector;

@SuppressWarnings("all")
public class FindConditionnalBound {


private static boolean tom_equal_term_Strategy(Object t1, Object t2) {
return  (t1.equals(t2)) ;
}
private static boolean tom_is_sort_Strategy(Object t) {
return  (t instanceof tom.library.sl.Strategy) ;
}
private static boolean tom_equal_term_Position(Object t1, Object t2) {
return  (t1.equals(t2)) ;
}
private static boolean tom_is_sort_Position(Object t) {
return  (t instanceof tom.library.sl.Position) ;
}
private static boolean tom_equal_term_int(int t1, int t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_int(int t) {
return  true ;
}
private static boolean tom_equal_term_char(char t1, char t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_char(char t) {
return  true ;
}
private static boolean tom_equal_term_String(String t1, String t2) {
return  t1.equals(t2) ;
}
private static boolean tom_is_sort_String(String t) {
return  t instanceof String ;
}
private static  tom.library.sl.Strategy  tom_make_mu( tom.library.sl.Strategy  var,  tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.Mu(var,v) );
}
private static  tom.library.sl.Strategy  tom_make_MuVar( String  name) { 
return ( new tom.library.sl.MuVar(name) );
}
private static  tom.library.sl.Strategy  tom_make_Identity() { 
return ( new tom.library.sl.Identity() );
}
private static  tom.library.sl.Strategy  tom_make_One( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.One(v) );
}
private static  tom.library.sl.Strategy  tom_make_All( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.All(v) );
}
private static  tom.library.sl.Strategy  tom_make_Fail() { 
return ( new tom.library.sl.Fail() );
}
private static boolean tom_is_fun_sym_Sequence( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.Sequence );
}
private static  tom.library.sl.Strategy  tom_empty_list_Sequence() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_Sequence( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.Sequence.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Sequence.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Sequence.THEN) );
}
private static boolean tom_is_empty_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( t == null );
}

  private static   tom.library.sl.Strategy  tom_append_list_Sequence( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 == null )) {
      return l2;
    } else if(( l2 == null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.Sequence )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.THEN) ) == null )) {
        return  tom.library.sl.Sequence.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.Sequence.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.FIRST) ),tom_append_list_Sequence(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.Sequence.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_Sequence( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end == null ) ||  (end.equals(tom_empty_list_Sequence())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.Sequence.make(((( begin instanceof tom.library.sl.Sequence ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Sequence.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_Sequence(((( begin instanceof tom.library.sl.Sequence ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Sequence.THEN) ):tom_empty_list_Sequence()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_Choice( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.Choice );
}
private static  tom.library.sl.Strategy  tom_empty_list_Choice() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_Choice( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.Choice.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Choice.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Choice.THEN) );
}
private static boolean tom_is_empty_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( t ==null );
}

  private static   tom.library.sl.Strategy  tom_append_list_Choice( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 ==null )) {
      return l2;
    } else if(( l2 ==null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.Choice )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.THEN) ) ==null )) {
        return  tom.library.sl.Choice.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.Choice.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.FIRST) ),tom_append_list_Choice(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.Choice.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_Choice( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end ==null ) ||  (end.equals(tom_empty_list_Choice())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.Choice.make(((( begin instanceof tom.library.sl.Choice ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Choice.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_Choice(((( begin instanceof tom.library.sl.Choice ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Choice.THEN) ):tom_empty_list_Choice()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_SequenceId( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.SequenceId );
}
private static  tom.library.sl.Strategy  tom_empty_list_SequenceId() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_SequenceId( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.SequenceId.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.SequenceId.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.SequenceId.THEN) );
}
private static boolean tom_is_empty_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( t == null );
}

  private static   tom.library.sl.Strategy  tom_append_list_SequenceId( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 == null )) {
      return l2;
    } else if(( l2 == null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.SequenceId )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.THEN) ) == null )) {
        return  tom.library.sl.SequenceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.SequenceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.FIRST) ),tom_append_list_SequenceId(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.SequenceId.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_SequenceId( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end == null ) ||  (end.equals(tom_empty_list_SequenceId())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.SequenceId.make(((( begin instanceof tom.library.sl.SequenceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.SequenceId.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_SequenceId(((( begin instanceof tom.library.sl.SequenceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.SequenceId.THEN) ):tom_empty_list_SequenceId()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_ChoiceId( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.ChoiceId );
}
private static  tom.library.sl.Strategy  tom_empty_list_ChoiceId() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_ChoiceId( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.ChoiceId.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.ChoiceId.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.ChoiceId.THEN) );
}
private static boolean tom_is_empty_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( t ==null );
}

  private static   tom.library.sl.Strategy  tom_append_list_ChoiceId( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 ==null )) {
      return l2;
    } else if(( l2 ==null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.ChoiceId )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.THEN) ) ==null )) {
        return  tom.library.sl.ChoiceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.ChoiceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.FIRST) ),tom_append_list_ChoiceId(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.ChoiceId.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_ChoiceId( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end ==null ) ||  (end.equals(tom_empty_list_ChoiceId())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.ChoiceId.make(((( begin instanceof tom.library.sl.ChoiceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.ChoiceId.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_ChoiceId(((( begin instanceof tom.library.sl.ChoiceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.ChoiceId.THEN) ):tom_empty_list_ChoiceId()),end,tail)) ;
  }
  private static  tom.library.sl.Strategy  tom_make_OneId( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.OneId(v) );
}
private static  tom.library.sl.Strategy  tom_make_AllSeq( tom.library.sl.Strategy  s) { 
return ( new tom.library.sl.AllSeq(s) );
}
private static  tom.library.sl.Strategy  tom_make_AUCtl( tom.library.sl.Strategy  s1,  tom.library.sl.Strategy  s2) { 
return ( 
tom_make_mu(tom_make_MuVar("x"),tom_cons_list_Choice(s2,tom_cons_list_Choice(tom_cons_list_Sequence(tom_cons_list_Sequence(s1,tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("x")),tom_empty_list_Sequence())),tom_cons_list_Sequence(tom_make_One(tom_make_Identity()),tom_empty_list_Sequence())),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_EUCtl( tom.library.sl.Strategy  s1,  tom.library.sl.Strategy  s2) { 
return ( 
tom_make_mu(tom_make_MuVar("x"),tom_cons_list_Choice(s2,tom_cons_list_Choice(tom_cons_list_Sequence(s1,tom_cons_list_Sequence(tom_make_One(tom_make_MuVar("x")),tom_empty_list_Sequence())),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_Try( tom.library.sl.Strategy  s) { 
return ( 
tom_cons_list_Choice(s,tom_cons_list_Choice(tom_make_Identity(),tom_empty_list_Choice())))
;
}
private static  tom.library.sl.Strategy  tom_make_Repeat( tom.library.sl.Strategy  s) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Choice(tom_cons_list_Sequence(s,tom_cons_list_Sequence(tom_make_MuVar("_x"),tom_empty_list_Sequence())),tom_cons_list_Choice(tom_make_Identity(),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_TopDown( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Sequence(v,tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("_x")),tom_empty_list_Sequence()))))
;
}
private static  tom.library.sl.Strategy  tom_make_OnceTopDown( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Choice(v,tom_cons_list_Choice(tom_make_One(tom_make_MuVar("_x")),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_RepeatId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_SequenceId(v,tom_cons_list_SequenceId(tom_make_MuVar("_x"),tom_empty_list_SequenceId()))))
;
}
private static  tom.library.sl.Strategy  tom_make_OnceTopDownId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_ChoiceId(v,tom_cons_list_ChoiceId(tom_make_OneId(tom_make_MuVar("_x")),tom_empty_list_ChoiceId()))))
;
}
private static  tom.library.sl.Strategy  tom_make_InnermostId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("_x")),tom_cons_list_Sequence(tom_cons_list_SequenceId(v,tom_cons_list_SequenceId(tom_make_MuVar("_x"),tom_empty_list_SequenceId())),tom_empty_list_Sequence()))))
;
}
private static  tom.library.sl.Strategy  tom_make_OutermostId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_RepeatId(tom_make_OnceTopDownId(v)))
;
}
private static boolean tom_equal_term_boolean(boolean t1, boolean t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_boolean(boolean t) {
return  true ;
}
private static boolean tom_equal_term_long(long t1, long t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_long(long t) {
return  true ;
}
private static boolean tom_equal_term_float(float t1, float t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_float(float t) {
return  true ;
}
private static boolean tom_equal_term_double(double t1, double t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_double(double t) {
return  true ;
}


private static <O> EList<O> enforce(EList l) {
return l;
}

private static <O> EList<O> append(O e,EList<O> l) {
l.add(e);
return l;
}
private static boolean tom_equal_term_EELong(Object l1, Object l2) {
return l1.equals(l2);
}
private static boolean tom_is_sort_EELong(Object t) {
return t instanceof java.lang.Long;
}
private static boolean tom_equal_term_BlockCopyManager(Object l1, Object l2) {
return l1.equals(l2);
}
private static boolean tom_is_sort_BlockCopyManager(Object t) {
return t instanceof fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;
}
private static boolean tom_equal_term_DependencyType(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_DependencyType(Object t) {
return t instanceof DependencyType;
}
private static boolean tom_equal_term_DAGOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_DAGOperator(Object t) {
return t instanceof DAGOperator;
}
private static boolean tom_equal_term_ArithmeticOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ArithmeticOperator(Object t) {
return t instanceof ArithmeticOperator;
}
private static boolean tom_equal_term_ComparisonOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ComparisonOperator(Object t) {
return t instanceof ComparisonOperator;
}
private static boolean tom_equal_term_LogicalOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_LogicalOperator(Object t) {
return t instanceof LogicalOperator;
}
private static boolean tom_equal_term_BitwiseOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_BitwiseOperator(Object t) {
return t instanceof BitwiseOperator;
}
private static boolean tom_equal_term_SelectOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_SelectOperator(Object t) {
return t instanceof SelectOperator;
}
private static boolean tom_equal_term_ReductionOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ReductionOperator(Object t) {
return t instanceof ReductionOperator;
}
private static boolean tom_equal_term_BranchType(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_BranchType(Object t) {
return t instanceof BranchType;
}
private static boolean tom_equal_term_StorageClassSpecifiers(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_StorageClassSpecifiers(Object t) {
return t instanceof StorageClassSpecifiers;
}
private static boolean tom_equal_term_Kinds(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_Kinds(Object t) {
return t instanceof Kinds;
}
private static boolean tom_equal_term_IntegerTypes(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_IntegerTypes(Object t) {
return t instanceof IntegerTypes;
}
private static boolean tom_equal_term_SignModifiers(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_SignModifiers(Object t) {
return t instanceof SignModifiers;
}
private static boolean tom_equal_term_FloatPrecisions(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_FloatPrecisions(Object t) {
return t instanceof FloatPrecisions;
}
private static boolean tom_equal_term_OverflowMode(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_OverflowMode(Object t) {
return t instanceof OverflowMode;
}
private static boolean tom_equal_term_QuantificationMode(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_QuantificationMode(Object t) {
return t instanceof QuantificationMode;
}
private static boolean tom_equal_term_Inst(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Inst(Object t) {
return t instanceof Instruction;
}
private static boolean tom_equal_term_Blk(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Blk(Object t) {
return t instanceof Block;
}
private static boolean tom_equal_term_Sym(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Sym(Object t) {
return t instanceof Symbol;
}
private static boolean tom_equal_term_SymL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_SymL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Symbol>)t).size() == 0 
    	|| (((EList<Symbol>)t).size()>0 && ((EList<Symbol>)t).get(0) instanceof Symbol));
}
private static boolean tom_equal_term_Type(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Type(Object t) {
return t instanceof Type;
}
private static boolean tom_equal_term_TypeL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_TypeL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Type>)t).size() == 0 
    	|| (((EList<Type>)t).size()>0 && ((EList<Type>)t).get(0) instanceof Type));
}
private static boolean tom_equal_term_Field(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Field(Object t) {
return t instanceof Field;
}
private static boolean tom_equal_term_FieldL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_FieldL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Field>)t).size() == 0 
    	|| (((EList<Field>)t).size()>0 && ((EList<Field>)t).get(0) instanceof Field));
}
private static boolean tom_equal_term_InstL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_InstL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Instruction>)t).size() == 0 
    	|| (((EList<Instruction>)t).size()>0 && ((EList<Instruction>)t).get(0) instanceof Instruction));
}
private static boolean tom_is_fun_sym_InstL( EList<Instruction>  t) {
return  t instanceof EList<?> &&
 		(((EList<Instruction>)t).size() == 0 
 		|| (((EList<Instruction>)t).size()>0 && ((EList<Instruction>)t).get(0) instanceof Instruction));
}
private static  EList<Instruction>  tom_empty_array_InstL(int n) { 
return  new BasicEList<Instruction>(n) ;
}
private static  EList<Instruction>  tom_cons_array_InstL(Instruction e,  EList<Instruction>  l) { 
return  append(e,l) ;
}
private static Instruction tom_get_element_InstL_InstL( EList<Instruction>  l, int n) {
return  l.get(n) ;
}
private static int tom_get_size_InstL_InstL( EList<Instruction>  l) {
return  l.size() ;
}

  private static   EList<Instruction>  tom_get_slice_InstL( EList<Instruction>  subject, int begin, int end) {
     EList<Instruction>  result =  new BasicEList<Instruction>(end-begin) ;
    while(begin!=end) {
      result =  append( subject.get(begin) ,result) ;
      begin++;
    }
    return result;
  }

  private static   EList<Instruction>  tom_append_array_InstL( EList<Instruction>  l2,  EList<Instruction>  l1) {
    int size1 =  l1.size() ;
    int size2 =  l2.size() ;
    int index;
     EList<Instruction>  result =  new BasicEList<Instruction>(size1+size2) ;
    index=size1;
    while(index >0) {
      result =  append( l1.get(size1-index) ,result) ;
      index--;
    }

    index=size2;
    while(index > 0) {
      result =  append( l2.get(size2-index) ,result) ;
      index--;
    }
    return result;
  }private static boolean tom_equal_term_BlkL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_BlkL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Block>)t).size() == 0 
    	|| (((EList<Block>)t).size()>0 && ((EList<Block>)t).get(0) instanceof Block));
}
private static boolean tom_equal_term_Node(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Node(Object t) {
return t instanceof DAGNode;
}
private static boolean tom_equal_term_NodeL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_NodeL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<DAGNode>)t).size() == 0 
    	|| (((EList<DAGNode>)t).size()>0 && ((EList<DAGNode>)t).get(0) instanceof DAGNode));
}
private static boolean tom_is_fun_sym_symref(Instruction t) {
return t instanceof SymbolInstruction;
}
private static Symbol tom_get_slot_symref_symbol(Instruction t) {
return ((SymbolInstruction)t).getSymbol();
}
private static Instruction tom_make_ival( long  _value) { 
return fr.irisa.cairn.gecos.model.tom.gecos.internal.GecosTomFactory.createIval(_value);
}
private static boolean tom_is_fun_sym_generic(Instruction t) {
return t instanceof GenericInstruction;
}
private static  String  tom_get_slot_generic_name(Instruction t) {
return ((GenericInstruction)t).getName();
}
private static  EList<Instruction>  tom_get_slot_generic_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_le(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("le");
}
private static Instruction tom_make_le( EList<Instruction>  _children) { 
return fr.irisa.cairn.gecos.model.tom.gecos.internal.GecosTomFactory.createLe(_children);
}
private static  EList<Instruction>  tom_get_slot_le_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_lt(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("lt");
}
private static  EList<Instruction>  tom_get_slot_lt_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_ge(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("ge");
}
private static Instruction tom_make_ge( EList<Instruction>  _children) { 
return fr.irisa.cairn.gecos.model.tom.gecos.internal.GecosTomFactory.createGe(_children);
}
private static  EList<Instruction>  tom_get_slot_ge_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_gt(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("gt");
}
private static  EList<Instruction>  tom_get_slot_gt_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_eq(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("eq");
}
private static  EList<Instruction>  tom_get_slot_eq_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_brcond(Instruction t) {
return t instanceof CondInstruction;
}
private static Instruction tom_get_slot_brcond_cond(Instruction t) {
return ((CondInstruction)t).getCond();
}
private static boolean tom_is_fun_sym_add(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("add");
}
private static Instruction tom_make_add( EList<Instruction>  _children) { 
return fr.irisa.cairn.gecos.model.tom.gecos.internal.GecosTomFactory.createAdd(_children);
}
private static  EList<Instruction>  tom_get_slot_add_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_sub(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("sub");
}
private static Instruction tom_make_sub( EList<Instruction>  _children) { 
return fr.irisa.cairn.gecos.model.tom.gecos.internal.GecosTomFactory.createSub(_children);
}
private static  EList<Instruction>  tom_get_slot_sub_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_mul(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("mul");
}
private static Instruction tom_make_mul( EList<Instruction>  _children) { 
return fr.irisa.cairn.gecos.model.tom.gecos.internal.GecosTomFactory.createMul(_children);
}
private static  EList<Instruction>  tom_get_slot_mul_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_div(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("div");
}
private static Instruction tom_make_div( EList<Instruction>  _children) { 
return fr.irisa.cairn.gecos.model.tom.gecos.internal.GecosTomFactory.createDiv(_children);
}
private static  EList<Instruction>  tom_get_slot_div_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}


private ForBlock forBlock;
private Instruction bound;

public FindConditionnalBound(ForBlock forBlock, Symbol loopVariable) {
this.forBlock = forBlock;
try {

tom_make_InnermostId(tom_make_CheckEqualities()).visitLight(forBlock.getTestBlock(), GenericIntrospector.INSTANCE);
} catch (VisitFailure e) {
}
findBound(loopVariable);
}

public Instruction getBound() {
return bound;
}


public static class CheckEqualities extends tom.library.sl.AbstractStrategyBasic {
public CheckEqualities() {
super(tom_make_Identity());
}
public tom.library.sl.Visitable[] getChildren() {
tom.library.sl.Visitable[] stratChildren = new tom.library.sl.Visitable[getChildCount()];
stratChildren[0] = super.getChildAt(0);
return stratChildren;}
public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
super.setChildAt(0, children[0]);
return this;
}
public int getChildCount() {
return 1;
}
public tom.library.sl.Visitable getChildAt(int index) {
switch (index) {
case 0: return super.getChildAt(0);
default: throw new IndexOutOfBoundsException();
}
}
public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
switch (index) {
case 0: return super.setChildAt(0, child);
default: throw new IndexOutOfBoundsException();
}
}
@SuppressWarnings("unchecked")
public <T> T visitLight(T v, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
if (tom_is_sort_Inst(v)) {
return ((T)visit_Inst(((Instruction)v),introspector));
}
if (!(( null  == environment))) {
return ((T)any.visit(environment,introspector));
} else {
return any.visitLight(v,introspector);
}
}
@SuppressWarnings("unchecked")
public Instruction _visit_Inst(Instruction arg, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
if (!(( null  == environment))) {
return ((Instruction)any.visit(environment,introspector));
} else {
return any.visitLight(arg,introspector);
}
}
@SuppressWarnings("unchecked")
public Instruction visit_Inst(Instruction tom__arg, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
{
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_gt(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_1=tom_get_slot_gt_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_1))) {
int tomMatch1_5=0;
if (!(tomMatch1_5 >= tom_get_size_InstL_InstL(tomMatch1_1))) {
Instruction tom_lhs=tom_get_element_InstL_InstL(tomMatch1_1,tomMatch1_5);
int tomMatch1_6=tomMatch1_5 + 1;
if (!(tomMatch1_6 >= tom_get_size_InstL_InstL(tomMatch1_1))) {
Instruction tom_rhs=tom_get_element_InstL_InstL(tomMatch1_1,tomMatch1_6);
if (tomMatch1_6 + 1 >= tom_get_size_InstL_InstL(tomMatch1_1)) {
Instruction tom_gt=((Instruction)tom__arg);

Instruction rhs = 
tom_rhs;
Instruction lhs = 
tom_lhs;
if ((rhs.getType() instanceof BaseType && ((BaseType)rhs.getType()).asFloat() != null) || (lhs.getType() instanceof BaseType && ((BaseType)lhs.getType()).asFloat() != null))
throw new SimpleLoopException((
tom_gt).getBasicBlock(), "Unable to find a bound");
Instruction instruction = 
tom_make_ival(1);
instruction.setType(
tom_rhs.getType());
instruction = 
tom_make_add(tom_cons_array_InstL(instruction,tom_cons_array_InstL(tom_rhs,tom_empty_array_InstL(2))));
instruction.setType(
tom_rhs.getType());
instruction = 
tom_make_ge(tom_cons_array_InstL(instruction,tom_cons_array_InstL(tom_lhs,tom_empty_array_InstL(2))));
instruction.setType(
tom_gt.getType());
return instruction;

}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_lt(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_9=tom_get_slot_lt_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_9))) {
int tomMatch1_13=0;
if (!(tomMatch1_13 >= tom_get_size_InstL_InstL(tomMatch1_9))) {
Instruction tom_lhs=tom_get_element_InstL_InstL(tomMatch1_9,tomMatch1_13);
int tomMatch1_14=tomMatch1_13 + 1;
if (!(tomMatch1_14 >= tom_get_size_InstL_InstL(tomMatch1_9))) {
Instruction tom_rhs=tom_get_element_InstL_InstL(tomMatch1_9,tomMatch1_14);
if (tomMatch1_14 + 1 >= tom_get_size_InstL_InstL(tomMatch1_9)) {
Instruction tom_lt=((Instruction)tom__arg);

Instruction rhs = 
tom_rhs;
Instruction lhs = 
tom_lhs;
if ((rhs.getType() instanceof BaseType && ((BaseType)rhs.getType()).asFloat() != null) || (lhs.getType() instanceof BaseType && ((BaseType)lhs.getType()).asFloat() != null))
throw new SimpleLoopException((
tom_lt).getBasicBlock(), "Unable to find a bound");
Instruction instruction = 
tom_make_ival(1);
instruction.setType(
tom_rhs.getType());
instruction = 
tom_make_sub(tom_cons_array_InstL(instruction,tom_cons_array_InstL(tom_rhs,tom_empty_array_InstL(2))));
instruction.setType(
tom_rhs.getType());
instruction = 
tom_make_le(tom_cons_array_InstL(instruction,tom_cons_array_InstL(tom_lhs,tom_empty_array_InstL(2))));
instruction.setType(
tom_lt.getType());
return instruction;

}
}
}
}
}
}
}
}
}
return _visit_Inst(tom__arg,introspector);
}
}
private static  tom.library.sl.Strategy  tom_make_CheckEqualities() { 
return new CheckEqualities();
}


private void findBound(Symbol loopVariable) {
BasicBlock condition = (BasicBlock)forBlock.getTestBlock();
if (condition.getInstructionCount() == 0)
throw new SimpleLoopException(forBlock, "Unable to find a bound");
Instruction boundInstr = condition.getInstruction(0);

{
{
if (tom_is_sort_Inst(boundInstr)) {
if (tom_is_sort_Inst(((Instruction)boundInstr))) {
if (tom_is_fun_sym_brcond(((Instruction)((Instruction)boundInstr)))) {
Instruction tom_generic=tom_get_slot_brcond_cond(((Instruction)boundInstr));

if (checkOperator(
tom_generic)) {
GenericInstruction genericInstruction = (GenericInstruction)
tom_generic;
//checkDivision(genericInstruction);
findBound(genericInstruction, loopVariable);
}

}
}
}
}
{
if (tom_is_sort_Inst(boundInstr)) {
if (tom_is_sort_Inst(((Instruction)boundInstr))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)boundInstr)))) {
Instruction tom_g=((Instruction)boundInstr);

if (checkOperator(
tom_g)) {
GenericInstruction genericInstruction = (GenericInstruction)
tom_g;
//checkDivision(genericInstruction);
findBound(genericInstruction, loopVariable);
}

}
}
}
}
}

if (bound == null)
throw new SimpleLoopException(forBlock, "Unable to find a bound");
}

private boolean checkOperator(Instruction instruction) {

{
{
if (tom_is_sort_Inst(instruction)) {
if (tom_is_sort_Inst(((Instruction)instruction))) {
if (tom_is_fun_sym_ge(((Instruction)((Instruction)instruction)))) {

return true;

}
}
}
}
{
if (tom_is_sort_Inst(instruction)) {
if (tom_is_sort_Inst(((Instruction)instruction))) {
if (tom_is_fun_sym_gt(((Instruction)((Instruction)instruction)))) {

return true;

}
}
}
}
{
if (tom_is_sort_Inst(instruction)) {
if (tom_is_sort_Inst(((Instruction)instruction))) {
if (tom_is_fun_sym_le(((Instruction)((Instruction)instruction)))) {

return true;

}
}
}
}
{
if (tom_is_sort_Inst(instruction)) {
if (tom_is_sort_Inst(((Instruction)instruction))) {
if (tom_is_fun_sym_lt(((Instruction)((Instruction)instruction)))) {

return true;

}
}
}
}
{
if (tom_is_sort_Inst(instruction)) {
if (tom_is_sort_Inst(((Instruction)instruction))) {
if (tom_is_fun_sym_eq(((Instruction)((Instruction)instruction)))) {

return true;

}
}
}
}
}

return false;
}

private void findBound(GenericInstruction genericInstruction, Symbol symbol) {
Instruction lhs = genericInstruction.getOperand(0);
Instruction rhs = genericInstruction.getOperand(1);
boolean symbolInLhs = false;
boolean symbolInRhs = false;
try {

tom_make_OutermostId(tom_make_IsThereLoopIterator(symbol)).visitLight(lhs, GenericIntrospector.INSTANCE);
} catch (VisitFailure e) {
symbolInLhs = true;
} catch (RuntimeException e) {
symbolInLhs = true;
}
try {

tom_make_OutermostId(tom_make_IsThereLoopIterator(symbol)).visitLight(rhs, GenericIntrospector.INSTANCE);
} catch (VisitFailure e) {
symbolInRhs = true;
} catch (RuntimeException e) {
symbolInRhs = true;
}
if (symbolInLhs ^ symbolInRhs)
manageLoopIterator(genericInstruction, symbol, symbolInLhs);
else if (symbolInLhs)
throw new SimpleLoopException(forBlock, "Unable to manage loop iterator on both side !");
else
throw new SimpleLoopException(forBlock, "Unable to manage loop without iterator !");
}


public static class IsThereLoopIterator extends tom.library.sl.AbstractStrategyBasic {
private Symbol symbol;
public IsThereLoopIterator(Symbol symbol) {
super(tom_make_Identity());
this.symbol=symbol;
}
public Symbol getsymbol() {
return symbol;
}
public tom.library.sl.Visitable[] getChildren() {
tom.library.sl.Visitable[] stratChildren = new tom.library.sl.Visitable[getChildCount()];
stratChildren[0] = super.getChildAt(0);
return stratChildren;}
public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
super.setChildAt(0, children[0]);
return this;
}
public int getChildCount() {
return 1;
}
public tom.library.sl.Visitable getChildAt(int index) {
switch (index) {
case 0: return super.getChildAt(0);
default: throw new IndexOutOfBoundsException();
}
}
public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
switch (index) {
case 0: return super.setChildAt(0, child);
default: throw new IndexOutOfBoundsException();
}
}
@SuppressWarnings("unchecked")
public <T> T visitLight(T v, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
if (tom_is_sort_Inst(v)) {
return ((T)visit_Inst(((Instruction)v),introspector));
}
if (!(( null  == environment))) {
return ((T)any.visit(environment,introspector));
} else {
return any.visitLight(v,introspector);
}
}
@SuppressWarnings("unchecked")
public Instruction _visit_Inst(Instruction arg, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
if (!(( null  == environment))) {
return ((Instruction)any.visit(environment,introspector));
} else {
return any.visitLight(arg,introspector);
}
}
@SuppressWarnings("unchecked")
public Instruction visit_Inst(Instruction tom__arg, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
{
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_symref(((Instruction)((Instruction)tom__arg)))) {

if (symbol.equals(
tom_get_slot_symref_symbol(((Instruction)tom__arg))))
throw new RuntimeException("LoopIterator found");

}
}
}
}
}
return _visit_Inst(tom__arg,introspector);
}
}
private static  tom.library.sl.Strategy  tom_make_IsThereLoopIterator(Symbol t0) { 
return new IsThereLoopIterator(t0);
}


private void manageLoopIterator(GenericInstruction genericInstruction, Symbol symbol, boolean symbolInLhs) {
Instruction iterator = null;
Instruction instruction = null;
Instruction otherSide = null;
if (symbolInLhs) {
iterator = genericInstruction.getChildren().get(0);
bound = genericInstruction.getChildren().get(1);
} else {
iterator = genericInstruction.getChildren().get(1);
bound = genericInstruction.getChildren().get(0);
}
while (iterator != null && !(iterator instanceof SymbolInstruction)) {
if (symbolInLhs) {
instruction = genericInstruction.getChildren().get(0);
otherSide = genericInstruction.getChildren().get(1);
} else {
instruction = genericInstruction.getChildren().get(1);
otherSide = genericInstruction.getChildren().get(0);
}

{
{
if (tom_is_sort_Inst(instruction)) {
if (tom_is_sort_Inst(((Instruction)instruction))) {
if (tom_is_fun_sym_add(((Instruction)((Instruction)instruction)))) {
 EList<Instruction>  tomMatch5_1=tom_get_slot_add_children(((Instruction)instruction));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch5_1))) {
int tomMatch5_5=0;
if (!(tomMatch5_5 >= tom_get_size_InstL_InstL(tomMatch5_1))) {
Instruction tom_lhs=tom_get_element_InstL_InstL(tomMatch5_1,tomMatch5_5);
int tomMatch5_6=tomMatch5_5 + 1;
if (!(tomMatch5_6 >= tom_get_size_InstL_InstL(tomMatch5_1))) {
Instruction tom_rhs=tom_get_element_InstL_InstL(tomMatch5_1,tomMatch5_6);
if (tomMatch5_6 + 1 >= tom_get_size_InstL_InstL(tomMatch5_1)) {

try {

tom_make_OutermostId(tom_make_IsThereLoopIterator(symbol)).visitLight(
tom_lhs, GenericIntrospector.INSTANCE);
genericInstruction.getChildren().clear();
genericInstruction.getChildren().add(0, 
tom_rhs);
Instruction substract = 
tom_make_sub(tom_cons_array_InstL(tom_lhs,tom_cons_array_InstL(otherSide,tom_empty_array_InstL(2))));
substract.setType(genericInstruction.getType());
genericInstruction.getChildren().add(1, substract);
} catch (VisitFailure e) {
throw new SimpleLoopException(forBlock, "Unable to manage loop with an addition on iterator !");
} catch (RuntimeException e) {
try {

tom_make_OutermostId(tom_make_IsThereLoopIterator(symbol)).visitLight(
tom_rhs, GenericIntrospector.INSTANCE);
genericInstruction.getChildren().clear();
genericInstruction.getChildren().add(0, 
tom_lhs);
Instruction substract = 
tom_make_sub(tom_cons_array_InstL(tom_rhs,tom_cons_array_InstL(otherSide,tom_empty_array_InstL(2))));
substract.setType(genericInstruction.getType());
genericInstruction.getChildren().add(1, substract);
} catch (VisitFailure visitFailure) {
throw new SimpleLoopException(forBlock, "Unable to manage loop with an addition on iterator !");
} catch (RuntimeException runtimeException) {
throw new SimpleLoopException(forBlock, "Unable to manage loop with an iterator on both side of addition !");
}
}

}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(instruction)) {
if (tom_is_sort_Inst(((Instruction)instruction))) {
if (tom_is_fun_sym_sub(((Instruction)((Instruction)instruction)))) {
 EList<Instruction>  tomMatch5_9=tom_get_slot_sub_children(((Instruction)instruction));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch5_9))) {
int tomMatch5_13=0;
if (!(tomMatch5_13 >= tom_get_size_InstL_InstL(tomMatch5_9))) {
Instruction tom_lhs=tom_get_element_InstL_InstL(tomMatch5_9,tomMatch5_13);
int tomMatch5_14=tomMatch5_13 + 1;
if (!(tomMatch5_14 >= tom_get_size_InstL_InstL(tomMatch5_9))) {
Instruction tom_rhs=tom_get_element_InstL_InstL(tomMatch5_9,tomMatch5_14);
if (tomMatch5_14 + 1 >= tom_get_size_InstL_InstL(tomMatch5_9)) {

try {

tom_make_OutermostId(tom_make_IsThereLoopIterator(symbol)).visitLight(
tom_lhs, GenericIntrospector.INSTANCE);
genericInstruction.getChildren().clear();
genericInstruction.getChildren().add(0, 
tom_rhs);
Instruction addition = 
tom_make_add(tom_cons_array_InstL(tom_lhs,tom_cons_array_InstL(otherSide,tom_empty_array_InstL(2))));
addition.setType(genericInstruction.getType());
genericInstruction.getChildren().add(1, addition);
} catch (VisitFailure e) {
throw new SimpleLoopException(forBlock, "Unable to manage loop with a subtraction on iterator !");
} catch (RuntimeException e) {
try {

tom_make_OutermostId(tom_make_IsThereLoopIterator(symbol)).visitLight(
tom_rhs, GenericIntrospector.INSTANCE);
genericInstruction.getChildren().clear();
genericInstruction.getChildren().add(0, 
tom_lhs);
Instruction addition = 
tom_make_add(tom_cons_array_InstL(tom_rhs,tom_cons_array_InstL(otherSide,tom_empty_array_InstL(2))));
addition.setType(genericInstruction.getType());
genericInstruction.getChildren().add(1, addition);
} catch (VisitFailure visitFailure) {
throw new SimpleLoopException(forBlock, "Unable to manage loop with a subtraction on iterator !");
} catch (RuntimeException runtimeException) {
throw new SimpleLoopException(forBlock, "Unable to manage loop with an iterator on both side of subtraction !");
}
}

}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(instruction)) {
if (tom_is_sort_Inst(((Instruction)instruction))) {
if (tom_is_fun_sym_mul(((Instruction)((Instruction)instruction)))) {
 EList<Instruction>  tomMatch5_17=tom_get_slot_mul_children(((Instruction)instruction));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch5_17))) {
int tomMatch5_21=0;
if (!(tomMatch5_21 >= tom_get_size_InstL_InstL(tomMatch5_17))) {
Instruction tom_lhs=tom_get_element_InstL_InstL(tomMatch5_17,tomMatch5_21);
int tomMatch5_22=tomMatch5_21 + 1;
if (!(tomMatch5_22 >= tom_get_size_InstL_InstL(tomMatch5_17))) {
Instruction tom_rhs=tom_get_element_InstL_InstL(tomMatch5_17,tomMatch5_22);
if (tomMatch5_22 + 1 >= tom_get_size_InstL_InstL(tomMatch5_17)) {

try {

tom_make_OutermostId(tom_make_IsThereLoopIterator(symbol)).visitLight(
tom_lhs, GenericIntrospector.INSTANCE);
genericInstruction.getChildren().clear();
genericInstruction.getChildren().add(0, 
tom_rhs);
Instruction division = 
tom_make_div(tom_cons_array_InstL(tom_lhs,tom_cons_array_InstL(otherSide,tom_empty_array_InstL(2))));
division.setType(genericInstruction.getType());
genericInstruction.getChildren().add(1, division);
} catch (VisitFailure e) {
throw new SimpleLoopException(forBlock, "Unable to manage loop with a multiplication on iterator !");
} catch (RuntimeException e) {
try {

tom_make_OutermostId(tom_make_IsThereLoopIterator(symbol)).visitLight(
tom_rhs, GenericIntrospector.INSTANCE);
genericInstruction.getChildren().clear();
genericInstruction.getChildren().add(0, 
tom_lhs);
Instruction division = 
tom_make_div(tom_cons_array_InstL(tom_rhs,tom_cons_array_InstL(otherSide,tom_empty_array_InstL(2))));
division.setType(genericInstruction.getType());
genericInstruction.getChildren().add(1, division);
} catch (VisitFailure visitFailure) {
throw new SimpleLoopException(forBlock, "Unable to manage loop with a multiplication on iterator !");
} catch (RuntimeException runtimeException) {
throw new SimpleLoopException(forBlock, "Unable to manage loop with an iterator on both side of multiplication !");
}
}

}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(instruction)) {
if (tom_is_sort_Inst(((Instruction)instruction))) {
if (tom_is_fun_sym_div(((Instruction)((Instruction)instruction)))) {
 EList<Instruction>  tomMatch5_25=tom_get_slot_div_children(((Instruction)instruction));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch5_25))) {
int tomMatch5_29=0;
if (!(tomMatch5_29 >= tom_get_size_InstL_InstL(tomMatch5_25))) {
Instruction tom_lhs=tom_get_element_InstL_InstL(tomMatch5_25,tomMatch5_29);
int tomMatch5_30=tomMatch5_29 + 1;
if (!(tomMatch5_30 >= tom_get_size_InstL_InstL(tomMatch5_25))) {
Instruction tom_rhs=tom_get_element_InstL_InstL(tomMatch5_25,tomMatch5_30);
if (tomMatch5_30 + 1 >= tom_get_size_InstL_InstL(tomMatch5_25)) {

try {

tom_make_OutermostId(tom_make_IsThereLoopIterator(symbol)).visitLight(
tom_lhs, GenericIntrospector.INSTANCE);
genericInstruction.getChildren().clear();
genericInstruction.getChildren().add(0, 
tom_rhs);
Instruction multiplication = 
tom_make_mul(tom_cons_array_InstL(tom_lhs,tom_cons_array_InstL(otherSide,tom_empty_array_InstL(2))));
multiplication.setType(genericInstruction.getType());
genericInstruction.getChildren().add(1, multiplication);
} catch (VisitFailure e) {
throw new SimpleLoopException(forBlock, "Unable to manage loop with a division on iterator !");
} catch (RuntimeException e) {
try {

tom_make_OutermostId(tom_make_IsThereLoopIterator(symbol)).visitLight(
tom_rhs, GenericIntrospector.INSTANCE);
genericInstruction.getChildren().clear();
genericInstruction.getChildren().add(0, 
tom_lhs);
Instruction multiplication = 
tom_make_mul(tom_cons_array_InstL(tom_rhs,tom_cons_array_InstL(otherSide,tom_empty_array_InstL(2))));
multiplication.setType(genericInstruction.getType());
genericInstruction.getChildren().add(1, multiplication);
} catch (VisitFailure visitFailure) {
throw new SimpleLoopException(forBlock, "Unable to manage loop with a division on iterator !");
} catch (RuntimeException runtimeException) {
throw new SimpleLoopException(forBlock, "Unable to manage loop with an iterator on both side of division !");
}
}

}
}
}
}
}
}
}
}
}

iterator = genericInstruction.getChildren().get(0);
bound = genericInstruction.getChildren().get(1);

}
}
}
