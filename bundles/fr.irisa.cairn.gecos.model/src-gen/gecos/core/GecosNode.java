/**
 */
package gecos.core;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Gecos Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.core.CorePackage#getGecosNode()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface GecosNode extends EObject {
} // GecosNode
