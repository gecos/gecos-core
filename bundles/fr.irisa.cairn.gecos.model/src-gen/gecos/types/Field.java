/**
 */
package gecos.types;

import gecos.core.ITypedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Field</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.Field#getRecord <em>Record</em>}</li>
 *   <li>{@link gecos.types.Field#getName <em>Name</em>}</li>
 *   <li>{@link gecos.types.Field#getType <em>Type</em>}</li>
 *   <li>{@link gecos.types.Field#getOffset <em>Offset</em>}</li>
 *   <li>{@link gecos.types.Field#getBitwidth <em>Bitwidth</em>}</li>
 * </ul>
 *
 * @see gecos.types.TypesPackage#getField()
 * @model
 * @generated
 */
public interface Field extends TypesVisitable, ITypedElement {
	/**
	 * Returns the value of the '<em><b>Record</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link gecos.types.RecordType#getFields <em>Fields</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record</em>' container reference.
	 * @see #setRecord(RecordType)
	 * @see gecos.types.TypesPackage#getField_Record()
	 * @see gecos.types.RecordType#getFields
	 * @model opposite="fields" transient="false"
	 * @generated
	 */
	RecordType getRecord();

	/**
	 * Sets the value of the '{@link gecos.types.Field#getRecord <em>Record</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Record</em>' container reference.
	 * @see #getRecord()
	 * @generated
	 */
	void setRecord(RecordType value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see gecos.types.TypesPackage#getField_Name()
	 * @model unique="false" dataType="gecos.types.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link gecos.types.Field#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(Type)
	 * @see gecos.types.TypesPackage#getField_Type()
	 * @model resolveProxies="false"
	 * @generated
	 */
	Type getType();

	/**
	 * Sets the value of the '{@link gecos.types.Field#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Type value);

	/**
	 * Returns the value of the '<em><b>Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Offset</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Offset</em>' attribute.
	 * @see #setOffset(int)
	 * @see gecos.types.TypesPackage#getField_Offset()
	 * @model unique="false" dataType="gecos.types.int"
	 * @generated
	 */
	int getOffset();

	/**
	 * Sets the value of the '{@link gecos.types.Field#getOffset <em>Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Offset</em>' attribute.
	 * @see #getOffset()
	 * @generated
	 */
	void setOffset(int value);

	/**
	 * Returns the value of the '<em><b>Bitwidth</b></em>' attribute.
	 * The default value is <code>"-1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bitwidth</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bitwidth</em>' attribute.
	 * @see #setBitwidth(long)
	 * @see gecos.types.TypesPackage#getField_Bitwidth()
	 * @model default="-1" unique="false" dataType="gecos.types.long" required="true"
	 * @generated
	 */
	long getBitwidth();

	/**
	 * Sets the value of the '{@link gecos.types.Field#getBitwidth <em>Bitwidth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bitwidth</em>' attribute.
	 * @see #getBitwidth()
	 * @generated
	 */
	void setBitwidth(long value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitField(this);'"
	 * @generated
	 */
	void accept(TypesVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.boolean" unique="false" fieldUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((field == this))\n{\n\treturn true;\n}\nboolean isSameName = false;\nif (((this.getName() == null) &amp;&amp; (field.getName() == null)))\n{\n\tisSameName = true;\n}\nelse\n{\n\t&lt;%java.lang.String%&gt; _name = this.getName();\n\tboolean _tripleEquals = (_name == null);\n\t&lt;%java.lang.String%&gt; _name_1 = field.getName();\n\tboolean _tripleEquals_1 = (_name_1 == null);\n\tboolean _xor = (_tripleEquals ^ _tripleEquals_1);\n\tif (_xor)\n\t{\n\t\tisSameName = false;\n\t}\n\telse\n\t{\n\t\tisSameName = this.getName().equals(field.getName());\n\t}\n}\nboolean isSameBase = false;\nif (((this.getType() == null) &amp;&amp; (field.getType() == null)))\n{\n\tisSameBase = true;\n}\nelse\n{\n\t&lt;%gecos.types.Type%&gt; _type = this.getType();\n\tboolean _tripleEquals_2 = (_type == null);\n\t&lt;%gecos.types.Type%&gt; _type_1 = field.getType();\n\tboolean _tripleEquals_3 = (_type_1 == null);\n\tboolean _xor_1 = (_tripleEquals_2 ^ _tripleEquals_3);\n\tif (_xor_1)\n\t{\n\t\treturn false;\n\t}\n\telse\n\t{\n\t\tisSameBase = this.getType().isEqual(field.getType());\n\t}\n}\nreturn ((isSameName &amp;&amp; isSameBase) &amp;&amp; (this.getOffset() == field.getOffset()));'"
	 * @generated
	 */
	boolean isEqual(Field field);

} // Field
