#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "tools.h"
#include "defs.h"
#include "data_size.h"

#if defined(__KAHRISMA__) //_kahrisma_
	extern void sim_timer_start(const char *);
	extern void sim_timer_stop(const char *);
	extern void sim_reset_stats();
#elif defined(__XENTIUM__)
#elif defined(__VEX__)
	#include <vex_simd.h>
//	extern void sim_ta_disable();	/* disable simulation */
//	extern void sim_ta_enable();
	extern unsigned long long get_sim_cycle_counter();
#elif defined(__ST240__)
	#include <platform.h>
	static volatile unsigned int *_st240_pmcr = (unsigned int *) LXPM_CR; //performance monitor Control Register
	static volatile unsigned long *_st240_pmclkr = (unsigned int *) LXPM_PCLK; //performance monitor CLocK Register
#else
	#include <time.h>
#endif

extern void fir(TYPE_0 xn[], TYPE_1 yn[]);


int main(int argc, const char **argv) {
	int i,j,n;
	char tmp[128];
	FILE *outfile, *infile;
	int _N, nDims;
	unsigned long long time0, time1;

	TYPE_0 *X;//Nc-1+N
	TYPE_1 *out;//N

#if defined(__VEX__)
	sim_ta_disable();
#endif

	if(argc < 3) {
		fprintf(stderr, "Usage: %s input_file output_file\n", argv[0]);
		exit(-1);
	}

	infile = fopen(argv[1], "r");
	if(infile == NULL) {
		fprintf(stderr, "couldn't open file \"%s\"!\n", argv[1]);
		exit(-2);
	}

	outfile = fopen(argv[2], "w");
	if(outfile == NULL) {
		fprintf(stderr, "couldn't open file \"%s\"!\n", argv[2]);
		exit(-2);
	}

	/* init data */
	fgets(tmp, 128, infile);
	nDims = atoi(tmp);
	if(nDims != 1) {
		fprintf(stderr, "nb of dimensions in '%s' (%d) is different from 1\n", argv[1], nDims);
		exit(-3);
	}

	fgets(tmp, 128, infile);
	_N = atoi(tmp);
	if(_N != N) {
		fprintf(stderr, "nb of samples %d in '%s' is different from %d\n", _N, argv[1], N);
		exit(-3);
	}

	X    = (TYPE_0 *) malloc(_N * sizeof(TYPE_0));
	out  = (TYPE_1 *) malloc(_N * sizeof(TYPE_1));

	for(j=0; j<_N; j++) {
		fgets(tmp, 128, infile);
		X[j] = (TYPE_0) (strcmp(TYPE2STR(TYPE_0), "float")? strtol(tmp, NULL, 10) : atof(tmp));

		out[j] = 0;
	}
	fclose(infile);


//	for(j=NC-1; j<N+NC-1; j++)
//		printf("%.1f ", X[j]);
//	printf("\n\n");


	/* 2- call fir() */
#if defined(__KAHRISMA__) //_kahrisma_
	sim_reset_stats();
	printf("\nSTATS RESET!\n");
	sim_timer_start("MY_KTIMER");
#elif defined(__XENTIUM__)
#elif defined(__VEX__)
	sim_ta_enable();
	time0 = get_sim_cycle_counter();
#elif defined(__ST240__)
//	bsp_pm_reset(); //reset performance monitor counters
//	bsp_pm_start(); // restart ..
//	time0 =bsp_pm_clock_get(); //get clock timer
	*_st240_pmcr = 0x2; /* disable and reset PM counter */
	*_st240_pmcr |= 1<<0; /* enable PM counter */
	time0 = *_st240_pmclkr;
#elif defined(CLOCK_PROCESS_CPUTIME_ID) //x86 & clock_gettime available
	struct timespec start, end;
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
#endif

	for(n=0; n<N; n++) {
		fir(&X[n], &out[n]);
	}

#if defined(__KAHRISMA__)
	sim_timer_stop("MY_KTIMER");
#elif defined(__XENTIUM__)
#elif defined(__VEX__)
	sim_ta_disable();
	sim_ta_finish();
	printf("%llu\n", get_sim_cycle_counter() - time0);
#elif defined(__ST240__)
//	time1 = bsp_pm_clock_get();
	time1 = *_st240_pmclkr;
	bsp_pm_stop(); //stop performance monitor counters
	printf("%llu\n", time1 - time0);
#elif defined(CLOCK_PROCESS_CPUTIME_ID) //x86 & clock_gettime available
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);
	double duration = (double)(end.tv_sec-start.tv_sec) + ((double)(end.tv_nsec-start.tv_nsec)/1000000000);
	printf("Execution time = %lf sec\n\n", duration);
#endif

	/* output result */
	fprintf(outfile, "1\n"); //number of dimensions
	fprintf(outfile, "%d\n", N);
	for (j = 0; j < N; j++) {
		fprintf(outfile, PRINT_ID(TYPE_1), out[j]);
		fprintf(outfile, "\n");
	}
	fclose(outfile);

	free(X);
	free(out);

	return 0;
}
