/**
 */
package gecos.blocks;

import gecos.core.CoreVisitor;
import gecos.core.ScopeContainer;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>For C99 Block</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.blocks.BlocksPackage#getForC99Block()
 * @model
 * @generated
 */
public interface ForC99Block extends ForBlock, ScopeContainer, BlocksVisitable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitForC99Block(this);'"
	 * @generated
	 */
	void accept(BlocksVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitScopeContainer(this);'"
	 * @generated
	 */
	void accept(CoreVisitor visitor);

} // ForC99Block
