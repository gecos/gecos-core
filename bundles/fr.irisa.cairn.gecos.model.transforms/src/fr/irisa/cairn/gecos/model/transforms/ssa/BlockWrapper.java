package fr.irisa.cairn.gecos.model.transforms.ssa;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.tools.controlflow.BuildControlFlow;
import fr.irisa.cairn.gecos.model.tools.controlflow.ClearControlFlow;
import fr.irisa.cairn.gecos.model.tools.controlflow.ClearControlFlow.MODE;
import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch;
import gecos.annotations.ExtendedAnnotation;
import gecos.annotations.IAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CaseBlock;
import gecos.blocks.CompositeBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.ForC99Block;
import gecos.blocks.IfBlock;
import gecos.blocks.SimpleForBlock;
import gecos.blocks.SwitchBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Procedure;
import gecos.gecosproject.GecosProject;

/**
 * <p>This class wraps all control structures in a CompositeBlock with a dummy
 * empty BasicBlock at the end. It is used before the ComputeSSAForm so that Phi
 * nodes are not inserted in initialization/condition/step blocks of control
 * structures. For instance,</p>
 * if (a == 0) {<br/>
 * &nbsp;&nbsp;a = 1;<br/>
 * }<br/>
 * for (i = 0; i < a; i++) {<br/>
 * &nbsp;&nbsp;S0(i);<br/>
 * }<br/>
 *<p>becomes</p>
 * {<br/>
 * &nbsp;&nbsp;if (a == 0) {<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;a = 1;<br/>
 * &nbsp;&nbsp;}<br/>
 * &nbsp;&nbsp;// dummy basicblock<br/>
 * }<br/>
 * {<br/>
 * &nbsp;&nbsp;for (i = 0; i < a; i++) {<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;S0(i);<br/>
 * &nbsp;&nbsp;}<br/>
 * &nbsp;&nbsp;// dummy basicblock<br/>
 * }<br/>
 *<br/> 
 */
public class BlockWrapper extends BlocksDefaultSwitch<Object> {
	
	private static final boolean debug = false;

	private static final String SSA_DUMMY_PHI_BLOCK_KEY = "BlockWrapper::ssaControlDummyPhiBlock";
	private static final String SSA_WRAPPER_BLOCK_KEY = "BlockWrapper::ssaControlWrapperBlock";
	
	List<Procedure> procs;

	public BlockWrapper (GecosProject p) {
		procs = p.listProcedures();
	}
	public BlockWrapper (Procedure p) {
		procs = new ArrayList<>(1);
		procs.add(p);
	}
	
	public void compute() {
		for(Procedure proc : procs) {
			ClearControlFlow.clearCF(proc,MODE.CFG);
			if(debug)System.out.println("Procedure "+proc.getSymbol().getName());
			doSwitch(proc.getBody());
			BuildControlFlow.buildCF(proc);
		}
	}

	private void wrapInCompositeblock(Block object) {
		if(debug)System.out.println("Wrapping "+object+ " into a Compositeblock");
		BasicBlock phiBlock = GecosUserBlockFactory.BBlock(object.getNumber()*10+2);
		CompositeBlock wrapperBlk = GecosUserBlockFactory.CompositeBlock(phiBlock);
		wrapperBlk.setNumber(object.getNumber()*10+1);
		object.getParent().replace(object, wrapperBlk);
		wrapperBlk.getChildren().add(0, object);
		
		// Annotate phiBlock as a dummy phi block used for control block if 
		ExtendedAnnotation annotation = GecosUserAnnotationFactory.extendedContains();
		annotation.getRefs().add(object);
		phiBlock.getAnnotations().put(SSA_DUMMY_PHI_BLOCK_KEY, annotation);
		
		// Annotate wrapperBlk as being a wrapper block
		wrapperBlk.getAnnotations().put(SSA_WRAPPER_BLOCK_KEY, null);
	}
	
	/**
	 * @param bb basic block
	 * @return null if bb is not a dummy phi block. Otherwise it returns 
	 * the Control block after which this dummy block was added.
	 */
	public static Block getDummyPhiBlockSource(BasicBlock bb) {
		IAnnotation annotation = bb.getAnnotation(SSA_DUMMY_PHI_BLOCK_KEY);
		if(annotation instanceof ExtendedAnnotation) {
			EObject source = ((ExtendedAnnotation) annotation).getRefs().get(0);
			if(source instanceof Block)
				return (Block) source;
		}
		return null;
	}
	
	public static boolean isWrapperBlock(CompositeBlock cb) {
		return cb.getAnnotations().containsKey(SSA_WRAPPER_BLOCK_KEY);
	}

	@Override
	public Object caseForBlock(ForBlock object) {
		wrapInCompositeblock(object);
		if (object.getBodyBlock() != null)
			doSwitch(object.getBodyBlock());
		return true;
	}

	@Override
	public Object caseIfBlock(IfBlock object) {
		wrapInCompositeblock(object);
		doSwitch(object.getTestBlock());
		if ( object.getThenBlock()  != null )
			doSwitch(object.getThenBlock());
		if ( object.getElseBlock()  != null )
			doSwitch(object.getElseBlock());
		return true;
	}

	@Override
	public Object caseWhileBlock(WhileBlock object) {
		wrapInCompositeblock(object);
		if (object.getBodyBlock() != null)
			doSwitch(object.getBodyBlock());
		return true;
	}

	@Override
	public Object caseDoWhileBlock(DoWhileBlock object) {
		wrapInCompositeblock(object);
		if (object.getBodyBlock() != null)
			doSwitch(object.getBodyBlock());
		return true;
	}

	@Override
	public Object caseSwitchBlock(SwitchBlock object) {
		wrapInCompositeblock(object);
		if (object.getBodyBlock() != null)
			doSwitch(object.getBodyBlock());
		return true;
	}

	@Override
	public Object caseCaseBlock(CaseBlock object) {
		if (object.getBody() != null)
			doSwitch(object.getBody());
		return true;
	}

	@Override
	public Object caseForC99Block(ForC99Block object) {
		wrapInCompositeblock(object);
		if (object.getBodyBlock() != null)
			doSwitch(object.getBodyBlock());
		return true;
	}

	@Override
	public Object caseSimpleForBlock(SimpleForBlock object) {
		wrapInCompositeblock(object);
		if (object.getBodyBlock() != null)
			doSwitch(object.getBodyBlock());
		return true;
	}
	
}
