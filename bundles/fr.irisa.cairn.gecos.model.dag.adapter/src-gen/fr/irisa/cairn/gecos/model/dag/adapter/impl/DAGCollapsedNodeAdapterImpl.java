package fr.irisa.cairn.gecos.model.dag.adapter.impl;

import java.util.List;

import fr.irisa.cairn.gecos.model.dag.adapter.DAGInstructionAdapterComponent;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGCollapsedNodeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGEdgeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGInstructionAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGNodeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGPortAdapter;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.implement.CollapsedNode;
import fr.irisa.cairn.graph.implement.Graph;
import gecos.dag.DAGNode;
import gecos.dag.DagFactory;

public class DAGCollapsedNodeAdapterImpl extends CollapsedNode implements
		IDAGCollapsedNodeAdapter {
	// private ICollapsedNode collapsed;

	private final DAGNode adapted;

	public DAGCollapsedNodeAdapterImpl(DAGNode adapted) {
		super(true);
		this.adapted = adapted;
	}

	public DAGNode getAdaptedNode() {
		return this.adapted;
	}

	// public void mapInput(IPort in, IPort nestedPort) {
	// collapsed.mapInput(in, nestedPort);
	// }
	//
	// public void mapOutput(IPort out, IPort nestedPort) {
	// collapsed.mapOutput(out, nestedPort);
	// }
	//
	// public IDAGPortAdapter getMappedInputPort(IPort in) {
	// return (IDAGPortAdapter) collapsed.getMappedInputPort(in);
	// }
	//
	// public IDAGPortAdapter getMappedOutputPort(IPort out) {
	// return (IDAGPortAdapter) collapsed.getMappedInputPort(out);
	// }
	//
	// @SuppressWarnings("unchecked")
	// public List<IDAGNodeAdapter> expand() {
	// return (List<IDAGNodeAdapter>) collapsed.expand();
	// }
	//
	// public boolean isDirected() {
	// return collapsed.isDirected();
	// }
	//
	// @SuppressWarnings("unchecked")
	// public Set<IDAGNodeAdapter> getNodes() {
	// return (Set<IDAGNodeAdapter>) collapsed.getNodes();
	// }
	//
	// @SuppressWarnings("unchecked")
	// public Set<IDAGEdgeAdapter> getEdges() {
	// return (Set<IDAGEdgeAdapter>) collapsed.getEdges();
	// }
	//
	// public void addEdge(IEdge e) {
	// collapsed.addEdge(e);
	// }
	//
	// public void addNode(INode e) {
	// collapsed.addNode(e);
	// }
	//
	// public void removeEdge(IEdge e) {
	// collapsed.removeEdge(e);
	// }
	//
	// public void removeNode(INode e) {
	// collapsed.removeNode(e);
	// }
	//
	// @SuppressWarnings("unchecked")
	// public Set<IDAGEdgeAdapter> getExternalInputs() {
	// return (Set<IDAGEdgeAdapter>) collapsed.getExternalInputs();
	// }
	//
	// @SuppressWarnings("unchecked")
	// public Set<IDAGEdgeAdapter> getExternalOutputs() {
	// return (Set<IDAGEdgeAdapter>) collapsed.getExternalOutputs();
	// }

	/**
	 * Add an input port to the node.
	 * 
	 * @model default=""
	 * 
	 * @return created port
	 */
	@Override
	public void addInputPort(IPort p) {
		super.addInputPort(p);

		if (p instanceof IDAGPortAdapter && this.getGraph() != null) {
			this.getGraph().getContainer().addPort((IDAGPortAdapter) p);
		}

	}

	@Override
	public void removeInputPort(IPort p) {
		super.removeInputPort(p);

		if (p instanceof IDAGPortAdapter && this.getGraph() != null) {
			this.getGraph().getContainer().removePort((IDAGPortAdapter) p);
		}

	}

	/**
	 * Add an output port to the node.
	 * 
	 * @model default=""
	 * 
	 * @return created port
	 */
	@Override
	public void addOutputPort(IPort p) {
		super.addOutputPort(p);

		if (p instanceof IDAGPortAdapter && this.getGraph() != null) {
			this.getGraph().getContainer().addPort((IDAGPortAdapter) p);
		}

	}

	@Override
	public void removeOutputPort(IPort p) {
		super.removeOutputPort(p);

		if (p instanceof IDAGPortAdapter && this.getGraph() != null) {
			this.getGraph().getContainer().removePort((IDAGPortAdapter) p);
		}

	}

	/**
	 * Get the graph containing the node.
	 * 
	 * @model default=""
	 * 
	 * @return the node graph or <code>null</code> if graph hasn't been set
	 */
	@Override
	public IDAGInstructionAdapter getGraph() {
		return (IDAGInstructionAdapter) super.getGraph();
	}

	/**
	 * Get a list of all incoming edges to the node.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<IDAGEdgeAdapter> getIncomingEdges() {
		return (List<IDAGEdgeAdapter>) super.getIncomingEdges();
	}

	/**
	 * Get a list of all outgoing edges from the node.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<IDAGEdgeAdapter> getOutgoingEdges() {
		return (List<IDAGEdgeAdapter>) super.getOutgoingEdges();
	}

	/**
	 * Get the list of all input ports.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<IDAGPortAdapter> getInputPorts() {
		return (List<IDAGPortAdapter>) super.getInputPorts();
	}

	/**
	 * Get the list of all output ports.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<IDAGPortAdapter> getOutputPorts() {
		return (List<IDAGPortAdapter>) super.getOutputPorts();
	}

	/**
	 * Get predecessors of this node in its graph.
	 * 
	 * @model default=""
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<IDAGNodeAdapter> getPredecessors() {
		return (List<IDAGNodeAdapter>) super.getPredecessors();
	}

	/**
	 * Get successors of this node in its graph.
	 * 
	 * @model default=""
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<IDAGNodeAdapter> getSuccessors() {
		return (List<IDAGNodeAdapter>) super.getSuccessors();
	}

	@Override
	public String toString() {
		return getAdaptedNode().toString();
	}

	@Override
	protected void initialize(boolean directed) {
		this.subgraph = (Graph) DAGInstructionAdapterComponent.INSTANCE
				.getAdapterFactory().createGraphAdapter(
						DagFactory.eINSTANCE.createDAGInstruction(), directed);
		this.subgraph.setParent(this);
	}
}
