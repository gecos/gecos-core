/**
 */
package gecos.instrs.impl;

import gecos.instrs.InstrsPackage;
import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;
import gecos.instrs.RetInstruction;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ret Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RetInstructionImpl extends ExprComponentImpl implements RetInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RetInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstrsPackage.eINSTANCE.getRetInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final InstrsVisitor visitor) {
		visitor.visitRetInstruction(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _elvis = null;
		Instruction _expr = this.getExpr();
		String _string = null;
		if (_expr!=null) {
			_string=_expr.toString();
		}
		if (_string != null) {
			_elvis = _string;
		} else {
			_elvis = "?";
		}
		String _plus = ("return " + _elvis);
		return (_plus + ";");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRet() {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSame(final Instruction instr) {
		if ((instr instanceof RetInstruction)) {
			return super.isSame(instr);
		}
		return false;
	}

} //RetInstructionImpl
