package fr.irisa.cairn.gecos.model.extensions.generators;

public interface IGecosCodeGenerator {
	
	public StringBuffer afterSymbolInformation = new StringBuffer();

	public String generate(Object o);
}