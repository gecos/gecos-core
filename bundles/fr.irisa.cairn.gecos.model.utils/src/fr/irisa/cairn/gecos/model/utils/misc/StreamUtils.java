package fr.irisa.cairn.gecos.model.utils.misc;

import static java.util.stream.Collectors.joining;

import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamUtils {

    public static <T, K, U, M extends Map<K, U>>
    Collector<T, ?, M> toMap(Function<? super T, ? extends K> keyMapper,
                                Function<? super T, ? extends U> valueMapper,
                                Supplier<M> mapSupplier) {
    	
    	return Collectors.toMap(keyMapper, valueMapper, 
    			(u,v) -> { throw new IllegalStateException(String.format("Duplicate key %s", u));}, mapSupplier);
	}
    
    public static <T,R extends T> Stream<R> filterByType(Stream<T> stream, Class<R> type) {
		return stream
			.filter(type::isInstance)
			.map(type::cast);
    }
    
    public static <T> String join(Stream<T> stream, CharSequence delimiter, Function<T,String> elementMap) {
    	return stream.map(elementMap).collect(joining(delimiter));
    }
}
