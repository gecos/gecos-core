package fr.irisa.cairn.gecos.core.testframework.utils;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import com.google.common.base.Strings;

import fr.irisa.cairn.gecos.model.c.generator.XtendCGenerator;
import fr.irisa.cairn.gecos.model.cdtfrontend.CDTFrontEnd;
import fr.irisa.cairn.gecos.model.cdtfrontend.exception.CDTParserProblemException;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.modules.AddIncludeDirToGecosProject;
import fr.irisa.cairn.gecos.model.modules.AddSourceToGecosProject;
import fr.irisa.cairn.gecos.model.modules.LoadGecosProject;
import fr.irisa.cairn.gecos.model.modules.SaveGecosProject;
import fr.irisa.r2d2.gecos.framework.utils.FileUtils;
import gecos.gecosproject.GecosProject;


public class GSProjectUtils {
	
	private GSProjectUtils() {}
	
	
	/**
	 * @deprecated used {@link GecosProject#copy()}
	 */
	@Deprecated
	public static GecosProject copy(GecosProject proj) {
//		return EcoreTools.<GecosProject>copy(proj);
		return proj.copy();
	}
	
	/**
	 * Shortcut to {@link #initGecosProject(String, Collection, Collection)}
	 * @param projectName
	 * @param sourceFiles
	 * @return a parsed {@link GecosProject} 
	 */
	public static GecosProject initGecosProject(final String projectName, final Path... sourceFiles) {
		return initGecosProject(projectName, Arrays.asList(sourceFiles), Collections.emptyList());
	}
	
	/**
	 * Shortcut to {@link #initGecosProject(String, Collection, Collection)}
	 * @param projectName
	 * @param sourceFiles
	 * 
	 * @return a parsed {@link GecosProject}
	 */
	public static GecosProject initGecosProject(final String projectName, final Path sourceFile, 
			final List<Path> includeDirs) {
		return initGecosProject(projectName, Collections.singletonList(sourceFile), includeDirs);
	}
	
	/**
	 * @param projectName
	 * @param sourceFiles
	 * @param includeDirs
	 * @return a {@link GecosProject} containing the specified sources and parse using
	 * 	{@link #parseGecosProject(GecosProject)}.
	 * @throws IllegalArgumentException if any of the parameters is null.
	 * @throws CDTParserProblemException if a parsing error occurred.
	 * @throws RuntimeException if failed to create/parse the project. 
	 */
	public static GecosProject initGecosProject(final String projectName, final Collection<Path> sourceFiles, final Collection<Path> includeDirs) {
		if(projectName == null || sourceFiles == null || includeDirs == null)
			throw new IllegalArgumentException("Parameters cannot be null!");
		
		GecosProject gProject = createGecosProject(projectName, sourceFiles, includeDirs);
		gProject = parseGecosProject(gProject);
		
		return gProject;
	}
	
	public static void setProjectSourceFiles(GecosProject gProject, final Collection<Path> sourceFiles) {
		if(sourceFiles != null) {
			gProject.getSources().clear();
			new AddSourceToGecosProject(gProject, sourceFiles.stream().map(Path::toFile).toArray(File[]::new)).compute();
		}
	}
	
	public static void setProjectIncDirs(GecosProject gProject, final Collection<Path> includeDirs) {
		if(includeDirs != null) {
			gProject.getIncludes().clear();
			new AddIncludeDirToGecosProject(gProject, includeDirs.stream().map(Path::toString).toArray(String[]::new)).compute();
		}
	}

	/**
	 * Try to parse @{code gProject} using {@link CDTFrontEnd} if it is 
	 * not already parsed, otherwise do nothing.
	 * 
	 * @param gProject {@link GecosProject} to be parsed.
	 * @return @{code gProject} after being parsed.
	 * @throws CDTParserProblemException if failed due to a parsing the project.
	 */
	public static GecosProject parseGecosProject(GecosProject gProject) {
		Objects.requireNonNull(gProject, "GecosProject cannot be null!");
		
		if(gProject.isParsed())
			return gProject;
		
		try {
			return new CDTFrontEnd(gProject).compute();
		} catch(Exception e) {
			Exception t = e;
			while(t instanceof Exception) {
				if(t instanceof CDTParserProblemException)
					throw (CDTParserProblemException) t;
				t = (Exception) t.getCause();
			}
			throw new RuntimeException(e);
		}
	}
	
	public static GecosProject createGecosProject(final String projectName, final Collection<Path> sourceFiles, final Collection<Path> includeDirs) {
		Path[] srcFiles = sourceFiles.stream()
			.filter(Objects::nonNull)
			.filter(Files::isRegularFile)
			.toArray(Path[]::new);
		Path[] incDirs = includeDirs.stream()
			.filter(Objects::nonNull)
			.filter(Files::isDirectory)
			.toArray(Path[]::new);
		
		try {
			GecosProject gProject = GecosUserCoreFactory.project(projectName, srcFiles, incDirs);
			projectNull(gProject);
			return gProject;
		} catch (Exception e) {
			throw new RuntimeException("Failed to create project", e);
		}
	}
	

	public static void generateCCode(GecosProject gecosProject, Path atDirectory) {
		generateCCode(gecosProject, atDirectory.toString());
	}
	
	public static void generateCCode(GecosProject gecosProject, String atDirectory) {
		emptyString("DirectoryGeneration" , atDirectory);
		projectNull(gecosProject);
		new XtendCGenerator(gecosProject, atDirectory).compute();
	}
	
	/**
	 * Modify {@code project} to match {@code by}.
	 * @param project {@link GecosProject} to be modified.
	 * @param by {@link GecosProject} to be copied.
	 */
	public static void replaceProject(GecosProject project, GecosProject by) {
		project.getSources().clear();
		project.getSources().addAll(by.getSources());
		
		project.getIncludes().clear();
		project.getIncludes().addAll(by.getIncludes());
		
		project.setName(by.getName());
		
		by.getMacros().forEach(project.getMacros()::add);
	}
	
	
	///////////////////////////////////////////////////////
	/**
	 * Same as saveIntermediaterepresentation but without asking for a explicit
	 * file name.
	 */
	public static void checkContainement(GecosProject gecosProject) {
		saveIntermediateRepresentation(gecosProject, FileUtils.generateDeleteOnExitFileName(".gecosproject"));
    }
    
	/**
	 * Same as saveIntermediaterepresentation but without asking for a explicit
	 * file name.
	 */
	public static void checkFileGeneration(GecosProject project) {
		String dirPath = FileUtils.generateDeleteOnExitDirName();
		generateCCode(project, dirPath);
		FileUtils.deleteRecursive(dirPath);
	}
    
	public static GecosProject loadIntermediateRepresentation(String path) {
		emptyString("File", path);
		GecosProject res = null;
		try {
			res = new LoadGecosProject(path).compute();
		} catch (Exception e) {
			throw new RuntimeException("Problem while loading Intermediate Representation.",e);
		}
		projectNull(res);
    	return res;
    }
    
	public static void saveIntermediateRepresentation(GecosProject gecosProject, String fileName) {
		emptyString("File", fileName);
		projectNull(gecosProject);
		
		try {
			new SaveGecosProject(gecosProject, fileName).compute();
		} catch (Exception e) {
			throw new RuntimeException("Problem while saving Intermediate Representation.",e);
		}
	}
	
	public static void projectNull(GecosProject gecosProject) {
		if(gecosProject == null)
			throw new RuntimeException("Gecos project should be initialized");
	}
	
	public static void emptyString(String message, String s) {
		if(Strings.isNullOrEmpty(s))
			throw new RuntimeException(message + " name can NOT be NULL or empty");
	}
	
	public static String makeUnique(String name, List<String> names) {
		String tmp = name;
		int i = 0;
		while (names.contains(tmp)) {
			tmp = name+"_"+(i++);
		}
		names.add(tmp);
		return tmp;
	}

}
