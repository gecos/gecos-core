package fr.irisa.cairn.gecos.model.tools.visitors.simpleforloops;

import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.SimpleForBlock;
import gecos.blocks.WhileBlock;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

public class RemoveSimpleForBlock {

	private EObject eObject;

	public RemoveSimpleForBlock(EObject eObject) {
		this.eObject = eObject;
	}

	public void compute() {
		List<SimpleForBlock> simpleForBlocks = EMFUtils.<SimpleForBlock>eAllContentsInstancesOf(eObject, SimpleForBlock.class);
		for(SimpleForBlock simpleForBlock : simpleForBlocks) {
			ForBlock forBlock;
			if (simpleForBlock.getScope().getSymbols().isEmpty())
				forBlock = GecosUserBlockFactory.For(simpleForBlock.getInitBlock(), simpleForBlock.getTestBlock(), simpleForBlock.getStepBlock(), simpleForBlock.getBodyBlock());
			else
				forBlock = GecosUserBlockFactory.ForC99(simpleForBlock.getInitBlock(), simpleForBlock.getTestBlock(), simpleForBlock.getStepBlock(), simpleForBlock.getBodyBlock(), simpleForBlock.getScope());
			forBlock.getAnnotations().addAll(simpleForBlock.getAnnotations());
			replace(simpleForBlock, forBlock);
		}
	}
	
	private void replace(Block oldBlock, Block newBlock) {
		Block parentBlock = oldBlock.getParent();
		if (parentBlock instanceof IfBlock) {
			IfBlock ifBlock = (IfBlock)parentBlock;
			if (ifBlock.getThenBlock() == oldBlock)
				ifBlock.setThenBlock(newBlock);
			else
				ifBlock.setElseBlock(newBlock);
		} else if (parentBlock instanceof CompositeBlock) {
			CompositeBlock compositeBlock = (CompositeBlock) parentBlock;
			int i = compositeBlock.getChildren().indexOf(oldBlock);
			compositeBlock.getChildren().add(i, newBlock);
			compositeBlock.removeBlock(oldBlock);
		} else if (parentBlock instanceof ForBlock) {
			ForBlock forBlock = (ForBlock) parentBlock;
			forBlock.setBodyBlock(newBlock);
		} else if (parentBlock instanceof WhileBlock) {
			WhileBlock whileBlock = (WhileBlock) parentBlock;
			whileBlock.setBodyBlock(newBlock);
		} else
			System.err.println("Maxime N. : " + "NotSupported");
	}
}
