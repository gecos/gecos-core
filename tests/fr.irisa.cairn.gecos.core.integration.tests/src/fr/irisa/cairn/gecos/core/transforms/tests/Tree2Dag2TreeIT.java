package fr.irisa.cairn.gecos.core.transforms.tests;

import org.junit.Test;

import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.core.integration.tests.utils.DotAppDirData;
import fr.irisa.cairn.gecos.core.integration.tests.utils.DotAppDirDataTemplate;
import fr.irisa.cairn.gecos.model.transforms.dag.DAGToTreeConvertion;
import fr.irisa.cairn.gecos.model.transforms.dag.TreeToDAGConvertion;
import fr.irisa.cairn.gecos.testframework.data.ICxxProjectData;
import fr.irisa.cairn.gecos.testframework.dataprovider.DataFromPathProvider;
import fr.irisa.cairn.gecos.testframework.dataprovider.ResourcesLocation;

/**
 * Tests {@link TreeToDAGConvertion} and {@link DAGToTreeConvertion}.
 * 
 * @author aelmouss
 */
public class Tree2Dag2TreeIT extends DotAppDirDataTemplate {
	
	@Test
	@UseDataProvider(location=DataFromPathProvider.class, value=DataFromPathProvider.PROVIDER_NAME)
//	@ResourcesLocation("resources/src-c/dot-app-data/misc")
	@ResourcesLocation(value = "resources/src-c/dot-app-data/idfix", dataClasses = DotAppDirData.class) 
	@ResourcesLocation("resources/src-c/block-outliner")
	public void testDAG(ICxxProjectData d) {
		runTest(d, v -> {
			new TreeToDAGConvertion(v.getProject()).compute();
			new DAGToTreeConvertion(v.getProject()).compute();
		});
	}
	
}
