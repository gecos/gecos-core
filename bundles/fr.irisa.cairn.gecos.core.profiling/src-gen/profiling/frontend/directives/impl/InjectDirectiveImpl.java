/**
 */
package profiling.frontend.directives.impl;

import fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport;

import fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport.NativeFunction;

import fr.irisa.cairn.gecos.core.profiling.frontend.InvalidDirective;

import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;

import gecos.core.ProcedureSet;
import gecos.core.Symbol;

import gecos.instrs.CallInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.SetInstruction;

import gecos.types.Type;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Objects;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions;

import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.ExclusiveRange;

import profiling.frontend.directives.DirectiveName;
import profiling.frontend.directives.DirectivesFactory;
import profiling.frontend.directives.DirectivesPackage;
import profiling.frontend.directives.InjectDirective;
import profiling.frontend.directives.InjectorSourceDirective;
import profiling.frontend.directives.RandomUniformDirective;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Inject Directive</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link profiling.frontend.directives.impl.InjectDirectiveImpl#getNAME <em>NAME</em>}</li>
 *   <li>{@link profiling.frontend.directives.impl.InjectDirectiveImpl#getAddress <em>Address</em>}</li>
 *   <li>{@link profiling.frontend.directives.impl.InjectDirectiveImpl#getSourceDirective <em>Source Directive</em>}</li>
 *   <li>{@link profiling.frontend.directives.impl.InjectDirectiveImpl#getSizesOutermostFirst <em>Sizes Outermost First</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InjectDirectiveImpl extends DirectiveImpl implements InjectDirective {
	/**
	 * The default value of the '{@link #getNAME() <em>NAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNAME()
	 * @generated
	 * @ordered
	 */
	protected static final DirectiveName NAME_EDEFAULT = DirectiveName.INJECT;

	/**
	 * The cached value of the '{@link #getNAME() <em>NAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNAME()
	 * @generated
	 * @ordered
	 */
	protected DirectiveName name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAddress() <em>Address</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAddress()
	 * @generated
	 * @ordered
	 */
	protected Instruction address;

	/**
	 * The cached value of the '{@link #getSourceDirective() <em>Source Directive</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceDirective()
	 * @generated
	 * @ordered
	 */
	protected InjectorSourceDirective sourceDirective;

	/**
	 * The cached value of the '{@link #getSizesOutermostFirst() <em>Sizes Outermost First</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSizesOutermostFirst()
	 * @generated
	 * @ordered
	 */
	protected EList<Instruction> sizesOutermostFirst;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InjectDirectiveImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DirectivesPackage.Literals.INJECT_DIRECTIVE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DirectiveName getNAME() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNAME(DirectiveName newNAME) {
		DirectiveName oldNAME = name;
		name = newNAME == null ? NAME_EDEFAULT : newNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DirectivesPackage.INJECT_DIRECTIVE__NAME, oldNAME, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getAddress() {
		if (address != null && address.eIsProxy()) {
			InternalEObject oldAddress = (InternalEObject)address;
			address = (Instruction)eResolveProxy(oldAddress);
			if (address != oldAddress) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DirectivesPackage.INJECT_DIRECTIVE__ADDRESS, oldAddress, address));
			}
		}
		return address;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction basicGetAddress() {
		return address;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAddress(Instruction newAddress) {
		Instruction oldAddress = address;
		address = newAddress;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DirectivesPackage.INJECT_DIRECTIVE__ADDRESS, oldAddress, address));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InjectorSourceDirective getSourceDirective() {
		return sourceDirective;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSourceDirective(InjectorSourceDirective newSourceDirective, NotificationChain msgs) {
		InjectorSourceDirective oldSourceDirective = sourceDirective;
		sourceDirective = newSourceDirective;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DirectivesPackage.INJECT_DIRECTIVE__SOURCE_DIRECTIVE, oldSourceDirective, newSourceDirective);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceDirective(InjectorSourceDirective newSourceDirective) {
		if (newSourceDirective != sourceDirective) {
			NotificationChain msgs = null;
			if (sourceDirective != null)
				msgs = ((InternalEObject)sourceDirective).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DirectivesPackage.INJECT_DIRECTIVE__SOURCE_DIRECTIVE, null, msgs);
			if (newSourceDirective != null)
				msgs = ((InternalEObject)newSourceDirective).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DirectivesPackage.INJECT_DIRECTIVE__SOURCE_DIRECTIVE, null, msgs);
			msgs = basicSetSourceDirective(newSourceDirective, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DirectivesPackage.INJECT_DIRECTIVE__SOURCE_DIRECTIVE, newSourceDirective, newSourceDirective));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instruction> getSizesOutermostFirst() {
		if (sizesOutermostFirst == null) {
			sizesOutermostFirst = new EObjectResolvingEList<Instruction>(Instruction.class, this, DirectivesPackage.INJECT_DIRECTIVE__SIZES_OUTERMOST_FIRST);
		}
		return sizesOutermostFirst;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void parse() {
		final EList<Instruction> args = this.getCall().getArgs();
		int _size = args.size();
		boolean _lessThan = (_size < 2);
		if (_lessThan) {
			String _printLocation = this.printLocation();
			String _plus = (_printLocation + "\nMust have at least 2 arguments (address, injection source)!");
			throw new InvalidDirective(_plus);
		}
		this.setAddress(Objects.<Instruction>requireNonNull(args.get(0)));
		CallInstruction _xtrycatchfinallyexpression = null;
		try {
			Instruction _get = args.get(1);
			_xtrycatchfinallyexpression = ((CallInstruction) _get);
		}
		catch (final Throwable _t) {
			if (_t instanceof Exception) {
				String _printLocation_1 = this.printLocation();
				String _plus_1 = (_printLocation_1 + "\nMust have an injector directive as second argument \n\t\t\t\t($from_file(filePath) | $from_var(var) | $random_uniform(min,max) | $random_normal(mean,dev))");
				throw new InvalidDirective(_plus_1);
			}
			else {
				throw Exceptions.sneakyThrow(_t);
			}
		}
		final CallInstruction source = _xtrycatchfinallyexpression;
		final String name = source.getProcedureSymbol().getName().toLowerCase();
		InjectorSourceDirective _xifexpression = null;
		String _literal = DirectiveName.FROM_FILE.getLiteral();
		boolean _equals = com.google.common.base.Objects.equal(name, _literal);
		if (_equals) {
			_xifexpression = DirectivesFactory.eINSTANCE.createFromFileDirective();
		}
		else {
			InjectorSourceDirective _xifexpression_1 = null;
			String _literal_1 = DirectiveName.FROM_VAR.getLiteral();
			boolean _equals_1 = com.google.common.base.Objects.equal(name, _literal_1);
			if (_equals_1) {
				_xifexpression_1 = DirectivesFactory.eINSTANCE.createFromVariableDirective();
			}
			else {
				RandomUniformDirective _xifexpression_2 = null;
				String _literal_2 = DirectiveName.RANDOM_UNIFORM.getLiteral();
				boolean _equals_2 = com.google.common.base.Objects.equal(name, _literal_2);
				if (_equals_2) {
					_xifexpression_2 = DirectivesFactory.eINSTANCE.createRandomUniformDirective();
				}
				else {
					RandomUniformDirective _xifexpression_3 = null;
					String _literal_3 = DirectiveName.RANDOM_NORMAL.getLiteral();
					boolean _equals_3 = com.google.common.base.Objects.equal(name, _literal_3);
					if (_equals_3) {
						_xifexpression_3 = DirectivesFactory.eINSTANCE.createRandomUniformDirective();
					}
					else {
						String _printLocation_2 = this.printLocation();
						String _plus_2 = (_printLocation_2 + "\nMust have an injector directive as second argument \n\t\t\t\t($from_file(filePath) | $from_var(var) | $random_uniform(min,max) | $random_normal(mean,dev))");
						throw new InvalidDirective(_plus_2);
					}
					_xifexpression_2 = _xifexpression_3;
				}
				_xifexpression_1 = _xifexpression_2;
			}
			_xifexpression = _xifexpression_1;
		}
		this.setSourceDirective(_xifexpression);
		InjectorSourceDirective _sourceDirective = this.getSourceDirective();
		_sourceDirective.setCall(source);
		this.getSourceDirective().parse();
		Type _type = this.getAddress().getType();
		final TypeAnalyzer ta = new TypeAnalyzer(_type, true);
		int _totalNbDims = ta.getTotalNbDims();
		ExclusiveRange _doubleDotLessThan = new ExclusiveRange(0, _totalNbDims, true);
		for (final Integer dim : _doubleDotLessThan) {
			{
				Instruction _xifexpression_4 = null;
				int _size_1 = args.size();
				boolean _greaterThan = (_size_1 > ((dim).intValue() + 2));
				if (_greaterThan) {
					_xifexpression_4 = args.get((2 + (dim).intValue()));
				}
				else {
					_xifexpression_4 = ta.tryGetDimSize((dim).intValue());
				}
				final Instruction sz = _xifexpression_4;
				if ((sz == null)) {
					String _printLocation_3 = this.printLocation();
					String _plus_3 = (_printLocation_3 + "\nMust specify a size for dimension \'");
					String _plus_4 = (_plus_3 + dim);
					String _plus_5 = (_plus_4 + "\' which couldn\'t be determined automatically!");
					throw new InvalidDirective(_plus_5);
				}
				this.getSizesOutermostFirst().add(sz);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block createCodeBlock() {
		final ProcedureSet ps = this.getCall().getContainingProcedureSet();
		final CompositeBlock cb = GecosUserBlockFactory.CompositeBlock();
		GecosUserAnnotationFactory.comment(cb, this.getCall().toString());
		GecosUserTypeFactory.setScope(cb.getScope());
		cb.addChildren(this.getSourceDirective().createCodeBlock());
		final BasicBlock innermostBody = GecosUserBlockFactory.BBlock();
		Block currentBlock = ((Block) innermostBody);
		final LinkedList<Instruction> indexesOutermostFirst = new LinkedList<Instruction>();
		EList<Instruction> _reverseView = XcoreEListExtensions.<Instruction>reverseView(this.getSizesOutermostFirst());
		for (final Instruction dimSize : _reverseView) {
			{
				final Symbol index = GecosUserCoreFactory.symbol("i", GecosUserTypeFactory.INT(), cb.getScope());
				cb.getScope().makeUnique(index);
				indexesOutermostFirst.addFirst(GecosUserInstructionFactory.symbref(index));
				currentBlock = GecosUserBlockFactory.For(index, 0, dimSize.copy(), 1, currentBlock);
			}
		}
		Instruction _xifexpression = null;
		boolean _isEmpty = indexesOutermostFirst.isEmpty();
		if (_isEmpty) {
			_xifexpression = this.getAddress();
		}
		else {
			_xifexpression = GecosUserInstructionFactory.array(this.getAddress(), indexesOutermostFirst);
		}
		final Instruction dest = _xifexpression;
		final CallInstruction value = GecosUserInstructionFactory.call(NativeProfilingEngineSupport.getNativeProc(ps, NativeFunction.INJECT_VALUE));
		final SetInstruction injectValue = GecosUserInstructionFactory.set(dest, value);
		innermostBody.addInstruction(injectValue);
		cb.addChildren(currentBlock);
		return cb;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DirectivesPackage.INJECT_DIRECTIVE__SOURCE_DIRECTIVE:
				return basicSetSourceDirective(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DirectivesPackage.INJECT_DIRECTIVE__NAME:
				return getNAME();
			case DirectivesPackage.INJECT_DIRECTIVE__ADDRESS:
				if (resolve) return getAddress();
				return basicGetAddress();
			case DirectivesPackage.INJECT_DIRECTIVE__SOURCE_DIRECTIVE:
				return getSourceDirective();
			case DirectivesPackage.INJECT_DIRECTIVE__SIZES_OUTERMOST_FIRST:
				return getSizesOutermostFirst();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DirectivesPackage.INJECT_DIRECTIVE__NAME:
				setNAME((DirectiveName)newValue);
				return;
			case DirectivesPackage.INJECT_DIRECTIVE__ADDRESS:
				setAddress((Instruction)newValue);
				return;
			case DirectivesPackage.INJECT_DIRECTIVE__SOURCE_DIRECTIVE:
				setSourceDirective((InjectorSourceDirective)newValue);
				return;
			case DirectivesPackage.INJECT_DIRECTIVE__SIZES_OUTERMOST_FIRST:
				getSizesOutermostFirst().clear();
				getSizesOutermostFirst().addAll((Collection<? extends Instruction>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DirectivesPackage.INJECT_DIRECTIVE__NAME:
				setNAME(NAME_EDEFAULT);
				return;
			case DirectivesPackage.INJECT_DIRECTIVE__ADDRESS:
				setAddress((Instruction)null);
				return;
			case DirectivesPackage.INJECT_DIRECTIVE__SOURCE_DIRECTIVE:
				setSourceDirective((InjectorSourceDirective)null);
				return;
			case DirectivesPackage.INJECT_DIRECTIVE__SIZES_OUTERMOST_FIRST:
				getSizesOutermostFirst().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DirectivesPackage.INJECT_DIRECTIVE__NAME:
				return name != NAME_EDEFAULT;
			case DirectivesPackage.INJECT_DIRECTIVE__ADDRESS:
				return address != null;
			case DirectivesPackage.INJECT_DIRECTIVE__SOURCE_DIRECTIVE:
				return sourceDirective != null;
			case DirectivesPackage.INJECT_DIRECTIVE__SIZES_OUTERMOST_FIRST:
				return sizesOutermostFirst != null && !sizesOutermostFirst.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (NAME: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //InjectDirectiveImpl
