/**
 */
package gecos.instrs;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Arithmetic Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.ArithmeticInstruction#getOperator <em>Operator</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getArithmeticInstruction()
 * @model
 * @generated
 */
public interface ArithmeticInstruction extends GenericInstruction {
	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The default value is <code>"add"</code>.
	 * The literals are from the enumeration {@link gecos.instrs.ArithmeticOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see gecos.instrs.ArithmeticOperator
	 * @see #setOperator(ArithmeticOperator)
	 * @see gecos.instrs.InstrsPackage#getArithmeticInstruction_Operator()
	 * @model default="add" unique="false"
	 * @generated
	 */
	ArithmeticOperator getOperator();

	/**
	 * Sets the value of the '{@link gecos.instrs.ArithmeticInstruction#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see gecos.instrs.ArithmeticOperator
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(ArithmeticOperator value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.ArithmeticInstruction%&gt;))\n{\n\treturn (super.isSame(instr) &amp;&amp; (this.getOperator().getValue() == ((&lt;%gecos.instrs.ArithmeticInstruction%&gt;)instr).getOperator().getValue()));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // ArithmeticInstruction
