package fr.irisa.cairn.gecos.model.analysis.observers.control;

import gecos.core.Procedure;

public class InstallControlFlowObserver {
	
	Procedure proc;
	ControlFlowObserver observer;
	
	public InstallControlFlowObserver(Procedure proc, ControlFlowObserver observer) {
		this.proc=proc;
		this.observer=observer;
	}

	public void compute() {
		proc.eAdapters().add(observer);
	}
}
