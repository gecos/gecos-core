package fr.irisa.cairn.gecos.model.analysis.types;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import com.google.common.base.Strings;

import gecos.instrs.Instruction;
import gecos.types.AliasType;
import gecos.types.Type;


/**
 * Provide an API to easily analyze types.
 * 
 * It splits a type into multiple levels: <ul>
 *    <li> {@link BaseLevel}: represents the outermost non-Pointer and non-Array type. </li>
 *    <li> List of outermost levels; each can be either a: </li> <ul>
 *        <li> {@link PointerLevel}: represents a sequence of PtrType along with their const qualifiers. </li>
 *        <li> {@link ArrayLevel}: represents a sequence of ArrayType.</li></ul>
 * </ul>   
 * Example: const static int32_t * * const * A[10][20] will be splited into 3 levels: <ul>
 *    <li> base level: const static int32_t aliasof int
 *    <li> level 1: 3 dimensional pointer with only the second one as const
 *    <li> level 2: 2 dimensional array with sizes [10, 20] 
 * </ul>
 * @author aelmouss
 */
public class TypeAnalyzer {
	
	private Type type;
	private List<TypeLevel> levels; //Levels in outermost first order (i.e. last level is the base level)
	private boolean skipAliases; // if true skip all aliases. Otherwise stop after first AliasType is encountered which is represented as Base level.

	
	/**
	 * @param type
	 * @param skipAliases if true skip all aliases. Otherwise stop after first AliasType is 
	 *  encountered which is represented as Base level.
	 */
	public TypeAnalyzer(Type type, boolean skipAliases) {
		if(type == null)
			throw new InvalidParameterException("type cannot be null");
		this.type = type;
		this.skipAliases = skipAliases;
	}
	
	public TypeAnalyzer(Type type) {
		this(type, false);
	}
	
	/**
	 * @return unmodifiable view of levels in outermost first order 
	 * (i.e. last level is the base level)
	 */
	public List<TypeLevel> getLevels() {
		if(levels == null)
			buildLevels();
		
		return Collections.unmodifiableList(levels);
	}
	
	private void buildLevels() {
		levels = new ArrayList<>();
		Type remainingType = this.type;
		while(true) {
			ArrayLevel arrayLevel = ArrayLevel.constructFrom(remainingType, this);
			if(arrayLevel != null) {
				levels.add(arrayLevel);
				remainingType = arrayLevel.getRemainingType();
			}
			
			PointerLevel ptrLevel = PointerLevel.constructFrom(remainingType, this);
			if(ptrLevel != null) {
				levels.add(ptrLevel);
				remainingType = ptrLevel.getRemainingType();
			}
			
			if(arrayLevel == null && ptrLevel == null)
				break;
		}
		
		BaseLevel baseLevel = BaseLevel.constructFrom(remainingType, this);
		if(baseLevel == null)
			throw new RuntimeException("Failed to analyse type: " + type + " at: " + remainingType);
		levels.add(baseLevel);
	}
	
	Type skipAliases(Type type) {
		if(skipAliases) {
			while((type instanceof AliasType))
				type = ((AliasType) type).getAlias();
		}
		return type;
	}
	
	//////
	
	/**
	 * Shorthand for {@link #getLevels()}.get(0)
	 * @return the outermost level
	 */
	public TypeLevel getOutermostLevel() {
		return getLevels().get(0);
	}
	
	/**
	 * @return a stream of levels (in outermost-first order) excluding base level.
	 */
	public Stream<AbstractMultiDimLevel> getOutermostLevels() {
		List<TypeLevel> list = getLevels();
		return IntStream.range(0, list.size()-1)
			.mapToObj(list::get)
			.filter(AbstractMultiDimLevel.class::isInstance)
			.map(AbstractMultiDimLevel.class::cast);
	}
	
	public BaseLevel getBaseLevel() {
		return (BaseLevel) getLevels().get(getLevels().size()-1);
	}
	
	/**
	 * Reproduce base level quantifiers (const and static) on type <code>baseType</code>
	 * @param baseType
	 * @return reproduced type
	 */
	public Type revertBaseLevelOn(Type baseType) {
		return getBaseLevel().revertOn(baseType);
	}
	
	public Type revertLevelOn(int levelIdx, Type type) {
		if(levelIdx >= getLevels().size())
			throw new InvalidParameterException("level is not found!" + levelIdx + ", max is " + getLevels().size());
		
		TypeLevel level = getLevels().get(levelIdx);
		return level.revertOn(type);
	}
	
	/**
	 * Reproduce this type levels on type <code>baseType</code>
	 * @param baseType
	 * @return reproduced type
	 */
	public Type revertAllLevelsOn(Type baseType) {
		for(int i = getLevels().size()-1; i>=0; i--)
			baseType = revertLevelOn(i, baseType);
		
		return baseType;
	}
	
	/**
	 * @return true if outermost level is ArrayType, false otherwise.
	 */
	public boolean isArray() {
		return getOutermostLevel() instanceof ArrayLevel;
	}
	
	/**
	 * @return true if outermost level is PointerType, false otherwise.
	 */
	public boolean isPointer() {
		return getOutermostLevel() instanceof PointerLevel;
	}
	
	/**
	 * @return true if has no Array/Pointer levels (BaseLevel Only), false otherwise.
	 */
	public boolean isBase() {
		return getOutermostLevel() instanceof BaseLevel;
	}

	/**
	 * @return the number of non-{@link BaseLevel} {@link Level}s (i.e. getLevels().size() -1);
	 */
	public int getNbLevels() {
		return getLevels().size() -1;
	}

	/**
	 * @param levelIdx type level index (levels are ordered outermost first)
	 * @return {@link TypeLevel} at index {@code levelIdx}
	 * @throws InvalidParameterException if {@code levelIdx} is out of range.
	 */
	public TypeLevel getLevel(int levelIdx) {
		if(levelIdx < 0 || levelIdx >= getLevels().size())
			throw new InvalidParameterException("level is not found!" + levelIdx + ", nb of levels is " + getLevels().size());
		
		return getLevels().get(levelIdx);
	}
	
	/**
	 * Considering all outer dimensions (array and pointer) in outermost-first order,
	 * find the level that represents the specified {@code dim} (0 begin outermost).
	 * @param dim 0 represent the outermost dimension.
	 * @return the level containing the specified dim if found.
	 * @throws InvalidParameterException if {@code dim} is out of range.
	 */
	public AbstractMultiDimLevel getLevelContainingDim(int dim) {
		if(dim < 0)
			throw new InvalidParameterException("dim cannot be negative");
		
		int n = 0;
		for(AbstractMultiDimLevel l : getOutermostLevels().toArray(AbstractMultiDimLevel[]::new)) {
			n += l.getNDims();
			if(dim < n)
				return l;
		}
		throw new InvalidParameterException("the specified dim is out of bound! " + dim); //XXX
	}
	
	/**
	 * Try to get the level at <code>levelIdx</code> as {@link PointerLevel}.
	 * @param levelIdx type level index (levels are ordered outermost first)
	 * @return PointerLevel if succeed, null otherwise.
	 * @throws InvalidParameterException if {@code levelIdx} is out of range.
	 */
	public PointerLevel tryGetPointerLevel(int levelIdx) {
		return tryGetLevelAs(levelIdx, PointerLevel.class);
	}
	
	/**
	 * Try to get the level at <code>levelIdx</code> as {@link ArrayLevel}.
  	 * @param levelIdx type level index (levels are ordered outermost first)
	 * @return ArrayLevel if succeed, null otherwise.
	 * @throws InvalidParameterException if {@code levelIdx} is out of range.
	 */
	public ArrayLevel tryGetArrayLevel(int levelIdx) {
		return tryGetLevelAs(levelIdx, ArrayLevel.class);
	}
	
	/**
	 * Try to get the level at <code>levelIdx</code> as {@link AbstractMultiDimLevel}.
  	 * @param levelIdx type level index (levels are ordered outermost first)
	 * @return AbstractMultiDimLevel if succeed, null otherwise.
	 * @throws InvalidParameterException if {@code levelIdx} is out of range.
	 */
	public AbstractMultiDimLevel tryGetAbstractMultiDimLevel(int levelIdx) {
		return tryGetLevelAs(levelIdx, AbstractMultiDimLevel.class);
	}
	
	/**
	 * Try to get the level at <code>levelIdx</code> as the type specified by 
	 * <code>levelCls</code>.
  	 * @param levelIdx type level index (levels are ordered outermost first)
	 * @return the level if succeed, null otherwise.
	 * @throws InvalidParameterException if {@code levelIdx} is out of range.
	 */
	public <T extends TypeLevel> T tryGetLevelAs(int levelIdx, Class<T> levelCls) {
		TypeLevel level = getLevel(levelIdx);
		if(levelCls.isAssignableFrom(level.getClass()))
			return levelCls.cast(level);
		return null;
	}
	
	/**
	 * Sum the number of dimensions in all outer ({@link AbstractMultiDimLevel}) levels.
	 * If this only consists of a {@link BaseLevel}, return 0.
	 * @return the total number (sum) of outer dimensions, 0 if isBaseLevel. 
	 */
	public int getTotalNbDims() {
		return getTotalNbDims(getOutermostLevels());
	}

	public static int getTotalNbDims(Stream<AbstractMultiDimLevel> stream) {
		return stream
			.mapToInt(AbstractMultiDimLevel::getNDims)
			.sum();
	}
	
	public int indexOf(TypeLevel lvl) {
		return levels.indexOf(lvl);
	}

	/**
	 * Considering all outer dimensions (array and pointer) in outermost-first order,
	 * if the specified {@code dim} (0 begin outermost) has a known size return it,
	 * otherwise return {@code null}.
	 * @param dim 0 represent the outermost dimension.
	 * @return the size of the specified dim if found, {@code null} otherwise.
	 * @throws InvalidParameterException if {@code dim} is out of range.
	 */
	public Instruction tryGetDimSize(int dim) {
		AbstractMultiDimLevel lvl = getLevelContainingDim(dim);
		int relatifDim = dim - getTotalNbDims(getOutermostLevels().limit(indexOf(lvl)));
		
		if(lvl instanceof ArrayLevel)
			return ((ArrayLevel) lvl).getSize(relatifDim);
		return null;
	}
	
	/**
	 * Considering all outer dimensions (array and pointer) in outermost-first order,
	 * if the specified {@code dim} (0 begin outermost) has a known immediate size, 
	 * it returns it, otherwise return {@code null}.
	 * @param dim 0 represent the outermost dimension.
	 * @return the size of the specified dim if found, {@code null} otherwise.
	 * @throws InvalidParameterException if {@code dim} is out of range.
	 */
	public Long tryGetDimSizeValue(int dim) {
		AbstractMultiDimLevel lvl = getLevelContainingDim(dim);
		int relatifDim = dim - getTotalNbDims(getOutermostLevels().limit(indexOf(lvl)));
		
		if(lvl instanceof ArrayLevel)
			return ((ArrayLevel) lvl).tryGetSize(relatifDim);
		return null;
	}
	
	
	/**
	 * Shorthand for {@link #getBaseLevel()}.{@link #getBase()}
	 */
	public Type getBase() {
		return getBaseLevel().getBase();
	}
	
	public boolean isStatic() {
		return getBaseLevel().isStatic();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < getLevels().size(); ++i)
			sb.append(Strings.repeat("  ", i)).append(getLevels().get(i)).append("\n");
		return sb.toString();
	}
	
	/**
	 * Compare {@code other} to {@code this} ignoring type qualifiers (const, static ...).
	 */
	public boolean isSimilar(TypeAnalyzer other) {
		if(other.getNbLevels() != getNbLevels())
			return false;
		
		for(int i = 0; i < getLevels().size(); ++i)
			if(!getLevels().get(i).isSimilar(other.getLevels().get(i)))
				return false;
		
		return true;
	}

	/**
	 * Exact comparison of all levels between {@code this} and {@code obj}
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if(!(obj instanceof TypeAnalyzer))
			return false;
		
		TypeAnalyzer other = (TypeAnalyzer)obj;
		if(other.getNbLevels() != getNbLevels())
			return false;
		
		for(int i = 0; i < getLevels().size(); ++i)
			if(!getLevels().get(i).equals(other.getLevels().get(i)))
				return false;
		
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getLevels() == null) ? 0 : getLevels().hashCode());
		return result;
	}
	
}