package fr.irisa.cairn.gecos.model.tools.utils;

import gecos.annotations.IASTAnnotation;
import gecos.annotations.IAnnotation;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;

public class GecosCopier extends Copier {

	private static final long serialVersionUID = 1L;

	@Override
	public EObject copy(EObject eObject) {
		EObject copy = super.copy(eObject);
		
		// keep link with original IASTNode from CDT
		if (eObject instanceof IAnnotation) {
			copy(eObject, copy);
		}
		if (eObject instanceof IASTAnnotation) {
			((IASTAnnotation)copy).setIastNode(((IASTAnnotation) eObject).getIastNode());
		}
		return copy;
	}
	
	public void copy(EObject orig, EObject newAnnot) {
		for (Adapter adapter : orig.eAdapters()) {
			newAnnot.eAdapters().add(adapter);
		}
	}
}
