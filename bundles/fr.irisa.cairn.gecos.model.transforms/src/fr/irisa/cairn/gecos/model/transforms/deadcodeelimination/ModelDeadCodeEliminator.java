package fr.irisa.cairn.gecos.model.transforms.deadcodeelimination;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.gecos.model.tools.controlflow.BuildControlFlow;
import fr.irisa.cairn.gecos.model.tools.controlflow.ClearControlFlow;
import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch;
import fr.irisa.cairn.gecos.model.tools.utils.BlocksEraser;
import fr.irisa.cairn.gecos.model.tools.utils.BlocksLabelFinder;
import fr.irisa.cairn.gecos.model.transforms.constants.ModelConstantPropagatorIterative;
import gecos.blocks.BasicBlock;
import gecos.blocks.ControlEdge;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.SwitchBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;

/**
 * This visitor tries to remove dead code. For an optimal process, use a 
 * constant evaluator before. <br/>
 * The process does not care about the control flow. But it returns a
 * control flowed IR.
 * @author amorvan
 *
 */
public class ModelDeadCodeEliminator  {

	private final static boolean debug = false;
	private final static String PREFIX = "[CODE_Eliminator] ";
	private final static void debug(Object o) { if (debug) System.out.print(o.toString()); }
	private final static void debugln(Object o) { debug(o.toString()+"\n"); }

	private List<Procedure> lProc;
	Procedure currentProc;

	public ModelDeadCodeEliminator(Procedure proc) {
		(lProc = new ArrayList<Procedure>(1)).add(proc);
	}

	public ModelDeadCodeEliminator(ProcedureSet ps) {
		lProc = ps.listProcedures();
	}

	public ModelDeadCodeEliminator(GecosProject project) {
		lProc = new ArrayList<Procedure>();
		for (GecosSourceFile f : project.getSources()) {
			if (f.getModel() == null) throw new RuntimeException("Error : project is not fully built.");
			for (Procedure p : f.getModel().listProcedures()) {
				lProc.add(p);
			}
		}
	}

	public void compute() {
		for (Procedure proc : lProc) {
			//clean ControlFlow : remove and rebuild

			ClearControlFlow mccf = new ClearControlFlow(proc);
			mccf.compute();

			currentProc = proc;
			
			///////
			ModelConstantPropagatorIterative constPropagator = new ModelConstantPropagatorIterative(proc);
			while (constPropagator.computeIterative());
			//////////
			
			//doSwitch(proc.getBody());

			//*
			BuildControlFlow mbcf = new BuildControlFlow(proc);
			mbcf.compute();

			ModelDeadCodeEliminatorV2 v2;
			do {
				v2 = new ModelDeadCodeEliminatorV2();
				v2.doSwitch(proc.getBody());
			} while (v2.changed);


			mccf = new ClearControlFlow(proc);
			mccf.compute();
			//*/
		}
	}

	private static class ModelDeadCodeEliminatorV2 extends BlocksDefaultSwitch<Object> {

		public boolean changed;

		public ModelDeadCodeEliminatorV2() {
			changed = false;
		}

		@Override
		public Object caseBasicBlock(BasicBlock b) {
			if (b.getParent() == null) return null;
			if (b.getParent().getParent() == null) return null;
			if (b.getInEdges().size() == 0) {
				for (ControlEdge e : b.getOutEdges().toArray(new ControlEdge[b.getOutEdges().size()])) {
					e.setTo(null);
					e.setFrom(null);
				}
				b.getParent().removeBlock(b);
			}
			return null;
		}

		@Override
		public Object caseForBlock(ForBlock b) {
			if (b.getInitBlock().getInEdges().size() == 0) {
				BlocksLabelFinder blf = new BlocksLabelFinder();
				blf.doSwitch(b.getBodyBlock());
				if (blf.containsLabels()) {
					for (BasicBlock bb : blf.getLabels()) {
						if (bb.getInEdges().size() > 1) return doSwitch(b.getBodyBlock());
					}
				}
				ClearControlFlow.clearCF(b);
				b.getParent().removeBlock(b);
				(new BlocksEraser()).doSwitch(b);
			}
			return null;
		}

		@Override
		public Object caseIfBlock(IfBlock b) {

			if (((BasicBlock)b.getTestBlock()).getInEdges().size() == 0) {

				BlocksLabelFinder blf = new BlocksLabelFinder();
				blf.doSwitch(b.getThenBlock());
				if (b.getElseBlock() != null) blf.doSwitch(b.getElseBlock());

				if (blf.containsLabels()) {
					for (BasicBlock bb : blf.getLabels()) {
						if (bb.getInEdges().size() > 1) {
							doSwitch(b.getThenBlock());
							if (b.getElseBlock() != null) doSwitch(b.getElseBlock());
							return null;
						}
					}
				}
				ClearControlFlow.clearCF(b);
				b.getParent().removeBlock(b);
				(new BlocksEraser()).doSwitch(b);
			}

			return null;
		}

		@Override
		public Object caseDoWhileBlock(DoWhileBlock b) {
			// TODO Auto-generated method stub
			return super.caseDoWhileBlock(b);
		}

		@Override
		public Object caseSwitchBlock(SwitchBlock b) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Object caseWhileBlock(WhileBlock b) {
			// TODO Auto-generated method stub
			return super.caseWhileBlock(b);
		}


	}
}