/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.transforms.blocks.simplifier;

import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch;
import fr.irisa.cairn.gecos.model.tools.validation.GecosModelValidator;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ForBlock;
import gecos.core.Procedure;

public class BlockSimplifier extends BlocksDefaultSwitch<Object> {

	private static boolean VERBOSE = false;
	private static boolean DEFENSIVE= false;

	private void debug(String mess) {
		if (VERBOSE)
			System.out.println(prefix + mess);
	}

	private Procedure proc;
	private String prefix = "";

	private BlockSimplifier() {
	};

	public BlockSimplifier(Procedure proc) {
		this.proc = proc;
	}

	public static void simplify(Block b) {
		(new BlockSimplifier()).doSwitch(b);
	}

	public void compute() {
		debug("warning: should extract value initialisation with ModelTransformValueInitialisation transform (case of merge scope)");
		doSwitch(proc.getBody());
		// FIXME : check whether validation is actually needed at this point.
		if(DEFENSIVE) {
			GecosModelValidator validator = new GecosModelValidator();
			validator.check(proc.eContainer());
		}

	}

	@Override
	public Object caseForBlock(ForBlock b) {
		return super.caseForBlock(b);
	}

	@Override
	public Object caseCompositeBlock(CompositeBlock currentBlock) {
		debug("Visiting " + currentBlock);
		indent();
		super.caseCompositeBlock(currentBlock);
		// don't merge the root composite in order not to loose
		// start and end node

		replaceCompositeByBasic(currentBlock); // check if children CB can be replace by
									// one
		if (currentBlock.getParent() != null) {
			debug("has parent");
			if (currentBlock.mergeChildren()) {
				debug("Children merged");
				Block simplified = currentBlock.simplifyBlock();
				debug("Simplifying " + currentBlock + " into " + simplified + "");
				if (simplified != currentBlock) {
					// //map.put(b,n);
					debug("replacing " + currentBlock + " by " + simplified + "");
					currentBlock.getParent().replace(currentBlock, simplified);
					return null;
				}
			}
			// b.getParent().mergeChildComposite(b);
		}
		deindent();
		// GecosModelValidator validator = new GecosModelValidator();
		// validator.check(b);
		return null;
	}

	private void indent() {
		prefix += "  ";

	}

	private void deindent() {
		prefix = prefix.substring(2);

	}

	/**
	 * function which checks in parent each composite children: if in this
	 * child, there is only one basic block with a list of instruction: it
	 * replace composite by basic bloc child warning: should use
	 * ModelTransformValueInitialisation to extract SetInstruction
	 * 
	 * @param parent
	 */
	private void replaceCompositeByBasic(CompositeBlock parent) {
		CompositeBlock childCB = null;
		if (parent.eContainer() instanceof Procedure) return;
		for (Block child : parent.getChildren()) {
			if (child instanceof CompositeBlock) {
				childCB = (CompositeBlock) child;
				if ((childCB.getChildren().size() == 1)
						&& (childCB.getChildren().get(0) instanceof BasicBlock)) {
					parent.getScope().mergeWith(childCB.getScope()); // merge
																		// des
																		// scopes
					parent.replace(childCB, childCB.getChildren().get(0));
				}
			}
		}
	}
}
