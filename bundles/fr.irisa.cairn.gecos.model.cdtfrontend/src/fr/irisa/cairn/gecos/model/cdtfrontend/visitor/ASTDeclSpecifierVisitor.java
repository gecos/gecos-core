/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.cdtfrontend.visitor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.cdt.core.dom.ast.IASTCompositeTypeSpecifier;
import org.eclipse.cdt.core.dom.ast.IASTDeclSpecifier;
import org.eclipse.cdt.core.dom.ast.IASTDeclaration;
import org.eclipse.cdt.core.dom.ast.IASTElaboratedTypeSpecifier;
import org.eclipse.cdt.core.dom.ast.IASTEnumerationSpecifier;
import org.eclipse.cdt.core.dom.ast.IASTEnumerationSpecifier.IASTEnumerator;
import org.eclipse.cdt.core.dom.ast.IASTNamedTypeSpecifier;
import org.eclipse.cdt.core.dom.ast.IASTSimpleDeclSpecifier;
import org.eclipse.cdt.core.dom.ast.c.ICASTCompositeTypeSpecifier;
import org.eclipse.cdt.core.dom.ast.c.ICASTDeclSpecifier;
import org.eclipse.cdt.core.dom.ast.c.ICASTElaboratedTypeSpecifier;
import org.eclipse.cdt.core.dom.ast.c.ICASTEnumerationSpecifier;
import org.eclipse.cdt.core.dom.ast.c.ICASTSimpleDeclSpecifier;
import org.eclipse.cdt.core.dom.ast.c.ICASTTypedefNameSpecifier;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTCompositeTypeSpecifier;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTDeclSpecifier;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTElaboratedTypeSpecifier;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTEnumerationSpecifier;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTNamedTypeSpecifier;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTSimpleDeclSpecifier;

import fr.irisa.cairn.gecos.model.cdtfrontend.CDTFrontEnd;
import fr.irisa.cairn.gecos.model.cdtfrontend.builder.CoreBuilder;
import fr.irisa.cairn.gecos.model.cdtfrontend.exception.CDTParserErrorException;
import fr.irisa.cairn.gecos.model.cdtfrontend.exception.CDTParserNotYetSupportedException;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.transforms.constants.ConstantExpressionEvaluator;
import gecos.core.Scope;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.types.AliasType;
import gecos.types.EnumType;
import gecos.types.Field;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.RecordType;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;

/**
 * @author mnaullet
 */
public class ASTDeclSpecifierVisitor {

	private CDTToGecosModel gecosModel;
	private Type type;
	private boolean isTypedef;
	
	int namelessRecordCounter = 0;
	static String namelessRecordPrefix = "__gcs_nameless_record_";
	
	public ASTDeclSpecifierVisitor() {
		this.isTypedef = false;
	}

	public void visit(CDTToGecosModel gecosModel, IASTDeclSpecifier declSpecifier) {
		this.gecosModel = gecosModel;
		if (declSpecifier instanceof IASTCompositeTypeSpecifier)
			if (declSpecifier instanceof ICASTCompositeTypeSpecifier)
				buildICASTCompositeTypeSpecifier((ICASTCompositeTypeSpecifier)declSpecifier);
			else if (declSpecifier instanceof ICPPASTCompositeTypeSpecifier)
				buildICPPASTCompositeTypeSpecifier((ICPPASTCompositeTypeSpecifier)declSpecifier);
			else
				buildIASTCompositeTypeSpecifier((IASTCompositeTypeSpecifier)declSpecifier);
		else if (declSpecifier instanceof IASTElaboratedTypeSpecifier)
			if (declSpecifier instanceof ICASTElaboratedTypeSpecifier)
				buildICASTElaboratedTypeSpecifier((ICASTElaboratedTypeSpecifier)declSpecifier);
			else if (declSpecifier instanceof ICPPASTElaboratedTypeSpecifier)
				buildICPPASTElaboratedTypeSpecifier((ICPPASTElaboratedTypeSpecifier)declSpecifier);
			else
				buildIASTElaboratedTypeSpecifier((IASTElaboratedTypeSpecifier)declSpecifier);
		else if (declSpecifier instanceof IASTEnumerationSpecifier)
			if (declSpecifier instanceof ICASTEnumerationSpecifier)
				buildICASTEnumerationSpecifier((ICASTEnumerationSpecifier)declSpecifier);
			else if (declSpecifier instanceof ICPPASTEnumerationSpecifier)
				buildICPPASTEnumerationSpecifier((ICPPASTEnumerationSpecifier)declSpecifier);
			else 
				buildIASTEnumerationSpecifier((IASTEnumerationSpecifier)declSpecifier);
		else if (declSpecifier instanceof IASTNamedTypeSpecifier)
			if (declSpecifier instanceof ICASTTypedefNameSpecifier)
				buildICASTTypedefNameSpecifier((ICASTTypedefNameSpecifier)declSpecifier);
			else if (declSpecifier instanceof ICPPASTNamedTypeSpecifier)
				buildICPPASTNamedTypeSpecifier((ICPPASTNamedTypeSpecifier)declSpecifier);
			else
				buildIASTNamedTypeSpecifier((IASTNamedTypeSpecifier)declSpecifier);
		else if (declSpecifier instanceof IASTSimpleDeclSpecifier)
			if (declSpecifier instanceof ICASTSimpleDeclSpecifier)
				buildICASTSimpleDeclSpecifier((ICASTSimpleDeclSpecifier)declSpecifier);
			else if (declSpecifier instanceof ICPPASTSimpleDeclSpecifier)
				buildICPPASTSimpleDeclSpecifier((ICPPASTSimpleDeclSpecifier)declSpecifier);
			else
				buildIASTSimpleDeclSpecifier((IASTSimpleDeclSpecifier)declSpecifier);
		else if (declSpecifier instanceof ICASTDeclSpecifier)
			buildICASTDeclSpecifier((ICASTDeclSpecifier)declSpecifier);
		else if (declSpecifier instanceof ICPPASTDeclSpecifier)
			buildICPPASTDeclSpecifier((ICPPASTDeclSpecifier)declSpecifier);
		
		checkStorageClassSpecifiers(declSpecifier);
		
		checkTypeQualifiers(declSpecifier);
	}

	public boolean isTypedef() {
		return isTypedef;
	}
	
	public Type getType() {
		return type;
	}
	
	
	private void buildIASTCompositeTypeSpecifier(IASTCompositeTypeSpecifier declSpecifier){
		declSpecifier.getName().accept(gecosModel);
		String name = ((ASTNameVisitor)gecosModel.getObject()).getName();
		RecordType recordType;
		
		/* 
		 * In case a Record with the same name already exists in the CURRENT scope:
		 *   - if existing type is complete => error (redefinition) 
		 *   - else complete the existing record type and move it to current position in CURRENT scope.
		 * Otherwise, create a new record type in the CURRENT scope.
		 */
		RecordType exisitngRecordType = gecosModel.getGecosCoreContextedFactory().findRecordType(name);
		if (exisitngRecordType != null && gecosModel.getGecosCoreContextedFactory().isInCurrentScope(exisitngRecordType)) {
			if(!exisitngRecordType.getFields().isEmpty()) //XXX should test if exisitngRecordType is complete
				throw new CDTParserErrorException(declSpecifier, "redefinition of '"+name+"'");
			else {
				/* use existingRecordType but move it to the current position in the CURRENT scope */
				recordType = exisitngRecordType;
				gecosModel.getGecosCoreContextedFactory().removeFromCurrentScope(recordType);
				gecosModel.getGecosCoreContextedFactory().addToCurrentScope(recordType);
			}
		} else {
			/* create a new record type in the CURRENT scope */
			if (name == null || name.isEmpty())
				name = namelessRecordPrefix+(namelessRecordCounter++);
			switch (declSpecifier.getKey()) {
			case IASTCompositeTypeSpecifier.k_struct:
				recordType = GecosUserTypeFactory.STRUCT(name);
				break;
			case IASTCompositeTypeSpecifier.k_union:
				recordType = GecosUserTypeFactory.UNION(name);
				break;
			default:
				throw new CDTParserNotYetSupportedException(declSpecifier, "Unsupported declaration specifier "+declSpecifier.getRawSignature());
			}
		}
		
		for(IASTDeclaration declaration : declSpecifier.getDeclarations(true)) {
			declaration.accept(gecosModel);
			ASTDeclarationVisitor declarationVisitor = (ASTDeclarationVisitor)gecosModel.getObject();
			List<Field> fields = declarationVisitor.getFields();
			if (fields == null || fields.isEmpty()) {
				if (declarationVisitor.getSymbol() == null)
					throw new UnsupportedOperationException("Support for "+declSpecifier.getClass().getSimpleName()+ ":" +declSpecifier+ " not yet supported ");
				else {
					fields = new ArrayList<>(1);
					Field f = GecosUserTypeFactory.FIELD(declarationVisitor.getSymbol().getName(), declarationVisitor.getSymbol().getType(),-1);
					fields.add(f);
				}
			}
			recordType.getFields().addAll(fields);
		}
		
		type = recordType;
		if (gecosModel.visitingInclude())
			gecosModel.ignore(type);
	}

	private void buildICASTCompositeTypeSpecifier(ICASTCompositeTypeSpecifier declSpecifier){
		buildIASTCompositeTypeSpecifier(declSpecifier);
	}

	private void buildICPPASTCompositeTypeSpecifier(ICPPASTCompositeTypeSpecifier declSpecifier){
		buildIASTCompositeTypeSpecifier(declSpecifier);
	}

	private void buildIASTElaboratedTypeSpecifier(IASTElaboratedTypeSpecifier declSpecifier){
		// elaborated type specifiers support syntax like 'typedef struct my_struct_t my_struct;' or 'struct my_struct_t array[SIZE];'
		declSpecifier.getName().accept(gecosModel);
		String name = ((ASTNameVisitor)gecosModel.getObject()).getName();
		type = gecosModel.getGecosCoreContextedFactory().findRecordType(name);
		if (type == null) {
			type = gecosModel.getGecosCoreContextedFactory().findEnumType(name);
			if (type == null) {
				AliasType aliasType = gecosModel.getGecosCoreContextedFactory().findAliasType(name);
				if (aliasType != null)
					type = aliasType.getAlias();
				else {
					if (name == null || name.isEmpty())
						name = namelessRecordPrefix+(namelessRecordCounter++);
					switch (declSpecifier.getKind()) {
					case IASTElaboratedTypeSpecifier.k_enum:
						type = GecosUserTypeFactory.ENUM(name);
						break;
					case IASTElaboratedTypeSpecifier.k_struct:
						type = GecosUserTypeFactory.STRUCT(name);
						//TODO mark type as is incomplete
						break;
					case IASTElaboratedTypeSpecifier.k_union:
						type = GecosUserTypeFactory.UNION(name);
						//TODO mark type as is incomplete
						break;
					default:
						throw new CDTParserNotYetSupportedException(declSpecifier, "Unsupported declaration specifier : "+declSpecifier.getRawSignature());
					}
				}
			}
		}
		if (gecosModel.visitingInclude()) 
			gecosModel.ignore(type);
	}

	private void buildICASTElaboratedTypeSpecifier(ICASTElaboratedTypeSpecifier declSpecifier){
		buildIASTElaboratedTypeSpecifier(declSpecifier);
	}

	private void buildICPPASTElaboratedTypeSpecifier(ICPPASTElaboratedTypeSpecifier declSpecifier){
		buildIASTElaboratedTypeSpecifier(declSpecifier);
	}

	private void buildIASTEnumerationSpecifier(IASTEnumerationSpecifier declSpecifier){
		EnumType enumType;
		declSpecifier.getName().accept(gecosModel);
		String name = ((ASTNameVisitor)gecosModel.getObject()).getName();
		enumType = GecosUserTypeFactory.ENUM(name);
		for (IASTEnumerator enumerator : declSpecifier.getEnumerators()) {
			enumerator.accept(gecosModel);
			enumType.getEnumerators().add(((ASTEnumeratorVisitor)gecosModel.getObject()).getEnumerator());
		}
		type = enumType;
		if (gecosModel.visitingInclude()) 
			gecosModel.ignore(type);
	}

	private void buildICASTEnumerationSpecifier(ICASTEnumerationSpecifier declSpecifier){
		buildIASTEnumerationSpecifier(declSpecifier);
	}

	private void buildICPPASTEnumerationSpecifier(ICPPASTEnumerationSpecifier declSpecifier){
		buildIASTEnumerationSpecifier(declSpecifier);
	}

	private void buildIASTNamedTypeSpecifier(IASTNamedTypeSpecifier declSpecifier){
		throw new CDTParserNotYetSupportedException(declSpecifier, "Unsupported declaration specifier "+declSpecifier.getRawSignature());
	}

	private void buildICASTTypedefNameSpecifier(ICASTTypedefNameSpecifier declSpecifier){
		declSpecifier.getName().accept(gecosModel);
		ASTNameVisitor nameVisitor = (ASTNameVisitor)gecosModel.getObject();
		CoreBuilder gecosCoreContextedFactory = gecosModel.getGecosCoreContextedFactory();
		String name = nameVisitor.getName();
		type = gecosCoreContextedFactory.findAliasType(name);
		if (type == null)
			type = gecosCoreContextedFactory.findEnumType(name);
		if (type == null)
			type = gecosCoreContextedFactory.findType(name);
		
		if(type != null) {
			//XXX this is to avoid accidental modifications
			Scope scope = type.getContainingScope();
			type = type.copy();
			if(scope != null)
				type.installOn(scope);
			gecosModel.ignore(type);
		}
			
	}

	private void buildICPPASTNamedTypeSpecifier(ICPPASTNamedTypeSpecifier declSpecifier){
		declSpecifier.getName().accept(gecosModel);
		ASTNameVisitor nameVisitor = (ASTNameVisitor)gecosModel.getObject();
		Map<Integer, Object> instructions = nameVisitor.getInstructions();
		ConstantExpressionEvaluator evaluator = new ConstantExpressionEvaluator();
		
		
		if (nameVisitor.getName().equals(GecosUserTypeFactory.ACTypes.ACIntType) ) {
			IntInstruction inst = evaluator.evaluate((Instruction)instructions.get(0));
			if(inst == null)
				throw new RuntimeException("AC int type must take a constant or a constant expression as bit width parameter");
			int size = (int)inst.getValue();
			boolean signed = !instructions.containsKey(1) || (int)((IntInstruction)instructions.get(1)).getValue() != 0;
			type = GecosUserTypeFactory.ACINT(size, signed);
		} else if ((nameVisitor.getName().equals(GecosUserTypeFactory.APTypes.APIntType))) {
				IntInstruction inst = evaluator.evaluate((Instruction)instructions.get(0));
				if(inst == null)
					throw new RuntimeException("AP int type must take a constant or a constant expression as bit width parameter");
				int size = (int)inst.getValue();
				type = GecosUserTypeFactory.ACINT(size, true);
		} else if ((nameVisitor.getName().equals(GecosUserTypeFactory.APTypes.APUIntType))) {
				IntInstruction inst = evaluator.evaluate((Instruction)instructions.get(0));
				if(inst == null)
					throw new RuntimeException("AP uint type must take a constant or a constant expression as bit width parameter");
				int size = (int)inst.getValue();
				type = GecosUserTypeFactory.ACINT(size, false);
		} else if ((nameVisitor.getName().equals(GecosUserTypeFactory.ACTypes.ACFixedType)) ||
					(nameVisitor.getName().equals(GecosUserTypeFactory.APTypes.APFixedType))
					) {
			IntInstruction inst = evaluator.evaluate((Instruction)instructions.get(0));
			if(inst == null)
				throw new RuntimeException("AC/AP fixed type must take a constant or a constant expression as bit width parameter");
			int bitwidth = (int)inst.getValue();
			inst =  evaluator.evaluate((Instruction)instructions.get(1));
			if(inst == null)
				throw new RuntimeException("AC/AP fixed type must take a constant or a constant expression as integer width parameter");
			int integerWidth = (int)inst.getValue();
			boolean signed = !instructions.containsKey(2) || (int)((IntInstruction)instructions.get(2)).getValue() != 0;
			// semantic of ac_fixed is ac_fixed<bitwidth,integerWidth,signed,Quantization,Overflow>
			type = GecosUserTypeFactory.ACFIXED(
					bitwidth, //bitwidth
					integerWidth, //integer width
					signed, // signed
					instructions.get(3) == null ? null : QuantificationMode.get((String)instructions.get(3)),
					instructions.get(4) == null ? null : OverflowMode.get((String)instructions.get(4)));
		} else if (nameVisitor.getName().equals(GecosUserTypeFactory.ACTypes.ACFloatType)) {
			IntInstruction inst = evaluator.evaluate((Instruction)instructions.get(0));
			if(inst == null)
				throw new RuntimeException("AC float type must take a constant or a constant expression as exponant parameter");
			int exponant = (int)inst.getValue();
			inst =  evaluator.evaluate((Instruction)instructions.get(1));
			if(inst == null)
				throw new RuntimeException("AC float type must take a constant or a constant expression as mantissa parameter");
			int mantissa = (int)inst.getValue();
			
			switch(instructions.size()) {
			case 2 :
				type = GecosUserTypeFactory.ACFLOAT(
						exponant,
						mantissa);
				break;
			case 4 :
				type = GecosUserTypeFactory.ACFLOAT(
						exponant,
						mantissa,
						((IntInstruction)instructions.get(2)).getValue()==1L,
						(int)((IntInstruction)instructions.get(3)).getValue());
				break;
			}
		} else if (nameVisitor.getName().equals(GecosUserTypeFactory.ACTypes.ACChannelType)) {
			//TODO Correct implementation
			type = GecosUserTypeFactory.ACCOMPLEX(((Instruction)instructions.get(0)).getType());
		} else if (nameVisitor.getName().equals(GecosUserTypeFactory.ACTypes.ACComplexType)) {
			//TODO Correct implementation
			type = GecosUserTypeFactory.ACCHANNEL();
		} else {
			type = gecosModel.getGecosCoreContextedFactory().findType(nameVisitor.getName());
		}
		
		if (gecosModel.visitingInclude())
			gecosModel.ignore(type);
	}

	private void buildIASTSimpleDeclSpecifier(IASTSimpleDeclSpecifier declSpecifier){
		if(declSpecifier.getChildren().length>0)
			throw new CDTParserNotYetSupportedException(declSpecifier, "Unsupported declaration specifier "+declSpecifier.getRawSignature());
		
		SignModifiers sign = declSpecifier.isUnsigned()  ? 	SignModifiers.UNSIGNED :
								declSpecifier.isSigned() ? 	SignModifiers.SIGNED : 
															SignModifiers.NONE;
		Type res = null;
		switch (declSpecifier.getType()) {
		case IASTSimpleDeclSpecifier.t_bool :
			res = GecosUserTypeFactory.BOOL();
			break;
		case IASTSimpleDeclSpecifier.t_char:
		case IASTSimpleDeclSpecifier.t_wchar_t : //FIXME
		case IASTSimpleDeclSpecifier.t_char32_t : //FIXME
		case IASTSimpleDeclSpecifier.t_char16_t : //FIXME
			res = GecosUserTypeFactory.CHAR(sign);
			break;
		case IASTSimpleDeclSpecifier.t_double :
			if (declSpecifier.isLong()) 
				res = GecosUserTypeFactory.LONGDOUBLE();
			else
				res = GecosUserTypeFactory.DOUBLE();
			break;
		case IASTSimpleDeclSpecifier.t_float :
			res = GecosUserTypeFactory.FLOAT();
			break;
		case IASTSimpleDeclSpecifier.t_void :
			res = GecosUserTypeFactory.VOID();
			break;
		case IASTSimpleDeclSpecifier.t_unspecified:
			if (CDTFrontEnd.VERBOSE) System.err.println("[Warning] Unspecified IASTSimpleDeclSpecifier : "+type+"; using INT as default.");
		case IASTSimpleDeclSpecifier.t_int :
			if (declSpecifier.isLong()) 
				res = GecosUserTypeFactory.LONG(sign);
			else if (declSpecifier.isLongLong())
				res = GecosUserTypeFactory.LONGLONG(sign);
			else if (declSpecifier.isShort())
				res = GecosUserTypeFactory.SHORT(sign);
			else
				res = GecosUserTypeFactory.INT(sign);
			break;
		default:
			throw new CDTParserNotYetSupportedException(declSpecifier, "Unsupported declaration specifier "+type);
		}
		
		type = res;
		if (gecosModel.visitingInclude())
			gecosModel.ignore(type);
	}

	private void buildICASTSimpleDeclSpecifier(ICASTSimpleDeclSpecifier declSpecifier){
		buildIASTSimpleDeclSpecifier(declSpecifier);
	}

	private void buildICPPASTSimpleDeclSpecifier(ICPPASTSimpleDeclSpecifier declSpecifier){
		buildIASTSimpleDeclSpecifier(declSpecifier);
	}

	private void buildICASTDeclSpecifier(ICASTDeclSpecifier declSpecifier){
		throw new CDTParserNotYetSupportedException(declSpecifier, "Unsupported declaration specifier "+declSpecifier.getRawSignature());
	}

	private void buildICPPASTDeclSpecifier(ICPPASTDeclSpecifier declSpecifier){
		throw new CDTParserNotYetSupportedException(declSpecifier, "Unsupported declaration specifier "+declSpecifier.getRawSignature());
	}
	
	private void checkStorageClassSpecifiers(IASTDeclSpecifier declSpecifier) {		
		if (type != null) {
			switch (declSpecifier.getStorageClass()) {
			case IASTDeclSpecifier.sc_static:
				type.setStorageClass(StorageClassSpecifiers.STATIC);
				break;
			case IASTDeclSpecifier.sc_register:
				type.setStorageClass(StorageClassSpecifiers.REGISTER);
				break;
			case IASTDeclSpecifier.sc_auto:
				type.setStorageClass(StorageClassSpecifiers.AUTO);
				break;
			case IASTDeclSpecifier.sc_extern:
				type.setStorageClass(StorageClassSpecifiers.EXTERN);
				break;
			case IASTDeclSpecifier.sc_unspecified:
				type.setStorageClass(StorageClassSpecifiers.NONE);
				break;
			case IASTDeclSpecifier.sc_typedef:
				this.isTypedef = true;
				break;
			case IASTDeclSpecifier.sc_mutable:
				if (CDTFrontEnd.VERBOSE)
					System.err.println("mutable specifier is not supported !!");
				break;
			default:
				if (CDTFrontEnd.VERBOSE)
					System.err.println("Unsupported storage class specifier !");
				break;
			}
		}
		else{
			throw new RuntimeException("Gecos Type representing C type '" + declSpecifier.getRawSignature() + "' not built");
		}
	}
	
	private void checkTypeQualifiers(IASTDeclSpecifier declSpecifier) {
		type.setConstant(declSpecifier.isConst());
		type.setVolatile(declSpecifier.isVolatile());
	}
	
}