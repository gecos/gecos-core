/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.tools.visitors.simpleforloops;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.ForBlock;
import gecos.blocks.ForC99Block;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.Instruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;
import tom.mapping.GenericIntrospector;

@SuppressWarnings("all")
public class SimpleLoop {

	public static SimpleLoop eInstance = SimpleLoop.init();
	private Symbol loopVariable;
	private Instruction lowerBound;
	private Instruction upperBound;
	private Instruction stride;

	%include { sl.tom }

	%include { gecos_common.tom }
	%include { gecos_terminals.tom }
	%include { gecos_basic.tom }
	%include { gecos_blocks.tom }
	%include { gecos_compare.tom }
	%include { gecos_control.tom }
	%include { gecos_arithmetic.tom }

	private SimpleLoop() {
	}

	private static SimpleLoop init() {
		if (eInstance == null)
			return new SimpleLoop();
		else
			return eInstance;
	}

	public ForBlock getForScop(ForBlock forBlock) {
		try {
			FindIteratorStepBound findIteratorStepBound = new FindIteratorStepBound(forBlock);
			loopVariable = findIteratorStepBound.getLoopVariable();
			lowerBound = findIteratorStepBound.getLowerBound();
			upperBound = findIteratorStepBound.getUpperBound();
			stride = findIteratorStepBound.getStride();

			`InnermostId(IsWithoutReferenceOrNotSet(loopVariable)).visitLight(forBlock.getBodyBlock(),  GenericIntrospector.INSTANCE);
			Scope scope = forBlock instanceof ForC99Block ? ((ForC99Block) forBlock).getScope() : GecosUserCoreFactory.scope();
			return GecosUserBlockFactory.SimpleFor(forBlock, loopVariable, lowerBound, upperBound, stride, scope);
		} catch(VisitFailure cisitFailure) {
			return (ForBlock)forBlock;
		} catch(SimpleLoopException simpleLoopException) {
			return (ForBlock)forBlock;
		} catch(NullPointerException nullPointerException) {
			return (ForBlock)forBlock;
		}
	}

	%strategy IsWithoutReferenceOrNotSet(Sym symb) extends Identity(){
		visit Inst {
			set(symref(s),_) -> {
				if (`s.equals(symb))
					throw new SimpleLoopException(null, "Symbol : " + symb +" must not be set");
			} 
			address(symref(s)) -> {
				if (`s.equals(symb))
					throw new SimpleLoopException(null, "Symbol : You could not have a reference on " + symb +" ");
			}
		}
		visit Blk {
			bb@BB(InstL(_, _*)) -> {
				for(Instruction instruction : ((BasicBlock)`bb).getInstructions())
					`InnermostId(IsWithoutReferenceOrNotSet(symb)).visitLight(instruction, GenericIntrospector.INSTANCE);
			}
		}
	}
}