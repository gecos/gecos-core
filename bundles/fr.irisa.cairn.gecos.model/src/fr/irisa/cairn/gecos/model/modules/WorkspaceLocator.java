package fr.irisa.cairn.gecos.model.modules;

import java.io.File;

public class WorkspaceLocator {
	
	public static String WORKSPACE_PATH = getGecosWorkspaceLocation();
	
	public static String getGecosWorkspaceLocation() {
		String res = null;
		File f = new File(".");
		if (f.isDirectory()) {
			res = recursiveSearch(f.getAbsoluteFile());
		} else {
			throw new RuntimeException("current dir is not a directory ...");
		}
		return res.replace("\\", "/"); 
	}

	
	private static String recursiveSearch(File f) {
		for (String s : f.list()) {
			if (s.compareTo(".metadata") == 0) {
				return f.getAbsolutePath();
			}
		}
		
		File parent = f.getParentFile();
		
		if (parent == null) {
			System.err.println("Error : the scripts was not launched in an eclipse's workspace subdirectory.");
			System.exit(-1);
		}
		
		return recursiveSearch(parent);
	}
}
