/**
 */
package gecos.annotations;

import gecos.core.Symbol;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Liveness Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.annotations.LivenessAnnotation#getRefs <em>Refs</em>}</li>
 * </ul>
 *
 * @see gecos.annotations.AnnotationsPackage#getLivenessAnnotation()
 * @model
 * @generated
 */
public interface LivenessAnnotation extends IAnnotation {
	/**
	 * Returns the value of the '<em><b>Refs</b></em>' reference list.
	 * The list contents are of type {@link gecos.core.Symbol}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Refs</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Refs</em>' reference list.
	 * @see gecos.annotations.AnnotationsPackage#getLivenessAnnotation_Refs()
	 * @model
	 * @generated
	 */
	EList<Symbol> getRefs();

} // LivenessAnnotation
