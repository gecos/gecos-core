int res[4] = {1,2,3,4};

int main() {
	#pragma outline
	{
		res[2] = 7;
	}

	if(res[2] != 7)
		abort();

	return 0;
}
