int main() {
	int A[10] = {1,2,3};
	int *pA = A;

	#pragma outline
	{
		A[0] = 4;
		*(A+1) = 5;
		*(pA+2) = A[0];
	}

	//-- test --
	if(A[0] != 4 || A[1] != 5 || A[2] != A[0])
		abort();

	return 0;
}
