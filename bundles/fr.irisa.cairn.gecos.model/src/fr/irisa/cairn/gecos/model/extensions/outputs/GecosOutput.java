package fr.irisa.cairn.gecos.model.extensions.outputs;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Abstract class for any Gecos output primitive. <br>
 * <br>
 * Concrete implementation has to specify output functions using following bean
 * :<br>
 * <br>
 * <code>
 * public void print (MyConcreteType o);
 * </code>
 * 
 * @author Antoine Floc'h - Initial Contribution and API
 * 
 */
public abstract class GecosOutput {
	protected Object[] args;
	protected String file;

	/**
	 * Print output of a given object using its type.
	 * 
	 * @param object
	 * @param args
	 */
	public void print(Object object, String file, Object... args) {
		this.args = args;
		this.file = file;
		try {
			Method[] methods = this.getClass().getMethods();
			for (int i = 0; i < methods.length; i++) {
				Method m = methods[i];
				if(isCompatible(object, m)){
					try {
						
						File dir = new File(this.file);
						if (dir.exists()) {
							if (!dir.isDirectory())
								throw new IllegalArgumentException(file+" exists and is not a directory.");
						} else
							dir.mkdirs();
						
						m.invoke(this,object);
					} catch (InvocationTargetException e) {
						throw new RuntimeException("Problem during output operation for method "+m+ " used with "+object+":"+object.getClass().getSimpleName(),e);
					}
				}
			}
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		
		}
	}

	private boolean isCompatible(Object o, Method m) {
		if (m.getName().equals("print")) {
			Class<?>[] parameterTypes = m.getParameterTypes();
			if (parameterTypes.length == 1) {
				Class<?> t = parameterTypes[0];
				if (t.isInterface()) {
					if (t.isInstance(o))
						return true;
				}else{
					return o.getClass()==t;
				}
			}
		}
		return false;
	}
}
