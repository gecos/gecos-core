package fr.irisa.cairn.gecos.model.utils.instructionprettyprinter;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.extensions.switchs.AdaptableInstrSwitch;
import gecos.core.Symbol;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.ArrayValueInstruction;
import gecos.instrs.BreakInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.CondInstruction;
import gecos.instrs.ContinueInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.FieldInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.GotoInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.InstrsFactory;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.LabelInstruction;
import gecos.instrs.MethodCallInstruction;
import gecos.instrs.NumberedSymbolInstruction;
import gecos.instrs.PhiInstruction;
import gecos.instrs.RangeInstruction;
import gecos.instrs.RetInstruction;
import gecos.instrs.SSADefSymbol;
import gecos.instrs.SSAUseSymbol;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.BaseType;
import gecos.types.PtrType;
import gecos.types.Type;

public class InstrsPrettyPrinter extends AdaptableInstrSwitch<String> {

	protected Map<Instruction, OperatorRegen> instructionPrecedence;

	private InstrsFactory factory = InstrsFactory.eINSTANCE;
	protected int nbIndent = 0;
	private boolean labelProvider;
	private boolean complexAccess = false;

	public InstrsPrettyPrinter(boolean labelProvider) {
		initOps();
		this.labelProvider = labelProvider;
	}

	public static class OperatorRegen {
		protected String name;
		int priority;

		public OperatorRegen(String name, int priority) {
			this.name = name;
			this.priority = priority;
		}
	}

	private static class ArrayOperatorRegen extends OperatorRegen {
		public ArrayOperatorRegen(int priority) {
			super("[]", priority);
		}
	}

	private static class MuxOperatorRegen extends OperatorRegen {
		public MuxOperatorRegen(int priority) {
			super("?:", priority);
		}
	}

	protected GenericInstruction makeGenericInstruction(String name) {
		GenericInstruction instr = (GenericInstruction) factory
				.createGenericInstruction();
		instr.setName(name);
		return instr;
	}

	protected void initOps() {
		instructionPrecedence = new LinkedHashMap<Instruction, OperatorRegen>();
		instructionPrecedence.put(factory.createSymbolInstruction(),
				new OperatorRegen("$sym$", 2));
		instructionPrecedence.put(makeGenericInstruction("array"),
				new ArrayOperatorRegen(2));
		instructionPrecedence.put(makeGenericInstruction("not"),
				new OperatorRegen("~", 3));
		instructionPrecedence.put(makeGenericInstruction("lnot"),
				new OperatorRegen("!", 3));
		instructionPrecedence.put(makeGenericInstruction("neg"),
				new OperatorRegen("-", 3));
		// pre/post inc/dec
		instructionPrecedence.put(makeGenericInstruction("mul"),
				new OperatorRegen("*", 5));
		instructionPrecedence.put(makeGenericInstruction("div"),
				new OperatorRegen("/", 5));
		instructionPrecedence.put(makeGenericInstruction("mod"),
				new OperatorRegen("%", 5));
		instructionPrecedence.put(makeGenericInstruction("add"),
				new OperatorRegen("+", 6));
		instructionPrecedence.put(makeGenericInstruction("sub"),
				new OperatorRegen("-", 6));
		instructionPrecedence.put(makeGenericInstruction("shr"),
				new OperatorRegen(">>", 7));
		instructionPrecedence.put(makeGenericInstruction("shl"),
				new OperatorRegen("<<", 7));
		instructionPrecedence.put(makeGenericInstruction("le"),
				new OperatorRegen("<=", 8));
		instructionPrecedence.put(makeGenericInstruction("ge"),
				new OperatorRegen(">=", 8));
		instructionPrecedence.put(makeGenericInstruction("lt"),
				new OperatorRegen("<", 8));
		instructionPrecedence.put(makeGenericInstruction("gt"),
				new OperatorRegen(">", 8));
		instructionPrecedence.put(makeGenericInstruction("ne"),
				new OperatorRegen("!=", 9));
		instructionPrecedence.put(makeGenericInstruction("eq"),
				new OperatorRegen("==", 9));
		instructionPrecedence.put(makeGenericInstruction("and"),
				new OperatorRegen("&", 10));
		instructionPrecedence.put(makeGenericInstruction("xor"),
				new OperatorRegen("^", 11));
		instructionPrecedence.put(makeGenericInstruction("or"),
				new OperatorRegen("|", 12));
		instructionPrecedence.put(makeGenericInstruction("land"),
				new OperatorRegen("&&", 13));
		instructionPrecedence.put(makeGenericInstruction("lor"),
				new OperatorRegen("||", 14));
		instructionPrecedence.put(makeGenericInstruction("mux"),
				new MuxOperatorRegen(15));
	}

	private boolean needParantheses(Instruction g) {
		if (g.getParent() == null)
			return false;
		OperatorRegen parentOp = findOperatorRegen(g.getParent());
		if (parentOp == null)
			return false;
		OperatorRegen op = findOperatorRegen(g);
		if (op == null) {
			return false;
		}
		return op.priority > parentOp.priority;
	}

	protected OperatorRegen findOperatorRegen(Instruction g) {
		for (Instruction inst : instructionPrecedence.keySet()) {
			if (g.getClass().equals(inst.getClass())) {
				if (g instanceof GenericInstruction
						&& !((GenericInstruction) g).getName().equals(
								((GenericInstruction) inst).getName())) {
					continue;
				}
				return instructionPrecedence.get(inst);
			}
		}
		return null;
	}

	@Override
	public String caseGenericInstruction(GenericInstruction object) {
		String instructionText = "";
		String name = object.getName() == null && labelProvider ? "Name?"
				: object.getName();
		boolean needParantheses = needParantheses(object);
		if (needParantheses) {
			instructionText = instructionText + "(";
		}
		EList<Instruction> operandes = object.getChildren();
		if (operandes.size() > 0) {
			String oper0 = operandes.get(0) == null ? "Operande0?"
					: doSwitch(operandes.get(0));

			if (name.equals("array")) {
				if (!(operandes.get(0) instanceof SymbolInstruction)) {
					complexAccess = true;
					oper0 = operandes.get(0) == null ? "Operande0?"
							: doSwitch(operandes.get(0));
					complexAccess = false;
				}
				instructionText = instructionText + oper0;
				for (int i = 1; i < operandes.size(); i++) {
					instructionText = instructionText + "["
							+ doSwitch(operandes.get(i)) + "]";
				}
			} else if (name.equals("not")) {
				instructionText = instructionText + "~" + oper0;
			} else if (name.equals("lnot")) {
				instructionText = instructionText + "!" + oper0;
			} else if (name.equals("neg")) {
				instructionText = instructionText + "-" + oper0;
			} else if (operandes.size() > 1) {
				String oper1 = operandes.get(1) == null ? "Operande1?"
						: doSwitch(operandes.get(1));
				if (name.equals("mul")) {
					instructionText = instructionText + oper0 + "*" + oper1;
				} else if (name.equals("div")) {
					instructionText = instructionText + oper0 + "/" + oper1;
				} else if (name.equals("mod")) {
					instructionText = instructionText + oper0 + "%" + oper1;
				} else if (name.equals("add")) {
					instructionText = instructionText + oper0 + "+" + oper1;
				} else if (name.equals("sub")) {
					instructionText = instructionText + oper0 + "-" + oper1;
				} else if (name.equals("shr")) {
					instructionText = instructionText + oper0 + ">>" + oper1;
				} else if (name.equals("shl")) {
					instructionText = instructionText + oper0 + "<<" + oper1;
				} else if (name.equals("le")) {
					instructionText = instructionText + oper0 + " <= " + oper1;
				} else if (name.equals("ge")) {
					instructionText = instructionText + oper0 + " >= " + oper1;
				} else if (name.equals("lt")) {
					instructionText = instructionText + oper0 + " < " + oper1;
				} else if (name.equals("gt")) {
					instructionText = instructionText + oper0 + " > " + oper1;
				} else if (name.equals("ne")) {
					instructionText = instructionText + oper0 + " != " + oper1;
				} else if (name.equals("eq")) {
					instructionText = instructionText + oper0 + " == " + oper1;
				} else if (name.equals("and")) {
					instructionText = instructionText + oper0 + " & " + oper1;
				} else if (name.equals("xor")) {
					instructionText = instructionText + oper0 + " ^ " + oper1;
				} else if (name.equals("or")) {
					instructionText = instructionText + oper0 + " | " + oper1;
				} else if (name.equals("land")) {
					instructionText = instructionText + oper0 + " && " + oper1;
				} else if (name.equals("lor")) {
					instructionText = instructionText + oper1 + " || " + oper0;
				} else if (name.equals("mux") && operandes.size() > 2) {
					String oper2 = operandes.get(2) == null ? "Operande2?"
							: doSwitch(operandes.get(2));
					instructionText = instructionText + oper0 + " ? " + oper1
							+ " : " + oper2;

				} else {
					// Default behavior
					return defaultGenericInstruction(object);
				}
			} else {
				// Default behavior
				return defaultGenericInstruction(object);
			}
		} else {
			instructionText = name + "()";
		}
		if (needParantheses) {
			instructionText = instructionText + ")";
		}
		return instructionText;
	}

	private String defaultGenericInstruction(GenericInstruction object) {
		// Default behavior
		StringBuffer parameters = new StringBuffer();
		for (int i = 0; i < object.getChildrenCount() - 1; ++i) {
			Instruction child = object.getChild(i);
			parameters.append(doSwitch(child)).append(",");
		}
		if (object.getChildrenCount() > 0) {
			Instruction lastChild = object
					.getChild(object.getChildrenCount() - 1);
			parameters.append(doSwitch(lastChild));
		}
		return object.getName() + "(" + parameters.toString() + ")";
	}

	@Override
	public String caseComplexInstruction(ComplexInstruction object) {
		return null;
	}

	@Override
	public String caseAddressInstruction(AddressInstruction object) {
		String instructionText = "";
		String addr = object.getAddress() == null && labelProvider ? "Adress?"
				: doSwitch(object.getAddress());
		instructionText = instructionText + "&(" + addr + ")";
		return instructionText;
	}

	@Override
	public String caseArrayValueInstruction(ArrayValueInstruction object) {
		String instructionText = "{";
		EList<Instruction> elements = object.getChildren();
		for (int i = 0; i < elements.size(); i++) {
			if (i != 0) {
				instructionText = instructionText + ", ";
			}
			instructionText = instructionText + doSwitch(elements.get(i));
		}
		instructionText = instructionText + "}";
		return instructionText;
	}

	@Override
	public String caseGotoInstruction(GotoInstruction object) {
		return "goto " + object.getLabelName();
	}

	@Override
	public String caseContinueInstruction(ContinueInstruction object) {
		return "continue";
	}

	@Override
	public String caseBreakInstruction(BreakInstruction object) {
		return "break";
	}

	@Override
	public String caseCondInstruction(CondInstruction object) {
		String cond = object.getCond() == null ? "null" : doSwitch(object.getCond());
		return "condBranch(" + cond + ")";// goto " + object.getLabel();
	}

	@Override
	public String caseCallInstruction(CallInstruction object) {
		String addr = object.getAddress() == null && labelProvider ? "Address?"
				: doSwitch(object.getAddress());
//		instructionText = instructionText + addr;
//		instructionText = instructionText + "(";
//		for (int i = 1; i < object.getChildren().size(); i++) {
//			if (i != 1) {
//				instructionText = instructionText + ", ";
//			}
//			instructionText = instructionText
//					+ doSwitch(object.getChildren().get(i));
//		}
//		instructionText = instructionText + ")";
		String args = object.getArgs().stream()
			.map(a -> doSwitch(a))
			.collect(Collectors.joining(", "));
		String instructionText = addr + "(" + args + ")";
		return instructionText;
	}

	@Override
	public String caseConvertInstruction(ConvertInstruction convert) {

		/*
		 * Implicit casts: -unsigned BaseType to unsigned BaseType -signed
		 * BaseType to signed BaseType Explicit casts : -others
		 */

		String instructionText = "";
		String type;
		if (convert.getType() == null || labelProvider) {
			type = "Type?";
		} else {
			type = "" + convert.getType();
		}
		String expr = convert.getExpr() == null && labelProvider ? "Expr?"
				: doSwitch(convert.getExpr());
		Type dest = convert.getType();
		Type origin = convert.getExpr().getType();
		if (!(complexAccess && convert.getType() instanceof PtrType)) {
			if (!(dest instanceof BaseType) || !(origin instanceof BaseType)) {
//				if ((dest instanceof BaseType) && !((BaseType) dest).isSigned()) {
//					instructionText = instructionText + "(" + type + ")";
//				} else {
					instructionText = instructionText + "(" + type + ")";
//				}
			} else if ((dest instanceof BaseType)
//					&& !((BaseType) dest).isSigned() //XXX 
			){
				instructionText = instructionText + "(" + type + ")";
			}
		}
		instructionText = instructionText + expr;
		return instructionText;
	}

	@Override
	public String caseFieldInstruction(FieldInstruction object) {
		boolean needParantheses = needParantheses(object);
		boolean recordPtr = object.getExpr() instanceof IndirInstruction;
		String instructionText = "";
		String expr;
		if (object.getExpr() == null) {
			expr = labelProvider ? "Expr?" : "";
		} else {
			expr = recordPtr ? doSwitch(((IndirInstruction) object.getExpr())
					.getAddress()) : doSwitch(object.getExpr());
		}
		String fieldName = object.getField() == null && labelProvider ? "FieldName?"
				: object.getField().getName();
		if (needParantheses) {
			instructionText = instructionText + "(";
		}
		instructionText = instructionText + expr + (recordPtr ? "->" : ".")
				+ fieldName;
		if (needParantheses) {
			instructionText = instructionText + ")";
		}
		return instructionText;
	}

	@Override
	public String caseFloatInstruction(FloatInstruction object) {
		String instructionText = "";
		instructionText = instructionText + object.getValue();
		return instructionText;
	}

	@Override
	public String caseIntInstruction(IntInstruction object) {
		String instructionText = "";
		instructionText = instructionText + object.getValue();
		return instructionText;
	}

	@Override
	public String caseIndirInstruction(IndirInstruction object) {
		boolean needParantheses = needParantheses(object);
		String instructionText = "";
		String addr;
		if (object.getAddress() == null) {
			if (labelProvider) {
				addr = "Address?";
			} else {
				addr = "";
			}
		} else {
			addr = doSwitch(object.getAddress());
		}
		if (needParantheses) {
			instructionText = instructionText + "(";
		}
		instructionText = instructionText + "*" + addr;
		if (needParantheses) {
			instructionText = instructionText + ")";
		}
		return instructionText;
	}

	@Override
	public String caseLabelInstruction(LabelInstruction object) {
		/*
		 * String instructionText = object.getName() + ":\n"; return
		 * instructionText;
		 */
		return object.getName();
		// throw new RuntimeException("BlockBuilder visitLabel not completed");
	}

	@Override
	public String caseRangeInstruction(RangeInstruction object) {
		int size = (int) (object.getHigh() - object.getLow() + 1);
		if (size == 0) {
			throw new RuntimeException("zero size in range" + object);
		}
		int mask = (1 << size) - 1;
		String instructionText = "";
		String expr = object.getExpr() == null && labelProvider ? "Expr?"
				: doSwitch(object.getExpr());
		/*
		 * if ((object.getExpr().getType().equals(Array) ||
		 * object.getExpr().getType().equals(Pointeur) ||
		 * object.getParent().getClass().equals(SetInstruction.class) &&
		 * ((SetInstruction) object.getParent()).getDest().equals(object))) {
		 * instructionText = instructionText + "((";
		 * 
		 * 
		 * PrintStream out = new PrintStream(""); CTypePrinter printer = new
		 * CTypePrinter(out, ""); printer.print(new BaseType(BaseType.INT, size,
		 * false));
		 * 
		 * 
		 * instructionText = instructionText + "*)"; if (!
		 * object.getExpr().getType().equals(Pointeur)) { instructionText =
		 * instructionText + "&"; } instructionText = instructionText +
		 * doSwitch(object.getExpr()); instructionText = instructionText + ")" +
		 * "[" + (object.getLow()/size) + "]"; } else {
		 */
		// if (g.expr().type().isBasic() && ! g.parent().isSet()) {
		if (object.getLow() == 0) {
			instructionText = instructionText + "(" + expr + ")&" + mask;
		} else {
			instructionText = instructionText + "((" + expr + ")>>"
					+ object.getLow() + "&" + mask;
		}
		// }

		return instructionText;
	}

	@Override
	public String caseRetInstruction(RetInstruction object) {
		String instructionText = "";
		if (object.getExpr() != null) {
			instructionText = instructionText + "return "
					+ doSwitch(object.getExpr());
		} else if (nbIndent != 1) {
			instructionText = instructionText + "return";
		}
		return instructionText;
	}

	@Override
	public String caseSetInstruction(SetInstruction object) {
		String dest = object.getDest() == null && labelProvider ? "Dest?"
				: doSwitch(object.getDest());
		String source = object.getSource() == null && labelProvider ? "Source?"
				: doSwitch(object.getSource());
		String instructionText = "";

		if (object.getSource() instanceof GenericInstruction) {
			GenericInstruction sourceInstr = (GenericInstruction) object
					.getSource();
			if (sourceInstr.getOperands().size() > 0) {
				if (sourceInstr.getOperand(0) instanceof SymbolInstruction
						&& object.getDest() instanceof SymbolInstruction) {
					SymbolInstruction sourceSymbol = (SymbolInstruction) sourceInstr
							.getOperand(0);
					SymbolInstruction destSymbol = (SymbolInstruction) object
							.getDest();
					if (sourceSymbol.getSymbol().equals(destSymbol.getSymbol()) && !(sourceSymbol instanceof SSAUseSymbol)) {
						if (sourceInstr.getName().equals("add")
								&& doSwitch(sourceInstr.getOperand(1)).equals(
										"1")) {
							instructionText = instructionText + dest + "++";
							return instructionText;
						} else if (sourceInstr.getName().equals("sub")
								&& doSwitch(sourceInstr.getOperand(1)).equals(
										"1")) {
							instructionText = instructionText + dest + "--";
							return instructionText;
						}
					}
				}
			}
		}
		if (source!=null && source.equals("")) {
			System.out
					.println("InstrsPrettyPrinter.caseSetInstruction()- DEBUG");
			source = doSwitch(object.getSource());
		}
		instructionText = instructionText + dest + " = " + source;
		return instructionText;
	}
	
	@Override
	public String caseSSADefSymbol(SSADefSymbol object) {
		return 
		//"ssadef:"+
		caseNumberedSymbolInstruction(object);
	}
	
	@Override
	public String caseSSAUseSymbol(SSAUseSymbol object) {
		return 
		//"ssause:"+
		caseNumberedSymbolInstruction(object);
	}

	@Override
	public String caseSymbolInstruction(SymbolInstruction object) {
		String instructionText = "";
		Symbol symbol = object.getSymbol();
		String symbolName = "SymbolName?";
		if (symbol != null) {
			symbolName = symbol.getName();
		}
		instructionText = instructionText + symbolName;
		return instructionText;
	}

	@Override
	public String caseNumberedSymbolInstruction(NumberedSymbolInstruction object) {
		String instructionText = "";
		Symbol symbol = object.getSymbol();
		String symbolName = "SymbolName?";
		if (symbol != null) {
			symbolName = symbol.getName();
			symbolName += "_" + object.getNumber();
		}
		instructionText = instructionText + symbolName;
		return instructionText;
	}

	@Override
	public String caseArrayInstruction(ArrayInstruction object) {
		String res = doSwitch(object.getDest());
		for (Instruction i : object.getIndex())
			res += "[" + doSwitch(i) + "]";
		return res;
	}

	@Override
	public String casePhiInstruction(PhiInstruction object) {
		String res = "phi(";
		for (int i = 0; i < object.getChildren().size(); i++) {
			if(i>0) res+=", ";
			res+=doSwitch(object.getChild(i));
		}
		return res+")";
	}

	@Override
	public String caseMethodCallInstruction(MethodCallInstruction object) {
		String res = doSwitch(object.getAddress());
		res += "."+object.getName()+"(";
//		for (int i = 0; i < object.getChildren().size(); i++) {
//			if(i>0) res+=", ";
//			res+=doSwitch(object.getChild(i));
//		}
		res += object.getArgs().stream()
			.map(arg -> doSwitch(arg))
			.collect(Collectors.joining(", "));
		
		return res+")";
	}
	
	

}
