/**
 */
package gecos.annotations.impl;

import gecos.annotations.AnnotatedElement;
import gecos.annotations.AnnotationsPackage;
import gecos.annotations.IAnnotation;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IAnnotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class IAnnotationImpl extends MinimalEObjectImpl.Container implements IAnnotation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IAnnotationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnnotationsPackage.Literals.IANNOTATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotatedElement getAnnotatedElement() {
		EObject _eContainer = this.eContainer();
		boolean _tripleNotEquals = (_eContainer != null);
		if (_tripleNotEquals) {
			final EObject containe = this.eContainer().eContainer();
			if ((containe instanceof AnnotatedElement)) {
				return ((AnnotatedElement)containe);
			}
		}
		return null;
	}

} //IAnnotationImpl
