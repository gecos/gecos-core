/*
 * Valid declarations: should compile correctly.
 * Execution performs no tests i.e. always succeed.
 */

static const int (* const (* a)[2]) (int), *b;

int c(short);

void e(int n, int a[n]) {
}


int (*f(short a))(char) {
}

int (*g(short a))(char);


typedef struct h T1;

struct h {
	int x;
	T1 *next;
};


typedef struct {
	struct h *x;
} T2;


int main() {
	exit(0);
}
