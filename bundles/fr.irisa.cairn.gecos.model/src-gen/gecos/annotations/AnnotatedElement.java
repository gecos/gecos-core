/**
 */
package gecos.annotations;

import gecos.core.GecosNode;

import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Annotated Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.annotations.AnnotatedElement#getAnnotations <em>Annotations</em>}</li>
 * </ul>
 *
 * @see gecos.annotations.AnnotationsPackage#getAnnotatedElement()
 * @model abstract="true"
 * @generated
 */
public interface AnnotatedElement extends GecosNode {
	/**
	 * Returns the value of the '<em><b>Annotations</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link gecos.annotations.IAnnotation},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotations</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotations</em>' map.
	 * @see gecos.annotations.AnnotationsPackage#getAnnotatedElement_Annotations()
	 * @model mapType="gecos.annotations.StringToIAnnotationMap&lt;gecos.annotations.String, gecos.annotations.IAnnotation&gt;"
	 * @generated
	 */
	EMap<String, IAnnotation> getAnnotations();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" keyDataType="gecos.annotations.String" keyUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getAnnotations().get(key);'"
	 * @generated
	 */
	IAnnotation getAnnotation(String key);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model keyDataType="gecos.annotations.String" keyUnique="false" annotUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getAnnotations().put(key, annot);'"
	 * @generated
	 */
	void setAnnotation(String key, IAnnotation annot);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return the {@link PragmaAnnotation} attached to this object,
	 * {@code null} if not found.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.annotations.IAnnotation%&gt; annot = this.getAnnotation(&lt;%gecos.annotations.AnnotationKeys%&gt;.PRAGMA_ANNOTATION_KEY.getLiteral());\nif ((annot instanceof &lt;%gecos.annotations.PragmaAnnotation%&gt;))\n{\n\treturn ((&lt;%gecos.annotations.PragmaAnnotation%&gt;)annot);\n}\nreturn null;'"
	 * @generated
	 */
	PragmaAnnotation getPragma();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return the {@link FileLocationAnnotation} attached to this object,
	 * {@code null} if not found.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%gecos.annotations.IAnnotation%&gt; annot = this.getAnnotation(&lt;%gecos.annotations.AnnotationKeys%&gt;.FILE_LOCATION_ANNOTATION_KEY.getLiteral());\nif ((annot instanceof &lt;%gecos.annotations.FileLocationAnnotation%&gt;))\n{\n\treturn ((&lt;%gecos.annotations.FileLocationAnnotation%&gt;)annot);\n}\nreturn null;'"
	 * @generated
	 */
	FileLocationAnnotation getFileLocation();

} // AnnotatedElement
