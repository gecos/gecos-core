#ifndef _ASSERT_H
#define _ASSERT_H

extern void __assert_fail (const char *__assertion, const char *__file,
      unsigned int __line, const char *__function);


extern void __assert_perror_fail (int __errnum, const char *__file,
      unsigned int __line,
      const char *__function);

extern void __assert (const char *__assertion, const char *__file, int __line);

extern void assert(int);

void ___assert_h();

#endif
