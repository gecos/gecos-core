package fr.irisa.cairn.gecos.model.utils.callgraph;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

public class CallGraphDottyExport extends CallGraphVisitor {

	PrintStream out; 
	String name;
	private CallGraph g;
	
	public CallGraphDottyExport(CallGraph g, String filename) throws FileNotFoundException {
		out = new PrintStream(new File(filename+".dot"));
		name=filename;
		this.g=g;
	}
	

	public void compute() {
		visitCallGraph(g);
	}

	@Override
	public Object visitCallGraph(CallGraph g) {
		out.print("digraph \""+name+"\" {");

		for (Iterator<CallGraphNode> i = g.nodes().iterator(); i.hasNext();) {
			CallGraphNode node = i.next();
			node.accept(this);
		}
		
		out.print("}");
		return null;
	}

	@Override
	public Object visitCallGraphEdge(CallGraphEdge edge) {
		// TODO Auto-generated method stub
		String dst = edge.to().procedure().getSymbol().getName();
		String src = edge.from().procedure().getSymbol().getName();
		out.println(src+"->"+dst+"");
		return null;
	}

	@Override
	public Object visitCallGraphNode(CallGraphNode node) {
		String name = node.procedure().getSymbol().getName();
		out.println(name+"[label=\""+name+"\"]");
		for (CallGraphEdge edge :node.outEdges()) {
			//CallGraphEdge edge = e.nextElement();
			edge.accept(this);
		}
		return null;
	}

}
