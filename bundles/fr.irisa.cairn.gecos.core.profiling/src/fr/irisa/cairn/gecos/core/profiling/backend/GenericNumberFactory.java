package fr.irisa.cairn.gecos.core.profiling.backend;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Executable;
import java.lang.reflect.Method;
import java.util.Objects;

/**
 * @author aelmouss
 */
public class GenericNumberFactory<T extends Number> {
	
	private Class<T> type;
	private Executable factory;
	
	public GenericNumberFactory(Class<T> type) {
		this.type = Objects.requireNonNull(type);
		this.factory = getFactory(type);
		if(factory == null)
			throw new InvalidProfilingFileFormat("The element type class '" + type 
				+ "' does not have a Constructor with a String parameter nor a static 'valueOf(String)' method!");
	}
	
	public T valueOf(String s) {
		return valueOf(factory, s);
	}
	
	@SuppressWarnings("unchecked")
	public T[] createArray(int length) {
		return (T[]) Array.newInstance(type, length);
	}
	
	
	private static <T extends Number> Executable getFactory(Class<T> cls) {
		Executable factory = getValueOfMethod(cls);
		if(factory == null)
			factory = getConstructorFromString(cls);
		return factory;
	}
	
	private static <T extends Number> Constructor<T> getConstructorFromString(Class<T> cls) {
		try {
			return cls.getConstructor(String.class);
		} catch (NoSuchMethodException e) {
			return null;
		}
	}
	
	private static <T extends Number> Method getValueOfMethod(Class<T> cls) {
		try {
			return cls.getMethod("valueOf", String.class);
		} catch (NoSuchMethodException e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	private static <T extends Number> T valueOf(Executable factory, String value) {
//		value = value.replace("nan", "NaN"); //'nan' cannot be parsed
		try {
			if(factory instanceof Constructor)
				return ((Constructor<T>) factory).newInstance(value);
			else
				return (T) ((Method)factory).invoke(null, value);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
