/**
 */
package gecos.instrs.impl;

import gecos.core.Symbol;

import gecos.instrs.InstrsPackage;
import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;
import gecos.instrs.SymbolInstruction;

import gecos.types.Type;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Symbol Instruction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.impl.SymbolInstructionImpl#getSymbol <em>Symbol</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SymbolInstructionImpl extends InstructionImpl implements SymbolInstruction {
	/**
	 * The cached value of the '{@link #getSymbol() <em>Symbol</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSymbol()
	 * @generated
	 * @ordered
	 */
	protected Symbol symbol;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SymbolInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstrsPackage.eINSTANCE.getSymbolInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol getSymbol() {
		return symbol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSymbol(Symbol newSymbol) {
		Symbol oldSymbol = symbol;
		symbol = newSymbol;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InstrsPackage.SYMBOL_INSTRUCTION__SYMBOL, oldSymbol, symbol));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final InstrsVisitor visitor) {
		visitor.visitSymbolInstruction(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol getUsedSymbol() {
		return this.getSymbol();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String sym = "(sym=?)";
		Symbol _symbol = this.getSymbol();
		boolean _tripleNotEquals = (_symbol != null);
		if (_tripleNotEquals) {
			sym = this.getSymbol().getName();
		}
		return sym;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isConstant() {
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSymbol() {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type computeType() {
		Symbol _symbol = this.getSymbol();
		boolean _tripleNotEquals = (_symbol != null);
		if (_tripleNotEquals) {
			return this.getSymbol().getType();
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSame(final Instruction instr) {
		if ((instr instanceof SymbolInstruction)) {
			return (super.isSame(instr) && (((SymbolInstruction)instr).getSymbol() == this.getSymbol()));
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean equals(final Object obj) {
		return ((obj instanceof SymbolInstruction) && (((SymbolInstruction) obj).getSymbol() == this.getSymbol()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void updateSymbol(final Symbol originalSymbol, final Symbol newSymbol) {
		Symbol _usedSymbol = this.getUsedSymbol();
		boolean _tripleEquals = (_usedSymbol == originalSymbol);
		if (_tripleEquals) {
			this.setSymbol(newSymbol);
		}
		else {
			throw new IllegalArgumentException("Given Symbol is not used.");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSymbolName() {
		return this.getUsedSymbol().getName();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InstrsPackage.SYMBOL_INSTRUCTION__SYMBOL:
				return getSymbol();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InstrsPackage.SYMBOL_INSTRUCTION__SYMBOL:
				setSymbol((Symbol)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InstrsPackage.SYMBOL_INSTRUCTION__SYMBOL:
				setSymbol((Symbol)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InstrsPackage.SYMBOL_INSTRUCTION__SYMBOL:
				return symbol != null;
		}
		return super.eIsSet(featureID);
	}

} //SymbolInstructionImpl
