package fr.irisa.cairn.gecos.model.extensions.generators;

import java.util.List;

public interface IGeneratorDispatchStrategy {

	public String dispatch(List<IGecosCodeGenerator> generators, Object o);
}