package fr.irisa.cairn.gecos.model.tools.utils;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import gecos.instrs.BreakInstruction;
import gecos.instrs.ContinueInstruction;
import gecos.instrs.GotoInstruction;
import gecos.instrs.LabelInstruction;
import gecos.instrs.RetInstruction;

/**
 * 
 * @author amorvan
 *	@generated NOT
 */
public class BranchInstructionFinder extends BlockInstructionSwitch<Object> {

	private List<GotoInstruction> gotos = new ArrayList<GotoInstruction>();
	public List<GotoInstruction> getGotos() {	return gotos; }
	public boolean containsGotos() { return getGotos().size() > 0; }

	private List<BreakInstruction> breaks = new ArrayList<BreakInstruction>();
	public List<BreakInstruction> getBreaks() {	return breaks; }
	public boolean containsBreaks() { return getBreaks().size() > 0; }

	private List<ContinueInstruction> continues = new ArrayList<ContinueInstruction>();
	public List<ContinueInstruction> getContinues() {	return continues; }
	public boolean containsContinues() { return getContinues().size() > 0; }

	private List<RetInstruction> returns = new ArrayList<RetInstruction>();
	public List<RetInstruction> getReturns() {	return returns; }
	public boolean containsReturns() { return getReturns().size() > 0; }

	private List<LabelInstruction> labels = new ArrayList<LabelInstruction>();
	public List<LabelInstruction> getLabels() {	return labels; }
	public boolean containsLabels() { return getLabels().size() > 0; }
	
	
	@Override
	public Object caseGotoInstruction(GotoInstruction object) {
		getGotos().add(object);
		return true;
	}
	
	@Override
	public Object caseRetInstruction(RetInstruction g) {
		getReturns().add(g);
		return true;
	}
	
	@Override
	public Object caseBreakInstruction(BreakInstruction object) {
		getBreaks().add(object);
		return true;
	}
	
	@Override
	public Object caseContinueInstruction(ContinueInstruction object) {
		getContinues().add(object);
		return true;
	}
	
	@Override
	public Object caseLabelInstruction(LabelInstruction object) {
		getLabels().add(object);
		return true;
	}	
}
