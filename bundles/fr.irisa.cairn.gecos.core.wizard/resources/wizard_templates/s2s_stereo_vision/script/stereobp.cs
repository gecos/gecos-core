project = CreateGecosProject("stereo_bp"); # Create a empty GeCoS project
AddSourceToGecosProject(project,"./../src/");  # Add new C sources in the project
AddIncDirToGecosProject(project,"./../includes/"); # Add header in the project
CDTFrontend(project); # Generate the GeCoS IR from the C source added

ModelProcedureInliner(project); # Inline procedure if inline pragma is found
ModelForUnroll(project); # Unroll for loop if unroll pragma is found

SaveGecosProject(project,"stereo_bp.gecosproject"); # Save the GeCoS IR into gecosproject format
output(project, "c", "../src-regen"); # Regenerate the C source files from the GeCoS IR
