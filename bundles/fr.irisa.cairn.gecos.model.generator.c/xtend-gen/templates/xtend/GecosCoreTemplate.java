package templates.xtend;

import fr.irisa.cairn.gecos.model.c.generator.ExtendableAnnotationCGenerator;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableBlockCGenerator;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableCompleteTypeCGenerator;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableCoreCGenerator;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableInstructionCGenerator;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableTypeCGenerator;
import fr.irisa.cairn.gecos.model.extensions.generators.IGecosCodeGenerator;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import gecos.annotations.AnnotatedElement;
import gecos.annotations.AnnotationKeys;
import gecos.annotations.CommentAnnotation;
import gecos.annotations.IAnnotation;
import gecos.annotations.PragmaAnnotation;
import gecos.blocks.CompositeBlock;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.Instruction;
import gecos.types.AliasType;
import gecos.types.EnumType;
import gecos.types.FunctionType;
import gecos.types.RecordType;
import gecos.types.Type;
import java.util.Arrays;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import templates.xtend.GecosTemplate;

@SuppressWarnings("all")
public class GecosCoreTemplate extends GecosTemplate {
  protected Object _generate(final EObject object) {
    return null;
  }
  
  public CharSequence generateMacros(final ProcedureSet procedureSet) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer stringBuffer = new StringBuffer();
      boolean _containsKey = procedureSet.getAnnotations().containsKey("need_define_min");
      if (_containsKey) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("#ifndef _min_");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("#define _min_(a, b)       ((a) < (b) ? (a) : (b))");
        _builder.newLine();
        _builder.append("#endif");
        _builder.newLine();
        stringBuffer.append(_builder);
        procedureSet.getAnnotations().removeKey("need_define_min");
      }
      boolean _containsKey_1 = procedureSet.getAnnotations().containsKey("need_define_max");
      if (_containsKey_1) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("#ifndef _max_");
        _builder_1.newLine();
        _builder_1.append("\t");
        _builder_1.append("#define _max_(a, b)       ((a) < (b) ? (b) : (a))");
        _builder_1.newLine();
        _builder_1.append("#endif");
        _builder_1.newLine();
        stringBuffer.append(_builder_1);
        procedureSet.getAnnotations().removeKey("need_define_max");
      }
      boolean _containsKey_2 = procedureSet.getAnnotations().containsKey("need_define_ceil");
      if (_containsKey_2) {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append("#ifndef _ceil_");
        _builder_2.newLine();
        _builder_2.append("\t");
        _builder_2.append("#define _ceil_(a,b) (((a)/(b)) + ((!(((a) >= 0) ^ ((b) >= 0))) ? ((((a)%(b)) == 0)?0:1):0))");
        _builder_2.newLine();
        _builder_2.append("#endif");
        _builder_2.newLine();
        stringBuffer.append(_builder_2);
        procedureSet.getAnnotations().removeKey("need_define_ceil");
      }
      boolean _containsKey_3 = procedureSet.getAnnotations().containsKey("need_define_floor");
      if (_containsKey_3) {
        StringConcatenation _builder_3 = new StringConcatenation();
        _builder_3.append("#ifndef _floor_");
        _builder_3.newLine();
        _builder_3.append("\t");
        _builder_3.append("#define _floor_(a,b) (((a)/(b)) - ((!(((a) >= 0) ^ ((b) >= 0))) ? 0:(((a)%(b)) == 0)?0:1))");
        _builder_3.newLine();
        _builder_3.append("#endif");
        _builder_3.newLine();
        stringBuffer.append(_builder_3);
        procedureSet.getAnnotations().removeKey("need_define_floor");
      }
      boolean _containsKey_4 = procedureSet.getAnnotations().containsKey("need_define_mod");
      if (_containsKey_4) {
        StringConcatenation _builder_4 = new StringConcatenation();
        _builder_4.append("#ifndef _mmod_");
        _builder_4.newLine();
        _builder_4.append("\t");
        _builder_4.append("#define _mmod_(a,b) (((a)>=0)?((a)%(b)):(((a)%(b)!=0)?((b)+(a)%(b)):0))");
        _builder_4.newLine();
        _builder_4.append("#endif");
        _builder_4.newLine();
        stringBuffer.append(_builder_4);
        procedureSet.getAnnotations().removeKey("need_define_floor");
      }
      int _length = stringBuffer.length();
      boolean _greaterThan = (_length > 0);
      if (_greaterThan) {
        stringBuffer.append("\n");
      }
      StringConcatenation _builder_5 = new StringConcatenation();
      String _string = stringBuffer.toString();
      _builder_5.append(_string);
      _xblockexpression = _builder_5;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final ProcedureSet procedureSet) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer stringBuffer = new StringBuffer();
      EList<Procedure> _listProcedures = procedureSet.listProcedures();
      for (final Procedure procedure : _listProcedures) {
        StringConcatenation _builder = new StringConcatenation();
        String _generate = ExtendableCoreCGenerator.eInstance.generate(procedure);
        _builder.append(_generate);
        _builder.newLineIfNotEmpty();
        _builder.newLine();
        stringBuffer.append(_builder);
      }
      CharSequence _generatePragma = this.generatePragma(procedureSet);
      String _plus = (_generatePragma + "\n");
      CharSequence _generateMacros = this.generateMacros(procedureSet);
      String _plus_1 = (_plus + _generateMacros);
      String _generate_1 = ExtendableCoreCGenerator.eInstance.generate(procedureSet.getScope());
      String _plus_2 = (_plus_1 + _generate_1);
      String _plus_3 = (_plus_2 + "\n");
      final String head = new String(_plus_3);
      stringBuffer.insert(0, head);
      StringConcatenation _builder_1 = new StringConcatenation();
      String _string = stringBuffer.toString();
      _builder_1.append(_string);
      _xblockexpression = _builder_1;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final Procedure procedure) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      if (((procedure.getPragma() != null) && (!procedure.getPragma().getContent().isEmpty()))) {
        StringConcatenation _builder = new StringConcatenation();
        CharSequence _generatePragma = this.generatePragma(procedure);
        _builder.append(_generatePragma);
        _builder.newLineIfNotEmpty();
        buffer.append(_builder);
      }
      buffer.append(this.generateProcedureSym(procedure.getSymbol(), true));
      CompositeBlock _body = procedure.getBody();
      boolean _tripleEquals = (_body == null);
      if (_tripleEquals) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append(";");
        buffer.append(_builder_1);
      } else {
        String _generate = ExtendableBlockCGenerator.eInstance.generate(procedure.getBody());
        String _plus = (" " + _generate);
        buffer.append(_plus);
      }
      StringConcatenation _builder_2 = new StringConcatenation();
      String _string = buffer.toString();
      _builder_2.append(_string);
      _xblockexpression = _builder_2;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final ProcedureSymbol symbol) {
    StringConcatenation _builder = new StringConcatenation();
    CharSequence _generateProcedureSym = this.generateProcedureSym(symbol, false);
    _builder.append(_generateProcedureSym);
    return _builder;
  }
  
  public CharSequence generateProcedureSym(final ProcedureSymbol symbol, final boolean genPragmas) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      boolean first = true;
      Type _type = symbol.getType();
      final FunctionType fType = ((FunctionType) _type);
      IGecosCodeGenerator.afterSymbolInformation.setLength(0);
      boolean _isInline = fType.isInline();
      if (_isInline) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("inline ");
        buffer.append(_builder);
      }
      StringConcatenation _builder_1 = new StringConcatenation();
      String _generate = ExtendableTypeCGenerator.eInstance.generate(fType.getReturnType());
      _builder_1.append(_generate);
      _builder_1.append(" ");
      String _name = symbol.getName();
      _builder_1.append(_name);
      buffer.append(_builder_1);
      final String afterSaved = IGecosCodeGenerator.afterSymbolInformation.toString();
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append("(");
      buffer.append(_builder_2);
      EList<ParameterSymbol> _listParameters = symbol.listParameters();
      for (final Symbol s : _listParameters) {
        {
          if (first) {
            first = false;
          } else {
            StringConcatenation _builder_3 = new StringConcatenation();
            _builder_3.append(", ");
            buffer.append(_builder_3);
          }
          if (genPragmas) {
            final CharSequence pragmas = this.generatePragma(s);
            int _length = pragmas.length();
            boolean _notEquals = (_length != 0);
            if (_notEquals) {
              buffer.append((("\n" + pragmas) + "\t"));
            }
          }
          IGecosCodeGenerator.afterSymbolInformation.setLength(0);
          StringConcatenation _builder_4 = new StringConcatenation();
          String _generate_1 = ExtendableCoreCGenerator.eInstance.generate(s);
          _builder_4.append(_generate_1);
          buffer.append(_builder_4);
          buffer.append(IGecosCodeGenerator.afterSymbolInformation);
        }
      }
      boolean _isHasElipsis = fType.isHasElipsis();
      if (_isHasElipsis) {
        StringConcatenation _builder_3 = new StringConcatenation();
        {
          if ((!first)) {
            _builder_3.append(", ");
          }
        }
        _builder_3.append("...");
        buffer.append(_builder_3);
      }
      StringConcatenation _builder_4 = new StringConcatenation();
      _builder_4.append(")");
      buffer.append(_builder_4);
      buffer.append(afterSaved);
      IGecosCodeGenerator.afterSymbolInformation.setLength(0);
      StringConcatenation _builder_5 = new StringConcatenation();
      String _string = buffer.toString();
      _builder_5.append(_string);
      _xblockexpression = _builder_5;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final Scope scope) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      EList<Type> _types = scope.getTypes();
      for (final Type type : _types) {
        {
          if (((((type instanceof AliasType) || (type instanceof RecordType)) || (type instanceof EnumType)) && this.mustBeGenerated(type))) {
            StringConcatenation _builder = new StringConcatenation();
            CharSequence _generateComment = this.generateComment(type);
            _builder.append(_generateComment);
            _builder.newLineIfNotEmpty();
            CharSequence _generatePragma = this.generatePragma(type);
            _builder.append(_generatePragma);
            _builder.newLineIfNotEmpty();
            String _generate = ExtendableCompleteTypeCGenerator.eInstance.generate(type);
            _builder.append(_generate);
            _builder.append(";");
            _builder.newLineIfNotEmpty();
            buffer.append(_builder);
          }
          IGecosCodeGenerator.afterSymbolInformation.setLength(0);
        }
      }
      EList<Symbol> _symbols = scope.getSymbols();
      for (final Symbol symbol : _symbols) {
        boolean _mustBeGenerated = this.mustBeGenerated(symbol);
        if (_mustBeGenerated) {
          StringConcatenation _builder = new StringConcatenation();
          CharSequence _generateComment = this.generateComment(symbol);
          _builder.append(_generateComment);
          _builder.newLineIfNotEmpty();
          CharSequence _generatePragma = this.generatePragma(symbol);
          _builder.append(_generatePragma);
          _builder.newLineIfNotEmpty();
          String _generate = ExtendableCoreCGenerator.eInstance.generate(symbol);
          _builder.append(_generate);
          _builder.append(";");
          _builder.newLineIfNotEmpty();
          buffer.append(_builder);
        }
      }
      int _length = buffer.length();
      boolean _greaterThan = (_length > 0);
      if (_greaterThan) {
        buffer.append("\n");
      }
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append(buffer);
      _xblockexpression = _builder_1;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final Symbol symbol) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      IGecosCodeGenerator.afterSymbolInformation.setLength(0);
      StringConcatenation _builder = new StringConcatenation();
      String _generate = ExtendableTypeCGenerator.eInstance.generate(symbol.getType());
      _builder.append(_generate);
      _builder.append(" ");
      String _name = symbol.getName();
      _builder.append(_name);
      String _string = IGecosCodeGenerator.afterSymbolInformation.toString();
      _builder.append(_string);
      buffer.append(_builder);
      Instruction _value = symbol.getValue();
      boolean _tripleNotEquals = (_value != null);
      if (_tripleNotEquals) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append(" ");
        _builder_1.append("= ");
        String _generate_1 = ExtendableInstructionCGenerator.eInstance.generate(symbol.getValue());
        _builder_1.append(_generate_1, " ");
        buffer.append(_builder_1);
      }
      IGecosCodeGenerator.afterSymbolInformation.setLength(0);
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append(buffer);
      _xblockexpression = _builder_2;
    }
    return _xblockexpression;
  }
  
  public CharSequence generatePragma(final AnnotatedElement annotatedElement) {
    StringConcatenation _builder = new StringConcatenation();
    String _xifexpression = null;
    if (((annotatedElement.getPragma() != null) && (!annotatedElement.getPragma().getContent().isEmpty()))) {
      _xifexpression = ExtendableAnnotationCGenerator.eInstance.generate(annotatedElement.getPragma());
    }
    _builder.append(_xifexpression);
    return _builder;
  }
  
  public CharSequence generateComment(final AnnotatedElement annotatedElement) {
    CharSequence _xblockexpression = null;
    {
      IAnnotation _annotation = annotatedElement.getAnnotation(AnnotationKeys.COMMENT_ANNOTATION_KEY.getLiteral());
      final CommentAnnotation com = ((CommentAnnotation) _annotation);
      StringConcatenation _builder = new StringConcatenation();
      String _xifexpression = null;
      if (((com != null) && (!com.getContent().isEmpty()))) {
        _xifexpression = ExtendableAnnotationCGenerator.eInstance.generate(com);
      }
      _builder.append(_xifexpression);
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  public boolean mustBeGenerated(final Type type) {
    boolean _isIgnorePragma = this.isIgnorePragma(type.getPragma());
    return (!_isIgnorePragma);
  }
  
  public boolean mustBeGenerated(final Symbol symbol) {
    boolean _isIgnorePragma = this.isIgnorePragma(symbol.getPragma());
    return (!_isIgnorePragma);
  }
  
  public boolean isIgnorePragma(final PragmaAnnotation pragmaAnnotation) {
    return (((pragmaAnnotation != null) && (pragmaAnnotation.getContent() != null)) && (pragmaAnnotation.getContent().contains(GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION) || pragmaAnnotation.getContent().contains("ignore")));
  }
  
  public Object generate(final EObject procedureSet) {
    if (procedureSet instanceof ProcedureSet) {
      return _generate((ProcedureSet)procedureSet);
    } else if (procedureSet instanceof ProcedureSymbol) {
      return _generate((ProcedureSymbol)procedureSet);
    } else if (procedureSet instanceof Procedure) {
      return _generate((Procedure)procedureSet);
    } else if (procedureSet instanceof Scope) {
      return _generate((Scope)procedureSet);
    } else if (procedureSet instanceof Symbol) {
      return _generate((Symbol)procedureSet);
    } else if (procedureSet != null) {
      return _generate(procedureSet);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(procedureSet).toString());
    }
  }
}
