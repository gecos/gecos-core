package fr.irisa.cairn.gecos.model.tools.visitors;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksVisitable;
import gecos.blocks.BlocksVisitor;
import gecos.blocks.CaseBlock;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ControlEdge;
import gecos.blocks.DataEdge;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.FlowEdge;
import gecos.blocks.ForBlock;
import gecos.blocks.ForC99Block;
import gecos.blocks.IfBlock;
import gecos.blocks.SimpleForBlock;
import gecos.blocks.SwitchBlock;
import gecos.blocks.WhileBlock;
import gecos.core.CoreVisitable;
import gecos.core.CoreVisitor;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.ScopeContainer;
import gecos.core.Symbol;
import gecos.dag.DAGArrayNode;
import gecos.dag.DAGArrayValueNode;
import gecos.dag.DAGCallNode;
import gecos.dag.DAGControlEdge;
import gecos.dag.DAGDataEdge;
import gecos.dag.DAGEdge;
import gecos.dag.DAGFieldInstruction;
import gecos.dag.DAGFloatImmNode;
import gecos.dag.DAGInPort;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGIntImmNode;
import gecos.dag.DAGJumpNode;
import gecos.dag.DAGNode;
import gecos.dag.DAGNumberedOutNode;
import gecos.dag.DAGNumberedSymbolNode;
import gecos.dag.DAGOpNode;
import gecos.dag.DAGOutNode;
import gecos.dag.DAGOutPort;
import gecos.dag.DAGPatternNode;
import gecos.dag.DAGPhiNode;
import gecos.dag.DAGSSADefNode;
import gecos.dag.DAGSSAUseNode;
import gecos.dag.DAGSimpleArrayNode;
import gecos.dag.DAGStringImmNode;
import gecos.dag.DAGSymbolNode;
import gecos.dag.DAGVectorArrayAccessNode;
import gecos.dag.DAGVectorExpandNode;
import gecos.dag.DAGVectorExtractNode;
import gecos.dag.DAGVectorOpNode;
import gecos.dag.DAGVectorPackNode;
import gecos.dag.DAGVectorShuffleNode;
import gecos.dag.DagVisitable;
import gecos.dag.DagVisitor;
import gecos.gecosproject.GecosHeaderDirectory;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;
import gecos.gecosproject.ProjectVisitable;
import gecos.gecosproject.ProjectVisitor;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.ArrayValueInstruction;
import gecos.instrs.BreakInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.CaseInstruction;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.CondInstruction;
import gecos.instrs.ContinueInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.DummyInstruction;
import gecos.instrs.EnumeratorInstruction;
import gecos.instrs.FieldInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.GotoInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.InstrsVisitable;
import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.LabelInstruction;
import gecos.instrs.MethodCallInstruction;
import gecos.instrs.NumberedSymbolInstruction;
import gecos.instrs.PhiInstruction;
import gecos.instrs.RangeArrayInstruction;
import gecos.instrs.RangeInstruction;
import gecos.instrs.RetInstruction;
import gecos.instrs.SSADefSymbol;
import gecos.instrs.SSAUseSymbol;
import gecos.instrs.SetInstruction;
import gecos.instrs.SimdExpandInstruction;
import gecos.instrs.SimdExtractInstruction;
import gecos.instrs.SimdGenericInstruction;
import gecos.instrs.SimdInstruction;
import gecos.instrs.SimdPackInstruction;
import gecos.instrs.SimdShuffleInstruction;
import gecos.instrs.SimpleArrayInstruction;
import gecos.instrs.SizeofInstruction;
import gecos.instrs.SizeofTypeInstruction;
import gecos.instrs.SizeofValueInstruction;
import gecos.instrs.StringInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.ACFixedType;
import gecos.types.ACFloatType;
import gecos.types.ACIntType;
import gecos.types.ACType;
import gecos.types.AliasType;
import gecos.types.ArrayType;
import gecos.types.BoolType;
import gecos.types.EnumType;
import gecos.types.Enumerator;
import gecos.types.Field;
import gecos.types.FloatType;
import gecos.types.FunctionType;
import gecos.types.IntegerType;
import gecos.types.PtrType;
import gecos.types.RecordType;
import gecos.types.SimdType;
import gecos.types.Type;
import gecos.types.TypesVisitable;
import gecos.types.TypesVisitor;
import gecos.types.VoidType;

/**
 * 
 * @author amorvan
 *
 */
public class GecosDefaultVisitor extends EObjectImpl 
	implements  ProjectVisitor, BlocksVisitor, InstrsVisitor, DagVisitor, CoreVisitor, TypesVisitor {

//	private boolean visitBlocks;
	private boolean visitInstructions;
	private boolean visitTypes;
	private boolean visitScopes;

	public GecosDefaultVisitor() {
		this(true,true,true);//,true);
	}
	
	public GecosDefaultVisitor(
//			boolean visitBlocks,
			boolean visitInstructions,
			boolean visitTypes,
			boolean visitScopes) {
//		this.visitBlocks = visitBlocks;
		this.visitInstructions = visitInstructions;
		this.visitTypes = visitTypes;
		this.visitScopes = visitScopes;
	}
	
	public void visit(EObject ast) {
		if (ast instanceof ProjectVisitable) {
			((ProjectVisitable) ast).accept(this);
		} else if (ast instanceof BlocksVisitable) {
			((BlocksVisitable) ast).accept(this);
		} else if (ast instanceof InstrsVisitable) {
			((InstrsVisitable) ast).accept(this);
		} else if (ast instanceof DagVisitable) {
			((DagVisitable) ast).accept(this);
		} else if (ast instanceof CoreVisitable) {
			((CoreVisitable) ast).accept(this);
		} else if (ast instanceof TypesVisitable) {
			((TypesVisitable) ast).accept(this);
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	@Override
	public void visitProject(GecosProject project) {
		for (GecosSourceFile src : project.getSources().toArray(new GecosSourceFile[project.getSources().size()]))
			src.accept(this);
		for (GecosHeaderDirectory dir : project.getIncludes().toArray(new GecosHeaderDirectory[project.getIncludes().size()]))
			dir.accept(this);
	}
	

	@Override
	public void visitSourceFile(GecosSourceFile sourceFile) {
		if (sourceFile.getModel() != null)
			sourceFile.getModel().accept(this);
	}

	@Override
	public void visitHeaderDirectory(GecosHeaderDirectory headerDirectory) { }


	@Override
	public void visitProcedure(Procedure procedure) {
		procedure.getBody().accept((BlocksVisitor)this);
	}


	@Override
	public void visitProcedureSet(ProcedureSet procedureSet) {
		visitScopeContainer(procedureSet);
		for (Procedure proc : procedureSet.listProcedures().toArray(new Procedure[procedureSet.listProcedures().size()]))
			proc.accept(this);
	}


	@Override
	public void visitScopeContainer(ScopeContainer scopeContainer) {
		if (visitScopes)
			scopeContainer.getScope().accept(this);
	}


	@Override
	public void visitScope(Scope scope) {
		for (Symbol s : scope.getSymbols().toArray(new Symbol[scope.getSymbols().size()]))
			s.accept(this);
		if (visitTypes)
			for (Type t : scope.getTypes().toArray(new Type[scope.getTypes().size()]))
				t.accept(this);
	}


	@Override
	public void visitSymbol(Symbol symbol) {
		if (symbol.getValue() != null)
			symbol.getValue().accept(this);
	}


	@Override
	public void visitParameterSymbol(ParameterSymbol paramSymbol) {
		visitSymbol(paramSymbol);
	}


	@Override
	public void visitProcedureSymbol(ProcedureSymbol procedureSymbol) {
		visitScopeContainer(procedureSymbol);
		visitSymbol(procedureSymbol);
	}


	protected void visitInstruction(Instruction i) {
		
	}
	
	protected void visitComplexInstruction(ComplexInstruction c) {
		visitInstruction(c);
		EList<Instruction> listChildren = c.listChildren();
		for (Instruction i : listChildren.toArray(new Instruction[listChildren.size()])) {
			i.accept(this);
		}
	}
	
	public void visitAddressInstruction(AddressInstruction a) {
		visitComplexInstruction(a);
	}

	
	public void visitArrayInstruction(ArrayInstruction a) {
		visitComplexInstruction(a);
	}

	
	public void visitArrayValueInstruction(ArrayValueInstruction a) {
		visitComplexInstruction(a);
	}

	
	public void visitBreakInstruction(BreakInstruction b) {
		visitComplexInstruction(b);
	}

	
	public void visitCallInstruction(CallInstruction c) {
		visitComplexInstruction(c);
	}

	
	public void visitCondInstruction(CondInstruction c) {
		visitComplexInstruction(c);
	}

	
	public void visitContinueInstruction(ContinueInstruction c) {
		visitComplexInstruction(c);
	}

	
	public void visitConvertInstruction(ConvertInstruction c) {
		visitComplexInstruction(c);
	}

	
	public void visitDummyInstruction(DummyInstruction d) {
		visitInstruction(d);
	}

	
	public void visitFieldInstruction(FieldInstruction f) {
		visitComplexInstruction(f);
	}

	
	public void visitFloatInstruction(FloatInstruction f) {
		visitInstruction(f);
	}

	
	public void visitGenericInstruction(GenericInstruction g) {
		visitComplexInstruction(g);
	}

	
	public void visitGotoInstruction(GotoInstruction g) {
		visitComplexInstruction(g);
	}

	
	public void visitIndirInstruction(IndirInstruction i) {
		visitComplexInstruction(i);
	}

	
	public void visitIntInstruction(IntInstruction i) {
		visitInstruction(i);
	}

	public void visitLabelInstruction(LabelInstruction l) {
		visitInstruction(l);
	}

	
	public void visitMethodCallInstruction(MethodCallInstruction m) {
		visitComplexInstruction(m);
	}

	public void visitPhiInstruction(PhiInstruction p) {
		visitComplexInstruction(p);
	}

	public void visitNumberedSymbolInstruction(NumberedSymbolInstruction n) { 
		visitSymbolInstruction(n);
	}

	public void visitSSADefSymbol(SSADefSymbol s) {	
		visitSymbolInstruction(s);
	}

	public void visitSSAUseSymbol(SSAUseSymbol s) {	
		visitSymbolInstruction(s);
	}

	public void visitRangeInstruction(RangeInstruction r) {
		visitComplexInstruction(r);
	}

	public void visitRetInstruction(RetInstruction r) {
		visitComplexInstruction(r);
	}

	public void visitSetInstruction(SetInstruction s) {
		visitComplexInstruction(s);
	}

	public void visitSizeofInstruction(SizeofInstruction s) {
		visitInstruction(s);
	}

	
	public void visitSizeofTypeInstruction(SizeofTypeInstruction s) {
		visitInstruction(s);
	}

	
	public void visitSizeofValueInstruction(SizeofValueInstruction s) {
		visitInstruction(s);
	}
	
	public void visitSymbolInstruction(SymbolInstruction s) {
		visitInstruction(s);
	}

	public void visitSimpleArrayInstruction(SimpleArrayInstruction s) {
		visitComplexInstruction(s);
	}

	public void visitCaseInstruction(CaseInstruction s) {
		visitComplexInstruction(s);
	}

	public void visitEnumeratorInstruction(EnumeratorInstruction e) {
		visitInstruction(e);
	}

	public void visitStringInstruction(StringInstruction s) {
		visitInstruction(s);
	}

	public void visitSimdInstruction(SimdInstruction s) {
		visitInstruction(s);
	}
	
	public void visitRangeArrayInstruction(RangeArrayInstruction r) {
		visitInstruction(r);
	}

	public void visitBasicBlock(BasicBlock b) {
		visitBlock(b);
		if (visitInstructions)
			for (Instruction i : b.getInstructions().toArray(new Instruction[b.getInstructions().size()])) {
				i.accept(this);
			}

		if(b.getInEdges()!=null)
			for (ControlEdge e : b.getInEdges().toArray(new ControlEdge[b.getInEdges().size()])) {
				e.accept(this);
			}
	}

	protected void visitBlock(Block b) {
		
	}

	public void visitCaseBlock(CaseBlock c) {
		visitBlock(c);
		c.getBody().accept(this);
		
	}

	public void visitCompositeBlock(CompositeBlock c) {
		visitBlock(c);
		visitScopeContainer(c);
		for (Block block : c.getChildren().toArray(new Block[c.getChildren().size()])) {
			block.accept(this);
		}
		
	}

	public void visitControlEdge(ControlEdge c) { }
	public void visitDataEdge(DataEdge c) { 	}
	public void visitFlowEdge(FlowEdge c) { 	}
	
	public void visitSimpleForBlock(SimpleForBlock f) {
		visitScopeContainer(f);
		visitForBlock(f);
	}
	
	public void visitForC99Block(ForC99Block f) {
		visitScopeContainer(f);
		visitForBlock(f);
	}


	public void visitForBlock(ForBlock f) {
		visitBlock(f);
		if (f.getInitBlock() != null)
			f.getInitBlock().accept(this);
		if (f.getTestBlock() != null)
			f.getTestBlock().accept(this);
		if (f.getStepBlock() != null)
			f.getStepBlock().accept(this);
		if (f.getBodyBlock() != null)
			f.getBodyBlock().accept(this);
	}

	public void visitIfBlock(IfBlock i) {
		visitBlock(i);
		if (i.getTestBlock() != null)
			i.getTestBlock().accept(this);
		if (i.getThenBlock() != null)
			i.getThenBlock().accept(this);
		if (i.getElseBlock() != null)
			i.getElseBlock().accept(this);
	}

	public void visitLoopBlock(DoWhileBlock l) {
		visitBlock(l);
		if (l.getBodyBlock() != null)
			l.getBodyBlock().accept(this);
		if (l.getTestBlock() != null)
			l.getTestBlock().accept(this);
	}

	public void visitSwitchBlock(SwitchBlock s) {
		visitBlock(s);
		s.getDispatchBlock().accept(this);
		s.getBodyBlock().accept(this);
	}

	public void visitWhileBlock(WhileBlock w) {
		visitBlock(w);
		if (w.getTestBlock() != null)
			w.getTestBlock().accept(this);
		if (w.getBodyBlock() != null)
			w.getBodyBlock().accept(this);
	}


	@Override
	public void visitField(Field f) {
	}


	@Override
	public void visitArrayType(ArrayType a) {
		if (a.getSizeExpr() != null)
			a.getSizeExpr().accept(this);
		a.getBase().accept(this);
	}


//	@Override
//	public void visitConstType(ConstType c) {
//		c.getBase().accept(this);
//	}


	@Override
	public void visitFunctionType(FunctionType f) {
		f.getReturnType().accept(this);
		for (Type t : f.listParameters().toArray(new Type[f.listParameters().size()])) {
			t.accept(this);
		}
	}


	@Override
	public void visitPtrType(PtrType p) {
		p.getBase().accept(this);
		if (p.getInnermostBase() != null)
			p.getInnermostBase().accept(this);
	}


	@Override
	public void visitRecordType(RecordType r) {
		for (Field f : r.getFields().toArray(new Field[r.getFields().size()]))
			f.accept(this);
	}


//	@Override
//	public void visitVolatileType(VolatileType v) {
//		v.getBase().accept(this);
//	}


	@Override
	public void visitACFixedType(ACFixedType a) {
	}


	@Override
	public void visitACFloatType(ACFloatType a) {
	}


	@Override
	public void visitACIntType(ACIntType a) {
	}


	@Override
	public void visitACType(ACType a) {
	}


	@Override
	public void visitAliasType(AliasType a) {
		a.getAlias().accept(this);
	}


	@Override
	public void visitEnumType(EnumType enumType) {
		for (Enumerator e : enumType.getEnumerators().toArray(new Enumerator[enumType.getEnumerators().size()]))
			e.accept(this);
	}


	@Override
	public void visitEnumerator(Enumerator enumerator) {
		Instruction expression = enumerator.getExpression();
		if (expression != null)
			expression.accept(this);
	}


	public void visitDAGInstruction(DAGInstruction s) {
		visitInstruction(s);
		for (DAGNode n : s.getNodes().toArray(new DAGNode[s.getNodes().size()])) {
			n.accept(this);
		}
		for (DAGEdge e : s.getEdges().toArray(new DAGEdge[s.getEdges().size()])) {
			e.accept(this);
		}
	}

	public void visitDAGInPort(DAGInPort d) {

	}

	public void visitDAGOutPort(DAGOutPort d) {

	}

	public void visitDAGSymbolNode(DAGSymbolNode d) {

	}

	public void visitDAGCallNode(DAGCallNode d) {

	}

	public void visitDAGArrayValueNode(DAGArrayValueNode d) {

	}

	public void visitDAGOpNode(DAGOpNode d) {

	}

	public void visitDAGOutNode(DAGOutNode d) {

	}

	public void visitDAGControlEdge(DAGControlEdge d) {

	}

	public void visitDAGDataEdge(DAGDataEdge d) {

	}

	public void visitDAGNumberedSymbolNode(DAGNumberedSymbolNode d) {

	}

	public void visitDAGNumberedOutNode(DAGNumberedOutNode d) {

	}

	public void visitDAGIntImmNode(DAGIntImmNode d) {

	}

	public void visitDAGFloatImmNode(DAGFloatImmNode d) {

	}

	public void visitDAGArrayNode(DAGArrayNode d) {

	}

	public void visitDAGSimpleArrayNode(DAGSimpleArrayNode d) {

	}

	public void visitDAGJumpNode(DAGJumpNode d) {

	}

	public void visitDAGPatternNode(DAGPatternNode d) {

	}
	
	public void visitDAGVectorArrayAccessNode(DAGVectorArrayAccessNode d) {
		
	}

	@Override
	public void visitDAGStringImmNode(DAGStringImmNode d) {
		
	}

//	@Override
//	public void visitRegisterType(RegisterType rt) {
//		rt.getBase().accept(this);
//	}


	@Override
	public void visitSimdType(SimdType st) {
		st.getSubwordType().accept(this);
	}


//	@Override
//	public void visitStaticType(StaticType st) {
//		st.getBase().accept(this);
//	}

	@Override
	public void visitDAGVectorOpNode(DAGVectorOpNode d) {
		
	}

	@Override
	public void visitDAGVectorShuffleNode(DAGVectorShuffleNode d) {
		
	}
	
	@Override
	public void visitDAGVectorExpandNode(DAGVectorExpandNode d) {
		
	}

	@Override
	public void visitDAGVectorExtractNode(DAGVectorExtractNode d) {

	}

	@Override
	public void visitDAGVectorPackNode(DAGVectorPackNode d) {
		
	}

	@Override
	public void visitSimdGenericInstruction(SimdGenericInstruction i) {
		
	}

	@Override
	public void visitSimdPackInstruction(SimdPackInstruction i) {
		
	}

	@Override
	public void visitSimdShuffleInstruction(SimdShuffleInstruction i) {
		
	}

	@Override
	public void visitSimdExtractInstruction(SimdExtractInstruction i) {
		
	}

	@Override
	public void visitSimdExpandInstruction(SimdExpandInstruction i) {
		
	}
	
	@Override
	public void visitDAGSSAUseNode(DAGSSAUseNode d) {
		
	}

	@Override
	public void visitDAGSSADefNode(DAGSSADefNode d) {
		
	}

	@Override
	public void visitDAGPhiNode(DAGPhiNode d) {
		
	}

	@Override
	public void visitDAGFieldInstruction(DAGFieldInstruction d) {
		
	}

	@Override
	public void visitVoidType(VoidType b) {
		
	}

	@Override
	public void visitIntegerType(IntegerType b) {
		
	}

	@Override
	public void visitBoolType(BoolType b) {
		
	}

	@Override
	public void visitFloatType(FloatType b) {
		
	}

}
