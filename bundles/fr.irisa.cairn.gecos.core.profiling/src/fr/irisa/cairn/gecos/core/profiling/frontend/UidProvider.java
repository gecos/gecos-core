package fr.irisa.cairn.gecos.core.profiling.frontend;

import static fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory.STRING;

import gecos.annotations.AnnotatedElement;
import gecos.annotations.IAnnotation;
import gecos.annotations.StringAnnotation;

public class UidProvider {
	
	private static final String SYMBOL_UID_ANNOTAION_KEY = "UIDProvider:SYMBOL_UID_KEY";
	private static int uid = 0;
	
	public static int setUID(AnnotatedElement s) {
//		int symbolUid = symbolToSave.hashCode();
		int symbolUid = (uid++);
		IAnnotation previous = s.getAnnotations().put(SYMBOL_UID_ANNOTAION_KEY, STRING(String.valueOf(symbolUid)));
		if(previous != null)
			System.err.println(new RuntimeException("[WARNING] Symbol '" + s + "' already had a UID annotation: " + previous).toString());
		return symbolUid;
	}
	
	public static Integer getUID(AnnotatedElement s) {
		IAnnotation uidAnnot = s.getAnnotation(SYMBOL_UID_ANNOTAION_KEY);
		if(uidAnnot instanceof StringAnnotation)
			return Integer.valueOf(((StringAnnotation) uidAnnot).getContent());
		return null;
	}
}