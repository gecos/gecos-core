package fr.irisa.cairn.gecos.model.utils.annotations;

import gecos.annotations.AnnotatedElement;
import gecos.annotations.IAnnotation;

public class AnnotationUtils {

	/**
	 * @param <T> the annotation type
	 * @param obj the annotated element
	 * @param annotationKey
	 * @return the annotation associated with {@code annotationKey} if it's of type T, {@code null} otherwise.
	 */
	@SuppressWarnings("unchecked")
	public static <T extends IAnnotation> T getAnnotation(AnnotatedElement obj, String annotationKey) {
		IAnnotation a = obj.getAnnotation(annotationKey);
		if(a == null)
			return null;
		try {
			return (T)a;
		} catch (Exception e) {
			return null;
		}
	}

}
