package fr.irisa.cairn.gecos.model.extensions.switchs;

import java.lang.reflect.Method;

import org.eclipse.emf.ecore.EObject;

/**
 * Switch adapters delegate.
 * 
 * @author Antoine Floc'h - Initial contribution and API
 * 
 * @param <T>
 */
public class SwitchDelegate<T> {

	/**
	 * Delegate doSwitch function to the first compatible registered switch adapter.
	 * 
	 * @param <Adaptable>
	 * @param o
	 * @param adaptable
	 * @return first result of delegated functions different of null
	 */
	public <Adaptable extends IAdaptableSwitch<T>> T doSwitch(EObject o,
			Adaptable adaptable) {
		for (Object adapter : GecosSwitchsRegistry.INSTANCE
				.getSwitchsAdaptersFor(adaptable)) {
			T result = reflexiveDoSwitchCall(adapter, o);
			if (result != null)
				return result;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private T reflexiveDoSwitchCall(Object switchInstance, EObject o) {
		try {
			Method call = switchInstance.getClass().getMethod("doSwitch",
					EObject.class);
			return (T) call.invoke(switchInstance, o);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
