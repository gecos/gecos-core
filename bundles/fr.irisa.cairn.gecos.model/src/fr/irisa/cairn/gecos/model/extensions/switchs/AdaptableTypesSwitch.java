/**
 * 
 */
package fr.irisa.cairn.gecos.model.extensions.switchs;

import gecos.types.util.TypesSwitch;

import org.eclipse.emf.ecore.EObject;

/**
 * @author kmartin
 * 
 */
public class AdaptableTypesSwitch<T> extends TypesSwitch<T> implements
		IAdaptableSwitch<T> {

	private SwitchDelegate<T> delegate = new SwitchDelegate<T>();

	public SwitchDelegate<T> getDelegate() {
		return delegate;
	}

	@Override
	public T doSwitch(EObject o) {
		T res = delegate.doSwitch(o, this);
		if (res == null) {
			res = super.doSwitch(o);
		}
		return res;
	}

}
