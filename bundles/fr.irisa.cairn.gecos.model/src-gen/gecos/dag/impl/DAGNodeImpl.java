/**
 */
package gecos.dag.impl;

import com.google.common.collect.Iterables;

import gecos.annotations.impl.AnnotatedElementImpl;

import gecos.dag.DAGControlEdge;
import gecos.dag.DAGDataEdge;
import gecos.dag.DAGEdge;
import gecos.dag.DAGInPort;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGNode;
import gecos.dag.DAGOutPort;
import gecos.dag.DagPackage;
import gecos.dag.DagVisitor;

import gecos.types.Type;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DAG Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.impl.DAGNodeImpl#getType <em>Type</em>}</li>
 *   <li>{@link gecos.dag.impl.DAGNodeImpl#getOutputs <em>Outputs</em>}</li>
 *   <li>{@link gecos.dag.impl.DAGNodeImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link gecos.dag.impl.DAGNodeImpl#getInputs <em>Inputs</em>}</li>
 *   <li>{@link gecos.dag.impl.DAGNodeImpl#getIdent <em>Ident</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class DAGNodeImpl extends AnnotatedElementImpl implements DAGNode {
	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected Type type;

	/**
	 * The cached value of the '{@link #getOutputs() <em>Outputs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputs()
	 * @generated
	 * @ordered
	 */
	protected EList<DAGOutPort> outputs;

	/**
	 * The cached value of the '{@link #getInputs() <em>Inputs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputs()
	 * @generated
	 * @ordered
	 */
	protected EList<DAGInPort> inputs;

	/**
	 * The default value of the '{@link #getIdent() <em>Ident</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdent()
	 * @generated
	 * @ordered
	 */
	protected static final int IDENT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getIdent() <em>Ident</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdent()
	 * @generated
	 * @ordered
	 */
	protected int ident = IDENT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DAGNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DagPackage.Literals.DAG_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject)type;
			type = (Type)eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DagPackage.DAG_NODE__TYPE, oldType, type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(Type newType) {
		Type oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DagPackage.DAG_NODE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGOutPort> getOutputs() {
		if (outputs == null) {
			outputs = new EObjectContainmentEList<DAGOutPort>(DAGOutPort.class, this, DagPackage.DAG_NODE__OUTPUTS);
		}
		return outputs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGInstruction getParent() {
		if (eContainerFeatureID() != DagPackage.DAG_NODE__PARENT) return null;
		return (DAGInstruction)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGInstruction basicGetParent() {
		if (eContainerFeatureID() != DagPackage.DAG_NODE__PARENT) return null;
		return (DAGInstruction)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParent(DAGInstruction newParent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParent, DagPackage.DAG_NODE__PARENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParent(DAGInstruction newParent) {
		if (newParent != eInternalContainer() || (eContainerFeatureID() != DagPackage.DAG_NODE__PARENT && newParent != null)) {
			if (EcoreUtil.isAncestor(this, newParent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParent != null)
				msgs = ((InternalEObject)newParent).eInverseAdd(this, DagPackage.DAG_INSTRUCTION__NODES, DAGInstruction.class, msgs);
			msgs = basicSetParent(newParent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DagPackage.DAG_NODE__PARENT, newParent, newParent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGInPort> getInputs() {
		if (inputs == null) {
			inputs = new EObjectContainmentEList<DAGInPort>(DAGInPort.class, this, DagPackage.DAG_NODE__INPUTS);
		}
		return inputs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getIdent() {
		return ident;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdent(int newIdent) {
		int oldIdent = ident;
		ident = newIdent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DagPackage.DAG_NODE__IDENT, oldIdent, ident));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(DagVisitor visitor) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGOutPort> getDataOutputs() {
		final BasicEList<DAGOutPort> res = new BasicEList<DAGOutPort>();
		EList<DAGOutPort> _outputs = this.getOutputs();
		for (final DAGOutPort out : _outputs) {
			if (((out.getSinks().size() > 0) && (out.getSinks().get(0) instanceof DAGDataEdge))) {
				res.add(out);
			}
		}
		return ECollections.<DAGOutPort>unmodifiableEList(res);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGOutPort> getControlOutputs() {
		final BasicEList<DAGOutPort> res = new BasicEList<DAGOutPort>();
		EList<DAGOutPort> _outputs = this.getOutputs();
		for (final DAGOutPort out : _outputs) {
			if (((out.getSinks().size() > 0) && (out.getSinks().get(0) instanceof DAGControlEdge))) {
				res.add(out);
			}
		}
		return ECollections.<DAGOutPort>unmodifiableEList(res);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGInPort> getDataInputs() {
		final BasicEList<DAGInPort> res = new BasicEList<DAGInPort>();
		EList<DAGInPort> _inputs = this.getInputs();
		for (final DAGInPort in : _inputs) {
			DAGEdge _source = in.getSource();
			if ((_source instanceof DAGDataEdge)) {
				res.add(in);
			}
		}
		return ECollections.<DAGInPort>unmodifiableEList(res);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGInPort> getControlInputs() {
		final BasicEList<DAGInPort> res = new BasicEList<DAGInPort>();
		EList<DAGInPort> _inputs = this.getInputs();
		for (final DAGInPort in : _inputs) {
			DAGEdge _source = in.getSource();
			if ((_source instanceof DAGControlEdge)) {
				res.add(in);
			}
		}
		return ECollections.<DAGInPort>unmodifiableEList(res);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGEdge> getInputEdges() {
		final Function1<DAGInPort, DAGEdge> _function = new Function1<DAGInPort, DAGEdge>() {
			public DAGEdge apply(final DAGInPort it) {
				return it.getSource();
			}
		};
		return ECollections.<DAGEdge>unmodifiableEList(ECollections.<DAGEdge>asEList(((DAGEdge[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<DAGEdge>filterNull(IterableExtensions.<DAGInPort, DAGEdge>map(IterableExtensions.<DAGInPort>filterNull(this.getInputs()), _function)), DAGEdge.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGDataEdge> getDataInputEdges() {
		final Function1<DAGInPort, DAGEdge> _function = new Function1<DAGInPort, DAGEdge>() {
			public DAGEdge apply(final DAGInPort it) {
				return it.getSource();
			}
		};
		return ECollections.<DAGDataEdge>unmodifiableEList(ECollections.<DAGDataEdge>asEList(((DAGDataEdge[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(Iterables.<DAGDataEdge>filter(IterableExtensions.<DAGInPort, DAGEdge>map(IterableExtensions.<DAGInPort>filterNull(this.getInputs()), _function), DAGDataEdge.class), DAGDataEdge.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGControlEdge> getControlInputEdges() {
		final Function1<DAGInPort, DAGEdge> _function = new Function1<DAGInPort, DAGEdge>() {
			public DAGEdge apply(final DAGInPort it) {
				return it.getSource();
			}
		};
		return ECollections.<DAGControlEdge>unmodifiableEList(ECollections.<DAGControlEdge>asEList(((DAGControlEdge[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(Iterables.<DAGControlEdge>filter(IterableExtensions.<DAGInPort, DAGEdge>map(IterableExtensions.<DAGInPort>filterNull(this.getInputs()), _function), DAGControlEdge.class), DAGControlEdge.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGEdge> getOutputEdges() {
		final Function1<DAGOutPort, EList<DAGEdge>> _function = new Function1<DAGOutPort, EList<DAGEdge>>() {
			public EList<DAGEdge> apply(final DAGOutPort it) {
				return it.getSinks();
			}
		};
		return ECollections.<DAGEdge>unmodifiableEList(ECollections.<DAGEdge>asEList(((DAGEdge[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<DAGEdge>filterNull(Iterables.<DAGEdge>concat(IterableExtensions.<DAGOutPort, EList<DAGEdge>>map(IterableExtensions.<DAGOutPort>filterNull(this.getOutputs()), _function))), DAGEdge.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGDataEdge> getDataOutputEdges() {
		final Function1<DAGOutPort, EList<DAGEdge>> _function = new Function1<DAGOutPort, EList<DAGEdge>>() {
			public EList<DAGEdge> apply(final DAGOutPort it) {
				return it.getSinks();
			}
		};
		return ECollections.<DAGDataEdge>unmodifiableEList(ECollections.<DAGDataEdge>asEList(((DAGDataEdge[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(Iterables.<DAGDataEdge>filter(Iterables.<DAGEdge>concat(IterableExtensions.<DAGOutPort, EList<DAGEdge>>map(IterableExtensions.<DAGOutPort>filterNull(this.getOutputs()), _function)), DAGDataEdge.class), DAGDataEdge.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGControlEdge> getControlOutputEdges() {
		final Function1<DAGOutPort, EList<DAGEdge>> _function = new Function1<DAGOutPort, EList<DAGEdge>>() {
			public EList<DAGEdge> apply(final DAGOutPort it) {
				return it.getSinks();
			}
		};
		return ECollections.<DAGControlEdge>unmodifiableEList(ECollections.<DAGControlEdge>asEList(((DAGControlEdge[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(Iterables.<DAGControlEdge>filter(Iterables.<DAGEdge>concat(IterableExtensions.<DAGOutPort, EList<DAGEdge>>map(IterableExtensions.<DAGOutPort>filterNull(this.getOutputs()), _function)), DAGControlEdge.class), DAGControlEdge.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGNode> getPredecessors() {
		final Function1<DAGInPort, DAGNode> _function = new Function1<DAGInPort, DAGNode>() {
			public DAGNode apply(final DAGInPort it) {
				return it.getSourceNode();
			}
		};
		return ECollections.<DAGNode>unmodifiableEList(ECollections.<DAGNode>asEList(((DAGNode[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<DAGNode>filterNull(XcoreEListExtensions.<DAGInPort, DAGNode>map(this.getInputs(), _function)), DAGNode.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGNode> getDataPredecessors() {
		final Function1<DAGInPort, DAGEdge> _function = new Function1<DAGInPort, DAGEdge>() {
			public DAGEdge apply(final DAGInPort it) {
				return it.getSource();
			}
		};
		final Function1<DAGDataEdge, DAGNode> _function_1 = new Function1<DAGDataEdge, DAGNode>() {
			public DAGNode apply(final DAGDataEdge it) {
				return it.getSourceNode();
			}
		};
		return ECollections.<DAGNode>unmodifiableEList(ECollections.<DAGNode>asEList(((DAGNode[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<DAGNode>filterNull(IterableExtensions.<DAGDataEdge, DAGNode>map(Iterables.<DAGDataEdge>filter(XcoreEListExtensions.<DAGInPort, DAGEdge>map(this.getInputs(), _function), DAGDataEdge.class), _function_1)), DAGNode.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGNode> getControlPredecessors() {
		final Function1<DAGInPort, DAGEdge> _function = new Function1<DAGInPort, DAGEdge>() {
			public DAGEdge apply(final DAGInPort it) {
				return it.getSource();
			}
		};
		final Function1<DAGControlEdge, DAGNode> _function_1 = new Function1<DAGControlEdge, DAGNode>() {
			public DAGNode apply(final DAGControlEdge it) {
				return it.getSourceNode();
			}
		};
		return ECollections.<DAGNode>unmodifiableEList(ECollections.<DAGNode>asEList(((DAGNode[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<DAGNode>filterNull(IterableExtensions.<DAGControlEdge, DAGNode>map(Iterables.<DAGControlEdge>filter(XcoreEListExtensions.<DAGInPort, DAGEdge>map(this.getInputs(), _function), DAGControlEdge.class), _function_1)), DAGNode.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGNode> getSuccessors() {
		final Function1<DAGOutPort, EList<DAGEdge>> _function = new Function1<DAGOutPort, EList<DAGEdge>>() {
			public EList<DAGEdge> apply(final DAGOutPort it) {
				return it.getSinks();
			}
		};
		final Function1<DAGEdge, DAGNode> _function_1 = new Function1<DAGEdge, DAGNode>() {
			public DAGNode apply(final DAGEdge it) {
				return it.getSinkNode();
			}
		};
		return ECollections.<DAGNode>unmodifiableEList(ECollections.<DAGNode>asEList(((DAGNode[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<DAGNode>filterNull(IterableExtensions.<DAGEdge, DAGNode>map(IterableExtensions.<DAGEdge>filterNull(Iterables.<DAGEdge>concat(XcoreEListExtensions.<DAGOutPort, EList<DAGEdge>>map(this.getOutputs(), _function))), _function_1)), DAGNode.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGNode> getDataSuccessors() {
		final Function1<DAGOutPort, EList<DAGEdge>> _function = new Function1<DAGOutPort, EList<DAGEdge>>() {
			public EList<DAGEdge> apply(final DAGOutPort it) {
				return it.getSinks();
			}
		};
		final Function1<DAGDataEdge, DAGNode> _function_1 = new Function1<DAGDataEdge, DAGNode>() {
			public DAGNode apply(final DAGDataEdge it) {
				return it.getSinkNode();
			}
		};
		return ECollections.<DAGNode>unmodifiableEList(ECollections.<DAGNode>asEList(((DAGNode[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<DAGNode>filterNull(IterableExtensions.<DAGDataEdge, DAGNode>map(Iterables.<DAGDataEdge>filter(Iterables.<DAGEdge>concat(XcoreEListExtensions.<DAGOutPort, EList<DAGEdge>>map(this.getOutputs(), _function)), DAGDataEdge.class), _function_1)), DAGNode.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DAGNode> getControlSuccessors() {
		final Function1<DAGOutPort, EList<DAGEdge>> _function = new Function1<DAGOutPort, EList<DAGEdge>>() {
			public EList<DAGEdge> apply(final DAGOutPort it) {
				return it.getSinks();
			}
		};
		final Function1<DAGControlEdge, DAGNode> _function_1 = new Function1<DAGControlEdge, DAGNode>() {
			public DAGNode apply(final DAGControlEdge it) {
				return it.getSinkNode();
			}
		};
		return ECollections.<DAGNode>unmodifiableEList(ECollections.<DAGNode>asEList(((DAGNode[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<DAGNode>filterNull(IterableExtensions.<DAGControlEdge, DAGNode>map(Iterables.<DAGControlEdge>filter(Iterables.<DAGEdge>concat(XcoreEListExtensions.<DAGOutPort, EList<DAGEdge>>map(this.getOutputs(), _function)), DAGControlEdge.class), _function_1)), DAGNode.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DagPackage.DAG_NODE__PARENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParent((DAGInstruction)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DagPackage.DAG_NODE__OUTPUTS:
				return ((InternalEList<?>)getOutputs()).basicRemove(otherEnd, msgs);
			case DagPackage.DAG_NODE__PARENT:
				return basicSetParent(null, msgs);
			case DagPackage.DAG_NODE__INPUTS:
				return ((InternalEList<?>)getInputs()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DagPackage.DAG_NODE__PARENT:
				return eInternalContainer().eInverseRemove(this, DagPackage.DAG_INSTRUCTION__NODES, DAGInstruction.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DagPackage.DAG_NODE__TYPE:
				if (resolve) return getType();
				return basicGetType();
			case DagPackage.DAG_NODE__OUTPUTS:
				return getOutputs();
			case DagPackage.DAG_NODE__PARENT:
				if (resolve) return getParent();
				return basicGetParent();
			case DagPackage.DAG_NODE__INPUTS:
				return getInputs();
			case DagPackage.DAG_NODE__IDENT:
				return getIdent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DagPackage.DAG_NODE__TYPE:
				setType((Type)newValue);
				return;
			case DagPackage.DAG_NODE__OUTPUTS:
				getOutputs().clear();
				getOutputs().addAll((Collection<? extends DAGOutPort>)newValue);
				return;
			case DagPackage.DAG_NODE__PARENT:
				setParent((DAGInstruction)newValue);
				return;
			case DagPackage.DAG_NODE__INPUTS:
				getInputs().clear();
				getInputs().addAll((Collection<? extends DAGInPort>)newValue);
				return;
			case DagPackage.DAG_NODE__IDENT:
				setIdent((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DagPackage.DAG_NODE__TYPE:
				setType((Type)null);
				return;
			case DagPackage.DAG_NODE__OUTPUTS:
				getOutputs().clear();
				return;
			case DagPackage.DAG_NODE__PARENT:
				setParent((DAGInstruction)null);
				return;
			case DagPackage.DAG_NODE__INPUTS:
				getInputs().clear();
				return;
			case DagPackage.DAG_NODE__IDENT:
				setIdent(IDENT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DagPackage.DAG_NODE__TYPE:
				return type != null;
			case DagPackage.DAG_NODE__OUTPUTS:
				return outputs != null && !outputs.isEmpty();
			case DagPackage.DAG_NODE__PARENT:
				return basicGetParent() != null;
			case DagPackage.DAG_NODE__INPUTS:
				return inputs != null && !inputs.isEmpty();
			case DagPackage.DAG_NODE__IDENT:
				return ident != IDENT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (ident: ");
		result.append(ident);
		result.append(')');
		return result.toString();
	}

} //DAGNodeImpl
