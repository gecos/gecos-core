project = CreateGecosProject("tutorial"); # Just create a GeCoS project
AddSourceToGecosProject(project, "src/ex1.c"); # Add source to the GeCoS project
CDTFrontend(project); # Call the GeCoS Front-end and build the GeCoS IR corresponding to the project
SaveGecosProject(project, "project.gecosproject"); # Save the project (IR) into a gecosproject format (usefull to see how is build the IR)
output(project, "dot", "dot_folder"); # Save the project (IR) into a graphviz format. Show the control dependencies between blocks