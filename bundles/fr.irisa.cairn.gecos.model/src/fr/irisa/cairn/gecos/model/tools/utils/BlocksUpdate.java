package fr.irisa.cairn.gecos.model.tools.utils;

import java.util.ArrayList;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.ControlEdge;

public class BlocksUpdate extends BlocksDefaultSwitch<Boolean> {
	private Block oldb;
	private Block newb;
	
	public BlocksUpdate(Block oldb, Block newb) {
		super();
		this.oldb = oldb;
		this.newb = newb;
	}
	
	@Override
	public Boolean caseBlock(Block object) {
		object.replace(oldb, newb);
		return true;
	}


	@Override
	public Boolean caseBasicBlock(BasicBlock bb) {
		for (ControlEdge e : new ArrayList<ControlEdge>(bb.getOutEdges())) {
			if (e.getTo() == oldb)
				e.setTo((BasicBlock) newb);
			if (e.getFrom() == oldb)
				e.setFrom((BasicBlock) newb);
		}
		return true;
	}
}