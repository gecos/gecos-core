/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.transforms.ssa;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.analysis.dataflow.dominator.DominatorAnalyser;
import fr.irisa.cairn.gecos.model.analysis.dataflow.dominator.DominatorTreeNode;
import fr.irisa.cairn.gecos.model.analysis.dataflow.liveness.LivenessAnalyser;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.tools.switches.DefaultInstructionSwitch;
import fr.irisa.cairn.gecos.model.transforms.others.AssignmentFinder;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.blocks.BasicBlock;
import gecos.blocks.ControlEdge;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Symbol;
import gecos.instrs.CallInstruction;
import gecos.instrs.PhiInstruction;
import gecos.instrs.SSADefSymbol;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.FunctionType;


public class SSAAnalyser extends DominatorAnalyser {

	public static final String SSATAG = "SCALAR_SSA";
	private Map<Symbol, Integer> counter;
	private Map<Symbol, Stack<Integer>> visibility;
	private Map<Symbol, Map<Integer,SSADefSymbol>> defUseMap;

	private boolean VERBOSE=true;
	
	private SSAAnalyser() {
		this(null);
	}
	public SSAAnalyser(Procedure proc) {
		super(proc);
	}

	public static void buildSSA(ProcedureSet ps) {
		SSAAnalyser ssaBuilder = new SSAAnalyser();
		for(Procedure lproc : ps.listProcedures()) {
			ssaBuilder.proc=lproc;
			ssaBuilder.compute();
		}
	}

	/*
	 * 
	 * 
	 */
	public void compute() {

		debug("Starting analysis for procedure: "+proc.getSymbolName());
		
		AssignmentFinder finder = new AssignmentFinder(proc,AssignmentFinder.ARRAYS+AssignmentFinder.SCALARS);
		Map<Symbol, List<BasicBlock>> assign = finder.compute();
		Symbol[] variables = EMFUtils.getAllReferencesOfType(proc.getBody(), Symbol.class)
									 .stream().filter(s -> !(s instanceof ProcedureSymbol)).collect(Collectors.toList())
									 .toArray(new Symbol[0]);

		debug("###### Liveness Analysis ######");
		LivenessAnalyser live = new LivenessAnalyser(proc, variables);
		live.compute();
		debug("######   Dominance Tree  ######");
		Map<BasicBlock, List<BasicBlock>> dominators2 = getDominators();

		// insertion of phi nodes
		debug("######    Join  Points   ######");
		for (int i = 0; i < variables.length; ++i) {
			Symbol key = variables[i];
			List<BasicBlock> blocks = assign.get(key);
			if (blocks!=null) {
				Set<BasicBlock> computeJointPoints = computeJointPoints(blocks);
				List<BasicBlock> joints = new ArrayList<BasicBlock>(computeJointPoints);

				debug("Computing joint points for "+key+ " : ");
				for (BasicBlock basicBlock : joints) {
					debug("   -> BB"+basicBlock.getNumber());
				}

				debug("Inserting joint points for "+key);
				insertJoints(joints, key, live);
			}
		}

		// Initialize renumbering
		counter = new LinkedHashMap<Symbol, Integer>();
		visibility = new LinkedHashMap<Symbol, Stack<Integer>>();
		defUseMap = new LinkedHashMap<Symbol, Map<Integer,SSADefSymbol>>();
		
		for (int i = 0; i < variables.length; ++i) {
			counter.put(variables[i], new Integer(1));
			visibility.put(variables[i], new Stack<Integer>());
			defUseMap .put(variables[i], new LinkedHashMap<Integer,SSADefSymbol>());
		}

		// Add procedure parameters
		for (Symbol symbol : proc.listParameters()) {
			addDefinition(symbol);
		}

		// Add globals
		for (Symbol s: proc.getContainingProcedureSet().getScope().getSymbols()) {
			if (! (s.getType() instanceof FunctionType))
				addDefinition(s);
		}

		debug("######   Renumbering   ######");
		renumberVariables(0,proc.getStart());
		
		
		if(VERBOSE) exportDominatorTreeAsDot();
		proc.getAnnotations().put(SSATAG, GecosUserAnnotationFactory.comment(proc.getSymbol().getName()+" is in SSA Form"));


	}

	public SSADefSymbol findSSADefinition(Symbol s, int i) {
		Map<Integer, SSADefSymbol> map = defUseMap.get(s);
		if(map==null) {
			debug("Could not find definition for <"+i+","+s+"> in "+defUseMap);
			return null;
		} else {
			return map.get(i);
		}
	}

	public void registerSSADefinition(SSADefSymbol newsym) {
		Map<Integer, SSADefSymbol> map = defUseMap.get(newsym.getSymbol());
		if (map==null) {
			map= new LinkedHashMap<Integer,SSADefSymbol>(); 
			defUseMap.put(newsym.getSymbol(), map);
		}
		if(map.containsKey(newsym.getNumber())) {
			throw new RuntimeException("SSADef " + newsym + " already in "+defUseMap);
		}
		map.put(newsym.getNumber(), newsym);
	}

	private void exportDominatorTreeAsDot() {
		try {
			getDominatorTree().outputDot(new PrintStream(
					new FileOutputStream(proc.getSymbol().getName() + "_domin.dot")));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	int addDefinition(Symbol var) {
		int i = getCounter(var);
		getVisibility(var).push(new Integer(i));
		incCounter(var);
		return i;
	}

	private Stack<Integer> getVisibility(Symbol var) {
		if (! visibility.containsKey(var))
			visibility.put(var, new Stack<Integer>());
		return visibility.get(var);
	}

	int topVisibility(Symbol var) {
		try {
			Stack<Integer> visibility = getVisibility(var);
			Integer peek = (Integer) visibility.peek();
			return peek.intValue();
		} catch(EmptyStackException e) {
			if (var.getValue()!=null) {
				return 0;
			} else {
				debug(indent + "    var "+var+" is used without any initialisation");
				return -1;
			}
		}
	}

	private int getCounter(Symbol var) {
		if (! counter.containsKey(var))
			counter.put(var, new Integer(1));
		return ((Integer) counter.get(var)).intValue();
	}

	private void incCounter(Symbol var) {
		int v = getCounter(var) + 1;
		counter.put(var, new Integer(v));
	}

	private class PopVisitor extends DefaultInstructionSwitch<Object> {

		private static final int NORMAL  = 0;
		private static final int NOPHI   = 1;
		private static final int PHI     = 2;

		private int mode = NORMAL;

		@Override
		public Object caseCallInstruction(CallInstruction g) {
			int oldmode = mode;
			mode = NORMAL;
			super.caseCallInstruction(g);
			mode = oldmode;
			return this;
		}
		@Override
		public Object caseSetInstruction(SetInstruction set) {
			int oldmode = mode;
			mode = NORMAL;
			doSwitch(set.getSource());
			//originally checked if mode != PHI to set mode to NOPHI, but this is not necessary
			//for every definition, you must pop it off the visibility stack
			//removing the mode == NOPHI check in the caseSymbolInstruction method causes too many pops,
			//since popping should only be triggered while visiting the LHS of SetInstruction
			mode = NOPHI;
			doSwitch(set.getDest());
			mode = oldmode;
			return this;
		}

		@Override
		public Object caseSymbolInstruction (SymbolInstruction sym) {
			Symbol symbol = sym.getSymbol();
			try {
				if (mode == NOPHI) {
					Stack<Integer> visibility = getVisibility(symbol);
					int top = visibility.peek();
					debug(indent+"\t-Popping "+symbol.getName()+"_"+top+" from visiblity stack "+visibility);
					visibility.pop();
				}
			} catch(Exception e) {
				debug("fatal: popping undefined var "+symbol+ " (Exception "+e.getMessage()+")");
			}
			return this;
		}


		@Override
		public Object casePhiInstruction(PhiInstruction g) {
			mode = PHI;
			return this;
		}
	}

	String indent="";
	int depth =0;
	private void renumberVariables(int depth, BasicBlock bb) {
		indent="";
		for(int i=0;i<depth;i++) {
			indent+="    ";
		}
		String origIndent = indent;
		// replace in current block
		debug(indent+"- Renumbering variables in "+bb.toShortString()+" at depth "+depth);

		(new RenumberDefUseSwitch(this)).run(bb);

		// replace phi functions in successors
		debug(indent+"- Replacing phi nodes in "+bb.toShortString()+" successor list "+bb.getSuccessors()+" :");
		for (ControlEdge e :  bb.getOutEdges()) {
			BasicBlock Y = e.getTo();
			int j = Y.whichPredecessor(bb);
			debug(indent+"  - Successor "+Y.toShortString()+" with "+bb.toShortString()+" as its "+j+"th predecessor");
			if (Y.getInstructions().size()==0) {
			} else {
				(new RenumberPhiSwitch(this, j)).run(Y);
			}
		}

		DominatorTreeNode node = getDominatorTree().node(bb);
		debug(indent+"- "+bb.toShortString()+" has "+node.children().size()+" children on its dominator Tree");
		// recurse on children
		for (DominatorTreeNode n : node.children()) {
			BasicBlock Y = n.block();
			debug(indent+"- Recursive call on "+Y);
			renumberVariables(depth+1,Y);
			indent = origIndent;
		}

		// pop the definitions off the stack
		debug(indent+"-Popping " + bb);
		(new PopVisitor()).run(bb);
	}

	/*
	 * Join computing
	 */
	private Set<BasicBlock> computeJointPoints(List<BasicBlock> S) {
		getDominanceFrontiers();

		Set<BasicBlock> D = new LinkedHashSet<BasicBlock>();
		Set<BasicBlock> DFP = dominanceList(S);
		boolean change = true;

		do {
			change = false;
			List<BasicBlock> Z = new ArrayList<BasicBlock>(S);
			Z.addAll(DFP);
			D = dominanceList(Z);
			if (! D.equals(DFP)) {	
				DFP = D;
				change = true;
			}
		} while (change);

		return D;
	}

	private Set<BasicBlock> dominanceList(List<BasicBlock> S) {
		Set<BasicBlock> D = new LinkedHashSet<BasicBlock>();
		for (BasicBlock x : S) {
			if (!dominanceFrontiers.containsKey(x)) {
				throw new RuntimeException("No dominanceFrontiers for BB ("+x.getNumber()+") in proc "+proc.getSymbol().getName()+"");
				//System.err.println("No dominanceFrontiers for BB ("+x.getNumber()+") in proc "+proc.getSymbol().getName()+"");
			} else {
				List<BasicBlock> set = dominanceFrontiers.get(x);
					D.addAll(set);

			}
		}
		return D;
	}

	@SuppressWarnings("unused")
	private void Warning(String string) {
		System.err.println(string);
	}

	private void insertJoints(List<BasicBlock> joints, Symbol sym, LivenessAnalyser live) {
		for (BasicBlock b : joints) {
			if (! live.getResultAt(b).getValue(sym)) {
				debug(indent+"     - "+sym+" is not alive in BB"+b.getNumber()+" no phi node added");
				// If value is not live, we can skip the phi node creation
				continue;
			}
			debug(indent+"     - Adding phi() for variable "+sym+" in BB"+b.getNumber());
			// We create a new phi() instruction with as many input as 
			// there are incoming BasicBlocs
			PhiInstruction inst = GecosUserInstructionFactory.phi(sym.getType());
			for (int j = 0; j < b.getInEdges().size(); ++j) {
				inst.getChildren().add(GecosUserInstructionFactory.symbref(sym));
			}
			b.prependInstruction(GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(sym), inst));
		}
	}
	private void debug(String string) {
		if (VERBOSE) System.out.println("[SSAAnalyzer]"+string);
	}
}