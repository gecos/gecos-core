/**
 */
package gecos.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter Symbol</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.core.CorePackage#getParameterSymbol()
 * @model
 * @generated
 */
public interface ParameterSymbol extends Symbol {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.core.Procedure%&gt; _xblockexpression = null;\n{\n\t&lt;%gecos.core.ScopeContainer%&gt; _container = this.getContainingScope().getContainer();\n\tfinal &lt;%gecos.core.ProcedureSymbol%&gt; procedureSymbol = ((&lt;%gecos.core.ProcedureSymbol%&gt;) _container);\n\t_xblockexpression = procedureSymbol.getProcedure();\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	Procedure findProcedure();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.core.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return super.toString();'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitParameterSymbol(this);'"
	 * @generated
	 */
	void accept(CoreVisitor visitor);

} // ParameterSymbol
