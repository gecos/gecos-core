#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>


#ifndef __TYPE__
	#define __TYPE__ float //XXX
#endif


#include "tools.h"


int main(int argc, const char **argv) {
	if(argc < 3) {
		fprintf(stderr, "Usage: %s file1 file2 <displayDiff?(0/1)>\n", argv[0]);
		return -1;
	}

	FILE *f1 = fopen(argv[1], "r");
	FILE *f2 = fopen(argv[2], "r");
	if(f1 == NULL || f2 == NULL) {
		fprintf(stderr, "couldn't open input files!\n");
		exit(-1);
	}

	int display_diff = 0;
	if(argc > 3)
		display_diff = atoi(argv[3]);

	int M, N, M2, N2;
	__TYPE__ **A1;
	__TYPE__ **A2;
	initFromFile2D(&A1, PRINT_ID(__TYPE__), &M, &N, argv[1]);
	initFromFile2D(&A2, PRINT_ID(__TYPE__), &M2, &N2, argv[2]);
	if(M != M2 || N != N2) {
		fprintf(stderr, "Matrix sizes does not match!\n");
		free2D((void **)A1, M);
		free2D((void **)A2, M);
		exit(-1);
	}

//	print(stdout, A1, "A1", M, N);
//	print(stdout, A2, "A2", M, N);

	double sumdiff = 0;
	double sumdiff2 = 0;
	double diffmax = 0;
	double diff;
	unsigned int nb_diffs = 0;
	int i,j;
	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			__TYPE__ a2 = A1[i][j] - A2[i][j];
			diff = (double)(a2 >= 0 ? a2 : -a2); //XXX y?

			if(diff > diffmax)
				diffmax = diff;
			if(diff != 0) {
				nb_diffs ++;
				sumdiff += diff;
				sumdiff2 += diff * diff;
			}

			if (display_diff != 0)
				printf("(%d, %d): %15f = %15f - %15f\n", i, j, a2, A1[i][j], A2[i][j]);
		}
	}

	double nelem = M*N;
	double mean = sumdiff / (double)nb_diffs;//XXX
	double deviation = sqrt((sumdiff2/(double)nb_diffs) - (mean*mean)); //XXX

	/*
	 * mean = sum(diff)/nelem
	 * variance = sum((diff - mean)^2) / nelem = sum(diff^2/nelem) + sum(mean^2/nelem) -2.mean.sum(diff/nelem)
	 * 			= sumdiff2/nelem + mean^2 -2.mean^2 = sumdiff2/nelem - mean^2
	 * deviation = sqrt(variance)
	 * Noise Power (dB) = 10.log10(deviation^2 + mean^2) = 10.log10(variance + mean^2) = 10.log10(sumdiff2/nelem)
	 */
	double meanT = sumdiff / nelem; //mean
	double variance = (sumdiff2/nelem) - (meanT*meanT); //variance
	double deviationT = sqrt(variance); //deviation
	double noisePower = 10 * log10(sumdiff2/nelem); // = 10 * log10(deviationT^2 + meanT^2)

	printf("\tNbElem     = %dx%d\n", M, N);
	printf("\tRATIO     = %.2f%%  (%d/%d)\n", (double)(nb_diffs*100)/nelem, nb_diffs, (int)nelem);
	printf("\tMAX  diff = %f\n", diffmax);
	printf("\tSUM  diff = %f\n", sumdiff);
//	printf("\tSUM  dif2 = %f\n", sumdiff2);
	printf("\tMEAN      = %f  (%f)\n", meanT, mean);
	printf("\tVARIANCE  = %f\n", variance);
	printf("\tDEVIATION = %f  (%f)\n", deviationT, deviation);
	printf("\tNOISE POW = %f dB\n", noisePower);

	free2D((void **)A1, M);
	free2D((void **)A2, M);

	return 0;
}
