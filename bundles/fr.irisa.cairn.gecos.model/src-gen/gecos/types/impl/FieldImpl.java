/**
 */
package gecos.types.impl;

import gecos.types.Field;
import gecos.types.RecordType;
import gecos.types.Type;
import gecos.types.TypesPackage;
import gecos.types.TypesVisitor;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Field</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.impl.FieldImpl#getRecord <em>Record</em>}</li>
 *   <li>{@link gecos.types.impl.FieldImpl#getName <em>Name</em>}</li>
 *   <li>{@link gecos.types.impl.FieldImpl#getType <em>Type</em>}</li>
 *   <li>{@link gecos.types.impl.FieldImpl#getOffset <em>Offset</em>}</li>
 *   <li>{@link gecos.types.impl.FieldImpl#getBitwidth <em>Bitwidth</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FieldImpl extends MinimalEObjectImpl.Container implements Field {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected Type type;

	/**
	 * The default value of the '{@link #getOffset() <em>Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffset()
	 * @generated
	 * @ordered
	 */
	protected static final int OFFSET_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getOffset() <em>Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffset()
	 * @generated
	 * @ordered
	 */
	protected int offset = OFFSET_EDEFAULT;

	/**
	 * The default value of the '{@link #getBitwidth() <em>Bitwidth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBitwidth()
	 * @generated
	 * @ordered
	 */
	protected static final long BITWIDTH_EDEFAULT = -1L;

	/**
	 * The cached value of the '{@link #getBitwidth() <em>Bitwidth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBitwidth()
	 * @generated
	 * @ordered
	 */
	protected long bitwidth = BITWIDTH_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FieldImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypesPackage.Literals.FIELD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordType getRecord() {
		if (eContainerFeatureID() != TypesPackage.FIELD__RECORD) return null;
		return (RecordType)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordType basicGetRecord() {
		if (eContainerFeatureID() != TypesPackage.FIELD__RECORD) return null;
		return (RecordType)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRecord(RecordType newRecord, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newRecord, TypesPackage.FIELD__RECORD, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRecord(RecordType newRecord) {
		if (newRecord != eInternalContainer() || (eContainerFeatureID() != TypesPackage.FIELD__RECORD && newRecord != null)) {
			if (EcoreUtil.isAncestor(this, newRecord))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newRecord != null)
				msgs = ((InternalEObject)newRecord).eInverseAdd(this, TypesPackage.RECORD_TYPE__FIELDS, RecordType.class, msgs);
			msgs = basicSetRecord(newRecord, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.FIELD__RECORD, newRecord, newRecord));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.FIELD__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(Type newType) {
		Type oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.FIELD__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getOffset() {
		return offset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOffset(int newOffset) {
		int oldOffset = offset;
		offset = newOffset;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.FIELD__OFFSET, oldOffset, offset));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getBitwidth() {
		return bitwidth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBitwidth(long newBitwidth) {
		long oldBitwidth = bitwidth;
		bitwidth = newBitwidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.FIELD__BITWIDTH, oldBitwidth, bitwidth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final TypesVisitor visitor) {
		visitor.visitField(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEqual(final Field field) {
		if ((field == this)) {
			return true;
		}
		boolean isSameName = false;
		if (((this.getName() == null) && (field.getName() == null))) {
			isSameName = true;
		}
		else {
			String _name = this.getName();
			boolean _tripleEquals = (_name == null);
			String _name_1 = field.getName();
			boolean _tripleEquals_1 = (_name_1 == null);
			boolean _xor = (_tripleEquals ^ _tripleEquals_1);
			if (_xor) {
				isSameName = false;
			}
			else {
				isSameName = this.getName().equals(field.getName());
			}
		}
		boolean isSameBase = false;
		if (((this.getType() == null) && (field.getType() == null))) {
			isSameBase = true;
		}
		else {
			Type _type = this.getType();
			boolean _tripleEquals_2 = (_type == null);
			Type _type_1 = field.getType();
			boolean _tripleEquals_3 = (_type_1 == null);
			boolean _xor_1 = (_tripleEquals_2 ^ _tripleEquals_3);
			if (_xor_1) {
				return false;
			}
			else {
				isSameBase = this.getType().isEqual(field.getType());
			}
		}
		return ((isSameName && isSameBase) && (this.getOffset() == field.getOffset()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TypesPackage.FIELD__RECORD:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetRecord((RecordType)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TypesPackage.FIELD__RECORD:
				return basicSetRecord(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case TypesPackage.FIELD__RECORD:
				return eInternalContainer().eInverseRemove(this, TypesPackage.RECORD_TYPE__FIELDS, RecordType.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TypesPackage.FIELD__RECORD:
				if (resolve) return getRecord();
				return basicGetRecord();
			case TypesPackage.FIELD__NAME:
				return getName();
			case TypesPackage.FIELD__TYPE:
				return getType();
			case TypesPackage.FIELD__OFFSET:
				return getOffset();
			case TypesPackage.FIELD__BITWIDTH:
				return getBitwidth();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TypesPackage.FIELD__RECORD:
				setRecord((RecordType)newValue);
				return;
			case TypesPackage.FIELD__NAME:
				setName((String)newValue);
				return;
			case TypesPackage.FIELD__TYPE:
				setType((Type)newValue);
				return;
			case TypesPackage.FIELD__OFFSET:
				setOffset((Integer)newValue);
				return;
			case TypesPackage.FIELD__BITWIDTH:
				setBitwidth((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TypesPackage.FIELD__RECORD:
				setRecord((RecordType)null);
				return;
			case TypesPackage.FIELD__NAME:
				setName(NAME_EDEFAULT);
				return;
			case TypesPackage.FIELD__TYPE:
				setType((Type)null);
				return;
			case TypesPackage.FIELD__OFFSET:
				setOffset(OFFSET_EDEFAULT);
				return;
			case TypesPackage.FIELD__BITWIDTH:
				setBitwidth(BITWIDTH_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TypesPackage.FIELD__RECORD:
				return basicGetRecord() != null;
			case TypesPackage.FIELD__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case TypesPackage.FIELD__TYPE:
				return type != null;
			case TypesPackage.FIELD__OFFSET:
				return offset != OFFSET_EDEFAULT;
			case TypesPackage.FIELD__BITWIDTH:
				return bitwidth != BITWIDTH_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", offset: ");
		result.append(offset);
		result.append(", bitwidth: ");
		result.append(bitwidth);
		result.append(')');
		return result.toString();
	}

} //FieldImpl
