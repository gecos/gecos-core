package fr.irisa.cairn.gecos.model.transforms.dag;

import fr.irisa.cairn.gecos.model.tools.visitors.GecosDagDefaultVisitor;
import gecos.core.Procedure;
import gecos.dag.DAGCallNode;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGNode;
import gecos.dag.DAGOpNode;
import gecos.dag.DAGOperator;
import gecos.dag.DAGOutNode;
import gecos.dag.DAGVectorOpNode;
import gecos.instrs.Instruction;

import java.util.List;

import org.eclipse.emf.common.util.UniqueEList;

/**
 * Build a List of tree Instructions from a {@link DAGInstruction}.
 * 
 * @author antoine
 * 
 */
public class DAGToTreeSequentialScan extends GecosDagDefaultVisitor {
	protected List<Instruction> instructions;
//	protected Procedure proc;
	public final static int DEPTH_INFINITE = -1;
	// private int maxDepth;
	
	private static final boolean DEBUG = false;
	private static int INDEX = 0;
	private static final void debug(String str) {
		if(DEBUG) {
			for(int i=0; i<INDEX; i++)
				System.out.print("   ");
			System.out.println(str);
		}
	}
	private static final void incINDEX() {
		if(DEBUG) INDEX ++;
	}
	private static final void decINDEX() {
		if(DEBUG) INDEX --;
	}
	

	protected DAGToTreeInstructionBuilder builder;

	public DAGToTreeSequentialScan(/*Procedure proc, */int maxDepth) {
//		this.proc = proc;
		builder = new DAGToTreeInstructionBuilder(maxDepth);
	}

	public DAGToTreeSequentialScan(Procedure proc) {
		this(/*proc, */DEPTH_INFINITE);
	}

	public List<Instruction> buildInstructionsList(DAGInstruction dag) {
		dag.accept(this);
		return instructions;
	}

	@Override
	public void visitDAGInstruction(DAGInstruction dag) {
		debug("DAG2Tree visit DAGInstruction: " + dag);
		incINDEX();
		
		instructions = new UniqueEList<Instruction>();
		
		for(DAGNode n : dag.getNodes())
			n.accept(this);
		
		decINDEX();
	}

	
	@Override
	public void visitDAGOutNode(DAGOutNode d) {
		debug("DAG2Tree visit OUTNode: " + d);
		incINDEX();
		builder.doSwitch(d);
		List<Instruction> res = builder.popInstructions();
		instructions.addAll(res);
		decINDEX();
		debug("|->" + res);
	}

	protected void build(DAGNode d) {
		debug("DAG2Tree build: " + d);
		builder.build(d);
		List<Instruction> res = builder.popInstructions();
		debug("|->" + res);
		instructions.addAll(res);
	}

	@Override
	public void visitDAGOpNode(DAGOpNode d) {
		if (d.getOpcode().equals(DAGOperator.SET.getLiteral()) || d.getSuccessors().size() == 0) {
			debug("DAG2Tree visit SETNode: " + d);
			incINDEX();
			build(d);
			decINDEX();
		}
	}
	
	@Override
	public void visitDAGVectorOpNode(DAGVectorOpNode d) {
		if (d.getOpcode().equals(DAGOperator.SET.getLiteral())) {
			debug("DAG2Tree visit VecSETNode: " + d);
			incINDEX();
			build(d);
			decINDEX();
		}
	}
	
	/**
	 * handle case of call without output
	 * @author aelmouss
	 */
	@Override
	public void visitDAGCallNode(DAGCallNode n) {
		if(n.getDataSuccessors().isEmpty()) {
			debug("DAG2Tree visit CALLNode: " + n);
			incINDEX();
			builder.doSwitch(n);
			List<Instruction> res = builder.popInstructions();
			instructions.addAll(res);
			decINDEX();
			debug("|->" + res);
		}
//		super.visitDAGCallNode(n);
	}


	public static class Dag2TreeException extends RuntimeException {
		private static final long serialVersionUID = 2721987374278691617L;

		public Dag2TreeException() {
			super();
		}

		public Dag2TreeException(String arg0, Throwable arg1) {
			super(arg0, arg1);
		}

		public Dag2TreeException(String arg0) {
			super(arg0);
		}

		public Dag2TreeException(Throwable arg0) {
			super(arg0);
		}

	}

	static class OperatorBuilder {
		public final static OperatorBuilder INSTANCE = new OperatorBuilder();

		private OperatorBuilder() {

		}

		public String operator(String dagop) {
			return dagop;
		}
	}

}
