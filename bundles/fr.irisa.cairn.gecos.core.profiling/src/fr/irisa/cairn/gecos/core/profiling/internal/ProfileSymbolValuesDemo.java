package fr.irisa.cairn.gecos.core.profiling.internal;

import java.nio.file.Path;
import java.nio.file.Paths;

import fr.irisa.cairn.gecos.core.profiling.SimulationBasedProfiler;
import fr.irisa.cairn.gecos.core.profiling.backend.ProfilingBackendFailure;
import fr.irisa.cairn.gecos.model.cdtfrontend.CDTFrontEnd;
import fr.irisa.cairn.gecos.model.utils.misc.PathUtils;
import fr.irisa.r2d2.gecos.framework.GSModule;
import gecos.gecosproject.GecosProject;

/**
 * @author aelmouss
 */
@GSModule("This pass is a demo for the simulation based profiler.\n"
		+ "It inserts profiling instructions where indicated by the '$save' directives\n"
		+ "in the source code. Then it compiles and run the transformed code to collect\n"
		+ "profiling information, which are printed at the end.")
public class ProfileSymbolValuesDemo {

	private GecosProject project;
	private Path outputDir;

	private boolean debug = true;


	public ProfileSymbolValuesDemo(GecosProject unparsedProjext, String outputDir) {
		this.project = unparsedProjext;
		this.outputDir = Paths.get(outputDir);
		
		parseOriginalProject();
	}

	private void parseOriginalProject() {
		//TODO add builtins definitions to avoid warnings
		CDTFrontEnd parser = new CDTFrontEnd(project);
		CDTFrontEnd.annotateFileLocations(true);
		parser.compute();
	}
	
	
	public void compute() throws ProfilingBackendFailure {
		SimulationBasedProfiler prof = new SimulationBasedProfiler(project, debug);
		
		prof.insertProfiling();
		
		/** generate generic instrumented code **/
		Path instrCodeDir = PathUtils.createDir(outputDir, "instrumented-code", true);
		prof.generateProfiledCode(instrCodeDir);
		
		prof.profile(instrCodeDir, null, null, null, null, null);
	}

}
