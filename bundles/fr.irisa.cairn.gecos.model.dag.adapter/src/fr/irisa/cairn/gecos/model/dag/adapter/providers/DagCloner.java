package fr.irisa.cairn.gecos.model.dag.adapter.providers;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.inject.Inject;

import fr.irisa.cairn.gecos.model.dag.adapter.builder.AdapterFactory;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGEdgeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGInstructionAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGNodeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGPortAdapter;
import fr.irisa.cairn.graph.GraphException;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.providers.ICloneProvider;
import fr.irisa.cairn.tools.ecore.EcoreTools;
import gecos.dag.DAGEdge;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGNode;
import gecos.dag.DAGPort;
import gecos.dag.DagFactory;
import gecos.dag.DagPackage;

/**
 * Default DAG Adapter cloner with an Injected factory.
 * 
 * @author Antoine Floc'h - Initial contribution and API
 * 
 */
public class DagCloner
		implements
		ICloneProvider<IDAGInstructionAdapter, IDAGNodeAdapter, IDAGEdgeAdapter, IDAGPortAdapter> {
	@Inject
	private AdapterFactory factory;

	public IDAGInstructionAdapter createGraph(boolean directed) {
		DAGInstruction dag = DagFactory.eINSTANCE.createDAGInstruction();
		return factory.createGraphAdapter(dag, true);
	}

	public IDAGNodeAdapter cloneNode(IDAGNodeAdapter n) {
		IDAGNodeAdapter copy = basicNodeCopy(n);
		for (IDAGPortAdapter port : n.getInputPorts()) {
			copy.addInputPort(clonePort(port));
		}
		for (IDAGPortAdapter port : n.getOutputPorts()) {
			copy.addOutputPort(clonePort(port));
		}
		return copy;
	}

	protected IDAGNodeAdapter basicNodeCopy(IDAGNodeAdapter n) {
		DAGNode adaptedNode = n.getAdaptedNode();
		//Copy the node without copying its ports
		EStructuralFeature inputsFeature = adaptedNode.eClass()
				.getEStructuralFeature(DagPackage.DAG_NODE__INPUTS);
		EStructuralFeature outputsFeature = adaptedNode.eClass()
				.getEStructuralFeature(DagPackage.DAG_NODE__OUTPUTS);
		DAGNode content = EcoreTools.copy(adaptedNode, inputsFeature,
				outputsFeature);
		IDAGNodeAdapter copy = factory.createNodeAdapter(content);
		return copy;
	}

	public IDAGEdgeAdapter cloneEdge(IDAGEdgeAdapter e) {
		IDAGEdgeAdapter copy = basicEdgeCopy(e);
		return copy;
	}

	protected IDAGEdgeAdapter basicEdgeCopy(IDAGEdgeAdapter e) {
		DAGEdge content = EcoreUtil.copy(e.getAdaptedEdge());
		IDAGEdgeAdapter copy = factory.createEdgeAdapter(content,
				e.getSourcePort(), e.getSinkPort());
		return copy;
	}

	public IDAGPortAdapter clonePort(IDAGPortAdapter p) {
		IDAGPortAdapter copy = basicPortCopy(p);
		return copy;
	}

	protected IDAGPortAdapter basicPortCopy(IDAGPortAdapter p) {
		DAGPort content = EcoreUtil.copy(p.getAdaptedPort());
		IDAGPortAdapter copy = factory.createPortAdapter(content);
		return copy;
	}

	public void reconnectClone(IDAGEdgeAdapter clone, IDAGEdgeAdapter edge,
			IDAGNodeAdapter newSource, IDAGNodeAdapter newSink)
			throws GraphException {
		int pSource = edge.getSourcePort().getAdaptedPort().getIdent();
		int pSink = edge.getSinkPort().getAdaptedPort().getIdent();

		IPort out = getOutputPort(newSource, pSource);
		clone.reconnectSourcePort(out);
		IPort inputPort = getInputPort(newSink, pSink);
		clone.reconnectSinkPort(inputPort);

	}
	
	/**
	 * Get DAG input port adapter using DAG port identifier.
	 * @param n
	 * @param p
	 * @return
	 */
	private IDAGPortAdapter getInputPort(IDAGNodeAdapter n, int p){
		for(IDAGPortAdapter pin:n.getInputPorts() ){
			if(pin.getAdaptedPort().getIdent()==p)
				return pin;
		}
		throw new IllegalArgumentException();
	}
	
	/**
	 * Get DAG output port adapter using DAG port identifier.
	 * @param n
	 * @param p
	 * @return
	 */
	private IDAGPortAdapter getOutputPort(IDAGNodeAdapter n, int p){
		for(IDAGPortAdapter pout:n.getOutputPorts() ){
			if(pout.getAdaptedPort().getIdent()==p)
				return pout;
		}
		throw new IllegalArgumentException();
	}

}
