package fr.irisa.cairn.gecos.core.profiling.backend;

public class ProfilingBackendFailure extends Exception {

	private static final long serialVersionUID = -2501286603857637027L;

	public ProfilingBackendFailure(String msg) {
		super(msg);
	}
	
	public ProfilingBackendFailure(String msg, Exception e) {
		super(msg, e);
	}

	public ProfilingBackendFailure(Exception e) {
		super(e);
	}
}
