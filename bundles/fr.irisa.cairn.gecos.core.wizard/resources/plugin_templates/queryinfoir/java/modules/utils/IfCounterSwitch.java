package $packageName$.modules.utils;

import fr.irisa.cairn.gecos.model.tools.switches.BasicBlockSwitch;
import gecos.blocks.BasicBlock;
import gecos.blocks.IfBlock;

/**
 * Implementation of a custom Switch which count each IfBlock detected.
 * 
 * This class extends BasicBlockSwitch which is a Default switch implementation.
 * BasicBlockSwitch performs a depth-first traversal of the IR until a BasicBlock is reached
 */
public class IfCounterSwitch extends BasicBlockSwitch<Boolean> {
	
	private int count = 0;
	
	
	/**
	 * Override the caseIfBlock to add the counter mechanism
	 */
	@Override
	public Boolean caseIfBlock(IfBlock b) {
		count++;
		// Call the parent method to continue the IR traversal.
		// The parent method implement the full traversal of the IfBlock.
		return super.caseIfBlock(b); 
	}
	
	/**
	 * We need to override this method because it is an abstract method in BasicBlockSwitch
	 * We Just return null because we do not have to do anything here.
	 */
	@Override
	public Boolean caseBasicBlock(BasicBlock b) {
		return null;
	}
	
	public int getCount() {
		return count;
	}

}
