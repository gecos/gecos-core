	
/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.tools.switches;


import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.Scope;
import gecos.core.ScopeContainer;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.instrs.Instruction;

public class BlockInstructionSwitch<T> extends DefaultInstructionSwitch<T> {

	protected BasicBlockSwitch bbvistor;
	T lastRes;
	
	public BlockInstructionSwitch() {
		bbvistor = new BasicBlockSwitch(this);
	}
	
	public T doSwitch(Block block) {
		bbvistor.doSwitch(block);
		return lastRes;
	}

	public T doSwitch(Procedure proc) {
		bbvistor.doSwitch(proc);
		return null;
	} 

	public T doSwitch(ProcedureSet ps) {
		caseScopeContainer(ps);
		for (Procedure proc : ps.listProcedures()) {
			bbvistor.doSwitch(proc);
		}
		return null;
	}
	
	public T doSwitch(GecosProject p) {
		for (ProcedureSet proc : p.listProcedureSets()) {
			bbvistor.doSwitch(proc);
		}
		return null;
	}

	public class BasicBlockSwitch extends BlocksDefaultSwitch<T> {
		
		BlockInstructionSwitch<T> single ;
		
		public BasicBlockSwitch(BlockInstructionSwitch<T> instvisiotr) {
			this.single=instvisiotr;
		}
		
		@Override
		public T caseBasicBlock(BasicBlock b) {
			single.caseBasicBlock(b);
			return lastRes;
		}

		@Override
		public T caseCompositeBlock(CompositeBlock b) {
			single.caseScopeContainer(b);
			return super.caseCompositeBlock(b);
		}		
	}

	public T caseScopeContainer(ScopeContainer scopeCont) {
		Scope scope = scopeCont.getScope();
		if(scope!=null) {
			for (Symbol syms : scope.getSymbols()) {
				if (syms.getValue()!=null)
					lastRes = doSwitch(syms.getValue());
			}
		}
		return lastRes;
	}
	
	public T caseBasicBlock(BasicBlock b) {
		if (b.getInstructions() == null) return lastRes;
		//iterate over a copy of the list in order to prevent from ConcurrentModificationException
		Instruction[] insts = (Instruction[]) b.getInstructions().toArray();
		for (Instruction inst  : insts) {
			lastRes = doSwitch(inst);
		}
		return lastRes;
	}
	
	
	
}
