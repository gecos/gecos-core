package gecos.core.provider.custom;

import gecos.core.Scope;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.UnexecutableCommand;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;

public abstract class TransientScopeItemProvider extends ItemProviderAdapter implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {

	@Override
	protected Command factorRemoveCommand(EditingDomain domain, CommandParameter commandParameter) {
	      return UnexecutableCommand.INSTANCE;
	}


	@Override
	protected Command factorAddCommand(EditingDomain domain, CommandParameter commandParameter) {
	      return UnexecutableCommand.INSTANCE;
	}

	@Override
	public Command createCommand(Object object, EditingDomain domain, Class<? extends Command> commandClass,
			CommandParameter commandParameter) {
	      return UnexecutableCommand.INSTANCE;
	}

	@Override
	protected Command factorMoveCommand(EditingDomain domain, CommandParameter commandParameter) {
	      return UnexecutableCommand.INSTANCE;
	}

	public TransientScopeItemProvider(AdapterFactory adapterFactory, Scope scope) {
		super(adapterFactory);
		scope.eAdapters().add(this);
	}

	@Override
	public Collection<?> getChildren(Object object) {
		return super.getChildren(target);
	}
	
	@Override
	public Collection<?> getNewChildDescriptors(Object object, EditingDomain editingDomain, Object sibling) {
		if(object instanceof EObject) {
			return super.getNewChildDescriptors(object, editingDomain, sibling);
		} else {
		    Collection<Object> newChildDescriptors = new ArrayList<Object>();
		    collectNewChildDescriptors(newChildDescriptors, object);
			return newChildDescriptors;
		}
	}

	@Override
	public Object getParent(Object object) {
		Scope scope = (Scope) ((TransientScopeItemProvider)object).target;
		return scope;
	}
	
	@Override
	protected Object getValue(EObject eObject, EStructuralFeature eStructuralFeature) {
		if(eObject!=null) {
			return super.getValue(eObject, eStructuralFeature);
		} else {
			return null;
		}
	}
	
}
