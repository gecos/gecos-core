package fr.irisa.cairn.gecos.model.tools.queries;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch;
import gecos.blocks.ForBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;

public class ForBlockSearch extends BlocksDefaultSwitch<Object>{
	
	private ProcedureSet ps;
	private Procedure proc;
	
	private EList<ForBlock> forBlocks;
	
	public ForBlockSearch(ProcedureSet ps) {
		this.ps = ps;
		this.proc=null;
	}

	public boolean add(ForBlock e) {
		if (forBlocks==null) forBlocks = new BasicEList<ForBlock>();
		return forBlocks.add(e);
	}

	public ForBlockSearch(Procedure proc) {
		this.proc = proc;
		this.ps=null;
	}
	
	public EList<ForBlock> compute() {
		if (ps!=null) {
			doSwitch(ps);
		}
		if (proc!=null) {
			doSwitch(proc);
		}
		if (forBlocks==null) forBlocks = new BasicEList<ForBlock>();
		return forBlocks; 
	}

	@Override
	public Object caseForBlock(ForBlock b) {
		add(b);
		return super.caseForBlock(b);
	}
	
	
}
