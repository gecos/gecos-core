package fr.irisa.cairn.gecos.model.factory;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import fr.irisa.cairn.gecos.model.modules.AddIncludeDirToGecosProject;
import fr.irisa.cairn.gecos.model.modules.AddSourceToGecosProject;
import fr.irisa.cairn.gecos.model.modules.CreateProject;
import gecos.blocks.BasicBlock;
import gecos.blocks.CompositeBlock;
import gecos.core.CoreFactory;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.gecosproject.GecosHeaderDirectory;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;
import gecos.gecosproject.GecosprojectFactory;
import gecos.instrs.Instruction;
import gecos.types.FunctionType;
import gecos.types.Type;

/**
 * Quite smart Factory to help to build EObject available in the gecos.core package.
 */
public class GecosUserCoreFactory {
	
	public static final String DEFAULT_PROJECT_NAME = "gecos_project";
	
	private static CoreFactory factory = CoreFactory.eINSTANCE;

	private GecosUserCoreFactory() {
	}

	public static ProcedureSet procedureSet(Procedure... procs) {
		ProcedureSet ps = factory.createProcedureSet();
		ps.setScope(scope());
		for (Procedure procedure : procs) {
			ps.addProcedure(procedure);
		}
		return ps;
	}
	
	public static Procedure proc(ProcedureSet ps, ProcedureSymbol psym, CompositeBlock body) {
		if (psym == null)
			throw new IllegalArgumentException();
		Procedure procedure = factory.createProcedure();
		procedure.setSymbol(psym);
		
		if (ps != null) {
			ps.getScope().getSymbols().add(psym);
			ps.addProcedure(procedure);
		}

		//set body
		if (body != null) {
			CompositeBlock intermediateBody = GecosUserBlockFactory.CompositeBlock();
			BasicBlock start = GecosUserBlockFactory.BBlock();
			BasicBlock end = GecosUserBlockFactory.BBlock();
			intermediateBody.addChildren(start);
			intermediateBody.addChildren(body);
			intermediateBody.addChildren(end);
			procedure.setStart(start);
			procedure.setEnd(end);
			procedure.setBody(intermediateBody);
		}
		return procedure;
	}
	
	public static Procedure proc(ProcedureSymbol procedureSymbol) {
		return proc(null, procedureSymbol, null);
	}
	
	@Deprecated
	public static Procedure proc(ProcedureSet ps,String name, Type resType ,CompositeBlock body, List<ParameterSymbol> params) {
		ProcedureSymbol procSymbol = procSymbol(name, resType,params);
		return proc(ps, procSymbol, body);
	}

	
	public static ParameterSymbol paramSymbol(String name, Type type) {
		ParameterSymbol procSym = factory.createParameterSymbol();
		procSym.setName(name);
		procSym.setType(type);
		return procSym ;
	}
	
	
	public static ProcedureSymbol procSymbol(String name, FunctionType funcType) {
		Scope params = scope(funcType.listParameters().stream()
			.map(p -> paramSymbol("", p))
			.toArray(size -> new ParameterSymbol[size])
		);
		return _procSymbol(name, funcType, params);
	}
	
	public static ProcedureSymbol procSymbol(String name, FunctionType funcType, Scope params) {
		List<Type> parameterTypes = funcType.listParameters();
		List<Symbol> paramSymbols = params.getSymbols();
		if(parameterTypes.size() != paramSymbols.size() 
				|| IntStream.range(0,parameterTypes.size())
					.mapToObj(i -> paramSymbols.get(i).getType().isEqual(parameterTypes.get(i)))
					.anyMatch(isEquals -> isEquals == false))
			throw new RuntimeException("Parameter Types do not match functionType for '" + name + "': " + funcType + " , " + params);
		return _procSymbol(name, funcType, params);
	}

	public static ProcedureSymbol procSymbol(String name, Type returnType, Scope params, boolean hasElipsis) {
		Type[] parameterTypes = params.getSymbols().stream()
			.map(s -> s.getType())
			.toArray(size -> new Type[size]);
		FunctionType funcType = GecosUserTypeFactory.FUNCTION(returnType, hasElipsis, parameterTypes);
		return _procSymbol(name, funcType, params);
	}
	
	private static ProcedureSymbol _procSymbol(String name, FunctionType funcType, Scope params) {
		ProcedureSymbol procedureSymbol = factory.createProcedureSymbol();
		procedureSymbol.setName(name);
		procedureSymbol.setType(funcType);
		procedureSymbol.setScope(params);
		return procedureSymbol;
	}
	
	/**
	 * @deprecated use {@link #procSymbol(String, Type, Scope, boolean)}
	 * with {@link #scope(Collection)} to create scope from {@code params}
	 */
	@Deprecated
	public static ProcedureSymbol procSymbol(String name, Type resType, List<ParameterSymbol> params, boolean hasElipsis) {
		Scope s;
		if(params == null || params.isEmpty())
			s = scope();
		else
			s = scope(params.toArray(new Symbol[params.size()]));
		return procSymbol(name, resType, s, hasElipsis);
	}
	
	public static ProcedureSymbol procSymbol(String name, Type resType, List<ParameterSymbol> params) {
		return procSymbol(name, resType, params, false);
	}
	
	
	public static Scope scope(Symbol... symbols) {
		return scope(Arrays.asList(symbols));
	}
	
	public static Scope scope(Collection<Symbol> symbols) {
		Scope scope = factory.createScope();
		scope.getSymbols().addAll(symbols);
		return scope;
	}
	
	
	public static Symbol symbol(String name, Type type, Scope scope, Instruction value) {
		if(type==null) 
			throw new IllegalArgumentException("Cannot create symbol with null type");

		Symbol sym = factory.createSymbol();
		sym.setName(name);
		sym.setType(type);
		if (value != null)
			sym.setValue(value);
		if (scope != null) {
			for (Symbol s : scope.getSymbols()) {
				if (s.getName().equals(name)) {
					throw new RuntimeException("Symbol with name \""+name+"\" already declared in current Scope : "+s+" while trying to create "+name+":"+type);
				}
			}
			scope.getSymbols().add(sym);
		}
		return sym;
	}

	public static Symbol symbol(String name, Type type, Instruction value) {
		return symbol(name, type, null, value);
	}
	public static Symbol symbol(String name, Type type, Scope scope) {
		return symbol(name, type, scope, null);
	}
	
	public static Symbol symbol(String name, Type type) {
		return symbol(name, type, null, null);
	}
	
	/**
	 * Create a temporary symbol with the name {@code namePrefix}[UID].
	 * Where UID is number that might be added to make the symbol unique in the specified {@code scope}.
	 * if namePrefix is null or empty then a default prefix (_t) is used 
	 * 
	 * @param namePrefix
	 * @param type
	 * @param scope
	 * @return
	 */
	public static Symbol tmpSymbol(String namePrefix, Type type, Scope scope) {
		int uid = 0;
		String name = namePrefix;
		if(name == null || name.isEmpty())
			name = "_t";
		
		while (scope.lookup(name) != null) {
			name = namePrefix + uid++;
		}
		return symbol(name, type, scope);
	}
	
	public static GecosProject project(String name, String ... sources) {
		GecosProject project = new CreateProject(name).compute();
		new AddSourceToGecosProject(project, sources).compute();
		return project;
	}
	
	public static GecosProject projectSources(String name, GecosSourceFile ... sources) {
		GecosProject project = new CreateProject(name).compute();
		new AddSourceToGecosProject(project, sources).compute();
		return project;
	}
	
	public static GecosProject project(String projectName, Collection<String> sourceFiles, Collection<String> includeDirs) {
		GecosProject project = new CreateProject(projectName).compute();
		new AddSourceToGecosProject(project, sourceFiles.toArray(new String[sourceFiles.size()])).compute();
		new AddIncludeDirToGecosProject(project, includeDirs.toArray(new String[includeDirs.size()])).compute();
		return project;
	}
	
	public static GecosProject project(String projectName, Path[] srcFiles, Path[] incDirs) {
		return project(projectName, 
				Arrays.stream(srcFiles).map(Path::toString).collect(Collectors.toList()),
				Arrays.stream(incDirs).map(Path::toString).collect(Collectors.toList()));
	}

	public static GecosHeaderDirectory includeDir(String path) {
		return includeDir(path, false);
	}
	
	public static GecosHeaderDirectory includeDir(String path, boolean isStandardLibrary) {
		GecosHeaderDirectory res = GecosprojectFactory.eINSTANCE.createGecosHeaderDirectory();
		res.setName(path);
		res.setIsStandardHeader(isStandardLibrary);
		return res;
	}
	
	public static GecosSourceFile source(String path, ProcedureSet model) {
		GecosSourceFile res = GecosprojectFactory.eINSTANCE.createGecosSourceFile();
		res.setModel(model);
		res.setName(path);
		return res;
	}
	
	public static GecosSourceFile source(String path) {
		return source(path,null);
	}

}