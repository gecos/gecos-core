/**
 */
package gecos.dag;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Vector Array Access Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.dag.DagPackage#getDAGVectorArrayAccessNode()
 * @model
 * @generated
 */
public interface DAGVectorArrayAccessNode extends DAGVectorNode, DAGSimpleArrayNode, DagVisitable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGVectorArrayAccessNode(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getPredecessors().get(1);'"
	 * @generated
	 */
	DAGNode getIndex();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.dag.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _ident = this.getIdent();\nreturn (\"VAN_\" + &lt;%java.lang.Integer%&gt;.valueOf(_ident));'"
	 * @generated
	 */
	String toString();

} // DAGVectorArrayAccessNode
