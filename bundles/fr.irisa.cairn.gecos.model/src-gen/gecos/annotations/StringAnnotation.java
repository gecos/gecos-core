/**
 */
package gecos.annotations;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>String Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.annotations.StringAnnotation#getContent <em>Content</em>}</li>
 * </ul>
 *
 * @see gecos.annotations.AnnotationsPackage#getStringAnnotation()
 * @model
 * @generated
 */
public interface StringAnnotation extends IAnnotation {
	/**
	 * Returns the value of the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Content</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content</em>' attribute.
	 * @see #setContent(String)
	 * @see gecos.annotations.AnnotationsPackage#getStringAnnotation_Content()
	 * @model unique="false" dataType="gecos.annotations.String"
	 * @generated
	 */
	String getContent();

	/**
	 * Sets the value of the '{@link gecos.annotations.StringAnnotation#getContent <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content</em>' attribute.
	 * @see #getContent()
	 * @generated
	 */
	void setContent(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.annotations.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getContent();'"
	 * @generated
	 */
	String toString();

} // StringAnnotation
