package fr.irisa.cairn.gecos.model.cdtfrontend.initcdt;

import org.eclipse.cdt.core.settings.model.extension.impl.CDefaultConfigurationDataProvider;

import fr.irisa.cairn.gecos.model.cdtfrontend.CDTFrontEnd;

/**
 * 
 * Simple extension.
 * 
 * @author amorvan
 *
 */
public class GecosCDTDataProvider extends CDefaultConfigurationDataProvider {
	public static final String PROVIDER_ID = CDTFrontEnd.PLUGIN_ID + ".gecosCDTDataProvider";
}
