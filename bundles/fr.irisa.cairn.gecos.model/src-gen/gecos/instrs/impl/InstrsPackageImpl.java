/**
 */
package gecos.instrs.impl;

import gecos.annotations.AnnotationsPackage;

import gecos.annotations.impl.AnnotationsPackageImpl;

import gecos.blocks.BlocksPackage;

import gecos.blocks.impl.BlocksPackageImpl;

import gecos.core.CorePackage;

import gecos.core.impl.CorePackageImpl;

import gecos.dag.DagPackage;

import gecos.dag.impl.DagPackageImpl;

import gecos.instrs.AddressComponent;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArithmeticInstruction;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.ArrayValueInstruction;
import gecos.instrs.BitwiseInstruction;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchInstruction;
import gecos.instrs.BranchType;
import gecos.instrs.BreakInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.CaseInstruction;
import gecos.instrs.ChildrenListInstruction;
import gecos.instrs.ComparisonInstruction;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.ComponentInstruction;
import gecos.instrs.CondInstruction;
import gecos.instrs.ConstantInstruction;
import gecos.instrs.ContinueInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.DummyInstruction;
import gecos.instrs.EnumeratorInstruction;
import gecos.instrs.ExprComponent;
import gecos.instrs.FieldInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.GotoInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.InstrsFactory;
import gecos.instrs.InstrsPackage;
import gecos.instrs.InstrsVisitable;
import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.LabelInstruction;
import gecos.instrs.LogicalInstruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.MethodCallInstruction;
import gecos.instrs.NumberedSymbolInstruction;
import gecos.instrs.PhiInstruction;
import gecos.instrs.RangeArrayInstruction;
import gecos.instrs.RangeInstruction;
import gecos.instrs.ReductionOperator;
import gecos.instrs.RetInstruction;
import gecos.instrs.SSADefSymbol;
import gecos.instrs.SSAUseSymbol;
import gecos.instrs.SelectOperator;
import gecos.instrs.SetInstruction;
import gecos.instrs.SimdExpandInstruction;
import gecos.instrs.SimdExtractInstruction;
import gecos.instrs.SimdGenericInstruction;
import gecos.instrs.SimdInstruction;
import gecos.instrs.SimdPackInstruction;
import gecos.instrs.SimdShuffleInstruction;
import gecos.instrs.SimpleArrayInstruction;
import gecos.instrs.SizeofInstruction;
import gecos.instrs.SizeofTypeInstruction;
import gecos.instrs.SizeofValueInstruction;
import gecos.instrs.StringInstruction;
import gecos.instrs.SymbolInstruction;

import gecos.types.TypesPackage;

import gecos.types.impl.TypesPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class InstrsPackageImpl extends EPackageImpl implements InstrsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass instrsVisitorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass instrsVisitableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass instructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dummyInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass labelInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass symbolInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass numberedSymbolInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ssaDefSymbolEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ssaUseSymbolEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sizeofInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sizeofTypeInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sizeofValueInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constantInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass intInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass floatInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass complexInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass childrenListInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass arrayValueInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass phiInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass setInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass addressComponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass addressInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass indirInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass callInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass methodCallInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass arrayInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simpleArrayInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rangeArrayInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass branchInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass condInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass breakInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass continueInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gotoInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass exprComponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass retInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass convertInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fieldInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass enumeratorInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rangeInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass caseInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass genericInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass arithmeticInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass comparisonInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass logicalInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bitwiseInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simdInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simdGenericInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simdPackInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simdExtractInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simdShuffleInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simdExpandInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum arithmeticOperatorEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum comparisonOperatorEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum logicalOperatorEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum bitwiseOperatorEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum selectOperatorEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum reductionOperatorEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum branchTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType stringEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType intEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType longEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType doubleEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see gecos.instrs.InstrsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private InstrsPackageImpl() {
		super(eNS_URI, InstrsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link InstrsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static InstrsPackage init() {
		if (isInited) return (InstrsPackage)EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI);

		// Obtain or create and register package
		InstrsPackageImpl theInstrsPackage = (InstrsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof InstrsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new InstrsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		DagPackageImpl theDagPackage = (DagPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DagPackage.eNS_URI) instanceof DagPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DagPackage.eNS_URI) : DagPackage.eINSTANCE);
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
		AnnotationsPackageImpl theAnnotationsPackage = (AnnotationsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI) instanceof AnnotationsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI) : AnnotationsPackage.eINSTANCE);
		TypesPackageImpl theTypesPackage = (TypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI) instanceof TypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI) : TypesPackage.eINSTANCE);
		BlocksPackageImpl theBlocksPackage = (BlocksPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI) instanceof BlocksPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI) : BlocksPackage.eINSTANCE);

		// Create package meta-data objects
		theInstrsPackage.createPackageContents();
		theDagPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theAnnotationsPackage.createPackageContents();
		theTypesPackage.createPackageContents();
		theBlocksPackage.createPackageContents();

		// Initialize created meta-data
		theInstrsPackage.initializePackageContents();
		theDagPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theAnnotationsPackage.initializePackageContents();
		theTypesPackage.initializePackageContents();
		theBlocksPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theInstrsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(InstrsPackage.eNS_URI, theInstrsPackage);
		return theInstrsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInstrsVisitor() {
		return instrsVisitorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInstrsVisitable() {
		return instrsVisitableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInstruction() {
		return instructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInstruction____internal_cached___type() {
		return (EReference)instructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDummyInstruction() {
		return dummyInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLabelInstruction() {
		return labelInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLabelInstruction_Name() {
		return (EAttribute)labelInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSymbolInstruction() {
		return symbolInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSymbolInstruction_Symbol() {
		return (EReference)symbolInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNumberedSymbolInstruction() {
		return numberedSymbolInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNumberedSymbolInstruction_Number() {
		return (EAttribute)numberedSymbolInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSSADefSymbol() {
		return ssaDefSymbolEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSSADefSymbol_SSAUses() {
		return (EReference)ssaDefSymbolEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSSAUseSymbol() {
		return ssaUseSymbolEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSSAUseSymbol_Def() {
		return (EReference)ssaUseSymbolEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSizeofInstruction() {
		return sizeofInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSizeofTypeInstruction() {
		return sizeofTypeInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSizeofTypeInstruction_TargetType() {
		return (EReference)sizeofTypeInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSizeofValueInstruction() {
		return sizeofValueInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConstantInstruction() {
		return constantInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntInstruction() {
		return intInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntInstruction_Value() {
		return (EAttribute)intInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFloatInstruction() {
		return floatInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFloatInstruction_Value() {
		return (EAttribute)floatInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringInstruction() {
		return stringInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringInstruction_Value() {
		return (EAttribute)stringInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComplexInstruction() {
		return complexInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChildrenListInstruction() {
		return childrenListInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChildrenListInstruction_Children() {
		return (EReference)childrenListInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getArrayValueInstruction() {
		return arrayValueInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhiInstruction() {
		return phiInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponentInstruction() {
		return componentInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSetInstruction() {
		return setInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSetInstruction_Dest() {
		return (EReference)setInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSetInstruction_Source() {
		return (EReference)setInstructionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAddressComponent() {
		return addressComponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAddressComponent_Address() {
		return (EReference)addressComponentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAddressInstruction() {
		return addressInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIndirInstruction() {
		return indirInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCallInstruction() {
		return callInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCallInstruction_Args() {
		return (EReference)callInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMethodCallInstruction() {
		return methodCallInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethodCallInstruction_Args() {
		return (EReference)methodCallInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMethodCallInstruction_Name() {
		return (EAttribute)methodCallInstructionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getArrayInstruction() {
		return arrayInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getArrayInstruction_Dest() {
		return (EReference)arrayInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getArrayInstruction_Index() {
		return (EReference)arrayInstructionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimpleArrayInstruction() {
		return simpleArrayInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRangeArrayInstruction() {
		return rangeArrayInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBranchInstruction() {
		return branchInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCondInstruction() {
		return condInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCondInstruction_Label() {
		return (EAttribute)condInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCondInstruction_Cond() {
		return (EReference)condInstructionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBreakInstruction() {
		return breakInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getContinueInstruction() {
		return continueInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGotoInstruction() {
		return gotoInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGotoInstruction_LabelInstruction() {
		return (EReference)gotoInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGotoInstruction_LabelName() {
		return (EAttribute)gotoInstructionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExprComponent() {
		return exprComponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExprComponent_Expr() {
		return (EReference)exprComponentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRetInstruction() {
		return retInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConvertInstruction() {
		return convertInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFieldInstruction() {
		return fieldInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFieldInstruction_Field() {
		return (EReference)fieldInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnumeratorInstruction() {
		return enumeratorInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEnumeratorInstruction_Enumerator() {
		return (EReference)enumeratorInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRangeInstruction() {
		return rangeInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRangeInstruction_Low() {
		return (EAttribute)rangeInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRangeInstruction_High() {
		return (EAttribute)rangeInstructionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCaseInstruction() {
		return caseInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGenericInstruction() {
		return genericInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGenericInstruction_Name() {
		return (EAttribute)genericInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getArithmeticInstruction() {
		return arithmeticInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getArithmeticInstruction_Operator() {
		return (EAttribute)arithmeticInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComparisonInstruction() {
		return comparisonInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComparisonInstruction_Operator() {
		return (EAttribute)comparisonInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLogicalInstruction() {
		return logicalInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLogicalInstruction_Operator() {
		return (EAttribute)logicalInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBitwiseInstruction() {
		return bitwiseInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBitwiseInstruction_Operator() {
		return (EAttribute)bitwiseInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimdInstruction() {
		return simdInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimdGenericInstruction() {
		return simdGenericInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimdPackInstruction() {
		return simdPackInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimdExtractInstruction() {
		return simdExtractInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimdShuffleInstruction() {
		return simdShuffleInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimdShuffleInstruction_Permutation() {
		return (EAttribute)simdShuffleInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimdExpandInstruction() {
		return simdExpandInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimdExpandInstruction_Permutation() {
		return (EAttribute)simdExpandInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getArithmeticOperator() {
		return arithmeticOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getComparisonOperator() {
		return comparisonOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getLogicalOperator() {
		return logicalOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getBitwiseOperator() {
		return bitwiseOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSelectOperator() {
		return selectOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getReductionOperator() {
		return reductionOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getBranchType() {
		return branchTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getString() {
		return stringEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getint() {
		return intEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getlong() {
		return longEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getdouble() {
		return doubleEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InstrsFactory getInstrsFactory() {
		return (InstrsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		instrsVisitorEClass = createEClass(INSTRS_VISITOR);

		instrsVisitableEClass = createEClass(INSTRS_VISITABLE);

		instructionEClass = createEClass(INSTRUCTION);
		createEReference(instructionEClass, INSTRUCTION__INTERNAL_CACHED_TYPE);

		dummyInstructionEClass = createEClass(DUMMY_INSTRUCTION);

		labelInstructionEClass = createEClass(LABEL_INSTRUCTION);
		createEAttribute(labelInstructionEClass, LABEL_INSTRUCTION__NAME);

		symbolInstructionEClass = createEClass(SYMBOL_INSTRUCTION);
		createEReference(symbolInstructionEClass, SYMBOL_INSTRUCTION__SYMBOL);

		numberedSymbolInstructionEClass = createEClass(NUMBERED_SYMBOL_INSTRUCTION);
		createEAttribute(numberedSymbolInstructionEClass, NUMBERED_SYMBOL_INSTRUCTION__NUMBER);

		ssaDefSymbolEClass = createEClass(SSA_DEF_SYMBOL);
		createEReference(ssaDefSymbolEClass, SSA_DEF_SYMBOL__SSA_USES);

		ssaUseSymbolEClass = createEClass(SSA_USE_SYMBOL);
		createEReference(ssaUseSymbolEClass, SSA_USE_SYMBOL__DEF);

		sizeofInstructionEClass = createEClass(SIZEOF_INSTRUCTION);

		sizeofTypeInstructionEClass = createEClass(SIZEOF_TYPE_INSTRUCTION);
		createEReference(sizeofTypeInstructionEClass, SIZEOF_TYPE_INSTRUCTION__TARGET_TYPE);

		sizeofValueInstructionEClass = createEClass(SIZEOF_VALUE_INSTRUCTION);

		constantInstructionEClass = createEClass(CONSTANT_INSTRUCTION);

		intInstructionEClass = createEClass(INT_INSTRUCTION);
		createEAttribute(intInstructionEClass, INT_INSTRUCTION__VALUE);

		floatInstructionEClass = createEClass(FLOAT_INSTRUCTION);
		createEAttribute(floatInstructionEClass, FLOAT_INSTRUCTION__VALUE);

		stringInstructionEClass = createEClass(STRING_INSTRUCTION);
		createEAttribute(stringInstructionEClass, STRING_INSTRUCTION__VALUE);

		complexInstructionEClass = createEClass(COMPLEX_INSTRUCTION);

		childrenListInstructionEClass = createEClass(CHILDREN_LIST_INSTRUCTION);
		createEReference(childrenListInstructionEClass, CHILDREN_LIST_INSTRUCTION__CHILDREN);

		arrayValueInstructionEClass = createEClass(ARRAY_VALUE_INSTRUCTION);

		phiInstructionEClass = createEClass(PHI_INSTRUCTION);

		componentInstructionEClass = createEClass(COMPONENT_INSTRUCTION);

		setInstructionEClass = createEClass(SET_INSTRUCTION);
		createEReference(setInstructionEClass, SET_INSTRUCTION__DEST);
		createEReference(setInstructionEClass, SET_INSTRUCTION__SOURCE);

		addressComponentEClass = createEClass(ADDRESS_COMPONENT);
		createEReference(addressComponentEClass, ADDRESS_COMPONENT__ADDRESS);

		addressInstructionEClass = createEClass(ADDRESS_INSTRUCTION);

		indirInstructionEClass = createEClass(INDIR_INSTRUCTION);

		callInstructionEClass = createEClass(CALL_INSTRUCTION);
		createEReference(callInstructionEClass, CALL_INSTRUCTION__ARGS);

		methodCallInstructionEClass = createEClass(METHOD_CALL_INSTRUCTION);
		createEReference(methodCallInstructionEClass, METHOD_CALL_INSTRUCTION__ARGS);
		createEAttribute(methodCallInstructionEClass, METHOD_CALL_INSTRUCTION__NAME);

		arrayInstructionEClass = createEClass(ARRAY_INSTRUCTION);
		createEReference(arrayInstructionEClass, ARRAY_INSTRUCTION__DEST);
		createEReference(arrayInstructionEClass, ARRAY_INSTRUCTION__INDEX);

		simpleArrayInstructionEClass = createEClass(SIMPLE_ARRAY_INSTRUCTION);

		rangeArrayInstructionEClass = createEClass(RANGE_ARRAY_INSTRUCTION);

		branchInstructionEClass = createEClass(BRANCH_INSTRUCTION);

		condInstructionEClass = createEClass(COND_INSTRUCTION);
		createEAttribute(condInstructionEClass, COND_INSTRUCTION__LABEL);
		createEReference(condInstructionEClass, COND_INSTRUCTION__COND);

		breakInstructionEClass = createEClass(BREAK_INSTRUCTION);

		continueInstructionEClass = createEClass(CONTINUE_INSTRUCTION);

		gotoInstructionEClass = createEClass(GOTO_INSTRUCTION);
		createEReference(gotoInstructionEClass, GOTO_INSTRUCTION__LABEL_INSTRUCTION);
		createEAttribute(gotoInstructionEClass, GOTO_INSTRUCTION__LABEL_NAME);

		exprComponentEClass = createEClass(EXPR_COMPONENT);
		createEReference(exprComponentEClass, EXPR_COMPONENT__EXPR);

		retInstructionEClass = createEClass(RET_INSTRUCTION);

		convertInstructionEClass = createEClass(CONVERT_INSTRUCTION);

		fieldInstructionEClass = createEClass(FIELD_INSTRUCTION);
		createEReference(fieldInstructionEClass, FIELD_INSTRUCTION__FIELD);

		enumeratorInstructionEClass = createEClass(ENUMERATOR_INSTRUCTION);
		createEReference(enumeratorInstructionEClass, ENUMERATOR_INSTRUCTION__ENUMERATOR);

		rangeInstructionEClass = createEClass(RANGE_INSTRUCTION);
		createEAttribute(rangeInstructionEClass, RANGE_INSTRUCTION__LOW);
		createEAttribute(rangeInstructionEClass, RANGE_INSTRUCTION__HIGH);

		caseInstructionEClass = createEClass(CASE_INSTRUCTION);

		genericInstructionEClass = createEClass(GENERIC_INSTRUCTION);
		createEAttribute(genericInstructionEClass, GENERIC_INSTRUCTION__NAME);

		arithmeticInstructionEClass = createEClass(ARITHMETIC_INSTRUCTION);
		createEAttribute(arithmeticInstructionEClass, ARITHMETIC_INSTRUCTION__OPERATOR);

		comparisonInstructionEClass = createEClass(COMPARISON_INSTRUCTION);
		createEAttribute(comparisonInstructionEClass, COMPARISON_INSTRUCTION__OPERATOR);

		logicalInstructionEClass = createEClass(LOGICAL_INSTRUCTION);
		createEAttribute(logicalInstructionEClass, LOGICAL_INSTRUCTION__OPERATOR);

		bitwiseInstructionEClass = createEClass(BITWISE_INSTRUCTION);
		createEAttribute(bitwiseInstructionEClass, BITWISE_INSTRUCTION__OPERATOR);

		simdInstructionEClass = createEClass(SIMD_INSTRUCTION);

		simdGenericInstructionEClass = createEClass(SIMD_GENERIC_INSTRUCTION);

		simdPackInstructionEClass = createEClass(SIMD_PACK_INSTRUCTION);

		simdExtractInstructionEClass = createEClass(SIMD_EXTRACT_INSTRUCTION);

		simdShuffleInstructionEClass = createEClass(SIMD_SHUFFLE_INSTRUCTION);
		createEAttribute(simdShuffleInstructionEClass, SIMD_SHUFFLE_INSTRUCTION__PERMUTATION);

		simdExpandInstructionEClass = createEClass(SIMD_EXPAND_INSTRUCTION);
		createEAttribute(simdExpandInstructionEClass, SIMD_EXPAND_INSTRUCTION__PERMUTATION);

		// Create enums
		arithmeticOperatorEEnum = createEEnum(ARITHMETIC_OPERATOR);
		comparisonOperatorEEnum = createEEnum(COMPARISON_OPERATOR);
		logicalOperatorEEnum = createEEnum(LOGICAL_OPERATOR);
		bitwiseOperatorEEnum = createEEnum(BITWISE_OPERATOR);
		selectOperatorEEnum = createEEnum(SELECT_OPERATOR);
		reductionOperatorEEnum = createEEnum(REDUCTION_OPERATOR);
		branchTypeEEnum = createEEnum(BRANCH_TYPE);

		// Create data types
		stringEDataType = createEDataType(STRING);
		intEDataType = createEDataType(INT);
		longEDataType = createEDataType(LONG);
		doubleEDataType = createEDataType(DOUBLE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		DagPackage theDagPackage = (DagPackage)EPackage.Registry.INSTANCE.getEPackage(DagPackage.eNS_URI);
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		AnnotationsPackage theAnnotationsPackage = (AnnotationsPackage)EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		BlocksPackage theBlocksPackage = (BlocksPackage)EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		instrsVisitableEClass.getESuperTypes().add(theCorePackage.getGecosNode());
		instructionEClass.getESuperTypes().add(theAnnotationsPackage.getAnnotatedElement());
		instructionEClass.getESuperTypes().add(this.getInstrsVisitable());
		instructionEClass.getESuperTypes().add(theCorePackage.getITypedElement());
		dummyInstructionEClass.getESuperTypes().add(this.getInstruction());
		labelInstructionEClass.getESuperTypes().add(this.getInstruction());
		symbolInstructionEClass.getESuperTypes().add(this.getInstruction());
		symbolInstructionEClass.getESuperTypes().add(theCorePackage.getISymbolUse());
		numberedSymbolInstructionEClass.getESuperTypes().add(this.getSymbolInstruction());
		ssaDefSymbolEClass.getESuperTypes().add(this.getNumberedSymbolInstruction());
		ssaUseSymbolEClass.getESuperTypes().add(this.getNumberedSymbolInstruction());
		sizeofInstructionEClass.getESuperTypes().add(this.getInstruction());
		sizeofTypeInstructionEClass.getESuperTypes().add(this.getSizeofInstruction());
		sizeofValueInstructionEClass.getESuperTypes().add(this.getExprComponent());
		sizeofValueInstructionEClass.getESuperTypes().add(this.getSizeofInstruction());
		constantInstructionEClass.getESuperTypes().add(this.getInstruction());
		intInstructionEClass.getESuperTypes().add(this.getConstantInstruction());
		floatInstructionEClass.getESuperTypes().add(this.getConstantInstruction());
		stringInstructionEClass.getESuperTypes().add(this.getConstantInstruction());
		complexInstructionEClass.getESuperTypes().add(this.getInstruction());
		childrenListInstructionEClass.getESuperTypes().add(this.getComplexInstruction());
		arrayValueInstructionEClass.getESuperTypes().add(this.getChildrenListInstruction());
		phiInstructionEClass.getESuperTypes().add(this.getChildrenListInstruction());
		componentInstructionEClass.getESuperTypes().add(this.getComplexInstruction());
		setInstructionEClass.getESuperTypes().add(this.getComponentInstruction());
		addressComponentEClass.getESuperTypes().add(this.getComponentInstruction());
		addressInstructionEClass.getESuperTypes().add(this.getAddressComponent());
		indirInstructionEClass.getESuperTypes().add(this.getAddressComponent());
		callInstructionEClass.getESuperTypes().add(this.getAddressComponent());
		methodCallInstructionEClass.getESuperTypes().add(this.getAddressComponent());
		arrayInstructionEClass.getESuperTypes().add(this.getComponentInstruction());
		simpleArrayInstructionEClass.getESuperTypes().add(this.getArrayInstruction());
		simpleArrayInstructionEClass.getESuperTypes().add(theCorePackage.getISymbolUse());
		rangeArrayInstructionEClass.getESuperTypes().add(this.getArrayInstruction());
		rangeArrayInstructionEClass.getESuperTypes().add(theCorePackage.getISymbolUse());
		branchInstructionEClass.getESuperTypes().add(this.getComponentInstruction());
		condInstructionEClass.getESuperTypes().add(this.getBranchInstruction());
		breakInstructionEClass.getESuperTypes().add(this.getBranchInstruction());
		continueInstructionEClass.getESuperTypes().add(this.getBranchInstruction());
		gotoInstructionEClass.getESuperTypes().add(this.getBranchInstruction());
		exprComponentEClass.getESuperTypes().add(this.getComponentInstruction());
		retInstructionEClass.getESuperTypes().add(this.getExprComponent());
		convertInstructionEClass.getESuperTypes().add(this.getExprComponent());
		fieldInstructionEClass.getESuperTypes().add(this.getExprComponent());
		enumeratorInstructionEClass.getESuperTypes().add(this.getInstruction());
		rangeInstructionEClass.getESuperTypes().add(this.getExprComponent());
		caseInstructionEClass.getESuperTypes().add(this.getExprComponent());
		genericInstructionEClass.getESuperTypes().add(this.getChildrenListInstruction());
		arithmeticInstructionEClass.getESuperTypes().add(this.getGenericInstruction());
		comparisonInstructionEClass.getESuperTypes().add(this.getGenericInstruction());
		logicalInstructionEClass.getESuperTypes().add(this.getGenericInstruction());
		bitwiseInstructionEClass.getESuperTypes().add(this.getGenericInstruction());
		simdInstructionEClass.getESuperTypes().add(this.getChildrenListInstruction());
		simdGenericInstructionEClass.getESuperTypes().add(this.getGenericInstruction());
		simdGenericInstructionEClass.getESuperTypes().add(this.getSimdInstruction());
		simdPackInstructionEClass.getESuperTypes().add(this.getSimdInstruction());
		simdExtractInstructionEClass.getESuperTypes().add(this.getSimdInstruction());
		simdShuffleInstructionEClass.getESuperTypes().add(this.getSimdInstruction());
		simdExpandInstructionEClass.getESuperTypes().add(this.getSimdInstruction());

		// Initialize classes and features; add operations and parameters
		initEClass(instrsVisitorEClass, InstrsVisitor.class, "InstrsVisitor", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = addEOperation(instrsVisitorEClass, null, "visitGenericInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getGenericInstruction(), "g", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitAddressInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAddressInstruction(), "a", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitArrayValueInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getArrayValueInstruction(), "a", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitCallInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCallInstruction(), "c", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitConvertInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getConvertInstruction(), "c", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitSizeofInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSizeofInstruction(), "s", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitSizeofValueInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSizeofValueInstruction(), "s", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitSizeofTypeInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSizeofTypeInstruction(), "s", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitFieldInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getFieldInstruction(), "f", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitEnumeratorInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEnumeratorInstruction(), "enumerator", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitFloatInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getFloatInstruction(), "f", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitIntInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIntInstruction(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitIndirInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIndirInstruction(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitLabelInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getLabelInstruction(), "l", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitRangeInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getRangeInstruction(), "r", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitRetInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getRetInstruction(), "r", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitSetInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSetInstruction(), "s", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitSymbolInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSymbolInstruction(), "s", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitDummyInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDummyInstruction(), "d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitPhiInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getPhiInstruction(), "p", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitNumberedSymbolInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNumberedSymbolInstruction(), "n", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitBreakInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBreakInstruction(), "b", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitGotoInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getGotoInstruction(), "g", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitContinueInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getContinueInstruction(), "c", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitCondInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCondInstruction(), "c", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitArrayInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getArrayInstruction(), "a", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitMethodCallInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMethodCallInstruction(), "m", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitSimpleArrayInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSimpleArrayInstruction(), "s", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitCaseInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCaseInstruction(), "s", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitDAGInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theDagPackage.getDAGInstruction(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitSimdInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSimdInstruction(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitRangeArrayInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getRangeArrayInstruction(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitSSAUseSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSSAUseSymbol(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitSSADefSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSSADefSymbol(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitStringInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getStringInstruction(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitSimdGenericInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSimdGenericInstruction(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitSimdPackInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSimdPackInstruction(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitSimdShuffleInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSimdShuffleInstruction(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitSimdExtractInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSimdExtractInstruction(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instrsVisitorEClass, null, "visitSimdExpandInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSimdExpandInstruction(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(instrsVisitableEClass, InstrsVisitable.class, "InstrsVisitable", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(instrsVisitableEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(instructionEClass, Instruction.class, "Instruction", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInstruction____internal_cached___type(), theTypesPackage.getType(), null, "___internal_cached___type", null, 0, 1, Instruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(instructionEClass, this.getComplexInstruction(), "getParent", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(instructionEClass, theTypesPackage.getType(), "getType", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instructionEClass, null, "setType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTypesPackage.getType(), "t", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(instructionEClass, theTypesPackage.getType(), "computeType", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instructionEClass, null, "replaceChild", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "s", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "value", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(instructionEClass, this.getInstruction(), "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(instructionEClass, theCorePackage.getProcedure(), "getContainingProcedure", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(instructionEClass, theCorePackage.getProcedureSet(), "getContainingProcedureSet", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(instructionEClass, this.getInstruction(), "getRoot", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(instructionEClass, theBlocksPackage.getBasicBlock(), "getBasicBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(instructionEClass, theEcorePackage.getEBoolean(), "isConstant", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(instructionEClass, theEcorePackage.getEBoolean(), "isUnconditionalBranch", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(instructionEClass, theEcorePackage.getEBoolean(), "isRet", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(instructionEClass, theEcorePackage.getEBoolean(), "isBranch", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(instructionEClass, theEcorePackage.getEBoolean(), "isSet", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(instructionEClass, theEcorePackage.getEBoolean(), "isSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(instructionEClass, null, "substituteWith", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "inst", 1, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(instructionEClass, null, "remove", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(dummyInstructionEClass, DummyInstruction.class, "DummyInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(dummyInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(dummyInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dummyInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(labelInstructionEClass, LabelInstruction.class, "LabelInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLabelInstruction_Name(), this.getString(), "name", null, 0, 1, LabelInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(labelInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(labelInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(labelInstructionEClass, theEcorePackage.getEBoolean(), "equals", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "obj", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(labelInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(symbolInstructionEClass, SymbolInstruction.class, "SymbolInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSymbolInstruction_Symbol(), theCorePackage.getSymbol(), null, "symbol", null, 0, 1, SymbolInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(symbolInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(symbolInstructionEClass, theCorePackage.getSymbol(), "getUsedSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(symbolInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(symbolInstructionEClass, theEcorePackage.getEBoolean(), "isConstant", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(symbolInstructionEClass, theEcorePackage.getEBoolean(), "isSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(symbolInstructionEClass, theTypesPackage.getType(), "computeType", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(symbolInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(symbolInstructionEClass, theEcorePackage.getEBoolean(), "equals", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "obj", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(numberedSymbolInstructionEClass, NumberedSymbolInstruction.class, "NumberedSymbolInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNumberedSymbolInstruction_Number(), this.getint(), "number", null, 0, 1, NumberedSymbolInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(numberedSymbolInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(numberedSymbolInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(numberedSymbolInstructionEClass, theEcorePackage.getEBoolean(), "equals", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "obj", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(numberedSymbolInstructionEClass, this.getInstruction(), "getDefinition", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(numberedSymbolInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(ssaDefSymbolEClass, SSADefSymbol.class, "SSADefSymbol", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSSADefSymbol_SSAUses(), this.getSSAUseSymbol(), this.getSSAUseSymbol_Def(), "SSAUses", null, 0, -1, SSADefSymbol.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(ssaDefSymbolEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(ssaDefSymbolEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ssaDefSymbolEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(ssaUseSymbolEClass, SSAUseSymbol.class, "SSAUseSymbol", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSSAUseSymbol_Def(), this.getSSADefSymbol(), this.getSSADefSymbol_SSAUses(), "def", null, 0, 1, SSAUseSymbol.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(ssaUseSymbolEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(ssaUseSymbolEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ssaUseSymbolEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(sizeofInstructionEClass, SizeofInstruction.class, "SizeofInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(sizeofInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(sizeofInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(sizeofTypeInstructionEClass, SizeofTypeInstruction.class, "SizeofTypeInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSizeofTypeInstruction_TargetType(), theTypesPackage.getType(), null, "targetType", null, 0, 1, SizeofTypeInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(sizeofTypeInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(sizeofTypeInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(sizeofTypeInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(sizeofValueInstructionEClass, SizeofValueInstruction.class, "SizeofValueInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(sizeofValueInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(sizeofValueInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(sizeofValueInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(constantInstructionEClass, ConstantInstruction.class, "ConstantInstruction", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(constantInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(intInstructionEClass, IntInstruction.class, "IntInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIntInstruction_Value(), this.getlong(), "value", null, 0, 1, IntInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(intInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(intInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(intInstructionEClass, theEcorePackage.getEBoolean(), "isConstant", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(intInstructionEClass, theEcorePackage.getEBoolean(), "equals", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "object", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(intInstructionEClass, theTypesPackage.getType(), "computeType", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(intInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(floatInstructionEClass, FloatInstruction.class, "FloatInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFloatInstruction_Value(), this.getdouble(), "value", null, 0, 1, FloatInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(floatInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(floatInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(floatInstructionEClass, theEcorePackage.getEBoolean(), "isConstant", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(floatInstructionEClass, theEcorePackage.getEBoolean(), "equals", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "obj", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(floatInstructionEClass, theTypesPackage.getType(), "computeType", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(floatInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(stringInstructionEClass, StringInstruction.class, "StringInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringInstruction_Value(), this.getString(), "value", null, 0, 1, StringInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(stringInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(stringInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(stringInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(complexInstructionEClass, ComplexInstruction.class, "ComplexInstruction", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(complexInstructionEClass, this.getInstruction(), "listChildren", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(complexInstructionEClass, this.getInstruction(), "listValidChildren", 0, -1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(complexInstructionEClass, this.getint(), "indexOfChild", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "child", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(complexInstructionEClass, this.getInstruction(), "getChildAt", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getint(), "index", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(complexInstructionEClass, null, "removeChildAt", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getint(), "index", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(complexInstructionEClass, null, "removeChild", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(complexInstructionEClass, null, "replaceChild", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "oldInstr", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "newInstr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(complexInstructionEClass, this.getint(), "getChildrenCount", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(complexInstructionEClass, theBlocksPackage.getBasicBlock(), "getBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(complexInstructionEClass, this.getInstruction(), "getRootInstuction", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(complexInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(childrenListInstructionEClass, ChildrenListInstruction.class, "ChildrenListInstruction", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getChildrenListInstruction_Children(), this.getInstruction(), null, "children", null, 0, -1, ChildrenListInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(childrenListInstructionEClass, this.getInstruction(), "listChildren", 0, -1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(childrenListInstructionEClass, null, "addChild", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "inst", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(childrenListInstructionEClass, null, "removeChild", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "inst", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(childrenListInstructionEClass, this.getint(), "getChildrenCount", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(childrenListInstructionEClass, this.getInstruction(), "getChild", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getint(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(childrenListInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(childrenListInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(arrayValueInstructionEClass, ArrayValueInstruction.class, "ArrayValueInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(arrayValueInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(arrayValueInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(arrayValueInstructionEClass, theEcorePackage.getEBoolean(), "isConstant", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(arrayValueInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(phiInstructionEClass, PhiInstruction.class, "PhiInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(phiInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(phiInstructionEClass, this.getSSADefSymbol(), "getSSADefinitions", 0, -1, IS_UNIQUE, IS_ORDERED);

		addEOperation(phiInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(phiInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(componentInstructionEClass, ComponentInstruction.class, "ComponentInstruction", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(componentInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(componentInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(setInstructionEClass, SetInstruction.class, "SetInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSetInstruction_Dest(), this.getInstruction(), null, "dest", null, 0, 1, SetInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSetInstruction_Source(), this.getInstruction(), null, "source", null, 0, 1, SetInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(setInstructionEClass, this.getInstruction(), "listChildren", 0, -1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(setInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(setInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(setInstructionEClass, theEcorePackage.getEBoolean(), "isSet", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(setInstructionEClass, theTypesPackage.getType(), "computeType", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(setInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "inst", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(addressComponentEClass, AddressComponent.class, "AddressComponent", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAddressComponent_Address(), this.getInstruction(), null, "address", null, 0, 1, AddressComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(addressComponentEClass, this.getInstruction(), "listChildren", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(addressComponentEClass, this.getString(), "getSymbolAddress", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(addressComponentEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(addressInstructionEClass, AddressInstruction.class, "AddressInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(addressInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(addressInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(addressInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(indirInstructionEClass, IndirInstruction.class, "IndirInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(indirInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(indirInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(indirInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(callInstructionEClass, CallInstruction.class, "CallInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCallInstruction_Args(), this.getInstruction(), null, "args", null, 0, -1, CallInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(callInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(callInstructionEClass, this.getInstruction(), "listChildren", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(callInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(callInstructionEClass, theCorePackage.getSymbol(), "getProcedureSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(callInstructionEClass, this.getString(), "getSymbolAddress", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(callInstructionEClass, null, "addArg", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "arg", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(callInstructionEClass, theEcorePackage.getEBoolean(), "isSimpleCallInstruction", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(callInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(methodCallInstructionEClass, MethodCallInstruction.class, "MethodCallInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMethodCallInstruction_Args(), this.getInstruction(), null, "args", null, 0, -1, MethodCallInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMethodCallInstruction_Name(), this.getString(), "name", null, 0, 1, MethodCallInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(methodCallInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(methodCallInstructionEClass, this.getInstruction(), "listChildren", 0, -1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(methodCallInstructionEClass, null, "addArg", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "arg", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(methodCallInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(arrayInstructionEClass, ArrayInstruction.class, "ArrayInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getArrayInstruction_Dest(), this.getInstruction(), null, "dest", null, 0, 1, ArrayInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getArrayInstruction_Index(), this.getInstruction(), null, "index", null, 0, -1, ArrayInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(arrayInstructionEClass, this.getInstruction(), "listChildren", 0, -1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(arrayInstructionEClass, null, "addIndex", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "idx", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(arrayInstructionEClass, null, "addIndexAt", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getint(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "idx", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(arrayInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(arrayInstructionEClass, theTypesPackage.getType(), "computeType", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(arrayInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(arrayInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(simpleArrayInstructionEClass, SimpleArrayInstruction.class, "SimpleArrayInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(simpleArrayInstructionEClass, theCorePackage.getSymbol(), "getSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(simpleArrayInstructionEClass, null, "setSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getSymbol(), "newSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(simpleArrayInstructionEClass, null, "setDest", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "newDest", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(simpleArrayInstructionEClass, theTypesPackage.getType(), "computeType", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(simpleArrayInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(simpleArrayInstructionEClass, theCorePackage.getSymbol(), "getUsedSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(simpleArrayInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(simpleArrayInstructionEClass, theTypesPackage.getArrayType(), "getTargetArrayType", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(simpleArrayInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(rangeArrayInstructionEClass, RangeArrayInstruction.class, "RangeArrayInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(rangeArrayInstructionEClass, theCorePackage.getSymbol(), "getSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(rangeArrayInstructionEClass, null, "setSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getSymbol(), "newSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(rangeArrayInstructionEClass, null, "setDest", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "newDest", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(rangeArrayInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(rangeArrayInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(rangeArrayInstructionEClass, theTypesPackage.getArrayType(), "getTargetArrayType", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(rangeArrayInstructionEClass, this.getSimpleArrayInstruction(), "getAccessedElement", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getint(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(rangeArrayInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(branchInstructionEClass, BranchInstruction.class, "BranchInstruction", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(branchInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(branchInstructionEClass, theEcorePackage.getEBoolean(), "isBranch", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(branchInstructionEClass, this.getInstruction(), "listChildren", 0, -1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(branchInstructionEClass, theEcorePackage.getEBoolean(), "equals", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "obj", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(branchInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(condInstructionEClass, CondInstruction.class, "CondInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCondInstruction_Label(), this.getString(), "label", null, 0, 1, CondInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCondInstruction_Cond(), this.getInstruction(), null, "cond", null, 0, 1, CondInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(condInstructionEClass, this.getInstruction(), "listChildren", 0, -1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(condInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(condInstructionEClass, theEcorePackage.getEBoolean(), "equals", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "obj", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(condInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(condInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(breakInstructionEClass, BreakInstruction.class, "BreakInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(breakInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(breakInstructionEClass, theEcorePackage.getEBoolean(), "equals", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "obj", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(breakInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(breakInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(continueInstructionEClass, ContinueInstruction.class, "ContinueInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(continueInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(continueInstructionEClass, theEcorePackage.getEBoolean(), "equals", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "obj", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(continueInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(continueInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(gotoInstructionEClass, GotoInstruction.class, "GotoInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGotoInstruction_LabelInstruction(), this.getLabelInstruction(), null, "labelInstruction", null, 0, 1, GotoInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGotoInstruction_LabelName(), this.getString(), "labelName", null, 0, 1, GotoInstruction.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		op = addEOperation(gotoInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(gotoInstructionEClass, theEcorePackage.getEBoolean(), "equals", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "obj", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(gotoInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(gotoInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(exprComponentEClass, ExprComponent.class, "ExprComponent", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getExprComponent_Expr(), this.getInstruction(), null, "expr", null, 0, 1, ExprComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(exprComponentEClass, this.getInstruction(), "listChildren", 0, -1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(exprComponentEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(retInstructionEClass, RetInstruction.class, "RetInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(retInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(retInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(retInstructionEClass, theEcorePackage.getEBoolean(), "isRet", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(retInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(convertInstructionEClass, ConvertInstruction.class, "ConvertInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(convertInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(convertInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(convertInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(fieldInstructionEClass, FieldInstruction.class, "FieldInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFieldInstruction_Field(), theTypesPackage.getField(), null, "field", null, 0, 1, FieldInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(fieldInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(fieldInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(fieldInstructionEClass, theEcorePackage.getEBoolean(), "equals", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "obj", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(fieldInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(enumeratorInstructionEClass, EnumeratorInstruction.class, "EnumeratorInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEnumeratorInstruction_Enumerator(), theTypesPackage.getEnumerator(), null, "enumerator", null, 0, 1, EnumeratorInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(enumeratorInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(enumeratorInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(rangeInstructionEClass, RangeInstruction.class, "RangeInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRangeInstruction_Low(), this.getlong(), "low", null, 0, 1, RangeInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRangeInstruction_High(), this.getlong(), "high", null, 0, 1, RangeInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(rangeInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(rangeInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(rangeInstructionEClass, theEcorePackage.getEBoolean(), "equals", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "obj", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(rangeInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(caseInstructionEClass, CaseInstruction.class, "CaseInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(caseInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(caseInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(genericInstructionEClass, GenericInstruction.class, "GenericInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGenericInstruction_Name(), this.getString(), "name", null, 0, 1, GenericInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(genericInstructionEClass, this.getInstruction(), "getOperands", 0, -1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(genericInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(genericInstructionEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(genericInstructionEClass, this.getInstruction(), "getOperand", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getint(), "i", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(genericInstructionEClass, null, "addOperand", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "inst", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(genericInstructionEClass, theEcorePackage.getEBoolean(), "isNamed", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getString(), "name", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(genericInstructionEClass, theTypesPackage.getType(), "computeType", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(genericInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(arithmeticInstructionEClass, ArithmeticInstruction.class, "ArithmeticInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getArithmeticInstruction_Operator(), this.getArithmeticOperator(), "operator", "add", 0, 1, ArithmeticInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(arithmeticInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(comparisonInstructionEClass, ComparisonInstruction.class, "ComparisonInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComparisonInstruction_Operator(), this.getComparisonOperator(), "operator", null, 0, 1, ComparisonInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(comparisonInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(logicalInstructionEClass, LogicalInstruction.class, "LogicalInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLogicalInstruction_Operator(), this.getLogicalOperator(), "operator", null, 0, 1, LogicalInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(logicalInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(bitwiseInstructionEClass, BitwiseInstruction.class, "BitwiseInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBitwiseInstruction_Operator(), this.getBitwiseOperator(), "operator", null, 0, 1, BitwiseInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(bitwiseInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(simdInstructionEClass, SimdInstruction.class, "SimdInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(simdInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(simdInstructionEClass, theEcorePackage.getEBoolean(), "isSame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "instr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(simdGenericInstructionEClass, SimdGenericInstruction.class, "SimdGenericInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(simdGenericInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(simdPackInstructionEClass, SimdPackInstruction.class, "SimdPackInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(simdPackInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(simdExtractInstructionEClass, SimdExtractInstruction.class, "SimdExtractInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(simdExtractInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(simdExtractInstructionEClass, this.getInstruction(), "getSubwordIndex", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(simdExtractInstructionEClass, this.getInstruction(), "getVector", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(simdExtractInstructionEClass, null, "setSubwordIndex", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "subwordIndex", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(simdExtractInstructionEClass, null, "setVector", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstruction(), "vector", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(simdShuffleInstructionEClass, SimdShuffleInstruction.class, "SimdShuffleInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSimdShuffleInstruction_Permutation(), this.getint(), "permutation", "0", 0, -1, SimdShuffleInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(simdShuffleInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(simdExpandInstructionEClass, SimdExpandInstruction.class, "SimdExpandInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSimdExpandInstruction_Permutation(), this.getint(), "permutation", "0", 0, -1, SimdExpandInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(simdExpandInstructionEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInstrsVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(arithmeticOperatorEEnum, ArithmeticOperator.class, "ArithmeticOperator");
		addEEnumLiteral(arithmeticOperatorEEnum, ArithmeticOperator.ADD);
		addEEnumLiteral(arithmeticOperatorEEnum, ArithmeticOperator.SUB);
		addEEnumLiteral(arithmeticOperatorEEnum, ArithmeticOperator.MUL);
		addEEnumLiteral(arithmeticOperatorEEnum, ArithmeticOperator.DIV);
		addEEnumLiteral(arithmeticOperatorEEnum, ArithmeticOperator.MOD);
		addEEnumLiteral(arithmeticOperatorEEnum, ArithmeticOperator.NEG);
		addEEnumLiteral(arithmeticOperatorEEnum, ArithmeticOperator.MMOD);

		initEEnum(comparisonOperatorEEnum, ComparisonOperator.class, "ComparisonOperator");
		addEEnumLiteral(comparisonOperatorEEnum, ComparisonOperator.EQ);
		addEEnumLiteral(comparisonOperatorEEnum, ComparisonOperator.NE);
		addEEnumLiteral(comparisonOperatorEEnum, ComparisonOperator.GT);
		addEEnumLiteral(comparisonOperatorEEnum, ComparisonOperator.LT);
		addEEnumLiteral(comparisonOperatorEEnum, ComparisonOperator.GE);
		addEEnumLiteral(comparisonOperatorEEnum, ComparisonOperator.LE);

		initEEnum(logicalOperatorEEnum, LogicalOperator.class, "LogicalOperator");
		addEEnumLiteral(logicalOperatorEEnum, LogicalOperator.AND);
		addEEnumLiteral(logicalOperatorEEnum, LogicalOperator.OR);
		addEEnumLiteral(logicalOperatorEEnum, LogicalOperator.NOT);

		initEEnum(bitwiseOperatorEEnum, BitwiseOperator.class, "BitwiseOperator");
		addEEnumLiteral(bitwiseOperatorEEnum, BitwiseOperator.NOT);
		addEEnumLiteral(bitwiseOperatorEEnum, BitwiseOperator.AND);
		addEEnumLiteral(bitwiseOperatorEEnum, BitwiseOperator.OR);
		addEEnumLiteral(bitwiseOperatorEEnum, BitwiseOperator.XOR);
		addEEnumLiteral(bitwiseOperatorEEnum, BitwiseOperator.SHL);
		addEEnumLiteral(bitwiseOperatorEEnum, BitwiseOperator.SHR);

		initEEnum(selectOperatorEEnum, SelectOperator.class, "SelectOperator");
		addEEnumLiteral(selectOperatorEEnum, SelectOperator.MUX);

		initEEnum(reductionOperatorEEnum, ReductionOperator.class, "ReductionOperator");
		addEEnumLiteral(reductionOperatorEEnum, ReductionOperator.MAX);
		addEEnumLiteral(reductionOperatorEEnum, ReductionOperator.MIN);
		addEEnumLiteral(reductionOperatorEEnum, ReductionOperator.SUM);
		addEEnumLiteral(reductionOperatorEEnum, ReductionOperator.PROD);

		initEEnum(branchTypeEEnum, BranchType.class, "BranchType");
		addEEnumLiteral(branchTypeEEnum, BranchType.UNCONDITIONAL);
		addEEnumLiteral(branchTypeEEnum, BranchType.IF_TRUE);
		addEEnumLiteral(branchTypeEEnum, BranchType.IF_FALSE);
		addEEnumLiteral(branchTypeEEnum, BranchType.DISPATCH);

		// Initialize data types
		initEDataType(stringEDataType, String.class, "String", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(intEDataType, int.class, "int", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(longEDataType, long.class, "long", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(doubleEDataType, double.class, "double", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/ocl/examples/OCL
		createOCLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/ocl/examples/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.eclipse.org/ocl/examples/OCL";	
		addAnnotation
		  (instructionEClass, 
		   source, 
		   new String[] {
			 "constr", "true"
		   });
	}

} //InstrsPackageImpl
