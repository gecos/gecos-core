package fr.irisa.cairn.gecos.model.transforms.dag;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.gecos.model.analysis.symbols.SymbolUseAnalyzer;
import fr.irisa.cairn.gecos.model.analysis.symbols.SymbolUseAnalyzer.SymbolUse;
import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDagDefaultVisitor;
import fr.irisa.r2d2.gecos.framework.GSModule;
import gecos.blocks.BasicBlock;
import gecos.core.ProcedureSet;
import gecos.dag.DAGEdge;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGNode;
import gecos.dag.DAGOutNode;


@GSModule("Removes useless DAG Output Nodes.\n"
		+ "A DAG output node is considered useless if it is used\n"
		+ "in only one BasicBlock and as non-mandatory use\n"
		+ "(i.e. it is not used by a RetInstruction nor is A Procedure Parameter)")
public class DAGRedoundantOutputNodeRemover {

	private ProcedureSet ps;

	public DAGRedoundantOutputNodeRemover(ProcedureSet ps) {
		this.ps = ps;
	}

	public void compute() {
		// Find useless outputs
		SymbolUseAnalyzer usesAnalyzer = new SymbolUseAnalyzer(ps);
		usesAnalyzer.compute();
		UselessOutputsFinder uselessOutputsFinder = new UselessOutputsFinder(
				usesAnalyzer);
		uselessOutputsFinder.visit(ps);

		// Remove them
		for (DAGNode n : uselessOutputsFinder.useless) {
			remove(n);
		}

	}

	/**
	 * Remove a DAG node from a {@link DAGInstruction}. Also remove all
	 * connected edges.
	 * 
	 * @param n
	 */
	private static void remove(DAGNode n) {
		DAGInstruction dag = n.getParent();
		
		List<DAGEdge> removedEdges = new ArrayList<DAGEdge>();
		for (DAGEdge e : dag.getEdges()) {
			if (e.getSinkNode() == n || e.getSourceNode() == n)
				removedEdges.add(e);
		}
		dag.getEdges().removeAll(removedEdges);
		dag.getNodes().remove(n);
	}

	/**
	 * A DAG output node is useless if it has no mandatory use and if it is used
	 * in only one {@link BasicBlock}.
	 * 
	 * @author Antoine Floc'h - Initial contribution and API
	 * 
	 */
	private static class UselessOutputsFinder extends
			GecosBlocksInstructionsDagDefaultVisitor {
		private SymbolUseAnalyzer symbolUseAnalyzer;
		private List<DAGNode> useless = new ArrayList<DAGNode>();

		public UselessOutputsFinder(SymbolUseAnalyzer symbolUseAnalyzer) {
			this.symbolUseAnalyzer = symbolUseAnalyzer;
		}

		@Override
		public void visitDAGOutNode(DAGOutNode d) {
			if (!isMandatory(d)) {
				if (symbolUseAnalyzer.getBasicBlocksUsingSymbol(d.getSymbol())
						.size() == 1)
					useless.add(d);
			}

		}

		private boolean isMandatory(DAGOutNode d) {
			List<SymbolUse> uses = symbolUseAnalyzer.getSymbolsUses(d
					.getSymbol());

			for (SymbolUse use : uses) {
				if (use.isMandatory()) {
					return true;
				}
			}
			return false;
		}

	}

}
