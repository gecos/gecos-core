/**
 */
package gecos.blocks;

import gecos.core.GecosNode;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visitable</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.blocks.BlocksPackage#getBlocksVisitable()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface BlocksVisitable extends GecosNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 * @generated
	 */
	void accept(BlocksVisitor visitor);

} // BlocksVisitable
