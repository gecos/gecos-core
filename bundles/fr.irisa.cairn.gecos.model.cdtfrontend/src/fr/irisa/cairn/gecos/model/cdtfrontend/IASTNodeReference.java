/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.cdtfrontend;

import gecos.annotations.AnnotatedElement;
import gecos.annotations.AnnotationsFactory;
import gecos.annotations.IASTAnnotation;
import gecos.annotations.IAnnotation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.cdt.core.dom.ast.IASTNode;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.EObjectImpl;
/**
 * HACK Implement Adapter to be able to store IASTNode in PragmaAnnotation
 * @author mnaullet
 *
 */
public class IASTNodeReference extends EObjectImpl implements Adapter {

	public static final String annotationKey = "IASTRef";
	
	/*
	 * convenient static method to annotate objects
	 */
	
	public static void eObjectAnnotate(EObject element, Collection<? extends EObject> gecosObjects) {
		annotate(element, getNode(gecosObjects));
	}
	public static void eObjectAnnotate(EObject element, EObject ... gecosObjects) {
		annotate(element, getNode(gecosObjects));
	}

	public static void annotate(EObject element, IASTNode ... node) {
		annotate(element, Arrays.asList(node));
	}

	public static void annotate(EObject element, Collection<IASTNode> node) {
		if (element instanceof AnnotatedElement) {
			((AnnotatedElement) element).getAnnotations().put(IASTNodeReference.annotationKey, createAnnotation(node));
		} else if (element instanceof IAnnotation) {
			element.eAdapters().add(create(node));
		} else
			throw new UnsupportedOperationException();
	}
	
	/*
	 * convenient static method to create annotation
	 */

	public static IASTNodeReference eObjectCreate(EObject ... gecosObjects) {
		return create(getNode(gecosObjects));
	}
	
	public static IASTNodeReference eObjectCreate(Collection<? extends EObject> gecosObjects) {
		return create(getNode(gecosObjects));
	}

	public static IASTAnnotation eObjectCreateAnnotation(EObject ... gecosObjects) {
		return createAnnotation(getNode(gecosObjects));
	}
	
	public static IASTAnnotation eObjectCreateAnnotation(Collection<? extends EObject> gecosObjects) {
		return createAnnotation(getNode(gecosObjects));
	}
	
	public static IASTAnnotation createAnnotation(Collection<IASTNode> node) {
		IASTAnnotation res = AnnotationsFactory.eINSTANCE.createIASTAnnotation();
		res.setIastNode(create(node));
		return res;
	}
	public static IASTNodeReference create(Collection<IASTNode> node) {
		return new IASTNodeReference(node);
	}
	

	public static IASTAnnotation createAnnotation(IASTNode ... node) {
		IASTAnnotation res = AnnotationsFactory.eINSTANCE.createIASTAnnotation();
		res.setIastNode(create(node));
		return res;
	}
	public static IASTNodeReference create(IASTNode ... node) {
		return new IASTNodeReference(node);
	}

	/*
	 * convenient static method to inspect objects
	 */
	
	public static IASTNodeReference getNodeReference(EObject eObject) {
		for (Adapter adapter : eObject.eAdapters())
			if (adapter instanceof IASTNodeReference)
				return (IASTNodeReference)adapter;
		return null;
	}

	private static Collection<IASTNode> getNode(EObject ... gecossObjects) {
		Set<IASTNode> res = new HashSet<>(gecossObjects.length);
		for (EObject e : gecossObjects) {
			res.addAll(getNode(e));
		}
		return res;
	}
	public static Collection<IASTNode> getNode(Collection<? extends EObject> a) {
		Set<IASTNode> res = new HashSet<>(a.size());
		for (EObject e : a) {
			List<IASTNode> node = getNode(e);
			res.addAll(node);
		}
		return res;
	}
	
	public static List<IASTNode> getNode(EObject eObject) {
		List<IASTNode> res = new ArrayList<>(0);
		
		if (eObject instanceof AnnotatedElement) {
			IASTAnnotation annotation = (IASTAnnotation) ((AnnotatedElement)eObject).getAnnotation(IASTNodeReference.annotationKey);
			if (annotation != null) {
				EObject iastNode2 = annotation.getIastNode();
				IASTNodeReference iastNode = (IASTNodeReference) iastNode2;
				if (iastNode != null) {
					res = iastNode.getRef();
				}
			}
		} else {
			for (Adapter adapter : eObject.eAdapters()) {
				if (adapter instanceof IASTNodeReference) {
					res = ((IASTNodeReference)adapter).getRef();
					break;
				}
			}
		}
		
		return res;
	}
	
	/*
	 * Object definition
	 */

	private List<IASTNode> ref;

	private IASTNodeReference(IASTNode ... ref) {
		this(Arrays.asList(ref));
	}
	
	private IASTNodeReference(Collection<IASTNode> ref) {
		this.ref = new ArrayList<>(ref);
	}
	
	public List<IASTNode> getRef() {
		return ref;
	}

	public void notifyChanged(Notification notification) {
	}

	public Notifier getTarget() {
		return null;
	}

	public void setTarget(Notifier newTarget) {
	}

	public boolean isAdapterForType(Object type) {
		return false;
	}
	
	@Override
	public String toString() {
		return "IASTNodeReference : ["+ref+"]";
	}

}