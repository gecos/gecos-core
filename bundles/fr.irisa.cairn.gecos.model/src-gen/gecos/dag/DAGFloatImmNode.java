/**
 */
package gecos.dag;

import gecos.instrs.FloatInstruction;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Float Imm Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.DAGFloatImmNode#getFloatValue <em>Float Value</em>}</li>
 *   <li>{@link gecos.dag.DAGFloatImmNode#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see gecos.dag.DagPackage#getDAGFloatImmNode()
 * @model
 * @generated
 */
public interface DAGFloatImmNode extends DAGImmNode, DagVisitable {
	/**
	 * Returns the value of the '<em><b>Float Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Float Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Float Value</em>' attribute.
	 * @see #setFloatValue(double)
	 * @see gecos.dag.DagPackage#getDAGFloatImmNode_FloatValue()
	 * @model unique="false" dataType="gecos.dag.double" derived="true"
	 * @generated
	 */
	double getFloatValue();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGFloatImmNode#getFloatValue <em>Float Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Float Value</em>' attribute.
	 * @see #getFloatValue()
	 * @generated
	 */
	void setFloatValue(double value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(FloatInstruction)
	 * @see gecos.dag.DagPackage#getDAGFloatImmNode_Value()
	 * @model containment="true"
	 * @generated
	 */
	FloatInstruction getValue();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGFloatImmNode#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(FloatInstruction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGFloatImmNode(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.dag.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _ident = this.getIdent();\n&lt;%java.lang.String%&gt; _plus = (\"FloatNode_\" + &lt;%java.lang.Integer%&gt;.valueOf(_ident));\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \":\");\n&lt;%gecos.instrs.FloatInstruction%&gt; _value = this.getValue();\nreturn (_plus_1 + _value);'"
	 * @generated
	 */
	String toString();

} // DAGFloatImmNode
