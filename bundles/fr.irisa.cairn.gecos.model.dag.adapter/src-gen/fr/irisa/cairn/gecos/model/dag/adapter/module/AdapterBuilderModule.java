package fr.irisa.cairn.gecos.model.dag.adapter.module;

import com.google.inject.Singleton;

import fr.irisa.cairn.componentElement.ServiceModule;
import fr.irisa.cairn.gecos.model.dag.adapter.builder.AdapterBuilder;
;

public class AdapterBuilderModule extends ServiceModule {

	@Override
	protected void configure() {
		this.bindBuilder();
	}

	protected void bindBuilder() {
		this.bind(AdapterBuilder.class).in(Singleton.class);
	}
}
