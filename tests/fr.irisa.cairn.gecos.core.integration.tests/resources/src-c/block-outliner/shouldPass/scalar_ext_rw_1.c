int f(int X, int Y) {

	#pragma outline
	{
		int a = 7;
		int b = 11;
		X += a;
		Y *= b;
	}

	return X + Y; // (X+7) + (Y*11)
}

int main() {
	if(f(2, 3) != 42)
		abort();

	return 0;
}
