/**
 */
package gecos.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scope Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.core.ScopeContainer#getScope <em>Scope</em>}</li>
 * </ul>
 *
 * @see gecos.core.CorePackage#getScopeContainer()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ScopeContainer extends CoreVisitable {
	/**
	 * Returns the value of the '<em><b>Scope</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link gecos.core.Scope#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope</em>' containment reference.
	 * @see #setScope(Scope)
	 * @see gecos.core.CorePackage#getScopeContainer_Scope()
	 * @see gecos.core.Scope#getContainer
	 * @model opposite="container" containment="true" required="true"
	 * @generated
	 */
	Scope getScope();

	/**
	 * Sets the value of the '{@link gecos.core.ScopeContainer#getScope <em>Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scope</em>' containment reference.
	 * @see #getScope()
	 * @generated
	 */
	void setScope(Scope value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model symbolUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getScope().addSymbol(symbol);'"
	 * @generated
	 */
	void addSymbol(Symbol symbol);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model symbolUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getScope().removeSymbol(symbol);'"
	 * @generated
	 */
	void removeSymbol(Symbol symbol);

} // ScopeContainer
