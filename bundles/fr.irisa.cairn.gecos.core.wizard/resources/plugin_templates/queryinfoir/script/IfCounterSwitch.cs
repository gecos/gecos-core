p = CreateGecosProject("IfCounterSwitch"); # Create a empty GeCoS project
AddSourceToGecosProject(p, "src-c/example.c"); # Add a new C source in the project
CDTFrontend(p); # Generate the GeCoS IR from the C source added

IfCounterSwitch(p); # Call the script command which correspond to our GeCoS module

