package fr.irisa.cairn.gecos.model.cdtfrontend;

import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;

@GSModule("Configure the module 'CDTFrontEnd'.\n"
		+ "Whether to use GeCoS internally defined Standard \n"
		+ "library include files or not.")
public class CDTUseGecosStandardIncludes {

	boolean use;
	
	@GSModuleConstructor("-arg1: if not '0' then use Gecos Standard Includes.")
	public CDTUseGecosStandardIncludes(int i) {
		this(i!=0);
	}
	
	@GSModuleConstructor("-arg1: if 'true' then use Gecos Standard Includes.")
	public CDTUseGecosStandardIncludes(boolean u) {
		this.use=u;
	}
	
	@GSModuleConstructor("Default: Use Gecos Standard Includes.")
	public CDTUseGecosStandardIncludes() {
		this(true);
	}
	
	
	public void compute() {
		CDTFrontEnd.useGecosStandardIncludes(use);
	}
}
