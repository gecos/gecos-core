package fr.irisa.cairn.gecos.model.dag.adapter.builder;

import com.google.inject.Inject;

import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGEdgeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGInstructionAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGNodeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGPortAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.observers.IDagSynchronizationObserver;
import gecos.dag.DAGInstruction;

/**
 * Builder for a synchronized Adapter. An {@link IDagSynchronizationObserver} is
 * added to the observers of {@link IDAGInstructionAdapter},
 * {@link IDAGNodeAdapter},{@link IDAGEdgeAdapter},{@link IDAGPortAdapter} in
 * order to maintain {@link DAGInstruction} consistency after the initialization
 * of the {@link IDAGInstructionAdapter}.
 * 
 * @author Antoine Floc'h - Initial contribution and API
 * 
 */

public class DagAdapterSynchronizedBuilder extends AdapterBuilder {
	@Inject
	private IDagSynchronizationObserver update;

	@Override
	public IDAGInstructionAdapter buildGraphAdapter(DAGInstruction adapted) {
		IDAGInstructionAdapter adapter = super.buildGraphAdapter(adapted);
		adapter.getObservers().add(update);
		for (IDAGNodeAdapter n : adapter.getNodes()) {
			n.getObservers().add(update);
			for (IDAGPortAdapter p : n.getInputPorts()) {
				p.getObservers().add(update);
			}
			for (IDAGPortAdapter p : n.getOutputPorts()) {
				p.getObservers().add(update);
			}
		}
		for (IDAGEdgeAdapter e : adapter.getEdges()) {
			e.getObservers().add(update);
		}
		return adapter;
	}
}
