package fr.irisa.cairn.gecos.model.extensions.visitors;

import java.util.WeakHashMap;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;

import fr.irisa.cairn.gecos.model.extensions.GecosRegistry;
import gecos.blocks.BlocksVisitor;
import gecos.instrs.InstrsVisitor;

/**
 * Register all the visitors adapters pointed by the <i>gecos.visitors</i>
 * extension point.
 * 
 * @author Antoine Floc'h - Initial contribution and API
 * 
 */
public class GecosVisitorsRegistry extends GecosRegistry {
	public final static GecosVisitorsRegistry INSTANCE = new GecosVisitorsRegistry();

	private final static String EXTENSION_POINT = "fr.irisa.cairn.gecos.model.GecosVisitors";
	private final static String ADAPTABLE_ATTRIBUTE = "visitor";
	private final static String ADAPTER_ATTRIBUTE = "adapter";
	private final static String TYPE_ATTRIBUTE = "type";

	private VisitorAdapterFactory adaptersFactory;
	private WeakHashMap<Object, Object> adaptersMap;

	protected GecosVisitorsRegistry() {
		this.adaptersFactory = new VisitorAdapterFactory();
		this.adaptersMap = new WeakHashMap<Object, Object>();
	}

	/**
	 * Get the class of a registered adapter visitor for an adaptable visitor
	 * and given extended visitor type.<br>
	 * <br>
	 * <b>Example:</b> How to get a registered ASM code generator adapter ?<br>
	 * <br>
	 * <code>
	 * CGenerator generator = new CGenerator();
	 * getAdapterClass(generator,ASMVisitor.class);
	 * </code>
	 * 
	 * @param <Visitor>
	 * @param adaptable
	 * @param type
	 * @return
	 */
	private <Visitor, AdapterType> Class<GecosVisitorAdapter<Visitor>> getAdapterClass(
			Visitor adaptable, Class<AdapterType> type) {
		for (IExtension e : getExtensions(EXTENSION_POINT)) {
			for (IConfigurationElement iConfigurationElement : e
					.getConfigurationElements()) {
				String adapterClassID = iConfigurationElement
						.getAttribute(ADAPTER_ATTRIBUTE);
				String typeClassID = iConfigurationElement
						.getAttribute(TYPE_ATTRIBUTE);
				String adaptableClassID = iConfigurationElement
						.getAttribute(ADAPTABLE_ATTRIBUTE);

				Class<AdapterType> extAdapterType = getClass(typeClassID,
						iConfigurationElement);
				if (type == extAdapterType) {
					try {
						Class<Visitor> extAdaptableClass = getClass(adaptableClassID, iConfigurationElement);
						if (adaptable.getClass() == extAdaptableClass) {
							return getClass(adapterClassID, iConfigurationElement);
						}
					} catch (Exception e2) {
						System.err.println("Inconsistent extension class "+adaptableClassID+" does not exists.");
					}
				}
			}

		}
		return null;
	}

	/**
	 * Get the instructions adapter visitor for an adaptable visitor instance.
	 * If the adaptable instance isn't mapped to any adapter instance then a new
	 * adapter is built. This function should be called by the accept function
	 * of visited objects which are not visitable by Gecos {@link InstrsVisitor}
	 * .
	 * 
	 * @param <Adapter>
	 * @param <Adaptable>
	 * @param adaptable
	 *            instance of adaptable visitor
	 * @param adapterType
	 *            type of the adapter
	 * @return
	 */
	public <Adapter, Adaptable extends InstrsVisitor> Adapter getInstructionsAdapter(
			Adaptable adaptable, Class<Adapter> adapterType) {
		return getAdapter(adaptable, adapterType);
	}

	/**
	 * Get the instructions adapter visitor for an adaptable visitor instance.
	 * If the adaptable instance isn't mapped to any adapter instance then a new
	 * adapter is built. This function should be called by the accept function
	 * of visited objects which are not visitable by Gecos {@link BlocksVisitor}
	 * .
	 * 
	 * 
	 * @param <Adapter>
	 * @param <Adaptable>
	 * @param adaptable
	 *            instance of adaptable visitor
	 * @param adapterType
	 *            type of the adapter
	 * @return
	 */
	public <Adapter, Adaptable extends BlocksVisitor> Adapter getBlocksAdapter(
			Adaptable adaptable, Class<Adapter> adapterType) {
		return getAdapter(adaptable, adapterType);
	}

	@SuppressWarnings("unchecked")
	private <Adapter, Adaptable> Adapter getAdapter(Adaptable adaptable,
			Class<Adapter> adapterType) {

		GecosVisitorAdapter<Adaptable> adapter = (GecosVisitorAdapter<Adaptable>) adaptersMap
				.get(adaptable);
		if (adapter == null) {

			Class<GecosVisitorAdapter<Adaptable>> adapterClass = getAdapterClass(
					adaptable, adapterType);
			if (adapterClass != null) {
				adapter = adaptersFactory.buildAdapter(adapterClass, adaptable);
				if (!adapterClass.isInstance(adapter)) {
					throw new IllegalArgumentException(adapter
							+ " isn't an instance of adapter type ("
							+ adapterType + ")");
				}
			}
			adaptersMap.put(adaptable, adapter);
		}
		return (Adapter) adapter;
	}

}
