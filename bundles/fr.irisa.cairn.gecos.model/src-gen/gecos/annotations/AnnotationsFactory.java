/**
 */
package gecos.annotations;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see gecos.annotations.AnnotationsPackage
 * @generated
 */
public interface AnnotationsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AnnotationsFactory eINSTANCE = gecos.annotations.impl.AnnotationsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>String Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>String Annotation</em>'.
	 * @generated
	 */
	StringAnnotation createStringAnnotation();

	/**
	 * Returns a new object of class '<em>Pragma Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pragma Annotation</em>'.
	 * @generated
	 */
	PragmaAnnotation createPragmaAnnotation();

	/**
	 * Returns a new object of class '<em>Comment Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Comment Annotation</em>'.
	 * @generated
	 */
	CommentAnnotation createCommentAnnotation();

	/**
	 * Returns a new object of class '<em>Extended Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Extended Annotation</em>'.
	 * @generated
	 */
	ExtendedAnnotation createExtendedAnnotation();

	/**
	 * Returns a new object of class '<em>Liveness Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Liveness Annotation</em>'.
	 * @generated
	 */
	LivenessAnnotation createLivenessAnnotation();

	/**
	 * Returns a new object of class '<em>Symbol Definition Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Symbol Definition Annotation</em>'.
	 * @generated
	 */
	SymbolDefinitionAnnotation createSymbolDefinitionAnnotation();

	/**
	 * Returns a new object of class '<em>IAST Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IAST Annotation</em>'.
	 * @generated
	 */
	IASTAnnotation createIASTAnnotation();

	/**
	 * Returns a new object of class '<em>File Location Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>File Location Annotation</em>'.
	 * @generated
	 */
	FileLocationAnnotation createFileLocationAnnotation();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	AnnotationsPackage getAnnotationsPackage();

} //AnnotationsFactory
