/**
 */
package gecos.gecosproject;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Gecos Header Directory</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.gecosproject.GecosHeaderDirectory#isIsStandardHeader <em>Is Standard Header</em>}</li>
 * </ul>
 *
 * @see gecos.gecosproject.GecosprojectPackage#getGecosHeaderDirectory()
 * @model
 * @generated
 */
public interface GecosHeaderDirectory extends GecosFile {
	/**
	 * Returns the value of the '<em><b>Is Standard Header</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * true if this header file is actually a standard library header
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Is Standard Header</em>' attribute.
	 * @see #setIsStandardHeader(boolean)
	 * @see gecos.gecosproject.GecosprojectPackage#getGecosHeaderDirectory_IsStandardHeader()
	 * @model unique="false"
	 * @generated
	 */
	boolean isIsStandardHeader();

	/**
	 * Sets the value of the '{@link gecos.gecosproject.GecosHeaderDirectory#isIsStandardHeader <em>Is Standard Header</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Standard Header</em>' attribute.
	 * @see #isIsStandardHeader()
	 * @generated
	 */
	void setIsStandardHeader(boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitHeaderDirectory(this);'"
	 * @generated
	 */
	void accept(ProjectVisitor visitor);

} // GecosHeaderDirectory
