package fr.irisa.cairn.gecos.model.utils.dotprinter;

import fr.irisa.cairn.gecos.model.extensions.outputs.GecosOutput;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;

/**
 * Link to the output script primitive and the dot printer.
 * 
 * @author Antoine Floc'h - Initial Contribution and API
 * 
 */
public class GecosCoreOutput extends GecosOutput {

	public void print(ProcedureSet ps) {
		GecosDotPrinter printer = new GecosDotPrinter(file, ps);
		printer.compute();
	}

	public void print(Procedure p) {
		GecosDotPrinter printer = new GecosDotPrinter(file, p);
		printer.compute();
	}
	
	public void print(GecosProject p) {
		GecosDotPrinter printer = new GecosDotPrinter(file, p);
		printer.compute();
	}

}
