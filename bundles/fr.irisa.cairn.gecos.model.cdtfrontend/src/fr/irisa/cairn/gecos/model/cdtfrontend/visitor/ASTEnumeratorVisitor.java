/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.cdtfrontend.visitor;

import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import gecos.instrs.Instruction;
import gecos.types.Enumerator;

import org.eclipse.cdt.core.dom.ast.IASTEnumerationSpecifier.IASTEnumerator;

public class ASTEnumeratorVisitor {

	private Enumerator enumerator;
	
	public ASTEnumeratorVisitor() {
	}
	
	public Enumerator getEnumerator() {
		return enumerator;
	}
	
	public void visit(CDTToGecosModel gecosModel, IASTEnumerator enumerator) {
		enumerator.getName().accept(gecosModel);
		String name = ((ASTNameVisitor)gecosModel.getObject()).getName();
		if (enumerator.getValue() == null) {
			this.enumerator = GecosUserTypeFactory.ENUMERATOR(name, null);
		} else {
			enumerator.getValue().accept(gecosModel);
			Instruction value = ((ASTExpressionVisitor)gecosModel.getObject()).getInstruction();
			this.enumerator = GecosUserTypeFactory.ENUMERATOR(name, value);
		}
	}
}