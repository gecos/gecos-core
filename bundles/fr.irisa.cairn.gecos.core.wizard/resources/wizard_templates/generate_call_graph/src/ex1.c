extern void func1();
extern void func2();
extern void func3();
extern void func4();
extern void func5();

void func1() { return;}
void func2() { func3();}
void func3() { func4();}
void func4() { func2(); func1();}
void func5() { func4();}

void main(){
	func1();
	func2();
	func3();
	func4();
	func5();
}
