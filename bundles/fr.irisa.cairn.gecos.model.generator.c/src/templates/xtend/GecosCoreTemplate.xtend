package templates.xtend

import fr.irisa.cairn.gecos.model.c.generator.ExtendableAnnotationCGenerator
import fr.irisa.cairn.gecos.model.c.generator.ExtendableBlockCGenerator
import fr.irisa.cairn.gecos.model.c.generator.ExtendableCompleteTypeCGenerator
import fr.irisa.cairn.gecos.model.c.generator.ExtendableCoreCGenerator
import fr.irisa.cairn.gecos.model.c.generator.ExtendableInstructionCGenerator
import fr.irisa.cairn.gecos.model.c.generator.ExtendableTypeCGenerator
import gecos.annotations.AnnotatedElement
import gecos.annotations.CommentAnnotation
import gecos.annotations.PragmaAnnotation
import gecos.core.Procedure
import gecos.core.ProcedureSet
import gecos.core.ProcedureSymbol
import gecos.core.Scope
import gecos.core.Symbol
import gecos.types.AliasType
import gecos.types.EnumType
import gecos.types.FunctionType
import gecos.types.RecordType
import gecos.types.Type
import org.eclipse.emf.ecore.EObject
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory
import gecos.annotations.AnnotationKeys

class GecosCoreTemplate extends GecosTemplate {
	

	def dispatch generate(EObject object) {
		null
	}
	
	def generateMacros (ProcedureSet procedureSet) {
		val stringBuffer = new StringBuffer()
		
		//check if a min, max. ceil or floor instruction has been generated in any of the procedures
		if(procedureSet.annotations.containsKey("need_define_min")) {
			stringBuffer.append('''
				#ifndef _min_
					#define _min_(a, b)       ((a) < (b) ? (a) : (b))
				#endif
				''')
			procedureSet.annotations.removeKey("need_define_min")
		}
		if(procedureSet.annotations.containsKey("need_define_max")) {
			stringBuffer.append('''
				#ifndef _max_
					#define _max_(a, b)       ((a) < (b) ? (b) : (a))
				#endif
				''')
				procedureSet.annotations.removeKey("need_define_max")
		}
		if(procedureSet.annotations.containsKey("need_define_ceil")) {
			stringBuffer.append('''
				#ifndef _ceil_
					#define _ceil_(a,b) (((a)/(b)) + ((!(((a) >= 0) ^ ((b) >= 0))) ? ((((a)%(b)) == 0)?0:1):0))
				#endif
				''')
				procedureSet.annotations.removeKey("need_define_ceil")
		}
		if(procedureSet.annotations.containsKey("need_define_floor")) {
			stringBuffer.append('''
				#ifndef _floor_
					#define _floor_(a,b) (((a)/(b)) - ((!(((a) >= 0) ^ ((b) >= 0))) ? 0:(((a)%(b)) == 0)?0:1))
				#endif
				''')
				procedureSet.annotations.removeKey("need_define_floor")
		}
		if(procedureSet.annotations.containsKey("need_define_mod")) {
			stringBuffer.append('''
				#ifndef _mmod_
					#define _mmod_(a,b) (((a)>=0)?((a)%(b)):(((a)%(b)!=0)?((b)+(a)%(b)):0))
				#endif
				''')
				procedureSet.annotations.removeKey("need_define_floor")
		}
		if (stringBuffer.length > 0)
			stringBuffer.append("\n")
				
		'''«stringBuffer.toString»'''
	}

	def dispatch generate(ProcedureSet procedureSet){
		val stringBuffer = new StringBuffer();
		
		//first visit all procedures so that, if some macros are needed they will be added as annotations to the procedure set 
		for(Procedure procedure : procedureSet.listProcedures) {
			stringBuffer.append('''«ExtendableCoreCGenerator::eInstance.generate(procedure)»

			''');
		}
		
		val head = new String(
			generatePragma(procedureSet) + "\n" + 
			generateMacros(procedureSet) +  
			ExtendableCoreCGenerator::eInstance.generate(procedureSet.scope) + "\n")
		
		stringBuffer.insert(0, head)
		
		'''«stringBuffer.toString»'''
	}

	def dispatch generate(Procedure procedure) {
		val buffer = new StringBuffer()
		
		if (procedure.pragma !== null && !procedure.pragma.content.empty)
			buffer.append('''«generatePragma(procedure)»
				''')
		
		buffer.append(generateProcedureSym(procedure.symbol, true))
		
		if (procedure.body === null)
			buffer.append(''';''')
		else
			buffer.append(" "+ExtendableBlockCGenerator::eInstance.generate(procedure.body))
		
		'''«buffer.toString»'''
	}
	
	def dispatch generate(ProcedureSymbol symbol) {
		'''«generateProcedureSym(symbol, false)»'''
	}
	
	def generateProcedureSym(ProcedureSymbol symbol, boolean genPragmas) {
		val buffer = new StringBuffer()
		var first = true;
		val fType = symbol.type as FunctionType
		
		afterSymbolInformation.setLength(0)
		if(fType.isInline) buffer.append('''inline ''')
		buffer.append('''«ExtendableTypeCGenerator::eInstance.generate(fType.returnType)» «symbol.name»''')
		val afterSaved = afterSymbolInformation.toString
		
		buffer.append('''(''')
		for (Symbol s : symbol.listParameters) {
			if (first) first = false
			else buffer.append(''', ''')
			
			if(genPragmas) {
				val pragmas = generatePragma(s)
				if(pragmas.length != 0)
					buffer.append("\n" + pragmas + "	");
			}
			
			afterSymbolInformation.setLength(0)
			buffer.append('''«ExtendableCoreCGenerator::eInstance.generate(s)»''')
			buffer.append(afterSymbolInformation)
		}
		if (fType.hasElipsis)
			buffer.append('''«IF !first», «ENDIF»...''')
		buffer.append(''')''')
		
		buffer.append(afterSaved)
		afterSymbolInformation.setLength(0);
		'''«buffer.toString»'''
	}

	def dispatch generate(Scope scope) {
		val buffer = new StringBuffer()
		//FIXME should keep order between types and symbols
		//e.g: typedef int T; { T a; typedef float T; T b; }
		//  a should be 'int' and b 'float' however, currently we generate:
		// typedef int T; { typedef float T; T a; T b; } which makes both a and b 'int'
		for(Type type : scope.types) {
			if ((type instanceof AliasType || type instanceof RecordType || type instanceof EnumType) && mustBeGenerated(type))
				buffer.append('''
				«generateComment(type)»
				«generatePragma(type)»
				«ExtendableCompleteTypeCGenerator::eInstance.generate(type)»;
				''')
				afterSymbolInformation.setLength(0);
		}
		for(Symbol symbol : scope.symbols) {
			if (mustBeGenerated(symbol)) {
				buffer.append('''
				«generateComment(symbol)»
				«generatePragma(symbol)»
				«ExtendableCoreCGenerator::eInstance.generate(symbol)»;
				''')
			}
		}
		
		if (buffer.length > 0)
			buffer.append("\n")
				
		'''«buffer»'''
	}

	def dispatch generate(Symbol symbol) {
		val buffer = new StringBuffer()
		afterSymbolInformation.setLength(0)
		buffer.append('''«ExtendableTypeCGenerator::eInstance.generate(symbol.type)» «symbol.name»«afterSymbolInformation.toString»''')
		if (symbol.value !== null) {
			buffer.append(''' = «ExtendableInstructionCGenerator::eInstance.generate(symbol.value)»''')
		}
		afterSymbolInformation.setLength(0);
		'''«buffer»'''
	}
	
	def generatePragma(AnnotatedElement annotatedElement) {
		'''«if (annotatedElement.pragma !== null && !annotatedElement.pragma.content.empty) 
			ExtendableAnnotationCGenerator::eInstance.generate(annotatedElement.pragma)»'''
	}
	
	def generateComment(AnnotatedElement annotatedElement) {
		val com = annotatedElement.getAnnotation(AnnotationKeys.COMMENT_ANNOTATION_KEY.literal) as CommentAnnotation
		'''«if (com !== null && !com.content.empty) 
			ExtendableAnnotationCGenerator::eInstance.generate(com)»'''
	}
	
	def boolean mustBeGenerated(Type type) {
		return !isIgnorePragma(type.pragma)
	}
	
	def boolean mustBeGenerated(Symbol symbol) {
		return !isIgnorePragma(symbol.pragma)
	}
	
	def boolean isIgnorePragma(PragmaAnnotation pragmaAnnotation) {
		return (pragmaAnnotation !== null) && (pragmaAnnotation.content !== null) && 
			(pragmaAnnotation.content.contains(GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION) || pragmaAnnotation.content.contains("ignore"))
	}
	
}