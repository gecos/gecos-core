/**
 */
package gecos.core.impl;

import gecos.core.CorePackage;
import gecos.core.CoreVisitor;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSymbol;
import gecos.core.ScopeContainer;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameter Symbol</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ParameterSymbolImpl extends SymbolImpl implements ParameterSymbol {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParameterSymbolImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.PARAMETER_SYMBOL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Procedure findProcedure() {
		Procedure _xblockexpression = null;
		{
			ScopeContainer _container = this.getContainingScope().getContainer();
			final ProcedureSymbol procedureSymbol = ((ProcedureSymbol) _container);
			_xblockexpression = procedureSymbol.getProcedure();
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		return super.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final CoreVisitor visitor) {
		visitor.visitParameterSymbol(this);
	}

} //ParameterSymbolImpl
