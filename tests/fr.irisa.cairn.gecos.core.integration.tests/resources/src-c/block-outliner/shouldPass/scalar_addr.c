int main() {
	int k;
	int *pK;

	#pragma outline
	{
		pK = &k;
	}

	if(pK != &k)
		abort();

	return 0;
}
