package fr.irisa.cairn.gecos.model.utils;

import org.eclipse.emf.common.util.EList;

import com.google.common.base.Preconditions;

import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSymbol;
import gecos.instrs.CallInstruction;

public class InstructionUtils {

	public static Procedure findCalledProcedure(CallInstruction call) {
		if(call.getProcedureSymbol() instanceof ProcedureSymbol)
			return ((ProcedureSymbol) call.getProcedureSymbol()).getProcedure();
		return null;
	}
	
	public static ParameterSymbol findCalledProcedureParameter(CallInstruction call, int paramIndex) {
		Preconditions.checkArgument(paramIndex >= 0, "parameter index must be non-negative");
		Procedure calledProc = findCalledProcedure(call);
		if(calledProc != null) {
			EList<ParameterSymbol> params = calledProc.listParameters();
			Preconditions.checkArgument(paramIndex < params.size(), "parameter index out of range");
			return params.get(paramIndex);
		}
		return null;
	}
	
}
