package fr.irisa.cairn.gecos.model.analysis.instructions.comparator;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import com.google.common.base.Strings;

import fr.irisa.cairn.gecos.model.analysis.instructions.comparator.InstructionComparator;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;
import tom.mapping.GenericIntrospector;

@SuppressWarnings("all")
public class InstructionComparatorUtils {


private static boolean tom_equal_term_Strategy(Object t1, Object t2) {
return  (t1.equals(t2)) ;
}
private static boolean tom_is_sort_Strategy(Object t) {
return  (t instanceof tom.library.sl.Strategy) ;
}
private static boolean tom_equal_term_Position(Object t1, Object t2) {
return  (t1.equals(t2)) ;
}
private static boolean tom_is_sort_Position(Object t) {
return  (t instanceof tom.library.sl.Position) ;
}
private static boolean tom_equal_term_int(int t1, int t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_int(int t) {
return  true ;
}
private static boolean tom_equal_term_char(char t1, char t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_char(char t) {
return  true ;
}
private static boolean tom_equal_term_String(String t1, String t2) {
return  t1.equals(t2) ;
}
private static boolean tom_is_sort_String(String t) {
return  t instanceof String ;
}
private static  tom.library.sl.Strategy  tom_make_mu( tom.library.sl.Strategy  var,  tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.Mu(var,v) );
}
private static  tom.library.sl.Strategy  tom_make_MuVar( String  name) { 
return ( new tom.library.sl.MuVar(name) );
}
private static  tom.library.sl.Strategy  tom_make_Identity() { 
return ( new tom.library.sl.Identity() );
}
private static  tom.library.sl.Strategy  tom_make_One( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.One(v) );
}
private static  tom.library.sl.Strategy  tom_make_All( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.All(v) );
}
private static  tom.library.sl.Strategy  tom_make_Fail() { 
return ( new tom.library.sl.Fail() );
}
private static  tom.library.sl.Strategy  tom_make_Not( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.Not(v) );
}
private static boolean tom_is_fun_sym_Sequence( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.Sequence );
}
private static  tom.library.sl.Strategy  tom_empty_list_Sequence() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_Sequence( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.Sequence.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Sequence.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Sequence.THEN) );
}
private static boolean tom_is_empty_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( t == null );
}

  private static   tom.library.sl.Strategy  tom_append_list_Sequence( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 == null )) {
      return l2;
    } else if(( l2 == null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.Sequence )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.THEN) ) == null )) {
        return  tom.library.sl.Sequence.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.Sequence.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.FIRST) ),tom_append_list_Sequence(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.Sequence.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_Sequence( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end == null ) ||  (end.equals(tom_empty_list_Sequence())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.Sequence.make(((( begin instanceof tom.library.sl.Sequence ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Sequence.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_Sequence(((( begin instanceof tom.library.sl.Sequence ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Sequence.THEN) ):tom_empty_list_Sequence()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_Choice( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.Choice );
}
private static  tom.library.sl.Strategy  tom_empty_list_Choice() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_Choice( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.Choice.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Choice.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Choice.THEN) );
}
private static boolean tom_is_empty_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( t ==null );
}

  private static   tom.library.sl.Strategy  tom_append_list_Choice( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 ==null )) {
      return l2;
    } else if(( l2 ==null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.Choice )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.THEN) ) ==null )) {
        return  tom.library.sl.Choice.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.Choice.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.FIRST) ),tom_append_list_Choice(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.Choice.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_Choice( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end ==null ) ||  (end.equals(tom_empty_list_Choice())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.Choice.make(((( begin instanceof tom.library.sl.Choice ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Choice.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_Choice(((( begin instanceof tom.library.sl.Choice ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Choice.THEN) ):tom_empty_list_Choice()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_SequenceId( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.SequenceId );
}
private static  tom.library.sl.Strategy  tom_empty_list_SequenceId() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_SequenceId( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.SequenceId.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.SequenceId.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.SequenceId.THEN) );
}
private static boolean tom_is_empty_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( t == null );
}

  private static   tom.library.sl.Strategy  tom_append_list_SequenceId( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 == null )) {
      return l2;
    } else if(( l2 == null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.SequenceId )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.THEN) ) == null )) {
        return  tom.library.sl.SequenceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.SequenceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.FIRST) ),tom_append_list_SequenceId(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.SequenceId.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_SequenceId( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end == null ) ||  (end.equals(tom_empty_list_SequenceId())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.SequenceId.make(((( begin instanceof tom.library.sl.SequenceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.SequenceId.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_SequenceId(((( begin instanceof tom.library.sl.SequenceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.SequenceId.THEN) ):tom_empty_list_SequenceId()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_ChoiceId( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.ChoiceId );
}
private static  tom.library.sl.Strategy  tom_empty_list_ChoiceId() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_ChoiceId( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.ChoiceId.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.ChoiceId.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.ChoiceId.THEN) );
}
private static boolean tom_is_empty_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( t ==null );
}

  private static   tom.library.sl.Strategy  tom_append_list_ChoiceId( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 ==null )) {
      return l2;
    } else if(( l2 ==null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.ChoiceId )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.THEN) ) ==null )) {
        return  tom.library.sl.ChoiceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.ChoiceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.FIRST) ),tom_append_list_ChoiceId(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.ChoiceId.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_ChoiceId( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end ==null ) ||  (end.equals(tom_empty_list_ChoiceId())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.ChoiceId.make(((( begin instanceof tom.library.sl.ChoiceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.ChoiceId.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_ChoiceId(((( begin instanceof tom.library.sl.ChoiceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.ChoiceId.THEN) ):tom_empty_list_ChoiceId()),end,tail)) ;
  }
  private static  tom.library.sl.Strategy  tom_make_OneId( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.OneId(v) );
}
private static  tom.library.sl.Strategy  tom_make_AllSeq( tom.library.sl.Strategy  s) { 
return ( new tom.library.sl.AllSeq(s) );
}
private static  tom.library.sl.Strategy  tom_make_AUCtl( tom.library.sl.Strategy  s1,  tom.library.sl.Strategy  s2) { 
return ( 
tom_make_mu(tom_make_MuVar("x"),tom_cons_list_Choice(s2,tom_cons_list_Choice(tom_cons_list_Sequence(tom_cons_list_Sequence(s1,tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("x")),tom_empty_list_Sequence())),tom_cons_list_Sequence(tom_make_One(tom_make_Identity()),tom_empty_list_Sequence())),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_EUCtl( tom.library.sl.Strategy  s1,  tom.library.sl.Strategy  s2) { 
return ( 
tom_make_mu(tom_make_MuVar("x"),tom_cons_list_Choice(s2,tom_cons_list_Choice(tom_cons_list_Sequence(s1,tom_cons_list_Sequence(tom_make_One(tom_make_MuVar("x")),tom_empty_list_Sequence())),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_Try( tom.library.sl.Strategy  s) { 
return ( 
tom_cons_list_Choice(s,tom_cons_list_Choice(tom_make_Identity(),tom_empty_list_Choice())))
;
}
private static  tom.library.sl.Strategy  tom_make_Repeat( tom.library.sl.Strategy  s) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Choice(tom_cons_list_Sequence(s,tom_cons_list_Sequence(tom_make_MuVar("_x"),tom_empty_list_Sequence())),tom_cons_list_Choice(tom_make_Identity(),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_TopDown( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Sequence(v,tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("_x")),tom_empty_list_Sequence()))))
;
}
private static  tom.library.sl.Strategy  tom_make_OnceTopDown( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Choice(v,tom_cons_list_Choice(tom_make_One(tom_make_MuVar("_x")),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_Innermost( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("_x")),tom_cons_list_Sequence(tom_make_Try(tom_cons_list_Sequence(v,tom_cons_list_Sequence(tom_make_MuVar("_x"),tom_empty_list_Sequence()))),tom_empty_list_Sequence()))))
;
}
private static  tom.library.sl.Strategy  tom_make_Outermost( tom.library.sl.Strategy  v) { 
return ( 
tom_make_Repeat(tom_make_OnceTopDown(v)))
;
}
private static  tom.library.sl.Strategy  tom_make_RepeatId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_SequenceId(v,tom_cons_list_SequenceId(tom_make_MuVar("_x"),tom_empty_list_SequenceId()))))
;
}
private static  tom.library.sl.Strategy  tom_make_OnceTopDownId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_ChoiceId(v,tom_cons_list_ChoiceId(tom_make_OneId(tom_make_MuVar("_x")),tom_empty_list_ChoiceId()))))
;
}
private static boolean tom_equal_term_EELong(Object l1, Object l2) {
return l1.equals(l2);
}
private static boolean tom_is_sort_EELong(Object t) {
return t instanceof java.lang.Long;
}
private static boolean tom_equal_term_BlockCopyManager(Object l1, Object l2) {
return l1.equals(l2);
}
private static boolean tom_is_sort_BlockCopyManager(Object t) {
return t instanceof fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;
}
private static boolean tom_equal_term_DependencyType(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_DependencyType(Object t) {
return t instanceof DependencyType;
}
private static boolean tom_equal_term_DAGOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_DAGOperator(Object t) {
return t instanceof DAGOperator;
}
private static boolean tom_equal_term_ArithmeticOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ArithmeticOperator(Object t) {
return t instanceof ArithmeticOperator;
}
private static boolean tom_equal_term_ComparisonOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ComparisonOperator(Object t) {
return t instanceof ComparisonOperator;
}
private static boolean tom_equal_term_LogicalOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_LogicalOperator(Object t) {
return t instanceof LogicalOperator;
}
private static boolean tom_equal_term_BitwiseOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_BitwiseOperator(Object t) {
return t instanceof BitwiseOperator;
}
private static boolean tom_equal_term_SelectOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_SelectOperator(Object t) {
return t instanceof SelectOperator;
}
private static boolean tom_equal_term_ReductionOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ReductionOperator(Object t) {
return t instanceof ReductionOperator;
}
private static boolean tom_equal_term_BranchType(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_BranchType(Object t) {
return t instanceof BranchType;
}
private static boolean tom_equal_term_StorageClassSpecifiers(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_StorageClassSpecifiers(Object t) {
return t instanceof StorageClassSpecifiers;
}
private static boolean tom_equal_term_Kinds(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_Kinds(Object t) {
return t instanceof Kinds;
}
private static boolean tom_equal_term_IntegerTypes(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_IntegerTypes(Object t) {
return t instanceof IntegerTypes;
}
private static boolean tom_equal_term_SignModifiers(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_SignModifiers(Object t) {
return t instanceof SignModifiers;
}
private static boolean tom_equal_term_FloatPrecisions(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_FloatPrecisions(Object t) {
return t instanceof FloatPrecisions;
}
private static boolean tom_equal_term_OverflowMode(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_OverflowMode(Object t) {
return t instanceof OverflowMode;
}
private static boolean tom_equal_term_QuantificationMode(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_QuantificationMode(Object t) {
return t instanceof QuantificationMode;
}
private static boolean tom_equal_term_Inst(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Inst(Object t) {
return t instanceof Instruction;
}
private static boolean tom_equal_term_Blk(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Blk(Object t) {
return t instanceof Block;
}
private static boolean tom_equal_term_Sym(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Sym(Object t) {
return t instanceof Symbol;
}
private static boolean tom_equal_term_SymL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_SymL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Symbol>)t).size() == 0 
    	|| (((EList<Symbol>)t).size()>0 && ((EList<Symbol>)t).get(0) instanceof Symbol));
}
private static boolean tom_equal_term_Type(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Type(Object t) {
return t instanceof Type;
}
private static boolean tom_equal_term_TypeL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_TypeL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Type>)t).size() == 0 
    	|| (((EList<Type>)t).size()>0 && ((EList<Type>)t).get(0) instanceof Type));
}
private static boolean tom_equal_term_Field(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Field(Object t) {
return t instanceof Field;
}
private static boolean tom_equal_term_FieldL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_FieldL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Field>)t).size() == 0 
    	|| (((EList<Field>)t).size()>0 && ((EList<Field>)t).get(0) instanceof Field));
}
private static boolean tom_equal_term_InstL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_InstL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Instruction>)t).size() == 0 
    	|| (((EList<Instruction>)t).size()>0 && ((EList<Instruction>)t).get(0) instanceof Instruction));
}
private static boolean tom_is_fun_sym_InstL( EList<Instruction>  t) {
return  t instanceof EList<?> &&
 		(((EList<Instruction>)t).size() == 0 
 		|| (((EList<Instruction>)t).size()>0 && ((EList<Instruction>)t).get(0) instanceof Instruction));
}
private static  EList<Instruction>  tom_empty_array_InstL(int n) { 
return  new BasicEList<Instruction>(n) ;
}
private static  EList<Instruction>  tom_cons_array_InstL(Instruction e,  EList<Instruction>  l) { 
return  append(e,l) ;
}
private static Instruction tom_get_element_InstL_InstL( EList<Instruction>  l, int n) {
return  l.get(n) ;
}
private static int tom_get_size_InstL_InstL( EList<Instruction>  l) {
return  l.size() ;
}

  private static   EList<Instruction>  tom_get_slice_InstL( EList<Instruction>  subject, int begin, int end) {
     EList<Instruction>  result =  new BasicEList<Instruction>(end-begin) ;
    while(begin!=end) {
      result =  append( subject.get(begin) ,result) ;
      begin++;
    }
    return result;
  }

  private static   EList<Instruction>  tom_append_array_InstL( EList<Instruction>  l2,  EList<Instruction>  l1) {
    int size1 =  l1.size() ;
    int size2 =  l2.size() ;
    int index;
     EList<Instruction>  result =  new BasicEList<Instruction>(size1+size2) ;
    index=size1;
    while(index >0) {
      result =  append( l1.get(size1-index) ,result) ;
      index--;
    }

    index=size2;
    while(index > 0) {
      result =  append( l2.get(size2-index) ,result) ;
      index--;
    }
    return result;
  }private static boolean tom_equal_term_BlkL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_BlkL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Block>)t).size() == 0 
    	|| (((EList<Block>)t).size()>0 && ((EList<Block>)t).get(0) instanceof Block));
}
private static boolean tom_equal_term_Node(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Node(Object t) {
return t instanceof DAGNode;
}
private static boolean tom_equal_term_NodeL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_NodeL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<DAGNode>)t).size() == 0 
    	|| (((EList<DAGNode>)t).size()>0 && ((EList<DAGNode>)t).get(0) instanceof DAGNode));
}
private static boolean tom_equal_term_boolean(boolean t1, boolean t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_boolean(boolean t) {
return  true ;
}
private static boolean tom_equal_term_long(long t1, long t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_long(long t) {
return  true ;
}
private static boolean tom_equal_term_float(float t1, float t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_float(float t) {
return  true ;
}
private static boolean tom_equal_term_double(double t1, double t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_double(double t) {
return  true ;
}


private static <O> EList<O> enforce(EList l) {
return l;
}

private static <O> EList<O> append(O e,EList<O> l) {
l.add(e);
return l;
}
private static boolean tom_is_fun_sym_ival(Instruction t) {
return t instanceof IntInstruction;
}
private static  long  tom_get_slot_ival_value(Instruction t) {
return ((IntInstruction)t).getValue();
}
private static boolean tom_is_fun_sym_fval(Instruction t) {
return t instanceof FloatInstruction;
}
private static  double  tom_get_slot_fval_value(Instruction t) {
return ((FloatInstruction)t).getValue();
}
private static boolean tom_is_fun_sym_generic(Instruction t) {
return t instanceof GenericInstruction;
}
private static  String  tom_get_slot_generic_name(Instruction t) {
return ((GenericInstruction)t).getName();
}
private static  EList<Instruction>  tom_get_slot_generic_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_add(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("add");
}
private static  EList<Instruction>  tom_get_slot_add_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_sub(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("sub");
}
private static  EList<Instruction>  tom_get_slot_sub_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_mul(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("mul");
}
private static  EList<Instruction>  tom_get_slot_mul_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_div(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("div");
}
private static  EList<Instruction>  tom_get_slot_div_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_neg(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("neg");
}
private static  EList<Instruction>  tom_get_slot_neg_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}


private static boolean VERBOSE = false;

private static int _space = 0;
private static void INC() {_space++;}
private static void DEC() {_space--;}

public static void rdebug(String name, Instruction... instructions) {
debug(Strings.repeat("  ", _space) + "rule " + name + ": ", instructions);
}
public static void sdebug(String msg, Instruction... instructions) {
debug(Strings.repeat("  ", _space) + msg, instructions);
}
public static void debug(String msg, Instruction... instructions) {
if(VERBOSE)
System.out.println(msg + InstructionComparator.instToString(instructions));
}


/**
* 1- Convert all sub(div) to add(mul) and merge adds(muls). Also eliminate neg and replace it by negation annotation
* 2- Simplify and Sort children as follows: first generic instructions (name natural order), 
* then symbols (name natural order), then constants (numeric order).
* - All constants are then merged into one ConstantInstruction.
* - Identical Symbols are merged: inverse pairs are eliminated, the rest (in case of ADD only), 
* if more than one (n), are replaced with a "MUL(symbol, n)" properly reordered.
*
* TODO support mul (similar to add) and div (similar to sub)
*
* @return Simplified Intruction in canonical form. Negation/Invertion are represented using 
* annotations and are not reflected in the instruction object !!! 
*/
public static Instruction makeCanonical(Instruction inst) {
try {
_space = 0; debug("\n> makeCanonical: ", inst);

/* XXX make sure inst has a container */
BasicBlock tmpContainer = null;
if(inst.eContainer() == null) {
tmpContainer = GecosUserBlockFactory.BBlock(inst);
}

//			inst = `Innermost(removeNeg()).visitLight((inst), GenericIntrospector.INSTANCE);
//			sdebug("|_", inst);

/* Convert all sub(div) to add(mul) and merge adds(muls) */
_space = 0;
inst = 
tom_make_Repeat(tom_cons_list_Sequence(tom_make_Outermost(tom_make_reduce()),tom_cons_list_Sequence(tom_make_Not(tom_make_stop()),tom_empty_list_Sequence()))).visitLight((inst), GenericIntrospector.INSTANCE);
sdebug("|_", inst);

/* 
* - Simplify and Sort children as follows: first generic instructions (name lexical order), 
* then symbols (lexical order), then constants (numeric order).
* - All constants are then merged into one ConstantInstruction.
* - Identical Symbols are merged: inverse pairs are eliminated, the rest (in case of ADD only), 
* if more than one (n), are replaced with a "MUL(symbol, n)" properly reordered.
*/
_space = 0;

tom_make_Innermost(tom_make_sortAndSimplify()).visitLight((inst), GenericIntrospector.INSTANCE);

/* XXX Restore container */
if(tmpContainer != null) {
if(tmpContainer.getInstructionCount() == 0) {
/* inst has been removed i.e. inst is equivalent to 0 */
inst = GecosUserInstructionFactory.Int(0);
}
else {
inst = tmpContainer.getFirstInstruction();
}
tmpContainer.removeAllInstructions();
}

_space = 0; sdebug("< ", 
inst);
} 
catch(Exception e) {
throw new RuntimeException(e);
}
return inst;
}

//	%strategy removeNeg() extends Fail() {
//		visit Inst {
//			inst@neg(InstL(a)) -> {
//				INC(); rdebug("neg(a)", `inst);
//				if(!InstructionComparator.isNeg(`inst))
//					InstructionComparator.negate(`a);
//				`inst.substituteWith(`a);
//				sdebug("|_ ", `a);
//				sdebug("|_ ", `a.getParent());
//			}
//		}
//	} 


public static class reduce extends tom.library.sl.AbstractStrategyBasic {
public reduce() {
super(tom_make_Fail());
}
public tom.library.sl.Visitable[] getChildren() {
tom.library.sl.Visitable[] stratChildren = new tom.library.sl.Visitable[getChildCount()];
stratChildren[0] = super.getChildAt(0);
return stratChildren;}
public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
super.setChildAt(0, children[0]);
return this;
}
public int getChildCount() {
return 1;
}
public tom.library.sl.Visitable getChildAt(int index) {
switch (index) {
case 0: return super.getChildAt(0);
default: throw new IndexOutOfBoundsException();
}
}
public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
switch (index) {
case 0: return super.setChildAt(0, child);
default: throw new IndexOutOfBoundsException();
}
}
@SuppressWarnings("unchecked")
public <T> T visitLight(T v, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
if (tom_is_sort_Inst(v)) {
return ((T)visit_Inst(((Instruction)v),introspector));
}
if (!(( null  == environment))) {
return ((T)any.visit(environment,introspector));
} else {
return any.visitLight(v,introspector);
}
}
@SuppressWarnings("unchecked")
public Instruction _visit_Inst(Instruction arg, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
if (!(( null  == environment))) {
return ((Instruction)any.visit(environment,introspector));
} else {
return any.visitLight(arg,introspector);
}
}
@SuppressWarnings("unchecked")
public Instruction visit_Inst(Instruction tom__arg, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
{
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_2=tom_get_slot_generic_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_2))) {
int tomMatch1_end_9=0;
do {
{
if (!(tomMatch1_end_9 >= tom_get_size_InstL_InstL(tomMatch1_2))) {
Instruction tomMatch1_13=tom_get_element_InstL_InstL(tomMatch1_2,tomMatch1_end_9);
if (tom_is_sort_Inst(tomMatch1_13)) {
if (tom_is_fun_sym_neg(((Instruction)tomMatch1_13))) {
 EList<Instruction>  tomMatch1_12=tom_get_slot_neg_children(tomMatch1_13);
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_12))) {
int tomMatch1_16=0;
if (!(tomMatch1_16 >= tom_get_size_InstL_InstL(tomMatch1_12))) {
Instruction tom_a=tom_get_element_InstL_InstL(tomMatch1_12,tomMatch1_16);
if (tomMatch1_16 + 1 >= tom_get_size_InstL_InstL(tomMatch1_12)) {
Instruction tom_ineg=tom_get_element_InstL_InstL(tomMatch1_2,tomMatch1_end_9);

INC(); rdebug("gen(*, neg(a), *)", 
((Instruction)tom__arg));
if(!InstructionComparator.isNeg(
tom_ineg))
InstructionComparator.negate(
tom_a);

tom_ineg.substituteWith(
tom_a);

}
}
}
}
}
}
tomMatch1_end_9=tomMatch1_end_9 + 1;
}
} while(!(tomMatch1_end_9 > tom_get_size_InstL_InstL(tomMatch1_2)));
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_sub(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_19=tom_get_slot_sub_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_19))) {
int tomMatch1_23=0;
if (!(tomMatch1_23 >= tom_get_size_InstL_InstL(tomMatch1_19))) {
int tomMatch1_24=tomMatch1_23 + 1;
if (!(tomMatch1_24 >= tom_get_size_InstL_InstL(tomMatch1_19))) {
if (tomMatch1_24 + 1 >= tom_get_size_InstL_InstL(tomMatch1_19)) {
Instruction tom_inst=((Instruction)tom__arg);

INC(); rdebug("sub(a,b)", 
tom_inst);
//`b.substituteWith(negate(`b));
InstructionComparator.negate(
tom_get_element_InstL_InstL(tomMatch1_19,tomMatch1_24));
((GenericInstruction)
tom_inst).setName(ArithmeticOperator.ADD.getLiteral());

}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_add(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_27=tom_get_slot_add_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_27))) {
int tomMatch1_end_34=0;
do {
{
if (!(tomMatch1_end_34 >= tom_get_size_InstL_InstL(tomMatch1_27))) {
Instruction tomMatch1_38=tom_get_element_InstL_InstL(tomMatch1_27,tomMatch1_end_34);
if (tom_is_sort_Inst(tomMatch1_38)) {
if (tom_is_fun_sym_add(((Instruction)tomMatch1_38))) {
 EList<Instruction>  tomMatch1_37=tom_get_slot_add_children(tomMatch1_38);
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_37))) {
Instruction tom_ad2=tom_get_element_InstL_InstL(tomMatch1_27,tomMatch1_end_34);
Instruction tom_inst=((Instruction)tom__arg);

INC(); rdebug("add(*,add(*),*)", 
tom_inst);
((GenericInstruction)
tom_inst).getChildren().remove(
tom_ad2);
boolean neg = InstructionComparator.isNeg(
tom_ad2);
for(Instruction arg : 
tom_get_slice_InstL(tomMatch1_37,0,tom_get_size_InstL_InstL(tomMatch1_37))) {
if(neg) InstructionComparator.negate(arg);
((GenericInstruction)
tom_inst).getChildren().add(arg);
}

}
}
}
}
tomMatch1_end_34=tomMatch1_end_34 + 1;
}
} while(!(tomMatch1_end_34 > tom_get_size_InstL_InstL(tomMatch1_27)));
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_div(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_44=tom_get_slot_div_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_44))) {
int tomMatch1_48=0;
if (!(tomMatch1_48 >= tom_get_size_InstL_InstL(tomMatch1_44))) {
int tomMatch1_49=tomMatch1_48 + 1;
if (!(tomMatch1_49 >= tom_get_size_InstL_InstL(tomMatch1_44))) {
if (tomMatch1_49 + 1 >= tom_get_size_InstL_InstL(tomMatch1_44)) {

INC(); rdebug("div(*,div(*),*)", 
((Instruction)tom__arg));
throw new RuntimeException("div not yet implemented");

}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_mul(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_52=tom_get_slot_mul_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_52))) {
int tomMatch1_end_59=0;
do {
{
if (!(tomMatch1_end_59 >= tom_get_size_InstL_InstL(tomMatch1_52))) {
Instruction tomMatch1_63=tom_get_element_InstL_InstL(tomMatch1_52,tomMatch1_end_59);
if (tom_is_sort_Inst(tomMatch1_63)) {
if (tom_is_fun_sym_mul(((Instruction)tomMatch1_63))) {
 EList<Instruction>  tomMatch1_62=tom_get_slot_mul_children(tomMatch1_63);
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_62))) {
Instruction tom_mul2=tom_get_element_InstL_InstL(tomMatch1_52,tomMatch1_end_59);
Instruction tom_inst=((Instruction)tom__arg);

INC(); rdebug("mul(*,mul(*),*)", 
tom_inst);
((GenericInstruction)
tom_inst).getChildren().remove(
tom_mul2);
boolean neg = InstructionComparator.isNeg(
tom_mul2);
for(Instruction arg : 
tom_get_slice_InstL(tomMatch1_62,0,tom_get_size_InstL_InstL(tomMatch1_62))) {
((GenericInstruction)
tom_inst).getChildren().add(arg);
}
if(neg) InstructionComparator.negate(
tom_inst);

}
}
}
}
tomMatch1_end_59=tomMatch1_end_59 + 1;
}
} while(!(tomMatch1_end_59 > tom_get_size_InstL_InstL(tomMatch1_52)));
}
}
}
}
}
}
return _visit_Inst(tom__arg,introspector);
}
}
private static  tom.library.sl.Strategy  tom_make_reduce() { 
return new reduce();
}
public static class stop extends tom.library.sl.AbstractStrategyBasic {
public stop() {
super(tom_make_Identity());
}
public tom.library.sl.Visitable[] getChildren() {
tom.library.sl.Visitable[] stratChildren = new tom.library.sl.Visitable[getChildCount()];
stratChildren[0] = super.getChildAt(0);
return stratChildren;}
public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
super.setChildAt(0, children[0]);
return this;
}
public int getChildCount() {
return 1;
}
public tom.library.sl.Visitable getChildAt(int index) {
switch (index) {
case 0: return super.getChildAt(0);
default: throw new IndexOutOfBoundsException();
}
}
public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
switch (index) {
case 0: return super.setChildAt(0, child);
default: throw new IndexOutOfBoundsException();
}
}
@SuppressWarnings("unchecked")
public <T> T visitLight(T v, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
if (tom_is_sort_Inst(v)) {
return ((T)visit_Inst(((Instruction)v),introspector));
}
if (!(( null  == environment))) {
return ((T)any.visit(environment,introspector));
} else {
return any.visitLight(v,introspector);
}
}
@SuppressWarnings("unchecked")
public Instruction _visit_Inst(Instruction arg, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
if (!(( null  == environment))) {
return ((Instruction)any.visit(environment,introspector));
} else {
return any.visitLight(arg,introspector);
}
}
@SuppressWarnings("unchecked")
public Instruction visit_Inst(Instruction tom__arg, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
{
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_neg(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch2_1=tom_get_slot_neg_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch2_1))) {
int tomMatch2_5=0;
if (!(tomMatch2_5 >= tom_get_size_InstL_InstL(tomMatch2_1))) {
if (tomMatch2_5 + 1 >= tom_get_size_InstL_InstL(tomMatch2_1)) {
//XXX
_space = 0; rdebug("continue neg", 
((Instruction)tom__arg));
throw new VisitFailure();

}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_add(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch2_8=tom_get_slot_add_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch2_8))) {
int tomMatch2_end_15=0;
do {
{
if (!(tomMatch2_end_15 >= tom_get_size_InstL_InstL(tomMatch2_8))) {
Instruction tomMatch2_19=tom_get_element_InstL_InstL(tomMatch2_8,tomMatch2_end_15);
if (tom_is_sort_Inst(tomMatch2_19)) {
if (tom_is_fun_sym_add(((Instruction)tomMatch2_19))) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tom_get_slot_add_children(tomMatch2_19)))) {

_space = 0; rdebug("continue addadd", 
((Instruction)tom__arg));
throw new VisitFailure();

}
}
}
}
tomMatch2_end_15=tomMatch2_end_15 + 1;
}
} while(!(tomMatch2_end_15 > tom_get_size_InstL_InstL(tomMatch2_8)));
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_add(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch2_25=tom_get_slot_add_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch2_25))) {
int tomMatch2_end_32=0;
do {
{
if (!(tomMatch2_end_32 >= tom_get_size_InstL_InstL(tomMatch2_25))) {
Instruction tomMatch2_36=tom_get_element_InstL_InstL(tomMatch2_25,tomMatch2_end_32);
if (tom_is_sort_Inst(tomMatch2_36)) {
if (tom_is_fun_sym_sub(((Instruction)tomMatch2_36))) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tom_get_slot_sub_children(tomMatch2_36)))) {

_space = 0; rdebug("continue addsub", 
((Instruction)tom__arg));
throw new VisitFailure();

}
}
}
}
tomMatch2_end_32=tomMatch2_end_32 + 1;
}
} while(!(tomMatch2_end_32 > tom_get_size_InstL_InstL(tomMatch2_25)));
}
}
}
}
}
}
return _visit_Inst(tom__arg,introspector);
}
}
private static  tom.library.sl.Strategy  tom_make_stop() { 
return new stop();
}
public static class sortAndSimplify extends tom.library.sl.AbstractStrategyBasic {
public sortAndSimplify() {
super(tom_make_Fail());
}
public tom.library.sl.Visitable[] getChildren() {
tom.library.sl.Visitable[] stratChildren = new tom.library.sl.Visitable[getChildCount()];
stratChildren[0] = super.getChildAt(0);
return stratChildren;}
public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
super.setChildAt(0, children[0]);
return this;
}
public int getChildCount() {
return 1;
}
public tom.library.sl.Visitable getChildAt(int index) {
switch (index) {
case 0: return super.getChildAt(0);
default: throw new IndexOutOfBoundsException();
}
}
public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
switch (index) {
case 0: return super.setChildAt(0, child);
default: throw new IndexOutOfBoundsException();
}
}
@SuppressWarnings("unchecked")
public <T> T visitLight(T v, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
if (tom_is_sort_Inst(v)) {
return ((T)visit_Inst(((Instruction)v),introspector));
}
if (!(( null  == environment))) {
return ((T)any.visit(environment,introspector));
} else {
return any.visitLight(v,introspector);
}
}
@SuppressWarnings("unchecked")
public Instruction _visit_Inst(Instruction arg, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
if (!(( null  == environment))) {
return ((Instruction)any.visit(environment,introspector));
} else {
return any.visitLight(arg,introspector);
}
}
@SuppressWarnings("unchecked")
public Instruction visit_Inst(Instruction tom__arg, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
{
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_generic(((Instruction)((Instruction)tom__arg)))) {
if (tom_is_fun_sym_InstL((( EList<Instruction> )tom_get_slot_generic_children(((Instruction)tom__arg))))) {
Instruction tom_inst=((Instruction)tom__arg);

rdebug("sort g(*)", 
tom_inst);
InstructionComparator.sort((GenericInstruction)
tom_inst);
int size = ((GenericInstruction)
tom_inst).getChildrenCount();
sdebug("|_", 
tom_inst);
if(size == 1)

tom_inst.substituteWith(((GenericInstruction)
tom_inst).getChild(0));
else if(size == 0)

tom_inst.remove();

}
}
}
}
}
}
return _visit_Inst(tom__arg,introspector);
}
}
private static  tom.library.sl.Strategy  tom_make_sortAndSimplify() { 
return new sortAndSimplify();
}


public static Double getValue(Instruction inst) {

{
{
if (tom_is_sort_Inst(inst)) {
if (tom_is_sort_Inst(((Instruction)inst))) {
if (tom_is_fun_sym_ival(((Instruction)((Instruction)inst)))) {
return (double)(
tom_get_slot_ival_value(((Instruction)inst))); 
}
}
}
}
{
if (tom_is_sort_Inst(inst)) {
if (tom_is_sort_Inst(((Instruction)inst))) {
if (tom_is_fun_sym_fval(((Instruction)((Instruction)inst)))) {
return 
tom_get_slot_fval_value(((Instruction)inst));	
}
}
}
}
}

return null;
}

}
