/**
 */
package profiling.frontend.directives.impl;

import fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport;

import fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport.NativeFunction;

import fr.irisa.cairn.gecos.core.profiling.frontend.InvalidDirective;

import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;

import gecos.core.ProcedureSet;

import gecos.instrs.Instruction;

import java.util.Objects;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import profiling.frontend.directives.DirectiveName;
import profiling.frontend.directives.DirectivesPackage;
import profiling.frontend.directives.RandomNormalDirective;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Random Normal Directive</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link profiling.frontend.directives.impl.RandomNormalDirectiveImpl#getNAME <em>NAME</em>}</li>
 *   <li>{@link profiling.frontend.directives.impl.RandomNormalDirectiveImpl#getMean <em>Mean</em>}</li>
 *   <li>{@link profiling.frontend.directives.impl.RandomNormalDirectiveImpl#getStddev <em>Stddev</em>}</li>
 *   <li>{@link profiling.frontend.directives.impl.RandomNormalDirectiveImpl#getSeed <em>Seed</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RandomNormalDirectiveImpl extends InjectorSourceDirectiveImpl implements RandomNormalDirective {
	/**
	 * The default value of the '{@link #getNAME() <em>NAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNAME()
	 * @generated
	 * @ordered
	 */
	protected static final DirectiveName NAME_EDEFAULT = DirectiveName.RANDOM_NORMAL;

	/**
	 * The cached value of the '{@link #getNAME() <em>NAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNAME()
	 * @generated
	 * @ordered
	 */
	protected DirectiveName name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMean() <em>Mean</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMean()
	 * @generated
	 * @ordered
	 */
	protected Instruction mean;

	/**
	 * The cached value of the '{@link #getStddev() <em>Stddev</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStddev()
	 * @generated
	 * @ordered
	 */
	protected Instruction stddev;

	/**
	 * The cached value of the '{@link #getSeed() <em>Seed</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSeed()
	 * @generated
	 * @ordered
	 */
	protected Instruction seed;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RandomNormalDirectiveImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DirectivesPackage.Literals.RANDOM_NORMAL_DIRECTIVE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DirectiveName getNAME() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNAME(DirectiveName newNAME) {
		DirectiveName oldNAME = name;
		name = newNAME == null ? NAME_EDEFAULT : newNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DirectivesPackage.RANDOM_NORMAL_DIRECTIVE__NAME, oldNAME, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getMean() {
		if (mean != null && mean.eIsProxy()) {
			InternalEObject oldMean = (InternalEObject)mean;
			mean = (Instruction)eResolveProxy(oldMean);
			if (mean != oldMean) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DirectivesPackage.RANDOM_NORMAL_DIRECTIVE__MEAN, oldMean, mean));
			}
		}
		return mean;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction basicGetMean() {
		return mean;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMean(Instruction newMean) {
		Instruction oldMean = mean;
		mean = newMean;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DirectivesPackage.RANDOM_NORMAL_DIRECTIVE__MEAN, oldMean, mean));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getStddev() {
		if (stddev != null && stddev.eIsProxy()) {
			InternalEObject oldStddev = (InternalEObject)stddev;
			stddev = (Instruction)eResolveProxy(oldStddev);
			if (stddev != oldStddev) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DirectivesPackage.RANDOM_NORMAL_DIRECTIVE__STDDEV, oldStddev, stddev));
			}
		}
		return stddev;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction basicGetStddev() {
		return stddev;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStddev(Instruction newStddev) {
		Instruction oldStddev = stddev;
		stddev = newStddev;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DirectivesPackage.RANDOM_NORMAL_DIRECTIVE__STDDEV, oldStddev, stddev));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getSeed() {
		if (seed != null && seed.eIsProxy()) {
			InternalEObject oldSeed = (InternalEObject)seed;
			seed = (Instruction)eResolveProxy(oldSeed);
			if (seed != oldSeed) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DirectivesPackage.RANDOM_NORMAL_DIRECTIVE__SEED, oldSeed, seed));
			}
		}
		return seed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction basicGetSeed() {
		return seed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSeed(Instruction newSeed) {
		Instruction oldSeed = seed;
		seed = newSeed;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DirectivesPackage.RANDOM_NORMAL_DIRECTIVE__SEED, oldSeed, seed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void parse() {
		final EList<Instruction> args = this.getCall().getArgs();
		int _size = args.size();
		boolean _lessThan = (_size < 2);
		if (_lessThan) {
			String _printLocation = this.printLocation();
			String _plus = (_printLocation + "\nMust have at least 2 arguments (mean, stddev)!");
			throw new InvalidDirective(_plus);
		}
		this.setMean(Objects.<Instruction>requireNonNull(args.get(0)));
		this.setStddev(Objects.<Instruction>requireNonNull(args.get(1)));
		int _size_1 = args.size();
		boolean _greaterThan = (_size_1 > 2);
		if (_greaterThan) {
			this.setSeed(Objects.<Instruction>requireNonNull(args.get(2)));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block createCodeBlock() {
		final ProcedureSet ps = this.getCall().getContainingProcedureSet();
		final BasicBlock bb = GecosUserBlockFactory.BBlock(
			GecosUserInstructionFactory.call(
				NativeProfilingEngineSupport.getNativeProc(ps, NativeFunction.INJECTOR_RANDOM_UNIFORN_DOUBLE), this.getMean(), this.getStddev()));
		Instruction _seed = this.getSeed();
		boolean _tripleNotEquals = (_seed != null);
		if (_tripleNotEquals) {
			bb.getInstructions().add(0, GecosUserInstructionFactory.call(
				NativeProfilingEngineSupport.getNativeProc(ps, NativeFunction.INJECTOR_RANDOM_SET_SEED), this.getSeed()));
		}
		return bb;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DirectivesPackage.RANDOM_NORMAL_DIRECTIVE__NAME:
				return getNAME();
			case DirectivesPackage.RANDOM_NORMAL_DIRECTIVE__MEAN:
				if (resolve) return getMean();
				return basicGetMean();
			case DirectivesPackage.RANDOM_NORMAL_DIRECTIVE__STDDEV:
				if (resolve) return getStddev();
				return basicGetStddev();
			case DirectivesPackage.RANDOM_NORMAL_DIRECTIVE__SEED:
				if (resolve) return getSeed();
				return basicGetSeed();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DirectivesPackage.RANDOM_NORMAL_DIRECTIVE__NAME:
				setNAME((DirectiveName)newValue);
				return;
			case DirectivesPackage.RANDOM_NORMAL_DIRECTIVE__MEAN:
				setMean((Instruction)newValue);
				return;
			case DirectivesPackage.RANDOM_NORMAL_DIRECTIVE__STDDEV:
				setStddev((Instruction)newValue);
				return;
			case DirectivesPackage.RANDOM_NORMAL_DIRECTIVE__SEED:
				setSeed((Instruction)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DirectivesPackage.RANDOM_NORMAL_DIRECTIVE__NAME:
				setNAME(NAME_EDEFAULT);
				return;
			case DirectivesPackage.RANDOM_NORMAL_DIRECTIVE__MEAN:
				setMean((Instruction)null);
				return;
			case DirectivesPackage.RANDOM_NORMAL_DIRECTIVE__STDDEV:
				setStddev((Instruction)null);
				return;
			case DirectivesPackage.RANDOM_NORMAL_DIRECTIVE__SEED:
				setSeed((Instruction)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DirectivesPackage.RANDOM_NORMAL_DIRECTIVE__NAME:
				return name != NAME_EDEFAULT;
			case DirectivesPackage.RANDOM_NORMAL_DIRECTIVE__MEAN:
				return mean != null;
			case DirectivesPackage.RANDOM_NORMAL_DIRECTIVE__STDDEV:
				return stddev != null;
			case DirectivesPackage.RANDOM_NORMAL_DIRECTIVE__SEED:
				return seed != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (NAME: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //RandomNormalDirectiveImpl
