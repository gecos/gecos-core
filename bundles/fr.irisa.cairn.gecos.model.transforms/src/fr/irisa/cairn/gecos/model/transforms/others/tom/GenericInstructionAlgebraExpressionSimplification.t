package fr.irisa.cairn.gecos.model.transforms.others;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.transforms.tools.AbstractInstructionTransformation;
import fr.irisa.cairn.gecos.model.transforms.tools.GenericInstructionTransformation;
import gecos.blocks.Block;
import gecos.core.Procedure;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;


@SuppressWarnings({"all"})
public class GenericInstructionAlgebraExpressionSimplification extends GenericInstructionTransformation {

	%include { sl.tom }
	%include { long.tom }
	%include { double.tom }
	%include { gecos_common.tom }
	%include { gecos_terminals.tom } 
	%include { gecos_basic.tom }
	%include { gecos_arithmetic.tom }  
	%include { gecos_compare.tom } 
	%include { gecos_logical.tom } 
    %include { gecos_operators.tom } 
    %include { gecos_misc.tom } 
   
  
	protected Instruction apply(Instruction instruction) throws VisitFailure{
		Instruction adapter = `BottomUp(Simplify()).visitLight(instruction, tom.mapping.GenericIntrospector.INSTANCE);

		return adapter; 
	}
   
	%strategy Simplify() extends Identity() {
		visit Inst {
 
			add(InstL(fval(a),fval(b))) -> { return `fval(a+(b)); }
			sub(InstL(fval(a),fval(b))) -> { return `fval(a-(b)); }
			mul(InstL(fval(a),fval(b))) -> { return `fval((a)*(b)); }
			div(InstL(fval(a),fval(b))) -> { return `fval(a/(b)); }

			add(InstL(ival(a),ival(b))) -> { return `ival(a+(b)); }
			sub(InstL(ival(a),ival(b))) -> { return `ival(a-(b)); }
			mul(InstL(ival(a),ival(b))) -> { return `ival((a)*(b)); }
			div(InstL(ival(a),ival(b))) -> { return `ival(a/(b)); }
			shr(InstL(ival(a),ival(b))) -> { return `ival((a)>>(b)); }
			shl(InstL(ival(a),ival(b))) -> { return `ival(a<<(b)); }
			
			add(InstL(ival(0L),a)) -> { return `a; }
			sub(InstL(a,ival(0L))) -> { return `a; }
			mul(InstL(ival(1L),a)) -> { return `a; }
			div(InstL(a,ival(1L))) -> { return `a; }
			shr(InstL(a,ival(0L))) -> { return `a; }
			shl(InstL(a,ival(0L))) -> { return `a; }

			add(InstL(fval(0.0),a)) -> { return `a; }
			sub(InstL(a,fval(0.0))) -> { return `a; }
			mul(InstL(fval(1.0),a)) -> { return `a; }
			div(InstL(a,fval(1.0))) -> { return `a; }

			sum(InstL(x*,ival(a),y*,ival(b),z*)) -> { return `sum(InstL(ival(a+b),x*,y*,z*)); } 

			indir(address(a)) -> { return `a; }
			

			le(InstL(ival(a),ival(b))) -> { return `ival(((a)<=(b))?0:1); }
			lt(InstL(ival(a),ival(b))) -> { return `ival(((a)<(b))?0:1); }
			gt(InstL(ival(a),ival(b))) -> { return `ival(((a)>(b))?0:1); }
			ge(InstL(ival(a),ival(b))) -> { return `ival(((a)>=(b))?0:1); }
			eq(InstL(ival(a),ival(b))) -> { return `ival(((a)==(b))?0:1); }			
			land(InstL(ival(a),ival(b))) -> { return `ival(((a!=0)&&(b!=0))?0:1); }
			lor(InstL(ival(a),ival(b))) -> { return `ival(((a!=0)||(b!=0))?0:1); }
			and(InstL(ival(a),ival(b))) -> { return `ival(((a)&(b))); }
			or(InstL(ival(a),ival(b))) -> { return `ival(((a)|(b))); }
			xor(InstL(ival(a),ival(b))) -> { return `ival(((a)^(b))); }

			mux(InstL(ival(c),b,a)) -> { 
				if((`c)!=0) {
					return `b;
				} else {
					return `a;
				} 
			}

		} 
	}
  
	public static class GecosAlgebraSymplifyPass extends AbstractTomTransformationPass {

		public GecosAlgebraSymplifyPass(Block block) {
			super(block);
		}
		public GecosAlgebraSymplifyPass(Procedure p) {
			super(p);
		}

		@Override
		public void compute() {
			super.compute();
		}

		@Override
		protected AbstractInstructionTransformation getTransformation() {
			return new GenericInstructionAlgebraExpressionSimplification();
		}
	}
}