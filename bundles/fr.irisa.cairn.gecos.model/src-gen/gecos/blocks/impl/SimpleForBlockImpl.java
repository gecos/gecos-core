/**
 */
package gecos.blocks.impl;

import fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;

import gecos.blocks.Block;
import gecos.blocks.BlocksPackage;
import gecos.blocks.BlocksVisitor;
import gecos.blocks.SimpleForBlock;

import gecos.core.Symbol;

import gecos.instrs.Instruction;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simple For Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.blocks.impl.SimpleForBlockImpl#getIterator <em>Iterator</em>}</li>
 *   <li>{@link gecos.blocks.impl.SimpleForBlockImpl#getLb <em>Lb</em>}</li>
 *   <li>{@link gecos.blocks.impl.SimpleForBlockImpl#getUb <em>Ub</em>}</li>
 *   <li>{@link gecos.blocks.impl.SimpleForBlockImpl#getStride <em>Stride</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SimpleForBlockImpl extends ForC99BlockImpl implements SimpleForBlock {
	/**
	 * The cached value of the '{@link #getIterator() <em>Iterator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIterator()
	 * @generated
	 * @ordered
	 */
	protected Symbol iterator;

	/**
	 * This is true if the Iterator reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iteratorESet;

	/**
	 * The cached value of the '{@link #getLb() <em>Lb</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLb()
	 * @generated
	 * @ordered
	 */
	protected Instruction lb;

	/**
	 * This is true if the Lb reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean lbESet;

	/**
	 * The cached value of the '{@link #getUb() <em>Ub</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUb()
	 * @generated
	 * @ordered
	 */
	protected Instruction ub;

	/**
	 * This is true if the Ub reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean ubESet;

	/**
	 * The cached value of the '{@link #getStride() <em>Stride</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStride()
	 * @generated
	 * @ordered
	 */
	protected Instruction stride;

	/**
	 * This is true if the Stride reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean strideESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimpleForBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BlocksPackage.Literals.SIMPLE_FOR_BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol getIterator() {
		if (iterator != null && iterator.eIsProxy()) {
			InternalEObject oldIterator = (InternalEObject)iterator;
			iterator = (Symbol)eResolveProxy(oldIterator);
			if (iterator != oldIterator) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BlocksPackage.SIMPLE_FOR_BLOCK__ITERATOR, oldIterator, iterator));
			}
		}
		return iterator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol basicGetIterator() {
		return iterator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIterator(Symbol newIterator) {
		Symbol oldIterator = iterator;
		iterator = newIterator;
		boolean oldIteratorESet = iteratorESet;
		iteratorESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.SIMPLE_FOR_BLOCK__ITERATOR, oldIterator, iterator, !oldIteratorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIterator() {
		Symbol oldIterator = iterator;
		boolean oldIteratorESet = iteratorESet;
		iterator = null;
		iteratorESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.SIMPLE_FOR_BLOCK__ITERATOR, oldIterator, null, oldIteratorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIterator() {
		return iteratorESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getLb() {
		if (lb != null && lb.eIsProxy()) {
			InternalEObject oldLb = (InternalEObject)lb;
			lb = (Instruction)eResolveProxy(oldLb);
			if (lb != oldLb) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BlocksPackage.SIMPLE_FOR_BLOCK__LB, oldLb, lb));
			}
		}
		return lb;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction basicGetLb() {
		return lb;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLb(Instruction newLb) {
		Instruction oldLb = lb;
		lb = newLb;
		boolean oldLbESet = lbESet;
		lbESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.SIMPLE_FOR_BLOCK__LB, oldLb, lb, !oldLbESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetLb() {
		Instruction oldLb = lb;
		boolean oldLbESet = lbESet;
		lb = null;
		lbESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.SIMPLE_FOR_BLOCK__LB, oldLb, null, oldLbESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetLb() {
		return lbESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getUb() {
		if (ub != null && ub.eIsProxy()) {
			InternalEObject oldUb = (InternalEObject)ub;
			ub = (Instruction)eResolveProxy(oldUb);
			if (ub != oldUb) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BlocksPackage.SIMPLE_FOR_BLOCK__UB, oldUb, ub));
			}
		}
		return ub;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction basicGetUb() {
		return ub;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUb(Instruction newUb) {
		Instruction oldUb = ub;
		ub = newUb;
		boolean oldUbESet = ubESet;
		ubESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.SIMPLE_FOR_BLOCK__UB, oldUb, ub, !oldUbESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetUb() {
		Instruction oldUb = ub;
		boolean oldUbESet = ubESet;
		ub = null;
		ubESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.SIMPLE_FOR_BLOCK__UB, oldUb, null, oldUbESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetUb() {
		return ubESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getStride() {
		if (stride != null && stride.eIsProxy()) {
			InternalEObject oldStride = (InternalEObject)stride;
			stride = (Instruction)eResolveProxy(oldStride);
			if (stride != oldStride) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BlocksPackage.SIMPLE_FOR_BLOCK__STRIDE, oldStride, stride));
			}
		}
		return stride;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction basicGetStride() {
		return stride;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStride(Instruction newStride) {
		Instruction oldStride = stride;
		stride = newStride;
		boolean oldStrideESet = strideESet;
		strideESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.SIMPLE_FOR_BLOCK__STRIDE, oldStride, stride, !oldStrideESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetStride() {
		Instruction oldStride = stride;
		boolean oldStrideESet = strideESet;
		stride = null;
		strideESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, BlocksPackage.SIMPLE_FOR_BLOCK__STRIDE, oldStride, null, oldStrideESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetStride() {
		return strideESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block managedCopy(final BlockCopyManager mng) {
		return super.managedCopy(mng);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final BlocksVisitor visitor) {
		visitor.visitSimpleForBlock(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BlocksPackage.SIMPLE_FOR_BLOCK__ITERATOR:
				if (resolve) return getIterator();
				return basicGetIterator();
			case BlocksPackage.SIMPLE_FOR_BLOCK__LB:
				if (resolve) return getLb();
				return basicGetLb();
			case BlocksPackage.SIMPLE_FOR_BLOCK__UB:
				if (resolve) return getUb();
				return basicGetUb();
			case BlocksPackage.SIMPLE_FOR_BLOCK__STRIDE:
				if (resolve) return getStride();
				return basicGetStride();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BlocksPackage.SIMPLE_FOR_BLOCK__ITERATOR:
				setIterator((Symbol)newValue);
				return;
			case BlocksPackage.SIMPLE_FOR_BLOCK__LB:
				setLb((Instruction)newValue);
				return;
			case BlocksPackage.SIMPLE_FOR_BLOCK__UB:
				setUb((Instruction)newValue);
				return;
			case BlocksPackage.SIMPLE_FOR_BLOCK__STRIDE:
				setStride((Instruction)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BlocksPackage.SIMPLE_FOR_BLOCK__ITERATOR:
				unsetIterator();
				return;
			case BlocksPackage.SIMPLE_FOR_BLOCK__LB:
				unsetLb();
				return;
			case BlocksPackage.SIMPLE_FOR_BLOCK__UB:
				unsetUb();
				return;
			case BlocksPackage.SIMPLE_FOR_BLOCK__STRIDE:
				unsetStride();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BlocksPackage.SIMPLE_FOR_BLOCK__ITERATOR:
				return isSetIterator();
			case BlocksPackage.SIMPLE_FOR_BLOCK__LB:
				return isSetLb();
			case BlocksPackage.SIMPLE_FOR_BLOCK__UB:
				return isSetUb();
			case BlocksPackage.SIMPLE_FOR_BLOCK__STRIDE:
				return isSetStride();
		}
		return super.eIsSet(featureID);
	}

} //SimpleForBlockImpl
