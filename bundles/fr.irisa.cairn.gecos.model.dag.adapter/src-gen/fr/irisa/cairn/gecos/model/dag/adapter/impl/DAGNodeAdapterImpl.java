package fr.irisa.cairn.gecos.model.dag.adapter.impl;
import java.util.List;

import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGEdgeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGInstructionAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGNodeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGPortAdapter;
import fr.irisa.cairn.graph.IPort;
import gecos.dag.DAGNode;

/**
 * implementation of a node adapter.
 * This graph adapt a DAGNode
 * @author <your name>
 *
 */
public class DAGNodeAdapterImpl extends fr.irisa.cairn.graph.implement.Node
		implements
			IDAGNodeAdapter {

	private final DAGNode adapted;

	public DAGNodeAdapterImpl(DAGNode adapted) {
		super();
		this.adapted = adapted;
	}

	public DAGNode getAdaptedNode() {
		return this.adapted;
	}

	/**
	 * Add an input port to the node.
	 * 
	 * @model default=""
	 * 
	 * @return created port
	 */
	@Override
	public void addInputPort(IPort p) {
		super.addInputPort(p);

		if (p instanceof IDAGPortAdapter && this.getGraph()!=null) {
			this.getGraph().getContainer().addPort((IDAGPortAdapter) p);
		}

	}

	@Override
	public void removeInputPort(IPort p) {
		super.removeInputPort(p);

		if (p instanceof IDAGPortAdapter && this.getGraph()!=null) {
			this.getGraph().getContainer().removePort((IDAGPortAdapter) p);
		}

	}

	/**
	 * Add an output port to the node.
	 * 
	 * @model default=""
	 * 
	 * @return created port
	 */
	@Override
	public void addOutputPort(IPort p) {
		super.addOutputPort(p);

		if (p instanceof IDAGPortAdapter && this.getGraph()!=null) {
			this.getGraph().getContainer().addPort((IDAGPortAdapter) p);
		}

	}

	@Override
	public void removeOutputPort(IPort p) {
		super.removeOutputPort(p);

		if (p instanceof IDAGPortAdapter && this.getGraph()!=null) {
			this.getGraph().getContainer().removePort((IDAGPortAdapter) p);
		}

	}

	/**
	 * Get the graph containing the node.
	 * 
	 * @model default=""
	 * 
	 * @return the node graph or <code>null</code> if graph hasn't been set
	 */
	@Override
	public IDAGInstructionAdapter getGraph() {
		return (IDAGInstructionAdapter) super.getGraph();
	}

	/**
	 * Get a list of all incoming edges to the node.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<IDAGEdgeAdapter> getIncomingEdges() {
		return (List<IDAGEdgeAdapter>) super.getIncomingEdges();
	}

	/**
	 * Get a list of all outgoing edges from the node.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<IDAGEdgeAdapter> getOutgoingEdges() {
		return (List<IDAGEdgeAdapter>) super.getOutgoingEdges();
	}

	/**
	 * Get the list of all input ports.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<IDAGPortAdapter> getInputPorts() {
		return (List<IDAGPortAdapter>) super.getInputPorts();
	}

	/**
	 * Get the list of all output ports.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<IDAGPortAdapter> getOutputPorts() {
		return (List<IDAGPortAdapter>) super.getOutputPorts();
	}

	/**
	 * Get predecessors of this node in its graph.
	 * 
	 * @model default=""
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<IDAGNodeAdapter> getPredecessors() {
		return (List<IDAGNodeAdapter>) super.getPredecessors();
	}

	/**
	 * Get successors of this node in its graph.
	 * 
	 * @model default=""
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<IDAGNodeAdapter> getSuccessors() {
		return (List<IDAGNodeAdapter>) super.getSuccessors();
	}
	
	@Override
	public String toString() {
		return getAdaptedNode().toString();
	}

}
