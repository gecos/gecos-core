package fr.irisa.cairn.gecos.model.c.generator;

import gecos.instrs.AddressInstruction;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.CallInstruction;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.SelectOperator;
import gecos.instrs.SetInstruction;
import gecos.instrs.SizeofInstruction;

import java.util.HashMap;
import java.util.Map;

/**
 * @author mnaullet : original implementation
 * @author amorvan : fixes + extensions
 */
public class OperatorPrecedenceTools {

	private static final Map<String, Integer> priorityOperator = new HashMap<String, Integer>(0);
	static {
		priorityOperator.put(ArithmeticOperator.NEG.getLiteral(), 2);
		priorityOperator.put(LogicalOperator.NOT.getLiteral(), 2);
		priorityOperator.put(BitwiseOperator.NOT.getLiteral(), 2);
		
		priorityOperator.put(ArithmeticOperator.MUL.getLiteral(), 3);
		priorityOperator.put(ArithmeticOperator.DIV.getLiteral(), 3);
		priorityOperator.put(ArithmeticOperator.MOD.getLiteral(), 3);
		
		priorityOperator.put(ArithmeticOperator.ADD.getLiteral(), 4);
		priorityOperator.put(ArithmeticOperator.SUB.getLiteral(), 4);
		
		priorityOperator.put(BitwiseOperator.SHL.getLiteral(), 5);
		priorityOperator.put(BitwiseOperator.SHR.getLiteral(), 5);

		priorityOperator.put(ComparisonOperator.LE.getLiteral(), 6);
		priorityOperator.put(ComparisonOperator.LT.getLiteral(), 6);
		priorityOperator.put(ComparisonOperator.GE.getLiteral(), 6);
		priorityOperator.put(ComparisonOperator.GT.getLiteral(), 6);
		
		priorityOperator.put(ComparisonOperator.NE.getLiteral(), 7);
		priorityOperator.put(ComparisonOperator.EQ.getLiteral(), 7);
		
		priorityOperator.put(BitwiseOperator.AND.getLiteral(), 8);
		
		priorityOperator.put(BitwiseOperator.XOR.getLiteral(), 9);
		
		priorityOperator.put(BitwiseOperator.OR.getLiteral(), 10);
		
		priorityOperator.put(LogicalOperator.AND.getLiteral(), 11);
		
		priorityOperator.put(LogicalOperator.OR.getLiteral(), 12);
		
		priorityOperator.put(SelectOperator.MUX.getLiteral(), 13);
		
		priorityOperator.put("sequence", 15);
	};
	
	public static boolean needParenthesis(Instruction instruction) {
		int instructionPriority = getPriority(instruction);
		Instruction parentInstruction = instruction.getParent();
		int parentInstructionPriority = getPriority(parentInstruction);
		boolean muxInAMux = instructionPriority == parentInstructionPriority && instructionPriority ==priorityOperator.get(SelectOperator.MUX.getLiteral());
		boolean rightHandSideOfSub = false;
		boolean shiftOperator = false;
		boolean rhsOfSamePriority = false;
		boolean logicalEvaluation = false;
		
		if (parentInstruction instanceof GenericInstruction) {
			String parentName = ((GenericInstruction)parentInstruction).getName();
			
			rightHandSideOfSub = ArithmeticOperator.SUB.getLiteral().equals(parentName)
				&& ((GenericInstruction)parentInstruction).getChildren().indexOf(instruction) != 0
				&& instructionPriority >= parentInstructionPriority;

			shiftOperator = (BitwiseOperator.SHL.getLiteral().equals(parentName) || BitwiseOperator.SHR.getLiteral().equals(parentName));
				
			rhsOfSamePriority =
					parentInstructionPriority == instructionPriority
				&& ((GenericInstruction)parentInstruction).getOperands().size() == 2
				&& ((GenericInstruction)parentInstruction).getOperand(1) == instruction;
			
			logicalEvaluation = parentInstructionPriority != instructionPriority && 
					(parentName.equals(LogicalOperator.AND.getLiteral()) 
							|| parentName.equals(LogicalOperator.NOT.getLiteral()) 
							|| parentName.equals(LogicalOperator.OR.getLiteral()));
		}
		if (parentInstruction instanceof ArrayInstruction && ((ArrayInstruction) parentInstruction).getDest() != instruction) {
			parentInstructionPriority = Integer.MAX_VALUE;
		}
		if (parentInstruction instanceof CallInstruction && ((CallInstruction) parentInstruction).getAddress() != instruction) {
			parentInstructionPriority = Integer.MAX_VALUE;
		}
		return instructionPriority > parentInstructionPriority || muxInAMux || rightHandSideOfSub || shiftOperator || rhsOfSamePriority || logicalEvaluation;
	}
	
	private static int getPriority(Instruction instruction) {
		if (	instruction instanceof ArrayInstruction
			 || instruction instanceof CallInstruction) {
			return 1;
		} else if (instruction instanceof GenericInstruction) {
			String name = ((GenericInstruction) instruction).getName();
			if(priorityOperator.containsKey(name))
				return priorityOperator.get(name);
		} else if (instruction instanceof ConvertInstruction 
				|| instruction instanceof AddressInstruction
				|| instruction instanceof IndirInstruction
				|| instruction instanceof SizeofInstruction)
			return 2;
		else if (instruction instanceof SetInstruction)
			return 14;
		return Integer.MAX_VALUE;
	}
}