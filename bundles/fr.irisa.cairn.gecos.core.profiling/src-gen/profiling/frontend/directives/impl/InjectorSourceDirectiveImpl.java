/**
 */
package profiling.frontend.directives.impl;

import org.eclipse.emf.ecore.EClass;

import profiling.frontend.directives.DirectivesPackage;
import profiling.frontend.directives.InjectorSourceDirective;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Injector Source Directive</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class InjectorSourceDirectiveImpl extends DirectiveImpl implements InjectorSourceDirective {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InjectorSourceDirectiveImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DirectivesPackage.Literals.INJECTOR_SOURCE_DIRECTIVE;
	}

} //InjectorSourceDirectiveImpl
