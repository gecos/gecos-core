package fr.irisa.cairn.gecos.model.transforms.bringOutCond;

import fr.irisa.cairn.gecos.model.tools.controlflow.BuildControlFlow;
import fr.irisa.cairn.gecos.model.tools.controlflow.ClearControlFlow;
import fr.irisa.cairn.gecos.model.transforms.bringOutCond.internal.BringOutConditionals;
import fr.irisa.r2d2.gecos.framework.GSModule;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;

@GSModule(" This pass moves the test out from if constructs.")

public class BringOutConditionalsModule {
	private GecosProject _project;
	
	public BringOutConditionalsModule(GecosProject project) {
		this._project = project;
	}

	public void compute() {
		for (ProcedureSet ps : _project.listProcedureSets()) { // Iterate on
																// each
																// procedure set
																// contained in
																// a GeCoS
																// project
			for (Procedure pr : ps.listProcedures()) { // Iterate on each
														// procedure contained
														// in a procedure set
				
				BringOutConditionals visitor = new BringOutConditionals();
				pr.getBody().accept(visitor); // apply the visitor on the body
											// block of a procedure
			}
		}

		ClearControlFlow ccf = new ClearControlFlow(_project);
		ccf.compute();
		BuildControlFlow bcf = new BuildControlFlow(_project);
		bcf.compute();

		//System.out.println("[info] BringOutConditionals: DONE");
	}
}
