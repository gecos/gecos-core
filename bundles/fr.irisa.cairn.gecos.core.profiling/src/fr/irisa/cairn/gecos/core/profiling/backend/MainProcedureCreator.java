package fr.irisa.cairn.gecos.core.profiling.backend;

import static fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport.BACKEND_PROF_FILES_OUTPUT_DIRNAME;
import static fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport.getNativeProc;
import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.BBlock;
import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.CompositeBlock;
import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.For;
import static fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory.paramSymbol;
import static fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory.proc;
import static fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory.procSymbol;
import static fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory.symbol;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.Int;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.call;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.cast;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.ret;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.sizeOf;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.string;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.CHAR;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.INT;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.PTR;
import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.List;

import com.google.common.collect.Iterables;

import fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport.NativeFunction;
import fr.irisa.cairn.gecos.core.profiling.frontend.DirectivesProcessor;
import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ForBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.instrs.CallInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.RetInstruction;
import gecos.types.ArrayType;
import gecos.types.Type;

/**
 * @author aelmouss
 */
public class MainProcedureCreator {	

	private static final int NB_SIMU_DEFAULT_VALUE = 1;
	private static final int SEED_INIT_DEFAULT_VALUE = -1;
	public static String SIM_COUNT_INDEX_NAME = "simCountIndex";
	public static String SIM_COUNT_MAX_NAME = "simCountMax";
	public static String SIM_INIT_SEED_NAME = "simInitSeed";
	
	private List<Procedure> listProcedures;
	
	
	public MainProcedureCreator(GecosProject project) {
		this.listProcedures = project.listProcedures();
	}
	
	public Procedure generate() {
		Procedure mainProc = findMainProcedure(listProcedures);
		if(mainProc == null)
			mainProc = generateMain();
		
		insertStartAndFinalizeProfilingCore(mainProc);
		return mainProc;
	}
	
	public static Procedure findMainProcedure(List<Procedure> listProcedures) {
		return Iterables.find(listProcedures, p -> p.getSymbolName().equals("main"), null);
	}
	
	private Procedure generateMain() {
		Procedure target = DirectivesProcessor.getTargetProcedure(listProcedures);
		ProcedureSet procedureSet = target.getContainingProcedureSet();

		GecosUserTypeFactory.setScope(procedureSet.getScope());
		// create main procedure
		CompositeBlock mainBody = CompositeBlock();
		Procedure mainProc = proc(procedureSet, createMainSymbol(), mainBody);
		Scope scope = mainBody.getScope();
		GecosUserTypeFactory.setScope(scope);

		// add target arguments declaration corresponding to target parameters
		List<Symbol> targetCallArguments = declareAndAllocateArguments(target, mainBody);

		Symbol atoiFunctionSymbol = symbol("atoi", GecosUserTypeFactory.FUNCTION(GecosUserTypeFactory.INT(), GecosUserTypeFactory.PTR(GecosUserTypeFactory.CHAR())));
		Symbol argvSymbol = scope.lookup("argv");
		Symbol argcSymbol = scope.lookup("argc");
		CallInstruction nbSimuAtoiCall = GecosUserInstructionFactory.call(GecosUserInstructionFactory.symbref(atoiFunctionSymbol),
				GecosUserInstructionFactory.array(argvSymbol, GecosUserInstructionFactory.Int(1)));
		CallInstruction initSeedAtoiCall = GecosUserInstructionFactory.call(GecosUserInstructionFactory.symbref(atoiFunctionSymbol),
				GecosUserInstructionFactory.array(argvSymbol, GecosUserInstructionFactory.Int(2)));

		GenericInstruction nbSimuValue = GecosUserInstructionFactory.mux(GecosUserInstructionFactory.gt(GecosUserInstructionFactory.symbref(argcSymbol),
				GecosUserInstructionFactory.Int(1)), nbSimuAtoiCall, GecosUserInstructionFactory.Int(NB_SIMU_DEFAULT_VALUE));
		GenericInstruction seedInitValue = GecosUserInstructionFactory.mux(GecosUserInstructionFactory.gt(GecosUserInstructionFactory.symbref(argcSymbol),
				GecosUserInstructionFactory.Int(2)), initSeedAtoiCall, GecosUserInstructionFactory.Int(SEED_INIT_DEFAULT_VALUE));

		symbol(SIM_COUNT_MAX_NAME, INT(), scope, nbSimuValue);
		symbol(SIM_INIT_SEED_NAME, INT(), scope, seedInitValue);

		Block callBlk = createTargetCalls(target, mainBody, targetCallArguments);
		mainBody.addBlock(callBlk);
		
		// add return 0 at the end of main
		mainBody.addChildren(BBlock(ret(Int(0))));
		
		return mainProc;
	}
	
	private static void insertStartAndFinalizeProfilingCore(Procedure mainProc) {
		ProcedureSet ps = mainProc.getContainingProcedureSet();
		CompositeBlock body = mainProc.getBody();
		if(body.getChildren().get(0) == mainProc.getStart())
			body = (CompositeBlock) body.getChildren().get(1);

		/* insert start() call at the begining of main */
		CallInstruction call = call(getNativeProc(ps, NativeFunction.START_SEEDED));
		call.getArgs().add(GecosUserInstructionFactory.symbref(body.getScope().lookup(SIM_INIT_SEED_NAME)));
		body.getChildren().add(0, BBlock(call));

		/* insert finalize() call before any return or at the end of main */
		CallInstruction finalizeCall = call(getNativeProc(ps, NativeFunction.FINALIZE), 
				string(BACKEND_PROF_FILES_OUTPUT_DIRNAME));
		long nbRet = EMFUtils.eAllContentsFirstInstancesOf(mainProc, RetInstruction.class).stream()
			.peek(r -> r.getBasicBlock().insertInstructionBefore(finalizeCall, r))
			.count();
		if(nbRet == 0) {
			body.addBlock(BBlock(finalizeCall));
		}
	}
	
	/**
	 * Create the main simulation procedure.
	 * @return Body composite block of the main procedure
	 */
	private static ProcedureSymbol createMainSymbol() {
		return procSymbol("main", INT(), Arrays.asList(
				paramSymbol("argc", INT()), paramSymbol("argv", PTR(PTR(CHAR())))));
	}
	
	/**
	 * @param pWithMainPragma
	 * @param bodyMain
	 * @return 
	 * @throws SimulationException
	 */
	private static List<Symbol> declareAndAllocateArguments(Procedure pWithMainPragma, CompositeBlock bodyMain) {
		Scope scope = bodyMain.getScope();
		return pWithMainPragma.listParameters().stream()
			.map(MainProcedureCreator::createSymbolDeclaration)
			.peek(scope::addSymbol)
			.collect(toList());
	}

	private static Symbol createSymbolDeclaration(Symbol param) {
		Type type = param.getType();
		TypeAnalyzer ta = new TypeAnalyzer(type, false);
		
		if(ta.isBase())
			return symbol(param.getName(), type);

		Type baseType = ta.getBaseLevel().getAlias();
		if(baseType == null) 
			baseType = ta.getBaseLevel().getBase();
		
//		int totalDims = ta.getTotalNbDims();
//		List<Instruction> dimSizesOuterFirst = IntStream.range(1, totalDims) //skip outermost dim (i.e. 0)
//			.mapToObj(dim -> ta.tryGetDimSize(dim))
//			.filter(Objects::nonNull)
//			.collect(toList());
//		
//		if(dimSizesOuterFirst.size() == totalDims-1) { //if all dimensions, except outermost, have a known size
//			
//		} else {
//			type = baseType;
//			for(int dim = 0; dim < totalDims; dim++)
//				type = PTR(type);
//		}
		
		/* replace outer type with a pointer */
		if(type instanceof ArrayType) {
			type = PTR(((ArrayType) type).getBase());
		}
				
		Instruction alloc = cast(type, call(procSymbol("malloc", INT(), null), 
				getDimensions(param).stream().reduce(sizeOf(baseType), GecosUserInstructionFactory::mul)));
		
		return symbol(param.getName(), type, alloc);
	}

	private static List<Instruction> getDimensions(Symbol s) {
		try {
			return DirectivesProcessor.getDimensionSizes(s);
		} catch (Exception e) {
				throw new RuntimeException("Could not determine size of Paramater; needed to allocate data: " + s, e);
		}
		
	}

	private Block createTargetCalls(Procedure target, CompositeBlock mainBody, List<Symbol> targetCallArguments) {
		Instruction[] args = targetCallArguments.stream()
				.map(GecosUserInstructionFactory::symbref)
				.toArray(Instruction[]::new);
		CallInstruction callTarget = call(target.getSymbol(), args);
		
		//TODO add Arguments vales initializations !!
		BasicBlock callBlock = BBlock(callTarget);
		//scope.makeUnique(index);
		Scope scopePS = target.getContainingProcedureSet().getScope();
		Scope scope = mainBody.getScope();
		Symbol index = symbol(SIM_COUNT_INDEX_NAME, INT(), scopePS, GecosUserInstructionFactory.Int(0));
		ForBlock forBlock = For(index ,0, GecosUserInstructionFactory.symbref(scope.lookup(SIM_COUNT_MAX_NAME)), 1, callBlock);
		return forBlock;
	}
	
}
