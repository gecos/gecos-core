package fr.irisa.cairn.gecos.model.cdtfrontend.builder;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CaseBlock;
import gecos.blocks.CompositeBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.ForC99Block;
import gecos.blocks.IfBlock;
import gecos.blocks.SwitchBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Scope;
import gecos.instrs.Instruction;

public interface ControlStructureBuilder {

	public BasicBlock createBasicBlock(Instruction... instructions);
	public CaseBlock createCaseBlock(long i);
	public boolean isCasedBlock(Block block);
	public CompositeBlock createCompositeBlock(Scope scope);
	public ForBlock createForBlock();
	public ForC99Block createForC99Block(Scope scope);
	public IfBlock createIfBlock();
	public DoWhileBlock createLoopBlock();
	public SwitchBlock createSwitchBlock();
	public void switchBlockCreated();
	public WhileBlock createWhileBlock();
	
}