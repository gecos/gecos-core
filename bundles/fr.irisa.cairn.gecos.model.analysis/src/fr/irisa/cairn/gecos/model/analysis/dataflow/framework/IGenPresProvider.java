package fr.irisa.cairn.gecos.model.analysis.dataflow.framework;

import gecos.instrs.Instruction;


public interface IGenPresProvider {

	void compute(Instruction i, BooleanLattice pres, BooleanLattice gen);
		
}
