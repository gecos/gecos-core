int main() {
	static int res = 0;

	#pragma outline
	{
		res = 4;
	}

	if(res != 4)
		abort();

	return 0;
}
