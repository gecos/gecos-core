package fr.irisa.cairn.gecos.model.cdtfrontend.exception;

import org.eclipse.cdt.core.dom.ast.IASTNode;

/**
 * This exception should be thrown when parsing an {@link IASTNode}
 * is not currently supported, but that does not indicate a parsing error.
 * Parsing errors should be indicated by {@link CDTParserErrorException}.
 * 
 * @author aelmouss
 */
public class CDTParserNotYetSupportedException extends CDTParserProblemException {

	private static final long serialVersionUID = -4438249134185772696L;
	
	public CDTParserNotYetSupportedException(IASTNode node) {
		super(node, "Not yet Supported");
	}
	public CDTParserNotYetSupportedException(IASTNode node, String string) {
		super(node, string);
	}
	public CDTParserNotYetSupportedException(IASTNode node, String string, Exception e) {
		super(node, string, e);
	}
	
}
