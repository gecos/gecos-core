/**
 */
package gecos.annotations;

import gecos.core.CorePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see gecos.annotations.AnnotationsFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel modelName='Gecos' modelPluginClass='' creationCommands='false' creationIcons='false' interfaceNamePattern='' importerID='org.eclipse.emf.importer.ecore' operationReflection='false' basePackage='gecos'"
 * @generated
 */
public interface AnnotationsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "annotations";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.gecos.org/annotations";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "annotations";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AnnotationsPackage eINSTANCE = gecos.annotations.impl.AnnotationsPackageImpl.init();

	/**
	 * The meta object id for the '{@link gecos.annotations.impl.IAnnotationImpl <em>IAnnotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.annotations.impl.IAnnotationImpl
	 * @see gecos.annotations.impl.AnnotationsPackageImpl#getIAnnotation()
	 * @generated
	 */
	int IANNOTATION = 0;

	/**
	 * The number of structural features of the '<em>IAnnotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IANNOTATION_FEATURE_COUNT = CorePackage.GECOS_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.annotations.impl.StringAnnotationImpl <em>String Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.annotations.impl.StringAnnotationImpl
	 * @see gecos.annotations.impl.AnnotationsPackageImpl#getStringAnnotation()
	 * @generated
	 */
	int STRING_ANNOTATION = 1;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_ANNOTATION__CONTENT = IANNOTATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>String Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_ANNOTATION_FEATURE_COUNT = IANNOTATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.annotations.impl.PragmaAnnotationImpl <em>Pragma Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.annotations.impl.PragmaAnnotationImpl
	 * @see gecos.annotations.impl.AnnotationsPackageImpl#getPragmaAnnotation()
	 * @generated
	 */
	int PRAGMA_ANNOTATION = 2;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRAGMA_ANNOTATION__CONTENT = IANNOTATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Pragma Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRAGMA_ANNOTATION_FEATURE_COUNT = IANNOTATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.annotations.impl.CommentAnnotationImpl <em>Comment Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.annotations.impl.CommentAnnotationImpl
	 * @see gecos.annotations.impl.AnnotationsPackageImpl#getCommentAnnotation()
	 * @generated
	 */
	int COMMENT_ANNOTATION = 3;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT_ANNOTATION__CONTENT = STRING_ANNOTATION__CONTENT;

	/**
	 * The number of structural features of the '<em>Comment Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT_ANNOTATION_FEATURE_COUNT = STRING_ANNOTATION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.annotations.impl.ExtendedAnnotationImpl <em>Extended Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.annotations.impl.ExtendedAnnotationImpl
	 * @see gecos.annotations.impl.AnnotationsPackageImpl#getExtendedAnnotation()
	 * @generated
	 */
	int EXTENDED_ANNOTATION = 4;

	/**
	 * The feature id for the '<em><b>Fields</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENDED_ANNOTATION__FIELDS = IANNOTATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Refs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENDED_ANNOTATION__REFS = IANNOTATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Extended Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENDED_ANNOTATION_FEATURE_COUNT = IANNOTATION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.annotations.impl.LivenessAnnotationImpl <em>Liveness Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.annotations.impl.LivenessAnnotationImpl
	 * @see gecos.annotations.impl.AnnotationsPackageImpl#getLivenessAnnotation()
	 * @generated
	 */
	int LIVENESS_ANNOTATION = 5;

	/**
	 * The feature id for the '<em><b>Refs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIVENESS_ANNOTATION__REFS = IANNOTATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Liveness Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIVENESS_ANNOTATION_FEATURE_COUNT = IANNOTATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.annotations.impl.SymbolDefinitionAnnotationImpl <em>Symbol Definition Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.annotations.impl.SymbolDefinitionAnnotationImpl
	 * @see gecos.annotations.impl.AnnotationsPackageImpl#getSymbolDefinitionAnnotation()
	 * @generated
	 */
	int SYMBOL_DEFINITION_ANNOTATION = 6;

	/**
	 * The feature id for the '<em><b>Symbol Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_DEFINITION_ANNOTATION__SYMBOL_DEFINITION = IANNOTATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Symbol Definition Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_DEFINITION_ANNOTATION_FEATURE_COUNT = IANNOTATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.annotations.impl.IASTAnnotationImpl <em>IAST Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.annotations.impl.IASTAnnotationImpl
	 * @see gecos.annotations.impl.AnnotationsPackageImpl#getIASTAnnotation()
	 * @generated
	 */
	int IAST_ANNOTATION = 7;

	/**
	 * The feature id for the '<em><b>Iast Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IAST_ANNOTATION__IAST_NODE = IANNOTATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>IAST Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IAST_ANNOTATION_FEATURE_COUNT = IANNOTATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.annotations.impl.FileLocationAnnotationImpl <em>File Location Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.annotations.impl.FileLocationAnnotationImpl
	 * @see gecos.annotations.impl.AnnotationsPackageImpl#getFileLocationAnnotation()
	 * @generated
	 */
	int FILE_LOCATION_ANNOTATION = 8;

	/**
	 * The feature id for the '<em><b>Filename</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_LOCATION_ANNOTATION__FILENAME = IANNOTATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Starting Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_LOCATION_ANNOTATION__STARTING_LINE = IANNOTATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Ending Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_LOCATION_ANNOTATION__ENDING_LINE = IANNOTATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Node Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_LOCATION_ANNOTATION__NODE_OFFSET = IANNOTATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Node Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_LOCATION_ANNOTATION__NODE_LENGTH = IANNOTATION_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>File Location Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_LOCATION_ANNOTATION_FEATURE_COUNT = IANNOTATION_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link gecos.annotations.impl.StringToIAnnotationMapImpl <em>String To IAnnotation Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.annotations.impl.StringToIAnnotationMapImpl
	 * @see gecos.annotations.impl.AnnotationsPackageImpl#getStringToIAnnotationMap()
	 * @generated
	 */
	int STRING_TO_IANNOTATION_MAP = 9;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_IANNOTATION_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_IANNOTATION_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>String To IAnnotation Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_IANNOTATION_MAP_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link gecos.annotations.impl.AnnotatedElementImpl <em>Annotated Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.annotations.impl.AnnotatedElementImpl
	 * @see gecos.annotations.impl.AnnotationsPackageImpl#getAnnotatedElement()
	 * @generated
	 */
	int ANNOTATED_ELEMENT = 10;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_ELEMENT__ANNOTATIONS = CorePackage.GECOS_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Annotated Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_ELEMENT_FEATURE_COUNT = CorePackage.GECOS_NODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link gecos.annotations.AnnotationKeys <em>Annotation Keys</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.annotations.AnnotationKeys
	 * @see gecos.annotations.impl.AnnotationsPackageImpl#getAnnotationKeys()
	 * @generated
	 */
	int ANNOTATION_KEYS = 11;

	/**
	 * The meta object id for the '{@link gecos.annotations.LivenessInfo <em>Liveness Info</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.annotations.LivenessInfo
	 * @see gecos.annotations.impl.AnnotationsPackageImpl#getLivenessInfo()
	 * @generated
	 */
	int LIVENESS_INFO = 12;

	/**
	 * The meta object id for the '<em>String</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see gecos.annotations.impl.AnnotationsPackageImpl#getString()
	 * @generated
	 */
	int STRING = 13;


	/**
	 * Returns the meta object for class '{@link gecos.annotations.IAnnotation <em>IAnnotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IAnnotation</em>'.
	 * @see gecos.annotations.IAnnotation
	 * @generated
	 */
	EClass getIAnnotation();

	/**
	 * Returns the meta object for class '{@link gecos.annotations.StringAnnotation <em>String Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Annotation</em>'.
	 * @see gecos.annotations.StringAnnotation
	 * @generated
	 */
	EClass getStringAnnotation();

	/**
	 * Returns the meta object for the attribute '{@link gecos.annotations.StringAnnotation#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Content</em>'.
	 * @see gecos.annotations.StringAnnotation#getContent()
	 * @see #getStringAnnotation()
	 * @generated
	 */
	EAttribute getStringAnnotation_Content();

	/**
	 * Returns the meta object for class '{@link gecos.annotations.PragmaAnnotation <em>Pragma Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pragma Annotation</em>'.
	 * @see gecos.annotations.PragmaAnnotation
	 * @generated
	 */
	EClass getPragmaAnnotation();

	/**
	 * Returns the meta object for the attribute list '{@link gecos.annotations.PragmaAnnotation#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Content</em>'.
	 * @see gecos.annotations.PragmaAnnotation#getContent()
	 * @see #getPragmaAnnotation()
	 * @generated
	 */
	EAttribute getPragmaAnnotation_Content();

	/**
	 * Returns the meta object for class '{@link gecos.annotations.CommentAnnotation <em>Comment Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Comment Annotation</em>'.
	 * @see gecos.annotations.CommentAnnotation
	 * @generated
	 */
	EClass getCommentAnnotation();

	/**
	 * Returns the meta object for class '{@link gecos.annotations.ExtendedAnnotation <em>Extended Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Extended Annotation</em>'.
	 * @see gecos.annotations.ExtendedAnnotation
	 * @generated
	 */
	EClass getExtendedAnnotation();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.annotations.ExtendedAnnotation#getFields <em>Fields</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fields</em>'.
	 * @see gecos.annotations.ExtendedAnnotation#getFields()
	 * @see #getExtendedAnnotation()
	 * @generated
	 */
	EReference getExtendedAnnotation_Fields();

	/**
	 * Returns the meta object for the reference list '{@link gecos.annotations.ExtendedAnnotation#getRefs <em>Refs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Refs</em>'.
	 * @see gecos.annotations.ExtendedAnnotation#getRefs()
	 * @see #getExtendedAnnotation()
	 * @generated
	 */
	EReference getExtendedAnnotation_Refs();

	/**
	 * Returns the meta object for class '{@link gecos.annotations.LivenessAnnotation <em>Liveness Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Liveness Annotation</em>'.
	 * @see gecos.annotations.LivenessAnnotation
	 * @generated
	 */
	EClass getLivenessAnnotation();

	/**
	 * Returns the meta object for the reference list '{@link gecos.annotations.LivenessAnnotation#getRefs <em>Refs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Refs</em>'.
	 * @see gecos.annotations.LivenessAnnotation#getRefs()
	 * @see #getLivenessAnnotation()
	 * @generated
	 */
	EReference getLivenessAnnotation_Refs();

	/**
	 * Returns the meta object for class '{@link gecos.annotations.SymbolDefinitionAnnotation <em>Symbol Definition Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Symbol Definition Annotation</em>'.
	 * @see gecos.annotations.SymbolDefinitionAnnotation
	 * @generated
	 */
	EClass getSymbolDefinitionAnnotation();

	/**
	 * Returns the meta object for the reference '{@link gecos.annotations.SymbolDefinitionAnnotation#getSymbolDefinition <em>Symbol Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Symbol Definition</em>'.
	 * @see gecos.annotations.SymbolDefinitionAnnotation#getSymbolDefinition()
	 * @see #getSymbolDefinitionAnnotation()
	 * @generated
	 */
	EReference getSymbolDefinitionAnnotation_SymbolDefinition();

	/**
	 * Returns the meta object for class '{@link gecos.annotations.IASTAnnotation <em>IAST Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IAST Annotation</em>'.
	 * @see gecos.annotations.IASTAnnotation
	 * @generated
	 */
	EClass getIASTAnnotation();

	/**
	 * Returns the meta object for the reference '{@link gecos.annotations.IASTAnnotation#getIastNode <em>Iast Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Iast Node</em>'.
	 * @see gecos.annotations.IASTAnnotation#getIastNode()
	 * @see #getIASTAnnotation()
	 * @generated
	 */
	EReference getIASTAnnotation_IastNode();

	/**
	 * Returns the meta object for class '{@link gecos.annotations.FileLocationAnnotation <em>File Location Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>File Location Annotation</em>'.
	 * @see gecos.annotations.FileLocationAnnotation
	 * @generated
	 */
	EClass getFileLocationAnnotation();

	/**
	 * Returns the meta object for the attribute '{@link gecos.annotations.FileLocationAnnotation#getFilename <em>Filename</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Filename</em>'.
	 * @see gecos.annotations.FileLocationAnnotation#getFilename()
	 * @see #getFileLocationAnnotation()
	 * @generated
	 */
	EAttribute getFileLocationAnnotation_Filename();

	/**
	 * Returns the meta object for the attribute '{@link gecos.annotations.FileLocationAnnotation#getStartingLine <em>Starting Line</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Starting Line</em>'.
	 * @see gecos.annotations.FileLocationAnnotation#getStartingLine()
	 * @see #getFileLocationAnnotation()
	 * @generated
	 */
	EAttribute getFileLocationAnnotation_StartingLine();

	/**
	 * Returns the meta object for the attribute '{@link gecos.annotations.FileLocationAnnotation#getEndingLine <em>Ending Line</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ending Line</em>'.
	 * @see gecos.annotations.FileLocationAnnotation#getEndingLine()
	 * @see #getFileLocationAnnotation()
	 * @generated
	 */
	EAttribute getFileLocationAnnotation_EndingLine();

	/**
	 * Returns the meta object for the attribute '{@link gecos.annotations.FileLocationAnnotation#getNodeOffset <em>Node Offset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Node Offset</em>'.
	 * @see gecos.annotations.FileLocationAnnotation#getNodeOffset()
	 * @see #getFileLocationAnnotation()
	 * @generated
	 */
	EAttribute getFileLocationAnnotation_NodeOffset();

	/**
	 * Returns the meta object for the attribute '{@link gecos.annotations.FileLocationAnnotation#getNodeLength <em>Node Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Node Length</em>'.
	 * @see gecos.annotations.FileLocationAnnotation#getNodeLength()
	 * @see #getFileLocationAnnotation()
	 * @generated
	 */
	EAttribute getFileLocationAnnotation_NodeLength();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>String To IAnnotation Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String To IAnnotation Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyUnique="false" keyDataType="gecos.annotations.String"
	 *        valueType="gecos.annotations.IAnnotation" valueContainment="true"
	 * @generated
	 */
	EClass getStringToIAnnotationMap();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToIAnnotationMap()
	 * @generated
	 */
	EAttribute getStringToIAnnotationMap_Key();

	/**
	 * Returns the meta object for the containment reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToIAnnotationMap()
	 * @generated
	 */
	EReference getStringToIAnnotationMap_Value();

	/**
	 * Returns the meta object for class '{@link gecos.annotations.AnnotatedElement <em>Annotated Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotated Element</em>'.
	 * @see gecos.annotations.AnnotatedElement
	 * @generated
	 */
	EClass getAnnotatedElement();

	/**
	 * Returns the meta object for the map '{@link gecos.annotations.AnnotatedElement#getAnnotations <em>Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Annotations</em>'.
	 * @see gecos.annotations.AnnotatedElement#getAnnotations()
	 * @see #getAnnotatedElement()
	 * @generated
	 */
	EReference getAnnotatedElement_Annotations();

	/**
	 * Returns the meta object for enum '{@link gecos.annotations.AnnotationKeys <em>Annotation Keys</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Annotation Keys</em>'.
	 * @see gecos.annotations.AnnotationKeys
	 * @generated
	 */
	EEnum getAnnotationKeys();

	/**
	 * Returns the meta object for enum '{@link gecos.annotations.LivenessInfo <em>Liveness Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Liveness Info</em>'.
	 * @see gecos.annotations.LivenessInfo
	 * @generated
	 */
	EEnum getLivenessInfo();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>String</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getString();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AnnotationsFactory getAnnotationsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link gecos.annotations.impl.IAnnotationImpl <em>IAnnotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.annotations.impl.IAnnotationImpl
		 * @see gecos.annotations.impl.AnnotationsPackageImpl#getIAnnotation()
		 * @generated
		 */
		EClass IANNOTATION = eINSTANCE.getIAnnotation();

		/**
		 * The meta object literal for the '{@link gecos.annotations.impl.StringAnnotationImpl <em>String Annotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.annotations.impl.StringAnnotationImpl
		 * @see gecos.annotations.impl.AnnotationsPackageImpl#getStringAnnotation()
		 * @generated
		 */
		EClass STRING_ANNOTATION = eINSTANCE.getStringAnnotation();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_ANNOTATION__CONTENT = eINSTANCE.getStringAnnotation_Content();

		/**
		 * The meta object literal for the '{@link gecos.annotations.impl.PragmaAnnotationImpl <em>Pragma Annotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.annotations.impl.PragmaAnnotationImpl
		 * @see gecos.annotations.impl.AnnotationsPackageImpl#getPragmaAnnotation()
		 * @generated
		 */
		EClass PRAGMA_ANNOTATION = eINSTANCE.getPragmaAnnotation();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRAGMA_ANNOTATION__CONTENT = eINSTANCE.getPragmaAnnotation_Content();

		/**
		 * The meta object literal for the '{@link gecos.annotations.impl.CommentAnnotationImpl <em>Comment Annotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.annotations.impl.CommentAnnotationImpl
		 * @see gecos.annotations.impl.AnnotationsPackageImpl#getCommentAnnotation()
		 * @generated
		 */
		EClass COMMENT_ANNOTATION = eINSTANCE.getCommentAnnotation();

		/**
		 * The meta object literal for the '{@link gecos.annotations.impl.ExtendedAnnotationImpl <em>Extended Annotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.annotations.impl.ExtendedAnnotationImpl
		 * @see gecos.annotations.impl.AnnotationsPackageImpl#getExtendedAnnotation()
		 * @generated
		 */
		EClass EXTENDED_ANNOTATION = eINSTANCE.getExtendedAnnotation();

		/**
		 * The meta object literal for the '<em><b>Fields</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXTENDED_ANNOTATION__FIELDS = eINSTANCE.getExtendedAnnotation_Fields();

		/**
		 * The meta object literal for the '<em><b>Refs</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXTENDED_ANNOTATION__REFS = eINSTANCE.getExtendedAnnotation_Refs();

		/**
		 * The meta object literal for the '{@link gecos.annotations.impl.LivenessAnnotationImpl <em>Liveness Annotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.annotations.impl.LivenessAnnotationImpl
		 * @see gecos.annotations.impl.AnnotationsPackageImpl#getLivenessAnnotation()
		 * @generated
		 */
		EClass LIVENESS_ANNOTATION = eINSTANCE.getLivenessAnnotation();

		/**
		 * The meta object literal for the '<em><b>Refs</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIVENESS_ANNOTATION__REFS = eINSTANCE.getLivenessAnnotation_Refs();

		/**
		 * The meta object literal for the '{@link gecos.annotations.impl.SymbolDefinitionAnnotationImpl <em>Symbol Definition Annotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.annotations.impl.SymbolDefinitionAnnotationImpl
		 * @see gecos.annotations.impl.AnnotationsPackageImpl#getSymbolDefinitionAnnotation()
		 * @generated
		 */
		EClass SYMBOL_DEFINITION_ANNOTATION = eINSTANCE.getSymbolDefinitionAnnotation();

		/**
		 * The meta object literal for the '<em><b>Symbol Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYMBOL_DEFINITION_ANNOTATION__SYMBOL_DEFINITION = eINSTANCE.getSymbolDefinitionAnnotation_SymbolDefinition();

		/**
		 * The meta object literal for the '{@link gecos.annotations.impl.IASTAnnotationImpl <em>IAST Annotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.annotations.impl.IASTAnnotationImpl
		 * @see gecos.annotations.impl.AnnotationsPackageImpl#getIASTAnnotation()
		 * @generated
		 */
		EClass IAST_ANNOTATION = eINSTANCE.getIASTAnnotation();

		/**
		 * The meta object literal for the '<em><b>Iast Node</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IAST_ANNOTATION__IAST_NODE = eINSTANCE.getIASTAnnotation_IastNode();

		/**
		 * The meta object literal for the '{@link gecos.annotations.impl.FileLocationAnnotationImpl <em>File Location Annotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.annotations.impl.FileLocationAnnotationImpl
		 * @see gecos.annotations.impl.AnnotationsPackageImpl#getFileLocationAnnotation()
		 * @generated
		 */
		EClass FILE_LOCATION_ANNOTATION = eINSTANCE.getFileLocationAnnotation();

		/**
		 * The meta object literal for the '<em><b>Filename</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FILE_LOCATION_ANNOTATION__FILENAME = eINSTANCE.getFileLocationAnnotation_Filename();

		/**
		 * The meta object literal for the '<em><b>Starting Line</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FILE_LOCATION_ANNOTATION__STARTING_LINE = eINSTANCE.getFileLocationAnnotation_StartingLine();

		/**
		 * The meta object literal for the '<em><b>Ending Line</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FILE_LOCATION_ANNOTATION__ENDING_LINE = eINSTANCE.getFileLocationAnnotation_EndingLine();

		/**
		 * The meta object literal for the '<em><b>Node Offset</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FILE_LOCATION_ANNOTATION__NODE_OFFSET = eINSTANCE.getFileLocationAnnotation_NodeOffset();

		/**
		 * The meta object literal for the '<em><b>Node Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FILE_LOCATION_ANNOTATION__NODE_LENGTH = eINSTANCE.getFileLocationAnnotation_NodeLength();

		/**
		 * The meta object literal for the '{@link gecos.annotations.impl.StringToIAnnotationMapImpl <em>String To IAnnotation Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.annotations.impl.StringToIAnnotationMapImpl
		 * @see gecos.annotations.impl.AnnotationsPackageImpl#getStringToIAnnotationMap()
		 * @generated
		 */
		EClass STRING_TO_IANNOTATION_MAP = eINSTANCE.getStringToIAnnotationMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_TO_IANNOTATION_MAP__KEY = eINSTANCE.getStringToIAnnotationMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRING_TO_IANNOTATION_MAP__VALUE = eINSTANCE.getStringToIAnnotationMap_Value();

		/**
		 * The meta object literal for the '{@link gecos.annotations.impl.AnnotatedElementImpl <em>Annotated Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.annotations.impl.AnnotatedElementImpl
		 * @see gecos.annotations.impl.AnnotationsPackageImpl#getAnnotatedElement()
		 * @generated
		 */
		EClass ANNOTATED_ELEMENT = eINSTANCE.getAnnotatedElement();

		/**
		 * The meta object literal for the '<em><b>Annotations</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANNOTATED_ELEMENT__ANNOTATIONS = eINSTANCE.getAnnotatedElement_Annotations();

		/**
		 * The meta object literal for the '{@link gecos.annotations.AnnotationKeys <em>Annotation Keys</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.annotations.AnnotationKeys
		 * @see gecos.annotations.impl.AnnotationsPackageImpl#getAnnotationKeys()
		 * @generated
		 */
		EEnum ANNOTATION_KEYS = eINSTANCE.getAnnotationKeys();

		/**
		 * The meta object literal for the '{@link gecos.annotations.LivenessInfo <em>Liveness Info</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.annotations.LivenessInfo
		 * @see gecos.annotations.impl.AnnotationsPackageImpl#getLivenessInfo()
		 * @generated
		 */
		EEnum LIVENESS_INFO = eINSTANCE.getLivenessInfo();

		/**
		 * The meta object literal for the '<em>String</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see gecos.annotations.impl.AnnotationsPackageImpl#getString()
		 * @generated
		 */
		EDataType STRING = eINSTANCE.getString();

	}

} //AnnotationsPackage
