package fr.irisa.cairn.gecos.core.testframework.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.gecos.testframework.exceptions.CheckerFailure;
import fr.irisa.cairn.gecos.testframework.model.IVersionOperator;
import fr.irisa.cairn.tools.ecore.DanglingAnalyzer;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.gecosproject.GecosProject;

public class GSCheckers {

	private GSCheckers() {}

	/**
	 * Check for dangling references in the {@link GecosProject}.
	 * 
	 * @return a dangling reference checker.
	 */
	public static <V extends GSProjectVersion> IVersionOperator<V> danglingRef() {
		return version -> {
			GecosProject project = version.getProject();
			if(project == null || !project.isParsed())
				throw new IllegalArgumentException("gecos project is null or not parsed: " + version);
			
			List<EObject> dr = new DanglingAnalyzer(project).getAllDanglingReferences(project);
			if(!dr.isEmpty()) {
				String msg = dr.stream()
					.map(r -> "- " + r + " referenced by: " + EMFUtils.getAllReferencesOf(dr.get(0), project))
					.collect(Collectors.joining("\n"));
				throw new CheckerFailure("The version '" + version + "' has dangling references:\n" + msg);
			}
		};
	}
}
