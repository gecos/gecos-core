package fr.irisa.cairn.gecos.core.ui.plugin.templates;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.pde.core.plugin.IMatchRules;
import org.eclipse.pde.core.plugin.IPluginReference;
import org.eclipse.pde.ui.templates.PluginReference;

import fr.irisa.cairn.gecos.core.ui.plugin.templates.utils.BasicCopyPluginTemplate;
import fr.irisa.cairn.gecos.model.GecosModelPlugin;

public class UseFactoriesToBuildIR extends BasicCopyPluginTemplate {
	@Override
	public IPluginReference[] getDependencies(String schemaVersion) {
		return new IPluginReference[] { new PluginReference("fr.irisa.r2d2.gecos.framework", null, IMatchRules.NONE),
				new PluginReference(GecosModelPlugin.PLUGIN_ID, null, IMatchRules.NONE) };
	}

	@Override
	protected String getTemplateDirectory() {
		return "resources/plugin_templates"; //$NON-NLS-1$
	}

	@Override
	public String getSectionId() {
		return "usefactoriestobuildir"; //$NON-NLS-1$
	}

	@Override
	public String getTitle() {
		return "Use GeCoS factories to build GeCoS IR example";
	}

	@Override
	public String getDescription() {
		return "Generate a GeCoS module with its implementation which use GeCoS factories to build a GeCoS IR";
	}

	@Override
	public Map<String, String> getModuleClass() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("BuildIRWithFactory", "modules.FactoryUseToBuildIR");
		return map;
	}

	@Override
	public String getSubPackageName() {
		return "factories";
	}
}