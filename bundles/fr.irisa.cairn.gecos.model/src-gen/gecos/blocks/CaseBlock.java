/**
 */
package gecos.blocks;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Case Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.blocks.CaseBlock#getValue <em>Value</em>}</li>
 *   <li>{@link gecos.blocks.CaseBlock#getBody <em>Body</em>}</li>
 * </ul>
 *
 * @see gecos.blocks.BlocksPackage#getCaseBlock()
 * @model annotation="gmf.node figure='rectangle' label='number' label.placement='external' label.icon='false'"
 * @generated
 */
public interface CaseBlock extends Block, BlocksVisitable {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(long)
	 * @see gecos.blocks.BlocksPackage#getCaseBlock_Value()
	 * @model default="0" unique="false" dataType="gecos.blocks.long" required="true"
	 * @generated
	 */
	long getValue();

	/**
	 * Sets the value of the '{@link gecos.blocks.CaseBlock#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(long value);

	/**
	 * Returns the value of the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body</em>' containment reference.
	 * @see #setBody(Block)
	 * @see gecos.blocks.BlocksPackage#getCaseBlock_Body()
	 * @model containment="true"
	 *        annotation="gmf.compartment foo='bar'"
	 * @generated
	 */
	Block getBody();

	/**
	 * Sets the value of the '{@link gecos.blocks.CaseBlock#getBody <em>Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body</em>' containment reference.
	 * @see #getBody()
	 * @generated
	 */
	void setBody(Block value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitCaseBlock(this);'"
	 * @generated
	 */
	void accept(BlocksVisitor visitor);

} // CaseBlock
