package fr.irisa.cairn.gecos.model.tools.switches;

import gecos.dag.DAGInstruction;
import gecos.dag.DAGNode;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

public class DAGForwardDataFlowSwitch<T> extends DAGDefaultInstructionSwitch<T>{

	public class FindDAGSourceNode extends DAGDefaultInstructionSwitch<T> {
		
		private EList<DAGNode> sourceNodes;


		public FindDAGSourceNode(DAGInstruction inst) {
			sourceNodes = new BasicEList<DAGNode>();
			for (DAGNode node : inst.getNodes()) {
				doSwitch(node);
			}
		}


		@Override
		public T caseDAGNode(DAGNode object) {
			if (object.getInputs().size()==0) {
				sourceNodes.add(object);
			}
			return super.caseDAGNode(object);
		}


		public EList<DAGNode> getSourceNodes() {
			return sourceNodes;
		}
		
	}
	
	EList<DAGNode> left;
	
	@Override
	public T caseDAGInstruction(DAGInstruction object) {
		EList<DAGNode> sources = (new FindDAGSourceNode(object)).getSourceNodes(); // TODO Auto-generated method stub
		left = new BasicEList<DAGNode>();
		left.addAll(object.getNodes());
		left.removeAll(sources);
		DAGNode visitedNode;
		while (left.size()!=0) {
			visitedNode = null;
			for (DAGNode dagNode : left) {
				boolean ready= true;
				for (DAGNode pred : dagNode.getPredecessors()) {
					if (left.contains(pred)) {
						ready= false;
						break;
					}
				}
				if (ready) {
					doSwitch(dagNode);
					visitedNode = dagNode;
					break;
				}
			}
			if (visitedNode!=null) {
				left.remove(visitedNode);
			} else {
				throw new RuntimeException("Something wrong in this visitor ...");
			}
		}
		return null;
	}


	@Override
	public T caseDAGNode(DAGNode object) {
		System.out.println("Node "+object);
		return super.caseDAGNode(object);
	}

	
}
