package fr.irisa.cairn.gecos.model.typesystem;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import gecos.blocks.Block;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.FloatType;
import gecos.types.IntegerType;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.PtrType;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;

@SuppressWarnings({"all"}) 
public class ArithmeticTypingRules  {
 
	%include { sl.tom }
	%include { long.tom  }
	%include { double.tom }
	%include { gecos_common.tom }
	%include { gecos_terminals.tom } 
	%include { gecos_basic.tom }
	%include { gecos_arithmetic.tom }  
	%include { gecos_compare.tom } 
	%include { gecos_ssa.tom } 
	%include { gecos_logical.tom } 
    %include { gecos_operators.tom } 
    %include { gecos_misc.tom } 
    %include { gecos_types.tom } 
 
   protected static void debug(String mess) {
		if(VERBOSE) System.out.println(mess);
	}
 /* aa */
	protected static void debug(int n, String mess) {  
		debug("Rule "+n+" : "+mess); 
	}

    public static final boolean VERBOSE = true;

	protected static void binaryArithmeticTypingRule(GenericInstruction instruction,Instruction operandA, Instruction operandB){
		String name =  instruction.getName();

		Type res = null;

		%match(String name,operandA.getType(),operandB.getType()){ 
			_,intType(_,_),b@floatType(_) -> {
				debug(6,"TypeResolver -> Arithmetic operation between Integer and Float "+`instruction); 
				instruction.setType(`b); 
				return;
			}

			_,b@floatType(_),intType(_,_) -> { 
				debug(7,"TypeResolver : double <- "+name+"(float,int) "+`instruction); 
				instruction.setType(`b); 
				return;
			}
	 
			_,a@intType(_,sizea),b@intType(_,sizeb) -> {
				debug(6,"TypeResolver -> "+name+" operation between two Integer "+`instruction);
				if((`sizea.getValue())>(`sizeb.getValue())) {
					instruction.setType(`a); 
				} else {
					instruction.setType(`b); //XXX Should'nt we consider sign?
				} 
				return;
			}

			_,a@floatType(sizea),b@floatType(sizeb) -> { 
				debug(7,"TypeResolver ->  "+name+" operation between two Float "+`instruction); 
				if((`sizea.getValue())>(`sizeb.getValue())) {
					instruction.setType(`a); 
				} else {
					instruction.setType(`b); 
				} 
				return;
			}
			op,ptrType(_),ptrType(_) -> { 
				%match(op) {
					"sub" -> {
						(`instruction).setType(GecosUserTypeFactory.INT());  
					}
				}  
				return;
			}
			op,p@ptrType(_),intType(_,_) -> { 
				%match(op) {
					"sub"|"add" -> {
						instruction.setType(`p);  
						return;
					} 
				}  
			}
			op,intType(_,_),p@ptrType(_) -> { 
				%match(op) {
					"sub" -> {
						instruction.setType(`p);  
						return;
					}
				}  
			}
			
		}
		throw new RuntimeException("Not typing rule for "+instruction);

	}
   
	protected static void unaryArithmeticTypingRule(GenericInstruction instruction,Instruction operandA){
		String name =  instruction.getName();
		Type res = null;
		%match(String name,Type operandA.getType()){ 
 		
			"neg"|"inv"|"not",b -> {
				instruction.setType(`b); 
				return;
			}
		}
		throw new RuntimeException("Not typing rule for "+instruction);

	}
}