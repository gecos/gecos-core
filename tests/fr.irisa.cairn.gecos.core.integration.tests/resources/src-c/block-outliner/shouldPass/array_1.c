int main() {
	int A[10] = {1,2,3};

	#pragma outline
	{
		A[0] = 4;
	}

	//-- test --
	if(A[0] != 4)
		abort();

	return 0;
}
