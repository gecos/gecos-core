/* this should compile but it does not: considers it as conflicting types */

int a[];   //incomplete type

int a[10]; //... completed

int main() {
	exit(0);
}