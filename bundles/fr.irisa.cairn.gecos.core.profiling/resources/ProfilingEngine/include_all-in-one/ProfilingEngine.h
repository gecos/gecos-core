#ifndef __PROFILING_ENGINE_H__
#define __PROFILING_ENGINE_H__

#include <string>
#include <list>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <typeinfo>
#include <random>
#include <functional>


namespace profengine {

	/**
	 *
	 */
	class Variable {
        protected:
            std::string m_name;
            int m_uid;
            int current_copy = 0;

            Variable(std::string name, int uid) : m_name(name), m_uid(uid) {};

        public:
            virtual ~Variable() {};
            std::string getName() { return m_name; }
            int getUid() { return m_uid; }
            std::string toString() { return m_name + "_" + std::to_string(m_uid); }

            void newCopy() {
#ifdef DEBUG
        std::cout << "Finish recording current copy values of variable '" << toString() << "'" << std::endl;
#endif
            	current_copy++;
            }

            virtual void writeHuman(std::fstream &fs) = 0;
//            virtual void writeBinary(std::fstream &fs) = 0;
    };


	/**
	 *
	 */
    template <class T> class GenericVariable : public Variable {
        private:
            std::map<int, std::list<T> > m_values; //<copy number, value>

        public:
            GenericVariable(std::string name, int uid):Variable(name, uid){};
            ~GenericVariable(){}


            void addValue(T value) {
           #ifdef DEBUG
                   std::cout << "  Pushing value to variable '" << toString() << "' = " << value <<std::endl;
           #endif

                   m_values[current_copy].push_back(value);
               }

//		   void writeBinary(std::fstream &fs){
//			   short varKind = 1;
//			   fs.write((char*)&varKind, sizeof(varKind));
//
//			   int nb_elements = m_values.size();
//			   fs.write((char*)&nb_elements, sizeof(nb_elements));
//
//			   for(auto const& entry : m_values) {
//				int index = entry.first;
//				std::list<T> values = entry.second;
//				int size = values.size();
//
//				   fs.write((char*)&index, sizeof(index));
//				   fs.write((char*)&size, sizeof(size));
//				   for(auto const& val : values)
//					fs.write((char*)&val, sizeof(val));
//			   }
//		   }


		   void writeHuman(std::fstream &fs){
			   fs << "uid: " << m_uid << std::endl;
			   fs << "name: " << m_name << std::endl;
			   fs << "elemTypeIdName: " << typeid(T).name() << std::endl;
			   fs << "elemSizeBytes: " << sizeof(T) << std::endl;
			   fs << "nbSnapshots: " << m_values.size() << std::endl;
			   fs << std::endl;

			   for(auto const& entry : m_values) {
				std::list<T> values = entry.second;

				   fs << "snapshot: " << entry.first << std::endl;
				   fs << "nbElemsPerCopy: " << values.size() << std::endl;
				   for(auto const& val : values)
					   fs << val << std::endl;

				   fs << std::endl;
			   }
		   }
    };


    /**
     *
     */
    class ProfilingEngine {
        private:
            std::map<int, Variable*> m_values; //saved variables <uid, variable>
            std::default_random_engine *random_generator = new std::default_random_engine(std::random_device()());

        public:

        	~ProfilingEngine() {
        		delete random_generator;
        		for(auto const& entry : m_values)
        			delete entry.second;
        	}

        	Variable* getVariable(int uid) {
        		return m_values[uid];
        	}

            void saveVariable(int uid, Variable *variable) {
                if(m_values[uid] != NULL){
                    std::cerr << "Variable already created. Problem during the construction of the manager variable mapping" << std::endl;
                    exit(1);
                }

                m_values[uid] = variable;
            }

            void writeHuman(std::string outputFolderPath){
                std::fstream fs;
                std::string pathOutput;

                for(auto const& entry : m_values){
                	Variable* var = entry.second;

                    pathOutput = outputFolderPath +"/" + var->toString() + ".txt";
                    fs.open(pathOutput.c_str(), std::fstream::out);
                    if(!fs.is_open()){
                        std::cerr << "File to write simulation result can not be open: " << pathOutput << std::endl;
                        exit(1);
                    }

                    var->writeHuman(fs);

                    fs.close();
                }
            }

//            void writeBinary(std::string outputFolderPath){
//                std::fstream fs;
//                std::string pathOutput;
//
//                for(auto const& entry : m_values){
//        			Variable* var = entry.second;
//
//                    pathOutput = outputFolderPath + "/" + var->toString() + ".bin";
//                    fs.open(pathOutput.c_str(), std::fstream::out | std::fstream::binary);
//                    if(!fs.is_open()){
//                        std::cerr << "File to write simulation result can not be open: " << pathOutput << std::endl;
//                        exit(1);
//                    }
//
//                    var->writeBinary(fs);
//
//                    fs.close();
//                }
//            }

            std::default_random_engine *getGenerator() {
            	return random_generator;
            }

            void setGeneratorSeed(int seed) {
            	random_generator = new std::default_random_engine(seed);
            }

    };

}

//----------------------------------------------------------------------------------------
//  Collect values API
//----------------------------------------------------------------------------------------


static profengine::ProfilingEngine *__internal_sim_manager;

void ProfilingEngine_start() {
	__internal_sim_manager = new profengine::ProfilingEngine;
}


template<typename T>
void ProfilingEngine_pushValue(int uid, const char* name, T value) {
	profengine::Variable* var = __internal_sim_manager->getVariable(uid);

	profengine::GenericVariable<T> *gvar;
    if(var == NULL) {
        gvar = new profengine::GenericVariable<T>(std::string(name), uid);
        __internal_sim_manager->saveVariable(uid, gvar);
    }
    else{
        gvar = dynamic_cast< profengine::GenericVariable<T>* >(var);
        if(gvar == NULL) {
        	std::cerr <<"The variable '" << name << "' was saved earlier as a different element type than '" << typeid(T).name() << "'" << std::endl;
        	exit(1);
    	}
    }

    gvar->addValue(value);
}

void ProfilingEngine_saveSnapshot(int uid) {
	profengine::Variable* var = __internal_sim_manager->getVariable(uid);
	if(var == NULL) {
		std::cerr <<"No variable with uid '" << uid << "' was registered!" << std::endl;
		exit(1);
	}

	var->newCopy();
}


void ProfilingEngine_writeHuman(const char* outputFolderPath) {
    __internal_sim_manager->writeHuman(std::string(outputFolderPath));
}

//void ProfilingEngine_writeBinary(const char* outputFolderPath) {
//    __internal_sim_manager->writeBinary(std::string(outputFolderPath));
//}

void ProfilingEngine_finalize(const char* outputFolderPath) {
	__internal_sim_manager->writeHuman(std::string(outputFolderPath));
//    __internal_sim_manager->writeBinary(std::string(outputFolderPath));
	delete __internal_sim_manager;
}



//----------------------------------------------------------------------------------------
//  Inject values API
//----------------------------------------------------------------------------------------

#define ProfilingEngine_initInjector_fromVar(filePath)  "TODO: not yet implemented"

#define ProfilingEngine_initInjector_fromFile(filePath)  "TODO: not yet implemented"


#define ProfilingEngine_initInjector_RandomSeed(newSeed) \
	__internal_sim_manager->setGeneratorSeed(newSeed);

#define ProfilingEngine_initInjector_RandomUniformInt(minInclusive, maxInclusive) \
	std::uniform_int_distribution<int> _distribution(minInclusive, maxInclusive);

#define ProfilingEngine_initInjector_RandomUniformDouble(minInclusive, maxInclusive) \
	std::uniform_real_distribution<double> _distribution(minInclusive, maxInclusive);


#define ProfilingEngine_initInjector_RandomNormalInt(mean, stddev) \
	std::normal_distribution<int> _distribution(mean, stddev);

#define ProfilingEngine_initInjector_RandomNormalDouble(mean, stddev) \
	std::normal_distribution<double> _distribution(mean, stddev);


#define ProfilingEngine_injectValue()   _distribution( *__internal_sim_manager->getGenerator() )



#endif // __PROFILING_ENGINE_H__
