/**
 */
package gecos.instrs;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>String Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.StringInstruction#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getStringInstruction()
 * @model
 * @generated
 */
public interface StringInstruction extends ConstantInstruction {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see gecos.instrs.InstrsPackage#getStringInstruction_Value()
	 * @model unique="false" dataType="gecos.instrs.String"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link gecos.instrs.StringInstruction#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitStringInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _value = this.getValue();\n&lt;%java.lang.String%&gt; _plus = (\"\\\"\" + _value);\nreturn (_plus + \"\\\"\");'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.StringInstruction%&gt;))\n{\n\tboolean isSameValue = false;\n\tif (((((&lt;%gecos.instrs.StringInstruction%&gt;)instr).getValue() == null) &amp;&amp; (this.getValue() == null)))\n\t{\n\t\tisSameValue = true;\n\t}\n\telse\n\t{\n\t\t&lt;%java.lang.String%&gt; _value = ((&lt;%gecos.instrs.StringInstruction%&gt;)instr).getValue();\n\t\tboolean _tripleEquals = (_value == null);\n\t\t&lt;%java.lang.String%&gt; _value_1 = this.getValue();\n\t\tboolean _tripleEquals_1 = (_value_1 == null);\n\t\tboolean _xor = (_tripleEquals ^ _tripleEquals_1);\n\t\tif (_xor)\n\t\t{\n\t\t\treturn false;\n\t\t}\n\t\telse\n\t\t{\n\t\t\tisSameValue = ((&lt;%gecos.instrs.StringInstruction%&gt;)instr).getValue().equals(this.getValue());\n\t\t}\n\t}\n\treturn (super.isSame(instr) &amp;&amp; isSameValue);\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // StringInstruction
