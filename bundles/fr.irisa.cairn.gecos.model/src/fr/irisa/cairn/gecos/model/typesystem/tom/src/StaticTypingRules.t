package fr.irisa.cairn.gecos.model.typesystem;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.extensions.typesystem.ITypeSystemRuleSet;
import gecos.blocks.Block;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.InstrsPackage;
import gecos.instrs.Instruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.PhiInstruction;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SSADefSymbol;
import gecos.instrs.SSAUseSymbol;
import gecos.instrs.SelectOperator;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;

@SuppressWarnings("all")
public class StaticTypingRules implements ITypeSystemRuleSet {

	%include { sl.tom }
	%include { long.tom }
	%include { double.tom }
	%include { gecos_common.tom }
	%include { gecos_terminals.tom } 
	%include { gecos_basic.tom }
	%include { gecos_arithmetic.tom }  
	%include { gecos_compare.tom } 
	%include { gecos_control.tom } 
	%include { gecos_ssa.tom } 
	%include { gecos_logical.tom } 
    %include { gecos_operators.tom } 
    %include { gecos_misc.tom } 
    %include { gecos_types.tom } 

	protected static void debug(String mess) {
		if(VERBOSE) System.out.println(mess);
	}

	protected static void debug(int n, String mess) {  
		debug("Rule "+n+" : "+mess); 
	}

   public static final boolean VERBOSE = true;
 
    public StaticTypingRules() {
	 
    }



	@Override
	public Type computeType(Instruction o) {
		if(o.eClass().getEPackage()!=InstrsPackage.eINSTANCE)
			return null;
		Type res = tomType(o);
		return res;
    }

    
	public Type tomType(Instruction o) {
		%match(o) {
			ssaDef(sym,_) -> { 
				Type type= (`sym).getType();
				debug(0,"SSA  "+o+" with Type "+type);
				return type;  
			}
 
			ssaUse(sym,_) -> { 
				Type type= (`sym).getType();
				debug(0,"SSA "+o+" with Type "+type);
				return type;  
			}
   
			set(dst,_) -> { 
				Type type= (`dst).getType();
				debug(0,"SET "+o+" with Type "+type);
				return type;  
			}
			
			phi(InstL(a,_*)) -> { 
				Type type= (`a).getType();
				debug(0,"Phi node "+o+" with Type "+type);
				return type;  
			}

			//  Symbol reference
			symref(sym) -> {
				Type type= (`sym).getType();
				debug(0,"Symbol instruction "+o+" with Type "+type);
				return type;  
			}


		}	
		return null;

	}
	
}