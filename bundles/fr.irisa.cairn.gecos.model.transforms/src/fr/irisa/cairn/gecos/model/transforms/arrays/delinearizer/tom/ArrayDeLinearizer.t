package fr.irisa.cairn.gecos.model.transforms.arrays.delinearizer;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.transforms.arrays.delinearizer.internal.DistributeMultiplyOverAdd;
import gecos.blocks.Block;
import gecos.core.Procedure;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.gecosproject.GecosProject;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.instrs.SimpleArrayInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.ArrayType;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.PtrType;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;

public class ArrayDeLinearizer {

	%include { sl.tom }  

	%include { gecos_common.tom }
	%include { gecos_terminals.tom } 
	%include { gecos_basic.tom }

	private static boolean debug = false; 
	private GecosProject proj;

	public ArrayDeLinearizer(GecosProject proj) {
		this.proj = proj;
	}

	public void compute() {
		for ( Procedure proc : proj.listProcedures() ) {
			this.delinearize(proc);
		}
	}

	public void delinearize(Procedure proc) {		
		try { 
			`TopDown(delinearizeAccess()).visitLight(proc, tom.mapping.GenericIntrospector.INSTANCE);
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Visitor Failure in DelinearizingArrays " + proc);
		}
	}

	%strategy delinearizeAccess() extends Identity() {
		visit Inst {
			arr@sarray(symref(sym),InstL(expr)) -> {
				if (debug) System.out.println("Changing type of inst: " + `arr);
				new DistributeMultiplyOverAdd(`expr).compute();
				new DistributeMultiplyOverAdd(`expr).compute();
				Instruction inst = `expr;
				if (debug) System.out.println("DistributedMul: " + inst + "  " + `expr);
				List<Instruction> sizeInstList = new ArrayList<>();
				boolean isPtr = getSizeExprs(`sym.getType(),sizeInstList);
				List<Instruction> list = getExpressionList(inst, false);
				if ( !isPtr ) {
					List<List<Instruction>> indexExpr = new ArrayList<>();
					boolean replaceInst = true;
					for ( Instruction op : list ) {
						Instruction expr = null;
						if (debug) System.out.println("Index of " + op );
						int index = getIndex(op, sizeInstList, indexExpr);
						if (debug) System.out.println("Index is " + op + "  " + index);
						if ( index == -1 ) {
							replaceInst = false;
							break;
						}
						
					}
					if ( replaceInst ) {
						if (debug) System.out.println("IndexExpr " + indexExpr.size());
						Instruction newInst = buildMultiDimArrayInstruction(`sym, indexExpr);
						`arr.getParent().replaceChild(`arr, newInst);
						if (debug) System.out.println("ArrayInstruction: replace by " + `newInst);
					}
				}	
			}
		}
	}

	private static int getIndex(Instruction inst, List<Instruction> sizeInstList, List<List<Instruction>> indexExprList) {

		Instruction indexExpr = null;
		Instruction currInst = inst;
		int currIndex = 0;
		while ( true ) {
			if ( (currInst instanceof GenericInstruction) && 
					(((GenericInstruction) currInst).getName() == "mul") ) {
				GenericInstruction mul = (GenericInstruction)currInst;
				if ( mul.getOperands().size() > 2 ) {
					currIndex = -1;
					break;
				}
				Instruction op1 = mul.getOperand(0);
				Instruction op2 = mul.getOperand(1);
				if ( isSizeParam(op1, sizeInstList) ) {
					currIndex++;
					currInst = op2;
				} else if ( isSizeParam(op2, sizeInstList) ) {
					currIndex++;
					currInst = op1;				
				} else if ( op1 instanceof IntInstruction ) {
					if ( indexExpr != null )
						indexExpr = GecosUserInstructionFactory.mul(indexExpr, op1.copy());
					else
						indexExpr = op1.copy();
					currInst = op2;
				} else if ( op2 instanceof IntInstruction ) {
					if ( indexExpr != null )
						indexExpr = GecosUserInstructionFactory.mul(indexExpr, op2.copy());
					else
						indexExpr = op2.copy();
					currInst = op1;
				} 
			} else if ( (currInst instanceof SymbolInstruction) || (currInst instanceof IntInstruction) ) {
			//	if ( isSizeParam(currInst, sizeInstList) ) {
			//		currIndex++;
			//		if ( indexExpr == null )
			//			indexExpr = GecosUserInstructionFactory.Int(1);
			//	} else {
					if ( indexExpr != null )
						indexExpr = GecosUserInstructionFactory.mul(indexExpr, currInst.copy());
					else
						indexExpr = currInst.copy();
			//	}
				break;
			} else {
				currIndex = -1;
				break;
			}
		}
		if ( currIndex >= indexExprList.size() ) {
			for ( int i = indexExprList.size(); i <= currIndex; i++ ) {
					List<Instruction> l = new ArrayList<>();
					indexExprList.add(l);
			}
		}
		if ( currIndex != -1)
			indexExprList.get(currIndex).add(indexExpr);
		return currIndex;

	}

	private static boolean isSizeParam(Instruction inst, List<Instruction> sizeInstList ) {
		if ( inst instanceof SymbolInstruction ) {
			SymbolInstruction sInst = (SymbolInstruction)inst;
			for ( Instruction i : sizeInstList ) {
				if ( !(i instanceof SymbolInstruction) )
					continue;
				if ( ((SymbolInstruction)i).getSymbolName().equals((sInst.getSymbolName())) )
					return true;
			}
		} else if ( inst instanceof IntInstruction ) {
			IntInstruction sInst = (IntInstruction)inst;
			for ( Instruction i : sizeInstList ) {
				if ( !(i instanceof IntInstruction) )
					continue;
				if ( ((IntInstruction)i).getValue() == sInst.getValue() )
					return true;
			}
		} else {
			return false;
		}
		return false;
	}

	private static Instruction buildMultiDimArrayInstruction(Symbol arraySym, 
									List<List<Instruction>> indexExpr)  {	
		List<Instruction> indexInst = new ArrayList<>();
		for ( List<Instruction> list : indexExpr ) {
			Instruction currInst = list.get(0);
			if ( list.size() > 1 ) {
				list.remove(0);
				for ( Instruction inst : list ) {
					Instruction add = GecosUserInstructionFactory.add(currInst, inst);
					currInst = add;
				}
			}			
			indexInst.add(0, currInst);
		}
		
		SimpleArrayInstruction sArray = GecosUserInstructionFactory.array(arraySym, indexInst);
		return sArray;
    }  

	private static List<Instruction> getExpressionList(Instruction inst, boolean subtract) {
		List<Instruction> list = new ArrayList<>();
		if ( isAddSubInstruction(inst)  ) {
			GenericInstruction add = (GenericInstruction)inst;
			boolean isSub = isSubInstruction(inst);
			boolean firstOp = true;
			for ( Instruction op : add.getOperands() ) {
				if ( !firstOp )
					list.addAll(getExpressionList(op, isSub));
				else
					list.addAll(getExpressionList(op, false));
				firstOp = false;
			}
		} else {
			if (debug) System.out.println("Adding to list " + inst);
			if ( subtract )
				list.add(GecosUserInstructionFactory.mul(inst.copy(), GecosUserInstructionFactory.Int(-1)));
			else 
				list.add(inst);
		}
		return list;
	}

	private static boolean getSizeExprs(Type type, List<Instruction> list ) {
		if (type instanceof PtrType) {
			return true;
		} else if ( type instanceof ArrayType) {
			list.add( ((ArrayType)type).getSizeExpr().copy());
			return getSizeExprs( ((ArrayType) type).getBase(),list);
		} else {
			return false;
		}
	}

	private static boolean isAddSubInstruction(Instruction inst) {
		return ( (inst instanceof GenericInstruction) && 
			( (((GenericInstruction) inst).getName() == "add") || 
			  (((GenericInstruction) inst).getName() == "sub") ) );
	}
	
	private static boolean isSubInstruction(Instruction inst) {
		return ( (inst instanceof GenericInstruction) && 
					(((GenericInstruction) inst).getName() == "sub") );
	}

	public static Instruction distributeMul( Instruction inst ) {
		return inst;
	}

		
}