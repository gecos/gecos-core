
void f(int n, int a[][n]) {
    if(a[1][1] != 2)
        abort();
}

int main() {
    int x[2][4] = {{0}, {1,2,3,4}, {0}, {0}};

    f(4, x);

    exit (0);
}
