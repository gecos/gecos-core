/**
 */
package gecos.instrs.util;

import gecos.annotations.AnnotatedElement;

import gecos.core.GecosNode;
import gecos.core.ISymbolUse;
import gecos.core.ITypedElement;

import gecos.instrs.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see gecos.instrs.InstrsPackage
 * @generated
 */
public class InstrsSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static InstrsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InstrsSwitch() {
		if (modelPackage == null) {
			modelPackage = InstrsPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case InstrsPackage.INSTRS_VISITOR: {
				InstrsVisitor instrsVisitor = (InstrsVisitor)theEObject;
				T result = caseInstrsVisitor(instrsVisitor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.INSTRS_VISITABLE: {
				InstrsVisitable instrsVisitable = (InstrsVisitable)theEObject;
				T result = caseInstrsVisitable(instrsVisitable);
				if (result == null) result = caseGecosNode(instrsVisitable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.INSTRUCTION: {
				Instruction instruction = (Instruction)theEObject;
				T result = caseInstruction(instruction);
				if (result == null) result = caseAnnotatedElement(instruction);
				if (result == null) result = caseInstrsVisitable(instruction);
				if (result == null) result = caseITypedElement(instruction);
				if (result == null) result = caseGecosNode(instruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.DUMMY_INSTRUCTION: {
				DummyInstruction dummyInstruction = (DummyInstruction)theEObject;
				T result = caseDummyInstruction(dummyInstruction);
				if (result == null) result = caseInstruction(dummyInstruction);
				if (result == null) result = caseAnnotatedElement(dummyInstruction);
				if (result == null) result = caseInstrsVisitable(dummyInstruction);
				if (result == null) result = caseITypedElement(dummyInstruction);
				if (result == null) result = caseGecosNode(dummyInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.LABEL_INSTRUCTION: {
				LabelInstruction labelInstruction = (LabelInstruction)theEObject;
				T result = caseLabelInstruction(labelInstruction);
				if (result == null) result = caseInstruction(labelInstruction);
				if (result == null) result = caseAnnotatedElement(labelInstruction);
				if (result == null) result = caseInstrsVisitable(labelInstruction);
				if (result == null) result = caseITypedElement(labelInstruction);
				if (result == null) result = caseGecosNode(labelInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.SYMBOL_INSTRUCTION: {
				SymbolInstruction symbolInstruction = (SymbolInstruction)theEObject;
				T result = caseSymbolInstruction(symbolInstruction);
				if (result == null) result = caseInstruction(symbolInstruction);
				if (result == null) result = caseISymbolUse(symbolInstruction);
				if (result == null) result = caseAnnotatedElement(symbolInstruction);
				if (result == null) result = caseInstrsVisitable(symbolInstruction);
				if (result == null) result = caseITypedElement(symbolInstruction);
				if (result == null) result = caseGecosNode(symbolInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.NUMBERED_SYMBOL_INSTRUCTION: {
				NumberedSymbolInstruction numberedSymbolInstruction = (NumberedSymbolInstruction)theEObject;
				T result = caseNumberedSymbolInstruction(numberedSymbolInstruction);
				if (result == null) result = caseSymbolInstruction(numberedSymbolInstruction);
				if (result == null) result = caseInstruction(numberedSymbolInstruction);
				if (result == null) result = caseISymbolUse(numberedSymbolInstruction);
				if (result == null) result = caseAnnotatedElement(numberedSymbolInstruction);
				if (result == null) result = caseInstrsVisitable(numberedSymbolInstruction);
				if (result == null) result = caseITypedElement(numberedSymbolInstruction);
				if (result == null) result = caseGecosNode(numberedSymbolInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.SSA_DEF_SYMBOL: {
				SSADefSymbol ssaDefSymbol = (SSADefSymbol)theEObject;
				T result = caseSSADefSymbol(ssaDefSymbol);
				if (result == null) result = caseNumberedSymbolInstruction(ssaDefSymbol);
				if (result == null) result = caseSymbolInstruction(ssaDefSymbol);
				if (result == null) result = caseInstruction(ssaDefSymbol);
				if (result == null) result = caseISymbolUse(ssaDefSymbol);
				if (result == null) result = caseAnnotatedElement(ssaDefSymbol);
				if (result == null) result = caseInstrsVisitable(ssaDefSymbol);
				if (result == null) result = caseITypedElement(ssaDefSymbol);
				if (result == null) result = caseGecosNode(ssaDefSymbol);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.SSA_USE_SYMBOL: {
				SSAUseSymbol ssaUseSymbol = (SSAUseSymbol)theEObject;
				T result = caseSSAUseSymbol(ssaUseSymbol);
				if (result == null) result = caseNumberedSymbolInstruction(ssaUseSymbol);
				if (result == null) result = caseSymbolInstruction(ssaUseSymbol);
				if (result == null) result = caseInstruction(ssaUseSymbol);
				if (result == null) result = caseISymbolUse(ssaUseSymbol);
				if (result == null) result = caseAnnotatedElement(ssaUseSymbol);
				if (result == null) result = caseInstrsVisitable(ssaUseSymbol);
				if (result == null) result = caseITypedElement(ssaUseSymbol);
				if (result == null) result = caseGecosNode(ssaUseSymbol);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.SIZEOF_INSTRUCTION: {
				SizeofInstruction sizeofInstruction = (SizeofInstruction)theEObject;
				T result = caseSizeofInstruction(sizeofInstruction);
				if (result == null) result = caseInstruction(sizeofInstruction);
				if (result == null) result = caseAnnotatedElement(sizeofInstruction);
				if (result == null) result = caseInstrsVisitable(sizeofInstruction);
				if (result == null) result = caseITypedElement(sizeofInstruction);
				if (result == null) result = caseGecosNode(sizeofInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.SIZEOF_TYPE_INSTRUCTION: {
				SizeofTypeInstruction sizeofTypeInstruction = (SizeofTypeInstruction)theEObject;
				T result = caseSizeofTypeInstruction(sizeofTypeInstruction);
				if (result == null) result = caseSizeofInstruction(sizeofTypeInstruction);
				if (result == null) result = caseInstruction(sizeofTypeInstruction);
				if (result == null) result = caseAnnotatedElement(sizeofTypeInstruction);
				if (result == null) result = caseInstrsVisitable(sizeofTypeInstruction);
				if (result == null) result = caseITypedElement(sizeofTypeInstruction);
				if (result == null) result = caseGecosNode(sizeofTypeInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.SIZEOF_VALUE_INSTRUCTION: {
				SizeofValueInstruction sizeofValueInstruction = (SizeofValueInstruction)theEObject;
				T result = caseSizeofValueInstruction(sizeofValueInstruction);
				if (result == null) result = caseExprComponent(sizeofValueInstruction);
				if (result == null) result = caseSizeofInstruction(sizeofValueInstruction);
				if (result == null) result = caseComponentInstruction(sizeofValueInstruction);
				if (result == null) result = caseComplexInstruction(sizeofValueInstruction);
				if (result == null) result = caseInstruction(sizeofValueInstruction);
				if (result == null) result = caseAnnotatedElement(sizeofValueInstruction);
				if (result == null) result = caseInstrsVisitable(sizeofValueInstruction);
				if (result == null) result = caseITypedElement(sizeofValueInstruction);
				if (result == null) result = caseGecosNode(sizeofValueInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.CONSTANT_INSTRUCTION: {
				ConstantInstruction constantInstruction = (ConstantInstruction)theEObject;
				T result = caseConstantInstruction(constantInstruction);
				if (result == null) result = caseInstruction(constantInstruction);
				if (result == null) result = caseAnnotatedElement(constantInstruction);
				if (result == null) result = caseInstrsVisitable(constantInstruction);
				if (result == null) result = caseITypedElement(constantInstruction);
				if (result == null) result = caseGecosNode(constantInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.INT_INSTRUCTION: {
				IntInstruction intInstruction = (IntInstruction)theEObject;
				T result = caseIntInstruction(intInstruction);
				if (result == null) result = caseConstantInstruction(intInstruction);
				if (result == null) result = caseInstruction(intInstruction);
				if (result == null) result = caseAnnotatedElement(intInstruction);
				if (result == null) result = caseInstrsVisitable(intInstruction);
				if (result == null) result = caseITypedElement(intInstruction);
				if (result == null) result = caseGecosNode(intInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.FLOAT_INSTRUCTION: {
				FloatInstruction floatInstruction = (FloatInstruction)theEObject;
				T result = caseFloatInstruction(floatInstruction);
				if (result == null) result = caseConstantInstruction(floatInstruction);
				if (result == null) result = caseInstruction(floatInstruction);
				if (result == null) result = caseAnnotatedElement(floatInstruction);
				if (result == null) result = caseInstrsVisitable(floatInstruction);
				if (result == null) result = caseITypedElement(floatInstruction);
				if (result == null) result = caseGecosNode(floatInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.STRING_INSTRUCTION: {
				StringInstruction stringInstruction = (StringInstruction)theEObject;
				T result = caseStringInstruction(stringInstruction);
				if (result == null) result = caseConstantInstruction(stringInstruction);
				if (result == null) result = caseInstruction(stringInstruction);
				if (result == null) result = caseAnnotatedElement(stringInstruction);
				if (result == null) result = caseInstrsVisitable(stringInstruction);
				if (result == null) result = caseITypedElement(stringInstruction);
				if (result == null) result = caseGecosNode(stringInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.COMPLEX_INSTRUCTION: {
				ComplexInstruction complexInstruction = (ComplexInstruction)theEObject;
				T result = caseComplexInstruction(complexInstruction);
				if (result == null) result = caseInstruction(complexInstruction);
				if (result == null) result = caseAnnotatedElement(complexInstruction);
				if (result == null) result = caseInstrsVisitable(complexInstruction);
				if (result == null) result = caseITypedElement(complexInstruction);
				if (result == null) result = caseGecosNode(complexInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.CHILDREN_LIST_INSTRUCTION: {
				ChildrenListInstruction childrenListInstruction = (ChildrenListInstruction)theEObject;
				T result = caseChildrenListInstruction(childrenListInstruction);
				if (result == null) result = caseComplexInstruction(childrenListInstruction);
				if (result == null) result = caseInstruction(childrenListInstruction);
				if (result == null) result = caseAnnotatedElement(childrenListInstruction);
				if (result == null) result = caseInstrsVisitable(childrenListInstruction);
				if (result == null) result = caseITypedElement(childrenListInstruction);
				if (result == null) result = caseGecosNode(childrenListInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.ARRAY_VALUE_INSTRUCTION: {
				ArrayValueInstruction arrayValueInstruction = (ArrayValueInstruction)theEObject;
				T result = caseArrayValueInstruction(arrayValueInstruction);
				if (result == null) result = caseChildrenListInstruction(arrayValueInstruction);
				if (result == null) result = caseComplexInstruction(arrayValueInstruction);
				if (result == null) result = caseInstruction(arrayValueInstruction);
				if (result == null) result = caseAnnotatedElement(arrayValueInstruction);
				if (result == null) result = caseInstrsVisitable(arrayValueInstruction);
				if (result == null) result = caseITypedElement(arrayValueInstruction);
				if (result == null) result = caseGecosNode(arrayValueInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.PHI_INSTRUCTION: {
				PhiInstruction phiInstruction = (PhiInstruction)theEObject;
				T result = casePhiInstruction(phiInstruction);
				if (result == null) result = caseChildrenListInstruction(phiInstruction);
				if (result == null) result = caseComplexInstruction(phiInstruction);
				if (result == null) result = caseInstruction(phiInstruction);
				if (result == null) result = caseAnnotatedElement(phiInstruction);
				if (result == null) result = caseInstrsVisitable(phiInstruction);
				if (result == null) result = caseITypedElement(phiInstruction);
				if (result == null) result = caseGecosNode(phiInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.COMPONENT_INSTRUCTION: {
				ComponentInstruction componentInstruction = (ComponentInstruction)theEObject;
				T result = caseComponentInstruction(componentInstruction);
				if (result == null) result = caseComplexInstruction(componentInstruction);
				if (result == null) result = caseInstruction(componentInstruction);
				if (result == null) result = caseAnnotatedElement(componentInstruction);
				if (result == null) result = caseInstrsVisitable(componentInstruction);
				if (result == null) result = caseITypedElement(componentInstruction);
				if (result == null) result = caseGecosNode(componentInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.SET_INSTRUCTION: {
				SetInstruction setInstruction = (SetInstruction)theEObject;
				T result = caseSetInstruction(setInstruction);
				if (result == null) result = caseComponentInstruction(setInstruction);
				if (result == null) result = caseComplexInstruction(setInstruction);
				if (result == null) result = caseInstruction(setInstruction);
				if (result == null) result = caseAnnotatedElement(setInstruction);
				if (result == null) result = caseInstrsVisitable(setInstruction);
				if (result == null) result = caseITypedElement(setInstruction);
				if (result == null) result = caseGecosNode(setInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.ADDRESS_COMPONENT: {
				AddressComponent addressComponent = (AddressComponent)theEObject;
				T result = caseAddressComponent(addressComponent);
				if (result == null) result = caseComponentInstruction(addressComponent);
				if (result == null) result = caseComplexInstruction(addressComponent);
				if (result == null) result = caseInstruction(addressComponent);
				if (result == null) result = caseAnnotatedElement(addressComponent);
				if (result == null) result = caseInstrsVisitable(addressComponent);
				if (result == null) result = caseITypedElement(addressComponent);
				if (result == null) result = caseGecosNode(addressComponent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.ADDRESS_INSTRUCTION: {
				AddressInstruction addressInstruction = (AddressInstruction)theEObject;
				T result = caseAddressInstruction(addressInstruction);
				if (result == null) result = caseAddressComponent(addressInstruction);
				if (result == null) result = caseComponentInstruction(addressInstruction);
				if (result == null) result = caseComplexInstruction(addressInstruction);
				if (result == null) result = caseInstruction(addressInstruction);
				if (result == null) result = caseAnnotatedElement(addressInstruction);
				if (result == null) result = caseInstrsVisitable(addressInstruction);
				if (result == null) result = caseITypedElement(addressInstruction);
				if (result == null) result = caseGecosNode(addressInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.INDIR_INSTRUCTION: {
				IndirInstruction indirInstruction = (IndirInstruction)theEObject;
				T result = caseIndirInstruction(indirInstruction);
				if (result == null) result = caseAddressComponent(indirInstruction);
				if (result == null) result = caseComponentInstruction(indirInstruction);
				if (result == null) result = caseComplexInstruction(indirInstruction);
				if (result == null) result = caseInstruction(indirInstruction);
				if (result == null) result = caseAnnotatedElement(indirInstruction);
				if (result == null) result = caseInstrsVisitable(indirInstruction);
				if (result == null) result = caseITypedElement(indirInstruction);
				if (result == null) result = caseGecosNode(indirInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.CALL_INSTRUCTION: {
				CallInstruction callInstruction = (CallInstruction)theEObject;
				T result = caseCallInstruction(callInstruction);
				if (result == null) result = caseAddressComponent(callInstruction);
				if (result == null) result = caseComponentInstruction(callInstruction);
				if (result == null) result = caseComplexInstruction(callInstruction);
				if (result == null) result = caseInstruction(callInstruction);
				if (result == null) result = caseAnnotatedElement(callInstruction);
				if (result == null) result = caseInstrsVisitable(callInstruction);
				if (result == null) result = caseITypedElement(callInstruction);
				if (result == null) result = caseGecosNode(callInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.METHOD_CALL_INSTRUCTION: {
				MethodCallInstruction methodCallInstruction = (MethodCallInstruction)theEObject;
				T result = caseMethodCallInstruction(methodCallInstruction);
				if (result == null) result = caseAddressComponent(methodCallInstruction);
				if (result == null) result = caseComponentInstruction(methodCallInstruction);
				if (result == null) result = caseComplexInstruction(methodCallInstruction);
				if (result == null) result = caseInstruction(methodCallInstruction);
				if (result == null) result = caseAnnotatedElement(methodCallInstruction);
				if (result == null) result = caseInstrsVisitable(methodCallInstruction);
				if (result == null) result = caseITypedElement(methodCallInstruction);
				if (result == null) result = caseGecosNode(methodCallInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.ARRAY_INSTRUCTION: {
				ArrayInstruction arrayInstruction = (ArrayInstruction)theEObject;
				T result = caseArrayInstruction(arrayInstruction);
				if (result == null) result = caseComponentInstruction(arrayInstruction);
				if (result == null) result = caseComplexInstruction(arrayInstruction);
				if (result == null) result = caseInstruction(arrayInstruction);
				if (result == null) result = caseAnnotatedElement(arrayInstruction);
				if (result == null) result = caseInstrsVisitable(arrayInstruction);
				if (result == null) result = caseITypedElement(arrayInstruction);
				if (result == null) result = caseGecosNode(arrayInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.SIMPLE_ARRAY_INSTRUCTION: {
				SimpleArrayInstruction simpleArrayInstruction = (SimpleArrayInstruction)theEObject;
				T result = caseSimpleArrayInstruction(simpleArrayInstruction);
				if (result == null) result = caseArrayInstruction(simpleArrayInstruction);
				if (result == null) result = caseISymbolUse(simpleArrayInstruction);
				if (result == null) result = caseComponentInstruction(simpleArrayInstruction);
				if (result == null) result = caseComplexInstruction(simpleArrayInstruction);
				if (result == null) result = caseInstruction(simpleArrayInstruction);
				if (result == null) result = caseAnnotatedElement(simpleArrayInstruction);
				if (result == null) result = caseInstrsVisitable(simpleArrayInstruction);
				if (result == null) result = caseITypedElement(simpleArrayInstruction);
				if (result == null) result = caseGecosNode(simpleArrayInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.RANGE_ARRAY_INSTRUCTION: {
				RangeArrayInstruction rangeArrayInstruction = (RangeArrayInstruction)theEObject;
				T result = caseRangeArrayInstruction(rangeArrayInstruction);
				if (result == null) result = caseArrayInstruction(rangeArrayInstruction);
				if (result == null) result = caseISymbolUse(rangeArrayInstruction);
				if (result == null) result = caseComponentInstruction(rangeArrayInstruction);
				if (result == null) result = caseComplexInstruction(rangeArrayInstruction);
				if (result == null) result = caseInstruction(rangeArrayInstruction);
				if (result == null) result = caseAnnotatedElement(rangeArrayInstruction);
				if (result == null) result = caseInstrsVisitable(rangeArrayInstruction);
				if (result == null) result = caseITypedElement(rangeArrayInstruction);
				if (result == null) result = caseGecosNode(rangeArrayInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.BRANCH_INSTRUCTION: {
				BranchInstruction branchInstruction = (BranchInstruction)theEObject;
				T result = caseBranchInstruction(branchInstruction);
				if (result == null) result = caseComponentInstruction(branchInstruction);
				if (result == null) result = caseComplexInstruction(branchInstruction);
				if (result == null) result = caseInstruction(branchInstruction);
				if (result == null) result = caseAnnotatedElement(branchInstruction);
				if (result == null) result = caseInstrsVisitable(branchInstruction);
				if (result == null) result = caseITypedElement(branchInstruction);
				if (result == null) result = caseGecosNode(branchInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.COND_INSTRUCTION: {
				CondInstruction condInstruction = (CondInstruction)theEObject;
				T result = caseCondInstruction(condInstruction);
				if (result == null) result = caseBranchInstruction(condInstruction);
				if (result == null) result = caseComponentInstruction(condInstruction);
				if (result == null) result = caseComplexInstruction(condInstruction);
				if (result == null) result = caseInstruction(condInstruction);
				if (result == null) result = caseAnnotatedElement(condInstruction);
				if (result == null) result = caseInstrsVisitable(condInstruction);
				if (result == null) result = caseITypedElement(condInstruction);
				if (result == null) result = caseGecosNode(condInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.BREAK_INSTRUCTION: {
				BreakInstruction breakInstruction = (BreakInstruction)theEObject;
				T result = caseBreakInstruction(breakInstruction);
				if (result == null) result = caseBranchInstruction(breakInstruction);
				if (result == null) result = caseComponentInstruction(breakInstruction);
				if (result == null) result = caseComplexInstruction(breakInstruction);
				if (result == null) result = caseInstruction(breakInstruction);
				if (result == null) result = caseAnnotatedElement(breakInstruction);
				if (result == null) result = caseInstrsVisitable(breakInstruction);
				if (result == null) result = caseITypedElement(breakInstruction);
				if (result == null) result = caseGecosNode(breakInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.CONTINUE_INSTRUCTION: {
				ContinueInstruction continueInstruction = (ContinueInstruction)theEObject;
				T result = caseContinueInstruction(continueInstruction);
				if (result == null) result = caseBranchInstruction(continueInstruction);
				if (result == null) result = caseComponentInstruction(continueInstruction);
				if (result == null) result = caseComplexInstruction(continueInstruction);
				if (result == null) result = caseInstruction(continueInstruction);
				if (result == null) result = caseAnnotatedElement(continueInstruction);
				if (result == null) result = caseInstrsVisitable(continueInstruction);
				if (result == null) result = caseITypedElement(continueInstruction);
				if (result == null) result = caseGecosNode(continueInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.GOTO_INSTRUCTION: {
				GotoInstruction gotoInstruction = (GotoInstruction)theEObject;
				T result = caseGotoInstruction(gotoInstruction);
				if (result == null) result = caseBranchInstruction(gotoInstruction);
				if (result == null) result = caseComponentInstruction(gotoInstruction);
				if (result == null) result = caseComplexInstruction(gotoInstruction);
				if (result == null) result = caseInstruction(gotoInstruction);
				if (result == null) result = caseAnnotatedElement(gotoInstruction);
				if (result == null) result = caseInstrsVisitable(gotoInstruction);
				if (result == null) result = caseITypedElement(gotoInstruction);
				if (result == null) result = caseGecosNode(gotoInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.EXPR_COMPONENT: {
				ExprComponent exprComponent = (ExprComponent)theEObject;
				T result = caseExprComponent(exprComponent);
				if (result == null) result = caseComponentInstruction(exprComponent);
				if (result == null) result = caseComplexInstruction(exprComponent);
				if (result == null) result = caseInstruction(exprComponent);
				if (result == null) result = caseAnnotatedElement(exprComponent);
				if (result == null) result = caseInstrsVisitable(exprComponent);
				if (result == null) result = caseITypedElement(exprComponent);
				if (result == null) result = caseGecosNode(exprComponent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.RET_INSTRUCTION: {
				RetInstruction retInstruction = (RetInstruction)theEObject;
				T result = caseRetInstruction(retInstruction);
				if (result == null) result = caseExprComponent(retInstruction);
				if (result == null) result = caseComponentInstruction(retInstruction);
				if (result == null) result = caseComplexInstruction(retInstruction);
				if (result == null) result = caseInstruction(retInstruction);
				if (result == null) result = caseAnnotatedElement(retInstruction);
				if (result == null) result = caseInstrsVisitable(retInstruction);
				if (result == null) result = caseITypedElement(retInstruction);
				if (result == null) result = caseGecosNode(retInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.CONVERT_INSTRUCTION: {
				ConvertInstruction convertInstruction = (ConvertInstruction)theEObject;
				T result = caseConvertInstruction(convertInstruction);
				if (result == null) result = caseExprComponent(convertInstruction);
				if (result == null) result = caseComponentInstruction(convertInstruction);
				if (result == null) result = caseComplexInstruction(convertInstruction);
				if (result == null) result = caseInstruction(convertInstruction);
				if (result == null) result = caseAnnotatedElement(convertInstruction);
				if (result == null) result = caseInstrsVisitable(convertInstruction);
				if (result == null) result = caseITypedElement(convertInstruction);
				if (result == null) result = caseGecosNode(convertInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.FIELD_INSTRUCTION: {
				FieldInstruction fieldInstruction = (FieldInstruction)theEObject;
				T result = caseFieldInstruction(fieldInstruction);
				if (result == null) result = caseExprComponent(fieldInstruction);
				if (result == null) result = caseComponentInstruction(fieldInstruction);
				if (result == null) result = caseComplexInstruction(fieldInstruction);
				if (result == null) result = caseInstruction(fieldInstruction);
				if (result == null) result = caseAnnotatedElement(fieldInstruction);
				if (result == null) result = caseInstrsVisitable(fieldInstruction);
				if (result == null) result = caseITypedElement(fieldInstruction);
				if (result == null) result = caseGecosNode(fieldInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.ENUMERATOR_INSTRUCTION: {
				EnumeratorInstruction enumeratorInstruction = (EnumeratorInstruction)theEObject;
				T result = caseEnumeratorInstruction(enumeratorInstruction);
				if (result == null) result = caseInstruction(enumeratorInstruction);
				if (result == null) result = caseAnnotatedElement(enumeratorInstruction);
				if (result == null) result = caseInstrsVisitable(enumeratorInstruction);
				if (result == null) result = caseITypedElement(enumeratorInstruction);
				if (result == null) result = caseGecosNode(enumeratorInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.RANGE_INSTRUCTION: {
				RangeInstruction rangeInstruction = (RangeInstruction)theEObject;
				T result = caseRangeInstruction(rangeInstruction);
				if (result == null) result = caseExprComponent(rangeInstruction);
				if (result == null) result = caseComponentInstruction(rangeInstruction);
				if (result == null) result = caseComplexInstruction(rangeInstruction);
				if (result == null) result = caseInstruction(rangeInstruction);
				if (result == null) result = caseAnnotatedElement(rangeInstruction);
				if (result == null) result = caseInstrsVisitable(rangeInstruction);
				if (result == null) result = caseITypedElement(rangeInstruction);
				if (result == null) result = caseGecosNode(rangeInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.CASE_INSTRUCTION: {
				CaseInstruction caseInstruction = (CaseInstruction)theEObject;
				T result = caseCaseInstruction(caseInstruction);
				if (result == null) result = caseExprComponent(caseInstruction);
				if (result == null) result = caseComponentInstruction(caseInstruction);
				if (result == null) result = caseComplexInstruction(caseInstruction);
				if (result == null) result = caseInstruction(caseInstruction);
				if (result == null) result = caseAnnotatedElement(caseInstruction);
				if (result == null) result = caseInstrsVisitable(caseInstruction);
				if (result == null) result = caseITypedElement(caseInstruction);
				if (result == null) result = caseGecosNode(caseInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.GENERIC_INSTRUCTION: {
				GenericInstruction genericInstruction = (GenericInstruction)theEObject;
				T result = caseGenericInstruction(genericInstruction);
				if (result == null) result = caseChildrenListInstruction(genericInstruction);
				if (result == null) result = caseComplexInstruction(genericInstruction);
				if (result == null) result = caseInstruction(genericInstruction);
				if (result == null) result = caseAnnotatedElement(genericInstruction);
				if (result == null) result = caseInstrsVisitable(genericInstruction);
				if (result == null) result = caseITypedElement(genericInstruction);
				if (result == null) result = caseGecosNode(genericInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.ARITHMETIC_INSTRUCTION: {
				ArithmeticInstruction arithmeticInstruction = (ArithmeticInstruction)theEObject;
				T result = caseArithmeticInstruction(arithmeticInstruction);
				if (result == null) result = caseGenericInstruction(arithmeticInstruction);
				if (result == null) result = caseChildrenListInstruction(arithmeticInstruction);
				if (result == null) result = caseComplexInstruction(arithmeticInstruction);
				if (result == null) result = caseInstruction(arithmeticInstruction);
				if (result == null) result = caseAnnotatedElement(arithmeticInstruction);
				if (result == null) result = caseInstrsVisitable(arithmeticInstruction);
				if (result == null) result = caseITypedElement(arithmeticInstruction);
				if (result == null) result = caseGecosNode(arithmeticInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.COMPARISON_INSTRUCTION: {
				ComparisonInstruction comparisonInstruction = (ComparisonInstruction)theEObject;
				T result = caseComparisonInstruction(comparisonInstruction);
				if (result == null) result = caseGenericInstruction(comparisonInstruction);
				if (result == null) result = caseChildrenListInstruction(comparisonInstruction);
				if (result == null) result = caseComplexInstruction(comparisonInstruction);
				if (result == null) result = caseInstruction(comparisonInstruction);
				if (result == null) result = caseAnnotatedElement(comparisonInstruction);
				if (result == null) result = caseInstrsVisitable(comparisonInstruction);
				if (result == null) result = caseITypedElement(comparisonInstruction);
				if (result == null) result = caseGecosNode(comparisonInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.LOGICAL_INSTRUCTION: {
				LogicalInstruction logicalInstruction = (LogicalInstruction)theEObject;
				T result = caseLogicalInstruction(logicalInstruction);
				if (result == null) result = caseGenericInstruction(logicalInstruction);
				if (result == null) result = caseChildrenListInstruction(logicalInstruction);
				if (result == null) result = caseComplexInstruction(logicalInstruction);
				if (result == null) result = caseInstruction(logicalInstruction);
				if (result == null) result = caseAnnotatedElement(logicalInstruction);
				if (result == null) result = caseInstrsVisitable(logicalInstruction);
				if (result == null) result = caseITypedElement(logicalInstruction);
				if (result == null) result = caseGecosNode(logicalInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.BITWISE_INSTRUCTION: {
				BitwiseInstruction bitwiseInstruction = (BitwiseInstruction)theEObject;
				T result = caseBitwiseInstruction(bitwiseInstruction);
				if (result == null) result = caseGenericInstruction(bitwiseInstruction);
				if (result == null) result = caseChildrenListInstruction(bitwiseInstruction);
				if (result == null) result = caseComplexInstruction(bitwiseInstruction);
				if (result == null) result = caseInstruction(bitwiseInstruction);
				if (result == null) result = caseAnnotatedElement(bitwiseInstruction);
				if (result == null) result = caseInstrsVisitable(bitwiseInstruction);
				if (result == null) result = caseITypedElement(bitwiseInstruction);
				if (result == null) result = caseGecosNode(bitwiseInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.SIMD_INSTRUCTION: {
				SimdInstruction simdInstruction = (SimdInstruction)theEObject;
				T result = caseSimdInstruction(simdInstruction);
				if (result == null) result = caseChildrenListInstruction(simdInstruction);
				if (result == null) result = caseComplexInstruction(simdInstruction);
				if (result == null) result = caseInstruction(simdInstruction);
				if (result == null) result = caseAnnotatedElement(simdInstruction);
				if (result == null) result = caseInstrsVisitable(simdInstruction);
				if (result == null) result = caseITypedElement(simdInstruction);
				if (result == null) result = caseGecosNode(simdInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.SIMD_GENERIC_INSTRUCTION: {
				SimdGenericInstruction simdGenericInstruction = (SimdGenericInstruction)theEObject;
				T result = caseSimdGenericInstruction(simdGenericInstruction);
				if (result == null) result = caseGenericInstruction(simdGenericInstruction);
				if (result == null) result = caseSimdInstruction(simdGenericInstruction);
				if (result == null) result = caseChildrenListInstruction(simdGenericInstruction);
				if (result == null) result = caseComplexInstruction(simdGenericInstruction);
				if (result == null) result = caseInstruction(simdGenericInstruction);
				if (result == null) result = caseAnnotatedElement(simdGenericInstruction);
				if (result == null) result = caseInstrsVisitable(simdGenericInstruction);
				if (result == null) result = caseITypedElement(simdGenericInstruction);
				if (result == null) result = caseGecosNode(simdGenericInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.SIMD_PACK_INSTRUCTION: {
				SimdPackInstruction simdPackInstruction = (SimdPackInstruction)theEObject;
				T result = caseSimdPackInstruction(simdPackInstruction);
				if (result == null) result = caseSimdInstruction(simdPackInstruction);
				if (result == null) result = caseChildrenListInstruction(simdPackInstruction);
				if (result == null) result = caseComplexInstruction(simdPackInstruction);
				if (result == null) result = caseInstruction(simdPackInstruction);
				if (result == null) result = caseAnnotatedElement(simdPackInstruction);
				if (result == null) result = caseInstrsVisitable(simdPackInstruction);
				if (result == null) result = caseITypedElement(simdPackInstruction);
				if (result == null) result = caseGecosNode(simdPackInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.SIMD_EXTRACT_INSTRUCTION: {
				SimdExtractInstruction simdExtractInstruction = (SimdExtractInstruction)theEObject;
				T result = caseSimdExtractInstruction(simdExtractInstruction);
				if (result == null) result = caseSimdInstruction(simdExtractInstruction);
				if (result == null) result = caseChildrenListInstruction(simdExtractInstruction);
				if (result == null) result = caseComplexInstruction(simdExtractInstruction);
				if (result == null) result = caseInstruction(simdExtractInstruction);
				if (result == null) result = caseAnnotatedElement(simdExtractInstruction);
				if (result == null) result = caseInstrsVisitable(simdExtractInstruction);
				if (result == null) result = caseITypedElement(simdExtractInstruction);
				if (result == null) result = caseGecosNode(simdExtractInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.SIMD_SHUFFLE_INSTRUCTION: {
				SimdShuffleInstruction simdShuffleInstruction = (SimdShuffleInstruction)theEObject;
				T result = caseSimdShuffleInstruction(simdShuffleInstruction);
				if (result == null) result = caseSimdInstruction(simdShuffleInstruction);
				if (result == null) result = caseChildrenListInstruction(simdShuffleInstruction);
				if (result == null) result = caseComplexInstruction(simdShuffleInstruction);
				if (result == null) result = caseInstruction(simdShuffleInstruction);
				if (result == null) result = caseAnnotatedElement(simdShuffleInstruction);
				if (result == null) result = caseInstrsVisitable(simdShuffleInstruction);
				if (result == null) result = caseITypedElement(simdShuffleInstruction);
				if (result == null) result = caseGecosNode(simdShuffleInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case InstrsPackage.SIMD_EXPAND_INSTRUCTION: {
				SimdExpandInstruction simdExpandInstruction = (SimdExpandInstruction)theEObject;
				T result = caseSimdExpandInstruction(simdExpandInstruction);
				if (result == null) result = caseSimdInstruction(simdExpandInstruction);
				if (result == null) result = caseChildrenListInstruction(simdExpandInstruction);
				if (result == null) result = caseComplexInstruction(simdExpandInstruction);
				if (result == null) result = caseInstruction(simdExpandInstruction);
				if (result == null) result = caseAnnotatedElement(simdExpandInstruction);
				if (result == null) result = caseInstrsVisitable(simdExpandInstruction);
				if (result == null) result = caseITypedElement(simdExpandInstruction);
				if (result == null) result = caseGecosNode(simdExpandInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visitor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visitor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInstrsVisitor(InstrsVisitor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInstrsVisitable(InstrsVisitable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInstruction(Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dummy Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dummy Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDummyInstruction(DummyInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Label Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Label Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLabelInstruction(LabelInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Symbol Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Symbol Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSymbolInstruction(SymbolInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Numbered Symbol Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Numbered Symbol Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNumberedSymbolInstruction(NumberedSymbolInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SSA Def Symbol</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SSA Def Symbol</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSSADefSymbol(SSADefSymbol object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SSA Use Symbol</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SSA Use Symbol</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSSAUseSymbol(SSAUseSymbol object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sizeof Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sizeof Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSizeofInstruction(SizeofInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sizeof Type Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sizeof Type Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSizeofTypeInstruction(SizeofTypeInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sizeof Value Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sizeof Value Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSizeofValueInstruction(SizeofValueInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constant Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constant Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstantInstruction(ConstantInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Int Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIntInstruction(IntInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Float Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Float Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFloatInstruction(FloatInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStringInstruction(StringInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Complex Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Complex Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComplexInstruction(ComplexInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Children List Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Children List Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChildrenListInstruction(ChildrenListInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Array Value Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Array Value Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArrayValueInstruction(ArrayValueInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Phi Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Phi Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhiInstruction(PhiInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentInstruction(ComponentInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Set Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Set Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSetInstruction(SetInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Address Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Address Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAddressComponent(AddressComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Address Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Address Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAddressInstruction(AddressInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Indir Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Indir Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIndirInstruction(IndirInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Call Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Call Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCallInstruction(CallInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Method Call Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Method Call Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMethodCallInstruction(MethodCallInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Array Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Array Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArrayInstruction(ArrayInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Array Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Array Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimpleArrayInstruction(SimpleArrayInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Range Array Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Range Array Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRangeArrayInstruction(RangeArrayInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Branch Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Branch Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBranchInstruction(BranchInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cond Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cond Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCondInstruction(CondInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Break Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Break Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBreakInstruction(BreakInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Continue Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Continue Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContinueInstruction(ContinueInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Goto Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Goto Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGotoInstruction(GotoInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Expr Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expr Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExprComponent(ExprComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ret Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ret Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRetInstruction(RetInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Convert Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Convert Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConvertInstruction(ConvertInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Field Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Field Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFieldInstruction(FieldInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enumerator Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enumerator Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnumeratorInstruction(EnumeratorInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Range Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Range Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRangeInstruction(RangeInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Case Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Case Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCaseInstruction(CaseInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generic Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generic Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGenericInstruction(GenericInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Arithmetic Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Arithmetic Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArithmeticInstruction(ArithmeticInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Comparison Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Comparison Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComparisonInstruction(ComparisonInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Logical Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Logical Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLogicalInstruction(LogicalInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Bitwise Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bitwise Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBitwiseInstruction(BitwiseInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simd Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simd Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimdInstruction(SimdInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simd Generic Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simd Generic Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimdGenericInstruction(SimdGenericInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simd Pack Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simd Pack Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimdPackInstruction(SimdPackInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simd Extract Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simd Extract Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimdExtractInstruction(SimdExtractInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simd Shuffle Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simd Shuffle Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimdShuffleInstruction(SimdShuffleInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simd Expand Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simd Expand Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimdExpandInstruction(SimdExpandInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Gecos Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Gecos Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGecosNode(GecosNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotatedElement(AnnotatedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ITyped Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ITyped Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseITypedElement(ITypedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISymbol Use</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISymbol Use</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISymbolUse(ISymbolUse object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //InstrsSwitch
