/**
 */
package gecos.dag.impl;

import gecos.dag.DAGInPort;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGOutPort;
import gecos.dag.DAGPatternNode;
import gecos.dag.DagPackage;
import gecos.dag.DagVisitor;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DAG Pattern Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.impl.DAGPatternNodeImpl#getPattern <em>Pattern</em>}</li>
 *   <li>{@link gecos.dag.impl.DAGPatternNodeImpl#getInputsMap <em>Inputs Map</em>}</li>
 *   <li>{@link gecos.dag.impl.DAGPatternNodeImpl#getOutputsMap <em>Outputs Map</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DAGPatternNodeImpl extends DAGOpNodeImpl implements DAGPatternNode {
	/**
	 * The cached value of the '{@link #getPattern() <em>Pattern</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPattern()
	 * @generated
	 * @ordered
	 */
	protected DAGInstruction pattern;

	/**
	 * The cached value of the '{@link #getInputsMap() <em>Inputs Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputsMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<DAGInPort, DAGInPort> inputsMap;

	/**
	 * The cached value of the '{@link #getOutputsMap() <em>Outputs Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputsMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<DAGOutPort, DAGOutPort> outputsMap;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DAGPatternNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DagPackage.Literals.DAG_PATTERN_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGInstruction getPattern() {
		return pattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPattern(DAGInstruction newPattern, NotificationChain msgs) {
		DAGInstruction oldPattern = pattern;
		pattern = newPattern;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DagPackage.DAG_PATTERN_NODE__PATTERN, oldPattern, newPattern);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPattern(DAGInstruction newPattern) {
		if (newPattern != pattern) {
			NotificationChain msgs = null;
			if (pattern != null)
				msgs = ((InternalEObject)pattern).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DagPackage.DAG_PATTERN_NODE__PATTERN, null, msgs);
			if (newPattern != null)
				msgs = ((InternalEObject)newPattern).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DagPackage.DAG_PATTERN_NODE__PATTERN, null, msgs);
			msgs = basicSetPattern(newPattern, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DagPackage.DAG_PATTERN_NODE__PATTERN, newPattern, newPattern));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<DAGInPort, DAGInPort> getInputsMap() {
		if (inputsMap == null) {
			inputsMap = new EcoreEMap<DAGInPort,DAGInPort>(DagPackage.Literals.IN_PORT_MAP, InPortMapImpl.class, this, DagPackage.DAG_PATTERN_NODE__INPUTS_MAP);
		}
		return inputsMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<DAGOutPort, DAGOutPort> getOutputsMap() {
		if (outputsMap == null) {
			outputsMap = new EcoreEMap<DAGOutPort,DAGOutPort>(DagPackage.Literals.OUT_PORT_MAP, OutPortMapImpl.class, this, DagPackage.DAG_PATTERN_NODE__OUTPUTS_MAP);
		}
		return outputsMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final DagVisitor visitor) {
		visitor.visitDAGPatternNode(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DagPackage.DAG_PATTERN_NODE__PATTERN:
				return basicSetPattern(null, msgs);
			case DagPackage.DAG_PATTERN_NODE__INPUTS_MAP:
				return ((InternalEList<?>)getInputsMap()).basicRemove(otherEnd, msgs);
			case DagPackage.DAG_PATTERN_NODE__OUTPUTS_MAP:
				return ((InternalEList<?>)getOutputsMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DagPackage.DAG_PATTERN_NODE__PATTERN:
				return getPattern();
			case DagPackage.DAG_PATTERN_NODE__INPUTS_MAP:
				if (coreType) return getInputsMap();
				else return getInputsMap().map();
			case DagPackage.DAG_PATTERN_NODE__OUTPUTS_MAP:
				if (coreType) return getOutputsMap();
				else return getOutputsMap().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DagPackage.DAG_PATTERN_NODE__PATTERN:
				setPattern((DAGInstruction)newValue);
				return;
			case DagPackage.DAG_PATTERN_NODE__INPUTS_MAP:
				((EStructuralFeature.Setting)getInputsMap()).set(newValue);
				return;
			case DagPackage.DAG_PATTERN_NODE__OUTPUTS_MAP:
				((EStructuralFeature.Setting)getOutputsMap()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DagPackage.DAG_PATTERN_NODE__PATTERN:
				setPattern((DAGInstruction)null);
				return;
			case DagPackage.DAG_PATTERN_NODE__INPUTS_MAP:
				getInputsMap().clear();
				return;
			case DagPackage.DAG_PATTERN_NODE__OUTPUTS_MAP:
				getOutputsMap().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DagPackage.DAG_PATTERN_NODE__PATTERN:
				return pattern != null;
			case DagPackage.DAG_PATTERN_NODE__INPUTS_MAP:
				return inputsMap != null && !inputsMap.isEmpty();
			case DagPackage.DAG_PATTERN_NODE__OUTPUTS_MAP:
				return outputsMap != null && !outputsMap.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DAGPatternNodeImpl
