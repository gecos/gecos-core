project = CreateGecosProject("tutorial"); # Just create a GeCoS project
AddSourceToGecosProject(project, "src/"); # Add source to the GeCoS project
CDTFrontend(project); # Call the GeCoS Front-end and build the GeCoS IR corresponding to the project

LocalCommonSubExpressionElimination(project, true,true); # Local Common Sub-Expressions Elimination
# Consider expressions containing function calls , be conservative regarding pointers 

SaveGecosProject(project, "project"); # Save the project (IR) into a gecosproject format (usefull to see how is build the IR)
output(project, "c", "src-regen"); # Generate project into graphviz format