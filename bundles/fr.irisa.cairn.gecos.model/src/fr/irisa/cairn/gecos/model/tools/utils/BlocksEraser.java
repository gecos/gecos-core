package fr.irisa.cairn.gecos.model.tools.utils;

import gecos.blocks.CompositeBlock;

/**
 * Do some specifics processes to properly remove Blocks from memory
 * 
 * @author amorvan
 * @generated NOT
 */
public class BlocksEraser extends BlocksDefaultSwitch<Object> {

	@Override
	public Object caseCompositeBlock(CompositeBlock cb) {
		super.caseCompositeBlock(cb);
		
		try {
			cb.getScope().getParent().getChildren().remove(cb.getScope());
		} catch (Exception e) {
			
		}
		
		return null;
	}
	
}
