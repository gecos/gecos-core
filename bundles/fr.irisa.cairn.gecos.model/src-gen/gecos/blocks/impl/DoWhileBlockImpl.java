/**
 */
package gecos.blocks.impl;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;

import fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;
import fr.irisa.cairn.gecos.model.tools.utils.BlocksContinuesBreaksFinder;
import fr.irisa.cairn.gecos.model.tools.utils.BranchInstructionFinder;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksFactory;
import gecos.blocks.BlocksPackage;
import gecos.blocks.BlocksVisitor;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ControlEdge;
import gecos.blocks.DoWhileBlock;

import gecos.instrs.BranchType;
import gecos.instrs.BreakInstruction;
import gecos.instrs.ContinueInstruction;

import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Do While Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.blocks.impl.DoWhileBlockImpl#getTestBlock <em>Test Block</em>}</li>
 *   <li>{@link gecos.blocks.impl.DoWhileBlockImpl#getBodyBlock <em>Body Block</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DoWhileBlockImpl extends BlockImpl implements DoWhileBlock {
	/**
	 * The cached value of the '{@link #getTestBlock() <em>Test Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestBlock()
	 * @generated
	 * @ordered
	 */
	protected Block testBlock;

	/**
	 * The cached value of the '{@link #getBodyBlock() <em>Body Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBodyBlock()
	 * @generated
	 * @ordered
	 */
	protected Block bodyBlock;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DoWhileBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BlocksPackage.Literals.DO_WHILE_BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block getTestBlock() {
		return testBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTestBlock(Block newTestBlock, NotificationChain msgs) {
		Block oldTestBlock = testBlock;
		testBlock = newTestBlock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BlocksPackage.DO_WHILE_BLOCK__TEST_BLOCK, oldTestBlock, newTestBlock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTestBlock(Block newTestBlock) {
		if (newTestBlock != testBlock) {
			NotificationChain msgs = null;
			if (testBlock != null)
				msgs = ((InternalEObject)testBlock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.DO_WHILE_BLOCK__TEST_BLOCK, null, msgs);
			if (newTestBlock != null)
				msgs = ((InternalEObject)newTestBlock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.DO_WHILE_BLOCK__TEST_BLOCK, null, msgs);
			msgs = basicSetTestBlock(newTestBlock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.DO_WHILE_BLOCK__TEST_BLOCK, newTestBlock, newTestBlock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block getBodyBlock() {
		return bodyBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBodyBlock(Block newBodyBlock, NotificationChain msgs) {
		Block oldBodyBlock = bodyBlock;
		bodyBlock = newBodyBlock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BlocksPackage.DO_WHILE_BLOCK__BODY_BLOCK, oldBodyBlock, newBodyBlock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBodyBlock(Block newBodyBlock) {
		if (newBodyBlock != bodyBlock) {
			NotificationChain msgs = null;
			if (bodyBlock != null)
				msgs = ((InternalEObject)bodyBlock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.DO_WHILE_BLOCK__BODY_BLOCK, null, msgs);
			if (newBodyBlock != null)
				msgs = ((InternalEObject)newBodyBlock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BlocksPackage.DO_WHILE_BLOCK__BODY_BLOCK, null, msgs);
			msgs = basicSetBodyBlock(newBodyBlock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.DO_WHILE_BLOCK__BODY_BLOCK, newBodyBlock, newBodyBlock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final BlocksVisitor visitor) {
		visitor.visitLoopBlock(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BasicBlock> listContinueBlocks() {
		final BlocksContinuesBreaksFinder finder = new BlocksContinuesBreaksFinder();
		finder.doSwitch(this.getBodyBlock());
		return ECollections.<BasicBlock>unmodifiableEList(finder.getContinues());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ContinueInstruction> listContinueInstructions() {
		final BranchInstructionFinder finder = new BranchInstructionFinder();
		finder.doSwitch(this.getBodyBlock());
		return ECollections.<ContinueInstruction>unmodifiableEList(finder.getContinues());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BasicBlock> listBreakBlocks() {
		final BlocksContinuesBreaksFinder finder = new BlocksContinuesBreaksFinder();
		finder.doSwitch(this.getBodyBlock());
		return ECollections.<BasicBlock>unmodifiableEList(finder.getBreaks());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BreakInstruction> listBreakInstructions() {
		final BranchInstructionFinder finder = new BranchInstructionFinder();
		finder.doSwitch(this.getBodyBlock());
		return ECollections.<BreakInstruction>unmodifiableEList(finder.getBreaks());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addBlockAfter(final Block a, final Block b) {
		throw new UnsupportedOperationException("Not yet implemented");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeBlock(final Block bb) {
		Block _testBlock = this.getTestBlock();
		boolean _tripleEquals = (_testBlock == bb);
		if (_tripleEquals) {
			this.setTestBlock(null);
		}
		else {
			Block _bodyBlock = this.getBodyBlock();
			boolean _tripleEquals_1 = (_bodyBlock == bb);
			if (_tripleEquals_1) {
				this.setBodyBlock(null);
			}
			else {
				Block _bodyBlock_1 = this.getBodyBlock();
				boolean _tripleNotEquals = (_bodyBlock_1 != null);
				if (_tripleNotEquals) {
					this.getBodyBlock().removeBlock(bb);
				}
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void mergeChildComposite(final CompositeBlock b) {
		throw new UnsupportedOperationException("Not yet implemented");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlEdge connectFromBasic(final BasicBlock dispatch, final BranchType cond) {
		return this.getBodyBlock().connectFromBasic(dispatch, cond);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlEdge> connectTo(final Block to, final BranchType cond) {
		final BasicEList<ControlEdge> res = ECollections.<ControlEdge>newBasicEList();
		Block _testBlock = this.getTestBlock();
		boolean _tripleNotEquals = (_testBlock != null);
		if (_tripleNotEquals) {
			res.addAll(this.getTestBlock().connectTo(to, BranchType.IF_FALSE));
		}
		Block _bodyBlock = this.getBodyBlock();
		boolean _tripleNotEquals_1 = (_bodyBlock != null);
		if (_tripleNotEquals_1) {
			final BlocksContinuesBreaksFinder visitor = new BlocksContinuesBreaksFinder();
			visitor.doSwitch(this.getBodyBlock());
			List<BasicBlock> _breaks = visitor.getBreaks();
			for (final BasicBlock bb : _breaks) {
				res.addAll(bb.connectTo(to, BranchType.UNCONDITIONAL));
			}
		}
		else {
			Block _testBlock_1 = this.getTestBlock();
			boolean _tripleNotEquals_2 = (_testBlock_1 != null);
			if (_tripleNotEquals_2) {
				res.addAll(this.getTestBlock().connectTo(to, BranchType.IF_TRUE));
			}
		}
		return res;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block managedCopy(final BlockCopyManager mng) {
		final DoWhileBlock loopBlock = BlocksFactory.eINSTANCE.createDoWhileBlock();
		loopBlock.setNumber(this.getNumber());
		loopBlock.setTestBlock(this.getTestBlock().managedCopy(mng));
		loopBlock.setBodyBlock(this.getBodyBlock().managedCopy(mng));
		GecosUserAnnotationFactory.copyAnnotations(this, loopBlock);
		mng.link(this, loopBlock);
		return loopBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void replace(final Block oldb, final Block newb) {
		Block _testBlock = this.getTestBlock();
		boolean _tripleEquals = (oldb == _testBlock);
		if (_tripleEquals) {
			this.setTestBlock(newb);
		}
		else {
			Block _bodyBlock = this.getBodyBlock();
			boolean _tripleEquals_1 = (oldb == _bodyBlock);
			if (_tripleEquals_1) {
				this.setBodyBlock(newb);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _pathString = this.getPathString();
		String _plus = (_pathString + "(");
		Block _testBlock = this.getTestBlock();
		String _plus_1 = (_plus + _testBlock);
		String _plus_2 = (_plus_1 + ") {");
		Block _bodyBlock = this.getBodyBlock();
		String _plus_3 = (_plus_2 + _bodyBlock);
		return (_plus_3 + "}");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toShortString() {
		int _number = this.getNumber();
		return ("DoLoop_" + Integer.valueOf(_number));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BlocksPackage.DO_WHILE_BLOCK__TEST_BLOCK:
				return basicSetTestBlock(null, msgs);
			case BlocksPackage.DO_WHILE_BLOCK__BODY_BLOCK:
				return basicSetBodyBlock(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BlocksPackage.DO_WHILE_BLOCK__TEST_BLOCK:
				return getTestBlock();
			case BlocksPackage.DO_WHILE_BLOCK__BODY_BLOCK:
				return getBodyBlock();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BlocksPackage.DO_WHILE_BLOCK__TEST_BLOCK:
				setTestBlock((Block)newValue);
				return;
			case BlocksPackage.DO_WHILE_BLOCK__BODY_BLOCK:
				setBodyBlock((Block)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BlocksPackage.DO_WHILE_BLOCK__TEST_BLOCK:
				setTestBlock((Block)null);
				return;
			case BlocksPackage.DO_WHILE_BLOCK__BODY_BLOCK:
				setBodyBlock((Block)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BlocksPackage.DO_WHILE_BLOCK__TEST_BLOCK:
				return testBlock != null;
			case BlocksPackage.DO_WHILE_BLOCK__BODY_BLOCK:
				return bodyBlock != null;
		}
		return super.eIsSet(featureID);
	}

} //DoWhileBlockImpl
