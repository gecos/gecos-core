typedef struct {
	int x[10];
} T1;

typedef struct s T2;
T2 a;

struct s {
	int x[5];
};

int main() {

	if(sizeof(T1) != 10*sizeof(int))
		abort();
	if(sizeof(T2) != 5*sizeof(int))
		abort();
	if(sizeof(a) != 5*sizeof(int))
		abort();

	exit(0);
}
