package fr.irisa.cairn.gecos.model.c.generator;

import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import templates.xtend.GecosTypeTemplate;

@GSModule("Select the HLS backend for bit-accurate datatypes among \n"
		+ "SystemC, MentorAC or VivadoAP.")
public class SetBitAccurateBackend {
	
	@GSModuleConstructor(
			"-arg1: String representing the backend to select. Available options:\n"
		  + "   'SystemC'\n"
		  + "   'MentorAC'\n"
		  + "   'VivadoAP'")
	public SetBitAccurateBackend(String backend) { 
		try {
			GecosTypeTemplate.bitLevelDataType= GecosTypeTemplate.BitLevelDataType.valueOf(backend);
		} catch (Exception e) {
			throw new RuntimeException("Uknown backend " + backend);
		}
	}
	
	public void  compute() {
		
	}

}
