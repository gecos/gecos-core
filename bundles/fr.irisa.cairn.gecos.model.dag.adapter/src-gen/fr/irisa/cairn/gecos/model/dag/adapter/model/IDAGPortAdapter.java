package fr.irisa.cairn.gecos.model.dag.adapter.model;

import gecos.dag.DAGPort;

import java.util.Set;

/**
 * Interface of a port adapter.

 * This port adapt a DAGPort

 * @author <your name>
 *
 */
public interface IDAGPortAdapter
		extends
			fr.irisa.cairn.graph.IPort,
			fr.irisa.cairn.graph.observer.IObservable {

	/**
	 * get the adapted port.
	 *
	 * @return the adapted port
	 */
	DAGPort getAdaptedPort();

	/** Get the port node */
	/**
	 * @model default=""
	 */
	IDAGNodeAdapter getNode();

	/** Get all connected edges */
	/**
	 * @model default=""
	 */
	Set<IDAGEdgeAdapter> getEdges();

}
