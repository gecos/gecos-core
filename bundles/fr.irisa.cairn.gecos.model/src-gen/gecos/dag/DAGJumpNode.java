/**
 */
package gecos.dag;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG Jump Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.DAGJumpNode#getLabel <em>Label</em>}</li>
 * </ul>
 *
 * @see gecos.dag.DagPackage#getDAGJumpNode()
 * @model
 * @generated
 */
public interface DAGJumpNode extends DAGNode, DagVisitable {
	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see gecos.dag.DagPackage#getDAGJumpNode_Label()
	 * @model unique="false" dataType="gecos.dag.String"
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGJumpNode#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGJumpNode(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

} // DAGJumpNode
