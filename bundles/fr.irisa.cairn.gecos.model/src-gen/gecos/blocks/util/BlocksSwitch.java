/**
 */
package gecos.blocks.util;

import gecos.annotations.AnnotatedElement;

import gecos.blocks.*;

import gecos.core.CoreVisitable;
import gecos.core.GecosNode;
import gecos.core.ScopeContainer;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see gecos.blocks.BlocksPackage
 * @generated
 */
public class BlocksSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static BlocksPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlocksSwitch() {
		if (modelPackage == null) {
			modelPackage = BlocksPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case BlocksPackage.BLOCKS_VISITOR: {
				BlocksVisitor blocksVisitor = (BlocksVisitor)theEObject;
				T result = caseBlocksVisitor(blocksVisitor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.BLOCKS_VISITABLE: {
				BlocksVisitable blocksVisitable = (BlocksVisitable)theEObject;
				T result = caseBlocksVisitable(blocksVisitable);
				if (result == null) result = caseGecosNode(blocksVisitable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.BLOCK: {
				Block block = (Block)theEObject;
				T result = caseBlock(block);
				if (result == null) result = caseAnnotatedElement(block);
				if (result == null) result = caseBlocksVisitable(block);
				if (result == null) result = caseGecosNode(block);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.BREAKABLE: {
				Breakable breakable = (Breakable)theEObject;
				T result = caseBreakable(breakable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.CONTINUABLE: {
				Continuable continuable = (Continuable)theEObject;
				T result = caseContinuable(continuable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.LOOP: {
				Loop loop = (Loop)theEObject;
				T result = caseLoop(loop);
				if (result == null) result = caseBreakable(loop);
				if (result == null) result = caseContinuable(loop);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.BASIC_BLOCK: {
				BasicBlock basicBlock = (BasicBlock)theEObject;
				T result = caseBasicBlock(basicBlock);
				if (result == null) result = caseBlock(basicBlock);
				if (result == null) result = caseAnnotatedElement(basicBlock);
				if (result == null) result = caseBlocksVisitable(basicBlock);
				if (result == null) result = caseGecosNode(basicBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.COMPOSITE_BLOCK: {
				CompositeBlock compositeBlock = (CompositeBlock)theEObject;
				T result = caseCompositeBlock(compositeBlock);
				if (result == null) result = caseBlock(compositeBlock);
				if (result == null) result = caseScopeContainer(compositeBlock);
				if (result == null) result = caseAnnotatedElement(compositeBlock);
				if (result == null) result = caseBlocksVisitable(compositeBlock);
				if (result == null) result = caseCoreVisitable(compositeBlock);
				if (result == null) result = caseGecosNode(compositeBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.CONTROL_EDGE: {
				ControlEdge controlEdge = (ControlEdge)theEObject;
				T result = caseControlEdge(controlEdge);
				if (result == null) result = caseFlowEdge(controlEdge);
				if (result == null) result = caseBlocksVisitable(controlEdge);
				if (result == null) result = caseAnnotatedElement(controlEdge);
				if (result == null) result = caseGecosNode(controlEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.DATA_EDGE: {
				DataEdge dataEdge = (DataEdge)theEObject;
				T result = caseDataEdge(dataEdge);
				if (result == null) result = caseFlowEdge(dataEdge);
				if (result == null) result = caseBlocksVisitable(dataEdge);
				if (result == null) result = caseAnnotatedElement(dataEdge);
				if (result == null) result = caseGecosNode(dataEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.FLOW_EDGE: {
				FlowEdge flowEdge = (FlowEdge)theEObject;
				T result = caseFlowEdge(flowEdge);
				if (result == null) result = caseBlocksVisitable(flowEdge);
				if (result == null) result = caseAnnotatedElement(flowEdge);
				if (result == null) result = caseGecosNode(flowEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.IF_BLOCK: {
				IfBlock ifBlock = (IfBlock)theEObject;
				T result = caseIfBlock(ifBlock);
				if (result == null) result = caseBlock(ifBlock);
				if (result == null) result = caseAnnotatedElement(ifBlock);
				if (result == null) result = caseBlocksVisitable(ifBlock);
				if (result == null) result = caseGecosNode(ifBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.WHILE_BLOCK: {
				WhileBlock whileBlock = (WhileBlock)theEObject;
				T result = caseWhileBlock(whileBlock);
				if (result == null) result = caseBlock(whileBlock);
				if (result == null) result = caseLoop(whileBlock);
				if (result == null) result = caseAnnotatedElement(whileBlock);
				if (result == null) result = caseBlocksVisitable(whileBlock);
				if (result == null) result = caseBreakable(whileBlock);
				if (result == null) result = caseContinuable(whileBlock);
				if (result == null) result = caseGecosNode(whileBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.DO_WHILE_BLOCK: {
				DoWhileBlock doWhileBlock = (DoWhileBlock)theEObject;
				T result = caseDoWhileBlock(doWhileBlock);
				if (result == null) result = caseBlock(doWhileBlock);
				if (result == null) result = caseLoop(doWhileBlock);
				if (result == null) result = caseAnnotatedElement(doWhileBlock);
				if (result == null) result = caseBlocksVisitable(doWhileBlock);
				if (result == null) result = caseBreakable(doWhileBlock);
				if (result == null) result = caseContinuable(doWhileBlock);
				if (result == null) result = caseGecosNode(doWhileBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.FOR_BLOCK: {
				ForBlock forBlock = (ForBlock)theEObject;
				T result = caseForBlock(forBlock);
				if (result == null) result = caseBlock(forBlock);
				if (result == null) result = caseLoop(forBlock);
				if (result == null) result = caseAnnotatedElement(forBlock);
				if (result == null) result = caseBlocksVisitable(forBlock);
				if (result == null) result = caseBreakable(forBlock);
				if (result == null) result = caseContinuable(forBlock);
				if (result == null) result = caseGecosNode(forBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.FOR_C99_BLOCK: {
				ForC99Block forC99Block = (ForC99Block)theEObject;
				T result = caseForC99Block(forC99Block);
				if (result == null) result = caseForBlock(forC99Block);
				if (result == null) result = caseScopeContainer(forC99Block);
				if (result == null) result = caseBlock(forC99Block);
				if (result == null) result = caseLoop(forC99Block);
				if (result == null) result = caseCoreVisitable(forC99Block);
				if (result == null) result = caseAnnotatedElement(forC99Block);
				if (result == null) result = caseBlocksVisitable(forC99Block);
				if (result == null) result = caseBreakable(forC99Block);
				if (result == null) result = caseContinuable(forC99Block);
				if (result == null) result = caseGecosNode(forC99Block);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.SIMPLE_FOR_BLOCK: {
				SimpleForBlock simpleForBlock = (SimpleForBlock)theEObject;
				T result = caseSimpleForBlock(simpleForBlock);
				if (result == null) result = caseForC99Block(simpleForBlock);
				if (result == null) result = caseForBlock(simpleForBlock);
				if (result == null) result = caseScopeContainer(simpleForBlock);
				if (result == null) result = caseBlock(simpleForBlock);
				if (result == null) result = caseLoop(simpleForBlock);
				if (result == null) result = caseCoreVisitable(simpleForBlock);
				if (result == null) result = caseAnnotatedElement(simpleForBlock);
				if (result == null) result = caseBlocksVisitable(simpleForBlock);
				if (result == null) result = caseBreakable(simpleForBlock);
				if (result == null) result = caseContinuable(simpleForBlock);
				if (result == null) result = caseGecosNode(simpleForBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.SWITCH_BLOCK: {
				SwitchBlock switchBlock = (SwitchBlock)theEObject;
				T result = caseSwitchBlock(switchBlock);
				if (result == null) result = caseBlock(switchBlock);
				if (result == null) result = caseBreakable(switchBlock);
				if (result == null) result = caseAnnotatedElement(switchBlock);
				if (result == null) result = caseBlocksVisitable(switchBlock);
				if (result == null) result = caseGecosNode(switchBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.CASE_BLOCK: {
				CaseBlock caseBlock = (CaseBlock)theEObject;
				T result = caseCaseBlock(caseBlock);
				if (result == null) result = caseBlock(caseBlock);
				if (result == null) result = caseAnnotatedElement(caseBlock);
				if (result == null) result = caseBlocksVisitable(caseBlock);
				if (result == null) result = caseGecosNode(caseBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BlocksPackage.CASE_MAP: {
				@SuppressWarnings("unchecked") Map.Entry<Long, Block> caseMap = (Map.Entry<Long, Block>)theEObject;
				T result = caseCaseMap(caseMap);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visitor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visitor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlocksVisitor(BlocksVisitor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlocksVisitable(BlocksVisitable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlock(Block object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Breakable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Breakable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBreakable(Breakable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Continuable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Continuable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContinuable(Continuable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Loop</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Loop</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLoop(Loop object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Basic Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Basic Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBasicBlock(BasicBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Composite Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Composite Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCompositeBlock(CompositeBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Control Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Control Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseControlEdge(ControlEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataEdge(DataEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Flow Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Flow Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFlowEdge(FlowEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>If Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>If Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIfBlock(IfBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>While Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>While Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWhileBlock(WhileBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Do While Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Do While Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDoWhileBlock(DoWhileBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>For Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>For Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseForBlock(ForBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>For C99 Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>For C99 Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseForC99Block(ForC99Block object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple For Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple For Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimpleForBlock(SimpleForBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Switch Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Switch Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSwitchBlock(SwitchBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Case Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Case Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCaseBlock(CaseBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Case Map</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Case Map</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCaseMap(Map.Entry<Long, Block> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Gecos Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Gecos Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGecosNode(GecosNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotatedElement(AnnotatedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCoreVisitable(CoreVisitable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Scope Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Scope Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScopeContainer(ScopeContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //BlocksSwitch
