#include "defs.h"
#include "data_size.h"


void gauss(TYPE_0 in[M][N], TYPE_1 out[M][N]){

	float CV[3] = {0.25, 0.5, 0.25};
	float CH[3] = {0.25, 0.5, 0.25};

	TYPE_1 tp[M][N]={0};
	int i, j;

	for(i = 1; i < M - 1; i++)
		for(j = 0; j < N; j++)
			tp[i][j]  = CV[0] * in[i - 1][j] + CV[1] * in[i][j] + CV[2] * in[i + 1][j];
	for(i = 0; i < M; i++)
		for(j = 1; j < N-1; j++)
			out[i][j] = CH[0] * tp[i][j - 1] + CH[1] * tp[i][j] + CH[2] * tp[i][j + 1];
}
