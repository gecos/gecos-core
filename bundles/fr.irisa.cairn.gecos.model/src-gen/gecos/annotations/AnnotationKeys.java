/**
 */
package gecos.annotations;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Annotation Keys</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see gecos.annotations.AnnotationsPackage#getAnnotationKeys()
 * @model
 * @generated
 */
public enum AnnotationKeys implements Enumerator {
	/**
	 * The '<em><b>PRAGMA ANNOTATION KEY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PRAGMA_ANNOTATION_KEY_VALUE
	 * @generated
	 * @ordered
	 */
	PRAGMA_ANNOTATION_KEY(0, "PRAGMA_ANNOTATION_KEY", "#pragma"),

	/**
	 * The '<em><b>COMMENT ANNOTATION KEY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COMMENT_ANNOTATION_KEY_VALUE
	 * @generated
	 * @ordered
	 */
	COMMENT_ANNOTATION_KEY(0, "COMMENT_ANNOTATION_KEY", "#comment"),

	/**
	 * The '<em><b>SYMBOL DEF ANNOTATION KEY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SYMBOL_DEF_ANNOTATION_KEY_VALUE
	 * @generated
	 * @ordered
	 */
	SYMBOL_DEF_ANNOTATION_KEY(0, "SYMBOL_DEF_ANNOTATION_KEY", "SymbolDefinitionAnnotation"),

	/**
	 * The '<em><b>FILE LOCATION ANNOTATION KEY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FILE_LOCATION_ANNOTATION_KEY_VALUE
	 * @generated
	 * @ordered
	 */
	FILE_LOCATION_ANNOTATION_KEY(0, "FILE_LOCATION_ANNOTATION_KEY", "gecos.core::FILE_LOCATION");

	/**
	 * The '<em><b>PRAGMA ANNOTATION KEY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PRAGMA ANNOTATION KEY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PRAGMA_ANNOTATION_KEY
	 * @model literal="#pragma"
	 * @generated
	 * @ordered
	 */
	public static final int PRAGMA_ANNOTATION_KEY_VALUE = 0;

	/**
	 * The '<em><b>COMMENT ANNOTATION KEY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>COMMENT ANNOTATION KEY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #COMMENT_ANNOTATION_KEY
	 * @model literal="#comment"
	 * @generated
	 * @ordered
	 */
	public static final int COMMENT_ANNOTATION_KEY_VALUE = 0;

	/**
	 * The '<em><b>SYMBOL DEF ANNOTATION KEY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SYMBOL DEF ANNOTATION KEY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SYMBOL_DEF_ANNOTATION_KEY
	 * @model literal="SymbolDefinitionAnnotation"
	 * @generated
	 * @ordered
	 */
	public static final int SYMBOL_DEF_ANNOTATION_KEY_VALUE = 0;

	/**
	 * The '<em><b>FILE LOCATION ANNOTATION KEY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FILE LOCATION ANNOTATION KEY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FILE_LOCATION_ANNOTATION_KEY
	 * @model literal="gecos.core::FILE_LOCATION"
	 * @generated
	 * @ordered
	 */
	public static final int FILE_LOCATION_ANNOTATION_KEY_VALUE = 0;

	/**
	 * An array of all the '<em><b>Annotation Keys</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final AnnotationKeys[] VALUES_ARRAY =
		new AnnotationKeys[] {
			PRAGMA_ANNOTATION_KEY,
			COMMENT_ANNOTATION_KEY,
			SYMBOL_DEF_ANNOTATION_KEY,
			FILE_LOCATION_ANNOTATION_KEY,
		};

	/**
	 * A public read-only list of all the '<em><b>Annotation Keys</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<AnnotationKeys> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Annotation Keys</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static AnnotationKeys get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			AnnotationKeys result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Annotation Keys</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static AnnotationKeys getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			AnnotationKeys result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Annotation Keys</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static AnnotationKeys get(int value) {
		switch (value) {
			case PRAGMA_ANNOTATION_KEY_VALUE: return PRAGMA_ANNOTATION_KEY;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private AnnotationKeys(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //AnnotationKeys
