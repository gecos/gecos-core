/**
 */
package gecos.blocks.impl;

import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;

import fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;
import fr.irisa.cairn.gecos.model.tools.utils.GecosCopier;

import gecos.annotations.impl.AnnotatedElementImpl;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksPackage;
import gecos.blocks.BlocksVisitor;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ControlEdge;

import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.ScopeContainer;

import gecos.instrs.BranchType;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.xtext.xbase.lib.Exceptions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.blocks.impl.BlockImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link gecos.blocks.impl.BlockImpl#getNumber <em>Number</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class BlockImpl extends AnnotatedElementImpl implements Block {
	/**
	 * The default value of the '{@link #getNumber() <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumber() <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber()
	 * @generated
	 * @ordered
	 */
	protected int number = NUMBER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BlocksPackage.Literals.BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block getParent() {
		EObject _eContainer = this.eContainer();
		if ((_eContainer instanceof Block)) {
			EObject _eContainer_1 = this.eContainer();
			return ((Block) _eContainer_1);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumber(int newNumber) {
		int oldNumber = number;
		number = newNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BlocksPackage.BLOCK__NUMBER, oldNumber, number));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block simplifyBlock() {
		return this;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPathString() {
		return this.getPathString(this.toShortString());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		boolean _eIsProxy = this.eIsProxy();
		if (_eIsProxy) {
			return super.toString();
		}
		String _string = super.toString();
		final StringBuffer result = new StringBuffer(_string);
		result.append(" (number: ");
		result.append(this.getNumber());
		result.append(")");
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addBlockAfter(final Block insertedBlock, final Block existingBlock) {
		final Block parent = this.getParent();
		final CompositeBlock compositeBlock = GecosUserBlockFactory.CompositeBlock();
		parent.replace(existingBlock, compositeBlock);
		compositeBlock.getChildren().add(0, existingBlock);
		compositeBlock.getChildren().add(1, insertedBlock);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void mergeChildComposite(final CompositeBlock b) {
		String _simpleName = this.getClass().getSimpleName();
		String _plus = ("Non applicable to an object of class " + _simpleName);
		throw new UnsupportedOperationException(_plus);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void mergeWithBasic(final BasicBlock to) {
		String _simpleName = this.getClass().getSimpleName();
		String _plus = ("Non applicable to an object of class " + _simpleName);
		throw new UnsupportedOperationException(_plus);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeBlock(final Block bb) {
		String _simpleName = this.getClass().getSimpleName();
		String _plus = ("Non applicable to an object of class " + _simpleName);
		throw new UnsupportedOperationException(_plus);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void replace(final Block oldBlock, final Block newBlock) {
		String _simpleName = this.getClass().getSimpleName();
		String _plus = ("Non applicable to an object of class " + _simpleName);
		throw new UnsupportedOperationException(_plus);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlEdge> connectTo(Block to, BranchType cond) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlEdge connectFromBasic(BasicBlock from, BranchType cond) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public <T extends Block> T copy() {
		final GecosCopier gecosCopier = new GecosCopier();
		EObject _copy = gecosCopier.copy(this);
		final T block = ((T) _copy);
		gecosCopier.copyReferences();
		return block;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block mergeWithComposite(final CompositeBlock cblock) {
		cblock.getScope().mergeWith(this.getScope());
		return cblock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean mergeChildren() {
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scope getScope() {
		if ((this instanceof ScopeContainer)) {
			final Scope res = ((ScopeContainer) this).getScope();
			if ((res != null)) {
				return res;
			}
		}
		Block _parent = this.getParent();
		Scope _scope = null;
		if (_parent!=null) {
			_scope=_parent.getScope();
		}
		return _scope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(BlocksVisitor visitor) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Procedure getContainingProcedure() {
		EObject containr = this.eContainer();
		while ((containr != null)) {
			if ((containr instanceof Procedure)) {
				return ((Procedure)containr);
			}
			else {
				containr = containr.eContainer();
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureSet getContainingProcedureSet() {
		Procedure _containingProcedure = this.getContainingProcedure();
		ProcedureSet _containingProcedureSet = null;
		if (_containingProcedure!=null) {
			_containingProcedureSet=_containingProcedure.getContainingProcedureSet();
		}
		return _containingProcedureSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPathString(final String nodeName) {
		final Block parent = this.getParent();
		try {
			if ((parent == null)) {
				Procedure _containingProcedure = this.getContainingProcedure();
				boolean _tripleNotEquals = (_containingProcedure != null);
				if (_tripleNotEquals) {
					final ProcedureSymbol symbol = this.getContainingProcedure().getSymbol();
					if ((symbol != null)) {
						String _name = symbol.getName();
						String _plus = ("/proc:" + _name);
						String _plus_1 = (_plus + "/");
						return (_plus_1 + nodeName);
					}
					else {
						return ("/??/" + nodeName);
					}
				}
				else {
					return ("/" + nodeName);
				}
			}
			else {
				final int indexOf = parent.eContents().indexOf(this);
				String _pathString = parent.getPathString();
				String _plus_2 = (_pathString + "/");
				String _plus_3 = (_plus_2 + nodeName);
				String _plus_4 = (_plus_3 + "#");
				return (_plus_4 + Integer.valueOf(indexOf));
			}
		}
		catch (final Throwable _t) {
			if (_t instanceof Exception) {
				return ("/exception_" + nodeName);
			}
			else {
				throw Exceptions.sneakyThrow(_t);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRootBlock() {
		EObject _eContainer = this.eContainer();
		return (_eContainer instanceof Procedure);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Procedure getRootProcedure() {
		return this.getContainingProcedure();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block managedCopy(BlockCopyManager mng) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toShortString() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BlocksPackage.BLOCK__PARENT:
				return getParent();
			case BlocksPackage.BLOCK__NUMBER:
				return getNumber();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BlocksPackage.BLOCK__NUMBER:
				setNumber((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BlocksPackage.BLOCK__NUMBER:
				setNumber(NUMBER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BlocksPackage.BLOCK__PARENT:
				return getParent() != null;
			case BlocksPackage.BLOCK__NUMBER:
				return number != NUMBER_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //BlockImpl
