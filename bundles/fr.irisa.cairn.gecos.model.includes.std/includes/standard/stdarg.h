#ifndef STD_ARG_H
#define STD_ARG_H

typedef unsigned char* va_list;

//void va_start(va_list ap, argN);
//void va_copy(va_list dest, va_list src);
//type va_arg(va_list ap, type);
//void va_end(va_list ap);

#define va_start(list, last)    list = (unsigned char *)&last + sizeof(last)
#define va_arg(list, type)      *((type *)((list += sizeof(type)) - sizeof(type)))
#define va_end(list)            list = ((va_list) 0)

#endif
