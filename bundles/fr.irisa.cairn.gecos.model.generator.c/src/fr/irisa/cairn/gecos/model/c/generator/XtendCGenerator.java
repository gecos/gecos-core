/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.c.generator;

import fr.irisa.cairn.gecos.model.extensions.generators.IGeneratorDispatchStrategy;
import fr.irisa.cairn.gecos.model.extensions.generators.PerfectMatchDispatchStrategy;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.modules.CreateProject;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import gecos.annotations.IAnnotation;
import gecos.annotations.PragmaAnnotation;
import gecos.annotations.StringAnnotation;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;
import gecos.gecosproject.GecosprojectFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import com.google.common.io.Files;

@GSModule("Generates C code corresponding to a given Project or Procedure Set.\n"
		+ "Each procedure set will be generated in a separate C file with the same"
		+ "name as the original source C file if it exists, or 'tmp_gecos_file.c' "
		+ "otherwise.")
public class XtendCGenerator {

	private boolean force = true;
	private GecosProject gecosProject;
	private String outputDir;
	private final static IGeneratorDispatchStrategy DEFAULT_STRATEGY = new PerfectMatchDispatchStrategy();

	@GSModuleConstructor(
		"-arg1: the GecosProject for which C code will be generated.\n"
	  + "   See 'CreateGecosProject' module."
	  + "\n-arg2: output directory path where the generated code will be placed.\n"
	  + "   If the directory does not exist it will be created unless the path refer\n"
	  + "   to an existing file in which case the command will fail."
	  + "\n-arg3: if 'true' then force (override) the generation of a file in case\n"
	  + "   it already exists, otherwise the command will fail.")
	public XtendCGenerator(GecosProject gecosProject, String outputDir, boolean force) {
		this.gecosProject = gecosProject;
		this.outputDir = outputDir;
		this.force = force;
	}
	
	@GSModuleConstructor("Force overriding files.")
	public XtendCGenerator(GecosProject gecosProject, String outputDir) {
		this(gecosProject, outputDir, true); //XXX default should be not force !
	}
	
	@GSModuleConstructor(
			"-arg1: the ProcedureSet for which C code will be generated.\n"
		  + "\n-arg2: output directory path where the generated code will be placed.\n"
		  + "   If the directory does not exist it will be created unless the path refer\n"
		  + "   to an existing file in which case the command will fail."
		  + "\n-arg3: if 'true' then force (override) the generation of a file in case\n"
		  + "   it already exists, otherwise the command will fail.")
	public XtendCGenerator(ProcedureSet ps, String outputDir, boolean force) {
		if(ps.eContainer() != null && ps.eContainer().eContainer() != null && ps.eContainer().eContainer() instanceof GecosProject){
			gecosProject = (GecosProject) ps.eContainer().eContainer();
		}
		else{
			this.gecosProject = new CreateProject(GecosUserCoreFactory.DEFAULT_PROJECT_NAME).compute();
			GecosSourceFile sfile = GecosprojectFactory.eINSTANCE.createGecosSourceFile();
			IAnnotation annotation = ps.getAnnotation(GecosUserAnnotationFactory.FILE_NAME_ANOTATION_KEY);
			if (annotation != null)
				sfile.setName(((StringAnnotation)annotation).getContent());
			else
				sfile.setName("tmp_gecos_file.c");
			sfile.setModel(ps);
			this.gecosProject.getSources().add(sfile);
		}
		this.outputDir = outputDir;
		this.force = force;
	}
	
	@GSModuleConstructor("Force overriding files.")
	public XtendCGenerator(ProcedureSet ps, String outputDir) {
		this(ps, outputDir, true); //XXX default should be not force !
	}
	
	
	public void compute() {
		PrintStream printStream = null;
		try {
			File file = new File(outputDir);
			if (file.exists() && !file.isDirectory())
				throw new IllegalArgumentException(file.getCanonicalPath()+" already exists but is not a directory.");
			file.mkdirs();
			for (GecosSourceFile gecosSourceFile : gecosProject.getSources()) {
				if (outputDir.endsWith("/"))
					file = new File(outputDir + getFileName(gecosSourceFile.getName()));
				else
					file = new File(outputDir + "/" + getFileName(gecosSourceFile.getName()));
				if (file.exists()) {
					if (file.equals(new File(gecosSourceFile.getName())))
						throw new IllegalArgumentException("File '" + gecosSourceFile.getName() + "' already exits! "
								+ "To force override set the argument 'force'.");
					if (force)
						file.delete();
					else {
						System.err.println("Could not generate file '"+file.getCanonicalPath()+"' : file already exists (use force to overide).");
						continue;
					}
				}
				file.createNewFile();
				printStream = new PrintStream(new FileOutputStream(file));
				ExtendableCGenerator gecoscoreTemplate = new ExtendableCGenerator(DEFAULT_STRATEGY);
				ProcedureSet model = gecosSourceFile.getModel();
				if (model == null)
					throw new NullPointerException("The GeCoS source file "+gecosSourceFile.getName()+" has not been parsed by the FrontEnd.");
				CharSequence charSequence = gecoscoreTemplate.generate(model);
				printStream.print(charSequence.toString());
				printStream.close();
				
				/* check if model requires coping files to outputDir */
				PragmaAnnotation pragmaAnnotation = model.getPragma();
				if(pragmaAnnotation != null) {
					for(String string : pragmaAnnotation.getContent()) {
						if(string.startsWith(GecosUserAnnotationFactory.CODEGEN_COPYFILE_ANNOTATION)) {
							String fileToCopy = string.substring(GecosUserAnnotationFactory.CODEGEN_COPYFILE_ANNOTATION.length());
							
							File from = new File(fileToCopy);
							if(from.exists() && from.isFile()) {
								File to = new File(outputDir, from.getName());
								if(to.exists()) {
									if(force)
										to.delete();
									else {
										System.err.println("[XtendCCenerator]: couldn't override file " + to + ". File already exists (use force to overide)");
										continue;
									}
								}
								Files.copy(from, to);
							}
							else {
								System.err.println("[XtendCCenerator]: couldn't copy file " + fileToCopy + ". File not found!");
							}
						}
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			printStream.close();
		}
	}
	
	private String getFileName(String file) {
		String fileName = file;
		File f = new File(file);
		if (f.isFile())
			fileName = f.getName();
		return fileName;
	}
}