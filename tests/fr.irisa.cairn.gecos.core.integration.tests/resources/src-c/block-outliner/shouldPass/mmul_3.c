#define M 5
#define N 4
#define O 6

int A[M][N] = {5};
int B[N][O] = {3};
int C[M][O] = {0};

int main() {
	int i, j, k;

	#pragma outline
	{
		for(i=0; i<M; i++) {
			for(j=0; j<O; j++) {
				int acc = 0;
				for(k=0; k<N; k++) {
					acc += A[i][k] * B[k][j];
				}
				C[i][j] = acc;
			}
		}
	}

	//-- test --
	for(i=0; i<M; i++) {
		for(j=0; j<O; j++) {
			int acc = 0;
			for(k=0; k<N; k++) {
				acc += A[i][k] * B[k][j];
			}
			if(C[i][j] != acc)
				abort();
		}
	}

	return 0;
}
