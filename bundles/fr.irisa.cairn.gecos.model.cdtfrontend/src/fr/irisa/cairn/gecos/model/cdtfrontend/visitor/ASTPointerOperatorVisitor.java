/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.cdtfrontend.visitor;

import org.eclipse.cdt.core.dom.ast.IASTPointer;
import org.eclipse.cdt.core.dom.ast.IASTPointerOperator;
import org.eclipse.cdt.core.dom.ast.c.ICASTPointer;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTPointerToMember;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTReferenceOperator;

import fr.irisa.cairn.gecos.model.cdtfrontend.exception.CDTParserNotYetSupportedException;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import gecos.types.PtrType;
import gecos.types.Type;

public class ASTPointerOperatorVisitor {

	private boolean isConstant;
	private boolean isVolatile;
	private boolean isRestrict;


	public ASTPointerOperatorVisitor() {
		isConstant = false;
		isVolatile = false;
		isRestrict = false;
	}
	
	public void visit(CDTToGecosModel gecosModel, IASTPointerOperator pointerOperator) {
		if (pointerOperator instanceof IASTPointer)
			if (pointerOperator instanceof ICASTPointer)
				buildICASTPointer((ICASTPointer)pointerOperator);
			else if (pointerOperator instanceof ICPPASTPointerToMember)
				buildICPPASTPointerToMember((ICPPASTPointerToMember)pointerOperator);
			else
				buildIASTPointer((IASTPointer)pointerOperator);
		
		else if (pointerOperator instanceof ICPPASTReferenceOperator)
			buildICPPASTReferenceOperator((ICPPASTReferenceOperator)pointerOperator);
	}

	public void buildIASTPointer(IASTPointer pointerOperator) {
		this.isConstant = pointerOperator.isConst();
		this.isVolatile = pointerOperator.isVolatile();
		this.isRestrict = pointerOperator.isRestrict();
	}

	public void buildICASTPointer(ICASTPointer pointerOperator) {
		buildIASTPointer(pointerOperator);
	}

	public void buildICPPASTPointerToMember(ICPPASTPointerToMember pointerOperator) {
		buildIASTPointer(pointerOperator);
	}

	public PtrType buildPtrType(Type base) {
		PtrType type = GecosUserTypeFactory.PTR(base);
		type.setConstant(isConstant);
		type.setVolatile(isVolatile);
		type.setRestrict(isRestrict);
		return type;
	}
	
	public void buildICPPASTReferenceOperator(ICPPASTReferenceOperator pointerOperator) {
		notYetSupported(pointerOperator);
	}

	public void buildIGPPASTPointerToMember(ICPPASTPointerToMember pointerOperator) {
		notYetSupported(pointerOperator);
	}
	
	private static void notYetSupported(IASTPointerOperator pointerOperator) {
		throw new CDTParserNotYetSupportedException(pointerOperator, "Support for "+pointerOperator.getClass().getSimpleName()+ ":" +pointerOperator+ " not yet implemented ");
	}

}