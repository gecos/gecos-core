#include <stdio.h>
#include <stdlib.h>

#define M 150
#define N 10

void testGlobal_CSE(int , int , int [N], int [M], int [M], int);

int main(int argc, const char **argv) {
	FILE *outfile;
	int a = 111, b = 5555, k;
	int f[M], g[M], res[N];
	int optionPtr;

	/* check parameters */
	if (argc < 3) {
		fprintf(stderr, "Usage: %s output_file pointer_option\n", argv[0]);
		exit(-1);
	}

	outfile = fopen(argv[1], "w");
	if (outfile == NULL) {
		fprintf(stderr, "couldn't open file \"%s\"!\n", argv[1]);
		exit(-2);
	}

	optionPtr = atoi(argv[argc-1]);

	for (k = 0; k < N; k++)
		res[k]=0;
	for (k = 0; k < M; k++)
		f[k]=0;
	for (k = 0; k < M; k++)
		g[k]=0;

	// test Global Common subExpressions Elimination
	testGlobal_CSE(a, b, res, f, g, optionPtr);

	fprintf(outfile, "Res : %d %d %d %d %d %d \n", res[0], res[1], res[2], res[3], res[4], res[5]);
	for (k = 0; k < M; k++)
		fprintf(outfile, "f[%d] = %d \n", k, f[k]);

	for (k = 0; k < M; k++)
		fprintf(outfile, "g[%d] = %d \n", k, g[k]);

	fclose(outfile);
	return 0;
}
