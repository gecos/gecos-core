package fr.irisa.cairn.gecos.model.tools.utils;

import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.util.BlocksSwitch;

import org.eclipse.emf.ecore.EObject;

public class ScopeRelinker extends BlocksSwitch<Object>{

//	private CompositeBlock block;

	public ScopeRelinker(CompositeBlock b) {
//		this.block=b;
		
	}

	public void relink(Block child) {
		doSwitch(child); 
	}
	
	@Override
	public Object caseCompositeBlock(CompositeBlock object) {
		//object.getScope().setParent(block.getScope());
		return super.caseCompositeBlock(object);
	}

	@Override
	public Object doSwitch(EObject theEObject) {
		for (EObject object : theEObject.eContents()) {
			doSwitch(object); 
		}
		return super.doSwitch(theEObject);
	}

}
