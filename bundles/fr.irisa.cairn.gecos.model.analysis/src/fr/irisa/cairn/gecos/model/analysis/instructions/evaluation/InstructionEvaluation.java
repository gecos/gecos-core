package fr.irisa.cairn.gecos.model.analysis.instructions.evaluation;

import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.Float;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.Int;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.RangeInstruction;

public class InstructionEvaluation {

	private static final boolean debug = true;
	private static final void debug(Object o) { if (debug) System.out.print(o.toString()); }
	private static final void debugln(Object o) { debug(o.toString()+"\n"); }
	private static final String PREFIX = "[INSTRUCTION_EVALUATION] ";

	public static Instruction applyBinary(GenericInstruction inst, long t, long b) {
		if (inst.getName().equals("add"))
			return Int(t + b);
		if (inst.getName().equals("sub"))
			return Int(t - b);
		if (inst.getName().equals("mul"))
			return Int(t * b);
		if (inst.getName().equals("mod"))
			return Int(t % b);
		if (inst.getName().equals("div")) {
			if(b!=0)
				return Int(t / b);
			else
				return inst;
		}
		if (inst.getName().equals("xor"))
			return Int(t ^ b);
		if (inst.getName().equals("or"))
			return Int(t | b);
		if (inst.getName().equals("and"))
			return Int(t & b);
		if (inst.getName().equals("sl"))
			return Int((t < b) ? 1 : 0);

		if (inst.getName().equals("eq")) 
			return Int((t == b)?1:0);
		if (inst.getName().equals("ne")) 
			return Int((t != b)?1:0);
		if (inst.getName().equals("lt")) 
			return Int((t < b)?1:0);
		if (inst.getName().equals("le")) 
			return Int((t <= b)?1:0);
		if (inst.getName().equals("gt")) 
			return Int((t > b)?1:0);
		if (inst.getName().equals("ge")) 
			return Int((t >= b)?1:0);
		if (inst.getName().equals("land"))
			return Int((t != 0 && b != 0)?1:0);
		if (inst.getName().equals("lor"))
			return Int((t != 0 || b != 0)?1:0);
		
		if (inst.getName().equals("shl"))
			return Int(t<<b);
		if (inst.getName().equals("shr"))
			return Int(t>>b);

		debugln(PREFIX+"does not know how to simplify operator ["+inst.getName()+"] in instruction ["+inst+"]");
		return null;
	}


	public static Instruction applyBinary(GenericInstruction inst, double t, double b) {
		if (inst.getName().equals("add"))
			return Float(t + b);
		if (inst.getName().equals("sub"))
			return Float(t - b);
		if (inst.getName().equals("mul"))
			return Float(t * b);
		if (inst.getName().equals("div"))
			return Float(t / b);

		debugln(PREFIX+"does not know how to simplify operator ["+inst.getName()+"] in instruction ["+inst+"]");
		return null;
	}

	public static Instruction applyUnary(GenericInstruction inst, long t) {
		if (inst.getName().equals("not"))
			return Int(~t);
		if (inst.getName().equals("lnot"))
			return Int((t == 0)?1:0);
		if (inst.getName().equals("neg"))
			return Int(-t);
		debugln(PREFIX+"does not know how to simplify operator ["+inst.getName()+"] in instruction ["+inst+"]");
		return null;
	}

	public static Instruction applyUnary(GenericInstruction inst, double t) {
		if (inst.getName().equals("neg"))
			return Float(-t);
		debugln(PREFIX+"does not know how to simplify operator ["+inst.getName()+"] in instruction ["+inst+"]");
		return null;
	}


	public static Instruction applyTernary(GenericInstruction inst,long c) {
		if (inst.getName().equals("mux"))
			return ((c != 0)?inst.getOperand(1):inst.getOperand(2));

		debugln(PREFIX+"does not know how to simplify operator ["+inst.getName()+"] in instruction ["+inst+"]");
		return null;
	}

	public static Instruction applyBinary(GenericInstruction g) {
		Instruction operand0 = g.getOperand(0);
		Instruction operand1 = g.getOperand(1);
		if (operand0 instanceof IntInstruction) {
			if (operand1 instanceof IntInstruction) {
				long value0 = ((IntInstruction)operand0).getValue();
				long value1 = ((IntInstruction)operand1).getValue();
				Instruction newInstr = applyBinary(g,value0,value1);
				newInstr.setType(g.getType());
				return newInstr;
			}
			if (operand1 instanceof FloatInstruction) {
				long value0 = ((IntInstruction)operand0).getValue();
				double value1 = ((FloatInstruction)operand1).getValue();
				Instruction newInstr = applyBinary(g,value0,value1);
				newInstr.setType(GecosUserTypeFactory.DOUBLE());
				return newInstr;
			}
		}
		if (operand0 instanceof FloatInstruction) {
			if (operand1 instanceof FloatInstruction) {
				double value0 = ((FloatInstruction)operand0).getValue();
				double value1 = ((FloatInstruction)operand1).getValue();
				Instruction newInstr = applyBinary(g,value0,value1);
				newInstr.setType(g.getType());
				return newInstr;
			}
			if (operand1 instanceof IntInstruction) {
				double value0 = ((FloatInstruction)operand0).getValue();
				long value1 = ((IntInstruction)operand1).getValue();
				Instruction newInstr = applyBinary(g,value0,value1);
				newInstr.setType(g.getType());
				return newInstr;
			}
		}
		throw new IllegalArgumentException("Cannot statically determine operation on non constant nodes :  "+operand0+","+operand1+".");
	}

	public static Instruction applyRange(RangeInstruction range) {
		if (range.getExpr() instanceof IntInstruction) {
			IntInstruction intInst = (IntInstruction) range.getExpr();
			long value = intInst.getValue() >> range.getLow();
			value = value & ((1<< range.getHigh())-1); 
		}
		throw new IllegalArgumentException("Cannot statically determine range operation on non constant expression :  "+range.getExpr());
	}


	public static Instruction applyUnary(GenericInstruction g) {
		Instruction operand0 = g.getOperand(0);
		if (operand0 instanceof IntInstruction) {
			long value = ((IntInstruction)operand0).getValue();
			Instruction newInst = applyUnary(g,value);
			newInst.setType(g.getType());
			return newInst;
		}
		if (operand0 instanceof FloatInstruction) {
			double value = ((FloatInstruction)operand0).getValue();
			Instruction newInst = applyUnary(g,value);
			newInst.setType(g.getType());
			return newInst;
		}
		return null;
	}

	public static Instruction applyTernary(GenericInstruction g) {
		Instruction operand0 = g.getOperand(0);
		if (operand0 instanceof IntInstruction) {
			long value = ((IntInstruction)operand0).getValue();
			Instruction newInst = applyTernary(g,value);
			newInst.setType(g.getType());
			return newInst;
		}
		return null;
	}

}
