package fr.irisa.cairn.gecos.model.tools.switches;

import gecos.dag.DAGArrayValueNode;
import gecos.dag.DAGCallNode;
import gecos.dag.DAGControlEdge;
import gecos.dag.DAGDataEdge;
import gecos.dag.DAGEdge;
import gecos.dag.DAGImmNode;
import gecos.dag.DAGInPort;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGNode;
import gecos.dag.DAGNumberedSymbolNode;
import gecos.dag.DAGOpNode;
import gecos.dag.DAGOutNode;
import gecos.dag.DAGOutPort;
import gecos.dag.DAGPort;
import gecos.dag.DAGSymbolNode;
import gecos.dag.util.DagSwitch;

public class DAGDefaultInstructionSwitch<T> extends DagSwitch<T> {

	@Override
	public T caseDAGArrayValueNode(DAGArrayValueNode object) {
		// TODO Auto-generated method stub
		return super.caseDAGArrayValueNode(object);
	}

	@Override
	public T caseDAGCallNode(DAGCallNode object) {
		// TODO Auto-generated method stub
		return super.caseDAGCallNode(object);
	}

	@Override
	public T caseDAGControlEdge(DAGControlEdge object) {
		// TODO Auto-generated method stub
		return super.caseDAGControlEdge(object);
	}

	@Override
	public T caseDAGDataEdge(DAGDataEdge object) {
		// TODO Auto-generated method stub
		return super.caseDAGDataEdge(object);
	}

	@Override
	public T caseDAGEdge(DAGEdge object) {
		// TODO Auto-generated method stub
		return super.caseDAGEdge(object);
	}

	@Override
	public T caseDAGImmNode(DAGImmNode object) {
		// TODO Auto-generated method stub
		return super.caseDAGImmNode(object);
	}

	@Override
	public T caseDAGInPort(DAGInPort object) {
		// TODO Auto-generated method stub
		return super.caseDAGInPort(object);
	}

	@Override
	public T caseDAGInstruction(DAGInstruction object) {
		for (DAGNode node : object.getNodes()) {
			doSwitch(node);
		}
		for (DAGEdge edge: object.getEdges()) {
			doSwitch(edge);
		}
		return super.caseDAGInstruction(object);
	}

	@Override
	public T caseDAGNode(DAGNode object) {
		// TODO Auto-generated method stub
		return super.caseDAGNode(object);
	}

	@Override
	public T caseDAGOpNode(DAGOpNode object) {
		// TODO Auto-generated method stub
		return super.caseDAGOpNode(object);
	}

	@Override
	public T caseDAGOutNode(DAGOutNode object) {
		// TODO Auto-generated method stub
		return super.caseDAGOutNode(object);
	}

	@Override
	public T caseDAGOutPort(DAGOutPort object) {
		// TODO Auto-generated method stub
		return super.caseDAGOutPort(object);
	}

	@Override
	public T caseDAGPort(DAGPort object) {
		// TODO Auto-generated method stub
		return super.caseDAGPort(object);
	}

	@Override
	public T caseDAGSymbolNode(DAGSymbolNode object) {
		// TODO Auto-generated method stub
		return super.caseDAGSymbolNode(object);
	}
	
	@Override
	public T caseDAGNumberedSymbolNode(
			DAGNumberedSymbolNode object) {
		// TODO Auto-generated method stub
		return super.caseDAGNumberedSymbolNode(object);
	}

}
