/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.cdtfrontend;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.cdt.core.CCorePlugin;
import org.eclipse.cdt.core.dom.ast.IASTNode;
import org.eclipse.cdt.core.dom.ast.IASTTranslationUnit;
import org.eclipse.cdt.core.model.CoreModel;
import org.eclipse.cdt.core.model.ICProject;
import org.eclipse.cdt.core.model.ITranslationUnit;
import org.eclipse.cdt.internal.core.model.ExternalTranslationUnit;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;

import fr.irisa.cairn.gecos.model.cdtfrontend.exception.CDTParserErrorException;
import fr.irisa.cairn.gecos.model.cdtfrontend.exception.CDTParserNotYetSupportedException;
import fr.irisa.cairn.gecos.model.cdtfrontend.exception.CDTParserProblemException;
import fr.irisa.cairn.gecos.model.cdtfrontend.initcdt.GecosCDTSettingsProvider;
import fr.irisa.cairn.gecos.model.cdtfrontend.initcdt.InitCDTProject;
import fr.irisa.cairn.gecos.model.cdtfrontend.visitor.ASTStatementVisitor;
import fr.irisa.cairn.gecos.model.cdtfrontend.visitor.CDTToGecosModel;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.modules.AddSourceToGecosProject;
import fr.irisa.cairn.gecos.model.modules.CreateProject;
import fr.irisa.cairn.gecos.model.tools.controlflow.BuildControlFlow;
import fr.irisa.cairn.gecos.model.tools.utils.FixBlockNumbers;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;

@GSModule("Uses CDT to parse a C project and construct the corresponding\n"
		+ "GeCoS Internal Representation (CDFG).\n"
		+ "\n"
		+ "NOTE: It supports a very limited C++ syntax, mainly to allow the use\n"
		+ "   of templated types such as 'ac_datatypes' (e.g. ac_fixed).\n"
		+ "   In this case the definition of such types should not be included\n"
		+ "   in the source files, GeCoS defines them internally.\n"
		+ "\n"
		+ "Return: the constructed GecosProject.\n"
		+ "\nSee: 'CreateGecosProject' and 'CDTUseGecosStandardIncludes' modules.")
public class CDTFrontEnd {
	
	public static final Map<String,String> GECOS_DEFAULT_MACROS = new LinkedHashMap<>();
	static {
		GECOS_DEFAULT_MACROS.put("__BEGIN_DECLS", "");
		GECOS_DEFAULT_MACROS.put("__END_DECLS", "");
		GECOS_DEFAULT_MACROS.put("__BEGIN_NAMESPACE_STD", "");
		GECOS_DEFAULT_MACROS.put("____BEGIN_NAMESPACE_C99", "");
		GECOS_DEFAULT_MACROS.put("__END_NAMESPACE_STD", "");
		GECOS_DEFAULT_MACROS.put("__END_NAMESPACE_C99", "");
		GECOS_DEFAULT_MACROS.put("__USING_NAMESPACE_STD(FILE)", "");
	}
	
	public static final String PLUGIN_ID = "fr.irisa.cairn.gecos.model.cdtfrontend";

	public static final boolean VERBOSE = false;
	public static final boolean showError = false;
	public static final void debug(Object o) { if (VERBOSE) System.out.println(o); }

	public static final CCorePlugin cCorePlugin = CCorePlugin.getDefault();
	public static final CoreModel cdtCoreModel = CoreModel.getDefault();

	private static boolean useGecosStandardIncludes = true;
	public static void useGecosStandardIncludes(boolean u) { useGecosStandardIncludes = u; }
	public static boolean useGecosStandardIncludes() { return useGecosStandardIncludes; }

	private static boolean useCPlusPlusParser = false;
	public static void useCPlusPlusParser(boolean u) { useCPlusPlusParser = u; }
	public static boolean useCPlusPlusParser() { return useCPlusPlusParser; }
	
	private static boolean IGNORE_UNSUPPORTED = false;
	public static void ignoreUnsupported(boolean u) { IGNORE_UNSUPPORTED = u; }
	public static boolean ignoreUnsupported() { return IGNORE_UNSUPPORTED; }
	
	private static boolean ANNOTATE_FILE_LOCATIONS = false;
	public static void annotateFileLocations(boolean b) { ANNOTATE_FILE_LOCATIONS = b; }
	public static boolean annotateFileLocations() { return ANNOTATE_FILE_LOCATIONS; }
	
	private static boolean GENERATE_SIMPLE_FOR = true;
	public static void generateSimpleFor(boolean b) { GENERATE_SIMPLE_FOR = b; }
	public static boolean generateSimpleFor() { return GENERATE_SIMPLE_FOR; }
	
	final GecosProject gecosProject;
	final IProgressMonitor monitor = null;

	/***********************************
	 * 		Constructors
	 ***********************************/
	
	@GSModuleConstructor(""
		+ "-arg1: GecosProject to pass to the frontend.\n"
		+ "   See: 'CreateGecosProject' module."
		+ "\n-arg2: forces the use of C++ parser if set to 'true'."
		+ "\n-arg3: in case an 'Unsupported' operation is encountered\n"
		+ "   output an error message and continue if set to 'true',\n"
		+ "   Otherwise the command will fail."
		+ "\n-arg4: if true, annotate GecosNodes with their corresponding file locations."
		+ "\n-arg5: if false, do not generate SimpleForBlocks.")
	public CDTFrontEnd(GecosProject project, boolean useCppParser, boolean ignoreUnsupported, boolean annotateFileLocations, boolean generateSimpleForBlocks) {
		this.gecosProject = project;
		useCPlusPlusParser = useCppParser;
		IGNORE_UNSUPPORTED = ignoreUnsupported;
		ANNOTATE_FILE_LOCATIONS = annotateFileLocations;
		generateSimpleFor(generateSimpleForBlocks);
	}
	
	@GSModuleConstructor(""
			+ "Do generate SimpleForBlocks.")
	public CDTFrontEnd(GecosProject project, boolean useCppParser, boolean ignoreUnsupported, boolean annotateFileLocations) {
		this(project, useCppParser, ignoreUnsupported, false, true);
	}
	
	@GSModuleConstructor(""
			+ "Do not annotate with file locations.")
	public CDTFrontEnd(GecosProject project, boolean useCppParser, boolean ignoreUnsupported) {
		this(project, useCppParser, ignoreUnsupported, false);
	}
	
	@GSModuleConstructor(""
		+ "Do not ignore 'Unsupported' operations.")
	public CDTFrontEnd(GecosProject project, boolean useCppParser) {
		this(project, useCppParser, false, false);
	}
	
	@GSModuleConstructor(""
		+ "Do not force using C++ parser.")
	public CDTFrontEnd(GecosProject project) {
		this(project, false, false, false);
	}
	
	/**
	 * @Deprecated Use the class CDTFrontEndPS instead
	 */
	@Deprecated
	public CDTFrontEnd(GecosSourceFile file) {
		System.err.println("This constructor is deprecated. Consider using the class CDTFrontEndPS instead.");
		String name = GecosUserCoreFactory.DEFAULT_PROJECT_NAME;
		if (file.eContainer() != null && file.eContainer() instanceof GecosProject)
			name = ((GecosProject)file.eContainer()).getName();
		this.gecosProject = new CreateProject(name).compute();
		new AddSourceToGecosProject(this.gecosProject, file).compute();
	}

	/**
	 * @Deprecated Use the class CDTFrontEndPS instead
	 */
	@Deprecated
	public CDTFrontEnd(String file) {
		System.err.println("This constructor is deprecated. Consider using the class CDTFrontEndPS instead.");
		this.gecosProject = new CreateProject(GecosUserCoreFactory.DEFAULT_PROJECT_NAME).compute();
		try {
			new AddSourceToGecosProject(this.gecosProject, file).compute();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/***********************************
	 * 		COMPUTE
	 ***********************************/
	/**
	 * @throws CDTParserNotYetSupportedException if encountered a {@link IASTNode} for which
	 * build is not yet implemented (not a parsing error), unless {@link #ignoreUnsupported()}
	 * is set ({@code true}, in which case this exception is ignored.
	 * @throws CDTParserErrorException if a parsing error was encountered (i.e. the source code is not correct)
	 * @throws RuntimeException in case any other problem occurs
	 */
	public GecosProject compute() throws CDTParserNotYetSupportedException, CDTParserErrorException, RuntimeException {
		if (VERBOSE) System.out.println("init compute CDT");
		//reset counters when building a new project.
		ASTStatementVisitor.labelCounter = 0;
		GecosUserBlockFactory.resetBlocksNumberCounter();
		
		/**************************************
		 * Create CDT Core Project
		 **************************************/
		ICProject cProject = null;
		try {
			cProject = InitCDTProject.createCDTCProject(gecosProject);
		} catch (CoreException e) {
			throw new RuntimeException("Could not initialize CDT project.",e);
		}
		
		if (VERBOSE) System.out.println("Project created");
		
		try {
			if (VERBOSE) System.out.println("Building Gecos project "+gecosProject.getName());
			for (GecosSourceFile srcFile : gecosProject.getSources()) {
				parse(srcFile,cProject);
			}
			if (VERBOSE) System.out.println("CDT Parsing done");
			new BuildControlFlow(gecosProject).compute();
			new FixBlockNumbers(gecosProject).compute();
			if (VERBOSE) System.out.println("Fixing done");
		} catch (CDTParserNotYetSupportedException e) {
			if(CDTFrontEnd.ignoreUnsupported()) {
				String warningMsg =  "** WARNING: Error being ignored! This may result into incorrect internal representation! **\n";
				System.err.println(warningMsg + e.getMessage() + "\n" + e.getStackTrace()[0]);
			}
			else 
				throw e;
		} catch (CDTParserErrorException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(),e);
		} finally {
			try {
				close(cProject);
			} catch (CoreException e) {
				throw new RuntimeException("Could not close CDT project.");
			}
			if (VERBOSE) System.out.println("project closed");
		}
		
		PostProcess.process(gecosProject);
		return gecosProject;	
	}
	
	/**
	 * Parses a single {@link GecosSourceFile} using the CDT project setup from
	 * cProject. The built {@link ProcedureSet} is stored in the srcFile model
	 * attribute.
	 * 
	 * @param srcFile
	 * @param cProject
	 * @throws CoreException
	 */
	void parse(GecosSourceFile srcFile, ICProject cProject) throws CoreException {
		if (useCPlusPlusParser)
			parse_new(srcFile, cProject);
		else
			parse_old(srcFile, cProject);
	}
	
	@SuppressWarnings("restriction")
	void parse_new(GecosSourceFile srcFile, ICProject cProject) throws CoreException {
		File file = new File(srcFile.getName());
		debug(" - Parsing "+file.getName());
		IPath path = new Path(srcFile.getName());
		IProject project = cProject.getProject();
		CDTToGecosModel builder = new CDTToGecosModel(gecosProject ,cProject);
		builder.setEnableFileLocationAnnotations(ANNOTATE_FILE_LOCATIONS);
		builder.setGenerateSimpleForBlock(GENERATE_SIMPLE_FOR);
		
		// Force content type to C++ in order to use the C++ front end (for AC
		// Datatypes)
		String contentTypeId = CoreModel.getRegistedContentTypeId(project, ".cpp");
		
		ITranslationUnit tunit = new ExternalTranslationUnit(cProject, path, contentTypeId);
		
		if (tunit != null) {
			IASTTranslationUnit ast = tunit.getAST();

			//XXX : hack to retry 3 times initializing the front end
			int cnt = 0;
			while (ast == null) {
				try {
					debug("getAST = null, sleeping 200ms.");
					Thread.sleep(200);
					tunit = new ExternalTranslationUnit(cProject, path, contentTypeId);
					ast = tunit.getAST();
					if (cnt == 3) 
						throw new RuntimeException("Could not initialize CDT Frontend.");
					cnt++;
				} catch (InterruptedException e) {
					continue;
				}
			}
			try {
				ast.accept(builder);
			} catch (CDTParserProblemException e) {
				throw new CDTParserProblemException(e.node,"Parsing error : "+e.getMessage(),e);
			}
		}
		ProcedureSet procedureSet = builder.getProcedureSet();
		srcFile.setModel(procedureSet);
		procedureSet.getAnnotations().put(GecosUserAnnotationFactory.FILE_NAME_ANOTATION_KEY, GecosUserAnnotationFactory.STRING(srcFile.getName()));
		
	}
	void parse_old(GecosSourceFile srcFile, ICProject cProject) throws CoreException {
		File file = new File(srcFile.getName());
		debug(" - Parsing "+file.getName());
		IPath path = new Path(srcFile.getName());
		
		CDTToGecosModel builder = new CDTToGecosModel(gecosProject ,cProject);
		builder.setEnableFileLocationAnnotations(ANNOTATE_FILE_LOCATIONS);
		builder.setGenerateSimpleForBlock(GENERATE_SIMPLE_FOR);
		ITranslationUnit tunit = CDTFrontEnd.cdtCoreModel.createTranslationUnitFrom(cProject, path);
		
		if (tunit != null) {
			IASTTranslationUnit ast = tunit.getAST();

			//XXX : hack to retry 3 times initializing the front end
			int cnt = 0;
			while (ast == null) {
				try {
					debug("getAST = null, sleeping 200ms.");
					Thread.sleep(200);
					tunit = CDTFrontEnd.cdtCoreModel.createTranslationUnitFrom(cProject, path);
					ast = tunit.getAST();
					if (cnt == 3) 
						throw new RuntimeException("Could not initialize CDT Frontend.");
					cnt++;
				} catch (InterruptedException e) {
					continue;
				}
			}
			ast.accept(builder);
		}
		ProcedureSet procedureSet = builder.getProcedureSet();
		srcFile.setModel(procedureSet);
		procedureSet.getAnnotations().put(GecosUserAnnotationFactory.FILE_NAME_ANOTATION_KEY, GecosUserAnnotationFactory.STRING(srcFile.getName()));
		
	}

	/**
	 * Properly closes the CDT project.
	 * @param cProject
	 * @throws CoreException
	 */
	private void close(ICProject cProject) throws CoreException {
		cProject.getProject().close(monitor);
		cProject.close();
		try {
			cProject.getProject().delete(true, true, monitor);
		} catch (CoreException e) {
			throw new RuntimeException("Could not delete the project");
		}
		GecosCDTSettingsProvider.deleteSettings(gecosProject);
		cdtCoreModel.shutdown();
	}
}