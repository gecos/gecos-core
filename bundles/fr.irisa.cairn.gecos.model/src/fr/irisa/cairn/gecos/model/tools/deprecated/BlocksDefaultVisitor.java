package fr.irisa.cairn.gecos.model.tools.deprecated;

import fr.irisa.cairn.gecos.model.extensions.switchs.AdaptableBlockSwitch;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CaseBlock;
import gecos.blocks.CompositeBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.SwitchBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
/**
 * 
 * @author amorvan
 * @generated NOT
 * @deprecated use {@link fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDefaultVisitor}
 */
public abstract class BlocksDefaultVisitor extends AdaptableBlockSwitch<Object> {

	public Object doSwitch(ProcedureSet ps) {
		for (Procedure p : ps.listProcedures()) {
			doSwitch(p);
		}
		return true;
	}
	
	public Object doSwitch(Procedure proc) {
		doSwitch(proc.getStart());
		doSwitch(proc.getBody());
		doSwitch(proc.getEnd());
		return true;
	}
	
	@Override
	public Object caseBasicBlock(BasicBlock b) {
		//no children here
		return true;
	}

	@Override
	public Object caseCaseBlock(CaseBlock b) {
		doSwitch(b.getBody());
		return true;
	}

	@Override
	public Object caseCompositeBlock(CompositeBlock b) {
		for (Block child : b.getChildren().toArray(new Block[b.getChildren().size()])) {
			doSwitch(child);
		}
		return true;
	}

	@Override
	public Object caseForBlock(ForBlock b) {
		doSwitch(b.getInitBlock());
		doSwitch(b.getTestBlock());
		doSwitch(b.getStepBlock());
		doSwitch(b.getBodyBlock());
		return true;
	}

	@Override
	public Object caseIfBlock(IfBlock b) {
		doSwitch(b.getTestBlock());
		if (b.getThenBlock() != null) doSwitch(b.getThenBlock());
		if (b.getElseBlock() != null) doSwitch(b.getElseBlock());
		return true;
	}

	@Override
	public Object caseDoWhileBlock(DoWhileBlock b) {
		doSwitch(b.getTestBlock());
		doSwitch(b.getBodyBlock());
		return true;
	}

	@Override
	public Object caseSwitchBlock(SwitchBlock b) {
		if (b.getDispatchBlock() != null) doSwitch(b.getDispatchBlock());
		if (b.getBodyBlock() != null) doSwitch(b.getBodyBlock());
		if (b.getDefaultBlock() != null) doSwitch(b.getDefaultBlock());
		return true;
	}

	@Override
	public Object caseWhileBlock(WhileBlock b) {
		doSwitch(b.getTestBlock());
		doSwitch(b.getBodyBlock());
		return true;
	}
	

}
