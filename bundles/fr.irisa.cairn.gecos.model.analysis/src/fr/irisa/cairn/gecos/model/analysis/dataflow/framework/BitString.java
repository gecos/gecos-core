/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.analysis.dataflow.framework;


import gecos.annotations.impl.StringAnnotationImpl;

@SuppressWarnings("restriction")
public class BitString extends StringAnnotationImpl {

	public static final int O = 0;
	public static final int I = 1;
	public static final int U = 2;
	public static final int X = 3;

	private static final int[][] resolveSUP = {
	/* Z O U D */
	/* O */{ O, X, O, X },
	/* I */{ X, I, I, X },
	/* U */{ O, I, U, X },
	/* X */{ X, X, X, X } };

	private static final int[][] resolveINF = {
	/* O I U X */
	/* O */{ O, U, U, O },
	/* I */{ U, I, U, I },
	/* U */{ U, U, U, U },
	/* X */{ O, I, U, X } };

	private static final int[][] resolveAND = {
	/* O I U X */
	/* O */{ O, O, O, O },
	/* I */{ O, I, U, X },
	/* U */{ O, U, U, X },
	/* X */{ O, X, X, X } };

	private static final int[][] resolveOR = {
	/* O I U X */
	/* O */{ O, I, U, X },
	/* I */{ I, I, I, I },
	/* U */{ U, I, U, X },
	/* X */{ X, I, X, X } };

	private static final int[][] resolveXOR = {
	/* O I U X */
	/* O */{ O, I, U, X },
	/* I */{ I, O, U, X },
	/* U */{ U, U, U, X },
	/* X */{ X, X, X, X } };

	private static final char[] digits = { '0', '1', 'U', 'X' };

	private int[] values;

	public BitString(BitString b) {
		values = new int[b.values.length];
		for (int i = 0; i < values.length; ++i)
			values[i] = b.values[i];
	}

	public BitString(int size, int t) {
		values = new int[size];
		for (int i = 0; i < size; ++i)
			values[i] = t;
	}

	public BitString(int size) {
		this(size, U);
	}

	public int size() {
		return values.length;
	}

	public int bitAt(int i) {
		return values[i];
	}

	public boolean isConstant() {
		for (int i = 0; i < size(); ++i)
			if (values[i] == U || values[i] == X)
				return false;
		return true;
	}

	public int getWidth() {
		int cnt = 0;
		for (int i = 0; i < size(); ++i)
			if (values[i] == U)
				cnt++;
		return cnt;
	}

	public int value() {
		if (!isConstant())
			throw new RuntimeException("cannot get value");
		int value = 0;
		boolean neg = values[size() - 1] == I;
		for (int i = size() - 2; i >= 0; --i) {
			value = value << 1;
			if (!neg && values[i] == I)
				value = value | 1;
			if (neg && values[i] == O)
				value = value | 1;
		}
		if (neg)
			return -(value + 1);
		return value;
	}

	public BitString extend(int size) {
		if (size <= size())
			return this;
		BitString n = new BitString(size);
		int i;
		for (i = 0; i < size(); ++i)
			n.values[i] = values[i];
		if (size() > 0) {
			for (; i < size; ++i)
				n.values[i] = values[size() - 1];
		}
		return n;
	}

	public BitString trunc(int size) {
		if (size >= size())
			return this;
		BitString n = new BitString(size);
		int i;
		for (i = 0; i < size; ++i)
			n.values[i] = values[i];
		return n;
	}

	public boolean equals(BitString b) {
		int s = size() > b.size() ? size() : b.size();
		BitString aa = extend(s);
		BitString bb = b.extend(s);
		for (int i = 0; i < s; ++i)
			if (aa.values[i] != bb.values[i])
				return false;
		return true;
	}

	public BitString unionWith(BitString b) {
		return resolve(false, b, resolveSUP);
	}

	public BitString interWith(BitString b) {
		return resolve(true, b, resolveINF);
	}

	public BitString not() {
		BitString n = new BitString(size());
		for (int i = 0; i < size(); ++i)
			n.values[i] = values[i] == O ? I : values[i] == I ? O : values[i];
		return n;
	}

	public BitString and(BitString b) {
		return resolve(true, b, resolveAND);
	}

	public BitString or(BitString b) {
		return resolve(true, b, resolveOR);
	}

	public BitString xor(BitString b) {
		return resolve(true, b, resolveXOR);
	}

	private BitString resolve(boolean max, BitString b, int[][] table) {
		int s;
		if (max)
			s = size() > b.size() ? size() : b.size();
		else
			s = b.size() > size() ? size() : b.size();
		BitString aa = extend(s);
		BitString bb = b.extend(s);
		BitString n = new BitString(s);
		for (int i = 0; i < s; ++i)
			n.values[i] = table[aa.values[i]][bb.values[i]];
		return n;
	}

	private BitString concat(BitString b) {
		BitString r = b.extend(b.size() + size());
		for (int i = b.size(); i < r.size(); ++i)
			r.values[i] = values[i - b.size()];
		return r;
	}

	private int[][][][] resolveADD = { {
	/* A = O O I U */
	/* B = O */{ { O, O }, { O, I }, { O, U } },
	/* B = I */{ { O, I }, { I, O }, { U, U } },
	/* B = U */{ { O, U }, { U, U }, { U, U } },
	/* B = X */{ { O, O }, { O, I }, { O, U } } }, {
	/* A = I O I U */
	/* B = O */{ { O, I }, { I, O }, { U, U } },
	/* B = I */{ { I, O }, { I, I }, { I, U } },
	/* B = U */{ { U, U }, { U, U }, { U, U } },
	/* B = X */{ { O, I }, { I, O }, { O, U } } }, {
	/* A = U O I U */
	/* B = O */{ { O, U }, { U, U }, { U, U } },
	/* B = I */{ { U, U }, { U, U }, { U, U } },
	/* B = U */{ { U, U }, { U, U }, { U, U } },
	/* B = X */{ { U, U }, { U, U }, { U, U } } }, {
	/* A = X O I U */
	/* B = O */{ { O, O }, { O, I }, { O, U } },
	/* B = I */{ { O, I }, { I, O }, { U, U } },
	/* B = U */{ { O, U }, { U, U }, { U, U } },
	/* B = X */{ { O, X }, { I, O }, { O, U } } } };

//	private int[][][][] resolveMUL = { {
//		/* A = O O I U */
//		/* B = O */{ { O, O }, { O, I }, { O, U } },
//		/* B = I */{ { O, I }, { I, O }, { U, U } },
//		/* B = U */{ { O, U }, { U, U }, { U, U } },
//		/* B = X */{ { O, O }, { O, I }, { O, U } } }, {
//		/* A = I O I U */
//		/* B = O */{ { O, I }, { I, O }, { U, U } },
//		/* B = I */{ { I, O }, { I, I }, { I, U } },
//		/* B = U */{ { U, U }, { U, U }, { U, U } },
//		/* B = X */{ { O, I }, { I, O }, { O, U } } }, {
//		/* A = U O I U */
//		/* B = O */{ { O, U }, { U, U }, { U, U } },
//		/* B = I */{ { U, U }, { U, U }, { U, U } },
//		/* B = U */{ { U, U }, { U, U }, { U, U } },
//		/* B = X */{ { U, U }, { U, U }, { U, U } } }, {
//		/* A = X O I U */
//		/* B = O */{ { O, O }, { O, I }, { O, U } },
//		/* B = I */{ { O, I }, { I, O }, { U, U } },
//		/* B = U */{ { O, U }, { U, U }, { U, U } },
//		/* B = X */{ { O, X }, { I, O }, { O, U } } } };

	private int[][][][] resolveSUB = { {
	/* A = O O I U */
	/* B = O */{ { O, O }, { I, I }, { U, U } },
	/* B = I */{ { I, I }, { I, O }, { I, U } },
	/* B = U */{ { U, U }, { I, U }, { U, U } },
	/* B = X */{ { O, O }, { O, I }, { U, U } } }, {
	/* A = I O I U */
	/* B = O */{ { O, I }, { O, O }, { O, U } },
	/* B = I */{ { O, O }, { I, I }, { U, U } },
	/* B = U */{ { O, U }, { U, U }, { U, U } },
	/* B = X */{ { O, I }, { I, O }, { O, U } } }, {
	/* A = U O I U */
	/* B = O */{ { O, U }, { U, U }, { U, U } },
	/* B = I */{ { U, U }, { I, U }, { U, U } },
	/* B = U */{ { U, U }, { U, U }, { U, U } },
	/* B = X */{ { O, U }, { U, U }, { U, U } } }, {
	/* A = X O I U */
	/* B = O */{ { O, O }, { O, O }, { O, O } },
	/* B = I */{ { O, O }, { I, O }, { U, U } },
	/* B = U */{ { O, O }, { O, U }, { U, U } },
	/* B = X */{ { O, X }, { O, X }, { O, X } } } };

	private BitString resolveBinary(int c, BitString b, int[][][][] table) {
		int s = size() > b.size() ? size() : b.size();
		++s;
		BitString aa = extend(s);
		BitString bb = b.extend(s);
		BitString n = new BitString(s);
		for (int i = 0; i < s; ++i) {
			int[] tab = table[aa.values[i]][bb.values[i]][c];
			n.values[i] = tab[1];
			c = tab[0];
		}
		return n;
	}

	public BitString add(BitString b) {
		return resolveBinary(O, b, resolveADD);
	}

	public BitString mul(int coef) {
		int add = Integer.bitCount(coef);
		int msb=size()-1;
		while (msb>0 && values[msb]=='O') msb--;
		int s = msb + add;
		BitString n = new BitString(s);
		for (int i = 0; i < n.values.length; i++) {
			n.values[i]='U';
		}
		return n;
	}
	// FIXME
	public BitString mul(BitString b) {
		int msb=size()-1;
		
		while (msb>0 && values[msb]==O) {
			msb--;
		}
		
		int msbb=b.size()-1;
		while (msbb>0 && b.values[msbb]==O) {
			msbb--;
		}
		int s = msb + msbb;
		BitString n = new BitString(s);
		for (int i = 0; i < n.values.length; i++) {
			n.values[i]=U;
		}
		return n;
	}

	public BitString sub(BitString b) {
		return resolveBinary(O, b, resolveSUB);
	}

	public BitString asl(BitString v) {
		int s = v.isConstant() ? v.value() : 1 << v.size();
		return concat(new BitString(s, O));
	}

	public BitString asr(BitString v) {
		int s = v.isConstant() ? v.value() : v.size();
		if (size() - s <= 0)
			return BitString.fromInt(O);
		BitString n = new BitString(size() - s);
		for (int i = 0; i < n.size(); ++i)
			n.values[i] = (i + s) < size() ? values[i + s] : O;
		return n;
	}

	public BitString lsl(BitString v) {
		int s = v.isConstant() ? v.value() : v.size();
		BitString n = new BitString(size() + s);
		for (int i = 0; i < n.size(); ++i)
			n.values[i] = i < s ? 0 : values[i - s];
		return n;
	}

	public BitString neq(BitString b) {
//		boolean neq = false;
		int s = size() > b.size() ? size() : b.size();
		BitString aa = extend(s);
		BitString bb = b.extend(s);
		int i;
		for (i = s - 1; i >= 0; --i) {
			if (aa.values[i] == U || bb.values[i] == U)
				return new BitString(1, U);
			/*
			 * if (aa.values[i] == X || bb.values[i] == X) return
			 * BitString.fromInt(0);
			 */
			if (aa.values[i] != bb.values[i])
				break;
		}
		return BitString.fromInt((i >= 0) ? 1 : 0);
	}

	public BitString lower(BitString b) {
		int s = size() > b.size() ? size() : b.size();
		BitString aa = extend(s);
		BitString bb = b.extend(s);
		for (int i = s - 1; i >= 0; --i) {
			if (aa.values[i] == U || bb.values[i] == U)
				return new BitString(1, U);
			if (aa.values[i] == O && bb.values[i] == I)
				return BitString.fromInt(1);
			if (aa.values[i] == I && bb.values[i] == O)
				return BitString.fromInt(0);
		}
		return BitString.fromInt(0);
	}

	// Inverse

	public BitString inverseAsr(BitString v, int maxSize) {
		int s = v.isConstant() ? v.value() : 1 << v.size();
		return concat(new BitString(s, X)).trunc(maxSize);
	}

	public BitString inverseLsl(BitString v, int maxSize) {
		int s = v.isConstant() ? v.value() : 1 << v.size();
		return new BitString(s, X).concat(this).trunc(maxSize);
	}

	private static final int[][] resolveEQ = {
	/* O I U X */
	/* O */{ X, O, O, O },
	/* I */{ I, X, I, I },
	/* U */{ U, U, U, U },
	/* X */{ X, X, X, X } };

	public BitString dontCareOfEqual(BitString b) {
		return resolve(true, b, resolveEQ);
	}

	public BitString dontCareOf(boolean onlyMost, BitString b, int t) {
		int s = size() > b.size() ? size() : b.size();
		BitString aa = extend(s);
		BitString bb = b.extend(s);
		BitString n = new BitString(s);
		boolean stop = false;
		for (int i = s - 1; i >= 0; --i) {
			if (bb.values[i] == t && (!(stop && onlyMost)))
				n.values[i] = X;
			else {
				n.values[i] = aa.values[i];
				stop = true;
			}
		}
		return n;
	}

	public BitString dontCareOfZERO(BitString b) {
		return dontCareOf(false, b, O);
	}

	public BitString dontCareOfONE(BitString b) {
		return dontCareOf(false, b, I);
	}

	public BitString dontCareOfDC(BitString b) {
		return dontCareOf(false, b, X);
	}

	public BitString dontCareOfMostDC(BitString b) {
		return dontCareOf(true, b, X);
	}

	public BitString[] expand(int t) {
		int n = 1;
		for (int i = 0; i < size(); ++i)
			if (values[i] == t)
				n <<= 1;
		BitString[] res = new BitString[n];
		fillExpand(res, 0, t);
		return res;
	}

	private int fillExpand(BitString[] res, int x, int t) {
		int y = x;
		for (int i = 0; i < size(); ++i) {
			if (values[i] == t) {
				BitString n = copy();
				n.values[i] = O;
				y = n.fillExpand(res, y, t);
				n = copy();
				n.values[i] = I;
				y = n.fillExpand(res, y, t);
				break;
			}
		}
		if (x == y) {
			res[x] = this;
			return x + 1;
		}
		return y;
	}

	public BitString copy() {
		BitString n = new BitString(size());
		for (int i = 0; i < size(); ++i)
			n.values[i] = values[i];
		return n;
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append('<');
		for (int i = size() - 1; i >= 0; --i)
			buffer.append(digits[values[i]]);
		buffer.append('>');
		return buffer.toString();
	}

	public static BitString fromInt(int value) {
		boolean neg = false;
		if (value < 0) {
			value = -value;
			neg = true;
		}
		int size = 0;
		int t = value;
		while (t != 0) {
			t >>= 1;
			++size;
		}
		++size; // sign bit
		BitString n = new BitString(size);
		if (neg)
			value = -value;
		for (int i = 0; i < size; ++i)
			n.values[i] = (value >> i) & 1;
		return n;
	}

	public static BitString fromString(String str) {
		BitString n = new BitString(str.length());
		for (int i = 0; i < str.length(); ++i)
			switch (str.charAt(i)) {
			case '0':
				n.values[str.length() - i - 1] = O;
				break;
			case '1':
				n.values[str.length() - i - 1] = I;
				break;
			case 'U':
				n.values[str.length() - i - 1] = U;
				break;
			case 'X':
				n.values[str.length() - i - 1] = X;
				break;
			}
		return n;
	}

	public static void main(String[] args) {
//		BitString a = BitString.fromInt(6);
//		BitString b = BitString.fromInt(1);
		BitString c = BitString.fromString("UUUU");
		BitString d = BitString.fromString("XXUU");
		/*
		 * BitString[] expb = b.expand(UND); for (int i = 0; i < expb.length;
		 * ++i) System.out.println(expb[i]);
		 */

		System.out.println(c.dontCareOfMostDC(d));
	}
}
