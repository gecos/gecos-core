package fr.irisa.cairn.gecos.model.extensions.outputs;

import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluable;
import fr.irisa.r2d2.gecos.framework.script.IScriptEvaluator;
import fr.irisa.r2d2.gecos.framework.script.ScriptException;
import fr.irisa.r2d2.gecos.framework.script.nodes.Node;
import fr.irisa.r2d2.gecos.framework.script.nodes.NullNode;
import fr.irisa.r2d2.parser.runtime.Span;

/**
 * Gecos output primitive. It automatically detect the printer using file
 * extension type.<br>
 * <br>
 * Default dot output:<code>
 * output(object)
 * </code> <br>
 * Output for specified format at a given path: <code>
 * output(object,format,path)
 * </code><br>
 * 
 * @author Antoine Floc'h - Initial Contribution and API
 * 
 */
public class Output implements IScriptEvaluable {
	private final static String DEFAULT_FORMAT = "dot";
	public final static int OBJECT_ARG = 0;
	private final static int FORMAT_ARG = 1;
	private final static int PATH_ARG = 2;

	public Output() {
	}

	public Node apply(IScriptEvaluator evaluator, Span span, Node[] args)
			throws ScriptException {
		if (args.length == 0)
			throw new ScriptException("output : expect at least 1 argument");

		Object obj = args[0].asObject();

		String file = "";

		if (args.length > 2)
			file = args[PATH_ARG].asString().value();
		if (file.isEmpty())
			file = ".";

		String format = DEFAULT_FORMAT;
		if (args.length > 1) {
			format = args[FORMAT_ARG].asString().value();
		}

		GecosOutput printer = GecosOutputRegistry.INSTANCE.getOutputPrinter(
				obj, format);
		if (printer == null) {
			throw new ScriptException("output : there is no available "
					+ format + " printer for " + obj);
		}
		Object[] additionnalArgs = additionnalArgs(args);
		printer.print(obj, file, additionnalArgs);
		return new NullNode(span);
	}

	private Object[] additionnalArgs(Node[] a) {
		int end = a.length;
		int start = 1;
		if (end >= 2) {
			start++;
		}
		if (end >= 3) {
			start++;
		}

		Object[] res = new Object[end - start];
		if (end - start > 0) {
			for (int i = 0; i < end - start; i++) {
				res[i] = a[start + i].asObject();
			}
		}
		return res;
	}
}
