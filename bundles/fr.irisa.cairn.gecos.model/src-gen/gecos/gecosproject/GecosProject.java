/**
 */
package gecos.gecosproject;

import gecos.annotations.AnnotatedElement;

import gecos.core.Procedure;
import gecos.core.ProcedureSet;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Gecos Project</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.gecosproject.GecosProject#getName <em>Name</em>}</li>
 *   <li>{@link gecos.gecosproject.GecosProject#getIncludes <em>Includes</em>}</li>
 *   <li>{@link gecos.gecosproject.GecosProject#getSources <em>Sources</em>}</li>
 *   <li>{@link gecos.gecosproject.GecosProject#getMacros <em>Macros</em>}</li>
 * </ul>
 *
 * @see gecos.gecosproject.GecosprojectPackage#getGecosProject()
 * @model
 * @generated
 */
public interface GecosProject extends ProjectVisitable, AnnotatedElement {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * The default value is <code>"gecos_project"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see gecos.gecosproject.GecosprojectPackage#getGecosProject_Name()
	 * @model default="gecos_project" unique="false" dataType="gecos.gecosproject.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link gecos.gecosproject.GecosProject#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Includes</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.gecosproject.GecosHeaderDirectory}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Includes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Includes</em>' containment reference list.
	 * @see gecos.gecosproject.GecosprojectPackage#getGecosProject_Includes()
	 * @model containment="true"
	 * @generated
	 */
	EList<GecosHeaderDirectory> getIncludes();

	/**
	 * Returns the value of the '<em><b>Sources</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.gecosproject.GecosSourceFile}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sources</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sources</em>' containment reference list.
	 * @see gecos.gecosproject.GecosprojectPackage#getGecosProject_Sources()
	 * @model containment="true"
	 * @generated
	 */
	EList<GecosSourceFile> getSources();

	/**
	 * Returns the value of the '<em><b>Macros</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Macros</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Macros</em>' map.
	 * @see gecos.gecosproject.GecosprojectPackage#getGecosProject_Macros()
	 * @model mapType="gecos.gecosproject.StringToStringMap&lt;gecos.gecosproject.String, gecos.gecosproject.String&gt;"
	 * @generated
	 */
	EMap<String, String> getMacros();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return an unmodifiable view to the list of (non-null) {@link ProcedureSet}s
	 * contained in the sources of this project. In case non such {@link ProcedureSet}
	 * exists an empty list is returned (never null)
	 * <!-- end-model-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.gecosproject.GecosSourceFile%&gt;, &lt;%gecos.core.ProcedureSet%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.gecosproject.GecosSourceFile%&gt;, &lt;%gecos.core.ProcedureSet%&gt;&gt;()\n{\n\tpublic &lt;%gecos.core.ProcedureSet%&gt; apply(final &lt;%gecos.gecosproject.GecosSourceFile%&gt; it)\n\t{\n\t\treturn it.getModel();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.core.ProcedureSet%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.core.ProcedureSet%&gt;&gt;asEList(((&lt;%gecos.core.ProcedureSet%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.core.ProcedureSet%&gt;&gt;filterNull(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%gecos.gecosproject.GecosSourceFile%&gt;, &lt;%gecos.core.ProcedureSet%&gt;&gt;map(this.getSources(), _function)), &lt;%gecos.core.ProcedureSet%&gt;.class))));'"
	 * @generated
	 */
	EList<ProcedureSet> listProcedureSets();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.core.ProcedureSet%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.core.Procedure%&gt;&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.core.ProcedureSet%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.core.Procedure%&gt;&gt;&gt;()\n{\n\tpublic &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.core.Procedure%&gt;&gt; apply(final &lt;%gecos.core.ProcedureSet%&gt; it)\n\t{\n\t\treturn it.listProcedures();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.core.Procedure%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.core.Procedure%&gt;&gt;asEList(((&lt;%gecos.core.Procedure%&gt;[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(&lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.core.Procedure%&gt;&gt;filterNull(&lt;%com.google.common.collect.Iterables%&gt;.&lt;&lt;%gecos.core.Procedure%&gt;&gt;concat(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%gecos.core.ProcedureSet%&gt;, &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%gecos.core.Procedure%&gt;&gt;&gt;map(this.listProcedureSets(), _function))), &lt;%gecos.core.Procedure%&gt;.class))));'"
	 * @generated
	 */
	EList<Procedure> listProcedures();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return false if any source is not parsed.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.gecosproject.GecosSourceFile%&gt;, &lt;%java.lang.Boolean%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.gecosproject.GecosSourceFile%&gt;, &lt;%java.lang.Boolean%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Boolean%&gt; apply(final &lt;%gecos.gecosproject.GecosSourceFile%&gt; it)\n\t{\n\t\treturn &lt;%java.lang.Boolean%&gt;.valueOf(it.isParsed());\n\t}\n};\nreturn &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.gecosproject.GecosSourceFile%&gt;&gt;forall(this.getSources(), _function);'"
	 * @generated
	 */
	boolean isParsed();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitProject(this);'"
	 * @generated
	 */
	void accept(ProjectVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.gecosproject.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getName();'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%fr.irisa.cairn.gecos.model.tools.utils.GecosCopier%&gt; gecosCopier = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.GecosCopier%&gt;();\n&lt;%org.eclipse.emf.ecore.EObject%&gt; _copy = gecosCopier.copy(this);\nfinal &lt;%gecos.gecosproject.GecosProject%&gt; copy = ((&lt;%gecos.gecosproject.GecosProject%&gt;) _copy);\ngecosCopier.copyReferences();\nreturn copy;'"
	 * @generated
	 */
	GecosProject copy();

} // GecosProject
