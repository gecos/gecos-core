package fr.irisa.cairn.gecos.model.extensions.typesystem;

import java.lang.reflect.Constructor;

/**
 * Factory for the extensions of code generators. 
 * 
 * @author Antoine Floc'h - Initial contribution and API
 * 
 */
final class TypeSystemExtensionFactory {

	ITypeSystemRuleSet buildGenerator(Class<? extends ITypeSystemRuleSet> generatorClass) {

		Constructor<Object> constructor = getDefaultConstructor(generatorClass);
		try {
			Object generator = constructor.newInstance();
			return (ITypeSystemRuleSet) generator;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Constructor<Object> getDefaultConstructor(Class generatorClass) {
		Constructor<Object>[] declaredConstructors = (Constructor<Object>[]) generatorClass
				.getDeclaredConstructors();
		for (Constructor<Object> constructor : declaredConstructors) {
			Class<?>[] parameterTypes = constructor.getParameterTypes();
			if (parameterTypes.length == 0) {
					return constructor;
			}
		}
		throw new IllegalArgumentException(
				"No matching constructor in the generator ("
						+ generatorClass.getCanonicalName() + ")");
	}
}