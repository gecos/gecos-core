package fr.irisa.cairn.gecos.model.dag.adapter.observers;


public class DagAdapterCoherencyException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5707050138776112180L;

	public DagAdapterCoherencyException() {
		super();
	}

	public DagAdapterCoherencyException(String message, Throwable cause) {
		super(message, cause);
	}

	public DagAdapterCoherencyException(String message) {
		super(message);
	}

	public DagAdapterCoherencyException(Throwable cause) {
		super(cause);
	}

}
