/**
 */
package gecos.instrs;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ret Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.instrs.InstrsPackage#getRetInstruction()
 * @model
 * @generated
 */
public interface RetInstruction extends ExprComponent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitRetInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _elvis = null;\n&lt;%gecos.instrs.Instruction%&gt; _expr = this.getExpr();\n&lt;%java.lang.String%&gt; _string = null;\nif (_expr!=null)\n{\n\t_string=_expr.toString();\n}\nif (_string != null)\n{\n\t_elvis = _string;\n} else\n{\n\t_elvis = \"?\";\n}\n&lt;%java.lang.String%&gt; _plus = (\"return \" + _elvis);\nreturn (_plus + \";\");'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return true;'"
	 * @generated
	 */
	boolean isRet();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.RetInstruction%&gt;))\n{\n\treturn super.isSame(instr);\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // RetInstruction
