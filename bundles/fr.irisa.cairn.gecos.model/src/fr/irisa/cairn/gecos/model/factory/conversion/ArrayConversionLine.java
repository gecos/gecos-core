/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.factory.conversion;

import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import gecos.types.ArrayType;
import gecos.types.BaseType;
import gecos.types.PtrType;
import gecos.types.Type;

public class ArrayConversionLine extends ConversionLine {

	/* ========================================================================
	 *        | char   | short  | int    | long   | longlong | float  | double
	 * ========================================================================
	 * char   | char   | short  | int    | long   | longlong | float  | double
	 * short  | short  | short  | int    | long   | longlong | float  | double
	 * int    | int    | int    | int    | long   | longlong | float  | double
	 * long   | long   | long   | long   | long   | longlong | float  | double
	 * longl  | longl  | longl  | longl  | longl  | longlong | float  | double
	 * float  | float  | float  | float  | float  | double   | float  | double
	 * double | double | double | double | double | double   | double | double
	 */
	private ArrayType baseType;

	public ArrayConversionLine(ArrayType type) {
		baseType = type;
	}
	
	public ArrayType getBaseType() {
		return baseType;
	}

	public Object caseBaseType(BaseType t) {
		GecosUserTypeFactory.setScope(t.getContainingScope());
		Type commonBaseType = ConversionMatrix.getCommon(t, baseType.getBase());
		Object commonType;
		if (commonBaseType!=null) {
			GecosUserTypeFactory.setScope(commonBaseType.getContainingScope());
			commonType = GecosUserTypeFactory.PTR(commonBaseType);
		} else {
			commonType = null;
		}
		return commonType;
	}

	@Override
	public Object casePtrType(PtrType t) {
		// XXX : There was an issue with array/pointer comparison, I believe that this should solve the problem
		Type commonBaseType = ConversionMatrix.getCommon(t.getBase(), baseType.getBase());
		PtrType commonType;
		if (commonBaseType!=null) {
			GecosUserTypeFactory.setScope(commonBaseType.getContainingScope());
			commonType = GecosUserTypeFactory.PTR(commonBaseType);
		} else {
			commonType = null;
		}
		return commonType;
	}

	@Override
	public Object caseArrayType(ArrayType t) {
		// no Array to Array base type ?
		Type commonBaseType = ConversionMatrix.getCommon(t.getBase(), baseType.getBase());
		PtrType commonType;
		if (commonBaseType!=null) {;
			// FIXME : When Array basetype is the same, the commonType 
			// should be a pointer to an array, not the array itself, since 
			// the arrays may have different sizes.
			GecosUserTypeFactory.setScope(commonBaseType.getContainingScope());
			commonType = GecosUserTypeFactory.PTR(commonBaseType);
			
		} else {
			commonType = null;	
		}
		return commonType;
	}

}
