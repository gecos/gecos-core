package fr.irisa.cairn.gecos.model.factory;


import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.Int;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.add;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.lt;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.set;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.symbref;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksFactory;
import gecos.blocks.CaseBlock;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ControlEdge;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.ForC99Block;
import gecos.blocks.IfBlock;
import gecos.blocks.SimpleForBlock;
import gecos.blocks.SwitchBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.BranchType;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.SymbolInstruction;

public class GecosUserBlockFactory {

	private static BlocksFactory factory = BlocksFactory.eINSTANCE;
	
	private GecosUserBlockFactory() {}
	
	private static int bbidx = 1;
	
	private static BasicBlock createBasicBlock() {
		BasicBlock b = factory.createBasicBlock();
		b.setNumber(bbidx++);
		return b;
	}
	
	public static void resetBlocksNumberCounter() {
		bbidx = 1;
	}
	
	
	
	public static CompositeBlock CompositeBlock(Block... b ) {
		return CompositeBlock(Arrays.asList(b));
	}
	public static CompositeBlock CompositeBlock(List<Block> list) {
		return CompositeBlock(GecosUserCoreFactory.scope(), list);
	}
	public static CompositeBlock CompositeBlock(Scope scope, Block... b) {
		return CompositeBlock(scope, Arrays.asList(b));
	}
	public static CompositeBlock CompositeBlock(Scope scope, List<Block> list) {
		if (scope == null)
			throw new IllegalArgumentException("the scope of a compositeblock cannot be null");
		CompositeBlock cb = factory.createCompositeBlock();
		cb.setScope(scope);
		
		for (Block blk : list) {
			cb.getChildren().add(blk);
		}
		return cb;
		
	}
	
	public static BasicBlock BBlock(Instruction ... a) {
		BasicBlock bb = createBasicBlock();
		for (Instruction instruction : a) {
			bb.getInstructions().add(instruction);
		}
		return bb;
	}

	public static BasicBlock BBlock(List<Instruction > a) {
		BasicBlock bb = createBasicBlock();
		bb.getInstructions().addAll(a);
		return bb;
	}
	
	public static BasicBlock BBlock(int number ) {
		BasicBlock bb = createBasicBlock();
		bb.setNumber(number);
		return bb;
	}


	public static ForBlock For(BasicBlock init, BasicBlock test, BasicBlock step) {
		ForBlock forBlock = factory.createForBlock();
		forBlock.setTestBlock(test);
		forBlock.setInitBlock(init);
		forBlock.setStepBlock(step);
		return forBlock;
	}

	public static ForBlock For(BasicBlock init, BasicBlock test, BasicBlock step, Block body) {
		ForBlock forBlock = For(init,test,step);
		forBlock.setBodyBlock(body);
		return forBlock;
	}

	public static ForBlock For(Instruction init, Instruction test, Instruction step, CompositeBlock body) {
		ForBlock forBlock = factory.createForBlock();
		forBlock.setBodyBlock(body);
		forBlock.setTestBlock(BBlock(test));
		forBlock.setInitBlock(BBlock(init));
		forBlock.setStepBlock(BBlock(step));
		return forBlock;
	}

	public static ForBlock For(Symbol iterator, int lb, int up, int step, Block body) {
		ForBlock forBlock = For(iterator,lb,up,step);
		forBlock.setBodyBlock(body);
		return forBlock;
	}

	public static ForBlock For(Symbol iterator, Instruction lb, Instruction ub, Instruction step) {
		ForBlock forBlock = factory.createForBlock();
		forBlock.setInitBlock(BBlock(set((symbref(iterator)),lb)));
		forBlock.setStepBlock(BBlock(set((symbref(iterator)),add(symbref(iterator),step))));
		
		forBlock.setTestBlock(BBlock(GecosUserInstructionFactory.condBranch(lt((symbref(iterator)),ub),"L1")));
		
		return forBlock;
	}

	public static SimpleForBlock SimpleFor(Block body, BasicBlock init, BasicBlock test, BasicBlock step, Symbol iterator, Instruction lb, Instruction ub, Instruction stride, Scope scope) {
		SimpleForBlock simpleForBlock = factory.createSimpleForBlock();
		simpleForBlock.setBodyBlock(body);
		simpleForBlock.setInitBlock(init);
		simpleForBlock.setTestBlock(test);
		simpleForBlock.setStepBlock(step);
		simpleForBlock.setIterator(iterator);
		simpleForBlock.setLb(lb);
		simpleForBlock.setUb(ub);
		simpleForBlock.setStride(stride);
		simpleForBlock.setScope(scope);
		
		return simpleForBlock;
	}

	public static SimpleForBlock SimpleFor(ForBlock originalForBlock, Symbol iterator, Instruction lb, Instruction ub, Instruction stride, Scope scope) {
		SimpleForBlock simpleForBlock = SimpleFor(
				originalForBlock.getBodyBlock(), originalForBlock.getInitBlock(), originalForBlock.getTestBlock(), originalForBlock.getStepBlock(),
				iterator,lb,ub,stride,scope);
		simpleForBlock.setNumber(originalForBlock.getNumber());
		simpleForBlock.getAnnotations().addAll(originalForBlock.getAnnotations());
		return simpleForBlock;
	}

	public static ForC99Block ForC99(BasicBlock init, BasicBlock test, BasicBlock step, Block body, Scope scope) {
		ForC99Block forC99Block = factory.createForC99Block();
		forC99Block.setInitBlock(init);
		forC99Block.setTestBlock(test);
		forC99Block.setStepBlock(step);
		forC99Block.setBodyBlock(body);
		forC99Block.setScope(scope);
		return forC99Block;
	}
	
	public static ForBlock For(Symbol iterator, Instruction lb, Instruction ub, Instruction step, Block body) {
		ForBlock for1 = For(iterator,lb,ub,step);
		for1.setBodyBlock(body);
		return for1;
	}

	public static ForBlock For(Symbol iterator, long lb, Instruction ub, long step, Block body) {
		ForBlock for1 = For(iterator,Int(lb),ub,Int(step));
		for1.setBodyBlock(body);
		return for1;
	}

	public static ForBlock For(Symbol iterator, Instruction lb, Instruction ub, int step) {
		return For(iterator,lb,ub,Int(step));
	}

	public static ForBlock For(Symbol iterator, long lowerBound, long upperBound, long step ) {
		ForBlock forBlock = For(iterator,Int(lowerBound),Int(upperBound),Int(step));
		return forBlock;
	}


	public static IfBlock IfThen(Instruction cond, Block then) {
		return IfThenElse(BBlock(cond), then,null);
	}

	public static IfBlock IfEqThen(Symbol sym, int value, Block then, Scope scope) {
		SymbolInstruction symbref = GecosUserInstructionFactory.symbref(sym);
		IntInstruction intInstr = GecosUserInstructionFactory.Int(value);
		return IfThenElse(BBlock(GecosUserInstructionFactory.eq(symbref,intInstr)),then,null);
	}

	public static IfBlock IfThen(BasicBlock cond, Block then) {
		return IfThenElse(cond, then,null);
	}

	public static IfBlock IfThenElse(Instruction cond, Block then, Block elseb) {
		return IfThenElse(BBlock(cond), then,elseb);
	}


	public static IfBlock IfThenElse(BasicBlock cond, Block thenBlock, Block elseBlock) {
		IfBlock ifBlock = factory.createIfBlock();
		ifBlock.setTestBlock(cond);
		ifBlock.setThenBlock(thenBlock);
		ifBlock.setElseBlock(elseBlock);
		return ifBlock;
	}

	public static WhileBlock While(BasicBlock cond, Block body) {
		WhileBlock whileBlock = factory.createWhileBlock();
		whileBlock.setTestBlock(cond);
		whileBlock.setBodyBlock(body);
		return whileBlock;
	}

	public static WhileBlock While(BasicBlock cond) {
		WhileBlock whileBlock = factory.createWhileBlock();
		whileBlock.setTestBlock(cond);
		return whileBlock;
	}

	public static WhileBlock While(Instruction cond, Block body) {
		return While(BBlock(cond),body);
	}

	public static DoWhileBlock DoWhile(BasicBlock cond, Block body) {
		DoWhileBlock whileBlock = factory.createDoWhileBlock();
		whileBlock.setTestBlock(cond);
		whileBlock.setBodyBlock(body);
		return whileBlock;
	}

	public static DoWhileBlock DoWhile(BasicBlock cond) {
		DoWhileBlock whileBlock = factory.createDoWhileBlock();
		whileBlock.setTestBlock(cond);
		return whileBlock;
	}

	public static DoWhileBlock DoWhile(Instruction cond, Block body) {
		return DoWhile(BBlock(cond),body);
	}


	public static void connectBB(BasicBlock src, BasicBlock dst) {
		ControlEdge ce  = factory.createControlEdge();
		ce.setFrom(src);
		ce.setTo(dst);
	}

	public static SwitchBlock Switch(Symbol choice, Block ...cases) {
		return Switch(symbref(choice), cases);
	}



	public static SwitchBlock Switch(Instruction inst, Block ...cases) {
		 return Switch(inst,Arrays.asList(cases)); 
	}
	
	public static SwitchBlock Switch(Instruction inst, Collection<Block> cases) {
		SwitchBlock switchBlock = factory.createSwitchBlock();
		CompositeBlock cb = GecosUserBlockFactory.CompositeBlock();
		BasicBlock dispatch = BBlock(inst);
		switchBlock.setDispatchBlock(dispatch);
		switchBlock.setBodyBlock(cb);
		long position=0;
		
		for (Block block : cases) {
			switchBlock.getCases().put(position++, block) ;
			cb.getChildren().add(block);
			block.connectFromBasic(dispatch, BranchType.DISPATCH);
		}
		BasicBlock defaultb = BBlock();
		cb.getChildren().add(defaultb);
		switchBlock.setDefaultBlock(defaultb);
		defaultb.connectFromBasic(dispatch, BranchType.DISPATCH);
		return switchBlock ;
	}

	public static SwitchBlock Switch(Instruction choice) {
		SwitchBlock switchBlock = factory.createSwitchBlock();

		BasicBlock dispatch = BBlock(choice);
		switchBlock.setDispatchBlock(dispatch);
		return switchBlock;
	}

	public static SwitchBlock Switch(Block ...blocks) {
		SwitchBlock switchBlock = factory.createSwitchBlock();
		CompositeBlock cb = CompositeBlock();
		BasicBlock dispatch = (BasicBlock) blocks[0];
		switchBlock.setDispatchBlock(dispatch);
		long position=0;
		
		boolean first =true;
		
		for (Block block : blocks) {
			if (first) {
				first =false;
			} else {
				switchBlock.getCases().put(position++, block) ;
				cb.getChildren().add(block);
				block.connectFromBasic(dispatch, BranchType.DISPATCH);
			}
		}
		BasicBlock defaultb = BBlock();
		switchBlock.setDefaultBlock(BBlock());
		defaultb.connectFromBasic(dispatch, BranchType.DISPATCH);
		return switchBlock ;
	}

	public static CaseBlock Case(int pos, Block body) {
		CaseBlock caseBlock = factory.createCaseBlock();
		caseBlock.setBody(body);
		caseBlock.setValue(pos);
		return caseBlock ;
	}

}