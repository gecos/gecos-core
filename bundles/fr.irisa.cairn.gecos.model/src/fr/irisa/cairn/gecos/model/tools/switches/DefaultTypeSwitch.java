package fr.irisa.cairn.gecos.model.tools.switches;

import gecos.types.AliasType;
import gecos.types.ArrayType;
import gecos.types.Field;
import gecos.types.FunctionType;
import gecos.types.PtrType;
import gecos.types.RecordType;
import gecos.types.Type;
import gecos.types.util.TypesSwitch;

public abstract class DefaultTypeSwitch<T> extends TypesSwitch<T> {

	@Override
	public T caseFunctionType(FunctionType object) {
		for(Type ptype : object.listParameters()) {
			if(ptype!=null) doSwitch(ptype);
		}
		if(object.getReturnType()!=null) doSwitch(object.getReturnType());
		return null;
	}

	@Override
	public T casePtrType(PtrType object) {
		doSwitch(object.getBase());
		return null;
	}

//	@Override
//	public T caseStaticType(StaticType object) {
//		doSwitch(object.getBase());
//		return null;
//	}
//
//	@Override
//	public T caseRegisterType(RegisterType object) {
//		doSwitch(object.getBase());
//		return null;
//	}

	@Override
	public T caseRecordType(RecordType object) {
		for(Field field: object.getFields()) {
			doSwitch(field);
		}
		return null;
	}

//	@Override
//	public T caseVolatileType(VolatileType object) {
//		doSwitch(object.getBase());
//		return null;
//	}

	@Override
	public T caseAliasType(AliasType object) {
		doSwitch(object.getAlias());
		return null;
	}

	@Override
	public T caseField(Field object) {
		return null;
	}

	@Override
	public T caseArrayType(ArrayType object) {
		if(object.getBase() != null)
			doSwitch(object.getBase());
		return null;
	}

//	@Override
//	public T caseConstType(ConstType object) {
//		doSwitch(object.getBase());
//		return null;
//	}

}
