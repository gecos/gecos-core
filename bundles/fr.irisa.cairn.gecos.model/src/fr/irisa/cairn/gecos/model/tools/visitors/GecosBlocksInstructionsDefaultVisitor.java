package fr.irisa.cairn.gecos.model.tools.visitors;

import org.eclipse.emf.common.util.EList;

import gecos.blocks.BasicBlock;
import gecos.dag.DAGInstruction;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.ArrayValueInstruction;
import gecos.instrs.BreakInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.CaseInstruction;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.CondInstruction;
import gecos.instrs.ContinueInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.DummyInstruction;
import gecos.instrs.EnumeratorInstruction;
import gecos.instrs.FieldInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.GotoInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.LabelInstruction;
import gecos.instrs.MethodCallInstruction;
import gecos.instrs.NumberedSymbolInstruction;
import gecos.instrs.PhiInstruction;
import gecos.instrs.RangeArrayInstruction;
import gecos.instrs.RangeInstruction;
import gecos.instrs.RetInstruction;
import gecos.instrs.SSADefSymbol;
import gecos.instrs.SSAUseSymbol;
import gecos.instrs.SetInstruction;
import gecos.instrs.SimdExpandInstruction;
import gecos.instrs.SimdExtractInstruction;
import gecos.instrs.SimdGenericInstruction;
import gecos.instrs.SimdInstruction;
import gecos.instrs.SimdPackInstruction;
import gecos.instrs.SimdShuffleInstruction;
import gecos.instrs.SimpleArrayInstruction;
import gecos.instrs.SizeofInstruction;
import gecos.instrs.SizeofTypeInstruction;
import gecos.instrs.SizeofValueInstruction;
import gecos.instrs.StringInstruction;
import gecos.instrs.SymbolInstruction;

/**
 * BlockInstructionVisitor implements the InstrsVisitor interface, so that the
 * default behavior when visiting a {@link BasicBlock} is to visit the
 * instructions it contains. Visitor also visit all contained instructions in
 * each {@link ComplexInstruction}.
 * 
 * @see BlocksDefaultVisitor, InstrsVisitor
 * 
 * @author anfloch
 */
public abstract class GecosBlocksInstructionsDefaultVisitor extends
		GecosBlocksDefaultVisitor implements IBlocksInstructionVisitor {

	
//	@Override
//	public void visitCompositeBlock(CompositeBlock c) {
//		for (Symbol s : c.getScope().getSymbols()) {
//			if (s.getValue() != null)
//				s.getValue().accept(this);
//		}
//		for (Block block : c.getChildren()) {
//			block.accept(this);
//		}
//	}
	
	@Override
	public void visitBasicBlock(BasicBlock b) {
		super.visitBasicBlock(b);
		for (Instruction i : b.getInstructions().toArray(new Instruction[b.getInstructions().size()])) {
			i.accept(this);
		}
	}

	protected void visitComplexInstruction(ComplexInstruction c) {
		visitInstruction(c);
		EList<Instruction> listChildren = c.listChildren();
		for (Instruction i : listChildren.toArray(new Instruction[listChildren.size()])) {
			i.accept(this);
		}
	}
	
	protected void visitInstruction(Instruction i){

	}

	
	public void visitAddressInstruction(AddressInstruction a) {
		visitComplexInstruction(a);
	}

	
	public void visitArrayInstruction(ArrayInstruction a) {
		visitComplexInstruction(a);
	}

	
	public void visitArrayValueInstruction(ArrayValueInstruction a) {
		visitComplexInstruction(a);
	}

	
	public void visitBreakInstruction(BreakInstruction b) {
		visitComplexInstruction(b);
	}

	
	public void visitCallInstruction(CallInstruction c) {
		visitComplexInstruction(c);
	}

	
	public void visitCondInstruction(CondInstruction c) {
		visitComplexInstruction(c);
	}

	
	public void visitContinueInstruction(ContinueInstruction c) {
		visitComplexInstruction(c);
	}

	
	public void visitConvertInstruction(ConvertInstruction c) {
		visitComplexInstruction(c);
	}

	
	public void visitDummyInstruction(DummyInstruction d) {
		visitInstruction(d);
	}

	
	public void visitFieldInstruction(FieldInstruction f) {
		visitComplexInstruction(f);
	}

	
	public void visitFloatInstruction(FloatInstruction f) {
		visitInstruction(f);
	}

	
	public void visitGenericInstruction(GenericInstruction g) {
		visitComplexInstruction(g);

	}

	
	public void visitGotoInstruction(GotoInstruction g) {
		visitComplexInstruction(g);
	}

	
	public void visitIndirInstruction(IndirInstruction i) {
		visitComplexInstruction(i);
	}

	
	public void visitIntInstruction(IntInstruction i) {
		visitInstruction(i);
	}

	public void visitLabelInstruction(LabelInstruction l) {
		visitInstruction(l);
	}

	
	public void visitMethodCallInstruction(MethodCallInstruction m) {
		visitComplexInstruction(m);
	}

	public void visitPhiInstruction(PhiInstruction p) {
		visitComplexInstruction(p);
	}

	public void visitNumberedSymbolInstruction(NumberedSymbolInstruction n) { 
		visitSymbolInstruction(n);
	}

	public void visitSSADefSymbol(SSADefSymbol s) {	
		visitSymbolInstruction(s);
	}

	public void visitSSAUseSymbol(SSAUseSymbol s) {	
		visitSymbolInstruction(s);
	}

	public void visitRangeInstruction(RangeInstruction r) {
		visitComplexInstruction(r);
	}

	public void visitRetInstruction(RetInstruction r) {
		visitComplexInstruction(r);
	}

	public void visitSetInstruction(SetInstruction s) {
		visitComplexInstruction(s);
	}

	public void visitSizeofInstruction(SizeofInstruction s) {
		visitInstruction(s);
	}

	
	public void visitSizeofTypeInstruction(SizeofTypeInstruction s) {
		visitSizeofInstruction(s);
	}

	
	public void visitSizeofValueInstruction(SizeofValueInstruction s) {
		visitSizeofInstruction(s);
	}

	
	public void visitSymbolInstruction(SymbolInstruction s) {
		visitInstruction(s);
	}

	public void visitSimpleArrayInstruction(SimpleArrayInstruction s) {
		visitComplexInstruction(s);
	}

	public void visitCaseInstruction(CaseInstruction s) {
		visitComplexInstruction(s);
	}

	public void visitEnumeratorInstruction(EnumeratorInstruction enumerator) {
		visitInstruction(enumerator);
	}

	public void visitDAGInstruction(DAGInstruction s) {
		visitInstruction(s);
	}

	public void visitStringInstruction(StringInstruction stringinst) {
		visitInstruction(stringinst);
	}

	public void visitSimdInstruction(SimdInstruction simdinst) {
		visitComplexInstruction(simdinst);
	}
	
	public void visitSimdGenericInstruction(SimdGenericInstruction simdinst) {
		visitSimdInstruction(simdinst);
	}
	
	public void visitSimdPackInstruction(SimdPackInstruction simdinst) {
		visitSimdInstruction(simdinst);
	}
	
	public void visitSimdShuffleInstruction(SimdShuffleInstruction simdinst) {
		visitSimdInstruction(simdinst);
	}
	
	public void visitSimdExtractInstruction(SimdExtractInstruction simdinst) {
		visitSimdInstruction(simdinst);
	}
	
	public void visitSimdExpandInstruction(SimdExpandInstruction simdinst) {
		visitSimdInstruction(simdinst);		
	}
	
	public void visitRangeArrayInstruction(RangeArrayInstruction rangearrayinst) {
		visitComplexInstruction(rangearrayinst);
	}
}
