package fr.irisa.cairn.gecos.model.analysis.dataflow.framework;

import gecos.blocks.BasicBlock;
import gecos.core.Procedure;
import gecos.core.Symbol;

import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;


/** AbstractLivenessAnalyser. Compute the liveness of procedure variables
 * using the BackwardAnalyser.
 *
 * For each block b :
 *    - DEFINED(b) is the set of (re)defined variables by b block
 *    		(killed definitions),
 *    - USED(b) is the set of variables used in b block (ie. living definitions).
 *
 *  Living variables at the end of a block b are :
 *  v_in(b)  = USED(b) or (v_out(b)-DEFINED(n))
 *  v_out(b) = \sum_{s\in succ(b)} v_in(s) 
 *
 * return getUsed(b).or(out.and(getDefined(b).not()));
 *
 * As we are more interested in living variables at the beginning
 * of the blocks, the final results are computed after the iterative
 * algorithm stop.
 */

/**
 * LivenessAnalyser. Compute the liveness of procedure variables using the
 * BackwardAnalyser.
 * 
 * For each block b : - DEFINED(b) is the set of (re)defined variables by 
 * block b, - USED(b) is the set of variables used in block b.
 * 
 * Living variables at the end of a block b are : 
 * v_new(b) = \sum_{n\in * succ(b)} v_old(b)+(USED(n)-DEFINED(n))
 * 
 * As we are more interested in living variables at the beginning of the blocks,
 * the final results are computed after the iterative algorithm stop.
 */

public abstract class AbstractLivenessAnalyser extends BackwardAnalyser {
	
	protected Map<BasicBlock, BooleanLattice> defined;
	protected Map<BasicBlock, BooleanLattice> used;
	protected Symbol[] variables;

	
	public AbstractLivenessAnalyser(Procedure proc, Symbol[] variables) {
		super(proc);
		this.variables = variables;
		defined = new LinkedHashMap<BasicBlock, BooleanLattice>();
		used = new LinkedHashMap<BasicBlock, BooleanLattice>();

		for (BasicBlock b : proc.getBasicBlocks()) {
			defined.put(b, computeDefined(b));
			used.put(b, computeUsed(b));
			
			if (isDebug()) {
				debug("Block B"+b.getNumber()+":");
				debug("\tDef : "+defined.get(b));
				debug("\tUse : "+used.get(b));
			}
		}
		computeDefined(proc);
		computeUsed(proc);
		
		initialize();
		
		if (isDebug()) {
			for (BasicBlock b : proc.getBasicBlocks()) {
				debug("Block B"+b.getNumber()+":");
				debug("\tInit : "+getLattice(b));
			}
		}
	}

	protected abstract BooleanLattice computeDefined(BasicBlock b);
	
	protected abstract BooleanLattice computeUsed(BasicBlock b);
	
	protected abstract void computeDefined(Procedure proc);

	protected abstract void computeUsed(Procedure proc);
	
	protected BooleanLattice getUsed(BasicBlock b) {
		return (BooleanLattice) used.get(b);
	}

	protected BooleanLattice getDefined(BasicBlock b) {
		return (BooleanLattice) defined.get(b);
	}

	protected ILattice newBottomLattice() {
		return new BooleanLattice(variables);
	}

	protected ILattice compute(BasicBlock b, ILattice out) {
		//	v_in(b)  = USED(b) or (v_out(b)-DEFINED(n))
		ILattice not = getDefined(b).not();
		ILattice and = out.and(not);
		BooleanLattice used = getUsed(b);
		ILattice or = used.or(and);

		if (isDebug()) {
			debug("\t\tcompute(BB"+b.getNumber()+","+out+")");
			debug("\t\t\t getDefined(BB"+b.getNumber()+")="+getDefined(b));
			debug("\t\t\t not(getDefined(BB"+b.getNumber()+"))"+not+")");
			debug("\t\t\t and()="+and+")");
			debug("\t\t\t used="+used+")");
			debug("\t\t\t res: "+or+" = "+used+" | "+and);
		}
		
		return or;

	}
}
