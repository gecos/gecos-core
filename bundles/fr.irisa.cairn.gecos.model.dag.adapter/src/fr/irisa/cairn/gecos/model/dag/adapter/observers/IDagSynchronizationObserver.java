package fr.irisa.cairn.gecos.model.dag.adapter.observers;

import fr.irisa.cairn.graph.observer.IObserver;

/**
 * Service for synchronization observer of a {@link DAGInstructionAdapter}
 * @author Antoine Floc'h - Initial contribution and API
 *
 */
public interface IDagSynchronizationObserver extends IObserver{

}
