package fr.irisa.cairn.gecos.model.analysis.instructions.comparator;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import com.google.common.base.Strings;

import fr.irisa.cairn.gecos.model.analysis.instructions.comparator.InstructionComparator;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;
import tom.mapping.GenericIntrospector;

@SuppressWarnings("all")
public class InstructionComparatorUtils {
 
	%include { sl.tom }
	%include { gecos_terminals.tom } 
	%include { gecos_common.tom }
	%include { gecos_basic.tom }
	%include { gecos_arithmetic.tom }
    %include { gecos_ssa.tom }
   
	private static boolean VERBOSE = false;

	private static int _space = 0;
	private static void INC() {_space++;}
	private static void DEC() {_space--;}

	public static void rdebug(String name, Instruction... instructions) {
		debug(Strings.repeat("  ", _space) + "rule " + name + ": ", instructions);
	}
	public static void sdebug(String msg, Instruction... instructions) {
		debug(Strings.repeat("  ", _space) + msg, instructions);
	}
	public static void debug(String msg, Instruction... instructions) {
		if(VERBOSE)
			System.out.println(msg + InstructionComparator.instToString(instructions));
	}


	/**
	 * 1- Convert all sub(div) to add(mul) and merge adds(muls). Also eliminate neg and replace it by negation annotation
	 * 2- Simplify and Sort children as follows: first generic instructions (name natural order), 
	 * then symbols (name natural order), then constants (numeric order).
	 * - All constants are then merged into one ConstantInstruction.
	 * - Identical Symbols are merged: inverse pairs are eliminated, the rest (in case of ADD only), 
	 * if more than one (n), are replaced with a "MUL(symbol, n)" properly reordered.
	 *
	 * TODO support mul (similar to add) and div (similar to sub)
	 *
	 * @return Simplified Intruction in canonical form. Negation/Invertion are represented using 
	 * annotations and are not reflected in the instruction object !!! 
	 */
	public static Instruction makeCanonical(Instruction inst) {
		try {
			_space = 0; debug("\n> makeCanonical: ", inst);

			/* XXX make sure inst has a container */
			BasicBlock tmpContainer = null;
			if(inst.eContainer() == null) {
				tmpContainer = GecosUserBlockFactory.BBlock(inst);
			}
			
//			inst = `Innermost(removeNeg()).visitLight((inst), GenericIntrospector.INSTANCE);
//			sdebug("|_", inst);

			/* Convert all sub(div) to add(mul) and merge adds(muls) */
			_space = 0;
			inst = `Repeat(Sequence(Outermost(reduce()), Not(stop()))).visitLight((inst), GenericIntrospector.INSTANCE);
			sdebug("|_", inst);
			
			/* 
			 * - Simplify and Sort children as follows: first generic instructions (name lexical order), 
			 * then symbols (lexical order), then constants (numeric order).
			 * - All constants are then merged into one ConstantInstruction.
			 * - Identical Symbols are merged: inverse pairs are eliminated, the rest (in case of ADD only), 
			 * if more than one (n), are replaced with a "MUL(symbol, n)" properly reordered.
			 */
			_space = 0;
			`Innermost(sortAndSimplify()).visitLight((inst), GenericIntrospector.INSTANCE);
			
			/* XXX Restore container */
			if(tmpContainer != null) {
				if(tmpContainer.getInstructionCount() == 0) {
					/* inst has been removed i.e. inst is equivalent to 0 */
					inst = GecosUserInstructionFactory.Int(0);
				}
				else {
					inst = tmpContainer.getFirstInstruction();
				}
				tmpContainer.removeAllInstructions();
			}

			_space = 0; sdebug("< ", `inst);
		} 
		catch(Exception e) {
			throw new RuntimeException(e);
		}
		return inst;
	}

//	%strategy removeNeg() extends Fail() {
//		visit Inst {
//			inst@neg(InstL(a)) -> {
//				INC(); rdebug("neg(a)", `inst);
//				if(!InstructionComparator.isNeg(`inst))
//					InstructionComparator.negate(`a);
//				`inst.substituteWith(`a);
//				sdebug("|_ ", `a);
//				sdebug("|_ ", `a.getParent());
//			}
//		}
//	} 
	
	%strategy reduce() extends Fail() {
		visit Inst {
			inst@generic(_, InstL(_*, ineg@neg(InstL(a)), _*)) -> {
				INC(); rdebug("gen(*, neg(a), *)", `inst);
				if(!InstructionComparator.isNeg(`ineg))
					InstructionComparator.negate(`a);
				`ineg.substituteWith(`a);
			}
			
			inst@sub(InstL(_,b)) -> {
				INC(); rdebug("sub(a,b)", `inst);
				//`b.substituteWith(negate(`b));
				InstructionComparator.negate(`b);
				((GenericInstruction)`inst).setName(ArithmeticOperator.ADD.getLiteral());
			}
			inst@add(InstL(_*, ad2@add(InstL(ad2Args*)), _*)) -> {
				INC(); rdebug("add(*,add(*),*)", `inst);
				((GenericInstruction)`inst).getChildren().remove(`ad2);
				boolean neg = InstructionComparator.isNeg(`ad2);
				for(Instruction arg : `ad2Args) {
					if(neg) InstructionComparator.negate(arg);
					((GenericInstruction)`inst).getChildren().add(arg);
				}
			}

			inst@div(InstL(_,b)) -> {
				INC(); rdebug("div(*,div(*),*)", `inst);
				throw new RuntimeException("div not yet implemented");
			}
			inst@mul(InstL(_*, mul2@mul(InstL(mul2Args*)), _*)) -> {
				INC(); rdebug("mul(*,mul(*),*)", `inst);
				((GenericInstruction)`inst).getChildren().remove(`mul2);
				boolean neg = InstructionComparator.isNeg(`mul2);
				for(Instruction arg : `mul2Args) {
					((GenericInstruction)`inst).getChildren().add(arg);
				}
				if(neg) InstructionComparator.negate(`inst);
			}
		}
	}

	%strategy stop() extends Identity() {
		visit Inst {
			inst@neg(InstL(a)) -> { //XXX
				_space = 0; rdebug("continue neg", `inst);
				throw new VisitFailure();
			}
			inst@add(InstL(_*, add(InstL(_*)), _*)) ->  {
				_space = 0; rdebug("continue addadd", `inst);
				throw new VisitFailure();
			}
			inst@add(InstL(_*, sub(InstL(_*)), _*)) ->  {
				_space = 0; rdebug("continue addsub", `inst);
				throw new VisitFailure();
			}

			//TODO support mul (similar to add) and div (similar to sub)
		}
	}
			
	%strategy sortAndSimplify() extends Fail() {
		visit Inst {
			inst@generic(_, InstL(_*)) -> {
				rdebug("sort g(*)", `inst);
				InstructionComparator.sort((GenericInstruction)`inst);
				int size = ((GenericInstruction)`inst).getChildrenCount();
				sdebug("|_", `inst);
				if(size == 1)
					`inst.substituteWith(((GenericInstruction)`inst).getChild(0));
				else if(size == 0)
					`inst.remove();
			}
		}
	}

	public static Double getValue(Instruction inst) {
		%match (inst){
			ival(v) -> { return (double)(`v); }
			fval(v) -> { return `v;	}
		}
		return null;
	}

}