/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.analysis.dataflow.framework;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

public class BooleanLattice<T> implements ILattice {

	private T[] objects;
	private boolean[] values;

	public BooleanLattice(T[] objects) {
		this(objects, false);
	}
	
	public BooleanLattice(T[] objects, boolean initial) {
		this.objects = objects;
		this.values = new boolean[objects.length];
		for (int i = 0; i < values.length; ++i) {
			values[i] = initial;
		}
	}

	public BooleanLattice(BooleanLattice<T> b) {
		objects = b.objects;
		values = new boolean[b.values.length];
		for (int i = 0; i < values.length; ++i) {
			values[i] = b.values[i];
		}
	}

	public boolean getValue(Object o) {
		for (int i = 0; i < objects.length; ++i)
			if (objects[i].equals(o))
				return values[i];
		throw new RuntimeException("object not in set \""+o+"\"");
	}

	public void setValue(Object o, boolean b) {
		for (int i = 0; i < objects.length; ++i)
			if (objects[i].equals(o)) {
 				values[i] = b;
				return;
			}
	}

	public int size() { return values.length; }
	
	public void merge(ILattice l) {
		BooleanLattice<T> bl = (BooleanLattice<T>) l;
		for (int i = 0; i < values.length; ++i) {
			values[i] |= bl.values[i];
		}
	}

	public void mask(ILattice l) {
		BooleanLattice<T> bl = (BooleanLattice<T>) l;
		for (int i = 0; i < values.length; ++i) {
			if (!bl.values[i]) values[i] = false;
		}
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof ILattice) {
			ILattice l = (ILattice) o;
			BooleanLattice<T> bl = (BooleanLattice<T>) l;
			for (int i = 0; i < values.length; ++i) {
				if (values[i] != bl.values[i]) return false;
			}
			return true;
		}
		return false;
	}

	public BooleanLattice<T> or(ILattice l) {
		BooleanLattice bl = (BooleanLattice) l;
		BooleanLattice res = new BooleanLattice(objects);
		for (int i = 0; i < values.length; ++i) {
			res.values[i] = values[i] | bl.values[i];
		}
		return res;
	}

	public BooleanLattice<T> not() {
		BooleanLattice res = new BooleanLattice(objects);
		for (int i = 0; i < values.length; ++i) {
			res.values[i] = ! values[i];
		}
		return res;
	}

	public BooleanLattice<T> and(ILattice l) {
		BooleanLattice bl = (BooleanLattice) l;
		BooleanLattice res = new BooleanLattice(objects);
		for (int i = 0; i < values.length; ++i) {
			res.values[i] = values[i] & bl.values[i];
		}
		return res;
	}

	public EList<T> getTrueResults() {
		EList<T> res = new BasicEList<T>();
		for (int i = 0; i < values.length; i++) {
			if (values[i]) res.add(objects[i]);
		}
		return res;
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer();
		boolean first = true;

		buffer.append('<');
		for (int i = 0; i < values.length; ++i) {
			if (values[i]) {
				if (! first) buffer.append(' ');
				buffer.append(objects[i].toString());
				first = false;
			}
			//buffer.append(values[i]?'1':'0');
		}
		buffer.append('>');
		return buffer.toString();
	}
}
