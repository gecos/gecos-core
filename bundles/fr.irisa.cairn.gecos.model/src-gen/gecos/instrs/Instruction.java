/**
 */
package gecos.instrs;

import gecos.annotations.AnnotatedElement;

import gecos.blocks.BasicBlock;

import gecos.core.ITypedElement;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;

import gecos.types.Type;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.Instruction#get___internal_cached___type <em>internal cached type</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getInstruction()
 * @model abstract="true"
 *        annotation="http://www.eclipse.org/ocl/examples/OCL constr='true'"
 * @generated
 */
public interface Instruction extends AnnotatedElement, InstrsVisitable, ITypedElement {
	/**
	 * Returns the value of the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>internal cached type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>internal cached type</em>' reference.
	 * @see #set___internal_cached___type(Type)
	 * @see gecos.instrs.InstrsPackage#getInstruction____internal_cached___type()
	 * @model resolveProxies="false"
	 * @generated
	 */
	Type get___internal_cached___type();

	/**
	 * Sets the value of the '{@link gecos.instrs.Instruction#get___internal_cached___type <em>internal cached type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>internal cached type</em>' reference.
	 * @see #get___internal_cached___type()
	 * @generated
	 */
	void set___internal_cached___type(Type value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return the first {@link ComplexInstruction} in the eContainer chain.
	 * If none is found then null.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.ecore.EObject%&gt; cont = this.eContainer();\nwhile (((!(cont instanceof &lt;%gecos.instrs.ComplexInstruction%&gt;)) &amp;&amp; (cont instanceof &lt;%gecos.instrs.Instruction%&gt;)))\n{\n\tcont = cont.eContainer();\n}\nif ((!(cont instanceof &lt;%gecos.instrs.ComplexInstruction%&gt;)))\n{\n\treturn null;\n}\nreturn ((&lt;%gecos.instrs.ComplexInstruction%&gt;) cont);'"
	 * @generated
	 */
	ComplexInstruction getParent();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.Type%&gt; ____internal_cached___type = this.get___internal_cached___type();\nboolean _tripleEquals = (____internal_cached___type == null);\nif (_tripleEquals)\n{\n\ttry\n\t{\n\t\tfinal &lt;%gecos.types.Type%&gt; t = this.computeType();\n\t\tif ((t == null))\n\t\t{\n\t\t\treturn null;\n\t\t}\n\t\tif (((this.getBasicBlock() != null) &amp;&amp; (this.getBasicBlock().getScope() != null)))\n\t\t{\n\t\t\tfinal &lt;%gecos.core.Scope%&gt; scope = this.getBasicBlock().getScope();\n\t\t\tfinal &lt;%gecos.types.Type%&gt; existingType = scope.lookup(t);\n\t\t\tif ((existingType == null))\n\t\t\t{\n\t\t\t\tthis.setType(t);\n\t\t\t\tscope.getTypes().add(t);\n\t\t\t}\n\t\t\telse\n\t\t\t{\n\t\t\t\tthis.setType(existingType);\n\t\t\t}\n\t\t}\n\t\telse\n\t\t{\n\t\t\treturn t;\n\t\t}\n\t}\n\tcatch (final Throwable _t) {\n\t\tif (_t instanceof &lt;%java.lang.Exception%&gt;) {\n\t\t\treturn null;\n\t\t}\n\t\telse\n\t\t{\n\t\t\tthrow &lt;%org.eclipse.xtext.xbase.lib.Exceptions%&gt;.sneakyThrow(_t);\n\t\t}\n\t}\n}\nreturn this.get___internal_cached___type();'"
	 * @generated
	 */
	Type getType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model tUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.set___internal_cached___type(t);'"
	 * @generated
	 */
	void setType(Type t);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * <h1> Non-API: Do not use ! </h1>
	 * 
	 * The method goal is to infer the Type of the current instruction
	 * at run-time using the type information of its children
	 * <!-- end-model-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return null;'"
	 * @generated
	 */
	Type computeType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This method computes a structural equivalence check only (i.e., n*2 and 2*n are not same).
	 * @see InstructionComparator
	 * for a semantic equivalence check.
	 * <!-- end-model-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean isSame = false;\nif (((instr.getType() == null) &amp;&amp; (this.getType() == null)))\n{\n\tisSame = true;\n}\nelse\n{\n\t&lt;%gecos.types.Type%&gt; _type = instr.getType();\n\tboolean _tripleEquals = (_type == null);\n\t&lt;%gecos.types.Type%&gt; _type_1 = this.getType();\n\tboolean _tripleEquals_1 = (_type_1 == null);\n\tboolean _xor = (_tripleEquals ^ _tripleEquals_1);\n\tif (_xor)\n\t{\n\t\treturn false;\n\t}\n\telse\n\t{\n\t\tisSame = instr.getType().isEqual(this.getType());\n\t}\n}\nreturn isSame;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _simpleName = this.getClass().getSimpleName();\n&lt;%java.lang.String%&gt; _plus = (_simpleName + \" not supported by \");\n&lt;%java.lang.String%&gt; _simpleName_1 = visitor.getClass().getSimpleName();\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + _simpleName_1);\nthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus_1);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model sUnique="false" valueUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='throw new &lt;%java.lang.UnsupportedOperationException%&gt;((\"Not applicable to \" + s));'"
	 * @generated
	 */
	void replaceChild(Instruction s, Instruction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%fr.irisa.cairn.gecos.model.tools.utils.GecosCopier%&gt; copier = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.GecosCopier%&gt;();\n&lt;%org.eclipse.emf.ecore.EObject%&gt; _copy = copier.copy(this);\nfinal &lt;%gecos.instrs.Instruction%&gt; instruction = ((&lt;%gecos.instrs.Instruction%&gt;) _copy);\ncopier.copyReferences();\nreturn instruction;'"
	 * @generated
	 */
	Instruction copy();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This method looks up in the containment hierarchy (see {@link #eContainer()})
	 * and returns the first object instance of {@link Procedure}, or null if {@link #eContainer()} returns null
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.ecore.EObject%&gt; container = this.eContainer();\nwhile ((container != null))\n{\n\tif ((container instanceof &lt;%gecos.core.Procedure%&gt;))\n\t{\n\t\treturn ((&lt;%gecos.core.Procedure%&gt;)container);\n\t}\n\telse\n\t{\n\t\tcontainer = container.eContainer();\n\t}\n}\nreturn null;'"
	 * @generated
	 */
	Procedure getContainingProcedure();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This method returns the {@link ProcedureSet} containing the {@link Procedure}
	 *  returned by {@link #getContainingProcedure()} if it exists, or null otherwise.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.core.Procedure%&gt; _containingProcedure = this.getContainingProcedure();\n&lt;%gecos.core.ProcedureSet%&gt; _containingProcedureSet = null;\nif (_containingProcedure!=null)\n{\n\t_containingProcedureSet=_containingProcedure.getContainingProcedureSet();\n}\nreturn _containingProcedureSet;'"
	 * @generated
	 */
	ProcedureSet getContainingProcedureSet();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return the root {@link Instruction} in the {@link #getParent()} hierarchy.
	 * If {@link #getParent()} is null, the method returns this.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.instrs.ComplexInstruction%&gt; _parent = this.getParent();\nboolean _tripleNotEquals = (_parent != null);\nif (_tripleNotEquals)\n{\n\treturn this.getParent().getRoot();\n}\nreturn this;'"
	 * @generated
	 */
	Instruction getRoot();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This method looks up for the first {@link BasicBlock} in the containment hierarchy
	 * (see {@link #eContainer()}).
	 * @return the first encountered {@link BasicBlock}, or null if none was found.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.ecore.EObject%&gt; container = this.eContainer();\nwhile ((container != null))\n{\n\t{\n\t\tif ((container instanceof &lt;%gecos.blocks.BasicBlock%&gt;))\n\t\t{\n\t\t\treturn ((&lt;%gecos.blocks.BasicBlock%&gt;)container);\n\t\t}\n\t\tcontainer = container.eContainer();\n\t}\n}\nreturn null;'"
	 * @generated
	 */
	BasicBlock getBasicBlock();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return true if the value of the instruction is sure to be constant,
	 * and false otherwise.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return false;'"
	 * @generated
	 */
	boolean isConstant();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return false;'"
	 * @generated
	 */
	boolean isUnconditionalBranch();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return false;'"
	 * @generated
	 */
	boolean isRet();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return false;'"
	 * @generated
	 */
	boolean isBranch();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return false;'"
	 * @generated
	 */
	boolean isSet();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return false;'"
	 * @generated
	 */
	boolean isSymbol();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Attempt to substitute this with the given argument.
	 * If this is not contained (see {@link #eContainer()}), throws exception.
	 * <!-- end-model-doc -->
	 * @model instUnique="false" instRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.instrs.ComplexInstruction%&gt; _parent = this.getParent();\nboolean _tripleNotEquals = (_parent != null);\nif (_tripleNotEquals)\n{\n\tthis.getParent().replaceChild(this, inst);\n}\nelse\n{\n\tif (((this.getBasicBlock() != null) &amp;&amp; this.getBasicBlock().getInstructions().contains(this)))\n\t{\n\t\tthis.getBasicBlock().replaceInstruction(this, inst);\n\t}\n\telse\n\t{\n\t\t&lt;%org.eclipse.emf.ecore.EObject%&gt; _eContainer = this.eContainer();\n\t\tboolean _tripleNotEquals_1 = (_eContainer != null);\n\t\tif (_tripleNotEquals_1)\n\t\t{\n\t\t\tfinal &lt;%org.eclipse.emf.ecore.EObject%&gt; container = this.eContainer();\n\t\t\tfinal &lt;%org.eclipse.emf.ecore.EStructuralFeature%&gt; feature = this.eContainingFeature();\n\t\t\tboolean _isMany = feature.isMany();\n\t\t\tif (_isMany)\n\t\t\t{\n\t\t\t\t&lt;%java.lang.Object%&gt; _eGet = container.eGet(feature);\n\t\t\t\tfinal &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.eclipse.emf.ecore.EObject%&gt;&gt; list = ((&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%org.eclipse.emf.ecore.EObject%&gt;&gt;) _eGet);\n\t\t\t\tboolean _contains = list.contains(this);\n\t\t\t\tboolean _not = (!_contains);\n\t\t\t\tif (_not)\n\t\t\t\t{\n\t\t\t\t\tthrow new &lt;%java.lang.RuntimeException%&gt;(((((\"Cannot find [\" + this) + \"] in the structural features of [\") + container) + \"].\"));\n\t\t\t\t}\n\t\t\t\tlist.set(list.indexOf(this), inst);\n\t\t\t}\n\t\t\telse\n\t\t\t{\n\t\t\t\tcontainer.eSet(feature, inst);\n\t\t\t}\n\t\t}\n\t\telse\n\t\t{\n\t\t\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(((((\"Cannot substitute [\" + this) + \"] with [\") + inst) + \"] as this latter one is not contained.\"));\n\t\t}\n\t}\n}'"
	 * @generated
	 */
	void substituteWith(Instruction inst);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.ecore.EObject%&gt; _eContainer = this.eContainer();\nboolean _tripleEquals = (_eContainer == null);\nif (_tripleEquals)\n{\n\tthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(((\"Cannot remove [\" + this) + \"] as it is not contained.\"));\n}\n&lt;%org.eclipse.emf.ecore.util.EcoreUtil%&gt;.remove(((&lt;%org.eclipse.emf.ecore.EObject%&gt;) this));'"
	 * @generated
	 */
	void remove();

} // Instruction
