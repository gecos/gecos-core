/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.transforms.ssa;

import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.symbref;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.tools.controlflow.BuildControlFlow;
import fr.irisa.cairn.gecos.model.tools.controlflow.ClearControlFlow;
import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import fr.irisa.cairn.gecos.model.transforms.blocks.simplifier.MergeBasicBlocks;
import fr.irisa.cairn.gecos.model.transforms.blocks.simplifier.MergeCompositeBlocks;
import fr.irisa.r2d2.gecos.framework.GSModule;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.DataEdge;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.BranchInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.NumberedSymbolInstruction;
import gecos.instrs.PhiInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;


/**
 *  RemovePhyNode is get back from SSA representation.
 * 
 * TODO Could be optimized : see "Efficiently computing
 * static single assignment" Cytron et al. section 7 
 * 
 * @author llhours
 */
@GSModule("Revert the SSA form computed using 'ComputeSSAForm' module.")
public class RemoveSSAForm extends BlockInstructionSwitch<Object> {

	private int pass;
	private boolean changed;
	private BasicBlock currentBlock;

	private List<Procedure> procs;
	
	public RemoveSSAForm(GecosProject proj) {
		procs = proj.listProcedures();
	}

	public RemoveSSAForm(ProcedureSet ps) {
		procs = ps.listProcedures();
	}
	
	public RemoveSSAForm(Procedure proc) {
		procs = new ArrayList<Procedure>(1);
		procs.add(proc);
	}

	public void compute() {
		for (Procedure proc : procs) {
			proc.getAnnotations().removeKey(SSAAnalyser.SSATAG);
			//remove SSA
			compute(proc.getBody());
			//remove update artefacts
			SSAArrayUpdateRemover updateRemover = new SSAArrayUpdateRemover(proc);
			updateRemover.compute();
			new ClearControlFlow(proc).compute();
			proc.accept(new MergeCompositeBlocks());
			proc.accept(new MergeBasicBlocks());
			new BuildControlFlow(proc).compute();
		}
	}
	
	public boolean compute(Block b) {
		changed = false;
		pass = 0;
		doSwitch(b);
		pass = 1;
		doSwitch(b);
		return changed;
	}

	@Override
	public Object caseBasicBlock(BasicBlock b) {
		currentBlock = b;
		//clear edges
		for(DataEdge d:b.getDefEdges()) {
			d.getUses().clear();
		}
		for(DataEdge d:b.getUseEdges()) {
			d.getUses().clear();
		}
		b.getDefEdges().clear();
		b.getUseEdges().clear();
		
		//process instructions
		super.caseBasicBlock(b);
		
		//clear annotations
		b.getAnnotations().remove("Liveness");
		b.getAnnotations().remove("Defined");
		b.getAnnotations().remove("Used");
		
		return true;
	}

	@Override
	public Object caseSetInstruction(SetInstruction p) {
		if (pass == 1) {
			if ( !(p.getSource() instanceof GenericInstruction )) {
				return super.caseSetInstruction(p);
			}
			GenericInstruction g = (GenericInstruction)p.getSource();
			if ( !g.isNamed("update") ) {
				return super.caseSetInstruction(p);
			}
			
			/* for update instructions reconstruct ArrayInstruction */
			EList<Instruction> listChildren = g.listChildren();
			List<Instruction> index = listChildren.stream().skip(2).collect(Collectors.toList());
			Instruction setValue = listChildren.get(1);
			ArrayInstruction dest = GecosUserInstructionFactory.array(p.getDest().copy(), index);
			
			p.setDest(dest);
			p.setSource(setValue);
			
			/* Remove SSA use and def in children */
			super.caseSetInstruction(p);
			return true;
		}
		
		/*
		 * For setInstruction with a Phi as source:
		 *  replace it with a setInstruction that have as source a child of phi, at the
		 *  end of the proceeding block.
		 */
		if (! (p.getSource() instanceof PhiInstruction)) return null;
		PhiInstruction phi = (PhiInstruction) p.getSource();

//		if (! (p.getDest() instanceof SymbolInstruction))
//			throw new RuntimeException("In "+p+", "+p.getDest()+" should be a symbol ");
//		SymbolInstruction destSym = (SymbolInstruction) p.getDest();
//		
//		for (ControlEdge e  : currentBlock.getInEdges()) {
//			BasicBlock f = e.getFrom();
//			int x = currentBlock.whichPredecessor(f);
//			if (alreadyHasCopy(f, destSym.getSymbol().getName())) continue;
//			// don't make copy for the same symbol
//			if (phi.getChildren().get(x) instanceof SymbolInstruction) {
//				SymbolInstruction s = (SymbolInstruction) phi.getChildren().get(x);
//				if (s.getSymbol().getName().equals(destSym.getSymbol().getName())) continue;
//			}
//			
//			
//			Instruction set = GecosUserInstructionFactory.set(p.getDest().copy(), phi.getChildren().get(x));
//			int size = f.getInstructions().size();
//			if (size > 0
//					&& f.getInstructions().get(size) instanceof BranchInstruction) //XXX shouldn't it be size-1 ? 
//				f.getInstructions().add(size-1, set);
//			else
//				f.getInstructions().add(set);
//		}
		
		//XXX very hacky !
		Instruction dest = p.getDest();
		EList<Instruction> children = phi.listChildren();
		for (int i = 0; i < children.size(); i++) { //XXX this assumes that the phi children size is <= currentBlock.getInEdges() size
			Instruction phiSrc = children.get(i);
			
			// don't make copy for the same symbol
			if(isSame(dest, phiSrc))
				continue;

			Instruction set = GecosUserInstructionFactory.set(dest.copy(), phiSrc.copy());
			BasicBlock srcBlk = currentBlock.getInEdges().get(i).getFrom();
			if(srcBlk.getInstructionCount() > 0 && srcBlk.getLastInstruction() instanceof BranchInstruction)
				srcBlk.insertInstructionBefore(set, srcBlk.getLastInstruction());
			else
				srcBlk.addInstruction(set);
		}
		
		currentBlock.removeInstruction(p);
		return true;
	}

	//XXX
	private boolean isSame(Instruction dest, Instruction phiSrc) {
		if(dest instanceof SymbolInstruction && phiSrc instanceof SymbolInstruction)
			return ((SymbolInstruction)dest).getSymbolName().equals(((SymbolInstruction)phiSrc).getSymbolName());
		return phiSrc.isSame(dest);
	}

	@Override
	public Object caseNumberedSymbolInstruction(NumberedSymbolInstruction s) {
		if (pass == 1) {
			SymbolInstruction symbolinst = symbref(s.getSymbol());
			s.getParent().replaceChild(s, symbref(symbolinst.getSymbol()));
		}
		return true;
	}
	
		
	private boolean alreadyHasCopy(BasicBlock block, String name) {
		for (int i = block.getInstructions().size() - 1; i >= 0; --i) {
			if (block.getInstructions().get(i) instanceof SetInstruction) {
				SetInstruction set = (SetInstruction) block.getInstructions().get(i);
				if (!(set.getDest() instanceof SymbolInstruction)) continue;
				SymbolInstruction sym = (SymbolInstruction) set.getDest();
				if (sym.getSymbol().getName().equals(name))
					return true;
			}
		}
		return false;
	}
}
