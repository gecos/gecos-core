/**
 */
package gecos.core.impl;

import fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;

import gecos.annotations.AnnotationsPackage;

import gecos.annotations.impl.AnnotationsPackageImpl;

import gecos.blocks.BlocksPackage;

import gecos.blocks.impl.BlocksPackageImpl;

import gecos.core.CoreFactory;
import gecos.core.CorePackage;
import gecos.core.CoreVisitable;
import gecos.core.CoreVisitor;
import gecos.core.GecosNode;
import gecos.core.ISymbolUse;
import gecos.core.ITypedElement;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.ScopeContainer;
import gecos.core.Symbol;

import gecos.dag.DagPackage;

import gecos.dag.impl.DagPackageImpl;

import gecos.instrs.InstrsPackage;

import gecos.instrs.impl.InstrsPackageImpl;

import gecos.types.TypesPackage;

import gecos.types.impl.TypesPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.ETypeParameter;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CorePackageImpl extends EPackageImpl implements CorePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gecosNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass coreVisitableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass coreVisitorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iTypedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass symbolEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iSymbolUseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass procedureSymbolEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopeContainerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scopeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass procedureSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass procedureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterSymbolEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType stringEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType intEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType blockCopyManagerEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see gecos.core.CorePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CorePackageImpl() {
		super(eNS_URI, CoreFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link CorePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CorePackage init() {
		if (isInited) return (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);

		// Obtain or create and register package
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CorePackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		TypesPackageImpl theTypesPackage = (TypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI) instanceof TypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI) : TypesPackage.eINSTANCE);
		AnnotationsPackageImpl theAnnotationsPackage = (AnnotationsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI) instanceof AnnotationsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI) : AnnotationsPackage.eINSTANCE);
		InstrsPackageImpl theInstrsPackage = (InstrsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI) instanceof InstrsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI) : InstrsPackage.eINSTANCE);
		BlocksPackageImpl theBlocksPackage = (BlocksPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI) instanceof BlocksPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI) : BlocksPackage.eINSTANCE);
		DagPackageImpl theDagPackage = (DagPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DagPackage.eNS_URI) instanceof DagPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DagPackage.eNS_URI) : DagPackage.eINSTANCE);

		// Create package meta-data objects
		theCorePackage.createPackageContents();
		theTypesPackage.createPackageContents();
		theAnnotationsPackage.createPackageContents();
		theInstrsPackage.createPackageContents();
		theBlocksPackage.createPackageContents();
		theDagPackage.createPackageContents();

		// Initialize created meta-data
		theCorePackage.initializePackageContents();
		theTypesPackage.initializePackageContents();
		theAnnotationsPackage.initializePackageContents();
		theInstrsPackage.initializePackageContents();
		theBlocksPackage.initializePackageContents();
		theDagPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCorePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CorePackage.eNS_URI, theCorePackage);
		return theCorePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGecosNode() {
		return gecosNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCoreVisitable() {
		return coreVisitableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCoreVisitor() {
		return coreVisitorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getITypedElement() {
		return iTypedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSymbol() {
		return symbolEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSymbol_Type() {
		return (EReference)symbolEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSymbol_Name() {
		return (EAttribute)symbolEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSymbol_Value() {
		return (EReference)symbolEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getISymbolUse() {
		return iSymbolUseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcedureSymbol() {
		return procedureSymbolEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcedureSymbol_Kind() {
		return (EAttribute)procedureSymbolEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcedureSymbol_Procedure() {
		return (EReference)procedureSymbolEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScopeContainer() {
		return scopeContainerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScopeContainer_Scope() {
		return (EReference)scopeContainerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScope() {
		return scopeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScope_Types() {
		return (EReference)scopeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScope_Symbols() {
		return (EReference)scopeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScope_Container() {
		return (EReference)scopeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScope_Parent() {
		return (EReference)scopeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScope_Children() {
		return (EReference)scopeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcedureSet() {
		return procedureSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcedureSet_Uses() {
		return (EReference)procedureSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcedure() {
		return procedureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcedure_Start() {
		return (EReference)procedureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcedure_Body() {
		return (EReference)procedureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcedure_End() {
		return (EReference)procedureEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcedure_Symbol() {
		return (EReference)procedureEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcedure_ContainingProcedureSet() {
		return (EReference)procedureEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParameterSymbol() {
		return parameterSymbolEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getString() {
		return stringEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getint() {
		return intEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getBlockCopyManager() {
		return blockCopyManagerEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoreFactory getCoreFactory() {
		return (CoreFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		gecosNodeEClass = createEClass(GECOS_NODE);

		coreVisitableEClass = createEClass(CORE_VISITABLE);

		coreVisitorEClass = createEClass(CORE_VISITOR);

		iTypedElementEClass = createEClass(ITYPED_ELEMENT);

		symbolEClass = createEClass(SYMBOL);
		createEReference(symbolEClass, SYMBOL__TYPE);
		createEAttribute(symbolEClass, SYMBOL__NAME);
		createEReference(symbolEClass, SYMBOL__VALUE);

		iSymbolUseEClass = createEClass(ISYMBOL_USE);

		procedureSymbolEClass = createEClass(PROCEDURE_SYMBOL);
		createEAttribute(procedureSymbolEClass, PROCEDURE_SYMBOL__KIND);
		createEReference(procedureSymbolEClass, PROCEDURE_SYMBOL__PROCEDURE);

		scopeContainerEClass = createEClass(SCOPE_CONTAINER);
		createEReference(scopeContainerEClass, SCOPE_CONTAINER__SCOPE);

		scopeEClass = createEClass(SCOPE);
		createEReference(scopeEClass, SCOPE__TYPES);
		createEReference(scopeEClass, SCOPE__SYMBOLS);
		createEReference(scopeEClass, SCOPE__CONTAINER);
		createEReference(scopeEClass, SCOPE__PARENT);
		createEReference(scopeEClass, SCOPE__CHILDREN);

		procedureSetEClass = createEClass(PROCEDURE_SET);
		createEReference(procedureSetEClass, PROCEDURE_SET__USES);

		procedureEClass = createEClass(PROCEDURE);
		createEReference(procedureEClass, PROCEDURE__START);
		createEReference(procedureEClass, PROCEDURE__BODY);
		createEReference(procedureEClass, PROCEDURE__END);
		createEReference(procedureEClass, PROCEDURE__SYMBOL);
		createEReference(procedureEClass, PROCEDURE__CONTAINING_PROCEDURE_SET);

		parameterSymbolEClass = createEClass(PARAMETER_SYMBOL);

		// Create data types
		stringEDataType = createEDataType(STRING);
		intEDataType = createEDataType(INT);
		blockCopyManagerEDataType = createEDataType(BLOCK_COPY_MANAGER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);
		AnnotationsPackage theAnnotationsPackage = (AnnotationsPackage)EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI);
		InstrsPackage theInstrsPackage = (InstrsPackage)EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		BlocksPackage theBlocksPackage = (BlocksPackage)EPackage.Registry.INSTANCE.getEPackage(BlocksPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		coreVisitableEClass.getESuperTypes().add(this.getGecosNode());
		symbolEClass.getESuperTypes().add(theAnnotationsPackage.getAnnotatedElement());
		symbolEClass.getESuperTypes().add(this.getITypedElement());
		symbolEClass.getESuperTypes().add(this.getCoreVisitable());
		procedureSymbolEClass.getESuperTypes().add(this.getSymbol());
		procedureSymbolEClass.getESuperTypes().add(this.getScopeContainer());
		scopeContainerEClass.getESuperTypes().add(this.getCoreVisitable());
		scopeEClass.getESuperTypes().add(this.getCoreVisitable());
		procedureSetEClass.getESuperTypes().add(this.getScopeContainer());
		procedureSetEClass.getESuperTypes().add(theAnnotationsPackage.getAnnotatedElement());
		procedureSetEClass.getESuperTypes().add(this.getCoreVisitable());
		procedureEClass.getESuperTypes().add(theAnnotationsPackage.getAnnotatedElement());
		procedureEClass.getESuperTypes().add(this.getCoreVisitable());
		procedureEClass.getESuperTypes().add(this.getISymbolUse());
		parameterSymbolEClass.getESuperTypes().add(this.getSymbol());

		// Initialize classes and features; add operations and parameters
		initEClass(gecosNodeEClass, GecosNode.class, "GecosNode", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(coreVisitableEClass, CoreVisitable.class, "CoreVisitable", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = addEOperation(coreVisitableEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCoreVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(coreVisitorEClass, CoreVisitor.class, "CoreVisitor", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(coreVisitorEClass, null, "visitProcedure", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getProcedure(), "procedure", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(coreVisitorEClass, null, "visitProcedureSet", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getProcedureSet(), "procedureSet", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(coreVisitorEClass, null, "visitScopeContainer", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScopeContainer(), "scopeContainer", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(coreVisitorEClass, null, "visitScope", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScope(), "scope", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(coreVisitorEClass, null, "visitSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSymbol(), "symbol", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(coreVisitorEClass, null, "visitParameterSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getParameterSymbol(), "paramSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(coreVisitorEClass, null, "visitProcedureSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getProcedureSymbol(), "procedureSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(iTypedElementEClass, ITypedElement.class, "ITypedElement", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(iTypedElementEClass, theTypesPackage.getType(), "getType", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(iTypedElementEClass, null, "setType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTypesPackage.getType(), "t", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(symbolEClass, Symbol.class, "Symbol", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSymbol_Type(), theTypesPackage.getType(), null, "type", null, 0, 1, Symbol.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSymbol_Name(), this.getString(), "name", null, 0, 1, Symbol.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSymbol_Value(), theInstrsPackage.getInstruction(), null, "value", null, 0, 1, Symbol.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(symbolEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(symbolEClass, null, "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		ETypeParameter t1 = addETypeParameter(op, "T");
		EGenericType g1 = createEGenericType(this.getSymbol());
		t1.getEBounds().add(g1);
		g1 = createEGenericType(t1);
		initEOperation(op, g1);

		addEOperation(symbolEClass, this.getScope(), "getContainingScope", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(symbolEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCoreVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(iSymbolUseEClass, ISymbolUse.class, "ISymbolUse", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(iSymbolUseEClass, this.getSymbol(), "getUsedSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(iSymbolUseEClass, null, "setSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSymbol(), "newSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(iSymbolUseEClass, null, "updateSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSymbol(), "originalSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSymbol(), "newSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(iSymbolUseEClass, this.getString(), "getSymbolName", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(procedureSymbolEClass, ProcedureSymbol.class, "ProcedureSymbol", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getProcedureSymbol_Kind(), this.getint(), "kind", null, 0, 1, ProcedureSymbol.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcedureSymbol_Procedure(), this.getProcedure(), this.getProcedure_Symbol(), "procedure", null, 0, 1, ProcedureSymbol.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(procedureSymbolEClass, this.getParameterSymbol(), "listParameters", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(procedureSymbolEClass, null, "setType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTypesPackage.getType(), "newType", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(procedureSymbolEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(procedureSymbolEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCoreVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(scopeContainerEClass, ScopeContainer.class, "ScopeContainer", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScopeContainer_Scope(), this.getScope(), this.getScope_Container(), "scope", null, 1, 1, ScopeContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(scopeContainerEClass, null, "addSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSymbol(), "symbol", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopeContainerEClass, null, "removeSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSymbol(), "symbol", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(scopeEClass, Scope.class, "Scope", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScope_Types(), theTypesPackage.getType(), null, "types", null, 0, -1, Scope.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScope_Symbols(), this.getSymbol(), null, "symbols", null, 0, -1, Scope.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScope_Container(), this.getScopeContainer(), this.getScopeContainer_Scope(), "container", null, 0, 1, Scope.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScope_Parent(), this.getScope(), this.getScope_Children(), "parent", null, 0, 1, Scope.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getScope_Children(), this.getScope(), this.getScope_Parent(), "children", null, 0, -1, Scope.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		addEOperation(scopeEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopeEClass, this.getScope(), "managedCopy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBlockCopyManager(), "manager", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopeEClass, this.getScope(), "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopeEClass, theTypesPackage.getType(), "lookupType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getString(), "name", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopeEClass, theTypesPackage.getType(), "lookupAliasType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getString(), "name", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopeEClass, theTypesPackage.getType(), "lookupEnumType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getString(), "name", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopeEClass, theTypesPackage.getType(), "lookupRecordType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getString(), "name", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopeEClass, theTypesPackage.getType(), "lookup", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTypesPackage.getType(), "typ", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopeEClass, null, "makeUnique", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSymbol(), "sym", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScope(), "ref", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopeEClass, null, "makeUnique", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSymbol(), "sym", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopeEClass, null, "mergeWith", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScope(), "scope", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopeEClass, this.getScope(), "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScope(), "scope", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopeEClass, this.getSymbol(), "lookup", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getString(), "name", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopeEClass, theEcorePackage.getEBoolean(), "hasInScope", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSymbol(), "symbol", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopeEClass, null, "moveUp", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSymbol(), "sym", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopeEClass, this.getScope(), "getRoot", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopeEClass, null, "addSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSymbol(), "symbol", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopeEClass, null, "removeSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSymbol(), "symbol", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopeEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCoreVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopeEClass, theEcorePackage.getEBoolean(), "isGlobal", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(scopeEClass, this.getScope(), "findFirstCommonScopeWith", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getScope(), "a", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(scopeEClass, this.getSymbol(), "lookupAllSymbols", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEClass(procedureSetEClass, ProcedureSet.class, "ProcedureSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getProcedureSet_Uses(), this.getISymbolUse(), null, "uses", null, 0, -1, ProcedureSet.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(procedureSetEClass, this.getProcedure(), "findProcedure", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getString(), "name", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(procedureSetEClass, this.getProcedure(), "listProcedures", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(procedureSetEClass, null, "addProcedure", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getProcedure(), "proc", 1, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(procedureSetEClass, null, "addAllProcedures", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getProcedure(), "procs", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(procedureSetEClass, null, "removeProcedure", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getProcedure(), "proc", 1, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(procedureSetEClass, null, "removeAllProcedures", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(procedureSetEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(procedureSetEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCoreVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(procedureSetEClass, this.getProcedureSet(), "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(procedureEClass, Procedure.class, "Procedure", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getProcedure_Start(), theBlocksPackage.getBasicBlock(), null, "start", null, 0, 1, Procedure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcedure_Body(), theBlocksPackage.getCompositeBlock(), null, "body", null, 0, 1, Procedure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcedure_End(), theBlocksPackage.getBasicBlock(), null, "end", null, 0, 1, Procedure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcedure_Symbol(), this.getProcedureSymbol(), this.getProcedureSymbol_Procedure(), "symbol", null, 0, 1, Procedure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcedure_ContainingProcedureSet(), this.getProcedureSet(), null, "containingProcedureSet", null, 0, 1, Procedure.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		addEOperation(procedureEClass, this.getParameterSymbol(), "listParameters", 0, -1, IS_UNIQUE, IS_ORDERED);

		addEOperation(procedureEClass, theBlocksPackage.getBasicBlock(), "getBasicBlocks", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(procedureEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCoreVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(procedureEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(procedureEClass, this.getScope(), "getScope", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(procedureEClass, null, "replaceBlock", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theBlocksPackage.getBlock(), "oldBlk", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theBlocksPackage.getBlock(), "newBlk", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(procedureEClass, this.getProcedure(), "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(procedureEClass, this.getSymbol(), "getUsedSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(procedureEClass, null, "setSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSymbol(), "newSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(parameterSymbolEClass, ParameterSymbol.class, "ParameterSymbol", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(parameterSymbolEClass, this.getProcedure(), "findProcedure", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(parameterSymbolEClass, this.getString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(parameterSymbolEClass, null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCoreVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		// Initialize data types
		initEDataType(stringEDataType, String.class, "String", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(intEDataType, int.class, "int", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(blockCopyManagerEDataType, BlockCopyManager.class, "BlockCopyManager", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/emf/2011/Xcore
		createXcoreAnnotations();
		// gmf.diagram
		createGmfAnnotations();
		// gmf.node
		createGmf_1Annotations();
		// gmf.compartment
		createGmf_2Annotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2011/Xcore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createXcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2011/Xcore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "GmfDiagram", "gmf.diagram",
			 "GmfCompartment", "gmf.compartment",
			 "GmfNode", "gmf.node",
			 "GmfLink", "gmf.link",
			 "OCL", "http://www.eclipse.org/ocl/examples/OCL"
		   });
	}

	/**
	 * Initializes the annotations for <b>gmf.diagram</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGmfAnnotations() {
		String source = "gmf.diagram";	
		addAnnotation
		  (procedureSetEClass, 
		   source, 
		   new String[] {
			 "foo", "bar"
		   });
	}

	/**
	 * Initializes the annotations for <b>gmf.node</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGmf_1Annotations() {
		String source = "gmf.node";	
		addAnnotation
		  (procedureEClass, 
		   source, 
		   new String[] {
			 "figure", "rectangle"
		   });
	}

	/**
	 * Initializes the annotations for <b>gmf.compartment</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGmf_2Annotations() {
		String source = "gmf.compartment";	
		addAnnotation
		  (getProcedure_Start(), 
		   source, 
		   new String[] {
			 "foo", "bar"
		   });	
		addAnnotation
		  (getProcedure_Body(), 
		   source, 
		   new String[] {
			 "foo", "bar"
		   });	
		addAnnotation
		  (getProcedure_End(), 
		   source, 
		   new String[] {
			 "foo", "bar"
		   });
	}

} //CorePackageImpl
