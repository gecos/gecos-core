/**
 * 
 */
package fr.irisa.cairn.gecos.model.transforms.constants;

import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDefaultVisitor;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.ForBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.NumberedSymbolInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * ConstantPropagator is used to propagate the definition of constant symbols
 * into their usage. Can only be used in SSAForm.
 * <p>
 * Simple transposition from the Legacy version
 * <p>
 * This version does not propagate values into the init, step and test blocks of
 * a {@link ForBlock}. You may use loops specific stuff for this (@see
 * Polyhedric Model).
 * 
 * @author kmartin
 * 
 */
public class ModelConstantPropagatorIterative extends
		GecosBlocksInstructionsDefaultVisitor {

	private static final int GET = 0;
	private static final int REPLACE = 1;

	private boolean changed;
	private List<Block> blocks;
	private Map<String, Instruction> consts;

	private int mode;
	private String currentName;
	private BasicBlock currentBlock;
	private List<Instruction> instructionsToRemove;

	/**
	 * 
	 */
	public ModelConstantPropagatorIterative(Block b) {
		this.blocks = new ArrayList<Block>();
		this.blocks.add(b);
	}
	public ModelConstantPropagatorIterative(Procedure proc) {
		this.blocks = new ArrayList<Block>(3);
		this.blocks.add(proc.getStart());
		this.blocks.add(proc.getBody());
		this.blocks.add(proc.getEnd());
	}
	public ModelConstantPropagatorIterative(ProcedureSet ps) {
		this.blocks = new ArrayList<Block>(ps.listProcedures().size()*3);
		for (Procedure proc : ps.listProcedures()) {
			this.blocks.add(proc.getStart());
			this.blocks.add(proc.getBody());
			this.blocks.add(proc.getEnd());
		}
	}
	public ModelConstantPropagatorIterative(GecosProject proj) {
		this.blocks = new ArrayList<Block>();
		for (ProcedureSet ps : proj.listProcedureSets()) {
			for (Procedure proc : ps.listProcedures()) {
				this.blocks.add(proc.getStart());
				this.blocks.add(proc.getBody());
				this.blocks.add(proc.getEnd());
			}
		}
		
	}

	public boolean computeIterative() {
		changed = false;
		consts = new LinkedHashMap<String, Instruction>();
		instructionsToRemove = new ArrayList<Instruction>();
		mode = GET;
		for (Block b : blocks)
			b.accept(this);
//		super.visit(proc);
		mode = REPLACE;
		for (Block b : blocks)
			b.accept(this);
//		super.visit(proc);
		return changed;
	}

	@Override
	public void visitBasicBlock(BasicBlock b) {
		currentBlock = b;
		super.visitBasicBlock(b);
		if (mode == GET) {
			currentBlock.getInstructions().removeAll(instructionsToRemove);
			instructionsToRemove.clear();
		}
	}

	@Override
	public void visitGenericInstruction(GenericInstruction g) {
		// don't get into sub expressions when getting names
		if (mode != REPLACE)
			return;
		super.visitGenericInstruction(g);
	}

	@Override
	public void visitNumberedSymbolInstruction(NumberedSymbolInstruction n) {
		if (mode == GET) {
			if (!n.getSymbol().getContainingScope().isGlobal()) {
				currentName = n.getSymbolName() + n.getNumber();
			}
		} else {
			Instruction t = consts.get(n.getSymbolName() + n.getNumber());
			if (t != null) {
				n.getParent().replaceChild(n, t.copy());
				// don't forget to visit the new child
				// which may also be replaced
				t.accept(this);
				changed = true;
			}
		}
	}

	@Override
	public void visitSetInstruction(SetInstruction s) {
		if (mode == GET) {
			currentName = null;
			// makes sure LHS variables are not replaced by constants  
			if(!(s.getDest() instanceof SymbolInstruction))
				s.getDest().accept(this);
			/*
			 * XXX used to also fetch symbols but their propagation can break
			 * SSA properties
			 */
			if (currentName != null && s.getSource().isConstant()) {
				consts.put(currentName, s.getSource());
				// Throws ConcurrentModificationException
				// currentBlock.removeInstruction(s);
				instructionsToRemove.add(s);
			}
		} else {
			s.getSource().accept(this);
			// visit the destination if it is not
			// directly a symbol
			if (!s.getDest().isSymbol()) {
				s.getDest().accept(this);
			}
		}
	}

	/**
	 * We do not visit the init, test and step blocks of a {@link ForBlock}, as
	 * it may lead to removing needed instructions
	 */
	public void visitForBlock(ForBlock f) {
		// f.getInitBlock().accept(this);
		// f.getTestBlock().accept(this);
		// f.getStepBlock().accept(this);
		f.getBodyBlock().accept(this);
	}
}
