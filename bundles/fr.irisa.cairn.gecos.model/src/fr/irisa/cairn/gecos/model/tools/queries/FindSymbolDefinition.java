package fr.irisa.cairn.gecos.model.tools.queries;

import java.util.Iterator;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.tools.utils.BlocksDefaultSwitch;
import gecos.blocks.CompositeBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.Symbol;

/**
 * Search for a Symbol definition in a ProcedureSet object matching the 
 * name passed as argument. The search is performed in the decreasing 
 * visibility order (e..G if a global and a Local symbol share the name 
 * name, the function will return the global symbol.  
 * 
 * @author sderrien
 * @version 0.1
 * 
 */
public class FindSymbolDefinition extends BlocksDefaultSwitch<Object> {

	private ProcedureSet ps;
	private String name;
	private Symbol result=null;

	public FindSymbolDefinition(ProcedureSet inArg, String name) {
		this.ps = inArg;
		this.name=name;
	}

	public Symbol compute() {
		result = search(ps.getScope().getSymbols());
		if (result==null) {
			for(Procedure proc : ps.listProcedures())
				doSwitch(proc);
		}
		System.out.println("result = "+result);
		return result;
	}

	@Override
	public Object caseCompositeBlock(CompositeBlock b) {
		// b.getTODO Auto-generated method stub
		result = search(b.getScope().getSymbols());
		if (result!=null) return result;
		return super.caseCompositeBlock(b);
	}

	private Symbol search(EList<Symbol> symbols) {
		Iterator<Symbol> i = symbols.iterator();
		while (i.hasNext()) {
			Symbol symbol = (Symbol) i.next();
			if (symbol.getName().equals(name)) {
				return symbol;
			}
		}
		return null;
	}

	@Override
	public Object doSwitch(Procedure proc) {
		result = search(proc.getScope().getSymbols());
		if (result!=null) return result;
		return super.doSwitch(proc);
	}
}