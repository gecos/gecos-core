package fr.irisa.cairn.gecos.model.modules;

import java.io.IOException;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosprojectFactory;

@GSModule("Creates an empty GecosProject objet.\n"
		+ "\nSee: 'AddSourceToGecosProject', 'AddIncDirToGecosProject' and 'AddMacroToGecosProject'  modules.")
public class CreateProject {
	
	private String name;

	@GSModuleConstructor(
			"-arg1: String specifying the project name.\n"
			+ "   '/' charachter is not allowed, will be silently converted to '_'.")
	public CreateProject(String name) {
		// project names do not accept "/"
		this.name=name.replace("/", "_");
	}

	public GecosProject compute() {
		GecosProject project = GecosprojectFactory.eINSTANCE.createGecosProject();
		project.setName(name);
//		project.setMacros(new LinkedHashMap<String, String>());
		return project;
		
	}
	
	public static String getStandardIncludePath() {
		try {
			Bundle bundle = Platform.getBundle("fr.irisa.cairn.gecos.model.includes.std");
			if (bundle == null)
				return WorkspaceLocator.WORKSPACE_PATH+"/fr.irisa.cairn.gecos.model.includes.std/includes/standard";
			URL url = Platform.getBundle("fr.irisa.cairn.gecos.model.includes.std").getEntry("includes/standard");
			if (url == null)
				return WorkspaceLocator.WORKSPACE_PATH+"/fr.irisa.cairn.gecos.model.includes.std/includes/standard";
			url = FileLocator.toFileURL(Platform.getBundle("fr.irisa.cairn.gecos.model.includes.std").getEntry("includes/standard"));
			if (url == null)
				return WorkspaceLocator.WORKSPACE_PATH+"/fr.irisa.cairn.gecos.model.includes.std/includes/standard";
			return url.getPath();
		} catch (IOException e) {
			return WorkspaceLocator.WORKSPACE_PATH+"/fr.irisa.cairn.gecos.model.includes.std/includes/standard";
		}
	}

}
