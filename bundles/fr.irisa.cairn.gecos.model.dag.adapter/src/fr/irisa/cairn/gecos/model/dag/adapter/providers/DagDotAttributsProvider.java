package fr.irisa.cairn.gecos.model.dag.adapter.providers;

import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGEdgeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGNodeAdapter;
import fr.irisa.cairn.gecos.model.tools.visitors.GecosDagDefaultVisitor;
import fr.irisa.cairn.graph.implement.providers.DotAttributsProvider;
import fr.irisa.cairn.graph.io.DOTExport.DotAttributs;
import gecos.dag.DAGControlEdge;
import gecos.dag.DAGDataEdge;
import gecos.dag.DAGFloatImmNode;
import gecos.dag.DAGIntImmNode;
import gecos.dag.DAGOpNode;
import gecos.dag.DAGOutNode;
import gecos.dag.DAGSimpleArrayNode;
import gecos.dag.DAGSymbolNode;
import gecos.dag.DAGVectorArrayAccessNode;
import gecos.dag.DAGVectorExpandNode;
import gecos.dag.DAGVectorExtractNode;
import gecos.dag.DAGVectorOpNode;
import gecos.dag.DAGVectorPackNode;
import gecos.dag.DAGVectorShuffleNode;
import gecos.types.SimdType;

public class DagDotAttributsProvider extends DotAttributsProvider<IDAGNodeAdapter, IDAGEdgeAdapter> {
	private DagShapeProvider shapes = new DagShapeProvider();

	@Override
	protected void findEdgeAttributs(IDAGEdgeAdapter e) {
		super.findEdgeAttributs(e);
		shapes.compute(e);
	}

	@Override
	protected void findNodeAttributs(IDAGNodeAdapter n) {
		super.findNodeAttributs(n);
		shapes.compute(n);
	}

	private class DagShapeProvider extends GecosDagDefaultVisitor {
		private IDAGNodeAdapter node;
		private IDAGEdgeAdapter edge;

		public void compute(IDAGNodeAdapter node) {
			this.node = node;
			node.getAdaptedNode().accept(this);
		}

		public void compute(IDAGEdgeAdapter edge) {
			this.edge = edge;
			edge.getAdaptedEdge().accept(this);
		}

		@Override
		public void visitDAGSimpleArrayNode(DAGSimpleArrayNode d) {
			addNodeProperty(node, DotAttributs.NODE_SHAPE, DotAttributsProvider.SHAPE_RECTANCLE);

			addNodeProperty(node, DotAttributs.STYLE, DotAttributsProvider.STYLE_FILLED);
			if(d.getAnnotations().containsKey("_READ_")) { //XXX
				addNodeProperty(node, DotAttributs.NODE_COLOR, "pink");
			}
			else if(d.getAnnotations().containsKey("_WRITE_")) {//XXX
				addNodeProperty(node, DotAttributs.NODE_COLOR, "orange");
			}
		}

		@Override
		public void visitDAGSymbolNode(DAGSymbolNode d) {
			addNodeProperty(node, DotAttributs.NODE_SHAPE,
					DotAttributsProvider.SHAPE_INV_HOUSE);
			addNodeProperty(node, DotAttributs.STYLE, DotAttributsProvider.STYLE_FILLED);
			addNodeProperty(node, DotAttributs.NODE_COLOR, "grey");
		}

		@Override
		public void visitDAGOutNode(DAGOutNode d) {
			addNodeProperty(node, DotAttributs.NODE_SHAPE,
					DotAttributsProvider.SHAPE_HOUSE);
			addNodeProperty(node, DotAttributs.STYLE, DotAttributsProvider.STYLE_FILLED);
			addNodeProperty(node, DotAttributs.NODE_COLOR, "grey");
		}

		@Override
		public void visitDAGDataEdge(DAGDataEdge d) {
			String color = "blue";
			if(d.getSrc().getParent().getType() instanceof SimdType)
				color = "green"; 
			addEdgeProperty(edge, DotAttributs.EDGE_COLOR, color);
		}

		@Override
		public void visitDAGControlEdge(DAGControlEdge d) {
			addEdgeProperty(edge, DotAttributs.STYLE,
					DotAttributsProvider.STYLE_DASHED);
		}
		
		@Override
		public void visitDAGOpNode(DAGOpNode d) {
			if(d.getOpcode().startsWith("SET")) {
				addNodeProperty(node, DotAttributs.STYLE, DotAttributsProvider.STYLE_FILLED);
				addNodeProperty(node, DotAttributs.NODE_COLOR, "orange");
			}
			else if(d.getAnnotations().containsKey("_INDEX_")) {
				addNodeProperty(node, DotAttributs.STYLE, DotAttributsProvider.STYLE_FILLED);
				addNodeProperty(node, DotAttributs.NODE_COLOR, "red");
			}
			else
				super.visitDAGOpNode(d);
		}
		
		
		@Override
		public void visitDAGIntImmNode(DAGIntImmNode d) {
			addNodeProperty(node, DotAttributs.STYLE, DotAttributsProvider.STYLE_FILLED);
			addNodeProperty(node, DotAttributs.NODE_COLOR, "grey");
		}

		@Override
		public void visitDAGFloatImmNode(DAGFloatImmNode d) {
			addNodeProperty(node, DotAttributs.STYLE, DotAttributsProvider.STYLE_FILLED);
			if(d.getAnnotations().containsKey("_INDEX_")) {
				addNodeProperty(node, DotAttributs.NODE_COLOR, "red");
			}
			else
				addNodeProperty(node, DotAttributs.NODE_COLOR, "grey");
		}
		
		@Override
		public void visitDAGVectorArrayAccessNode(DAGVectorArrayAccessNode d) {
			addNodeProperty(node, DotAttributs.NODE_SHAPE, DotAttributsProvider.SHAPE_BOX3D);
			addNodeProperty(node, DotAttributs.STYLE, DotAttributsProvider.STYLE_FILLED);
			
			if(d.getAnnotations().containsKey("_READ_")) { //XXX
				addNodeProperty(node, DotAttributs.NODE_COLOR, "pink");
			}
			else if(d.getAnnotations().containsKey("_WRITE_")) { //XXX
				addNodeProperty(node, DotAttributs.NODE_COLOR, "orange");
			}
		}

		@Override
		public void visitDAGVectorOpNode(DAGVectorOpNode d) {
			addNodeProperty(node, DotAttributs.STYLE, DotAttributsProvider.STYLE_FILLED);
			addNodeProperty(node, DotAttributs.NODE_COLOR, "green");
		}

		@Override
		public void visitDAGVectorShuffleNode(DAGVectorShuffleNode d) {
			addNodeProperty(node, DotAttributs.STYLE, DotAttributsProvider.STYLE_FILLED);
			addNodeProperty(node, DotAttributs.NODE_SHAPE, DotAttributsProvider.SHAPE_DIAMOND);
			addNodeProperty(node, DotAttributs.NODE_COLOR, "magenta");
		}
		
		@Override
		public void visitDAGVectorExpandNode(DAGVectorExpandNode d) {
			addNodeProperty(node, DotAttributs.STYLE, DotAttributsProvider.STYLE_FILLED);
			addNodeProperty(node, DotAttributs.NODE_SHAPE, DotAttributsProvider.SHAPE_DIAMOND);
			addNodeProperty(node, DotAttributs.NODE_COLOR, "magenta");
		}

		@Override
		public void visitDAGVectorExtractNode(DAGVectorExtractNode d) {
			addNodeProperty(node, DotAttributs.STYLE, DotAttributsProvider.STYLE_FILLED);
			addNodeProperty(node, DotAttributs.NODE_COLOR, "purple");
		}

		@Override
		public void visitDAGVectorPackNode(DAGVectorPackNode d) {
			addNodeProperty(node, DotAttributs.STYLE, DotAttributsProvider.STYLE_FILLED);
			addNodeProperty(node, DotAttributs.NODE_SHAPE, DotAttributsProvider.SHAPE_DIAMOND);
			addNodeProperty(node, DotAttributs.NODE_COLOR, "yellow");
		}
	}
}
