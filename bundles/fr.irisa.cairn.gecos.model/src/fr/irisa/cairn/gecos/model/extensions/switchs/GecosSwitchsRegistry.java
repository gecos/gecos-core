package fr.irisa.cairn.gecos.model.extensions.switchs;

import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;

import fr.irisa.cairn.gecos.model.extensions.GecosRegistry;

/**
 * Register all the EMF switches adapters pointed by the <i>gecos.switches</i>
 * extension point. <br>
 * <br>
 * 
 * For each instance of an adaptable switch (attribute <i>switch</i> of
 * <i>gecos.switches</i> extension point). Result of
 * <code>doSwitch(EObject)</code> function of the adaptable instance is the
 * first non <i>null</i> result of the <code>doSwitch()</code> function
 * of registered adapters instances.
 * 
 * <br>
 * <br>
 * <b>Important : </b> A switch adapter has a reference on the adaptable switch
 * instance. It has to be constructed with only one parameter which is this
 * adaptable switch instance. 
 * <b>Example:</b> <code>
 * 	 MySwitchAdapter(AdaptableSwitch sw)
 * </code>
 * 
 * @author Antoine Floc'h - Initial contribution and API
 * 
 */
public class GecosSwitchsRegistry extends GecosRegistry {
	
	
	public final static GecosSwitchsRegistry INSTANCE = new GecosSwitchsRegistry();

	private final static String EXTENSION_POINT = "fr.irisa.cairn.gecos.model.GecosSwitches";
	private final static String ADAPTABLE_ATTRIBUTE = "switch";
	private final static String ADAPTER_ATTRIBUTE = "adapter";

	private WeakHashMap<Object, List<Object>> adaptersMap;
	private SwitchAdapterFactory factory;

	protected GecosSwitchsRegistry() {
		this.adaptersMap = new WeakHashMap<Object, List<Object>>();
		this.factory = new SwitchAdapterFactory();
	}

	public <T> List<Object> getSwitchsAdaptersFor(IAdaptableSwitch<T> adaptable) {
		List<Object> adapters = adaptersMap.get(adaptable);
		if (adapters == null) {
			adapters = new ArrayList<Object>();
			for (Class<? extends Object> adapterClass : getSwitchsAdaptersClassesFor(adaptable)) {
				Object adapter = factory.buildAdapter(adapterClass, adaptable);
				adapters.add(adapter);
			}
			adaptersMap.put(adaptable, adapters);
		}
		return adapters;
	}

	<T> List<Class<? extends Object>> getSwitchsAdaptersClassesFor(
			IAdaptableSwitch<T> adaptable) {
		List<Class<? extends Object>> adapters = new ArrayList<Class<? extends Object>>();

		for (IExtension e : getExtensions(EXTENSION_POINT)) {
			for (IConfigurationElement iConfigurationElement : e
					.getConfigurationElements()) {
				String adapterClassID = iConfigurationElement
						.getAttribute(ADAPTER_ATTRIBUTE);
				String adaptableClassID = iConfigurationElement
						.getAttribute(ADAPTABLE_ATTRIBUTE);
				Class<IAdaptableSwitch<T>> extAdaptableClass = getClass(
						adaptableClassID, iConfigurationElement);
				if (adaptable.getClass() == extAdaptableClass) {
					Class<? extends Object> adapter = getClass(adapterClassID,iConfigurationElement);
					adapters.add(adapter);
				}

			}
		}
		return adapters;
	}
}
