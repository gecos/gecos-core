#ifndef SIGNAL_H
#define SIGNAL_H
typedef unsigned int sigset_t;

extern int sigemptyset(sigset_t *blocksigs)  ;//Panic("/home/sderrien/workspace/fr.irisa.cairn.gecos.s2s4hls.hmmer/scripts/../src-c/hmmcalibrate.c", 402);
extern int sigaddset(sigset_t *blocksigs, int value)  ; //Panic("/home/sderrien/workspace/fr.irisa.cairn.gecos.s2s4hls.hmmer/scripts/../src-c/hmmcalibrate.c", 403);
extern int sigprocmask(int value, sigset_t *blocksigs, int a) ; //Panic("/home/sderrien/workspace/fr.irisa.cairn.gecos.s2s4hls.hmmer/scripts/../src-c/hmmcalibrate.c", 404);

const int SIGINT  = 0x2;// Interrupt 	ANSI
const int SIGILL  = 0x3;//Illegal instruction 	ANSI
const int SIGABRT = 0x4;//	Abort 	ANSI
const int SIGFPE  = 0x5;//Floating-point exception 	ANSI
const int SIGSEGV = 0x6; //Segmentation violation 	ANSI
const int SIGTERM = 0x7;//Termination 	ANSI

const int SIG_BLOCK = 0x8;//Termination 	ANSI
const int SIG_UNBLOCK = 0x8;//Termination 	ANSI

#endif
