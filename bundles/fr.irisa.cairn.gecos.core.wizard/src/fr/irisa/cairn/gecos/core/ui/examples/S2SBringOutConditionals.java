package fr.irisa.cairn.gecos.core.ui.examples;

import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

import fr.irisa.cairn.gecos.core.ui.Activator;
import fr.irisa.cairn.gecos.core.ui.examples.utils.BasicGecosExampleWizard;

public class S2SBringOutConditionals extends BasicGecosExampleWizard {
	@Override
	public String getResourcesFolderToCopyIntoProject() {
		return "resources/wizard_templates/s2s_bring_out_conditionals/";
	}

	@Override
	public String getPageName() {
		return "Moving the test out from if constructs Example";
	}

	@Override
	public String getPageDescription() {
		return "This wizard will create an example project for Moving the test out from if constructs.";
	}

	@Override
	protected Bundle getPluginBundle() {
		return Platform.getBundle(Activator.PLUGIN_ID);
	}

}
