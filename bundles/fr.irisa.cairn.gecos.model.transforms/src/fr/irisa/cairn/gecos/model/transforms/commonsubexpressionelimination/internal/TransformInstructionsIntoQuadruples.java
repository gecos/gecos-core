package fr.irisa.cairn.gecos.model.transforms.commonsubexpressionelimination.internal;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.gecos.model.analysis.instructions.comparator.InstructionComparator;
import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDefaultVisitor;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.ForC99Block;
import gecos.blocks.IfBlock;
import gecos.blocks.SimpleForBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.CallInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.SetInstruction;
import gecos.types.IntegerType;
import gecos.types.Type;

/**
 * 
 * This pass transforms the instructions in the MIR format (Medium Level
 * Intermediate Representation). A typical MIR statement is a ← b ⊕ c with four
 * components (a, b, c, ⊕). These simple statements are often called quadruples.
 * We use ⊕ to stand for an arbitrary binary operator.
 * 
 * @author Imen Fassi
 * @date 20/04/2016
 */

public class TransformInstructionsIntoQuadruples extends GecosBlocksInstructionsDefaultVisitor {

	private int pos = 0;
	private int counter = 0;
	
	
	
	public TransformInstructionsIntoQuadruples() {
		super();
	}

	private int incr_counter() {
		counter++;
		return counter;
	}

	@Override
	public void visitSimpleForBlock(SimpleForBlock f) {	
		Block parent = f.getParent();
		Scope scp ;
		CompositeBlock cb ;
		BasicBlock newb = GecosUserBlockFactory.BBlock();
		boolean delete = true;
		
		if(parent instanceof CompositeBlock) {
			scp = f.getParent().getScope();
			cb = (CompositeBlock) parent;
			
			int idx = cb.getChildren().indexOf(f);
			if (idx>0) {
				Block pbb = cb.getChildren().get(idx-1);
				if (pbb instanceof BasicBlock) {
					newb = (BasicBlock)pbb;
					delete = false;
				} else
					cb.addBlockBefore(newb, f);
			}
			if (idx == 0)
				cb.addBlockBefore(newb, f);
				
		} else {
			BasicBlock aux = GecosUserBlockFactory.BBlock();
			parent.replace(f, aux);
			cb = GecosUserBlockFactory.CompositeBlock(newb,f);
			scp = cb.getScope();
			parent.replace(aux, cb);
		}
		
		ParserOfConditionalExpr condparse = new ParserOfConditionalExpr(scp);

		if (f.getInitBlock() != null)
			f.getInitBlock().accept(condparse);

		if (f.getTestBlock() != null)
			f.getTestBlock().accept(condparse);

		if (f.getStepBlock() != null)
			f.getStepBlock().accept(condparse);

		if (f.getBodyBlock() != null)
			f.getBodyBlock().accept(this);

		ArrayList<Instruction> listOfInstr = condparse.getlistInst();		

		if (!listOfInstr.isEmpty()) {
			for (Instruction instr : listOfInstr)
				newb.addInstruction(instr);
		} else
			if (delete)
				cb.removeBlock(newb);
	}

	@Override
	public void visitForC99Block(ForC99Block f) {
		boolean delete = true;
		Block parent = f.getParent();
		Scope scp ;
		CompositeBlock cb ;
		BasicBlock newb = GecosUserBlockFactory.BBlock();
		
		if(parent instanceof CompositeBlock) {
			scp = f.getParent().getScope();
			cb = (CompositeBlock) parent;
			
			int idx = cb.getChildren().indexOf(f);
			if (idx>0) {
				Block pbb = cb.getChildren().get(idx-1);
				if (pbb instanceof BasicBlock) {
					newb = (BasicBlock)pbb;
					delete = false;
				} else
					cb.addBlockBefore(newb, f);
			}
			if (idx == 0)
				cb.addBlockBefore(newb, f);
			
		} else {
			BasicBlock aux = GecosUserBlockFactory.BBlock();
			parent.replace(f, aux);
			cb = GecosUserBlockFactory.CompositeBlock(newb,f);
			scp = cb.getScope();
			parent.replace(aux, cb);
		}
		
		ParserOfConditionalExpr condparse = new ParserOfConditionalExpr(scp);

		if (f.getInitBlock() != null)
			f.getInitBlock().accept(condparse);

		if (f.getTestBlock() != null)
			f.getTestBlock().accept(condparse);

		if (f.getStepBlock() != null)
			f.getStepBlock().accept(condparse);

		if (f.getBodyBlock() != null)
			f.getBodyBlock().accept(this);

		ArrayList<Instruction> listOfInstr = condparse.getlistInst();

		if (!listOfInstr.isEmpty()) {
			for (Instruction instr : listOfInstr)
				newb.addInstruction(instr);
		} else
			if (delete)
				cb.removeBlock(newb);
	}

	@Override
	public void visitForBlock(ForBlock f) {
		boolean delete = true;
		Block parent = f.getParent();
		Scope scp ;
		CompositeBlock cb ;
		BasicBlock newb = GecosUserBlockFactory.BBlock();
		
		if(parent instanceof CompositeBlock) {
			scp = f.getParent().getScope();
			cb = (CompositeBlock) parent;
			int idx = cb.getChildren().indexOf(f);
			if (idx>0) {
				Block pbb = cb.getChildren().get(idx-1);
				if (pbb instanceof BasicBlock) {
					newb = (BasicBlock)pbb;
					delete = false;
				} else
					cb.addBlockBefore(newb, f);
			}
			if (idx == 0)
				cb.addBlockBefore(newb, f);

		} else {
			BasicBlock aux = GecosUserBlockFactory.BBlock();
			parent.replace(f, aux);
			cb = GecosUserBlockFactory.CompositeBlock(newb,f);
			scp = cb.getScope();
			parent.replace(aux, cb);
		}
		
		ParserOfConditionalExpr condparse = new ParserOfConditionalExpr(scp);

		if (f.getInitBlock() != null)
			f.getInitBlock().accept(condparse);

		if (f.getTestBlock() != null)
			f.getTestBlock().accept(condparse);

		if (f.getStepBlock() != null)
			f.getStepBlock().accept(condparse);

		if (f.getBodyBlock() != null)
			f.getBodyBlock().accept(this);

		ArrayList<Instruction> listOfInstr = condparse.getlistInst();

		if (!listOfInstr.isEmpty()) {
			for (Instruction instr : listOfInstr)
				newb.addInstruction(instr);
		} else
			if (delete)
				cb.removeBlock(newb);
	}

	@Override
	public void visitIfBlock(IfBlock i) {
		boolean delete = true;
		Block parent = i.getParent();
		Scope scp ;
		CompositeBlock cb ;
		BasicBlock newb = GecosUserBlockFactory.BBlock();
		
		if(parent instanceof CompositeBlock) {
			scp = i.getParent().getScope();
			cb = (CompositeBlock) parent;
			int idx = cb.getChildren().indexOf(i);
			if (idx>0) {
				Block pbb = cb.getChildren().get(idx-1);
				if (pbb instanceof BasicBlock) {
					newb = (BasicBlock)pbb;
					delete = false;
				} else
					cb.addBlockBefore(newb, i);
			}
			if (idx == 0)
				cb.addBlockBefore(newb, i);
		} else {
			BasicBlock aux = GecosUserBlockFactory.BBlock();
			parent.replace(i, aux);
			cb = GecosUserBlockFactory.CompositeBlock(newb,i);
			scp = cb.getScope();
			parent.replace(aux, cb);
		}
		
		ParserOfConditionalExpr condparse = new ParserOfConditionalExpr(scp);

		if (i.getTestBlock() != null)
			i.getTestBlock().accept(condparse);

		if (i.getThenBlock() != null)
			i.getThenBlock().accept(this);

		if (i.getElseBlock() != null)
			i.getElseBlock().accept(this);

		ArrayList<Instruction> listOfInstr = condparse.getlistInst();

		if (!listOfInstr.isEmpty()) {
			for (Instruction instr : listOfInstr) 
				newb.addInstruction(instr);
		} else
			if(delete)
				cb.removeBlock(newb);
	}

	@Override
	public void visitLoopBlock(DoWhileBlock l) {
		boolean delete = true;
		Block parent = l.getParent();
		Scope scp ;
		CompositeBlock cb ;
		BasicBlock newb = GecosUserBlockFactory.BBlock();
		
		if(parent instanceof CompositeBlock) {
			scp = l.getParent().getScope();
			cb = (CompositeBlock) parent;
			
			int idx = cb.getChildren().indexOf(l);
			if (idx>0) {
				Block pbb = cb.getChildren().get(idx-1);
				if (pbb instanceof BasicBlock) {
					newb = (BasicBlock)pbb;
					delete = false;
				} else
					cb.addBlockBefore(newb, l);
			}
			if (idx == 0)
				cb.addBlockBefore(newb, l);
			
		} else {
			BasicBlock aux = GecosUserBlockFactory.BBlock();
			parent.replace(l, aux);
			cb = GecosUserBlockFactory.CompositeBlock(newb,l);
			scp = cb.getScope();
			parent.replace(aux, cb);
		}
		
		ParserOfConditionalExpr condparse = new ParserOfConditionalExpr(scp);

		if (l.getTestBlock() != null)
			l.getTestBlock().accept(condparse);

		if (l.getBodyBlock() != null)
			l.getBodyBlock().accept(this);

		ArrayList<Instruction> listOfInstr = condparse.getlistInst();
		
		if (!listOfInstr.isEmpty()) {
			for (Instruction instr : listOfInstr)
				newb.addInstruction(instr);
		} else
			if (delete)
				cb.removeBlock(newb);
	}

	@Override
	public void visitWhileBlock(WhileBlock w) {
		boolean delete = true;
		Block parent = w.getParent();
		Scope scp ;
		CompositeBlock cb ;
		BasicBlock newb = GecosUserBlockFactory.BBlock();
		
		if(parent instanceof CompositeBlock) {
			scp = w.getParent().getScope();
			cb = (CompositeBlock) parent;
			
			int idx = cb.getChildren().indexOf(w);
			if (idx>0) {
				Block pbb = cb.getChildren().get(idx-1);
				if (pbb instanceof BasicBlock) {
					newb = (BasicBlock)pbb;
					delete = false;
				} else
					cb.addBlockBefore(newb, w);
			}
			if (idx == 0)
				cb.addBlockBefore(newb, w);
			
		} else {
			BasicBlock aux = GecosUserBlockFactory.BBlock();
			parent.replace(w, aux);
			cb = GecosUserBlockFactory.CompositeBlock(newb,w);
			scp = cb.getScope();
			parent.replace(aux, cb);
		}
		
		ParserOfConditionalExpr condparse = new ParserOfConditionalExpr(scp);

		if (w.getTestBlock() != null)
			w.getTestBlock().accept(condparse);

		if (w.getBodyBlock() != null)
			w.getBodyBlock().accept(this);

		ArrayList<Instruction> listOfInstr = condparse.getlistInst();
		
		if (!listOfInstr.isEmpty()) {
			for (Instruction instr : listOfInstr)
				newb.addInstruction(instr);
		} else
			if (delete)
				cb.removeBlock(newb);
	}

	@Override
	public void visitBasicBlock(BasicBlock b) {
		pos = 0;
		for (Instruction inst : new ArrayList<Instruction>(b.getInstructions())) {
			inst.accept(this);
			pos++;
		}
	}

	@Override
	public void visitGenericInstruction(GenericInstruction gi) {
		/**
		 * Arithmetic, Comparison and logical operator are generic instruction.
		 */
		super.visitGenericInstruction(gi);
		if ((gi.getParent() != null) && (gi.getParent() instanceof GenericInstruction)) {
			/**
			 * get the containing basic block
			 */
			BasicBlock bb = gi.getBasicBlock();
			Scope scope = bb.getScope();
			
			Block parent  = bb.getParent();
			if (parent != null)
			if (!(parent instanceof CompositeBlock)) {
				BasicBlock aux = GecosUserBlockFactory.BBlock();
				parent.replace(bb, aux);
				CompositeBlock cb = GecosUserBlockFactory.CompositeBlock(bb);
				parent.replace( aux, cb);
				scope = cb.getScope();
			}
			
			GenericInstruction ggi = (GenericInstruction) gi.copy();
			/**
			 * Create a symbol with the appropriate type
			 */
			Symbol symb = null;
			
			String name = new_temp(scope);
			Type type = gi.getType();

			if (bb.getInstructions().get(pos) instanceof SetInstruction) {
				SetInstruction instPos = (SetInstruction) bb.getInstructions().get(pos);
				type = instPos.getDest().getType();
			}

			if (bb.getInstructions().get(pos) instanceof CallInstruction) {
				CallInstruction instPos = (CallInstruction) bb.getInstructions().get(pos);
				List<Instruction> listArgs = new ArrayList<Instruction>(instPos.getArgs());
				for (Instruction instr : listArgs) {
					String instr_str = InstructionComparator.instToString(instr);
					if (instr_str.equals(InstructionComparator.instToString(gi))) {
						type = instr.getType();
					}
				}
			}

			TypeAnalyzer type_var = new TypeAnalyzer(type);
			if (type_var.getBaseLevel().isInt()) {
				IntegerType base = type_var.getBaseLevel().getBaseAsInt();
				if (!base.getSigned() && base.getSize() == 1)
					type = GecosUserTypeFactory.INT();
			}

			symb = GecosUserCoreFactory.symbol(name, type, scope);

			/**
			 * Create the Symbol Instruction
			 */
			Instruction symbInstrTMP = GecosUserInstructionFactory.symbref(symb);

			/**
			 * Create a new SetInstruction
			 */
			Instruction setInstTEMP = GecosUserInstructionFactory.set(symbInstrTMP, ggi);

			bb.getInstructions().add(pos, setInstTEMP);
			pos++;
			/**
			 * Create the SymbolInstruction
			 */
			Instruction symbInstrTMP2 = GecosUserInstructionFactory.symbref(symb);
			/**
			 * replace gi by the SymbolInstruction
			 */
			gi.getParent().replaceChild(gi, symbInstrTMP2);
		}
	}

	/**
	 * returns a new temporary name and verifies if The symbol is already used
	 * or not
	 */
	private String new_temp(Scope scope) {
		String name;
		boolean ok;
		if (scope != null) {
			do {
				name = "_t" + incr_counter();
				ok = true;
				for (Symbol s : scope.getSymbols()) {
					if (s.getName().equals(name)) {
						ok = false;
						break;
					}
				}
			} while (ok == false);
		} else
			name = "_t" + incr_counter();
		return name;
	}

	private class ParserOfConditionalExpr extends GecosBlocksInstructionsDefaultVisitor {

		private ArrayList<Instruction> listInst = null;
		Scope scope ;
		private int ps ;

		public ArrayList<Instruction> getlistInst() {
			return this.listInst;
		}

		public ParserOfConditionalExpr(Scope s) {
			super();
			this.listInst = new ArrayList<Instruction>();
			this.scope = s;
		}

		@Override
		public void visitBasicBlock(BasicBlock b) {
			ps = 0;
			for (Instruction inst : new ArrayList<Instruction>(b.getInstructions())) {
				inst.accept(this);
				ps++;
			}
		}

		@Override
		public void visitGenericInstruction(GenericInstruction gi) {
			super.visitGenericInstruction(gi);
			if ((gi.getParent() != null) && (gi.getParent() instanceof GenericInstruction)) {
				/**
				 * get the containing basic block
				 */
				BasicBlock bb = gi.getBasicBlock();
				/**
				 * Create a symbol with the appropriate type
				 */
				Symbol symb = null;
				String name = new_temp(scope);

				Type type = gi.getType();

				if (bb.getInstructions().get(ps) instanceof SetInstruction) {
					SetInstruction instPos = (SetInstruction) bb.getInstructions().get(ps);
					type = instPos.getDest().getType();
				}
				if (bb.getInstructions().get(ps) instanceof CallInstruction) {
					CallInstruction instPos = (CallInstruction) bb.getInstructions().get(ps);
					List<Instruction> listArgs = new ArrayList<Instruction>(instPos.getArgs());
					for (Instruction instr : listArgs) {
						String instr_str = InstructionComparator.instToString(instr);
						if (instr_str.equals(InstructionComparator.instToString(gi))) {
							type = instr.getType();
						}
					}
				}

				TypeAnalyzer type_var = new TypeAnalyzer(type);
				if (type_var.getBaseLevel().isInt()) {
					IntegerType base = type_var.getBaseLevel().getBaseAsInt();
					if (!base.getSigned() && base.getSize() == 1)
						type = GecosUserTypeFactory.INT();
				}

				symb = GecosUserCoreFactory.symbol(name, type, scope);
				
				/**
				 * Create the SymbolInstruction
				 */
				Instruction symbInstrTMP2 = GecosUserInstructionFactory.symbref(symb);
				/**
				 * replace gi by the SymbolInstruction
				 */
				gi.getParent().replaceChild(gi, symbInstrTMP2);
			
				/**
				 * Create the Symbol Instruction
				 */
				Instruction symbInstrTMP = GecosUserInstructionFactory.symbref(symb);
				
				/**
				 * Create a new SetInstruction
				 */
				Instruction setInstTEMP = GecosUserInstructionFactory.set(symbInstrTMP, gi);
				listInst.add(setInstTEMP);
				
			}
		}

	}

}
