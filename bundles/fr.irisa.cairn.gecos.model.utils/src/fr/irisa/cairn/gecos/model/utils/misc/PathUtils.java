package fr.irisa.cairn.gecos.model.utils.misc;

import java.io.File;
import java.nio.file.Path;

import fr.irisa.r2d2.gecos.framework.utils.FileUtils;

public class PathUtils {

	/**
	 * Create a new directory named {@code name} in the the specified {@code parentDir}.
	 * 
	 * @param parentDir where the new directory will be created
	 * @param name the new dir's name
	 * @param deleteFirst if true delete the directory if it already exists before creating a new one.
	 * @return
	 */
	public static Path createDir(Path parentDir, String name, boolean deleteFirst) {
		Path dir = parentDir.resolve(name);
		File dirF = dir.toFile();
		if(deleteFirst && dirF.isDirectory())
			FileUtils.deleteRecursive(dir.toString());
		// see https://stackoverflow.com/questions/5189534/how-to-use-mkdirs-in-a-thread-safe-manner-in-java
		while (!dirF.mkdirs()) { Thread.yield(); }
		return dir;
	}

}
