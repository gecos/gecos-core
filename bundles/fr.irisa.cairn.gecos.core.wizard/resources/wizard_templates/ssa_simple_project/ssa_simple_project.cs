p = CreateGecosProject("ssa_simple_project");  # Create a empty GeCoS project
AddSourceToGecosProject(p,"src/ssa_simple_example.c"); # Add new C sources in the project
CDTFrontend(p); # Generate the GeCoS IR from the C source added

ComputeSSAForm(p); # Apply SSA form on the GeCoS IR

SaveGecosProject(p,"ssa_applied.gecosproject"); # Save the GeCoS IR into gecosproject format
output(p,"dot","dot_applied/"); # Generate a dotty format of the GeCoS IR

RemoveSSAForm(p); # Remove SSA form on the GeCoS IR

SaveGecosProject(p,"ssa_removed.gecosproject"); # Save the GeCoS IR into gecosproject format
output(p,"dot","dot_removed/"); # Generate a dotty format of the GeCoS IR