package fr.irisa.cairn.gecos.model.instructions.utils;

import static fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory.scope;
import static fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory.symbol;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.Int;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.call;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.methodCallInstruction;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.set;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.symbref;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.INT;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.tngtech.java.junit.dataprovider.DataProvider;

import fr.irisa.cairn.gecos.core.testframework.utils.GSProjectUtils;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.type.utils.Utils;
import gecos.blocks.BasicBlock;
import gecos.blocks.CompositeBlock;
import gecos.core.Procedure;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.instrs.Instruction;
import gecos.instrs.SymbolInstruction;

public class InstructionDataProvider {

	private InstructionDataProvider() {}
	
	private static Scope tscope = scope();
	
	private static Stream<BasicBlock> streamBasicBlocks() {
		GecosUserTypeFactory.setScope(tscope);
		
		return Stream.of(
			parseCInst(
				"int a;",
				//------------
				"a = 1;"
			),
			parseCInst(
				"int a;",
				"int b;",
				"int X[100];",
				//------------
				"a = b + 1;",
				"X[0] = 1;",
				"X[X[0]] = a;",
				"f(2*a+1, X[1], 3);",
				"X[X[0]] = f(2*a+1, X[1], 3);"
			)
		);
	}
	
	@DataProvider
	public static Object[][] provideBasicBlocks() {
		return Utils.toRaw(Utils.merge(streamBasicBlocks()));
	}

	@DataProvider
	public static Object[][] provideComplexInstructions() {
		Stream<Instruction> fromC = streamBasicBlocks()
			.flatMap(bb -> bb.getInstructions().stream());
		
		Stream<Instruction> custom = Stream.of(
			set(sym("a") , null),
			call(symrf("object"), symrf("a"), Int(3)),
			methodCallInstruction("method", symrf("object"), symrf("a"), Int(3))
		);
		
		return Utils.toRaw(Utils.merge(
			fromC,
			custom
		));
	}

	private static SymbolInstruction symrf(String name) {
		return symbref(sym(name));
	}
	
	public static Symbol sym(String name) {
		Symbol s = tscope.lookup(name);
		if(s == null)
			s = symbol(name, INT(), tscope);
		return s;
	}
	
	/**
	 * Join lines with '\n'.
	 * 
	 * @param lines lines
	 * @return
	 */
	public static String joinLines(String... lines) {
		return Stream.of(lines).collect(Collectors.joining("\n"));
	}
	
	public static BasicBlock parseCInst(String... cCodeLines) {
		return parseCInst(joinLines(cCodeLines));
	}
	
	/**
	 * Assumes that cCode is a String that represents a valid sequence of C instructions (BasicBlock).
	 * 
	 * @param cCode a String that represents a valid sequence of C instructions (BasicBlock).
	 * @return the list of instructions in the first BasicBlock in the body of the first Procedure.
	 */
	public static BasicBlock parseCInst(String cCode) {
		cCode = "void main() { " + cCode + "}";
		
		Path cFile = Utils.wrapInCFile(cCode);
		GecosProject proj = GSProjectUtils.initGecosProject("tmp", cFile);
		
		Procedure proc = proj.getSources().get(0).getModel().listProcedures().get(0);
		BasicBlock bb = (BasicBlock) ((CompositeBlock) proc.getBody().getChildren().get(1)).getChildren().get(0);
		
		try {
			Files.delete(cFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return bb;
	}
	
}
