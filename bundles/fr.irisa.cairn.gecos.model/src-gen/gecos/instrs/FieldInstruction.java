/**
 */
package gecos.instrs;

import gecos.types.Field;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Field Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.FieldInstruction#getField <em>Field</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getFieldInstruction()
 * @model
 * @generated
 */
public interface FieldInstruction extends ExprComponent {
	/**
	 * Returns the value of the '<em><b>Field</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Field</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Field</em>' reference.
	 * @see #setField(Field)
	 * @see gecos.instrs.InstrsPackage#getFieldInstruction_Field()
	 * @model resolveProxies="false"
	 * @generated
	 */
	Field getField();

	/**
	 * Sets the value of the '{@link gecos.instrs.FieldInstruction#getField <em>Field</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Field</em>' reference.
	 * @see #getField()
	 * @generated
	 */
	void setField(Field value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitFieldInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _elvis = null;\n&lt;%gecos.types.Type%&gt; _type = this.getType();\n&lt;%java.lang.String%&gt; _string = null;\nif (_type!=null)\n{\n\t_string=_type.toString();\n}\nif (_string != null)\n{\n\t_elvis = _string;\n} else\n{\n\t_elvis = \"?\";\n}\n&lt;%java.lang.String%&gt; _plus = (\"((\" + _elvis);\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \")\");\n&lt;%java.lang.String%&gt; _elvis_1 = null;\n&lt;%gecos.types.Field%&gt; _field = this.getField();\n&lt;%java.lang.String%&gt; _string_1 = null;\nif (_field!=null)\n{\n\t_string_1=_field.toString();\n}\nif (_string_1 != null)\n{\n\t_elvis_1 = _string_1;\n} else\n{\n\t_elvis_1 = \"?\";\n}\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + _elvis_1);\nreturn (_plus_2 + \")\");'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" objUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((obj instanceof &lt;%gecos.instrs.FieldInstruction%&gt;))\n{\n\t&lt;%gecos.types.Field%&gt; _field = ((&lt;%gecos.instrs.FieldInstruction%&gt;)obj).getField();\n\tboolean _tripleNotEquals = (_field != null);\n\tif (_tripleNotEquals)\n\t{\n\t\treturn (super.equals(obj) &amp;&amp; ((&lt;%gecos.instrs.FieldInstruction%&gt;)obj).getField().equals(this.getField()));\n\t}\n}\nreturn false;'"
	 * @generated
	 */
	boolean equals(Object obj);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.FieldInstruction%&gt;))\n{\n\tboolean isSame = false;\n\tif (((((&lt;%gecos.instrs.FieldInstruction%&gt;)instr).getField() == null) &amp;&amp; (this.getField() == null)))\n\t{\n\t\tisSame = true;\n\t}\n\telse\n\t{\n\t\t&lt;%gecos.types.Field%&gt; _field = ((&lt;%gecos.instrs.FieldInstruction%&gt;)instr).getField();\n\t\tboolean _tripleEquals = (_field == null);\n\t\t&lt;%gecos.types.Field%&gt; _field_1 = this.getField();\n\t\tboolean _tripleEquals_1 = (_field_1 == null);\n\t\tboolean _xor = (_tripleEquals ^ _tripleEquals_1);\n\t\tif (_xor)\n\t\t{\n\t\t\treturn false;\n\t\t}\n\t\telse\n\t\t{\n\t\t\tisSame = ((&lt;%gecos.instrs.FieldInstruction%&gt;)instr).getField().isEqual(this.getField());\n\t\t}\n\t}\n\treturn (isSame &amp;&amp; super.isSame(instr));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // FieldInstruction
