/**
 */
package gecos.types;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scalar Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.types.TypesPackage#getScalarType()
 * @model abstract="true"
 * @generated
 */
public interface ScalarType extends Type, TypesVisitable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return super.toString();'"
	 * @generated
	 */
	String toString();

} // ScalarType
