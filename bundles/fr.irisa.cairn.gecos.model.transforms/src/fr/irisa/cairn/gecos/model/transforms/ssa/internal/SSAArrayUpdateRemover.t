/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.transforms.ssa;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDefaultVisitor;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.core.Procedure;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.instrs.SetInstruction;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;

/**
 * Remove SSA array "update" instructions introduced by SSA form.
 * 
 * @author llhours
 */
@SuppressWarnings("all")
public class SSAArrayUpdateRemover extends GecosBlocksInstructionsDefaultVisitor {

	%include { sl.tom }
	%include { gecos_common.tom }
	%include { gecos_terminals.tom }  
	%include { gecos_basic.tom }
	%include { gecos_arithmetic.tom }  
	%include { gecos_compare.tom } 
	%include { gecos_logical.tom } 
    %include { gecos_misc.tom } 
    
	private final static boolean VERBOSE = false;

	private static void debug(String mess) {
		if(VERBOSE) System.out.println(mess);
	}
	private BasicBlock currentBasic;

	private Block block;
 
	public SSAArrayUpdateRemover(Procedure proc) {
		this.block = proc.getBody();
	}

	public SSAArrayUpdateRemover(Block block) {
		this.block = block;
	}
 
	public void compute() {
		block.accept(this);
	}

	@Override
	protected void visitInstruction(Instruction i) {
		debug("Falling back to default behavior for "+i+" of type "+i.getClass().getSimpleName());
	}
  
	@Override
	public void visitBasicBlock(BasicBlock b) {
		currentBasic = b;
		super.visitBasicBlock(b);
		currentBasic = null;
	}
 
	@Override 
	public void visitSetInstruction(SetInstruction inst) {
		%match (Inst inst){
			a@set(_, c@generic("update", InstL(sym,value,index)))-> {
				SetInstruction set= (SetInstruction) `a;
				debug("Remove SSA update "+(`c)+" in "+(`a));
				Instruction newSrc = (`value).copy();
				set.setSource(newSrc); 
				set.setDest(`sarray(sym,InstL(index)));  
			} 
		}		
	}
} 