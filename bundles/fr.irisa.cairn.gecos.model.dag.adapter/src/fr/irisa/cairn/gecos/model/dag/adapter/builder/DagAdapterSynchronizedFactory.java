package fr.irisa.cairn.gecos.model.dag.adapter.builder;

import com.google.inject.Inject;

import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGCollapsedNodeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGEdgeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGInstructionAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGNodeAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGPortAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.observers.IDagSynchronizationObserver;
import gecos.dag.DAGEdge;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGNode;
import gecos.dag.DAGPort;

public class DagAdapterSynchronizedFactory extends AdapterFactory{
	@Inject
	private IDagSynchronizationObserver update;


	@Override
	public IDAGInstructionAdapter createGraphAdapter(DAGInstruction adapted,
			boolean directed) {
		IDAGInstructionAdapter a =  super.createGraphAdapter(adapted, directed);
		a.getObservers().add(update);
		return a;
	}
	

	@Override
	public IDAGNodeAdapter createNodeAdapter(DAGNode adapted) {
		IDAGNodeAdapter a = super.createNodeAdapter(adapted);
		a.getObservers().add(update);
		return a;
	}
	
	@Override
	public IDAGEdgeAdapter createEdgeAdapter(DAGEdge adapted,
			IDAGPortAdapter source, IDAGPortAdapter sink) {
		IDAGEdgeAdapter a = super.createEdgeAdapter(adapted, source, sink);
		a.getObservers().add(update);
		return a;
	}
	
	@Override
	public IDAGPortAdapter createPortAdapter(DAGPort adapted) {
		IDAGPortAdapter a = super.createPortAdapter(adapted);
		a.getObservers().add(update);
		return a;
	}
	
	@Override
	public IDAGCollapsedNodeAdapter createCollapsedNodeAdapter(DAGNode adapted) {
		IDAGCollapsedNodeAdapter a = super.createCollapsedNodeAdapter(adapted);
		a.getObservers().add(update);
		return a;
	}
}
