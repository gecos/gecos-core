/**
 */
package gecos.instrs;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cond Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.CondInstruction#getLabel <em>Label</em>}</li>
 *   <li>{@link gecos.instrs.CondInstruction#getCond <em>Cond</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getCondInstruction()
 * @model
 * @generated
 */
public interface CondInstruction extends BranchInstruction {
	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see gecos.instrs.InstrsPackage#getCondInstruction_Label()
	 * @model unique="false" dataType="gecos.instrs.String"
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link gecos.instrs.CondInstruction#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Cond</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cond</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cond</em>' containment reference.
	 * @see #setCond(Instruction)
	 * @see gecos.instrs.InstrsPackage#getCondInstruction_Cond()
	 * @model containment="true"
	 * @generated
	 */
	Instruction getCond();

	/**
	 * Sets the value of the '{@link gecos.instrs.CondInstruction#getCond <em>Cond</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cond</em>' containment reference.
	 * @see #getCond()
	 * @generated
	 */
	void setCond(Instruction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.instrs.Instruction%&gt;&gt;asEList(this.getCond()));'"
	 * @generated
	 */
	EList<Instruction> listChildren();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitCondInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" objUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean sameBranchType = false;\nif ((obj instanceof &lt;%gecos.instrs.CondInstruction%&gt;))\n{\n\tsameBranchType = true;\n}\nreturn (super.equals(obj) &amp;&amp; sameBranchType);'"
	 * @generated
	 */
	boolean equals(Object obj);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _elvis = null;\n&lt;%gecos.instrs.Instruction%&gt; _cond = this.getCond();\n&lt;%java.lang.String%&gt; _string = null;\nif (_cond!=null)\n{\n\t_string=_cond.toString();\n}\nif (_string != null)\n{\n\t_elvis = _string;\n} else\n{\n\t_elvis = \"\";\n}\nreturn _elvis;'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.CondInstruction%&gt;))\n{\n\tboolean isSameLabel = false;\n\tif (((((&lt;%gecos.instrs.CondInstruction%&gt;)instr).getLabel() == null) &amp;&amp; (this.getLabel() == null)))\n\t{\n\t\tisSameLabel = true;\n\t}\n\telse\n\t{\n\t\t&lt;%java.lang.String%&gt; _label = ((&lt;%gecos.instrs.CondInstruction%&gt;)instr).getLabel();\n\t\tboolean _tripleEquals = (_label == null);\n\t\t&lt;%java.lang.String%&gt; _label_1 = this.getLabel();\n\t\tboolean _tripleEquals_1 = (_label_1 == null);\n\t\tboolean _xor = (_tripleEquals ^ _tripleEquals_1);\n\t\tif (_xor)\n\t\t{\n\t\t\treturn false;\n\t\t}\n\t\telse\n\t\t{\n\t\t\tisSameLabel = ((&lt;%gecos.instrs.CondInstruction%&gt;)instr).getLabel().equals(this.getLabel());\n\t\t}\n\t}\n\tboolean isSameCond = false;\n\tif (((((&lt;%gecos.instrs.CondInstruction%&gt;)instr).getCond() == null) &amp;&amp; (this.getCond() == null)))\n\t{\n\t\tisSameCond = true;\n\t}\n\telse\n\t{\n\t\t&lt;%gecos.instrs.Instruction%&gt; _cond = ((&lt;%gecos.instrs.CondInstruction%&gt;)instr).getCond();\n\t\tboolean _tripleEquals_2 = (_cond == null);\n\t\t&lt;%gecos.instrs.Instruction%&gt; _cond_1 = this.getCond();\n\t\tboolean _tripleEquals_3 = (_cond_1 == null);\n\t\tboolean _xor_1 = (_tripleEquals_2 ^ _tripleEquals_3);\n\t\tif (_xor_1)\n\t\t{\n\t\t\treturn false;\n\t\t}\n\t\telse\n\t\t{\n\t\t\tisSameCond = ((&lt;%gecos.instrs.CondInstruction%&gt;)instr).getCond().isSame(this.getCond());\n\t\t}\n\t}\n\treturn ((super.isSame(instr) &amp;&amp; isSameCond) &amp;&amp; isSameLabel);\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // CondInstruction
