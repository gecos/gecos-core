package $packageName$.templates.xtend

import custom.type.FooType
import org.eclipse.emf.ecore.EObject
import templates.xtend.GecosTemplate

/**
 * Extension implementation of the GeCoS C generator
 */
class CustomTypeTemplate extends GecosTemplate {
	
	def dispatch generate(EObject o){
		null
	}
	
	/**
	 * Implement how is pretty print the FooInstruction object
	 */
	def dispatch generate(FooType foo){
		'''FooType'''			
	}
}