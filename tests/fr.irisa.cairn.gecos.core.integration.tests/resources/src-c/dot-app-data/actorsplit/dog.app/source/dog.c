#include "defs.h"
#include "data_size.h"


void dog(TYPE_0 inout[M][N]) { //, TYPE_0 CV[MC], TYPE_0 CH[NC]) { //XXX
	TYPE_0 tmp[M][N] = {0};
	int i, j, t;
	TYPE_0 CV[3] = {1, 2, 1}; //XXX
	TYPE_0 CH[3] = {1, 2, 1}; //XXX

	for(t=0; t<= T; t++) {
		for (i = 1; i < M-1; i++)
			for (j = 0; j < N; j++)
				tmp[i][j] = CV[0]*inout[i-1][j] + CV[1]*inout[i][j] + CV[2]*inout[i+1][j];

		for (i = 1; i < M-1; i++)
			for (j = 1; j< N-1; j++)
				inout[i][j] = CH[0]*tmp[i][j-1] + CH[1]*tmp[i][j] + CH[2]*tmp[i][j+1];
	}
}
