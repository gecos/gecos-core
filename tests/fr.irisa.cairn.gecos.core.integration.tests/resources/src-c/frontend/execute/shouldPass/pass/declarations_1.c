typedef char T1, *T2, T3[5], *T4[7], (*T5)[10], (*T6[3])[6], ((T7[2])[4]);


int main() {
	if(sizeof(T1) != sizeof(char))
		abort();
	if(sizeof(T2) != sizeof(char*))
		abort();
	if(sizeof(T3) != 5*sizeof(char))
		abort();
	if(sizeof(T4) != 7*sizeof(char*))
		abort();
	if(sizeof(T5) != sizeof(char (*)[10]))
		abort();
	if(sizeof(T6) != 3*sizeof(char (*)[6]))
		abort();
	if(sizeof(T7) != 2*4*sizeof(char))
		abort();

	exit(0);
}
