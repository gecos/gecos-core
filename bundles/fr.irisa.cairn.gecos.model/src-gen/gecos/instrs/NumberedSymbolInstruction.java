/**
 */
package gecos.instrs;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Numbered Symbol Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.NumberedSymbolInstruction#getNumber <em>Number</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getNumberedSymbolInstruction()
 * @model
 * @generated
 */
public interface NumberedSymbolInstruction extends SymbolInstruction {
	/**
	 * Returns the value of the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number</em>' attribute.
	 * @see #setNumber(int)
	 * @see gecos.instrs.InstrsPackage#getNumberedSymbolInstruction_Number()
	 * @model unique="false" dataType="gecos.instrs.int"
	 * @generated
	 */
	int getNumber();

	/**
	 * Sets the value of the '{@link gecos.instrs.NumberedSymbolInstruction#getNumber <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number</em>' attribute.
	 * @see #getNumber()
	 * @generated
	 */
	void setNumber(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitNumberedSymbolInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _string = super.toString();\n&lt;%java.lang.String%&gt; _plus = (_string + \"_\");\nint _number = this.getNumber();\nreturn (_plus + &lt;%java.lang.Integer%&gt;.valueOf(_number));'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" objUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((obj instanceof &lt;%gecos.instrs.NumberedSymbolInstruction%&gt;))\n{\n\treturn (super.getSymbol().equals(((&lt;%gecos.instrs.NumberedSymbolInstruction%&gt;)obj).getSymbol()) &amp;&amp; (((&lt;%gecos.instrs.NumberedSymbolInstruction%&gt;)obj).getNumber() == this.getNumber()));\n}\nreturn false;'"
	 * @generated
	 */
	boolean equals(Object obj);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getAnnotation(\"\");\nreturn null;'"
	 * @generated
	 */
	Instruction getDefinition();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.NumberedSymbolInstruction%&gt;))\n{\n\treturn (super.isSame(instr) &amp;&amp; (this.getNumber() == ((&lt;%gecos.instrs.NumberedSymbolInstruction%&gt;)instr).getNumber()));\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // NumberedSymbolInstruction
