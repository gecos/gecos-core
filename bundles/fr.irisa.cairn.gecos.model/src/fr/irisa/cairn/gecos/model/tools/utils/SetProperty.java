/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.tools.utils;

public class SetProperty {

	private IProperties properties;
	private String key;
	private String value;

	public SetProperty(IProperties properties, String key, String value) {
		this.properties = properties;
		this.key = key;
		this.value = value;
	}
	
	public void compute() {
		properties.setProperty(key, value);
	}

}
