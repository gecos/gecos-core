package fr.irisa.cairn.gecos.model.transforms.procedures.outlining;

import static fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory.paramSymbol;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.address;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.indir;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.symbref;
import static java.util.stream.Collectors.groupingBy;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.analysis.dataflow.liveness.LivenessAnalyser;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.transforms.procedures.outlining.ProcedureOutlining.AnnotationsUtils;
import fr.irisa.cairn.gecos.model.transforms.procedures.separation.ProcedureSeparation;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.annotations.LivenessAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.core.ISymbolUse;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.ScopeContainer;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.RetInstruction;
import gecos.instrs.SSADefSymbol;
import gecos.instrs.SimpleArrayInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.AliasType;
import gecos.types.PtrType;
import gecos.types.RecordType;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;

/**
 * Requires SSA Form
 *  
 * @author aelmouss
 */
public class BlockOutliner {
	
	private static final boolean VERBOSE = false;
	
	private Block block;
	private ProcedureSet ps;
	private GecosProject proj;
	private boolean outlineInNewFile;
	private Map<Symbol, List<ISymbolUse>> symbolsUsedInBlock; // < Symbol used in block, its uses in block [] >
	private List<Symbol> externalSymbols; // < external symbol, readOnly? > (external := declared outside block but used in block)

	private List<ParameterSymbol> parameters; // outlined procedure parameters
	private List<Instruction> callArgs; // arguments of the call to the outlined procedure
	
	private List<ISymbolUse> getSymbolUsesInBlock(Symbol s) {
		return symbolsUsedInBlock.get(s);
	}
	
	public BlockOutliner(Block toOutline, GecosProject proj, boolean outlineInNewFile) {
		this.block = toOutline;
		this.ps = block.getContainingProcedureSet();
		this.proj = proj;
		this.outlineInNewFile = outlineInNewFile;
		
		if(block == null || ps == null)
			throw new InvalidParameterException("Block cannot be null and must be contained in a ProcedureSet.");
		if (outlineInNewFile && !(proj instanceof GecosProject))
			throw new InvalidParameterException("GecosProject cannot be null if inlining in new file.");
	}
	

	public Procedure outline() {
		if(VERBOSE) System.out.println("outline: " + block);
		
		GecosUserTypeFactory.setScope(ps.getScope());
		
		analyzeReturns();
		
		/* find set of external input/output symbols used by the block */
		analyzeSymbols();
		
		outlineSymbols();
		
		/* Create the outline procedure symbol */
		String name = generateProcedureName();
		Type returnType = GecosUserTypeFactory.VOID(); //XXX
		ProcedureSymbol procSym = GecosUserCoreFactory.procSymbol(name, returnType, parameters);		
		
		/* Replace outline blk with a new basicblock containing a call to the outline procedure */
		CallInstruction call = GecosUserInstructionFactory.call(procSym, callArgs);
		BasicBlock callBlk = GecosUserBlockFactory.BBlock(call);
		Block parent = block.getParent();
		if(parent == null)
			throw new RuntimeException("Unhandled case: the block to outline has no parent Block");
		parent.replace(block, callBlk);
		
		/* move blk to new procedure */
		CompositeBlock body;
		if(block instanceof CompositeBlock)
			body = (CompositeBlock) block;
		else
			body = GecosUserBlockFactory.CompositeBlock(block);
		
		/* Write procedure in new file or same procedure set */
		Procedure proc = GecosUserCoreFactory.proc(ps, procSym, body);
		
		AnnotationsUtils.removePragma(ProcedureOutlining.BLOCK_OUTLINE_PRAGMA, block);
		
		EList<Type> procScopeTypes = proc.getScope().getTypes();
		for (ParameterSymbol param : parameters) {
			Type paramType = param.getType();
			procScopeTypes.add(paramType);
			if (paramType instanceof PtrType) {
				procScopeTypes.add(((PtrType) paramType).getBase());
			}
		}
		
		if (outlineInNewFile) {
			new ProcedureSeparation(proj, proc).compute();
		}
		
		return proc;
	}

	private void analyzeReturns() {
		EList<RetInstruction> retInsts = EMFUtils.eAllContentsInstancesOf(block, RetInstruction.class);
		if (!retInsts.isEmpty())
			throw new RuntimeException("Return are not supported in outlined block");
	}
	
	private void analyzeSymbols() {
		this.symbolsUsedInBlock = EMFUtils.<ISymbolUse>eAllContentsInstancesOf(block, ISymbolUse.class).stream()
				.filter(Objects::nonNull)
				.filter(u -> Objects.nonNull(u.getUsedSymbol()))
				.collect(groupingBy(ISymbolUse::getUsedSymbol));
		
		externalSymbols = new ArrayList<Symbol>();
		symbolsUsedInBlock.keySet().stream()
				.filter(Objects::nonNull)
				.distinct()
				.filter(this::isDeclaredOutside)
				.filter(s -> !ps.getScope().hasInScope(s)) // Ignore globally declared symbols
				.forEach(s -> externalSymbols.add(s));

		if (VERBOSE) System.out.println("[BlockOutliner][INFO] External Symbols: " +  externalSymbols);
	}

	private void outlineSymbols() {
		this.parameters = new ArrayList<>();
		this.callArgs = new ArrayList<>();

		Block exitBlock = block;
		while (exitBlock instanceof CompositeBlock) {
			exitBlock = ((CompositeBlock) exitBlock).getLastChilld();
		}

		List<Symbol> liveoutSymbols = ((LivenessAnnotation) exitBlock.getAnnotation(LivenessAnalyser.LIVEOUT_ANNOTATION)).getRefs();
		
		for(Symbol s : externalSymbols) {
			// TODO fix struct access in SSA form
			boolean addrNeverAccessed = neverAccessAddressOf(s);
			if (!addrNeverAccessed || isRecordType(s.getType()))
				passSymbolAddress(s);
			else if (s.getType().isArray() || isReadOnly(s) || !liveoutSymbols.contains(s))
				passSymbolDirectly(s);
			else
				passSymbolAddress(s);
		}
	}
	
	static private boolean isRecordType(Type t) {
		if (t instanceof AliasType) {
			return ((AliasType) t).getAlias() instanceof RecordType;
		}
		return t instanceof RecordType;
	}
	
	/**
	 * @param s
	 * @param block
	 * @return true if s is not 
	 */
	private boolean isReadOnly(Symbol s) {
		//FIXME also simple check if s is in any 'update' ??
		
		Optional<SSADefSymbol> def = getDefs(s).findAny();
		return !def.isPresent();
	}

	/**
	 * @param s
	 * @return true if the address of s (&s) is certainly never used in block.
	 */
	private boolean neverAccessAddressOf(Symbol s) {
		return ! getSymbolUsesInBlock(s).stream()
			.filter(Instruction.class::isInstance)
			.map(Instruction.class::cast)
			.anyMatch(i -> i.getParent() instanceof AddressInstruction)
			;
	}

	private void passSymbolDirectly(Symbol s) {
		Type type = s.getType().copy();
		type.setInnermostStorageClass(StorageClassSpecifiers.NONE);
		
		ParameterSymbol paramSym = paramSymbol(s.getName(), type);
		parameters.add(paramSym);
		Instruction callInst = symbref(s);
		callInst.setType(type);
		callArgs.add(callInst);
		
		for (ISymbolUse use : getSymbolUsesInBlock(s)) {
			use.setSymbol(paramSym);
		}
	}
	
	private void passSymbolAddress(Symbol s) {
		Type type = s.getType().copy();
		type.setInnermostStorageClass(StorageClassSpecifiers.NONE);
		
		/* pass a pointer to s (&s) as argument to the outlined procedure */ 
		parameters.add(paramSymbol(s.getName(), GecosUserTypeFactory.PTR(type)));
		Instruction callInst = symbref(s);
		callInst.setType(type);
		callArgs.add(address(callInst));
		
		/* Replace all uses of Symbol s by (*s) in block */
		for(ISymbolUse use : getSymbolUsesInBlock(s)) {
			// Only consider SymbolInstructions i.e. exclude ArrayInstructions since it will actually contain a Symbol 
			// which is what we actually need to replace!
			// e.g.: A[i] is a use(use(A), i) we need to make it use(*(use(A)), i)
			if(use instanceof SymbolInstruction) {
				Instruction useInstruction = (Instruction) use;
				
				// replace use with indir(use)
				IndirInstruction indir = indir(useInstruction.copy());
				ComplexInstruction parent = useInstruction.getParent();
				if(parent instanceof SimpleArrayInstruction && use == ((SimpleArrayInstruction) parent).getDest()) {
					//convert parent to ArrayInstruction: since SimpleArrayInstruction cannot have IndirIntsrution as dest. 
					ArrayInstruction array = GecosUserInstructionFactory.array(indir, ((SimpleArrayInstruction) parent).getIndex());
					parent.substituteWith(array);
				} else {
					useInstruction.substituteWith(indir);
				}
			}
		}
	}

	private String generateProcedureName() {
		String base = "outlined_" + block.getContainingProcedure().getSymbolName() + "_blk" + block.getNumber();
		String name = base;
		int id = 0;
		while(ps.getScope().lookup(name) != null) {
			name = base + "_" + (id++);
		}
		return name;
	}

	private Stream<SSADefSymbol> getDefs(Symbol s) {
		return getSymbolUsesInBlock(s).stream()
			.filter(SSADefSymbol.class::isInstance) // is def ?
			.map(SSADefSymbol.class::cast);
	}
		

	/**
	 * @param s Symbol visible in block's scope 
	 * @return true if s is visible (i.e. declared) outside block
	 */
	private boolean isDeclaredOutside(Symbol s) {
		if(block instanceof ScopeContainer) {
			Scope scope = ((ScopeContainer)block).getScope();
			Scope parent = scope.getParent();
			return (parent != null && parent.hasInScope(s));
		}
		throw new RuntimeException("Unhandled case for block: " + block);
	}
	
}
