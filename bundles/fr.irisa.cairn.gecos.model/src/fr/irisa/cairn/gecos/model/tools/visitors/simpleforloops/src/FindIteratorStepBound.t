/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.tools.visitors.simpleforloops;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.ForBlock;
import gecos.blocks.ForC99Block;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;

@SuppressWarnings({"unchecked", "unused", "rawtypes"})
public class FindIteratorStepBound {

	%include { sl.tom }

	%include { gecos_common.tom }
	%include { gecos_terminals.tom }
	%include { gecos_basic.tom }
	%include { gecos_blocks.tom }
	%include { gecos_compare.tom }
	%include { gecos_control.tom }
	%include { gecos_arithmetic.tom }

	private Symbol loopVariable;
	private Instruction lowerBound;
	private Instruction upperBound;
	private Instruction bound;
	private Instruction stride;
	private Instruction step;

	public FindIteratorStepBound(ForBlock forBlock) {
		findIterator(forBlock);
		findStride(forBlock.getStepBlock());
		FindConditionnalBound findConditionnalBound = new FindConditionnalBound(forBlock, loopVariable);
		if (lowerBound == null)
			lowerBound = findConditionnalBound.getBound();
		else
			upperBound = findConditionnalBound.getBound();
			
	}

	public Symbol getLoopVariable() {
		return loopVariable;
	}

	public Instruction getLowerBound() {
		return lowerBound;
	}

	public Instruction getUpperBound() {
		return upperBound;
	}

	public Instruction getStride() {
		return stride;
	}

	private void findIterator(ForBlock forBlock) {
		if (forBlock instanceof ForC99Block) {
			if (forBlock.getScope().getSymbols().size() == 1) {
				loopVariable = forBlock.getScope().getSymbols().get(0);
				bound = forBlock.getScope().getSymbols().get(0).getValue();
				return;
			}
		} else {
			%match(forBlock.getInitBlock()) {
				BB(InstL(set(symref(nameDest), bound))) -> {
					loopVariable = `nameDest;
					bound = `bound;
					return;
				}
			}
		}
		throw new SimpleLoopException(forBlock, "No loop variable found !");
	}

	private void findStride(Block block) {
		%match(block) {
			BB(InstL(set(symref(symbol), add(InstL(symref(symbol), stride))))) -> {
				if (loopVariable.equals(`symbol)) {
					stride = `stride;
					lowerBound = bound;
					return;
				}
			}
			BB(InstL(set(symref(symbol), add(InstL(stride, symref(symbol)))))) -> {
				if (loopVariable.equals(`symbol)) {
					stride = `stride;
					lowerBound = bound;
					return;
				}
			}
			BB(InstL(set(symref(symbol), substraction@sub(InstL(symref(symbol), val@ival(_)))))) -> {
				if (loopVariable.equals(`symbol)) {
					IntInstruction intInst = (IntInstruction)`val;
					intInst.setValue(intInst.getValue() * -1);
					stride = (IntInstruction)`val;
					((GenericInstruction)(`substraction)).setName("add");
					upperBound = bound;
					return;
				}
			}
			BB(InstL(set(symref(symbol), substraction@sub(InstL(symref(symbol), step))))) -> {
				if (loopVariable.equals(`symbol)) {
					Instruction oldStride = `step;
					stride = `mul(InstL(ival(-1), oldStride));
					((GenericInstruction)(`substraction)).getChildren().remove(oldStride);
					((GenericInstruction)(`substraction)).getChildren().add(stride);
					((GenericInstruction)(`substraction)).setName("add");
					upperBound = bound;
					return;
				}
			}
		}
		throw new SimpleLoopException(block,"No loop index:"+ block +" is not a simple loop");
	}
}