/**
 */
package gecos.instrs.impl;

import gecos.instrs.InstrsPackage;
import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;
import gecos.instrs.SizeofTypeInstruction;

import gecos.types.Type;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sizeof Type Instruction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.impl.SizeofTypeInstructionImpl#getTargetType <em>Target Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SizeofTypeInstructionImpl extends SizeofInstructionImpl implements SizeofTypeInstruction {
	/**
	 * The cached value of the '{@link #getTargetType() <em>Target Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetType()
	 * @generated
	 * @ordered
	 */
	protected Type targetType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SizeofTypeInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstrsPackage.eINSTANCE.getSizeofTypeInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type getTargetType() {
		if (targetType != null && targetType.eIsProxy()) {
			InternalEObject oldTargetType = (InternalEObject)targetType;
			targetType = (Type)eResolveProxy(oldTargetType);
			if (targetType != oldTargetType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InstrsPackage.SIZEOF_TYPE_INSTRUCTION__TARGET_TYPE, oldTargetType, targetType));
			}
		}
		return targetType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type basicGetTargetType() {
		return targetType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetType(Type newTargetType) {
		Type oldTargetType = targetType;
		targetType = newTargetType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InstrsPackage.SIZEOF_TYPE_INSTRUCTION__TARGET_TYPE, oldTargetType, targetType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final InstrsVisitor visitor) {
		visitor.visitSizeofTypeInstruction(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		Type _targetType = this.getTargetType();
		String _plus = ("sizeof(" + _targetType);
		return (_plus + ")");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSame(final Instruction instr) {
		if ((instr instanceof SizeofTypeInstruction)) {
			boolean isSame = false;
			if (((this.getTargetType() == null) && (((SizeofTypeInstruction)instr).getTargetType() == null))) {
				isSame = true;
			}
			else {
				Type _targetType = this.getTargetType();
				boolean _tripleEquals = (_targetType == null);
				Type _targetType_1 = ((SizeofTypeInstruction)instr).getTargetType();
				boolean _tripleEquals_1 = (_targetType_1 == null);
				boolean _xor = (_tripleEquals ^ _tripleEquals_1);
				if (_xor) {
					return false;
				}
				else {
					isSame = this.getTargetType().isEqual(((SizeofTypeInstruction)instr).getTargetType());
				}
			}
			return (super.isSame(instr) && isSame);
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InstrsPackage.SIZEOF_TYPE_INSTRUCTION__TARGET_TYPE:
				if (resolve) return getTargetType();
				return basicGetTargetType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InstrsPackage.SIZEOF_TYPE_INSTRUCTION__TARGET_TYPE:
				setTargetType((Type)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InstrsPackage.SIZEOF_TYPE_INSTRUCTION__TARGET_TYPE:
				setTargetType((Type)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InstrsPackage.SIZEOF_TYPE_INSTRUCTION__TARGET_TYPE:
				return targetType != null;
		}
		return super.eIsSet(featureID);
	}

} //SizeofTypeInstructionImpl
