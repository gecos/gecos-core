/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.tools.switches;

import gecos.blocks.BasicBlock;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.util.InstrsSwitch;

import java.util.Iterator;

/**
 * 
 *This is a switch based implementation(cf. switches package)
 *
 */
public abstract class SimpleBlockInstructionSwitch extends InstrsSwitch<Object> {

	public SimpleBlockInstructionSwitch() {
	}

	public void caseBasicBlock(BasicBlock b) {
		Iterator<Instruction> i = b.getInstructions().iterator();
		while (i.hasNext()) {
			Instruction inst = (Instruction) i.next();
			doSwitch(inst);
		}
	}


	@Override
	public Object caseComplexInstruction(ComplexInstruction cplx) {
		for (Instruction insn : cplx.listChildren()) {
			doSwitch(insn);
		}
		return super.doSwitch(cplx);
	}
	
	
}
