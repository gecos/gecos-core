package fr.irisa.cairn.gecos.model.extensions.generators;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;

import fr.irisa.cairn.gecos.model.extensions.GecosRegistry;

/**
 * Register all the generators pointed by the <i>gecos.generators</i> extension
 * point.
 * 
 * @author Antoine Floc'h - Initial contribution and API
 * 
 */
final class GecosGeneratorsRegistry extends GecosRegistry {
	public final static GecosGeneratorsRegistry INSTANCE = new GecosGeneratorsRegistry();

	private final static String EXTENSION_POINT = "fr.irisa.cairn.gecos.model.GecosGenerators";
	private final static String EXTENSION_ATTRIBUTE = "extension";
	private final static String EXTENDED_ATTRIBUTE = "extended";

	private GeneratorExtensionFactory extensionFactory;

	private GecosGeneratorsRegistry() {
		this.extensionFactory = new GeneratorExtensionFactory();
	}

	private List<Class<? extends IGecosCodeGenerator>> getAllGeneratorExtensionsClassesFor(
			IGecosCodeGenerator extended) {
		List<Class<? extends IGecosCodeGenerator>> extensions = new ArrayList<Class<? extends IGecosCodeGenerator>>();
		for (IExtension e : getExtensions(EXTENSION_POINT)) {
			for (IConfigurationElement iConfigurationElement : e
					.getConfigurationElements()) {
				String extensionClassID = iConfigurationElement
						.getAttribute(EXTENSION_ATTRIBUTE);
				String extendedClassID = iConfigurationElement
						.getAttribute(EXTENDED_ATTRIBUTE);

				Class<IGecosCodeGenerator> extendedType = getClass(
						extendedClassID, iConfigurationElement);
				if (extended.getClass() == extendedType) {
					Class<IGecosCodeGenerator> extensionClass = getClass(
							extensionClassID, iConfigurationElement);
					extensions.add(extensionClass);
				}
			}
		}
		return extensions;
	}

	public List<IGecosCodeGenerator> getAllGeneratorExtensionsFor(
			IGecosCodeGenerator extended) {
		List<IGecosCodeGenerator> extensions = new ArrayList<IGecosCodeGenerator>();
		List<Class<? extends IGecosCodeGenerator>> allGeneratorExtensionsClassesFor = getAllGeneratorExtensionsClassesFor(extended);
		for (Class<? extends IGecosCodeGenerator> generatorClass : allGeneratorExtensionsClassesFor) {
			extensions.add(extensionFactory.buildGenerator(generatorClass));
		}
		return extensions;

	}
}