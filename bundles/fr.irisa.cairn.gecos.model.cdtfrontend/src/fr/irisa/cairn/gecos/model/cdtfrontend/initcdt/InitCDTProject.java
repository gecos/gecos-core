package fr.irisa.cairn.gecos.model.cdtfrontend.initcdt;

import fr.irisa.cairn.gecos.model.cdtfrontend.CDTFrontEnd;
import fr.irisa.cairn.gecos.model.includes.std.GecosStandardIncludes;
import fr.irisa.cairn.gecos.model.modules.AddIncludeDirToGecosProject;
import gecos.gecosproject.GecosProject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.eclipse.cdt.core.CCProjectNature;
import org.eclipse.cdt.core.CCorePlugin;
import org.eclipse.cdt.core.CProjectNature;
import org.eclipse.cdt.core.dom.IPDOMManager;
import org.eclipse.cdt.core.model.CoreModel;
import org.eclipse.cdt.core.model.ICProject;
import org.eclipse.cdt.core.settings.model.ICConfigExtensionReference;
import org.eclipse.cdt.core.settings.model.ICConfigurationDescription;
import org.eclipse.cdt.core.settings.model.ICProjectDescription;
import org.eclipse.cdt.core.settings.model.ICProjectDescriptionManager;
import org.eclipse.cdt.core.settings.model.util.CDataUtil;
import org.eclipse.cdt.internal.core.pdom.indexer.IndexerPreferences;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;

public class InitCDTProject {
	private final static String tmpPrefix = "_CDT_tmp_";
	
	/**
	 * Create a CDT project and initializes name, macros, and includes out of a
	 * GeCoS project.
	 * 
	 * @return
	 * @throws CoreException
	 */
	public static ICProject createCDTCProject(GecosProject gecosProject) throws CoreException {
		String projectName =  tmpPrefix + gecosProject.getName();		
		
		try {
			Path workspace = ResourcesPlugin.getWorkspace().getRoot().getLocation().toFile().toPath();
			Path tempPath = Files.createTempDirectory(workspace, tmpPrefix);
			projectName = workspace.relativize(tempPath).toString();
		} catch (IOException e1) {
			System.err.println("Warning: " + e1.getMessage());
		}
		
		ICProject cProject = InitCDTProject.process(projectName);

		if (CDTFrontEnd.useGecosStandardIncludes())
			new AddIncludeDirToGecosProject(gecosProject, true, GecosStandardIncludes.getDefault().getIncludePaths()).compute();
		
		GecosCDTSettingsProvider.setup(cProject, gecosProject);
		
		return cProject;
	}
	
	
	/**
	 * Creates an Eclipse CDT project, register it as a C project (add C
	 * nature), and initialize its name and ID.
	 * 
	 * Copy/Paste + in-line from
	 * {@link org.eclipse.cdt.core.testplugin.CProjectHelper}
	 * 
	 * @param projectName
	 * @param projectId
	 * @return
	 * @throws CoreException
	 */
	public static ICProject process(final String projectName) throws CoreException {
		
		final String indexerID = IPDOMManager.ID_NO_INDEXER;
		final String finalCfgProviderId = GecosCDTDataProvider.PROVIDER_ID;
		final IWorkspace ws = ResourcesPlugin.getWorkspace();
		final ICProject newProject[] = new ICProject[1];
		
		ws.run(new IWorkspaceRunnable() {
			@Override
			public void run(IProgressMonitor monitor) throws CoreException {
				IWorkspaceRoot root = ws.getRoot();
				IProject project = root.getProject(projectName);
				//LanguageManager languageManager = LanguageManager.getInstance();
				if (indexerID != null)
					IndexerPreferences.set(project, IndexerPreferences.KEY_INDEXER_ID, indexerID);
				
				if (!project.exists())
					project.create(monitor);
				else
					project.refreshLocal(IResource.DEPTH_INFINITE, null);
				
				if (!project.isOpen())
					project.open(monitor);
				
				if (!CoreModel.hasCNature(project)) {
					//copy current natures and add C nature
					IProjectDescription description = project.getDescription();
					String[] prevNatures = description.getNatureIds();
					String[] newNatures = new String[prevNatures.length + 2];
					System.arraycopy(prevNatures, 0, newNatures, 0, prevNatures.length);
					newNatures[prevNatures.length] = CProjectNature.C_NATURE_ID;
					newNatures[prevNatures.length+1] = CCProjectNature.CC_NATURE_ID;
					
					description.setNatureIds(newNatures);
					project.setDescription(description, monitor);
					
					//initialize C project description
					ICConfigurationDescription prefCfg = CCorePlugin.getDefault().getPreferenceConfiguration(finalCfgProviderId);
					ICProjectDescriptionManager mngr = CCorePlugin.getDefault().getProjectDescriptionManager();
					ICProjectDescription projDes = mngr.createProjectDescription(project, false, false);
					String genId = CDataUtil.genId("test");
					String genId2 = CDataUtil.genId(null);
					projDes.createConfiguration(genId2, genId, prefCfg);
					mngr.setProjectDescription(project, projDes);
					/*
					//C files are considerd as C++ files
					ProjectLanguageConfiguration languageConfiguration = languageManager.getLanguageConfiguration(project);
					System.out.println("1 : "+languageConfiguration);
					System.out.println(languageConfiguration.getContentTypeMappings());
					
					CConfigurationData cfgData = projDes.getActiveConfiguration().getConfigurationData();
					CFolderData rootFolderData = cfgData.getRootFolderData();
					CLanguageData[] languageDatas = rootFolderData.getLanguageDatas();

					String CxxLanguageId = "org.eclipse.cdt.core.g++";
					String CLanguageId =   "org.eclipse.cdt.core.gcc";
					
					String[] languageSourceContentIds = new String[0];
					String[] languageExtensionIds = new String[0];
					for (CLanguageData d : languageDatas) {
						String languageId = d.getLanguageId();
						if (languageId.equals(CLanguageId)) {
							languageSourceContentIds = d.getSourceContentTypeIds();
							languageExtensionIds = d.getSourceExtensions();
							d.setSourceContentTypeIds(new String[0]);
							d.setSourceExtensions(new String[0]);
							break;
						}
					}
					for (CLanguageData d : languageDatas) {
						String languageId = d.getLanguageId();
						if (languageId.equals(CxxLanguageId)) {
							String[] prevContentIds = d.getSourceContentTypeIds();
							String[] prevExtensionIds = d.getSourceExtensions();
							
							String[] newContentIds = new String[languageSourceContentIds.length+prevContentIds.length];
							System.arraycopy(prevContentIds, 0, newContentIds, 0, prevContentIds.length);
							System.arraycopy(languageSourceContentIds, 0, newContentIds, prevContentIds.length, languageSourceContentIds.length);

							String[] newExtensionIds = new String[languageExtensionIds.length+prevExtensionIds.length];
							System.arraycopy(prevExtensionIds, 0, newExtensionIds, 0, prevExtensionIds.length);
							System.arraycopy(languageExtensionIds, 0, newExtensionIds, prevExtensionIds.length, languageExtensionIds.length);
							
							d.setSourceContentTypeIds(newContentIds);
							d.setSourceExtensions(newExtensionIds);
							break;
						}
					}
					

//					IContentType[] affectedContentTypes = new IContentType[languageSourceContentIds.length];
//					int i = 0;
//					for (String c : languageSourceContentIds) {
//						IContentType contentType = ContentTypeManager.getInstance().getContentType(c);
//						languageConfiguration.addContentTypeMapping(prefCfg, c, CxxLanguageId);
//						affectedContentTypes[i++] = contentType;
//					}
//					
//					languageManager.storeLanguageMappingConfiguration(project, affectedContentTypes);
//					
					*/

				}

				/*
				ProjectLanguageConfiguration languageConfiguration = languageManager.getLanguageConfiguration(project);
				System.out.println("2 : "+languageConfiguration);
				System.out.println(languageConfiguration.getContentTypeMappings());
				*/
				
				
				ICConfigExtensionReference[] binaryParsers= CCorePlugin.getDefault().getDefaultBinaryParserExtensions(project);
				if (binaryParsers == null || binaryParsers.length == 0) {
					ICProjectDescription desc= CCorePlugin.getDefault().getProjectDescription(project);
					if (desc != null) {
						desc.getDefaultSettingConfiguration().create(CCorePlugin.BINARY_PARSER_UNIQ_ID, CCorePlugin.DEFAULT_BINARY_PARSER_UNIQ_ID);
						CCorePlugin.getDefault().setProjectDescription(project, desc);
					}
				}
				newProject[0] = CCorePlugin.getDefault().getCoreModel().create(project);
			}
		}, null);
		return newProject[0];
	}
}
