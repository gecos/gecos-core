package templates.xtend;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import gecos.annotations.CommentAnnotation;
import gecos.annotations.PragmaAnnotation;
import gecos.blocks.CompositeBlock;
import java.util.Arrays;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import templates.xtend.GecosTemplate;

@SuppressWarnings("all")
public class GecosAnnotationTemplate extends GecosTemplate {
  protected Object _generate(final PragmaAnnotation pragmaAnnotation) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      if (((pragmaAnnotation != null) && (pragmaAnnotation.getContent() != null))) {
        EObject _eContainer = pragmaAnnotation.eContainer().eContainer().eContainer().eContainer();
        if ((_eContainer instanceof CompositeBlock)) {
          EObject _eContainer_1 = pragmaAnnotation.eContainer().eContainer().eContainer().eContainer();
          int position = ((CompositeBlock) _eContainer_1).getChildren().indexOf(pragmaAnnotation.eContainer().eContainer().eContainer());
          if (((position > 0) && (((CompositeBlock) pragmaAnnotation.eContainer().eContainer().eContainer().eContainer()).getChildren().get((position - 1)) instanceof CompositeBlock))) {
            StringConcatenation _builder = new StringConcatenation();
            _builder.append("\t\t\t\t\t");
            _builder.newLine();
            buffer.append(_builder);
          }
        }
        EList<String> _content = pragmaAnnotation.getContent();
        for (final String string : _content) {
          boolean _startsWith = string.startsWith(GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION);
          if (_startsWith) {
            StringConcatenation _builder_1 = new StringConcatenation();
            String _substring = string.substring(GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION.length());
            _builder_1.append(_substring);
            _builder_1.newLineIfNotEmpty();
            buffer.append(_builder_1);
          } else {
            boolean _startsWith_1 = string.startsWith(GecosUserAnnotationFactory.CODEGEN_COPYFILE_ANNOTATION);
            if (_startsWith_1) {
            } else {
              StringConcatenation _builder_2 = new StringConcatenation();
              _builder_2.newLine();
              _builder_2.append("#pragma ");
              _builder_2.append(string);
              _builder_2.newLineIfNotEmpty();
              buffer.append(_builder_2);
            }
          }
        }
      }
      StringConcatenation _builder_3 = new StringConcatenation();
      _builder_3.append(buffer);
      _xblockexpression = _builder_3;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final CommentAnnotation comment) {
    String _content = comment.getContent();
    String _plus = ("/* " + _content);
    return (_plus + " */");
  }
  
  protected Object _generate(final EObject object) {
    return null;
  }
  
  public Object generate(final EObject comment) {
    if (comment instanceof CommentAnnotation) {
      return _generate((CommentAnnotation)comment);
    } else if (comment instanceof PragmaAnnotation) {
      return _generate((PragmaAnnotation)comment);
    } else if (comment != null) {
      return _generate(comment);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(comment).toString());
    }
  }
}
