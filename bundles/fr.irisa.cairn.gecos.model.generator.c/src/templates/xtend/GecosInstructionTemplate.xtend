package templates.xtend

import fr.irisa.cairn.gecos.model.c.generator.CharBuilder
import fr.irisa.cairn.gecos.model.c.generator.ExtendableInstructionCGenerator
import fr.irisa.cairn.gecos.model.c.generator.ExtendableTypeCGenerator
import fr.irisa.cairn.gecos.model.c.generator.OperatorPrecedenceTools
import gecos.annotations.PragmaAnnotation
import gecos.instrs.AddressInstruction
import gecos.instrs.ArrayInstruction
import gecos.instrs.ArrayValueInstruction
import gecos.instrs.BreakInstruction
import gecos.instrs.CallInstruction
import gecos.instrs.CondInstruction
import gecos.instrs.ConstantInstruction
import gecos.instrs.ContinueInstruction
import gecos.instrs.ConvertInstruction
import gecos.instrs.DummyInstruction
import gecos.instrs.EnumeratorInstruction
import gecos.instrs.FieldInstruction
import gecos.instrs.FloatInstruction
import gecos.instrs.GenericInstruction
import gecos.instrs.GotoInstruction
import gecos.instrs.IndirInstruction
import gecos.instrs.InstrsPackage
import gecos.instrs.Instruction
import gecos.instrs.IntInstruction
import gecos.instrs.LabelInstruction
import gecos.instrs.MethodCallInstruction
import gecos.instrs.RetInstruction
import gecos.instrs.SetInstruction
import gecos.instrs.SimpleArrayInstruction
import gecos.instrs.SizeofTypeInstruction
import gecos.instrs.SizeofValueInstruction
import gecos.instrs.StringInstruction
import gecos.instrs.SymbolInstruction
import gecos.types.IntegerType
import gecos.types.IntegerTypes
import java.util.List
import org.eclipse.emf.ecore.EObject
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory

class GecosInstructionTemplate extends GecosTemplate {

	def dispatch generate(AddressInstruction addressInstruction) {
		'''&«ExtendableInstructionCGenerator::eInstance.generate(addressInstruction.address)»'''
	}

	def dispatch generate(ArrayInstruction arrayInstruction) {
		val buffer = new StringBuffer()
		buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(arrayInstruction.dest)»''')
		for (Instruction instruction:arrayInstruction.index) {
			buffer.append('''[«ExtendableInstructionCGenerator::eInstance.generate(instruction)»]''')
		}
		'''«buffer»'''
	}

	def dispatch generate(ArrayValueInstruction arrayValueInstruction) {
		val buffer = new StringBuffer()
		buffer.append('''{''')
		for (Instruction instruction:arrayValueInstruction.children) {
			buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(instruction)»''')
			if (arrayValueInstruction.children.indexOf(instruction) < arrayValueInstruction.children.size - 1)
				buffer.append(", ")
		}
		buffer.append('''}''')
	}
	
	def dispatch generate(BreakInstruction breakInstruction) {
		'''break'''
	}

	def dispatch generate(MethodCallInstruction methodInstruction) {
		val buffer = new StringBuffer()
		buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(methodInstruction.address)»''')
		buffer.append("." + methodInstruction.name + "(")		
	
		for(Instruction instruction : methodInstruction.args){
			buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(instruction)»''')
			if (methodInstruction.getArgs().indexOf(instruction) < methodInstruction.args.size - 1)
				buffer.append(", ")
		}
		buffer.append(")")
		'''«buffer»'''
	}

	def dispatch generate(CallInstruction callInstruction) {
		val buffer = new StringBuffer()
		buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(callInstruction.address)»''')
		buffer.append("(")
//		for (Instruction instruction : callInstruction.args) {
//			buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(instruction)»''')
//			if (callInstruction.children.indexOf(instruction) < callInstruction.children.size - 1)
//				buffer.append(", ")
//		}
		buffer.append(callInstruction.args.join(", ", [ExtendableInstructionCGenerator::eInstance.generate(it)]))
		buffer.append(")")
		'''«buffer»'''
	}

	def dispatch generate(CondInstruction condInstruction) {
		'''«ExtendableInstructionCGenerator::eInstance.generate(condInstruction.cond)»'''
	}
	
	def dispatch generate(ContinueInstruction continueInstruction) {
		'''continue'''
	}

	def dispatch generate(ConvertInstruction convertInstruction) {
		val buffer = new StringBuffer()
		val needParenthesis = OperatorPrecedenceTools::needParenthesis(convertInstruction)
		var type = convertInstruction.type
//		while (type instanceof StaticType || type instanceof ConstType) {
//			if (type instanceof StaticType) type = (type as StaticType).base
//			if (type instanceof ConstType) type = (type as ConstType).base
//		}
		if (needParenthesis)
			buffer.append('''(''')
		
		afterSymbolInformation.setLength(0)
		buffer.append('''(«ExtendableTypeCGenerator::eInstance.generate(type)»«afterSymbolInformation»)''')
		afterSymbolInformation.setLength(0)
		buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(convertInstruction.expr)»''')
		
		if (needParenthesis)
			buffer.append(''')''')
		'''«buffer.toString»'''
	}

	def dispatch generate(DummyInstruction dummyInstruction) {
		''''''
	}

	def dispatch generate(EnumeratorInstruction enumeratorInstruction) {
		val gecosTypeTemplate = new GecosTypeTemplate()
		'''«gecosTypeTemplate.generate(enumeratorInstruction.enumerator)»'''
	}

	def dispatch generate(FieldInstruction fieldInstruction) {
		val gecosTypeTemplate = new GecosTypeTemplate()	
		val buffer = new StringBuffer()
		if (fieldInstruction.childrenCount == 1)
			if (fieldInstruction.expr instanceof IndirInstruction) {
				buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate((fieldInstruction.expr as IndirInstruction).address)»''')
				buffer.append('''->«gecosTypeTemplate.generate(fieldInstruction.field)»''')
			} else {
				buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(fieldInstruction.expr)»''')
				buffer.append('''.«gecosTypeTemplate.generate(fieldInstruction.field)»''')
			}
		else
			buffer.append('''/* Not manageable !!!*/''')
		'''«buffer.toString»'''
	}

	def dispatch generate(FloatInstruction floatInstruction) {
		var String res = ""
		if (floatInstruction.value.infinite) {
			if (floatInstruction.value > 0) {
				res = "(1.0/0.0) /*+INFINITE*/"
			} else {
				res = "-(1.0/0.0) /*-INFINITE*/"
			}
		} else {
			if (floatInstruction.value.naN) {
				res = "(0.0/0.0) /*NAN*/"
			} else {
				res = floatInstruction.value.toString()
			}
		}
		return '''«res»'''
	}

	def dispatch generate(GenericInstruction genericInstruction) {
		if (!(genericInstruction.eClass.EPackage == InstrsPackage.eINSTANCE)) {
			/*
			 * Since the default case for the instruction name doesn't return null, 
			 * we add a filter on the package: this code generator only handles
			 * instructions built from the original Gecos plug-in.
			 */
			return null
		}
		val buffer = new StringBuffer()
		val needParenthesis = OperatorPrecedenceTools::needParenthesis(genericInstruction)
		if (needParenthesis)
			buffer.append('''(''')
			
		switch (genericInstruction.name) {
			case "add" : buffer.append('''«generateInstructionWithMultipleSubInstruction(genericInstruction.operands, "+")»''')
			case "sub" : buffer.append('''«generateInstructionWithMultipleSubInstruction(genericInstruction.operands, "-")»''')
			case "mul" : buffer.append('''«generateInstructionWithMultipleSubInstruction(genericInstruction.operands, "*")»''')
			case "div" : buffer.append('''«generateInstructionWithMultipleSubInstruction(genericInstruction.operands, "/")»''')
			case "shl" : buffer.append('''«generateInstructionWithMultipleSubInstruction(genericInstruction.operands, "<<")»''')
			case "shr" : buffer.append('''«generateInstructionWithMultipleSubInstruction(genericInstruction.operands, ">>")»''')
			case "neg" : buffer.append('''-«ExtendableInstructionCGenerator::eInstance.generate(genericInstruction.operands.get(0))»''')
			case "not" : buffer.append('''~«ExtendableInstructionCGenerator::eInstance.generate(genericInstruction.operands.get(0))»''')
			case "lnot" : buffer.append('''!«ExtendableInstructionCGenerator::eInstance.generate(genericInstruction.operands.get(0))»''')
			case "mod" : buffer.append('''«generateInstructionWithMultipleSubInstruction(genericInstruction.operands, "%")»''')
			case "eq" : buffer.append('''«generateInstructionWithMultipleSubInstruction(genericInstruction.operands, "==")»''')
			case "ne" : buffer.append('''«generateInstructionWithMultipleSubInstruction(genericInstruction.operands, "!=")»''')
			case "neq" : buffer.append('''«generateInstructionWithMultipleSubInstruction(genericInstruction.operands, "!=")»''')
			case "lt" : buffer.append('''«generateInstructionWithMultipleSubInstruction(genericInstruction.operands, "<")»''')
			case "le" : buffer.append('''«generateInstructionWithMultipleSubInstruction(genericInstruction.operands, "<=")»''')
			case "gt" : buffer.append('''«generateInstructionWithMultipleSubInstruction(genericInstruction.operands, ">")»''')
			case "ge" : buffer.append('''«generateInstructionWithMultipleSubInstruction(genericInstruction.operands, ">=")»''')
			case "land" :{
				for (Instruction instruction : genericInstruction.operands) {
					buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(instruction)»''')
					if (instruction !== (genericInstruction.operands.get(genericInstruction.operands.size - 1)))
						buffer.append(" && ")
				}
			}
			case "and" :{
				for (Instruction instruction : genericInstruction.operands) {
					buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(instruction)»''')
					if (instruction !== (genericInstruction.operands.get(genericInstruction.operands.size - 1)))
						buffer.append(" & ")
				}
			}
			case "lor" :{
				for (Instruction instruction : genericInstruction.operands) {
					buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(instruction)»''')
					if (instruction !== (genericInstruction.operands.get(genericInstruction.operands.size - 1)))
						buffer.append(" || ")
				}
			}
			case "or" :{
				for (Instruction instruction : genericInstruction.operands) {
					buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(instruction)»''')
					if (instruction !== (genericInstruction.operands.get(genericInstruction.operands.size - 1)))
						buffer.append(" | ")
				}
			}
			case "xor" :{
				for (Instruction instruction : genericInstruction.operands) {
					buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(instruction)»''')
					if (instruction !== (genericInstruction.operands.get(genericInstruction.operands.size - 1)))
						buffer.append(" ^ ")
				}
			}
			case "mux" :{
				if (genericInstruction.operands.size == 3) {
					buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(genericInstruction.operands.get(0))»''')
					buffer.append(" ? ")
					buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(genericInstruction.operands.get(1))»''')
					buffer.append(" : ")
					buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(genericInstruction.operands.get(2))»''')
				}
			}
			case "sequence" :{
				for (Instruction instruction : genericInstruction.operands) {
					buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(instruction)»''')
					if (instruction !== (genericInstruction.operands.get(genericInstruction.operands.size - 1)))
						buffer.append(", ")
				}
			}
			case "min" :{
				val cps = genericInstruction.containingProcedureSet
				//in order to generate min macro
				cps.setAnnotation("need_define_min", null);
				
				for(i : 0..genericInstruction.operands.size()-2)
					buffer.append("_min_(")
					
				var tmp=false
				for (Instruction instruction : genericInstruction.operands) {
					buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(instruction)»''')
					if(!tmp) 
						tmp = true
					else
						buffer.append(")")
						
					if (instruction !== (genericInstruction.operands.get(genericInstruction.operands.size - 1)))
						buffer.append(", ")
				}
			}
			case "max" :{
				val cps = genericInstruction.containingProcedureSet
				//in order to generate max macro
				cps.setAnnotation("need_define_max", null);
		
				for(i : 0..genericInstruction.operands.size()-2)
					buffer.append("_max_(")
					
				var tmp=false
				for (Instruction instruction : genericInstruction.operands) {
					buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(instruction)»''')
					if(!tmp) 
						tmp = true
					else
						buffer.append(")")
						
					if (instruction !== (genericInstruction.operands.get(genericInstruction.operands.size - 1)))
						buffer.append(", ")
				}
			}
			case "floor" : {
				val cps = genericInstruction.containingProcedureSet
				//in order to generate floor macro
				cps.setAnnotation("need_define_floor", null);
				buffer.append('''_floor_(«ExtendableInstructionCGenerator::eInstance.generate(genericInstruction.operands.get(0))», «ExtendableInstructionCGenerator::eInstance.generate(genericInstruction.operands.get(1))»)''')
			}
			case "ceil" : {
				val cps = genericInstruction.containingProcedureSet
				//in order to generate ceil macro
				cps.setAnnotation("need_define_ceil", null);
				buffer.append('''_ceil_(«ExtendableInstructionCGenerator::eInstance.generate(genericInstruction.operands.get(0))», «ExtendableInstructionCGenerator::eInstance.generate(genericInstruction.operands.get(1))»)''')
			}
			case "mmod" : {
				val cps = genericInstruction.containingProcedureSet
				//in order to generate mod macro
				cps.setAnnotation("need_define_mod", null);
				buffer.append('''_mmod_(«ExtendableInstructionCGenerator::eInstance.generate(genericInstruction.operands.get(0))», «ExtendableInstructionCGenerator::eInstance.generate(genericInstruction.operands.get(1))»)''')
			}
			default : 
					buffer.append('''/* GenericInstruction : "«genericInstruction»" not supported */''')
		}
		if (needParenthesis)
			buffer.append(''')''')
		'''«buffer.toString»'''
	}
	
	def generateInstructionWithMultipleSubInstruction(List<Instruction> subInstructions, String operator) {
		val buffer = new StringBuffer()
		var first = true
		for (instruction : subInstructions) {
			if (first) first = false
			else {
				if (operator.equals("*") && (instruction instanceof ConstantInstruction || instruction instanceof SymbolInstruction))
					buffer.append('''«operator»''')
				else
					buffer.append(''' «operator» ''')
			}
			buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(instruction)»''')
		}
		'''«buffer»'''
	}

	def dispatch generate(GotoInstruction gotoInstruction) {
		'''goto «gotoInstruction.labelName»'''
	}

	def dispatch generate(IndirInstruction indirInstruction) {
		val buffer = new StringBuffer()
		val needParenthesis = OperatorPrecedenceTools::needParenthesis(indirInstruction)
		if (needParenthesis)
			buffer.append('''(''')
		buffer.append('''*«ExtendableInstructionCGenerator::eInstance.generate(indirInstruction.address)»''')
		if (needParenthesis)
			buffer.append(''')''')
		'''«buffer»'''
	}

	def dispatch generate(IntInstruction intInstruction) {
		var PragmaAnnotation pragmaAnnotation = intInstruction.getPragma();
		var String unsigned = ""
		var String longlong = ""
		if (intInstruction.type instanceof IntegerType) {
			val t = (intInstruction.type as IntegerType)
			if(!t.getSigned()) {
//				if (intInstruction.value < 0 || intInstruction.value > Integer.MAX_VALUE) //XXX
					unsigned = "u"
			}
			if(t.type.equals(IntegerTypes.LONG))
				longlong = "l"
			else if(t.type.equals(IntegerTypes.LONG_LONG))
				longlong = "ll"
		}
		
		if (pragmaAnnotation !== null) {
			for (String directive : pragmaAnnotation.getContent()) {
				if (directive.contains(GecosUserAnnotationFactory.PRAGMA_CHAR_LITTERAL)) {
					return ("\'"+CharBuilder.buildCharLitteralFromIntInstruction(intInstruction.value)+ "\'");
				} else {
					if (directive.contains(GecosUserAnnotationFactory.PRAGMA_HEX_LITTERAL)) {
						return ("0x" + Long.toHexString(intInstruction.getValue()))+unsigned+longlong;
					} else {
						if (directive.contains(GecosUserAnnotationFactory.PRAGMA_OCT_LITTERAL)) {
							return ("0" + Long.toOctalString(intInstruction.getValue()))+unsigned+longlong;
						} else {
							if (directive.contains(GecosUserAnnotationFactory.PRAGMA_BIN_LITTERAL)) {
								return ("0b" + Long.toBinaryString(intInstruction.getValue()))+unsigned+longlong;
							} else {
								//continue iterating over directives
							}
						}
					}
				}
			}
		}
		'''«Long.toString(intInstruction.getValue())+unsigned+longlong»'''
	}

	def dispatch generate(LabelInstruction labelInstruction) {
		'''«labelInstruction.name»:'''
	}

	def dispatch generate(RetInstruction retInstruction) {
		'''return «ExtendableInstructionCGenerator::eInstance.generate(retInstruction.expr)»'''
	}

	def dispatch generate(SetInstruction setInstruction) {
		val buffer = new StringBuffer()
		val needParenthesis = OperatorPrecedenceTools::needParenthesis(setInstruction)
		if (needParenthesis)
			buffer.append('''(''')
		buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(setInstruction.dest)» = «ExtendableInstructionCGenerator::eInstance.generate(setInstruction.source)»''')
		if (needParenthesis)
			buffer.append(''')''')
		'''«buffer»'''
	}

	def dispatch generate(SimpleArrayInstruction simpleArrayInstruction) {
		val buffer = new StringBuffer()
		buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(simpleArrayInstruction.dest)»''')
		for(Instruction instruction : simpleArrayInstruction.index) {
			buffer.append('''[«ExtendableInstructionCGenerator::eInstance.generate(instruction)»]''')
		}
		'''«buffer»'''
	}

	def dispatch generate(SizeofTypeInstruction sizeofTypeInstruction) {
		val buffer = new StringBuffer()
		val gecosTypeTemplate = new ExtendableTypeCGenerator()
		afterSymbolInformation.setLength(0)
		buffer.append('''sizeof («gecosTypeTemplate.generate(sizeofTypeInstruction.targetType)»«afterSymbolInformation.toString»)''')
		afterSymbolInformation.setLength(0);
		'''«buffer»'''
	}

	def dispatch generate(SizeofValueInstruction sizeofValueInstruction) {
		'''sizeof «ExtendableInstructionCGenerator::eInstance.generate(sizeofValueInstruction.expr)»'''
	}

	def dispatch generate(StringInstruction stringInstruction) {
		'''"«stringInstruction.value»"'''
	}

	def dispatch generate(SymbolInstruction symbolInstruction) {
		'''«symbolInstruction.symbolName»'''
	}

	def dispatch generate(EObject object) {
		null
	}	
}