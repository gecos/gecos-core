package fr.irisa.cairn.gecos.model.analysis.observers.control.internal;

import fr.irisa.cairn.gecos.model.analysis.observers.control.ControlFlowObserver;
import fr.irisa.cairn.gecos.model.analysis.observers.control.InstallControlFlowObserver;
import fr.irisa.cairn.gecos.model.analysis.observers.control.RemoveControlFlowObserver;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import gecos.blocks.BasicBlock;
import gecos.blocks.BlocksFactory;
import gecos.blocks.CompositeBlock;
import gecos.core.CoreFactory;
import gecos.core.Procedure;
import gecos.instrs.InstrsFactory;
import gecos.instrs.IntInstruction;

public class ControlFlowObserverExample {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		CoreFactory corefactory = CoreFactory.eINSTANCE;
		BlocksFactory blockfactory = BlocksFactory.eINSTANCE;
		Procedure proc = corefactory.createProcedure();

		CompositeBlock cblock= GecosUserBlockFactory.CompositeBlock();
		proc.setBody(cblock);
		
		BasicBlock bb = blockfactory.createBasicBlock();
		bb.setNumber(666);
		
		System.out.println("I'm adding a BB to current CB.");
		cblock.getChildren().add(bb);
		

		System.out.println("I'm installing the obsever.");

		ControlFlowObserver observer = new ControlFlowObserver();
		InstallControlFlowObserver installer = new InstallControlFlowObserver(proc, observer);
		installer.compute();
		
		System.out.println("I'm changing the BB number ");
		BasicBlock bb2 = (BasicBlock) cblock.getChildren().get(0);
		bb2.setNumber(667);
		
		System.out.println("I'm adding an Instruction to the BB ");
		IntInstruction intInst = InstrsFactory.eINSTANCE.createIntInstruction();
		intInst.setValue(45);
		bb2.getInstructions().add(intInst);

		System.out.println("I'm adding another BB to current CB.");
		cblock.getChildren().add(blockfactory.createBasicBlock());

		System.out.println("I'm removing the obsever.");
		RemoveControlFlowObserver remover = new RemoveControlFlowObserver(proc, observer);
		remover.compute();

		System.out.println("I'm adding yet another BB to current CB.");
		cblock.getChildren().add(blockfactory.createBasicBlock());

		System.out.println("I'm changing the BB number again");
		bb2.setNumber(667);
	}

}
