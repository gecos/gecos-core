/**
 */
package gecos.dag;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAG In Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.DAGInPort#getSource <em>Source</em>}</li>
 * </ul>
 *
 * @see gecos.dag.DagPackage#getDAGInPort()
 * @model
 * @generated
 */
public interface DAGInPort extends DAGPort, DagVisitable {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link gecos.dag.DAGEdge#getSink <em>Sink</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(DAGEdge)
	 * @see gecos.dag.DagPackage#getDAGInPort_Source()
	 * @see gecos.dag.DAGEdge#getSink
	 * @model opposite="sink" derived="true"
	 * @generated
	 */
	DAGEdge getSource();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGInPort#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(DAGEdge value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGInPort(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.dag.DAGEdge%&gt; _source = this.getSource();\n&lt;%gecos.dag.DAGOutPort%&gt; _src = null;\nif (_source!=null)\n{\n\t_src=_source.getSrc();\n}\n&lt;%gecos.dag.DAGNode%&gt; _parent = null;\nif (_src!=null)\n{\n\t_parent=_src.getParent();\n}\nreturn _parent;'"
	 * @generated
	 */
	DAGNode getSourceNode();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.dag.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _elvis = null;\n&lt;%org.eclipse.emf.ecore.EObject%&gt; _eContainer = this.eContainer();\n&lt;%java.lang.String%&gt; _string = null;\nif (_eContainer!=null)\n{\n\t_string=_eContainer.toString();\n}\nif (_string != null)\n{\n\t_elvis = _string;\n} else\n{\n\t_elvis = \"?\";\n}\n&lt;%java.lang.String%&gt; _plus = (_elvis + \".In[\");\nint _ident = this.getIdent();\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + &lt;%java.lang.Integer%&gt;.valueOf(_ident));\nreturn (_plus_1 + \"]\");'"
	 * @generated
	 */
	String toString();

} // DAGInPort
