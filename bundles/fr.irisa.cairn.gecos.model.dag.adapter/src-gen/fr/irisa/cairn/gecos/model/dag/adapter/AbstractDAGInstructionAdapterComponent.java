package fr.irisa.cairn.gecos.model.dag.adapter;

import fr.irisa.cairn.componentElement.ServiceModule;
import fr.irisa.cairn.gecos.model.dag.adapter.builder.AdapterBuilder;
import fr.irisa.cairn.gecos.model.dag.adapter.builder.AdapterFactory;
import fr.irisa.cairn.gecos.model.dag.adapter.model.IDAGInstructionAdapter;
import fr.irisa.cairn.gecos.model.dag.adapter.module.AdapterBuilderModule;
import fr.irisa.cairn.gecos.model.dag.adapter.module.AdapterFactoryModule;
import fr.irisa.cairn.graph.guice.GraphComponent;
import fr.irisa.cairn.graph.guice.modules.servicesBinding.FactoryServiceModule;
import gecos.dag.DAGInstruction;

/**
 * Abstract component injecting basic services (factory and builder).
 * For manual modifications go to {@link fr.irisa.cairn.gecos.model.dag.adapter.DAGInstructionAdapterComponent}
 */
public abstract class AbstractDAGInstructionAdapterComponent {
	private GraphComponent graphComponent;

	protected AbstractDAGInstructionAdapterComponent() {
		graphComponent = new GraphComponent();
		configure();
	}

	/**
	 * Configure the {@link GraphComponent}. Install the
	 * {@link AdapterBuilderModule} and {@link AdapterFactoryModule}.
	 */
	protected void configure() {
		replaceModule(FactoryServiceModule.class, new AdapterFactoryModule());
		installModule(new AdapterBuilderModule());
	}

	public void installModule(ServiceModule serviceModule) {
		graphComponent.installModule(serviceModule);

	}

	/**
	 * Get the object in charge of a service in the component.
	 * @param <T> service type
	 * @param serviceClassName
	 * @return 
	 */
	public <T> T getService(Class<T> serviceClassName) {
		return graphComponent.getService(serviceClassName);
	}

	public void installModules(Iterable<ServiceModule> servicesModules) {
		graphComponent.installModules(servicesModules);

	}

	public void installModules(ServiceModule... servicesModules) {
		graphComponent.installModules(servicesModules);

	}

	public void replaceModule(ServiceModule oldServiceModule,
			ServiceModule newServiceModule) {
		graphComponent.replaceModule(oldServiceModule, newServiceModule);
	}

	public void replaceModule(
			Class<? extends ServiceModule> oldServiceModuleClass,
			ServiceModule newServiceModule) {
		graphComponent.replaceModule(oldServiceModuleClass, newServiceModule);
	}

	public void unInstallModule(ServiceModule serviceModule) {
		graphComponent.unInstallService(serviceModule);

	}

	public void unInstallModules(ServiceModule... serviceModules) {
		graphComponent.unInstallServices(serviceModules);
	}

	/**
	 * Get the injected {@link AdapterFactory} of the component.
	 * 
	 * @return the factory in charge of adapters creations
	 */
	public AdapterFactory getAdapterFactory() {
		return getService(AdapterFactory.class);
	}

	/**
	 * Create and initialize the adapter of an {@link DAGInstruction}.
	 * @param adaptedGraph
	 * @return 
	 */
	public IDAGInstructionAdapter build(DAGInstruction adaptedGraph) {
		AdapterBuilder buider = DAGInstructionAdapterComponent.INSTANCE.getService(
				AdapterBuilder.class);
		return buider.buildGraphAdapter(adaptedGraph);
	}

}
