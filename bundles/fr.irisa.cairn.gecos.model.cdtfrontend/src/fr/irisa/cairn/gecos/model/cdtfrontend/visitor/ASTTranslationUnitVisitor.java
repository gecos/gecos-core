/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.cdtfrontend.visitor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.cdt.core.dom.ast.IASTDeclaration;
import org.eclipse.cdt.core.dom.ast.IASTPreprocessorIncludeStatement;
import org.eclipse.cdt.core.dom.ast.IASTPreprocessorPragmaStatement;
import org.eclipse.cdt.core.dom.ast.IASTPreprocessorStatement;
import org.eclipse.cdt.core.dom.ast.IASTTranslationUnit;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import gecos.annotations.PragmaAnnotation;

/**
 * @author mnaullet
 */
public class ASTTranslationUnitVisitor {

	public ASTTranslationUnitVisitor() {
	}

	public void visit(CDTToGecosModel gecosModel, IASTTranslationUnit translationUnit) {
		String stringPath = translationUnit.getFilePath();
		List<IASTPreprocessorPragmaStatement> preprocessorStatements = new ArrayList<IASTPreprocessorPragmaStatement>();
		List<IASTPreprocessorIncludeStatement> includeStatements = new ArrayList<IASTPreprocessorIncludeStatement>();
		for(IASTPreprocessorStatement preprocessorStatement :translationUnit.getAllPreprocessorStatements()) {
			if (preprocessorStatement.getContainingFilename().equals(stringPath)) {
				if (preprocessorStatement.isActive()) {	
					if (preprocessorStatement instanceof IASTPreprocessorPragmaStatement)
						preprocessorStatements.add((IASTPreprocessorPragmaStatement)preprocessorStatement);
					else if (preprocessorStatement instanceof IASTPreprocessorIncludeStatement)
						includeStatements.add((IASTPreprocessorIncludeStatement)preprocessorStatement);
				}
			}
		}

//		//Include management
//		long time = System.currentTimeMillis();
//		parseIncludeFiles(includeStatements, gecosModel, gecosModel.getCProject());
//		
//		if (CDTFrontEnd.VERBOSE) 
//			System.out.println("Includes visited ("+(System.currentTimeMillis() - time)+" ms).");
		
		if (!gecosModel.visitingInclude()) {
			List<String> pragmaAnnotation = new ArrayList<>();
			for(int i = 0; i < includeStatements.size(); i++) {
				if (includeStatements.get(i).isSystemInclude())
					pragmaAnnotation.add(GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION+"#include <" + includeStatements.get(i).getName().getRawSignature() + ">");
				else
					pragmaAnnotation.add(GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION+"#include \"" + includeStatements.get(i).getName().getRawSignature() + "\"");
			}
			if (pragmaAnnotation.size() > 0)
				GecosUserAnnotationFactory.pragma(gecosModel.getProcedureSet(), pragmaAnnotation.toArray(new String[pragmaAnnotation.size()]));
//				gecosModel.getProcedureSet().getPragma().getContent().addAll(pragmaAnnotation);
		}

		gecosModel.setPragmaList(preprocessorStatements);
		//load the wanted file
		IASTDeclaration[] declarations = translationUnit.getDeclarations();
		for(int i = 0; i < declarations.length; i++) {
			if (declarations[i].getContainingFilename().equals(stringPath) && (declarations[i].isActive())) {
				declarations[i].accept(gecosModel);
			} else {
				gecosModel.setIgnoreImplementation(true);
				declarations[i].accept(gecosModel);
				gecosModel.setIgnoreImplementation(false);
 			}
		}
	}

//	private void parseIncludeFiles(List<IASTPreprocessorIncludeStatement> includeStatements, CDTToGecosModel includeVisitor,ICProject cProject) {
//		if (includeStatements.size() > 0) {
//			IASTPreprocessorIncludeStatement lastIncludeStmt = null;
//			try {
//				// Manage include location
//				CDTToGecosModel gecosModel = includeVisitor;
//				GecosProject gProject = gecosModel.getProject();
//				for (IASTPreprocessorIncludeStatement iastPreprocessorIncludeStatement : includeStatements) {
//					lastIncludeStmt = iastPreprocessorIncludeStatement;
//					String rawSignature = iastPreprocessorIncludeStatement.getName().getRawSignature();
//					String includeFilepath = null;
//					for (GecosHeaderDirectory inc : gProject.getIncludes()) {
//						File file = new File(inc.getName() + "/" + rawSignature);
//						if (file.exists()) {
//							includeFilepath = file.getAbsolutePath();
//							break;
//						}
//					}
//					if (includeFilepath == null)
//						throw new FileNotFoundException("Could not find a header file for : \"" + iastPreprocessorIncludeStatement.getRawSignature() + "\"");
//					if (!rawSignature.contains("ac_") && !gecosModel.alreadVisited(includeFilepath)) {
//						gecosModel.visited(includeFilepath);
//						Path path = new Path(includeFilepath);
//						ITranslationUnit tunit = CDTFrontEnd.cdtCoreModel.createTranslationUnitFrom(cProject, path);
//						if (tunit == null) {
//							return;
//						}
//						IASTTranslationUnit ast = tunit.getAST();
//						if (CDTFrontEnd.VERBOSE) 
//							System.out.println(">> Visiting : "+includeFilepath);
//						ast.accept(includeVisitor);
//					}
//				}
//			} catch (Exception e) {
//				throw new CDTParserProblemException(lastIncludeStmt, e.getMessage(),e);
//			}
//		}
//	}
}