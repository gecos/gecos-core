package fr.irisa.cairn.gecos.model.transforms.commonsubexpressionelimination;

import java.util.ArrayList;

import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.tools.controlflow.BuildControlFlow;
import fr.irisa.cairn.gecos.model.tools.controlflow.ClearControlFlow;
import fr.irisa.cairn.gecos.model.transforms.commonsubexpressionelimination.internal.LocalCommonSubExpressionElimination;
import fr.irisa.r2d2.gecos.framework.GSModule;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.instrs.Instruction;

/**
 * LocalCommonSubExpressionEliminationMudule is a sample module. When the script
 * evaluator encounters the 'LocalCommonSubExpressionElimination' function, it
 * calls the compute method.
 */

/**
 * This Module eliminates the Local Common-Subexpressions in the code. This
 * transformation works within a single basic block. It assumes that the
 * instructions are in the MIR (Medium Level Intermediate Representation) format
 * i.e. quadruples.
 * 
 * The module implements the algorithm described in the book : Advanced Compiler
 * Design & Implementation, Steven S. Muchnick
 * 
 * @author Imen Fassi
 * @date 20/04/2016
 */

@GSModule("This Module eliminates the Local Common-Subexpressions in the code.")

public class LocalCommonSubExpressionEliminationModule {

	private GecosProject _project;

	/**
	 * if you want to consider expressions, having a function call as an
	 * operand, set consider_function_call to true or false otherwise.
	 */
	private boolean consider_function_call;
	/**
	 * if you want to be conservative regarding pointer, set
	 * pointer_conservative to true.
	 */
	private boolean pointer_conservative;

	public LocalCommonSubExpressionEliminationModule(GecosProject project, boolean bf, boolean bp) {
		this._project = project;
		this.consider_function_call = bf;
		this.pointer_conservative = bp;
	}

	public void compute() {
		
		new TransformInstructionsIntoQuadruplesModule(_project).compute();
		
//		System.out.println("[info] The Local Common SubExpression Elimination transformation supposes that "
//				+ "\n       the Instructions are transformed Into Quadruples."
//				+ "\n       (Call TransformInstructionsIntoQuadruples before running this pass.)");
		ArrayList<Instruction> list_global = new ArrayList<Instruction>();

		for (ProcedureSet ps : _project.listProcedureSets()) { // Iterate on
																// each
																// procedure set
																// contained in
																// a GeCoS
																// project
			Scope scope = ps.getScope();
			if (scope != null) {
				for (Symbol s : scope.getSymbols()) {
					if (!(s instanceof ProcedureSymbol)) {
						Instruction symbInstr = GecosUserInstructionFactory.symbref(s);
						list_global.add(symbInstr);
					}
				}
			}
			for (Procedure pr : ps.listProcedures()) { // Iterate on each
														// procedure contained
														// in a procedure set
				LocalCommonSubExpressionElimination visitor = new LocalCommonSubExpressionElimination(
						consider_function_call, pointer_conservative, list_global);
				pr.getBody().accept(visitor); // apply the visitor on the body
												// block of a procedure
			}
		}

		ClearControlFlow ccf = new ClearControlFlow(_project);
		ccf.compute();
		BuildControlFlow bcf = new BuildControlFlow(_project);
		bcf.compute();

//		System.out.println("[info] LocalCommonSubExpressionElimination: DONE");
	}
}