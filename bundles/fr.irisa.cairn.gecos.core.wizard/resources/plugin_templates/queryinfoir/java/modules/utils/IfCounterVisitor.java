package $packageName$.modules.utils;

import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksDefaultVisitor;
import gecos.blocks.IfBlock;

/**
 * Implementation of a custom Visitor which count each IfBlock detected.
 * 
 * This class extends GecosBlocksDefaultVisitor which is a Default visitor implementation.
 * GecosBlocksDefaultVisitor performs a depth-first traversal of the IR until a BasicBlock is reached
 */
public class IfCounterVisitor extends  GecosBlocksDefaultVisitor {
	
	private int count = 0;
	
	public int getCount() {
		return count;
	}
	

	/**
	 * Override the visitIfBlock to add the counter mechanism
	 */
	@Override
	public void visitIfBlock(IfBlock i) {
		count++;
		// Call the parent method to continue the IR traversal.
		// The parent method implement the full traversal of the IfBlock.
		super.visitIfBlock(i);
	}
}
