package fr.irisa.cairn.gecos.model.cdtfrontend;

import java.util.Set;
import java.util.stream.Stream;

import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.tools.ecore.EcoreUpdater;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import fr.irisa.r2d2.gecos.framework.GSArg;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import gecos.annotations.AnnotatedElement;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.gecosproject.GecosProject;

@GSModule("Try to resolve external Symbol links by looking up all ProcedureSets in a given project.")
public class ExternalLinkResolver {

	private static final String EXTERNAL_PROCEDURE_ANNOT_KEY = "ExternalLinkResolver::EXTERNAL_PROCEDURE";
	private GecosProject project;

	@GSModuleConstructor(value = "", args = {
		@GSArg(name = "project", info = "a parsed GecosProject.")
	})
	public ExternalLinkResolver(GecosProject project) {
		this.project = project;
	}
	
	public void compute() {
		for(ProcedureSet ps : project.listProcedureSets())
			findUnresolvedProcedureSymbols(ps).forEach(s -> tryResolve(s, ps));
	}

	private Stream<ProcedureSymbol> findUnresolvedProcedureSymbols(ProcedureSet ps) {
		Stream<ProcedureSymbol> symbols = EMFUtils.eAllContentsInstancesOf(ps, ProcedureSymbol.class).stream()
			.filter(s -> s.getProcedure() == null)
			.filter(s -> s.eContainer() != null)
			.filter(s -> !s.getScope().isGlobal()); //XXX
		return symbols;
	}
	
	/**
	 * If symbol does not contain a {@link Procedure},
	 * lookup project for one with same name.
	 * If any is found, replace the given {@link ProcedureSymbol} by the
	 * found procedure's symbol.
	 * 
	 * NOTE: a {@link Procedure} is contained in a {@link ProcedureSymbol},
	 * so we can not simply set the procedure attribute of the given symbol!
	 * 
	 * @param sym procedure symbol containing no procedure.
	 * @param ps {@link ProcedureSet} where the {@code sym} is contained (containment hierarchy).
	 */
	private void tryResolve(ProcedureSymbol sym, ProcedureSet ps) {
		assert(sym.getProcedure() == null);
		
		Procedure proc = lookupProcedure(project, sym.getName());
		if(proc != null)
			setExternalProcedure(sym, ps, proc);
	}

	private static void setExternalProcedure(ProcedureSymbol oldSym, ProcedureSet ps, Procedure proc) {
		ProcedureSymbol externalProcSym = proc.getSymbol();
		if(externalProcSym == null) return;
		
		// Replace all references to oldSym within the procedureSet in which it was found
		// by a reference to the external Symbol (with the procedure definition).
		Set<EObject> refsToOldSym = EMFUtils.getAllReferencesOf(oldSym, ps);
		for (EObject referencing : refsToOldSym) {
			EcoreUpdater.updateCrossReferences(referencing, oldSym, externalProcSym);
			if(referencing instanceof AnnotatedElement)
				((AnnotatedElement) referencing).getAnnotations().put(EXTERNAL_PROCEDURE_ANNOT_KEY, null);
		}
	}

	public static Procedure lookupProcedure(GecosProject project, String procName) {
		Procedure proc = project.listProcedures().stream()
			.filter(p -> p.getSymbolName().equals(procName))
			.findFirst().orElse(null);
		//XXX case of multiple procedures with same name are found ?
		return proc;
	}
	
}
