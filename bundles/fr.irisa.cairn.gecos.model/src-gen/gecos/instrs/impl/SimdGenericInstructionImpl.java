/**
 */
package gecos.instrs.impl;

import gecos.instrs.InstrsPackage;
import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;
import gecos.instrs.SimdGenericInstruction;
import gecos.instrs.SimdInstruction;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simd Generic Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SimdGenericInstructionImpl extends GenericInstructionImpl implements SimdGenericInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimdGenericInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstrsPackage.eINSTANCE.getSimdGenericInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final InstrsVisitor visitor) {
		visitor.visitSimdGenericInstruction(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSame(final Instruction instr) {
		if ((instr instanceof SimdInstruction)) {
			return super.isSame(instr);
		}
		return false;
	}

} //SimdGenericInstructionImpl
