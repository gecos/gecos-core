/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.c.generator;

import templates.xtend.GecosBlockTemplate;
import fr.irisa.cairn.gecos.model.extensions.generators.ExtendableGenerator;
import fr.irisa.cairn.gecos.model.extensions.generators.IGeneratorDispatchStrategy;

public class ExtendableBlockCGenerator extends ExtendableGenerator{

	public static ExtendableBlockCGenerator eInstance = new ExtendableBlockCGenerator();

	public ExtendableBlockCGenerator() {
		super(new GecosBlockTemplate());
	}
	
	public ExtendableBlockCGenerator(IGeneratorDispatchStrategy strategy) {
		super(strategy,new GecosBlockTemplate());
	}
}
