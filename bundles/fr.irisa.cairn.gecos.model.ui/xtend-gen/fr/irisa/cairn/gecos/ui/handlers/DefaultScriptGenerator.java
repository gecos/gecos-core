package fr.irisa.cairn.gecos.ui.handlers;

import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class DefaultScriptGenerator {
  public CharSequence generate() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#------------------------------------------------------------------------ ");
    _builder.newLine();
    _builder.append("#       ______     ______     _____");
    _builder.newLine();
    _builder.append("#      / ____/__  / ____/___ / ___/");
    _builder.newLine();
    _builder.append("#     / / __/ _ \\/ /   / __ \\\\__ \\ ");
    _builder.newLine();
    _builder.append("#    / /_/ /  __/ /___/ /_/ /__/ / ");
    _builder.newLine();
    _builder.append("#    \\____/\\___/\\____/\\____/____/  --Generic Compiler Suite--");
    _builder.newLine();
    _builder.append("#                              \t   (c) INRIA-IRISA 2011-2014");
    _builder.newLine();
    _builder.append("#------------------------------------------------------------------------  ");
    _builder.newLine();
    _builder.append("#");
    _builder.newLine();
    _builder.append("# This is a default GeCoS compiler script. It is called when selecting ");
    _builder.newLine();
    _builder.append("# \'compile\' in the Gecos menu available in the C editor. ");
    _builder.newLine();
    _builder.append("#");
    _builder.newLine();
    _builder.append("#");
    _builder.newLine();
    _builder.append("# Parameter : edited C file path");
    _builder.newLine();
    _builder.append("#");
    _builder.newLine();
    _builder.newLine();
    _builder.append("#Create a new GeCoS project and add the C source file");
    _builder.newLine();
    _builder.append("project = CreateGecosProject(\"project\");");
    _builder.newLine();
    _builder.append("AddSourceToGecosProject(project,$1);");
    _builder.newLine();
    _builder.newLine();
    _builder.append("#Build the procedure set using CDT");
    _builder.newLine();
    _builder.append("CDTFrontend(project);");
    _builder.newLine();
    _builder.newLine();
    _builder.append("#Add your compilation passes here");
    _builder.newLine();
    _builder.append("for file in project do");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("ps = GetProcedureSet(file);");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("#outputs a dotty representing the CDFG for ps");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("output(ps);");
    _builder.newLine();
    _builder.append("done;");
    _builder.newLine();
    return _builder;
  }
}
