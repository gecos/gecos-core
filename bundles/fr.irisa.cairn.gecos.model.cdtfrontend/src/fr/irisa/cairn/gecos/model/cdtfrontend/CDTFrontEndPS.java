/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.cdtfrontend;

import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.modules.AddIncludeDirToGecosProject;
import fr.irisa.cairn.gecos.model.modules.AddSourceToGecosProject;
import fr.irisa.cairn.gecos.model.modules.CreateProject;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.EList;

/**
 * Wrapper to call the front end on a single file.
 */
@GSModule("Wrapper to call CDTFrontEnd on a single source file.\n"
		+ "\nReturn: the constructed ProcedureSet.\n"
		+ "\nSee: 'CDTFrontEnd' Module.")
public class CDTFrontEndPS {

	private String path;
	private String[] _include_dirs;
	
	@GSModuleConstructor(
			"-arg1: source file path."
		  + "\n-arg2: list of include directories.")
	public CDTFrontEndPS(String cFilePath, String[] includeDirs) {
		this(cFilePath);
		_include_dirs = includeDirs;
	}
	
	@GSModuleConstructor("-arg1: source file path.")
	public CDTFrontEndPS(String cFilePath) {
		if (cFilePath == null)
			throw new NullPointerException();
		path = cFilePath;
	}

	public ProcedureSet compute() throws CoreException {
		GecosProject project = new CreateProject(GecosUserCoreFactory.DEFAULT_PROJECT_NAME).compute();
		new AddSourceToGecosProject(project, path);
		if(_include_dirs != null)
			new AddIncludeDirToGecosProject(project, _include_dirs).compute();
		
		new CDTFrontEnd(project).compute();
		EList<ProcedureSet> procedureSets = project.listProcedureSets();
		if (procedureSets.size() != 1)
			throw new RuntimeException();
		return procedureSets.get(0);
	}
}
