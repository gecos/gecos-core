package fr.irisa.cairn.gecos.model.includes.std;

import java.util.List;

import org.eclipse.core.runtime.Plugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class GecosStandardIncludes extends Plugin {
	
	// The plug-in ID
	public static final String PLUGIN_ID = "fr.irisa.cairn.gecos.model.includes.std";
	
	private String[] paths;
	
	// The shared instance
	private static GecosStandardIncludes plugin;
	
	/**
	 * The constructor
	 */
	public GecosStandardIncludes() {
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		List<String> pathsList = ComputeIncludesPath.getPaths();
		paths = pathsList.toArray(new String[pathsList.size()]);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}
	
	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static GecosStandardIncludes getDefault() {
		return plugin;
	}

	public String[] getIncludePaths() {
		return paths;
	}
	
	public boolean isGecosStandardInclude(String dirPath) {
		return dirPath.contains("fr.irisa.cairn.gecos.model.includes.std");
	}
}
