#ifndef STAT_H
#define STAT_H

struct stat {
    int         st_dev;      /* Périphérique                */
    int         st_ino;      /* Numéro i-noeud              */
    int        st_mode;     /* Protection                  */
    int       st_nlink;    /* Nb liens matériels          */
    int         st_uid;      /* UID propriétaire            */
    int         st_gid;      /* GID propriétaire            */
    int         st_rdev;     /* Type périphérique           */
    int         st_size;     /* Taille totale en octets     */
    unsigned long st_blksize;  /* Taille de bloc pour E/S     */
    unsigned long st_blocks;   /* Nombre de blocs alloués     */
    int        st_atime;    /* Heure dernier accès         */
    int        st_mtime;    /* Heure dernière modification */
    int        st_ctime;    /* Heure dernier changement    */
};

extern int stat (const char * __file,  struct stat * __buf) ;
extern int fstat (int __fd, struct stat *__buf);
extern int lstat (const char * __file,  struct stat * __buf) ;
extern int chmod (const char *__file, int __mode);

#endif
