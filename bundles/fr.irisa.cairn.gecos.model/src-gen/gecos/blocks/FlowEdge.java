/**
 */
package gecos.blocks;

import gecos.annotations.AnnotatedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Flow Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.blocks.BlocksPackage#getFlowEdge()
 * @model interface="true" abstract="true"
 *        annotation="gmf.link source='from' target='to' style='dot' width='1'"
 * @generated
 */
public interface FlowEdge extends BlocksVisitable, AnnotatedElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 * @generated
	 */
	BasicBlock getFrom();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 * @generated
	 */
	BasicBlock getTo();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitFlowEdge(this);'"
	 * @generated
	 */
	void accept(BlocksVisitor visitor);

} // FlowEdge
