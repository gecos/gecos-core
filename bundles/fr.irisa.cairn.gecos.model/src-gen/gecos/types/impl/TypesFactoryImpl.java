/**
 */
package gecos.types.impl;

import gecos.types.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TypesFactoryImpl extends EFactoryImpl implements TypesFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TypesFactory init() {
		try {
			TypesFactory theTypesFactory = (TypesFactory)EPackage.Registry.INSTANCE.getEFactory(TypesPackage.eNS_URI);
			if (theTypesFactory != null) {
				return theTypesFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TypesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TypesPackage.FIELD: return createField();
			case TypesPackage.ENUMERATOR: return createEnumerator();
			case TypesPackage.ARRAY_TYPE: return createArrayType();
			case TypesPackage.PTR_TYPE: return createPtrType();
			case TypesPackage._INTERNAL_PARAMETER_TYPE_WRAPPER: return create__Internal_ParameterTypeWrapper();
			case TypesPackage.FUNCTION_TYPE: return createFunctionType();
			case TypesPackage.RECORD_TYPE: return createRecordType();
			case TypesPackage.ALIAS_TYPE: return createAliasType();
			case TypesPackage.ENUM_TYPE: return createEnumType();
			case TypesPackage.SIMD_TYPE: return createSimdType();
			case TypesPackage.UNDEFINED_TYPE: return createUndefinedType();
			case TypesPackage.INTEGER_TYPE: return createIntegerType();
			case TypesPackage.FLOAT_TYPE: return createFloatType();
			case TypesPackage.BOOL_TYPE: return createBoolType();
			case TypesPackage.VOID_TYPE: return createVoidType();
			case TypesPackage.AC_TYPE: return createACType();
			case TypesPackage.AC_INT_TYPE: return createACIntType();
			case TypesPackage.AC_FIXED_TYPE: return createACFixedType();
			case TypesPackage.AC_FLOAT_TYPE: return createACFloatType();
			case TypesPackage.AC_COMPLEX_TYPE: return createACComplexType();
			case TypesPackage.AC_CHANNEL_TYPE: return createACChannelType();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case TypesPackage.STORAGE_CLASS_SPECIFIERS:
				return createStorageClassSpecifiersFromString(eDataType, initialValue);
			case TypesPackage.KINDS:
				return createKindsFromString(eDataType, initialValue);
			case TypesPackage.INTEGER_TYPES:
				return createIntegerTypesFromString(eDataType, initialValue);
			case TypesPackage.SIGN_MODIFIERS:
				return createSignModifiersFromString(eDataType, initialValue);
			case TypesPackage.FLOAT_PRECISIONS:
				return createFloatPrecisionsFromString(eDataType, initialValue);
			case TypesPackage.OVERFLOW_MODE:
				return createOverflowModeFromString(eDataType, initialValue);
			case TypesPackage.QUANTIFICATION_MODE:
				return createQuantificationModeFromString(eDataType, initialValue);
			case TypesPackage.STRING:
				return createStringFromString(eDataType, initialValue);
			case TypesPackage.LONG:
				return createlongFromString(eDataType, initialValue);
			case TypesPackage.INT:
				return createintFromString(eDataType, initialValue);
			case TypesPackage.BOOLEAN:
				return createbooleanFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case TypesPackage.STORAGE_CLASS_SPECIFIERS:
				return convertStorageClassSpecifiersToString(eDataType, instanceValue);
			case TypesPackage.KINDS:
				return convertKindsToString(eDataType, instanceValue);
			case TypesPackage.INTEGER_TYPES:
				return convertIntegerTypesToString(eDataType, instanceValue);
			case TypesPackage.SIGN_MODIFIERS:
				return convertSignModifiersToString(eDataType, instanceValue);
			case TypesPackage.FLOAT_PRECISIONS:
				return convertFloatPrecisionsToString(eDataType, instanceValue);
			case TypesPackage.OVERFLOW_MODE:
				return convertOverflowModeToString(eDataType, instanceValue);
			case TypesPackage.QUANTIFICATION_MODE:
				return convertQuantificationModeToString(eDataType, instanceValue);
			case TypesPackage.STRING:
				return convertStringToString(eDataType, instanceValue);
			case TypesPackage.LONG:
				return convertlongToString(eDataType, instanceValue);
			case TypesPackage.INT:
				return convertintToString(eDataType, instanceValue);
			case TypesPackage.BOOLEAN:
				return convertbooleanToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Field createField() {
		FieldImpl field = new FieldImpl();
		return field;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Enumerator createEnumerator() {
		EnumeratorImpl enumerator = new EnumeratorImpl();
		return enumerator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArrayType createArrayType() {
		ArrayTypeImpl arrayType = new ArrayTypeImpl();
		return arrayType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PtrType createPtrType() {
		PtrTypeImpl ptrType = new PtrTypeImpl();
		return ptrType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public __Internal_ParameterTypeWrapper create__Internal_ParameterTypeWrapper() {
		__Internal_ParameterTypeWrapperImpl __Internal_ParameterTypeWrapper = new __Internal_ParameterTypeWrapperImpl();
		return __Internal_ParameterTypeWrapper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionType createFunctionType() {
		FunctionTypeImpl functionType = new FunctionTypeImpl();
		return functionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordType createRecordType() {
		RecordTypeImpl recordType = new RecordTypeImpl();
		return recordType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AliasType createAliasType() {
		AliasTypeImpl aliasType = new AliasTypeImpl();
		return aliasType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumType createEnumType() {
		EnumTypeImpl enumType = new EnumTypeImpl();
		return enumType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimdType createSimdType() {
		SimdTypeImpl simdType = new SimdTypeImpl();
		return simdType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UndefinedType createUndefinedType() {
		UndefinedTypeImpl undefinedType = new UndefinedTypeImpl();
		return undefinedType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerType createIntegerType() {
		IntegerTypeImpl integerType = new IntegerTypeImpl();
		return integerType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FloatType createFloatType() {
		FloatTypeImpl floatType = new FloatTypeImpl();
		return floatType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BoolType createBoolType() {
		BoolTypeImpl boolType = new BoolTypeImpl();
		return boolType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VoidType createVoidType() {
		VoidTypeImpl voidType = new VoidTypeImpl();
		return voidType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ACType createACType() {
		ACTypeImpl acType = new ACTypeImpl();
		return acType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ACIntType createACIntType() {
		ACIntTypeImpl acIntType = new ACIntTypeImpl();
		return acIntType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ACFixedType createACFixedType() {
		ACFixedTypeImpl acFixedType = new ACFixedTypeImpl();
		return acFixedType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ACFloatType createACFloatType() {
		ACFloatTypeImpl acFloatType = new ACFloatTypeImpl();
		return acFloatType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ACComplexType createACComplexType() {
		ACComplexTypeImpl acComplexType = new ACComplexTypeImpl();
		return acComplexType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ACChannelType createACChannelType() {
		ACChannelTypeImpl acChannelType = new ACChannelTypeImpl();
		return acChannelType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StorageClassSpecifiers createStorageClassSpecifiersFromString(EDataType eDataType, String initialValue) {
		StorageClassSpecifiers result = StorageClassSpecifiers.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStorageClassSpecifiersToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Kinds createKindsFromString(EDataType eDataType, String initialValue) {
		Kinds result = Kinds.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertKindsToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerTypes createIntegerTypesFromString(EDataType eDataType, String initialValue) {
		IntegerTypes result = IntegerTypes.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIntegerTypesToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignModifiers createSignModifiersFromString(EDataType eDataType, String initialValue) {
		SignModifiers result = SignModifiers.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSignModifiersToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FloatPrecisions createFloatPrecisionsFromString(EDataType eDataType, String initialValue) {
		FloatPrecisions result = FloatPrecisions.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFloatPrecisionsToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OverflowMode createOverflowModeFromString(EDataType eDataType, String initialValue) {
		OverflowMode result = OverflowMode.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertOverflowModeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QuantificationMode createQuantificationModeFromString(EDataType eDataType, String initialValue) {
		QuantificationMode result = QuantificationMode.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertQuantificationModeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createStringFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStringToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Long createlongFromString(EDataType eDataType, String initialValue) {
		return (Long)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertlongToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer createintFromString(EDataType eDataType, String initialValue) {
		return (Integer)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertintToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean createbooleanFromString(EDataType eDataType, String initialValue) {
		return (Boolean)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertbooleanToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypesPackage getTypesPackage() {
		return (TypesPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TypesPackage getPackage() {
		return TypesPackage.eINSTANCE;
	}

} //TypesFactoryImpl
