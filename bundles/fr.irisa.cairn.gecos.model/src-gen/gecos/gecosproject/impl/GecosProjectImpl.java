/**
 */
package gecos.gecosproject.impl;

import com.google.common.collect.Iterables;

import fr.irisa.cairn.gecos.model.tools.utils.GecosCopier;

import gecos.annotations.AnnotatedElement;
import gecos.annotations.AnnotationKeys;
import gecos.annotations.AnnotationsPackage;
import gecos.annotations.FileLocationAnnotation;
import gecos.annotations.IAnnotation;
import gecos.annotations.PragmaAnnotation;

import gecos.annotations.impl.StringToIAnnotationMapImpl;

import gecos.core.Procedure;
import gecos.core.ProcedureSet;

import gecos.gecosproject.GecosHeaderDirectory;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;
import gecos.gecosproject.GecosprojectPackage;
import gecos.gecosproject.ProjectVisitor;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Gecos Project</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.gecosproject.impl.GecosProjectImpl#getAnnotations <em>Annotations</em>}</li>
 *   <li>{@link gecos.gecosproject.impl.GecosProjectImpl#getName <em>Name</em>}</li>
 *   <li>{@link gecos.gecosproject.impl.GecosProjectImpl#getIncludes <em>Includes</em>}</li>
 *   <li>{@link gecos.gecosproject.impl.GecosProjectImpl#getSources <em>Sources</em>}</li>
 *   <li>{@link gecos.gecosproject.impl.GecosProjectImpl#getMacros <em>Macros</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GecosProjectImpl extends MinimalEObjectImpl.Container implements GecosProject {
	/**
	 * The cached value of the '{@link #getAnnotations() <em>Annotations</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, IAnnotation> annotations;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = "gecos_project";

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getIncludes() <em>Includes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncludes()
	 * @generated
	 * @ordered
	 */
	protected EList<GecosHeaderDirectory> includes;

	/**
	 * The cached value of the '{@link #getSources() <em>Sources</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSources()
	 * @generated
	 * @ordered
	 */
	protected EList<GecosSourceFile> sources;

	/**
	 * The cached value of the '{@link #getMacros() <em>Macros</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMacros()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> macros;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GecosProjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GecosprojectPackage.Literals.GECOS_PROJECT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, IAnnotation> getAnnotations() {
		if (annotations == null) {
			annotations = new EcoreEMap<String,IAnnotation>(AnnotationsPackage.Literals.STRING_TO_IANNOTATION_MAP, StringToIAnnotationMapImpl.class, this, GecosprojectPackage.GECOS_PROJECT__ANNOTATIONS);
		}
		return annotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GecosprojectPackage.GECOS_PROJECT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GecosHeaderDirectory> getIncludes() {
		if (includes == null) {
			includes = new EObjectContainmentEList<GecosHeaderDirectory>(GecosHeaderDirectory.class, this, GecosprojectPackage.GECOS_PROJECT__INCLUDES);
		}
		return includes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GecosSourceFile> getSources() {
		if (sources == null) {
			sources = new EObjectContainmentEList<GecosSourceFile>(GecosSourceFile.class, this, GecosprojectPackage.GECOS_PROJECT__SOURCES);
		}
		return sources;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, String> getMacros() {
		if (macros == null) {
			macros = new EcoreEMap<String,String>(GecosprojectPackage.Literals.STRING_TO_STRING_MAP, StringToStringMapImpl.class, this, GecosprojectPackage.GECOS_PROJECT__MACROS);
		}
		return macros;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcedureSet> listProcedureSets() {
		final Function1<GecosSourceFile, ProcedureSet> _function = new Function1<GecosSourceFile, ProcedureSet>() {
			public ProcedureSet apply(final GecosSourceFile it) {
				return it.getModel();
			}
		};
		return ECollections.<ProcedureSet>unmodifiableEList(ECollections.<ProcedureSet>asEList(((ProcedureSet[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<ProcedureSet>filterNull(XcoreEListExtensions.<GecosSourceFile, ProcedureSet>map(this.getSources(), _function)), ProcedureSet.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Procedure> listProcedures() {
		final Function1<ProcedureSet, EList<Procedure>> _function = new Function1<ProcedureSet, EList<Procedure>>() {
			public EList<Procedure> apply(final ProcedureSet it) {
				return it.listProcedures();
			}
		};
		return ECollections.<Procedure>unmodifiableEList(ECollections.<Procedure>asEList(((Procedure[])org.eclipse.xtext.xbase.lib.Conversions.unwrapArray(IterableExtensions.<Procedure>filterNull(Iterables.<Procedure>concat(XcoreEListExtensions.<ProcedureSet, EList<Procedure>>map(this.listProcedureSets(), _function))), Procedure.class))));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isParsed() {
		final Function1<GecosSourceFile, Boolean> _function = new Function1<GecosSourceFile, Boolean>() {
			public Boolean apply(final GecosSourceFile it) {
				return Boolean.valueOf(it.isParsed());
			}
		};
		return IterableExtensions.<GecosSourceFile>forall(this.getSources(), _function);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final ProjectVisitor visitor) {
		visitor.visitProject(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		return this.getName();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GecosProject copy() {
		final GecosCopier gecosCopier = new GecosCopier();
		EObject _copy = gecosCopier.copy(this);
		final GecosProject copy = ((GecosProject) _copy);
		gecosCopier.copyReferences();
		return copy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IAnnotation getAnnotation(final String key) {
		return this.getAnnotations().get(key);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnnotation(final String key, final IAnnotation annot) {
		this.getAnnotations().put(key, annot);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PragmaAnnotation getPragma() {
		final IAnnotation annot = this.getAnnotation(AnnotationKeys.PRAGMA_ANNOTATION_KEY.getLiteral());
		if ((annot instanceof PragmaAnnotation)) {
			return ((PragmaAnnotation)annot);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FileLocationAnnotation getFileLocation() {
		final IAnnotation annot = this.getAnnotation(AnnotationKeys.FILE_LOCATION_ANNOTATION_KEY.getLiteral());
		if ((annot instanceof FileLocationAnnotation)) {
			return ((FileLocationAnnotation)annot);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GecosprojectPackage.GECOS_PROJECT__ANNOTATIONS:
				return ((InternalEList<?>)getAnnotations()).basicRemove(otherEnd, msgs);
			case GecosprojectPackage.GECOS_PROJECT__INCLUDES:
				return ((InternalEList<?>)getIncludes()).basicRemove(otherEnd, msgs);
			case GecosprojectPackage.GECOS_PROJECT__SOURCES:
				return ((InternalEList<?>)getSources()).basicRemove(otherEnd, msgs);
			case GecosprojectPackage.GECOS_PROJECT__MACROS:
				return ((InternalEList<?>)getMacros()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GecosprojectPackage.GECOS_PROJECT__ANNOTATIONS:
				if (coreType) return getAnnotations();
				else return getAnnotations().map();
			case GecosprojectPackage.GECOS_PROJECT__NAME:
				return getName();
			case GecosprojectPackage.GECOS_PROJECT__INCLUDES:
				return getIncludes();
			case GecosprojectPackage.GECOS_PROJECT__SOURCES:
				return getSources();
			case GecosprojectPackage.GECOS_PROJECT__MACROS:
				if (coreType) return getMacros();
				else return getMacros().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GecosprojectPackage.GECOS_PROJECT__ANNOTATIONS:
				((EStructuralFeature.Setting)getAnnotations()).set(newValue);
				return;
			case GecosprojectPackage.GECOS_PROJECT__NAME:
				setName((String)newValue);
				return;
			case GecosprojectPackage.GECOS_PROJECT__INCLUDES:
				getIncludes().clear();
				getIncludes().addAll((Collection<? extends GecosHeaderDirectory>)newValue);
				return;
			case GecosprojectPackage.GECOS_PROJECT__SOURCES:
				getSources().clear();
				getSources().addAll((Collection<? extends GecosSourceFile>)newValue);
				return;
			case GecosprojectPackage.GECOS_PROJECT__MACROS:
				((EStructuralFeature.Setting)getMacros()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GecosprojectPackage.GECOS_PROJECT__ANNOTATIONS:
				getAnnotations().clear();
				return;
			case GecosprojectPackage.GECOS_PROJECT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case GecosprojectPackage.GECOS_PROJECT__INCLUDES:
				getIncludes().clear();
				return;
			case GecosprojectPackage.GECOS_PROJECT__SOURCES:
				getSources().clear();
				return;
			case GecosprojectPackage.GECOS_PROJECT__MACROS:
				getMacros().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GecosprojectPackage.GECOS_PROJECT__ANNOTATIONS:
				return annotations != null && !annotations.isEmpty();
			case GecosprojectPackage.GECOS_PROJECT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case GecosprojectPackage.GECOS_PROJECT__INCLUDES:
				return includes != null && !includes.isEmpty();
			case GecosprojectPackage.GECOS_PROJECT__SOURCES:
				return sources != null && !sources.isEmpty();
			case GecosprojectPackage.GECOS_PROJECT__MACROS:
				return macros != null && !macros.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == AnnotatedElement.class) {
			switch (derivedFeatureID) {
				case GecosprojectPackage.GECOS_PROJECT__ANNOTATIONS: return AnnotationsPackage.ANNOTATED_ELEMENT__ANNOTATIONS;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == AnnotatedElement.class) {
			switch (baseFeatureID) {
				case AnnotationsPackage.ANNOTATED_ELEMENT__ANNOTATIONS: return GecosprojectPackage.GECOS_PROJECT__ANNOTATIONS;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //GecosProjectImpl
