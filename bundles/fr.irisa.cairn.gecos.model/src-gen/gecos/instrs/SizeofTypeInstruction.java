/**
 */
package gecos.instrs;

import gecos.types.Type;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sizeof Type Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.instrs.SizeofTypeInstruction#getTargetType <em>Target Type</em>}</li>
 * </ul>
 *
 * @see gecos.instrs.InstrsPackage#getSizeofTypeInstruction()
 * @model
 * @generated
 */
public interface SizeofTypeInstruction extends SizeofInstruction {
	/**
	 * Returns the value of the '<em><b>Target Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Type</em>' reference.
	 * @see #setTargetType(Type)
	 * @see gecos.instrs.InstrsPackage#getSizeofTypeInstruction_TargetType()
	 * @model
	 * @generated
	 */
	Type getTargetType();

	/**
	 * Sets the value of the '{@link gecos.instrs.SizeofTypeInstruction#getTargetType <em>Target Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Type</em>' reference.
	 * @see #getTargetType()
	 * @generated
	 */
	void setTargetType(Type value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitSizeofTypeInstruction(this);'"
	 * @generated
	 */
	void accept(InstrsVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.instrs.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.Type%&gt; _targetType = this.getTargetType();\n&lt;%java.lang.String%&gt; _plus = (\"sizeof(\" + _targetType);\nreturn (_plus + \")\");'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" instrUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((instr instanceof &lt;%gecos.instrs.SizeofTypeInstruction%&gt;))\n{\n\tboolean isSame = false;\n\tif (((this.getTargetType() == null) &amp;&amp; (((&lt;%gecos.instrs.SizeofTypeInstruction%&gt;)instr).getTargetType() == null)))\n\t{\n\t\tisSame = true;\n\t}\n\telse\n\t{\n\t\t&lt;%gecos.types.Type%&gt; _targetType = this.getTargetType();\n\t\tboolean _tripleEquals = (_targetType == null);\n\t\t&lt;%gecos.types.Type%&gt; _targetType_1 = ((&lt;%gecos.instrs.SizeofTypeInstruction%&gt;)instr).getTargetType();\n\t\tboolean _tripleEquals_1 = (_targetType_1 == null);\n\t\tboolean _xor = (_tripleEquals ^ _tripleEquals_1);\n\t\tif (_xor)\n\t\t{\n\t\t\treturn false;\n\t\t}\n\t\telse\n\t\t{\n\t\t\tisSame = this.getTargetType().isEqual(((&lt;%gecos.instrs.SizeofTypeInstruction%&gt;)instr).getTargetType());\n\t\t}\n\t}\n\treturn (super.isSame(instr) &amp;&amp; isSame);\n}\nreturn false;'"
	 * @generated
	 */
	boolean isSame(Instruction instr);

} // SizeofTypeInstruction
