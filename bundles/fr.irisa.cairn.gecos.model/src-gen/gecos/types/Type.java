/**
 */
package gecos.types;

import gecos.annotations.AnnotatedElement;

import gecos.core.Scope;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.types.Type#isConstant <em>Constant</em>}</li>
 *   <li>{@link gecos.types.Type#isVolatile <em>Volatile</em>}</li>
 *   <li>{@link gecos.types.Type#getStorageClass <em>Storage Class</em>}</li>
 * </ul>
 *
 * @see gecos.types.TypesPackage#getType()
 * @model abstract="true"
 * @generated
 */
public interface Type extends AnnotatedElement, TypesVisitable {
	/**
	 * Returns the value of the '<em><b>Constant</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant</em>' attribute.
	 * @see #setConstant(boolean)
	 * @see gecos.types.TypesPackage#getType_Constant()
	 * @model default="false" unique="false" dataType="gecos.types.boolean"
	 * @generated
	 */
	boolean isConstant();

	/**
	 * Sets the value of the '{@link gecos.types.Type#isConstant <em>Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant</em>' attribute.
	 * @see #isConstant()
	 * @generated
	 */
	void setConstant(boolean value);

	/**
	 * Returns the value of the '<em><b>Volatile</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile</em>' attribute.
	 * @see #setVolatile(boolean)
	 * @see gecos.types.TypesPackage#getType_Volatile()
	 * @model default="false" unique="false" dataType="gecos.types.boolean"
	 * @generated
	 */
	boolean isVolatile();

	/**
	 * Sets the value of the '{@link gecos.types.Type#isVolatile <em>Volatile</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Volatile</em>' attribute.
	 * @see #isVolatile()
	 * @generated
	 */
	void setVolatile(boolean value);

	/**
	 * Returns the value of the '<em><b>Storage Class</b></em>' attribute.
	 * The default value is <code>"NONE"</code>.
	 * The literals are from the enumeration {@link gecos.types.StorageClassSpecifiers}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Class</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Class</em>' attribute.
	 * @see gecos.types.StorageClassSpecifiers
	 * @see #setStorageClass(StorageClassSpecifiers)
	 * @see gecos.types.TypesPackage#getType_StorageClass()
	 * @model default="NONE" unique="false"
	 * @generated
	 */
	StorageClassSpecifiers getStorageClass();

	/**
	 * Sets the value of the '{@link gecos.types.Type#getStorageClass <em>Storage Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Storage Class</em>' attribute.
	 * @see gecos.types.StorageClassSpecifiers
	 * @see #getStorageClass()
	 * @generated
	 */
	void setStorageClass(StorageClassSpecifiers value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model innermostStorageUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.setStorageClass(innermostStorage);'"
	 * @generated
	 */
	void setInnermostStorageClass(StorageClassSpecifiers innermostStorage);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.types.long" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _simpleName = this.getClass().getSimpleName();\n&lt;%java.lang.String%&gt; _plus = (\"No default size for abstract type class (\" + _simpleName);\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \")\");\nthrow new &lt;%java.lang.UnsupportedOperationException%&gt;(_plus_1);'"
	 * @generated
	 */
	long getSize();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 * @generated
	 */
	void accept(TypesVisitor visitor);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.ecore.EObject%&gt; _eContainer = this.eContainer();\nif ((_eContainer instanceof &lt;%gecos.core.Scope%&gt;))\n{\n\t&lt;%org.eclipse.emf.ecore.EObject%&gt; _eContainer_1 = this.eContainer();\n\treturn ((&lt;%gecos.core.Scope%&gt;) _eContainer_1);\n}\nreturn null;'"
	 * @generated
	 */
	Scope getContainingScope();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.String" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%java.lang.StringBuffer%&gt; result = new &lt;%java.lang.StringBuffer%&gt;();\nint _value = this.getStorageClass().getValue();\nboolean _notEquals = (_value != &lt;%gecos.types.StorageClassSpecifiers%&gt;.NONE_VALUE);\nif (_notEquals)\n{\n\tresult.append(this.getStorageClass().getLiteral().toLowerCase()).append(\" \");\n}\nboolean _isConstant = this.isConstant();\nif (_isConstant)\n{\n\tresult.append(\"const \");\n}\nboolean _isVolatile = this.isVolatile();\nif (_isVolatile)\n{\n\tresult.append(\"volatile \");\n}\nreturn result.toString();'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%fr.irisa.cairn.gecos.model.tools.utils.GecosCopier%&gt; copier = new &lt;%fr.irisa.cairn.gecos.model.tools.utils.GecosCopier%&gt;();\n&lt;%org.eclipse.emf.ecore.EObject%&gt; _copy = copier.copy(this);\nfinal T t = ((T) _copy);\ncopier.copyReferences();\nreturn t;'"
	 * @generated
	 */
	<T extends Type> T copy();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model scopeUnique="false" scopeRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='scope.getTypes().add(this);'"
	 * @generated
	 */
	void installOn(Scope scope);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.types.boolean" unique="false" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.StorageClassSpecifiers%&gt; _storageClass = this.getStorageClass();\nreturn &lt;%com.google.common.base.Objects%&gt;.equal(_storageClass, &lt;%gecos.types.StorageClassSpecifiers%&gt;.STATIC);'"
	 * @generated
	 */
	boolean isStatic();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.types.boolean" unique="false" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.StorageClassSpecifiers%&gt; _storageClass = this.getStorageClass();\nreturn &lt;%com.google.common.base.Objects%&gt;.equal(_storageClass, &lt;%gecos.types.StorageClassSpecifiers%&gt;.REGISTER);'"
	 * @generated
	 */
	boolean isRegister();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.types.boolean" unique="false" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.StorageClassSpecifiers%&gt; _storageClass = this.getStorageClass();\nreturn &lt;%com.google.common.base.Objects%&gt;.equal(_storageClass, &lt;%gecos.types.StorageClassSpecifiers%&gt;.EXTERN);'"
	 * @generated
	 */
	boolean isExtern();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.types.boolean" unique="false" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.StorageClassSpecifiers%&gt; _storageClass = this.getStorageClass();\nreturn &lt;%com.google.common.base.Objects%&gt;.equal(_storageClass, &lt;%gecos.types.StorageClassSpecifiers%&gt;.AUTO);'"
	 * @generated
	 */
	boolean isAuto();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((this instanceof &lt;%gecos.types.BaseType%&gt;))\n{\n\treturn ((&lt;%gecos.types.BaseType%&gt;) this);\n}\nreturn null;'"
	 * @generated
	 */
	BaseType asBase();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((this instanceof &lt;%gecos.types.ArrayType%&gt;))\n{\n\treturn ((&lt;%gecos.types.ArrayType%&gt;) this);\n}\nreturn null;'"
	 * @generated
	 */
	ArrayType asArray();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((this instanceof &lt;%gecos.types.PtrType%&gt;))\n{\n\treturn ((&lt;%gecos.types.PtrType%&gt;) this);\n}\nreturn null;'"
	 * @generated
	 */
	PtrType asPointer();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((this instanceof &lt;%gecos.types.FunctionType%&gt;))\n{\n\treturn ((&lt;%gecos.types.FunctionType%&gt;) this);\n}\nreturn null;'"
	 * @generated
	 */
	FunctionType asFunction();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((this instanceof &lt;%gecos.types.RecordType%&gt;))\n{\n\treturn ((&lt;%gecos.types.RecordType%&gt;) this);\n}\nreturn null;'"
	 * @generated
	 */
	RecordType asRecord();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((this instanceof &lt;%gecos.types.AliasType%&gt;))\n{\n\treturn ((&lt;%gecos.types.AliasType%&gt;) this);\n}\nreturn null;'"
	 * @generated
	 */
	AliasType asAlias();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((this instanceof &lt;%gecos.types.EnumType%&gt;))\n{\n\treturn ((&lt;%gecos.types.EnumType%&gt;) this);\n}\nreturn null;'"
	 * @generated
	 */
	EnumType asEnum();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((this instanceof &lt;%gecos.types.SimdType%&gt;))\n{\n\treturn ((&lt;%gecos.types.SimdType%&gt;) this);\n}\nreturn null;'"
	 * @generated
	 */
	SimdType asSimd();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.types.boolean" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return (this instanceof &lt;%gecos.types.ArrayType%&gt;);'"
	 * @generated
	 */
	boolean isArray();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="gecos.types.boolean" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return (this instanceof &lt;%gecos.types.PtrType%&gt;);'"
	 * @generated
	 */
	boolean isPointer();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.boolean" unique="false" otherUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.isEqual(other, false, false, false);'"
	 * @generated
	 */
	boolean isEqual(Type other);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.boolean" unique="false" otherUnique="false" ignoreStorageClassDataType="gecos.types.boolean" ignoreStorageClassUnique="false" ignoreConstDataType="gecos.types.boolean" ignoreConstUnique="false" ignoreVolatileDataType="gecos.types.boolean" ignoreVolatileUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((this == other))\n{\n\treturn true;\n}\nreturn (((ignoreConst || (&lt;%java.lang.Boolean%&gt;.valueOf(this.isConstant()) == &lt;%java.lang.Boolean%&gt;.valueOf(other.isConstant()))) &amp;&amp; (ignoreVolatile || (&lt;%java.lang.Boolean%&gt;.valueOf(this.isVolatile()) == &lt;%java.lang.Boolean%&gt;.valueOf(other.isVolatile())))) &amp;&amp; (ignoreStorageClass || this.getStorageClass().equals(other.getStorageClass())));'"
	 * @generated
	 */
	boolean isEqual(Type other, boolean ignoreStorageClass, boolean ignoreConst, boolean ignoreVolatile);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.boolean" unique="false" otherUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.isSimilar(other, false, false, false);'"
	 * @generated
	 */
	boolean isSimilar(Type other);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="gecos.types.boolean" unique="false" otherUnique="false" ignoreStorageClassDataType="gecos.types.boolean" ignoreStorageClassUnique="false" ignoreConstDataType="gecos.types.boolean" ignoreConstUnique="false" ignoreVolatileDataType="gecos.types.boolean" ignoreVolatileUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _equals = &lt;%com.google.common.base.Objects%&gt;.equal(other, this);\nif (_equals)\n{\n\treturn true;\n}\n&lt;%gecos.types.AliasType%&gt; _asAlias = other.asAlias();\nboolean _tripleNotEquals = (_asAlias != null);\nif (_tripleNotEquals)\n{\n\treturn other.isSimilar(this, ignoreStorageClass, ignoreConst, ignoreVolatile);\n}\nelse\n{\n\treturn this.isEqual(other, ignoreStorageClass, ignoreConst, ignoreVolatile);\n}'"
	 * @generated
	 */
	boolean isSimilar(Type other, boolean ignoreStorageClass, boolean ignoreConst, boolean ignoreVolatile);

} // Type
