/**
 */
package gecos.dag.impl;

import gecos.dag.DAGArrayNode;
import gecos.dag.DAGNode;
import gecos.dag.DagPackage;
import gecos.dag.DagVisitor;

import gecos.types.Type;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DAG Array Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DAGArrayNodeImpl extends DAGNodeImpl implements DAGArrayNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DAGArrayNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DagPackage.Literals.DAG_ARRAY_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final DagVisitor visitor) {
		visitor.visitDAGArrayNode(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGNode getIndex() {
		return this.getPredecessors().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGNode getDest() {
		return this.getPredecessors().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		int _ident = this.getIdent();
		String _plus = ("ArrayNode_" + Integer.valueOf(_ident));
		String _plus_1 = (_plus + ":");
		Type _type = this.getType();
		return (_plus_1 + _type);
	}

} //DAGArrayNodeImpl
