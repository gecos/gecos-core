#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>


#ifndef __TYPE__
	#define __TYPE__ float //XXX
#endif


#include "tools.h"


int main(int argc, const char **argv) {
	if(argc < 3) {
		fprintf(stderr, "Usage: %s file1 file2 <displayDiff?(0/1)>\n", argv[0]);
		return -1;
	}

	int display_diff = 0;
	if(argc > 3)
		display_diff = atoi(argv[3]);

	int M, M2;
	__TYPE__ *A1;
	__TYPE__ *A2;
	initFromFile1D(&A1, PRINT_ID(__TYPE__), &M, argv[1]);
	initFromFile1D(&A2, PRINT_ID(__TYPE__), &M2, argv[2]);
	if(M != M2) {
		fprintf(stderr, "Matrix sizes does not match!\n");
		free(A1);
		free(A2);
		exit(-1);
	}

//	print(stdout, A1, "A1", M, N);
//	print(stdout, A2, "A2", M, N);

	M = M/2;
	__TYPE__ *A1I = A1 + M;
	__TYPE__ *A2I = A2 + M ;

	double sumdiff = 0;
	double sumdiff2 = 0;
	double diffmax = 0;
	double diff;
	unsigned int nb_diffs = 0;
	double Isumdiff = 0;
	double Isumdiff2 = 0;
	double Idiffmax = 0;
	double Idiff;
	unsigned int Inb_diffs = 0;
	int i,j;
	for (i = 0; i < M; i++) {
		/* real */
		__TYPE__ a2 = A1[i] - A2[i];
		diff = (double)(a2 >= 0 ? a2 : -a2); //XXX y?

		if(diff > diffmax)
			diffmax = diff;
		if(diff != 0) {
			nb_diffs ++;
			sumdiff += diff;
			sumdiff2 += diff * diff;
		}

		/* imag */
		__TYPE__ a2I = A1I[i] - A2I[i];
		Idiff = (double)(a2I >= 0 ? a2I : -a2I); //XXX y?

		if(Idiff > Idiffmax)
			Idiffmax = Idiff;
		if(Idiff != 0) {
			Inb_diffs ++;
			Isumdiff += Idiff;
			Isumdiff2 += Idiff * Idiff;
		}

//		if (display_diff != 0)
//			printf("(%d, %d): %f+i%f = %f+i%f - %f+i%f\n", i, j, a2,a2I, A1[i],A1I[i], A2[i],A2I[i]);
	}

	double nelem = M;
	double mean = sumdiff / (double)nb_diffs;//XXX
	double deviation = sqrt((sumdiff2/(double)nb_diffs) - (mean*mean)); //XXX

	double Imean = Isumdiff / (double)Inb_diffs;//XXX
	double Ideviation = sqrt((Isumdiff2/(double)Inb_diffs) - (Imean*Imean)); //XXX

	/*
	 * mean = sum(diff)/nelem
	 * variance = sum((diff - mean)^2) / nelem = sum(diff^2/nelem) + sum(mean^2/nelem) -2.mean.sum(diff/nelem)
	 * 			= sumdiff2/nelem + mean^2 -2.mean^2 = sumdiff2/nelem - mean^2
	 * deviation = sqrt(variance)
	 * Noise Power (dB) = 10.log10(deviation^2 + mean^2) = 10.log10(variance + mean^2) = 10.log10(sumdiff2/nelem)
	 */
	double meanT = sumdiff / nelem; //mean
	double variance = (sumdiff2/nelem) - (meanT*meanT); //variance
	double deviationT = sqrt(variance); //deviation
	double noisePower = 10 * log10(sumdiff2/nelem); // = 10 * log10(deviationT^2 + meanT^2)

	double ImeanT = Isumdiff / nelem; //mean
	double Ivariance = (Isumdiff2/nelem) - (ImeanT*ImeanT); //variance
	double IdeviationT = sqrt(Ivariance); //deviation
	double InoisePower = 10 * log10(Isumdiff2/nelem); // = 10 * log10(deviationT^2 + meanT^2)

	printf("\tNbElem     = %d Complex\n", M);
	printf("\tRATIO     = %.2f%%  (%d/%d)\n", (double)(nb_diffs*100)/nelem, nb_diffs, (int)nelem);
	printf("\tMAX  diff = %f, i%f\n", diffmax, Idiffmax);
	printf("\tSUM  diff = %f, i%f\n", sumdiff, Isumdiff);
//	printf("\tSUM  dif2 = %f\n", sumdiff2);
	printf("\tMEAN      = %f (%f), i%f (%f)\n", meanT, mean, ImeanT, Imean);
	printf("\tVARIANCE  = %f, i%f\n", variance);
	printf("\tDEVIATION = %f (%f), i%f (%f)\n", deviationT, deviation, IdeviationT, Ideviation);
	printf("\tNOISE POW = %f, i%f dB\n", noisePower, InoisePower);

	free(A1);
	free(A2);

	return 0;
}
