/**
 */
package gecos.dag.impl;

import gecos.dag.DAGNode;
import gecos.dag.DAGSimpleArrayNode;
import gecos.dag.DagPackage;
import gecos.dag.DagVisitor;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DAG Simple Array Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DAGSimpleArrayNodeImpl extends DAGSymbolNodeImpl implements DAGSimpleArrayNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DAGSimpleArrayNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DagPackage.Literals.DAG_SIMPLE_ARRAY_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final DagVisitor visitor) {
		visitor.visitDAGSimpleArrayNode(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DAGNode getIndex() {
		return this.getPredecessors().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		int _ident = this.getIdent();
		return ("SAN_" + Integer.valueOf(_ident));
	}

} //DAGSimpleArrayNodeImpl
