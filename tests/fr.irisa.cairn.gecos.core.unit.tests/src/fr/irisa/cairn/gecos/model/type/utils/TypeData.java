package fr.irisa.cairn.gecos.model.type.utils;

import gecos.types.Type;

/**
 * This class represents a simple declaration.
 * 
 * @author aelmouss
 */
public class TypeData {
	
	/**
	 * declaration = [STOR and/or SPEC and/or QUAL]+  DECL
	 * 
	 *    STOR = [extern | static | auto | register | typedef]
	 *    SPEC = [void | char | short | int | long | float | double | signed | unsigned | ...(union, struct ...) ]
	 *    QUAL = [const | volatile]
	 *    
	 *    DECL = [POINTER] identifier | (DECL) [ '['[const-expr]']' | '('[params | identifiers]')' ]*
	 */
	private Specifier specifiers; 
	private Declarator declarator;
	private Type type;
	
	
	
	/**
	 * @param declarationSpecifiers the C code representing the declaration specifiers 
	 * that correspond to {@code type}.
	 * @param type the Gecos {@link Type}
	 * @param declarator the C code representing the declarators that correspond to {@code type}.
	 */
	public TypeData(Specifier declarationSpecifiers, Type type, Declarator declarator) {
		this.specifiers = declarationSpecifiers;
		this.declarator = declarator;
		this.type = type;
	}
	
	public Specifier getSpecifiers() {
		return specifiers;
	}
	
	public Declarator getDeclarator() {
		return declarator;
	}

	/**
	 * @return {@link #getSpecifiers()} + " " + {@link #getDeclarator()} + ";"
	 */
	public String getCCode() {
		String code = specifiers.getSpec() + " " + declarator.getCCode();
		if(!code.endsWith(";"))
			code += ";";
		return code;
	}

	public Type getType() {
		return type;
	}
	
	@Override
	public String toString() {
		return getCCode();
	}
	
}