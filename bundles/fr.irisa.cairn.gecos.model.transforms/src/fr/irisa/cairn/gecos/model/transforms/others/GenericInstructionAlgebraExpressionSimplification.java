package fr.irisa.cairn.gecos.model.transforms.others;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.transforms.tools.AbstractInstructionTransformation;
import fr.irisa.cairn.gecos.model.transforms.tools.GenericInstructionTransformation;
import gecos.blocks.Block;
import gecos.core.Procedure;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.dag.DAGOperator;
import gecos.dag.DependencyType;
import gecos.instrs.AddressInstruction;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.BranchType;
import gecos.instrs.ComparisonOperator;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.IndirInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.LogicalOperator;
import gecos.instrs.ReductionOperator;
import gecos.instrs.SelectOperator;
import gecos.types.Field;
import gecos.types.FloatPrecisions;
import gecos.types.IntegerTypes;
import gecos.types.Kinds;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import tom.library.sl.VisitFailure;


@SuppressWarnings({"all"})
public class GenericInstructionAlgebraExpressionSimplification extends GenericInstructionTransformation {


private static boolean tom_equal_term_Strategy(Object t1, Object t2) {
return  (t1.equals(t2)) ;
}
private static boolean tom_is_sort_Strategy(Object t) {
return  (t instanceof tom.library.sl.Strategy) ;
}
private static boolean tom_equal_term_Position(Object t1, Object t2) {
return  (t1.equals(t2)) ;
}
private static boolean tom_is_sort_Position(Object t) {
return  (t instanceof tom.library.sl.Position) ;
}
private static boolean tom_equal_term_int(int t1, int t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_int(int t) {
return  true ;
}
private static boolean tom_equal_term_char(char t1, char t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_char(char t) {
return  true ;
}
private static boolean tom_equal_term_String(String t1, String t2) {
return  t1.equals(t2) ;
}
private static boolean tom_is_sort_String(String t) {
return  t instanceof String ;
}
private static  tom.library.sl.Strategy  tom_make_mu( tom.library.sl.Strategy  var,  tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.Mu(var,v) );
}
private static  tom.library.sl.Strategy  tom_make_MuVar( String  name) { 
return ( new tom.library.sl.MuVar(name) );
}
private static  tom.library.sl.Strategy  tom_make_Identity() { 
return ( new tom.library.sl.Identity() );
}
private static  tom.library.sl.Strategy  tom_make_One( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.One(v) );
}
private static  tom.library.sl.Strategy  tom_make_All( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.All(v) );
}
private static  tom.library.sl.Strategy  tom_make_Fail() { 
return ( new tom.library.sl.Fail() );
}
private static boolean tom_is_fun_sym_Sequence( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.Sequence );
}
private static  tom.library.sl.Strategy  tom_empty_list_Sequence() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_Sequence( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.Sequence.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Sequence.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Sequence.THEN) );
}
private static boolean tom_is_empty_Sequence_Strategy( tom.library.sl.Strategy  t) {
return ( t == null );
}

  private static   tom.library.sl.Strategy  tom_append_list_Sequence( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 == null )) {
      return l2;
    } else if(( l2 == null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.Sequence )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.THEN) ) == null )) {
        return  tom.library.sl.Sequence.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.Sequence.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.FIRST) ),tom_append_list_Sequence(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Sequence.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.Sequence.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_Sequence( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end == null ) ||  (end.equals(tom_empty_list_Sequence())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.Sequence.make(((( begin instanceof tom.library.sl.Sequence ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Sequence.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_Sequence(((( begin instanceof tom.library.sl.Sequence ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Sequence.THEN) ):tom_empty_list_Sequence()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_Choice( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.Choice );
}
private static  tom.library.sl.Strategy  tom_empty_list_Choice() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_Choice( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.Choice.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Choice.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.Choice.THEN) );
}
private static boolean tom_is_empty_Choice_Strategy( tom.library.sl.Strategy  t) {
return ( t ==null );
}

  private static   tom.library.sl.Strategy  tom_append_list_Choice( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 ==null )) {
      return l2;
    } else if(( l2 ==null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.Choice )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.THEN) ) ==null )) {
        return  tom.library.sl.Choice.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.Choice.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.FIRST) ),tom_append_list_Choice(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.Choice.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.Choice.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_Choice( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end ==null ) ||  (end.equals(tom_empty_list_Choice())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.Choice.make(((( begin instanceof tom.library.sl.Choice ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Choice.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_Choice(((( begin instanceof tom.library.sl.Choice ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.Choice.THEN) ):tom_empty_list_Choice()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_SequenceId( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.SequenceId );
}
private static  tom.library.sl.Strategy  tom_empty_list_SequenceId() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_SequenceId( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.SequenceId.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.SequenceId.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.SequenceId.THEN) );
}
private static boolean tom_is_empty_SequenceId_Strategy( tom.library.sl.Strategy  t) {
return ( t == null );
}

  private static   tom.library.sl.Strategy  tom_append_list_SequenceId( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 == null )) {
      return l2;
    } else if(( l2 == null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.SequenceId )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.THEN) ) == null )) {
        return  tom.library.sl.SequenceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.SequenceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.FIRST) ),tom_append_list_SequenceId(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.SequenceId.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.SequenceId.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_SequenceId( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end == null ) ||  (end.equals(tom_empty_list_SequenceId())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.SequenceId.make(((( begin instanceof tom.library.sl.SequenceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.SequenceId.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_SequenceId(((( begin instanceof tom.library.sl.SequenceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.SequenceId.THEN) ):tom_empty_list_SequenceId()),end,tail)) ;
  }
  private static boolean tom_is_fun_sym_ChoiceId( tom.library.sl.Strategy  t) {
return ( t instanceof tom.library.sl.ChoiceId );
}
private static  tom.library.sl.Strategy  tom_empty_list_ChoiceId() { 
return  null ;
}
private static  tom.library.sl.Strategy  tom_cons_list_ChoiceId( tom.library.sl.Strategy  head,  tom.library.sl.Strategy  tail) { 
return  tom.library.sl.ChoiceId.make(head,tail) ;
}
private static  tom.library.sl.Strategy  tom_get_head_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.ChoiceId.FIRST) );
}
private static  tom.library.sl.Strategy  tom_get_tail_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( (tom.library.sl.Strategy)t.getChildAt(tom.library.sl.ChoiceId.THEN) );
}
private static boolean tom_is_empty_ChoiceId_Strategy( tom.library.sl.Strategy  t) {
return ( t ==null );
}

  private static   tom.library.sl.Strategy  tom_append_list_ChoiceId( tom.library.sl.Strategy  l1,  tom.library.sl.Strategy  l2) {
    if(( l1 ==null )) {
      return l2;
    } else if(( l2 ==null )) {
      return l1;
    } else if(( l1 instanceof tom.library.sl.ChoiceId )) {
      if(( ( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.THEN) ) ==null )) {
        return  tom.library.sl.ChoiceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.FIRST) ),l2) ;
      } else {
        return  tom.library.sl.ChoiceId.make(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.FIRST) ),tom_append_list_ChoiceId(( (tom.library.sl.Strategy)l1.getChildAt(tom.library.sl.ChoiceId.THEN) ),l2)) ;
      }
    } else {
      return  tom.library.sl.ChoiceId.make(l1,l2) ;
    }
  }
  private static   tom.library.sl.Strategy  tom_get_slice_ChoiceId( tom.library.sl.Strategy  begin,  tom.library.sl.Strategy  end, tom.library.sl.Strategy  tail) {
    if( (begin.equals(end)) ) {
      return tail;
    } else if( (end.equals(tail))  && (( end ==null ) ||  (end.equals(tom_empty_list_ChoiceId())) )) {
      /* code to avoid a call to make, and thus to avoid looping during list-matching */
      return begin;
    }
    return  tom.library.sl.ChoiceId.make(((( begin instanceof tom.library.sl.ChoiceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.ChoiceId.FIRST) ):begin),( tom.library.sl.Strategy )tom_get_slice_ChoiceId(((( begin instanceof tom.library.sl.ChoiceId ))?( (tom.library.sl.Strategy)begin.getChildAt(tom.library.sl.ChoiceId.THEN) ):tom_empty_list_ChoiceId()),end,tail)) ;
  }
  private static  tom.library.sl.Strategy  tom_make_OneId( tom.library.sl.Strategy  v) { 
return ( new tom.library.sl.OneId(v) );
}
private static  tom.library.sl.Strategy  tom_make_AllSeq( tom.library.sl.Strategy  s) { 
return ( new tom.library.sl.AllSeq(s) );
}
private static  tom.library.sl.Strategy  tom_make_AUCtl( tom.library.sl.Strategy  s1,  tom.library.sl.Strategy  s2) { 
return ( 
tom_make_mu(tom_make_MuVar("x"),tom_cons_list_Choice(s2,tom_cons_list_Choice(tom_cons_list_Sequence(tom_cons_list_Sequence(s1,tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("x")),tom_empty_list_Sequence())),tom_cons_list_Sequence(tom_make_One(tom_make_Identity()),tom_empty_list_Sequence())),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_EUCtl( tom.library.sl.Strategy  s1,  tom.library.sl.Strategy  s2) { 
return ( 
tom_make_mu(tom_make_MuVar("x"),tom_cons_list_Choice(s2,tom_cons_list_Choice(tom_cons_list_Sequence(s1,tom_cons_list_Sequence(tom_make_One(tom_make_MuVar("x")),tom_empty_list_Sequence())),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_Try( tom.library.sl.Strategy  s) { 
return ( 
tom_cons_list_Choice(s,tom_cons_list_Choice(tom_make_Identity(),tom_empty_list_Choice())))
;
}
private static  tom.library.sl.Strategy  tom_make_Repeat( tom.library.sl.Strategy  s) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Choice(tom_cons_list_Sequence(s,tom_cons_list_Sequence(tom_make_MuVar("_x"),tom_empty_list_Sequence())),tom_cons_list_Choice(tom_make_Identity(),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_TopDown( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Sequence(v,tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("_x")),tom_empty_list_Sequence()))))
;
}
private static  tom.library.sl.Strategy  tom_make_BottomUp( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Sequence(tom_make_All(tom_make_MuVar("_x")),tom_cons_list_Sequence(v,tom_empty_list_Sequence()))))
;
}
private static  tom.library.sl.Strategy  tom_make_OnceTopDown( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_Choice(v,tom_cons_list_Choice(tom_make_One(tom_make_MuVar("_x")),tom_empty_list_Choice()))))
;
}
private static  tom.library.sl.Strategy  tom_make_RepeatId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_SequenceId(v,tom_cons_list_SequenceId(tom_make_MuVar("_x"),tom_empty_list_SequenceId()))))
;
}
private static  tom.library.sl.Strategy  tom_make_OnceTopDownId( tom.library.sl.Strategy  v) { 
return ( 
tom_make_mu(tom_make_MuVar("_x"),tom_cons_list_ChoiceId(v,tom_cons_list_ChoiceId(tom_make_OneId(tom_make_MuVar("_x")),tom_empty_list_ChoiceId()))))
;
}
private static boolean tom_equal_term_long(long t1, long t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_long(long t) {
return  true ;
}
private static boolean tom_equal_term_double(double t1, double t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_double(double t) {
return  true ;
}
private static boolean tom_equal_term_boolean(boolean t1, boolean t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_boolean(boolean t) {
return  true ;
}
private static boolean tom_equal_term_float(float t1, float t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_float(float t) {
return  true ;
}


private static <O> EList<O> enforce(EList l) {
return l;
}

private static <O> EList<O> append(O e,EList<O> l) {
l.add(e);
return l;
}
private static boolean tom_equal_term_EELong(Object l1, Object l2) {
return l1.equals(l2);
}
private static boolean tom_is_sort_EELong(Object t) {
return t instanceof java.lang.Long;
}
private static boolean tom_equal_term_BlockCopyManager(Object l1, Object l2) {
return l1.equals(l2);
}
private static boolean tom_is_sort_BlockCopyManager(Object t) {
return t instanceof fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;
}
private static boolean tom_equal_term_DependencyType(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_DependencyType(Object t) {
return t instanceof DependencyType;
}
private static boolean tom_equal_term_DAGOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_DAGOperator(Object t) {
return t instanceof DAGOperator;
}
private static boolean tom_equal_term_ArithmeticOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ArithmeticOperator(Object t) {
return t instanceof ArithmeticOperator;
}
private static boolean tom_equal_term_ComparisonOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ComparisonOperator(Object t) {
return t instanceof ComparisonOperator;
}
private static boolean tom_equal_term_LogicalOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_LogicalOperator(Object t) {
return t instanceof LogicalOperator;
}
private static boolean tom_equal_term_BitwiseOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_BitwiseOperator(Object t) {
return t instanceof BitwiseOperator;
}
private static boolean tom_equal_term_SelectOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_SelectOperator(Object t) {
return t instanceof SelectOperator;
}
private static boolean tom_equal_term_ReductionOperator(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_ReductionOperator(Object t) {
return t instanceof ReductionOperator;
}
private static boolean tom_equal_term_BranchType(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_BranchType(Object t) {
return t instanceof BranchType;
}
private static boolean tom_equal_term_StorageClassSpecifiers(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_StorageClassSpecifiers(Object t) {
return t instanceof StorageClassSpecifiers;
}
private static boolean tom_equal_term_Kinds(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_Kinds(Object t) {
return t instanceof Kinds;
}
private static boolean tom_equal_term_IntegerTypes(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_IntegerTypes(Object t) {
return t instanceof IntegerTypes;
}
private static boolean tom_equal_term_SignModifiers(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_SignModifiers(Object t) {
return t instanceof SignModifiers;
}
private static boolean tom_equal_term_FloatPrecisions(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_FloatPrecisions(Object t) {
return t instanceof FloatPrecisions;
}
private static boolean tom_equal_term_OverflowMode(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_OverflowMode(Object t) {
return t instanceof OverflowMode;
}
private static boolean tom_equal_term_QuantificationMode(Object l1, Object l2) {
return l1==l2;
}
private static boolean tom_is_sort_QuantificationMode(Object t) {
return t instanceof QuantificationMode;
}
private static boolean tom_equal_term_Inst(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Inst(Object t) {
return t instanceof Instruction;
}
private static boolean tom_equal_term_Blk(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Blk(Object t) {
return t instanceof Block;
}
private static boolean tom_equal_term_Sym(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Sym(Object t) {
return t instanceof Symbol;
}
private static boolean tom_equal_term_SymL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_SymL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Symbol>)t).size() == 0 
    	|| (((EList<Symbol>)t).size()>0 && ((EList<Symbol>)t).get(0) instanceof Symbol));
}
private static boolean tom_equal_term_Type(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Type(Object t) {
return t instanceof Type;
}
private static boolean tom_equal_term_TypeL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_TypeL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Type>)t).size() == 0 
    	|| (((EList<Type>)t).size()>0 && ((EList<Type>)t).get(0) instanceof Type));
}
private static boolean tom_equal_term_Field(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Field(Object t) {
return t instanceof Field;
}
private static boolean tom_equal_term_FieldL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_FieldL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Field>)t).size() == 0 
    	|| (((EList<Field>)t).size()>0 && ((EList<Field>)t).get(0) instanceof Field));
}
private static boolean tom_equal_term_InstL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_InstL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Instruction>)t).size() == 0 
    	|| (((EList<Instruction>)t).size()>0 && ((EList<Instruction>)t).get(0) instanceof Instruction));
}
private static boolean tom_is_fun_sym_InstL( EList<Instruction>  t) {
return  t instanceof EList<?> &&
 		(((EList<Instruction>)t).size() == 0 
 		|| (((EList<Instruction>)t).size()>0 && ((EList<Instruction>)t).get(0) instanceof Instruction));
}
private static  EList<Instruction>  tom_empty_array_InstL(int n) { 
return  new BasicEList<Instruction>(n) ;
}
private static  EList<Instruction>  tom_cons_array_InstL(Instruction e,  EList<Instruction>  l) { 
return  append(e,l) ;
}
private static Instruction tom_get_element_InstL_InstL( EList<Instruction>  l, int n) {
return  l.get(n) ;
}
private static int tom_get_size_InstL_InstL( EList<Instruction>  l) {
return  l.size() ;
}

  private static   EList<Instruction>  tom_get_slice_InstL( EList<Instruction>  subject, int begin, int end) {
     EList<Instruction>  result =  new BasicEList<Instruction>(end-begin) ;
    while(begin!=end) {
      result =  append( subject.get(begin) ,result) ;
      begin++;
    }
    return result;
  }

  private static   EList<Instruction>  tom_append_array_InstL( EList<Instruction>  l2,  EList<Instruction>  l1) {
    int size1 =  l1.size() ;
    int size2 =  l2.size() ;
    int index;
     EList<Instruction>  result =  new BasicEList<Instruction>(size1+size2) ;
    index=size1;
    while(index >0) {
      result =  append( l1.get(size1-index) ,result) ;
      index--;
    }

    index=size2;
    while(index > 0) {
      result =  append( l2.get(size2-index) ,result) ;
      index--;
    }
    return result;
  }private static boolean tom_equal_term_BlkL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_BlkL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<Block>)t).size() == 0 
    	|| (((EList<Block>)t).size()>0 && ((EList<Block>)t).get(0) instanceof Block));
}
private static boolean tom_equal_term_Node(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_Node(Object t) {
return t instanceof DAGNode;
}
private static boolean tom_equal_term_NodeL(Object l1, Object l2) {
return (l1!=null && l1.equals(l2)) || l1==l2;
}
private static boolean tom_is_sort_NodeL(Object t) {
return  t instanceof EList<?> &&
    	(((EList<DAGNode>)t).size() == 0 
    	|| (((EList<DAGNode>)t).size()>0 && ((EList<DAGNode>)t).get(0) instanceof DAGNode));
}
private static boolean tom_is_fun_sym_ival(Instruction t) {
return t instanceof IntInstruction;
}
private static Instruction tom_make_ival( long  _value) { 
return fr.irisa.cairn.gecos.model.tom.gecos.internal.GecosTomFactory.createIval(_value);
}
private static  long  tom_get_slot_ival_value(Instruction t) {
return ((IntInstruction)t).getValue();
}
private static boolean tom_is_fun_sym_fval(Instruction t) {
return t instanceof FloatInstruction;
}
private static Instruction tom_make_fval( double  _value) { 
return fr.irisa.cairn.gecos.model.tom.gecos.internal.GecosTomFactory.createFval(_value);
}
private static  double  tom_get_slot_fval_value(Instruction t) {
return ((FloatInstruction)t).getValue();
}
private static boolean tom_is_fun_sym_address(Instruction t) {
return t instanceof AddressInstruction;
}
private static Instruction tom_get_slot_address_address(Instruction t) {
return ((AddressInstruction)t).getAddress();
}
private static boolean tom_is_fun_sym_indir(Instruction t) {
return t instanceof IndirInstruction;
}
private static Instruction tom_get_slot_indir_address(Instruction t) {
return ((IndirInstruction)t).getAddress();
}
private static boolean tom_is_fun_sym_add(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("add");
}
private static  EList<Instruction>  tom_get_slot_add_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_sub(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("sub");
}
private static  EList<Instruction>  tom_get_slot_sub_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_mul(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("mul");
}
private static  EList<Instruction>  tom_get_slot_mul_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_div(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("div");
}
private static  EList<Instruction>  tom_get_slot_div_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_shr(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("shr");
}
private static  EList<Instruction>  tom_get_slot_shr_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_shl(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("shl");
}
private static  EList<Instruction>  tom_get_slot_shl_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_le(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("le");
}
private static  EList<Instruction>  tom_get_slot_le_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_lt(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("lt");
}
private static  EList<Instruction>  tom_get_slot_lt_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_ge(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("ge");
}
private static  EList<Instruction>  tom_get_slot_ge_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_gt(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("gt");
}
private static  EList<Instruction>  tom_get_slot_gt_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_eq(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("eq");
}
private static  EList<Instruction>  tom_get_slot_eq_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_lor(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("lor");
}
private static  EList<Instruction>  tom_get_slot_lor_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_land(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("land");
}
private static  EList<Instruction>  tom_get_slot_land_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_and(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("and");
}
private static  EList<Instruction>  tom_get_slot_and_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_or(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("or");
}
private static  EList<Instruction>  tom_get_slot_or_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_xor(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("xor");
}
private static  EList<Instruction>  tom_get_slot_xor_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}

private static boolean tom_is_fun_sym_sum(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("sum");
}
private static Instruction tom_make_sum( EList<Instruction>  _children) { 
return fr.irisa.cairn.gecos.model.tom.gecos.internal.GecosTomFactory.createSum(_children);
}
private static  EList<Instruction>  tom_get_slot_sum_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}
private static boolean tom_is_fun_sym_mux(Instruction t) {
return t instanceof GenericInstruction && ((GenericInstruction)t).getName().equals("mux");
}
private static  EList<Instruction>  tom_get_slot_mux_children(Instruction t) {
return enforce(((GenericInstruction)t).getChildren());
}



protected Instruction apply(Instruction instruction) throws VisitFailure{
Instruction adapter = 
tom_make_BottomUp(tom_make_Simplify()).visitLight(instruction, tom.mapping.GenericIntrospector.INSTANCE);

return adapter; 
}


public static class Simplify extends tom.library.sl.AbstractStrategyBasic {
public Simplify() {
super(tom_make_Identity());
}
public tom.library.sl.Visitable[] getChildren() {
tom.library.sl.Visitable[] stratChildren = new tom.library.sl.Visitable[getChildCount()];
stratChildren[0] = super.getChildAt(0);
return stratChildren;}
public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
super.setChildAt(0, children[0]);
return this;
}
public int getChildCount() {
return 1;
}
public tom.library.sl.Visitable getChildAt(int index) {
switch (index) {
case 0: return super.getChildAt(0);
default: throw new IndexOutOfBoundsException();
}
}
public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
switch (index) {
case 0: return super.setChildAt(0, child);
default: throw new IndexOutOfBoundsException();
}
}
@SuppressWarnings("unchecked")
public <T> T visitLight(T v, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
if (tom_is_sort_Inst(v)) {
return ((T)visit_Inst(((Instruction)v),introspector));
}
if (!(( null  == environment))) {
return ((T)any.visit(environment,introspector));
} else {
return any.visitLight(v,introspector);
}
}
@SuppressWarnings("unchecked")
public Instruction _visit_Inst(Instruction arg, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
if (!(( null  == environment))) {
return ((Instruction)any.visit(environment,introspector));
} else {
return any.visitLight(arg,introspector);
}
}
@SuppressWarnings("unchecked")
public Instruction visit_Inst(Instruction tom__arg, tom.library.sl.Introspector introspector)
 throws tom.library.sl.VisitFailure {
{
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_add(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_1=tom_get_slot_add_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_1))) {
int tomMatch1_5=0;
if (!(tomMatch1_5 >= tom_get_size_InstL_InstL(tomMatch1_1))) {
Instruction tomMatch1_9=tom_get_element_InstL_InstL(tomMatch1_1,tomMatch1_5);
if (tom_is_sort_Inst(tomMatch1_9)) {
if (tom_is_fun_sym_fval(((Instruction)tomMatch1_9))) {
int tomMatch1_6=tomMatch1_5 + 1;
if (!(tomMatch1_6 >= tom_get_size_InstL_InstL(tomMatch1_1))) {
Instruction tomMatch1_12=tom_get_element_InstL_InstL(tomMatch1_1,tomMatch1_6);
if (tom_is_sort_Inst(tomMatch1_12)) {
if (tom_is_fun_sym_fval(((Instruction)tomMatch1_12))) {
if (tomMatch1_6 + 1 >= tom_get_size_InstL_InstL(tomMatch1_1)) {
return 
tom_make_fval(tom_get_slot_fval_value(tomMatch1_9)+(tom_get_slot_fval_value(tomMatch1_12))); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_sub(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_15=tom_get_slot_sub_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_15))) {
int tomMatch1_19=0;
if (!(tomMatch1_19 >= tom_get_size_InstL_InstL(tomMatch1_15))) {
Instruction tomMatch1_23=tom_get_element_InstL_InstL(tomMatch1_15,tomMatch1_19);
if (tom_is_sort_Inst(tomMatch1_23)) {
if (tom_is_fun_sym_fval(((Instruction)tomMatch1_23))) {
int tomMatch1_20=tomMatch1_19 + 1;
if (!(tomMatch1_20 >= tom_get_size_InstL_InstL(tomMatch1_15))) {
Instruction tomMatch1_26=tom_get_element_InstL_InstL(tomMatch1_15,tomMatch1_20);
if (tom_is_sort_Inst(tomMatch1_26)) {
if (tom_is_fun_sym_fval(((Instruction)tomMatch1_26))) {
if (tomMatch1_20 + 1 >= tom_get_size_InstL_InstL(tomMatch1_15)) {
return 
tom_make_fval(tom_get_slot_fval_value(tomMatch1_23)-(tom_get_slot_fval_value(tomMatch1_26))); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_mul(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_29=tom_get_slot_mul_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_29))) {
int tomMatch1_33=0;
if (!(tomMatch1_33 >= tom_get_size_InstL_InstL(tomMatch1_29))) {
Instruction tomMatch1_37=tom_get_element_InstL_InstL(tomMatch1_29,tomMatch1_33);
if (tom_is_sort_Inst(tomMatch1_37)) {
if (tom_is_fun_sym_fval(((Instruction)tomMatch1_37))) {
int tomMatch1_34=tomMatch1_33 + 1;
if (!(tomMatch1_34 >= tom_get_size_InstL_InstL(tomMatch1_29))) {
Instruction tomMatch1_40=tom_get_element_InstL_InstL(tomMatch1_29,tomMatch1_34);
if (tom_is_sort_Inst(tomMatch1_40)) {
if (tom_is_fun_sym_fval(((Instruction)tomMatch1_40))) {
if (tomMatch1_34 + 1 >= tom_get_size_InstL_InstL(tomMatch1_29)) {
return 
tom_make_fval((tom_get_slot_fval_value(tomMatch1_37))*(tom_get_slot_fval_value(tomMatch1_40))); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_div(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_43=tom_get_slot_div_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_43))) {
int tomMatch1_47=0;
if (!(tomMatch1_47 >= tom_get_size_InstL_InstL(tomMatch1_43))) {
Instruction tomMatch1_51=tom_get_element_InstL_InstL(tomMatch1_43,tomMatch1_47);
if (tom_is_sort_Inst(tomMatch1_51)) {
if (tom_is_fun_sym_fval(((Instruction)tomMatch1_51))) {
int tomMatch1_48=tomMatch1_47 + 1;
if (!(tomMatch1_48 >= tom_get_size_InstL_InstL(tomMatch1_43))) {
Instruction tomMatch1_54=tom_get_element_InstL_InstL(tomMatch1_43,tomMatch1_48);
if (tom_is_sort_Inst(tomMatch1_54)) {
if (tom_is_fun_sym_fval(((Instruction)tomMatch1_54))) {
if (tomMatch1_48 + 1 >= tom_get_size_InstL_InstL(tomMatch1_43)) {
return 
tom_make_fval(tom_get_slot_fval_value(tomMatch1_51)/(tom_get_slot_fval_value(tomMatch1_54))); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_add(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_57=tom_get_slot_add_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_57))) {
int tomMatch1_61=0;
if (!(tomMatch1_61 >= tom_get_size_InstL_InstL(tomMatch1_57))) {
Instruction tomMatch1_65=tom_get_element_InstL_InstL(tomMatch1_57,tomMatch1_61);
if (tom_is_sort_Inst(tomMatch1_65)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_65))) {
int tomMatch1_62=tomMatch1_61 + 1;
if (!(tomMatch1_62 >= tom_get_size_InstL_InstL(tomMatch1_57))) {
Instruction tomMatch1_68=tom_get_element_InstL_InstL(tomMatch1_57,tomMatch1_62);
if (tom_is_sort_Inst(tomMatch1_68)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_68))) {
if (tomMatch1_62 + 1 >= tom_get_size_InstL_InstL(tomMatch1_57)) {
return 
tom_make_ival(tom_get_slot_ival_value(tomMatch1_65)+(tom_get_slot_ival_value(tomMatch1_68))); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_sub(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_71=tom_get_slot_sub_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_71))) {
int tomMatch1_75=0;
if (!(tomMatch1_75 >= tom_get_size_InstL_InstL(tomMatch1_71))) {
Instruction tomMatch1_79=tom_get_element_InstL_InstL(tomMatch1_71,tomMatch1_75);
if (tom_is_sort_Inst(tomMatch1_79)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_79))) {
int tomMatch1_76=tomMatch1_75 + 1;
if (!(tomMatch1_76 >= tom_get_size_InstL_InstL(tomMatch1_71))) {
Instruction tomMatch1_82=tom_get_element_InstL_InstL(tomMatch1_71,tomMatch1_76);
if (tom_is_sort_Inst(tomMatch1_82)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_82))) {
if (tomMatch1_76 + 1 >= tom_get_size_InstL_InstL(tomMatch1_71)) {
return 
tom_make_ival(tom_get_slot_ival_value(tomMatch1_79)-(tom_get_slot_ival_value(tomMatch1_82))); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_mul(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_85=tom_get_slot_mul_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_85))) {
int tomMatch1_89=0;
if (!(tomMatch1_89 >= tom_get_size_InstL_InstL(tomMatch1_85))) {
Instruction tomMatch1_93=tom_get_element_InstL_InstL(tomMatch1_85,tomMatch1_89);
if (tom_is_sort_Inst(tomMatch1_93)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_93))) {
int tomMatch1_90=tomMatch1_89 + 1;
if (!(tomMatch1_90 >= tom_get_size_InstL_InstL(tomMatch1_85))) {
Instruction tomMatch1_96=tom_get_element_InstL_InstL(tomMatch1_85,tomMatch1_90);
if (tom_is_sort_Inst(tomMatch1_96)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_96))) {
if (tomMatch1_90 + 1 >= tom_get_size_InstL_InstL(tomMatch1_85)) {
return 
tom_make_ival((tom_get_slot_ival_value(tomMatch1_93))*(tom_get_slot_ival_value(tomMatch1_96))); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_div(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_99=tom_get_slot_div_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_99))) {
int tomMatch1_103=0;
if (!(tomMatch1_103 >= tom_get_size_InstL_InstL(tomMatch1_99))) {
Instruction tomMatch1_107=tom_get_element_InstL_InstL(tomMatch1_99,tomMatch1_103);
if (tom_is_sort_Inst(tomMatch1_107)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_107))) {
int tomMatch1_104=tomMatch1_103 + 1;
if (!(tomMatch1_104 >= tom_get_size_InstL_InstL(tomMatch1_99))) {
Instruction tomMatch1_110=tom_get_element_InstL_InstL(tomMatch1_99,tomMatch1_104);
if (tom_is_sort_Inst(tomMatch1_110)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_110))) {
if (tomMatch1_104 + 1 >= tom_get_size_InstL_InstL(tomMatch1_99)) {
return 
tom_make_ival(tom_get_slot_ival_value(tomMatch1_107)/(tom_get_slot_ival_value(tomMatch1_110))); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_shr(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_113=tom_get_slot_shr_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_113))) {
int tomMatch1_117=0;
if (!(tomMatch1_117 >= tom_get_size_InstL_InstL(tomMatch1_113))) {
Instruction tomMatch1_121=tom_get_element_InstL_InstL(tomMatch1_113,tomMatch1_117);
if (tom_is_sort_Inst(tomMatch1_121)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_121))) {
int tomMatch1_118=tomMatch1_117 + 1;
if (!(tomMatch1_118 >= tom_get_size_InstL_InstL(tomMatch1_113))) {
Instruction tomMatch1_124=tom_get_element_InstL_InstL(tomMatch1_113,tomMatch1_118);
if (tom_is_sort_Inst(tomMatch1_124)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_124))) {
if (tomMatch1_118 + 1 >= tom_get_size_InstL_InstL(tomMatch1_113)) {
return 
tom_make_ival((tom_get_slot_ival_value(tomMatch1_121))>>(tom_get_slot_ival_value(tomMatch1_124))); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_shl(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_127=tom_get_slot_shl_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_127))) {
int tomMatch1_131=0;
if (!(tomMatch1_131 >= tom_get_size_InstL_InstL(tomMatch1_127))) {
Instruction tomMatch1_135=tom_get_element_InstL_InstL(tomMatch1_127,tomMatch1_131);
if (tom_is_sort_Inst(tomMatch1_135)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_135))) {
int tomMatch1_132=tomMatch1_131 + 1;
if (!(tomMatch1_132 >= tom_get_size_InstL_InstL(tomMatch1_127))) {
Instruction tomMatch1_138=tom_get_element_InstL_InstL(tomMatch1_127,tomMatch1_132);
if (tom_is_sort_Inst(tomMatch1_138)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_138))) {
if (tomMatch1_132 + 1 >= tom_get_size_InstL_InstL(tomMatch1_127)) {
return 
tom_make_ival(tom_get_slot_ival_value(tomMatch1_135)<<(tom_get_slot_ival_value(tomMatch1_138))); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_add(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_141=tom_get_slot_add_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_141))) {
int tomMatch1_145=0;
if (!(tomMatch1_145 >= tom_get_size_InstL_InstL(tomMatch1_141))) {
Instruction tomMatch1_149=tom_get_element_InstL_InstL(tomMatch1_141,tomMatch1_145);
if (tom_is_sort_Inst(tomMatch1_149)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_149))) {
 long  tomMatch1_148=tom_get_slot_ival_value(tomMatch1_149);
if ( true ) {
if (tom_equal_term_long(0L, tomMatch1_148)) {
int tomMatch1_146=tomMatch1_145 + 1;
if (!(tomMatch1_146 >= tom_get_size_InstL_InstL(tomMatch1_141))) {
if (tomMatch1_146 + 1 >= tom_get_size_InstL_InstL(tomMatch1_141)) {
return 
tom_get_element_InstL_InstL(tomMatch1_141,tomMatch1_146); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_sub(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_154=tom_get_slot_sub_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_154))) {
int tomMatch1_158=0;
if (!(tomMatch1_158 >= tom_get_size_InstL_InstL(tomMatch1_154))) {
int tomMatch1_159=tomMatch1_158 + 1;
if (!(tomMatch1_159 >= tom_get_size_InstL_InstL(tomMatch1_154))) {
Instruction tomMatch1_162=tom_get_element_InstL_InstL(tomMatch1_154,tomMatch1_159);
if (tom_is_sort_Inst(tomMatch1_162)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_162))) {
 long  tomMatch1_161=tom_get_slot_ival_value(tomMatch1_162);
if ( true ) {
if (tom_equal_term_long(0L, tomMatch1_161)) {
if (tomMatch1_159 + 1 >= tom_get_size_InstL_InstL(tomMatch1_154)) {
return 
tom_get_element_InstL_InstL(tomMatch1_154,tomMatch1_158); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_mul(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_167=tom_get_slot_mul_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_167))) {
int tomMatch1_171=0;
if (!(tomMatch1_171 >= tom_get_size_InstL_InstL(tomMatch1_167))) {
Instruction tomMatch1_175=tom_get_element_InstL_InstL(tomMatch1_167,tomMatch1_171);
if (tom_is_sort_Inst(tomMatch1_175)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_175))) {
 long  tomMatch1_174=tom_get_slot_ival_value(tomMatch1_175);
if ( true ) {
if (tom_equal_term_long(1L, tomMatch1_174)) {
int tomMatch1_172=tomMatch1_171 + 1;
if (!(tomMatch1_172 >= tom_get_size_InstL_InstL(tomMatch1_167))) {
if (tomMatch1_172 + 1 >= tom_get_size_InstL_InstL(tomMatch1_167)) {
return 
tom_get_element_InstL_InstL(tomMatch1_167,tomMatch1_172); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_div(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_180=tom_get_slot_div_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_180))) {
int tomMatch1_184=0;
if (!(tomMatch1_184 >= tom_get_size_InstL_InstL(tomMatch1_180))) {
int tomMatch1_185=tomMatch1_184 + 1;
if (!(tomMatch1_185 >= tom_get_size_InstL_InstL(tomMatch1_180))) {
Instruction tomMatch1_188=tom_get_element_InstL_InstL(tomMatch1_180,tomMatch1_185);
if (tom_is_sort_Inst(tomMatch1_188)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_188))) {
 long  tomMatch1_187=tom_get_slot_ival_value(tomMatch1_188);
if ( true ) {
if (tom_equal_term_long(1L, tomMatch1_187)) {
if (tomMatch1_185 + 1 >= tom_get_size_InstL_InstL(tomMatch1_180)) {
return 
tom_get_element_InstL_InstL(tomMatch1_180,tomMatch1_184); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_shr(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_193=tom_get_slot_shr_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_193))) {
int tomMatch1_197=0;
if (!(tomMatch1_197 >= tom_get_size_InstL_InstL(tomMatch1_193))) {
int tomMatch1_198=tomMatch1_197 + 1;
if (!(tomMatch1_198 >= tom_get_size_InstL_InstL(tomMatch1_193))) {
Instruction tomMatch1_201=tom_get_element_InstL_InstL(tomMatch1_193,tomMatch1_198);
if (tom_is_sort_Inst(tomMatch1_201)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_201))) {
 long  tomMatch1_200=tom_get_slot_ival_value(tomMatch1_201);
if ( true ) {
if (tom_equal_term_long(0L, tomMatch1_200)) {
if (tomMatch1_198 + 1 >= tom_get_size_InstL_InstL(tomMatch1_193)) {
return 
tom_get_element_InstL_InstL(tomMatch1_193,tomMatch1_197); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_shl(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_206=tom_get_slot_shl_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_206))) {
int tomMatch1_210=0;
if (!(tomMatch1_210 >= tom_get_size_InstL_InstL(tomMatch1_206))) {
int tomMatch1_211=tomMatch1_210 + 1;
if (!(tomMatch1_211 >= tom_get_size_InstL_InstL(tomMatch1_206))) {
Instruction tomMatch1_214=tom_get_element_InstL_InstL(tomMatch1_206,tomMatch1_211);
if (tom_is_sort_Inst(tomMatch1_214)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_214))) {
 long  tomMatch1_213=tom_get_slot_ival_value(tomMatch1_214);
if ( true ) {
if (tom_equal_term_long(0L, tomMatch1_213)) {
if (tomMatch1_211 + 1 >= tom_get_size_InstL_InstL(tomMatch1_206)) {
return 
tom_get_element_InstL_InstL(tomMatch1_206,tomMatch1_210); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_add(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_219=tom_get_slot_add_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_219))) {
int tomMatch1_223=0;
if (!(tomMatch1_223 >= tom_get_size_InstL_InstL(tomMatch1_219))) {
Instruction tomMatch1_227=tom_get_element_InstL_InstL(tomMatch1_219,tomMatch1_223);
if (tom_is_sort_Inst(tomMatch1_227)) {
if (tom_is_fun_sym_fval(((Instruction)tomMatch1_227))) {
 double  tomMatch1_226=tom_get_slot_fval_value(tomMatch1_227);
if ( true ) {
if (tom_equal_term_double(0.0, tomMatch1_226)) {
int tomMatch1_224=tomMatch1_223 + 1;
if (!(tomMatch1_224 >= tom_get_size_InstL_InstL(tomMatch1_219))) {
if (tomMatch1_224 + 1 >= tom_get_size_InstL_InstL(tomMatch1_219)) {
return 
tom_get_element_InstL_InstL(tomMatch1_219,tomMatch1_224); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_sub(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_232=tom_get_slot_sub_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_232))) {
int tomMatch1_236=0;
if (!(tomMatch1_236 >= tom_get_size_InstL_InstL(tomMatch1_232))) {
int tomMatch1_237=tomMatch1_236 + 1;
if (!(tomMatch1_237 >= tom_get_size_InstL_InstL(tomMatch1_232))) {
Instruction tomMatch1_240=tom_get_element_InstL_InstL(tomMatch1_232,tomMatch1_237);
if (tom_is_sort_Inst(tomMatch1_240)) {
if (tom_is_fun_sym_fval(((Instruction)tomMatch1_240))) {
 double  tomMatch1_239=tom_get_slot_fval_value(tomMatch1_240);
if ( true ) {
if (tom_equal_term_double(0.0, tomMatch1_239)) {
if (tomMatch1_237 + 1 >= tom_get_size_InstL_InstL(tomMatch1_232)) {
return 
tom_get_element_InstL_InstL(tomMatch1_232,tomMatch1_236); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_mul(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_245=tom_get_slot_mul_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_245))) {
int tomMatch1_249=0;
if (!(tomMatch1_249 >= tom_get_size_InstL_InstL(tomMatch1_245))) {
Instruction tomMatch1_253=tom_get_element_InstL_InstL(tomMatch1_245,tomMatch1_249);
if (tom_is_sort_Inst(tomMatch1_253)) {
if (tom_is_fun_sym_fval(((Instruction)tomMatch1_253))) {
 double  tomMatch1_252=tom_get_slot_fval_value(tomMatch1_253);
if ( true ) {
if (tom_equal_term_double(1.0, tomMatch1_252)) {
int tomMatch1_250=tomMatch1_249 + 1;
if (!(tomMatch1_250 >= tom_get_size_InstL_InstL(tomMatch1_245))) {
if (tomMatch1_250 + 1 >= tom_get_size_InstL_InstL(tomMatch1_245)) {
return 
tom_get_element_InstL_InstL(tomMatch1_245,tomMatch1_250); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_div(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_258=tom_get_slot_div_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_258))) {
int tomMatch1_262=0;
if (!(tomMatch1_262 >= tom_get_size_InstL_InstL(tomMatch1_258))) {
int tomMatch1_263=tomMatch1_262 + 1;
if (!(tomMatch1_263 >= tom_get_size_InstL_InstL(tomMatch1_258))) {
Instruction tomMatch1_266=tom_get_element_InstL_InstL(tomMatch1_258,tomMatch1_263);
if (tom_is_sort_Inst(tomMatch1_266)) {
if (tom_is_fun_sym_fval(((Instruction)tomMatch1_266))) {
 double  tomMatch1_265=tom_get_slot_fval_value(tomMatch1_266);
if ( true ) {
if (tom_equal_term_double(1.0, tomMatch1_265)) {
if (tomMatch1_263 + 1 >= tom_get_size_InstL_InstL(tomMatch1_258)) {
return 
tom_get_element_InstL_InstL(tomMatch1_258,tomMatch1_262); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_sum(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_271=tom_get_slot_sum_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_271))) {
int tomMatch1_275=0;
int tomMatch1_end_278=tomMatch1_275;
do {
{
if (!(tomMatch1_end_278 >= tom_get_size_InstL_InstL(tomMatch1_271))) {
Instruction tomMatch1_286=tom_get_element_InstL_InstL(tomMatch1_271,tomMatch1_end_278);
if (tom_is_sort_Inst(tomMatch1_286)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_286))) {
int tomMatch1_279=tomMatch1_end_278 + 1;
int tomMatch1_end_282=tomMatch1_279;
do {
{
if (!(tomMatch1_end_282 >= tom_get_size_InstL_InstL(tomMatch1_271))) {
Instruction tomMatch1_289=tom_get_element_InstL_InstL(tomMatch1_271,tomMatch1_end_282);
if (tom_is_sort_Inst(tomMatch1_289)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_289))) {
return 
tom_make_sum(tom_append_array_InstL(tom_get_slice_InstL(tomMatch1_271,tomMatch1_end_282 + 1,tom_get_size_InstL_InstL(tomMatch1_271)),tom_append_array_InstL(tom_get_slice_InstL(tomMatch1_271,tomMatch1_279,tomMatch1_end_282),tom_append_array_InstL(tom_get_slice_InstL(tomMatch1_271,tomMatch1_275,tomMatch1_end_278),tom_cons_array_InstL(tom_make_ival(tom_get_slot_ival_value(tomMatch1_286)+tom_get_slot_ival_value(tomMatch1_289)),tom_empty_array_InstL(4)))))); 
}
}
}
tomMatch1_end_282=tomMatch1_end_282 + 1;
}
} while(!(tomMatch1_end_282 > tom_get_size_InstL_InstL(tomMatch1_271)));
}
}
}
tomMatch1_end_278=tomMatch1_end_278 + 1;
}
} while(!(tomMatch1_end_278 > tom_get_size_InstL_InstL(tomMatch1_271)));
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_indir(((Instruction)((Instruction)tom__arg)))) {
Instruction tomMatch1_292=tom_get_slot_indir_address(((Instruction)tom__arg));
if (tom_is_sort_Inst(tomMatch1_292)) {
if (tom_is_fun_sym_address(((Instruction)tomMatch1_292))) {
return 
tom_get_slot_address_address(tomMatch1_292); 
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_le(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_299=tom_get_slot_le_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_299))) {
int tomMatch1_303=0;
if (!(tomMatch1_303 >= tom_get_size_InstL_InstL(tomMatch1_299))) {
Instruction tomMatch1_307=tom_get_element_InstL_InstL(tomMatch1_299,tomMatch1_303);
if (tom_is_sort_Inst(tomMatch1_307)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_307))) {
int tomMatch1_304=tomMatch1_303 + 1;
if (!(tomMatch1_304 >= tom_get_size_InstL_InstL(tomMatch1_299))) {
Instruction tomMatch1_310=tom_get_element_InstL_InstL(tomMatch1_299,tomMatch1_304);
if (tom_is_sort_Inst(tomMatch1_310)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_310))) {
if (tomMatch1_304 + 1 >= tom_get_size_InstL_InstL(tomMatch1_299)) {
return 
tom_make_ival(((tom_get_slot_ival_value(tomMatch1_307))<=(tom_get_slot_ival_value(tomMatch1_310)))?0:1); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_lt(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_313=tom_get_slot_lt_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_313))) {
int tomMatch1_317=0;
if (!(tomMatch1_317 >= tom_get_size_InstL_InstL(tomMatch1_313))) {
Instruction tomMatch1_321=tom_get_element_InstL_InstL(tomMatch1_313,tomMatch1_317);
if (tom_is_sort_Inst(tomMatch1_321)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_321))) {
int tomMatch1_318=tomMatch1_317 + 1;
if (!(tomMatch1_318 >= tom_get_size_InstL_InstL(tomMatch1_313))) {
Instruction tomMatch1_324=tom_get_element_InstL_InstL(tomMatch1_313,tomMatch1_318);
if (tom_is_sort_Inst(tomMatch1_324)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_324))) {
if (tomMatch1_318 + 1 >= tom_get_size_InstL_InstL(tomMatch1_313)) {
return 
tom_make_ival(((tom_get_slot_ival_value(tomMatch1_321))<(tom_get_slot_ival_value(tomMatch1_324)))?0:1); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_gt(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_327=tom_get_slot_gt_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_327))) {
int tomMatch1_331=0;
if (!(tomMatch1_331 >= tom_get_size_InstL_InstL(tomMatch1_327))) {
Instruction tomMatch1_335=tom_get_element_InstL_InstL(tomMatch1_327,tomMatch1_331);
if (tom_is_sort_Inst(tomMatch1_335)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_335))) {
int tomMatch1_332=tomMatch1_331 + 1;
if (!(tomMatch1_332 >= tom_get_size_InstL_InstL(tomMatch1_327))) {
Instruction tomMatch1_338=tom_get_element_InstL_InstL(tomMatch1_327,tomMatch1_332);
if (tom_is_sort_Inst(tomMatch1_338)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_338))) {
if (tomMatch1_332 + 1 >= tom_get_size_InstL_InstL(tomMatch1_327)) {
return 
tom_make_ival(((tom_get_slot_ival_value(tomMatch1_335))>(tom_get_slot_ival_value(tomMatch1_338)))?0:1); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_ge(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_341=tom_get_slot_ge_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_341))) {
int tomMatch1_345=0;
if (!(tomMatch1_345 >= tom_get_size_InstL_InstL(tomMatch1_341))) {
Instruction tomMatch1_349=tom_get_element_InstL_InstL(tomMatch1_341,tomMatch1_345);
if (tom_is_sort_Inst(tomMatch1_349)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_349))) {
int tomMatch1_346=tomMatch1_345 + 1;
if (!(tomMatch1_346 >= tom_get_size_InstL_InstL(tomMatch1_341))) {
Instruction tomMatch1_352=tom_get_element_InstL_InstL(tomMatch1_341,tomMatch1_346);
if (tom_is_sort_Inst(tomMatch1_352)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_352))) {
if (tomMatch1_346 + 1 >= tom_get_size_InstL_InstL(tomMatch1_341)) {
return 
tom_make_ival(((tom_get_slot_ival_value(tomMatch1_349))>=(tom_get_slot_ival_value(tomMatch1_352)))?0:1); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_eq(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_355=tom_get_slot_eq_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_355))) {
int tomMatch1_359=0;
if (!(tomMatch1_359 >= tom_get_size_InstL_InstL(tomMatch1_355))) {
Instruction tomMatch1_363=tom_get_element_InstL_InstL(tomMatch1_355,tomMatch1_359);
if (tom_is_sort_Inst(tomMatch1_363)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_363))) {
int tomMatch1_360=tomMatch1_359 + 1;
if (!(tomMatch1_360 >= tom_get_size_InstL_InstL(tomMatch1_355))) {
Instruction tomMatch1_366=tom_get_element_InstL_InstL(tomMatch1_355,tomMatch1_360);
if (tom_is_sort_Inst(tomMatch1_366)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_366))) {
if (tomMatch1_360 + 1 >= tom_get_size_InstL_InstL(tomMatch1_355)) {
return 
tom_make_ival(((tom_get_slot_ival_value(tomMatch1_363))==(tom_get_slot_ival_value(tomMatch1_366)))?0:1); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_land(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_369=tom_get_slot_land_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_369))) {
int tomMatch1_373=0;
if (!(tomMatch1_373 >= tom_get_size_InstL_InstL(tomMatch1_369))) {
Instruction tomMatch1_377=tom_get_element_InstL_InstL(tomMatch1_369,tomMatch1_373);
if (tom_is_sort_Inst(tomMatch1_377)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_377))) {
int tomMatch1_374=tomMatch1_373 + 1;
if (!(tomMatch1_374 >= tom_get_size_InstL_InstL(tomMatch1_369))) {
Instruction tomMatch1_380=tom_get_element_InstL_InstL(tomMatch1_369,tomMatch1_374);
if (tom_is_sort_Inst(tomMatch1_380)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_380))) {
if (tomMatch1_374 + 1 >= tom_get_size_InstL_InstL(tomMatch1_369)) {
return 
tom_make_ival(((tom_get_slot_ival_value(tomMatch1_377)!=0)&&(tom_get_slot_ival_value(tomMatch1_380)!=0))?0:1); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_lor(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_383=tom_get_slot_lor_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_383))) {
int tomMatch1_387=0;
if (!(tomMatch1_387 >= tom_get_size_InstL_InstL(tomMatch1_383))) {
Instruction tomMatch1_391=tom_get_element_InstL_InstL(tomMatch1_383,tomMatch1_387);
if (tom_is_sort_Inst(tomMatch1_391)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_391))) {
int tomMatch1_388=tomMatch1_387 + 1;
if (!(tomMatch1_388 >= tom_get_size_InstL_InstL(tomMatch1_383))) {
Instruction tomMatch1_394=tom_get_element_InstL_InstL(tomMatch1_383,tomMatch1_388);
if (tom_is_sort_Inst(tomMatch1_394)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_394))) {
if (tomMatch1_388 + 1 >= tom_get_size_InstL_InstL(tomMatch1_383)) {
return 
tom_make_ival(((tom_get_slot_ival_value(tomMatch1_391)!=0)||(tom_get_slot_ival_value(tomMatch1_394)!=0))?0:1); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_and(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_397=tom_get_slot_and_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_397))) {
int tomMatch1_401=0;
if (!(tomMatch1_401 >= tom_get_size_InstL_InstL(tomMatch1_397))) {
Instruction tomMatch1_405=tom_get_element_InstL_InstL(tomMatch1_397,tomMatch1_401);
if (tom_is_sort_Inst(tomMatch1_405)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_405))) {
int tomMatch1_402=tomMatch1_401 + 1;
if (!(tomMatch1_402 >= tom_get_size_InstL_InstL(tomMatch1_397))) {
Instruction tomMatch1_408=tom_get_element_InstL_InstL(tomMatch1_397,tomMatch1_402);
if (tom_is_sort_Inst(tomMatch1_408)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_408))) {
if (tomMatch1_402 + 1 >= tom_get_size_InstL_InstL(tomMatch1_397)) {
return 
tom_make_ival(((tom_get_slot_ival_value(tomMatch1_405))&(tom_get_slot_ival_value(tomMatch1_408)))); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_or(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_411=tom_get_slot_or_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_411))) {
int tomMatch1_415=0;
if (!(tomMatch1_415 >= tom_get_size_InstL_InstL(tomMatch1_411))) {
Instruction tomMatch1_419=tom_get_element_InstL_InstL(tomMatch1_411,tomMatch1_415);
if (tom_is_sort_Inst(tomMatch1_419)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_419))) {
int tomMatch1_416=tomMatch1_415 + 1;
if (!(tomMatch1_416 >= tom_get_size_InstL_InstL(tomMatch1_411))) {
Instruction tomMatch1_422=tom_get_element_InstL_InstL(tomMatch1_411,tomMatch1_416);
if (tom_is_sort_Inst(tomMatch1_422)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_422))) {
if (tomMatch1_416 + 1 >= tom_get_size_InstL_InstL(tomMatch1_411)) {
return 
tom_make_ival(((tom_get_slot_ival_value(tomMatch1_419))|(tom_get_slot_ival_value(tomMatch1_422)))); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_xor(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_425=tom_get_slot_xor_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_425))) {
int tomMatch1_429=0;
if (!(tomMatch1_429 >= tom_get_size_InstL_InstL(tomMatch1_425))) {
Instruction tomMatch1_433=tom_get_element_InstL_InstL(tomMatch1_425,tomMatch1_429);
if (tom_is_sort_Inst(tomMatch1_433)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_433))) {
int tomMatch1_430=tomMatch1_429 + 1;
if (!(tomMatch1_430 >= tom_get_size_InstL_InstL(tomMatch1_425))) {
Instruction tomMatch1_436=tom_get_element_InstL_InstL(tomMatch1_425,tomMatch1_430);
if (tom_is_sort_Inst(tomMatch1_436)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_436))) {
if (tomMatch1_430 + 1 >= tom_get_size_InstL_InstL(tomMatch1_425)) {
return 
tom_make_ival(((tom_get_slot_ival_value(tomMatch1_433))^(tom_get_slot_ival_value(tomMatch1_436)))); 
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Inst(tom__arg)) {
if (tom_is_sort_Inst(((Instruction)tom__arg))) {
if (tom_is_fun_sym_mux(((Instruction)((Instruction)tom__arg)))) {
 EList<Instruction>  tomMatch1_439=tom_get_slot_mux_children(((Instruction)tom__arg));
if (tom_is_fun_sym_InstL((( EList<Instruction> )tomMatch1_439))) {
int tomMatch1_443=0;
if (!(tomMatch1_443 >= tom_get_size_InstL_InstL(tomMatch1_439))) {
Instruction tomMatch1_448=tom_get_element_InstL_InstL(tomMatch1_439,tomMatch1_443);
if (tom_is_sort_Inst(tomMatch1_448)) {
if (tom_is_fun_sym_ival(((Instruction)tomMatch1_448))) {
int tomMatch1_444=tomMatch1_443 + 1;
if (!(tomMatch1_444 >= tom_get_size_InstL_InstL(tomMatch1_439))) {
int tomMatch1_445=tomMatch1_444 + 1;
if (!(tomMatch1_445 >= tom_get_size_InstL_InstL(tomMatch1_439))) {
if (tomMatch1_445 + 1 >= tom_get_size_InstL_InstL(tomMatch1_439)) {

if((
tom_get_slot_ival_value(tomMatch1_448))!=0) {
return 
tom_get_element_InstL_InstL(tomMatch1_439,tomMatch1_444);
} else {
return 
tom_get_element_InstL_InstL(tomMatch1_439,tomMatch1_445);
} 

}
}
}
}
}
}
}
}
}
}
}
}
return _visit_Inst(tom__arg,introspector);
}
}
private static  tom.library.sl.Strategy  tom_make_Simplify() { 
return new Simplify();
}


public static class GecosAlgebraSymplifyPass extends AbstractTomTransformationPass {

public GecosAlgebraSymplifyPass(Block block) {
super(block);
}
public GecosAlgebraSymplifyPass(Procedure p) {
super(p);
}

@Override
public void compute() {
super.compute();
}

@Override
protected AbstractInstructionTransformation getTransformation() {
return new GenericInstructionAlgebraExpressionSimplification();
}
}
}
