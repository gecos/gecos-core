package templates.xtend

import gecos.annotations.PragmaAnnotation
import gecos.blocks.CompositeBlock
import org.eclipse.emf.ecore.EObject
import gecos.annotations.CommentAnnotation
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory

class GecosAnnotationTemplate extends GecosTemplate {

	def dispatch generate(PragmaAnnotation pragmaAnnotation) {
		val buffer = new StringBuffer()
		if (pragmaAnnotation !== null && pragmaAnnotation.content !== null) {
			if (pragmaAnnotation.eContainer.eContainer.eContainer.eContainer instanceof CompositeBlock) {
				var position = (pragmaAnnotation.eContainer.eContainer.eContainer.eContainer as CompositeBlock).children.indexOf(pragmaAnnotation.eContainer.eContainer.eContainer)
				if (position > 0 && (pragmaAnnotation.eContainer.eContainer.eContainer.eContainer as CompositeBlock).children.get(position-1) instanceof CompositeBlock)
					buffer.append('''
					
					''')
			}
			for(String string : pragmaAnnotation.content)
				if (string.startsWith(GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION))
					buffer.append('''«string.substring(GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION.length)»
				''')
				else if(string.startsWith(GecosUserAnnotationFactory.CODEGEN_COPYFILE_ANNOTATION)) {
				}
				else {
					// Line return to avoid adding annotation directly after an else
					buffer.append('''
						
						#pragma «string»
					''')
				}
		}
		'''«buffer»'''
	}

	def dispatch generate(CommentAnnotation comment) {
//		val buffer = new StringBuffer();
		return "/* "+comment.content +" */"
		
	}
	def dispatch generate(EObject object) {
		null
	}
}