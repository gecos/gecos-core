int main() {
	static int res[4] = {1, 2, 3, 5};

	#pragma outline
	{
		res[3] = 4;
	}

	if(res[3] != 4)
		abort();

	return 0;
}
