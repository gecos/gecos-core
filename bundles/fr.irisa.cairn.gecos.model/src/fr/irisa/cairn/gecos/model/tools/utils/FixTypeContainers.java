package fr.irisa.cairn.gecos.model.tools.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import fr.irisa.cairn.gecos.model.tools.switches.DefaultTypeSwitch;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.core.ITypedElement;
import gecos.core.ProcedureSet;
import gecos.core.Scope;
import gecos.gecosproject.GecosProject;
import gecos.types.Type;

public class FixTypeContainers extends DefaultTypeSwitch<Object>{

	GecosProject project;
	ProcedureSet ps;
	Scope scope;
	
	@Override
	public Object caseType(Type object) {
		object.installOn(ps.getScope());
		
		return super.caseType(object);
	}


	public FixTypeContainers(GecosProject project) {
		this.project = project;
	}

	public FixTypeContainers(ProcedureSet project) {
		this.ps= project;
	}

	public void compute() throws FileNotFoundException, IOException {
//		System.out.println("Fixing Type containement in Project "+project);
		if(project!=null) {
			// FIXME : Improper support for nested types (Arrays/struct/functionType)
			for(ProcedureSet _ps : project.listProcedureSets()) {
				ps= _ps;
				fixProcSet();
			} 
		} else {
			fixProcSet();
		}
	}

	private void fixProcSet() {
		List<ITypedElement> typedElmnts = EMFUtils.<ITypedElement>eAllContentsInstancesOf(ps, ITypedElement.class);
		for (ITypedElement typedElmnt : typedElmnts) {
			Type type = typedElmnt.getType();
			if(type==null) {
				System.err.println("Warning : no type associated to "+typedElmnt);
			} else {
				if (type.getContainingScope()==null) {
					type.installOn(ps.getScope());
				}
			}
		}
			
	}
	
}