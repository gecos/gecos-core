package fr.irisa.cairn.gecos.model.extensions.visitors;

import java.lang.reflect.Constructor;

/**
 * Factory of the adapters registered in the {@link GecosVisitorsRegistry}.
 * 
 * @author Antoine Floc'h - Initial contribution and API
 * 
 */
public class VisitorAdapterFactory {

	public <Visitor> GecosVisitorAdapter<Visitor> buildAdapter(
			Class<? extends GecosVisitorAdapter<Visitor>> adapterClass,
			Visitor adaptable) {

		Constructor<GecosVisitorAdapter<Visitor>> constructor = getDefaultConstructor(
				adapterClass, adaptable.getClass());
		try {
			GecosVisitorAdapter<Visitor> adapter = constructor
					.newInstance(adaptable);
			return adapter;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private <Visitor> Constructor<GecosVisitorAdapter<Visitor>> getDefaultConstructor(
			Class<? extends GecosVisitorAdapter<Visitor>> adapterClass,
			Class adaptableClass) {
		Constructor<GecosVisitorAdapter<Visitor>>[] declaredConstructors = (Constructor<GecosVisitorAdapter<Visitor>>[]) adapterClass
				.getDeclaredConstructors();
		for (Constructor<GecosVisitorAdapter<Visitor>> constructor : declaredConstructors) {
			Class<?>[] parameterTypes = constructor.getParameterTypes();
			if (parameterTypes.length == 1) {
				if (parameterTypes[0] == adaptableClass) {
					return constructor;
				}
			}
		}
		throw new IllegalArgumentException(
				"No matching constructor in the adapter ("
						+ adapterClass.getCanonicalName() + ")");
	}

	// public Class[] getAdapterList() {
	// List<Class> registeredAdapters = registry.getRegisteredAdapters();
	// Class[] array = new Class[registeredAdapters.size()];
	// for (int i = 0; i < array.length; i++) {
	// array[i] = registeredAdapters.get(i);
	// }
	// return array;
	// }

}
