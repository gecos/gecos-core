int a, b, c, d;

#pragma gcs_inline=yes
int add(int a, int b){
	return a+b;
}

int mult(int a, int b){
	return a*b;
}

void main(){
	a = 9;
	b = 6;
	c = add(a, b);
	d = mult(a, b);

	b = add(a, d);

#pragma gcs_inline
	a = mult(d, c);
}
