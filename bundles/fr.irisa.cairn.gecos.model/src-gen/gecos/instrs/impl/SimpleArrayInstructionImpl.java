/**
 */
package gecos.instrs.impl;

import gecos.core.Symbol;

import gecos.instrs.InstrsPackage;
import gecos.instrs.InstrsVisitor;
import gecos.instrs.Instruction;
import gecos.instrs.SimpleArrayInstruction;
import gecos.instrs.SymbolInstruction;

import gecos.types.ArrayType;
import gecos.types.PtrType;
import gecos.types.Type;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.ExclusiveRange;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simple Array Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SimpleArrayInstructionImpl extends ArrayInstructionImpl implements SimpleArrayInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimpleArrayInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstrsPackage.eINSTANCE.getSimpleArrayInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol getSymbol() {
		Symbol _xblockexpression = null;
		{
			final Instruction si = this.getDest();
			Symbol _xifexpression = null;
			if ((si instanceof SymbolInstruction)) {
				_xifexpression = ((SymbolInstruction)si).getSymbol();
			}
			else {
				_xifexpression = null;
			}
			_xblockexpression = _xifexpression;
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSymbol(final Symbol newSymbol) {
		final Instruction si = this.getDest();
		if ((si instanceof SymbolInstruction)) {
			((SymbolInstruction)si).setSymbol(newSymbol);
		}
		else {
			throw new UnsupportedOperationException("Not yet Implemented");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDest(final Instruction newDest) {
		if ((newDest instanceof SymbolInstruction)) {
			super.setDest(newDest);
		}
		else {
			String _simpleName = this.getClass().getSimpleName();
			String _plus = ("Illegal use of " + _simpleName);
			String _plus_1 = (_plus + ".setDest() : target must be a symbol not a compex instrcytion such as ");
			String _plus_2 = (_plus_1 + newDest);
			throw new UnsupportedOperationException(_plus_2);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type computeType() {
		Symbol _symbol = this.getSymbol();
		boolean _tripleEquals = (_symbol == null);
		if (_tripleEquals) {
			throw new UnsupportedOperationException((("SimpleArrayInstruction " + this) + " doesn\'t have any symbol!"));
		}
		int nb_index = 0;
		EList<Instruction> _index = this.getIndex();
		boolean _tripleNotEquals = (_index != null);
		if (_tripleNotEquals) {
			nb_index = this.getIndex().size();
		}
		if ((nb_index == 0)) {
			return this.getSymbol().getType();
		}
		Type _type = this.getSymbol().getType();
		if ((_type instanceof ArrayType)) {
			Type _type_1 = this.getSymbol().getType();
			final ArrayType symType = ((ArrayType) _type_1);
			final int nb_dim = symType.getNbDims();
			if ((nb_index > nb_dim)) {
				throw new UnsupportedOperationException((((("Accessing " + Integer.valueOf(nb_dim)) + " dimensions Array using ") + Integer.valueOf(nb_index)) + " indexes!"));
			}
			Type base = symType.getBase();
			ExclusiveRange _doubleDotLessThan = new ExclusiveRange(1, nb_index, true);
			for (final Integer i : _doubleDotLessThan) {
				base = ((ArrayType) base).getBase();
			}
			return base;
		}
		else {
			Type _type_2 = this.getSymbol().getType();
			if ((_type_2 instanceof PtrType)) {
				Type _type_3 = this.getSymbol().getType();
				final PtrType symType_1 = ((PtrType) _type_3);
				final int nb_dim_1 = symType_1.getNbDims();
				if ((nb_index > nb_dim_1)) {
					throw new UnsupportedOperationException((((("Accessing " + Integer.valueOf(nb_dim_1)) + " dimensions Array using ") + Integer.valueOf(nb_index)) + " indexes!"));
				}
				Type base_1 = symType_1.getBase();
				ExclusiveRange _doubleDotLessThan_1 = new ExclusiveRange(1, nb_index, true);
				for (final Integer i_1 : _doubleDotLessThan_1) {
					base_1 = ((PtrType) base_1).getBase();
				}
				return base_1;
			}
		}
		Type _type_4 = this.getSymbol().getType();
		String _plus = ((("SimpleArrayInstruction " + this) + " with symbol type ") + _type_4);
		String _plus_1 = (_plus + " unhandeled !");
		throw new UnsupportedOperationException(_plus_1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void accept(final InstrsVisitor visitor) {
		visitor.visitSimpleArrayInstruction(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol getUsedSymbol() {
		return this.getSymbol();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		Symbol _symbol = this.getSymbol();
		boolean _tripleNotEquals = (_symbol != null);
		if (_tripleNotEquals) {
			String _name = this.getSymbol().getName();
			String _plus = ("" + _name);
			final StringBuffer sb = new StringBuffer(_plus);
			try {
				EList<Instruction> _index = this.getIndex();
				boolean _tripleNotEquals_1 = (_index != null);
				if (_tripleNotEquals_1) {
					EList<Instruction> _index_1 = this.getIndex();
					for (final Instruction i : _index_1) {
						if ((i != null)) {
							sb.append("[").append(i).append("]");
						}
						else {
							sb.append("[null]");
						}
					}
				}
			}
			catch (final Throwable _t) {
				if (_t instanceof Exception) {
					sb.append("[#error#]");
				}
				else {
					throw Exceptions.sneakyThrow(_t);
				}
			}
			return sb.toString();
		}
		else {
			return super.toString();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArrayType getTargetArrayType() {
		Type _type = this.getSymbol().getType();
		if ((_type instanceof ArrayType)) {
			Type _type_1 = this.getSymbol().getType();
			return ((ArrayType) _type_1);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSame(final Instruction instr) {
		if ((instr instanceof SimpleArrayInstruction)) {
			return (super.isSame(instr) && (this.getSymbol() == ((SimpleArrayInstruction)instr).getSymbol()));
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void updateSymbol(final Symbol originalSymbol, final Symbol newSymbol) {
		Symbol _usedSymbol = this.getUsedSymbol();
		boolean _tripleEquals = (_usedSymbol == originalSymbol);
		if (_tripleEquals) {
			this.setSymbol(newSymbol);
		}
		else {
			throw new IllegalArgumentException("Given Symbol is not used.");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSymbolName() {
		return this.getUsedSymbol().getName();
	}

} //SimpleArrayInstructionImpl
