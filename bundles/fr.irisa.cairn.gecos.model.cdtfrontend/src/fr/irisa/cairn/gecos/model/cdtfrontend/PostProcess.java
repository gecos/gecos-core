package fr.irisa.cairn.gecos.model.cdtfrontend;

import fr.irisa.cairn.gecos.model.tools.visitors.GecosDefaultVisitor;
import gecos.blocks.BasicBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.WhileBlock;
import gecos.gecosproject.GecosProject;
import gecos.instrs.SetInstruction;


/**
 * 
 * PostProcess the AST:
 * <ul>
 *   <li>splits chains of SetInstructions into several instructions; For instance, "a = b = c;" becomes "b = c; a = b;".</li>
 * </ul>
 * 
 * @author amorvan
 *
 */
public class PostProcess {
	
	public static void process(GecosProject project) {
		project.accept(new RemoveChainedSets());
	}
	
	static class RemoveChainedSets  extends GecosDefaultVisitor {
		BasicBlock currentBB = null;
		public RemoveChainedSets() {
			super(true,true,true);
		}
		
		@Override
		public void visitBasicBlock(BasicBlock b) {
			
			currentBB = b;
			super.visitBasicBlock(b);
			currentBB = null;
		}
		
		@Override
		public void visitIfBlock(IfBlock i) {
//			i.getTestBlock().accept(this);
			i.getThenBlock().accept(this);
			if (i.getElseBlock() != null)
				i.getElseBlock().accept(this);
		}
		
		@Override
		public void visitForBlock(ForBlock f) {
//			f.getInitBlock().accept(this);
//			f.getTestBlock().accept(this);
//			f.getStepBlock().accept(this);
			if (f.getBodyBlock() != null)
				f.getBodyBlock().accept(this);
		}
		
		@Override
		public void visitWhileBlock(WhileBlock w) {
//			w.getTestBlock().accept(this);
			w.getBodyBlock().accept(this);
		}
		
		@Override
		public void visitLoopBlock(DoWhileBlock l) {
			l.getBodyBlock().accept(this);
//			l.getTestBlock().accept(this);
		}
		
		@Override
		public void visitSetInstruction(SetInstruction s) {
			super.visitSetInstruction(s);
			if (s.getParent() != null && currentBB != null) {
				int i = currentBB.getInstructions().indexOf(s.getRoot());
				s.getParent().replaceChild(s, s.getDest().copy());
				currentBB.getInstructions().add(i, s);
			}
		}
	}
}
