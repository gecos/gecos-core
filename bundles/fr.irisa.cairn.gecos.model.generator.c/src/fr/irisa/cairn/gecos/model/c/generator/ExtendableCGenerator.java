/*******************************************************************************
 * Copyright (c) 2012 Universite de Rennes 1 / Inria.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the FreeBSD License v1.0
 * which accompanies this distribution, and is available at
 * http://www.freebsd.org/copyright/freebsd-license.html
 *
 * Contributors:
 *    DERRIEN Steven - initial API and implementation
 *    MORVAN Antoine - initial API and implementation
 *    NAULLET Maxime - initial API and implementation
 *******************************************************************************/
package fr.irisa.cairn.gecos.model.c.generator;

import templates.xtend.GecosCoreTemplate;
import fr.irisa.cairn.gecos.model.extensions.generators.ExtendableGenerator;
import fr.irisa.cairn.gecos.model.extensions.generators.IGeneratorDispatchStrategy;

public class ExtendableCGenerator extends ExtendableGenerator{

	public static ExtendableCGenerator eInstance = new ExtendableCGenerator();
	
	public ExtendableCGenerator() {
		super(new GecosCoreTemplate());
	}
	
	public ExtendableCGenerator(IGeneratorDispatchStrategy strategy) {
		super(strategy,new GecosCoreTemplate());
	}
}
