package fr.irisa.cairn.gecos.core.testframework.impl;

import static fr.irisa.cairn.gecos.core.testframework.impl.GSGenerators.cGenerator;
import static fr.irisa.cairn.gecos.core.testframework.impl.GSGenerators.codeCopier;
import static fr.irisa.cairn.gecos.core.testframework.impl.GSOperators.defaultDataConvertor;
import static fr.irisa.cairn.gecos.core.testframework.impl.GSOperators.versionCopier;
import static fr.irisa.cairn.gecos.testframework.s2s.Operators.makefileExecutor;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.COMPILE;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.LINK;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.RUN;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.NOP;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.convert;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.forEach;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.forEach2;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.transform;

import java.util.function.Function;
import java.util.function.Predicate;

import fr.irisa.cairn.gecos.testframework.data.CxxFileData;
import fr.irisa.cairn.gecos.testframework.data.CxxFilesGroupData;
import fr.irisa.cairn.gecos.testframework.data.CxxMakefileData;
import fr.irisa.cairn.gecos.testframework.data.ICxxProjectData;
import fr.irisa.cairn.gecos.testframework.model.AbstractTestTemplate;
import fr.irisa.cairn.gecos.testframework.model.IVersion;
import fr.irisa.cairn.gecos.testframework.s2s.Compilers;
import fr.irisa.cairn.gecos.testframework.s2s.Linkers;
import fr.irisa.cairn.gecos.testframework.s2s.Operators;
import fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow;
import fr.irisa.cairn.gecos.testframework.s2s.TestFlow;

public class GSDefaultTestFlows {
	
	private GSDefaultTestFlows() {}	
	

	/**
	 * Create a new test flow with default support for {@link CxxFileData}.
	 * 
	 * @return a new test flow
	 */
	public static TestFlow cxxFileDataTestFlow() {
		return S2STestFlow.builder()
			.convert(convert(defaultDataConvertor(GSProjectVersion::new)))
			.transform(transform(versionCopier(GSProjectVersion::new)))
			.then(forEach2((t,v) -> v.setCodegenDir(t.getTestMethodOutputDir().resolve(v.getName()))))
			.check(forEach(GSCheckers.danglingRef()))
			.codegen(forEach(IVersion::isRoot, codeCopier(false), cGenerator(false)))
			.compile(forEach(Compilers.gcc()))
			.link(forEach(Linkers.gcc()))
			.run(forEach(Operators.defaultExecutorNoArgs()))
			.build();
	}
	
	/**
	 * Create a new test flow with default support for {@link CxxMakefileData}.
	 * 
	 * @return a new test flow
	 */
	public static TestFlow cxxMakefileDataTestFlow(
			Function<GSProjectVersion, String> makefileCompileTarget,
			Function<GSProjectVersion, String> makefileRunTarget) {
		return cxxFileDataTestFlow()
				.replaceStage(COMPILE, NOP)
				.replaceStage(LINK, forEach(makefileExecutor(IVersion::getMakefile, makefileCompileTarget)))
				.replaceStage(RUN, forEach(makefileExecutor(IVersion::getMakefile, makefileRunTarget)));
	}
	
	/**
	 * Create a new test flow with default support for {@link CxxFilesGroupData}.
	 * 
	 * @return a new test flow
	 */
	@SuppressWarnings("rawtypes")
	public static TestFlow cxxFilesGroupDataTestFlow(
			Function<GSProjectVersion, String> makefileCompileTarget,
			Function<GSProjectVersion, String> makefileRunTarget) {
		Predicate<Object> cond = t -> ((ICxxProjectData)((AbstractTestTemplate)t).getData()).getMakefile() != null;
		return cxxFileDataTestFlow()
			.replaceStageIf(COMPILE, NOP, cond)
			.replaceStageIf(LINK, forEach(makefileExecutor(IVersion::getMakefile, makefileCompileTarget)), cond)
			.replaceStageIf(RUN, forEach(makefileExecutor(IVersion::getMakefile, makefileRunTarget)), cond);
	}
	
}
