package fr.irisa.cairn.gecos.model.dag.adapter.module;

import fr.irisa.cairn.gecos.model.dag.adapter.providers.DagCommutativityProvider;
import fr.irisa.cairn.gecos.model.dag.adapter.providers.DagEquivalenceProvider;
import fr.irisa.cairn.gecos.model.dag.adapter.providers.DagLatencyProvider;
import fr.irisa.cairn.gecos.model.dag.adapter.providers.DagSynchronizedCloner;
import fr.irisa.cairn.graph.guice.modules.providersbinding.AnalysisProviderModule;
import fr.irisa.cairn.graph.providers.ICloneProvider;
import fr.irisa.cairn.graph.providers.ICommutativityProvider;
import fr.irisa.cairn.graph.providers.IEquivalenceProvider;
import fr.irisa.cairn.graph.providers.ILatencyProvider;

public class DagAnalysisProvidersModule extends AnalysisProviderModule {

	@Override
	protected void bindICloneProvider() {
		this.bind(ICloneProvider.class).to(DagSynchronizedCloner.class);
	}

	@Override
	protected void bindILatencyProvider() {
		this.bind(ILatencyProvider.class).to(DagLatencyProvider.class);
	}
	
	@Override
	protected void bindIEquivalenceProvider() {
		this.bind(IEquivalenceProvider.class).to(DagEquivalenceProvider.class);
	}
	
	@Override
	protected void bindICommutativityProvider() {
		this.bind(ICommutativityProvider.class).to(DagCommutativityProvider.class);
	}
}
