#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <typeinfo>

#include "ProfilingEngine.h"

#ifndef TYPE
#  define TYPE double
#endif


template<typename T> 
void test(T seed) {
	T s = seed+11;
	T a1[10]; // = {seed+1,seed+2,seed+3,seed+4,seed+5,seed+6,seed+7,seed+8,seed+9};
	T a3[2][3][2] = {seed+12,seed+11,seed+10,seed+9,seed+8,seed+7,seed+6,seed+5,seed+4,seed+3,seed+2,seed+1};
	T *p1 = a1;
	T (*pa1)[10] = &a1;
	T **p2 = (T**) malloc(3*sizeof(T*));
	p2[0] = a1;
	p2[1] = a1;
	
	
	{
		ProfilingEngine_initInjector_RandomSeed(20+seed);
		ProfilingEngine_initInjector_RandomUniformDouble(-10, 20);
		for(int i = 0; i < 10; i++)
			a1[i] = ProfilingEngine_injectValue();
	}



	{
		ProfilingEngine_pushValue(0, "s", s);
		ProfilingEngine_saveSnapshot(0);
	}

	{
		for(int i = 0; i < 10; i++)
			ProfilingEngine_pushValue(1, "a1", a1[i]);
		ProfilingEngine_saveSnapshot(1);
	}

	{
		for(int i = 0; i < 12; i++)
			ProfilingEngine_pushValue(2, "a3", ((T*) a3)[i]);
		ProfilingEngine_saveSnapshot(2);
	}

	{
		for(int i = 0; i < 10; i++)
			ProfilingEngine_pushValue(3, "p1", p1[i]);
		ProfilingEngine_saveSnapshot(3);
	}

	{
		for(int i = 0; i < 10; i++)
			ProfilingEngine_pushValue(4, "pa1", pa1[0][i]);
		ProfilingEngine_saveSnapshot(4);
	}

	{
		// segment 1
		for(int i = 0; i < 10; i++)
			ProfilingEngine_pushValue(5, "p2", p2[0][i]);
		// segment 2
		for(int i = 0; i < 10; i++)
			ProfilingEngine_pushValue(5, "p2", p2[1][i]);
		ProfilingEngine_saveSnapshot(5);
	}

}


int main(int argc, char * argv[]) {

	ProfilingEngine_start();

	test<TYPE>(1);
	test<TYPE>(5);

	ProfilingEngine_finalize("result/");
	return 0;
}

