package fr.irisa.cairn.gecos.model.utils.callgraph;

import java.io.FileNotFoundException;

import fr.irisa.r2d2.gecos.framework.modules.ui.ShowDotty;
import gecos.core.ProcedureSet;

public class ShowCallGraph extends CallGraphBuilder {

	public ShowCallGraph(ProcedureSet procset)  {
	 		super(procset);
	}

	@Override
	public CallGraph compute()  {
		// TODO Auto-generated method stub
		CallGraph res = super.compute();
		try {
			CallGraphDottyExport exporter;
			exporter = new CallGraphDottyExport(res, "cg.dot");
			exporter.visitCallGraph(res);
			ShowDotty shower = new ShowDotty("cg.dot");
			shower.compute();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}
	
	

}
