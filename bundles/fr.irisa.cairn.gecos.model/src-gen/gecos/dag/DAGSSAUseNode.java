/**
 */
package gecos.dag;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DAGSSA Use Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.dag.DAGSSAUseNode#getDef <em>Def</em>}</li>
 * </ul>
 *
 * @see gecos.dag.DagPackage#getDAGSSAUseNode()
 * @model
 * @generated
 */
public interface DAGSSAUseNode extends DAGSymbolNode, DagVisitable {
	/**
	 * Returns the value of the '<em><b>Def</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Def</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Def</em>' reference.
	 * @see #setDef(DAGSSADefNode)
	 * @see gecos.dag.DagPackage#getDAGSSAUseNode_Def()
	 * @model
	 * @generated
	 */
	DAGSSADefNode getDef();

	/**
	 * Sets the value of the '{@link gecos.dag.DAGSSAUseNode#getDef <em>Def</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Def</em>' reference.
	 * @see #getDef()
	 * @generated
	 */
	void setDef(DAGSSADefNode value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model visitorUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='visitor.visitDAGSSAUseNode(this);'"
	 * @generated
	 */
	void accept(DagVisitor visitor);

} // DAGSSAUseNode
