package fr.irisa.cairn.gecos.model.cdtfrontend.exception;

import org.eclipse.cdt.core.dom.ast.IASTNode;

/**
 * This exception should be thrown when a parsing error occurs.
 * 
 * @author aelmouss
 */
public class CDTParserErrorException extends CDTParserProblemException {

	private static final long serialVersionUID = 636217977115718006L;
	
	public CDTParserErrorException(IASTNode node, String reason) {
		super(node, reason);
	}
	public CDTParserErrorException(IASTNode node, String reason, Exception e) {
		super(node, reason, e);
	}
	
}
